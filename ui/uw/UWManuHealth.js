//程序名称：UWManuHealth.js
//程序功能：新契约人工核保体检资料录入
//创建日期：2004-11-19 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var mSQL;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var InsureNo=fm.InsureNo.value;
  if(fm.InsureNo.value=="")
  {
			alert("体检人信息不能为空");
			return false;
	} 
	
  if(HealthGrid.mulLineCount==0)
  {
			alert("体检项目不能为空");
			return false;
	}	 	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action= "./UWManuHealthSave.jsp";
  ChangeDecodeStr();
  fm.submit(); //提交
}

//删除操作
function deleteClick(){      
	  var i = 0;                                                     
  var checkFlag = 0; 
  var mOperate="";                                              
  var state = "0";                                                 
                                                                   
  for (i=0; i<HistroyGrid.mulLineCount; i++)                          
  {                                         
    if (HistroyGrid.getSelNo(i))               
    {                                       
      checkFlag = HistroyGrid.getSelNo();      
      break;                                
    }                                       
  }                                         
	
  if (checkFlag)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            	
  {
    var	Prtseq = HistroyGrid.getRowColData(checkFlag - 1, 5);                                                                                                                                                                                                                                                                                                                                                                                                                                                                 	                                                                                                                                                                                                                                                                                                                                                                                                                                                     	                                                                                                                                                                                                                                                                                                                                                                                                                                                     	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     	                                                                                                                                                                                                                                                                                                                                                                                                                                               	
    var strReturn="1";                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              	
    sFeatures = ""; 
    
    var tStrSql = "select 1 from LCPENotice lcpen "
  	        + " where lcpen.prtseq ='" + Prtseq + "'"
  	        + " and not exists (select 1 from lwmission where missionprop4 = '" + Prtseq + "' "
  	        + " and missionid = '" + tMissionID + "' "
  	        + " and activityid = '0000001106') "
  	        + " and not exists (select 1 from lbmission where missionprop3 = '" + Prtseq + "' "
  	        + " and missionid = '" + tMissionID + "' "
  	        + " and activityid = '0000001106') ";
  	var tTmpFlag = easyExecSql(tStrSql);
  	if(tTmpFlag != "1")
  	{
  	    alert("该体检件已经下发,不能删除！");                                                                                                                                                                                                                                                                                                                                                                                	
	    return false;
  	}
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       	                                                                                                                                                                                                                                                                                                                                                                                                                                  	
    var strSql = "select * from es_doc_main  where doccode = '"+ Prtseq +"' ";                                                                                                                                                                                                                                                                                                                                                                                                                                 	
	  var strmSql = "select * from lcpenotice where pestate='01' and prtseq ='" + Prtseq + "'";
//	  var mSQL = "select prtseq from LCPENotice where name='" + InsureNo + "'";
	  var arrResult = easyExecSql(strSql); 
	  var arrmResult = easyExecSql(strmSql);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     	
	  if (arrResult!=null && arrResult[0][1]!=operator) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                     	
	    alert("该体检件已经扫描,不能删除！");                                                                                                                                                                                                                                                                                                                                                                                	
	    return;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               	
	  }
	else  if (arrmResult!=null){
		 alert("该体检件已经预约不能删除！");
		 return;
		 }
		else if
			(confirm("您确实想删除该记录吗?"))
		{
		 var i = 0;                                                                                                                                                                                                                                                                                                                                              
 	 	 var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";                                                                                                                                                                                                                                                                                
 	 	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;                                                                                                                                                                                                                                                                                
 	 	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
// 	   fm.all("fmAction").value = ""; 
// 	   fm.all("DelFlag").value="1";  //flag 为1 删除动作    
//	   document.fm.hideOperate.value = "DELETE||MAIN";    
//	   fm.fmtransact.value = "DELETE||MAIN"; 
//    var strPrtseq = Prtseq;  //印刷号	                                                                                                                                                                                                                                    	
 	   fm.action= "./UWManuHealthCancel.jsp?strPrtseq="+Prtseq;   
     fm.submit(); //提交                 
//		 mOperate="DELETE||MAIN";     
			return;
			}   
		 else {
		 	 alert("您取消了删除操作！");
		 	 }	 	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    	
   }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           	
  else                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      	
  {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         	
    alert("请先选择一条记录！");                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        	
  }	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        	 	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               	
	}

//下发体检通知书
function sendHealth()
{
  //var InsureNo=fm.InsureNo.value;
  //if(fm.InsureNo.value==""){
	//	alert("体检人信息不能为空");
	//	return false;
	//} 
	
  //if(HealthGrid.mulLineCount==0){
	//	alert("体检项目不能为空");
	//	return false;
	//}	 
	
	if(HistroyGrid.mulLineCount == 0)
	{
	  alert("请录体检信息");
	  return false;
	}
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action= "./UWManuHealthChk.jsp";
  ChangeDecodeStr();
  fm.submit(); //提交
}
 
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  UnChangeDecodeStr();
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
    showHistoryPEInfo();
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function manuchkhealthmain()
{
  fm.submit();
}

/*********************************************************************
 *  执行待新契约体检通知书的EasyQuery
 *  描述:查询显示对象是体检通知书
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClickSingle()
{
  // 书写SQL语句
  var strsql = "";
  var tContNo = "";
  var tEdorNo = "";
  tContNo = fm.ContNo.value;
  tMissionID = fm.MissionID.value;
  tInsuredNo = fm.InsureNo.value;
//	alert(tContNo);
  if(tContNo != "" && tInsuredNo != "")
  {

    strsql = "select LWMission.SubMissionID from LWMission where 1=1"
             + " and LWMission.MissionProp2 = '"+ tContNo + "'"
             + " and LWMission.ActivityID = '0000001101'"
             + " and LWMission.ProcessID = '0000000003'"
             + " and LWMission.MissionID = '" +tMissionID +"'";

    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult)
    {
      alert("不容许录入新体检通知书！");
      return "";
    }

    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.SubMissionID.value = turnPage.arrDataCacheSet[0][0];
    turnPage = new turnPageClass();
    strsql = "select LCPENotice.ContNo,LCPENotice.name,LCPENotice.pedate,LCPENotice.peaddress,LCPENotice.PEBeforeCond,LCPENotice.remark,LCPENotice.printflag from LCPENotice where 1=1"
             + " and LCPENotice.ContNo = '"+ tContNo + "'"
             + " and LCPENotice.customerno = '"+ tInsuredNo + "'";

    //查询SQL，返回结果字符串
    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult)
    {
      //alert("查询失败！");
      easyQueryClickInit();
      return "";
    }

    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    fm.ContNo.value = turnPage.arrDataCacheSet[0][0];
    //fm.InsureNo.value = turnPage.arrDataCacheSet[0][1];
    //fm.EDate.value = turnPage.arrDataCacheSet[0][2];
    //fm.Hospital.value = turnPage.arrDataCacheSet[0][3];
    fm.IfEmpty.value = turnPage.arrDataCacheSet[0][4];
    //fm.Note.value = turnPage.arrDataCacheSet[0][5];
    //fm.PrintFlag.value = turnPage.arrDataCacheSet[0][6];

    strSQL = "select a.contno,a.uwno,a.uwerror,a.modifydate from LCUWError a where 1=1 "
             + " and a.PolNo in (select distinct polno from lcpol where contno ='" +tContNo+ "')"
             + " and SugPassFlag = 'H'"
             + " and (a.uwno = (select max(b.uwno) from LCUWError b where b.ContNo = '" +tContNo + "' and b.PolNo = a.PolNo))"
             + " union "
             + "select c.contno,c.uwno,c.uwerror,c.modifydate from LCCUWError c where 1=1"
             + " and c.ContNo ='" + tContNo + "'"
             + " and SugPassFlag = 'H'"
             + " and (c.uwno = (select max(d.uwno) from LCCUWError d where d.ContNo = '" + tContNo + "'))"

             //查询SQL，返回结果字符串
             turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult)
    {
      return "";
    }

    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = UWErrGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strsql;

    //设置查询起始位置
    turnPage.pageIndex       = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

    easyQueryClick();

  }
  return true;
}


// 查询按钮
function easyQueryClick()
{
  // 书写SQL语句
  var strsql = "";
  var tContNo = "";
  var tEdorNo = "";
  tContNo = fm.ContNo.value;
  tInsuredNo = fm.InsureNo.value;


  if(tContNo != "" && tInsuredNo != "")
  {
    strsql = "select peitemcode,peitemname from LCPENoticeItem where 1=1"
             + " and ContNo = '"+ tContNo + "'"
             + " and PrtSeq in (select distinct prtseq from lcpenotice where contno = '"+ tContNo + "'"
             + " and customerno = '"+ tInsuredNo + "')";


    //查询SQL，返回结果字符串
    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult)
    {
      return "";
    }

    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = HealthGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strsql;

    //设置查询起始位置
    turnPage.pageIndex       = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }



  return true;
}


// 无体检资料查询按钮
function easyQueryClickInit()
{
  fm.action= "./UWManuHealthQuery.jsp";
  fm.submit();
}

function displayEasyResult( arrResult )
{
  var i, j, m, n;
  if( arrResult == null )
    alert( "没有找到相关的数据!" );
  else
  {
    arrGrid = arrResult;
    // 显示查询结果
    n = arrResult.length;
    for( i = 0; i < n; i++ )
    {
      m = arrResult[i].length;
      for( j = 0; j < m; j++ )
      {
        HealthGrid.setRowColData( i, j+1, arrResult[i][j] );

      } // end of for
    } // end of for
    //alert("result:"+arrResult);
  } // end of if
}


function initInsureNo(tPrtNo)
{
  var i,j,m,n;
  var returnstr;

  var strSql = "select InsuredNo,'第'||SequenceNo||'被保险人'||' '||name from lcinsured where 1=1 "
               + " and Prtno = '" +tPrtNo + "'"
               + " union select CustomerNo , Name from LCInsuredRelated where polno in (select polno from lcpol where Prtno = '" +tPrtNo+"')";
  //查询SQL，返回结果字符串

  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);

  //判断是否查询成功
  //alert(turnPage.strQueryResult);
  if (turnPage.strQueryResult == "")
  {
    alert("查询失败！");
    return "";
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  var returnstr = "";
  var n = turnPage.arrDataCacheSet.length;
  //alert("N:"+n);
  if (n > 0)
  {
    for( i = 0;i < n; i++)
    {
      m = turnPage.arrDataCacheSet[i].length;
      //alert("M:"+m);
      if (m > 0)
      {
        for( j = 0; j< m; j++)
        {
          if (i == 0 && j == 0)
          {
            returnstr = "0|^"+turnPage.arrDataCacheSet[i][j];
          }
          if (i == 0 && j > 0)
          {
            returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
          }
          if (i > 0 && j == 0)
          {
            returnstr = returnstr+"^"+turnPage.arrDataCacheSet[i][j];
          }
          if (i > 0 && j > 0)
          {
            returnstr = returnstr+"|"+turnPage.arrDataCacheSet[i][j];
          }

        }
      }
      else
      {
        alert("查询失败!!");
        return "";
      }
    }
  }
  else
  {
    alert("查询失败!");
    return "";
  }
  //alert("returnstr:"+returnstr);
  fm.InsureNo.CodeData = returnstr;
  return returnstr;
}

//初始化医院
function initHospital(tContNo)
{
  var i,j,m,n;
  var returnstr;

  var strSql = "select code,codename from ldcode where 1=1 "
               + "and codetype = 'hospitcodeuw'"
               + "and comcode = (select managecom from lccont where ContNo = '"+tContNo+"')";
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);

  //判断是否查询成功
  //alert(turnPage.strQueryResult);
  if (turnPage.strQueryResult == "")
  {
    alert("医院初始化失败！");
    return "";
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  //turnPage.pageDisplayGrid = VarGrid;

  //保存SQL语句
  //turnPage.strQuerySql     = strSql;

  //设置查询起始位置
  //turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  var returnstr = "";
  var n = turnPage.arrDataCacheSet.length;
  //alert("N:"+n);
  if (n > 0)
  {
    for( i = 0;i < n; i++)
    {
      m = turnPage.arrDataCacheSet[i].length;
      //alert("M:"+m);
      if (m > 0)
      {
        for( j = 0; j< m; j++)
        {
          if (i == 0 && j == 0)
          {
            returnstr = "0|^"+turnPage.arrDataCacheSet[i][j];
          }
          if (i == 0 && j > 0)
          {
            returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
          }
          if (i > 0 && j == 0)
          {
            returnstr = returnstr+"^"+turnPage.arrDataCacheSet[i][j];
          }
          if (i > 0 && j > 0)
          {
            returnstr = returnstr+"|"+turnPage.arrDataCacheSet[i][j];
          }

        }
      }
      else
      {
        alert("查询失败!!");
        return "";
      }
    }
  }
  else
  {
    alert("查询失败!");
    return "";
  }
  //alert("returnstr:"+returnstr);
  fm.Hospital.CodeData = returnstr;
  return returnstr;
}

function showNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cContNo=fm.ContNo.value;

  //window.open("./PUWErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  window.open("./UWPEErrMain.jsp?ContNo="+cContNo,"window1");

  showInfo.close();

}
//用于体检医院的查询、录入

//function OpenHospital()
//{
//showInfo=window.open("../healthmanage/LDHospitalQuery.html");
//}
function afterQuery0(arr)
{
  if(arr)
  {
    fm.Hospital.value=arr[0][0];//fm.all('HospitName').value = easyExecSql("select hospitname from ldhospital where hospitcode='"+arr[0][0]+"'");
  }
}
//套餐反显
function afterCodeSelect(CodeName,field)
{;
  if(CodeName=="testgrpname")
  {
//    var strSql = "select distinct TestGrpCode from "
//                 +"LDGrpTest where  "
//                 +" TestGrpName='"+field.value+"'";

	    var strSql = " select distinct a.testitemcode, b.testgrpname, "
                  +" (select distinct c.othersign from ldtestgrpmgt c where c.testgrpcode = a.testitemcode) "
                  +" from   ldtestgrpmgt a, ldtestgrpmgt b"
                  +" where  a.testgrptype = '4'"
                  +" and    a.testgrpcode = '"+field.value+"'"
							  	+" and    b.testgrptype = '3' "
                  +" and    a.testitemcode = b.testgrpcode " 
                  +" order by a.testitemcode"
								  ;
	     
//    var arr = easyExecSql(strSql);
//    var testCode = "";
//    if(arr)
//    {
//      testCode=arr[0][0];
//    }
//    if(testCode!="")
//    {
//      strSql = "select TestItemCode,TestItemCommonName,IfEmpty from LDGrpTest  where "
//               +"   TestGrpCode='"+testCode+"'";
//    }
    try
    {
      turnPage.pageLineNum = 50 ; 


      turnPage.queryModal(strSql, HealthGrid);
//      turnPage.strQueryResult = easyQueryVer3(strSql,1,1,1);



      //设置查询起始位置
//      turnPage.pageIndex = 0;
//      //在查询结果数组中取出符合页面显示大小设置的数组
//      turnPage.pageLineNum = 50 ;
//      //查询成功则拆分字符串，返回二维数组
//      turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
//      //设置初始化过的MULTILINE对象
//      turnPage.pageDisplayGrid = HealthGrid;
//      //保存SQL语句
//      turnPage.strQuerySql = strSql ;
//      arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
//      //调用MULTILINE对象显示查询结果
//      displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
    catch(ex)
    {
      alert(ex);
    }
    //alert(testCode);
    //var strSql = "select medicaitemprice from LDTESTPRICEMGT where hospitcode='"
    //						+fm.Hospital.value+"' and medicaitemcode='"+testCode+"'";
    //var arr = easyExecSql(strSql);
    //if(arr)
    //{
    //	fm.TestFee.value=arr[0][0];
    //}
    showMoney2();
  }
  if(CodeName=="hmchoosetestuw")
  {
    //var	strsql = "select  IfEmpty from LDGrpTest  where 1=1"
    //
    //		  + "  and TestItemCode='"+field.value+"'";
    // var arr = easyExecSql(strsql);
    //
    //var yesno = "";
    //if(arr)
    //{
    //	yesno=arr[0][0];
    //
    //	var count = 0;
    //	count = HealthGrid.mulLineCount-1;
    //
    //	HealthGrid.setRowColData(count,3,yesno);
    //}
    var wherePart = "(";
    for(var i=0 ; i<HealthGrid.mulLineCount ; i++)
    {
      wherePart += "'";
      wherePart += HealthGrid.getRowCowData(i,1);
      wherePart += "'";
      if(i != HealthGrid.mulLineCount-1)
      {
        wherePart += ",";
      }
    }
    alert(wherePart);
    var strSql = "select medicaitemprice from LDTESTPRICEMGT where hospitcode='"
                 +fm.Hospital.value+"' and medicaitemcode='"+wherePart+"'";
    var arr = easyExecSql(strSql);
    if(arr)
    {
       if(trim(arr[0][0])!="null")
         {
            fm.TestFee.value = arr[0][0];
         }
         else
         {
            fm.TestFee.value = "0.0";
         }
    }
    showMoney()
  }

  if(CodeName=="lhhospitnamehtuw")
  {
    //	   	var strSql = "select sum(double(medicaitemprice)) from "
    //	   +"LDTESTPRICEMGT where hospitcode='"+field.value+"' and medicaitemcode in(select testitemcode "
    //	   +" from ldgrptest where testgrpname='"+fm.TestGrpCode.value+"')";
    //	   var arr = easyExecSql(strSql);
    //	   if(arr)
    //	   {
    //	   	fm.TestFee.value = arr[0][0];
    //	   }
    showMoney2();
  }
  if(CodeName == "InsureNo"){
  	var sql = "Select 被保人客户号,"
			+ " 被保人姓名,"
			+ " Substr(年龄, 1, Length(年龄) - 4),"
			+ " (Select Codename"
			+ " From Ldcode"
			+ " Where Codetype = 'sex'"
			+ " And Code = 性别),"
			+ " Decimal(Double(体重) / (Double(身高) * 0.01 * Double(身高) * 0.01), 5, 2),"
			+ " 身高,"
			+ " 体重"
			+ " From (Select Insuredno 被保人客户号,"
			+ " Name 被保人姓名,"
			+ " trim(To_Char(Current Date - Birthday)) 年龄,"
			+ " Sex 性别,"
			+ " (Select Impartparam"
			+ " From Lccustomerimpartparams"
			+ " Where Contno = Ci.Contno"
			+ " And Customerno = Ci.Insuredno"
			+ " And Impartparamname = 'Stature') 身高,"
			+ " (Select Impartparam"
			+ " From Lccustomerimpartparams"
			+ " Where Contno = Ci.Contno"
			+ " And Customerno = Ci.Insuredno"
			+ " And Impartparamname = 'Avoirdupois') 体重"
			+ " From Lcinsured Ci"
			+ " Where Contno = '" + ContNo + "'"
			+ " And Insuredno = '" + field.value + "') As x";
		turnPage.queryModal(sql, CustomerGrid);
		
  }
  if (CodeName == "testreason") {
		if (fm.Reason.value == "09") {
			fm.Note.value = "";
		}
	}
}
function initBooking()
{
  if(PEFlag!=1)
  {
    return;
  }
  var strSql = "select customerno,date(to_date(PEDate)+10),PEAddress,PrintFlag from LCPENotice where "
               +" ProposalContNo='"+ContNo+"' and PrtSeq='"+PrtSeq+"'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    alert("未查询到客户体检信息！\n此问题可能是由于数据库操作造成垃圾数据的原因！");
    return;
  }
  fm.InsureNo.value=arr[0][0];
  fm.all('InsureNoName').value = easyExecSql("select name from LDPerson where CustomerNo='"+ arr[0][0] + "'");
  fm.EDate.value=arr[0][1];
  if(arr[0][2]=="")
  {
    fm.all('HospitName').value=""
                             }
                             else
                             {
                               fm.Hospital.value=arr[0][2];
                               fm.all('HospitName').value = easyExecSql("select hospitname from ldhospital where hospitcode='"+arr[0][2]+"'");
                             }
                             fm.InsureNo.readOnly=true;
  var strSql = "select TestGrpCode from LCPENoticeItem where ProposalContNo='"+ContNo+"' and PrtSeq='"+PrtSeq+"'"
               ;
  //fm.sql123.value=strSql
  var arr = easyExecSql(strSql);
  var testgrpcode=""
                  if(arr)
                  {
                    testgrpcode=arr[0][0]

                              }
                              var strSql = "select a.PEItemCode,a.PEItemName,b.IfEmpty from LCPENoticeItem a,LDGrpTest b where "
                                           +" ProposalContNo='"+ContNo+"' and PrtSeq='"+PrtSeq+"' and a.TestGrpCode= '"+testgrpcode+"'"
                                           +"and a.PEItemCode=b.TestItemCode and a.TestGrpCode=b.TestGrpCode "
                                           +" union select distinct a.PEItemCode,a.PEItemName,b.IfEmpty from LCPENoticeItem a,LDGrpTest b where "
                                           +" ProposalContNo='"+ContNo+"' and PrtSeq='"+PrtSeq+"' "
                                           +"and a.PEItemCode=b.TestItemCode "
                                           ;
  //fm.sql.value=strSql
  turnPage2.queryModal(strSql, HealthGrid);
  //反显体检费用。
  showMoney();
  showPEState();
  showreturn();
  showflag();
  //显示预约时间面板
  divBooking.style.display="";
  divAddPeItem.style.display="none";
  meal.style.display="none";
  divPage123.style.display="";
  divinfo.style.display="";
  flag456.style.display="";
  flag457.style.display="";
  fm.Pestateflag.style.display="";
}
function showMoney()
{
  var arr;
  var strSql = "select sum(double(medicaitemprice)) from "
               +"LDTESTPRICEMGT where hospitcode='"+fm.Hospital.value+"' and medicaitemcode in(select PEItemCode "
               +" from LCPENoticeItem where ProposalContNo='"
               +ContNo+"' and prtseq='"+PrtSeq+"')";
  arr = easyExecSql(strSql);
  if(arr)
  {
    if(trim(arr[0][0])!="null")
    {
      fm.TestFee.value = arr[0][0];
    }
    else
    {
      fm.TestFee.value = "0.0";
    }
  }
}
//确认预约时间
function addBookingTime()
{
  if( verifyInput2() == false )
    return false;
  var Hospital=fm.Hospital.value;
  if(Hospital=="")
  {
    alert("请录入体检医院!!!");
    return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action= "./LCPENoticeSave.jsp?PrtSeq="+PrtSeq;
  fm.submit(); //提交
}
function showPEState()
{
  var strSql = "select PEState,BookingDate,BookingTime, remark from LCPENotice where "
               +" ProposalContNo='"+ContNo+"' and PrtSeq='"+PrtSeq+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    if(arr[0][0]=="")
    {
      fm.all('PEStateName').value=""
                                }
                                else
                                {
                                  fm.PEState.value = arr[0][0];
                                  fm.all('PEStateName').value = easyExecSql("select codename from ldcode where codetype='pecode' and code='"+arr[0][0]+"'");
                                }
                                fm.BookingDate.value = arr[0][1];
    fm.BookingTime.value = arr[0][2];
    fm.Note.value = arr[0][3];
  }
}




function addPEItem()
{

  //alert(fm.GrpTestCode.value);
  var AddFlag = "01";
  showInfo = window.open("./MainUWInputMain.jsp?ProposalContNo="+fm.ContNo.value+"&PrtSeq="+PrtSeq+"&hospitcode="+fm.Hospital.value+"&Pestateflag="+AddFlag);

}

function dFirstRecoTime()
{
  var d = new Date();
  var h = d.getHours();
  var m = d.getMinutes();
  if(h<10)
  {
    h = "0"+d.getHours();
  }
  if(m<10)
  {
    m = "0"+d.getMinutes();
  }
  fm.BookingTime.value = h+":"+m;
}



function showreturn()
{
  var strSql="select a.Birthday,a.Sex,a.IDType,a.IDNo,b.HomePhone ,b.Mobile ,"
             +" b.CompanyPhone , b.HomeAddress "
             +" from lcinsured a,LCAddress b where 1=1  and a.InsuredNo = '"+fm.InsureNo.value+"' "
             +" and a.InsuredNo=b.CustomerNo and a.AddressNo=b.AddressNo"
             +" and a.ContNo='"+ContNo+"'";

  var arr = easyExecSql(strSql);

  if(arr)
  {
    fm.Birthday.value = arr[0][0];


    //alert(parseInt(fm.Birthday.value.substring(0,4)));
    fm.InsuredAppAge.value = parseInt(currentDate.substring(0,4))-parseInt(fm.Birthday.value.substring(0,4));
    fm.Sex.value = arr[0][1];
    fm.IDType.value = arr[0][2];
    fm.IDNo.value = arr[0][3];
    fm.HomePhone.value = arr[0][4];
    fm.Mobile.value = arr[0][5];
    fm.GrpPhone.value = arr[0][6];
    fm.HomeAddress.value = arr[0][7];
  }

}

function showflag()
{
  var strSql = "select max(Pestateflag) from LCPENoticeItem where "
               +" ProposalContNo='"+ContNo+"' and PrtSeq='"+PrtSeq+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    if(arr[0][0]=="")
    {
      fm.all('Pestateflag').value=""
    }
    else
    {
      //fm.Pestateflag.value = arr[0][0];
      fm.all('Pestateflag').value = easyExecSql("select codename from ldcode where codetype='addflag' and code='"+arr[0][0]+"'");
    }

  }
}

function showHistoryPEInfo()
{
  var strSql = "select Name,Remark,'','',PrtSeq,ContNo from LCPENotice where ContNo='"+ContNo+"'";
  turnPage.queryModal(strSql,HistroyGrid);
  for(var i=0 ; i < HistroyGrid.mulLineCount ; i++)
  {
    var strSql_Item = "select PEItemName,PEStateFlag from LCPENoticeItem where ProposalContNo='"
                      +HistroyGrid.getRowColData(i,6)
                      +"' and PrtSeq='"+HistroyGrid.getRowColData(i,5)+"'";
    var arr = easyExecSql(strSql_Item);
    if(arr)
    {
      var AddItem = "";
      var PEItem = "";
      for(var m=0 ; m<arr.length ; m++)
      {
        if(arr[m][1]=="01")
        {
          AddItem += arr[m][0]+",";
        }
        else
        {
          PEItem += arr[m][0]+",";
        }
      }
      HistroyGrid.setRowColData(i,3,PEItem);
      HistroyGrid.setRowColData(i,4,AddItem);
    }
  }
}

function checkInsureNo()
{
  if(fm.InsureNo.value=="")
  {
    alert("体检人信息不能为空");
    try
    {
      fm.all('Hospital').value= "";
    }
    catch(ex)
    {}
    ;
    try
    {
      fm.all('HospitName').value= "";
    }
    catch(ex)
    {}
    ;
    try
    {
      fm.all('TestGrpCode').value= "";
    }
    catch(ex)
    {}
    ;
    initHealthGrid
    try
    {
      initHealthGrid();
    }
    catch(ex)
    {}
    ;
    return false;
  }
  return true;
}

//计算体检费用
function showMoney2()
{
	try
	{
	  //若没有录入体检项，则不用计算费用
	  if(HealthGrid.mulLineCount == 0)
	  {
	    return true;
	  }
	  
	  //若没有录入医院，则不用计算费用
	  if(fm.Hospital.value == "")
	  {
	    return true;
	  }
	  
	  var wherePart = "(";
	  for(var i=0 ; i < HealthGrid.mulLineCount ; i++)
	  {
	    wherePart += "'";
	    wherePart += HealthGrid.getRowColData(i,1);
	    wherePart += "'";
	    if(i != HealthGrid.mulLineCount-1)
	    {
	      wherePart += ",";
	    }
	  }
	  wherePart += ")";
	
	  var strSql = "select sum(decimal(medicaitemprice,12,2)) from LDTESTPRICEMGT where hospitcode='"
	               +fm.Hospital.value+"' and medicaitemcode in "+wherePart;
	  var arr = easyExecSql(strSql);		 
	  if(arr)
	  {		 
	     if(trim(arr[0][0])!="null")
       {
          fm.TestFee.value = arr[0][0];
       }
       else
       {        
          fm.TestFee.value = "0.0";
       }
	  }
	}
	catch(ex)
	{
	  alert(ex.message);
  }
}

//返回父亲页面
function returnParent()
{
  top.opener.focus();
  top.close();
}