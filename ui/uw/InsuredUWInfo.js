/*********************************************************************
 *  程序名称：InsuredUWInfo.js
 *  程序功能：人工核保被保人信息页面函数
 *  创建日期：2005-01-06 11:10:36
 *  创建人  ：HYQ
 *  返回值：  无
 *  更新记录：  更新人    更新日期     更新原因/内容
 *********************************************************************
 */

var arrResult1 = new Array();
var arrResult2 = new Array();
var arrResult3 = new Array();
var arrResult4 = new Array();
var arrResult5 = new Array();
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var turnPage5 = new turnPageClass();
var temp = new Array();
var mOperate="";
var oldDataSet ; 
var showInfo;

//alert(PrtNo);
window.onfocus=myonfocus;

/*********************************************************************
 *  查询核保被保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryInsuredInfo()
{
	
	var arrReturn1 = new Array();
	var arrReturn2 = new Array();
	var arrReturn3 = new Array();
	var tSql1 = "select InsuredNo,name,sex,birthday,NativePlace,RelationToMainInsured,RelationToAppnt,OccupationCode,(select Occupationname from ldOccupation where OccupationCode=t.OccupationCode),OccupationType,AddressNo,SequenceNo, "
	                        + " db2inst1.getAccumRiskAmnt(t.insuredno),"
	                        + " db2inst1.getAccumAcciAmnt(t.insuredno),"
	                        + " db2inst1.getAccumDiseaRiskAmnt(t.insuredno),"
	                        + " db2inst1.getAccumCareRiskAmnt(t.insuredno),"
	                        + " db2inst1.getAccDeaRiskAmnt(t.contno,t.insuredno), "
	                        + " (select sum(amnt) from lcpol where insuredno=t.insuredno and (((stateflag='0' and uwflag <> '1' and uwflag <> '8' and uwflag <> 'a')) or (appflag='1' and stateflag='1'))) "
	                        + " from LCInsured t where 1=1"
							+ " and PrtNo='"+PrtNo+"'"
							+ " and insuredno ='"+InsuredNo+"' with ur";

	turnPage1.strQueryResult  = easyQueryVer3(tSql1, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage1.strQueryResult) {
    alert("被保人信息查询失败!");
    return "";
  }
  var tSql2 = "select ImpartParam from LCCustomerImpartParams where 1=1 "
                            + " and ImpartParamName = 'Stature'"
                            + " and PrtNo='"+PrtNo+"'"
							+ " and customerno ='"+InsuredNo+"' with ur"
							;						
							fm.all('Stature').value = tSql2;
 var tSql3 = "select ImpartParam from LCCustomerImpartParams where 1=1 "
                            + " and ImpartParamName = 'Avoirdupois'"
                            + " and PrtNo='"+PrtNo+"'"
							+ " and customerno ='"+InsuredNo+"' with ur"
							;
							
  //查询成功则拆分字符串，返回二维数组
  arrResult1 = decodeEasyQueryResult(turnPage1.strQueryResult);	

  fm.all('InsuredNo').value = arrResult1[0][0];
  fm.all('Name').value = arrResult1[0][1];
  fm.all('Sex').value = arrResult1[0][2];
  fm.all('InsuredAppAge').value = calAge(arrResult1[0][3]);
  fm.all('NativePlace').value = arrResult1[0][4];
  fm.all('RelationToMainInsured').value = arrResult1[0][5];
   
  fm.all('RelationToAppnt').value = arrResult1[0][6];
  fm.all('OccupationCode').value = arrResult1[0][7];
  fm.all('OccupationCodeName').value = arrResult1[0][8];
  fm.all('OccupationType').value = arrResult1[0][9];
  fm.all('AddressNo').value = arrResult1[0][10]; 
  fm.all('AccumRiskAmnt').value = arrResult1[0][12]; 
  fm.all('AccumAcciAmnt').value = arrResult1[0][13]; 
  fm.all('AccumDiseaRiskAmnt').value = arrResult1[0][14]; 
  fm.all('AccumCareRiskAmnt').value = arrResult1[0][15]; 
  fm.all('AccumDeaRiskAmnt').value = arrResult1[0][16]; 
  fm.all('BaseAmnt').value = arrResult1[0][17]; 
  fm.all('AddressNo_ch').value = easyExecSql("select PostalAddress from lcaddress where addressno='"+arrResult1[0][10]+"' and customerno='"+arrResult1[0][0]+"' with ur");
  //年收入的查询（先从 LCInsured 中查，没有再从 LCCustomerImpartdetail 中查）    zhangjianbao    2007-10-29
  var impartparam = easyExecSql("select salary from LCInsured where PrtNo='" + PrtNo + "' and insuredno ='" + InsuredNo + "' with ur"); 
  if(impartparam == 0) impartparam = easyExecSql("select diseasecontent from LCCustomerImpartdetail where impartcode='010' and impartver='000' and customerno='"+arrResult1[0][0]+"' with ur");
  if(impartparam != null) fm.all('Income').value = impartparam;
  //医疗费用支付方式 从  中查    zhangjianbao    2007-10-29
  var impartparamno = easyExecSql("select case impartparamno when '1' then '公费医疗' when '2' then '社会医疗保险' when '3' then '商业医疗保险' when '4' then '自费' end from LCCustomerImpartParams where impartver='001' and impartcode='170' and impartparam='Y' and customerno='"+arrResult1[0][0]+"' and PrtNo='"+PrtNo+"' with ur");
  if(impartparamno != null) fm.all('MedicalPayMode').value = impartparamno;
  fm.all('RiskGrade').value = easyExecSql("select OccupationType from LCInsured where insuredno='"+arrResult1[0][0]+"' and PrtNo='"+PrtNo+"' with ur");
//  fm.all('Pulse').value = easyExecSql("select Heartbeat from LCPENoticeResult where customerno='"+arrResult1[0][0]+"' and PrtNo='"+PrtNo+"'"); 
  turnPage2.strQueryResult  = easyQueryVer3(tSql2, 1, 0, 1);  
  arrResult2 = decodeEasyQueryResult(turnPage2.strQueryResult);	
 
   if (!turnPage2.strQueryResult)
   { 
     
   	 fm.all('Stature').value = ' ';
   }
   else
   {
  fm.all('Stature').value = arrResult2[0][0];
}

  turnPage3.strQueryResult  = easyQueryVer3(tSql3, 1, 0, 1);  
  arrResult3 = decodeEasyQueryResult(turnPage3.strQueryResult);
  if(!turnPage3.strQueryResult)
  {
  	fm.all('Avoirdupois').value = ' ';
}
else
{
  fm.all('Avoirdupois').value = arrResult3[0][0];
}
//  if(turnPage2.strQueryResult)
    if(fm.all('Avoirdupois').value != "0" && fm.all('Avoirdupois').value.trim()!="")
  {

  	fm.all('BMI').value = Math.round((arrResult3[0][0]/((arrResult2[0][0]/100)*(arrResult2[0][0]/100)))*100)/100;
  }
	else
	{
		fm.all('BMI').value = "0";
	}
	
  if(arrResult1[0][11]=="1")
  {  	    
  	 MainAppnt.style.display = "";
  	 MainAppntInput.style.display = "";
  }
  else
	{
  	 MainInsured.style.display = "";
  	 MainInsuredInput.style.display = "";	
	}
	var oySql = "SELECT count(ll.caseno),nvl(sum(a.realpay), 0) FROM llclaimpolicy a,llcase ll" +
	" WHERE ll.customerno = '"+ InsuredNo +
	" ' AND ll.caseno = a.caseno AND a.grpcontno =" +
	" '00000000000000000000' AND EXISTS (" +
	" SELECT 1 FROM ljaget c WHERE c.confDate IS NOT NULL" +
	" AND (c.otherno = ll.caseno OR c.otherno = ll.rgtno)) with ur";
  var claimnumber = easyExecSql(oySql);
  if(claimnumber != null) {
     fm.all('ClaimNumber').value = claimnumber[0][0];
     fm.all('ClaimMoney').value = claimnumber[0][1];
  }
 
  return true;
 
}
//扫描件查询
function ScanQuery() {
  var prtNo = fm.PrtNo.value;
  if (prtNo == "")
    return;
    divEsDocPages1Title.style.display = "";
		divEsDocPages1.style.display = "";
		initEsDocPagesGrid(); 
		easyqueryClick1();
//  window.open("../sys/LCProposalScanQuery.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");

}
function easyqueryClick1()
{

	var strSQL = "select a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+fm.PrtNo.value+"%%'"
				  + " and a.busstype='TB'"
				  + " and b.busstype='TB' with ur";			
					
					
	  turnPage.queryModal(strSQL,EsDocPagesGrid);
	 
}

function ChangePic()
{
	tRow = EsDocPagesGrid.getSelNo();
		
	if( tRow == 0 || tRow == null  )
	{
		alert("请选择一条记录！");
		return;
	}
	var cDocID = EsDocPagesGrid.getRowColData(tRow-1,10);	
 	var cDocCode = EsDocPagesGrid.getRowColData(tRow-1,1); 	
  var	cBussTpye = "TB" ;
  var cSubTpye = EsDocPagesGrid.getRowColData(tRow-1,2); 
    
    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
}
//查询疾病详细信息
function easyQueryDisease(){
	var tSel = DiseaseGrid.getSelNo();
	var tDiseaseCode = DiseaseGrid.getRowColData(tSel - 1,1);
	var tRiskKind = DiseaseGrid.getRowColData(tSel - 1,5);
	window.open("./InsuredDiseaseMain.jsp?DiseaseCode="+tDiseaseCode+"&RiskKind="+tRiskKind);
}

/*********************************************************************
 *  查询核保被保人险种信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryRiskInfo()
{
//	var tSql = "select lcpol.polno,lcpol.MainPolNo,lcpol.riskcode,lmrisk.riskname,"
//							+"lcpol.Mult,lcpol.Amnt,lcpol.prem,'',lcpol.CValiDate,lcpol.EndDate-1 days,"
//							+"lcpol.PayIntv,case when lcpol.payintv=0 then '-' else char(lcpol.PayYears) end from LCPol,lmrisk where 1=1"
//							+ " and PrtNo='"+PrtNo+"'"
//							+ " and insuredno ='"+InsuredNo+"'"
//							+ " and lcpol.riskcode = lmrisk.riskcode"
//					   	+ " and lcpol.polno = '"+PolNo+"'"
//							;

	//修改查询SQL，得到总保费  zhangjianbao   2007-11-8
	var tSql = "select lcpol.polno,lcpol.MainPolNo,lcpol.riskcode,lmrisk.riskname,lcpol.Mult,lcpol.Amnt,"
							+"(select sum(p.prem) from lcprem p where p.polno = '" + PolNo + "' and substr(p.payplancode,1,6) != '000000'),"
							+"'',lcpol.CValiDate,lcpol.EndDate-1 days,lcpol.PayIntv,"
							+"case when lcpol.payintv=0 then '-' else char(lcpol.PayYears) end, "
                            + " SupplementaryPrem "
							+"from LCPol,lmrisk where 1=1"
							+ " and PrtNo='"+PrtNo+"'"
							+ " and insuredno ='"+InsuredNo+"'"
							+ " and lcpol.riskcode = lmrisk.riskcode"
					   	+ " and lcpol.polno = '"+PolNo+"' with ur"
							;
  //fm.BMI.value=tSql;
	turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("被保人信息查询失败!");
    return "";
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = RiskGrid ;    

  //保存SQL语句
  turnPage.strQuerySql     = tSql ; 

  //设置查询起始位置
  turnPage.pageIndex       = 0;  

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  if(arrDataSet.length!=0)
  {
	  for(i=0;i<arrDataSet.length;i++)
	  {
	  	 temp[i] = arrDataSet[i][2];
	  }
  }
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	dealAddFee(arrDataSet);
  return true;  					
}
/*********************************************************************
 *  查询核保被保人既往信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryOldInfo()
{
	//alert(PrtNo);
	var oSql =  "select A,C,D,E,F,G,H,L,M,N,O,P,Q,ctrim1(Q,G),R,S"
             +" from "
             +"("
             +"select"
             +" Z.prtno A,Z.CValiDate C,Z.riskcode D,Z.Amnt E,int(Z.mult) F,case appflag when '1' then Z.sumprem else Z.prem end G,trim(char(Z.InsuYear))||Z.InsuYearFlag H,trim(char(Z.PayEndYear))||char(Z.PayEndYearFlag) L,codeName('stateflag', Z.StateFlag) M,Z.polno R,Z.contno S," 
             +"(select case when count(1)>0 then '是' else '否' end from LCPENotice where ProposalContNo=Z.ProposalContNo  AND customerNo=Z.insuredno) N,"
             +"(select codename from ldcode where codetype='uwflag' and code=Z.uwflag) O," 
             +"LPCSH1(Z.polno,'"+InsuredNo+"') P,"
             +"LPJE1(Z.polno) Q"
             +" from lcpol Z where Z.conttype='1' and Z.insuredno='"+InsuredNo+"' AND Z.prtno <> '"+PrtNo+"' and uwflag<>'a'"
             +")as x"
             +" union "
             +"select A,C,D,E,F,G,H,L,M,N,O,P,Q,ctrim1(Q,G),R,S"
             +" from "
             +"("
             +"select"
             +"  Z.prtno A,Z.CValiDate C,Z.riskcode D,Z.Amnt E,int(Z.mult) F,Z.sumprem G,trim(char(Z.InsuYear))||Z.InsuYearFlag H,trim(char(Z.PayEndYear))||char(Z.PayEndYearFlag) L,'退保' M,Z.polno R,Z.contno S,"
             +"(select case when count(1)>0 then '是' else '否' end from LCPENotice where ProposalContNo=Z.ProposalContNo  AND customerNo=Z.insuredno) N,"
             +"(select codename from ldcode where codetype='uwflag' and code=Z.uwflag) O," 
             +"LPCSH1(Z.polno,'"+InsuredNo+"') P,"
             +"LPJE1(Z.polno) Q"
             +" from lbpol Z where Z.conttype='1' and Z.insuredno='"+InsuredNo+"' and Z.PrtNo <> '" + PrtNo + "' "
             +")as x"
             +" union "
             +"select A,C,D,E,F,G,H,L,M,N,O,0,0,0,R,S"
             +" from "
             +"("
             +"select"
             +" Z.prtno A,Z.CValiDate C,Z.riskcode D,Z.Amnt E,int(Z.mult) F,Z.prem G,trim(char(Z.InsuYear))||Z.InsuYearFlag H,trim(char(Z.PayEndYear))||char(Z.PayEndYearFlag) L,'撤保' M,Z.polno R,Z.ContNo S,"
             +"(select case when count(1)>0 then '是' else '否' end from LCPENotice where ProposalContNo=Z.ProposalContNo AND customerNo=Z.insuredno) N,"
             +"(select codename from ldcode where codetype='uwflag' and code=Z.uwflag) O"
             +" from lcpol Z where Z.conttype='1' and Z.insuredno='"+InsuredNo+"' and Z.uwflag='a' and Z.PrtNo <> '" + PrtNo + "' "
             +")as x order by A with ur"
              ;

  turnPage.strQueryResult  = easyQueryVer3(oSql, 1, 0, 1); 

 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return "";
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult); 
  oldDataSet = turnPage.arrDataCacheSet;

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = OldoldInfoGrid ;   
  
  //调用MULTILINE对象显示查询结果 
   displayMultiline(oldDataSet, turnPage.pageDisplayGrid);
   changeChar(); 
  return true;  
					
}
function changeChar(){
	for(var a=0 ; a< OldoldInfoGrid.mulLineCount ; a++){
			for(var i=1 ; i < OldoldInfoGrid.colCount ; i++){
				//alert(PolGrid.getRowColData(a,i).indexOf("Y"));
				//alert(PolGrid.getRowColData(a,i+1));
					OldoldInfoGrid.setRowColData(a,i,replace(OldoldInfoGrid.getRowColData(a,i),"1000A","终身"));
					OldoldInfoGrid.setRowColData(a,i,replace(OldoldInfoGrid.getRowColData(a,i),"70A","到70岁"));
					OldoldInfoGrid.setRowColData(a,i,replace(OldoldInfoGrid.getRowColData(a,i),"Y","年"));	

		}
	}
}
/*********************************************************************
 *  执行新契约人工核保的EasyQueryAddClick
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryAddClick()
{
	var tSel = OldoldInfoGrid.getSelNo();
	var activityid = OldoldInfoGrid.getRowColData(tSel - 1,19);
	var ContNo = OldoldInfoGrid.getRowColData(tSel - 1,16);
	var PrtNo = OldoldInfoGrid.getRowColData(tSel - 1,1);
	var MissionID = OldoldInfoGrid.getRowColData(tSel - 1,17);
	var SubMissionID = OldoldInfoGrid.getRowColData(tSel - 1,18);
  fm.LoadFlag.value = "";
	

		window.location="./OldUWManuInputMain.jsp?ContNo="+ContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PrtNo="+PrtNo+"&ActivityID="+activityid+"&LoadFlag=2";
}
/*********************************************************************
 *  核保被保人既往信息理赔信息
 *  参数  ： pContNo,pRiseCode,pRowCount
 *  返回值：  无
 *********************************************************************
 */
function setEdorInfo(pContNo,pRiseCode,pRowCount)
{	

	var oSql = "select count(LJSGetClaim.OtherNo),sum(LJSGetClaim.Pay) from LLCase,LJSGetClaim where 1=1"		                   
							+ " and LLCase.CustomerNo ='"+InsuredNo+"'"
							+ " and LJSGetClaim.riskcode ='" + pRiseCode + "'"
							+ " and LJSGetClaim.ContNo = '" + pContNo + "'"
							+ " and LJSGetClaim.OtherNo	 = LLCase.CaseNo with ur"
							;		
   turnPage4.strQueryResult  = easyQueryVer3(oSql, 1, 0, 1); 

   //判断是否查询成功
   if (!turnPage4.strQueryResult) {
     alert("被保人既往信息理赔信息查询失败!");
     return "";
   }

  //查询成功则拆分字符串，返回二维数组
  turnPage4.arrDataCacheSet = decodeEasyQueryResult(turnPage4.strQueryResult);
  var arrDataSet = turnPage4.arrDataCacheSet;

  //设置理赔信息
  if (arrDataSet.length !=0) 
  {	  
	  //设置理赔数
	  oldDataSet[pRowCount][8] = arrDataSet[0][0];

	  //设置理赔总额
	   if (arrDataSet[0][1] == "null"){	
		  oldDataSet[pRowCount][9] = 0;
	  }else
	  {		 
		   oldDataSet[pRowCount][9] = arrDataSet[0][1];
	  }
	  
  }else
  {
	 oldDataSet[pRowCount][8] = 0;
     oldDataSet[pRowCount][9] = 0;
  }
							
}

/*********************************************************************
 *  体检资料查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHealthQ()
{

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  
  var cContNo = fm.ContNo.value;

  var cMissionID = fm.MissionID.value;
  var cSubMissionID = fm.SubMissionID.value;
  var cPrtNo = fm.PrtNo.value;
  
  
  if (cContNo!= ""  )
  {     
  	//var tSelNo = PolAddGrid.getSelNo()-1;
  	//var tNo = PolAddGrid.getRowColData(tSelNo,1);	
  	//showHealthFlag[tSelNo] = 1 ;
  	window.open("./UWManuHealthQMain.jsp?ContNo="+cContNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&PrtNo="+cPrtNo,"window1");
  	showInfo.close();
  	
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}       

/*********************************************************************
 *  生存调查报告查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function RReportQuery()
{
  cContNo = fm.ContNo.value;  //保单号码

  //var tSel = PolAddGrid.getSelNo();
  //if (tSel == 0 || tSel == null)
  //{
  // 	alert("请先选择保单!");  	
	//return;
  //}
  //var cNo = PolAddGrid.getRowColData(tSel - 1, 1);

  
  if (cContNo != "")
  {			
	window.open("./RReportQueryMain.jsp?ContNo="+cContNo);
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

/*********************************************************************
 *  既往投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showApp(cindex)
{
	 var cPrtNo = fm.PrtNo.value;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cInsureNo = fm.InsuredNo.value;
  window.open("../uw/UWAppMain.jsp?ContNo="+ContNo+"&CustomerNo="+cInsureNo+"&Prtno="+cPrtNo);
  	showInfo.close();
}         

/*********************************************************************
 *  体检资料录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHealth()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cContNo = fm.ContNo.value;
  var cMissionID = fm.MissionID.value;
  var cSubMissionID = fm.SubMissionID.value;
  var cPrtNo = fm.PrtNo.value;
  
  if (cContNo != "")
  {
  	//var tSelNo = PolAddGrid.getSelNo()-1;
  	//var tNo = PolAddGrid.getRowColData(tSelNo,1);	
  	//showHealthFlag[tSelNo] = 1 ;
  	window.open("./UWManuHealthMain.jsp?ContNo1="+cContNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&PrtNo="+cPrtNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}           

/*********************************************************************
 *  生存调查报告
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showRReport()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ContNo.value;
  alert(cProposalNo);
  var cMissionID =fm.MissionID.value; 
  var cSubMissionID =fm.SubMissionID.value; 
  var tSelNo = PolAddGrid.getSelNo()-1;
  var cPrtNo = PolAddGrid.getRowColData(tSelNo,3);	
  if (cProposalNo != ""  && cMissionID != "")
  {
  	window.open("./UWManuRReportMain.jsp?ContNo="+cProposalNo+"&MissionID="+cMissionID+"&PrtNo="+cPrtNo+"&SubMissionID="+cSubMissionID+"&Flag="+pflag,"window1");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}         
   
/*********************************************************************
 *  显示已经录入的疾病信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDisDesb()
{
	//alert(PrtNo);
	for(i=0;i<temp.length;i++)
	{
        // 和疾病风险库相关。目前风险库未上，进行注释。
        /*
	   var wherePart = " and a."
	   var tSql = 	" select a.DiseaseCode,a.DiseaseDes,a.DiseasDegree,a.UWRule,a.RiskKind,a.RiskGrade from LCDiseaseResult t,LDUWAddFee a,LCPol b,LMRiskApp c where 1=1"
							+ " and t.customerno = '"+InsuredNo+"'"
							+ " and t.DiseaseCode = a.DiseaseCode "
							+ " and t.customerno=b.InsuredNo "
							+ " and b.RiskCode=c.RiskCode"
							+ " and c.RiskType1=a.RiskKind"
							+ " union "
							+ " select a.DiseaseCode,a.DiseaseDes,a.DiseasDegree,a.UWRule,a.RiskKind,a.RiskGrade from LCPENoticeResult t,LDUWAddFee a,LCPol b,LMRiskApp c where 1=1"
							+ " and t.customerno = '"+InsuredNo+"'"
							+ " and t.DiseaseCode = a.DiseaseCode "
							+ " and t.customerno=b.InsuredNo "
							+ " and b.RiskCode=c.RiskCode"
							+ " and c.RiskType1=a.RiskKind"
							+ " union "
							+ " select a.DiseaseCode,a.DiseaseDes,a.DiseasDegree,a.UWRule,a.RiskKind,a.RiskGrade from LCRReportResult t,LDUWAddFee a,LCPol b,LMRiskApp c where 1=1"
							+ " and t.customerno = '"+InsuredNo+"'"
							+ " and t.DiseaseCode = a.DiseaseCode "
							+ " and t.customerno=b.InsuredNo "
							+ " and b.RiskCode=c.RiskCode"
							+ " and c.RiskType1=a.RiskKind";
       //---------------------------------------------------------------*/
       
    var tSql = " select t.disresult,t.ICDCode,t.DisDesb,a.RiskGrade,a.ObservedTime,a.State,a.uwresult from LCDiseaseResult t,LDUWAddFee a where 1=1"
        + " and t.customerno = '" + InsuredNo + "'"
        + " and t.ICDCode = a.ICDCode AND t.CONTNO <> '0' "
        + " union "
        + " select t.disresult,t.ICDCode,t.DisDesb,a.RiskGrade,a.ObservedTime,a.State,a.uwresult from LCPENoticeResult t,LDUWAddFee a where 1=1"
        + " and t.customerno = '" + InsuredNo + "' "
        + " and t.ICDCode = a.ICDCode AND t.CONTNO <> '0' "
        + " union "
        + " select t.RReportResult,t.ICDCode,t.RReportDesb,a.RiskGrade,a.ObservedTime,a.State,a.uwresult from LCRReportResult t,LDUWAddFee a where 1=1"
        + " and t.customerno = '" + InsuredNo + "' "
        + " and t.ICDCode = a.ICDCode AND t.CONTNO <> '0' with ur";

	turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return "";
  }
  if(i==0)
  {
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  }
 else
 	 {
 	 	 
 		 l = turnPage.arrDataCacheSet[0].length;
 		 for(j=0;j<decodeEasyQueryResult(turnPage.strQueryResult).length;j++)
 		 {  
 		 	  turnPage.arrDataCacheSet[j][l] = decodeEasyQueryResult(turnPage.strQueryResult)[j][6];
 		 }
 	 }
  }
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = DiseaseGrid ;    

  //保存SQL语句
  turnPage.strQuerySql     = tSql ; 

  //设置查询起始位置
  turnPage.pageIndex       = 0;  

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;  			
}

/*********************************************************************
 *  理赔给付查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function ClaimGetQuery()
{
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	 var cInsuredNo = fm.InsuredNo.value;				
	if (cInsuredNo == "")
		  return;
	window.open("../sys/AllClaimGetQueryMain.jsp?InsuredNo=" + cInsuredNo);										
    showInfo.close();
}
/*********************************************************************
 *  理赔给付查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function familyGetQuery()
{
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	var cInsuredNo = fm.InsuredNo.value;				
	if (cInsuredNo == "")
		   return;
    window.open("../uw/FamilyGetQuery.jsp?InsuredNo=" + cInsuredNo);										
    showInfo.close();
}
/*********************************************************************
 *  既往保全信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showEdor()
{
	var cPrtNo = fm.PrtNo.value;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cInsureNo = fm.InsuredNo.value;
  window.open("../uw/UWEdorApp.jsp?ContNo="+ContNo+"&CustomerNo="+cInsureNo+"&type=2&Prtno="+cPrtNo);
  	showInfo.close();
} 

/*********************************************************************
 *  既往保全信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showFimily()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var cInsureNo = fm.InsuredNo.value;
  window.open("../uw/UWEdorApp.jsp?ContNo="+ContNo+"&CustomerNo="+cInsureNo+"&type=2");
  	showInfo.close();
}         
/*********************************************************************
 *  加费承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function showAdd()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  

	window.open("./UWManuAddMain.jsp?ContNo="+ContNo,"window1"); 

}

/*********************************************************************
 *  特约承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function showSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  if (ContNo != ""&& PrtNo !="" && MissionID != "" )
  { 	
  	window.open("./UWManuSpecMain.jsp?ContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID,"window1");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("数据传输错误!");
  }
}

/*********************************************************************
 *  返回
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function InitClick(){
	window.close();
}

/*********************************************************************
 *  提交，保存按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function submitForm(FlagStr)
{
	
	//alert(FlagStr);
    
    // 再保尚未上线，目前先注释掉。
    // 但该判断SQL可能存在问题。
	//if(!checkReInsure())
	//{
	//  return false;
	//}
	//------------------------------------------
    
	if(!verifyInput2())
	return false;
	if(FlagStr == 1)
	{
		var tSelNo = RiskGrid.getSelNo();
 		if(tSelNo<=0)
 		{
 			alert("请选择险种保单！");
 			return;
 		}
  	fm.all('PolNo').value = RiskGrid.getRowColData(tSelNo - 1,1);	
  	fm.all('flag').value = "risk";
  	fm.LoadFlag.value = "";
  	if(fm.uwstate.value=='8')
	{
		if(!Postponecheck()){
			return;
		}
	}
	}
	if(fm.uwstate.value=='1')
	{
		var  tRejectReason=fm.RejectReason.value;
		if(tRejectReason=null||tRejectReason==""){
			alert("存在险种核保结论为谢绝承保，须选择拒保原因！");
			return;
		}
	}
	if(FlagStr == 2)
	{
		fm.all('flag').value = "Insured";
		fm.LoadFlag.value = "";
	}
	if(FlagStr == 3)
	{
		fm.all('flag').value = "risk";
		fm.LoadFlag.value = "L";
		var tSelNo = RiskGrid.getSelNo();
 		if(tSelNo<=0)
 		{
 			alert("请选择险种保单！");
 			return;
 		}
		fm.all('PolNo').value = RiskGrid.getRowColData(tSelNo - 1,1);

	}
	if(FlagStr == 4)
	{
		fm.all('flag').value = "risk";
		fm.LoadFlag.value = "E";
		var tSelNo = RiskGrid.getSelNo();
 		if(tSelNo<=0)
 		{
 			alert("请选择险种保单！");
 			return;
 		}
		fm.all('PolNo').value = RiskGrid.getRowColData(tSelNo - 1,1);	
	}
	if(FlagStr == 5)
	{
		fm.all('flag').value = "risk";
		fm.LoadFlag.value = "M";
		var tSelNo = RiskGrid.getSelNo();
 		if(tSelNo<=0)
 		{
 			alert("请选择险种保单！");
 			return;
 		}
		fm.all('PolNo').value = RiskGrid.getRowColData(tSelNo - 1,1);	
	}
	if(FlagStr == 6)
	{
		fm.all('flag').value = "risk";
		fm.LoadFlag.value = "A";
		var tSelNo = RiskGrid.getSelNo();
 		if(tSelNo<=0)
 		{
 			alert("请选择险种保单！");
 			return;
 		}
		fm.all('PolNo').value = RiskGrid.getRowColData(tSelNo - 1,1);	
	}
	if(FlagStr == 7)//变更缴费期间
	{
		fm.all('flag').value = "risk";
		fm.LoadFlag.value = "C";
		var tSelNo = RiskGrid.getSelNo();
 		if(tSelNo<=0)
 		{
 			alert("请选择险种保单！");
 			return;
 		}
		fm.all('PolNo').value = RiskGrid.getRowColData(tSelNo - 1,1);
		
		
		
	}
	if(FlagStr == 11)
	{
		var ContNo = fm.ContNo.value;
		var inputExplain = fm.inputExplain.value;
		var OperatePos = 3;
//		alert(fm.inputExplain.value);
//		alert(fm.ContNo.value);
  if (mOperate==""){
  	mOperate="INSERT||MAIN";

   }
	}
  var i = 0;
//  alert("111");
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "./InsuredUWInfoChk.jsp";
  if (FlagStr == 11)
  {
  fm.action = "./InsuredUWInfoChkExplain.jsp";
  }
  fm.submit(); //提交
}

/**
 * 延期信息只可录一项的校验
 * @returns {Boolean}
 */
function Postponecheck(){
	
	var inputDelayDays = fm.inputDelayDays.value;
	var inputDelayDate = fm.inputDelayDate.value;
	var inputDelayDescribe = fm.inputDelayDescribe.value;
	var checknum=0;
	
	if(inputDelayDays!=null&&inputDelayDays!=""){
		checknum=checknum+1;
	}
	if(inputDelayDate!=null&&inputDelayDate!=""){
		checknum=checknum+1;
	}
	if(inputDelayDescribe!=null&&inputDelayDescribe!=""){
		checknum=checknum+1;
	}
	if(checknum>1){
		alert("延期天数、延期日期、延期描述只可填选一项，请去掉多余的延期信息重新保存");
		checknum=0;
		return false;
	}
	return true;
}

/**
1、符合再保审核条件，但没有发送给再保模块，需要给出提示
2、若有未结案的再保审核任务，需要给出提示
*/
function checkReInsure()
{
  var sql = "select 1 from LCUWError a "
          + "where PolNo = '" + PolNo + "' "
          + "   and not exists (select 1 from LCReinsurTask where PolNo = a.PolNo)  with ur";
  var rs = easyExecSql(sql);
  if(rs)
  {
    return confirm("险种符合再保审核条件，但没有发送给再保模块，继续？");
  }
  
  sql = "select 1 from LCReinsurTask "
      + "where PolNo = '" + PolNo + "' "
      + "   and State != '02'  with ur";
  rs = easyExecSql(sql);
  if(rs)
  {
    return confirm("险种还有未办结的再保审核任务，继续？");
  }
  
  return true;
}

/*免责信息查询*/
function QueryDisease(){
  window.open("./QueryDiseaseInputMain.jsp");
  fm.submit(); //提交
	}

/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function afterSubmit( FlagStr, content , otherSign)
{
	 window.focus();
	var flag = fm.all('flag').value;
	window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      

    	showInfo.close();
    alert(content);
    //parent.close();
  }
  else
  { 
  	
	var showStr="操作成功";
  	showInfo.close();
  	// 豁免险处理
  	if(otherSign != null && otherSign != ""){
  		alert(showStr + otherSign)
  	} else {
  		alert(showStr);
  	}
  	
  	if(flag=="Insured")
    {
  		parent.close();
  	}
    else
  	{
  		showInsuredResult();
  	}
    //执行下一步操作
  }
  top.opener.queryProposalInfo();
}
//查询上一个险种
function getPreviousRisk(){
  
  var row = top.opener.ProposalGrid.getSelNo();
  if(row == 1)
  {
    alert("已经是第一个险种");
    return false;
  }
  
	top.opener.ProposalGrid.checkBoxSelM(row-1);
  top.opener.showInsuredInformation(); 
}

function getNextRisk(){
  
  var row = top.opener.ProposalGrid.getSelNo();
  if(row == top.opener.ProposalGrid.mulLineCount)
  {
    alert("已经是最后一个险种");
    return false;
  }

	top.opener.ProposalGrid.checkBoxSelM(row+1);
	top.opener.showInsuredInformation(); 
}


/*********************************************************************
 *  取消
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelchk()
{
	fm.all('uwstate').value = "";
	fm.all('UWIdea').value = "";
}

/*********************************************************************
 *  返回
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function InitClick(){
	parent.close();
}

/*********************************************************************
 *  暂停被保人，向后台提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function StopInsured()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.flag.value = "StopInsured";
	
	fm.action = "./InsuredUWInfoChk.jsp";
  fm.submit(); //提交
}

/*********************************************************************
 *  下险种结论
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function showRiskResult()
{
	var icdcode;
	var arrSelected = new Array();
	var arrResult = new Array();
	var arrTemp = new Array();
		for (m=0;m<DiseaseGrid.mulLineCount;m++)
		{
				icdcode=	DiseaseGrid.getRowColData(m,2);
				strSQL = "select uwresult from lduwaddfee where 1=1 "
									+" and icdcode ='"+icdcode+"'"
									+" and riskkind = '1' with ur"
									;
				turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
				
				arrResult = decodeEasyQueryResult(turnPage.strQueryResult);
		
  		if (turnPage.strQueryResult) 
  		{ 
				for(j=0;j<arrResult.length;j++)
				{
					arrSelected[j] = arrResult[j][0];
				}
			}
			arrTemp[m]=calRiskResult(arrSelected);
		}
	fm.SugUWFlag.value = calRiskResult(arrTemp);
	fm.uwstate.value = "";
	fm.UWIdea.value = "";
	divPropValueName.style.display="none";
	divPropValue.style.display="none";
	divUWResult.style.display ="";	
	divCancelReason.style.display = "none";
	divRejectReason.style.display = "none";
	DivAddFee.style.display = "none";
	divDelay.style.display = "none";
	spanstopInsuredButton.style.display="none";
	//如果有险种，默认第一个待审批险种为已选中状态   zhangjianbao   2007-11-9
	if(RiskGrid.mulLineCount > 0)
	{
		RiskGrid.radioBoxSel(1);
		UWResultInfo();
		ShowAddFeeInfo();
		initAddFeeInfo();
		ShowSpecInfo();
		showSubMult();
		showDelayInfo();
		showAllCodeName();
		showChangePayEndYear();
		divPage.style.display ="";
	}
	else
	{
		alert("没有待审批险种信息！");
		return;
	}
}

/*********************************************************************
 *  初始化上报
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initSendTrace()
{
	if(SendFlag =="1")
	{
		divUWButton1.style.display = "none";
		divUWButton2.style.display = "none";
		fm.AddFeeButton.style.display = "none";
		fm.SpecButton.style.display = "none";
	}
}

/*********************************************************************
 *  计算核保结论
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function calRiskResult(arrSelected)
{
	var tResult="S";
	var arrSelected =new Array();
	var dFlag = 0 ;
	var pFlag = 0 ;
	var lFlag = 0 ;
	var eFlag = 0 ;
	var rFlag = 0 ;

	for(i=0;i<arrSelected.length;i++)
	{
		if(arrSelected[i]=="D")
		{
			dFlag = 1;
		}
		if(arrSelected[i]=="P")
		{
			pFlag = 1;
		}
		if(arrSelected[i]=="R")
		{
			rFlag = 1;
		}		
		if(arrSelected[i]=="E")
		{
			eFlag = 1;
		}			
		if(arrSelected[i]=="L")
		{
			lFlag = 1;
		}	
	}
	if(dFlag == 1)
	{
		tResult="D";
	}
  else
  {
  	if(pFlag == 1)
  	{
  		tResult="P";
  	}
  	else
  	{
  		if(rFlag == 1)
  		{
  			tResult="R";
  		}
  		else
  		{
  			  	if(eFlag == 1)
  					{
  						tResult="E";
  					}
  					else
  					{
  						  	if(lFlag == 1)
  								{
  									tResult="L";
  								}
  								else
  								{
  									tResult="S";
  								}
  					} 
  		}  		  
  	}         
  }
  	
  return tResult;     
}             
              
/*********************************************************************
 *  得出被保人建议结论
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */              
function showInsuredResult()
{
	var arrSelected = new Array();
	var arrResult = new Array();

	strSQL = "select sugpassflag from lcuwmaster where 1=1 "
						+" and contno ='"+ContNo+"' with ur"
						;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	arrResult = decodeEasyQueryResult(turnPage.strQueryResult);

	//判断是否查询成功
  if (turnPage.strQueryResult) { 
		for(j=0;j<arrResult.length;j++)
		{
			arrSelected[j] = arrResult[j][0];
		}
		fm.SugIndUWFlag.value = calRiskResult(arrSelected);
	}	
}        

/*********************************************************************
 *  显示参数输入框
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 
function afterCodeSelect( cCodeName, Field )
{
	var uwstate = fm.all('uwstate').value;
	var cAddfeeType=fm.all('AddFeeType').value;
	if(cCodeName=="AddFeeType"){
		if(cAddfeeType=="1"){
			//alert(1);
			DivAddFee_1_2.style.display='';
			DivAddFee_1_2_1.style.display='none';
			}
		else if(cAddfeeType=="2"){
			//alert(2);
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_2_1.style.display='';
			}
		}
	//if (cCodeName=="uwstate")
	//{
	//	if(uwstate=="E"||uwstate=="L")
	//	{
	//		divPropValueName.style.display='';
	//		divPropValue.style.display='';
	//		fm.PropValue.value = "";
	//	}
	//	else
	//	{
	//		divPropValueName.style.display='none';
	//		divPropValue.style.display='none';			
	//	}
	//}
	if (cCodeName=="uwstate")
	{
		if(uwstate=="4")
		{
			DivAddFee.style.display='';
//			DivMult.style.display='none';
      divExplainName.style.display='';
			divExplain.style.display='';
//			DivSpec.style.display='none';
//			DivAmnt.style.display='none';
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			//fm.PropValue.value = "";
		}
		else
		{
			DivAddFee.style.display='none';
			divPropValueName.style.display='none';
			divPropValue.style.display='none';	
		}
//		if(uwstate=="3")
//		{
//			DivSpec.style.display='';
//			DivAddFee.style.display='none';
//      divExplainName.style.display='';
//			divExplain.style.display='';
//			DivMult.style.display='none';
//			DivAmnt.style.display='none';
//			divPropValueName.style.display='none';
//			divPropValue.style.display='none';
			//fm.PropValue.value = "";
//		}
//		else
//		{
//			DivSpec.style.display='none';
//			divPropValueName.style.display='none';
//			divPropValue.style.display='none';			
//		}
//		if(uwstate=="4")
//		{
//			DivMult.style.display='';
//			DivSpec.style.display='none';
//      divExplainName.style.display='';
//			divExplain.style.display='';
//			DivSpec.style.display='none';
//			DivAmnt.style.display='none';
//			divPropValueName.style.display='none';
//			divPropValue.style.display='none';
			//fm.PropValue.value = "";
//		}
//		else
//		{
//			DivMult.style.display='none';
//			divPropValueName.style.display='none';
//			divPropValue.style.display='none';			
//		}
//				if(uwstate=="5")
//		{
//			DivAmnt.style.display='';
//		  DivAddFee.style.display='none';
//      divExplainName.style.display='';
//			divExplain.style.display='';
//			DivMult.style.display='none';
//			DivSpec.style.display='none';
//			divPropValueName.style.display='none';
//			divPropValue.style.display='none';
			//fm.PropValue.value = "";
//		}
//		else
//		{
//			DivAmnt.style.display='none';
//			divPropValueName.style.display='none';
//			divPropValue.style.display='none';			
//		}	
		if(uwstate=="8")
		{
			divDelay.style.display='';
//		  DivAmnt.style.display='none';
//		  DivAddFee.style.display='none';
//			DivMult.style.display='none';
      divExplainName.style.display='';
			divExplain.style.display='';
//			DivSpec.style.display='none';
		}
		else{
			divDelay.style.display='none';
		}
		if(uwstate=="9")
		{
			divExplainName.style.display='';
			divExplain.style.display='';
//		  DivAmnt.style.display='none';
//		  DivAddFee.style.display='none';
//			DivMult.style.display='none';
//			DivSpec.style.display='none';
		}
		else{
      divExplainName.style.display='';
			divExplain.style.display='';
		}
		if(uwstate=="a")
		{
			divCancelReason.style.display='';
//		divExplain.style.display='';
//			DivAmnt.style.display='none';
//		  DivAddFee.style.display='none';
//			DivMult.style.display='none';
//			DivSpec.style.display='none';
			showHistoryUWIdea();
		}
		else{
			divCancelReason.style.display='none';
		}
		if(uwstate=="1")
		{
			divRejectReason.style.display='';
		}
		else{
			divRejectReason.style.display='none';
		}
		
		
	}
}

//填充核保结论
function UWResultInfo()
{
	var ContNo = fm.ContNo.value;
	if(ContNo==null||ContNo=="")
	{
		alert("没有可用的可合同信息！");
		return false;
	}
	var row = RiskGrid.getSelNo() - 1;
	var polno = RiskGrid.getRowColData(row,1);
	var strSQL = "select PassFlag,UWIdea,PostponeDay,PostponeDate,CustomerReply,PostPoneDescribe,RejectReason from LCUWMaster where polno='"+polno+"' with ur";
	var arr  = easyExecSql(strSQL);
	if(arr)
	{
			fm.uwstate.value = arr[0][0];
				if(fm.uwstate.value==5){
					fm.uwstateName.value="";
					}
			fm.UWIdea.value = arr[0][1];
			fm.inputDelayDays.value = arr[0][2];
			fm.inputDelayDate.value = arr[0][3];
			fm.inputDelayDescribe.value = arr[0][5];
	}
	if(fm.uwstate.value=='L')
	{
		divPropValueName.style.display="";
		divPropValue.style.display="";
		var strSQL = "select prem from lcprem where payplancode like '000000%25' and PolNo='"+polno+"' with ur";
		var arr = easyExecSql(strSQL);
		
		if(arr)
		{
			fm.PropValue.value = arr[0][0];
		}
	}
	if(fm.uwstate.value=='E')
	{
		divPropValueName.style.display="";
		divPropValue.style.display="";
		fm.PropValue.value = fm.UWIdea.value;
	}
	if(fm.uwstate.value=='a')
	{
		divCancelReason.style.display='';
		fm.CancelReason.value = arr[0][4];
		fm.UWCheck[3].checked=true;
		initUWCheck(3);
	}	
	if(fm.uwstate.value=='9')
	{
		fm.UWCheck[0].checked=true;
		initUWCheck(0);
	}
	if(fm.uwstate.value=='8')
	{
		fm.UWCheck[1].checked=true;
		initUWCheck(1);
	}
	if(fm.uwstate.value=='1')
	{	
		divRejectReason.style.display='';
		fm.RejectReason.value = arr[0][6];
		fm.UWCheck[2].checked=true;
		initUWCheck(2);
	}
}

//查询被保人核保结论
function queryInsurdInfo(){
	fm.stopInsuredButton.style.display='none';
	var strSql="select PassFlag,UWIdea from LCIndUWMaster where Contno='"+fm.all('ContNo').value+"' and InsuredNo = '"+fm.all('InsuredNo').value+"' with ur";
	var arrResult = easyExecSql(strSql);
	if(arrResult){
		fm.SugIndUWFlag.value = "S"
		fm.uwindstate.value = arrResult[0][0];
		fm.UWIndIdea.value = arrResult[0][1];
		}
	var strSql = "select 1 from LCInsured where InsuredNo='"+InsuredNo+"' and SequenceNo='1' with ur";
	var arr = easyExecSql(strSql);
	if(arr){
		fm.stopInsuredButton.style.display='none';
	}
	var strSql3 = "select notepadcont from lcNotePad where Contno='"+fm.all('ContNo').value+"' order by SerialNo desc  with ur";
	var arr3 = easyExecSql(strSql3);
	if(arr3){
		fm.inputExplain.value = arr3[0][0];
	}
	
}
function checkAddfee()
{    
	if(AddFeeGrid.mulLineCount>1)
	{
		alert("加费只能加一个！");
		AddFeeGrid.delBlankLine();
	}
}
//将加费的信息显示出来；
function ShowAddFeeInfo()
{
	var tSelNo = RiskGrid.getSelNo();
	var PolNo = RiskGrid.getRowColData(tSelNo - 1,1);	
	var strSql = "select 1 from LCUWMaster where AddPremFlag <> '0' and PolNo='"+PolNo+"' with ur";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		var strSql1= "select 1 from LCPrem where PolNo = '" + PolNo + "' and PayPlanCode like '000000%' and SuppRiskScore > 0 with ur";
		var arr1 = easyExecSql(strSql1);
		if(arr1){//如果有评点的信息
			var strSql2= "select round(SuppRiskScore, 2), prem, paystartdate, payenddate from LCPrem where PolNo = '" + PolNo + "' and PayPlanCode like '000000%' and SuppRiskScore > 0 with ur";	
			turnPage.queryModal(strSql2, DiseaseCheckGrid);
			fm.UWCheck[4].checked=true;
			initUWCheck(4);
			DivAddFee_1_2.style.display='';
			DivAddFee_1_2_1.style.display='none';
			fm.AddFeeType.value="1";
            fm.AddFeeTypeName.value="评点加费";
			}
		else{
			var strSql3 = "select prem,round(rate,2),paystartdate,payenddate from lcprem where PolNo='"
			+PolNo+"' and substr(payplancode,1,6)='000000'  with ur";
			turnPage.queryModal(strSql3, AddFeeGrid);
			fm.UWCheck[4].checked=true;
			initUWCheck(4);
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_2_1.style.display='';
			fm.AddFeeType.value="2";
            fm.AddFeeTypeName.value="正常加费";
			}
		
	}
}
function initAddFeeInfo()
{
	if(fm.uwstate.value == "2")
	{
		DivAddFee.style.display='';
	}
}

//将免责的信息显示出来；
function ShowSpecInfo()
{
	var tSelNo = RiskGrid.getSelNo();
	var PolNo = RiskGrid.getRowColData(tSelNo - 1,1);	
	var strSql = "select 1 from LCUWMaster where SpecFlag <> '0' and PolNo='"+PolNo+"' with ur";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		var strSql = "select SpecCode,SpecContent,SpecStartDate,SpecEndDate,SerialNo from LCSpec where PolNo='"
		+PolNo+"' with ur";
		turnPage.queryModal(strSql, SpecGrid);
		fm.UWCheck[5].checked=true;
		initUWCheck(5);
	}
	else
		{
			initSpecGrid();
		}
}
//处理加费信息
function dealAddFee(PremInfo)
{
  var arr = PremInfo;
  for(var i=0;i<arr.length;i++)
  {
  	var PolNo = arr[i][0];
  	var strSql = "select prem from lcprem where PolNo='"+PolNo+"' and substr(payplancode,1,6)='000000' with ur";
  	var addArr = easyExecSql(strSql);
  	if(addArr)
  	{
  		var dealPrem = RiskGrid.getRowColData(i,5);
  		var dealPrem = dealPrem/1;//不知道强制转化double怎么用
  		var queryPrem = addArr[0][0]/1;
  		var calPrem = Math.round((dealPrem-queryPrem)*100)/100;
  		//RiskGrid.setRowColData(i,5,String(calPrem));
  		RiskGrid.setRowColDataByName(i,"AddPrem",addArr[0][0]);
  	}
  	else
  	{
  		RiskGrid.setRowColData(i,8,"0");
  	}
  }
}
//降低档次
function showSubMult()
{
	fm.initMult.value="";
	fm.inputMult.value="";
	fm.initAmnt.value="";
	fm.inputAmnt.value="";
	var initMult = "0";
	var initAmnt = "0";
	
	var tSelNo= RiskGrid.getSelNo()-1;
	var PolNo = RiskGrid.getRowColData(tSelNo,1);
	var sql = "select a.SubMultFlag,a.Mult,b.Mult,a.SubAmntFlag,a.Amnt,b.Amnt from LCUWMaster a,lcpol b where a.PolNo='"+PolNo+"' and b.PolNo='"+PolNo+"' with ur"
	var result = easyExecSql(sql);
	var arr;
	if(result)
	{
		if(result[0][0]=="1")
		{
			fm.initMult.value = result[0][1];
			fm.inputMult.value = result[0][2];
			initMult = result[0][1];
			fm.UWCheck[6].checked=true;
			initUWCheck(6);
		}
		else if(result[0][3]=="1")
		{
			fm.initAmnt.value = result[0][4];
			fm.inputAmnt.value = result[0][5];
			initAmnt = result[0][4];
			fm.UWCheck[7].checked=true;
			initUWCheck(7);
		}
		else
		{
			var strSql = "select mult,Amnt from lcpol where polno ='"+PolNo+"' with ur";
			arr = easyExecSql(strSql);
		}
	}
	if(arr)
	{
		initMult = arr[0][0];
		initAmnt = arr[0][1];
	}
	
	if(initMult!="0")
	{
		fm.initMult.value = initMult;
		
		fm.initAmnt.disabled = true;
		fm.inputAmnt.disabled = true;
		fm.AmntButton.disabled =true;
		
		fm.initAmnt.value="";
		fm.inputAmnt.value="";
		
		fm.initMult.disabled = false;
		fm.inputMult.disabled = false;
		fm.MultButton.disabled = false;
	}
	else
	{
		
		fm.initAmnt.value = initAmnt;
		fm.initMult.disabled = true;
		fm.inputMult.disabled = true;
		fm.MultButton.disabled = true;
		
		fm.initMult.value="";
		fm.inputMult.value="";
		
		fm.initAmnt.disabled = false;
		fm.inputAmnt.disabled = false;
		fm.AmntButton.disabled =false;
	}
	
}
function showDelayInfo(){
	var PassFlag = fm.uwstate.value;
	if(PassFlag == "8"){
		//结论为8 表示延期处理
		divDelay.style.display = '';
	}
}
//如果是撤销申请，将上次核保结论，包括加费，免责信息填入
function showHistoryUWIdea()
{
	var Result ="";
	var tSelNo = RiskGrid.getSelNo();
	if(tSelNo<=0)
	{
		alert("请选择险种保单！");
		return;
	}
	var tPolNo = RiskGrid.getRowColData(tSelNo - 1,1);	
	//先看是否有加费
	var strSql = "select rate,prem,paystartdate,payenddate from lcprem where payplancode like '000000%%' and PolNo='"+tPolNo+"' with ur";
	var arr = easyExecSql(strSql);
	if(arr != null){
		if(arr[0][0]>0){
			Result = "加费"+arr[0][0]*100+"％,";
		}else{
			Result = "加费"+arr[0][1]+",";
		}
		Result+="加费自"+arr[0][2]+"开始";
		if(arr[0][3]=="null"||arr[0][3]==null||arr[0][3]==""){
		}else{
			Result += "至"+arr[0][3]+"终止；";
		}
	}
	var strSql = "select SpecContent from lcspec where PolNo='"+tPolNo+"' with ur";
	var arr = easyExecSql(strSql);
	if(arr != null){
		for(var i=0;i<arr.length;i++){
			Result += arr[i][0]+";"
		}
	}
	var strSql = "select UWIdea from lcuwsub where PolNo='"+tPolNo+"' order by uwno desc  with ur";
	var arr = easyExecSql(strSql);
	if(arr != null){
		if(arr[0][0]!="")
			Result += arr[0][0]+" ；";
	}
	fm.UWIdea.value = Result;
}
function initUWCheck(flag){
	if(flag==0){//正常承保
		if(fm.UWCheck[0].checked){//如果是选中的
			//alert("1");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[4].checked=false;
			fm.UWCheck[5].checked=false;
			fm.UWCheck[6].checked=false;
			fm.UWCheck[7].checked=false;
			fm.UWCheck[8].checked=false;
			fm.UWCheck[4].disabled=true;
			fm.UWCheck[5].disabled=true;
			fm.UWCheck[6].disabled=true;
			fm.UWCheck[7].disabled=true;
			fm.UWCheck[8].disabled=true;
			fm.uwstate.value="9";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_3.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			divChangePayEndYear.style.display='none';
			
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[4].checked=false;
			fm.UWCheck[5].checked=false;
			fm.UWCheck[6].checked=false;
			fm.UWCheck[7].checked=false;
			fm.UWCheck[8].checked=false;
			fm.UWCheck[4].disabled=false;
			fm.UWCheck[5].disabled=false;
			fm.UWCheck[6].disabled=false;
			fm.UWCheck[7].disabled=false;
			fm.UWCheck[8].disabled=false;
			fm.uwstate.value="";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_3.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			divChangePayEndYear.style.display='none';
			}
		
		}
	else if(flag==1){//延期承保
		if(fm.UWCheck[1].checked){//如果是选中的
			//alert("1");
			//初始化checkbox和div
			fm.UWCheck[0].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[4].checked=false;
			fm.UWCheck[5].checked=false;
			fm.UWCheck[6].checked=false;
			fm.UWCheck[7].checked=false;
			fm.UWCheck[8].checked=false;
			fm.UWCheck[4].disabled=true;
			fm.UWCheck[5].disabled=true;
			fm.UWCheck[6].disabled=true;
			fm.UWCheck[7].disabled=true;
			fm.UWCheck[8].disabled=true;
			fm.uwstate.value="8";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_3.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			divChangePayEndYear.style.display='none';
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[0].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[4].checked=false;
			fm.UWCheck[5].checked=false;
			fm.UWCheck[6].checked=false;
			fm.UWCheck[7].checked=false;
			fm.UWCheck[8].checked=false;
			fm.UWCheck[4].disabled=false;
			fm.UWCheck[5].disabled=false;
			fm.UWCheck[6].disabled=false;
			fm.UWCheck[7].disabled=false;
			fm.UWCheck[8].disabled=false;
			fm.uwstate.value="";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_3.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			divChangePayEndYear.style.display='none';
			}		
		}
	else if(flag==2){//谢绝承保
		if(fm.UWCheck[2].checked){//如果是选中的
			//alert("1");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[0].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[4].checked=false;
			fm.UWCheck[5].checked=false;
			fm.UWCheck[6].checked=false;
			fm.UWCheck[7].checked=false;
			fm.UWCheck[8].checked=false;
			fm.UWCheck[4].disabled=true;
			fm.UWCheck[5].disabled=true;
			fm.UWCheck[6].disabled=true;
			fm.UWCheck[7].disabled=true;
			fm.UWCheck[8].disabled=true;
			fm.uwstate.value="1";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_3.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			divChangePayEndYear.style.display='none';
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[0].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[4].checked=false;
			fm.UWCheck[5].checked=false;
			fm.UWCheck[6].checked=false;
			fm.UWCheck[7].checked=false;
			fm.UWCheck[8].checked=false;
			fm.UWCheck[4].disabled=false;
			fm.UWCheck[5].disabled=false;
			fm.UWCheck[6].disabled=false;
			fm.UWCheck[7].disabled=false;
			fm.UWCheck[8].disabled=false;
			fm.uwstate.value="";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_3.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			divChangePayEndYear.style.display='none';
			}		
		}
	else if(flag==3){//撤销申请
		if(fm.UWCheck[3].checked){//如果是选中的
			//alert("1");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[0].checked=false;
			fm.UWCheck[4].checked=false;
			fm.UWCheck[5].checked=false;
			fm.UWCheck[6].checked=false;
			fm.UWCheck[7].checked=false;
			fm.UWCheck[8].checked=false;
			fm.UWCheck[4].disabled=true;
			fm.UWCheck[5].disabled=true;
			fm.UWCheck[6].disabled=true;
			fm.UWCheck[7].disabled=true;
			fm.UWCheck[8].disabled=true;
			fm.uwstate.value="a";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_3.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='';
			divChangePayEndYear.style.display='none';
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[0].checked=false;
			fm.UWCheck[4].checked=false;
			fm.UWCheck[5].checked=false;
			fm.UWCheck[6].checked=false;
			fm.UWCheck[7].checked=false;
			fm.UWCheck[8].checked=false;
			fm.UWCheck[4].disabled=false;
			fm.UWCheck[5].disabled=false;
			fm.UWCheck[6].disabled=false;
			fm.UWCheck[7].disabled=false;
			fm.UWCheck[8].disabled=false;
			fm.uwstate.value="";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_3.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			divChangePayEndYear.style.display='none';
			}				
		}
	else if(flag==4){//加费承保
		if(fm.UWCheck[4].checked){//如果是选中的
			//alert("1");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.uwstate.value="4";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='';//加费
			//MultAndAmntDiv.style.display='';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='';
			if(fm.AddFeeType.value=="2"){//评点加费
				DivAddFee_1_2.style.display='none';
				DivAddFee_1_2_1.style.display='';
				}
			else{//正常加费
				DivAddFee_1_2.style.display='';
				DivAddFee_1_2_1.style.display='none';
				}
			DivAddFee_1_3.style.display='';
			divCancelReason.style.display='none';
			
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.uwstate.value="";
			divExplainName.style.display='';
			divExplain.style.display='';
			//DivAddFee.style.display='none';//加费
			//MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_1_1.style.display='none';
			DivAddFee_1_2.style.display='none';
			DivAddFee_1_2_1.style.display='none';
			DivAddFee_1_3.style.display='none';
			divCancelReason.style.display='none';
			
			}		
		}
	else if(flag==5){//免责承保
		if(fm.UWCheck[5].checked){//如果是选中的
			//alert("1");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.uwstate.value="4";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='';//加费
			//MultAndAmntDiv.style.display='';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_2_1.style.display='';
			DivAddFee_2_2.style.display='';
			DivAddFee_2_3.style.display='';
			divCancelReason.style.display='none';
			
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.uwstate.value="";
			divExplainName.style.display='';
			divExplain.style.display='';
			//DivAddFee.style.display='none';//加费
			//MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			DivAddFee_2_1.style.display='none';
			DivAddFee_2_2.style.display='none';
			DivAddFee_2_3.style.display='none';
			divCancelReason.style.display='none';
			
			}		
		}
	else if(flag==6){//降低档次
		if(fm.UWCheck[6].checked){//如果是选中的
			var tsql = "select 1 from lmriskapp where riskcode in (select riskcode from lcpol where prtno='"
				 + fm.PrtNo.value
				 + "') and taxoptimal='Y' with ur";
			var riskresult = easyExecSql(tsql);
			if(riskresult){
				var strSQL = "select Totaldiscountfactor from lccontsub where prtno = '"
					+ fm.PrtNo.value + "' with ur";
				var arrResult = easyExecSql(strSQL);
				if (arrResult) {
					if(arrResult[0][0]<1&&arrResult[0][0]!=""){
						alert("该单有税优折扣，不可操作档次调整。");
						fm.UWCheck[6].checked=false;
						return false;
					}
				}
			}
			//123501、123601、123602三款税优不能操作降档，提示需退回复核处理降低保额 赵庆涛 20170622
			var ttsql = "select 1 from ldcode where codetype='sypremnult' and code in (select riskcode from lcpol where prtno='"
				 + fm.PrtNo.value
				 + "')  with ur";
			var arr = easyExecSql(ttsql);
			if(arr){
				alert("该税优产品不可操作降档，需退回复核处理降低保额。");
				fm.UWCheck[6].checked=false;
				return false;
			}
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.UWCheck[7].checked=false;
			fm.uwstate.value="4";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='';//加费
			MultAndAmntDiv.style.display='';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			MultAndAmntDiv_1_1.style.display='';
			MultAndAmntDiv_1_2.style.display='';
			MultAndAmntDiv_1_3.style.display='';
			MultAndAmntDiv_1_4.style.display='';
			MultAndAmntDiv_1_5.style.display='';
			MultAndAmntDiv_1_6.style.display='';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.uwstate.value="";
			divExplainName.style.display='';
			divExplain.style.display='';
			//DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			
			}		
		}
	else if(flag==7){//降低保额
		if(fm.UWCheck[7].checked){//如果是选中的
			//alert("1");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.UWCheck[6].checked=false;
			fm.uwstate.value="4";
			divExplainName.style.display='';
			divExplain.style.display='';
			DivAddFee.style.display='';//加费
			MultAndAmntDiv.style.display='';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='';
			MultAndAmntDiv_2_2.style.display='';
			MultAndAmntDiv_2_3.style.display='';
			MultAndAmntDiv_2_4.style.display='';
			MultAndAmntDiv_2_5.style.display='';
			MultAndAmntDiv_2_6.style.display='';
			divCancelReason.style.display='none';
			
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			//fm.UWCheck[5].checked=true;
			//fm.UWCheck[6].checked=true;
			//fm.UWCheck[7].checked=true;
			//fm.UWCheck[8].checked=true;
			//fm.UWCheck[4].disabled=false;
			//fm.UWCheck[5].disabled=false;
			//fm.UWCheck[6].disabled=false;
			//fm.UWCheck[7].disabled=false;
			//fm.UWCheck[8].disabled=false;
			fm.uwstate.value="";
			divExplainName.style.display='';
			divExplain.style.display='';
			//DivAddFee.style.display='none';//加费
			MultAndAmntDiv.style.display='none';//减额
			divDelay.style.display='none';//延期
			divCancelReason.display='none';//撤销申请
			divRejectReason.style.display='none';//谢绝承保
			divPropValueName.style.display='none';
			divPropValue.style.display='none';
			MultAndAmntDiv_1_1.style.display='none';
			MultAndAmntDiv_1_2.style.display='none';
			MultAndAmntDiv_1_3.style.display='none';
			MultAndAmntDiv_1_4.style.display='none';
			MultAndAmntDiv_1_5.style.display='none';
			MultAndAmntDiv_1_6.style.display='none';
			MultAndAmntDiv_2_1.style.display='none';
			MultAndAmntDiv_2_2.style.display='none';
			MultAndAmntDiv_2_3.style.display='none';
			MultAndAmntDiv_2_4.style.display='none';
			MultAndAmntDiv_2_5.style.display='none';
			MultAndAmntDiv_2_6.style.display='none';
			divCancelReason.style.display='none';
			
			}		
		}
	else if(flag==8){//变更缴费期
		if(fm.UWCheck[8].checked){//如果是选中的
			//alert("1");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.uwstate.value="4";
			divChangePayEndYear.style.display='';
			
			}
		else{//如果没有是取消的
			//alert("0");
			//初始化checkbox和div
			fm.UWCheck[1].checked=false;
			fm.UWCheck[2].checked=false;
			fm.UWCheck[3].checked=false;
			fm.UWCheck[0].checked=false;
			fm.uwstate.value="";
			divChangePayEndYear.style.display='none';
			
			}		
		}
	else{
		alert("在initUWCheck时，界面初始化出错！");
		return;
	}
	if((fm.UWCheck[4].checked==true)||(fm.UWCheck[5].checked==true)||(fm.UWCheck[6].checked==true)||(fm.UWCheck[7].checked==true)||(fm.UWCheck[8].checked==true)){
		fm.uwstate.value="4";
	}
	
	var sql = "select CodeName "
	          + "from LDCode "
	          + "where CodeType = 'uwflag' "
	          + "   and Code = '" + fm.uwstate.value + "' with ur ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.uwstateName.value = rs[0][0];  
  }
}
function showChangePayEndYear(){
	var tSelNo = RiskGrid.getSelNo();
	var PolNo = RiskGrid.getRowColData(tSelNo - 1,1);	
	var strSql = "select 1 from LCUWMaster where CHPayEndYearFlag <> '0' and PolNo='"+PolNo+"' with ur";
	var arr = easyExecSql(strSql);
	//如果已经进行过变更
		if(arr)
	{
		var strSql2 = "select a.PayEndYear,a.PayEndYearFlag,b.PayEndYear,b.PayEndYearFlag from LCUWMaster a,LCPol b where a.PolNo=b.PolNo and a.PolNo='"+PolNo+"' with ur";
		turnPage5.strQueryResult = easyQueryVer3(strSql2, 1, 0, 1);
  	arrResult5 = decodeEasyQueryResult(turnPage5.strQueryResult);
		fm.all('oldPayEndYear').value=arrResult5[0][0];
		fm.all('oldPayEndYearFlag').value=arrResult5[0][1];
		fm.all('newPayEndYear').value=arrResult5[0][2];
		fm.all('newPayEndYearFlag').value=arrResult5[0][3];
		fm.UWCheck[8].checked=true;
		initUWCheck(8);
	}
	else{//如果还没有进行变更
		var strSql2 = "select a.PayEndYear,a.PayEndYearFlag from LCPol a where a.PolNo='"+PolNo+"' with ur";
		turnPage5.strQueryResult  = easyQueryVer3(strSql2, 1, 0, 1);
  	arrResult5 = decodeEasyQueryResult(turnPage5.strQueryResult);
		fm.all('oldPayEndYear').value=arrResult5[0][0];
		fm.all('oldPayEndYearFlag').value=arrResult5[0][1];
		fm.all('newPayEndYear').value="";
		fm.all('newPayEndYearFlag').value="";
		}
	}