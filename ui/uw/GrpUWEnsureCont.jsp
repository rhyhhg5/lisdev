<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html> 
<%
//程序名称：GrpUWEnsureCont.jsp
//程序功能：既往查询
//创建日期：2005-4-26
//创建人  ： LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="GrpUWEnsureCont.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpUWEnsureContInit.jsp"%>
  <title>团体信息</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCont);">
    		</td>
    		<td class= titleImg>
    			团体基本信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrpCont" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title8>
            团体客户号
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=GrpNo >
          </TD>
          <TD  class= title8>
            投保单位名称
          </TD>
          <TD  class= input8>
            <Input class="readonly" readonly name=GrpName>
          </TD>
          <TD  class= title8 style= "display: 'none'">
            团体保障号
          </TD>
          <TD  class= input8 style= "display: 'none'">
            <Input class="readonly" readonly name=GrpPolNo>
          </TD>
        </TR>
    </table>
    </DIV>
    
    <br />
    
    <!-- 既往保全部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpInsu);">
    		</td>
    		<td class= titleImg>
    			 既往投保信息(未承保) 
    		</td>
    	</tr>
    </table>
    
	<div id="divLPGrpInsu" style="display:''" align="center">
    	<table class="common">
        	<tr class="common">
    	  		<td>
					<span id="spanEnsureGrid" ></span>
				</td>
			</tr>
		</table>
		
        <div id="divEnsureGridPage" align="center" style="display:'none'">
            <input class="cssbutton" type="button" value="首  页" onclick="turnPage1.firstPage();" /> 
            <input class="cssbutton" type="button" value="上一页" onclick="turnPage1.previousPage();" />                    
            <input class="cssbutton" type="button" value="下一页" onclick="turnPage1.nextPage();" /> 
            <input class="cssbutton" type="button" value="尾  页" onclick="turnPage1.lastPage();" />
        </div>
    </div>
    
       </DIV>
       
       <br />
       
    <!-- 既往保全部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpInsu);">
    		</td>
    		<td class= titleImg>
    			 既往投保明细信息
    		</td>
    	</tr>
    </table>
    
	<div id= "divLPGrpInsu" style="display:''" align="center">
    	<table class="common">
        	<tr class="common">
    	  		<td>
					<span id="spanEnsureDetailGrid"></span> 
				</td>
			</tr>
		</table>
		
        <div id="divEnsureDetailGridPage" align="center" style="display:'none'">
            <input class="cssbutton" type="button" value="首  页" onclick="turnPage2.firstPage();" /> 
            <input class="cssbutton" type="button" value="上一页" onclick="turnPage2.previousPage();" />                    
            <input class="cssbutton" type="button" value="下一页" onclick="turnPage2.nextPage();" /> 
            <input class="cssbutton" type="button" value="尾  页" onclick="turnPage2.lastPage();" />
        </div>
    </div>
    
    <div id=UWidea style="display:'none'">
    <table class=common border=0 width=100%>
  <TR class=common>
    <TD height="29" class=title>      团体保单核保结论
    	<input class=common name=GUWState readonly=true>
      <input class=common name=GUWStateName readonly=true>
      <!--input class="code" name=t ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);"-->
    </TD>
  </TR>
  <tr>  </tr>
  
  <TD class=title>团体保单核保意见  </TD>
  <tr>  </tr>
  <TD class=input>
    <textarea name="GUWIdea" cols="100%" rows="5" witdh=100% readonly=true class="common">    </textarea>
  </TD>
</table>
</div>

<br />

<INPUT VALUE="投保单明细" Class="cssButton" TYPE=button onclick="showGrpCont();">
     <HR>
      <!-- <INPUT VALUE = "既往询价信息" Class="cssButton" TYPE=button onclick= "showAskApp();"> -->
      <INPUT VALUE = "既往承保信息" Class="cssButton"  TYPE=button onclick= "showGrpUWHistoryCont();">
      <input value = "既往投保信息" class="cssButton" type="button" onclick="showEnsureCont();" />
      <INPUT VALUE = "既往保全信息" Class="cssButton" TYPE=button onclick= "showRejiggerApp();">
      <INPUT VALUE = "既往理赔信息" Class="cssButton" TYPE=button onclick= "showInsuApp();">
    <HR>
  	<div id = "divUWAgree" style = "display: ''">
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="GoBack();">
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>