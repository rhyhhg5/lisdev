<%
//程序名称：SysCertTakeBackSave.jsp
//程序功能：
//创建日期：2002-10-25
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%@page contentType="text/html;charset=gb2312" %>

<%!
	String buildMsg(boolean bFlag, String strMsg) {
		String strReturn = "";
		
		strReturn += "<html><script language=\"javascript\">";
		
		if( bFlag ) {
			strReturn += "  parent.fraInterface.afterSubmit('Succ', '操作成功完成');";
		} else {
			strReturn += "  parent.fraInterface.afterSubmit('Fail','" + strMsg + "');";
		}
		strReturn += "</script></html>";
		
		return strReturn;
	}
%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;

    GlobalInput tGI = new GlobalInput(); 
    tGI=(GlobalInput)session.getValue("GI");  
    
   String tCertifyNo = request.getParameter("CertifyNo"); 
   String tCertifyCode = request.getParameter("CertifyCode"); 
   String tContNo = request.getParameter("ContNo"); 
   String tMissionID = request.getParameter("MissionID"); 
   String tSubMissionID = request.getParameter("SubMissionID"); 
   String tTakeBackOperator=  request.getParameter("TakeBackOperator") ;
   String tTakeBackMakeDate = request.getParameter("TakeBackMakeDate") ;
   String tSendOutCom = request.getParameter("SendOutCom") ;
   
   ExeSQL tExeSQL=new ExeSQL();
  String tYagentcode=tExeSQL.getOneValue(" select agentcode from laagent where groupagentcode='"+tSendOutCom+"' ");
  if(!"".equals(tYagentcode) && tYagentcode!=null){
	  tSendOutCom="D"+tYagentcode;
  }
   
   String tReceiveCom= "";
	  String tagentcode=tExeSQL.getOneValue(" select agentcode from laagent where groupagentcode='"+request.getParameter("ReceiveCom")+"' ");
	  if(!"".equals(tagentcode) && tagentcode!=null){
		  tReceiveCom="D"+tagentcode;
	  }else{
		  tReceiveCom=request.getParameter("ReceiveCom");
	  }
	  
   System.out.println("tCertifyCode:"+tCertifyCode);
   System.out.println("tCertifyNo:"+tCertifyNo);
   System.out.println("tMissionID:"+tMissionID);
   System.out.println("tContNo:"+tContNo);
   System.out.println("tTakeBackOperator:"+tTakeBackOperator);
   System.out.println("TakeBackMakeDate:"+tTakeBackMakeDate);
   System.out.println("tTakeBackOperator:"+tTakeBackOperator);
   System.out.println("tSendOutCom:"+tSendOutCom);
   System.out.println("tReceiveCom:"+tReceiveCom);
  if( tGI == null ) {
  	out.println( buildMsg(false, "网页超时或者没有操作员信息") );
  	return;
  } else {
  }
  //内容待填充
	try {
  	LZSysCertifySchema tLZSysCertifySchema = new LZSysCertifySchema();
  	tLZSysCertifySchema.setCertifyCode( tCertifyCode );
		tLZSysCertifySchema.setCertifyNo( tCertifyNo );
		tLZSysCertifySchema.setTakeBackOperator( tTakeBackOperator );
		tLZSysCertifySchema.setTakeBackMakeDate( tTakeBackMakeDate );
		tLZSysCertifySchema.setSendOutCom( tSendOutCom );
		tLZSysCertifySchema.setReceiveCom( tReceiveCom );
					
	  // 准备传输数据 VData
	    String tOperate = new String();
		TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("CertifyNo",tCertifyNo);
		tTransferData.setNameAndValue("CertifyCode",tCertifyCode) ;
		tTransferData.setNameAndValue("ContNo",tContNo) ;
		tTransferData.setNameAndValue("MissionID",tMissionID);
		tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
		tTransferData.setNameAndValue("LZSysCertifySchema",tLZSysCertifySchema);
	   if(tCertifyCode == "8888" || tCertifyCode.trim().equals("8888") )	
	      tOperate = "0000001111";//回收体检通知书任务节点编码
	   if(tCertifyCode == "9999" || tCertifyCode.trim().equals("9999"))	
	      tOperate = "0000001112"; //回收核保通知书任务节点编码    
	   if(tCertifyCode == "9996" || tCertifyCode.trim().equals("9996"))	
	      tOperate = "0000001019"; //回收业务员通知书任务节点编码
	   	if(tCertifyCode == "1113" || tCertifyCode.trim().equals("1113"))	
	       tOperate = "0000001113"; //回收业务员通知书任务节点编码    
	    if(tCertifyCode == "7777" || tCertifyCode.trim().equals("7777"))	
	       tOperate = "0000001025"; //回收业务员通知书任务节点编码     
	       
	            
	   VData tVData = new VData();
	   tVData.add(tTransferData);
	   tVData.add(tGI);
	   System.out.println("tOperate:"+tOperate);
       // 数据传输
		TbWorkFlowUI tTbWorkFlowUI   = new TbWorkFlowUI();
		if (!tTbWorkFlowUI.submitData(tVData,tOperate))//执行保全核保工作流单证回收
		{
	    out.println( buildMsg(false, " 保存失败，原因是: " + tTbWorkFlowUI.mErrors.getFirstError()));
	  	return;
	  }
	  
	  out.println( buildMsg(true, "") );
	  
	} catch(Exception ex) {
		ex.printStackTrace( );
	 	out.println( buildMsg(false, " 保存失败，原因是: " + ex.toString()));
	 	return;
	}
%>