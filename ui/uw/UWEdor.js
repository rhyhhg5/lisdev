//程序名称：UWEdor.js
//程序功能：既往保全信息查询
//创建日期：2002-06-19 11:10:36
//创建人  ： wanglong
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var tPOLNO = "";
var turnPage = new turnPageClass();
var cflag = "1" //操作位置　1.以往投保信息查询


/*********************************************************************
 *  查询被保人既往保全信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */	
function queryEdor()
{
    // 书写SQL语句

	var strSQL = "";
	var i, j, m, n;
	var contno = fm.all('ContNoHide').value;
	var insureno = fm.all('InsureNoHide').value;
	
	
	if ((insureno != null) && (insureno != "null") && (insureno != ""))
	{
			 	
		 var oSql = "select lmrisk.riskcode,LCCont.AppFlag,lccont.uwflag,lmrisk.riskname,lmrisk.riskname from LCPol,lmrisk,lccont where 1=1"
							+ " and lcpol.appflag='1'"
							+ " and lcpol.insuredno ='"+insureno+"'"
							+ " and lcpol.prtno = lccont.prtno"
							+ " and lcpol.riskcode = lmrisk.riskcode"
							;
	}

   turnPage.strQueryResult  = easyQueryVer3(oSql, 1, 0, 1); 

 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("被保人信息查询失败!");
    return "";
  }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);   
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;           
 
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  return true;
}


