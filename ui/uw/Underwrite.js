//程序名称：Underwrite.js
//程序功能：个人人工核保
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
var pflag = "1";  //保单类型 1.个人单

// 标记核保师是否已查看了相应的信息
var showPolDetailFlag ;
var showAppFlag ;
var showHealthFlag ;
var QuestQueryFlag ;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  //alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
    showInfo.close();
    alert(content);
    //parent.close();
  }
  else
  { 
	var showStr="操作成功";
  	
  	showInfo.close();
  	alert(showStr);
  	//parent.close();
  	
    //执行下一步操作
  }

}

//提交后操作,服务器数据返回后执行的操作
function afterApply( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {                     
    alert(content);
    
    	// 初始化表格
	HideChangeResult();
	initPolGrid();
	divLCPol1.style.display= "";
    	divLCPol2.style.display= "none";
    	divMain.style.display = "none";    
  }
  else
  { 
	var showStr="申请成功";
  	
  	alert(showStr);
  }

}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

/*********************************************************************
 *  选择核保结后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {
	
	    //alert("uwstate" + cCodeName + Field.value);
		if( cCodeName == "UWState" ) {
			DoUWStateCodeSelect(Field.value);//loadFlag在页面出始化的时候声明

		}
}

/*********************************************************************
 *  根据不同的核保结论,处理不同的事务
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DoUWStateCodeSelect(cSelectCode) {
	
	if(trim(cSelectCode) == '6')//上保核保
	{
		 uwgrade = fm.all('UWGrade').value;
         appgrade= fm.all('AppGrade').value;
         if(uwgrade==null||uwgrade<appgrade)
         {
         	uwpopedomgrade = appgrade ;
         }
        else
         {
        	uwpopedomgrade = uwgrade ;
         }
        //alert(uwpopedomgrade);
        codeSql="#1# and Comcode like #"+ comcode+"%%#"+" and uwpopedom > #"+uwpopedomgrade+"#"	;
         //alert(codeSql);	
	}
	else
	codeSql="";
    
	
}


//既往投保信息
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  cInsureNo = fm.InsuredNo.value;
  
  if (cProposalNo != "")
  {
  	var tSelNo = PolAddGrid.getSelNo()-1;
  	showAppFlag[tSelNo] = 1 ;
	
  	window.open("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,"window1","top=0,left=0,resizable=yes ");  

  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  	
  }
}           

//以往核保记录
function showOldUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  //showModalDialog("./UWSubMain.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  if (cProposalNo != "")
  {
  	window.open("./UWSubMain.jsp?ProposalNo1="+cProposalNo,"window2","top=80,left=80,resizable=yes ");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  	
  }
}

//当前核保记录
function showNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	window.open("./UWErrMain.jsp?ProposalNo1="+cProposalNo,"window3","top=80,left=80,resizable=yes ");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  
  }
}                      

// 理赔给付查询
function ClaimGetQuery()
{
	var arrReturn = new Array();
	var tSel = PolAddGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cInsuredNo = fm.InsuredNo.value;				
		if (cInsuredNo == "")
		    return;
		  window.open("../sys/AllClaimGetQueryMain.jsp?InsuredNo=" + cInsuredNo);										
	}	
}
//保单明细信息
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	var tSelNo = PolAddGrid.getSelNo()-1;
  	showPolDetailFlag[tSelNo] = 1 ;
  	//window.open("../app/ProposalDisplay.jsp?PolNo="+cProposalNo,"window1");
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cProposalNo );
  	mSwitch.updateVar("PolNo", "", cProposalNo);
		
  	//window.open("../app/ProposalMain.jsp?LoadFlag=3");]
  	var prtNo = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
  	window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+prtNo);
  	//showInfo.close();
  }
  else
  {  	
  	alert("请先选择保单!");	
  }

}           

//体检资料查询
function showHealthQ()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	
  	window.open("./UWManuHealthQMain.jsp?ProposalNo1="+cProposalNo,"window4");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}           

//体检资料
function showHealth()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	var tSelNo = PolAddGrid.getSelNo()-1;
  	showHealthFlag[tSelNo] = 1 ;
  	window.open("./UWManuHealthMain.jsp?ProposalNo1="+cProposalNo,"window5");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}           

//条件承保
function showSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  tUWIdea = fm.all('UWIdea').value;
  if (cProposalNo != "")
  {
  	window.open("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag=3&UWIdea="+tUWIdea,"window6");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}

//生存调查报告
function showRReport()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  tUWIdea = fm.all('UWIdea').value;
  if (cProposalNo != "")
  {
  	window.open("./UWManuRReportMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+pflag,"window7");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}                     

//核保报告书
function showReport()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  tUWIdea = fm.all('UWIdea').value;
  if (cProposalNo != "")
  {
  	window.open("./UWManuReportMain.jsp?ProposalNo1="+cProposalNo,"window8");  	
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}         

//发核保通知书
function SendNotice()
{
  cProposalNo = fm.ProposalNo.value;
  fm.UWState.value = "8";
  
  if (cProposalNo != "")
  {  	
	manuchk();
  }
  else
  {  	
  	alert("请先选择保单!");
  }
}

//发首交通知书
function SendFirstNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  cOtherNoType="00"; //其他号码类型
  cCode="07";        //单据类型
  
  if (cProposalNo != "")
  {
  	showModalDialog("./UWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}

//发催办通知书
function SendPressNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  cOtherNoType="00"; //其他号码类型
  cCode="06";        //单据类型
  
  if (cProposalNo != "")
  {
  	showModalDialog("./UWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

var withdrawFlag = false;
//撤单申请查询,add by Minim
function withdrawQueryClick() {
  withdrawFlag = true;
  easyQueryClick();
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	HideChangeResult();
	initPolGrid();
	divLCPol1.style.display= "";
    divLCPol2.style.display= "none";
    divMain.style.display = "none";
	
	// 书写SQL语句
	k++;
	var uwgradestatus = fm.UWGradeStatus.value;
	var mOperate = fm.Operator.value;
	var state = fm.State.value;       //投保单所处状态 
	var strSQL = "";
	if (uwgradestatus == "1")//本级保单
	{
		strSQL = "select LCPol.ProposalNo,LCPol.PrtNo,LMRisk.RiskName,LCPol.AppntName,LCPol.InsuredName,LCPol.uwdate from LCPol,LCUWMaster,LMRisk where "+k+"="+k				 	
				 + " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'"+comcode+"','Pe')=1"
				 + " and LCPol.AppFlag='0' "
				 //+ " and LCPol.ApproveFlag = '9'"
				 + " and LCPol.UWFlag in ('3','5','6','7','8','b') "         //自动核保待人工核保
				 + " and LCPol.grppolno = '00000000000000000000'"
				 + " and LCPol.contno = '00000000000000000000'"
				 + " and LCPol.ProposalNo = LCPol.MainPolNo "
				 + " and LMRisk.RiskCode = LCPol.RiskCode "
				 + getWherePart( 'LCPol.ProposalNo','QProposalNo' )
				 + getWherePart( 'LCPol.ManageCom','QManageCom', 'like' ) //查询条件中的集中权限管理体现
				 + getWherePart( 'LCPol.AgentCode','QAgentCode' )
				 + getWherePart( 'LCPol.AgentGroup','QAgentGroup' )
				 + getWherePart( 'LCUWMaster.Operator','QOperator')
				 + getWherePart( 'LCPol.RiskCode','QRiskCode' )
				 + getWherePart( 'LCPol.PrtNo','PrtNo')
				 //+ getWherePart( 'LCPol.RiskVersion','QRiskVersion' )
				 + " and LCPol.ProposalNo = LCUWMaster.ProposalNo "
				 + " and LCUWMaster.appgrade = (select UWPopedom from LDUser where usercode = '"+mOperate+"')"
				 + " and LCPol.ManageCom like '" + comcode + "%%'" ; //集中权限管理体现	
			
	}
	else 
	  if (uwgradestatus == "2")//下级保单
	  {
		strSQL = "select LCPol.ProposalNo,LCPol.PrtNo,LMRisk.RiskName,LCPol.AppntName,LCPol.InsuredName,LCPol.uwdate  from LCPol,LCUWMaster,LMRisk where "+k+"="+k
				 + " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'"+comcode+"','Pe')=1 "
				 + " and LCPol.AppFlag='0' "
				 //+ " and LCPol.ApproveFlag = '9'"
				 + " and LCPol.UWFlag in ('3','5','6','7','8','b') "         //自动核保待人工核保
				 + " and LCPol.grppolno = '00000000000000000000'"
				 + " and LCPol.contno = '00000000000000000000'"
				 + " and LCPol.ProposalNo = LCPol.MainPolNo "
				 + " and LMRisk.RiskCode = LCPol.RiskCode "
				 + getWherePart( 'LCPol.ProposalNo','QProposalNo' )
				 + getWherePart( 'LCPol.ManageCom','QManageCom' , 'like')
				 + getWherePart( 'LCPol.AgentCode','QAgentCode' )
				 + getWherePart( 'LCPol.AgentGroup','QAgentGroup' )
				 + getWherePart( 'LCPol.RiskCode','QRiskCode' )
				 + getWherePart( 'LCPol.PrtNo','PrtNo')
				 //+ getWherePart( 'LCPol.RiskVersion','QRiskVersion' )				 
				 + " and LCPol.ProposalNo = LCUWMaster.ProposalNo "
				 + " and LCUWMaster.appgrade < (select UWPopedom from LDUser where usercode = '"+mOperate+"')"
				 + " and LCPol.ManageCom like '" + comcode + "%%'";
				 
	   }
	   else //本级+下级保单
	   {
		strSQL = "select LCPol.ProposalNo,LCPol.PrtNo,LMRisk.RiskName,LCPol.AppntName,LCPol.InsuredName,LCPol.uwdate  from LCPol,LCUWMaster,LMRisk where "+k+"="+k
				 + " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'"+comcode+"','Pe')=1 "
				 + " and LCPol.AppFlag='0' "
				 //+ " and LCPol.ApproveFlag = '9'"
				 + " and LCPol.UWFlag in ('3','5','6','7','8','b') "         //自动核保待人工核保
				 + " and LCPol.grppolno = '00000000000000000000'"
				 + " and LCPol.contno = '00000000000000000000'"
				 + " and LCPol.ProposalNo = LCPol.MainPolNo "
				 + " and LMRisk.RiskCode = LCPol.RiskCode "
				 + getWherePart( 'LCPol.ProposalNo','QProposalNo' )
				 + getWherePart( 'LCPol.ManageCom','QManageCom', 'like' )
				 + getWherePart( 'LCPol.AgentCode','QAgentCode' )
				 + getWherePart( 'LCPol.AgentGroup','QAgentGroup' )
				 + getWherePart( 'LCPol.RiskCode','QRiskCode' )
				 + getWherePart( 'LCPol.PrtNo','PrtNo')
				 //+ getWherePart( 'LCPol.RiskVersion','QRiskVersion' )				 
				 + " and LCPol.ProposalNo = LCUWMaster.ProposalNo "
				 + " and LCUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"')"
				 + " and LCPol.ManageCom like '" + comcode + "%%'";
				
	}
	
     //alert(strSQL);
	if(state == "1")
	{
		strSQL = strSQL + " and (LCPol.UWFlag in ('5','6') and LCUWMaster.QuesFlag = '0' and LCUWMaster.SpecFlag = '0' and LCUWMaster.HealthFlag = '0' and LCUWMaster.ReportFlag = '0' and LCUWMaster.PrintFlag = '0' and LCUWMaster.ChangePolFlag = '0')";
	}
	//alert(state);	
	if(state == "2")
	{
		//strSQL = strSQL + " and LCUWMaster.QuesFlag in ('0','2') and LCUWMaster.SpecFlag in ('0','2') and LCUWMaster.HealthFlag in ('0','2') and LCUWMaster.ReportFlag in ('0','2')";
		strSQL = strSQL + " and (LCUWMaster.QuesFlag = '2' or LCUWMaster.SpecFlag = '2' or LCUWMaster.HealthFlag in('3') or LCUWMaster.ReportFlag = '2' or LCUWMaster.PrintFlag = '3') and LCUWMaster.QuesFlag <> '1' and LCUWMaster.SpecFlag <> '1' and LCUWMaster.HealthFlag not in ('1','2') and LCUWMaster.ReportFlag <> '1' and LCUWMaster.PrintFlag not in ('1','2','4') and LCUWMaster.ChangePolFlag <> '1'"
				+ getWherePart( 'LCUWMaster.Operator','QOperator')
				+ " and LCPol.ApproveFlag in('9')";
	}
	
	if(state == "3")
	{
		//strSQL = strSQL + " and LCUWMaster.QuesFlag in ('0','1') and LCUWMaster.SpecFlag in ('0','1') and LCUWMaster.HealthFlag in ('0','1') and LCUWMaster.ReportFlag in ('0','1')";
		strSQL = strSQL + " and (LCUWMaster.QuesFlag = '1' or LCUWMaster.SpecFlag = '1' or LCUWMaster.HealthFlag in('1','2') or LCUWMaster.ReportFlag = '1' or LCUWMaster.PrintFlag in ('1','2','4') or LCUWMaster.ChangePolFlag = '1'or LCPol.ApproveFlag='2')"
				+ getWherePart( 'LCUWMaster.Operator','QOperator');
	}
	
	if (withdrawFlag) {
	  //strSQL = strSQL + " and LCPol.PrtNo in (select prtno from LCApplyRecallPol where ApplyType='0') ";
	  strSQL = "select LCPol.ProposalNo,LCPol.PrtNo,LMRisk.RiskName,LCPol.AppntName,LCPol.InsuredName,LCPol.uwdate  "
           + " from LCPol,LCUWMaster,LMRisk where 10=10 "
           + " and LCPol.AppFlag='0'  "
           + " and LCPol.UWFlag not in ('1','2','a','4','9')  "
           + " and LCPol.grppolno = '00000000000000000000' and LCPol.contno = '00000000000000000000' "
           + " and LCPol.ProposalNo = LCPol.MainPolNo  and LCPol.ProposalNo= LCUWMaster.ProposalNo  "
           + " and LCUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') "
           + " and LCPol.ManageCom like '" + manageCom + "%%'"
           + " and LMRisk.RiskCode = LCPol.RiskCode "
           + getWherePart( 'LCUWMaster.Operator','QOperator')
           + " and LCPol.PrtNo in (select prtno from LCApplyRecallPol where ApplyType='0')"
           ;

	  withdrawFlag = false;
	}
	strSQL = strSQL + " order by LCPol.uwdate" ; 
	//alert(strSQL);
	//execEasyQuery( strSQL );
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有没通过核保个人单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  return true;
}

// 查询按钮
function easyQueryAddClick(parm1,parm2)
{
	
	// 书写SQL语句
	k++;
	var uwgrade = fm.UWGradeStatus.value;
	var mOperate = fm.Operator.value;
	var state = fm.State.value;       //投保单所处状态 
	var tProposalNo = "";
	var strSQL = "";
	
	if(fm.all(parm1).all('InpPolGridSel').value == '1' )
	{
	//当前行第1列的值设为：选中
   		tProposalNo = fm.all(parm1).all('PolGrid1').value;
  	}

	if(state == "1")
	{
		checkDouble(tProposalNo);
	}
	
	// 初始化表格
	initPolAddGrid();
	initPolBox();
	divLCPol1.style.display= "none";
	divLCPol2.style.display= "";
	divMain.style.display = "";

	//alert(uwgrade);
	if (uwgrade == "1")
	{
		strSQL = "select LCPol.ProposalNo,LCPol.MainPolNo,LCPol.PrtNo,LCPol.RiskCode,LCPol.RiskVersion,LCPol.AppntName,LCPol.InsuredName from LCPol,LCUWMaster where "+k+"="+k				 	
				 + " and LCPol.AppFlag='0' "
				 //+ " and LCPol.ApproveFlag in ('1','9') "
				 + " and LCPol.UWFlag in ('3','5','6','7','8','b') "         //自动核保待人工核保
				 + " and LCPol.grppolno = '00000000000000000000'"
				 + " and LCPol.contno = '00000000000000000000'"
				 + " and LCPol.MainPolNo = '" + tProposalNo + "'"
				 //+ getWherePart( 'LCPol.ProposalNo','QProposalNo' )
				 //+ getWherePart( 'LCPol.ManageCom','QManageCom' )
				 + getWherePart( 'LCPol.AgentCode','QAgentCode' )
				 + getWherePart( 'LCPol.AgentGroup','QAgentGroup' )
				 + getWherePart( 'LCPol.RiskCode','QRiskCode' )
				 + getWherePart( 'LCPol.PrtNo','PrtNo')
				 //+ getWherePart( 'LCPol.RiskVersion','QRiskVersion' )
				 + " and LCPol.ProposalNo = LCUWMaster.ProposalNo "
				 //+ " and LCUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"')"
				 + " and LCPol.ManageCom like '" + manageCom + "%%'"
				 + " order by LCPol.ProposalNo";
	}
	else
	{
		strSQL = "select LCPol.ProposalNo,LCPol.MainPolNo,LCPol.PrtNo,LCPol.RiskCode,LCPol.RiskVersion,LCPol.AppntName,LCPol.InsuredName from LCPol,LCUWMaster where "+k+"="+k
				 + " and LCPol.AppFlag='0' "
				 //+ " and LCPol.ApproveFlag in ('1','9') "
				 + " and LCPol.UWFlag in ('3','5','6','7','8','b') "         //自动核保待人工核保
				 + " and LCPol.grppolno = '00000000000000000000'"
				 + " and LCPol.contno = '00000000000000000000'"
				 + " and LCPol.MainPolNo = '" + tProposalNo +"'"
				 //+ getWherePart( 'LCPol.ProposalNo','QProposalNo' )
				 //+ getWherePart( 'LCPol.ManageCom','QManageCom' )
				 + getWherePart( 'LCPol.AgentCode','QAgentCode' )
				 + getWherePart( 'LCPol.AgentGroup','QAgentGroup' )
				 + getWherePart( 'LCPol.RiskCode','QRiskCode' )
				 + getWherePart( 'LCPol.PrtNo','PrtNo')
				 //+ getWherePart( 'LCPol.RiskVersion','QRiskVersion' )
				 + " and LCPol.ProposalNo = LCUWMaster.ProposalNo "
				 //+ " and LCUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"')"
				 + " and LCPol.ManageCom like '" + manageCom + "%%'"
				 + " order by LCPol.ProposalNo";
	}
	
	//alert(strSQL);
	//execEasyQuery( strSQL );
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有没通过的核保个人单！");
    divLCPol1.style.display= "";
    divLCPol2.style.display= "none";
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolAddGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet   = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //alert("查询easyQueryAddClick"+arrDataSet.length);
  initFlag(  arrDataSet.length );
  return true;
}


function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			}// end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

//申请要人工核保保单
function checkDouble(tProposalNo)
{
	fm.PolNoHide.value = tProposalNo;
	fm.action = "./UnderwriteApply.jsp";
	fm.submit();
}	

//选择要人工核保保单
function getPolGridCho()
{
	//setproposalno();
	codeSql = "code";
	fm.UWPopedomCode.value = "";
	fm.action = "./UnderwriteCho.jsp";
	fm.submit();
	
}

function checkBackPol(ProposalNo) {
  var strSql = "select * from LCApplyRecallPol where ProposalNo='" + ProposalNo + "' and InputState='0'";
  var arrResult = easyExecSql(strSql);
  //alert(arrResult);
  
  if (arrResult != null) {
    return false;
  }
  return true;
}

//  初始化核保师是否已查看了相应的信息标记数组
function initFlag(  tlength )
{
	// 标记核保师是否已查看了相应的信息
      showPolDetailFlag =  new Array() ;
      showAppFlag = new Array() ;
      showHealthFlag = new Array() ;
      QuestQueryFlag = new Array() ;
    
     var i=0;
	  for( i = 0; i < tlength ; i++ )
		{
			showPolDetailFlag[i] = 0;
			showAppFlag[i] = 0;
			showHealthFlag[i] = 0;
			QuestQueryFlag[i] = 0;
		} 
	
	}
//下核保结论
function manuchk()
{
	
	flag = fm.all('UWState').value;
	var ProposalNo = fm.all('ProposalNo').value;
	var MainPolNo = fm.all('MainPolNoHide').value;
	
	if (trim(fm.UWState.value) == "") {
    alert("必须先录入核保结论！");
    return;
  }
   
	if (!checkBackPol(ProposalNo)) {
	  if (!confirm("该投保单有撤单申请，继续下此结论吗？")) return;
	}
	
    if (trim(fm.UWState.value) == "6") {
      if(trim(fm.UWPopedomCode.value) !="")
         fm.UWOperater.value = fm.UWPopedomCode.value
      else 
         fm.UWOperater.value = operator;
}

    var tSelNo = PolAddGrid.getSelNo()-1;
        
	if(fm.State.value == "1"&&(fm.UWState.value == "1"||fm.UWState.value == "2"||fm.UWState.value =="4"||fm.UWState.value =="6"||fm.UWState.value =="9"||fm.UWState.value =="a")) {
      if( showPolDetailFlag[tSelNo] == 0 || showAppFlag[tSelNo] == 0 || showHealthFlag[tSelNo] == 0 || QuestQueryFlag[tSelNo] == 0 ){
         var tInfo = "";
         if(showPolDetailFlag[tSelNo] == 0)
            tInfo = tInfo + " [投保单明细信息]";
         if(showAppFlag[tSelNo] == 0)   
            tInfo = tInfo + " [既往投保信息]";
         if( PolAddGrid.getRowColData(tSelNo,1,PolAddGrid) == PolAddGrid.getRowColData(tSelNo ,2,PolAddGrid)) {
         	if(showHealthFlag[tSelNo] == 0)
              tInfo = tInfo + " [体检资料录入]";
         }
         if(QuestQueryFlag[tSelNo] == 0)
            tInfo = tInfo + " [问题件查询]";
         if ( tInfo != "" ) {
         	tInfo = "有重要信息:" + tInfo + " 未查看,是否要下该核保结论?";
         	if(!window.confirm( tInfo ))
         	    return;
             }
             
        } 
     }
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.action = "./UWManuNormChk.jsp";
	fm.submit();

	if (flag != "b"&&ProposalNo == MainPolNo)
	{
		initInpBox();
   		initPolBox();
		initPolGrid();
		initPolAddGrid();
	}
}

//function manuchk()
//{
//	
//	flag = fm.all('UWState').value;
//	tUWIdea = fm.all('UWIdea').value;
//	
//	//录入承保计划变更结论要区分主附险
//	if( flag == "b")
//	{
//		cProposalNo=fm.PolNoHide.value;
//	}
//	else
//	{
//		cProposalNo=fm.ProposalNo.value;
//	}
//	
//	//alert("flag:"+flag);
//	if (cProposalNo == "")
//	{
//		alert("请先选择保单!");
//	}
//	else
//	{  	
//		if (flag == "0"||flag == "1"||flag == "4"||flag == "6"||flag == "9"||flag == "b")
//		{
//			showModalDialog("./UWManuNormMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
//		}
//		
//		if (flag == "2") //延期
//		{
//			//showModalDialog("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
//			window.open("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
//		}
//		
//		if (flag == "3") //条件承保
//		{
//			//showModalDialog("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
//			window.open("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
//		}
//		if (flag == "7") //问题件录入
//		{
//			QuestInput();
//		}
//		
//		if (flag != "b")
//		{
//		initInpBox();
//    		initPolBox();
//		initPolGrid();
//		initPolAddGrid();
//		}
//	}
//}

//问题件录入
function QuestInput()
{
	cProposalNo = fm.ProposalNo.value;  //保单号码	
	var strSql = "select * from LCUWMaster where ProposalNo='" + cProposalNo + "' and ChangePolFlag ='1'";
    //alert(strSql);
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      var tInfo = "承保计划变更未回复,确认要录入新的问题件?";
   if(!window.confirm( tInfo )){ 
          return;
        }
      } 
      
    cPrtNo = fm.PrtNoHide.value; //印刷号  
    strSql = "select * from LCPol where PrtNo='" + cPrtNo + "' and  approveflag = '2'";
    //alert(strSql);
    arrResult = easyExecSql(strSql);
    if (arrResult != null) {
      alert("已发核保通知书，但相关问题件的通知书还未全部回复,不容许在此时再录入新的问题件");        
        return;
       }       
	window.open("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window","top=0,left=0,resizable=yes ");  

}

//问题件回复
function QuestReply()
{
	cProposalNo = fm.ProposalNo.value;  //保单号码
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestReplyMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window","top=0,left=0,resizable=yes ");  
	
	initInpBox();
    initPolBox();
	initPolGrid();
	
}

//问题件查询
function QuestQuery()
{
  cProposalNo = fm.ProposalNo.value;  //保单号码

  
  if (cProposalNo != "")
  {	
  	var tSelNo = PolAddGrid.getSelNo()-1;
  	QuestQueryFlag[tSelNo] = 1 ;	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window9","top=0,left=0,resizable=yes ");  
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }
	
}

//生存调查报告查询
function RReportQuery()
{
  cProposalNo = fm.ProposalNo.value;  //保单号码

  
  if (cProposalNo != "")
  {			
	window.open("./RReportQueryMain.jsp?ProposalNo1="+cProposalNo);
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

//保单撤销申请查询
function BackPolQuery()
{
  cProposalNo = fm.ProposalNo.value;  //保单号码

  
  if (cProposalNo != "")
  {			
	window.open("./BackPolQueryMain.jsp?ProposalNo1="+cProposalNo);
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

//催办超时查询
function OutTimeQuery()
{
  cProposalNo = fm.ProposalNo.value;  //保单号码

  
  if (cProposalNo != "")
  {			
	window.open("./OutTimeQueryMain.jsp?ProposalNo1="+cProposalNo);
  }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

//保险计划变更
function showChangePlan()
{
  var cProposalNo = fm.ProposalNo.value;  //保单号码
  var cPrtNo = fm.PrtNoHide.value; //印刷号
  var cType = "ChangePlan";
  mSwitch.deleteVar( "PolNo" );
  mSwitch.addVar( "PolNo", "", cProposalNo );
  
  if (cProposalNo != ""&&cPrtNo != "")
  {	
  	 var strSql = "select * from LCIssuepol where ProposalNo='" + cProposalNo + "' and (( backobjtype in('1','4') and replyresult is null) or ( backobjtype in('2','3') and needprint = 'Y' and replyresult is null))";
     var arrResult = easyExecSql(strSql);
     if (arrResult != null) {
       var tInfo = "有未回复的问题件,确认要进行承保计划变更?";
       if(!window.confirm( tInfo ))
         	    return;
      }
    window.open("../app/ProposalEasyScan.jsp?LoadFlag=3&Type="+cType+"&prtNo="+cPrtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");  	
      
   }
  else
  {  	
  	alert("请先选择保单!");  	
  }	
}

//保险计划变更结论录入显示
function showChangeResultView()
{
	fm.ChangeIdea.value = "";
	fm.PolNoHide.value = fm.ProposalNo.value;  //保单号码
	divUWResult.style.display= "none";
	divChangeResult.style.display= "";	
}


//显示保险计划变更结论
function showChangeResult()
{
	fm.UWState.value = "b";
	fm.UWIdea.value = fm.ChangeIdea.value;
	var cProposalNo = fm.PolNoHide.value ;
	cChangeResult = fm.UWIdea.value;
	
	if (cChangeResult == "")
	{
		alert("没有录入结论!");
	}
	else
	{
		var strSql = "select * from LCIssuepol where ProposalNo='" + cProposalNo + "' and (( backobjtype in('1','4') and replyresult is null) or ( backobjtype in('2','3') and needprint = 'Y' and replyresult is null))";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
       if (arrResult != null) {
       var tInfo = "有未回复的问题件,确认要进行承保计划变更?";
       if(!window.confirm( tInfo )){
       	      HideChangeResult()
         	    return;
          }
       }
	   manuchk();
	 
    }	
	divUWResult.style.display= "";
	divChangeResult.style.display= "none";
	fm.UWState.value = "";
	fm.UWIdea.value = "";
	fm.UWPopedomCode.value = "";		
}

//隐藏保险计划变更结论
function HideChangeResult()
{
	divUWResult.style.display= "";
	divChangeResult.style.display= "none";
	fm.UWState.value = "";
	fm.UWIdea.value = "";
	fm.UWPopedomCode.value = "";			
}


function cancelchk()
{
	fm.all('UWState').value = "";
	fm.all('UWPopedomCode').value = "";
	fm.all('UWIdea').value = "";
}

function setproposalno()
{
	var count = PolGrid.getSelNo();
	fm.all('ProposalNo').value = PolGrid.getRowColData(count - 1,1,PolGrid);
}

//附件险按钮隐藏函数
function hideAddButton()
{
	parent.fraInterface.divAddButton1.style.display= "none";
	parent.fraInterface.divAddButton2.style.display= "none";
	parent.fraInterface.divAddButton3.style.display= "none";
	parent.fraInterface.divAddButton4.style.display= "none";
	parent.fraInterface.divAddButton5.style.display= "none";
	parent.fraInterface.divAddButton6.style.display= "none";
	parent.fraInterface.divAddButton7.style.display= "none";
	parent.fraInterface.divAddButton8.style.display= "none";
	parent.fraInterface.divAddButton9.style.display= "none";
	parent.fraInterface.divAddButton10.style.display= "none";
	//parent.fraInterface.fm.UWState.CodeData = "0|^4|通融承保^9|正常承保";
	parent.fraInterface.UWResult.innerHTML="核保结论<Input class=\"code\" name=UWState CodeData = \"0|^4|通融/条件承保^9|正常承保\" ondblclick= \"showCodeListEx('UWState1',[this,''],[0,1]);\" onkeyup=\"showCodeListKeyEx('UWState1',[this,''],[0,1]);\">";
	

}

//显示隐藏按钮
function showAddButton()
{	
	parent.fraInterface.divAddButton1.style.display= "";
	parent.fraInterface.divAddButton2.style.display= "";
	parent.fraInterface.divAddButton3.style.display= "";
	parent.fraInterface.divAddButton4.style.display= "";
	parent.fraInterface.divAddButton5.style.display= "";
	parent.fraInterface.divAddButton6.style.display= "";
	parent.fraInterface.divAddButton7.style.display= "";
	parent.fraInterface.divAddButton8.style.display= "";
	parent.fraInterface.divAddButton9.style.display= "";
	parent.fraInterface.divAddButton10.style.display= "";
	parent.fraInterface.UWResult.innerHTML="核保结论<Input class=\"code\" name=UWState CodeData = \"0|^1|拒保^2|延期^4|通融/条件承保^6|待上级核保^9|正常承保^a|撤销投保单\" ondblclick= \"showCodeListEx('UWState',[this,''],[0,1]);\" onkeyup=\"showCodeListKeyEx('UWState',[this,''],[0,1]);\">";
	//parent.fraInterface.fm.UWState.CodeData = "0|^1|拒保^2|延期^4|通融承保^6|待上级核保^9|正常承保^a|撤销投保单";
}

function showNotePad() {
  var ProposalNo = PolAddGrid.getRowColData(PolAddGrid.getSelNo()-1, 1);
  var PrtNo = PolAddGrid.getRowColData(PolAddGrid.getSelNo()-1, 3);
  
  if (ProposalNo!="" && PrtNo!="") {
  	window.open("./UWNotePadMain.jsp?ProposalNo="+ProposalNo+"&PrtNo="+PrtNo+"&OperatePos=1", "window10");
  }
  else {
  	alert("请先选择保单!");
  }
}