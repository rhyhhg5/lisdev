<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：UWManuHealthInit.jsp
	//程序功能：保全人工核保体检资料录入
	//创建日期：2002-06-19 11:10:36
	//创建人  ：WHN
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="java.util.*"%>
<%@page import="java.lang.Math.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String tContNo = "";
	String tMissionID = "";
	String tSubMissionID = "";
	String tFlag = "";
	String tUWIdea = "";
	String tPrtNo = "";
	Date today = new Date();
	today = PubFun.calDate(today, 15, "D", null);
	String tday = UWPubFun.getFixedDate(today);
	tContNo = request.getParameter("ContNo2");
	tMissionID = request.getParameter("MissionID");
	tSubMissionID = request.getParameter("SubMissionID");
	tPrtNo = request.getParameter("PrtNo");
%>

<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
try
  {                                  
    fm.all('ContNo').value = "";
    fm.all('MissionID').value = "";
    fm.all('SubMissionID').value = '';
    fm.all('EDate').value = '<%=tday%>';
    fm.all('PrintFlag').value = '';
    fm.all('Hospital').value = '';
    fm.all('IfEmpty').value = '';
    fm.all('InsureNo').value = '';
    fm.all('Note').value = '';
    
  }
  catch(ex)
  {
    alert("在UWManuHealthInit.jsp-->InitInpBox函数中发生异常:初始化界面错误1!");
  }   
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在UWSubInit.jsp-->InitSelBox函数中发生异常:初始化界面错误2!");
  }
}                                        

function initForm(tContNo,tMissionID,tSubMission,tPrtNo)
{
  try
  {
	initInpBox();

	initHealthGrid();
	
	initCustomerGrid();

	initUWErrGrid();

	initHide(tContNo,tMissionID,tSubMission,tPrtNo);

	//initHospital(tContNo);
	initInsureNo(tPrtNo);

	initHistroyGrid();
	
	easyQueryClickSingle();

	initBooking();
	//easyQueryClick();
 showAllCodeName();  
 showHistoryPEInfo();
 }
  catch(re) {
    alert("UWManuHealthInit.jsp-->InitForm函数中发生异常:初始化界面错误3!");
  }
  
}

function initUWErrGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="投保单号";    	//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="核保顺序号";         			//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="核保意见";         		//列名
      iArray[3][1]="280px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="核保日期";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      UWErrGrid = new MulLineEnter( "fm" , "UWErrGrid" ); 
      //这些属性必须在loadMulLine前
      UWErrGrid.mulLineCount = 0;   
      UWErrGrid.displayTitle = 1;
      UWErrGrid.locked = 1;
      UWErrGrid.hiddenSubtraction = 1;
      UWErrGrid.hiddenPlus = 1;
      
      UWErrGrid.loadMulLine(iArray);  
      }
      
      catch(ex)
      {
        alert(ex);
      }
}

// 责任信息列表的初始化
function initHealthGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="体检项目代码";    	//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      if(PEFlag!=1){
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="hmchoosetestuw";           //CodeQueryBL中的引用参数         
			iArray[1][5]="1|2|3";     								//引用代码对应第几列，'|'为分割符 
			iArray[1][6]="0|1|2";    									//上面的列中放置引用代码中第几位值
			iArray[1][9]="检查项目代码|LEN<20";                                       
			iArray[1][15]="MedicaItemCode";         //数据库中字段，用于模糊查询      
			iArray[1][17]="1";                                                        
		  iArray[1][19]="1" ;   }
	else{
		iArray[1][3]=0;  
		} 
			
			iArray[2]=new Array();
      iArray[2][0]="体检项目";    	//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
     
      


      
      iArray[3]=new Array();
      iArray[3][0]="是否空腹";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      if(PEFlag!=1)
      {iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      //iArray[3][4]="yesno";           //CodeQueryBL中的引用参数         
			//iArray[3][5]="3";     								//引用代码对应第几列，'|'为分割符 
			//iArray[3][6]="0";    									//上面的列中放置引用代码中第几位值
			iArray[3][9]="是否空腹|LEN<20";                                       
			//iArray[3][15]="MedicaItemCode";         //数据库中字段，用于模糊查询
			//iArray[3][10]="yesno"; //名字最好有唯一性
      //iArray[3][11]= "0|^Y|是^N|否" ;       
			//iArray[3][17]="1";                                                        
			//iArray[3][19]="1" ;  
			}
		else
			{iArray[3][3]=0}
			  

      
      HealthGrid = new MulLineEnter( "fm" , "HealthGrid" ); 
      //这些属性必须在loadMulLine前                            
      HealthGrid.mulLineCount = 0;
      HealthGrid.displayTitle = 1;
      if(PEFlag!=1)
      {
      	HealthGrid. hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      HealthGrid. hiddenSubtraction=0;
      	}
      else{
      	HealthGrid. hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      HealthGrid. hiddenSubtraction=1;
      	}
      HealthGrid.canChk = 0;

      HealthGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //HealthGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initCustomerGrid(){

	var iArray = new Array();
      
	try {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;
		
		iArray[1]=new Array();
		iArray[1][0]="体检人客户号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[1][1]="40px";         			//列宽
		iArray[1][2]=10;          			//列最大值
		iArray[1][3]=0;
		
		iArray[2]=new Array();
		iArray[2][0]="体检人姓名";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[2][1]="60px";         			//列宽
		iArray[2][2]=10;          			//列最大值
		iArray[2][3]=0;
		
		iArray[3]=new Array();
		iArray[3][0]="年龄";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[3][1]="20px";         			//列宽
		iArray[3][2]=10;          			//列最大值
		iArray[3][3]=0;
		
		iArray[4]=new Array();
		iArray[4][0]="性别";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[4][1]="20px";         			//列宽
		iArray[4][2]=10;          			//列最大值
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="BMI";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[5][1]="20px";         			//列宽
		iArray[5][2]=10;          			//列最大值
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="身高";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[6][1]="20px";         			//列宽
		iArray[6][2]=10;          			//列最大值
		iArray[6][3]=0;
		
		iArray[7]=new Array();
		iArray[7][0]="体重";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[7][1]="20px";         			//列宽
		iArray[7][2]=10;          			//列最大值
		iArray[7][3]=0;
      
		CustomerGrid = new MulLineEnter( "fm" , "CustomerGrid" ); 
		//这些属性必须在loadMulLine前
		CustomerGrid.mulLineCount = 0;   
		CustomerGrid.displayTitle = 1;
		CustomerGrid.locked = 1;
		CustomerGrid.hiddenSubtraction = 1;
		CustomerGrid.hiddenPlus = 1;
		CustomerGrid.loadMulLine(iArray);
    
	} catch(ex) {
		alert(ex);
	}
}

function initHide(tContNo,tMissionID,tSubMission,tPrtNo)
{
	fm.all('ContNo').value = tContNo;
    fm.all('MissionID').value = tMissionID;
	fm.all('SubMissionID').value = tSubMission ;
	fm.all('PrtNo').value = tPrtNo ;
	//alert("pol:"+tContNo);
}
function initHistroyGrid()
{                               
  var iArray = new Array();
    
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="被保险人";    	//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="体检原因";         			//列名
    iArray[2][1]="40px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="体检项目";         		//列名
    iArray[3][1]="280px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="体检增项";         		//列名
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="打印流水号";         		//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="团体合同号";         		//列名
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    HistroyGrid = new MulLineEnter( "fm" , "HistroyGrid" ); 
    //这些属性必须在loadMulLine前
    HistroyGrid.canSel = 1;   
    HistroyGrid.mulLineCount = 0;   
    HistroyGrid.displayTitle = 1;
    HistroyGrid.locked = 1;
    HistroyGrid.hiddenSubtraction = 1;
    HistroyGrid.hiddenPlus = 1;
    
    HistroyGrid.loadMulLine(iArray);  
    }
    
    catch(ex)
    {
      alert(ex);
    }
}
</script>


