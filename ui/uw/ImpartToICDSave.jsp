<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuHealthQChk.jsp
//程序功能：人工核保体检资料查询
//创建日期：2005-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
 
 	String tContNo = request.getParameter("ContNo");
 	String tProposalContNo =request.getParameter("ContNo");
 	//String tCustomerNo=request.getParameter("InsureNo");
 	String tPrtSeq=request.getParameter("PrtSeq");
 	System.out.println("tContNo="+tContNo);
 	//System.out.println("tCustomerNo="+tCustomerNo);
 	System.out.println("tPrtSeq="+tPrtSeq);
 	
 	LCDiseaseResultSchema tLCDiseaseResultSchema ;
 	LCDiseaseResultSet tLCDiseaseResultSet = new LCDiseaseResultSet();
 	
	int lineCount = 0;
	String arrCount[] = request.getParameterValues("DisDesbGridNo");

	if(arrCount!=null)
	{
		String tCustomerNo[]=request.getParameterValues("DisDesbGrid1");
		String tDisDesb[]=request.getParameterValues("DisDesbGrid3");
		String tDisResult[]=request.getParameterValues("DisDesbGrid4");
		String tICDCode[]=request.getParameterValues("DisDesbGrid5");
		String tSerialNo[]=request.getParameterValues("DisDesbGrid6");
		String tDiseaseCode[]=request.getParameterValues("DisDesbGrid8");
		String tRemark[]=request.getParameterValues("DisDesbGrid10");
		
		lineCount = arrCount.length;
		System.out.println("lineCount="+lineCount);
		for(int i = 0;i<lineCount;i++)
		{
			tLCDiseaseResultSchema = new LCDiseaseResultSchema();
			tLCDiseaseResultSchema.setContNo(tContNo);
			tLCDiseaseResultSchema.setProposalContNo(tProposalContNo);
			tLCDiseaseResultSchema.setDiseaseSource("1");          //告知整理信息
			tLCDiseaseResultSchema.setCustomerNo(tCustomerNo[i]);
			tLCDiseaseResultSchema.setDisDesb(tDisDesb[i]);
			tLCDiseaseResultSchema.setSerialNo(tSerialNo[i]);
			System.out.println("tDisDesb="+tDisDesb[i]);
			System.out.println("tDisResult="+tDisResult[i]);
			System.out.println("tICDCode="+tICDCode[i]);
			tLCDiseaseResultSchema.setDisResult(tDisResult[i]);
			tLCDiseaseResultSchema.setICDCode(tICDCode[i]);
			tLCDiseaseResultSchema.setRemark(tRemark[i]);
			tLCDiseaseResultSchema.setDiseaseCode(tDiseaseCode[i]);
			tLCDiseaseResultSet.add(tLCDiseaseResultSchema);
		}
	}
	else
	{
	}
	
	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	TransferData tTransferData=new TransferData();
	tTransferData.setNameAndValue("ContNo",tContNo);
	tVData.add(tG);
	tVData.add(tLCDiseaseResultSet);
	tVData.add(tTransferData);
	
	ImpartToICDUI tImpartToICDUI = new ImpartToICDUI();
	
	try{
		System.out.println("this will save the data!!!");
		tImpartToICDUI.submitData(tVData,"");
	}
	catch(Exception ex){
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	
	if (!FlagStr.equals("Fail")){
		tError = tImpartToICDUI.mErrors;
		if (!tError.needDealError()){
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}
		else{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
