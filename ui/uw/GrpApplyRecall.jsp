<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ApplyRecall.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
 <script>
	var	tCardFlag = "<%=request.getParameter("cardflag")%>";	
	var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
	</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="GrpApplyRecall.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>撤单申请</title>
  <%@include file="GrpApplyRecallInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./GrpApplyRecallChk.jsp">
     <table  class= common align=center>
      	<TR  class= common>
          <!--<TD  class= title>  投保单号码   </TD>
          <TD  class= input> --> <Input class= common name=ContNo  type="hidden">
          <TD  class= title>  印 刷 号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo verify="印刷号码|int"> </TD>
          <TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>       
          <TD  class= title8> 业务员代码 </TD>
          <TD  class= input8> <Input class=codeNo name=AgentCode  ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >  </TD>
          <TD  class= title8 style="display:'none'"> 业务员组别 </TD>
          <TD  class= input8 style="display:'none'">
          <Input class="codeNo" name=AgentGroup verify="代理人组别|code:branchcode" ondblclick="return showCodeList('agentgroup2',[this,AgentGroupName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('agentgroup2',[this,AgentGroupName],[0,1]);"><input class=codename name=AgentGroupName readonly=true >
          </TD>
         </TR> 
         <TR  class= common>
         <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
          <TD  class= title>  生效日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=CvaliDate verify="生效日期|date"> </TD>
         	<TD  class= title> 申请日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=PolApplyDate verify="申请日期|date">   </TD>
         </TR> 
       
    </table>
    
  	<INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick1();">

    <DIV id=DivLCContInfo1 STYLE="display:''"> 
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
    		</td>
    		<td class= titleImg>
    			 要撤单的保单：
    		</td>    		
    	</tr>  	
    </table>
    </Div>
         <Div  id= "divLCPol" align=center style= "display: ''" align = left>
       		<tr  class=common>
      	  	<td text-align: left colSpan=1 >
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		<Div id= "div1" align=center style= "display: '' ">
	  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">    
    	</div>
    	</table>    
    </div>	
    <DIV id=DivLCContInfo STYLE="display:'none'"> 
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 单证催办过期的保单：
    		</td>    		
    	</tr>  	
    </table>
    </Div>
         <Div  id= "divLCPol1" align=center style= "display: 'none'" align = left>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		<Div id= "div1" align=center style= "display: 'none' ">
	  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">    
    	</div>
    	</table>    
    </div>	
    
    <table class = common>
    <TR class = common>
    <TD  class= title> 撤单原因 </TD>
    <TD  class= input>  <Input name=ApplyType class=code CodeData="0|2^0|更改保障计划^1|产品内容不满意^2|公司服务不满意^3|其他" ondblclick="showCodeListEx('ApplyType',[this,ApplyName],[0,1]);" onkeyup="return showCodeListKeyEx('ApplyType',[this,ApplyName],[0,1]);" verify="操作类型|NOTNULL"><input class=codename name=ApplyName readonly=true >  </TD>      
    <td class=title></td> <td class=input></td>   
    <td class=title></td> <td class=input></td>           
    </TR>    
    </table>
    <table class = common>  
    <TR  class= common> 
    <TD  class= title> 撤单说明 </TD>
    </TR>
    <TR  class= common>
    <TD  class= title><textarea name="Content" cols="120" rows="5" class="common"></textarea></TD>
    </TR>
    </table>
  	<input type=hidden id="PrtNo1" name="PrtNo1">
  	<input type=hidden id = "CardFlag" name = "CardFlag">
</form>  
<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
<input class = cssbutton type= "button" name= "sure" value="确  认" onClick="submitForm()">

</body>
</html>