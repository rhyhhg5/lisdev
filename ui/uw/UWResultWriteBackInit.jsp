<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWResultWriteBackInit.jsp
//程序功能：个人人工核保
//创建日期：2005-9-26 17:19
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
function initInpBox()
{

  try
  {}
  catch(ex)
  {
    alert("在PManuUWInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
  	initUWResultGrid();
  	QueryUWResultNotice();
  }
  catch(re)
  {
    alert("在PManuUWInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initUWResultGrid()
{
  var iArray = new Array();

  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名

    iArray[1]=new Array();
    iArray[1][0]="合同号";         		//列名
    iArray[1][1]="100px";         		//列名
    iArray[1][3]=3;         		//列名

    iArray[2]=new Array();
    iArray[2][0]="核保通知书号";         		//列名
    iArray[2][1]="70px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="被保险人";         		//列名
    iArray[3][1]="30px";         		//列名
    iArray[3][3]=0;         		//列名
    
    iArray[4]=new Array();
    iArray[4][0]="险种代码";         		//列名
    iArray[4][1]="30px";         		//列名
    iArray[4][3]=0;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="保额/档次";         		//列名
    iArray[5][1]="30px";         		//列名
    iArray[5][3]=0;         		//列名
    
    iArray[6]=new Array();
    iArray[6][0]="是否有加费";         		//列名
    iArray[6][1]="30px";         		//列名
    iArray[6][3]=0;         		//列名
    
    iArray[7]=new Array();
    iArray[7][0]="是否有免责";         		//列名
    iArray[7][1]="30px";         		//列名
    iArray[7][3]=0;         		//列名
    
    iArray[8]=new Array();
    iArray[8][0]="核保结论";         		//列名
    iArray[8][1]="30px";         		//列名
    iArray[8][3]=0;         		//列名
    
    iArray[9]=new Array();
    iArray[9][0]="险种号码";         		//列名
    iArray[9][1]="100px";         		//列名
    iArray[9][3]=0;         		//列名

    UWResultGrid = new MulLineEnter( "fm" , "UWResultGrid" );
    //这些属性必须在loadMulLine前

    UWResultGrid.canSel = 1;
    UWResultGrid.mulLineCount = 0;
    UWResultGrid.displayTitle = 1;
    UWResultGrid.hiddenPlus = 1;
    UWResultGrid.hiddenSubtraction = 1;
    UWResultGrid.canSel = 1;
    UWResultGrid.canChk = 0;
    //UWResultGrid.selBoxEventFuncName = "showOne";
    UWResultGrid.loadMulLine(iArray);
    //这些操作必须在loadMulLine后面
    //LDClassInfoGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>
