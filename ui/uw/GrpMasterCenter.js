//程序名称：GrpMasterCenter.js
//程序功能：管理中心
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var cflag = "4";

/*********************************************************************
 *  显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



/*********************************************************************
 *   数据返回父窗口
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}



/*********************************************************************
 *  查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryBug = 1;
function initQuery() 
{
	// 初始化表格
	initPolGrid();
	// 书写SQL语句
	var strSql = "select LCGrpPol.GrpPolNo,LCGrpPol.PrtNo,LCGrpPol.RiskCode,LCGrpPol.GrpName from LCGrpPol,lcissuepol where " + ++queryBug + "=" + queryBug
    				 + " and LCGrpPol.AppFlag='0' "                 //个人保单    				 
    				 + " and LCGrpPol.ContNo='" + contNo + "'"      //内容号，必须为20个0
    				 + " and LCGrpPol.ManageCom like '" + manageCom + "%%'"
    				 + " and LCGrpPol.ApproveFlag ='2'"    				 
    				 + " and trim(LCGrpPol.GrpPolNo) = trim(lcissuepol.ProposalNo) "
    				 + " and lcissuepol.ReplyResult is  null "
    				 + " and lcissuepol.backobjtype = '4' "
    				 + " order by LCGrpPol.makedate";
  //alert(strSql);
	turnPage.queryModal(strSql, PolGrid);
}


/*********************************************************************
 *  问题修改按钮
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
var mSwitch = parent.VD.gVSwitch;
function modifyClick() {
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<PolGrid.mulLineCount; i++) {
    if (PolGrid.getSelNo(i)) { 
      checkFlag = PolGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
  	var cPolNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
  	mSwitch.deleteVar( "PolNo" );
  	mSwitch.addVar( "PolNo", "", cPolNo );
  	
    urlStr = "./ProposalMain.jsp?LoadFlag=3";
    window.open(urlStr);
    initQuery();
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
}

/*********************************************************************
 *  问题件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
	  var i = 0;
	  var checkFlag = 0;
	  var cProposalNo = "";
	  
	  for (i=0; i<PolGrid.mulLineCount; i++) {
	    if (PolGrid.getSelNo(i)) { 
	      checkFlag = PolGrid.getSelNo();
	      break;
	    }
	  }
	  
	  if (checkFlag > 0) { 
	  	cProposalNo = PolGrid.getRowColData(checkFlag - 1, 1); 	
	  	window.open("../uw/QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	    
	  }
	  else {
	    alert("请先选择一条保单信息！"); 	    
	  }
	
	
	
}


