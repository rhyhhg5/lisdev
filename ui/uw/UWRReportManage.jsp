<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：UWRReportManage.jsp
//程序功能：契约调查管理报告录入
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
	
<script>
	var PEFlag = "<%=request.getParameter("PEFlag")%>";
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var PrtSeq = "<%=request.getParameter("PrtSeq")%>";	
	//	alert(PrtSeq);
	var Customerno = "<%=request.getParameter("Customerno")%>";
	var ProposalContNo = "<%=request.getParameter("ProposalContNo")%>";
	var PrtNo = "<%=request.getParameter("PrtNo")%>";
	var Appntno = "<%=request.getParameter("Appntno")%>";
	var currentDate = "<%=PubFun.getCurrentDate()%>";
</script>

<head >
 <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
 <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
 <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
 <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
 <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
 <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
 <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
 <SCRIPT src="./UWRReportManage.js"></SCRIPT>
 <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
 <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 <%@include file="UWRReportManageInit.jsp"%>
 <title> 契约调查管理回销录入 </title>
</head>

<body  onload="initForm('<%=tContNo%>','<%=tPrtSeq%>','<%=tCustomerno%>','<%=tPrtNo%>','<%=tAppntno%>');" >
<form method=post name=fm target="fraSubmit" action= "./UWRReportManageChk.jsp">
  <table  class= common align=center>
    <TR class= common>
      <Input type= "hidden" class="readonly" name=ContNo > 
      <INPUT  type= "hidden" class= Common name=PrtSeq>
      <INPUT  type= "hidden" class= Common name=Customerno>
      <INPUT  type= "hidden" class= Common name=Appntno>
      <INPUT  type= "hidden" class= Common name=PrtNo>
      <input  type=hidden id="fmAction" name="fmAction">
      <INPUT  type= "hidden" class= Common name=RReportItemCode>
      <INPUT  type= "hidden" class= Common name=RReportItemName>
      <INPUT  type= "hidden" class= Common name=RRItemContent>
      <INPUT  type= "hidden" class= Common name=RRItemResult>
    </TR>
  </table>

<table>
  <tr>
   <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
     <td class= titleImg>契约调查项目明细</td>  		                             
  </tr>	
</table>
   
<Div  id= "divUWSpec1" style= "display: ''">
   <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1 >
  				<span id="spanHealthGrid">
  				  </span> 
  		  </td>
  		</tr>
  </table>
<hr>  
  
<table>
  <tr>
   <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec2);"></td>
     <td class= titleImg>契约调查报告</td>  		                             
  </tr>	
</table>
   
<Div  id= "divUWSpec2" style= "display: ''" >
   <table  class= common>
      <tr  class= common>
      	<td text-align: left colSpan=1 >
  				<span id="spanUWErrGrid">
  				  </span> 
  		  </td>
  		</tr>
  </table>
  <Div id= "divPage123" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
  </Div>
 
<hr>  
<Div  id= "divAddDelButton" style= "display: ''" align=left>
    <input type =button name="modRReportButton" class=cssButton value="保  存" onclick="modifyRecord();"></TD>
</DIV>
</div>

<hr>
  <table width="80%" height="20%" class= common>
    <TR  class= common> 
      <TD width="100%" height="15%"  class= title> 契调结果</TD>
    </TR>
    <TR  class= common>
      <TD height="85%"  class= title><textarea name="Content" cols="135" rows="5" class="common" ></textarea></TD>
    </TR>
  </table>
<hr>
<Div  id= "divcontentButton" style= "display: ''" align=left>
    <input type =button name="modcontentButton" class=cssButton value="确  认" onclick="modifycontentRecord();"></TD>
    <input type =button name="backButton" class=cssButton value="返回主页面" onclick="parent.close();"> </TD>
</DIV>
<hr>
  <div id="divAppInfo" style="display: ''" align= center>
    <input value="告知整理" name="ImpartTrim" class=cssButton type=button onclick="ImpartToICD();">
    <INPUT VALUE="投保单明细" class=cssButton TYPE=button onclick="showPolDetail();"> 
    <INPUT VALUE="扫描件查询" class=cssButton  TYPE=button onclick="ScanQuery();">  
    <INPUT VALUE="既往投保信息" class=cssButton TYPE=button onclick="showApp(1);"> 
  </div>


      <div id="divAddPeItem" style="display: 'none'">
      	<INPUT type= "button" name= "sure" value="确  认" class=CssButton onclick="submitForm()">	
    	</div>		
    	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanHistroyGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
  
    <!--读取信息-->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>