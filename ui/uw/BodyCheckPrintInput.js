//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var ContNo;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //showSubmitFrame(mDebug);
  initPolGrid();
  fm.submit(); //提交
}

//提交，保存按钮对应操作
function printPol()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();

  if( tSel == 0 || tSel == null ){
		showInfo.close();
		alert( "请先选择一条记录！" );
	}
	else
	{
		     if(RePrintFlag==1){
		     			if(PolGrid.getRowColData(tSel-1,12)=="未打"){
		     				showInfo.close();
		     				alert("该单已在打印池中，不需要发送重打！");
		     				return false;
		     				}
		     	}
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		//tMissionID = PolGrid.getRowColData(tSel-1,14);
		//tSubMissionID = PolGrid.getRowColData(tSel-1,15);
		tMissionID = "";
		tSubMissionID = "";
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		var strSQL="select ContNo from lccont where PrtNo='"+tPrtNo+"' and conttype='1'";
		var arr= easyExecSql(strSQL);
		if (arr)
		{
		  	tContNo=arr[0][0];
		}
		//alert(ContNo);
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.ContNo.value = tContNo ;
		fmSave.PrtNo.value = tPrtNo ;
		fmSave.fmtransact.value = "PRINT";
		fmSave.target = "../f1print";
		if(RePrintFlag==1){
			fmSave.target = "fraSubmit";
			}
    fmSave.action = "./BodyCheckPrintSave.jsp?flag="+RePrintFlag;
		fmSave.submit();
		showInfo.close();
	}
}
//提交，保存按钮对应操作
function printPolpdf()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();

  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
  }
  	else
	{
		arrReturn = getQueryResult();
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
	}
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		//tMissionID = PolGrid.getRowColData(tSel-1,10);
		//tSubMissionID = PolGrid.getRowColData(tSel-1,11);
		tMissionID = "";
		tSubMissionID = "";
		tPrtNo = PolGrid.getRowColData(tSel-1,9);
		tContNo = PolGrid.getRowColData(tSel-1,2);
		var strSQL="select ContNo from lccont where PrtNo='"+tPrtNo+"' and conttype='1'";
		var arr= easyExecSql(strSQL);
		if (arr)
		{
		  	tContNo=arr[0][0]
		}
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.ContNo.value = tContNo ;
		fmSave.PrtNo.value = tPrtNo ;
		fmSave.fmtransact.value = "PRINT";
		fmSave.target = "fraSubmit";
		fmSave.action = "./PrintPDFSave.jsp?Code=003&RePrintFlag="+RePrintFlag;
		fmSave.submit();
		showInfo.close();
}

//提交，保存按钮对应操作
function printPolpdfNew()
{
  var i = 0;
  
  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
  }
  else
	{
		arrReturn = getQueryResult();
		if( null == arrReturn ) {
			alert("无效的数据");
			return;
		}
	}
	tPrtSeq = PolGrid.getRowColData(tSel-1,1);
	//tMissionID = PolGrid.getRowColData(tSel-1,10);
	//tSubMissionID = PolGrid.getRowColData(tSel-1,11);
	tMissionID = "";
	tSubMissionID = "";
	tPrtNo = PolGrid.getRowColData(tSel-1,9);
	tContNo = PolGrid.getRowColData(tSel-1,2);
	var strSQL="select ContNo from lccont where PrtNo='"+tPrtNo+"' and conttype='1'";
	var arr= easyExecSql(strSQL);
	if (arr)
	{
	  	tContNo=arr[0][0]
	}
	fmSave.PrtSeq.value = tPrtSeq;
	fmSave.MissionID.value = tMissionID;
	fmSave.SubMissionID.value = tSubMissionID;
	fmSave.ContNo.value = tContNo ;
	fmSave.PrtNo.value = tPrtNo ;
	fmSave.fmtransact.value = "PRINT";

	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmSave.target = "fraSubmit";
	fmSave.action = "../uw/PDFPrintSave.jsp?Code=03&OtherNo="+tContNo+"&OldPrtSeq="+tPrtSeq;
	fmSave.submit();
	showInfo.close();
}



function printvts()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //showSubmitFrame(mDebug);

  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();

  if( tSel == 0 || tSel == null ){
		showInfo.close();
		alert( "请先选择一条记录！" );
	}
  	else
	{
		arrReturn = getQueryResult();
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		tPrtNo = PolGrid.getRowColData(tSel-1,9);
		if( null == arrReturn )
			alert("无效的数据");
			return;

	}
		tPrtSeq = PolGrid.getRowColData(tSel-1,1);
		//tMissionID = PolGrid.getRowColData(tSel-1,14);
		//tSubMissionID = PolGrid.getRowColData(tSel-1,15);
		tMissionID = "";
		tSubMissionID = "";
		tPrtNo = PolGrid.getRowColData(tSel-1,2);
		var strSQL="select ContNo from lccont where PrtNo='"+tPrtNo+"' and conttype='1'";
		var arr= easyExecSql(strSQL);
		if (arr)
		{
		  	tContNo=arr[0][0]
		}
		fmSave.PrtSeq.value = tPrtSeq;
		fmSave.MissionID.value = tMissionID;
		fmSave.SubMissionID.value = tSubMissionID;
		fmSave.ContNo.value = tContNo ;
		fmSave.PrtNo.value = tPrtNo ;
		fmSave.fmtransact.value = "PRINT";
		fmSave.target = "../f1print";
    fmSave.action = "./BodyCheckPrintSave.jsp?viewflag=1";
		fmSave.submit();
		showInfo.close();
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];

	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
    tContNo = "00000120020110000050";
    top.location.href="./ProposalQueryDetail.jsp?ContNo="+tContNo;
}


// 查询按钮
function easyQueryClick()
{
    initPolGrid();
    // 书写SQL语句
    if(!verifyInput2())
        return false;
    
    var agentGroupSQL = "";
    if(fm.agentGroup.value != "")
        strSQL += " and b.AgentGroup in (select 1 from LABranchGroup bg where bg.AgentGroup = '" + fm.agentGroup.value + "') ";
    
    var tSeltype="";//是否回销
    var tStateFlag="";//是否已打
    if(RePrintFlag == "1")
    {
        if (fm.all('SelType').value=="0")
        {
            tSeltype=" and not exists (select 1 from Es_Doc_Main where SubType = 'TB15' and a.PrtSeq = DocCode) ";
        }
        else if(fm.all('SelType').value=="1")
        {
            tSeltype=" and exists (select 1 from Es_Doc_Main where SubType = 'TB15' and a.PrtSeq = DocCode) ";
        }
        
        if(fm.all('StateFlag').value=="0")
        {
            tStateFlag=" and a.StateFlag = '0' ";
        }
        else if(fm.all('StateFlag').value=="1" || fm.all('StateFlag').value=="2")
        {
            tStateFlag=" and a.StateFlag <> '0' ";
        }
    }
    else
    {
        tStateFlag=" and a.StateFlag = '0' ";
    }
	var strSQL = "SELECT a.PrtSeq, b.PrtNo, b.PolApplyDate, b.CValiDate, b.AppntName, "
        + "c.Name,b.Prem,getUniteCode(a.AgentCode),a.ManageCom,a.MakeDate,"
        + "(select MakeDate from Es_Doc_Main where DocCode=a.PrtSeq and SubType='TB15'),"
        + "case when a.StateFlag = '0' then '未打' else '已打' end StateFlag, "
        + "a.PrintTimes, c.Operator, c.Remark "
        + "FROM LOPRTManager a, LCCont b, LCPENotice c "
        + "WHERE a.Code = '03' "//a.Code='03' 单证类型为体检
        + "and b.ContType='1' and b.UWFlag not in ('1','2','4','8','9','a') "
        + "and a.OtherNo = b.ContNo "
        + "and a.PrtSeq = c.PrtSeq and a.OtherNo = c.ContNo "
        + "and exists (select 1 from lbmission lbm where lbm.missionprop3 = a.prtseq and activityid = '0000001106' and activitystatus = '3') "
        + getWherePart('b.CvaliDate', 'CvaliDate')
        + getWherePart('b.PrtNo', 'PrtNo')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('a.ManageCom', 'ManageCom', 'like')
        + getWherePart('getUniteCode(a.AgentCode)','AgentCode')
        + getWherePart('a.MakeDate','MakeDate')
        //+ getWherePart('a.AgentCode','AgentCode')
        + getWherePart('b.PolApplyDate','PolApplyDate')
        + tSeltype
        + tStateFlag
        + "order by a.MakeDate, b.PrtNo with ur"
        ;
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult) {
        alert("没有要打印的体检通知书！");
        return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strSQL;

    //设置查询起始位置
    turnPage.pageIndex       = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //tArr=chooseArray(arrDataSet,[0])
    //调用MULTILINE对象显示查询结果

    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    //displayMultiline(tArr, turnPage.pageDisplayGrid);

    fmSave.querySql.value = strSQL;
}

function queryBranch()
{
  showInfo = window.open("../certify/AgentTrussQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{

  if(arrResult!=null)
  {
  	fm.BranchGroup.value = arrResult[0][3];
  	fm.agentGroup.value = arrResult[0][0];
  }
}

var turnPage2 = new turnPageClass();
var mPrtNo;
function queryHealthPrint()
{
  var sql = "select ManageCom, (select PrtNo from LCCont where ContNo = LPPENotice.ContNo), PrtSeq, EdorNo, AppName, Name, MakeDate, " +
            "       case when PrintFlag = '0' then '未打印' " +
            "            when PrintFlag = '1' then '已打印' end " +
            "from LPPENotice " +
            "where 1 = 1 " +
            "and PrintFlag = '0'";
 // if (fmSave.HealthPrintFlag.value != "")
 // {
 //     sql += "and PrintFlag = '" + fmSave.HealthPrintFlag.value + "' ";
 // }
  if (fmSave.ManageCom1.value != "")
  {
      sql += "and ManageCom = '" + fmSave.ManageCom1.value + "' ";
  }
  if (fmSave.InsuredNo1.value != "")
  {
      sql += "and CustomerNo = '" + fmSave.InsuredNo1.value + "' ";
  }
  if ( fmSave.EdorNo.value != "")
	{
		  sql += "and EdorNo = '" + fmSave.EdorNo.value + "' ";
	}
	if (fmSave.PrtSeq1.value != "")
	{
      sql += "and PrtSeq = '" + fmSave.PrtSeq1.value + "' ";
  }
  if (fmSave.PrtNo1.value != "")
	{
      sql += "and ContNo = (select ContNo from LCCont where PrtNo = '" + fmSave.PrtNo1.value + "') ";
  }
  if (fmSave.SendDate.value != "")
	{
      sql += "and MakeDate = '" + fmSave.SendDate.value + "'";
  }
  sql += "order by PrtSeq desc ";
  turnPage2.queryModal(sql, HealthGrid);
}

function onClickedPrint()
{
  var selNo = HealthGrid.getSelNo() - 1;
  mPrtNo = HealthGrid.getRowColData(selNo, 3);
}

function printNotice()
{
	var tSel = HealthGrid.getSelNo();
  if (tSel == 0 || tSel == null)
  {
		alert( "请先选择一条记录！" );
		return false;
	}
  window.open("../bq/PEdorBodyCheckPrintMain.jsp?PrtNo=" + mPrtNo);
}

//下拉框选择后执行   zhangjianbao   2007-11-16
function afterCodeSelect(cName, Filed)
{
  if(cName=='statuskind')
  {
    saleChnl = fm.saleChannel.value;
	}
}

//选择营销机构前执行   zhangjianbao   2007-11-16
function beforeCodeSelect()
{
	if(saleChnl == "") alert("请先选择销售渠道");
}

//体检通知书清单下载
function downloadList()
{
    if(PolGrid.mulLineCount == 0)
    {
      alert("没有需要下载的数据");
      return false;
    }
    fmSave.target = '_blank';
    fmSave.action = "BodyCheckDownload.jsp";
    fmSave.submit();
}
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}