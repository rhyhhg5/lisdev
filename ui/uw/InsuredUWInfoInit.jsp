<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：InsuredUWInfoInit.jsp
//程序功能：人工核保被保人信息
//创建日期：2005-01-04 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
    out.println("session has expired");
    return;
  }

String strOperator = globalInput.Operator;
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInsuredInfoBox() {
  queryInsuredInfo();
  try {
      fm.all('ContNo').value = ContNo;
      fm.all('MissionID').value = MissionID;
      fm.all('SubMissionID').value = SubMissionID;
      fm.all('PrtNo').value = PrtNo;

    } catch(ex) {
      alert("在InsuredUWInfoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
}

function initForm() {
  try {
  		
      initInsuredInfoBox();
      initRiskGrid();
      
      initEsDocPagesGrid();
      initOldoldInfoGrid();
      initUWRateGrid();
      queryRiskInfo();
      initDiseaseGrid();
      initOtherInfoGrid();
      initSendTrace();
      showDisDesb();
      
      queryInsurdInfo();  //查询被保人核保结论
      queryOldInfo();
      initAddFeeGrid();
      initDiseaseCheckGrid();
      initSpecGrid();
      showAllCodeName();
      fm.RelationToAppnt.ondblclick="";
      fm.RelationToMainInsured.ondblclick="";
      fm.NativePlace.ondblclick="";
      fm.Sex.ondblclick="";
      fm.OccupationCode.ondblclick="";
      
    } catch(re) {
      alert("在PManuUWInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
    }
}

// 保单信息列表的初始化
function initRiskGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单险种号码";         	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="主险保单号码";         		//列名
      iArray[2][1]="0px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="险种编码";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="险种名称";         		//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="档次";         		    //列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][21]="Mult";
      
      iArray[6]=new Array();
      iArray[6][0]="保额";         		    //列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][21]="Amnt";
      
      iArray[7]=new Array();
      iArray[7][0]="基本保费";         		    //列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[7][21]="Prem";
      
      iArray[8]=new Array();
      iArray[8][0]="风险保费";         		    //列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][21]="AddPrem"; 

      iArray[9]=new Array();
      iArray[9][0]="保险起期";         		//列名
      iArray[9][1]="100px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[10]=new Array();
      iArray[10][0]="保险止期";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[11]=new Array();
      iArray[11][0]="交费间隔(月)";         	//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[12]=new Array();
      iArray[12][0]="交费年期";         	//列名
      iArray[12][1]="100px";            	//列宽
      iArray[12][2]=100;            		//列最大值
      iArray[12][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="追加保费";
      iArray[13][1]="100px";
      iArray[13][2]=100;
      iArray[13][3]=0;

      RiskGrid = new MulLineEnter( "fm" , "RiskGrid" );
      //这些属性必须在loadMulLine前
      RiskGrid.mulLineCount = 3;
      RiskGrid.displayTitle = 1;
      RiskGrid.locked = 1;
      RiskGrid.canSel = 1;
      RiskGrid.hiddenPlus = 1;
      RiskGrid.hiddenSubtraction = 1;
      RiskGrid.loadMulLine(iArray);

      RiskGrid. selBoxEventFuncName = "showRiskResult";
    } catch(ex) {
      alert(ex);
    }
}
// 核保评点列表的初始化
function initUWRateGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="代码";         	//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="名称";         		//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="评点";         		//列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      UWRateGrid = new MulLineEnter( "fm" , "UWRateGrid" );
      //这些属性必须在loadMulLine前
      UWRateGrid.mulLineCount = 0;
      UWRateGrid.displayTitle = 1;
      UWRateGrid.locked = 0;
      UWRateGrid.canSel = 0;
      UWRateGrid.hiddenPlus = 0;
      UWRateGrid.hiddenSubtraction = 0;
//      UWRateGrid.loadMulLine(iArray);

//      UWRateGrid. selBoxEventFuncName = "showRiskResult";
    } catch(ex) {
      alert(ex);
    }
}

// 和疾病风险库相关。目前风险库未上，进行注释。
/*
// 保单信息列表的初始化
function initDiseaseGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="风险代码";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="风险描述";         		//列名
      iArray[2][1]="240px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="风险等级";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="核保原则";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="险种类型";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="险类风险";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" );
      //这些属性必须在loadMulLine前
      DiseaseGrid.mulLineCount = 0;
      DiseaseGrid.displayTitle = 1;
      DiseaseGrid.locked = 1;
      DiseaseGrid.canSel = 1;
      DiseaseGrid.hiddenPlus = 1;
      DiseaseGrid.hiddenSubtraction = 1;
      DiseaseGrid.loadMulLine(iArray);

      DiseaseGrid.selBoxEventFuncName = "easyQueryDisease";

      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
    } catch(ex) {
      alert(ex);
    }
}
//------------------------------------------------------*/

// 保单信息列表的初始化
function initDiseaseGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";                    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";                  //列宽
      iArray[0][2]=30;                      //列最大值
      iArray[0][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="风险评估";              //列名
      iArray[1][1]="80px";                  //列宽
      iArray[1][2]=100;                     //列最大值
      iArray[1][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="ICD码";              //列名
      iArray[2][1]="140px";                 //列宽
      iArray[2][2]=100;                     //列最大值
      iArray[2][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="疾病文本";              //列名
      iArray[3][1]="140px";                 //列宽
      iArray[3][2]=100;                     //列最大值
      iArray[3][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="风险等级";              //列名
      iArray[4][1]="60px";                  //列宽
      iArray[4][2]=100;                     //列最大值
      iArray[4][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="观察期";               //列名
      iArray[5][1]="60px";                  //列宽
      iArray[5][2]=100;                     //列最大值
      iArray[5][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="状态";                    //列名
      iArray[6][1]="60px";                  //列宽
      iArray[6][2]=100;                     //列最大值
      iArray[6][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      if(temp.length!=0) {
          for(i=0;i<temp.length;i++) {
              iArray[7+i]=new Array();
              iArray[7+i][0]=temp[i];         //列名
              iArray[7+i][1]="50px";          //列宽
              iArray[7+i][2]=100;             //列最大值
              iArray[7+i][3]=0;              //是否允许输入,1表示允许，0表示不允许
            }
        }
      DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" );
      //这些属性必须在loadMulLine前
      DiseaseGrid.mulLineCount = 0;
      DiseaseGrid.displayTitle = 1;
      DiseaseGrid.locked = 1;
      DiseaseGrid.canSel = 0;
      DiseaseGrid.hiddenPlus = 1;
      DiseaseGrid.hiddenSubtraction = 1;
      DiseaseGrid.loadMulLine(iArray);

      //DiseaseGrid.selBoxEventFuncName = "";

      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
    } catch(ex) {
      alert(ex);
    }
}

// 保单信息列表的初始化
function initOtherInfoGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="内容";         		    //列名
      iArray[1][1]="500px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      OtherInfoGrid = new MulLineEnter( "fm" , "OtherInfoGrid" );
      //这些属性必须在loadMulLine前
      OtherInfoGrid.mulLineCount = 2;
      OtherInfoGrid.displayTitle = 1;
      OtherInfoGrid.locked = 1;
      OtherInfoGrid.canSel = 0;
      OtherInfoGrid.hiddenPlus = 1;
      OtherInfoGrid.hiddenSubtraction = 1;
      OtherInfoGrid.loadMulLine(iArray);

      //PolGrid. selBoxEventFuncName = "easyQueryAddClick";

      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
    } catch(ex) {
      alert(ex);
    }
}

// 既往信息列表的初始化
function initOldoldInfoGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="生效日期";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="险种";         		//列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="保额";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="档次";         		//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="累计保费";         		    //列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="保险期间";         		    //列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

 
      iArray[8]=new Array();
      iArray[8][0]="缴费期间";         		    //列名
      iArray[8][1]="40px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9]=new Array();
      iArray[9][0]="状态";         		    //列名
      iArray[9][1]="30px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[10]=new Array();
      iArray[10][0]="体检";         		    //列名
      iArray[10][1]="30px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[11]=new Array();
      iArray[11][0]="核保结论";         		    //列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[12]=new Array();
      iArray[12][0]="理赔次数";         		//列名
      iArray[12][1]="45px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[13]=new Array();
      iArray[13][0]="理赔总金额";         	//列名
      iArray[13][1]="80px";            		//列宽
      iArray[13][2]=100;            		//列最大值
      iArray[13][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[14]=new Array();
      iArray[14][0]="赔付比";         		//列名
      iArray[14][1]="30px";            		//列宽
      iArray[14][2]=100;            		//列最大值
      iArray[14][3]=0;              		//是否允许输入,1表示允许，0表示不允许


      iArray[15]=new Array();
      iArray[15][0]="险种保单号码";         	//列名
      iArray[15][1]="0px";            		//列宽
      iArray[15][2]=100;            		//列最大值
      iArray[15][3]=3;              		//是否允许输入,1表示允许，0表示不允许

      iArray[16]=new Array();                                                       
      iArray[16][0]="合同号码";         	//列名                                    
      iArray[16][1]="0px";            		//列宽                                    
      iArray[16][2]=100;            		//列最大值                                  
      iArray[16][3]=3;              		//是否允许输入,1表示允许，0表示不允许   
      
      iArray[17]=new Array();
      iArray[17][0]="工作流任务号";         		//列名
      iArray[17][1]="0px";            		//列宽
      iArray[17][2]=60;            			//列最大值
      iArray[17][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[18]=new Array();
      iArray[18][0]="工作流子任务号";         		//列名
      iArray[18][1]="0px";            		//列宽
      iArray[18][2]=60;            			//列最大值
      iArray[18][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[19]=new Array();
      iArray[19][0]="工作流活动Id";         		//列名
      iArray[19][1]="0px";            		//列宽
      iArray[19][2]=60;            			//列最大值
      iArray[19][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    
      
      OldoldInfoGrid = new MulLineEnter( "fm" , "OldoldInfoGrid" );                                                                                                                                                                               
      //这些属性必须在loadMulLine前                                                                                                                                                                                                               OldoldInfoGrid.locked = 1;
      //OldoldInfoGrid.mulLineCount = 1;                                                                                                                                                                                                            OldoldInfoGrid.canSel = 0;
      OldoldInfoGrid.displayTitle = 1;                                                                                                                                                                                                            OldoldInfoGrid.hiddenPlus = 1;
      OldoldInfoGrid.hiddenSubtraction = 1;
      OldoldInfoGrid.canSel = 1;
      OldoldInfoGrid.loadMulLine(iArray);

      OldoldInfoGrid.selBoxEventFuncName = "easyQueryAddClick";

      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
    } catch(ex) {
      alert(ex);
    }
}
function initAddFeeGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="费用值";         	//列名
      iArray[1][1]="20px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="费用比例";         	//列名
      iArray[2][1]="10px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="加费起始日期";         		//列名
      iArray[3][1]="10px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="加费终止日期";         		//列名
      iArray[4][1]="10px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      

      AddFeeGrid = new MulLineEnter( "fm" , "AddFeeGrid" );
      //这些属性必须在loadMulLine前
      AddFeeGrid.mulLineCount = 0;
      AddFeeGrid.displayTitle = 1;
      AddFeeGrid.locked = 0;
      AddFeeGrid.canSel = 0;
      AddFeeGrid.hiddenPlus = 0;
      AddFeeGrid.hiddenSubtraction = 0;
      AddFeeGrid.addEventFuncName="checkAddfee";
      AddFeeGrid.loadMulLine(iArray);

      //RiskGrid. selBoxEventFuncName = "showRiskResult";
    } catch(ex) {
      alert(ex);
    }
}
function initDiseaseCheckGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="评点值";         	//列名
      iArray[1][1]="20px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="加费金额";         	//列名
      iArray[2][1]="10px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="加费起始日期";         		//列名
      iArray[3][1]="10px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="加费终止日期";         		//列名
      iArray[4][1]="10px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
          

      DiseaseCheckGrid = new MulLineEnter( "fm" , "DiseaseCheckGrid" );
      //这些属性必须在loadMulLine前
      DiseaseCheckGrid.mulLineCount = 0;
      DiseaseCheckGrid.displayTitle = 1;
      DiseaseCheckGrid.locked = 0;
      DiseaseCheckGrid.canSel = 0;
      DiseaseCheckGrid.hiddenPlus = 0;
      DiseaseCheckGrid.hiddenSubtraction = 0;
      //DiseaseCheckGrid.addEventFuncName="checkAddfee";
      DiseaseCheckGrid.loadMulLine(iArray);

      //RiskGrid. selBoxEventFuncName = "showRiskResult";
    } catch(ex) {
      alert(ex);
    }
}

function initSpecGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="免责信息代码";         	//列名
      iArray[1][1]="12px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="speccode";              	//是否引用代码:null||""为不引用
      iArray[1][5]="1|2";              	    //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";              	 
      iArray[1][19]=1;

      iArray[2]=new Array();
      iArray[2][0]="免责信息";         	//列名
      iArray[2][1]="20px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="免责起始日期";         		//列名
      iArray[3][1]="10px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="免责终止日期";         		//列名
      iArray[4][1]="10px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[5]=new Array();
      iArray[5][0]="流水号";         		//列名
      iArray[5][1]="10px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许


      SpecGrid = new MulLineEnter( "fm" , "SpecGrid" );
      //这些属性必须在loadMulLine前
      SpecGrid.mulLineCount = 0;
      SpecGrid.displayTitle = 1;
      SpecGrid.locked = 0;
      SpecGrid.canSel = 0;
      SpecGrid.hiddenPlus = 0;
      SpecGrid.hiddenSubtraction = 0;
      SpecGrid.loadMulLine(iArray);

      //RiskGrid. selBoxEventFuncName = "showRiskResult";
    } catch(ex) {
      alert(ex);
    }
}
function initEsDocPagesGrid()
{                               
    var iArray = new Array();
      
      try
      {
        
		    iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列宽
        iArray[0][2]=100;            //列最大值
        iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="单证号码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=50;         //最大长度
        iArray[1][3]=1;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="单证类型编码";         //列名
        iArray[2][1]="60px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="单证类型";         //列名
        iArray[3][1]="90px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="60px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="操作员";         //列名
        iArray[5][1]="40px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
    
        iArray[6]=new Array();
        iArray[6][0]="扫描日期";         //列名
        iArray[6][1]="60px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

		    iArray[7]=new Array();
        iArray[7][0]="扫描时间";         //列名
        iArray[7][1]="60px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="修改日期";         //列名
        iArray[8][1]="60px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=3;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="修改时间";         //列名
        iArray[9][1]="60px";         //列宽
        iArray[9][2]=100;            //列最大值
        iArray[9][3]=3;              //是否允许输入,1表示允许，0表示不允许
        
        iArray[10]=new Array();
        iArray[10][0]="docid";         //列名
        iArray[10][1]="0px";         //列宽
        iArray[10][2]=100;            //列最大值
        iArray[10][3]=3;              //是否允许输入,1表示允许，0表示不允许
     
                       
        EsDocPagesGrid = new MulLineEnter( "fm" , "EsDocPagesGrid" ); 

        //这些属性必须在loadMulLine前
        EsDocPagesGrid.mulLineCount = 1;   
        EsDocPagesGrid.displayTitle = 1;
        EsDocPagesGrid.canSel=1;
        EsDocPagesGrid.hiddenSubtraction=1;
        EsDocPagesGrid.hiddenPlus=1;        
        EsDocPagesGrid.loadMulLine(iArray);  
        EsDocPagesGrid.selBoxEventFuncName ="ChangePic";

      }
      catch(ex)
      {
        alert("初始化EsDocPagesGrid时出错："+ ex);
      }
}
</script>
