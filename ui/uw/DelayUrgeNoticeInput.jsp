<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：DelayUrgeNoticeInput.jsp
//程序功能：延发催办通知书
//创建日期：2005-3-25 9:20
//创建人  ：LiHongkang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	
%>
<script>
	var operator = "<%=tGI.Operator%>";     //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var ContNo = "<%=request.getParameter("ContNo")%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="DelayUrgeNoticeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="DelayUrgeNoticeInit.jsp"%>          
  <title>延发催办通知书 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./DelayUrgeNoticeSave.jsp">
    <!-- 催办通知书信息部分 -->
    <!--%@include file="../common/jsp/OperateButton.jsp"%-->
    <!--%@include file="../common/jsp/InputButton.jsp"%-->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDelayUrgeNoticeInput1);">
    		</td>
    		<td class= titleImg>
    			 催办通知书信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divDelayUrgeNoticeInput1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanDelayUrgeNoticeInputGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<TD  class= title>
      延长催办日期到
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' dateFormat="Short" name=DateDelay verify="延长催办日期到|len<=20">
    </TD>
  	<p>
      <INPUT VALUE="延长催办当前所选通知书" class= CssButton TYPE=button onclick= "setActionKind();"> 
      <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="parent.close();"> 
  	</p>
    
  <!--	<Div  id= "divUrgeNotice2Input" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanUrgeNotice2InputGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
    </div>-->

    <input type=hidden id="fmtransact" name="fmtransact">
   
          
    </Div>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
