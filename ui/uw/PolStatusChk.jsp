<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PolStatusChk.jsp
//程序功能：保单状态查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LCContSet tLCContSet = new LCContSet();

	String tProposalContNo[] = request.getParameterValues("PolGrid1");
	String tPrtNo[] = request.getParameterValues("PolGrid2");
	String tSel[] = request.getParameterValues("InpPolGridSel");
	boolean flag = false;
	int proposalCount = tProposalContNo.length;	
        System.out.println("proposalCount=:" + proposalCount);
	for (int i = 0; i < proposalCount; i++)
	{
		if (!tProposalContNo[i].equals("") && tSel[i].equals("1"))
		{
			System.out.println("ProposalContNo:"+i+":"+tProposalContNo[i]);
	  		LCContSchema tLCContSchema = new LCContSchema();
	
		    tLCContSchema.setContNo( tProposalContNo[i] );
	    	tLCContSchema.setPrtNo(tPrtNo[i]);
		    tLCContSet.add( tLCContSchema );
		    flag = true;
		}
	}
System.out.println("3233"+tLCContSet.size());
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tG );
		System.out.println("2342342342:"+tG.Operator);
		tVData.add( tLCContSet );
		
		// 数据传输
		PolStatusChkUI tPolStatusChkUI   = new PolStatusChkUI();
		
		if (tPolStatusChkUI.submitData(tVData,"INSERT") == false)
		{        
		        
			int n = tPolStatusChkUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tPolStatusChkUI.mErrors.getError(i).errorMessage);
			Content = " 查询失败，原因是: " + tPolStatusChkUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
			
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPolStatusChkUI.mErrors;
		    //tErrors = tPolStatusChkUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	FlagStr = "Succ";
		    	
		    	LMCalModeSet tLMCalModeSet = new LMCalModeSet();
		    	VData tResult = tPolStatusChkUI.getResult();
		    	if(tResult != null)
		    	{
		    		tLMCalModeSet = (LMCalModeSet)tResult.getObjectByObjectName("LMCalModeSet",0);
		    	}
		    	
		    	if(tLMCalModeSet.size() > 0)
		    	{
%>
				<script language="javascript">					
					parent.fraInterface.PolStatuGrid.clearData ();				
				</script>         				
<%
		    	
		    		for(int i = 1;i <= tLMCalModeSet.size();i++)
		    		{
		    			LMCalModeSchema tLMCalModeSchema = new LMCalModeSchema();
		    			tLMCalModeSchema = tLMCalModeSet.get(i);
%>
					<script language="javascript">
					//alert(tLMCalModeSchema.getRemark());					
						parent.fraInterface.PolStatuGrid.addOne();
						parent.fraInterface.PolStatuGrid.setRowColData(<%=i-1%>,1,"<%=tLMCalModeSchema.getRemark()%>");						
                    			</script>         
<%		    			
		    		}
		    	}
		    	else
		    	{
		    	%>
		    	<script language="javascript">	
		    		parent.fraInterface.initPolStatuGrid();	
		    	</script> 
		    	<%}
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}
	}
	else
	{
		Content = "请选择保单！";
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
