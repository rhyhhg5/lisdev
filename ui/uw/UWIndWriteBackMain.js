//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在UWIndWirteBackMain.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}    
//查询
function easyQueryClick(){
	var tagentcode="";
	if(fm.AgentName.value!=""){
		var magentcode=easyExecSql(" select getAgentCode('"+fm.AgentName.value+"') from dual");
		tagentcode=" and b.agentcode='"+magentcode+"'";
	}	
	var strSql = "select a.missionid,a.submissionid,b.proposalcontno,a.missionprop1,a.missionprop6,"
							+"b.AppntName,b.polapplydate,b.CValiDate,getUniteCode(b.agentcode),"
							+"a.makedate,a.missionprop7 "+
							"from lwmission a,lccont b "
                            + " where a.activityid='0000001200' "
                            + " and b.uwflag not in('1','8','a')"
							+" and a.missionprop2=b.proposalcontno "
							+ getWherePart("a.missionprop1", "PrtNo")
							+ getWherePart("b.AppntName", "AppntName")
							+ getWherePart("a.makedate", "SendDate")
							+ getWherePart("a.missionprop7", "ReceiveDate")
							+ getWherePart("b.ManageCom", "ManageCom","like")
							+ getReceiveWherePart("a.missionprop7", "Receive")
							+ getInsuredWherePart("b.contno", "InsuredName")
							+ tagentcode;
	turnPage.queryModal(strSql,CustomerGrid);
	if(CustomerGrid.mulLineCount==0){
		alert("没有查询到符合条件的结果！");
		initForm();
	}
}

function getInsuredWherePart(fieldname,fieldvalue)
{
	var SqlValue = eval("fm." + trim(fieldvalue) + ".value");
	if(SqlValue == "")
	{
		return SqlValue;
	}
	var strSql = " and "+fieldname + " in (select contno from lcinsured where name ='"+SqlValue+"') ";
	return strSql;	
}
//回复日期
function getReceiveWherePart(fieldname,fieldvalue)
{
	var SqlValue = eval("fm." + trim(fieldvalue) + ".value");
	if(SqlValue == "")
	{
		return SqlValue;
	}
	var wherePart=""
	if(SqlValue == "0"){
		wherePart = " is null ";
	}else if (SqlValue == "1"){
		wherePart = " is not null ";
	}
	var strSql = " and missionprop7 "+wherePart;
	return strSql;	
}
//进入明晰信息
function intoCont(){
	var sel = CustomerGrid.getSelNo();
	if(sel == 0){
		alert("请选择一个合同信息！");
		return;
	}
	var tMissionID = CustomerGrid.getRowColData(sel-1,1);
	var tSubMissionID = CustomerGrid.getRowColData(sel-1,2);
	var tProposalContNo = CustomerGrid.getRowColData(sel-1,3);
	var tPrtNo = CustomerGrid.getRowColData(sel-1,4);
	var tBackDate = CustomerGrid.getRowColData(sel-1,11);
	if(tMissionID=="" || tSubMissionID=="" || tProposalContNo==""){
		alert("数据信息不完整，请程序员检查信息！");
		return;
	}
	if(tBackDate == null || tBackDate == "null" || tBackDate == ""){
		alert("还未扫描回销，因此不能进行回复意见录入");
		return;
	}
	var urlStr = "./UWIndWriteBackFrame.jsp?MissionID="+tMissionID+"&SubMissionID="+tSubMissionID+"&ProposalContNo="+tProposalContNo+"&PrtNo="+tPrtNo;
	var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
	window.open(urlStr,"","");
}