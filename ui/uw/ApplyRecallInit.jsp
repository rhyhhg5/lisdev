<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ApplyRecallInit.jsp
//程序功能：
//创建日期：
//创建人  :
//更新记录：  更新人    更新日期     更新原因/内容
%>                      
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
try
  {                                   
    fm.all('PrtNo').value = '';
    fm.all('Content').value = '';
    //fm.all('Quest').value = '099'; //问题件类型
    //fm.all('OperatePos').value = '9';//附加险问题键
  }
  catch(ex)
  {
    alert("ApplyRecallInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }   
}                                      

function initForm(tPrtNo,tFlag)
{
  try
  {
  
	initInpBox();
	initPolGrid();
	initContGrid();
	easyQueryClick();   
	initHidden();
  }
  catch(re)
  {
    alert("ApplyRecallInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initPolGrid()
  {                               
    var iArray = new Array();
    
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="申请时间";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="生效时间";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="投保人";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许            

      iArray[5]=new Array();                                                       
      iArray[5][0]="业务员";         		//列名                                     
      iArray[5][1]="100px";            		//列宽                                   
      iArray[5][2]=100;            			//列最大值                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
            
      iArray[6]=new Array();
      iArray[6][0]="管理机构";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
     
    
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
     
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);     
  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


function initContGrid()
  {                               
    var iArray = new Array();
    
      try
      {
       iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="申请时间";         		//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="生效时间";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="投保人";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许            

      iArray[5]=new Array();                                                       
      iArray[5][0]="业务员";         		//列名                                     
      iArray[5][1]="100px";            		//列宽                                   
      iArray[5][2]=100;            			//列最大值                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
            
      iArray[6]=new Array();
      iArray[6][0]="管理机构";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      
      iArray[7]=new Array();
    iArray[7][0]="销售渠道";                 //列名
    iArray[7][1]="40px";               //列名
    iArray[7][3]=0;                //列名

    iArray[8]=new Array();
    iArray[8][0]="网点";                 //列名
    iArray[8][1]="80px";               //列名
    iArray[8][3]=0;  
    
      ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
     
      //这些属性必须在loadMulLine前
      ContGrid.mulLineCount = 0;   
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      ContGrid.canSel = 1;
      ContGrid.hiddenPlus = 1;
      ContGrid.hiddenSubtraction = 1;
      ContGrid.loadMulLine(iArray);     
      
   
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initHidden()
{
  fm.CardFlag.value = tCardFlag; 
}

</script>


