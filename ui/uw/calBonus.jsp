<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：CutBonus.jsp
//程序功能：分红处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="CalBonus.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>分红计算</title>
</head>
<body >
  <form method=post name=fm target="fraSubmit" action= "./verifyAndCalBonus.jsp">
    <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>
            红利计算会计年度
          </TD>
          <TD  class= input>
            <Input class=common name=AccountYear>
          </TD>
        </TR>
    </table>
  <p>
   <INPUT VALUE="红利校验" class=common TYPE=button onclick="verifyBonus();"> 	
   <INPUT VALUE="红利计算" class=common TYPE=button onclick = "calBonus();"> 
   <INPUT VALUE="红利分配" class=common TYPE=button onclick = "assignBonus();"> 
   <Input type=hidden name=action >
  </p>
  <p>
   <INPUT VALUE="填写红利分配主表" class=common TYPE=hidden onclick="addLOBonusMain();"> 	
  </p>
  
  <p>
   <INPUT VALUE="查看红利分配错误日志表" class=common TYPE=button onclick="viewErrLog();"> 	
  </p> 
  
 </form>
   <form method=post name=fm2 target="fraSubmit" action= "./CreatBonusNoticeSave.jsp">
       <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>
            红利计算会计年度
          </TD>
          <TD  class= input>
            <Input class=common name=AccountYear>
          </TD>                  
        </TR>
    </table>
   <p>
   <INPUT VALUE="生成红利分配通知书" class=common TYPE=button onclick="CreatBonusNotice();"> 	
  </p> 

 </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
