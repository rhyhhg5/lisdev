//程序名称：Underwrite.js
//程序功能：个人人工核保
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnConfirmPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
var pflag = "1";  //保单类型 1.个人单
parent.fraMain.rows = "0,0,0,0,*";
// 标记核保师是否已查看了相应的信息
var showPolDetailFlag ;
var showAppFlag ;
var showHealthFlag ;
var QuestQueryFlag ;
window.onfocus=myonfocus;
//提交，保存按钮对应操作
function submitForm(flag) {
	if(!verifyInput2())
	return false;
	//校验保单状态：如果投保件处于待回复状态，须先置为待处理才能下发整单核保结论。
	if(!checkState())
	{
	  return false;
	}
	//校验是否有未回销的体检件、问题件、契约调查
	if(!checkCard())
	{
	  return false;
	}
	if(flag==0){
		var qurSql="select bussno from es_issuedoc where bussno like '"+fm.PrtNo.value+"%' and status='1' and stateflag='1'";
  	var arrqurSql=easyExecSql(qurSql);
  	if(arrqurSql){
//  		if (!confirm("该单还有扫描修改申请待审批，确认要继续吗？"))
//	       {
//	         	return;
//	       }
              //#1868 关于扫描修改待审批状态及预收保费状态时的限制规则
              alert("保单目前为扫描修改待审批状态，不能核保通过，请先完成扫描修改。");
              return;
  		}
  		qurSql="select 1 from LJSPay where OtherNo = '"+fm.PrtNo.value+"' and OtherNoType in ('9','16') and BankOnTheWayFlag = '1' ";
        arrqurSql=easyExecSql(qurSql);
        if(arrqurSql){
              alert("保单目前为银行在途时，不能新单复核通过，请财务确认回盘后再次操作。");
              return;
        }
		}
	
    // 再保校验，目前先注释掉。
	//if(flag == 0)
	//{
	//  if(!checkReInsure())
	//  {
	//    return false;
	//  }
	//}
    // ------------------------
    
  	// 在点击确认时增加豁免险保额校验 张成轩
	if(flag == 0) {
		 var s = "select 1 from lcpol where prtno = '"+fm.PrtNo.value+"' and riskcode='232901'";
			var result = easyExecSql(s);
			if (result && result[0][0] == "1") {
			}else{
    	if(!checkExemption()){
    		return false;
   		}
	    }
   	  }
   	  
  if(!checkTranSucc()){
     return false;
  } 	  
  
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  var strSql = "select a.passflag,b.InsuredStat from LCUWMaster a,lcinsured b,lcpol c "
  +" where a.proposalcontno='"+ContNo+"'"
  +" and a.contno=b.contno "
  +" and a.polno=c.polno "
  +" and c.insuredno=b.insuredno" ;
  if(fm.uwState.value==1 || fm.uwState.value=="a"){}
  else{
  var arr = easyExecSql(strSql);
  if(arr) {
      for(var a=0;a<arr.length;a++) {
      	if(arr[a][1] != 1){
          if(arr[a][0]==5 ) {
              showInfo.close();
              alert("请确认是否对所有险种已下核保结论！");
              return;
            }
          }
        }
    }
  var strSql2="select uwflag from lcpol where contno='"+ContNo+"'";
  var arr2=easyExecSql(strSql2);
  if(arr2){
  	      for(var a=0;a<arr2.length;a++) {
  	      	//alert(arr2[a][0]);
      	if(arr2[a][0] == 5||arr2[a][0] ==""){
              showInfo.close();
              alert("请确认是否对所有险种已下核保结论！");
              return;
          }
       }
  	      for(var a=0;a<arr2.length;a++) {
  	      	//alert(arr2[a][0]);
      	if(arr2[a][0] !=9){
              fm.NeedSendNoticeChk.checked = true;
              break;
          }
              fm.NeedSendNoticeChk.checked = false; 
       }     
  	}


  
  }
  
  if(flag==1) {
      fm.all('YesOrNo').value = "Y";
    }
  if(flag ==2) {
      fm.all('YesOrNo').value = "N";
    }
  //var tSelNo = PolAddGrid.getSelNo();
  //fm.all('PrtNo').value = PolAddGrid.getRowColData(tSelNo - 1,3);
  //fm.all('PolNo').value = PolAddGrid.getRowColData(tSelNo - 1,1);

  var tUWState = fm.uwState.value;
  //var tUWDelay = fm.UWDelay.value;
  var tUWIdea = fm.UWIdea.value;
  var tUWPopedomCode = fm.UWPopedomCode.value;
  if(tUWState == "") {
      showInfo.close();
      alert("请先录入承保核保结论!");

      return ;
    }
  if(tUWState == "6" && tUWPopedomCode == "") {
      showInfo.close();
      alert("若要上报核保,请选择上报级别!");

      return ;
    }

  if(tUWState == "2" && tUWDelay == "") {
      showInfo.close();
      alert("若要延期承保,请录入延长时间信息!");
      return ;
    }
  k++;
  var strsql = "select submissionid from lwmission where "+k+"="+k
               +" and activityid = '0000001110'"
               +" and missionid = '"+fm.MissionID.value+"'"
               ;

  turnConfirmPage.strQueryResult = easyQueryVer3(strsql, 1, 0, 1);
  if (!turnConfirmPage.strQueryResult) {
      //alert("未查到工作流SubMissionID");
      fm.SubConfirmMissionID.value = "";
      //return;
    }else{
    	turnConfirmPage.arrDataCacheSet = decodeEasyQueryResult(turnConfirmPage.strQueryResult);
  		fm.SubConfirmMissionID.value = turnConfirmPage.arrDataCacheSet[0][0];
    }
  
  //if(tActivityID=="0000001160")
  //{
  //	fm.ActivityID.value="0000001110";
  //}
  fm.action = "./UWManuNormChk.jsp";
  ChangeDecodeStr();
  fm.submit(); //提交
  //alert("submit");
}

/**
1、符合再保审核条件，但没有发送给再保模块，需要给出提示
2、若有未结案的再保审核任务，需要给出提示
*/
function checkReInsure()
{
  var sql = "select 1 from LCUWError a "
          + "where ContNo = '" + ContNo + "' "
          + "   and not exists (select 1 from LCReinsurTask where PolNo = a.PolNo) ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    return confirm("保单还有险种符合再保审核条件，但没有发送给再保模块，继续？");
  }
  
  sql = "select 1 from LCReinsurTask "
      + "where PolNo in(select PolNo from LCPol where ContNo = '" + ContNo + "') "
      + "   and State != '02' ";
  rs = easyExecSql(sql);
  if(rs)
  {
    return confirm("保单还有未办结的再保审核任务，继续？");
  }
  
  return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
	UnChangeDecodeStr();
	window.focus();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  if (FlagStr == "Fail" ) {
      //var showInfo = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
      //showInfo.close();
      alert(content);
      //parent.close();
    } else {
      //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
      //var showInfo = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
      //showInfo.close();
      //alert(content);
      //parent.close();

      //执行下一步操作
    }

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit3( FlagStr, content ) {
 UnChangeDecodeStr();
  showInfo.close();
  window.focus();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;

  if (FlagStr == "Fail" ) {
      alert(content);
    } else {
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
      //执行下一步操作
      InitClick();
    }

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit2( FlagStr, content ) {
	UnChangeDecodeStr();
	window.focus();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  if (FlagStr == "Fail" ) {
      showInfo.close();
      alert(content);
      //parent.close();
    } else {
      showInfo.close();
      alert(content);
      //easyQueryClick();
      //parent.close();
    }

}
//提交后操作,服务器数据返回后执行的操作
function afterApply( FlagStr, content ) {
  if (FlagStr == "Fail" ) {
      alert(content);
      // 初始化表格
      HideChangeResult();
      initPolGrid();
      divLCPol1.style.display= "";
      divLCPol2.style.display= "none";
      divMain.style.display = "none";

    } else {
      makeWorkFlow();
    }
}

function afterAddFeeApply( FlagStr, content ) {
  if (FlagStr == "Fail" ) {
      alert(content);
      // 初始化表格
    } else {
      var cPolNo=fm.ProposalNo.value;
      var cMissionID =fm.MissionID.value;
      var cSubMissionID =fm.SubMissionID.value;
      var tSelNo = PolAddGrid.getSelNo()-1;
      var cNo = PolAddGrid.getRowColData(tSelNo,1);
      var cPrtNo = PolAddGrid.getRowColData(tSelNo,3);
      var cType = PolAddGrid.getRowColData(tSelNo,7);

      if (cPrtNo != ""&& cNo !="" && cMissionID != "" ) {
          window.open("./UWManuAddMain.jsp?PrtNo="+cPrtNo+"&PolNo="+cPolNo+"&No="+cNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&Type="+cType,"window1");
          showInfo.close();
        } else {
          showInfo.close();
          alert("请先选择保单!");
        }
    }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
      parent.fraMain.rows = "0,0,50,82,*";
    } else {
      parent.fraMain.rows = "0,0,0,82,*";
    }
}

/*********************************************************************
 *  选择核保结后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) {

  //alert("uwstate" + cCodeName + Field.value);
  if( cCodeName == "uwstate" ) {
      DoUWStateCodeSelect(Field.value);//loadFlag在页面出始化的时候声明
    }
}

/*********************************************************************
 *  根据不同的核保结论,处理不同的事务
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function DoUWStateCodeSelect(cSelectCode) {

  if(trim(cSelectCode) == '6')//上保核保
    {
      uwgrade = fm.all('UWGrade').value;
      appgrade= fm.all('AppGrade').value;
      if(uwgrade==null||uwgrade<appgrade) {
          uwpopedomgrade = appgrade ;
        } else {
          uwpopedomgrade = uwgrade ;
        }
      //alert(uwpopedomgrade);
      codeSql="#1# and Comcode like #"+ comcode+"%%#"+" and popedom > #"+uwpopedomgrade+"#"	;
      // alert(codeSql);
    } else
    codeSql="";
}


//既往投保信息
function showApp(cindex) {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo=fm.ContNo.value;
  var cAppntNo = fm.AppntNo.value;

  if (cindex == 1)
    window.open("../uw/UWAppMain.jsp?ContNo="+cContNo+"&CustomerNo="+cAppntNo+"&type=1");
  else
    window.open("../uw/UWAppFamilyMain.jsp?ContNo="+cContNo);

  showInfo.close();

}

//以往核保记录
function showOldUWSub() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fm.ProposalNo.value;
  //showModalDialog("./UWSubMain.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  if (cProposalNo != "") {
      window.open("./UWSubMain.jsp?ProposalNo1="+cProposalNo,"window1");
      showInfo.close();
    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

//当前核保记录
function showNewUWSub() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var tSQL="select contno from lccont where ProposalContNo='"+fm.ProposalContNo.value+"'";
  var tarr=easyExecSql(tSQL);
  if(tarr)
  {
  cContNo=tarr[0][0]
  }
  else
  {
	alert("数据错误,未查到保单号");
	return;
  }

  //window.open("./PUWErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  window.open("./UWErrMain.jsp?ContNo="+cContNo,"window1");

  showInfo.close();

}
function MagNumUWSub(){	
var mPrtNo = fm.PrtNo.value;
var Uuid="";
 var tSQL="select Uuid from lccontsub where prtno='"+mPrtNo+"'";
 Uuid=easyExecSql(tSQL);
if(Uuid == null || Uuid == ""||Uuid=="null"){
	alert("Uuid查询失败");
	  return;
}
window.open("../magnum/decision.jsp?Uuid="+Uuid);
}


// 该投保单理赔给付查询
function ClaimGetQuery2() {
  var arrReturn = new Array();
  var tSel = PolAddGrid.getSelNo();

  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录。" );
  else {
      var cPolNo = PolAddGrid.getRowColData(tSel - 1,2);
      if (cPolNo == "")
        return;
      window.open("../sys/ClaimGetQueryMain.jsp?PolNo=" + cPolNo);
    }
}
// 理赔给付查询
function ClaimGetQuery() {
  var arrReturn = new Array();
  var tSel = PolAddGrid.getSelNo();

  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录。" );
  else {
      var cInsuredNo = fm.InsuredNo.value;
      if (cInsuredNo == "")
        return;
      window.open("../sys/AllClaimGetQueryMain.jsp?InsuredNo=" + cInsuredNo);
    }
}

//保单明细信息
function showPolDetail() {
  //var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  ////showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //
  //cContNo=fm.ContNo.value;
  //if (cContNo != "")
  //{
  //	var tSelNo = PolAddGrid.getSelNo()-1;
  //	showPolDetailFlag[tSelNo] = 1 ;
  //	mSwitch.deleteVar( "ContNo" );
  //	mSwitch.addVar( "ContNo", "", cContNo );
  //	mSwitch.updateVar("ContNo", "", cContNo);
  //	window.open("../sys/AllProQueryMain.jsp?LoadFlag=3","window1");
  //
  //}
  var cContNo = fm.ProposalContNo.value;
  var cPrtNo = fm.PrtNo.value;

  window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+cPrtNo+"&ContNo="+cContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");

}


//扫描件查询
function ScanQuery() {

  var prtNo = fm.PrtNo.value;
  if (prtNo == "")
    return;
  var subType="";
  if(prtNo.substring(0,2)=='PD'){
     subType = "TB1028";
  }else{
     subType = "TB1001";
  }
  window.open("../sys/LCProposalScanQuery.jsp?prtNo="+prtNo+"&subType="+subType, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}


//体检资料查询
function showHealthQ() {
       var Str="select 1 from LCpenotice where proposalcontno='"+fm.ContNo.value+"' ";
  var ARR = easyExecSql(Str);
  if(!ARR) {
      alert("未录入体检件!");
      return false;
    }	  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo = fm.ContNo.value;
  var cMissionID = fm.MissionID.value;
  var cSubMissionID = fm.SubMissionID.value;
  var cPrtNo = fm.PrtNo.value;
  var cLoadFlag = fm.LoadFlag.value;

  if (cContNo!= ""  ) {
      //var tSelNo = PolAddGrid.getSelNo()-1;
      //var tNo = PolAddGrid.getRowColData(tSelNo,1);
      //showHealthFlag[tSelNo] = 1 ;
      window.open("./UWManuHealthQMain.jsp?ContNo="+cContNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&PrtNo="+cPrtNo+"&LoadFlag="+cLoadFlag,"window1");
      showInfo.close();

    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

// 项目明细查询
function ItemQuery() {
  var arrReturn = new Array();
  var tSel = PolAddGrid.getSelNo();

  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击明细查询按钮。" );
  else {
      var cNo = PolAddGrid.getRowColData(tSel - 1,1);
      var cSumGetMoney = 	PolAddGrid.getRowColData(tSel - 1,8);

      if (cNo == "") {
          alert( "请先选择一条申请了承保项目的记录，再点击承保项目查询按钮。" );
          return;
        }
      window.open("../sys/AllPBqItemQueryMain.jsp?No=" + cNo + "&SumGetMoney=" + cSumGetMoney);

    }
}

//查看批单信息
function Prt() {
  var tSel = PolAddGrid.getSelNo();
  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击批单查看按钮。" );
  else {
      var cNo = PolAddGrid.getRowColData(tSel - 1,1);
      if (cNo == "") {
          alert( "请先选择一条申请了承保项目的记录，再点击承保明细查询按钮。" );
          return;
        }
      fm.all('No').value = PolAddGrid.getRowColData(tSel - 1,1);
      fm.all('PolNo').value = PolAddGrid.getRowColData(tSel - 1,2);

      var taction = parent.fraInterface.fm.action;
      var ttarget = parent.fraInterface.fm.target;
      parent.fraInterface.fm.action = "../f1print/EndorsementF1P.jsp";
      parent.fraInterface.fm.target="f1print";
      fm.submit();

      parent.fraInterface.fm.action = taction;
      parent.fraInterface.fm.target=ttarget;
      fm.all('No').value = '';
      fm.all('PolNo').value = '';
    }
}


//体检资料
function showHealth() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo = fm.ContNo.value;
  var cMissionID = fm.MissionID.value;
  var cSubMissionID = fm.SubMissionID.value;
  var cPrtNo = fm.PrtNo.value;

  if (cContNo != "") {
      //var tSelNo = PolAddGrid.getSelNo()-1;
      //var tNo = PolAddGrid.getRowColData(tSelNo,1);
      //showHealthFlag[tSelNo] = 1 ;
      window.open("./UWManuHealthMain.jsp?ContNo1="+cContNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&PrtNo="+cPrtNo,"window1");
      showInfo.close();
    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

//特约承保
function showSpec() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo=fm.ContNo.value;
  var cMissionID =fm.MissionID.value;
  var cSubMissionID =fm.SubMissionID.value;
  var tSelNo = PolAddGrid.getSelNo()-1;
  tUWIdea = fm.all('UWIdea').value;
  var cPrtNo = PolAddGrid.getRowColData(tSelNo,3);
  if (cContNo != ""&& cPrtNo !="" && cMissionID != "" ) {
      window.open("./UWManuSpecMain.jsp?ContNo="+cContNo+"&PrtNo="+cPrtNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window1");
      showInfo.close();
    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

//加费承保
function showAdd() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  var cContNo=fm.ContNo.value;
  if (cContNo != "") {
      window.open("./UWManuAddMain.jsp?ContNo="+cContNo,"window1");
    } else {
      alert("请先选择保单!");
    }
}

//生存调查报告
function showRReport() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo=fm.ContNo.value;

  var cMissionID =fm.MissionID.value;
  var cSubMissionID =fm.SubMissionID.value;
  var cPrtNo = fm.PrtNo.value;

  window.open("./UWManuRReportMain.jsp?ContNo="+cContNo+"&MissionID="+cMissionID+"&PrtNo="+cPrtNo+"&SubMissionID="+cSubMissionID+"&Flag="+pflag,"window2");
  showInfo.close();
}

//核保报告书
function showReport() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo=fm.ContNo.value;
  tUWIdea = fm.all('UWIdea').value;
  if (cContNo != "") {
      window.open("../uw/UWManuReportMain.jsp?ContNo="+cContNo,"window2");
      showInfo.close();
    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

//发核保通知书
function SendNotice() {
  cContNo = fm.ContNo.value;
  fm.uwState.value = "8";

  if (cContNo != "") {
      //manuchk();
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      var cMissionID =fm.MissionID.value;

      var cPrtNo = fm.PrtNo.value;

      fm.PrtNoHide.value = cPrtNo ;
      strsql2 = "select LWMission.SubMissionID from LWMission where 1=1"
               + " and LWMission.MissionProp1 = '" + cPrtNo +"'"
               + " and LWMission.MissionProp2 = '"+ cContNo + "'"
               + " and LWMission.ActivityID = '0000001112'"
               + " and LWMission.ProcessID = '0000000003'"
               + " and LWMission.MissionID = '" +cMissionID +"'";
      turnPage.strQueryResult = easyQueryVer3(strsql2, 1, 1, 1);
      if (turnPage.strQueryResult) {
          showInfo.close();
          alert("还有未扫描回销的核保通知书！");
          fm.SubNoticeMissionID.value = "";
          return ;
        }
      strsql = "select LWMission.SubMissionID from LWMission where 1=1"
               + " and LWMission.MissionProp1 = '" + cPrtNo +"'"
               + " and LWMission.MissionProp2 = '"+ cContNo + "'"
               + " and LWMission.ActivityID = '0000001105'"
               + " and LWMission.ProcessID = '0000000003'"
               + " and LWMission.MissionID = '" +cMissionID +"'";
      turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

      //判断是否查询成功
      if (!turnPage.strQueryResult) {
          showInfo.close();
          alert("不容许发放新的核保通知书,原因可能是:1.已发核保通知书,但未打印.2.未录入核保通知书内容.");
          fm.SubNoticeMissionID.value = "";
          return ;
        }
      turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
      fm.SubNoticeMissionID.value = turnPage.arrDataCacheSet[0][0];
      var tSubNoticeMissionID =   fm.SubNoticeMissionID.value ;
      var cMissionID =fm.MissionID.value;                  
      var cSubMissionID =fm.SubMissionID.value;            
      
      if (cContNo != "" && cPrtNo != "" && cMissionID != "" && tSubNoticeMissionID != "" ) {
          fm.action = "./UWManuSendNoticeChk.jsp?MissionID="+cMissionID+"&SubMissionID="+cSubMissionID;
          fm.submit();
        } else {
          showInfo.close();
          alert("请先选择保单!");
        }
    } else {
      alert("请先选择保单!");
    }
}    
//发体检通知书
 function SendHealth() 
 {
 	
 }
//发首交通知书
function SendFirstNotice() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fm.ProposalContNo.value;
  cOtherNoType="00"; //其他号码类型
  cCode="07";        //单据类型

  if (cProposalNo != "") {
      showModalDialog("./UWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
      showInfo.close();
    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

//发催办通知书
function SendPressNotice() {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fm.ProposalNo.value;
  cOtherNoType="00"; //其他号码类型
  cCode="06";        //单据类型

  if (cProposalNo != "") {
      showModalDialog("./UWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
      showInfo.close();
    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow) {
  if (cShow=="true") {
      cDiv.style.display="";
    } else {
      cDiv.style.display="none";
    }
}

var withdrawFlag = false;
//撤单申请查询,add by Minim
function withdrawQueryClick() {
  withdrawFlag = true;
  easyQueryClick();
}

/*********************************************************************
 *  执行新契约人工核保的EasyQuery
 *  描述:查询显示对象是合同保单.显示条件:合同未进行人工核保，或状态为待核保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick() {
  // 初始化表格

  HideChangeResult();
  initPolGrid();
  divLCPol1.style.display= "";
  divLCPol2.style.display= "none";
  divMain.style.display = "none";

  // 书写SQL语句
  k++;
  var uwgradestatus = fm.UWGradeStatus.value;
  var mOperate = fm.Operator.value;
  var state = fm.State.value;       //保单所处状态
  var strSQL = "";
  if (uwgradestatus == "1")//本级保单
    {
      strSQL = "select LWMission.MissionProp1,LWMission.MissionProp2,LWMission.MissionProp7,LWMission.MissionProp5,LWMission.MissionProp8,LWMission.MissionID ,LWMission.SubMissionID from LWMission where "+k+"="+k
               //+ " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'"+comcode+"','Pe')=1"
               + " and LWMission.ProcessID = '0000000003' " //承保核保工作流
               + " and LWMission.ActivityID = '0000001100' " //承保核保工作流中的待人工核保任务节点
               //+ getWherePart( 'LWMission.DefaultOperator','QOperator' )
               + getWherePart( 'LWMission.MissionProp1','QPrtNo' )
               + getWherePart( 'LWMission.MissionProp9','QAppntName' )
               + getWherePart( 'LWMission.MissionProp2','QProposalNo')
               + getWherePart( 'LWMission.MissionProp10','QManageCom' )
               + " and ((select UWPopedom from LDUser where usercode = '"+mOperate+"') = (select AppGrade from LccUWMaster where trim(ContNo) = LWMission.MissionProp2 ))"
               + " and LWMission.MissionProp10 like '" + comcode + "%%'";  //集中权限管理体现
    } else
    if (uwgradestatus == "2")//下级保单
      {
        strSQL = "select LWMission.MissionProp1,LWMission.MissionProp2,LWMission.MissionProp7,LWMission.MissionProp5,LWMission.MissionProp8,LWMission.MissionID ,LWMission.SubMissionID from LWMission where "+k+"="+k
                 //+ " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'"+comcode+"','Pe')=1"
                 + " and LWMission.ProcessID = '0000000003' " //承保核保工作流
                 + " and LWMission.ActivityID = '0000001100' " //承保核保工作流中的待人工核保任务节点
                 //+ getWherePart( 'LWMission.DefaultOperator','QOperator' )
                 + getWherePart( 'LWMission.MissionProp1','QPrtNo' )
                 + getWherePart( 'LWMission.MissionProp9','QAppntName' )
                 + getWherePart( 'LWMission.MissionProp3','QProposalNo')
                 + getWherePart( 'LWMission.MissionProp10','QManageCom' )
                 + " and ((select UWPopedom from LDUser where usercode = '"+mOperate+"') > (select AppGrade from LCCUWMaster where trim(ContNo) = LWMission.MissionProp2 ))"
                 + " and LWMission.MissionProp10 like '" + comcode + "%%'";  //集中权限管理体现
      } else //本级+下级保单
      {
        strSQL = "select LWMission.MissionProp1,LWMission.MissionProp2,LWMission.MissionProp7,LWMission.MissionProp5,LWMission.MissionProp8,LWMission.MissionID ,LWMission.SubMissionID from LWMission where "+k+"="+k
                 //+ " and VERIFYOPERATEPOPEDOM(LCPol.Riskcode,LCPol.Managecom,'"+comcode+"','Pe')=1"
                 + " and LWMission.ProcessID = '0000000003' " //承保核保工作流
                 + " and LWMission.ActivityID = '0000001100' " //承保核保工作流中的待人工核保任务节点
                 //+ getWherePart( 'LWMission.DefaultOperator','QOperator' )
                 + getWherePart( 'LWMission.MissionProp1','QPrtNo' )
                 + getWherePart( 'LWMission.MissionProp9','QAppntName' )
                 + getWherePart( 'LWMission.MissionProp3','QProposalNo')
                 + getWherePart( 'LWMission.MissionProp10','QManageCom' )
                 + " and ((select UWPopedom from LDUser where usercode = '"+mOperate+"') >= (select AppGrade from LccUWMaster where trim(ContNo) = LWMission.MissionProp2 ))"
                 + " and LWMission.MissionProp10 like '" + comcode + "%%'";  //集中权限管理体现
      }

  //alert(strSQL);
  var tOperator = fm.QOperator.value;
  if(state == "1") {
      strSQL = strSQL + " and  LWMission.ActivityStatus = '1'"
               + " and ( LWMission.DefaultOperator is null or LWMission.DefaultOperator = '" + tOperator + "')";
    }
  if(state == "2") {
      strSQL = strSQL + " and  LWMission.ActivityStatus = '3'"
               + "and LWMission.DefaultOperator = '" + tOperator + "'" ;
    }
  if(state == "3") {
      strSQL = strSQL + " and  LWMission.ActivityStatus = '2'"
               + "and LWMission.DefaultOperator = '" + tOperator + "'" ;
    }


  if (withdrawFlag) {
      //strSQL = strSQL + " and LCPol.PrtNo in (select prtno from LCApplyRecallPol where ApplyType='0') ";
      strSQL = "select LCPol.ProposalNo,LCPol.PrtNo,LMRisk.RiskName,LCPol.AppntName,LCPol.InsuredName "
               + " from LCPol,LCUWMaster,LMRisk where 10=10 "
               + " and LCPol.AppFlag='0'  "
               + " and LCPol.UWFlag not in ('1','2','a','4','9')  "
               + " and LCPol.grppolno = '00000000000000000000' and LCPol.contno = '00000000000000000000' "
               + " and LCPol.ProposalNo = LCPol.MainPolNo  and LCPol.ProposalNo= LCUWMaster.ProposalNo  "
               + " and LCUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') "
               + " and LCPol.ManageCom like '" + manageCom + "%%'"
               + " and LMRisk.RiskCode = LCPol.RiskCode "
               + getWherePart( 'LCUWMaster.Operator','QOperator')
               + " and LCPol.PrtNo in (select prtno from LCApplyRecallPol where ApplyType='0')";

      withdrawFlag = false;
    }

  //execEasyQuery( strSQL );
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
      //alert(turnPage.strQueryResult);
      alert("没有没通过承保核保的个人单！");
      return "";
    }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strSQL;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  return true;
}

/*********************************************************************
 *  执行multline的点击事件
 *  描述:此处生成工作流的发体检、生调、核保通知书等结点
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryAddClick(parm1,parm2) {
  // 书写SQL语句
  k++;
  var uwgrade = fm.UWGradeStatus.value;
  var mOperate = fm.Operator.value;
  var state = fm.State.value;       //投保单所处状态
  var tProposalNo = "";
  var tPNo = "";
  var strSQL = "";

  if(fm.all(parm1).all('InpPolGridSel').value == '1' ) {
      //当前行第1列的值设为：选中
      tProposalNo = fm.all(parm1).all('PolGrid2').value;
      tPNo = fm.all(parm1).all('PolGrid1').value;
    }

  fm.all('ContNo').value = fm.all(parm1).all('PolGrid2').value;
  fm.all('MissionID').value = fm.all(parm1).all('PolGrid6').value;
  fm.all('PrtNo').value = fm.all(parm1).all('PolGrid1').value;
  fm.all('SubMissionID').value = fm.all(parm1).all('PolGrid7').value;
  var ContNo = fm.all('ContNo').value;

  //alert("contno11="+fm.all('ContNo').value);


  if(state == "1") {
      checkDouble(tProposalNo);
      //生成工作流新节点
    } else {
      makeWorkFlow();
    }

  // 初始化表格
  initPolAddGrid();
  initPolBox();
  DivLCContInfo.style.display = "none";
  divLCPol1.style.display= "none";
  divSearch.style.display= "none";
  divLCPol2.style.display= "";
  divMain.style.display = "";
  DivLCContButton.style.display="";
  DivLCCont.style.display="";
  DivLCAppntInd.style.display="";
  DivLCAppntIndButton.style.display="";
  if (uwgrade == "1") {
      strSQL = "select LCInsured.InsuredNo,LCInsured.Name,LCInsured.IDType,LCInsured.IDNo,case when LCInsured.INSUREDSTAT='1' then '已暂停' end  from LCInsured where "+k+"="+k
               + " and LCInsured.Contno = '" + ContNo + "'";
      //+ " union"
      //+ " select '  ',LCPol.PolNo,LCPol.PrtNo,LCPol.RiskCode,LCPol.AppntName,LCPol.InsuredName,' ',' ' from LCPol where "+k+"="+k
      //+ " and LCPol.prtno = (select prtno from lcpol where polno ='" + tProposalNo + "')"
      //+ " and LCPol.polno <> '" + tProposalNo + "'"
      //+ " order by 2 ";
    } else {
      strSQL = "select LCInsured.InsuredNo,LCInsured.Name,LCInsured.IDType,LCInsured.IDNo,case when LCInsured.INSUREDSTAT='1' then '已暂停' end from LCInsured where "+k+"="+k
               + " and LCInsured.Contno = '" + ContNo + "'";

    }
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
      //alert(turnPage.strQueryResult);
      alert("没有没通过承保核保的个人单！");
      window.location.href("./ManuUWInput.jsp");
      return "";
    }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolAddGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strSQL;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet   = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //alert("查询easyQueryAddClick"+arrDataSet.length);
  initFlag(  arrDataSet.length );

  //alert("contno21="+fm.all(parm1).all('PolGrid2').value);
  //alert("PrtNo="+fm.all('PrtNo').value);
  var arrSelected = new Array();

  strSQL = "select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,ReMark,PolApplyDate,CValiDate from lccont where contno='"+ContNo+"'";
  alert(ContNo);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

  fm.all('ProposalContNo').value = arrSelected[0][0];
  fm.all('PrtNo').value = arrSelected[0][1];
  fm.all('ManageCom').value = arrSelected[0][2];
  fm.all('ManageCom_ch').value = easyExecSql("select codename from ldcode where codetype='station' and code='"+arrSelected[0][2]+"'");
  fm.all('SaleChnl').value = arrSelected[0][3];
  fm.all('SaleChnl_ch').value = easyExecSql("select codename from ldcode where codetype='lcsalechnl' and code='"+arrSelected[0][3]+"'");
  fm.all('AgentCode').value = easyExecSql(" select GroupAgentCode from laagent where agentcode='"+arrSelected[0][4]+"'");
  fm.all('AgentGroup').value = arrSelected[0][5];
  fm.all('AgentCode1').value = arrSelected[0][6];
  fm.all('AgentCom').value = arrSelected[0][7];
  fm.all('AgentType').value = arrSelected[0][8];
  fm.all('ReMark').value = arrSelected[0][9];
  fm.all('PolApplyDate').value = arrSelected[0][10];
  fm.all('CValiDate').value = arrSelected[0][11];

  strSQL = "select AppntName,AppntSex,AppntBirthday,AppntNo,AddressNo,VIPValue,(case when BlacklistFlag='1' then '是' end) from lcappnt,LDPerson where contno='"+ContNo+"'"
           + "and LDPerson.CustomerNo = LCAppnt.AppntNo";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

  fm.all('AppntName').value = arrSelected[0][0];
  fm.all('AppntSex').value = arrSelected[0][1];
  fm.all('AppntBirthday').value = arrSelected[0][2];
  fm.all('AppntNo').value = arrSelected[0][3];
  fm.all('AddressNo').value = arrSelected[0][4];
  fm.all('VIPValue').value = arrSelected[0][5];
  fm.all('BlacklistFlag').value = arrSelected[0][6];

  return true;
}


function displayEasyResult( arrResult ) {
  var i, j, m, n;

  if( arrResult == null )
    alert( "没有找到相关的数据!" );
  else {
      // 初始化表格
      initPolGrid();

      arrGrid = arrResult;
      // 显示查询结果
      n = arrResult.length;
      for( i = 0; i < n; i++ ) {
          m = arrResult[i].length;
          for( j = 0; j < m; j++ ) {
              PolGrid.setRowColData( i, j+1, arrResult[i][j] );
            }// end of for
        } // end of for
      //alert("result:"+arrResult);
    } // end of if
}

//申请要人工核保保单
function checkDouble(tProposalNo) {
  fm.PolNoHide.value = tProposalNo;
  fm.action = "./ManuUWApply.jsp";
  fm.submit();
}

//选择要人工核保保单
function getPolGridCho() {
  //setproposalno();
  var cSelNo = PolAddGrid.getSelNo()-1;

  codeSql = "code";
  fm.UWPopedomCode.value = "";
  fm.action = "./ManuUWCho.jsp";
  fm.submit();
}

/*********************************************************************
 *  生成工作流后续结点
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function makeWorkFlow() {
  fm.action = "./ManuUWChk.jsp";
  fm.submit();
}

function checkBackPol(ProposalNo) {
  var strSql = "select * from LCApplyRecallPol where ProposalNo='" + ProposalNo + "' and InputState='0'";
  var arrResult = easyExecSql(strSql);
  //alert(arrResult);

  if (arrResult != null) {
      return false;
    }
  return true;
}

//  初始化核保师是否已查看了相应的信息标记数组
function initFlag(  tlength ) {
  // 标记核保师是否已查看了相应的信息
  showPolDetailFlag =  new Array() ;
  showAppFlag = new Array() ;
  showHealthFlag = new Array() ;
  QuestQueryFlag = new Array() ;

  var i=0;
  for( i = 0; i < tlength ; i++ ) {
      showPolDetailFlag[i] = 0;
      showAppFlag[i] = 0;
      showHealthFlag[i] = 0;
      QuestQueryFlag[i] = 0;
    }

}
//下核保结论
function manuchk() {

  flag = fm.all('UWState').value;
  var ProposalNo = fm.all('ProposalNo').value;
  var MainPolNo = fm.all('MainPolNoHide').value;

  if (trim(fm.UWState.value) == "") {
      alert("必须先录入核保结论！");
      return;
    }

  if (!checkBackPol(ProposalNo)) {
      if (!confirm("该投保单有撤单申请，继续下此结论吗？"))
        return;
    }

  if (trim(fm.UWState.value) == "6") {
      if(trim(fm.UWPopedomCode.value) !="")
        fm.UWOperater.value = fm.UWPopedomCode.value
                              else
                                fm.UWOperater.value = operator;
    }

  var tSelNo = PolAddGrid.getSelNo()-1;

  if(fm.State.value == "1"&&(fm.UWState.value == "1"||fm.UWState.value == "2"||fm.UWState.value =="4"||fm.UWState.value =="6"||fm.UWState.value =="9"||fm.UWState.value =="a")) {
      if( showPolDetailFlag[tSelNo] == 0 || showAppFlag[tSelNo] == 0 || showHealthFlag[tSelNo] == 0 || QuestQueryFlag[tSelNo] == 0 ) {
          var tInfo = "";
          if(showPolDetailFlag[tSelNo] == 0)
            tInfo = tInfo + " [投保单明细信息]";
          if(showAppFlag[tSelNo] == 0)
            tInfo = tInfo + " [既往投保信息]";
          if( PolAddGrid.getRowColData(tSelNo,1,PolAddGrid) == PolAddGrid.getRowColData(tSelNo ,2,PolAddGrid)) {
              if(showHealthFlag[tSelNo] == 0)
                tInfo = tInfo + " [体检资料录入]";
            }
          if(QuestQueryFlag[tSelNo] == 0)
            tInfo = tInfo + " [问题件查询]";
          if ( tInfo != "" ) {
              tInfo = "有重要信息:" + tInfo + " 未查看,是否要下该核保结论?";
              if(!window.confirm( tInfo ))
                return;
            }

        }
    }
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action = "./UWManuNormChk.jsp";
  fm.submit();

  if (flag != "b"&&ProposalNo == MainPolNo) {
      initInpBox();
      initPolBox();
      initPolGrid();
      initPolAddGrid();
    }
}

//function manuchk()
//{
//
//	flag = fm.all('UWState').value;
//	tUWIdea = fm.all('UWIdea').value;
//
//	//录入承保计划变更结论要区分主附险
//	if( flag == "b")
//	{
//		cProposalNo=fm.PolNoHide.value;
//	}
//	else
//	{
//		cProposalNo=fm.ProposalNo.value;
//	}
//
//	//alert("flag:"+flag);
//	if (cProposalNo == "")
//	{
//		alert("请先选择保单!");
//	}
//	else
//	{
//		if (flag == "0"||flag == "1"||flag == "4"||flag == "6"||flag == "9"||flag == "b")
//		{
//			showModalDialog("./UWManuNormMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
//		}
//
//		if (flag == "2") //延期
//		{
//			//showModalDialog("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
//			window.open("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
//		}
//
//		if (flag == "3") //条件承保
//		{
//			//showModalDialog("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
//			window.open("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
//		}
//		if (flag == "7") //问题件录入
//		{
//			QuestInput();
//		}
//
//		if (flag != "b")
//		{
//		initInpBox();
//    		initPolBox();
//		initPolGrid();
//		initPolAddGrid();
//		}
//	}
//}

//问题件录入
function QuestInput() {
  cContNo = fm.ContNo.value;  //保单号码
  var cMissionID = fm.MissionID.value;         
  var cSubMissionID = fm.SubMissionID.value;   
  var cPrtNo = fm.PrtNo.value;                 
  var strSql = "select * from LCcUWMaster where ContNo='" + cContNo + "' and ChangePolFlag ='1'";
  //alert(strSql);
  var arrResult = easyExecSql(strSql);
  //alert(arrResult);
  if (arrResult != null) {
      var tInfo = "承保计划变更未回复,确认要录入新的问题件?";
      if(!window.confirm( tInfo )) {
          return;
        }
    }
  window.open("./QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+cflag+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window1");

}
//再保审核
function ReInsure() {
  cContNo = fm.ContNo.value;  //保单号码
  var cMissionID = fm.MissionID.value;         
  var cSubMissionID = fm.SubMissionID.value;   
  var cPrtNo = fm.PrtNo.value;                 
  
  window.open("./ReInsureInputMain.jsp?ContNo="+cContNo+"&Flag="+cflag+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID,"window1");

}
//问题件回复
function QuestReply() {
  cProposalNo = fm.ProposalNo.value;  //保单号码

  //showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./QuestReplyMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");

  initInpBox();
  initPolBox();
  initPolGrid();

}

//问题件查询
function QuestQuery() {
  cContNo = fm.ContNo.value;  //保单号码


  if (cContNo != "") {
      var tSelNo = PolAddGrid.getSelNo()-1;
      QuestQueryFlag[tSelNo] = 1 ;
      window.open("./QuestQueryMain.jsp?ContNo="+cContNo+"&Flag="+cflag,"window2");
    } else {
      alert("请先选择保单!");
    }

}

//生存调查报告查询
function RReportQuery() {
      var Str="select 1 from LCRReport where proposalcontno='"+fm.ContNo.value+"' ";
  var ARR = easyExecSql(Str);
  if(!ARR) {
      alert("未录入契调件!");
      return false;
    }	
  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");


  var cContNo = fm.ContNo.value;
  var cMissionID = fm.MissionID.value;
  var cSubMissionID = fm.SubMissionID.value;
  var cPrtNo = fm.PrtNo.value;
  var cLoadFlag = fm.LoadFlag.value;

  if (cContNo!= ""  ) {
      //var tSelNo = PolAddGrid.getSelNo()-1;
      //var tNo = PolAddGrid.getRowColData(tSelNo,1);
      //showHealthFlag[tSelNo] = 1 ;
      window.open("./RReportQueryMain.jsp?ContNo="+cContNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&PrtNo="+cPrtNo+"&LoadFlag="+cLoadFlag,"window1");
      showInfo.close();

    } else {
      showInfo.close();
      alert("请先选择保单!");
    }
}

//保单撤销申请查询
function BackPolQuery() {
  cProposalNo = fm.ProposalNo.value;  //保单号码


  if (cProposalNo != "") {
      window.open("./BackPolQueryMain.jsp?ProposalNo1="+cProposalNo);
    } else {
      alert("请先选择保单!");
    }
}

//催办超时查询
function OutTimeQuery() {
  cProposalNo = fm.ProposalNo.value;  //保单号码


  if (cProposalNo != "") {
      window.open("./OutTimeQueryMain.jsp?ProposalNo1="+cProposalNo);
    } else {
      alert("请先选择保单!");
    }
}

//保险计划变更
function showChangePlan() {
  /*
   cProposalNo = fm.ProposalNo.value;  //保单号码
   cPrtNo = fm.PrtNoHide.value; //印刷号
   var cType = "ChangePlan";
   mSwitch.deleteVar( "PolNo" );
   mSwitch.addVar( "PolNo", "", cProposalNo );
   
   if (cProposalNo != ""&&cPrtNo != "")
   {	
   	 var strSql = "select * from LCIssuepol where ProposalNo='" + cProposalNo + "' and (( backobjtype in('1','4') and replyresult is null) or ( backobjtype in('2','3') and needprint = 'Y' and replyresult is null))";
      var arrResult = easyExecSql(strSql);
      if (arrResult != null) {
        var tInfo = "有未回复的问题件,确认要进行承保计划变更?";
        if(!window.confirm( tInfo ))
          	    return;
       }
     window.open("../app/ProposalEasyScan.jsp?LoadFlag=3&Type="+cType+"&prtNo="+cPrtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");  	
       
    }
   else
   {  	
   	alert("请先选择保单!");  	
   }	
   */

  var cPrtNo = fm.PrtNo.value;

  window.open("../app/ProposalEasyScan.jsp?ContNo="+ContNo+"&LoadFlag=25&prtNo="+cPrtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}

//保险计划变更结论录入显示
function showChangeResultView() {
  fm.ChangeIdea.value = "";
  //fm.PolNoHide.value = fm.ProposalNo.value;  //保单号码
  divUWResult.style.display= "none";
  divChangeResult.style.display= "";
}


//显示保险计划变更结论
function showChangeResult() {
  fm.uwState.value = "b";
  fm.UWIdea.value = fm.ChangeIdea.value;
  var cContNo = fm.ProposalContNo.value;

  cChangeResult = fm.UWIdea.value;

  if (cChangeResult == "") {
      alert("没有录入结论!");
    } else {
      var strSql = "select * from LCIssuepol where ContNo='" + cContNo + "' and (( backobjtype in('1','4') and replyresult is null) or ( backobjtype in('2','3') and needprint = 'Y' and replyresult is null))";
      var arrResult = easyExecSql(strSql);
      //alert(arrResult);
      if (arrResult != null) {
          var tInfo = "有未回复的问题件,确认要进行承保计划变更?";
          if(!window.confirm( tInfo )) {
              HideChangeResult()
              return;
            }
        }

      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      fm.action = "./UWManuNormChk.jsp";
      fm.submit(); //提交
    }
  divUWResult.style.display= "";
  divChangeResult.style.display= "none";
  fm.uwState.value = "";
  fm.UWIdea.value = "";
  fm.UWPopedomCode.value = "";
}

//延长催办时间
function DelayUrgeNotice() {
  var cContNo = fm.ContNo.value;
  window.open("./DelayUrgeNoticeInputMain.jsp?ContNo="+cContNo, "");
}

//隐藏保险计划变更结论
function HideChangeResult() {
  divUWResult.style.display= "";
  divChangeResult.style.display= "none";
  fm.uwState.value = "";
  fm.UWIdea.value = "";
  fm.UWPopedomCode.value = "";
}


function cancelchk() {
  fm.all('uwState').value = "";
  fm.all('UWPopedomCode').value = "";
  fm.all('UWIdea').value = "";
}

function setproposalno() {
  var count = PolGrid.getSelNo();
  fm.all('ProposalNo').value = PolGrid.getRowColData(count - 1,1,PolGrid);
}

//附件险按钮隐藏函数
function hideAddButton() {
  parent.fraInterface.divAddButton1.style.display= "none";
  parent.fraInterface.divAddButton2.style.display= "none";
  parent.fraInterface.divAddButton3.style.display= "none";
  parent.fraInterface.divAddButton4.style.display= "none";
  parent.fraInterface.divAddButton5.style.display= "none";
  parent.fraInterface.divAddButton6.style.display= "none";
  parent.fraInterface.divAddButton7.style.display= "none";
  parent.fraInterface.divAddButton8.style.display= "none";
  parent.fraInterface.divAddButton9.style.display= "none";
  parent.fraInterface.divAddButton10.style.display= "none";
  //parent.fraInterface.fm.UWState.CodeData = "0|^4|通融承保^9|正常承保";
  //parent.fraInterface.UWResult.innerHTML="核保结论<Input class=\"code\" name=UWState CodeData = \"0|^4|通融/条件承保^9|正常承保\" ondblclick= \"showCodeListEx('UWState1',[this,''],[0,1]);\" onkeyup=\"showCodeListKeyEx('UWState1',[this,''],[0,1]);\">";
}

//显示隐藏按钮
function showAddButton() {
  parent.fraInterface.divAddButton1.style.display= "";
  parent.fraInterface.divAddButton2.style.display= "";
  parent.fraInterface.divAddButton3.style.display= "";
  parent.fraInterface.divAddButton4.style.display= "";
  parent.fraInterface.divAddButton5.style.display= "";
  parent.fraInterface.divAddButton6.style.display= "";
  parent.fraInterface.divAddButton7.style.display= "";
  parent.fraInterface.divAddButton8.style.display= "";
  parent.fraInterface.divAddButton9.style.display= "";
  parent.fraInterface.divAddButton10.style.display= "";
  //parent.fraInterface.UWResult.innerHTML="核保结论<Input class=\"code\" name=UWState CodeData = \"0|^1|拒保^2|延期^4|通融/条件承保^6|待上级核保^9|正常承保^a|撤销投保单\" ondblclick= \"showCodeListEx('UWState',[this,''],[0,1]);\" onkeyup=\"showCodeListKeyEx('UWState',[this,''],[0,1]);\">";
  //parent.fraInterface.fm.UWState.CodeData = "0|^1|拒保^2|延期^4|通融承保^6|待上级核保^9|正常承保^a|撤销投保单";
}

function showNotePad() {

  if (ContNo!="" && PrtNo!="") {
      window.open("../uw/UWNotePadMain.jsp?ContNo="+ContNo+"&PrtNo="+PrtNo+"&OperatePos=3", "window1");
    }
}

function InitClick() {
  location.href  = "./LCManuUWAll.jsp?type=1";
}

//被保险人信息
function showInsuredInformation(){

  var tSel = ProposalGrid.getSelNo();
  var tContNo = fm.ContNo.value;
  var tMissionID = fm.MissionID.value;
  var tSubMissionID = fm.SubMissionID.value;
  var tPrtNo = fm.PrtNo.value;
  var tSendFlag = fm.SendFlag.value;                                                                                                                                                                                                                                                                                                                                                                                                                                                            	                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           
  var cPolNo=ProposalGrid.getRowColData(tSel - 1,10);
  var strSql = "select InsuredNo from lcpol where polno='"+cPolNo +"' ";
 // and InsuredNo in (select insuredno from lcpol where polno ='"+cPolNo+"'");
  var tInsuredNo = easyExecSql(strSql);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
   window.open("./InsuredUWInformationMain.jsp?ScanFlag=0&SelNo="+tSel+"&ContNo="+tContNo+"&PolNo="+cPolNo+"&InsuredNo="+tInsuredNo+"&MissionID="+tMissionID+"&SubMissionID="+tSubMissionID+"&PrtNo="+tPrtNo+"&SendFlag="+tSendFlag, "window1", "");                                                         

	}

/*********************************************************************
 *  进入被保人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsuredInfo(SequenceNo) {
  //将进入被保险人信息的方式由Multline变成按钮因此,不要从multline中取数
  //改为查询
  //var tSelNo = PolAddGrid.getSelNo();
  var tPrtNo = fm.PrtNo.value;
  var tContNo = fm.ContNo.value;
  var strSql = "select InsuredNo from lcinsured where prtno='"+tPrtNo
               +"' and SequenceNo='"+SequenceNo+"'";
  //var tInsuredNo = PolAddGrid.getRowColData(tSelNo - 1,1);
  var tInsuredNo = easyExecSql(strSql);
  if(!tInsuredNo) {
      alert("未查询到第"+SequenceNo+"被保险人!");
      return;
    }
  var tMissionID = fm.MissionID.value;
  var tSubMissionID = fm.SubMissionID.value;
  var tPrtNo = fm.PrtNo.value;
  var tSendFlag = fm.SendFlag.value;
  if(tLoadFlag == 1)
  {
  	tSendFlag=1;
  }
  window.open("./InsuredUWInfoMain.jsp?ScanFlag=0&ContNo="+tContNo+"&InsuredNo="+tInsuredNo+"&MissionID="+tMissionID+"&SubMissionID="+tSubMissionID+"&PrtNo="+tPrtNo+"&SendFlag="+tSendFlag, "", "");
}

/*********************************************************************
 *  进入系统后
 *  描述:此处生成工作流的发体检、生调、核保通知书等结点
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initUW() {
  // 书写SQL语句
  k++;
  var uwgrade = fm.UWGradeStatus.value;
  var mOperate = fm.Operator.value;
  var state = fm.State.value;       //投保单所处状态
  var tProposalNo = "";
  var tPNo = "";
  var strSQL = "";
  fm.all('ContNo').value = ContNo;
  fm.all('MissionID').value = MissionID;
  fm.all('PrtNo').value = PrtNo;
  
  fm.all('SubMissionID').value = SubMissionID;
  fm.ActivityID.value=tActivityID;
  fm.all('LoadFlag').value=tLoadFlag;
  
  //alert(tLoadFlag);
  // 初始化表格
  initPolAddGrid();
  initPolBox();
  initSendTrace();
  if(tLoadFlag != 1 && tLoadFlag != 2)
  {
  	makeWorkFlow();
	}
	if(tLoadFlag == 1)
	{
		//处理既往核保信息显示问题
		dealHistoryUwInfo();
	}


  DivLCContInfo.style.display = "none";
  divLCPol1.style.display= "none";
  divSearch.style.display= "none";
  divLCPol2.style.display= "none";
  divMain.style.display = "";
  DivLCContButton.style.display="";
  DivLCCont.style.display="";
  DivLCAppntInd.style.display="";
  DivLCAppntIndButton.style.display="";
  
    // 修改保单换号后，ContNo != PropoSalContNo 的问题。
    var tTmpPropoSalContNo = "";
    var tTmpSql = " select lcc.PropoSalContNo from LCCont lcc "
        + " where lcc.ContNo = '" + ContNo + "' "
        + " and lcc.PrtNo = '" + PrtNo + "' ";
    var arr = easyExecSql(tTmpSql);
    if(arr)
    {
        tTmpPropoSalContNo = arr[0][0];
    }
    else
    {
        tTmpPropoSalContNo = ContNo;
    }
    //alert("tTmpPropoSalContNo:" + tTmpPropoSalContNo + "ContNo:" + ContNo);
    //----------------------------------------
  

  if (uwgrade == "1") {
      strSQL = "select LCInsured.InsuredNo,LCInsured.Name,(select codename from ldcode where code = LCInsured.IDType and codetype = 'idtype'),LCInsured.IDNo,case when LCInsured.INSUREDSTAT='1' then '已暂停' end  from LCInsured where "+k+"="+k
               + " and LCInsured.prtno = '" + PrtNo + "'"
               + " order by LCInsured.SequenceNo "
               ;
      //+ " union"
      //+ " select '  ',LCPol.PolNo,LCPol.PrtNo,LCPol.RiskCode,LCPol.AppntName,LCPol.InsuredName,' ',' ' from LCPol where "+k+"="+k
      //+ " and LCPol.prtno = (select prtno from lcpol where polno ='" + tProposalNo + "')"
      //+ " and LCPol.polno <> '" + tProposalNo + "'"
      //+ " order by 2 ";
    } else {
      strSQL = "select LCInsured.InsuredNo,LCInsured.Name,(select codename from ldcode where code = LCInsured.IDType and codetype = 'idtype'),LCInsured.IDNo,case when LCInsured.INSUREDSTAT='1' then '已暂停' end from LCInsured where "+k+"="+k
               + " and LCInsured.proposalContno = '" + tTmpPropoSalContNo + "'"
               + " order by LCInsured.SequenceNo "
               ;

    }
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
      //alert(turnPage.strQueryResult);
      alert("没有未通过新契约核保的个人合同单！");
      return;
    }

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolAddGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strSQL;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet   = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //alert("查询easyQueryAddClick"+arrDataSet.length);
  initFlag(  arrDataSet.length );

  //alert("contno21="+fm.all(parm1).all('PolGrid2').value);
  //alert("PrtNo="+fm.all('PrtNo').value);
  var arrSelected = new Array();


  strSQL = "select ProposalContNo,PrtNo,ManageCom,SaleChnl,AgentCode,AgentGroup,AgentCode1,AgentCom,AgentType,ReMark,PolApplyDate,CValiDate from lccont where proposalcontno='" + tTmpPropoSalContNo + "'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

  fm.all('ProposalContNo').value = arrSelected[0][0];
  fm.all('PrtNo').value = arrSelected[0][1];
  fm.all('ManageCom').value = arrSelected[0][2];
  fm.all('ManageCom_ch').value = easyExecSql("select codename from ldcode where codetype='station' and code='"+arrSelected[0][2]+"'");
  fm.all('SaleChnl').value = arrSelected[0][3];
  fm.all('SaleChnl_ch').value = easyExecSql("select codename from ldcode where codetype='lcsalechnl' and code='"+arrSelected[0][3]+"'");
  fm.all('AgentCode').value = easyExecSql(" select GroupAgentCode from laagent where agentcode='"+arrSelected[0][4]+"'");
  fm.all('AgentCode_ch').value = easyExecSql("select name from laagent where agentcode='"+arrSelected[0][4]+"'");
  fm.all('AgentGroup').value = arrSelected[0][5];
 // fm.all('AgentGroup').value = easyExecSql("select agentgroup from LCCont where contno='"+arrSelected[0][0]+"'");
    fm.all('AgentGroup_ch').value = easyExecSql("select name from LABranchGroup where AgentGroup='"+arrSelected[0][5]+"'");
  fm.all('AgentCode1').value = arrSelected[0][6];
  fm.all('AgentCom').value = arrSelected[0][7];
  fm.all('AgentType').value = arrSelected[0][8];  
  fm.all('ReMark').value = arrSelected[0][9];
   fm.all('PolApplyDate').value = arrSelected[0][10];
    fm.all('CValiDate').value = arrSelected[0][11];
  strSQL = "select AppntName,AppntSex,AppntBirthday,AppntNo,AddressNo,VIPValue,(case when BlacklistFlag='1' then '是' end),LCAppnt.OccupationCode,lcappnt.NativePlace from lcappnt,LDPerson where prtno='"+PrtNo+"'"
           + "and LDPerson.CustomerNo = LCAppnt.AppntNo";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

  fm.all('AppntName').value = arrSelected[0][0];
  fm.all('AppntSex').value = arrSelected[0][1];
  fm.all('AppntSex_ch').value = easyExecSql("select codename from ldcode where codetype='sex' and code='"+arrSelected[0][1]+"'");
  fm.all('AppntBirthday').value = calAge(arrSelected[0][2]);
  fm.all('AppntNo').value = arrSelected[0][3];
  fm.all('AddressNo').value = arrSelected[0][4];
  fm.all('AddressNo_ch').value = easyExecSql("select PostalAddress from lcaddress where addressno='"+arrSelected[0][4]+"' and customerno='"+arrSelected[0][3]+"'");
  fm.all('VIPValue').value = arrSelected[0][5];
  fm.all('BlacklistFlag').value = arrSelected[0][6];

  fm.all('OccupationCode').value = arrSelected[0][7];
  fm.all('OccupationCode_ch').value = easyExecSql("select Occupationname from ldoccupation where OccupationCode='"+arrSelected[0][7]+"'");
  fm.all('NativePlace').value = arrSelected[0][8];
  fm.all('NativePlace_ch').value = easyExecSql("select codename from ldcode where codetype='nativeplace' and code='"+arrSelected[0][8]+"'");

  return true;
}

/*********************************************************************
 *  初始化是否上报核保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function initSendTrace() {
  
  //先根据险种自动判断整单核保结论，若已下整单核保结论，则覆盖自动判断的结论
  setUWState();
  
  var tSql = "";
	if(tActivityID=="0000001160"){
	//如果是上报下报自动带出是否自动发放通知书
	var strSql = "select missionprop13 from LWMission where MissionID='"+MissionID+"' and ActivityID='"+tActivityID+"' and SubMissionID='"+SubMissionID+"'";
	var arr = easyExecSql(strSql);
	if(!arr){
		alert("初始化错误，原因是查询上报下报节点失败！\n此问题为程序问题\n如果出现出现此提示将不会影响后续操作，但会导致丢失下级是否自动发放核保通知书的记录，请从新判断是否自动发放，请it报错！");
	}else{
		fm.NeedSendNotice.value = arr[0][0];
		//alert(arr[0][0]);
		if(fm.NeedSendNotice.value == "0"){
			fm.NeedSendNoticeChk.checked = false; 
			fm.NeedSendNoticeChk.disable = true;
		}
	}
  tSql = "select sendflag,uwflag,uwidea,sendtype from lcuwsendtrace where 1=1 "
         + " and otherno = '"+ContNo+"'"
         + " and othernotype = '1'"
         + " and uwno in (select max(uwno) from lcuwsendtrace where otherno = '"+ContNo+"')"
         ;
  turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  //判断是否查询成功
  if (turnPage.strQueryResult) {
      fm.all('SendFlag').value =arrSelected[0][0];
      fm.all('SendUWFlag').value =arrSelected[0][1];
      fm.all('SendUWIdea').value =arrSelected[0][2];
      fm.all('SendType').value =arrSelected[0][3];      
      DivLCSendTrance.style.display="";
    }

  if(fm.all('SendFlag').value == '0') {
      divUWSave.style.display = "";
      divUWAgree.style.display = "none";
      if(fm.all('SendType').value == '2'){
      fm.all('PSendUpFlag').value = '3';
      }
//          alert(fm.all('PSendUpFlag').value);
    } else if(fm.all('SendFlag').value == '1') {
      divUWAgree.style.display = "";
      divUWSave.style.display = "none";
      //divUWButton2.style.display = "none";
      tSql = "select sugpassflag,suguwidea from lccuwmaster where 1=1 "
             + " and proposalcontno = '"+ContNo+"'"
             ;
      turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
      arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

      fm.all('uwState').value = arrSelected[0][0];
      fm.all('UWIdea').value = arrSelected[0][1];
    }
  }
}

//判断整单核保结论
//自动判断的条件如下：
//正常承保：该单所有被保险人下所有险种核保结论为“正常承保”
//变更承保：两种以上结论并存，或者险种结论为加费、免责、降档、减额、变更缴费期等
//延期承保：该单所有被保险人下所有险种核保结论为“延期承保”
//谢绝承保：该单所有被保险人下所有险种核保结论为“谢绝承保”
//撤销申请：该单所有被保险人下所有险种核保结论为“撤消申请”
//概括为：若所有险种为同一核保结论，则整单取该核保结论，否则整单为变更承保
function setUWState()
{
  var sql = "  select 1 from LCUWMaster "
            + "where PassFlag = '5' "
            + "   and ContNo = '" + ContNo + "' ";
  var rs = easyExecSql(sql);
  if(rs == null || rs[0][0] == "" || rs[0][0] == "null")
  {
  	sql = "select UWFlag, CodeName('uwflag', UWFlag) from LCCont where UWFlag != '5' and ContNo = '" + ContNo + "'";
	  rs = easyExecSql(sql);
	  if(rs == null || rs[0][0] == "" || rs[0][0] == "null")
	  {
	    //所有险种都下发完核保结论才进行判断
	    sql = "  select distinct PassFlag, CodeName('uwflag', PassFlag) from LCUWMaster "
	          + "where PassFlag != '5' "
	          + "   and ContNo = '" + ContNo + "' ";
	    rs = easyExecSql(sql);
	    try{
	      if(rs != null && rs.length == 1)
	      {
	        fm.uwState.value = rs[0][0];
	        fm.uwStateName.value = rs[0][1];
	      }
	      else
	      {
	        fm.uwState.value = "4";
	        fm.uwStateName.value = "变更承保";
	      }
	    }
	    catch(ex)
	    {
	      //由于既往核保信息中没有fm.uwState和fm.uwStateName控件，此处不需要做处理
	    }
	  }
	  else
	  {
			fm.uwState.value = rs[0][0];
			fm.uwStateName.value = rs[0][1];
	  }
  }
}

//初始化核保轨迹信息
function initQueryUWidea(){
	//var strSql="SELECT A.OPERATOR,B.USERNAME,'',CHAR(A.MAKEDATE)||' '||CHAR(A.MAKETIME),CodeName('sendtype', A.SENDTYPE),"
	//					+ "CodeName('uwflag', uwflag),UWIDEA FROM LCUWSENDTRACE A,LDUSER B,LDCODE WHERE A.OPERATOR=B.USERCODE "
	//					+ "AND CODETYPE='uwflag' AND CODE=A.uwflag AND otherno='"+ContNo+"' and sendtype not in ('4','5') order by uwno";
    var strSql = ""
        + " select "
        + " lcuwst.Operator, "
        + " ldu.UserName, "
        + " '', "
        + " Char(lcuwst.MakeDate) || ' ' || Char(lcuwst.MakeTime), "
        + " CodeName('sendtype', lcuwst.SendType), "
        + " CodeName('uwflag', lcuwst.UWFlag), "
        + " lcuwst.UWIdea "
        + " from LCUWSendTrace lcuwst "
        + " inner join LDUser ldu on ldu.UserCode = lcuwst.Operator "
        + " where 1 = 1 "
        + " and lcuwst.OtherNo='" + ContNo + "' "
        + " and lcuwst.SendType not in ('5') "
        + " order by Char(lcuwst.MakeDate) || ' ' || Char(lcuwst.MakeTime) "
        + " with ur "
        ;
	var arr = easyExecSql(strSql);
	if(arr){
		displayMultiline(arr,UWInfoGrid);
	}
}	
/*********************************************************************
 *  告知整理
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ImpartToICD() {
  var cContNo = fm.ContNo.value;//PolAddGrid.getRowColData(PolAddGrid.getSelNo()-1, 1);
  var PrtNo = fm.PrtNo.value;

  if (cContNo!="" && PrtNo!="") {
      window.open("../uw/ImaprtToICDMain.jsp?ContNo="+cContNo+"&PrtNo="+PrtNo, "window1");
    } else {
      alert("请先选择保单!");
    }
}
//发问题件通知书
function SendIssue() {
  cContNo = fm.ContNo.value;

  if (cContNo != "") {
      //manuchk();
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      var cMissionID =fm.MissionID.value;

      var cPrtNo = fm.PrtNo.value;

      fm.PrtNoHide.value = cPrtNo ;

      strsql = "select * from lcissuepol where contno = '" +cContNo+ "' and backobjtype = '3' and (state = '0' or state is null)";

      turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
      //判断是否查询成功
      if (!turnPage.strQueryResult) {
          showInfo.close();
          alert("不容许发放新的问题件通知单");
          fm.SubNoticeMissionID.value = "";
          return ;
        }

      strsql = "select submissionid from lwmission where missionprop2 = '" +cContNo+ "'and missionprop1 = '"+cPrtNo+"' and activityid = '0000001022'";
      turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
      turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

      fm.SubNoticeMissionID.value = turnPage.arrDataCacheSet[0][0];
      fm.action = 'UWManuSendIssueCHK.jsp';
      fm.submit();
    } else {
      alert("请先选择保单!");
    }
}
//问题件回销
function QuestBack() {
   var Str="select 1 from lcissuepol where proposalcontno='"+fm.ContNo.value+"' ";
  var ARR = easyExecSql(Str);
  if(!ARR) {
      alert("未录入问题件!");
      return false;
    }	
  
  cContNo = fm.ContNo.value;  //保单号码
  var cLoadFlag = fm.LoadFlag.value;

  if (cContNo != "") {
      var tSelNo = PolAddGrid.getSelNo()-1;
      QuestQueryFlag[tSelNo] = 1 ;
      window.open("./UWManuQuestQMain.jsp?ContNo="+cContNo+"&Flag="+cflag+"&LoadFlag="+cLoadFlag,"window2");
    } else {
      alert("请先选择保单!");
    }
}
//发客户变更通知书
function PrintUWResult() {
  var cContNo = fm.ContNo.value;
  fm.uwState.value = "8";

  if (cContNo != "") {
      //manuchk();
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      var cMissionID =fm.MissionID.value;

      var cPrtNo = fm.PrtNo.value;

      fm.PrtNoHide.value = cPrtNo ;

      strsql = "select LWMission.SubMissionID from LWMission where 1=1"
               + " and LWMission.MissionProp1 = '" + cPrtNo +"'"
               + " and LWMission.MissionProp2 = '"+ cContNo + "'"
               + " and LWMission.ActivityID = '0000001105'"
               + " and LWMission.ProcessID = '0000000003'"
               + " and LWMission.MissionID = '" +cMissionID +"'";
      turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

      //判断是否查询成功
      if (!turnPage.strQueryResult) {
          showInfo.close();
          alert("不容许发放新的核保通知书,原因可能是:1.已发核保通知书,但未打印.2.未录入核保通知书内容.");
          fm.SubNoticeMissionID.value = "";
          return ;
        }
      turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
      fm.SubNoticeMissionID.value = turnPage.arrDataCacheSet[0][0];
      var tSubNoticeMissionID =   fm.SubNoticeMissionID.value ;
      if (cContNo != "" && cPrtNo != "" && cMissionID != "" && tSubNoticeMissionID != "" ) {
          fm.UWResult.value="PRINT||RESULT"
                            fm.action = "./UWManuSendNoticeChk.jsp";
          fm.submit();
        } else {
          showInfo.close();
          alert("请先选择保单!");
        }
    } else {
      alert("请先选择保单!");
    }
}
//处理险种信息页面
function queryProposalInfo() {
  
  var row = ProposalGrid.getSelNo();
  
	var strSql = "select a.Name,char(rownumber() over()) as riskNo,b.riskcode,b.mult,b.amnt,(select sum(prem) from lcprem where substr(lcprem.payplancode,1,6)<>'000000' and lcprem.polno=b.polno),"
							+"(select sum(prem) from lcprem where substr(lcprem.payplancode,1,6)='000000' and lcprem.polno=b.polno)"
							+",d.codename,(select uwidea from lcuwmaster where lcuwmaster.polno=b.polno),b.polno, b.SupplementaryPrem "
							+"from lcinsured a,lcpol b,LCUWMaster c,ldcode d where a.prtno=b.prtno "
							+" and a.insuredno=b.insuredno "
							+" and b.polno=c.polno "
							+" and d.codetype='uwflag' "
							+" and d.code=c.passflag "
							+" and b.prtno='"+PrtNo+"' order by a.SequenceNo,b.RiskSeqNo ";
	var arr = easyExecSql(strSql);
	for(var i=0;i<arr.length;i++)
	{
		for(var m=0;m<arr[i].length;m++)
		{
			if(arr[i][m]=='null')
				arr[i][m]="";
		}
	}
	displayMultiline(arr,ProposalGrid,turnPage);
	
	if(row != null && row > 0)
	{
	  ProposalGrid.radioBoxSel(row);
	}
	setUWState();
}

//既往核保信息显示
function dealHistoryUwInfo()
{
	var strSql = "select passflag,UWIdea from LCcUWMaster where proposalContNo ='"+ContNo+"'";
	var arr = easyExecSql(strSql);
	fm.uwState.value=arr[0][0];
	fm.UWIdea.value=arr[0][1];
	showAllCodeName();
	divUWSave.style.display='none';
	displayInfoDiv.style.display='none';
	//fm.ImpartTrim.style.display='none';
	//displayAutoUw.style.display='';
	divAddButton13.style.display='none';
}
function showChangePlanScan()
{
	var cLoadFlag = fm.LoadFlag.value;
  var ContNo = fm.ContNo.value;
  if (ContNo == "")
    return;
  var strSql = "select prtseq from loprtmanager where otherno='"+ContNo+"' and code='05'";
  var arr = easyExecSql(strSql);
  if(arr){
  	for(var i=0 ; i< arr.length ; i++){
		  window.open("./LCUWResultScanQuery.jsp?prtNo="+arr[i][0]+"&ContNo="+ContNo+"&LoadFlag="+cLoadFlag+"&BussNoType=15&BussType=TB&SubType=TB21", "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
		}
	}else{
		alert("没有查询到核保扫描件");	
	}
}
function dealUWState(state){
	if(fm.MissionID.value == "" || fm.MissionID.value == "null" || fm.MissionID.value == null){
		alert("查询工作流信息失败！");
		return false;
	}
	if(fm.ActivityID.value == "" || fm.ActivityID.value == "null" || fm.ActivityID.value == null){
		alert("查询工作流节点信息失败！");
		return false;
	}
	if(fm.SubMissionID.value == "" || fm.SubMissionID.value == "null" || fm.SubMissionID.value == null){
		alert("查询工作流子节点信息失败！");
		return false;
	}
	fm.fmtransact.value = "UPDATE||MAIN"
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./UWIndStateSave.jsp?ActivityStatus="+state;
	fm.submit();
}

//如果投保件处于待回复状态，须先置为待处理才能下发整单核保结论。
function checkState()
{
  var checkStateSql = "select 1 from lwmission t,lccont a "
			+ "where t.activityid in ('0000001160', '0000001100') and t.activitystatus = '2' and t.activitystatus = '2'"
			+ "and t.missionprop2 = a.proposalcontno and t.missionprop1 = '" + PrtNo + "'";
  var result = easyExecSql(checkStateSql);
  if(result)
  {
    alert("投保件处于待回复状态，须先置为'待处理'才能下发整单核保结论。");
    return false;
  }
  else
  {
	  return true;
  }
}

//如果有体检件、问题件、契约调查未回销时，需核保人确认是否整单通过
function checkCard()
{
	//体检件回销校验
	var checkSql = "select MissionProp1 from LWMission where processid ='0000000003' and MissionProp2='"
	              + fm.ContNo.value + "' and activityid in ('0000001106','0000001111')";
	var result = easyExecSql(checkSql);
	if(result)
	{
		if(!confirm("有体检件未回销，确认整单通过？"))
		{
		  return false;
		}
	}
	//问题件回销校验
	var issueSql = "select MissionProp1 from LWMission where processid ='0000000003' and MissionProp2='"
	              + fm.ContNo.value + "' and activityid in ('0000001023','0000001025')";
	var result = easyExecSql(issueSql);
	if(result)
	{
		if(!confirm("有问题件未回销，确认整单通过？"))
		{
		  return false;
		}
	}
	//契调通知书回销校验
	var reportSql = "select MissionProp1 from LWMission where processid ='0000000003' and MissionProp2='"
	              + fm.ContNo.value + "' and activityid in ('0000001108','0000001113')";
	var result = easyExecSql(reportSql);
	if(result)
	{
		if(!confirm("有契调通知书未回销，确认整单通过？"))
		{
			  return false;
		}
	}
	return true;
}

// 豁免险保额校验
function checkExemption(){
	var sql = "select 1 from lcpol where prtno = '" + fm.PrtNo.value 
					+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
	var result= easyExecSql(sql);
	if(result && result[0][0] == "1"){
		var sql1 = "select 1 from lcpol a where prtno = '" + fm.PrtNo.value + "' and exists (select 1 from lmriskapp where riskcode=a.riskcode and kindcode='S') ";
        var sql2 = "select 1 from ldcode1 b where codetype='checkexemptionrisk' and exists ("+sql1+" and a.riskcode=b.code1 )  ";
        var sql3 = "select 1 from ldcode1 c where codetype='checkappendrisk' and exists ("+sql2+" and c.code1=b.code ) ";
		var sql = "select sum(prem) from lcpol lc where prtno = '" + fm.PrtNo.value + "' and ( exists ("+sql2+" and lc.riskcode= b.code) or exists ("+sql3+" and lc.riskcode=c.code) ) ";
		result= easyExecSql(sql);
		if(result && !isNull(result[0][0]) && result[0][0] != "null"){
			var sumPrem = result[0][0];
			sql = "select amnt,appntno,insuredno from lcpol where prtno = '" + fm.PrtNo.value 
					+ "' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and kindcode='S')";
			result= easyExecSql(sql);
			if(sumPrem != result[0][0]){
				alert("豁免险保额异常，请核查！");
				return false;
			}
			if(result[0][1] != result[0][2]){
				alert("豁免险险种的被保人必须为投保人，请核查！");
				return false;
			}
		} else {
			alert("保单下尚未添加除豁免险外其他险种，或其他险种总保费为零，请核查！");
			return false;
		}
	}
	return true;
}

// 判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}
function customerHosp(){
   var sFeatures = "status:no;help:0;close:0;dialogWidth:500px;dialogHeight:500px;resizable=1";
   showInfo = window.open( "./CustomerHosptailMain.jsp?PrtNo="+PrtNo,"",sFeatures);
  
}

function initSY(){
    fm.all("TransApply").disabled=true;
    fm.all("ClaimQuery").disabled=true;
    fm.all("MagNumUW").disabled=true;
    var sql = "select lcc.transflag,codename('transflag',lcc.transflag),lcc.premmult,codename('premmult',lcc.premmult) "
            + " from lcpol lcp,lccontsub lcc,lmriskapp lm "
            + " where lcp.prtno='" + fm.PrtNo.value + "' "
            + " and lcp.riskcode=lm.riskcode and lm.TaxOptimal='Y' "
            + " and lcp.prtno=lcc.prtno  ";
    var rs = easyExecSql(sql);
    if(rs){
       fm.SYFlag.value="是";
       fm.TransFlag.value=rs[0][1];
       fm.PremMult.value=rs[0][3];
       if(rs[0][0]=='1'){
          fm.all("TransApply").disabled=false;
          fm.all("ClaimQuery").disabled=false;
       }
    }else{
       fm.SYFlag.value="否";
    }
    var sql1="select uuid from lccontsub" + " where prtno='" + fm.PrtNo.value + "' "
    var rs1 = easyExecSql(sql1);
    if(rs1){
    if(rs1[0][0]!=null&&rs1[0][0]!=""&&rs1[0][0]!="null"){
        fm.all("MagNumUW").disabled=false;
  }
    }
}

function transApply(){
   if(!checkApply()){
      return false;
   }
   window.open("../app/TransContInfoMain.jsp?PrtNo="+fm.PrtNo.value+"&AddFlag=2","", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");  
}

function claimQuery(){

   var showStr = "正在获取既往理赔信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ; 
   showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
   var ttAction = fm.action; 
   fm.action="./ManuUWClaimQuery.jsp";
   fm.submit();
   fm.action=ttAction;  
}

function afterSubmit4( FlagStr, Content )
{	
	showInfo.close();
	window.focus();
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
	    fm.all("ClaimDetailID").style.display = "";
	    var claimInfo =  Content.split("#");
	    fm.TransAmnt.value=claimInfo[0];
	    fm.TransClaim.value=claimInfo[1];
	    fm.TransLifeClaim.value=claimInfo[2];
	}

}

function checkApply(){
   var strSql2="select uwflag from lcpol where contno='"+ContNo+"'";
   var arr2=easyExecSql(strSql2);
   if(arr2){
  	  for(var a=0;a<arr2.length;a++) {
      	if(arr2[a][0] == 5||arr2[a][0] ==""){
              alert("请确认是否对所有险种已下核保结论！");
              return false;
          }
      }     
   }
   strSql2="select 1 from lcpol where contno='"+ContNo+"' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and TaxOptimal='Y') and uwflag not in ('4','9')";
   arr2=easyExecSql(strSql2);
   if(arr2){
  	   alert("税优险种为非正常承保/变更承保，不能进行税优转移申请！");
       return false; 
   } 
   return true;
}

function checkTranSucc(){
   var strSql2="select uwflag from lcpol where contno='"+ContNo+"' and exists (select 1 from lmriskapp where riskcode=lcpol.riskcode and TaxOptimal='Y') "
              +" and exists (select 1 from lccontsub where prtno=lcpol.prtno and transflag='1' )";
   var arr2=easyExecSql(strSql2);
   strSql2 = "select SuccFlag from lstransinfo where prtno='"+fm.PrtNo.value+"'  ";
   var arr3=easyExecSql(strSql2);
   if(arr2&&arr3){
       var uwflag = arr2[0][0];
       var succflag = arr3[0][0];
       if(uwflag=='4'||uwflag=='9'){
          if(succflag=='99'||succflag==''||succflag==null){
             alert("该单承保为税优产品，且为转入保单，需进行税优转移申请！");
             return false;
          }
          if(succflag=='00'){
             alert("该单承保为税优产品，且为转入保单，税优转移申请未返回，不能进行核保确认！");
             return false;
          }
          if(succflag=='02'){
             alert("中保信返回无法转出，需进行撤单处理！");
             return false;
          } 
       }else{
          if(uwflag!='a'){
             alert("该单承保为税优产品，核保结论需为：变更承保/正常承保/撤销申请！");
             return false;
          }else{
             if(succflag=='99'||succflag==''||succflag==null){
          
             }else{
               if(succflag=='00'){
                 alert("该单承保为税优产品，且为转入保单，税优转移申请未返回，不能进行核保确认！");
                 return false;
               }
               if(succflag=='01'){
                 if(!confirm("税优转移申请中保信已返回原保单预计终止日期，是否继续撤单？")){
                    return false;
                 }
               }             
             }
          }
          
       }   
   }
    
   return true;

}
