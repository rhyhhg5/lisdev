<%
//程序名称：GrpUWEnsureInit.jsp
//程序功能：
//创建日期：2005-4-26
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	String tAppntNo = request.getParameter("pmAppntNo");
    String tGrpContNo = request.getParameter("pmGrpContNo");

    //添加页面控件的初始化。
%>
<SCRIPT Language="JavaScript">
//得到保单号码
var mGrpContNo = "<%=tGrpContNo%>";
//得到团体客户号码
var mAppntNo = "<%=tAppntNo%>";

//画面初始化
function initForm()
{
  try
  {
	initEnsureGrid();
	initEnsureDetailGrid();
	easyQueryI();
	easyQuery();
  }
  catch(re)
  {
    alert("程序名称：GrpUWEnsureAppInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initEnsureGrid()
{
    var iArray = new Array();
    
    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="保单合同号";                   //列名
        iArray[1][1]="120px";                   //列宽
        iArray[1][2]=100;                       //列最大值
        iArray[1][3]=3;                         //是否允许输入,1表示允许，0表示不允许
        
        iArray[2]=new Array();
        iArray[2][0]="集体投保单号码";                   //列名
        iArray[2][1]="120px";                   //列宽
        iArray[2][2]=100;                       //列最大值
        iArray[2][3]=3;                         //是否允许输入,1表示允许，0表示不允许
        
        iArray[3]=new Array();
        iArray[3][0]="保险印刷号";			    	//列名
        iArray[3][1]="120px";            		//列宽
        iArray[3][2]=100;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[4]=new Array();
        iArray[4][0]="参保人数";			    	//列名
        iArray[4][1]="80px";            		//列宽
        iArray[4][2]=100;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[5]=new Array();
        iArray[5][0]="投保保费";       				//列名
        iArray[5][1]="80px";            		//列宽
        iArray[5][2]=100;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[6]=new Array();
        iArray[6][0]="核保结论";			    		//列名
        iArray[6][1]="80px";            		//列宽
        iArray[6][2]=100;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[7]=new Array();
        iArray[7][0]="保单状态";       			//列名
        iArray[7][1]="80px";            		//列宽
        iArray[7][2]=100;            			//列最大值
        iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[8]=new Array();
        iArray[8][0]="投保时间";       			//列名
        iArray[8][1]="100px";            		//列宽
        iArray[8][2]=100;            			//列最大值
        iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        
        EnsureGrid = new MulLineEnter("fm", "EnsureGrid");
        
        EnsureGrid.mulLineCount = 0;
        EnsureGrid.displayTitle = 1;
        EnsureGrid.locked = 1;
        EnsureGrid.canSel = 1;
        EnsureGrid.hiddenPlus = 1;
        EnsureGrid.hiddenSubtraction = 1;
        EnsureGrid.loadMulLine(iArray);
        EnsureGrid.selBoxEventFuncName = "queryEnsureDetail";
    }
    catch(ex)
    {
        alert(ex);
    }
}


function initEnsureDetailGrid()
{                               
    var iArray = new Array();
    
    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=30;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="保障计划";         		//列名
        iArray[1][1]="60px";            		//列宽
        iArray[1][2]=100;            			//列最大值
        iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[2]=new Array();
        iArray[2][0]="人员类别";         		//列名
        iArray[2][1]="60px";            		//列宽
        iArray[2][2]=100;            			//列最大值
        iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[3]=new Array();
        iArray[3][0]="投保险种代码";         		//列名
        iArray[3][1]="80px";            		//列宽
        iArray[3][2]=100;            			//列最大值
        iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[4]=new Array();
        iArray[4][0]="投保险种名称";         		//列名
        iArray[4][1]="150px";            		//列宽
        iArray[4][2]=100;            			//列最大值
        iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[5]=new Array();
        iArray[5][0]="参保人数";         		//列名
        iArray[5][1]="60px";            		//列宽
        iArray[5][2]=200;            			//列最大值
        iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[6]=new Array();
        iArray[6][0]="男士占比";         		//列名
        iArray[6][1]="60px";            		//列宽
        iArray[6][2]=100;            			//列最大值
        iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
        
        iArray[7]=new Array();
        iArray[7][0]="女士占比";         		//列名
        iArray[7][1]="60px";            		//列宽
        iArray[7][2]=100;            			//列最大值
        iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许    
        
        iArray[8]=new Array();
        iArray[8][0]="退休比例";         		//列名
        iArray[8][1]="60px";            		//列宽
        iArray[8][2]=100;            			//列最大值
        iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        
        iArray[9]=new Array();
        iArray[9][0]="平均年龄";         		//列名
        iArray[9][1]="60px";            		//列宽
        iArray[9][2]=100;            			//列最大值
        iArray[9][3]=0;
        
        iArray[10]=new Array();
        iArray[10][0]="标准保费";         		//列名
        iArray[10][1]="60px";            		//列宽
        iArray[10][2]=100;            			//列最大值
        iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        
        iArray[11]=new Array();
        iArray[11][0]="录入保费";         		//列名
        iArray[11][1]="60px";            		//列宽
        iArray[11][2]=100;            			//列最大值
        iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        
        iArray[12]=new Array();
        iArray[12][0]="人均保费";         		//列名
        iArray[12][1]="60px";            		//列宽
        iArray[12][2]=100;            			//列最大值
        iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许    
        
        iArray[13]=new Array();
        iArray[13][0]="折扣比例";         		//列名
        iArray[13][1]="60px";            		//列宽
        iArray[13][2]=100;            			//列最大值
        iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
        
        EnsureDetailGrid = new MulLineEnter("fm", "EnsureDetailGrid");

        EnsureDetailGrid.mulLineCount = 0;
        EnsureDetailGrid.displayTitle = 1;
        EnsureDetailGrid.hiddenPlus = 1;
        EnsureDetailGrid.hiddenSubtraction = 1;
        EnsureDetailGrid.locked = 1;
        EnsureDetailGrid.canSel = 0;
        EnsureDetailGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
}


function GoBack()
{
	top.close();
}
</SCRIPT>