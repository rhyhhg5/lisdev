<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupUWInit.jsp
//程序功能：集体人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	String strOperator = globalInput.Operator;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
    // 保单查询条件
    fmQuery.all('GrpProposalContNo').value = '';
//    fmQuery.all('GrpMainProposalNo').value = GrpMainPolNo;
    fmQuery.all('PrtNoHide').value = PrtNo;
    fmQuery.all('Operator').value = '<%= strOperator %>';
    
  }
  catch(ex)
  {
    alert("在ContUWInit.jsp-->InitInp4Box函数中发生异常:初始化界面错误!"+ex.message);
  }      
}

// 保单基本信息显示框的初始化（单记录部分）
function initPolBox()
{ 

  try
  {                                   
  }
  catch(ex)
  {
    alert("在ContUWInit.jsp-->InitPo5lBox函数中发生异常:初始化界面错误!"+ex.message);
  }      
}

function initForm()
{
  try
  {
    //initInpBox();
    //initPolBox();
    //initPolGrid();
    initGrpGrid();
		initGrpPolFeeGrid();    
		initGrpAppntGrid();
		initFeeRateGrid();
		initHisUWInfoGrid();
		initUpSendReasonGrid();
		fmQuery.all('LoadFlag').value = LoadFlag;
  	fmQuery.all('Resource').value = Resource;
    if(Resource==1|Resource==2|Resource==3){
  	GrpQuest11.style.display="none";
  	GrpQuest111.style.display="none";
  	GrpQuest13.style.display="none";
  	divUWSave.style.display="none";
  	divUWAgree.style.display="none";
  	}
    querygrp();   
    
    // 查询中介手续费
    queryAgentChargeRate();
    // -------------------------
    
    showAllCodeName();   
    showFeeRate();   
    //校验标准保费是否存在,不存在显示N/
  	checkStandPrem();
//  	   alert(10);
 //   alert(LoadFlag);

  }
  catch(re)
  {
    alert("ContUWInit.jsp-->InitF6orm函数中发生异常:初始化界面错误!   "+re.message+";");
  }
}

// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="个人投保单号";         		//列名
      iArray[1][1]="160px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="投保人";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="险种编码";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="险种版本";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="被保人";         		//列名
      iArray[5][1]="120px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="管理机构";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      PolGrid = new MulLineEnter( "fmQuery" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 3;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 保单信息列表的初始化
function initGrpGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保障计划";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="人员类别";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="参保人数";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="参保比例";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="男女比例";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="退休比例";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;  
      //iArray[5][10]="condtion";
      //iArray[5][11]="0|^1|拒保^4|通融承保^9|正常承保^5|自核不通过^z|核保订正类别";               			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="平均年龄";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
      
      iArray[8]=new Array();
      iArray[8][0]="投保险种代码";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许    
      
      iArray[9]=new Array();
      iArray[9][0]="投保险种名称";         		//列名
      iArray[9][1]="100px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[10]=new Array();
      iArray[10][0]="责任项目";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[11]=new Array();
      iArray[11][0]="标准保费";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[12]=new Array();
      iArray[12][0]="录入保费";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[13]=new Array();
      iArray[13][0]="人均保费";         		//列名
      iArray[13][1]="60px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许    
      
      iArray[14]=new Array();
      iArray[14][0]="折扣比例";         		//列名
      iArray[14][1]="60px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
      iArray[15]=new Array();
      iArray[15][0]="标准保费";         		//列名
      iArray[15][1]="0px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许              
       
        
      GrpGrid = new MulLineEnter( "fmQuery" , "GrpGrid" ); 
      //这些属性必须在loadMulLine前
      GrpGrid.mulLineCount = 3;   
      GrpGrid.displayTitle = 1;
      GrpGrid.hiddenPlus = 1;
      GrpGrid.hiddenSubtraction = 1;
      GrpGrid.locked = 1;
      GrpGrid.canSel = 0;
      GrpGrid.selBoxEventFuncName = "getfee";          
      GrpGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
function initGrpPolFeeGrid()
{
      var iArray = new Array();
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="应交保费";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="实交保费";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="折扣率";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      GrpPolFeeGrid = new MulLineEnter( "fmQuery" , "GrpPolFeeGrid" ); 
      GrpPolFeeGrid.mulLineCount = 3;   
      GrpPolFeeGrid.displayTitle = 1;
      GrpPolFeeGrid.hiddenPlus = 1;
      GrpPolFeeGrid.hiddenSubtraction = 1;
      GrpPolFeeGrid.locked = 1;
      GrpPolFeeGrid.canSel = 0;
      GrpPolFeeGrid.loadMulLine(iArray);    
}

function initGrpAppntGrid()
{
   var iArray = new Array();
   iArray[0]=new Array();
   iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
   iArray[0][1]="30px";            		//列宽
   iArray[0][2]=30;            			//列最大值
   iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   
   iArray[1]=new Array();
   iArray[1][0]="投保团体";         		//列名
   iArray[1][1]="60px";            		//列宽
   iArray[1][2]=100;            			//列最大值
   iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   
   iArray[2]=new Array();
   iArray[2][0]="行业性质";         		//列名
   iArray[2][1]="60px";            		//列宽
   iArray[2][2]=100;            			//列最大值
   iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   iArray[2][4]="grpnature";
   iArray[2][5]="3";
   iArray[2][9]="行业性质|code:grpnature&NOTNULL";
   iArray[2][18]=250;
   iArray[2][19]= 0 ;
   
   iArray[3]=new Array();
   iArray[3][0]="行业类别";         		//列名
   iArray[3][1]="60px";            		//列宽
   iArray[3][2]=100;            			//列最大值
   iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[4]=new Array();
   iArray[4][0]="企业类型";         		//列名
   iArray[4][1]="60px";            		//列宽
   iArray[4][2]=100;            			//列最大值
   iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 businesstype
   iArray[4][4]="businesstype";
   iArray[4][5]="3";
   iArray[4][9]="企业类型|code:businesstype&NOTNULL";
   
   
   iArray[5]=new Array();
   iArray[5][0]="参保人数";         		//列名
   iArray[5][1]="60px";            		//列宽
   iArray[5][2]=100;            			//列最大值
   iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[6]=new Array();
   iArray[6][0]="被保险人";         		//列名
   iArray[6][1]="60px";            		//列宽
   iArray[6][2]=100;            			//列最大值
   iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[7]=new Array();
   iArray[7][0]="被保险人参保比例";         		//列名
   iArray[7][1]="60px";            		//列宽
   iArray[7][2]=100;            			//列最大值
   iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[8]=new Array();
   iArray[8][0]="被保险人退休比例";         		//列名
   iArray[8][1]="60px";            		//列宽
   iArray[8][2]=100;            			//列最大值
   iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[9]=new Array();
   iArray[9][0]="被保险人平均年龄";         		//列名
   iArray[9][1]="60px";            		//列宽
   iArray[9][2]=100;            			//列最大值
   iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[10]=new Array();
   iArray[10][0]="连带被保险人数";         		//列名
   iArray[10][1]="60px";            		//列宽
   iArray[10][2]=100;            			//列最大值
   iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[11]=new Array();
   iArray[11][0]="询价标志";         		//列名
   iArray[11][1]="60px";            		//列宽
   iArray[11][2]=100;            			//列最大值
   iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[12]=new Array();
   iArray[12][0]="既往保障标志";         		//列名
   iArray[12][1]="60px";            		//列宽
   iArray[12][2]=100;            			//列最大值
   iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[13]=new Array();
   iArray[13][0]="有无协议";         		//列名
   iArray[13][1]="60px";            		//列宽
   iArray[13][2]=100;            			//列最大值
   iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[14]=new Array();
   iArray[14][0]="VIP客户标志";         		//列名
   iArray[14][1]="60px";            		//列宽
   iArray[14][2]=100;            			//列最大值
   iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   iArray[15]=new Array();
   iArray[15][0]="黑名单标志";         		//列名
   iArray[15][1]="60px";            		//列宽
   iArray[15][2]=100;            			//列最大值
   iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
   
   
   GrpAppntGrid = new MulLineEnter( "fmQuery" , "GrpAppntGrid" ); 
   GrpAppntGrid.mulLineCount = 3;   
   GrpAppntGrid.displayTitle = 1;
   GrpAppntGrid.hiddenPlus = 1;
   GrpAppntGrid.hiddenSubtraction = 1;
   GrpAppntGrid.locked = 1;
   GrpAppntGrid.canSel = 0;
   GrpAppntGrid.loadMulLine(iArray);    
}
function initFeeRateGrid(){
	var iArray = new Array();
	iArray[0]=new Array();
	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	iArray[0][1]="30px";            		//列宽
	iArray[0][2]=30;            			//列最大值
	iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	iArray[1]=new Array();
	iArray[1][0]="险种代码";         		//列名
	iArray[1][1]="60px";            		//列宽
	iArray[1][2]=100;            			//列最大值
	iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	iArray[2]=new Array();
	iArray[2][0]="险种名称";         		//列名
	iArray[2][1]="120px";            		//列宽
	iArray[2][2]=100;            			//列最大值
	iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	iArray[3]=new Array();
	iArray[3][0]="折扣比例";         		//列名
	iArray[3][1]="60px";            		//列宽
	iArray[3][2]=100;            			//列最大值
	iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	
	
	iArray[4]=new Array();
	iArray[4][0]="总公司费用率";         		//列名
	iArray[4][1]="60px";            		//列宽
	iArray[4][2]=100;            			//列最大值
	iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	iArray[5]=new Array();
	iArray[5][0]="分公司费用率";         		//列名
	iArray[5][1]="60px";            		//列宽
	iArray[5][2]=100;            			//列最大值
	iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	FeeRateGrid = new MulLineEnter( "fmQuery" , "FeeRateGrid" ); 
	FeeRateGrid.mulLineCount = 0;   
	FeeRateGrid.displayTitle = 1;
	FeeRateGrid.hiddenPlus = 1;
	FeeRateGrid.hiddenSubtraction = 1;
	FeeRateGrid.locked = 1;
	FeeRateGrid.canSel = 0;
	FeeRateGrid.loadMulLine(iArray);    
}

// 保单信息列表的初始化
function initHisUWInfoGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="处理人代码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="处理人姓名";         		//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="处理开始时间";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="处理完成时间";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="上报标示";         		//列名
      iArray[5][1]="40px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许         
      
      iArray[6]=new Array();
      iArray[6][0]="核保结论";         		//列名
      iArray[6][1]="40px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
      iArray[7]=new Array();
      iArray[7][0]="核保人意见";         		//列名
      iArray[7][1]="230px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
       
        
      HisUWInfoGrid = new MulLineEnter( "fmQuery" , "HisUWInfoGrid" ); 
      //这些属性必须在loadMulLine前
      HisUWInfoGrid.mulLineCount = 0;   
      HisUWInfoGrid.displayTitle = 1;
      HisUWInfoGrid.hiddenPlus = 1;
      HisUWInfoGrid.hiddenSubtraction = 1;
      HisUWInfoGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


function initUpSendReasonGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="下级核保师";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="上报原因";         		//列名
      iArray[2][1]="260px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许          
       
        
      UpSendReasonGrid = new MulLineEnter( "fmQuery" , "UpSendReasonGrid" ); 
      //这些属性必须在loadMulLine前
      UpSendReasonGrid.mulLineCount = 0;   
      UpSendReasonGrid.displayTitle = 1;
      UpSendReasonGrid.hiddenPlus = 1;
      UpSendReasonGrid.hiddenSubtraction = 1;
      UpSendReasonGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>