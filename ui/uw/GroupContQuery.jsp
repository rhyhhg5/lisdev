<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：GroupContQuery.jsp.
//程序功能：团单下个人人工核保
//创建日期：2003-12-29 11:10:36
//创建人 ：sxy  zhangrong
//更新记录： 更新人  更新日期   更新原因/内容
%>
<html>
<%
  String tLoadFlag="";
  String tResource="";
  tLoadFlag = request.getParameter("LoadFlag");
  tResource= request.getParameter("Resource");
%>
<script>
	var LoadFlag= "<%=tLoadFlag%>";
	var Resource="<%=tResource%>";
	//alert(LoadFlag);
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="GroupContQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="GroupContQueryInit.jsp"%>
</head>
<body onload="initForm('<%=tGrpContNo%>');">
	<form method=post name=fm target="fraSubmit">
	  <!-- 团单下个人单查询条件 -->
		<div id="divSearch">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>请输入查询条件：</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<!--<TD class=title>合同号码 </TD>
					<TD class=input>--><Input class=common name=QContNo TABINDEX="-1"  MAXLENGTH="40" type="hidden">
					<TD class=title>被保人姓名</TD>
					<TD class=input><Input class="common" name=QInsuredName verify="被保人姓名|len<=20"></TD>
					<TD class=title style="display:'none'">管理机构 </TD>
					<TD class=input style="display:'none'">
						<Input class="code" name=QManageCom verify="管理机构|code:station" ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
						<!--Input class="code" name=GUWState CodeData= "0|^1|拒保^4|通融承保^6|待上级核保^9|正常承保^a|撤销投保单" ondblclick= "showCodeListEx('cond',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('cond',[this,''],[0,1]);"-->
					</TD>
				</TR>
			</table>
			<INPUT VALUE="查  询" class="cssButton" TYPE=button onclick="easyQueryClick('<%=tGrpContNo%>');">
			<INPUT type="hidden" name="Operator" value="">
			<INPUT type="hidden" name="blank" value="">
			<TD class=input><INPUT type=checkbox name=SearchFlag checked=true></TD>
			<TD class=title>仅查询未通过自动核保的险种保单</TD>
		</div>
		<Div id="divLCPol1" style="display: ''" >
			<table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);"></td>
					<td class=titleImg>保单查询结果</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span id="spanPolGrid"></span></td>
				</tr>
				<tr>
					<td align=center>
			<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
			<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
			<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
					</td>
				</tr>					
			</table>


			<P>
			<INPUT VALUE="返  回" class="cssButton" TYPE=button onclick="goBack();">
			<span id= spanmanuchk STYLE="display:''">
			<INPUT VALUE="批量核保通过" class="cssButton" TYPE=button onclick="manuchk(3);">
		</span>
			</P>
		</div>
		<!--个人单合同信息-->
		<DIV id=DivLCContButton STYLE="display:'none'">
			<table id="table1">
				<tr>
					<td><img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivLCCont);"></td>
					<td class="titleImg">个单合同信息</td>
				</tr>
			</table>
		</DIV>
		<div id="DivLCCont" STYLE="display:'none'">
			<table class="common" id="table2">
				<tr CLASS="common">
				<!--	<td CLASS="title">个单合同投保单号码</td>
					<td CLASS="input" COLSPAN="1">-->
						<input NAME="ProposalContNo" CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="40"  type="hidden">
					<td CLASS="title">印刷号码</td>
					<td CLASS="input" COLSPAN="1">
						<input NAME="PrtNo" VALUE CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="20">
					</td>
					<td CLASS="title">管理机构</td>
					<td CLASS="input" COLSPAN="1">
						<input NAME="ManageCom" MAXLENGTH="10" CLASS="readonly" readonly>
					</td>
					<td CLASS="title">销售渠道</td>
					<td CLASS="input" COLSPAN="1">
						<input NAME="SaleChnl" CLASS="readonly" readonly MAXLENGTH="2">
					</td>
				</tr>
				<tr CLASS="common">
					<td CLASS="title">业务员代码</td>
					<td CLASS="input" COLSPAN="1">
						<input NAME="GroupAgentCode" MAXLENGTH="10" CLASS="readonly" readonly>
						<input NAME="AgentCode" MAXLENGTH="10" CLASS="readonly" readonly type='hidden'>
					</td>
					<td CLASS="title">业务员姓名</td>
					<td CLASS="input" COLSPAN="1">
						<input NAME="AgentGroup" CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="12">
					</td>

				<tr CLASS="common" style="display:'none'">
					<td CLASS="title" style="display:'none'">联合代理人编码</td>
					<td CLASS="input" COLSPAN="1">
						<input NAME="AgentCode1" MAXLENGTH="10" CLASS="readonly" readonly>
					</td>
									</tr>
					<td CLASS="title" style="display:'none'">代理机构</td>
					<td CLASS="input" COLSPAN="1" style="display:'none'">
						<input NAME="AgentCom" CLASS="readonly" readonly>
					</td>
					<td CLASS="title" style="display:'none'">银行营业网点</td>
					<td CLASS="input" COLSPAN="1" style="display:'none'">
						<input NAME="AgentType" CLASS="readonly" readonly>
					</td>
				</tr>
				<tr CLASS="common" style="display:'none'">
					<td CLASS="title" >备注</td>
					<td CLASS="input" COLSPAN="5" style="display:'none'">
						<input NAME="Remark" CLASS="readonly" readonly MAXLENGTH="255">
					</td>
				</tr>
			</table>
		</div>
		<DIV id=DivLCAppntIndButton STYLE="display:'none'">
			<!-- 个人单投保人信息部分 -->
			<table>
				<tr>
					<td><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivLCAppntInd);"></td>
					<td class=titleImg>投保单位信息</td>
				</tr>
			</table>
		</DIV>
		<DIV id=DivLCAppntInd STYLE="display:'none'">
			<table class=common>
				<TR class=common>
					<TD class=title>投保单位</TD>
					<TD class=input>
						<Input CLASS="readonly" readonly name=GrpName>
					</TD>
					<TD class=title>电话</TD>
					<TD class=input>
						<Input name=Phone CLASS="readonly" readonly>
					</TD>
					<TD class=title>单位地址</TD>
					<TD class=input>
						<Input CLASS="readonly" readonly name="PostalAddress">
					</TD>
				</TR>
				<TR class=common>
					
					<TD class=title>邮政编码</TD>
					<TD class=input>
						<Input CLASS="readonly" readonly name="ZipCode">
					</TD>
				</TR>
			</table>
		</DIV>
		<!-- 个人单单查询结果部分（列表） -->
		<Div id="divMain" style="display: 'none'"></div>
		<!--附加险-->
		<Div id="divLCPol2" style="display: 'none'">
			<table>
				<tr>
					<td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" ></td>
					<td class=titleImg>险种查询结果</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span id="spanPolAddGrid" onclick="getPolGridCho();"></span></td>
				</tr>
				<tr>
					<td align=center>
						<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
						<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
						<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
						<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
					</td>
				</tr>	
			</table>
			<!-- 保全核保 -->
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>保单信息：</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<!--<TD class=title>投保单号码</TD>
					<TD class=input>--><Input class="readonly" readonly name=ProposalNo  type="hidden">
					<TD class=title>险种编码</TD>
					<TD class=input><Input class="readonly" readonly name=RiskCode></TD>
					<TD class=title>险种版本</TD>
					<TD class=input><Input class="readonly" readonly name=RiskVersion></TD>
				</TR>
				<TR class=common>
					<TD class=title>被保人客户号</TD>
					<TD class=input><Input class="readonly" readonly name=InsuredNo></TD>
					<TD class=title>被保人姓名</TD>
					<TD class=input><Input class="readonly" readonly name=InsuredName></TD>
					<TD class=title>被保人性别</TD>
					<TD class=input><Input class="readonly" readonly name=InsuredSex></TD>
				</TR>
				<TR class=common>
					<TD class=title>份数</TD>
					<TD class=input><Input class="readonly" readonly name=Mult></TD>
					<TD class=title>保费</TD>
					<TD class=input><Input class="readonly" readonly name=Prem></TD>
					<TD class=title>保额</TD>
					<TD class=input><Input class="readonly" readonly name=Amnt></TD>
				</TR>
				<TR class=common>
					<TD class=input>
						<INPUT type="hidden" class=Common name=UWGrade value="">
						<INPUT type="hidden" class=Common name=AppGrade value="">
						<INPUT type="hidden" class=Common name=PolNo value="">
						<INPUT type="hidden" class=Common name=ContNo value="">
						<INPUT type="hidden" class=Common name=GrpContNo value="">
					</TD>
					</TR>
				</table>
				<hr>
				<INPUT VALUE="个单保单明细信息" Class="cssButton" TYPE=button onclick="showPolDetail();">
				<INPUT VALUE="个单既往投保信息" Class="cssButton" TYPE=button onclick="showApp();">
				<INPUT VALUE="个单以往核保记录" Class="cssButton" TYPE=button onclick="showOldUWSub();">
				<INPUT VALUE="自动核保信息" Class="cssButton" TYPE=button onclick="showNewUWSub();">
				
				<hr>			
				<INPUT VALUE="个单加费承保录入" Class="cssButton" type=button onclick="showAdd();">
				<INPUT VALUE="个单特约承保录入" Class="cssButton" type=button onclick="showSpec();">	
			<hr>			
		</Div>
		<Div id = divLCPolButton style="display: 'none'">
		</Div>
		<!-- 个人单核保结论 -->
		<div id="divUWResult" style="display : 'none'">
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>个人单核保结论：</td>
				</tr>
			</table>
			<table class=common border=0 width=100%>
				<TR class=common>
					<TD class=title>
						个人单核保结论
						<Input class="code" name=UWState  CodeData="0|^1|拒保^4|通融承保^9|正常承保" ondblclick="showCodeListEx('condition',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('condition',[this,''],[0,1]);">
					</TD>
				</TR>
				<tr>
					<TD class=title>个人单核保意见</TD>
				</tr>
				<tr>
					<TD class=input><textarea name="UWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
				</tr>
			</table>
			<INPUT VALUE="个单核保确定" Class="cssButton" TYPE=button onclick="manuchk(1);">
			<INPUT TYPE="hidden" NAME="UWDelay" VALUE="">
			<INPUT TYPE="hidden" NAME="PolNoHide" VALUE="">
			<INPUT VALUE="返  回" class="cssButton" TYPE=button onclick="InitClick();">
		</Div>
		<div id="divContUWResult" style="display : 'none'">
			<hr></hr>
			<INPUT VALUE="个单保单明细信息" Class="cssButton" TYPE=button onclick="showPolDetail();">
			<INPUT VALUE="个单既往投保信息" Class="cssButton" TYPE=button onclick="showApp();">
			<INPUT VALUE="个单以往核保记录" Class="cssButton" TYPE=button onclick="showOldUWSub();">
			<INPUT VALUE="个单最终核保信息" Class="cssButton" TYPE=button onclick="showNewUWSub();">
			<span id="spantjqd"  style="display : ''">			
			<hr></hr>	
			 <input value="体检资料查询" class=cssButton type=button onclick="showHealthQ();" >         
	    
	         <INPUT VALUE="契调资料查询" class=cssButton TYPE=button onclick="RReportQuery();">	
			
	     
	      <hr></hr>	
	      	<input value="体检资料录入" class=cssButton type=button onclick="showHealth();">
		    <input value="契调资料录入" class=cssButton type=button onclick="showRReport('<%=tGrpContNo%>');">
		  </span>
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>个人合同单核保结论：</td>
				</tr>
			</table>
			<table class=common border=0 width=100%>
				<TR class=common>
					<TD class=title>
						个人合同单核保结论
						<Input class="code" name=ContUWState verify="个人合同单核保结论|code:condition" CodeData="0|^1|拒保^4|通融承保^9|正常承保" ondblclick="showCodeListEx('condition',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('condition',[this,''],[0,1]);">
					</TD>
				</TR>
				<tr>
					<TD class=title>个人合同单核保意见</TD>
				</tr>
				<tr>
					<TD class=input><textarea name="ContUWIdea" cols="100%" rows="5" witdh=100% class="common" verify="个人合同单核保意见|len<=255"></textarea></TD>
				</tr>
			</table>
						<span id= spanmanuchk1 STYLE="display:''">
			<INPUT VALUE="个单合同核保确定" Class="cssButton" TYPE=button onclick="manuchk(2);">
		</span>
			<INPUT VALUE="返  回" class="cssButton" TYPE=button onclick="InitClick();">
		</Div>
		<div id="divChangeResult" style="display: ''"></div>
	<INPUT type=hidden name="LoadFlag" value="">
	<INPUT type=hidden name="Resource" value="">
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>