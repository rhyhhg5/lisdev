//程序名称：UWHistoryErr.js

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var contState = "C";

function initContState(ContNo){
	var sql = "select 1 from lccont where contno='" 
			+ ContNo 
			+ "' union all "
			+ "select 2 from lbcont where contno='" 
			+ ContNo 
			+ "' and edorno not like 'xb%'";
	var rs = easyExecSql(sql);
	if(rs){
		if(rs[0][0] == "2") {
			// 退保保单
			contState="B";
		} else if(rs[0][0] == "1") {
			contState="C";
		}
	}
}


function easyQueryClick()
{
	// 初始化表格
	initUWErrGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var ContNo = fm.all('ContNo').value;
	
	var PolNo = fm.all('PolNo').value;
	strSQL = "select a.uwrulecode,a.uwno,a.insuredname,(select riskcode from lcpol where polno = a.polno ),a.uwerror,a.modifydate from LCUWError a where 1=1 "
			 + " and a.PolNo in (select distinct polno from l" + contState + "pol where contno ='" +ContNo+ "')"
			 + " and a.manuuwflag is null "	
			 + " union "
			 + " select c.uwrulecode,c.uwno,'公共自核规则','公共自核规则',c.uwerror,c.modifydate from LCCUWError c where 1=1"
			 + " and c.ContNo ='" + ContNo + "'"
			 + " and c.manuuwflag is null "
			 + " order by uwno";
    turnPage.queryModal(strSQL, UWErrGrid);
	return true;
}


function manuEasyQueryClick()
{
	// 初始化表格
	initManuUWErrGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var ContNo = fm.all('ContNo').value;
	
	var PolNo = fm.all('PolNo').value;
	strSQL = "select a.uwrulecode,a.uwno,a.insuredname,(select riskcode from lcpol where polno = a.polno),a.uwerror,a.modifydate from LCUWError a where 1=1 "
			 + " and a.PolNo in (select distinct polno from l" + contState + "pol where contno ='" +ContNo+ "')"
			 + " and a.manuuwflag = 'Y'"			
			 + " union "
			 + "select c.uwrulecode,c.uwno,'公共自核规则','公共自核规则',c.uwerror,c.modifydate from LCCUWError c where 1=1"
			 + " and c.ContNo ='" + ContNo + "'"
			 + " and c.manuuwflag = 'Y'"
			 + " order by uwno"
			 ;
	turnPage1.queryModal(strSQL, ManuUWErrGrid);
	return true;
}