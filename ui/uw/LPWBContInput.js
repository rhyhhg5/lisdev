
var turnPage = new turnPageClass();

//查询
function easyQueryClick(){
	//初始化MulLine
	initLPWBContGrid();
	strSQL1 = "select * from " +
					"  (   " +
					" select lc.grpcontno,lc.managecom ,lc.grpname,lc.signdate,lc.stateflag, " +
					" (case when ld.codetype is not null then '已配置' else '未配置' end) as cs " +
					" from lcgrpcont lc left join ldcode ld on ld.code=lc.grpcontno  and ld.codetype='LPWBCont' " +
					"  )t  " +
					"  where 1=1  "
					+ getWherePart('t.grpcontno','PolicyNo')
					+ getWherePart('t.managecom','manageCom','like')//加like模糊查询
					+ getWherePart('t.cs','isDeployName');
	
    turnPage.pageLineNum = 10;//查询后翻页每页显示的条数
    turnPage.strQueryResult  = easyQueryVer3(strSQL1);//sql执行查询出来的结果，赋值给翻页查询结果（easyQueryVer3没有查询到结果返回false）
    if (!turnPage.strQueryResult) 
    {
        alert("未查询到数据！");
        return false;
    }
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，LPWBContGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = LPWBContGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL1;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 10);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//添加
function insertCode(){
	//判断是否重复添加
    var tResult = easyExecSql("select 1 from ldcode  where codetype = 'LPWBCont' and code = '" + fm.PolicyNo1.value +"'");
    if(tResult)
    {
        alert('保单号：'+fm.PolicyNo1.value+' 已配置');
        return false;
    }
    
    //客户交互提示窗
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.methodName.value = "save";
    fm.submit();
}

//选中MulLine（单元格）执行给input框赋值的操作
function onclkSelBox(){
    var tSel = LPWBContGrid.getSelNo();//判断改行的单选框被选中，返回被选中的行号
    fm.PolicyNo1.value = LPWBContGrid.getRowColData(tSel-1,1);//MulLine对象的getRowColData(row,col)方法,取到单元格中的值
    fm.ManageCom1.value = LPWBContGrid.getRowColData(tSel-1,2);
    
    var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all("ManageCom1").value+"'");
    fm.ManageComName1.value = arrResult;
    divUpdate.style.display = "";
}

function onclkSelBoxclear(){
    var tSel = LPWBContGrid.getSelNo();//判断改行的单选框被选中，返回被选中的行号
    fm.PolicyNo1.value = "";
    fm.ManageCom1.value = "";
    fm.ManageComName1.value = "";
    divUpdate.style.display = "";
}

//删除
function deleteCode(){
	strSQL1 ="select tab.cs from "+
					"  ( "+
					"  select * from (    "+
					"  select  lc.grpcontno,lc.managecom ,lc.grpname,lc.signdate,lc.stateflag,  "+
					"   (case when ld.codetype is not null then '已配置' else '未配置' end) as cs  "+
					"   from lcgrpcont lc left join ldcode ld on ld.code=lc.grpcontno  and ld.codetype='LPWBCont'  "+
					"    )t   "+
					"    where t.cs='未配置' and  t.grpcontno='"+fm.all("PolicyNo1").value+"'  "+
					"  )tab ";
	
	var qResult = easyExecSql(strSQL1);
    if (qResult=="未配置") 
    {
        alert("该保单未配置，不需要进行删除！");
        return false;
    }
	
	//交互提示窗
	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
    fm.all("methodName").value="delete";
	fm.submit(); 
}


//提交后（控制层执行完成返回结果后的操作）——save页面通过js调用这个函数
function afterSubmit(FlagStr, content, transact){
	showInfo.close();//关闭Dialog
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	easyQueryClick();
}
