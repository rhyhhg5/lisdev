var arrDataSet
var showInfo;
var turnPage = new turnPageClass();

function easyQueryClick2(){
   
    initPolGrid();
 
    strSQL1 = "select code,code1,CodeName,(select username from lduser where usercode=ldcode1.code) from ldcode1 where codetype='uwuserconf' "
            + getWherePart('code','UWOperator');
    
    turnPage.pageLineNum = 10;
    turnPage.strQueryResult  = easyQueryVer3(strSQL1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        alert("没有查询到核保师核保机构配置！");
        return false;
    }
    
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = PolGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL1;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 10);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

function afterSubmit(FlagStr, content, transact)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        if(transact == "delete"){
            divUpdate.style.display = "none";
            easyQueryClick2();
        }
	}
}


function insertCode(){
   
    if(!verifyInput2())
    {
		return false;
    }
   
    var tResult = easyExecSql("select 1 from ldcode1  where codetype = 'uwuserconf' and code = '" + fm.UWOperator1.value + "' and code1='"+ fm.ManageCom1.value +"'");
    if(tResult)
    {
        alert('核保师：'+fm.UWOperator1.value+' 已配置管理机构：'+fm.ManageCom1.value+' ，无需再次配置。');
        return false;
    }
    
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.methodName.value = "save";
	fm.submit();
}

function updateCode(){

    var tResult = easyExecSql("select 1 from ldcode1  where codetype = 'uwuserconf' and code = '" + fm.UWOperator.value + "' and code1='"+ fm.ManageCom.value +"'");
    if(tResult)
    {
        alert('核保师：'+fm.UWOperator.value+' 已配置管理机构：'+fm.ManageCom.value+'，无需修改。');
        return false;
    }
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fmUpdate.all("methodName").value="change";
    fmUpdate.submit(); 
    return true;
}

//选中grid执行的操作
function onclkSelBox(){
    var tSel = PolGrid.getSelNo();
    fmUpdate.upUWOperator.value = PolGrid.getRowColData(tSel-1,1);
    fmUpdate.upUWOperatorName.value = PolGrid.getRowColData(tSel-1,4);
    fmUpdate.upComCode.value = PolGrid.getRowColData(tSel-1,2);
    fmUpdate.upComCodeName.value = PolGrid.getRowColData(tSel-1,3);
    divUpdate.style.display = "";
}

function deleteCode(){
    
	var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fmUpdate.all("methodName").value="delete";
    fmUpdate.submit(); 
    return true;
}
