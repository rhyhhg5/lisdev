<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ManuUWInput.jsp.
//程序功能：新契约个人人工核保
//创建日期：2005-12-29 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<html>
<%
  //个人下个人
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput) session.getValue("GI");
%>
<head >
<%@page contentType="text/html;charset=GBK" %>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="QueryDiseaseInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="QueryDiseaseInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./QueryDisease.jsp">
    <!-- 保单查询条件 -->
    <div id="divSearch">
	    <table class= common border=0 width=100%>
	    	<tr>
					<td class= titleImg align= center>请输入查询条件：</td>
				</tr>
	    </table>
	    <table  class= common align=center>
	      	<TR  class= common>
	          <TD  class= title> 免责信息代码 </TD>
	          <TD  class= input> <Input class= common name=SpecNo > </TD>
	          <TD  class= title>  疾病名称  </TD>
	          <TD  class= input> <Input class=common name=DiseaseName> </TD>          
	        </TR>
	    </table>
	    <input value="查  询" class="cssButton" type="button" onclick="easyQueryClick();" id="btnQuery" name="btnQuery" /> 
	    <input value="新  增" class="cssButton" type="button" onclick="AddClick();" id="btnAdd" name="btnAdd" /> 
	    <input value="修  改" class="cssButton" type="button" onclick="UpdateClick();" id="btnEdit" name="btnEdit" /> 
	    <input value="删  除" class="cssButton" type="button" onclick="DeleteClick();" id="btnDel" name="btnDel" /> 
	    <input value="保  存" class="cssButton" type="button" onclick="addNewClick();" id="btnSave" name="btnSave" />
        <input value="取  消" class="cssButton" type="button" onclick="cancelAddNewClick();" id="btnCancelSave" name="btnCancelSave" />
    </div>


  <Div  id= "divQueryDisease" style= "display: ''" align = center>
  	<table class=common >
	    <tr  class=common>
	     	<td text-align: left colSpan=1 >
	  			<span id="spanQueryDiseaseGrid" >
	  			</span> 
	  		</td>
	  	</tr>
    </table>
  </div>
   <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
  </Div>

    <input type=hidden id="fmtransact" name="fmtransact">     
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
