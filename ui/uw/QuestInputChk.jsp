<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QuestInputChk.jsp
//程序功能：问题件录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LCContSet tLCContSet = new LCContSet();
	LCIssuePolSet tLCIssuePolSet = new LCIssuePolSet();

	String tContNo = request.getParameter("ContNo");
	String tFlag = request.getParameter("Flag");
	String tBackObj = request.getParameter("BackObj");
	String tQuest = request.getParameter("Quest");
	String tContent = request.getParameter("Content");
	String tQuestionField = request.getParameter("QuestionField");
	String tQuestionObjValue = request.getParameter("QuestionObjValue");
	String tQuestionFieldValue = request.getParameter("QuestionFieldValue");
	String tOldFieldValue = request.getParameter("OldFieldValue");
	String tQuestionObj = request.getParameter("QuestionObj");
	String tAction = request.getParameter("fmtransact");
	String tSerialNo = request.getParameter("SerialNo");
	String tOperate ="";
	
	//String tcont[] = request.getParameterValues("QuestGrid3");
	//String tChk[] = request.getParameterValues("InpQuestGridChk");
	LCIssuePolDB tLCIssuePolDB ;
	LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();
	System.out.println("dshfsdjf"+tSerialNo);
	System.out.println(tContNo);
	if(!StrTool.cTrim(tSerialNo).equals("")&&!StrTool.cTrim(tContNo).equals("")){
		tLCIssuePolDB = new LCIssuePolDB();
		tLCIssuePolDB.setProposalContNo(tContNo);
		tLCIssuePolDB.setSerialNo(tSerialNo);
		if(!tLCIssuePolDB.getInfo()){
			Content = "没有查询到当前问题件!";
			FlagStr = "Fail";
		}
		
		tLCIssuePolSchema = tLCIssuePolDB.getSchema();
		tBackObj = tLCIssuePolSchema.getBackObjType();
		tContent = tLCIssuePolSchema.getIssueCont();
		tQuest = tLCIssuePolSchema.getQuestionObj();
	}
	
	System.out.println("ContNo:"+tContNo);
	System.out.println("operatepos:"+tFlag);
	System.out.println("backobj:"+tBackObj);
	System.out.println("issuetype:"+tQuest);
	
	boolean flag = true;
	//int feeCount = tcont.length;

	if (tContNo.equals("") || tFlag.equals("") || tBackObj.equals("") || tContent.equals("") || tQuest.equals(""))
	{
		Content = "问题件数据录入不完全或传输失败!";
		FlagStr = "Fail";
		flag = false;
		System.out.println("失败");
	}
	else
	{
		System.out.println("begin");

		LCContSchema tLCContSchema = new LCContSchema();
 			
		tLCContSchema.setContNo( tContNo);
			    		
    	tLCContSet.add( tLCContSchema );
	    				
		//LCIssuePolSchema tLCIssuePolSchema = new LCIssuePolSchema();
		
		System.out.println("operatepos:"+tFlag);
		tLCIssuePolSchema.setIssueCont(tContent);
		tLCIssuePolSchema.setOperatePos(tFlag);
		tLCIssuePolSchema.setBackObjType(tBackObj);
		tLCIssuePolSchema.setQuestionObj(tQuestionObj);
		System.out.println("ertwertwetwertwert    "+tQuestionObj);
		tLCIssuePolSchema.setIssueType(tQuest);
		//tLCIssuePolSchema.setQuestionObj(tQuestionObjValue);
		tLCIssuePolSchema.setErrField(tQuestionField);
		tLCIssuePolSchema.setErrFieldName(tQuestionFieldValue);
		tLCIssuePolSchema.setErrContent(tOldFieldValue);
		
		tLCIssuePolSet.add(tLCIssuePolSchema);			    
	}
	
	System.out.println("flag:"+flag);
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		System.out.println("contno="+tContNo);
		tVData.add( tLCContSet );
		tVData.add( tLCIssuePolSet);
		tVData.add( tG );
		
		// 数据传输
		if(tAction.equals("INSERT||MAIN")){
			tOperate="INSERT";
		}
		if(tAction.equals("UPDATE||MAIN")){
			tOperate="UPDATE";
		}
		if(tAction.equals("DELETE||MAIN")){
			tOperate="DELETE";
		}
		QuestInputChkUI tQuestInputChkUI   = new QuestInputChkUI();
		if (tQuestInputChkUI.submitData(tVData,tOperate) == false)
		{
			int n = tQuestInputChkUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			Content = " 问题件录入失败，原因是: " + tQuestInputChkUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tQuestInputChkUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 问题件录入成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 操作失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
		}
	} 
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
