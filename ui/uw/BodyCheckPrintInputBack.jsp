<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var RePrint = "<%=request.getParameter("RePrint")%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="BodyCheckPrintInputBack.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BodyCheckPrintInitBack.jsp"%>
  <title>打印承保体检通知书回执 </title>   
</head>
<body  onload="initForm();" >
  <form action="./BodyCheckPrintQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>  印刷号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo verify="印刷号码|int&len=11"> </TD>
          <TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class="codeno" name=AgentCode verify="业务员代码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this,AgentCodeName],[0,1]);" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName],[0,1]);"><input class=codename name=AgentCodeName></TD>
          </TR> 
         <TR  class= common>
         	<TD  class= title>  生效日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=CvaliDate verify="生效日期|date"> </TD>
          <TD  class= title>  下发日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=MakeDate verify="下发日期|date">   </TD>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeno" name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName></TD>   
         </TR> 
         <TR  class= common>
         <TD  class= title>  预约状态 </TD>
         <TD  class= input>
          <Input class=codeno name=MarketType  CodeData="0|^1|已预约^2|未预约" ondblclick="return showCodeListEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('MarketType',[this,MarketTypeName],[0,1],null,null,null,1);"><input class=codename name=MarketTypeName readonly=true > 		     		
        	</td>
        </TR>     
    </table>
          <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./BodyCheckPrintSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont1);">
    		</td>
    		<td class= titleImg>
    			 承保体检回执信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCCont1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
			<INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<p>
      <INPUT VALUE="打印承保体检回执通知书(vts)" class= CssButton TYPE=button onclick="printPol();">.
      <!--INPUT VALUE="打印承保体检通知书(pdf)" class= CssButton TYPE=button onclick="printPol1();"-->  
  	</p>  	
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="ContNo" name="ContNo">
  	<input type=hidden id="PrtSeq" name="PrtSeq">
  	<input type=hidden id="MissionID" name="MissionID">
  	<input type=hidden id="SubMissionID" name="SubMissionID">
  	<input type=hidden id="PrtNo" name="PrtNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
