<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：UWModifySpecSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  
<%
  System.out.println("\n\n---UWModifySpecSave Start---");
  System.out.println("PolNo:" + request.getParameter("PolNo2"));

  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  
  LCSpecSchema tLCSpecSchema = new LCSpecSchema();
  tLCSpecSchema.setPolNo(request.getParameter("PolNo2"));
  tLCSpecSchema.setSpecContent(request.getParameter("Content"));
  LCSpecSet inLCSpecSet = new LCSpecSet();
  inLCSpecSet.add(tLCSpecSchema);

  VData inVData = new VData();
  inVData.add(inLCSpecSet);
  inVData.add(tGlobalInput);
  
  String Content = "";
  String FlagStr = "";
  
  UWModifySpecUI UWModifySpecUI1 = new UWModifySpecUI();

  if (!UWModifySpecUI1.submitData(inVData, "INSERT")) {
    VData rVData = UWModifySpecUI1.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
  	FlagStr = "Fail";
  }
  else {
    Content = " 处理成功! ";
  	FlagStr = "Succ";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---UWModifySpecSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.queryClick();
</script>
</html>
