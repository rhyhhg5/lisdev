<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var saleChnl = "";//销售渠道   zhangjianbao   2007-11-16
</script>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="RePrintRecheckProblem.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="RePrintRecheckProblemInit.jsp"%>
  <title>重打影像复查问题件</title>
</head>
<body  onload="initForm();" >
  <form action="./RePrintRecheckProblemSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入投保单查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
          <TD  class= title> 印刷号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo verify="印刷号码"> </TD>
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
         </TR> 
         <TR  class= common>
         	<TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>
          <TD  class= title> 下发起始日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=startMakeDate verify="下发日期|date">   </TD>
          <TD  class= title> 下发终止日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=endMakeDate verify="下发日期|date">   </TD>
         </TR> 
         <TR>
         	<TD  class= title>  生效日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=CvaliDate verify="生效日期|date"> </TD>
          <TD  class= title> 申请日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=PolApplyDate verify="申请日期|date">   </TD>
          <TD  class= title> 是否回销</TD>
          <TD  class= input><Input class="codeNo" name=SelType ondblclick="return showCodeList('takebackstate',[this,SelTypeName],[0,1]);" onkeyup="return showCodeListKey('takebackstate',[this,SelTypeName],[0,1]);"><input class=codename name=SelTypeName readonly=true ><input class=readonly name=SelTypeName2 value="" readonly=true >  </TD>
          </TR>
       <tr>
          <TD  class= title style="display:none;"> 是否已打</TD>
          <TD  class= input style="display:none;"><Input class="codeNo" name=StateFlag value="1" ondblclick="return showCodeList('printstate',[this,StateFlagName],[0,1]);" onkeyup="return showCodeListKey('printstate',[this,StateFlagName],[0,1]);"><input class=codename name=StateFlagName readonly=true value="已打印"><input class=readonly name=StateFlagName2 value="已打印" readonly=true ></TD>
          <TD  class= title> 销售渠道 </TD>
          <TD  class= input><Input class="codeNo" name=saleChannel ondblclick="return showCodeList('statuskind',[this,saleChannelName],[0,1]);" onkeyup="return showCodeListKey('statuskind',[this,saleChannelName],[0,1]);"><input class=codename name=saleChannelName readonly=true ></TD>
          <TD  class= title> 营销机构 </TD>
          <TD  class= input><Input class="codeNo" name=branchAttr readonly ondblclick="beforeCodeSelect();return showCodeList('branchattrandagentgroup',[this,branchAttrName,agentGroup],[0,1,2],null,saleChnl,'saleChannel',1,200);"><input class=codename name=branchAttrName readonly=true ></TD>
      </tr>
    </table>
    <input type=hidden name="agentGroup">
  	<input type=hidden name="querySql">
  	<input type=hidden id="ContNo" name="ContNo">
    <input value="查  询" class= CssButton type=button onclick="easyQueryClick();">
    
  </form>
  <form action="./RePrintRecheckProblemSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();"> 	
  	</div>
  	<div id= "reprintissue" style= "display: ''" align = left>
  		<table>
  			<tr>
  				<td>
  	          <p>
                <INPUT VALUE="提交申请" class= CssButton TYPE=button onclick="printPolpdfNew();">
  	          </p>
  	      </td>
  	     </tr>
  	  </table>  	
    </div>
    <input type=hidden id="fmtransact" name="fmtransact">
  </form>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>