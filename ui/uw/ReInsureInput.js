	/*********************************************************************
 *  程序名称：QueryDiseaseInput.js
 *  程序功能：人工核保免得信息维护页面
 *  创建日期：2006-10-30 
 *  创建人  ：zhousp
 *  返回值：  无
 *  更新记录：  更新人    更新日期     更新原因/内容
 *********************************************************************
 */

var arrResult1 = new Array();
var arrResult2 = new Array();
var arrResult3 = new Array();
var arrResult4 = new Array();
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var temp = new Array();
var mOperate="";
var ImportPath;
var oldDataSet ; 
//alert(PrtNo);
window.onfocus=myonfocus;
/*********************************************************************
 *  查询免责信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function 	QueryRiskInfo(){
 var tContNo = fm.ContNo.value;
// alert(tContNo);
  mSQL ="select a.insuredname,a.riskcode,a.Mult,a.Amnt,a.Prem,(select case (select state from lcreinsurtask where uwno=(select max(uwno) from lcreinsurtask where polno=a.polno) and polno=a.polno) when '00' then '待回复' when '01' then '已回复' when '02' then '办结' end from dual),a.polno from lcpol a where a.contno ='" + tContNo +"'";
	turnPage1.queryModal(mSQL, RiskInfoGrid);
 	}

//查询再保审核任务 	
function QueryReInsureAudit()
{
  var mContNo = fm.ContNo.value;	
  
  var sql = "select b.insuredname,b.riskcode,a.uwno,a.uwoperator,a.uwidea,a.makedate, "
          + "   a.adjunctpath "
          + "from lcreinsuruwidea a ,lcpol b "
          + "where a.polno=b.polno "
          + "   and b.contno = '" + mContNo + "' "
          + "union "
          + "select b.insuredname,b.riskcode,a.uwno,a.operator,a.uwidea,a.makedate, "
          + "   a.adjunctpath "
          + "from lcreinsuridea a , lcpol b "
          + "where a.polno = b.polno "
          + "   and b.contno = '" + mContNo + "' "
          + "order by uwno";	
  turnPage1.queryModal(sql,ReInsureAuditGrid);
}
function AutoReInsure()
{
	  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.action = "./AutoReInsureChk.jsp";
    fm.submit();
} 	
function 	ReInsureAudit(){
  var tContNo = fm.ContNo.value;
  mSQL = "select a.insuredname,(select riskcode from lcpol where polno=a.polno),a.uwerror "
        + "from LCUWError a "
        + "where a.contno = '" + tContNo +"' and SugPassFlag = 'R' "
        + "   and UWNo = (select max(UWNo) from LCUWError "
        + "               where ContNo = a.ContNo and SugPassFlag = a.SugPassFlag) ";
	turnPage.queryModal(mSQL, ReInsureGrid);
}

//响应发送再保审核按钮事件
function SendUWReInsure()
{
  var row = RiskInfoGrid.getSelNo();
  if(row == null || row == 0)
  {
    alert("请选择险种");
    return false;
  }
  
  var	PolNo = RiskInfoGrid.getRowColData(row - 1, 7);
	var tsql ="select state from lcreinsurtask where uwno=(select max(uwno) from lcreinsurtask where polno='" +PolNo + "') and polno='" +PolNo + "'";
	var arr=easyExecSql(tsql);	
	if(tsql != null && arr == '00')
	{
    alert("该任务为待回复状态不能再发送再保审核!");
    return false;
	}
	
  ReInsureUpload();
}
			
//上载附件
function ReInsureUpload() 
{
  var i = 0;

  var tImportFile = fmImport.all('FileName').value;

  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmImport.action = "./UpLodeSave.jsp";
  fmImport.submit();
}

//下载
function DownLoad()
{
  var row = ReInsureAuditGrid.getSelNo();
  if(row == null || row == 0)
  {
    alert("请选择险种");
    return false;
  }
  
  var FilePath = ReInsureAuditGrid.getRowColData(row - 1, 7);  
  if (FilePath=="")
  {
    alert("没有附件,不能进行下载操作！")	
    return false;
  }   
  //alert(FilePath);
  //var showStr="正在下载数据……";
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./DownLoadSave.jsp?FilePath="+FilePath;
  fm.submit();
}

//再保审核任务办结
function ReInsureOver()
{
  var row = RiskInfoGrid.getSelNo();
	if(row == 0)
	{
	  alert("请选择险种");
	  return false;
	}
	
  var	State = RiskInfoGrid.getRowColData(row - 1, 6);
  
  if (State!='已回复')
  {
    alert("该险种处于"+State+"状态不能进行办结");
    return false;
  }
  
  if(!confirm("是否结束所选险种的再保审核？"))
  {
    return false;
  }
  
  fm.PolNo.value = RiskInfoGrid.getRowColDataByName(row - 1, "PolNo");
  fm.action = "ReInsureSave.jsp";
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
  fm.submit();
}
 		
function afterSubmit( FlagStr, content )
{ 

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initReInsureGrid();
    var tContNo = fm.ContNo.value;
// alert(tContNo);
    ReInsureAudit();
    QueryRiskInfo();
  }
}

function UpLodeReInsure(FilePath,FileName){
//alert(FilePath);
	  for (i=0; i<RiskInfoGrid.mulLineCount; i++)
  {
  ImportFile = fmImport.all('FileName').value;
    if (RiskInfoGrid.getSelNo(i))
    {
      checkFlag = RiskInfoGrid.getSelNo();
      break;
    }
  }
  var	PolNo = RiskInfoGrid.getRowColData(checkFlag - 1, 7);	
  //alert(PolNo);
 fm.action = "./SendUWReInsureChk.jsp?FilePath="+FilePath+"&FileName="+FileName+"&PolNo="+PolNo;
 fm.submit();
}

//办结反馈提示
function afterSubmitFinish( FlagStr, content )
{ 

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    QueryRiskInfo();
  }
}