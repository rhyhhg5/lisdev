//程序名称：UWErr.js
//程序功能：人工核保未过原因查询
//创建日期：2002-06-19 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var tPOLNO = "";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  //alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {   
  
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  easyQueryClick();
  manuEasyQueryClick();
  
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//既往投保信息
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  showModalDialog("./UWApp.jsp?ProposalNo="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  showInfo.close();
}           

//以往核保记录
function showOldUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  showModalDialog("./UWSub.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  //window.open("./UWSubMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}

//当前核保记录
function showNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  showModalDialog("./UWSub.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  showInfo.close();
}                      

//保单明细信息
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  window.open("../app/ProposalDisplay.jsp?PolNo="+cProposalNo,"window1");
  showInfo.close();
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//选择要人工核保保单
function queryUWErr()
{
	//showSubmitFrame(mDebug);
	fm.submit();
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initUWErrGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var ContNo = fm.all('ContNo').value;
	
	var PolNo = fm.all('PolNo').value;
	strSQL = "select a.uwrulecode,a.uwno,a.insuredname,(select riskcode from lcpol where polno = a.polno ),a.uwerror,a.modifydate from LCUWError a where 1=1 "
			 + " and a.PolNo in (select distinct polno from lcpol where contno ='" +ContNo+ "')"
			// + " and (a.uwno = (select max(b.uwno) from LCUWError b where b.ContNo = '" +ContNo + "' and b.PolNo = a.PolNo))"
			 + " and a.manuuwflag is null "	
			 + " union "
			 + " select c.uwrulecode,c.uwno,'公共自核规则','公共自核规则',c.uwerror,c.modifydate from LCCUWError c where 1=1"
			 + " and c.ContNo ='" + ContNo + "'"
			// + " and (c.uwno = (select max(d.uwno) from LCCUWError d where d.ContNo = '" + ContNo + "'))"
			 + " and c.manuuwflag is null "
			 + " order by uwno"
       ;
       turnPage.queryModal(strSQL, UWErrGrid);
		return true;
}


function manuEasyQueryClick()
{
	// 初始化表格
	initManuUWErrGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var ContNo = fm.all('ContNo').value;
	
	var PolNo = fm.all('PolNo').value;
	strSQL = "select a.uwrulecode,a.uwno,a.insuredname,(select riskcode from lcpol where polno = a.polno),a.uwerror,a.modifydate from LCUWError a where 1=1 "
			 + " and a.PolNo in (select distinct polno from lcpol where contno ='" +ContNo+ "')"
			// + " and (a.uwno = (select max(b.uwno) from LCUWError b where b.ContNo = '" +ContNo + "' and b.PolNo = a.PolNo))"
			 + " and a.manuuwflag = 'Y'"			
			 + " union "
			 + "select c.uwrulecode,c.uwno,'公共自核规则','公共自核规则',c.uwerror,c.modifydate from LCCUWError c where 1=1"
			 + " and c.ContNo ='" + ContNo + "'"
			// + " and (c.uwno = (select max(d.uwno) from LCCUWError d where d.ContNo = '" + ContNo + "'))"
			 + " and c.manuuwflag = 'Y'"
			 + " order by uwno"
			 ;
	turnPage1.queryModal(strSQL, ManuUWErrGrid);
		return true;
	

}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				UWErrGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function AutoUWError()
{
	  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.action = "./UWAutoUWErrorChk.jsp";
    fm.submit();
}
