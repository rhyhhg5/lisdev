<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html> 
<%
//程序名称：UWApp.jsp
//程序功能：既往投保信息查询
//创建日期：2002-06-19 11:10:36
//创建人  ： WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="UWHistoryApp.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UWHistoryAppInit.jsp"%>
  <title>既往投保信息 </title>
</head>
<body  onload="initForm('<%=tContNo%>','<%=tCustomerNo%>','<%=tPrtNo%>');" >
  <form method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 既往投保信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);">
    		</td>
    		<td class= titleImg>
    			 家庭成员
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCCont" style= "display: ''" align=center>
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanContGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="getLastPage();">		
	</div>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 既往投保信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCPol1" style= "display: ''" align=center>
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1 >
					<span id="spanPolGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="getFirstPage();changeChar();"> 
      <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="getPreviousPage();changeChar();"> 					
      <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="getNextPage();changeChar();"> 
      <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="getLastPage();changeChar();">
	</div>
	<!-- 没有进行人工核保 -->
<div id = "divNoUWResult" style = "display: 'none'">
<table class= common align=center>
	<tr class = common>
		<td class =input>
			<Input class= readonly readonly=true name="NoUWResult"  value="还未进行人工核保">
		</td>
	</tr>
</table>
</div>
<!-- 进行过人工核保 -->
<div id = "divUWResult" style = "display: 'none'">
    	  <table class= common border=0 width=100%>
    	  	<tr>
			<td class= titleImg align= center>险种核保结论：</td>
	  	</tr>
	  </table>
	   		 	   	
  	  <table  class= common align=center>
    	  				<tr class = common>
      		<TD class= title>
      		 	核保结论
      		 </TD>
      		 <TD class=input>
      		 <Input class="codeNo" name=uwstate><input class=codename name=uwstateName readonly=true >
	   	     </TD>
	   	   
	   	   <TD>
	   	   </TD>
	   	   <td id=divPropValue style="display:none" class=input >
	   	   	<Input class=common name=PropValue >
	   	   </td>
	   	   </div>
	   	   <td>
	   	   </td>
      	</TR>
      </table>
          <!--存放加费或免责的信息-->
<DIV id=DivAddFee STYLE="display:'none'">
    <table class= common border=0 width=100%>
    	 <tr>
	        <td class= titleImg align= center>加费信息 : </td>
	        <td class= titleImg align= center>免责信息 : </td>
	     </tr>
    </table>
    <table  class= common>
       <tr  class= common>
      	 <td text-align: left colSpan=1>
  					<span id="spanAddFeeGrid" >
  					</span>
  			 </td>
  			 <td text-align: left colSpan=1>
  					<span id="spanSpecGrid" >
  					</span>
  			 </td>
  		 </tr>
  	</table>
</DIV>
		<div id="divDelay" style="display: 'none'">
			<table class="common">
				<TR class="common">
					<td class= title align= left>延期天数 : </td>
	  			<td class= input align= left>
	  				<INPUT VALUE="" class=common name="inputDelayDays" >
	  			</td>
	  			<td class= title align= left>延期之 : </td>
	  			<td class= input align= left>
	  				<INPUT VALUE="" class=coolDatePicker name="inputDelayDate" >
	  			</td>
	  			<td></td><td></td><td></td>
				</TR>
			</table>
		</div>
		<div id="divCancelReason" style="display: 'none'">
			<table class="common">
				<TR class="common">
					<td class= title align= left>撤销原因 : </td>
	  			<td class= input align= left>
	  				<INPUT VALUE="" class=code name="CancelReason" verify="核保结论|code:CancelReason" CodeData="0^01|变更承保不同意^02|客户主动撤销^03|问题件逾期未回复^04|其他" ondblclick="showCodeListEx('CancelReason',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('CancelReason',[this],[0],'', '', '', true);">
	  			</td>
	  			<td></td><td></td><td></td>
				</TR>
			</table>
		</div>
         <table class=common>
		
		<tr>
		      	<TD height="24"  class= titleImg>
            		核保意见
          	</TD>
          	<td></td>
          	</tr>
		<tr>
      		<TD  class= input> <textarea name="UWIdea" verify="核保结论|len<=255" cols="100%" rows="5" witdh=120% class="common"></textarea></TD>
      		<td></td>
      		</tr>
	  </table>
</div>      
      <INPUT type= "hidden" name= "ProposalNoHide" value= "">
      <INPUT type= "hidden" name= "ProposalNoHide2" value= "">
      <INPUT type= "hidden" name= "InsureNoHide" value= "">
      <INPUT type= "hidden" name= "ContNoHide" value= "">
      <INPUT type= "hidden" name= "PrtNoHide" value= "">
      <hr>
      </hr>					
      <INPUT VALUE="关闭" class= cssButton TYPE=button onclick="parent.close();"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
