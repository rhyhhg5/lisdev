//程序名称：RReportQuery.js
//程序功能：生存调查报告查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var k = 0;
var turnPage = new turnPageClass();
var cflag = "";  //问题件操作位置 1.核保
var mCustomerNo ="";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showInfo.close();

  }
  else
  {
    var showStr="操作成功";
    showInfo.close();
    alert(showStr);
    if (fm.fmAction.value="DELETE||RECORD")
    {
    	easyQueryClick();
    }
    //parent.close();
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

// 查询按钮
function easyQueryClick()
{
  var strsql = "";
  //var tContNo = fm.ContNo.value;
  var tPrtSeq="";
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<LCRReportGrid.mulLineCount; i++) 
  {
    if (LCRReportGrid.getSelNo(i)) 
    { 
      checkFlag = LCRReportGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) 
  {
    tPrtSeq = LCRReportGrid.getRowColData(checkFlag - 1, 1);
    var	tContNo = LCRReportGrid.getRowColData(checkFlag - 1, 2);
    var	tcustomer = LCRReportGrid.getRowColData(checkFlag - 1, 11);
    fm.all('PrtSeq').value=tPrtSeq;
    fm.all('customer').value=tcustomer;
  }
  else 
  {
    alert("请先选择一条契调回销信息！"); 
    return;
  }
  
  if(tContNo != "")
  {
    strsql = "select b.RReportItemCode,a.name,b.RReportItemName,b.RRITEMCONTENT,b.RRITEMRESULT from lcrreport a,lcrreportitem b where a.contno = '"+tContNo+"' and a.PrtSeq='"+tPrtSeq+"' and a.contno=b.contno and a.PrtSeq=b.PrtSeq";
    //alert(strsql);

    //查询SQL，返回结果字符串
    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult)
    {
      alert("未查到该客户的契调项目明细报告信息！");
      initQuestGrid();
      return true;
    }

    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = QuestGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strsql;

    //设置查询起始位置
    turnPage.pageIndex       = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    divAddDelButton.style.display = "";
    //*****content
    var strsqlre = "select contente,replycontente from LCRReport where "
             + " ProposalContNo = '"+ tContNo + "'"
             + " and PrtSeq = '"+ tPrtSeq + "'";
    var arrreport = easyExecSql(strsqlre);
    if(arrreport)
    {
      fm.Contente.value=arrreport[0][0];
      fm.ReplyContente.value=arrreport[0][1];
    }
    //************
   if(fm.LoadFlag.value!=1)
   {
     divRReportButton.style.display = "";
   } 
  }
  return true;
}

// 查询按钮
function easyQueryChoClick(parm1,parm2)
{
  // 书写SQL语句
  k++;
  var tContNo = "";
  var strSQL = "";
	
  if(fm.all(parm1).all('InpQuestGridSel').value == '1' )
  {
    //当前行第1列的值设为：选中
    tContNo = fm.all(parm1).all('QuestGrid1').value;
    tSerialNo = fm.all(parm1).all('QuestGrid5').value;
    
  }

  if (tContNo != "")
  {
    strSQL = "select a.name,b.RReportItemName,a.RReportReason,,from LCRReport a,LCRReportResult b where contno = '"+tContNo+"' and prtseq = '"+tSerialNo+"'";
  }

  var strsql= "select Contente from LCRReport where 1=1"
              +" 	and ContNo = '"+tContNo+"'";


  var arrReturn = new Array();
  arrReturn = easyExecSql(strsql);

  if(arrReturn!=null)
  {

    fm.all('Contente').value = arrReturn[0][0];
  }

  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
    return "";

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = RReportGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strSQL;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  var strSql = "select RReportResult,ICDCode from LCRReportResult where contno='"+tContNo+"' and customerno='"+mCustomerNo+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    turnPage.queryModal(strSql,RReportResultGrid);
  }

  var strSql = "select ReplyContente from LCRReport where contno='"+tContNo+"' and customerno='"+mCustomerNo+"'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    fm.ReplyContente.value = arr[0][0];
  }
  return true;
}

function QueryCustomerList(tContNo)
{
  var i = 0;
  var j = 0;
  var m = 0;
  var n = 0;
  var strsql = "";
  var tCodeData = "0|";
  strsql = "select appntno, appntname from lcappnt where contno = '" + tContNo+"'";
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    m = turnPage.arrDataCacheSet.length;
    for (i = 0; i < m; i++)
    {
      j = i + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][1] + "|" + turnPage.arrDataCacheSet[i][0];
    }
  }
  strsql = "select insuredno, name from lcinsured where contno = '" + tContNo + "' and not insuredno in (select appntno from lcappnt where contno = '" + tContNo + "')";
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  if (turnPage.strQueryResult != null && turnPage.strQueryResult != "")
  {
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    n = turnPage.arrDataCacheSet.length;
    for (i = 0; i < n; i++)
    {
      j = i + m + 1;
      //			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
      tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][1] + "|" + turnPage.arrDataCacheSet[i][0];
    }
  }
  //	alert ("tcodedata : " + tCodeData);

  return tCodeData;
}

function showSelectRecord(tContNo)
{
  easyQueryClick(tContNo, fm.all("CustomerNo").value);
}

function afterCodeSelect(cCodeName, Field)
{
  if (cCodeName == "CustomerName")
  {
    showSelectRecord(fm.all("ContNo").value);
  }
}

/*********************************************************************
 *  生调信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function saveRReport()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //var tSelNo = QuestGrid.getSelNo();
  //if(tSelNo==0)
  //{
  //  alert("请选择生调履历！");
  //  return;
  //}
  //fm.all('PrtSeq').value = QuestGrid.getRowColData(tSelNo - 1,8);
  fm.action= "./RReportQuerySave.jsp";
  fm.submit(); //提交
}


function easyQueryLCRRClick(tContNo)
{
	// 初始化表格
	initLCRReportGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select c.PrtSeq,a.ContNo,a.Name,c.makedate "+
 ",b.PrtNo,b.PolApplyDate,b.CValiDate,a.AppntName,getUniteCode(b.AgentCode),a.ManageCom,a.customerno,a.Appntno,'','',a.replyoperator,a.replydate from LCRReport a ,lccont b ,LOPRTManager c where 1=1 and a.contno=b.contno and a.PrtSeq=c.PrtSeq  "
	                 + "and a.ManageCom like '"+manageCom+"%%'"
	                 + "and a.ContNo = '"+tContNo+"'"
				           + " order by a.makedate, a.maketime"
				 ;	 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有找到相关的数据！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = LCRReportGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


//********modify//
function modifyRecord()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<QuestGrid.mulLineCount; i++) 
  {
    if (QuestGrid.getSelNo(i)) 
    { 
      checkFlag = QuestGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) 
  {
    fm.RReportItemName.value = QuestGrid.getRowColData(checkFlag - 1, 3);
    fm.RRItemContent.value   = QuestGrid.getRowColData(checkFlag - 1, 4);
    fm.RRItemResult.value    = QuestGrid.getRowColData(checkFlag - 1, 5);
    fm.RReportItemCode.value = QuestGrid.getRowColData(checkFlag - 1, 1);
   }
  else 
  {
    alert("请先选择一条契调项目明细报告信息修改！"); 
    return;
  }

  fm.all('fmAction').value="MODIFY||RECORD";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action= "./RReporItemSava.jsp";
  fm.submit();
}

//******//


//********delete//
function delRecord()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<QuestGrid.mulLineCount; i++) 
  {
    if (QuestGrid.getSelNo(i)) 
    { 
      checkFlag = QuestGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) 
  {
    fm.RReportItemName.value = QuestGrid.getRowColData(checkFlag - 1, 3);
    fm.RRItemContent.value   = QuestGrid.getRowColData(checkFlag - 1, 4);
    fm.RRItemResult.value    = QuestGrid.getRowColData(checkFlag - 1, 5);
    fm.RReportItemCode.value = QuestGrid.getRowColData(checkFlag - 1, 1);
   }
  else 
  {
    alert("请先选择一条契调项目明细报告信息修改！"); 
    return;
  }

  fm.all('fmAction').value="DELETE||RECORD";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action= "./RReporItemSava.jsp";
  fm.submit();
}

//******//
