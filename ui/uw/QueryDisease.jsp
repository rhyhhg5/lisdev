<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QueryDisease.jsp
//程序功能：
//创建日期：2006-03-20 18:03:35
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDCodeSchema tLDCodeSchema   = new LDCodeSchema();
  UWSpecInfoUI tUWSpecInfoUI   = new UWSpecInfoUI();

  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
   	System.out.println(transact);
    String tRadio[] = request.getParameterValues("InpQueryDiseaseGridSel");
    String tCodeName[] = request.getParameterValues("QueryDiseaseGrid1");
    String tCCodeType[] = request.getParameterValues("QueryDiseaseGrid2");
    for (int index = 0; index< tRadio.length; index++)
    {
        if(tRadio[index].equals("1"))
        {
            tLDCodeSchema.setCode(tCodeName[index]);
          	System.out.println("tCodeName[" + index + "]:" + tCodeName[index]);
            tLDCodeSchema.setCodeName(tCCodeType[index]);
            System.out.println("tCCodeType[" + index + "]:" + tCCodeType[index]);
            tLDCodeSchema.setCodeType("speccode");
        }
    }

	  TransferData tTransferData= new TransferData();
	  tTransferData.setNameAndValue("SpecNoOld",request.getParameter("SpecNoOld"));
   	System.out.println(request.getParameter("SpecNoOld"));	  
	  tTransferData.setNameAndValue("DiseaseNameOld",request.getParameter("DiseaseNameOld"));
    try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();

	tVData.add(tLDCodeSchema);
  	tVData.add(tG);
	tVData.add(tTransferData);
    tUWSpecInfoUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tUWSpecInfoUI.mErrors;
    if (!tError.needDealError())
    {                  
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	System.out.println(Content);   
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
