<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：FinFeeSure.jsp
//程序功能：到帐确认
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>

<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="FinFeeSure.js"></SCRIPT>
  <%@include file="FinFeeSureInit.jsp"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <title>到帐确认</title>
</head>

<script>
  var currentDate = "<%=PubFun.getCurrentDate()%>";
</script>

<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./FinFeeSureChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            帐户名
          </TD>
          <TD  class= input>
            <Input class= common name=AppntName >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class=codeNo name=ManageCom ondblclick="return showCodeList('station',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('station',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            交费收据号
          </TD>
          <TD  class= input>
            <Input class= common name=TempFeeNo>
          </TD>
          <TD  class= title>
            交费方式
          </TD>
          <TD  class= input>
            <Input class=codeNo name=PayMode ondblclick="return showCodeList('paymode',[this,PayModeName],[0,1]);" onkeyup="return showCodeListKey('paymode',[this,PayModeName],[0,1]);"><input class=codename name=PayModeName readonly=true >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            票据号
          </TD>
          <TD  class= input>
            <Input class=common name=ChequeNo>
          </TD>
        </TR>
    </table> 
    <table align=right>
      <tr>
        <td> 
          <INPUT VALUE="查询" class=cssButton TYPE=button onclick="easyQueryClick();">
        </td>
        <td>
          <INPUT VALUE="填写当天日期" class=cssButton TYPE=button Title="填写当天日期作为选择暂交费的到帐日期" onclick="insertCurrentDate();"> 
        </td>
      </tr>
    </table>
    <br>
    <br>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 未到帐数据信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
    	<Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="getLastPage();"> 	
      </Div>			
  	</div>
  	<p>
  	  <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            交费金额合计
          </TD>
          <TD  class= input>
            <Input class=common name=sumMoney >
          </TD>
           <td class = input>
      	    <input type= hidden name= sqltext>
           </td>	
        </TR>
      </table>
      <table align=right>
        <tr>
          <td>
      <INPUT VALUE="到帐确认" class=cssButton TYPE=button onclick = "autochk();"> 
          </td>
        </tr>
      </table>
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
