<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CutBonusChk.jsp
//程序功能：分红处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
  
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
	
	String tFiscalYear = request.getParameter("FiscalYear");
	String tAssignRate = request.getParameter("AssignRate");
	String tActuRate = request.getParameter("ActuRate");
	String tEnsuRateDefault = request.getParameter("EnsuRateDefault");
	
	String tGrpPolNo[] = request.getParameterValues("PolGrid1");
	String tEnsuRate[] = request.getParameterValues("PolGrid4");
	String tChk[] = request.getParameterValues("InpPolGridChk");
	
	boolean flag = false;
	int feeCount = tGrpPolNo.length;
    LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = new LOBonusGrpPolParmSchema();
    LOBonusGrpPolParmSet tLOBonusGrpPolParmSet=new LOBonusGrpPolParmSet();
	for (int i = 0; i < feeCount; i++)
	{
		if (!tGrpPolNo[i].equals("") && tChk[i].equals("1"))
		{
		    System.out.println("GrpPolNo:"+i+":"+tGrpPolNo[i]);
	  	    
	  	    tLOBonusGrpPolParmSchema = new LOBonusGrpPolParmSchema();
	  	    tLOBonusGrpPolParmSchema.setGrpPolNo(tGrpPolNo[i]);
	  	    tLOBonusGrpPolParmSchema.setEnsuRate(tEnsuRate[i]);
	  	    tLOBonusGrpPolParmSchema.setFiscalYear(tFiscalYear);
		    tLOBonusGrpPolParmSchema.setAssignRate(tAssignRate);
		    tLOBonusGrpPolParmSchema.setActuRate(tActuRate);
		    tLOBonusGrpPolParmSchema.setEnsuRateDefault(tEnsuRateDefault);
		    tLOBonusGrpPolParmSchema.setComputeState("0");
		    tLOBonusGrpPolParmSet.add( tLOBonusGrpPolParmSchema );
		    System.out.println("size:"+tLOBonusGrpPolParmSet.size());
		    flag = true;
		}
	}

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLOBonusGrpPolParmSet );
		tVData.add( tG );
		
		// 数据传输
		BonusGrpPolParmSaveUI tBonusGrpPolParmSaveUI   = new BonusGrpPolParmSaveUI();
		tBonusGrpPolParmSaveUI.submitData(tVData,"INSERT");
		int errNum = tBonusGrpPolParmSaveUI.mErrors.getErrorCount();
		if(errNum>0)
		{			
			for (int i = 0; i < errNum; i++)
			System.out.println("Error: "+tBonusGrpPolParmSaveUI.mErrors.getError(i).errorMessage);
			Content = " 红利处理失败，原因是: ";
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tBonusGrpPolParmSaveUI.mErrors;
		    //tErrors = tBonusGrpPolParmSaveUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 红利处理成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			          Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
				}
		    	FlagStr = "Fail";
		    }
		}
	}
	else
	{
	Content = "请选择并填写数据!";
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	
</script>
</html>
