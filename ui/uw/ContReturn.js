var showInfo;
var turnPage = new turnPageClass();
var k = 0;
// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		// 执行下一步操作
	}
	 easyQueryClick();
}

// 显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}

// 查询按钮
function easyQueryClick() {
	// 初始化表格
	initPolGrid();
	if (!verifyInput2())
		return false;
	// 书写SQL语句
	var strSQL = "";
	PrtNo = fm.PrtNo.value;
	ManageCom = fm.ManageCom.value;
	GroupAgentCode = fm.AgentCode.value;//新增查询条件：业务员
	var agentcode = "";
	var strSQL1 = "select cardflag from lccont where prtno='" + PrtNo + "'";
	cardflag = easyExecSql(strSQL1);
	if(GroupAgentCode != "" && GroupAgentCode != "null" && GroupAgentCode != null){
		var result = easyExecSql(" select Agentcode from laagent where groupagentcode = '"+GroupAgentCode+"' ");
		agentcode = " and b.Agentcode = '"+result+"'";
	}else{
		agentcode = "";
	}
	if (cardflag == 0) {
//		// 0000001150人核完了待签单，0000001110正在人核//0000001100未核保
		alert("没有查询到符合条件的简易单！");
		return false;
//	
		strSQL = "select b.contno,b.prtno,b.appntname,b.ManageCom,"
				+ "(Case When Activityid = '0000001150' Then '待收费' else'录入完毕'end)"
				+ ",a.submissionid,a.MissionID ,a.activityid "
				+ "  from lwmission a,lccont b where 1=1"
				+ " and activityid in ('0000001150','0000001001')"
				+ " and processid = '0000000003' "
				+ " and a.MissionProp2=b.prtno and b.appflag='0' " +
						"and not exists (select 1 from ljtempfee where otherno= b.Prtno and EnterAccDate is not null ) "
				+ agentcode
				+ getWherePart('b.ManageCom', 'ManageCom', 'like')
				+ getWherePart('a.MissionProp2', 'PrtNo');
		turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
		if (!turnPage.strQueryResult) {
			strSQL = "select b.contno,b.prtno,b.appntname,b.ManageCom,'人工核保',a.submissionid,a.MissionID ,a.activityid "
					+ "  from lwmission a,lccont b where 1=1"
					 + " and activityid ='0000001110'"
					+ " and processid = '0000000003' "
					+ " and a.MissionProp1=b.prtno and b.appflag='0'  "
					+ "	And exists (select 1 from Lwmission c where c.missionid=a.missionid and c.activityid ='0000001110')" +
							"and not exists (select 1 from ljtempfee where otherno= b.Prtno and EnterAccDate is not null )"
					+ agentcode
					+ getWherePart('b.ManageCom', 'ManageCom', 'like')
					+ getWherePart('a.MissionProp1', 'PrtNo')
					+ "Fetch First 1 Rows Only With Ur";
			turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

			if (!turnPage.strQueryResult) {

				strSQL = "select b.contno,b.prtno,b.appntname,b.ManageCom,"
						+ "(Case  When Activityid = '0000001098' Then '无扫描录入'When Activityid = '0000001099' Then '扫描录入' Else'新单复核完毕' end),"
						+ "a.submissionid,a.MissionID ,a.activityid "
						+ "  from lwmission a,lccont b where 1=1"
						+ " and activityid in ('0000001100','0000001098','0000001099')"
						+ " and processid = '0000000003' "
						+ " and a.MissionProp1=b.prtno and b.appflag='0' " +
								"and not exists (select 1 from ljtempfee where otherno= b.Prtno and EnterAccDate is not null ) "
						+ agentcode
						+ getWherePart('b.ManageCom', 'ManageCom', 'like')
						+ getWherePart('a.MissionProp1', 'PrtNo');
				turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

				// 判断是否查询成功
				if (!turnPage.strQueryResult) {
					alert("没有查询到符合条件的的个单！");
					return false;
				}

			}

		}
	} else {
		strSQL = "select b.contno,b.prtno,b.appntname,b.ManageCom,'录入完毕',a.submissionid,a.MissionID ,a.activityid "
				+ "  from lwmission a,lccont b where "
				+ "  processid in( '0000000007','0000000013') "
				+ " and a.MissionProp2=b.prtno and b.appflag='0' and a.ActivityID in('0000007002','0000013002' )" +
						"and not exists (select 1 from ljtempfee where otherno= b.Prtno and EnterAccDate is not null )"
				+ " and a.MissionProp2='" + PrtNo + "'"
				+ agentcode
				+ getWherePart('b.ManageCom', 'ManageCom', 'like');
		turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);

		// 判断是否查询成功
		if (!turnPage.strQueryResult) {
			strSQL = "select b.contno,b.prtno,b.appntname,b.ManageCom,'待录入',a.submissionid,a.MissionID ,a.activityid "
				+ "  from lwmission a,lccont b where "
				+ "  processid in( '0000000007','0000000013') "
				+ " and a.MissionProp1=b.prtno and b.appflag='0' and a.ActivityID in('0000007001','0000007003','0000013001')   "
				+ " and a.MissionProp1='" + PrtNo + "'"
				+ agentcode
				+ getWherePart('b.ManageCom', 'ManageCom', 'like');
			turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
			// alert(turnPage.strQueryResult);	
			if (!turnPage.strQueryResult) {
			alert("没有查询到符合条件的个单！");
			return "";
		}
	}
	}

	// 查询SQL，返回结果字符串

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	// 设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = PolGrid;

	// 保存SQL语句
	turnPage.strQuerySql = strSQL;

	// 设置查询起始位置
	turnPage.pageIndex = 0;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
			turnPage.pageIndex, MAXSCREENLINES);
	// alert(arrDataSet);
	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	return true;
}

function checkForm() {
	var checkFlag = 0;
	for (i = 0; i < PolGrid.mulLineCount; i++) {
		if (PolGrid.getSelNo(i)) {
			checkFlag = PolGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var tPrtNo = PolGrid.getRowColData(checkFlag - 1, 2);
		if (tPrtNo == null || tPrtNo == "" || tPrtNo == "null") {
			alert("请选择保单！");
			return false;
		}
		var tSQL = "select uwdate,uwflag,(select codename from ldcode where codetype='uwflag' and code=lccont.uwflag ) from lccont where prtno = '"
				+ tPrtNo + "'";
		var arrResult = easyExecSql(tSQL);
		if (arrResult[0][0] != null && arrResult[0][1] != null) {
			if (arrResult[0][1] == '1' || arrResult[0][1] == '2'
					|| arrResult[0][1] == 'a') {
				alert("改单核保结论为" + arrResult[0][2] + "不能回退");
				return false;
			}

		}
		var strsql = "select 1 from ljtempfee t where otherno = '" + tPrtNo
				+ "' " + " and EnterAccDate is not null ";
		var arr = easyExecSql(strsql);
		if (arr) {

			alert("保单已交费,不能回退!");
			return false;
		}
		strSQL = "select 1"
				+ "  from lwmission a,lccont b where 1=1"
				+ " and activityid in('0000001104','0000001105')"
				+ " and processid = '0000000003' "
				+ " and a.MissionProp1=b.prtno and b.appflag='0'  "
				+ "	And exists (select 1 from Lwmission c where c.missionid=a.missionid and c.activityid ='0000001110')"
				+ getWherePart('b.ManageCom', 'ManageCom', 'like')
				+ getWherePart('a.MissionProp1', 'PrtNo')
				+ "Fetch First 1 Rows Only With Ur";
		var arr = easyExecSql(strSQL);
		if (arr) {

			alert("保单正在进行人工核保,不能回退!");
			return false;
		}
		strSQL = "select 1" + "  from lwmission a,lccont b where 1=1"
				+ " and activityid in('0000001001')"
				+ " and processid = '0000000003' "
				+ " and a.MissionProp2=b.prtno and b.appflag='0'  "
				+ getWherePart('b.ManageCom', 'ManageCom', 'like')
				+ getWherePart('a.MissionProp2', 'PrtNo')
				+ "Fetch First 1 Rows Only With Ur";
		var arr = easyExecSql(strSQL);
		if (arr) {

			alert("保单为待新单复核状态,不需要回退!");
			return false;
		}
		strSQL = "select 1" + "  from lwmission a,lccont b where 1=1"
				+ " and activityid in('0000001098','0000001099')"
				+ " and processid = '0000000003' "
				+ " and a.MissionProp1=b.prtno and b.appflag='0'  "
				+ getWherePart('b.ManageCom', 'ManageCom', 'like')
				+ getWherePart('a.MissionProp1', 'PrtNo')
				+ "Fetch First 1 Rows Only With Ur";
		var arr = easyExecSql(strSQL);
		if (arr) {

			alert("保单为待录入状态,不需要回退!");
			return false;
		}
		strSQL = "select 1 "
			+ "  from lwmission a,lccont b where "
			+ "  processid in( '0000000007','0000000013') "
			+ " and a.MissionProp1=b.prtno and b.appflag='0' and a.ActivityID in('0000007001','0000007003','0000013001')  "
			+ " and a.MissionProp1='" + PrtNo + "'";
		var arr = easyExecSql(strSQL);
		if (arr) {

			alert("保单为待录入状态,不需要回退!");
			return false;
		}
	} else {
		alert("请先选择保单信息！");
		return false;
	}
	return true;
}
function back() {
	if (!checkForm()) {
		return false;
	}

	var i = 0;
	var showStr = "正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./ContReturnSave.jsp"
	fm.submit(); // 提交

}

// 查询按钮
function easyQueryAddClick(parm1, parm2) {

	var tcontno = "";
	var tPrtNo = "";
	var tAppntName = "";
	var tManageCom = "";
	var tMissionID = "";
	var tSubMissionID = "";
	var tactivityid = "";
	if (fm.all(parm1).all('InpPolGridSel').value == '1') {
		// 当前行第1列的值设为：选中
		tcontno = fm.all(parm1).all('PolGrid1').value;
		tPrtNo = fm.all(parm1).all('PolGrid2').value;
		tAppntName = fm.all(parm1).all('PolGrid3').value;
		tManageCom = fm.all(parm1).all('PolGrid4').value;
		tSubMissionID = fm.all(parm1).all('PolGrid6').value;
		tMissionID = fm.all(parm1).all('PolGrid7').value;
		tactivityid = fm.all(parm1).all('PolGrid8').value;

	}

	fm.PrtNo.value = tPrtNo;
	fm.ContNo.value = tcontno;
	fm.SubMissionID.value = tSubMissionID;
	fm.MissionId.value = tMissionID;
	fm.ActivityID.value = tactivityid;

	return true;
}
