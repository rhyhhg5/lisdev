<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：InsuredUWInfoChk.jsp
//程序功能：人工核保体检资料查询
//创建日期：2005-01-19 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
String Flag = request.getParameter("flag");

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

if(tG == null) {
    out.println("session has expired");
    return;
  }
System.out.println("Flag : "+Flag);
if(Flag.equals("StopInsured")) {
    LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();

    tLCInsuredSchema.setContNo(request.getParameter("ContNo"));
    tLCInsuredSchema.setInsuredNo(request.getParameter("InsuredNo"));
    tLCInsuredSchema.setInsuredStat("1");    //1-暂停 0-未暂停

    VData tVData = new VData();
    FlagStr="";

    tVData.add(tG);
    tVData.add(tLCInsuredSchema);

    StopInsuredUI tStopInsuredUI = new StopInsuredUI();

    try {
        System.out.println("this will save the data!!!");
        tStopInsuredUI.submitData(tVData,"");
      } catch(Exception ex) {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
      }

    if (!FlagStr.equals("Fail")) {
        tError = tStopInsuredUI.mErrors;
        if (!tError.needDealError()) {
            Content = " 保存成功! ";
            FlagStr = "Succ";
          } else {
            Content = " 保存失败，原因是:" + tError.getFirstError();
            FlagStr = "Fail";
          }
      }
  } 
  else if(Flag.equals("risk")) {
  	if(request.getParameter("uwstate").equals("9")){
  		//如果是正常承保，判断如果有加费等其他信息的话需要先删除,然后再执行正常通过
  		LCUWMasterDB queryLCUWMasterDB =new LCUWMasterDB();
  		queryLCUWMasterDB.setProposalNo(request.getParameter("PolNo"));
  		LCUWMasterSet queryLCUWMasterSet=queryLCUWMasterDB.query();
  		LCUWMasterSchema queryLCUWMasterSchema=queryLCUWMasterSet.get(1);
  		
  		
  		
  	System.out.println("queryLCUWMasterSchema.getAddPremFlag()"+queryLCUWMasterSchema.getAddPremFlag());
  	System.out.println("queryLCUWMasterSchema.getSpecFlag()"+queryLCUWMasterSchema.getSpecFlag());
  	System.out.println("queryLCUWMasterSchema.getSubMultFlag()"+queryLCUWMasterSchema.getSubMultFlag());
  	System.out.println("queryLCUWMasterSchema.getSubAmntFlag()"+queryLCUWMasterSchema.getSubAmntFlag());
  	System.out.println("queryLCUWMasterSchema.getChPayEndYearFlag()"+queryLCUWMasterSchema.getChPayEndYearFlag());
  	
  			//加费
  			if((!(StrTool.cTrim(queryLCUWMasterSchema.getAddPremFlag()).equals("")))&&(!(StrTool.cTrim(queryLCUWMasterSchema.getAddPremFlag()).equals("0")))){
						    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
						    System.out.println("减去AddFee");
						    String tPolNo=request.getParameter("PolNo");
						    String tUWIdea=request.getParameter("UWIdea");
						    String tContNo=request.getParameter("ContNo");
						    String tinputExplain = request.getParameter("inputExplain");
						
								String tinputDelayDays = request.getParameter("inputDelayDays");
								System.out.println("tinputDelayDays : "+tinputDelayDays);
								String tinputDelayDate = request.getParameter("inputDelayDate");
						    String tPassFlag="4";
						    String tSugUWFlag = request.getParameter("SugUWFlag");
						    String tLoadFlag = "L";
						    String tAddFeeType = request.getParameter("AddFeeType");
						    
						    System.out.println("===============------"+tLoadFlag);
						    tLCUWMasterSchema.setPolNo(tPolNo);
						    tLCUWMasterSchema.setProposalNo(tPolNo);
						    tLCUWMasterSchema.setPassFlag(tPassFlag);
						    tLCUWMasterSchema.setUWIdea(tUWIdea);
						    tLCUWMasterSchema.setPostponeDay(tinputDelayDays);
						    tLCUWMasterSchema.setPostponeDate(tinputDelayDate);
						    tLCUWMasterSchema.setUWIdea(tUWIdea);
						    tLCUWMasterSchema.setSugPassFlag(tSugUWFlag);
						//    tLCNotePadSchema.setNotePadcont(""); 
						    System.out.println("YYYY"+request.getParameter("CancelReason"));
								tLCUWMasterSchema.setCustomerReply(request.getParameter("CancelReason"));
							tLCUWMasterSchema.setRejectReason(request.getParameter("RejectReason"));
						    TransferData mTransferData = new TransferData();
						    //如果是加费的话，要区别健康加费与职业加费
						    //核保结论取消加费，将加费作为一个标志进行处理，
						    //核保结论统一为正常通过，变更承保，拒保
						    
						    //封装加费信息
						    LCPremSet tLCPremSet = new LCPremSet();
						    String tAddFeeValue[] = request.getParameterValues("AddFeeGrid1");
								String tAddFeeRate[] = request.getParameterValues("AddFeeGrid2");            //告知版别
								String tPayStartDate[] = request.getParameterValues("AddFeeGrid3");           //告知编码
								String tPayEndDate[] = request.getParameterValues("AddFeeGrid4");           //告知编码
								String tNo[] = request.getParameterValues("AddFeeGridNo");
							
                            //评点加费信息
						    LCUWDiseaseCheckInfoSet tLCUWDiseaseCheckInfoSet=new LCUWDiseaseCheckInfoSet(); 
						    String tDiseaseCode[] = request.getParameterValues("DiseaseCheckGrid1");
								String tDiseaseType[] = request.getParameterValues("DiseaseCheckGrid3");            //告知版别
								String tDiseaseValue[] = request.getParameterValues("DiseaseCheckGrid4");           //告知编码
								String tDiseaseNo[] = request.getParameterValues("DiseaseCheckGridNo");
						    
						    if(tLoadFlag.equals("L")) {
						    System.out.println("tAddFeeType"+tAddFeeType);
						        mTransferData.setNameAndValue("AddFeeFlag",tLoadFlag);//tAddFeeType
						        System.out.println("AddFeeFlag"+tLoadFlag);
						        mTransferData.setNameAndValue("AddFeeType",tAddFeeType);
						        System.out.println("AddFeeType"+tAddFeeType);
						        
						      //封装免责信息
						      
						    // 准备传输数据 VData
						    VData tVData = new VData();
						    FlagStr="";
						    tVData.add(tG);
						    tVData.add(tLCUWMasterSchema);
						    tVData.add(mTransferData);
    						tVData.add(tLCPremSet);
    						tVData.add(tLCUWDiseaseCheckInfoSet);
						    
						    ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
						    try {
						        System.out.println("this will save the data!!!");
						        tManuUWRiskSaveUI.submitData(tVData,"");
						      } catch(Exception ex) {
						        Content = "保存失败，原因是:" + ex.toString();
						        FlagStr = "Fail";
						      }
						
						    if (!FlagStr.equals("Fail")) {
						        tError = tManuUWRiskSaveUI.mErrors;
						        if (!tError.needDealError()) {
						            Content = " 保存成功! ";
						            FlagStr = "Succ";
						          } else {
						            Content = " 保存失败，原因是:" + tError.getFirstError();
						            FlagStr = "Fail";
						          }
						      }
  					}
  			}
  			//免责
  			if((!(StrTool.cTrim(queryLCUWMasterSchema.getSpecFlag()).equals("")))&&(!(StrTool.cTrim(queryLCUWMasterSchema.getSpecFlag()).equals("0")))){
  					System.out.println("减去免责");
  			    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
				    String tPolNo=request.getParameter("PolNo");
				    String tUWIdea=request.getParameter("UWIdea");
				
				    String tContNo=request.getParameter("ContNo");
				    String tinputExplain = request.getParameter("inputExplain");
				    
				  //延期天数，延期日期
						String tinputDelayDays = request.getParameter("inputDelayDays");
						System.out.println("tinputDelayDays : "+tinputDelayDays);
						String tinputDelayDate = request.getParameter("inputDelayDate");
				    String tPassFlag="4";
				    String tSugUWFlag = request.getParameter("SugUWFlag");
				    String tLoadFlag = "E";
				    String tAddFeeType = request.getParameter("AddFeeType");
				    
				    System.out.println("===============------"+tLoadFlag);
				    tLCUWMasterSchema.setPolNo(tPolNo);
				    tLCUWMasterSchema.setProposalNo(tPolNo);
				    tLCUWMasterSchema.setPassFlag(tPassFlag);
				    tLCUWMasterSchema.setUWIdea(tUWIdea);
				    tLCUWMasterSchema.setPostponeDay(tinputDelayDays);
				    tLCUWMasterSchema.setPostponeDate(tinputDelayDate);
				    tLCUWMasterSchema.setUWIdea(tUWIdea);
				    tLCUWMasterSchema.setSugPassFlag(tSugUWFlag);
				    System.out.println("YYYY"+request.getParameter("CancelReason"));
						tLCUWMasterSchema.setCustomerReply(request.getParameter("CancelReason"));
					tLCUWMasterSchema.setRejectReason(request.getParameter("RejectReason"));
				    TransferData mTransferData = new TransferData();
				    //如果是加费的话，要区别健康加费与职业加费
				    //核保结论取消加费，将加费作为一个标志进行处理，
				    //核保结论统一为正常通过，变更承保，拒保
				    
				    //免责信息
				    LCSpecSet tLCSpecSet = new LCSpecSet();
				    String tSpecCode[] = request.getParameterValues("SpecGrid1");
						String tSpecInfo[] = request.getParameterValues("SpecGrid2");            //告知版别
						String tSpecStartDate[] = request.getParameterValues("SpecGrid3");           //告知编码
						String tSpecEndDate[] = request.getParameterValues("SpecGrid4");           //告知编码
						String tSerialNo[] = request.getParameterValues("SpecGrid5");
						String tSpecNo[] = request.getParameterValues("SpecGridNo");
				    //封装加费信息
				    LCPremSet tLCPremSet = new LCPremSet();
				    String tAddFeeValue[] = request.getParameterValues("AddFeeGrid1");
						String tAddFeeRate[] = request.getParameterValues("AddFeeGrid2");            //告知版别
						String tPayStartDate[] = request.getParameterValues("AddFeeGrid3");           //告知编码
						String tPayEndDate[] = request.getParameterValues("AddFeeGrid4");           //告知编码
						String tNo[] = request.getParameterValues("AddFeeGridNo");
						//评点加费信息
				    LCUWDiseaseCheckInfoSet tLCUWDiseaseCheckInfoSet=new LCUWDiseaseCheckInfoSet(); 
				    String tDiseaseCode[] = request.getParameterValues("DiseaseCheckGrid1");
						String tDiseaseType[] = request.getParameterValues("DiseaseCheckGrid3");            //告知版别
						String tDiseaseValue[] = request.getParameterValues("DiseaseCheckGrid4");           //告知编码
						String tDiseaseNo[] = request.getParameterValues("DiseaseCheckGridNo");
				    
						//降档信息
						LCDutySet tLCDutySet = new LCDutySet();
						String tPayEndYear = request.getParameter("newPayEndYear");
						String tPayEndYearFlag = request.getParameter("newPayEndYearFlag");
						System.out.println("newPayEndYear"+tPayEndYear);
						System.out.println("newPayEndYearFlag"+tPayEndYearFlag);
						//变更缴费期间信息
						String tMult = request.getParameter("inputMult");
						String tinputAmnt = request.getParameter("inputAmnt");
						int n = 0;
						int nn = 0;
						if(tNo != null )
							 n = tNo.length ;
						if(tDiseaseNo != null )
							 nn = tDiseaseNo.length ;
							  
				      //封装免责信息
				      if(tLoadFlag.equals("E")) {
				    		mTransferData.setNameAndValue("SpecFlag",tLoadFlag);
				    		mTransferData.setNameAndValue("SpecReason",tUWIdea);
				      }
				    // 准备传输数据 VData
				    VData tVData = new VData();
				    FlagStr="";
				    tVData.add(tG);
				    tVData.add(tLCUWMasterSchema);
				    tVData.add(mTransferData);
				    tVData.add(tLCPremSet);
				    tVData.add(tLCSpecSet);
				    tVData.add(tLCDutySet);
				    tVData.add(tLCUWDiseaseCheckInfoSet);
				    
				    
				    ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
				    try {
				        System.out.println("this will save the data!!!");
				        tManuUWRiskSaveUI.submitData(tVData,"");
				      } catch(Exception ex) {
				        Content = "保存失败，原因是:" + ex.toString();
				        FlagStr = "Fail";
				      }
				
				    if (!FlagStr.equals("Fail")) {
				        tError = tManuUWRiskSaveUI.mErrors;
				        if (!tError.needDealError()) {
				            Content = " 保存成功! ";
				            FlagStr = "Succ";
				          } else {
				            Content = " 保存失败，原因是:" + tError.getFirstError();
				            FlagStr = "Fail";
				          }
				      }
  			}
  			//降档
  			if((!(StrTool.cTrim(queryLCUWMasterSchema.getSubMultFlag()).equals("")))&&(!(StrTool.cTrim(queryLCUWMasterSchema.getSubMultFlag()).equals("0")))){
				  	System.out.println("开始处理降档");
				    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
				    String tPolNo=request.getParameter("PolNo");
				    String tUWIdea=request.getParameter("UWIdea");
				    String tContNo=request.getParameter("ContNo");
				    String tinputExplain = request.getParameter("inputExplain");
				  //延期天数，延期日期
						String tinputDelayDays = request.getParameter("inputDelayDays");
						System.out.println("tinputDelayDays : "+tinputDelayDays);
						String tinputDelayDate = request.getParameter("inputDelayDate");
				    String tPassFlag="4";
				    String tSugUWFlag = request.getParameter("SugUWFlag");
				    String tLoadFlag = "M";
				    String tAddFeeType = request.getParameter("AddFeeType");
				    
				    System.out.println("===============------"+tLoadFlag);
				    tLCUWMasterSchema.setPolNo(tPolNo);
				    tLCUWMasterSchema.setProposalNo(tPolNo);
				    tLCUWMasterSchema.setPassFlag(tPassFlag);
				    tLCUWMasterSchema.setUWIdea(tUWIdea);
				    tLCUWMasterSchema.setPostponeDay(tinputDelayDays);
				    tLCUWMasterSchema.setPostponeDate(tinputDelayDate);
				    tLCUWMasterSchema.setUWIdea(tUWIdea);
				    tLCUWMasterSchema.setSugPassFlag(tSugUWFlag);
				//    tLCNotePadSchema.setNotePadcont(""); 
				    System.out.println("YYYY"+request.getParameter("CancelReason"));
						tLCUWMasterSchema.setCustomerReply(request.getParameter("CancelReason"));
			 	 	tLCUWMasterSchema.setRejectReason(request.getParameter("RejectReason"));
					TransferData mTransferData = new TransferData();
				    //如果是加费的话，要区别健康加费与职业加费
				    //核保结论取消加费，将加费作为一个标志进行处理，
				    //核保结论统一为正常通过，变更承保，拒保
				    
				    //免责信息
				    LCSpecSet tLCSpecSet = new LCSpecSet();
				    String tSpecCode[] = request.getParameterValues("SpecGrid1");
						String tSpecInfo[] = request.getParameterValues("SpecGrid2");            //告知版别
						String tSpecStartDate[] = request.getParameterValues("SpecGrid3");           //告知编码
						String tSpecEndDate[] = request.getParameterValues("SpecGrid4");           //告知编码
						String tSerialNo[] = request.getParameterValues("SpecGrid5");
						String tSpecNo[] = request.getParameterValues("SpecGridNo");
				    //封装加费信息
				    LCPremSet tLCPremSet = new LCPremSet();
				    String tAddFeeValue[] = request.getParameterValues("AddFeeGrid1");
						String tAddFeeRate[] = request.getParameterValues("AddFeeGrid2");            //告知版别
						String tPayStartDate[] = request.getParameterValues("AddFeeGrid3");           //告知编码
						String tPayEndDate[] = request.getParameterValues("AddFeeGrid4");           //告知编码
						String tNo[] = request.getParameterValues("AddFeeGridNo");
						//评点加费信息
				    //LCUWDiseaseCheckInfoSet tLCUWDiseaseCheckInfoSet=new LCUWDiseaseCheckInfoSet(); 
				    String tDiseaseCode[] = request.getParameterValues("DiseaseCheckGrid1");
						String tDiseaseType[] = request.getParameterValues("DiseaseCheckGrid3");            //告知版别
						String tDiseaseValue[] = request.getParameterValues("DiseaseCheckGrid4");           //告知编码
						String tDiseaseNo[] = request.getParameterValues("DiseaseCheckGridNo");
				    
						//降档信息
						LCDutySet tLCDutySet = new LCDutySet();
						String tPayEndYear = request.getParameter("newPayEndYear");
						String tPayEndYearFlag = request.getParameter("newPayEndYearFlag");
						System.out.println("newPayEndYear"+tPayEndYear);
						System.out.println("newPayEndYearFlag"+tPayEndYearFlag);
						//变更缴费期间信息
						String tMult = request.getParameter("initMult");
						String tinputAmnt = request.getParameter("initAmnt");
						int n = 0;
						int nn = 0;
						if(tNo != null )
							 n = tNo.length ;
						 //封装免责信息
				     if(tLoadFlag.equals("M")) {
									LCDutySchema tLCDutySchema = new LCDutySchema();
									tLCDutySchema.setMult(tMult);
									tLCDutySchema.setAmnt(tinputAmnt);
									tLCDutySchema.setPolNo(tPolNo);
									tLCDutySet.add(tLCDutySchema);
    							mTransferData.setNameAndValue("LoadFlag",tLoadFlag);
				      }
				      
				    // 准备传输数据 VData
				    VData tVData = new VData();
				    FlagStr="";
				
				    tVData.add(tG);
				    tVData.add(tLCUWMasterSchema);
				    tVData.add(mTransferData);
				    tVData.add(tLCPremSet);
				    tVData.add(tLCSpecSet);
				    tVData.add(tLCDutySet);
				    //tVData.add(tLCUWDiseaseCheckInfoSet);
				    ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
				    try {
				        System.out.println("this will save the data!!!");
				        tManuUWRiskSaveUI.submitData(tVData,"");
				      } catch(Exception ex) {
				        Content = "保存失败，原因是:" + ex.toString();
				        FlagStr = "Fail";
				      }
				
				    if (!FlagStr.equals("Fail")) {
				        tError = tManuUWRiskSaveUI.mErrors;
				        if (!tError.needDealError()) {
				            Content = " 保存成功! ";
				            FlagStr = "Succ";
				          } else {
				            Content = " 保存失败，原因是:" + tError.getFirstError();
				            FlagStr = "Fail";
				          }
				      }  			
  				}
  			
  			//减额
  			if((!(StrTool.cTrim(queryLCUWMasterSchema.getSubAmntFlag()).equals("")))&&(!(StrTool.cTrim(queryLCUWMasterSchema.getSubAmntFlag()).equals("0")))){
				  	System.out.println("开始处理降档");
				    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
				    String tPolNo=request.getParameter("PolNo");
				    String tUWIdea=request.getParameter("UWIdea");
				    String tContNo=request.getParameter("ContNo");
				    String tinputExplain = request.getParameter("inputExplain");
				  //延期天数，延期日期
						String tinputDelayDays = request.getParameter("inputDelayDays");
						System.out.println("tinputDelayDays : "+tinputDelayDays);
						String tinputDelayDate = request.getParameter("inputDelayDate");
				    String tPassFlag="4";
				    String tSugUWFlag = request.getParameter("SugUWFlag");
				    String tLoadFlag = "A";
				    String tAddFeeType = request.getParameter("AddFeeType");
				    
				    System.out.println("===============------"+tLoadFlag);
				    tLCUWMasterSchema.setPolNo(tPolNo);
				    tLCUWMasterSchema.setProposalNo(tPolNo);
				    tLCUWMasterSchema.setPassFlag(tPassFlag);
				    tLCUWMasterSchema.setUWIdea(tUWIdea);
				    tLCUWMasterSchema.setPostponeDay(tinputDelayDays);
				    tLCUWMasterSchema.setPostponeDate(tinputDelayDate);
				    tLCUWMasterSchema.setUWIdea(tUWIdea);
				    tLCUWMasterSchema.setSugPassFlag(tSugUWFlag);
				//    tLCNotePadSchema.setNotePadcont(""); 
				    System.out.println("YYYY"+request.getParameter("CancelReason"));
						tLCUWMasterSchema.setCustomerReply(request.getParameter("CancelReason"));
					tLCUWMasterSchema.setRejectReason(request.getParameter("RejectReason"));
					TransferData mTransferData = new TransferData();
				    //如果是加费的话，要区别健康加费与职业加费
				    //核保结论取消加费，将加费作为一个标志进行处理，
				    //核保结论统一为正常通过，变更承保，拒保
				    
				    //免责信息
				    LCSpecSet tLCSpecSet = new LCSpecSet();
				    String tSpecCode[] = request.getParameterValues("SpecGrid1");
						String tSpecInfo[] = request.getParameterValues("SpecGrid2");            //告知版别
						String tSpecStartDate[] = request.getParameterValues("SpecGrid3");           //告知编码
						String tSpecEndDate[] = request.getParameterValues("SpecGrid4");           //告知编码
						String tSerialNo[] = request.getParameterValues("SpecGrid5");
						String tSpecNo[] = request.getParameterValues("SpecGridNo");
				    //封装加费信息
				    LCPremSet tLCPremSet = new LCPremSet();
				    String tAddFeeValue[] = request.getParameterValues("AddFeeGrid1");
						String tAddFeeRate[] = request.getParameterValues("AddFeeGrid2");            //告知版别
						String tPayStartDate[] = request.getParameterValues("AddFeeGrid3");           //告知编码
						String tPayEndDate[] = request.getParameterValues("AddFeeGrid4");           //告知编码
						String tNo[] = request.getParameterValues("AddFeeGridNo");
						//评点加费信息
				    LCUWDiseaseCheckInfoSet tLCUWDiseaseCheckInfoSet=new LCUWDiseaseCheckInfoSet(); 
				    String tDiseaseCode[] = request.getParameterValues("DiseaseCheckGrid1");
						String tDiseaseType[] = request.getParameterValues("DiseaseCheckGrid3");            //告知版别
						String tDiseaseValue[] = request.getParameterValues("DiseaseCheckGrid4");           //告知编码
						String tDiseaseNo[] = request.getParameterValues("DiseaseCheckGridNo");
				    
						//降档信息
						LCDutySet tLCDutySet = new LCDutySet();
						String tPayEndYear = request.getParameter("newPayEndYear");
						String tPayEndYearFlag = request.getParameter("newPayEndYearFlag");
						System.out.println("newPayEndYear"+tPayEndYear);
						System.out.println("newPayEndYearFlag"+tPayEndYearFlag);
						//变更缴费期间信息
						String tMult = request.getParameter("initMult");
						String tinputAmnt = request.getParameter("initAmnt");
						int n = 0;
						int nn = 0;
						if(tNo != null )
							 n = tNo.length ;
						 //封装免责信息
				     if(tLoadFlag.equals("A")) {
									LCDutySchema tLCDutySchema = new LCDutySchema();
									tLCDutySchema.setMult(tMult);
									tLCDutySchema.setAmnt(tinputAmnt);
									tLCDutySchema.setPolNo(tPolNo);
									tLCDutySet.add(tLCDutySchema);
    							mTransferData.setNameAndValue("LoadFlag",tLoadFlag);
				      }
				      
				    // 准备传输数据 VData
				    VData tVData = new VData();
				    FlagStr="";
				    tVData.add(tG);
				    tVData.add(tLCUWMasterSchema);
				    tVData.add(mTransferData);
				    tVData.add(tLCPremSet);
				    tVData.add(tLCSpecSet);
				    tVData.add(tLCDutySet);
				    tVData.add(tLCUWDiseaseCheckInfoSet);
				    ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
				    try {
				        System.out.println("this will save the data!!!");
				        tManuUWRiskSaveUI.submitData(tVData,"");
				      } catch(Exception ex) {
				        Content = "保存失败，原因是:" + ex.toString();
				        FlagStr = "Fail";
				      }
				
				    if (!FlagStr.equals("Fail")) {
				        tError = tManuUWRiskSaveUI.mErrors;
				        if (!tError.needDealError()) {
				            Content = " 保存成功! ";
				            FlagStr = "Succ";
				          } else {
				            Content = " 保存失败，原因是:" + tError.getFirstError();
				            FlagStr = "Fail";
				          }
				      }  			
  				}
  			//变更缴费期
  			if((!(StrTool.cTrim(queryLCUWMasterSchema.getChPayEndYearFlag()).equals("")))&&(!(StrTool.cTrim(queryLCUWMasterSchema.getChPayEndYearFlag()).equals("0")))){
				  	System.out.println("减去变更缴费期");
				    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
				    String tPolNo=request.getParameter("PolNo");
				    String tUWIdea=request.getParameter("UWIdea");
				    String tContNo=request.getParameter("ContNo");
				    String tinputExplain = request.getParameter("inputExplain");
				  //延期天数，延期日期
						String tinputDelayDays = request.getParameter("inputDelayDays");
						System.out.println("tinputDelayDays : "+tinputDelayDays);
						String tinputDelayDate = request.getParameter("inputDelayDate");
				    String tPassFlag="4";
				    String tSugUWFlag = request.getParameter("SugUWFlag");
				    String tLoadFlag = "C";
				    String tAddFeeType = request.getParameter("AddFeeType");
				    
				    System.out.println("===============------"+tLoadFlag);
				    tLCUWMasterSchema.setPolNo(tPolNo);
				    tLCUWMasterSchema.setProposalNo(tPolNo);
				    tLCUWMasterSchema.setPassFlag(tPassFlag);
				    tLCUWMasterSchema.setUWIdea(tUWIdea);
				    tLCUWMasterSchema.setPostponeDay(tinputDelayDays);
				    tLCUWMasterSchema.setPostponeDate(tinputDelayDate);
				    tLCUWMasterSchema.setUWIdea(tUWIdea);
				    tLCUWMasterSchema.setSugPassFlag(tSugUWFlag);
				//    tLCNotePadSchema.setNotePadcont(""); 
				    System.out.println("YYYY"+request.getParameter("CancelReason"));
						tLCUWMasterSchema.setCustomerReply(request.getParameter("CancelReason"));
					tLCUWMasterSchema.setRejectReason(request.getParameter("RejectReason"));
				    TransferData mTransferData = new TransferData();
				    //如果是加费的话，要区别健康加费与职业加费
				    //核保结论取消加费，将加费作为一个标志进行处理，
				    //核保结论统一为正常通过，变更承保，拒保
				    //免责信息
				    LCSpecSet tLCSpecSet = new LCSpecSet();
				    String tSpecCode[] = request.getParameterValues("SpecGrid1");
						String tSpecInfo[] = request.getParameterValues("SpecGrid2");            //告知版别
						String tSpecStartDate[] = request.getParameterValues("SpecGrid3");           //告知编码
						String tSpecEndDate[] = request.getParameterValues("SpecGrid4");           //告知编码
						String tSerialNo[] = request.getParameterValues("SpecGrid5");
						String tSpecNo[] = request.getParameterValues("SpecGridNo");
				    //封装加费信息
				    LCPremSet tLCPremSet = new LCPremSet();
				    String tAddFeeValue[] = request.getParameterValues("AddFeeGrid1");
						String tAddFeeRate[] = request.getParameterValues("AddFeeGrid2");            //告知版别
						String tPayStartDate[] = request.getParameterValues("AddFeeGrid3");           //告知编码
						String tPayEndDate[] = request.getParameterValues("AddFeeGrid4");           //告知编码
						String tNo[] = request.getParameterValues("AddFeeGridNo");
						//评点加费信息
				    LCUWDiseaseCheckInfoSet tLCUWDiseaseCheckInfoSet=new LCUWDiseaseCheckInfoSet(); 
				    String tDiseaseCode[] = request.getParameterValues("DiseaseCheckGrid1");
						String tDiseaseType[] = request.getParameterValues("DiseaseCheckGrid3");            //告知版别
						String tDiseaseValue[] = request.getParameterValues("DiseaseCheckGrid4");           //告知编码
						String tDiseaseNo[] = request.getParameterValues("DiseaseCheckGridNo");
				    
						//降档信息
						LCDutySet tLCDutySet = new LCDutySet();
						String tPayEndYear = request.getParameter("oldPayEndYear");
						String tPayEndYearFlag = request.getParameter("oldPayEndYearFlag");
						System.out.println("newPayEndYear"+tPayEndYear);
						System.out.println("newPayEndYearFlag"+tPayEndYearFlag);
						//变更缴费期间信息
						String tMult = request.getParameter("inputMult");
						String tinputAmnt = request.getParameter("inputAmnt");
						int n = 0;
						int nn = 0;
						if(tNo != null )
							 n = tNo.length ;
						if(tDiseaseNo != null )
							 nn = tDiseaseNo.length ;
							 if(tLoadFlag.equals("C")) {//如果变更缴费期间
								LCDutySchema tLCDutySchema = new LCDutySchema();
								tLCDutySchema.setPayEndYear(tPayEndYear);
								tLCDutySchema.setPayEndYearFlag(tPayEndYearFlag);
								tLCDutySchema.setPolNo(tPolNo);
								tLCDutySet.add(tLCDutySchema);
				    		mTransferData.setNameAndValue("LoadFlag",tLoadFlag);
				      }
				      
				    // 准备传输数据 VData
				    VData tVData = new VData();
				    FlagStr="";
				
				    tVData.add(tG);
				    tVData.add(tLCUWMasterSchema);
				    tVData.add(mTransferData);
				    tVData.add(tLCPremSet);
				    tVData.add(tLCSpecSet);
				    tVData.add(tLCDutySet);
				    tVData.add(tLCUWDiseaseCheckInfoSet);
				    ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
				    try {
				        System.out.println("this will save the data!!!");
				        tManuUWRiskSaveUI.submitData(tVData,"");
				      } catch(Exception ex) {
				        Content = "保存失败，原因是:" + ex.toString();
				        FlagStr = "Fail";
				      }
				
				    if (!FlagStr.equals("Fail")) {
				        tError = tManuUWRiskSaveUI.mErrors;
				        if (!tError.needDealError()) {
				            Content = " 保存成功! ";
				            FlagStr = "Succ";
				          } else {
				            Content = " 保存失败，原因是:" + tError.getFirstError();
				            FlagStr = "Fail";
				          }
				      }
  			}
  	}
  	System.out.println("开始处理正常承保");
    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();
    

    String tPolNo=request.getParameter("PolNo");
    String tUWIdea=request.getParameter("UWIdea");

    String tContNo=request.getParameter("ContNo");
    String tinputExplain = request.getParameter("inputExplain");



  //String tAddFeeValue = request.getParameter("AddFeeValue");
  //String tAddFeeRate = request.getParameter("AddFeeRate");
  //String tAddFeeStartDate = request.getParameter("AddFeeStartDate");
  //String tAddFeeEndDate = request.getParameter("AddFeeEndDate");
  //延期天数，延期日期，延期描述
		String tinputDelayDays = request.getParameter("inputDelayDays");
		System.out.println("tinputDelayDays : "+tinputDelayDays);
		String tinputDelayDate = request.getParameter("inputDelayDate");
		String tinputDelayDescribe = request.getParameter("inputDelayDescribe");
		System.out.println("tinputDelayDescribe : "+tinputDelayDescribe);
    String tPassFlag=request.getParameter("uwstate");
    String tSugUWFlag = request.getParameter("SugUWFlag");
    String tLoadFlag = request.getParameter("LoadFlag");
    String tAddFeeType = request.getParameter("AddFeeType");
    
    System.out.println("===============------"+tLoadFlag);
    tLCUWMasterSchema.setPolNo(tPolNo);
    tLCUWMasterSchema.setProposalNo(tPolNo);
    tLCUWMasterSchema.setPassFlag(tPassFlag);
    tLCUWMasterSchema.setUWIdea(tUWIdea);
    tLCUWMasterSchema.setPostponeDay(tinputDelayDays);
    tLCUWMasterSchema.setPostponeDate(tinputDelayDate);
    tLCUWMasterSchema.setPostponeDescribe(tinputDelayDescribe);
    tLCUWMasterSchema.setUWIdea(tUWIdea);
    tLCUWMasterSchema.setSugPassFlag(tSugUWFlag);
//    tLCNotePadSchema.setNotePadcont(""); 
    System.out.println("YYYY"+request.getParameter("CancelReason"));
		tLCUWMasterSchema.setCustomerReply(request.getParameter("CancelReason"));
	tLCUWMasterSchema.setRejectReason(request.getParameter("RejectReason"));
    TransferData mTransferData = new TransferData();
    //如果是加费的话，要区别健康加费与职业加费
    //核保结论取消加费，将加费作为一个标志进行处理，
    //核保结论统一为正常通过，变更承保，拒保
    
    //免责信息
    LCSpecSet tLCSpecSet = new LCSpecSet();
    String tSpecCode[] = request.getParameterValues("SpecGrid1");
		String tSpecInfo[] = request.getParameterValues("SpecGrid2");            //告知版别
		String tSpecStartDate[] = request.getParameterValues("SpecGrid3");           //告知编码
		String tSpecEndDate[] = request.getParameterValues("SpecGrid4");           //告知编码
		String tSerialNo[] = request.getParameterValues("SpecGrid5");
		String tSpecNo[] = request.getParameterValues("SpecGridNo");
    //封装加费信息
    LCPremSet tLCPremSet = new LCPremSet();
    String tAddFeeValue[] = request.getParameterValues("AddFeeGrid1");
		String tAddFeeRate[] = request.getParameterValues("AddFeeGrid2");            //告知版别
		String tPayStartDate[] = request.getParameterValues("AddFeeGrid3");           //告知编码
		String tPayEndDate[] = request.getParameterValues("AddFeeGrid4");           //告知编码
		String tNo[] = request.getParameterValues("AddFeeGridNo");
		
        //评点加费信息
    //LCUWDiseaseCheckInfoSet tLCUWDiseaseCheckInfoSet=new LCUWDiseaseCheckInfoSet(); 
    String tSuppRiskScores[] = request.getParameterValues("DiseaseCheckGrid1");
		String tSRStartDates[] = request.getParameterValues("DiseaseCheckGrid3");            //告知版别
		String tSREndDates[] = request.getParameterValues("DiseaseCheckGrid4");           //告知编码
		String tSRNos[] = request.getParameterValues("DiseaseCheckGridNo");
    
		//降档信息
		LCDutySet tLCDutySet = new LCDutySet();
		String tPayEndYear = request.getParameter("newPayEndYear");
		String tPayEndYearFlag = request.getParameter("newPayEndYearFlag");
		System.out.println("newPayEndYear"+tPayEndYear);
		System.out.println("newPayEndYearFlag"+tPayEndYearFlag);
		//变更缴费期间信息
		String tMult = request.getParameter("inputMult");
		String tinputAmnt = request.getParameter("inputAmnt");
		int n = 0;
		int nn = 0;
		if(tNo != null )
			 n = tNo.length ;
		if(tSRNos != null )
			 nn = tSRNos.length ;
			 
    if(tLoadFlag.equals("L")) {
    System.out.println("tAddFeeType"+tAddFeeType);
    	if(tAddFeeType.equals("2")){
    			if(n>0)
    			{
    				for(int i=0 ; i<n ; i++ )
    				{
	    				LCPremSchema tLCPremSchema = new LCPremSchema();
	    				tLCPremSchema.setPrem(tAddFeeValue[i]);
	    				tLCPremSchema.setRate(tAddFeeRate[i]);
	    				tLCPremSchema.setPayStartDate(tPayStartDate[i]);
	    				tLCPremSchema.setPayEndDate(tPayEndDate[i]);
	    				tLCPremSet.add(tLCPremSchema);
    				}
    			}
    			if(n == 0)
    			{
    				//如果进行加费操作而未填加费数据
    				//表示取消加费
    			}
    	}
    	else if(tAddFeeType.equals("1")){
    			if(nn>0)
    			{
    			System.out.println("nn"+nn);
    				for(int i=0 ; i<nn ; i++ )
    				{                        
                        LCPremSchema tLCPremSchema = new LCPremSchema();
                        tLCPremSchema.setSuppRiskScore(tSuppRiskScores[i]);
                        tLCPremSchema.setPayStartDate(tSRStartDates[i]);
                        tLCPremSchema.setPayEndDate(tSREndDates[i]);
                        tLCPremSet.add(tLCPremSchema);
    				}
    			}
    			if(nn == 0)
    			{
    				//如果进行加费操作而未填加费数据
    				//表示取消加费
    			}
    	}
        mTransferData.setNameAndValue("AddFeeFlag",tLoadFlag);//tAddFeeType
        System.out.println("AddFeeFlag"+tLoadFlag);
        mTransferData.setNameAndValue("AddFeeType",tAddFeeType);
        System.out.println("AddFeeType"+tAddFeeType);
        
      } 
      //封装免责信息
      else if(tLoadFlag.equals("E")) {
      	int m = 0;
				if(tSpecNo != null )
					 m = tSpecNo.length ;
        for(int i=0 ; i<m ; i++ )
    		{
	    		LCSpecSchema tLCSpecSchema = new LCSpecSchema();
	    		tLCSpecSchema.setSpecCode(tSpecCode[i]);
	    		tLCSpecSchema.setSpecContent(tSpecInfo[i]);
	    		tLCSpecSchema.setSerialNo(tSerialNo[i]);
	    		tLCSpecSchema.setSpecStartDate(tSpecStartDate[i]);
	    	 	tLCSpecSchema.setSpecEndDate(tSpecEndDate[i]);
	    		tLCSpecSet.add(tLCSpecSchema);
    		}
    		mTransferData.setNameAndValue("SpecFlag",tLoadFlag);
    		mTransferData.setNameAndValue("SpecReason",tUWIdea);
      }
      else if(tLoadFlag.equals("M")||tLoadFlag.equals("A")) {
				LCDutySchema tLCDutySchema = new LCDutySchema();
				tLCDutySchema.setMult(tMult);
				tLCDutySchema.setAmnt(tinputAmnt);
				tLCDutySchema.setPolNo(tPolNo);
				tLCDutySet.add(tLCDutySchema);
    		mTransferData.setNameAndValue("LoadFlag",tLoadFlag);
      }
      else if(tLoadFlag.equals("C")) {//如果变更缴费期间
				LCDutySchema tLCDutySchema = new LCDutySchema();
				tLCDutySchema.setPayEndYear(tPayEndYear);
				tLCDutySchema.setPayEndYearFlag(tPayEndYearFlag);
				tLCDutySchema.setPolNo(tPolNo);
				tLCDutySet.add(tLCDutySchema);
    		mTransferData.setNameAndValue("LoadFlag",tLoadFlag);
      }
      
    // 准备传输数据 VData
    VData tVData = new VData();
    FlagStr="";

    tVData.add(tG);
    tVData.add(tLCUWMasterSchema);
    tVData.add(mTransferData);
    tVData.add(tLCPremSet);
    tVData.add(tLCSpecSet);
    tVData.add(tLCDutySet);
    //tVData.add(tLCUWDiseaseCheckInfoSet);
    
    
    ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
    try {
        System.out.println("this will save the data!!!");
        tManuUWRiskSaveUI.submitData(tVData,"");
      } catch(Exception ex) {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
      }

    if (!FlagStr.equals("Fail")) {
        tError = tManuUWRiskSaveUI.mErrors;
        if (!tError.needDealError()) {
            Content = " 保存成功! ";
            FlagStr = "Succ";
          } else {
            Content = " 保存失败，原因是:" + tError.getFirstError();
            FlagStr = "Fail";
          }
      }
  } else if(Flag.equals("Insured")) {

    LCIndUWMasterSchema tLCIndUWMasterSchema = new LCIndUWMasterSchema();

    String tContNo=request.getParameter("ContNo");
    String tInsuredNo = request.getParameter("InsuredNo");
    String tUWIndIdea=request.getParameter("UWIndIdea");
    String tIndPassFlag=request.getParameter("uwindstate");
    String tSugUWIndIdea=request.getParameter("SugIndUWIdea");
    String tSugIndPassFlag=request.getParameter("SugIndUWFlag");

    tLCIndUWMasterSchema.setContNo(tContNo);
    tLCIndUWMasterSchema.setPassFlag(tIndPassFlag);
    tLCIndUWMasterSchema.setUWIdea(tUWIndIdea);
    tLCIndUWMasterSchema.setSugPassFlag(tSugIndPassFlag);
    tLCIndUWMasterSchema.setSugUWIdea(tSugUWIndIdea);

    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ContNo",tContNo);
    tTransferData.setNameAndValue("InsuredNo",tInsuredNo);
    tTransferData.setNameAndValue("LCIndUWMasterSchema",tLCIndUWMasterSchema);
    // 准备传输数据 VData
    VData tVData = new VData();
    FlagStr="";

    tVData.add(tG);
    tVData.add(tTransferData);

    LCInsuredUWUI tLCInsuredUWUI = new LCInsuredUWUI();

    try {
        System.out.println("this will save the data!!!");
        tLCInsuredUWUI.submitData(tVData,"submit");
      } catch(Exception ex) {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
      }

    if (!FlagStr.equals("Fail")) {
        tError = tLCInsuredUWUI.mErrors;
        if (!tError.needDealError()) {
            Content = " 保存成功! ";
            FlagStr = "Succ";
          } else {
            Content = " 保存失败，原因是:" + tError.getFirstError();
            FlagStr = "Fail";
          }
      }
  }
  
    String OtherSign = "";
  	ExemptionRiskBL tExemptionRiskBL = new ExemptionRiskBL();
  	// 判断保额是否需要重算
  	if(!tExemptionRiskBL.checkContAmnt(request.getParameter("ContNo"))){
  		LCPolSchema tLCPolSchema = new LCPolSchema();
		tLCPolSchema.setContNo(request.getParameter("ContNo"));
		       
		VData exemptionVD = new VData();
		exemptionVD.addElement(tG);
		exemptionVD.addElement(tLCPolSchema);
		// 豁免险处理
		if(tExemptionRiskBL.checkPrem(request.getParameter("ContNo"))){
			//判断豁免险是否存在加费
			Content = "保存成功";
			FlagStr = "Succ";
			OtherSign = "，豁免险存在加费，请删除豁免险加费并重新添加！";
		} else if(!tExemptionRiskBL.submitData(exemptionVD, "")){
			Content = "保存成功";
			FlagStr = "Succ";
			OtherSign = "，但豁免险险种重算处理失败！";
		} else {
			Content = "保存成功";
			FlagStr = "Succ";
			OtherSign = "，豁免险保额发生变化，需要重新对豁免险下核保结论！";
		}
  	}
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=OtherSign%>");
</script>
</html>
