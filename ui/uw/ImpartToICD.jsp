<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ImpartToICD.jsp
//程序功能：告知整理录入
//创建日期：2002-06-19 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./ImpartToICD.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <%@include file="ImpartToICDInit.jsp"%>
  <title> 新契约体检资料录入 </title>
  
</head>
<body  onload="initForm('<%=tContNo%>','<%=tPrtNo%>','<%=tLoadFlag%>');" >
  <form method=post name=fm target="fraSubmit" action= "./ImpartToICDSave.jsp">
    <!-- 非列表 -->
    <table>
    	<TR  class= common>
          <TD  class= title id="ff" style="display:'none'">  合同号码  </TD>          
          <INPUT  type= "hidden" class= Common name= PrtNo value= "">
      </TR> 
     </table>
     <div id=DivLcImpartemp Style="display:'none'">
          <TD  class= title>  被保人 </TD>
          <TD  class= input id="ff" style="display:''" > 
              <Input class="readonly" name=ContNo> 
          </TD>
          <TD  class=Input >
          <Input class=codeNo name=InsureNo verify="被保人|code:InsureNo" ondblClick="showCodeListEx('InsureNo',[this,InsureNoName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('InsureNo',[this,InsureNoName],[0,1],null,null,null,1);" onFocus= "easyQueryClickSingle();">
          <input class=codename name=InsureNoName readonly=true elementtype=nacessary onFocus= "easyQueryClickSingle();easyQueryClick();"></TD>
          </TR>
     </div>
 
<DIV id=DivLCImpart STYLE="display:''">
<!-- 告知信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart2);">
</td>
<td class= titleImg>
被保险人告知明细信息
</td>
</tr>
</table>

<div  id= "divLCImpart2" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartDetailGrid" >
</span>
</td>
</tr>
</table>
</div>
</DIV>


<%@include file="ICDDisDesbGrid.jsp"%>

<!--
    <table>
    	<tr>
        	<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
    		<td class= titleImg>   疾病结果</td>                            
    	</tr>	
    </table>
    <Div  id= "divUWDis" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanDisDesbGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
      </div>
-->
    <span  id= "divUWDis1" style= "display: ''">  
      <INPUT type= "button" name= "sure" value="保  存" class=CssButton onclick="submitForm()">	
		</span>
      <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="parent.close();">		

    <!--读取信息-->
  </form>
  <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>