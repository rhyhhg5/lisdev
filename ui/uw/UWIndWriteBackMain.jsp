<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：UWIndWirteBackMain.jsp
//程序功能：人工核保通过之后,如果险种核保结论为变更承保将会自动发送核保通知书,及待客会回复
//创建日期：2006-5-27 16:40
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="UWIndWriteBackMain.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UWIndWriteBackMainInit.jsp"%>
  <title>核保通知书客户回复</title>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./UWIndWriteBackMainSave.jsp" method=post name=fm target="fraSubmit">
		<Div  id= "queryPage" style= "display: ''" style="float: right">
	  	<table class=common>
	  		<tr class=common>
	  			<TD  class= title>
	          印刷号
	        </TD>
	        <TD  class= input_Acc>
	          <Input class=common_title name="PrtNo" verify="印刷号码|len=11&int">
	        </TD>
	        <TD  class= title>
	          投保人姓名
	        </TD>
	        <TD  class= input>
	          <Input class=common_title name="AppntName" verify="投保人|len<=20">
	        </TD>
	        <TD  class= title>
	          被保险人姓名
	        </TD>
	        <TD  class= input>
	          <Input class=common_title name="InsuredName" verify="被保险人|len<=20">
	        </TD>
	        <TD  class= title>
	          业务员代码
	        </TD>
	        <TD  class= input>
	          <Input class=common_title name="AgentName" verify="业务员代码|len<=40&int">
	        </TD>
	  		</tr>
	  		<tr class=common>
	  			<TD  class= title>
	          下发日期
	        </TD>
	        <TD  class= input>
	          <Input class=coolDatePicker3 name="SendDate" verify="申请日期|date">
	        </TD>
	        <TD  class= title>
	          回销日期
	        </TD>
	        <TD  class= input>
	          <Input class=coolDatePicker3 name="ReceiveDate" verify="申请日期|date">
	        </TD>
	        <TD  class= title>
	          管理机构
	        </TD>
	        <TD  class= input>
	          <Input class=codeNo readonly=true name=ManageCom verify="管理机构|code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true >
	        </TD>
	        <TD  class= title>
	          客户回复状态
	        </TD>
	        <TD  class= input>
	          <Input class=codeNo name="Receive" verify="客户回复状态|code:Receive" CodeData="0^0|未回复^1|已回复" ondblclick="showCodeListEx('Receive',[this,ReceiveName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Receive',[this,ReceiveName],[0,1],null,null,null,1);"><input class=codename name=ReceiveName readonly=true >
	        </TD>
	  		</tr>
	  		<td class=common>    		
    		 	<INPUT class=cssButton id="riskbutton" VALUE="查  询" TYPE=button onClick="easyQueryClick();">
    		</td>
	  	</table>
	  </div>
	  <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 客户回复信息：
    		</td>
    	</tr>  	
    </table>
	  <Div  id= "divLCPol1" align=center style= "display: ''" align = left>
      <tr  class=common>
      	<td text-align: left colSpan=1 >
	  			<span id="spanCustomerGrid" >
	  			</span> 
  			</td>
  	  </tr>
  	</Div>
  	<Div id= "divPage" align=center style= "display: 'none' ">
	    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	  </Div>
	  <INPUT class=cssButton id="riskbutton" VALUE="开始录入" TYPE=button onClick="intoCont();">
	</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
