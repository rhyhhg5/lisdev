//程序名称：GrpUWInsuCont.js
//程序功能：团体既往理陪函数
//创建日期：2005-4-26
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容

/*********************************************************************
 *  该文件中包含客户端需要处理的函数和事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

/*********************************************************************
 *  查询承保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQuery()
{
	// 初始化表格
	initAskGrid();
	initAskGrid1();
	// 书写SQL语句

    var strSQL = "select distinct A.GrpContNo,";             // 询价代码
    strSQL    += "       A.PolApplyDate,";          // 询价时间
    strSQL    += "       A.Peoples,";               // 人数
    strSQL    += "       B.RiskCode,";              // 险种
    strSQL    += "       e.DutyCode,";              // 责任项目
    strSQL    += "       0,";                       // 询价费率
    strSQL    += "       A.UWFlag,";                // 询价核保结论
    strSQL    += "       0,";                       // 核定费率
    strSQL    += "       f.RiskPrem,";                  // 核定保费合计
    strSQL    += "       D.State";                  // 询价跟踪结果
    strSQL    += "  from LCGrpCont A,";             // 集体保单表
    strSQL    += "       LCGrpPol B,";              // 集体险种表
    strSQL    += "       LMRiskDutyFactor C,";      // 险种责任计算要素表
    strSQL    += "       LCAskTrackMain D, ";         // 业务跟踪主表
    strSQL    += "       LCContPlanDutyParam e, ";         // 业务跟踪主表
    strSQL    += "       LCContPlanRisk f ";         // 业务跟踪主表
    strSQL    += " where A.Appflag='8'";
    strSQL    += "   and A.GrpContNo = B.GrpContNo";
    strSQL    += "   and B.RiskCode = C.RiskCode";
    strSQL    += "   and D.GrpContNo = A.GrpContNo";
    strSQL    += "   and e.GrpContNo = A.GrpContNo";
    strSQL    += "   and e.RiskCode = B.RiskCode";
    strSQL    += "   and f.GrpContNo = B.GrpContNo";
    strSQL    += "   and f.RiskCode = B.RiskCode";
    strSQL    += "   and A.AppntNo = '"+mAppntNo+"'";

	//查询SQL，返回结果字符串
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

	//判断是否查询成功
    if (!turnPage.strQueryResult) {
      alert("没有既往询价信息！");
      return "";
    }

    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = AskGrid;

    //保存SQL语句
    turnPage.strQuerySql = strSQL;

    //设置查询起始位置
    turnPage.pageIndex = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

/*********************************************************************
 *  取的客户团体的名称
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryI()
{
	// 书写SQL语句
	var strSQL = "select GrpName";
	var arrRs;
	strSQL    += "  from LDGrp";
	strSQL    += " where CustomerNo='"+mAppntNo+"'";

	fm.all("GrpNo").value=mAppntNo;
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	
	//查询成功设置画面上的数据
	if(turnPage.strQueryResult)
	{
	  arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
	  fm.all("GrpName").value=arrRs[0][0];
	}
	
	strSQL  = "select ProposalGrpContNo";
	strSQL += "  from LCGrpCont";
	strSQL += " where GrpContNo = '" + mGrpContNo + "'";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	
	//查询成功设置画面上的数据
	if(turnPage.strQueryResult)
	{
	  arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
	  fm.all("GrpPolNo").value=arrRs[0][0];
	}
}

//返回
function GoBack()
{
	top.close();
}