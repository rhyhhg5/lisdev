//程序名称：ManuUWAll.js
//程序功能：个人人工核保
//创建日期：2005-01-24 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();       
var turnPage3 = new turnPageClass();       
var turnPage4 = new turnPageClass();
var turnPage5 = new turnPageClass(); 
var turnPage6 = new turnPageClass(); //理赔二次核保   
var turnPage7 = new turnPageClass(); //理赔续保待核受理   
var turnPage8 = new turnPageClass();//理赔续保待核待回复
var turnPage9 = new turnPageClass();//既往理赔续保待核信息   
var turnPage10 = new turnPageClass();//既往理赔二核信息   
var turnConfirmPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约人工核保的EasyQuery
 *  描述:查询显示对象是合同保单.显示条件:合同未进行人工核保，或状态为待核保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
    if(!verifyInput2())
    return false;
    initPolGrid();
    initUpReportPolGrid();
    var tSql = "select count(1) from lwmission t where 1=1 "
        +" and t.activityid in ('0000001100','0000001180') "
        +" and (t.defaultoperator is null or trim(t.defaultoperator)='')"
        ;
    var arrSelected = easyExecSql(tSql);
    fm.UWNo.value = arrSelected[0][0];
    // 书写SQL语句
    // A 印刷号
    // B 合同投保单号
    // C 投保人
    // D 状态
    // O 核保类型
    // E 工作流任务号
    // F 工作流子任务号
    // G 工作流活动Id
    // H 来源
    // I 申请日期
    // J 生效日期
    // K 接受日期
    // L 险种个数
    // N 排序条件
    // Z 销售渠道
    var uwUser = "";
    if(operator!="group"){
        uwUser = " and (t.defaultoperator ='" +operator+ "' or t.defaultoperator in (select usercode from lduwuser where agentusercode ='"+operator+"' and uwtype='01'))";
    }
    var tagentcode="";
	if(fm.AgentName.value!=""){
		var magentcode=easyExecSql(" select getAgentCode('"+fm.AgentName.value+"') from dual");
		tagentcode=" and t.missionprop4='"+magentcode+"'";
	} 
    var strSql = "select A,B,C,case when D='1' then '新单待核保' when D='2' then '待回复' when D='3' then '客户已回复' end,case when H='0' then '上报' when H='1' then '审批未通过' when H='2' then '抽检'  else '新契约' end,E,F,G,case when H='0' then R when H='1' then R when H='2' then R  else '新单' end,P,J,K,L,Q,Y,Z,W "
        //+ " select A,B,C,case when D='1' then '新单待核保'  when D='2' then '待回复' when D='3' then '客户已回复' end,case when O='0000001100' then '新契约' end,E,F,G,case when H='0' then R when H='1' then '审批未通过' when H='2' then '抽检' else '新单' end,P,J,K,L,Q,Y "
        + "from ("
        + "  select t.missionprop1 A,t.missionprop2 B,a.AppntName C,t.activitystatus D,"
        + "    t.missionid E,t.submissionid F,t.activityid G,t.missionprop12 H,a.PolApplyDate I,"
        + "    a.CValiDate J,a.ApproveDate K,(select count(1) from lcpol where lcpol.contno=a.contno ) L,"
        + "    t.modifydate N,"
        + "    t.activityid O, "
        + "    t.MissionProp19 P, "
        + "    t.MissionProp18 Q ,(select min(downuwcode) from lcuwsendtrace where otherno=a.contno and downuwcode<>upusercode and sendtype in ('1','2','6') and upusercode=t.missionprop11) R,a.managecom Y,"
        + "    CodeName('lcsalechnl', SaleChnl) Z,(case when (select count(1) from LJSPay where OtherNo = a.prtno and BankOnTheWayFlag = '1') > 0 then '是' else '否' end) W"
        + "  from lwmission t,lccont a where 1=1  and t.activityid='0000001100'  and  t.activitystatus in ('1','3')"
        + "    and t.missionprop2=a.proposalcontno "
        + uwUser
        + getWherePart("t.missionprop1", "PrtNo")
        + getWherePart("a.AppntName", "AppntName")
        + getWherePart("t.MissionProp19", "ApplyDate")
        + getWherePart("a.CValiDate", "CValiDate")
        + getWherePart("a.SaleChnl", "SaleChnl")
        + getWherePart("a.ManageCom", "ManageCom","like",null,"%")
        + getWherePart("t.activitystatus", "Receive")
        + getInsuredWherePart("t.missionprop2", "InsuredName")
        + tagentcode
        + ") as X "
        + "union "
        //+ " select A,B,C,case when D='1' then '新单待核保' when D='2' then '待回复' when D='3' then '客户已回复' end,case when O='0000001100' then '新契约' end,E,F,G,case when H='0' then R when H='1' then '审批未通过' when H='2' then '抽检'  else '新单' end,P,J,K,L,Q,Y "
        + "select A,B,C,case when D='1' then '新单待核保' when D='2' then '待回复' when D='3' then '客户已回复' end,case when H='0' then '上报' when H='1' then '审批未通过' when H='2' then '抽检'  else '新契约' end,E,F,G,case when H='0' then R when H='1' then R when H='2' then R  else '新单' end,P,J,K,L,Q,Y,Z,W "
        + "from ("
        + "  select t.missionprop1 A,t.missionprop2 B,a.AppntName C,t.activitystatus D,"
        + "    t.missionid E,t.submissionid F,t.activityid G,t.missionprop12 H,a.PolApplyDate I,"
        + "    a.CValiDate J,a.ApproveDate K,(select count(1) from lcpol where lcpol.contno=a.contno ) L,"
        + "    t.modifydate N,"
        + "    t.activityid O, "
        + "    t.MissionProp19 P, "
        + "    t.MissionProp18 Q , (select min(downuwcode) from lcuwsendtrace where otherno=a.contno and downuwcode<>upusercode and sendtype in ('1','2','6') and upusercode=t.missionprop11) R,a.managecom Y,"
        + "    CodeName('lcsalechnl', SaleChnl) Z,(case when (select count(1) from LJSPay where OtherNo = a.prtno and BankOnTheWayFlag = '1') > 0 then '是' else '否' end) W"
        + "  from lwmission t,lccont a where 1=1  and t.activityid='0000001160'  and t.activitystatus in ('1','3')"
        + "    and t.missionprop2=a.proposalcontno "
        + uwUser
        + getWherePart("t.missionprop1", "PrtNo")
        + getWherePart("a.AppntName", "AppntName")
        + getWherePart("t.MissionProp19", "ApplyDate")
        + getWherePart("a.CValiDate", "CValiDate")
        + getWherePart("a.SaleChnl", "SaleChnl")
        + getWherePart("a.ManageCom", "ManageCom","like",null,"%")
        + getWherePart("t.activitystatus", "Receive")
        + getInsuredWherePart("t.missionprop2", "InsuredName")
        + tagentcode
        + ") as X group by D,H,A,B,C,O,E,F,G,P,J,K,L,Q,Y,R,Z,W order by K asc";//修改排序方式为“送核日期”（K）（原来为“申请日期”（P））
    turnPage.queryModal(strSql,PolGrid,0,0,1);
    var strSql_1 = "select A,B,C,case when D='1' then '新单待核保' when D='2' then '待回复' when D='3' then '客户已回复' end,case when H='0' then '上报' when H='1' then '审批未通过' when H='2' then '抽检'  else '新契约' end,E,F,G,case when H='0' then R when H='1' then R when H='2' then R  else '新单' end,I,J,K,L,N,Y,Z "
        //+ " select A,B,C,case when D='1' then '新单待核保'  when D='2' then '待回复' when D='3' then '客户已回复' end,case when O='0000001100' then '新契约' end,E,F,G,case when H='0' then R when H='1' then '审批未通过' when H='2' then '抽检'  else '新单' end,I,J,K,L,N,Y "
        + "from ("
        + "  select t.missionprop1 A,t.missionprop2 B,a.AppntName C,t.activitystatus D,"
        + "    t.missionid E,t.submissionid F,t.activityid G,t.missionprop12 H,a.PolApplyDate I,"
        + "    a.CValiDate J,a.ApproveDate K,(select count(1) from lcpol where lcpol.contno=a.contno ) L,"
        + "    t.modifydate N,"
        + "    t.activityid O,UWD(a.contno,t.missionprop11) R,a.managecom Y,CodeName('lcsalechnl', SaleChnl) Z "
        + "  from lwmission t,lccont a where 1=1  and t.activityid='0000001100'  and  t.activitystatus='2'"
        + "    and t.missionprop2=a.proposalcontno "
        + uwUser
        + getWherePart("t.missionprop1", "PrtNo")
        + getWherePart("a.AppntName", "AppntName")
        + getWherePart("a.PolApplyDate", "ApplyDate")
        + getWherePart("a.CValiDate", "CValiDate")
        + getWherePart("a.SaleChnl", "SaleChnl")
        + getWherePart("a.ManageCom", "ManageCom","like")
        + getWherePart("t.activitystatus", "Receive")
        + getInsuredWherePart("t.missionprop2", "InsuredName")
        + tagentcode
        + ") as X "
        + "union "
        + "select A,B,C,case when D='1' then '新单待核保' when D='2' then '待回复' when D='3' then '客户已回复' end,case when H='0' then '上报' when H='1' then '审批未通过' when H='2' then '抽检'  else '新契约' end,E,F,G,case when H='0' then R when H='1' then R when H='2' then R  else '新单' end,I,J,K,L,N,Y,Z "
        //+ " select A,B,C,case when D='1' then '新单待核保' when D='2' then '待回复' when D='3' then '客户已回复' end,case when O='0000001100' then '新契约' end,E,F,G,case when H='0' then R when H='1' then '审批未通过' when H='2' then '抽检'   else '新单' end,I,J,K,L,N,Y "
        + "from ("
        + "  select t.missionprop1 A,t.missionprop2 B,a.AppntName C,t.activitystatus D,"
        + "    t.missionid E,t.submissionid F,t.activityid G,t.missionprop12 H,a.PolApplyDate I,"
        + "    a.CValiDate J,a.ApproveDate K,(select count(1) from lcpol where lcpol.contno=a.contno ) L,"
        + "    t.modifydate N,"
        + "    t.activityid O,UWD(a.contno,t.missionprop11) R,a.managecom Y,CodeName('lcsalechnl', SaleChnl) Z"
        + "  from lwmission t,lccont a where 1=1  and t.activityid='0000001160'  and t.activitystatus='2'"
        + "    and t.missionprop2=a.proposalcontno and t.activitystatus in ('2') "
        + uwUser
        + getWherePart("t.missionprop1", "PrtNo")
        + getWherePart("a.AppntName", "AppntName")
        + getWherePart("a.PolApplyDate", "ApplyDate")
        + getWherePart("a.CValiDate", "CValiDate")
        + getWherePart("a.SaleChnl", "SaleChnl")
        + getWherePart("a.ManageCom", "ManageCom","like")
        + getWherePart("t.activitystatus", "Receive")
        + getInsuredWherePart("t.missionprop2", "InsuredName")
        + tagentcode+
        ") as X  group by D,H,A,B,C,O,E,F,G,I,J,K,L,N,Y,R,Z order by I desc";
    turnPage1.queryModal(strSql_1,UpReportPolGrid);
    return true;
}

/*********************************************************************
 *  执行新契约人工核保的EasyQueryAddClick
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryAddClick()
{
	var tSel = PolGrid.getSelNo();
	var activityid = PolGrid.getRowColData(tSel - 1,8);
	var ContNo = PolGrid.getRowColData(tSel - 1,2);
	var PrtNo = PolGrid.getRowColData(tSel - 1,1);
	var MissionID = PolGrid.getRowColData(tSel - 1,6);
	var SubMissionID = PolGrid.getRowColData(tSel - 1,7);
	if(activityid == "0000001100" || activityid == "0000001160")
	{
		window.location="./UWManuInputMain.jsp?ContNo="+ContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PrtNo="+PrtNo+"&ActivityID="+activityid;
	}
	if(activityid == "0000002004")
	{
		window.location="./GroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
	}
	if(activityid == "0000006004")
	{
		window.location="../askapp/AskGroupUW.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+activityid; 
	}
}

/*********************************************************************
 *  申请核保
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyUW()
{
	if(!verifyInput2())
	return false;
	fm.PrtNo.value="";
	var cApplyNo = fm.ApplyNo.value;
	
	if(cApplyNo >30)
	{
		alert("每次申请最大数为30件");
		return;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	//fm.ApplyNo.value ="5";     //每次申请为5条
	fm.action = "./ManuUWAllChk.jsp";
  fm.submit(); //提交
}

/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	window.focus();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  

  if (FlagStr == "Fail" )
  {                 
    alert(content);
  }
  else
  { 
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");     
    //执行下一步操作
  }
  easyQueryClick();

  queryEdorList();//查询待核保的保全信息
  queryLPXBList();//查询理赔续保待核受理信息
  queryLPNoReplyEdorList();//查询理赔续保待核待回复信息
  querySecondLPGrid();//查询理赔二核信息
}

function dealUpReport()
{
	var strSql = "select t.missionprop1,t.missionprop2,a.AppntName,"
								+"case t.activitystatus "
		 						+"when '1' then '未人工核保'  "
		 						+"when '3' then '核保已回复' "
		 						+"when '2' then '核保未回复' end,"
		 						+"case t.activityid when '0000001160' "
		 						+"then '新契约' end,t.missionid,t.submissionid,t.activityid ,"
		 						+" case t.missionprop12 "
		 						+" when '0' then '上报' "
		 						+" when '1' then '审批未通过' "
		 						+" when '2' then '抽检' " 
		 						+" else '新单' end ,"
		 						+" a.PolApplyDate ,"
		 						+" a.CValiDate ,"
		 						+" a.ReceiveDate ,"
		 						+" (select count(1) from lcpol where lcpol.contno=a.contno )" 
		 						+"from lwmission t,lccont a where 1=1 "
								+ " and t.activityid in ('0000001160')"
								+ " and t.defaultoperator ='" +operator+ "' "
								+ " and t.missionprop2=a.contno"
								+ " order by t.modifydate asc,t.modifytime asc ";
turnPage.queryModal(strSql, UpReportPolGrid);

}

function easyQueryAddClick2()
{
	var tSel = UpReportPolGrid.getSelNo();
	var activityid = UpReportPolGrid.getRowColData(tSel - 1,8);
	var ContNo = UpReportPolGrid.getRowColData(tSel - 1,2);
	var PrtNo = UpReportPolGrid.getRowColData(tSel - 1,1);
	var MissionID = UpReportPolGrid.getRowColData(tSel - 1,6);
	var SubMissionID = UpReportPolGrid.getRowColData(tSel - 1,7);

	if(activityid == "0000001100" || activityid == "0000001160")
	{
		window.location="./UWManuInputMain.jsp?ContNo="+ContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PrtNo="+PrtNo+"&ActivityID="+activityid;
	}
	if(activityid == "0000002004")
	{
		window.location="./GroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
	}
	if(activityid == "0000006004")
	{
		window.location="../askapp/AskGroupUW.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+activityid; 
	}
}

function getInsuredWherePart(fieldname,fieldvalue)
{
	var SqlValue = eval("fm." + trim(fieldvalue) + ".value");
	if(SqlValue == "")
	{
		return SqlValue;
	}
	var strSql = " and "+fieldname + " in (select contno from lcinsured where name ='"+SqlValue+"') ";
	return strSql;	
}
function getAgentWherePart(fieldname,fieldvalue)
{
	var SqlValue = eval("fm." + trim(fieldvalue) + ".value");
	if(SqlValue == "")
	{
		return SqlValue;
	}
	var strSql = " and "+fieldname + " in (select lccont.contno from lccont,laagent where lccont.agentcode=laagent.agentcode and laagent.name ='"+SqlValue+"') ";
	return strSql;	
}
//查询既往核保信息
function queryHistory()
{
	if(!verifyInput2())
	return false;
	var strSql = " select a.PrtNo,a.ProposalContNo,a.AppntName,a.uwflag,"
								+"case a.appflag when '1' then '已承保' when '0' then '未承保' end,a.ManageCom,"
								+" a.PolApplyDate,a.CValiDate,a.ReceiveDate,"
								+" count(c.polno) "
								+" from lccont a,LCCUWMaster b ,lcpol c"
								+" where 1=1 "
								+" and a.contno=b.contno"
								+" and b.PassFlag is not null "
								+" and a.contno=c.contno "
								+" and a.conttype='1' "
								+ getWherePart("a.PrtNo", "PrtNo")
								+ getWherePart("a.AppntName", "AppntName")
								+ getWherePart("a.PolApplyDate", "ApplyDate")
								+ getWherePart("a.CValiDate", "CValiDate")
								+ getWherePart("a.ManageCom", "ManageCom","like")
								+ getWherePart("a.insuredname", "InsuredName")
								+ getWherePart("a.agentcode", "AgentName")
								+" group by a.PrtNo,a.ProposalContNo,a.AppntName,a.uwflag,a.appflag,"
								+" a.ManageCom, a.PolApplyDate,a.CValiDate,a.ReceiveDate"
								;
			// 加入退保保单
	strSql = strSql + " union all select a.PrtNo,a.ProposalContNo,a.AppntName,a.uwflag,"
			+ " '退保',a.ManageCom,"
			+ " a.PolApplyDate,a.CValiDate,a.ReceiveDate,"
			+ " count(c.polno) "
			+ " from lbcont a,LCCUWMaster b ,lbpol c"
			+ " where 1=1 "
			+ " and a.contno=b.contno"
			+ " and b.PassFlag is not null "
			+ " and a.contno=c.contno "
			+ " and a.conttype='1' "
			+ getWherePart("a.PrtNo", "PrtNo")
			+ getWherePart("a.AppntName", "AppntName")
			+ getWherePart("a.PolApplyDate", "ApplyDate")
			+ getWherePart("a.CValiDate", "CValiDate")
			+ getWherePart("a.ManageCom", "ManageCom", "like")
			+ getWherePart("a.insuredname", "InsuredName")
			+ getWherePart("a.agentcode", "AgentName")
			+ " group by a.PrtNo,a.ProposalContNo,a.AppntName,a.uwflag,a.appflag,"
			+ " a.ManageCom, a.PolApplyDate,a.CValiDate,a.ReceiveDate";
			
	turnPage2.queryModal(strSql,HistoryUwGrid);
}
//进入报单信息明细
function easyQueryHistory()
{

	var tSel = HistoryUwGrid.getSelNo();
	var ContNo = HistoryUwGrid.getRowColData(tSel - 1,2);
	var PrtNo = HistoryUwGrid.getRowColData(tSel - 1,1);
	window.location="./UWHistoryMain.jsp?ContNo="+ContNo+"&PrtNo="+PrtNo+"&LoadFlag=1";
}

//查询待核保的保全信息
function queryNoReplyEdorList()
{
	var sql = "select MissionId, SubMissionId, Missionprop1, Missionprop2, Missionprop3, " +
	          "       Missionprop4, Missionprop5, Missionprop6, Missionprop7, Missionprop8, Missionprop9,(case Activitystatus when '2' then '待回复' else '待处理' end )  " +
	          "from LWMission " +
	          "where ProcessId = '0000000003' " +
	          "and ActivityId = '0000001180' " +
	          " and defaultoperator ='" +operator+ "' "+
	          " and Activitystatus='2' "+
	          "order by Missionprop1 desc ";
	turnPage5.pageDivName = "divEdor";
	turnPage5.queryModal(sql, NoReplyEdorGrid);
}
//查询待核保的保全信息
function queryEdorList()
{
	var sql = "select MissionId, SubMissionId, Missionprop1, Missionprop2, Missionprop3, " +
	          "       Missionprop4, Missionprop5, Missionprop6, Missionprop7, Missionprop8, Missionprop9 " +
	          "from LWMission " +
	          "where ProcessId = '0000000003' " +
	          "and ActivityId = '0000001180' " +
	          " and defaultoperator ='" +operator+ "' "+
	          " and Activitystatus='1' "+
	          "order by Missionprop1 desc ";
	turnPage3.pageDivName = "divEdor";
	turnPage3.queryModal(sql, EdorGrid);
}

//查询待核保的保全信息
function queryOldEdorList()
{
    var EdorNo=fm.EdorNo.value;
    var ShouLiRen=fm.ShouLiRen.value;
    var wherePar="";
    if(trim(EdorNo)!=null&&trim(EdorNo)!=""&&trim(EdorNo)!="null")
    {
        wherePar+= "and Missionprop1='"+EdorNo+"'";
    }
    if(trim(ShouLiRen)!=null&&trim(ShouLiRen)!=""&&trim(ShouLiRen)!="null")
    {
        wherePar+= "and Missionprop5='"+ShouLiRen+"'";
    }
	var sql = "select MissionId, SubMissionId, Missionprop1, Missionprop2, Missionprop3, " +
	          "       Missionprop4, Missionprop5, Missionprop6, Missionprop7, Missionprop8, Missionprop9, ModifyDate, defaultoperator " +
	          "from LBMission " +
	          "where ProcessId = '0000000003' " +
	          "and ActivityId = '0000001180' " +
	          wherePar+
	          "order by Missionprop1 desc ";
	turnPage4.pageDivName = "divOldEdor";
	turnPage4.queryModal(sql, OldEdorGrid);
	if(OldEdorGrid.mulLineCount == 0)
	{
	     alert("没有既往保全核保信息");
         return false;
	}
}

//进入保全人工核保界面
function openEdorUwWindow()
{
	var selNo = EdorGrid.getSelNo();
	var missionId = EdorGrid.getRowColData(selNo - 1, 1);
	var subMissionId = EdorGrid.getRowColData(selNo - 1, 2);
	var edorNo = EdorGrid.getRowColData(selNo - 1, 3);
	var appntNo = EdorGrid.getRowColData(selNo - 1, 5);
	var url = "../bq/PEdorUWManuMain.jsp" +
	          "?MissionId=" + missionId + 
	          "&SubMissionId=" + subMissionId +
	          "&EdorNo=" + edorNo +
	          "&AppntNo=" + appntNo +
	          "&ActivityId=0000001180";
	window.open(url);
}

//进入保全人工核保界面
function openNoReplyEdorUwWindow()
{
	var selNo = NoReplyEdorGrid.getSelNo();
	var missionId = NoReplyEdorGrid.getRowColData(selNo - 1, 1);
	var subMissionId = NoReplyEdorGrid.getRowColData(selNo - 1, 2);
	var edorNo = NoReplyEdorGrid.getRowColData(selNo - 1, 3);
	var appntNo = NoReplyEdorGrid.getRowColData(selNo - 1, 5);
	var url = "../bq/PEdorUWManuMain.jsp" +
	          "?MissionId=" + missionId + 
	          "&SubMissionId=" + subMissionId +
	          "&EdorNo=" + edorNo +
	          "&AppntNo=" + appntNo +
	          "&ActivityId=0000001180";
	window.open(url);
}

//进入保全人工核保界面
function openOldEdorUwWindow()
{
	var selNo = OldEdorGrid.getSelNo();
	var missionId = OldEdorGrid.getRowColData(selNo - 1, 1);
	var subMissionId = OldEdorGrid.getRowColData(selNo - 1, 2);
	var edorNo = OldEdorGrid.getRowColData(selNo - 1, 3);
	var appntNo = OldEdorGrid.getRowColData(selNo - 1, 5);
	var url = "../bq/PEdorUWManuMain.jsp" +
	          "?MissionId=" + missionId + 
	          "&SubMissionId=" + subMissionId +
	          "&EdorNo=" + edorNo +
	          "&AppntNo=" + appntNo +
	          "&ActivityId=0000001180";
	window.open(url);
}

//进入理赔续保人工核保界面（待处理入口）-与保全二核是一个页面
function openLPXBUwWindow()
{
	var selNo = LPXBGrid.getSelNo();
	var missionId = LPXBGrid.getRowColData(selNo - 1, 1);
	var subMissionId = LPXBGrid.getRowColData(selNo - 1, 2);
	var caseNo = LPXBGrid.getRowColData(selNo - 1, 3);
	var appntNo = LPXBGrid.getRowColData(selNo - 1, 5);
	var edorno = LPXBGrid.getRowColData(selNo - 1, 12);

	var url = "../case/LPPEdorUWManuMain.jsp" +
	          "?MissionId=" + missionId + 
	          "&SubMissionId=" + subMissionId +
	          "&CaseNo=" + caseNo +
	          "&EdorNo=" + edorno +
	          "&AppntNo=" + appntNo +
	          "&ActivityId=0000001181";
	window.open(url);
}

//进入理赔续保人工核保界面（待回复入口）-与保全二核是一个页面
function openLPNoReplyEdorWindow()
{
	var selNo = LPNoReplyEdorGrid.getSelNo();
	var missionId = LPNoReplyEdorGrid.getRowColData(selNo - 1, 1);
	var subMissionId = LPNoReplyEdorGrid.getRowColData(selNo - 1, 2);
	var caseNo = LPNoReplyEdorGrid.getRowColData(selNo - 1, 3);
	var appntNo = LPNoReplyEdorGrid.getRowColData(selNo - 1, 5);
	var edorno = LPNoReplyEdorGrid.getRowColData(selNo - 1, 12);

	var url = "../case/LPPEdorUWManuMain.jsp" +
	          "?MissionId=" + missionId + 
	          "&SubMissionId=" + subMissionId +
	          "&CaseNo=" + caseNo +
	          "&EdorNo=" + edorno +
	          "&AppntNo=" + appntNo +
	          "&ActivityId=0000001181";
	window.open(url);
}

//进入理赔续保人工核保界面（既往信息查询入口）-与保全二核是一个页面
function openLPOldEdorWindow()
{
	var selNo = LPOldEdorGrid.getSelNo();
	var missionId = LPOldEdorGrid.getRowColData(selNo - 1, 1);
	var subMissionId = LPOldEdorGrid.getRowColData(selNo - 1, 2);
	var edorNo = LPOldEdorGrid.getRowColData(selNo - 1, 12);
	var appntNo = LPOldEdorGrid.getRowColData(selNo - 1, 5);
	var url = "../case/LPPEdorUWManuMain.jsp" +
	          "?MissionId=" + missionId + 
	          "&SubMissionId=" + subMissionId +
	          "&EdorNo=" + edorNo +
	          "&AppntNo=" + appntNo +
	          "&ActivityId=0000001181";
	window.open(url);
}

//进入理赔二核人工核保页面
function openSecondLPUwWindow(){
	var selNo = SecondLPGrid.getSelNo();
	var missionId = SecondLPGrid.getRowColData(selNo - 1, 1);
	var subMissionId = SecondLPGrid.getRowColData(selNo - 1, 2);
	var caseNo = SecondLPGrid.getRowColData(selNo - 1, 3);
	var edorNo = SecondLPGrid.getRowColData(selNo - 1, 11);	
	var insuredno = SecondLPGrid.getRowColData(selNo - 1, 4);

	var url = "../case/LLSecondLPUWManuMain.jsp" +
	          "?MissionId=" + missionId + 
	          "&SubMissionId=" + subMissionId +
	          "&CaseNo=" + caseNo +
	          "&insuredno=" + insuredno +
	          "&EdorNo=" +edorNo +
	          "&ActivityId=0000001182";
	window.open(url);	
}

//进入既往理赔二核页面
function openLPTwoOldEdorWindow(){
	var selNo = LPTwoOldEdorGrid.getSelNo();
	var missionId = LPTwoOldEdorGrid.getRowColData(selNo - 1, 1);
	var subMissionId = LPTwoOldEdorGrid.getRowColData(selNo - 1, 2);
	var caseNo = LPTwoOldEdorGrid.getRowColData(selNo - 1, 3);
	var edorNo = LPTwoOldEdorGrid.getRowColData(selNo - 1, 11);	
	var insuredno = LPTwoOldEdorGrid.getRowColData(selNo - 1, 4);

	var url = "../case/LLSecondLPTwoUWManuMain.jsp" +
	          "?MissionId=" + missionId + 
	          "&SubMissionId=" + subMissionId +
	          "&CaseNo=" + caseNo +
	          "&insuredno=" + insuredno +
	          "&EdorNo=" +edorNo +
	          "&ActivityId=0000001182";
	window.open(url);	
}

//查询理赔续保待核受理信息
function queryLPXBList()
{
	var sql = "select MissionId 工作流任务号, SubMissionId 工作流子任务号, Missionprop5 案件号, Missionprop2 保单号, Missionprop3 投保人客户号, " 
	        + "Missionprop4 投保人, Missionprop5 受理人, Missionprop6 受理日期, Missionprop7 经办人, Missionprop8 送核日期, Missionprop9 送核时间" +
	        		", Missionprop12  工单号 " 
	        + " from LWMission where ProcessId = '0000000003' and ActivityId = '0000001181' " 
	        + "and defaultoperator ='" +operator+ "' "
	        + "and activitystatus ='1' "
	        + "order by Missionprop1 desc ";
	turnPage7.pageDivName = "divLPXB";
	turnPage7.queryModal(sql, LPXBGrid);
}

//查询理赔续保待核待回复信息
function queryLPNoReplyEdorList()
{
	var sql = "select MissionId 工作流任务号, SubMissionId 工作流子任务号, Missionprop5 案件号, Missionprop2 保单号, Missionprop3 投保人客户号, " 
	        + "Missionprop4 投保人, Missionprop5 受理人, Missionprop6 受理日期, Missionprop7 经办人, Missionprop8 送核日期, Missionprop9 送核时间" +
	        		", Missionprop12  工单号,'待回复' " 
	        + " from LWMission where ProcessId = '0000000003' and ActivityId = '0000001181' " 
	        + "and defaultoperator ='" +operator+ "' "
	        + "and activitystatus ='2' "
	        + "order by Missionprop1 desc ";
	turnPage8.pageDivName = "divLPNoReplyEdor";
	turnPage8.queryModal(sql, LPNoReplyEdorGrid);
}

//查询既往理赔续保待核信息
function queryLPOldEdorList()
{
    var EdorNo=fm.EdorNoLP.value;
    var ShouLiRen=fm.ShouLiRenLP.value;
    var wherePar="";
    if(trim(EdorNo)!=null&&trim(EdorNo)!=""&&trim(EdorNo)!="null")
    {
        wherePar+= "and Missionprop5='"+EdorNo+"' ";
    }
    if(trim(ShouLiRen)!=null&&trim(ShouLiRen)!=""&&trim(ShouLiRen)!="null")
    {
        wherePar+= "and Missionprop7='"+ShouLiRen+"' ";
    }
	var sql = "select MissionId, SubMissionId, Missionprop5, Missionprop2, Missionprop3, " +
	          "       Missionprop4, Missionprop5, Missionprop6, Missionprop7, Missionprop8, Missionprop9,Missionprop12, ModifyDate, defaultoperator " +
	          "from LBMission " +
	          "where ProcessId = '0000000003' " +
	          "and ActivityId = '0000001181' " +
	          wherePar+
	          "order by Missionprop1 desc ";
	turnPage9.pageDivName = "divLPOldEdor";
	turnPage9.queryModal(sql, LPOldEdorGrid);
	if(LPOldEdorGrid.mulLineCount == 0)
	{
	     alert("没有既往理赔续保信息");
         return false;
	}
}
//查询既往理赔二次核保信息
function queryLPTwoOldEdorList()
{
    var CaseNo=fm.CaseNo.value;
    var Maker=fm.Maker.value;
    var wherePar="";
    if(trim(CaseNo)!=null&&trim(CaseNo)!=""&&trim(CaseNo)!="null")
    {
        wherePar+= "and Missionprop1='"+CaseNo+"' ";
    }
    if(trim(Maker)!=null&&trim(Maker)!=""&&trim(Maker)!="null")
    {
        wherePar+= "and Missionprop4='"+Maker+"' ";
    }
	var sql = "select MissionId 工作流任务号, SubMissionId 工作流子任务号, Missionprop1  案件号, Missionprop2 被保人客户号, " 
             + "Missionprop3 被保人, Missionprop4 受理人, Missionprop5 受理日期, Missionprop9 经办人, Missionprop6 送核日期, Missionprop7 送核时间,  " 
             + " Missionprop10 工单号 " 
             + "from LBMission " 
	         + "where ProcessId = '0000000003' " 
	         + "and ActivityId = '0000001182' " 
	         + wherePar
	         + "order by Missionprop1 desc ";
	turnPage10.pageDivName = "divLPOldEdor";
	turnPage10.queryModal(sql, LPTwoOldEdorGrid);
	if(LPTwoOldEdorGrid.mulLineCount == 0)
	{
	     alert("没有既往理赔二次核保信息");
         return false;
	}
}

//查询理赔二核信息
function querySecondLPGrid(){
	var sql = "select MissionId 工作流任务号, SubMissionId 工作流子任务号, Missionprop1  案件号, Missionprop2 被保人客户号, " 
        + "Missionprop3 被保人, Missionprop4 受理人, Missionprop5 受理日期, Missionprop9 经办人, Missionprop6 送核日期, Missionprop7 送核时间,  " 
        + " Missionprop10 工单号 " 
        + " from LWMission where ProcessId = '0000000003' and ActivityId = '0000001182' " 
        + "and defaultoperator ='" +operator+ "' "
        + "order by Missionprop1 desc ";
	turnPage6.pageDivName = "divSecondLP";
	turnPage6.queryModal(sql, SecondLPGrid);
}

function afterGoToPage(){
}