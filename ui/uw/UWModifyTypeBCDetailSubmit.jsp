<%
//程序名称：UWModifyBCSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----BCsubmit---");
  LCBnfSet tLCBnfSet = new LCBnfSet();
  UWModifyTypeBCUI tUWModifyTypeBCUI   = new UWModifyTypeBCUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  
  //个人保单批改信息
	String tPolNo = request.getParameter("PolNo"); 	
       System.out.println("here1");         
    int Count = 0;
    String tGridTest[] = request.getParameterValues("LCBnfGridNo");
    if (tGridTest == null) 
        Count = 0;
    else 
	   Count = tGridTest.length;         
 System.out.println("Count : " + Count);
         //准备给付项信息
	 if (Count > 0) {
 
	     String tGridNo[] = request.getParameterValues("LCBnfGridNo");
	     String tBnfType[] = request.getParameterValues("LCBnfGrid1");
	     String tCustomerNo[] = request.getParameterValues("LCBnfGrid2");
	     String tName[] = request.getParameterValues("LCBnfGrid3");
	     String tSex[] = request.getParameterValues("LCBnfGrid4");
         String tBirthday[] = request.getParameterValues("LCBnfGrid5");
	     String tIDType[] = request.getParameterValues("LCBnfGrid6");
	     String tIDNo[] = request.getParameterValues("LCBnfGrid7");
	     String tRelationToInsured[] = request.getParameterValues("LCBnfGrid8");
	     String tBnfGrade[] = request.getParameterValues("LCBnfGrid9");
	     String tBnfLot[] = request.getParameterValues("LCBnfGrid10");
	     String tAddress[] = request.getParameterValues("LCBnfGrid11");
	     String tZipCode[] = request.getParameterValues("LCBnfGrid12");
          
	   for (int i=0;i<Count;i++)
	   {  	
	    LCBnfSchema tLCBnfSchema   = new LCBnfSchema();
  	    tLCBnfSchema.setPolNo(request.getParameter("PolNo"));
	    tLCBnfSchema.setBnfType(tBnfType[i]);
		tLCBnfSchema.setCustomerNo(tCustomerNo[i]);
		tLCBnfSchema.setName(tName[i]);
		tLCBnfSchema.setSex(tSex[i]);
		tLCBnfSchema.setBirthday(tBirthday[i]);
		tLCBnfSchema.setIDType(tIDType[i]);
		tLCBnfSchema.setIDNo(tIDNo[i]);
		tLCBnfSchema.setRelationToInsured(tRelationToInsured[i]);
		tLCBnfSchema.setBnfGrade(tBnfGrade[i]);
		tLCBnfSchema.setBnfLot(tBnfLot[i]);
		tLCBnfSchema.setAddress(tAddress[i]);
//		tLCBnfSchema.setRgtAddress(tRgtAddress[i]);
		tLCBnfSchema.setZipCode(tZipCode[i]);
//		tLCBnfSchema.setPhone(tPhone[i]);
		System.out.println("----tBnfLot[i]: " + tBnfLot[i]);
		System.out.println("----tBnfGrade[i]: " + tBnfGrade[i]);
		System.out.println("---- " + tLCBnfSchema.getBnfLot());
	 	tLCBnfSet.add(tLCBnfSchema);
  	   }
  	 }	

try
  {
     VData tVData = new VData();  
  	
	 //保存个人保单信息(保全)	
	tVData.addElement(tG);
 	tVData.addElement(tLCBnfSet);
 	tVData.addElement(tPolNo);
  	System.out.println("PEdorBCDetailLUI Start..");
    tUWModifyTypeBCUI.submitData(tVData, tPolNo );	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tUWModifyTypeBCUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
      FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

