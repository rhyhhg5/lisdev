//程序名称：UWApp.js
//程序功能：既往投保信息查询
//创建日期：2002-06-19 11:10:36
//创建人  ： WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var tPOLNO = "";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var cflag = "1" //操作位置　1.以往投保信息查询
var cPrtNo,cContNo;

var contState = "C";

function initContState(contNo){
	var sql = "select 1 from lccont where proposalcontno='" 
			+ contNo 
			+ "' union all "
			+ "select 2 from lbcont where proposalcontno='" 
			+ contNo 
			+ "' and edorno not like 'xb%'";
	var rs = easyExecSql(sql);
	if(rs){
		if(rs[0][0] == "2") {
			// 退保保单
			contState="B";
		} else if(rs[0][0] == "1") {
			contState="C";
		}
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//既往投保信息
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNoHide.value;
  cInsureNo = fm.InsuredNoHide.value;
  
  if (cProposalNo != "")
  {
  	//showModalDialog("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	window.open("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,"window2");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  	
  }
}                   

//以往核保记录
function showOldUWSub()
{
  var tSelNo = PolGrid.getSelNo();

	if( tSelNo == 0 || tSelNo == null )
	{
		alert( "请先选择一条记录。" );
		return;
	}
 	var cProposalNo = PolGrid.getRowColData(tSelNo - 1,1);	
  //cProposalNo=fm.ProposalNoHide.value;
  //showModalDialog("./UWSubMain.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  if (cProposalNo != "")
  {
  	window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+cPrtNo+"&ContNo="+cContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  	
  }
}

//当前核保记录
function showNewUWSub()
{
	var tSelNo = PolGrid.getSelNo();
  
  if( tSelNo == 0 || tSelNo == null )
	{
		alert( "请先选择一条记录。" );
		return;
	}
	
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var cProposalNo = PolGrid.getRowColData(tSelNo - 1,1);	
  //cProposalNo=fm.ProposalNoHide.value;
  if (cProposalNo != "")
  {
  	window.open("./UWErrMain.jsp?ProposalNo1="+cProposalNo,"window2");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");  
  }
}                      
      

//保单明细信息
function showPolDetail()
{
  var tSel = PolGrid.getSelNo();
   //alert(tSel);
  if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录。" );
		return;
	} 
	var cPrtNo=PolGrid.getRowColData( tSel - 1, 1 );
	var cContNo=PolGrid.getRowColData( tSel - 1, 16 );
	//alert(cPrtNo);
	//alert(cContNo);	
  if (cPrtNo != ""&&cContNo!="")
  {
  	window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+cPrtNo+"&ContNo="+cContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  	  	//showInfo.close();
  //urlStr = "./ProposalEasyScan.jsp?LoadFlag=6&ContNo="+cProposalContNo+"&prtNo="+cprtno ,"status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1"; 
  }
  else
  {  	
  	alert("请先选择保单!");	
  }

}


//核保报告书
function showReport()
{
  var tSelNo = PolGrid.getSelNo();

	if( tSelNo== 0 || tSelNo == null )
	{
		alert( "请先选择一条记录。" );
		return;
	}
	  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
	var cProposalNo = PolGrid.getRowColData(tSelNo - 1,1);	
  //cProposalNo=fm.ProposalNoHide.value;
  if (cProposalNo != "")
  {
  	//window.open("./UWManuReportMain.jsp?ProposalNo1="+cProposalNo+"&flag="+cflag,"window2");  	
  	window.open("UWQueryOldSubReportMain.jsp?PolNo="+cProposalNo,"window2");
 	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}                    
    

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//选择要人工核保保单
function queryUWSub()
{
	//showSubmitFrame(mDebug);
	fm.submit();
}

// 查询按钮
function easyQueryClick()
{
	
	// 书写SQL语句
	//alert("test");
	var strSQL = "";
	var i, j, m, n;
	var PrtNo = fm.all('PrtNoHide').value;
	var InsuredNo = fm.all('InsureNoHide').value;
	//alert(PrtNo);
	//alert(InsuredNo);
	//var appntno = fm.all('AppntNoHide').value;
	if ((InsuredNo != null) && (InsuredNo != "null") && (InsuredNo != ""))
	{

		 strSQL = "select A,B,C,D,E,F,G,H,L,M,N,O,P,Q,ctrim1(Q,G),R,S"
             +" from "
             +"("
             +"select"
             +" Z.prtno A,Z.insuredname B,Z.CValiDate C,Z.riskcode D,Z.Amnt E,int(Z.mult) F,case appflag when '1' then Z.sumprem else Z.prem end G,trim(char(Z.InsuYear))||Z.InsuYearFlag H,trim(char(Z.PayEndYear))||char(Z.PayEndYearFlag) L,case Z.appflag when '1' then '承保' else '投保' end M,Z.polno R,Z.contno S," 
             +"(select case when count(1)>0 then '是' else '否' end from LCPENotice where ProposalContNo=Z.ProposalContNo  AND customerNo=Z.insuredno) N,"
             +"(select codename from ldcode where codetype='uwflag' and code=Z.uwflag) O," 
             +"LPCSH1(Z.polno,'"+InsuredNo+"') P,"
             +"LPJE1(Z.polno) Q"
             +" from lcpol Z where Z.conttype='1' and Z.insuredno='"+InsuredNo+"' AND Z.prtno<>'"+PrtNo+"' and uwflag<>'a'"
             +")as x"
             +" union "
             +"select A,B,C,D,E,F,G,H,L,M,N,O,P,Q,ctrim1(Q,G),R,S"
             +" from "
             +"("
             +"select"
             +"  Z.prtno A,Z.insuredname B,Z.CValiDate C,Z.riskcode D,Z.Amnt E,int(Z.mult) F,Z.sumprem G,trim(char(Z.InsuYear))||Z.InsuYearFlag H,trim(char(Z.PayEndYear))||char(Z.PayEndYearFlag) L,'退保' M,Z.polno R,Z.contno S," 
             +"(select case when count(1)>0 then '是' else '否' end from LCPENotice where ProposalContNo=Z.ProposalContNo  AND customerNo=Z.insuredno) N,"
             +"(select codename from ldcode where codetype='uwflag' and code=Z.uwflag) O," 
             +"LPCSH1(Z.polno,'"+InsuredNo+"') P,"
             +"LPJE1(Z.polno) Q"
             +" from lbpol Z where Z.conttype='1' and Z.insuredno='"+InsuredNo+"'" 
             +" AND Z.prtno<>'"+PrtNo+"')as x"
             +" union "
             +"select A,B,C,D,E,F,G,H,L,M,N,O,0,0,0,R,S"
             +" from "
             +"("
             +"select"
             +" Z.prtno A,Z.insuredname B,Z.CValiDate C,Z.riskcode D,Z.Amnt E,int(Z.mult) F,Z.prem G,trim(char(Z.InsuYear))||Z.InsuYearFlag H,trim(char(Z.PayEndYear))||char(Z.PayEndYearFlag) L,'撤保' M,Z.polno R,Z.ContNo S," 
             +"(select case when count(1)>0 then '是' else '否' end from LobPENotice where ProposalContNo=Z.ProposalContNo AND customerNo=Z.insuredno) N,"
             +"(select codename from ldcode where codetype='uwflag' and code=Z.uwflag) O"
             +" from lcpol Z where Z.conttype='1' and Z.insuredno='"+InsuredNo+"' and uwflag='a'"
             +" AND Z.prtno<>'"+PrtNo+"')as x order by A"
		 	 	     ;
		 	 	 
	}
			turnPage.queryModal(strSQL,PolGrid,0,0,1); 	
/*
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
	alert();
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("无既往投保信息！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
*/
//  alert();
  changeChar();
  return true;

	
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	alert(1);
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PolGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		alert("result:"+arrResult);
	} // end of if
}

function ChoClick()
{
	initDiv();
	fm.uwstate.value = "";
	fm.UWIdea.value = "";
	//divPropValueName.style.display="none";
	//divPropValue.style.display="none";
	//divUWResult.style.display ="";
	UWResultInfo();
	//ShowAddFeeInfo();
	//initAddFeeInfo();
	//ShowSpecInfo();
	//showSubMult();
	//showDelayInfo();
}

/*********************************************************************
 *  查询个人合同信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */	
function queryCont()
{
	// 书写SQL语句
	//alert("test");
	var strSQL = "";
	var i, j, m, n;
	var contno = fm.all('ContNoHide').value;
	var insureno = fm.all('InsureNoHide').value;
//	var appntno = fm.all('AppntNoHide').value;
	if ((insureno != null) && (insureno != "null") && (insureno != ""))
	{
		strSQL ="select distinct name,case sex when '1' then '女' else '男' end,birthday,idtype,idno,(select codename from ldcode where  codetype='relation' "
					 +"and code in (select RelationTorelacustomer from LCContCustomerRelaInfo where RelaCustomerNo='"+insureno+"' and RelationTorelacustomer<>'00' and CustomerNo=lcinsured.insuredno)) "
					 +"from l" + contState + "insured where insuredno in(select customerno from LCContCustomerRelaInfo where RelaCustomerNo='"+insureno+"' and RelationTorelacustomer<>'00')";
	}	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("无家庭关系信息！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = ContGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  return true;
	
}

function changeChar(){
	for(var a=0 ; a< PolGrid.mulLineCount ; a++){
			for(var i=1 ; i < PolGrid.colCount ; i++){
				//alert(PolGrid.getRowColData(a,i).indexOf("Y"));
				//alert(PolGrid.getRowColData(a,i+1));
					PolGrid.setRowColData(a,i,replace(PolGrid.getRowColData(a,i),"1000A","终身"));
					PolGrid.setRowColData(a,i,replace(PolGrid.getRowColData(a,i),"70A","到70岁"));
					PolGrid.setRowColData(a,i,replace(PolGrid.getRowColData(a,i),"Y","年"));	

		}
	}
}
function initDiv(){
      divUWResult.style.display ="none";
		  divNoUWResult.style.display ="none";
		  DivAddFee.style.display='none';
		  divDelay.style.display='none';
}
function UWResultInfo()
{
	var ContNo = fm.all('ContNoHide').value;
	if(ContNo==null||ContNo=="")
	{
		alert("没有可用的可合同信息！");
		return false;
	}
	var tSel = PolGrid.getSelNo();
  //alert(tSel);
 	var polno=PolGrid.getRowColData( tSel - 1, 16 );
 	//alert(polno);	
	var strSQL = "select PassFlag,UWIdea,PostponeDay,PostponeDate,(select codename from ldcode where codetype='uwflag' and code=LCUWMaster.PassFlag) from LCUWMaster where polno='"+polno+"'";
	var arr  = easyExecSql(strSQL);
	if(arr)
	{   divUWResult.style.display ="";
		  divNoUWResult.style.display ="none";
			fm.uwstate.value = arr[0][0];
			fm.UWIdea.value = arr[0][1];
			fm.inputDelayDays.value = arr[0][2];
			fm.inputDelayDate.value = arr[0][3];
			fm.uwstateName.value=arr[0][4];
	}
 else{
 	divNoUWResult.style.display ="";
 	}
 	if(fm.uwstate.value == "4")
	{
		DivAddFee.style.display='';
		ShowAddFeeInfo();
		ShowSpecInfo();
	}
	if(fm.uwstate.value == "8")
	{
		divDelay.style.display='';
		
	}
}

function ShowSpecInfo()
{
	var tSelNo = PolGrid.getSelNo();
	var PolNo = PolGrid.getRowColData(tSelNo - 1,15);	
	var strSql = "select 1 from LCUWMaster where SpecFlag <> '0' and PolNo='"+PolNo+"'";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		var strSql = "select SpecCode,SpecContent,SpecStartDate,SpecEndDate,SerialNo from LCSpec where PolNo='"
		+PolNo+"'";
		turnPage.queryModal(strSql, SpecGrid);
	}
	else
		{
			initSpecGrid();
		}
}
function ShowAddFeeInfo()
{
	var tSelNo = PolGrid.getSelNo();
	var PolNo = PolGrid.getRowColData(tSelNo - 1,15);	
	var strSql = "select 1 from LCUWMaster where AddPremFlag <> '0' and PolNo='"+PolNo+"'";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		var strSql = "select prem,round(rate,2),paystartdate,payenddate from l" + contState + "prem where PolNo='"
		+PolNo+"' and substr(payplancode,1,6)='000000' ";
		turnPage.queryModal(strSql, AddFeeGrid);
	}
}