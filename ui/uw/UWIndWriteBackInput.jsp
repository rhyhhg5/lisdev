<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：UWIndWirteBackInput.jsp
//程序功能：人工核保通过之后,如果险种核保结论为变更承保将会自动发送核保通知书,及待客会回复
//创建日期：2006-5-29 11:06
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var mPrtNo = "<%=request.getParameter("PrtNo")%>";
	var mMissionID = "<%=request.getParameter("MissionID")%>";
	var mSubMissionID = "<%=request.getParameter("SubMissionID")%>";
	var mProposalContNo = "<%=request.getParameter("ProposalContNo")%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="UWIndWriteBackInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UWIndWriteBackInit.jsp"%>
  <title>核保通知书客户回复</title>
</head>

	<body  onload="initForm();initElementtype();" >
		<form method=post name=fm target="fraSubmit" >
			<Div  id= "divLCPol3" style= "display: ''" >
			  <table  class= common>
			    <tr  class= common>
			      <td text-align: left colSpan=1 >
							<span id="spanProposalGrid">
							</span> 
			      </td>
			    </tr>
			  </table>
			</Div>
				<table class=common>
					<TR  class= common>
					  <TD  class= title> 客户意见  </TD>
					  <TD  class= input>  <Input class="code" name=CustomerIdea value= "" CodeData= "0|^01|不同意^00|同意" ondblClick="showCodeListEx('Grade',[this,''],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('Grade',[this,''],[0,1],null,null,null,1);">  </TD>
					  <TD  class= input> <INPUT id="savebutton" VALUE="保  存" class=cssButton TYPE=button onclick="submitForm();"></TD>  
					  <TD></TD> 
					  <td></td> 
					</TR>
				</table>
		<table class=common>
			<tr>
				<TD height="24"  class= title>
					备注
				</TD>
				<td></td>
			</tr>
			<tr>
				<TD  class= input> <textarea name="UWIdea" cols="123%" rows="5" witdh=150% class="common"></textarea></TD>
				<td></td>
			</tr>
		</table>
		<p align="left">
			<INPUT id="savebutton" VALUE="提  交" class=cssButton TYPE=button onclick="confirmInput();">
		</p>
		<INPUT name="fmAction" TYPE="hidden">
		<INPUT name="PolNo" TYPE="hidden">
		<INPUT name="ContNo" TYPE="hidden">

		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
