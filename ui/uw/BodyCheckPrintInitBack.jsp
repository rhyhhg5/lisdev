<%
//程序名称：BodyCheckPrintInit.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	String strOperator = globalInput.Operator;
	String strManageCom = globalInput.ComCode;
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {
  fm.all('PrtNo').value = '';	     
  fm.all('AgentCode').value = '';
	fm.all('ManageCom').value = <%=strManageCom%>;
	fm.all('MarketType').value = '';	
  }
  catch(ex)
  {
    alert("在BodyCheckPrintInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("在BodyCheckPrintInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initPolGrid();   
  }
  catch(re)
  {
    alert("BodyCheckPrintInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var PolGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	    iArray[0][1]="30px";            	//列宽
	    iArray[0][2]=10;            			//列最大值
	    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	    iArray[1]=new Array();
	    iArray[1][0]="流水号";         		//列名
	    iArray[1][1]="100px";            	//列宽
	    iArray[1][2]=100;            			//列最大值
	    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	    iArray[2]=new Array();
	    iArray[2][0]="印刷号";       		//列名
	    iArray[2][1]="100px";            	//列宽
	    iArray[2][2]=100;            			//列最大值
	    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[3]=new Array();
	    iArray[3][0]="投保人";       		//列名
	    iArray[3][1]="60px";            	//列宽
	    iArray[3][2]=100;            			//列最大值
	    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    	  
	    iArray[4]=new Array();
	    iArray[4][0]="体检人";       		//列名
	    iArray[4][1]="60px";            	//列宽
	    iArray[4][2]=100;            			//列最大值
	    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    	  
	    iArray[5]=new Array();
	    iArray[5][0]="业务员代码";         	//列名
	    iArray[5][1]="100px";            	//列宽
	    iArray[5][2]=100;            			//列最大值
	    iArray[5][3]=0; 

	  
	    iArray[6]=new Array();
	    iArray[6][0]="生效日期";        //列名
	    iArray[6][1]="100px";            	//列宽
	    iArray[6][2]=200;            			//列最大值
	    iArray[6][3]=0; 									//是否允许输入,1表示允许，0表示不允许
        
        
      iArray[7]=new Array();
	    iArray[7][0]="下发日期";        //列名
	    iArray[7][1]="100px";            	//列宽
	    iArray[7][2]=200;            			//列最大值
	    iArray[7][3]=0; 									//是否允许输入,1表示允许，0表示不允许
	    
      iArray[8]=new Array();
	    iArray[8][0]="管理机构";         	//列名
	    iArray[8][1]="100px";            	//列宽
	    iArray[8][2]=100;            			//列最大值
	    iArray[8][3]=0; 

	  	
	    iArray[9]=new Array();
	    iArray[9][0]="保单印刷号";         	//列名
	    iArray[9][1]="100px";            	//列宽
	    iArray[9][2]=100;            			//列最大值
	    iArray[9][3]=3; 
	    
      iArray[10]=new Array();
	    iArray[10][0]="工作流任务编码";         	//列名
	    iArray[10][1]="0px";            	//列宽
	    iArray[10][2]=100;            			//列最大值
	    iArray[10][3]=3; 
	    
	    iArray[11]=new Array();
	    iArray[11][0]="工作流子任务编码";         	//列名
	    iArray[11][1]="0px";            	//列宽
	    iArray[11][2]=100;            			//列最大值
	    iArray[11][3]=3; 

      iArray[12]=new Array();
	    iArray[12][0]="预约状态";        //列名
	    iArray[12][1]="50px";            	//列宽
	    iArray[12][2]=200;            			//列最大值
	    iArray[12][3]=0; 									//是否允许输入,1表示允许，0表示不允许

	  		
      PolGrid = new MulLineEnter( "fmSave" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 10;   
      PolGrid.displayTitle = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus=1;
      PolGrid.hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>