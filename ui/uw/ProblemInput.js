//程序名称：QuestInput.js
//程序功能：问题件录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var k = 0;
var turnPage = new turnPageClass();
this.window.onfocus=myonfocus;
//提交，保存按钮对应操作
function submitForm()
{
	if(!verifyInput2())
	return false;
	fm.SerialNo.value="";
	fm.fmtransact.value="INSERT||MAIN";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  ChangeDecodeStr();
  fm.action = './QuestInputChk.jsp';
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	window.focus();
    
  UnChangeDecodeStr();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
    alert(content);
    //parent.close();
  }
  else
  { 
	var showStr="操作成功";
  	easyQueryClick();
  	alert(showStr);
  	//parent.close();
  	
    //执行下一步操作
  }
}

//下发问题件反馈方法
function afterSubmit2( FlagStr, content )
{
	//window.focus();//取消发问题件通知书后的焦点设定    xiaoxin    2008-1-4
  showInfo.close();
    
  UnChangeDecodeStr();
  if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

//发问题件通知书
function SendIssue() {
 
  cContNo = fm.ContNo.value;
  cMissionID =fm.MissionID.value;

  if (cContNo != "") {
      //manuchk();
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      var cPrtNo = fm.PrtNoHide.value;

     // strsql = "select * from lcissuepol where contno = '" +cContNo+ "' and backobjtype = '2' and (state = '0' or state is null)";
      strsql = "select * from lcissuepol where contno = '" +cContNo+ "'  and (state = '0' or state is null)";

      turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
      //判断是否查询成功
      if (!turnPage.strQueryResult) {
        showInfo.close();
        
        window.focus();
        
        alert("不容许发放新的问题件通知单");
        fm.SubNoticeMissionID.value = "";
        return ;
      }

      fm.SubNoticeMissionID.value = tSubMissionID;
      fm.action = 'ProblemInputSave.jsp';
      fm.submit();
    } 
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function manuchkspecmain()
{
	fm.submit();
}

function QuestQuery(tContNo, tFlag)
{
	// 初始化表格
	var i,j,m,n;
	//initQuestGrid();
	
	
	// 书写SQL语句
	k++;
	var strSQL = "";
	//if (tFlag == "1")
	//{
		strSQL = "select code,cont from ldcodemod where "+k+"="+k				 	
				 + " and codetype = 'Question'";
	//}
	
	//alert(strSQL);
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有问题件描述");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  //turnPage.pageDisplayGrid = QuestGrid;    
          
  //保存SQL语句
  //turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  //turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  var returnstr = "";
  var n = turnPage.arrDataCacheSet.length;  
    if (n > 0)
  {
  	for( i = 0;i < n; i++)
  	{
  		m = turnPage.arrDataCacheSet[i].length;
  		//alert("M:"+m);
  		if (m > 0)
  		{
  			for( j = 0; j< m; j++)
  			{
  				if (i == 0 && j == 0)
  				{
  					returnstr = "0|^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i == 0 && j > 0)
  				{
  					returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j == 0)
  				{
  					returnstr = returnstr+"^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j > 0)
  				{
  					returnstr = returnstr+"|"+turnPage.arrDataCacheSet[i][j];
  				}
  				
  			}
  		}
  		else
  		{
  			alert("查询失败!!");
  			return "";
  		}
  	}
}
else
{
	alert("查询失败!");
	return "";
}
  //alert("returnstr:"+returnstr);		
  fm.Quest.CodeData = returnstr;
  return "";	
}


function QueryCont(tContNo, tFlag)
{	
	
	// 书写SQL语句

	k++;

	var strSQL = "";

	//if (tFlag == "1")
	//{
		strSQL = "select issuecont from lcissuepol where "+k+"="+k				 	
				 + " and ContNo = '"+tContNo+"'";
	//}
	
	//alert(strSQL);
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有录入过问题键！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  var returnstr = "";
  var n = turnPage.arrDataCacheSet.length;
  //alert("N:"+n);
  if (n > 0)
  {
  	m = turnPage.arrDataCacheSet[0].length;
  		//alert("M:"+m);
  		if (m > 0)
  		{
  			//alert("turnPage:"+turnPage.arrDataCacheSet[0][0]);
			returnstr = turnPage.arrDataCacheSet[0][0];
  		}
  		else
  		{
  			alert("没有录入过问题键！");
  			return "";
  		}
  	
  }
  else
  {
  	alert("没有录入过问题键！");
	return "";
  }

  if (returnstr == "")
  {
  	alert("没有录入过问题键！");
  }
  
  //alert("returnstr:"+returnstr);		
  fm.all('Content').value = returnstr;
  //alert("已经录入过问题键，请考虑清楚再重新录入！");
  return returnstr;
}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL = "";		
	// 书写SQL语句
	k++;
	var ContNo = fm.ContNo.value;
	strSQL = "select t.QuestionObj,t.ErrFieldName,t.ErrContent,t.IssueCont,'',t.SerialNo from lcissuepol t where 1=1 "
			+ " and contno = '" +ContNo+"'"
			;
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = IssueGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 //tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}

/*********************************************************************
 *  选择核保结后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field ) 
{
	if(cCodeName == '')
	{
		
	}
	
	if(cCodeName == 'issueerrfield')
	{
		queryOldFieldValue(fm.SqlObj.value);
	}
}
//查询投保人和被保人姓名
function easyQueryClickSingle()
{
	var strSQL=" select A,B from ("
							+" select '投保人-'||AppntName as A,AppntNo as B from lccont where contno='"+fm.ContNo.value+"'"
							+" union select '第'||SequenceNo||'被保险人-'||name A,insuredno B from lcinsured where contno='"+fm.ContNo.value+"')"
							+" as c  order by A";
	//alert(fm.ContNo.value);
//alert(strSQL);
	fm.all("QuestionObj").CodeData=easyQueryVer3(strSQL, 1, 0, 1);
	//alert(easyQueryVer3(strSQL, 1, 0, 1));

}

//查询原填写内容
function queryOldFieldValue(strSQL)
{
	if(strSQL == null || strSQL == "")
	{
		fm.OldFieldValue.value = "";
		return;
	}
	//alert(fm.QuestionObjName.value);
	//alert(fm.ContNo.value);
	//---------------------- 2007-10-26 修改原填写内容中性别
	strSQL = strSQL.replace("#sex#", "'sex'");
	//---------------------- 2007-10-26 修改原填写内容中性别
	strSQL = strSQL.replace("customerno=","customerno='"+fm.QuestionObjName.value+"'");
	strSQL = strSQL.replace("contno=","contno='"+fm.ContNo.value+"'");
	//alert(strSQL);
	var arr = easyExecSql(strSQL);
	if(arr)
		fm.OldFieldValue.value = arr;
}
//查询原填写内容
function updateClick()
{
	if(!verifyInput2())
		return false;
	fm.fmtransact.value="UPDATE||MAIN";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  ChangeDecodeStr();
  fm.submit(); //提交
}
function deleteClick()
{
	//if(!verifyInput2())
	//	return false;
	var tSel = IssueGrid.getSelNo()-1;
	if(tSel<0){
		alert("请选择一条要删除的问题件");
		return false;
	}
	fm.SerialNo.value = IssueGrid.getRowColData(tSel,6);
	fm.fmtransact.value="DELETE||MAIN";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = './QuestInputChk.jsp';
  fm.submit(); //提交
}


/**
 * 问题件回退
 */
function questBack()
{
    // 如果保单有加费信息，需先删除了加费信息再问题件回退
    var strSql = "select 1 from LCPrem where ContNo = '" 
        + fm.ContNo.value + "' and substr(PayPlanCode, 1, 6) = '000000'";
    var arr = easyExecSql(strSql);
    if(arr)
    {
        alert("保单有加费信息，请删除加费信息，再进行问题件回退！");
        return false;
    }
    // --------------------
    
    var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = ""
        + "./UWQuestBackSave.jsp"
        + "?MissionID=" + tMissionID 
        + "&SubMissionId=" + tSubMissionID
        + "&ContNo=" + mContNo
        ;
    fm.submit();
}

function afterQuestBackSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
        //initForm();
    }
    easyQueryClick();
}