//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  
  //if(!verifyInput()) return false;
  var prtno = "";
  var tSel = PolGrid.getSelNo();  
  var tSel1 = ContGrid.getSelNo();
	
	if((tSel == 0 || tSel == null) && (tSel1 == 0 || tSel1 == null) )
	{
			alert("请选择一条记录")
			return;
	}	
else if(tSel == 0 || tSel == null ) 
	{
		prtno = ContGrid.getRowColData(tSel1 - 1,1);
	}
  else if(tSel1 == 0 || tSel1 == null)
  {
  	prtno = PolGrid.getRowColData(tSel - 1,1);
  }
  else
	{
		alert("不能同时选择两条记录")
		return;
	}
	
  //判断保单是否是银保通的单子，如果是则报错   by zhangyang
  if(!checkIsYBT(prtno))
  {
  	return false;
  }
  //判断当前保单的收费是否为实时收费状态 zxs
  if(!checkIsRealTime(prtno))
  {
  	return false;
  }
  
  fm.PrtNo1.value = prtno;
 
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
    showInfo.close();
    alert(content);
    //parent.close();
  }
  else
  { 
	  var showStr="操作成功";
  	
  	showInfo.close();
  	alert(showStr);
  	//parent.close();
  	
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function queryPol()
{
  //调用查询投保单的文件
	 window.open("./QuestionAdditionalRiskQuery.html");
}
function easyQueryClick()
{
	
  var strSQL = "";		
  if(tCardFlag == "1")
 	  {
		strSQL = strSQL = " select prtno,polapplydate,cvalidate,appntname,getUniteCode(lc.agentcode),lc.managecom "
				+ "from lccont lc, loprtmanager lop "
				+ "where otherno = lc.contno and lop.stateflag = '3' and code <> '07' and formakedate < current date - 13 day"
				+ " and uwflag not in ('a', '1', '8') and appflag in ('0', '9') and conttype = '1'"
				+ " and cardflag = '1'"
				+ "and trim(lc.managecom) like'"
				+ comcode
				+ "%%'"
				+ " union select distinct lccont.prtno,lccont.polapplydate,lccont.cvalidate,lccont.appntname,getUniteCode(lccont.agentcode),lccont.managecom from lccont, ljtempfee, loprtmanager lop"
				+ " where lop.otherno = contno and ljtempfee.otherno = lccont.prtno and lop.stateflag = '3' and code <> '07'  and formakedate < current date - 13 day "
				+ " and ljtempfee.enteraccdate is null and lccont.appflag in ('0', '9')   and lccont.conttype = '1'   "
				+ " and lccont.cardflag = '1'"
				+ " and trim(lccont.managecom) like'" + comcode + "%%'"; 
		;
	}	
  else
   {
		strSQL = " select prtno,polapplydate,cvalidate,appntname,getUniteCode(lc.agentcode),lc.managecom "
				+ "from lccont lc, loprtmanager lop "
				+ "where otherno = lc.contno and lop.stateflag = '3' and code <> '07' and formakedate < current date - 13 day"
				+ " and uwflag not in ('a', '1', '8') and appflag in ('0', '9') and conttype = '1'"
				+ "and trim(lc.managecom) like'"
				+ comcode
				+ "%%'" 
				+ " union select distinct lccont.prtno,lccont.polapplydate,lccont.cvalidate,lccont.appntname,getUniteCode(lccont.agentcode),lccont.managecom from lccont, ljtempfee, loprtmanager lop"
				+ " where lop.otherno = contno and ljtempfee.otherno = lccont.prtno and lop.stateflag = '3' and code <> '07'  and formakedate < current date - 13 day "
				+ " and ljtempfee.enteraccdate is null and lccont.appflag in ('0', '9')   and lccont.conttype = '1'   "
				+ " and trim(lccont.managecom) like '" + comcode + "%%'"; 
		;
	}    			
   
  turnPage.pageLineNum = 50;         
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
   //alert("没有催办过期的保单！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
 //tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function easyQueryClick1()
{
    var strSQL = "";
    //书写SQL语句
    strSQL = " select lcc.prtno,lcc.polapplydate,lcc.cvalidate,lcc.appntname,getUniteCode(lcc.agentcode),lcc.managecom,lcc.SaleChnl,lcc.AgentCom "
        + " from lccont lcc"
        + " left join LCRnewStateLog lcr on lcr.newcontno = lcc.contno"
        + " where 1 = 1"
        + " and (lcc.UWFlag is null or lcc.UWFlag not in ('a','1','8'))"
        + " and lcc.appflag in ('0','9')"
        + " and lcc.conttype = '1'"
        + " and lcr.newcontno is null"
        + getWherePart('lcc.PrtNo','PrtNo')
        + getWherePart('lcc.polapplydate','PolApplyDate')
        + getWherePart('lcc.cvalidate','CvaliDate')
        + getWherePart('lcc.appntname','AppntName')
        + getWherePart('getUniteCode(lcc.agentcode)','AgentCode')
        + getWherePart('lcc.managecom','ManageCom')
        + getWherePart('lcc.SaleChnl','SaleChnlCode')
        + getWherePart('lcc.AgentCom','AgentComBank')
        + " and lcc.managecom like '" + comcode + "%%'";  //集中权限管理体现
        ;
    if(tCardFlag == "1")
    {
        strSQL = strSQL + " and cardflag = '1'";
    }

    turnPage.pageLineNum = 50;

    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
    fm.querySql.value = strSQL;
    //判断是否查询成功
    if(!turnPage.strQueryResult) {
        alert("该保单不存在，或者该保单已经撤单！");
        return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = ContGrid;

    //保存SQL语句
    turnPage.strQuerySql     = strSQL;

    //设置查询起始位置
    turnPage.pageIndex       = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果

    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}


/**
 * 下拉框，选定后触发事件。
 */
function afterCodeSelect(tCodeName, tObj)
{
    if (tCodeName == "lcsalechnl")
    {
        var tSaleChnl = fm.SaleChnlCode.value;
        displayAgentComBank(tSaleChnl == "04");
    }
}

/**
 * 显示/隐藏银行代理录入控件。
 */
function displayAgentComBank(isDisplay)
{
    if(isDisplay == true)
    {
        fm.AgentComBank.style.display = "";
    }
    else
    {
        fm.AgentComBank.value = "";
        fm.AgentComBank.style.display = "none";        
    }
}

/**
 * 查询银行网点
 */
function queryAgentComBank()
{
    showInfo=window.open("../agentbranch/LAComQuery.html?BranchType=3&BranchType2=01");
    //alert(showInfo.fm);
}

/**
 * 查询结构返现
 */
function afterQuery(arrQueryResult)
{
    if(arrQueryResult)
    {
        fm.AgentComBank.value = arrQueryResult[0][0];
    }
}

//add by hyy
function downloadClick()
{
	  if(ContGrid.mulLineCount==0)
    {
        alert("列表中没有数据可下载");
    }
    var oldAction = fm.action;
    fm.action = "ApplyRecallDownload.jsp";
    fm.submit();
    fm.action = oldAction;
}

//判断保单是否是银保通保单  by zhangyang
function checkIsYBT(prtno)
{
	var strSql = "select 1 from lccont where prtno = '" + prtno + "' and CardFlag = '9'";
	var arrResult = easyExecSql(strSql);
	
	if(arrResult != null)
	{
		alert("该保单为银保通的单子，银保通不能由核心撤单！");
		return false
	}
	return true;
} 
//判断保单是否处于实时收费状态  by renwei
function checkIsRealTime(prtno)
{
	var strSql = "select 1 from ljspay a,lccontsub b "
		 	+ " where a.otherno=b.prtno "
		 	+ " and a.otherno = '" + prtno + "' "
			+ " and a.othernotype = '9' "
			+ " and a.cansendbank = 'p' " 
			+ " and a.otherno like 'PD%' "
			+ " and b.agreedpayflag='0' ";
	
	var arrResult = easyExecSql(strSql);
	if(arrResult)
	{
		alert("PAD实时收费、非约定缴费保单不允许撤单！");
		return false;
	}
	//判断PAD实时收费约定缴费日期
	var sql1="select agreedpaydate from lccontsub where prtno='"+fm.PrtNo.value+"' " 
			+ " and prtno like 'PD%' " 
			+ " and agreedpayflag='1' ";
	var arrPADSql=easyExecSql(sql1);
	if(arrPADSql){
		var sql2="select 1 from ljspay where otherno = '" + fm.PrtNo.value + "' " 
				+ " and cansendbank = 'p' " 
				+ " and othernotype = '9' ";
		var sql2Result=easyExecSql(sql2);
		var agreedPayDate=arrPADSql[0][0];
		if(sql2Result){
			if(Sysdate >= arrPADSql[0][0]){
				alert("该保单为PAD实时收费约定缴费保单，且约定缴费日期小于或等于当前系统日期，不允许撤单！");
				return false;
			}
		}
		var year=agreedPayDate.substring(0,4);
		var month=agreedPayDate.substring(5,7);
		var day=agreedPayDate.substring(8);
		var msg=confirm("该单约定缴费日为" + year + "年" + month + "月" + day + "日，请确认是否进行撤单！可以点击“确认”或“取消”。“取消”则撤销失败，“确认”则撤销成功！");
		if(!msg){
			return false;
		}
	}
	return true;
}