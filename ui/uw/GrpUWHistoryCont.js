//程序名称：GrpUWInsuCont.js
//程序功能：团体既往承保函数
//创建日期：2005-4-26
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容

/*********************************************************************
 *  该文件中包含客户端需要处理的函数和事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var cGrpContNo;
var PrtNo;

/*********************************************************************
 *  查询承保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQuery()
{
    // 初始化表格
    initGrpHistoryGrid();

    var tStrSql = ""
        + " select tmp.GrpContNo, tmp.ProposalGrpContNo, tmp.PrtNo, tmp.CValidate, tmp.CInvalidate, tmp.Peoples2, "
        + " Div(AppntOnWorkPeoples, tmp.OnWorkPeoples) OnWorkScale, tmp.Prem, tmp.ContState, tmp.SignDate, tmp.WTFlag "
        + " from ( "
        + " select lgc.GrpContNo, lgc.ProposalGrpContNo, lgc.PrtNo, lgc.CValidate, lgc.CInvalidate, lgc.Peoples2, "
        + " (select ldg.OnWorkPeoples from LDGrp ldg where lgc.AppntNo = ldg.CustomerNo) OnWorkPeoples, "
        + " lgc.OnWorkPeoples AppntOnWorkPeoples, "
        + " lgc.Prem, '承保' ContState, lgc.SignDate, '1' WTFlag "
        + " from LCGrpCont lgc "
        + " where 1 = 1 "
        + " and lgc.AppntNo = '" + mAppntNo + "' "
        + " and lgc.AppFlag = '1' "
        + " union all "
        + " select lbgc.GrpContNo, lbgc.ProposalGrpContNo, lbgc.PrtNo, lbgc.CValidate, lbgc.CInvalidate, lbgc.Peoples2, "
        + " (select ldg.OnWorkPeoples from LDGrp ldg where lbgc.AppntNo = ldg.CustomerNo) OnWorkPeoples, "
        + " lbgc.OnWorkPeoples AppntOnWorkPeoples, "
        + " lbgc.Prem, '退保' ContState, lbgc.SignDate, '0' WTFlag "
        + " from LBGrpCont lbgc "
        + " where 1 = 1 "
        + " and lbgc.AppntNo = '" + mAppntNo + "' "
        + " and lbgc.AppFlag = '1' "
        + " ) as tmp "
        + " order by tmp.GrpContNo "
        + " with ur "
        ;
    
    turnPage1.pageDivName = "divGrpHistoryGridPage";
    turnPage1.queryModal(tStrSql, GrpHistoryGrid);

    // 判断是否查询成功
    if (!turnPage1.strQueryResult)
    {
        alert("没有既往承保信息！");
        return false;
    }
}


/*********************************************************************
 *  取的客户团体的名称
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryI()
{
  // 书写SQL语句
  var arrRs;
  var strSQL = "select GrpName";
  strSQL    += "  from LDGrp";
  strSQL    += " where CustomerNo='"+mAppntNo+"'";

  fm.all("GrpNo").value=mAppntNo;

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //查询成功设置画面上的数据
  if(turnPage.strQueryResult)
  {
    arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.all("GrpName").value=arrRs[0][0];
  }

  strSQL  = "select ProposalGrpContNo";
  strSQL += "  from LCGrpCont";
  strSQL += " where GrpContNo = '" + mGrpContNo + "'";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //查询成功设置画面上的数据
  if(turnPage.strQueryResult)
  {
    arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.all("GrpPolNo").value=arrRs[0][0];
  }
}

/*********************************************************************
 *  既往询价信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAskApp()
{
  window.open("../uw/GrpUWAskCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往保全信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showRejiggerApp()
{
  window.open("../uw/GrpUWRejiggerCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往理陪信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsuApp()
{
  window.open("../uw/GrpUWInsuCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往保障信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showEnsureCont()
{
  window.open("../uw/GrpUWEnsureCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往承保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSignContH()
{
	window.open("../uw/GrpUWHistoryCont.jsp?pmGrpContNo=" + mGrpContNo + "&pmAppntNo=" + mAppntNo + "", "window2");
}


/**
 * 关闭当前查询页面。
 */
function GoBack()
{
  top.close();
}


/**
 * 显示某保单的详细承保信息。
 */
function queryGrpHistoryDetail()
{
    var tRow = GrpHistoryGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = GrpHistoryGrid.getRowData(tRow);
    
    var tGrpContNo = tRowDatas[0];
    var tWTFlag = tRowDatas[10];
    
    var tStrSql = ""
        + " select tmp.ContPlanCode, tmp.ContPlanName, tmp.RiskCode, tmp.RiskName, tmp.PeoplesCount, tmp.Prem, "
        + " Div(tmp.GentlemanCount, (tmp.GentlemanCount + tmp.LadyCount)), "
        + " Div(tmp.LadyCount, (tmp.GentlemanCount + tmp.LadyCount)), "
        + " Div(tmp.OffWorkPeoplesCount, tmp.ContCount), "
        + " tmp.AvgInsuredAppAge "
        + " from ( "
        + " select lccp.ContPlanCode, lccp.ContPlanName, lccpr.RiskCode, "
        + " (select lmr.RiskName from LMRisk lmr where lmr.RiskCode = lccpr.RiskCode) RiskName, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode) PeoplesCount, "
        + " (select nvl(sum(lcp.Prem), 0) from LCPol lcp where lcp.GrpContNo = lccp.GrpContNo and lcp.RiskCode = lccpr.RiskCode and lcp.ContPlanCode = lccp.ContPlanCode) Prem, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode and lci.Sex = '0') GentlemanCount, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode and lci.Sex = '1') LadyCount, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode and lci.InsuredStat = '2') OffWorkPeoplesCount, "
        + " (select count(distinct lci.InsuredNo) from LCInsured lci where lci.GrpContNo = lccp.GrpContNo and lci.ContPlanCode = lccp.ContPlanCode and lci.RelationToMainInsured = '00') ContCount, "
        + " (select varchar(Round(nvl(avg(double(lcp.InsuredAppAge)), 0), 2)) InsuredAppAge from LCPol lcp where lcp.GrpContNo = lccp.GrpContNo and lcp.RiskCode = lccpr.RiskCode and lcp.ContPlanCode = lccp.ContPlanCode) AvgInsuredAppAge "
        + " From LCContPlan lccp "
        + " inner join LCContPlanRisk lccpr on lccp.GrpContNo = lccpr.GrpContNo and lccp.ContPlanCode = lccpr.ContPlanCode "
        + " where 1 = 1 "
        + " and 1 = " + tWTFlag
        + " and lccp.GrpContNo = '" + tGrpContNo + "' "
        + " and lccp.ContPlanCode not in ('11') "
        + " ) as tmp "
        + " union all "
        + " select tmp.ContPlanCode, tmp.ContPlanName, tmp.RiskCode, tmp.RiskName, tmp.PeoplesCount, tmp.Prem, "
        + " Div(tmp.GentlemanCount, (tmp.GentlemanCount + tmp.LadyCount)), "
        + " Div(tmp.LadyCount, (tmp.GentlemanCount + tmp.LadyCount)), "
        + " Div(tmp.OffWorkPeoplesCount, tmp.ContCount), "
        + " tmp.AvgInsuredAppAge "
        + " from ( "
        + " select lbcp.ContPlanCode, lbcp.ContPlanName, lbcpr.RiskCode, "
        + " (select lmr.RiskName from LMRisk lmr where lmr.RiskCode = lbcpr.RiskCode) RiskName, "
        + " (select count(distinct lbi.InsuredNo) from LBInsured lbi where lbi.GrpContNo = lbcp.GrpContNo and lbi.ContPlanCode = lbcp.ContPlanCode) PeoplesCount, "
        + " (select nvl(sum(lbp.Prem), 0) from LBPol lbp where lbp.GrpContNo = lbcp.GrpContNo and lbp.RiskCode = lbcpr.RiskCode and lbp.ContPlanCode = lbcp.ContPlanCode) Prem, "
        + " (select count(distinct lbi.InsuredNo) from LBInsured lbi where lbi.GrpContNo = lbcp.GrpContNo and lbi.ContPlanCode = lbcp.ContPlanCode and lbi.Sex = '0') GentlemanCount, "
        + " (select count(distinct lbi.InsuredNo) from LBInsured lbi where lbi.GrpContNo = lbcp.GrpContNo and lbi.ContPlanCode = lbcp.ContPlanCode and lbi.Sex = '1') LadyCount, "
        + " (select count(distinct lbi.InsuredNo) from LBInsured lbi where lbi.GrpContNo = lbcp.GrpContNo and lbi.ContPlanCode = lbcp.ContPlanCode and lbi.InsuredStat = '2') OffWorkPeoplesCount, "
        + " (select count(distinct lbi.InsuredNo) from LBInsured lbi where lbi.GrpContNo = lbcp.GrpContNo and lbi.ContPlanCode = lbcp.ContPlanCode and lbi.RelationToMainInsured = '00') ContCount, "
        + " (select varchar(Round(nvl(avg(double(lbp.InsuredAppAge)), 0),2 )) InsuredAppAge from LBPol lbp where lbp.GrpContNo = lbcp.GrpContNo and lbp.RiskCode = lbcpr.RiskCode and lbp.ContPlanCode = lbcp.ContPlanCode) AvgInsuredAppAge "
        + " From LBContPlan lbcp "
        + " inner join LBContPlanRisk lbcpr on lbcp.GrpContNo = lbcpr.GrpContNo and lbcp.ContPlanCode = lbcpr.ContPlanCode "
        + " where 1 = 1 "
        + " and 0 = " + tWTFlag
        + " and lbcp.GrpContNo = '" + tGrpContNo + "' "
        + " and lbcp.ContPlanCode not in ('11') "
        + " ) as tmp "
        + " order by ContPlanCode, RiskCode "
        + " with ur "
        ;
    
    turnPage2.pageDivName = "divGrpHistoryDetailGridPage";
    turnPage2.queryModal(tStrSql, GrpHistoryDetailGrid);

    // 判断是否查询成功
    if (!turnPage2.strQueryResult)
    {
        alert("没有明细既往承保信息！");
        return false;
    }
    
    return true;
}

/**
 * 显示团体保单信息页面。
 */
function showGrpCont()
{
    var tRow = GrpHistoryGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = GrpHistoryGrid.getRowData(tRow);
    
    var tGrpContNo = tRowDatas[0];
    var tPrtNo = tRowDatas[3];

    window.open("../app/GroupPolApproveInfo.jsp?prtNo=" + tPrtNo + "&LoadFlag=16&polNo=" + tGrpContNo, "window1");
    
    return true;
}

function grpRiskPlanInfo()
{
	if(cGrpContNo&&PrtNo)
	{
	var newWindow = window.open("../app/ContPlan.jsp?prtNo="+PrtNo+"&GrpContNo="+cGrpContNo+"&LoadFlag=16");
	}else{
		alert("请先选择一条投保单!");
	}	
}
