<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

<%
	String tContNo = "";
	String tGrpPolNo="";
	String tGrpPrtNo="";
	try
	{
		tContNo = request.getParameter( "ContNo" );
		tGrpPolNo = request.getParameter( "GrpProposalNo" );
		tGrpPrtNo = request.getParameter( "GrpPrtNo" );
		//默认情况下为集体投保单
		if( tContNo == null || tContNo.equals( "" ))
			tContNo = "00000000000000000000";
	}
	
	catch( Exception e1 )
	{
		tContNo = "00000000000000000000";
			System.out.println("---contno:"+tContNo);

	}
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("---contno:"+tContNo);
%>
<script>
	
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var grpprtno = "<%=tGrpPrtNo%>";  
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	
</script>


<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="GrpQuestModifyDetail.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpQuestModifyDetailInit.jsp"%>
  <title>团体投保单查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 团体保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common  TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
  	 <br></br>
  	 <!--INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"--> 
  	 <INPUT VALUE="团体扫描件查询"    Class=Common TYPE=button onclick="ScanQuery();"> 
     <INPUT VALUE="修改团体投保单明细" class= common TYPE=button onclick="returnParent();"> 
     <INPUT VALUE="团体单问题件查询" class= common TYPE=button onclick="QuestQuery();">
     <INPUT VALUE="修改完成确认" class= common TYPE=button onclick="ModifyMakeSure();">
    
     		
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
