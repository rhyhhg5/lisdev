
<jsp:directive.page import="com.sinosoft.lis.pubfun.GlobalInput"/><%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var RePrintFlag = "<%=request.getParameter("RePrintFlag")%>";//记录登陆机构
	var saleChnl = "";//销售渠道   zhangjianbao   2007-11-16
</script>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="HeathIssueManager.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="HeathIssueManagerInit.jsp"%>
  <title>单证件删除</title>
</head>
<body  onload="initForm();" >
  <form action="./HeathIssueManagerSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入投保单查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  </TD>   
          <TD  class= title> 印刷号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo > </TD>
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet1',[this,AgentCodeName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet1',[this,AgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >   </TD> 
         </TR> 
         <TR  class= common>
          <TD  class= title> 投保人</TD>
          <TD  class= input><Input class="common" name=AppntName verify="投保人|len<=20"></TD>
          <TD  class= title> 下发起始日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=startMakeDate verify="下发日期|date">   </TD>
          <TD  class= title> 下发终止日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" name=endMakeDate verify="下发日期|date">   </TD>
         </TR> 
         <TR>
          <TD  class= title> 单证类型</TD>
          <TD  class= input><Input CLASS=codeNo name=CodeType  CodeData="0|^0|问题件^1|体检件 ^2|契调件"  ondblclick="return showCodeListEx('CodeType',[this,CodeTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CodeType',[this,CodeTypeName],[0,1],null,null,null,1);"><input class=codename name=CodeTypeName readonly=true  ></TD>
          <TD  class= title> 是否回销</TD>
          <TD  class= input><Input class="codeNo" name=SelType ondblclick="return showCodeList('takebackstate',[this,SelTypeName],[0,1]);" onkeyup="return showCodeListKey('takebackstate',[this,SelTypeName],[0,1]);"><input class=codename name=SelTypeName readonly=true ></TD>
          <TD  class= title> 是否已打</TD>
          <TD  class= input><Input class="codeNo" name=StateFlag CodeData="0|^0|未打印^1|已打印 ^9|所有" ondblclick="return showCodeListEx('printstate1',[this,StateFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('printstate1',[this,StateFlagName],[0,1]);"><input class=codename name=StateFlagName readonly=true >  </TD>
      </tr>
    </table>
    <input type=hidden name="agentGroup">
  	<input type=hidden name="querySql">
    <input value="查  询" class= CssButton type=button onclick="easyQueryClick();">
  </form>
  <form action="./HeathIssueManagerSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();"> 	
  	</div>
  	<div id= "printissue" style= "display: ''" align = left>
  		<table>
  			<tr>
  				<td>
  	          <p>
                <INPUT VALUE="单证删除" class= CssButton  type=button onclick="printPol();">
  	          </p>
  	      </td>
  	     </tr>
  	  </table>  	
    </div>
    <br><br>
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="PrtSeq" name="PrtSeq">
  	<input type=hidden id="CodeType" name="CodeType">
  	<input type=hidden id="MissionID" name="MissionID">
  	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    	<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>
</body>
</html>