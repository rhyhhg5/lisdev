<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*" %>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%@page import="com.sinosoft.lis.cbcheck.*" %>
<%@page import="java.util.List" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.tb.*" %>
<%@page import="com.sinosoft.lis.bl.*" %>

<%
/*
save页面执行流程： 
1，先从页面取到客户输入的信息数据
2，判断用户执行了什么操作（增加，删除，修改）——这里省略了这一步
3，传递过来的数据封装到TransferData对象中
4，TransferData对象放在VData容器中
5，BL类中（逻辑控制层）submitData方法 提交数据到控制层（返回布尔类型的变量）
*/
 %>
<%
System.out.println("进入save页面--拿到页面的数据，对数据进行封装");
String Content = "";
String FlagStr = "";
//获取页面保存或删除操作的标志 
String methodName = request.getParameter("methodName");
String PolicyNo1 = request.getParameter("PolicyNo1");//保单号
String ManageCom1 = request.getParameter("ManageCom1");//管理机构

TransferData transferData = new TransferData();//把页面传递过来的值打包到TransferData对象中
transferData.setNameAndValue("PolicyNo1", PolicyNo1);//保单号
transferData.setNameAndValue("ManageCom1", ManageCom1);//管理机构

//VData是一个容器继承了Vector 相当于list   
VData tVData = new VData();
tVData.add(transferData);//打包的数据添加到容器中

LPWBContUI  dcodebl = new LPWBContUI();
if (!dcodebl.submitData(tVData, methodName)){ 
	CErrors errors = dcodebl.mErrors;
	Content = " 处理失败，原因是:" + (String) errors.getFirstError();
	FlagStr = "Fail";
}
else
{
    Content = "处理成功!";
    FlagStr = "Succ";
}

 %>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>' , '<%=methodName%>');
	parent.fraInterface.onclkSelBoxclear();//删除后点击Dialog确定清空input框中的数据 
</script>
</html>
