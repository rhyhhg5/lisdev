<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWIndWirteBackMainInit.jsp
//程序功能：人工核保通过之后,如果险种核保结论为变更承保将会自动发送核保通知书,及待客会回复
//创建日期：2006-5-27 16:46
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）

function initInpBox()
{ 
  try
  {  
  	fm.ManageCom.value = manageCom;
  }
  catch(ex)
  {
    alert("在WIndWirteBackMainInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!原因是:"+ex.message);
  }      
}
function initForm()
{
  try
  {
  	initInpBox();
  	initCustomerGrid();
  }
  catch(re)
  {
    alert("UWIndWirteBackMainInit.jsp-->InitForm函数中发生异常:初始化界面错误!原因是:"+re.message);
  }
}
function initCustomerGrid(){
	var iArray = new Array();
  
  try
  {
	  iArray[0]=new Array();																
	  iArray[0][0]="序号";         																			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            																		//列宽
	  iArray[0][2]=10;            																			//列最大值
	  iArray[0][3]=0;              																			//是否允许输入,1表示允许，0表示不允许
	  																
	  iArray[1]=new Array();																
	  iArray[1][0]="工作流任务号";    																     		//列名
	  iArray[1][1]="0px";            																		//列宽
	  iArray[1][2]=200;            																			//列最大值
	  iArray[1][3]=3;              																			//是否允许输入,1表示允许，0表示不允许 
	  																
	  iArray[2]=new Array();																
	  iArray[2][0]="工作流子任务号";  																       		//列名
	  iArray[2][1]="0px";            																		//列宽
	  iArray[2][2]=200;            																			//列最大值
	  iArray[2][3]=3;              																			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[3]=new Array();																
	  iArray[3][0]="合同号";  																       		//列名
	  iArray[3][1]="0px";            																		//列宽
	  iArray[3][2]=200;            																			//列最大值
	  iArray[3][3]=3;              																			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[4]=new Array();																
	  iArray[4][0]="印刷号";         																		//列名
	  iArray[4][1]="65px";           																 		//列宽
	  iArray[4][2]=170;            																			//列最大值
	  iArray[4][3]=0;              																			//是否允许输入,1表示允许，0表示不允许
	  																
	  iArray[5]=new Array();																
	  iArray[5][0]="管理机构";        																 		//列名
	  iArray[5][1]="50px";           																 		//列宽
	  iArray[5][2]=100;            																			//列最大值
	  iArray[5][3]=0;              																			//是否允许输入,1表示允许，0表示不允许

	  iArray[6]=new Array();																
	  iArray[6][0]="投保人";        																 		//列名
	  iArray[6][1]="70px";            																		//列宽
	  iArray[6][2]=200;            																			//列最大值
	  iArray[6][3]=0;
	  																
	  iArray[7]=new Array();																
	  iArray[7][0]="申请日期";        																 		//列名
	  iArray[7][1]="70px";            																		//列宽
	  iArray[7][2]=200;            																			//列最大值
	  iArray[7][3]=0;              																			//是否允许输入,1表示允许，0表示不允许
	  																
	  iArray[8]=new Array();																
	  iArray[8][0]="下发日期";         																		//列名
	  iArray[8][1]="70px";           																 		//列宽
	  iArray[8][2]=100;            																			//列最大值
	  iArray[8][3]=0;              																			//是否允许输入,1表示允许，0表示不允许
	  																
	  iArray[9]=new Array();																
	  iArray[9][0]="业务员代码";      																   		//列名
	  iArray[9][1]="80px";            																		//列宽
	  iArray[9][2]=200;            																			//列最大值
	  iArray[9][3]=0;              																			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();																
	  iArray[10][0]="通知书下发日期";      																   		//列名
	  iArray[10][1]="80px";            																		//列宽
	  iArray[10][2]=200;            																			//列最大值
	  iArray[10][3]=0;              																			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[11]=new Array();																
	  iArray[11][0]="回销日期";      																   		//列名
	  iArray[11][1]="80px";            																		//列宽
	  iArray[11][2]=200;            																			//列最大值
	  iArray[11][3]=0;              																			//是否允许输入,1表示允许，0表示不允许 
	  																
	  CustomerGrid = new MulLineEnter( "fm", "CustomerGrid" ); 
	  //这些属性必须在loadMulLine前																
	  CustomerGrid.mulLineCount = 10;   																
	  CustomerGrid.displayTitle = 1;																
	  CustomerGrid.locked = 1;																
	  CustomerGrid.canSel = 1;																
	  CustomerGrid.canChk = 0;																
	  CustomerGrid.hiddenSubtraction = 1;																
	  CustomerGrid.hiddenPlus = 1;																
	  CustomerGrid.loadMulLine(iArray);  																
	  
	  
	  //这些操作必须在loadMulLine后面
	  //GrpGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert(ex.message);        
  }
}
</script>