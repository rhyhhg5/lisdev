<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ViewErrLogBonusAssign.jsp
//程序功能：分红处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="ViewErrLogGrpBonusAssign.js"></SCRIPT>
  	<%@include file="ViewErrLogGrpBonusAssignInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>查询条件</title>
</head>
<body onload="initErrLogBonusGrid()">
  <form method=post name=fm target="fraSubmit" action= "">
    <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>
            团体保单号
          </TD>
          <TD  class= input>
            <Input class=common name=GrpPolNo>
          </TD>
			<TD  class= title>
	     	操作类型	     
	        </TD>
	        <TD  class= input>
	          <Input class=common name=Type >
	        </TD>                    
        </TR>
        <TR  class= common>
          <TD  class= title>
            会计年度
          </TD>
          <TD  class= input>
            <Input class=common name=FiscalYear>
          </TD>  
          <TD  class= title>
            入机时间
          </TD>
          <TD  class= input>
            <Input class=common name=MakeDate>
          </TD>         
        </TR>
    </table>
  <p>
   <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 	
  </p>
 
  	<Div  id= "divErrLogBonus" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanErrLogBonusGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class = common TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页"  class = common TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页"  class = common TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页"  class = common TYPE=button onclick="turnPage.lastPage();">				
  	</div>  
  
 </form>
 
 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>
