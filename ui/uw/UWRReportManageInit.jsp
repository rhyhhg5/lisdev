<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.util.*"%>
  <%@page import="java.lang.Math.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String tContNo = "";
  String tPrtSeq = "";
  String tCustomerno = "";
  String tFlag = "";
  String tUWIdea = "";  
  String tPrtNo = "";
  String tAppntno = "";
  Date today = new Date();
  today = PubFun.calDate(today,15,"D",null);
  String tday = UWPubFun.getFixedDate(today);
  tContNo = request.getParameter("ContNo");
  tPrtSeq = request.getParameter("PrtSeq");
  tCustomerno = request.getParameter("Customerno");
  tPrtNo = request.getParameter("PrtNo");
  tAppntno = request.getParameter("Appntno");
 %>                            

<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox()
{ 
try
  {                                  
    fm.all('ContNo').value = "";
    fm.all('PrtSeq').value = '';
    fm.all('Customerno').value = '';
    fm.all('Content').value = '';
   	fm.all('PrtNo').value = "" ;
   	fm.all('Appntno').value = "" ;

  }
  catch(ex)
  {
    alert("在UWManuHealthInit.jsp-->InitInpBox函数中发生异常:初始化界面错误1!");
  }   
}


function initForm(tContNo,tPrtSeq,tCustomerno,tPrtNo,tAppntno)
{
  try
  {
  	//alert(tContNo);
	initInpBox();
	initUWHealthGrid();
	initHide(tContNo,tPrtSeq,tCustomerno,tPrtNo,tAppntno);
  initUWErrGrid();
	easyQueryClick();
 }
  catch(re) {
    alert("UWRReportManageInit.jsp-->InitForm函数中发生异常:初始化界面错误3!");
  }
  
}

function initUWErrGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="契约调查对象";    	//列名
      iArray[1][1]="130px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="契约调查项目";         			//列名
      iArray[2][1]="130px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="补充说明";         		//列名
      iArray[3][1]="220px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="契约调查项目结果";         		//列名
      iArray[4][1]="310px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="契约调查项目编号";         		//列名
      iArray[5][1]="0px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      UWErrGrid = new MulLineEnter( "fm" , "UWErrGrid" ); 
      //这些属性必须在loadMulLine前
      UWErrGrid.mulLineCount = 0;
      UWErrGrid.displayTitle = 1;
      UWErrGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      UWErrGrid.hiddenSubtraction=1;
      UWErrGrid.canSel = 1;//radio
      
      UWErrGrid.loadMulLine(iArray);  
      }
      
      catch(ex)
      {
        alert(ex);
      }
}

// 契约调查项目明细列表的初始化
function initUWHealthGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50px";         			//列宽
      iArray[0][2]=10;          			  //列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="契约调查对象";    	//列名
      iArray[1][1]="200px";            	//列宽
      iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;  
			
			iArray[2]=new Array();
      iArray[2][0]="契约调查项目";    	    //列名
      iArray[2][1]="200px";            	//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
            
      iArray[3]=new Array();
      iArray[3][0]="补充说明";         	//列名
      iArray[3][1]="400px";            	//列宽
      iArray[3][2]=100;            			//列最大值
			iArray[3][3]=0

      iArray[4]=new Array();
      iArray[4][0]="契约调查项目结果";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="契约调查项目编号";         		//列名
      iArray[5][1]="0px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许



      HealthGrid = new MulLineEnter( "fm" , "HealthGrid" ); 
      //这些属性必须在loadMulLine前                            
      HealthGrid.mulLineCount = 0;
      HealthGrid.displayTitle = 1;
      HealthGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      HealthGrid.hiddenSubtraction=1;
      HealthGrid.canChk = 0;
      HealthGrid.loadMulLine(iArray);  
      //这些操作必须在loadMulLine后面
      //HealthGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initHide(tContNo,tPrtSeq,tCustomerno,tPrtNo,tAppntno)
{
	fm.all('ContNO').value = tContNo;
  fm.all('PrtSeq').value = tPrtSeq;
	fm.all('Customerno').value = tCustomerno ;
	fm.all('PrtNo').value = tPrtNo ;
	fm.all('Appntno').value = tAppntno ;
	
}

</script>


