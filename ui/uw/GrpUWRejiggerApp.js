//程序名称：GrpUWRejiggerApp.js
//程序功能：团体既往保全函数
//创建日期：2005-4-26
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容


var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var cGrpContNo;
var PrtNo;

var strSQL;
var mulLineC;

//查询作业历史(团单)
function queryPEndorItemGrid()
{
    var tStrSql = ""
        + " select lpea.EdorAcceptNo, lpgei.GrpContNo, lmei.EdorCode || '-' || lmei.EdorName EdorName, lpea.ConfDate, "
        + " (case lpea.EdorState when '0' then '已结案' else '未结案' end) EdorState, "
        + " '--' ContPlanCode, "
        + " lpgei.GetMoney, "
        + " '--' ChangePeoples "
        + " from LPEdorApp lpea "
        + " inner join LPGrpEdorItem lpgei on lpgei.EdorNo = lpea.EdorAcceptNo "
        + " inner join LMEdorItem lmei on lmei.EdorCode = lpgei.EdorType and lmei.AppObj = 'G' "
        + " where 1 = 1 "
        + " and lpea.OtherNo = '" + mAppntNo + "' "
        + " and lpgei.EdorType not in ('ZT', 'NI') "
        + " union all "
        + " select lpea.EdorAcceptNo, lpgei.GrpContNo, lmei.EdorCode || '-' || lmei.EdorName EdorName, lpea.ConfDate, "
        + " (case lpea.EdorState when '0' then '已结案' else '未结案' end) EdorState, "
        + " lcil.ContPlanCode, "
        + " nvl(sum(ljage.GetMoney), 0) GetMoney, "
        + " char(integer(Count(distinct lcil.InsuredNo))) ChangePeoples "        
        + " from LPEdorApp lpea "
        + " inner join LPGrpEdorItem lpgei on lpgei.EdorNo = lpea.EdorAcceptNo "
        + " inner join LMEdorItem lmei on lmei.EdorCode = lpgei.EdorType and lmei.AppObj = 'G' "
        + " inner join LCInsuredList lcil on lcil.GrpContNo = lpgei.GrpContNo and lcil.EdorNo = lpgei.EdorNo "
        + " inner join LJAGetEndorse ljage on ljage.EndorsementNo = lpgei.EdorNo and ljage.GrpContNo = lpgei.GrpContNo and lcil.InsuredNo = ljage.InsuredNo "
        + " where 1 = 1 "
        + " and lpea.OtherNo = '" + mAppntNo + "' "
        + " and lpgei.EdorType in ('NI') "
        + " group by lpea.EdorAcceptNo, lpgei.GrpContNo, lmei.EdorCode || '-' || lmei.EdorName, lpea.ConfDate, "
        + " (case lpea.EdorState when '0' then '已结案' else '未结案' end), "
        + " lcil.ContPlanCode "
        + " union all "
        + " select lpea.EdorAcceptNo, lpgei.GrpContNo, lmei.EdorCode || '-' || lmei.EdorName EdorName, lpea.ConfDate, "
        + " (case lpea.EdorState when '0' then '已结案' else '未结案' end) EdorState, "
        + " lpi.ContPlanCode, "
        + " nvl(sum(ljage.GetMoney), 0) GetMoney, "
        + " char(integer(Count(distinct lpi.InsuredNo))) ChangePeoples "
        + " from LPEdorApp lpea "
        + " inner join LPGrpEdorItem lpgei on lpgei.EdorNo = lpea.EdorAcceptNo "
        + " inner join LMEdorItem lmei on lmei.EdorCode = lpgei.EdorType and lmei.AppObj = 'G' "
        + " inner join LPInsured lpi on lpi.GrpContNo = lpgei.GrpContNo and lpi.EdorNo = lpgei.EdorNo and lpi.EdorType = 'ZT' "
        + " inner join LJAGetEndorse ljage on ljage.EndorsementNo = lpgei.EdorNo and ljage.GrpContNo = lpgei.GrpContNo and lpi.InsuredNo = ljage.InsuredNo "
        + " where 1 = 1 "
        + " and lpea.OtherNo = '" + mAppntNo + "' "
        + " and lpgei.EdorType in ('ZT') "
        + " group by lpea.EdorAcceptNo, lpgei.GrpContNo, lmei.EdorCode || '-' || lmei.EdorName, lpea.ConfDate, "
        + " (case lpea.EdorState when '0' then '已结案' else '未结案' end), "
        + " lpi.ContPlanCode "
        + " order by EdorAcceptNo, GrpContNo, EdorName, ConfDate, ContPlanCode "
        + " with ur "
        ;

    turnPage1.pageDivName = "divbqViewGridPage";
    turnPage1.queryModal(tStrSql, bqViewGrid);

    if (!turnPage1.strQueryResult)
    {
        alert("没有既往保全信息！");
        return false;
    }
    
    return true;
}

/* 查看扫描 */
function queryBQScanner()
{
    var tRow = bqViewGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = bqViewGrid.getRowData(tRow);
    
    var tWorkNo = tRowDatas[0];
    var tEdorState = tRowDatas[4];
    
    if(tWorkNo == null || tWorkNo == "")
    {
        alert("保全受理号不存在！");
        return false;
    }
    
    if(tEdorState != "已结案")
    {
        alert("该工单尚未结案，不存在批单。");
        return false;
    }
    
    window.open("../f1print/GrpEndorsementF1PJ1.jsp?EdorNo=" + tWorkNo);
    
    return true;
}


/*********************************************************************
 *  既往询价信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAskApp()
{
  window.open("../uw/GrpUWAskCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往保全信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showRejiggerApp()
{
  window.open("../uw/GrpUWRejiggerCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往理陪信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showInsuApp()
{
  window.open("../uw/GrpUWInsuCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往保障信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showEnsureCont()
{
  window.open("../uw/GrpUWEnsureCont.jsp?pmGrpContNo="+mGrpContNo+"&pmAppntNo="+mAppntNo+"","window2");
}

/*********************************************************************
 *  既往承保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSignContH()
{
    window.open("../uw/GrpUWHistoryCont.jsp?pmGrpContNo=" + mGrpContNo + "&pmAppntNo=" + mAppntNo + "", "window2");
}


/**
 * 关闭当前查询页面。
 */
function GoBack()
{
  top.close();
}


function easyQueryI()
{
  // 书写SQL语句
  var arrRs;
  var strSQL = "select GrpName";
  strSQL    += "  from LDGrp";
  strSQL    += " where CustomerNo='"+mAppntNo+"'";

  fm.all("GrpNo").value=mAppntNo;

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //查询成功设置画面上的数据
  if(turnPage.strQueryResult)
  {
    arrRs = decodeEasyQueryResult(turnPage.strQueryResult);
    fm.all("GrpName").value=arrRs[0][0];
  }
}

