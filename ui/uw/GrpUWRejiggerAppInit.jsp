<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWAppInit.jsp
//程序功能：既往投保信息查询
//创建日期：2002-06-19 11:10:36
//创建人  ： WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
  
<%
    String tAppntNo = request.getParameter("pmAppntNo");
    String tGrpContNo = request.getParameter("pmGrpContNo");
%>
                         
<script>
var mAppntNo = "<%=tAppntNo%>";
var mGrpContNo = "<%=tGrpContNo%>";


function initForm()
{
	initbqViewGrid();
    easyQueryI();
}


function initbqViewGrid()
{
    var iArray = new Array();
    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=10;
        iArray[0][3]=0;
        
        iArray[1]=new Array();
        iArray[1][0]="保全受理号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="保单合同号";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="变更项目";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="保全时间";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="保全状态";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;  
        
        iArray[6]=new Array();
        iArray[6][0]="保障计划";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;    
        
        iArray[7]=new Array();
        iArray[7][0]="涉及金额";
        iArray[7][1]="100px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="涉及人数";
        iArray[8][1]="100px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        bqViewGrid = new MulLineEnter("fm", "bqViewGrid");
        
        bqViewGrid.mulLineCount = 0;
        bqViewGrid.displayTitle = 1;
        bqViewGrid.canSel =1;
        bqViewGrid.canChk =0;
        bqViewGrid.hiddenPlus=1;
        bqViewGrid.hiddenSubtraction=1;
        bqViewGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert(ex);
    }
    queryPEndorItemGrid();
}
</script>

