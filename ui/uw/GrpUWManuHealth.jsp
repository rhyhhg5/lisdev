<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWManuHealth.jsp
//程序功能：承保人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./GrpUWManuHealth.js"></SCRIPT>
  <SCRIPT> 
  	var PRTNO = "<%=request.getParameter("PrtNo")%>";
  	</SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpUWManuHealthInit.jsp"%>
  <title> 承保体检资料录入 </title>
  
</head>
<body  onload="initForm('<%=tContNo%>','<%=tPrtNo%>');" >
  <form method=post name=fm target="fraSubmit" action= "./GrpUWManuHealthChk.jsp">
    <!-- 非列表 -->
    <table>
    	<TR  class= common>
          <TD  class= title id="ff" style="display:'none'">  合同号码  </TD>
          <TD  class= input id="ff" style="display:'none'"> <Input class="readonly" name=ContNo readonly> </TD>
          <INPUT  type= "hidden" class= Common name= ProposalContNo value= "">
           <INPUT  type= "hidden" class= Common name= MissionID value= ""><!-- 工作流任务编码 -->
           <INPUT  type= "hidden" class= Common name= SubMissionID value= "">
           <INPUT  type= "hidden" class= Common name= PrtNo value= "">
          <TD  class= title id="ff" style="display:'none'">  打印状态 </TD>
          <TD  class= inputid="ff" style="display:'none'">  <Input class="readonly" name=PrintFlag > </TD>
          <TD  class= title>  体检人  </TD>
          <TD  class= input> <Input class=Common name=InsureName readonly> <!-- verify="体检人|code:InsureNo" ondblClick="showCodeListEx('InsureNo',[this,''],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('InsureNo',[this,''],[0,1],null,null,null,1);" onFocus= "easyQueryClickSingle();"-- onFocus= "easyQueryClickSingle();easyQueryClick();"--> 
          <Input class="readonly" name=InsureNo type=hidden>
          </TD>

          
          <TD  class= title> 体检日期   </TD>
          <TD  class= input>  <Input class=Common dateFormat="short" name=EDate readonly >  </TD>
          <TD  class= title> 体检医院  </TD>
          <TD  class= input>  <Input class=code name=Hospital verify="体检医院|code:Hospital" ondblclick="showCodeListEx('Hospital',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('Hospital',[this,''],[0,1]);">  </TD>
          <TD  class= title> 是否空腹 </TD>
          <TD  class= input>   <Input class=code name=IfEmpty verify="是否空腹|code:YesNo" ondblclick="return showCodeList('YesNo',[this]);" onkeyup="return showCodeListKey('YesNo',[this]);">  </TD>
        </TR>
       
    </table>
    <table>
    	<tr>
        	<td class=common>    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divUWSpec1);"></td>
    		<td class= titleImg>	 体检项目录入</td>                            
    	</tr>	
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanHealthGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
      </div>
     <table class=common>
         <TR  class= common> 
           <TD  class= common> 其他体检信息 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Note" cols="120" rows="3" class="common" >
             </textarea>
           </TD>
         </TR>
      </table>
      <INPUT type= "button" name= "sure" value="确  认" class=Cssbutton onclick="submitForm()">			
		
    <!--读取信息-->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>