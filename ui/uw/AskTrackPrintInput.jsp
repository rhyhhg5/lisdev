<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="AskTrackPrint.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AskTrackPrintInit.jsp"%>
  <title>打印询价跟踪通知书 </title>   
</head>
<body  onload="initForm();" >
  <form action="./BodyCheckPrintQuery.jsp" method=post name=fm target="fraSubmit">
    <!-- 投保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TR  class= common>
          <TD  class= title>  团体保单号码   </TD>
          <TD  class= input>  <Input class= common name=GrpContNo > </TD>
          <TD  class= title>  投保单印刷号   </TD>
          <TD  class= input>  <Input class= common name=PrtNo > </TD>
          <TD  class= title>   管理机构  </TD>
          
          <TD  class= input>  
          	<Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          	  </TD>   
         </TR> 
         <TR  class= common>
         <TD  class= title> 业务员代码 </TD>
          <TD  class= input>  
          	<Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCode',[this,AgentCodeName],[0,1]);" onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName],[0,1]);"><input class=codename name=AgentCodeName readonly=true >
          	   </TD> 
        </TR> 
        </TR>     
    </table>
          <INPUT VALUE="查  询" class= CssButton TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./AskTrackPrintSave.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont1);">
    		</td>
    		<td class= titleImg>
    			 询价跟踪信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCCont1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
			<INPUT VALUE="首  页" class= CssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= CssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= CssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= CssButton TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<p>
      <INPUT VALUE="打印询价跟踪通知书" class= CssButton TYPE=button onclick="printPol();"> 
  	</p>  	
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="GrpContNo" name="GrpContNo">
  	<input type=hidden id="PrtSeq" name="PrtSeq">
  	<input type=hidden id="MissionID" name="MissionID">
  	<input type=hidden id="SubMissionID" name="SubMissionID">
  	<input type=hidden id="PrtNo" name="PrtNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
