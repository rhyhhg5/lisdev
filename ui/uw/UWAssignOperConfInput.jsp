<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
GlobalInput tGI = new GlobalInput();
tGI = (GlobalInput)session.getValue("GI");
tGI.ManageCom = "86";
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>"; 	//记录管理机构
    var comcode = "<%=tGI.ComCode%>";		//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<script src="UWAssignOperConfInput.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="UWAssignOperConfInit.jsp" %>
<title>核保师机构配置</title>
</head>
<body onload="initForm();">
	<form action="UWAssignOperConfSave.jsp" method="post" name="fm" target="fraSubmit">
		<table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFFIBaseInfo);">
                </td>
                <td class="titleImg">添加核保师机构信息</td>
            </tr>
           </table>
		<table class="common">
			<TR class=common>
				<TD class=title>核保师</TD>
				<TD class="input"><Input class= 'codeno' name=UWOperator1  verify="核保师|NOTNULL&len<=10&code:uwusercode" ondblclick="return showCodeList('uwusercode',[this,UserCodeName1],[0,1],null,'1 and #1# = #1# and comcode = #86# and UserState=#0# ','1',1);" onkeyup="return showCodeListKey('uwusercode',[this,UserCodeName1],[0,1],null,null,null,null,1);"><input  class="codename" name="UserCodeName1" ></TD> 
				<TD class=title>管理机构</TD>
				<TD class="input"><Input class=codeNo name=ManageCom1 verify="管理机构|NOTNULL&code:comcode" ondblclick="return showCodeList('comcode',[this,ManageComName1],[0,1],null,'1 and #1#=#1# and length(trim(comcode))=4 ','1');" onkeyup="return showCodeListKey('comcode',[this,ManageComName1],[0,1]);"><input class=codename name=ManageComName1 readonly=true ></TD>
			</TR>
		</table>
		<br>
		<input type="hidden" name="methodName" value="" />
    	<INPUT VALUE="添  加" class= cssButton TYPE=button onclick="insertCode();">
        <br><br><hr>
        <table>
            <tr>
                <td>
                    <img src="../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFFIBaseInfo);">
                </td>
                <td class="titleImg">查询核保师机构信息</td>
            </tr>
        </table>
        <div id="divComCode" style= "display:''">
            <table class="common" align='center' >
                <tr class="common">
				<td class="title">核保师</td>
				<td><Input class= 'codeno' name=UWOperator  verify="分配给核保师|len<=10&code:uwusercode" ondblclick="return showCodeList('uwusercode',[this,UserCodeName],[0,1],null,'1 and #1# = #1# and comcode = #86# and UserState=#0# ','1',1);" onkeyup="return showCodeListKey('uwusercode',[this,UserCodeName],[0,1],null,null,null,null,1);"><input  class="codename" name="UserCodeName" ></td> 
			    </tr>
            </table>
        </div>
        <input value="查  询" class="cssButton" type="button" onclick="easyQueryClick2();">
        <p />
        
         <Div  id= "divComCodeAndIpGrid" align=center style= "display: ''">
        <Table  class= common>
            <TR  class= common>
             <TD text-align: left colSpan=1>
                 <span id="spanPolGrid" ></span> 
       	    </TD>
           </TR>
         </Table>					
           <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
           <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
           <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
           <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>	
    </form>
    <br><br><hr>
	<form action="UWAssignOperConfSave.jsp" method="post" name="fmUpdate"
		target="fraSubmit">
		<div id="divUpdate" style="display: 'none'">
			<table class="common" align='center'>
				<tr class="common">
					<td class="title">核保师</td>
					<td class=input><Input class= 'codeno' name=upUWOperator readonly=true><input  class="codename" name="upUWOperatorName" readonly=true></td> 
					<TD class=title>管理机构</TD>
					<TD class=input><Input class= 'codeno' name=upComCode readonly=true><input  class="codename" name="upComCodeName" readonly=true></td>
					</TD>
				</tr>
			</table>
			<input type="hidden" name="methodName" value="" /> <input
				value="修  改" class="cssButton" type="hidden"
				onclick="updateCode();"> <input value="删  除"
				class="cssButton" type="button" onclick="deleteCode();">
		</div>
	</form>
	<span id="spanCode"  style="display:none; position:absolute; slategray"></span>
</body>
</html>