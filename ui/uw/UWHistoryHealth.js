//程序名称：PEdorUWManuHealth.js
//程序功能：保全人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug = "0";
var flag;
var turnPage = new turnPageClass();

// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

// 显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}

function manuchkhealthmain() {
	fm.submit();
}

// 查询按钮
function easyQueryClickSingle() {
	// 书写SQL语句
	var strsql = "";
	var tContNo = "";
	var tEdorNo = "";
	tContNo = fm.ContNo.value;
	if (tContNo != "") {

		strsql = "select LCPENotice.ContNo,LCPENotice.PrtSeq,LCPENotice.CustomerNo,LCPENotice.Name,LCPENotice.PEDate,(select max(makedate) from es_doc_main where es_doc_main.doccode = LOPRTManager.prtseq),LOPRTManager.StateFlag from LCPENotice,LOPRTManager where 1=1"
				+ " and LCPENotice.PrtSeq = LOPRTManager.PrtSeq"
				+ " and proposalContNo = '" + tContNo + "'";

		turnPage.queryModal(strsql, MainHealthGrid);
	}
	return true;

}

// 查询按钮
function easyQueryClick(parm1, parm2) {

	initDisDesbGrid();
	// 书写SQL语句
	var strsql = "";
	var tContNo = "";
	var tEdorNo = "";
	tContNo = fm.ContNo.value;
	var tSelNo = MainHealthGrid.getSelNo() - 1;
	var tPrtSeq = MainHealthGrid.getRowColData(tSelNo, 2);
	var tInsurdNo = MainHealthGrid.getRowColData(tSelNo, 3);
	fm.InsureNo.value = tInsurdNo;
	if (tContNo != "" && tPrtSeq != "") {
		strsql = "select peitemcode,peitemname from LCPENoticeItem where 1=1"

		+ " and proposalContNo = '" + tContNo + "'" + " and PrtSeq = '" + tPrtSeq + "'";

		// 查询其他体检信息
		var strSQL = "select Remark from LCPENotice where 1=1"
				+ " and proposalContNo = '" + tContNo + "'" + " and PrtSeq = '"
				+ tPrtSeq + "'";

		var arrReturn = new Array();
		arrReturn = easyExecSql(strSQL);

		fm.all('Note').value = arrReturn[0];

		// 查询SQL，返回结果字符串
		turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

		// 判断是否查询成功
		if (!turnPage.strQueryResult) {
			return "";
		}

		// 查询成功则拆分字符串，返回二维数组
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

		// 设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
		turnPage.pageDisplayGrid = HealthGrid;

		// 保存SQL语句
		turnPage.strQuerySql = strsql;

		// 设置查询起始位置
		turnPage.pageIndex = 0;

		// 在查询结果数组中取出符合页面显示大小设置的数组
		var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
				turnPage.pageIndex, MAXSCREENLINES);

		// 调用MULTILINE对象显示查询结果
		displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	}
	var strSql = "select DisDesb,DisResult,ICDCode from LCPENoticeResult where ProposalContNo='"
			+ tContNo + "' and prtseq='" + tPrtSeq + "'";
	var arr = easyExecSql(strSql);
	if (arr) {
		turnPage.queryModal(strSql, DisDesbGrid);
	}
	return true;
}

// 无体检资料查询按钮
function easyQueryClickInit() {
	fm.action = "./UWManuHealthQuery.jsp";
	fm.submit();
}

function displayEasyResult(arrResult) {
	var i, j, m, n;
	if (arrResult == null)
		alert("没有找到相关的数据!");
	else {
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for (i = 0; i < n; i++) {
			m = arrResult[i].length;
			for (j = 0; j < m; j++) {
				HealthGrid.setRowColData(i, j + 1, arrResult[i][j]);

			}
		}
	}

}

function initInsureNo(tPrtNo) {
	var i, j, m, n;
	var returnstr;

	var strSql = "select InsuredNo,name from lcinsured where 1=1 "
			+ " and Prtno = '"
			+ tPrtNo
			+ "'"
			+ " union select CustomerNo , Name from LCInsuredRelated where polno in (select polno from lcpol where Prtno = '"
			+ tPrtNo + "')";
	// 查询SQL，返回结果字符串

	turnPage.strQueryResult = easyQueryVer3(strSql, 1, 1, 1);

	// 判断是否查询成功
	// alert(turnPage.strQueryResult);
	if (turnPage.strQueryResult == "") {
		alert("查询失败！");
		return "";
	}

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	var returnstr = "";
	var n = turnPage.arrDataCacheSet.length;
	// alert("N:"+n);
	if (n > 0) {
		for (i = 0; i < n; i++) {
			m = turnPage.arrDataCacheSet[i].length;
			// alert("M:"+m);
			if (m > 0) {
				for (j = 0; j < m; j++) {
					if (i == 0 && j == 0) {
						returnstr = "0|^" + turnPage.arrDataCacheSet[i][j];
					}
					if (i == 0 && j > 0) {
						returnstr = returnstr + "|"
								+ turnPage.arrDataCacheSet[i][j];
					}
					if (i > 0 && j == 0) {
						returnstr = returnstr + "^"
								+ turnPage.arrDataCacheSet[i][j];
					}
					if (i > 0 && j > 0) {
						returnstr = returnstr + "|"
								+ turnPage.arrDataCacheSet[i][j];
					}

				}
			} else {
				alert("查询失败!!");
				return "";
			}
		}
	} else {
		alert("查询失败!");
		return "";
	}
	fm.InsureNo.CodeData = returnstr;
	return returnstr;
}

// 初始化医院
function initHospital(tContNo) {
	var i, j, m, n;
	var returnstr;

	alert(tContNo);
	var strSql = "select code,codename from ldcode where 1=1 "
			+ "and codetype = 'hospitalcodeuw'"
			+ "and comcode = (select managecom from lccont where ContNo = '"
			+ tContNo + "')";
	// 查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSql, 1, 1, 1);

	// 判断是否查询成功
	if (turnPage.strQueryResult == "") {
		alert("医院初始化失败！");
		return "";
	}

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	var returnstr = "";
	var n = turnPage.arrDataCacheSet.length;
	if (n > 0) {
		for (i = 0; i < n; i++) {
			m = turnPage.arrDataCacheSet[i].length;
			if (m > 0) {
				for (j = 0; j < m; j++) {
					if (i == 0 && j == 0) {
						returnstr = "0|^" + turnPage.arrDataCacheSet[i][j];
					}
					if (i == 0 && j > 0) {
						returnstr = returnstr + "|"
								+ turnPage.arrDataCacheSet[i][j];
					}
					if (i > 0 && j == 0) {
						returnstr = returnstr + "^"
								+ turnPage.arrDataCacheSet[i][j];
					}
					if (i > 0 && j > 0) {
						returnstr = returnstr + "|"
								+ turnPage.arrDataCacheSet[i][j];
					}

				}
			} else {
				alert("查询失败!!");
				return "";
			}
		}
	} else {
		alert("查询失败!");
		return "";
	}
	fm.Hospital.CodeData = returnstr;
	return returnstr;
}

function initBooking() {
}