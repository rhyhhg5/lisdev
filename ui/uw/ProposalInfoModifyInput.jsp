<%
//程序名称：ProposalQueryState.jsp
//程序功能：新契约信息修改
//创建日期：2002-11-23 17:06:57
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
  //个人下个人
		
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var prtNo = "<%=request.getParameter("prtNo")%>";
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var agentComCode = "agentcombank";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	
  <SCRIPT src="ProposalInfoModify.js"></SCRIPT>
  <%@include file="ProposalInfoModifyInit.jsp"%>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>投保件状态明细</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraTitle">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>合同信息</td>
		</tr>
	</table>
    <table class= common align=center>
    	<TR class= common>
    		<TD  class= title>  印刷号码  </TD> <TD  class= input> <Input class=common name=PrtNo1  readonly> </TD>          
	        <TD  class= title>  管理机构  </TD> <TD  class= input> <Input class=common name=ManageCom readonly></TD>       
            <TD  class= title> 业务员代码 </TD>
            <TD  class= input><input NAME="GroupAgentCode" VALUE MAXLENGTH="0" CLASS="code" elementtype=nacessary ondblclick="return queryAgentCode();" onblur="">
			<input name="AgentCode" MAXLENGTH="0" class="code" elementtype=nacessary type='hidden'></TD>
        </TR>
    	<tr class= common>
    		<td  class= title>  销售渠道 </td>
    		<td  class= input><input name="SaleChnl" MAXLENGTH="2" class=codeNo ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" verify="销售渠道|code:LCSaleChnl&notnull"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary></td>          
	        <td  class= title id="AgentComTitleID" style="display: none">  代理机构 </td>
	        <td  class= input id="AgentComInputID" style="display: none"><input name="AgentCom" class="code" ondblclick="return showCodeList(agentComCode,[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" onkeyup="return showCodeListKey(agentComCode,[this,AgentComName],[0,1],null, fm.all('ManageCom').value, 'ManageCom');" verify="代理机构|code:AgentCom"><Input type='hidden' name=AgentComName readonly ></td>
	    </tr>
    </table>
    <input name="AgentGroup" class="common" type="hidden" readonly TABINDEX="-1" MAXLENGTH="12" verify="代理人组别|notnull">
    <input name="operate" type="hidden">
    <input class=cssButton value="修  改" type=button onclick="modifyCont();" style="float: right">
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>投保人信息</td>
		</tr>
	</table>
    <table  class= common align=center>
    	<TR  class= common>
    		 <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=AppntName elementtype=nacessary verify="投保人姓名|notnull&len<=20" >
          </TD>
    	    <TD  class= title>
             证件类型
          </TD>
          <TD  class= input>
            <Input class=codeNo name="AppntIDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input colspan=1>
            <Input class= common3 name="AppntIDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
          </TD>   
      </TR>  
          
      <TR class = common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="AppntPostalAddress" verify="投保人联系地址|len<=80" >
          </TD>
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common name="AppntZipCode" verify="投保人邮政编码|zipcode" >
          </TD>
       </TR>
       <TR>
          <TD class=title>
        	住宅电话
        	</TD>
        	<TD class=input>
        	 <Input class=common name="HomePhone" verify="住宅电话|int">
        	</TD>
        	<TD class=title>
        	移动电话
        	</TD>
        	<TD class=input>
        	<Input class=common name="Mobile" verify="移动电话|int">
        	</TD>
      </TR>           
    </table>
     
		    <INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="modifyAppnt();" style="float: right">      
	
    
   <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>被保险人信息</td>
	  	</tr>
	 </table>
     <Div  id= "divTempFeeInput" style= "display: ''">
    	<Table  class= common>
    		<tr>
    			<td text-align: left colSpan=1>
	  				<span id="spanInsuredGrid" >
	  				</span> 
	  			</td>
    		</tr>
    	</Table>
    </div>
    <Input class= mulinetitle value="第一被保险人资料"  name=InsuredSequencename readonly >（客户号：<Input class= common name=InsuredNo readonly>
  <table  class= common align=center>
    	<TR  class= common>
    		 <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=InsuredName elementtype=nacessary verify="投保人姓名|notnull&len<=20" >
          </TD>
    	    <TD  class= title>
             证件类型
          </TD>
          <TD  class= input>
            <Input class=codeNo name="InsuredIDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this,InsuredIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,InsuredIDTypeName],[0,1]);"><input class=codename name=InsuredIDTypeName readonly=true elementtype=nacessary>    
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input colspan=1>
            <Input class= common3 name="InsuredIDNo" onblur="checkidtype();getBirthdaySexByIDNo(this.value);"  verify="投保人证件号码|len<=20" >
          </TD>   
      </TR>  
          
         
    </table>
    
      <INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="modifyInsured();" style="float: right">      
    <!--被保险人险种信息-->
    <DIV id=DivLCPol STYLE="display:''">
  <table>
      <tr>
         <td class=common>
         <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
         </td>
         <td class= titleImg>
         被保险人险种信息
        </td>
     </tr>
</table>

<div  id= "divLCPol1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanPolGrid" >
</span>
</td>
</tr>
</table>
</div>
</DIV>


  <!-- 受益人信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCBnf1);">
    		</td>
    		<td class= titleImg>
    			 受益人信息
    		</td>
    	</tr>
    </table>
	<Div  id= "divLCBnf1" style= "display: 'none'" >
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanBnfGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
	
	 <DIV id = "divBnfButton" style = "display:'none'" style="float: right">
     
      <INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="modifyBnf();">      
     
    </DIV>
     <INPUT  type= "hidden" class= Common name= SequenceNo value= "">
     <INPUT  type= "hidden" class= Common name= ContNo value= "">
     <INPUT  type= "hidden" class= Common name= PrtNo value= "">	
     <INPUT  type= "hidden" class= common name= AppntNo value="">
     	<INPUT type= "hidden" class= common name=PolNo value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>
