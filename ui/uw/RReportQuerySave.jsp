<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuHealthQChk.jsp
//程序功能：人工核保体检资料查询
//创建日期：2005-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  String tGrpContNo = "000000000000";
 	String tContNo = request.getParameter("ContNo");
 	String tProposalContNo =request.getParameter("ContNo");
 	String tCustomerNo=request.getParameter("customer");
 	String tPrtSeq=request.getParameter("PrtSeq");
 	String tReplyContente = request.getParameter("ReplyContente");
 	String tContente = request.getParameter("Contente");
 	
 	LCRReportSchema tLCRReportSchema = new LCRReportSchema();
 	LCRReportSet tLCRReportSet = new LCRReportSet();
 	tLCRReportSchema.setReplyContente(tReplyContente);
 	tLCRReportSchema.setProposalContNo(tContNo);
 	tLCRReportSchema.setPrtSeq(tPrtSeq);
 	tLCRReportSchema.setContente(tContente);
 	tLCRReportSchema.setCustomerNo(tCustomerNo);
 	
 	
 	//LCRReportResultSchema tLCRReportResultSchema ;
 	//LCRReportResultSet tLCRReportResultSet = new LCRReportResultSet();
 	//
 	//
	//int lineCount = 0;
	//String arrCount[] = request.getParameterValues("RReportResultGridNo");
  //
	//if(arrCount!=null)
	//{
	//	String tRReportResult[]=request.getParameterValues("RReportResultGrid1");
	//	String tICDcode[]=request.getParameterValues("RReportResultGrid2");
	//	
	//	lineCount = arrCount.length;
	//	
	//	for(int i = 0;i<lineCount;i++)
	//	{
	//		tLCRReportResultSchema = new LCRReportResultSchema();
	//		tLCRReportResultSchema.setGrpContNo(tGrpContNo);
	//		tLCRReportResultSchema.setContNo(tContNo);
	//		tLCRReportResultSchema.setProposalContNo(tProposalContNo);
	//		tLCRReportResultSchema.setPrtSeq(tPrtSeq);
	//		tLCRReportResultSchema.setCustomerNo(tCustomerNo);
	//		tLCRReportResultSchema.setRReportResult(tRReportResult[i]);
	//		tLCRReportResultSchema.setICDCode(tICDcode[i]);
	//		tLCRReportResultSet.add(tLCRReportResultSchema);
	//	}
		
		// 准备传输数据 VData
		VData tVData = new VData();
		FlagStr="";
  	
		tVData.add(tG);
		//tVData.add(tLCRReportResultSet);
		tVData.add(tLCRReportSchema);
		
		RReportResultUI tRReportResultUI = new RReportResultUI();
		
		
		try{
			System.out.println("this will save the data!!!");
			tRReportResultUI.submitData(tVData,"");
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		
		if (!FlagStr.equals("Fail")){
			tError = tRReportResultUI.mErrors;
			if (!tError.needDealError()){
				Content = " 保存成功! ";
				FlagStr = "Succ";
			}
			else{
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	//}
	//else
	//{
	//	FlagStr = "Fail";
	//	Content = "生调信息录入不完整";
	//}
	

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
