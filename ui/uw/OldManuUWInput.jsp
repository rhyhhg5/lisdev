<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：ManuUWInput.jsp.
//程序功能：新契约个人人工核保
//创建日期：2005-12-29 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String tPrtNo = "";
	String tContNo = "";
	String tMissionID = "";
	String tSubMissionID = "";
	String tActivityID = "";
	String tLoadFlag = "";
	tPrtNo = request.getParameter("PrtNo");
	tContNo = request.getParameter("ContNo"); 
	tMissionID = request.getParameter("MissionID");
	tSubMissionID =  request.getParameter("SubMissionID");
  tActivityID =  request.getParameter("ActivityID");
  tLoadFlag =  request.getParameter("LoadFlag");
	session.putValue("ContNo",tContNo);
%>
<html>
<%
  //个人下个人
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var ContNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var MissionID = "<%=tMissionID%>";
	var SubMissionID = "<%=tSubMissionID%>";
	var tActivityID = "<%=tActivityID%>";
	var tLoadFlag = "<%=tLoadFlag%>";
	var PrtNo = "<%=tPrtNo%>";
	var uwgrade = "";
    var appgrade= "";
    var uwpopedomgrade = "";
    var codeSql = "code";
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="ManuUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="OldManuUWInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./ManuUWCho.jsp">
    <!-- 保单查询条件 -->
    <div id="divSearch">
	    <table class= common border=0 width=100%>
	    	<tr>
					<td class= titleImg align= center>请输入查询条件：</td>
				</tr>
	    </table>
	    <table  class= common align=center>
	      	<TR  class= common>
	         <!-- <TD  class= title> 投保单号 </TD>
	          <TD  class= input>--> <Input class= common name=QProposalNo  type="hidden">
	          <TD  class= title>  印刷号码  </TD>
	          <TD  class= input> <Input class=common name=QPrtNo> </TD>          
	          <TD  class= title> 管理机构  </TD>
	          <TD  class= input>  <Input class="code" name=QManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">  </TD>
	        </TR>
	        <TR  class= common>
	          <TD  class= title> 核保级别  </TD>
	          <TD  class= input>  <Input class="code" name=UWGradeStatus value= "1" CodeData= "0|^1|本级保单^2|下级保单" ondblClick="showCodeListEx('Grade',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('Grade',[this,''],[0,1]);">  </TD>
	          <TD  class= title>  投保人名称 </TD>
	          <TD  class= input> <Input class=common name=QAppntName > </TD>  
	          <TD  class= title>  核保人  </TD>
	          <TD  class= input>   <Input class= common name=QOperator value= "">   </TD>     
	        </TR>
	        <TR  class= common>
	          <TD  class= title>  保单状态 </TD>
	          <TD  class= input>  <Input class="code" readonly name=State value= "" CodeData= "0|^1|未人工核保^2|核保已回复^3|核保未回复" ondblClick="showCodeListEx('State',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('State',[this,''],[0,1]);"> </TD>
	        </TR>
	        <TR  class= common>
	          <TD  class= input>   <Input  type= "hidden" class= Common name = QComCode >  </TD>
	        </TR>
	    </table>
	    <INPUT VALUE="查  询" class=cssButton TYPE=button onclick="easyQueryClick();"> 
	    <!--INPUT VALUE="撤单申请查询" class=common TYPE=button onclick="withdrawQueryClick();"--> 
	    <INPUT type= "hidden" name= "Operator" value= ""> 
    </div>
    <!--合同信息-->
	<DIV id=DivLCContButton STYLE="display:'none'">
	<table id="table1">
			<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivLCCont);">
				</td>
				<td class="titleImg">管理信息
				</td>
			</tr>
	</table>
	</DIV>
	<div id="DivLCCont" STYLE="display:'none'">
		<table class="common" id="table2">
			<tr CLASS="common">
				<td CLASS="title">印刷号码 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="PrtNo" VALUE CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="14">
	    		</td>
	    		<td CLASS="title">申请日期 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="PolApplyDate" VALUE CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="14">
	    		</td>
	    	<td CLASS="title">生效日期 
	    		</td>
				<td CLASS="input" COLSPAN="1">
					<input NAME="CValiDate" VALUE CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="14">
	    	</td>
				<td CLASS="title">管理机构 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="ManageCom_ch"  MAXLENGTH="10" CLASS="readonly" readonly><input type=hidden name=ManageCom>
	    		</td>
			</tr>
			<tr CLASS="common">
				<td CLASS="title">销售渠道 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="SaleChnl_ch" CLASS="readonly" readonly MAXLENGTH="2"><input type=hidden name=SaleChnl>
	    		</td>
	    	<td CLASS="title">营销机构 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="AgentGroup_ch" CLASS="readonly" readonly MAXLENGTH="2"><input type=hidden name=AgentGroup>
	    		</td>		
				<td CLASS="title">业务员代码 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input CLASS="readonly" readonly name=AgentCode>
	    		</td>
	    		<td CLASS="title">业务员 
	    		</td>
	    		<td CLASS="input" COLSPAN="3">
				<input NAME="AgentCode_ch" MAXLENGTH="10" CLASS="readonly" readonly>
				</td>   		
			</tr>
			<tr class=common>
				<td CLASS="title">特别约定 
	    		</td>
				<td CLASS="input" COLSPAN="7">
					<input NAME="Remark" CLASS="readonly4" readonly MAXLENGTH="255" COLSPAN="7">
	    		</td>	 
			</tr>
			  <input NAME="ProposalContNo" type=hidden CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="20">
				<input NAME="AgentGroup" type=hidden CLASS="readonly" readonly TABINDEX="-1" MAXLENGTH="12">
				<input NAME="AgentCode1" type=hidden MAXLENGTH="10" CLASS="readonly" readonly>
				<input NAME="AgentCom" type=hidden CLASS="readonly" readonly>
				<input NAME="AgentType" type=hidden CLASS="readonly" readonly>
		</table>
	</div>
	<div id=DivLCSendTrance STYLE="display:'none'">
		<table class = "common">
				<tr CLASS="common">
			    <td CLASS="title">上报标志 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="SendFlag" MAXLENGTH="10" CLASS="readonly" readonly>
	    		</td>
				<td CLASS="title">核保结论 
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="SendUWFlag" CLASS="readonly" readonly>
	    		</td>
				<td CLASS="title">核保意见
	    		</td>
				<td CLASS="input" COLSPAN="1">
				<input NAME="SendUWIdea" CLASS="readonly" readonly>
	    		</td>
			</tr>
		</table>
	</div>
	<DIV id=DivLCAppntIndButton STYLE="display:'none'">
		<!-- 投保人信息部分 -->
		<table>
			<tr>
				<td>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
				</td>
				<td class= titleImg>
				投保人信息
				</td>
			</tr>
		</table>
	</DIV>
	
	<DIV id=DivLCAppntInd STYLE="display:'none'">
	 <table  class= common>
	   <TR  class= common>        
	     <TD  class= title COLSPAN="1">
	       姓名
	     </TD>
	     <TD  class= input>
	       <Input CLASS="readonly" readonly name=AppntName>
	     </TD>
	     <TD  class= title>
	       性别
	     </TD>
	     <TD  class= input COLSPAN="1">
	       <Input name=AppntSex_ch CLASS="readonly" ><input type=hidden name=AppntSex>
	     </TD>
	     <TD  class= title COLSPAN="1">
	       年龄
	     </TD>
	     <TD  class= input COLSPAN="1">
	     <input CLASS="readonly" readonly name="AppntBirthday">
	     </TD>
	       <Input CLASS="readonly" readonly type=hidden name="AppntNo">
	       <Input CLASS="readonly" readonly  type=hidden name="AddressNo">
	     <TD  class= title COLSPAN="1">
	       职业
	     </TD>
	     <TD  class= input COLSPAN="1">
	     <input CLASS="readonly" readonly name="OccupationCode_ch"><input type=hidden name=OccupationCode>
	     </TD>	
	   	</tr>
	     <tr class=common>         
	     <TD  class= title>
	       国籍
	     </TD>
	     <TD  class= input COLSPAN="1">
	     <input CLASS="readonly" readonly name="NativePlace_ch"><input type=hidden name=NativePlace>
	     </TD>
	      <TD  class= title>
	       联系地址
	     </TD>
	     <TD  class= input COLSPAN="1">
	     <input CLASS="readonly" readonly name="AddressNo_ch"><input type=hidden name=AddressNo>
	     </TD>  
	     
	     <TD  class= title>
      	 VIP标记
     	</TD>
     	<TD  class= input COLSPAN="1">
       	<Input class="readonly" readonly name=VIPValue >
       	
     	</TD>
	   <TD  class= title>
       	黑名单标记
     	</TD>
      <TD  class= input COLSPAN="3">
       	<Input class="readonly" readonly name=BlacklistFlag >
       
     	</TD>
	   </TR>
	  </table>
	</DIV>
    <!-- 保单查询结果部分（列表） -->
  <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单查询结果
    		</td>
    	</tr>  	
    </table>
  </Div>
  <Div  id= "divLCPol1" style= "display: ''" align = center>
  	<table class=common >
	    <tr  class=common>
	     	<td text-align: left colSpan=1 >
	  			<span id="spanPolGrid" >
	  			</span> 
	  		</td>
	  	</tr>
    </table>
    <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
    <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
    <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
    <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();">      
  </div>
  <div id="divAppInfo" style="display: ''" align= center>
    <input value="告知整理" name="ImpartTrim" class=cssButton type=hidden onclick="ImpartToICD();">
    <INPUT VALUE="投保单明细" class=cssButton TYPE=hidden onclick="showPolDetail();"> 
    <INPUT VALUE="扫描件查询" class=cssButton  TYPE=hidden onclick="ScanQuery();">  
    <INPUT VALUE="既往投保信息" type=hidden class=cssButton TYPE=hidden onclick="showApp(1);"> 
    <!--INPUT VALUE="被保人既往投保信息" class=cssButton TYPE=button onclick="showApp(2);"--> 
    <!--INPUT VALUE="以往核保记录" class=common TYPE=button onclick="showOldUWSub();"-->
    <input value="记事本" class=cssButton type=hidden onclick="showNotePad();" width="200">
  </div>

  <hr>
   <Div  id= "divMain" style= "display: 'none'">
    <!--附加险-->
      <table>
	    	<tr>
	        <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "return ;showPage(this,divLCPol2);">
	    		</td>
	    		<td class= titleImg>
	    			被保人信息
	    		</td>
	    	</tr>
      </table>
      <div id="divInsuredInfo" style="display: ''" align= center >
	      <INPUT VALUE="审核第一被保险人" class=cssButton TYPE=hidden onclick="showInsuredInfo(1);">
			  <INPUT VALUE="审核第二被保险人" class=cssButton TYPE=hidden onclick="showInsuredInfo(2);">  
	    	<INPUT VALUE="审核第三被保险人" class=cssButton TYPE=hidden onclick="showInsuredInfo(3);">
    	</div>
      <Div  id= "divLCPol2" style= "display: 'none'" >
        	<table  class= common>
         		<tr  class= common>
        	  	<td text-align: left colSpan=1 >
	  	  				<span id="spanPolAddGrid">
	  	  				</span> 
  	  		  	</td>
  	  			</tr>
      		</table>			
      </Div>
	<table class= common border=0 width=100% >
	<hr>
		<div id=divUWButton1 style="display:''" align = center>
	  	<span id= "divAddButton1" style= "display: ''" >
	  		<!--input value="体检资料查询" class=common type=button onclick="showHealthQ();" width="200"-->
	  	</span>
		  	<div id= "displayAutoUw" style= "display: 'none'" >
			  	<INPUT VALUE="自核报告" class=cssButton TYPE=button onclick="showNewUWSub();">
			  	<input value="体检回销" class=cssButton type=button onclick="showHealthQ();" > 
			  	<INPUT VALUE="契调回销" class=cssButton TYPE=button onclick="RReportQuery();">
			  	<input value="问题件回销" class=cssButton type=button onclick="QuestBack();">
		    	<input value="查询核保通知书" class=cssButton type=button onclick="showChangePlanScan();">
			  </div>
		  	<div id= "displayInfoDiv" style= "display: ''" >
		  		<tr>
		  			<INPUT VALUE="自核报告" class=cssButton TYPE=button onclick="showNewUWSub();">
		  			<INPUT VALUE="扫描件查询" class=cssButton  TYPE=button onclick="ScanQuery();">  
		  		  <INPUT VALUE="问题件录入" class=cssButton TYPE=button onclick="QuestInput();">
				  	<input value="体检录入" class=cssButton type=button onclick="showHealth();" width="200">
				  	<input value="疾病问卷录入" class=cssButton type=button onclick="showDisease();" width="200">		
				  	<input value="财务问卷录入" class=cssButton type=button onclick="showFinance();" width="200">			  	
				  	<input value="契调录入" class=cssButton type=button onclick="showRReport();"> 
				    <input value="记 事 本" class=cssButton type=button onclick="showNotePad();" width="200">
				    <input value="核保通知书发放" type=hidden class=cssButton type=button onclick="SendNotice();"> 
				    <INPUT VALUE="保单明细" class=cssButton TYPE=hidden onclick="showPolDetail();"> 
				    <span  id= "divAddButton9" style= "display: ''">
				    	<input value="承保计划变更" class=cssButton type=hidden onclick="showChangePlan();">
				    </span> 
				  </tr>
				  <tr>
            <input value="告知整理" name="ImpartTrim" class=cssButton type=button onclick="ImpartToICD();">  
            <input value=" 再保审核 " class=cssButton type=button onclick="Reinsure();">     
         	  <input value="问题件回销" class=cssButton type=button onclick="QuestBack();">     				  	
				  	<input value="体检回销" class=cssButton type=button onclick="showHealthQ();" >
				  	<input value="疾病问卷回销" class=cssButton type=button onclick="DiseaseBack();" width="200">	
				  	<input value="财务问卷回销" class=cssButton type=button onclick="FinanceBack();" width="200">		 
				  	<INPUT VALUE="契调回销" class=cssButton TYPE=button onclick="RReportQuery();">
				  	<INPUT VALUE="保单明细" class=cssButton TYPE=button onclick="showPolDetail();"> 	
				  	<input value="核保通知书回销" type=hidden class=cssButton type=hidden onclick="NoticeBack();"> 			  					  	
				  	<INPUT VALUE="扫描件查询" class=cssButton  TYPE=hidden onclick="ScanQuery();">  
				    <input value="记 事 本" class=cssButton type=hidden onclick="showNotePad();" width="200">
				  </tr> 	
				  	<!--INPUT VALUE="保全明细查询" class= common TYPE=button onclick="Prt();"--> 
				  	<!--INPUT VALUE="保全项目查询" class= common TYPE=button onclick="ItemQuery();"--> 
				  	<input value="问题件回销" class=cssButton type=hidden onclick="QuestBack();">
				  	<INPUT VALUE="家庭既往投保信息" class=cssButton TYPE=hidden onclick="showApp(2);">
				  	<span  id= "divAddButton9" style= "display: ''">
				    	<input value="承保计划变更" class=cssButton type=hidden onclick="showChangePlan();">
				    </span> 
				    <span  id= "divAddButton14" style= "display: 'none'">
				    	<input value="查询核保通知书" class=cssButton type=hidden onclick="showChangePlanScan();">
				    </span> 
		 	  </div>
	  	
	  	<!--input value="问题件查询" class=cssButton type=button onclick="QuestQuery();"-->
	  	
	  	<!--INPUT  VALUE="既往理赔查询" class=cssButton TYPE=button onclick="ClaimGetQuery();"--> 
	  	
	  	<span  id= "divAddButton2" style= "display: ''">
	  		<!--INPUT VALUE="生存调查查询" class=common TYPE=button onclick="RReportQuery();"-->
	  	</span>         
	  	
	  	<span  id= "divAddButton3" style= "display: ''">
	  		<!--input value="撤单申请查询" class=common type=button onclick="BackPolQuery();"-->
	  	</span>
	  	<span  id= "divAddButton4" style= "display: ''">
	  		<!--input value="催办查询" class=common type=button onclick="OutTimeQuery();"-->
	  	</span>
	  	
	  	<span  id= "divAddButton5" style= "display: ''">
	  	<!--input value="体检资料录入" class=common type=button onclick="showHealth();" width="200"-->          
	  	</span>
	  	
	  	
	  	<span  id= "divAddButton6" style= "display: ''">
	  		<!--input value="生调请求说明" class=common type=button onclick="showRReport();"-->
	  	</span>
	  	
	  	<!--input value="加费承保录入" class=cssButton type=button name= "AddFee"  onclick="showAdd();"> 
	  	<input value="特约承保录入" class=cssButton type=button onclick="showSpec();"--> 
	    <br>
			<div  id= "divAddButton13" style= "display: ''">
	     <input value="发核保通知书" type=hidden class=cssButton type=hidden onclick="SendNotice();"> 
	     <input value="发问题件通知书" class=cssButton type=hidden onclick="SendIssue();"> 
	     <input value="发体检通知书" class=cssButton type=hidden onclick="SendHealth();">
	    </div>
	    <!--tr> <hr> </hr>  </tr-->
	    <span  id= "divAddButton7" style= "display: 'none'">
	    
	    <!--input value="发首期交费通知书" class=cssButton type=button onclick="SendFirstNotice();"--> 
	    </span>
	    <!--<input value="发催办通知书" type=button onclick="SendPressNotice();">-->  
	    <span  id= "divAddButton8" style= "display: ''">
	    <!--input value="发核保通知书" class=common type=button onclick="SendNotice();">-->
	    </span>
	    
	    <!--input value="核保分析报告录入" class=cssButton type=button onclick="showReport();" width="200"-->                         
	            
	    <span  id= "divAddButton10" style= "display: 'none'">
	    	<input value="承保计划变更结论录入" class=cssButton type=button onclick="showChangeResultView();">
	    </span>
	    <span  id= "divAddButton11" style= "display: 'none'">
	    	<input value="延长催办时间" class=cssButton type=button onclick="DelayUrgeNotice();">
	    </span>
	    <span  id= "divAddButton12" style= "display: 'none'">
	    	<input value="发承保内容变更通知书" class=cssButton type=button onclick="PrintUWResult();">
	    	<input type="hidden" name="UWResult">
	    </span>
	  </div>
  </table>

  <!--HR-->
   <Div  id= "divMainI" style= "display: ''">
  <HR>

  <input type="hidden" name= "PrtNoHide" value= "">
  <input type="hidden" name= "PolNoHide" value= "">
  <input type="hidden" name= "MainPolNoHide" value= "">
  <input type="hidden" name= "NoHide" value= "">
  <input type="hidden" name= "TypeHide" value= "">
  <input type="hidden" class= Common name= UWGrade value= "">
  <input type="hidden" class= Common name= AppGrade value= "">
  <input type="hidden" class= Common name= PolNo  value= "">
  <input type="hidden" class= Common name= "ContNo"  value= "">
  <input type="hidden" class= Common name= "YesOrNo"  value= "">
  <input type="hidden"  class= Common name= "MissionID"  value= "">
  <input type="hidden"  class= Common name= "ActivityID"  value= "">
  <input type="hidden"  class= Common name= "SubMissionID"  value= "">
  <input type="hidden"  class= Common name= "SubConfirmMissionID"  value= "">
  <input type="hidden" class= Common name= SubNoticeMissionID  value= "">
  <input type="hidden" class= Common name= UWPopedomCode value= "">
  <input type="hidden" class= Common name= "LoadFlag"  value= "">
  <input type="hidden" class= Common name= "fmtransact"  value= "">

      <Div  id= "divPolInfo" style= "display: ''" >
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1 >
  					<span id="spanProposalGrid">
  					</span> 
          </td>
        </tr>
      </table>
    </Div>
  <table  class= common align=center>
      	
	</table>          
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
