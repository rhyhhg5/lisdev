//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
parent.fraMain.rows = "0,0,0,0,*";
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	var n = ProposalGrid.getSelNo();
	if(n==0){
		alert("请选择一个险种信息！");
		return false;
	}
	
	fm.PolNo.value = ProposalGrid.getRowColData(n-1,10);
	fm.ContNo.value = ProposalGrid.getRowColData(n-1,11);
	
  var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmAction.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./UWIndWriteBackSave.jsp";
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initForm();
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在UWIndWirteBackInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  if( verifyInput2() == false ) return false;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}
//处理险种信息页面
function queryProposalInfo() {
	var strSql = "select a.Name,char(rownumber() over()) as riskNo,b.riskcode,b.mult,b.amnt,(select sum(prem) from lcprem where substr(lcprem.payplancode,1,6)<>'000000' and lcprem.polno=b.polno),"
							+"(select sum(prem) from lcprem where substr(lcprem.payplancode,1,6)='000000' and lcprem.polno=b.polno)"
							+",d.codename,case when c.CustomerReply='00' then '同意' when c.CustomerReply='01' then '变成承保不同意' end ,b.polno,b.contno "
							+"from lcinsured a,lcpol b,LCUWMaster c,ldcode d where a.contno=b.contno "
							+" and a.insuredno=b.insuredno "
							+" and b.polno=c.polno "
							+" and d.codetype='uwflag' "
							+" and d.code=c.passflag "
							+" and b.prtno='"+mPrtNo+"' order by a.SequenceNo";
	var arr = easyExecSql(strSql);
	for(var i=0;i<arr.length;i++)
	{
		for(var m=0;m<arr[i].length;m++)
		{
			if(arr[i][m]=='null')
				arr[i][m]="";
		}
	}
	displayMultiline(arr,ProposalGrid,turnPage);
}
//录入完毕
function confirmInput(){
	if(!confirm("确定要最终确认？")){
		return;
	}
	var i = 0;
  if( verifyInput2() == false ) return false;
  fm.fmAction.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action="./UWIndWriteBackConfirm.jsp?MissionID="+mMissionID+"&SubMissionID="+mSubMissionID+"&ProposalContNo="+mProposalContNo;
  fm.submit(); //提交
}