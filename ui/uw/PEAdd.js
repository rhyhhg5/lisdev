//程序名称：PEdorUWManuHealth.js
//程序功能：保全人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//var turnPage1 = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.action= "./UWManuHealthChk.jsp";
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
		window.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      
    showInfo.close();
    alert(content);
    //parent.close();
  }
  else
  { 
	var showStr="操作成功";
  	showInfo.close();
  	alert(showStr);
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function manuchkhealthmain()
{
	fm.submit();
}


// 查询按钮
function easyQueryClickSingle()
{
	// 书写SQL语句
	var strsql = "";
	var tContNo = "";
	var tEdorNo = "";
	tContNo = fm.ContNo.value;
	tInsuredNo = fm.InsureNo.value;
  if(tContNo != "" && tInsuredNo != "")
  {
	strsql = "select LCPENotice.ContNo,LCPENotice.PrtSeq,LCPENotice.CustomerNo,LCPENotice.Name,LCPENotice.PEDate,LCPENotice.MakeDate,LOPRTManager.StateFlag from LCPENotice,LOPRTManager where 1=1"
				+ " and LCPENotice.PrtSeq = LOPRTManager.PrtSeq"
				 + " and ContNo = '"+ tContNo + "'"
				 + " and Customerno = '"+ tInsuredNo + "'";
				 				 
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
//  turnPage.pageDisplayGrid = MainHealthGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
  return true;
  
}


// 查询按钮
function easyQueryClick(parm1,parm2)
{


	// 书写SQL语句
	var strsql = "";
	var tContNo = "";
	var tEdorNo = "";
	tContNo = fm.ContNo.value;
	tInsuredNo = fm.InsureNo.value;
//	var tSelNo = MainHealthGrid.getSelNo()-1;
//  	var tPrtSeq = MainHealthGrid.getRowColData(tSelNo,2);
  if(tContNo != "" && tPrtSeq != "")
  {
	strsql = "select peitemcode,peitemname from LCPENoticeItem where 1=1"
				
				  + " and ContNo = '"+ tContNo + "'"
				 + " and PrtSeq = '"+ tPrtSeq + "'";
				 
	//查询其他体检信息			 
	var strSQL = "select Remark from LCPENotice where 1=1"	
				+" and ContNo = '"+tContNo+"'"; 	
		
	var arrReturn = new Array();
        arrReturn = easyExecSql(strSQL);
        
	fm.all('Note').value = arrReturn[0][0];
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = HealthGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
 	var strSql = "select DisDesb,DisResult,ICDCode from LCPENoticeResult where ProposalContNo='"+tContNo+"' and CustomerNo='"+tInsuredNo+"'";
 	var arr = easyExecSql(strSql);
 	if(arr)
 	{
 		turnPage.queryModal(strSql,DisDesbGrid);
 	}
 	  return true;
}


// 无体检资料查询按钮
function easyQueryClickInit()
{
	fm.action= "./UWManuHealthQuery.jsp";
	fm.submit();
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				HealthGrid.setRowColData( i, j+1, arrResult[i][j] );
				
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if

}

function initInsureNo(tPrtNo)
{
	var i,j,m,n;
	var returnstr;
	
	var strSql = "select InsuredNo,name from lcinsured where 1=1 "
	       + " and Prtno = '" +tPrtNo + "'"
	       + " union select CustomerNo , Name from LCInsuredRelated where polno in (select polno from lcpol where Prtno = '" +tPrtNo+"')";
	//查询SQL，返回结果字符串
 
  
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  
  //判断是否查询成功
  //alert(turnPage.strQueryResult);
  if (turnPage.strQueryResult == "")
  {
    alert("查询失败！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  var returnstr = "";
  var n = turnPage.arrDataCacheSet.length;
  //alert("N:"+n);
  if (n > 0)
  {
  	for( i = 0;i < n; i++)
  	{
  		m = turnPage.arrDataCacheSet[i].length;
  		//alert("M:"+m);
  		if (m > 0)
  		{
  			for( j = 0; j< m; j++)
  			{
  				if (i == 0 && j == 0)
  				{
  					returnstr = "0|^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i == 0 && j > 0)
  				{
  					returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j == 0)
  				{
  					returnstr = returnstr+"^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j > 0)
  				{
  					returnstr = returnstr+"|"+turnPage.arrDataCacheSet[i][j];
  				}
  				
  			}
  		}
  		else
  		{
  			alert("查询失败!!");
  			return "";
  		}
  	}
}
else
{
	alert("查询失败!");
	return "";
}
  //alert("returnstr:"+returnstr);		
  fm.InsureNo.CodeData = returnstr;
  return returnstr;
}

//初始化医院
function initHospital(tContNo)
{
	var i,j,m,n;
	var returnstr;
	
	alert(tContNo);
	var strSql = "select code,codename from ldcode where 1=1 "
	       + "and codetype = 'hospitalcodeuw'"
	       + "and comcode = (select managecom from lccont where ContNo = '"+tContNo+"')";
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  
  //判断是否查询成功
  //alert(turnPage.strQueryResult);
  if (turnPage.strQueryResult == "")
  {
    alert("医院初始化失败！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  //turnPage.pageDisplayGrid = VarGrid;    
          
  //保存SQL语句
  //turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  //turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  var returnstr = "";
  var n = turnPage.arrDataCacheSet.length;
  //alert("N:"+n);
  if (n > 0)
  {
  	for( i = 0;i < n; i++)
  	{
  		m = turnPage.arrDataCacheSet[i].length;
  		//alert("M:"+m);
  		if (m > 0)
  		{
  			for( j = 0; j< m; j++)
  			{
  				if (i == 0 && j == 0)
  				{
  					returnstr = "0|^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i == 0 && j > 0)
  				{
  					returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j == 0)
  				{
  					returnstr = returnstr+"^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j > 0)
  				{
  					returnstr = returnstr+"|"+turnPage.arrDataCacheSet[i][j];
  				}
  				
  			}
  		}
  		else
  		{
  			alert("查询失败!!");
  			return "";
  		}
  	}
}
else
{
	alert("查询失败!");
	return "";
}
  //alert("returnstr:"+returnstr);		
  fm.Hospital.CodeData = returnstr;
  return returnstr;
}


/*********************************************************************
 *  体检疾病信息保存
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function saveDisDesb()
{

	
	var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	
  //var tSelNo = MainHealthGrid.getSelNo();
// 	if(tSelNo==0)
// 	{
// 		alert("请选择体检履历！");
// 		return;	
// 	}
 // fm.all('PrtSeq').value = MainHealthGrid.getRowColData(tSelNo - 1,2);

  fm.action= "./LCPENoticeItemSave.jsp?PrtSeq="+PrtSeq+"&Pestateflag="+Pestateflag+"&HospitCode="+hospitcode;
  fm.submit(); //提交
}

function easyQueryClickLine()
{
	// 书写SQL语句
	var	strsql = "select a.peitemcode,a.peitemname,(select medicaitemprice from ldtestpricemgt where medicaitemcode=a.PEItemCode and hospitcode='"+hospitcode+"'),b.IfEmpty,a.pestateflag from LCPENoticeItem a ,LDGrpTest b where 1=1"
				  + " and PrtSeq = '"+PrtSeq+"'"
				  + " and ProposalContNo = '"+ProposalContNo+ "' and a.PEItemCode=b.TestItemCode and a.TestGrpCode=b.TestGrpCode"
				  + " union select a.peitemcode,a.peitemname,(select medicaitemprice from ldtestpricemgt where medicaitemcode=a.PEItemCode and hospitcode='"+hospitcode+"'),b.IfEmpty,a.pestateflag from LCPENoticeItem a ,LDGrpTest b where 1=1"
				  + " and PrtSeq = '"+PrtSeq+"'"
				  + " and ProposalContNo = '"+ProposalContNo+ "' and a.PEItemCode=b.TestItemCode";
				  	
			//fm.sql.value = strsql;	  	
	turnPage.strQueryResult = easyQueryVer3(strsql,1,1,1);
//	if (!turnPage.strQueryResult)
//	{
//		alert("未查询到满足条件的数据！");
//		return false;
//	}				
//  
// //turnPage1.queryModal(strsql, HealthGrid);
//	 //设置查询起始位置
 turnPage.pageIndex = 0;
// //在查询结果数组中取出符合页面显示大小设置的数组
 turnPage.pageLineNum = 50 ;
// //查询成功则拆分字符串，返回二维数组
 turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
// //设置初始化过的MULTILINE对象
turnPage.pageDisplayGrid = HealthGrid;
// //保存SQL语句
 turnPage.strQuerySql = strsql ;
arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
// //调用MULTILINE对象显示查询结果
displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
// TaskGrid.checkBoxAll();
}

function easyQueryClickLable(){
var strsql = "select ContNo,CustomerNo,remark from LCPENotice where 1=1 and contno='"+ProposalContNo+"'"
						+" and PrtSeq='"+PrtSeq+"'";
				 
				
				 var arr = easyExecSql(strsql);
	if(arr)
	{
    fm.ContNo.value = arr[0][0];
    
    fm.InsureNo.value = arr[0][1];
    fm.all('InsureNoName').value = easyExecSql("select name from LDPerson where CustomerNo='"+arr[0][1]+"'");
   fm.Note.value = arr[0][2];
	}
			
}

function afterCodeSelect(CodeName,field)
{
	if(CodeName=="hmchoosetestuw")
	{
		var	strsql = "select a.medicaitemprice, b.IfEmpty from ldtestpricemgt a,LDGrpTest b  where 1=1"
				
				  + " and  a.medicaitemcode='"+field.value+"' and a.hospitcode='"+hospitcode+"' and b.TestItemCode='"+field.value+"'";
	   var arr = easyExecSql(strsql);
	  var price = "";
	  var yesno = "";
		if(arr)
		{
			price=arr[0][0];
			yesno=arr[0][1];
			var count = 0;
			count = HealthGrid.mulLineCount-1;
			HealthGrid.setRowColData(count,3,price);
			HealthGrid.setRowColData(count,4,yesno);	
		}
	}
	
}



function returnParent()
{
	try
	{
		top.opener.initBooking();
			
	}
		catch(ex)
		{
			alert( "要返回的页面函数出错！" );
		}
		top.close();
}

