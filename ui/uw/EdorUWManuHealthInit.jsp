<%
//程序名称：EdorUWManuHealthInit.jsp
//程序功能：保全人工核保体检资料录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT>                            
function initForm(tContNo,tMissionID,tSubMission,tPrtNo)
{
	initUWHealthGrid();
}

function initUWErrGrid()
{                               
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="投保单号";    	//列名
		iArray[1][1]="80px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="核保顺序号";         			//列名
		iArray[2][1]="40px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="核保意见";         		//列名
		iArray[3][1]="280px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="核保日期";         		//列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		UWErrGrid = new MulLineEnter( "fm" , "UWErrGrid" ); 
		//这些属性必须在loadMulLine前
		UWErrGrid.mulLineCount = 0;   
		UWErrGrid.displayTitle = 1;
		UWErrGrid.locked = 1;
		UWErrGrid.hiddenSubtraction = 1;
		UWErrGrid.hiddenPlus = 1;
		UWErrGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}

// 责任信息列表的初始化
function initUWHealthGrid()
{                              
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="体检项目代码";    	//列名
		iArray[1][1]="120px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[1][4]="hmchoosetestuw";           //CodeQueryBL中的引用参数         
		iArray[1][5]="1|2";     								//引用代码对应第几列，'|'为分割符 
		iArray[1][6]="0|1";    									//上面的列中放置引用代码中第几位值
		iArray[1][9]="检查项目代码|LEN<20";                                       
		iArray[1][15]="MedicaItemCode";         //数据库中字段，用于模糊查询      
		iArray[1][17]="1";                                                        
		iArray[1][19]="1" ; 
		
		iArray[2]=new Array();
		iArray[2][0]="体检项目";    	//列名
		iArray[2][1]="120px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
		
		iArray[3]=new Array();
		iArray[3][0]="是否空腹";         			//列名
		iArray[3][1]="100px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][9]="是否空腹|LEN<20";                                       
		iArray[3][3]=0
		
		HealthGrid = new MulLineEnter("fm", "HealthGrid");                          
		HealthGrid.mulLineCount = 0;
		HealthGrid.displayTitle = 1;
		HealthGrid. hiddenSubtraction=0;
		HealthGrid. hiddenPlus=0;  
		HealthGrid.canChk = 0;
		HealthGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}
</script>


