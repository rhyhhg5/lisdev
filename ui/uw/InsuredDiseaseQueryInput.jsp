<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.utility.*"%>
<%
  //程序名称：InsuredDiseaseQueryInput.jsp
  //程序功能：被保人核保信息界面
  //创建日期：2006-11-17 11:05
  //创建人  ：Yangming
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
	<%
	  //个人下个人
	  GlobalInput tGI = new GlobalInput();
	  tGI = (GlobalInput) session.getValue("GI");
	%>
	<script>
	
		var DiseaseCode = "<%=StrTool.unicodeToGBK(request.getParameter("DiseaseCode"))%>";	
		var RiskKind = "<%=StrTool.unicodeToGBK(request.getParameter("RiskKind"))%>";	
		
	</script>
		<head>
			<meta http-equiv="Content-Type" content="text/html; charset=GBK">
			<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
			<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
			<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
			<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
			<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
			<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
			<SCRIPT src="InsuredDiseaseQueryInput.js"></SCRIPT>
			<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
			<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
			<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
			<%@include file="InsuredDiseaseQueryInit.jsp"%>
			<title>风险代码查询</title>
		</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit" >
			<table class=common border=0 width=100%>
		    <tr>
		      <td class=titleImg align=center>疾病风险库：</td>
		    </tr>
		  </table>
		  <table class=common>
		    <tr class=common>
		      <td text-align: left colSpan=1>
		        <span id="spanDiseaseGrid">        </span>
		      </td>
		    </tr>
		  </table>
		</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>