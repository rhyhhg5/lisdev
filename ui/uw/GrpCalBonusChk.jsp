 <%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CutBonusChk.jsp
//程序功能：分红处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
  
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
  LOBonusGrpPolParmSet tLOBonusGrpPolParmSet = new LOBonusGrpPolParmSet();
	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
 	String opertype=request.getParameter("opertype");
	String tGrpPolNo[] = request.getParameterValues("PolGrid1");
	String tYear[] = request.getParameterValues("PolGrid3");
	String tChk[] = request.getParameterValues("InpPolGridChk");
	
	boolean flag = false;
	int feeCount = tGrpPolNo.length;
    LOBonusGrpPolParmSchema tLOBonusGrpPolParmSchema = new LOBonusGrpPolParmSchema();
	for (int i = 0; i < feeCount; i++)
	{
		if (!tGrpPolNo[i].equals("") && tChk[i].equals("1"))
		{
		    System.out.println("PolNo:"+i+":"+tGrpPolNo[i]);
	  	    
	  	    tLOBonusGrpPolParmSchema = new LOBonusGrpPolParmSchema();
	  	    tLOBonusGrpPolParmSchema.setGrpPolNo(tGrpPolNo[i]);
	  	    tLOBonusGrpPolParmSchema.setFiscalYear(tYear[i]);
		    
		    tLOBonusGrpPolParmSet.add( tLOBonusGrpPolParmSchema );
		    System.out.println("size:"+tLOBonusGrpPolParmSet.size());
		    flag = true;
		}
	}

try
{
  	if (flag == true)
  	{
  	  if(opertype.equals("Cal"))
  	  {		
		VData tVData = new VData();
		tVData.add( tLOBonusGrpPolParmSet );
		tVData.add( tG );				
		GrpAssignBonus tGrpAssignBonus   = new GrpAssignBonus();
		if (tGrpAssignBonus.runCalBonus(tVData) == false)
		{			
			Content = " 红利计算完成，部分数据有误，请查看错误信息表";
			FlagStr = "Fail";
		}
		else
		{
			Content = " 红利计算完成! ";
		    FlagStr = "Succ";
		}
	   }
	   else
	   {	   			
		  
		 Content = "删除成功";
		 FlagStr = "Succ"; 
		 for(int i=1;i<=tLOBonusGrpPolParmSet.size();i++)
		 {
		 	LOBonusGrpPolParmDB tLOBonusGrpPolParmDB =new LOBonusGrpPolParmDB();
		 	tLOBonusGrpPolParmDB.setSchema(tLOBonusGrpPolParmSet.get(i));
		 	if(tLOBonusGrpPolParmDB.delete()==false)
		 	{
		 		Content = " 删除失败:"+tLOBonusGrpPolParmSet.get(i).getGrpPolNo();
		    	FlagStr = "Fail";
		    	break;
		 	}
		 }
	   
	   }	
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initPolGrid();
</script>
</html>
