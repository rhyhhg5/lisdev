<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：QueryDiseaseInit.jsp
//程序功能：人工核保免责信息
//创建日期：2005-01-04 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
    out.println("session has expired");
    return;
  }

String strOperator = globalInput.Operator;
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）

function initForm() {
  try {
  		
      initQueryDisease();
      showSaveButton(false);
      
//      fm.RelationToAppnt.ondblclick="";
//      fm.RelationToMainInsured.ondblclick="";
//      fm.NativePlace.ondblclick="";
//      fm.Sex.ondblclick="";
//      fm.OccupationCode.ondblclick="";
      
    } catch(re) {
      alert("在QueryDiseaseInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

// 保单信息列表的初始化
function initQueryDisease() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="免责信息代码";         	//列名
      iArray[1][1]="30px";            		//列宽
      iArray[1][2]=30;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="免责信息";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=30;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      QueryDiseaseGrid = new MulLineEnter( "fm" , "QueryDiseaseGrid" );
      //这些属性必须在loadMulLine前
      QueryDiseaseGrid.mulLineCount = 0;
      QueryDiseaseGrid.displayTitle = 1;
      QueryDiseaseGrid.locked = 1;
      QueryDiseaseGrid.canSel = 1;
      QueryDiseaseGrid.hiddenPlus = 1;
      QueryDiseaseGrid.hiddenSubtraction = 1;
      QueryDiseaseGrid.loadMulLine(iArray);

//      QueryDiseaseGrid. selBoxEventFuncName = "";
    } catch(ex) {
      alert(ex);
    }
}
</script>
