<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：UWNotePadSave.jsp
//程序功能：
//创建日期：2006-9-21 20:01
//创建人  ：Wulg
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  
<%
  System.out.println("===========================");
  System.out.println("----UWNotePadSave Start----");
  System.out.println("===========================");
  System.out.println("GrpContNo:" + request.getParameter("GrpContNo"));

  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  
  LCNotePadSchema tLCNotePadSchema = new LCNotePadSchema();
  tLCNotePadSchema.setGrpContNo(request.getParameter("GrpContNo"));
  tLCNotePadSchema.setNotePadCont(request.getParameter("Content"));
  tLCNotePadSchema.setPrtNo(request.getParameter("PrtNo"));
  UWGrpNotePadUI iUWGrpNotePadUI = new UWGrpNotePadUI();
  String Content = "";
  String FlagStr = "";
  CErrors tError = null;
  
  try {
  
  VData inVData = new VData();
  inVData.add(tLCNotePadSchema);
  inVData.add(tGlobalInput);
  
  iUWGrpNotePadUI.submitData(inVData, "INSERT");
  } catch (Exception ex) {
       Content = "操作失败，原因是:" + ex.toString();
       FlagStr = "Fail";
  }
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = iUWGrpNotePadUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

	System.out.println(Content + "\n" + FlagStr + "\n---UWNotePadSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.queryClick();
</script>
</html>
