<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String tOperate ="";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  if(tG == null) 
  {
  
		out.println("session has expired");
		return;
	}
	
  String tGrpContNo = "000000000000";
 	String tContNo = request.getParameter("ContNo");
 	String tProposalContNo =request.getParameter("ContNo");
 	String tCustomerNo=request.getParameter("CustomerNo");
 	String tPrtSeq=request.getParameter("PrtSeq");
 	String tAction=request.getParameter("fmAction");
 	String tRReportItemCode = request.getParameter("RReportItemCode");
 	String tRReportItemName = StrTool.unicodeToGBK(request.getParameter("RReportItemName"));
 	String tRRItemContent = StrTool.unicodeToGBK(request.getParameter("RRItemContent"));
 	String tRRItemResult = StrTool.unicodeToGBK(request.getParameter("RRItemResult"));
 	
 	String tContent = StrTool.unicodeToGBK(request.getParameter("Content"));
 	
 	 LCRReportSchema tLCRReportSchema = new LCRReportSchema();
 	 
 	 LCRReportItemSchema tLCRReportItemSchema = new LCRReportItemSchema();
	 
	 tLCRReportItemSchema.setGrpContNo(tGrpContNo);
	 tLCRReportItemSchema.setContNo(tContNo);
	 tLCRReportItemSchema.setProposalContNo(tProposalContNo);
	 tLCRReportItemSchema.setPrtSeq(tPrtSeq);
	 tLCRReportItemSchema.setRReportItemCode(tRReportItemCode);
	 tLCRReportItemSchema.setRReportItemName(tRReportItemName);	 
	 tLCRReportItemSchema.setRRItemContent(tRRItemContent);
	 tLCRReportItemSchema.setRRItemResult(tRRItemResult);

	 tLCRReportSchema.setProposalContNo(tProposalContNo);
	 tLCRReportSchema.setPrtSeq(tPrtSeq);
	 tLCRReportSchema.setContente(tContent);
		
		// 准备传输数据 VData
		VData tVData = new VData();
		FlagStr="";
    tVData.add(tG);
		if(tAction.equals("MODIFY||RECORD"))
		{
			tOperate="UPDATE";
			tVData.add(tLCRReportItemSchema);
		}

		if(tAction.equals("MODIFY||CONTENT"))
		{
			tOperate="MODIFY";
			tVData.add(tLCRReportSchema);
		}
			
		UWRReportItemUI tUWRReportItemUI = new UWRReportItemUI();		
		
		try
		{
			System.out.println("this will save the data!!!");
			tUWRReportItemUI.submitData(tVData,tOperate);
		}
		
		catch(Exception ex)
		{
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		
		if (!FlagStr.equals("Fail"))
		{
			tError = tUWRReportItemUI.mErrors;
			if (!tError.needDealError())
			{
				Content = " 保存成功! ";
				FlagStr = "Succ";
			}
			else
			{
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
