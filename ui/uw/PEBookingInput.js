//程序名称：PEBookingInput.js
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{

  if( verifyInput2() == false ) return false;  
	cPrtNo = fm.PrtNo.value;
	cManageCom = fm.ManageCom.value;	
	if(cPrtNo == "")
	{
		alert("请录入印刷号！");
		return;
	}
	if(cManageCom == "")
	{
		alert("请录入管理机构！");
		return;		
	}
	if(isNumeric(cPrtNo)==false){
		 alert("印刷号应为数字");
		 return false;
	}	
	if(type=='2')//对于集体保单的申请
	{
		if (GrpbeforeSubmit() == false)
		{
		    alert("已存在该印刷号，请选择其他值!");
		    return false;		
		}
	}
	else //对于个人保单的申请
	{
		if (beforeSubmit() == false)
		{
		    alert("已存在该印刷号，请选择其他值!");
		    return false;		
		}
	}
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
	easyQueryClick();
}

/*********************************************************************
 *  执行新契约扫描的EasyQuery
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	initGrpGrid();
	if(!verifyInput2())
	return false;
	// 书写SQL语句
	var cond ="";
	if(fm.PEStatecode.value=="00"){
		cond= " and a.PEState is null ";
		fm.PEStatecode.value="";
	}
	var tagentcode="";
	if(fm.AgentCode.value!=""){
		var magentcode=easyExecSql(" select getAgentCode('"+fm.AgentCode.value+"') from dual");
		tagentcode=" and a.AgentCode='"+magentcode+"'";
	}  
	var strSQL = "";
	strSQL = "select c.PrtSeq,a.ContNo,a.Name,c.makedate "+
 "  ,b.PrtNo,b.PolApplyDate,b.CValiDate,a.AppName,a.AgentName,a.ManageCom ,(select hospitname from ldhospital where hospitcode=a.PEAddress),a.Operator,a.PEResult,(select codename from ldcode where codetype='pecode' and code=a.PEState) from lcpenotice a ,lccont b ,LOPRTManager c where 1=1 and a.contno=b.contno and a.PrtSeq=c.PrtSeq "
	                 + getWherePart( 'a.ManageCom','ManageCom','like' )
	                 + "and a.ManageCom like '"+manageCom+"%%'"+cond
				           + getWherePart( 'b.PrtNo','PrtNo' )
				           + getWherePart( 'a.PrtSeq','PrtSeq' ) 
				           + getWherePart( 'a.Name','Name' ) 
				           + getWherePart( 'c.makedate','EDate' ) 
				           + getWherePart( 'b.PolApplyDate','InputDate' ) 
				           + getWherePart( 'b.CValiDate','CValiDate' ) 
				           + tagentcode
				           + getWherePart( 'a.PEState','PEStatecode' ) 
				           + "order by a.makedate, a.maketime"
				           
				           
				 ;	 
				 
				 
//fm.sql.value=strSQL;
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有找到相关的数据！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}
function GoToInput()
{
  var i = 0;
  var checkFlag = 0;
  var state = "0";
  
  for (i=0; i<GrpGrid.mulLineCount; i++) {
    if (GrpGrid.getSelNo(i)) { 
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) { 
    var	ContNo = GrpGrid.getRowColData(checkFlag - 1, 2); 	
    var PrtNo = GrpGrid.getRowColData(checkFlag - 1, 5);
    var PrtSeq = GrpGrid.getRowColData(checkFlag - 1, 1);
    var Name = GrpGrid.getRowColData(checkFlag - 1, 3); 
    var PEAddress = GrpGrid.getRowColData(checkFlag - 1, 11);
    var Operator = GrpGrid.getRowColData(checkFlag - 1, 12);
    var PEResult = GrpGrid.getRowColData(checkFlag - 1, 13);

    //var urlStr = "./ProposalScanApply.jsp?prtNo=" + prtNo + "&Name=" + Name + "&PEAddress=" + PEAddress+ "&Operator=" + Operator+ "&PEResult=" + PEResult;
    //var sFeatures = "status:no;help:0;close:0;dialogWidth:400px;dialogHeight:200px;resizable=1";
    //申请该印刷号
    //var strReturn = window.showModalDialog(urlStr, "", sFeatures);
    var strReturn="1";
    //打开扫描件录入界面
    sFeatures = "";
    //sFeatures = "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1";

    if (strReturn == "1") 
     /**if(type=='1')
     {*/
    	window.open("./PEDrugQuery.jsp?ScanFlag=0&ContNo2="+ContNo+"&PrtNo="+PrtNo+"&PrtSeq="+ PrtSeq + "&Name="+ Name + "&PEAddress=" + PEAddress+ "&Operator=" + Operator+ "&PEResult=" + PEResult+"&PEFlag=1", sFeatures);        
    /** }else if(type=='2')
     {
     	window.open("./UWManuHealth.jsp?ScanFlag=0&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID, "", sFeatures);
     	}*/	
     	
  }
  else {
    alert("请先选择一条保单信息！"); 
  }
  
}

function beforeSubmit()
{
	var strSQL = "";
	strSQL = "select missionprop1 from lwmission where 1=1 "
				 + " and activityid = '0000001098' "
				 + " and processid = '0000000003'" 
				 + " and missionprop1='"+fm.PrtNo.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
	//判断是否查询成功
	if (turnPage.strQueryResult) {
	    return false;
	}

	strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
			 "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
	//判断是否查询成功
	if (turnPage.strQueryResult) {
	    return false;
	}	   	
}
function GrpbeforeSubmit()
{
	var strSQL = "";
	strSQL = "select missionprop1 from lwmission where 1=1 "
				 + " and activityid = '0000002098' "
				 + " and processid = '0000000004'" 
				 + " and missionprop1='"+fm.PrtNo.value+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
	//判断是否查询成功
	if (turnPage.strQueryResult) {

	    return false;
	}

	strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
			 "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1); 
	//判断是否查询成功
	if (turnPage.strQueryResult) {
	    return false;
	}	   	
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
    if(fm.all('AgentCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&branchtype=1","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	  }
	if(fm.all('AgentCode').value != "")	 {
	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      //fm.AgentName.value = arrResult[0][1];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}	
}

function queryAgent2()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
	if(fm.all('AgentCode').value != "" && fm.all('AgentCode').value.length==10 )	 {
	var cAgentCode = fm.AgentCode.value;  //保单号码	
	var strSql = "select AgentCode,Name,AgentGroup from LAAgent where AgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     alert("编码为:["+fm.all('AgentCode').value+"]的代理人不存在，请确认!");
     }
	}	
}                         

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  	//fm.AgentName.value = arrResult[0][1];
  	fm.all('AgentName').value = easyExecSql("select name from laagent where agentcode='"+arrResult[0][0]+"'");
  	//fm.AgentGroup.value = arrResult[0][1];//fm.all('AgentGroupName').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrResult[0][1]+"'");
  }
}
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  	fm.AgentGroupName.value = "";
  	fm.AgentCodeName.value = "";   
 }
 if(cCodeName=="agentgroup1"){
    fm.AgentCode.value = "";
  	fm.AgentCodeName.value = "";   
 }
 
}
 