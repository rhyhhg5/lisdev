 <%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：CutBonus.jsp
//程序功能：分红处理
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="GrpCalBonusView.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GrpCalBonusViewInit.jsp"%>
  <title>团体分红查询</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./GrpCalBonusViewChk.jsp">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            团单号码  
          </TD>
          <TD  class= input>
            <Input class= common name=GrpPolNo>
          </TD>
          <TD  class= title>
            保单类型标记
          </TD>
          <TD  class= input>            
             <Input class="code" name=PolTypeFlag  CodeData="0|^0|个人单^1|无名单^2|公共帐户" ondblclick="return showCodeListEx('PolTypeFlag',[this]);" onkeyup="return showCodeListKeyEx('PolTypeFlag',[this]);">          
          </TD>
        </TR>
        <TR  class= common>
        </TR>
        <TR  class= common>
			<TD  class= title>
	     	会计年度	     
	        </TD> 
	        <TD  class= input>
	          <Input class=common name=FiscalYear>	  
	        </TD>
	       <TD  class= title>
            帐户号码
          </TD>
          <TD  class= input>            
             <Input class="code" name=InsuAccNo  CodeData="0|^000002|个人下集体交费帐户^000003|个人下个人交费帐户^000004|团体下公共帐户" ondblclick="return showCodeListEx('InsuAccNo',[this]);" onkeyup="return showCodeListKeyEx('InsuAccNo',[this]);">          
          </TD>
		</TR>  		  
    </table>
          <INPUT VALUE="查询" class=common TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 分红数据
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=common TYPE=button onclick="getLastPage();"> 					
  	</div>
  	
    <table  class= common align=center>
      	        
        <TR  class= common>
      	</TR> 
      	<TR  class= common>
          <TD  class= title>
            团体合计分红金额  
          </TD>
          <TD  class= input>
            <Input class= common name=sumBonus>
          </TD>
          <TD  class= input>
          <INPUT VALUE="计算" class=common TYPE=button onclick="calSum();"> 
          </TD>
 		</TR> 
  	</table>
  	
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
