//               该文件中包含客户端需要处理的函数和事件
var arrDataSet;
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();



//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //initPolGrid();
  //showSubmitFrame("1");
  fmSave.submit(); //提交
}



//提交，保存按钮对应操作
function printPol()
{
  var tRow = PolGrid.getSelNo();	
  if( tRow == 0 || tRow == null )
		alert( "请先选择一条记录，再点击补打按钮。" );
	else
	{
		fmSave.PrtSeq.value = PolGrid.getRowColData(tRow-1,1);
		fmSave.Code.value = PolGrid.getRowColData(tRow-1,2);
		fmSave.ContNo.value = PolGrid.getRowColData(tRow-1,3);
		fmSave.MissionID.value = PolGrid.getRowColData(tRow-1,7);
		fmSave.SubMissionID.value = PolGrid.getRowColData(tRow-1,8);
		fmSave.PrtNo.value = PolGrid.getRowColData(tRow-1,6);
		fmSave.fmtransact.value = "CONFIRM";
		submitForm();
		showInfo.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
		return arrSelected;

	arrSelected = new Array();
	//设置需要返回的数组
	//arrSelected[0] = arrDataSet[tRow-1];
	arrSelected[0]=new Array();
	arrSelected[0][0]=PolGrid.getRowColData(tRow-1,1);
	arrSelected[0][1]=PolGrid.getRowColData(tRow-1,3);
	arrSelected[0][1]
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");       	
  }
  
  easyQueryClick();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

// 查询按钮
function easyQueryClick()
{
	 //初始化表格
	initPolGrid();
	
		 //书写SQL语句
	var strSQL = "";
	strSQL = " select LWMission.MissionProp14,LWMission.MissionProp4,LWMission.MissionProp2,LWMission.MissionProp5,getUniteCode(LWMission.MissionProp6),LWMission.MissionProp1,LWMission.MissionID,LWMission.SubMissionID from LWMission where 1=1"
		+ " and LWMission.ProcessID = '0000000003'"
		+ " and LWMission.ActivityID in ('0000001114','0000001115','0000001116','0000001018')"
		+ getWherePart('LWMission.MissionProp7','StartDay','>=')
		+ getWherePart('LWMission.MissionProp7','EndDay','<=')
		+ getWherePart('LWMission.MissionProp2','OtherNo') 
	    + getWherePart('getUniteCode(LWMission.MissionProp6)','AgentCode') 
	    + getWherePart('LWMission.MissionProp3','PrtSeq') 
	    + getWherePart('LWMission.MissionProp4','Code')
	    + getWherePart('LWMission.MissionProp8','ExeOperator');
	  
	  
	  
	turnPage.strQueryResult  = easyQueryVer3(strSQL);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
   alert("没有要提交补打通知书的信息！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //tArr = chooseArray(arrDataSet,[0,1,3,4]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}