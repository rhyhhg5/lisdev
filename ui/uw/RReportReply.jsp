<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：RReportReply.jsp
//程序功能：生存调查报告回复
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpContNo = "00000000000000000000";
	
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	
%>
<script>
	var grpContNo = "<%=tGrpContNo%>";      //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<head >
<title>生调通知书查询 </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="RReportReply.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="RReportReplyInit.jsp"%>
  
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
        <TR>
      	<!--<TD  class= title>  投保单号 </TD>
        <TD  class= input>--> <Input class= common name=QContNo  type="hidden">
        <TD  class= title>  投保人 </TD>
        <TD  class= input> <Input class= common name=QAppntName > </TD>
        <TD  class= title> 通知书流水号 </TD>
        <TD  class= input> <Input class= common name=QPrtSeq > </TD>
        </TR>
        <TD  class= title>  投保单印刷号 </TD>
        <TD  class= input> <Input class= common name=QPrtNo > </TD>
        <input type=hidden id="ContNo" name="ContNo">
	  	<input type=hidden id="PrtSeq" name="PrtSeq">
	  	<input type=hidden id="PrtNo" name="PrtNo">
	  	<input type=hidden id="MissionID" name="MissionID">
	  	<input type=hidden id="SubMissionID" name="SubMissionID">
	  	<input type=hidden id="Code" name="Code">
    </table>
    <input type= "button" class= common name= "Reply" value="查询" onClick= "easyQueryClick()">
   
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanQuestGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
      <INPUT VALUE="首页" class = common  TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class = common  TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class = common  TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" class = common  TYPE=button onclick="turnPage.lastPage();">	
    </div>
    <br> </br>
    
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>