<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuUWAllInit.jsp
//程序功能：个人人工核保
//创建日期：2005-01-29 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	String strOperator = globalInput.Operator;
	String strManageCom = globalInput.ComCode;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {            
  	if(operFlag == "1")
  	{
  		fm.ApplyNo.value = "5";    
  		fm.ApplyType.value = "1";  				//个单申请
  	}
  	if(operFlag == "2")
  	{
  		fm.ApplyNo.value = "2";  
  		fm.ApplyType.value = "2";    		  //团单申请
  	}  	      
  	if(operFlag == "3")
  	{
  		fm.ApplyNo.value = "2";  
  		fm.ApplyType.value = "3";    			//询价申请
  	}            
  }
  catch(ex)
  {
    alert("在ManuUWAllInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  	fm.all('ManageCom').value = <%=strManageCom%>;
    if(fm.all('ManageCom').value==86){
    	fm.all('ManageCom').readOnly=false;
    	}
    else{
    	fm.all('ManageCom').readOnly=true;
    	}
    	if(fm.all('ManageCom').value!=null)
    {
    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
            //显示代码选择中文
            if (arrResult != null) {
            fm.all('ManageComName').value=arrResult[0][0];
            } 
    	}
    initInpBox();
    initPolGrid();   
    initUpReportPolGrid();
    initHistoryUwGrid();
    initEdorGrid();
    initNoReplyEdorGrid();
    initOldEdorGrid();
    queryEdorList();
    queryNoReplyEdorList();
    //queryOldEdorList();
    
    initSecondLPGrid();//初始化理赔二核
    querySecondLPGrid();//查询理赔二核信息
    initLPXBGrid();//理赔续保待核受理
    queryLPXBList();
    initLPNoReplyEdorGrid();//理赔续保待核待回复
    queryLPNoReplyEdorList();
    initLPOldEdorGrid();//既往理赔续保待核信息
    initLPTwoOldEdorGrid();//既往理赔二次核保信息
    
    easyQueryClick();
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("在ManuUWAllInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="90px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="合同投保单号";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="投保人";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="状态";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许            

      iArray[5]=new Array();                                                       
      iArray[5][0]="核保类型";         		//列名                                     
      iArray[5][1]="60px";            		//列宽                                   
      iArray[5][2]=100;            			//列最大值                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
            
      iArray[6]=new Array();
      iArray[6][0]="工作流任务号";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[7]=new Array();
      iArray[7][0]="工作流子任务号";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[8]=new Array();
      iArray[8][0]="工作流活动Id";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=60;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[9]=new Array();
      iArray[9][0]="来源";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=60;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[10]=new Array();
      iArray[10][0]="申请日期";         		//列名
      iArray[10][1]="80px";            		//列宽
      iArray[10][2]=60;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[11]=new Array();
      iArray[11][0]="生效日期";         		//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=60;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[12]=new Array();
      iArray[12][0]="送核日期";         		//列名
      iArray[12][1]="80px";            		//列宽
      iArray[12][2]=60;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[13]=new Array();
      iArray[13][0]="险种个数";         		//列名
      iArray[13][1]="50px";            		//列宽
      iArray[13][2]=60;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[14]=new Array();
      iArray[14][0]="排序条件";         		//列名
      iArray[14][1]="50px";            		//列宽
      iArray[14][2]=60;            			//列最大值
      iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
     
      iArray[15]=new Array();
      iArray[15][0]="管理机构";         		//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=60;            			//列最大值
      iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        
      iArray[16]=new Array();
      iArray[16][0]="销售渠道";
      iArray[16][1]="60";
      iArray[16][2]=60;
      iArray[16][3]=0;
      iArray[16][3]="SaleChnl";
            
      iArray[17]=new Array();
      iArray[17][0]="是否银行在途";
      iArray[17][1]="80";
      iArray[17][2]=80;
      iArray[17][3]=0;
      iArray[17][3]="BankOnTheWayFlag";
   
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray);     
      
      PolGrid. selBoxEventFuncName = "easyQueryAddClick";
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 保单信息列表的初始化
function initUpReportPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="90px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="集体合同单号";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="投保人";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="状态";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许            

      iArray[5]=new Array();                                                       
      iArray[5][0]="核保类型";         		//列名                                     
      iArray[5][1]="60px";            		//列宽                                   
      iArray[5][2]=100;            			//列最大值                                 
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
            
      iArray[6]=new Array();
      iArray[6][0]="工作流任务号";         		//列名
      iArray[6][1]="110px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[7]=new Array();
      iArray[7][0]="工作流子任务号";         		//列名
      iArray[7][1]="110px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[8]=new Array();
      iArray[8][0]="工作流活动Id";         		//列名
      iArray[8][1]="110px";            		//列宽
      iArray[8][2]=60;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[9]=new Array();
      iArray[9][0]="来源";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=60;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[10]=new Array();
      iArray[10][0]="申请日期";         		//列名
      iArray[10][1]="80px";            		//列宽
      iArray[10][2]=60;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[11]=new Array();
      iArray[11][0]="生效日期";         		//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=60;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[12]=new Array();
      iArray[12][0]="送核日期";         		//列名
      iArray[12][1]="80px";            		//列宽
      iArray[12][2]=60;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[13]=new Array();
      iArray[13][0]="险种个数";         		//列名
      iArray[13][1]="50px";            		//列宽
      iArray[13][2]=60;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[14]=new Array();
      iArray[14][0]="排序条件";         		//列名
      iArray[14][1]="50px";            		//列宽
      iArray[14][2]=60;            			//列最大值
      iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[15]=new Array();
      iArray[15][0]="管理机构";         		//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=60;            			//列最大值
      iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
        
      iArray[16]=new Array();
      iArray[16][0]="销售渠道";
      iArray[16][1]="60";
      iArray[16][2]=60;
      iArray[16][3]=0;
      iArray[16][3]="SaleChnl";
   
      UpReportPolGrid = new MulLineEnter( "fm" , "UpReportPolGrid" ); 
      //这些属性必须在loadMulLine前
      UpReportPolGrid.mulLineCount = 3;   
      UpReportPolGrid.displayTitle = 1;
      UpReportPolGrid.locked = 1;
      UpReportPolGrid.canSel = 1;
      UpReportPolGrid.hiddenPlus = 1;
      UpReportPolGrid.hiddenSubtraction = 1;
      UpReportPolGrid.loadMulLine(iArray);     
      
      UpReportPolGrid.selBoxEventFuncName = "easyQueryAddClick2";
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initHistoryUwGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="印刷号";         		//列名
	  iArray[1][1]="90px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="保单号";         		//列名
	  iArray[2][1]="160px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="投保人";         		//列名
	  iArray[3][1]="60px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="核保结论";         		//列名
	  iArray[4][1]="80px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许            
	
	  iArray[5]=new Array();                                                       
	  iArray[5][0]="签单状态";         		//列名                                     
	  iArray[5][1]="60px";            		//列宽                                   
	  iArray[5][2]=100;            			//列最大值                                 
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[6]=new Array();
	  iArray[6][0]="管理机构";         		//列名
	  iArray[6][1]="60px";            		//列宽
	  iArray[6][2]=60;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[7]=new Array();
	  iArray[7][0]="申请日期";         		//列名
	  iArray[7][1]="80px";            		//列宽
	  iArray[7][2]=60;            			//列最大值
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[8]=new Array();
	  iArray[8][0]="生效日期";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=60;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="送核日期";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=60;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="险种个数";         		//列名
	  iArray[10][1]="50px";            		//列宽
	  iArray[10][2]=60;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  HistoryUwGrid = new MulLineEnter( "fm" , "HistoryUwGrid" ); 
	  //这些属性必须在loadMulLine前
	  HistoryUwGrid.mulLineCount = 0;   
	  HistoryUwGrid.displayTitle = 1;
	  HistoryUwGrid.locked = 1;
	  HistoryUwGrid.canSel = 1;
	  HistoryUwGrid.hiddenPlus = 1;
	  HistoryUwGrid.hiddenSubtraction = 1;
	  HistoryUwGrid.loadMulLine(iArray);     
	  
	  HistoryUwGrid.selBoxEventFuncName = "easyQueryHistory";
  }
  catch(ex)
  {
    alert(ex);
  }
}

//初始化保全保全核保待回复列表
function initNoReplyEdorGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="工作流任务号";         		//列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[2]=new Array();
	  iArray[2][0]="工作流子任务号";         		//列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="保全受理号";         		//列名
	  iArray[3][1]="120px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="保单号";         		//列名
	  iArray[4][1]="100px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="投保人客户号";         		//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="投保人";         		//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许           
	
	  iArray[7]=new Array();                                                       
	  iArray[7][0]="受理人";         		//列名                                     
	  iArray[7][1]="60px";            		//列宽                                   
	  iArray[7][2]=100;            			//列最大值                                 
	  iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="受理日期";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="经办人";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="送核日期";         		//列名
	  iArray[10][1]="80px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[11]=new Array();
	  iArray[11][0]="送核时间";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[12]=new Array();
	  iArray[12][0]="核保状态";         		//列名
	  iArray[12][1]="80px";            		//列宽
	  iArray[12][2]=100;            			//列最大值
	  iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	  
	  NoReplyEdorGrid = new MulLineEnter("fm", "NoReplyEdorGrid"); 
	  NoReplyEdorGrid.mulLineCount = 0;
	  NoReplyEdorGrid.displayTitle = 1;
	  NoReplyEdorGrid.locked = 1;
	  NoReplyEdorGrid.canSel = 1;
	  NoReplyEdorGrid.hiddenPlus = 1;
	  NoReplyEdorGrid.hiddenSubtraction = 1;
	  NoReplyEdorGrid.loadMulLine(iArray);
	  NoReplyEdorGrid.selBoxEventFuncName = "openNoReplyEdorUwWindow";
  }
  catch(ex)
  {
    alert(ex);
  }
}



//初始化保全待核列表
function initEdorGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="工作流任务号";         		//列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[2]=new Array();
	  iArray[2][0]="工作流子任务号";         		//列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="保全受理号";         		//列名
	  iArray[3][1]="120px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="保单号";         		//列名
	  iArray[4][1]="100px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="投保人客户号";         		//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="投保人";         		//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许           
	
	  iArray[7]=new Array();                                                       
	  iArray[7][0]="受理人";         		//列名                                     
	  iArray[7][1]="60px";            		//列宽                                   
	  iArray[7][2]=100;            			//列最大值                                 
	  iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="受理日期";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="经办人";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="送核日期";         		//列名
	  iArray[10][1]="80px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[11]=new Array();
	  iArray[11][0]="送核时间";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  EdorGrid = new MulLineEnter("fm", "EdorGrid"); 
	  EdorGrid.mulLineCount = 0;
	  EdorGrid.displayTitle = 1;
	  EdorGrid.locked = 1;
	  EdorGrid.canSel = 1;
	  EdorGrid.hiddenPlus = 1;
	  EdorGrid.hiddenSubtraction = 1;
	  EdorGrid.loadMulLine(iArray);
	  EdorGrid.selBoxEventFuncName = "openEdorUwWindow";
  }
  catch(ex)
  {
    alert(ex);
  }
}

//初始化既往保全核保列表
function initOldEdorGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="工作流任务号";         		//列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[2]=new Array();
	  iArray[2][0]="工作流子任务号";         		//列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="保全受理号";         		//列名
	  iArray[3][1]="120px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="保单号";         		//列名
	  iArray[4][1]="100px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="投保人客户号";         		//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="投保人";         		//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许           
	
	  iArray[7]=new Array();                                                       
	  iArray[7][0]="受理人";         		//列名                                     
	  iArray[7][1]="60px";            		//列宽                                   
	  iArray[7][2]=100;            			//列最大值                                 
	  iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="受理日期";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="经办人";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="送核日期";         		//列名
	  iArray[10][1]="80px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[11]=new Array();
	  iArray[11][0]="送核时间";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[12]=new Array();
	  iArray[12][0]="核保日期";         		//列名
	  iArray[12][1]="80px";            		//列宽
	  iArray[12][2]=100;            			//列最大值
	  iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[13]=new Array();
	  iArray[13][0]="核保师工号";         		//列名
	  iArray[13][1]="80px";            		//列宽
	  iArray[13][2]=100;            			//列最大值
	  iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  OldEdorGrid = new MulLineEnter("fm", "OldEdorGrid"); 
	  OldEdorGrid.mulLineCount = 0;
	  OldEdorGrid.displayTitle = 1;
	  OldEdorGrid.locked = 1;
	  OldEdorGrid.canSel = 1;
	  OldEdorGrid.hiddenPlus = 1;
	  OldEdorGrid.hiddenSubtraction = 1;
	  OldEdorGrid.loadMulLine(iArray);
	  OldEdorGrid.selBoxEventFuncName = "openOldEdorUwWindow";
  }
  catch(ex)
  {
    alert(ex);
  }
}

//初始化理赔续保待核受理
function initLPXBGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="工作流任务号";         		//列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[2]=new Array();
	  iArray[2][0]="工作流子任务号";         		//列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="案件号";         		//列名
	  iArray[3][1]="120px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="保单号";         		//列名
	  iArray[4][1]="120px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="投保人客户号";         		//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="投保人";         		//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许           
	
	  iArray[7]=new Array();                                                       
	  iArray[7][0]="受理人";         		//列名                                     
	  iArray[7][1]="60px";            		//列宽                                   
	  iArray[7][2]=100;            			//列最大值                                 
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="受理日期";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="经办人";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="送核日期";         		//列名
	  iArray[10][1]="80px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[11]=new Array();
	  iArray[11][0]="送核时间";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许   

	  iArray[12]=new Array();
	  iArray[12][0]="工单号";         		//列名
	  iArray[12][1]="0px";            		//列宽
	  iArray[12][2]=100;            			//列最大值
	  iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  LPXBGrid = new MulLineEnter("fm", "LPXBGrid"); 
	  LPXBGrid.mulLineCount = 0;
	  LPXBGrid.displayTitle = 1;
	  LPXBGrid.locked = 1;
	  LPXBGrid.canSel = 1;
	  LPXBGrid.hiddenPlus = 1;
	  LPXBGrid.hiddenSubtraction = 1;
	  LPXBGrid.loadMulLine(iArray);
	  LPXBGrid.selBoxEventFuncName = "openLPXBUwWindow";
  }
  catch(ex)
  {
    alert(ex);
  }
}

//理赔续保待核待回复
function initLPNoReplyEdorGrid()
{
	var iArray = new Array();
  try
  {
	  	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="工作流任务号";         		//列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[2]=new Array();
	  iArray[2][0]="工作流子任务号";         		//列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="案件号";         		//列名
	  iArray[3][1]="120px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="保单号";         		//列名
	  iArray[4][1]="120px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="投保人客户号";         		//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="投保人";         		//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许           
	
	  iArray[7]=new Array();                                                       
	  iArray[7][0]="受理人";         		//列名                                     
	  iArray[7][1]="60px";            		//列宽                                   
	  iArray[7][2]=100;            			//列最大值                                 
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="受理日期";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="经办人";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="送核日期";         		//列名
	  iArray[10][1]="80px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[11]=new Array();
	  iArray[11][0]="送核时间";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许   

	  iArray[12]=new Array();
	  iArray[12][0]="工单号";         		//列名
	  iArray[12][1]="0px";            		//列宽
	  iArray[12][2]=100;            			//列最大值
	  iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[13]=new Array();
	  iArray[13][0]="核保状态";         		//列名
	  iArray[13][1]="80px";            		//列宽
	  iArray[13][2]=100;            			//列最大值
	  iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
	  
	  LPNoReplyEdorGrid = new MulLineEnter("fm", "LPNoReplyEdorGrid"); 
	  LPNoReplyEdorGrid.mulLineCount = 0;
	  LPNoReplyEdorGrid.displayTitle = 1;
	  LPNoReplyEdorGrid.locked = 1;
	  LPNoReplyEdorGrid.canSel = 1;
	  LPNoReplyEdorGrid.hiddenPlus = 1;
	  LPNoReplyEdorGrid.hiddenSubtraction = 1;
	  LPNoReplyEdorGrid.loadMulLine(iArray);
	  LPNoReplyEdorGrid.selBoxEventFuncName = "openLPNoReplyEdorWindow";
  }
  catch(ex)
  {
    alert(ex);
  }
}

//既往理赔续保待核信息
function initLPOldEdorGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="工作流任务号";         		//列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[2]=new Array();
	  iArray[2][0]="工作流子任务号";         		//列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="案件号";         		//列名
	  iArray[3][1]="120px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="保单号";         		//列名
	  iArray[4][1]="120px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="投保人客户号";         		//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="投保人";         		//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许           
	
	  iArray[7]=new Array();                                                       
	  iArray[7][0]="受理人";         		//列名                                     
	  iArray[7][1]="60px";            		//列宽                                   
	  iArray[7][2]=100;            			//列最大值                                 
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="受理日期";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="经办人";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="送核日期";         		//列名
	  iArray[10][1]="80px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[11]=new Array();
	  iArray[11][0]="送核时间";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许   

	  iArray[12]=new Array();
	  iArray[12][0]="工单号";         		//列名
	  iArray[12][1]="0px";            		//列宽
	  iArray[12][2]=100;            			//列最大值
	  iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[13]=new Array();
	  iArray[13][0]="核保日期";         		//列名
	  iArray[13][1]="80px";            		//列宽
	  iArray[13][2]=100;            			//列最大值
	  iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[14]=new Array();
	  iArray[14][0]="核保师工号";         		//列名
	  iArray[14][1]="100px";            		//列宽
	  iArray[14][2]=100;            			//列最大值
	  iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  
	  LPOldEdorGrid = new MulLineEnter("fm", "LPOldEdorGrid"); 
	  LPOldEdorGrid.mulLineCount = 0;
	  LPOldEdorGrid.displayTitle = 1;
	  LPOldEdorGrid.locked = 1;
	  LPOldEdorGrid.canSel = 1;
	  LPOldEdorGrid.hiddenPlus = 1;
	  LPOldEdorGrid.hiddenSubtraction = 1;
	  LPOldEdorGrid.loadMulLine(iArray);
	  LPOldEdorGrid.selBoxEventFuncName = "openLPOldEdorWindow";
  }
  catch(ex)
  {
    alert(ex);
  }
}

//初始化理赔二核列表
function initSecondLPGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="工作流任务号";         		//列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[2]=new Array();
	  iArray[2][0]="工作流子任务号";         		//列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="案件号";         		//列名
	  iArray[3][1]="120px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="投保人客户号";         		//列名
	  iArray[4][1]="800px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="被保人";         		//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="受理人";         		//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许           
	
	  iArray[7]=new Array();                                                       
	  iArray[7][0]="受理日期";         		//列名                                     
	  iArray[7][1]="60px";            		//列宽                                   
	  iArray[7][2]=100;            			//列最大值                                 
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="经办人";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="送核日期";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="送核时间";         		//列名
	  iArray[10][1]="80px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[11]=new Array();
	  iArray[11][0]="工单号";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许   

	  
	  SecondLPGrid = new MulLineEnter("fm", "SecondLPGrid"); 
	  SecondLPGrid.mulLineCount = 0;
	  SecondLPGrid.displayTitle = 1;
	  SecondLPGrid.locked = 1;
	  SecondLPGrid.canSel = 1;
	  SecondLPGrid.hiddenPlus = 1;
	  SecondLPGrid.hiddenSubtraction = 1;
	  SecondLPGrid.loadMulLine(iArray);
	  SecondLPGrid.selBoxEventFuncName = "openSecondLPUwWindow";
  }
  catch(ex)
  {
    alert(ex);
  }
}

//既往理赔二次核保信息
function initLPTwoOldEdorGrid()
{
	var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="工作流任务号";         		//列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[2]=new Array();
	  iArray[2][0]="工作流子任务号";         		//列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="案件号";         		//列名
	  iArray[3][1]="120px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="投保人客户号";         		//列名
	  iArray[4][1]="800px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="被保人";         		//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="受理人";         		//列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许           
	
	  iArray[7]=new Array();                                                       
	  iArray[7][0]="受理日期";         		//列名                                     
	  iArray[7][1]="60px";            		//列宽                                   
	  iArray[7][2]=100;            			//列最大值                                 
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="经办人";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[9]=new Array();
	  iArray[9][0]="送核日期";         		//列名
	  iArray[9][1]="80px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[10]=new Array();
	  iArray[10][0]="送核时间";         		//列名
	  iArray[10][1]="80px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
	  
	  iArray[11]=new Array();
	  iArray[11][0]="工单号";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许   

	  
	  LPTwoOldEdorGrid = new MulLineEnter("fm", "LPTwoOldEdorGrid"); 
	  LPTwoOldEdorGrid.mulLineCount = 0;
	  LPTwoOldEdorGrid.displayTitle = 1;
	  LPTwoOldEdorGrid.locked = 1;
	  LPTwoOldEdorGrid.canSel = 1;
	  LPTwoOldEdorGrid.hiddenPlus = 1;
	  LPTwoOldEdorGrid.hiddenSubtraction = 1;
	  LPTwoOldEdorGrid.loadMulLine(iArray);
	  LPTwoOldEdorGrid.selBoxEventFuncName = "openLPTwoOldEdorWindow";
  }
  catch(ex)
  {
    alert(ex);
  }
}

</script>