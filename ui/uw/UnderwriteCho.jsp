<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UnderwriteCho.jsp
//程序功能：个人人工核保
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//1-得到所有纪录，显示纪录值
  int index=0;
  int TempCount=0;
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  
  //得到radio列的数组
  System.out.println("---4----");  
  
  String tRadio[] = request.getParameterValues("InpPolAddGridSel");  
  String tTempClassNum[] = request.getParameterValues("PolAddGridNo");
  String tPolCode[] = request.getParameterValues("PolAddGrid1");
  String tProposalCode[] = request.getParameterValues("PolAddGrid2");
  
  //得到check列的数组
  //String tChk[] = request.getParameterValues("PolAddGridChk");  
  
  int temp = tRadio.length;
  System.out.println("radiolength:"+temp);
  
  //保单表 
  LCPolSchema mLCPolSchema = new LCPolSchema();
  LCUWMasterSchema mLCUWMasterSchema = new LCUWMasterSchema();
  if(tTempClassNum!=null)//如果不是空纪录	
  {
  	TempCount = tTempClassNum.length; //记录数      
   	System.out.println("Start query Count="+TempCount);   
  	while(index<TempCount)
  	{
  		System.out.println("----3-----");
  		System.out.println("polcode:"+tPolCode[index]);
  		System.out.println("radio:"+tRadio[index]);
  		if (!tPolCode[index].equals("")&&tRadio[index].equals("1"))
  		{
  			System.out.println("GridNO="+tTempClassNum[index]);
  			System.out.println("Grid 1="+tPolCode[index]);
  			System.out.println("Grid 2="+tProposalCode[index]);
  			System.out.println("Radio:"+tRadio[index]);
  			
  			//if(tRadio[index].equals("1"))
    			System.out.println("this radio is selected");
    			
    			//查询并显示单条信息
    			LCPolSchema tLCPolSchema = new LCPolSchema();
    			
    			tLCPolSchema.setPolNo(tPolCode[index]);
    			
    			// 准备传输数据 VData
			VData tVData = new VData();
			VData tVData2 = new VData();
			tVData.addElement(tLCPolSchema);

			// 数据传输
  			ProposalQueryUI tProposalQueryUI   = new ProposalQueryUI();
  			LCUWMasterQueryUI tLCUWMasterQueryUI = new LCUWMasterQueryUI();
  			
			
			if (!tProposalQueryUI.submitData(tVData,"QUERY||MAIN"))
			{
      				Content = " 查询失败，原因是: " + tProposalQueryUI.mErrors.getError(0).errorMessage;
      				FlagStr = "Fail";
			}
			else
			 if (!tLCUWMasterQueryUI.submitData(tVData,"QUERY||MAIN"))
			 {
      			Content = " 查询失败，原因是: " + tLCUWMasterQueryUI.mErrors.getError(0).errorMessage;
      			FlagStr = "Fail";
      			System.out.println("查询失败LCUWMasterQueryUI");
			 }
			else
			{
				tVData.clear();
				tVData = tProposalQueryUI.getResult();
		        tVData2 = tLCUWMasterQueryUI.getResult();
				// 显示
				LCPolSet mLCPolSet = new LCPolSet(); 
				LCUWMasterSet mLCUWMasterSet = new LCUWMasterSet();
				mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
				mLCUWMasterSet.set((LCUWMasterSet)tVData2.getObjectByObjectName("LCUWMasterSet",0));
				
				if (mLCPolSet.size() == 1)
				{
					mLCPolSchema = mLCPolSet.get(1);
					System.out.println("---LCPolSchema ok ---");
				}
				System.out.println("---LCUWMasterSet.size()---"+mLCUWMasterSet.size());
				if (mLCUWMasterSet.size() == 1)
				{
					mLCUWMasterSchema = mLCUWMasterSet.get(1);
					System.out.println("---LCUWMasterSchema ok---");
				}
				System.out.println("proposalno:"+mLCPolSchema.getProposalNo().trim());
				System.out.println("mainpolno:"+mLCPolSchema.getMainPolNo());
				
				if(mLCPolSchema.getProposalNo().equals(mLCPolSchema.getMainPolNo()))
				{
%>
<html>
	<head>
				<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
				<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
				<SCRIPT src="Underwrite.js"></SCRIPT>
	</head>
	<script language="javascript">
				showAddButton();
	</script>
</html>			
<%
				}
				else
				{
%>
<html>
	<head>
				<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
				<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
				<SCRIPT src="Underwrite.js"></SCRIPT>
	</head>
	<script language="javascript">
  				hideAddButton();
  	</script>
</html>				
<%
				}									
%>
			<script language="javascript">
				parent.fraInterface.fm.ProposalNo.value="<%=mLCPolSchema.getProposalNo()%>";
          		parent.fraInterface.fm.RiskCode.value="<%=mLCPolSchema.getRiskCode()%>";
         	 	parent.fraInterface.fm.RiskVersion.value="<%=mLCPolSchema.getRiskVersion()%>";
                parent.fraInterface.fm.ManageCom.value="<%=mLCPolSchema.getManageCom()%>";
                parent.fraInterface.fm.AppntNo.value="<%=mLCPolSchema.getAppntNo()%>";
                parent.fraInterface.fm.AppntName.value="<%=mLCPolSchema.getAppntName()%>";
                parent.fraInterface.fm.InsuredNo.value="<%=mLCPolSchema.getInsuredNo()%>";
                parent.fraInterface.fm.InsuredName.value="<%=mLCPolSchema.getInsuredName()%>";
                parent.fraInterface.fm.InsuredSex.value="<%=mLCPolSchema.getInsuredSex()%>";
                parent.fraInterface.fm.Mult.value="<%=mLCPolSchema.getMult()%>";
                parent.fraInterface.fm.Prem.value="<%=mLCPolSchema.getPrem()%>";
                parent.fraInterface.fm.Amnt.value="<%=mLCPolSchema.getAmnt()%>";
                parent.fraInterface.fm.PrtNoHide.value="<%=mLCPolSchema.getPrtNo()%>";
                parent.fraInterface.fm.MainPolNoHide.value="<%=mLCPolSchema.getMainPolNo()%>";
                parent.fraInterface.fm.UWGrade.value="<%=mLCUWMasterSchema.getUWGrade()%>";
                parent.fraInterface.fm.AppGrade.value="<%=mLCUWMasterSchema.getAppGrade()%>";
               
            </script>
         
<%
			} // end of if
  			System.out.println("---2---");
			//如果在Catch中发现异常，则不从错误类中提取错误信息
			if (FlagStr == "Fail")
			{
				tError = tProposalQueryUI.mErrors;
				if (!tError.needDealError())
				{                          
			    		Content = " 查询成功! ";
			    		FlagStr = "Succ";
				}
			 	else                                                                           
 			 	{
 			   		Content = " 查询失败，原因是:" + tError.getFirstError();
  			  		FlagStr = "Fail";
  			 	}
 			}
    			
    			if(tRadio[index].equals("1"))
      				System.out.println("the "+index+" line is checked!");
    			else
      				System.out.println("the "+index+" line is not checked!");
      		}
    		
    		index=index+1;
    		System.out.println("index:"+index);          
	}
}
   
%>  