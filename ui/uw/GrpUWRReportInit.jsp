<%
//GrpUWRReportInit.jsp
//Function：立案界面的初始化程序
//Date：2005-08-1 
//Author  ：cuiwei
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%
	  String tContNo = "";
	  String tPrtNo = "";
	 
	  String tFlag = "";
	  tContNo = request.getParameter("ContNo2");  
	   tPrtNo = request.getParameter("PrtNo");  
	
	  tFlag = request.getParameter("Flag");
	%>    	
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	String strOperator = globalInput.Operator;
	
%>

<script language="JavaScript">
var turnPage = new turnPageClass(); 

function initForm(tContNo,tPrtNo)
{
  var str = "";
  try
  {
  fm.Resource.value=Resource;
    if(Resource==1|Resource==2|Resource==3){
  GrpQuest11.style.display="none";
  }
  initInvestigateGrid();
 initList();
  ListQuery();
  }
  catch(re)
  {
    alert("PEdorUWManuRReportInit-->InitForm函数中发生异常:初始化界面错误!");
  }
  
}

//立案附件信息
function initInvestigateGrid()
{
    var iArray = new Array();

      try
      {
          iArray[0]=new Array("序号","30px","0",0);
          iArray[1]=new Array("编码","30px","0",0);
          iArray[2]=new Array("项目名称","200px",0);
	        iArray[3]=new Array("契调原因","200px","20",1);

	
      InvestigateGrid = new MulLineEnter( "fm" , "InvestigateGrid" );
      //这些属性必须在loadMulLine前
      InvestigateGrid.mulLineCount = 47;
      InvestigateGrid.displayTitle = 1;
      InvestigateGrid.canChk = 1;
      InvestigateGrid.hiddenPlus=1;   
      InvestigateGrid.hiddenSubtraction=1; 
      InvestigateGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //InvestigateGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }


</script>