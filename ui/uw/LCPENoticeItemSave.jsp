<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LCPENoticeItemSave.jsp
//程序功能：
//创建日期：2005-01-15 13:07:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
<%
	  //接收信息，并作校验处理。
	  //输入参数
//	  LCPENoticeItemSchema tLCPENoticeItemSchema   = new LCPENoticeItemSchema();
	  
	  LCPENoticeItemUI tLCPENoticeItemUI   = new LCPENoticeItemUI();
	  LCPENoticeItemSet tLCPENoticeItemSet = new LCPENoticeItemSet();//p1
	  
	  //输出参数
	  CErrors tError = null;
	  String tRela  = "";                
	  String FlagStr = "";
	  String Content = "";
	  String transact = "";
	  GlobalInput tG = new GlobalInput(); 
	  tG=(GlobalInput)session.getValue("GI");
		
	  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	  
	  String tS1[] = request.getParameterValues("HealthGrid1");		//P2
	  String tS2[] = request.getParameterValues("HealthGrid2");
    String tS5[] = request.getParameterValues("HealthGrid5");
	  
	  int count = 0;
	count = tS1.length;
	if(tS1 != null)     
	{	
			for(int i = 0; i < count; i++)
			{
					   LCPENoticeItemSchema tLCPENoticeItemSchema = new LCPENoticeItemSchema(); //p3
						System.out.println(tS1[i]);
						tLCPENoticeItemSchema.setGrpContNo("00000000000000000000");
					  tLCPENoticeItemSchema.setPEItemCode(tS1[i]);
					  tLCPENoticeItemSchema.setPEItemName(tS2[i]);	
					  tLCPENoticeItemSchema.setContNo(request.getParameter("ContNo"));
					  tLCPENoticeItemSchema.setProposalContNo(request.getParameter("ContNo"));
					  tLCPENoticeItemSchema.setPrtSeq(request.getParameter("PrtSeq"));
					  tLCPENoticeItemSchema.setTestGrpCode(request.getParameter("GrpTestCode"));
					  //tLCPENoticeItemSchema.setPEStateFlag(request.getParameter("Pestateflag"));
					  tLCPENoticeItemSchema.setPEStateFlag(tS5[i]);
					  tLCPENoticeItemSchema.setFreePE("N");
					  tLCPENoticeItemSchema.setTestGrpCode(request.getParameter("HospitCode"));
					  tLCPENoticeItemSet.add(tLCPENoticeItemSchema);			  
			}
	}
	  
	  
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
//	tVData.add(tLCPENoticeItemSchema);
 		tVData.add(tLCPENoticeItemSet); //p4
  	tVData.add(tG);
    tLCPENoticeItemUI.submitData(tVData,"UPDATE||MAIN");
  
  }
  catch(Exception ex)
  {
    Content="保存失败，可能是检查项目有重复，请检查！";
    //Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLCPENoticeItemUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content="保存失败，可能是检查项目有重复，请检查！";
    	//Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
