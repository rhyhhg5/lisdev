<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：FinFeeSureChk.jsp
//程序功能：到帐确认
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
System.out.println("Auto-begin:");

%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.taskservice.*"%>  
  <%@page import="com.sinosoft.task.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%

  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tGI = new GlobalInput();
  
	tGI=(GlobalInput)session.getValue("GI");
 
  	if(tGI == null) {
		out.println("session has expired");
		return;
	}        
  //校验处理
  //内容待填充
  
  	//接收信息
  	// 投保单列表
	LJTempFeeClassSet tLJTempFeeClassSet = new LJTempFeeClassSet();

	String tTempFeeNo[] = request.getParameterValues("PolGrid1");
	String tPayintv[] = request.getParameterValues("PolGrid3");
	String tDate[] = request.getParameterValues("PolGrid7");
	String tOtherNo[] = request.getParameterValues("PolGrid8");
	String tTempFeeType[] = request.getParameterValues("PolGrid9");
	String tChk[] = request.getParameterValues("InpPolGridChk");
	
	boolean flag = false;
	int feeCount = tTempFeeNo.length;
	System.out.println("feecout:"+feeCount);
	

	for (int i = 0; i < feeCount; i++)
	{
		if (!tTempFeeNo.equals("") && tChk[i].equals("1"))
		{
			if (tDate[i].equals(""))
			{
				Content = " 请录入到帐日期! ";
			}
			else
			{
				System.out.println("TempFeeNo:"+i+":"+tTempFeeNo[i]);
	  			LJTempFeeClassSchema tLJTempFeeClassSchema = new LJTempFeeClassSchema();
	
		    		tLJTempFeeClassSchema.setTempFeeNo( tTempFeeNo[i] );
		    		tLJTempFeeClassSchema.setPayMode( tPayintv[i] );
		    		tLJTempFeeClassSchema.setEnterAccDate( tDate[i] );
	    
		    		tLJTempFeeClassSet.add( tLJTempFeeClassSchema );
		    		flag = true;
		    	}
		}
	}
	System.out.println("tLJTempFeeClassSet:" + tLJTempFeeClassSet.encode());

	try
	{
		if (flag == true)
		{
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.add( tLJTempFeeClassSet );
			tVData.add( tGI );
			
			// 数据传输
			FinFeeSureUI tFinFeeSureUI   = new FinFeeSureUI();
			if (tFinFeeSureUI.submitData(tVData,"INSERT") == false)
			{
				int n = tFinFeeSureUI.mErrors.getErrorCount();
				for (int i = 0; i < n; i++)
				System.out.println("Error: "+tFinFeeSureUI.mErrors.getError(i).errorMessage);
				Content = " 到帐确认失败，原因是: " + tFinFeeSureUI.mErrors.getError(0).errorMessage;
				FlagStr = "Fail";
			}
			
			//如果在Catch中发现异常，则不从错误类中提取错误信息
			if (FlagStr == "Fail")   //==错误
			{
				tError = tFinFeeSureUI.mErrors;
				//tErrors = tFinFeeSureUI.mErrors;
				if (!tError.needDealError())
				{                          
					Content = " 到帐确认成功! ";
					FlagStr = "Succ";
				}
				else                                                                           
				{
					Content = " 到帐确认失败，原因是：";
					int n = tError.getErrorCount();
					if (n > 0)
					{
					  for(int i = 0;i < n;i++)
					  {
					    //tError = tErrors.getError(i);
					    Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
					  }
					}
					FlagStr = "Fail";
				}
			}
			if (FlagStr.equals("Succ"))
			{
				if ((tTempFeeType != null) && (tTempFeeType[0] != null) && (tTempFeeType[0].equals("4")))
				{
					BqFinishTask tBqFinishTask = new BqFinishTask(tGI.ManageCom);
					tBqFinishTask.run();
				}
			}
		}  
	}
	catch(Exception e)
	{
		e.printStackTrace();
		Content = Content.trim() +" 提示:异常退出.";
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.initPolGrid();
</script>
</html>
