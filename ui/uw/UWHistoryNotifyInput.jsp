<%@page contentType="text/html;charset=gb2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：UWHistoryNotifyInput.jsp
	//程序功能：既往告知整理录入
	//创建日期：2012-10-15
	//创建人  ：张成轩
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="./UWHistoryNotifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<%@include file="UWHistoryNotifyInit.jsp"%>
		<title>新契约体检资料录入</title>

	</head>
	<body onload="initForm('<%=tContNo%>','<%=tPrtNo%>','<%=tLoadFlag%>');">
		<form method=post name=fm target="fraSubmit">
			<INPUT type="hidden" class=Common name=PrtNo value="">
			<Input type="hidden" class="readonly" name=ContNo>
			<Input type="hidden" class="readonly" name=InsureNo>
			<DIV id=DivLCImpart STYLE="display: ''">
				<!-- 告知信息部分（列表） -->
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
								OnClick="showPage(this,divLCImpart2);">
						</td>
						<td class=titleImg>
							被保险人告知明细信息
						</td>
					</tr>
				</table>

				<div id="divLCImpart2" style="display: ''">
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanImpartDetailGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
			</DIV>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
							OnClick="showPage(this,divUWSpec1);">
					</td>
					<td class=titleImg>
						疾病结果
					</td>
				</tr>
			</table>
			<Div id="divUWDis" style="display: ''">
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanDisDesbGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<br>
			<INPUT VALUE="返  回" class=cssButton TYPE=button
				onclick="parent.close();">

			<!--读取信息-->
		</form>
		<span id="spanCode" style="display: ''; position: absolute;"></span>
	</body>
</html>