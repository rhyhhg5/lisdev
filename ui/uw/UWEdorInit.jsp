<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWAppInit.jsp
//程序功能：既往投保信息查询
//创建日期：2002-06-19 11:10:36
//创建人  ： WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
<%
  String tContNo = "";
  String tCustomerNo = "";
  String  tPrtNo= "";
  tContNo = request.getParameter("ContNo");
  tCustomerNo = request.getParameter("CustomerNo");
  tPrtNo = request.getParameter("Prtno");
  System.out.println("ContNo:"+tContNo);
  System.out.println("CustomerNo:"+tCustomerNo);
  System.out.println("PrtNo:"+tPrtNo);
%>                            
<SCRIPT>
var turnPage = new turnPageClass();
var strSQL;
var mulLineC;
function initForm()
{
	initbqViewGrid();
	initHide("<%=tContNo%>","<%=tCustomerNo%>","<%=tPrtNo%>");
}

//查询作业历史(个单)
function queryPEndorItemGrid()
{
	strSQL ="select contno,edoracceptno,makedate,"+
					"(select edorname from lmedoritem where appobj='I' and edorcode=edortype) "+
		      "from lpedoritem "+
					"where edoracceptno not in (select edoracceptno from lpgrpedoritem)"+
					"and edorstate='0' "+
					"and insuredno='"+tinsuredno+"'";
		
	turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, bqViewGrid);
	
	mulLineC=bqViewGrid.mulLineCount;
	
	if(mulLineC==0)
	{
	fm.Query.disabled=true;
	}
	return true;
}

/* 查看批单 */
function viewpidan()
{	
	var chkNum = 0;
	var WorkNo;


	
	for (i = 0; i < bqViewGrid.mulLineCount; i++)
	{
		if (bqViewGrid.getChkNo(i)) 
		{
			chkNum = chkNum + 1;
			WorkNo = bqViewGrid.getRowColData(i, 2);

		}
	}
	if(chkNum > 1)
	{
		alert("您只能选择一条工单");
	}
	else if(chkNum == 0)
	{
		alert("请先选择一条工单！"); 
	}
 else	if(WorkNo )
	{
		window.open("../bq/ShowEdorPrint.jsp?EdorAcceptNo="+WorkNo )
		
	}

	
}

/* 查看扫描 */
function ScanQuery() {
	var chkNum = 0;
	var WorkNo;

	
	for (i = 0; i < bqViewGrid.mulLineCount; i++)
	{
		if (bqViewGrid.getChkNo(i)) 
		{
			chkNum = chkNum + 1;
			WorkNo = bqViewGrid.getRowColData(i, 1);
			
			
		}
	}
	if(chkNum > 1)
	{
		alert("您只能选择一条工单");
	}
	else if(chkNum == 0)
	{
		alert("请先选择一条工单！"); 
	}
	else {
	   			
		
		if (WorkNo == "") return;
		    
	
		window.open("../sys/ProposalEasyScan.jsp?prtNo="+WorkNo+"&SubType=TB1001&BussType=TB&BussNoType=11", "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");								
	}	     
}

//初始化项目列表
function initbqViewGrid()
{                               
	var iArray = new Array();
	try
	{
	  	iArray[0]=new Array();
	  	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  	iArray[0][1]="30px";            		//列宽
	  	iArray[0][2]=10;            			//列最大值
	  	iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
	  	iArray[1]=new Array();
	  	iArray[1][0]="合同号";
	  	iArray[1][1]="100px";
	  	iArray[1][2]=100;
	  	iArray[1][3]=0;
	  	
	  	iArray[2]=new Array();
	  	iArray[2][0]="受理号";
	  	iArray[2][1]="100px";
	  	iArray[2][2]=100;
	  	iArray[2][3]=0;
	  	
	  	iArray[3]=new Array();
	  	iArray[3][0]="受理时间";
	  	iArray[3][1]="100px";
	  	iArray[3][2]=100;
	  	iArray[3][3]=0;

	  	
	  	iArray[4]=new Array();
	  	iArray[4][0]="变更项目";
	  	iArray[4][1]="100px";
	  	iArray[4][2]=100;
	  	iArray[4][3]=0;        
	  	
	  	
		bqViewGrid = new MulLineEnter( "fm" , "bqViewGrid" ); 
		//这些属性必须在loadMulLine前
		bqViewGrid.mulLineCount = 0;   
		bqViewGrid.displayTitle = 1;
		bqViewGrid.canSel =0;
	  bqViewGrid.canChk =1;
		bqViewGrid.selBoxEventFuncName ="getRowData" ;     //点击RadioBox时响应的JS函数
		bqViewGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		bqViewGrid.hiddenSubtraction=1;
		bqViewGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
	queryPEndorItemGrid();
	
}
function showInsuredInfo() {
  //将进入被保险人信息的方式由Multline变成按钮因此,不要从multline中取数
  //改为查询
  //var tSelNo = PolAddGrid.getSelNo();
 	var tPrtNo=fm.all('PrtNoHide').value;
 	var tContNo=fm.all('ContNoHide').value;
 	//alert(tPrtNo);
 	//alert(tContNo);
  var strSql = "select InsuredNo from lcinsured where prtno='"+tPrtNo
               +"' and SequenceNo='1'";
  //var tInsuredNo = PolAddGrid.getRowColData(tSelNo - 1,1);
  var tInsuredNo = easyExecSql(strSql);
  if(!tInsuredNo) {
      alert("未查询到第1被保险人!");
      return;
    }
//  var tMissionID = fm.MissionID.value;
//  var tSubMissionID = fm.SubMissionID.value;
//  var tSendFlag = fm.SendFlag.value;
  window.open("./InsuredUWInfoMain.jsp?ScanFlag=0&ContNo="+tContNo+"&InsuredNo="+tInsuredNo+"&PrtNo="+tPrtNo);
}
function initHide(tContNo,tCustomerNo,tPrtNo)
{

	fm.all('InsureNoHide').value= tCustomerNo;
	fm.all('ContNoHide').value = tContNo;
	fm.all('PrtNoHide').value =tPrtNo;	
}



</SCRIPT>