<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：GroupUWAuto.jsp
//程序功能：团体核保订正
//创建日期：2004-12-08 11:10:36
//创建人  ：HEYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人	
	String tContNo = "";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>	
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="GroupUWModify.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GroupUWModifyInit.jsp"%>
  <title>自动核保 </title>
</head>
<body  onload="initForm();" >
  <form action="./GroupUWModifyChk.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询团体条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
         <!-- <TD  class= title>
            团体投保单号
          </TD>
          <TD  class= input>-->
            <Input class= common name=ProposalGrpContNo type="hidden">
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
            
          </TD>
          <TD  class= title>
            团体客户号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNo verify="团体客户号|int&len=8">
          </TD>          
          <TD  class= title>
            投保单位名称
          </TD>
          <TD  class= input>
            <Input class= common name=Name verify="投保单位名称|len<=120">
          </TD>
        </TR>
        <TR  class= common>          
           <TD  class= title>
            业务员代码
          </TD>
          <TD  class= input>
            <Input class=codeNo name=AgentCode ondblclick="return showCodeList('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet2',[this,AgentCodeName,AgentGroup,AgentGroupName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
          </TD> 
          <TD  class= title style="display:'none'">
            业务员组别
          </TD>
          <TD  class= input style="display:'none'">
            <Input class="codeNo" name=AgentGroup verify="业务员组别|code:branchcode" ondblclick="return showCodeList('agentgroup2',[this,AgentGroupName],[0,1],null,fm.all('ManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('agentgroup2',[this,AgentGroupName],[0,1]);"><input class=codename name=AgentGroupName readonly=true >
          </TD>
        </TR>
        
    </table>
          <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrp1);">
    		</td>
    		<td class= titleImg>
    			 团体投保单信息
    		</td>
    	</tr>
    	<tr>
    	    <INPUT  type= "hidden" class= Common name= GrpContNo  value= "">
          <INPUT  type= "hidden" class= Common name= MissionID  value= "">
          <INPUT  type= "hidden" class= Common name= SubMissionID  value= "">
    	</tr>
    </table>
  	<Div  id= "divLCGrp1" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpGrid" >
  					</span> 
  			  	</td>
  			</tr>

    	</table>
      <INPUT VALUE="首  页"  class =  cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="getLastPage();"> 	
  	<table class= common>
  			<TR class= common>
  				<td class= titleImg>
    			 	核保订正原因
    			</td>
  			</TR>
  	  	<TR class= common>
			   		<TD class= common>
                <textarea name="tUWIdea" verify="核保订正原因|len<800" verifyorder="1" cols="110" rows="3" class="common">
                </textarea>		
            </TD>
         </TR>
    </table>				
  	</div>
  	<p>
      <INPUT VALUE="核保订正" class = cssButton TYPE=button onclick="GrpUWModify();"> 
      <!--INPUT VALUE="设置特殊标志" class = cssButton TYPE=button onclick="SetSpecialFlag();"--> 
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
