//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  UnChangeDecodeStr();
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}






// 查询按钮
function easyQueryClick()
{
  // 书写SQL语句
  var strsql = "";
  var strsqlre = "";
  var tContNo = "";
  var tPrtseq = "";
  var tCustomerno = "";
  tContNo = fm.ContNo.value;
  tCustomerno = fm.Customerno.value;
  tPrtseq = fm.PrtSeq.value;
  if(tContNo != "" && tCustomerno != "")
  {
    strsql = "select a.name,b.Rreportitemname,b.RRItemContent,b.RRItemResult,b.Rreportitemcode from LCRReport a ,LCRReportItem b where 1=1"
             + " and a.ContNo=b.ContNo  and a.prtseq = b.prtseq and a.ContNo = '"+ tContNo + "'"
             + " and a.PrtSeq = '"+ tPrtseq + "'"
             + " and a.customerno = '"+ tCustomerno + "'";

    //查询SQL，返回结果字符串
    turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);

    //判断是否查询成功
    if (!turnPage.strQueryResult)
    {
      return "";
    }

    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

    //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = HealthGrid;

    //保存SQL语句
    turnPage.strQuerySql = strsql;

    //设置查询起始位置
    turnPage.pageIndex = 0;

    //在查询结果数组中取出符合页面显示大小设置的数组
    var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    //******契约调查项目明细录入框//
    turnPage.pageDisplayGrid = UWErrGrid;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    //*******end//
    
    strsqlre = "select contente from LCRReport where "
             + " ProposalContNo = '"+ tContNo + "'"
             + " and PrtSeq = '"+ tPrtseq + "'";
    var arrreport = easyExecSql(strsqlre);

    if(arrreport)
    {
      fm.Content.value=arrreport[0][0];
    }
     
  }
  return true;
}




//********modify//
function modifyRecord()
{
  var i = 0;
  var checkFlag = 0;
  
  for (i=0; i<UWErrGrid.mulLineCount; i++) 
  {
    if (UWErrGrid.getSelNo(i)) 
    { 
      checkFlag = UWErrGrid.getSelNo();
      break;
    }
  }
  
  if (checkFlag) 
  {
    fm.RReportItemName.value = UWErrGrid.getRowColData(checkFlag - 1, 2);
    fm.RRItemContent.value   = UWErrGrid.getRowColData(checkFlag - 1, 3);
    fm.RRItemResult.value    = UWErrGrid.getRowColData(checkFlag - 1, 4);
    fm.RReportItemCode.value = UWErrGrid.getRowColData(checkFlag - 1, 5);
   }
  else 
  {
    alert("请先选择一条契调报告信息保存！"); 
    return;
  }

  fm.all('fmAction').value="MODIFY||RECORD";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}

//******//

function modifycontentRecord()
{
  if(trim(fm.Content.value)=="")
  {
  	alert("其它契调信息结论录入框为空，请检查！");
    return;
  }
		if( confirm("是否保存契调结果?保存请点击“确定”，否则请点“取消”。"))
		{
		}
		else
		{
			return;
		}
  fm.all('fmAction').value="MODIFY||CONTENT";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}

function ImpartToICD() 
{
  var cContNo = fm.ContNo.value;
  var PrtNo = fm.PrtNo.value;
  if (cContNo!="" && PrtNo!="") 
  {
      window.open("../uw/ImaprtToICDMain.jsp?ContNo="+cContNo+"&PrtNo="+PrtNo+"&LoadFlag=1", "window1");
  } 
  else 
  {
      alert("保单号码有误!");
  }
}


function showPolDetail() 
{
  var cContNo = fm.ContNo.value;
  var cPrtNo = fm.PrtNo.value;
  if (cContNo!="" && PrtNo!="") 
  {
    window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+cPrtNo+"&ContNo="+cContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  } 
  else 
  {
      alert("保单号码有误!");
  }
}

function ScanQuery() 
{
  var prtNo = fm.PrtNo.value;
  if (prtNo != "")
  {
    window.open("../sys/LCProposalScanQuery.jsp?prtNo="+prtNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  }
  else 
  {
      alert("保单号码有误!");
  }  
}

//既往投保信息
function showApp(cindex) {
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cContNo=fm.ContNo.value;
  var cAppntNo = fm.Appntno.value;
  if (cindex == 1)
    window.open("../uw/UWAppMain.jsp?ContNo="+cContNo+"&CustomerNo="+cAppntNo+"&type=1");
  else
    window.open("../uw/UWAppFamilyMain.jsp?ContNo="+cContNo);

  showInfo.close();

}


