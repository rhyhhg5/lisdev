	/*********************************************************************
 *  程序名称：GrpReInsureInput.js
 *  程序功能：人工核保免得信息维护页面
 *  创建日期：2006-10-30 
 *  创建人  ：zhousp
 *  返回值：  无
 *  更新记录：  更新人    更新日期     更新原因/内容
 *********************************************************************
 */

var arrResult1 = new Array();
var arrResult2 = new Array();
var arrResult3 = new Array();
var arrResult4 = new Array();
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var temp = new Array();
var mOperate="";
var ImportPath;
var oldDataSet ; 
//alert(PrtNo);
window.onfocus=myonfocus;
/*********************************************************************
 *  查询免责信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QueryRiskInfo()
{
    var tGrpContNo = fm.GrpContNo.value;
    // alert(tGrpContNo);
    // mSQL ="select (select grpname from lcgrpcont where grpcontno=a.grpcontno),a.riskcode,a.Mult,a.Amnt,a.Prem,(select case (select state from lcreinsurtask where uwno=(select max(uwno) from lcreinsurtask where polno=a.grppolno) and polno=a.grppolno) when '00' then '待回复' when '01' then '已回复' when '02' then '办结' end from dual),a.grppolno from lcgrppol a where a.grpcontno ='" + tGrpContNo +"'";
    mSQL = " select GrpName, RiskCode, RiskName, Mult, max(Amnt), sum(Prem), State, GrpPolNo from ( "
        + "select (select grpname from lcgrpcont where grpcontno=a.grpcontno) grpname, "
        + " a.riskcode, "
        + " (select RiskName from LMRisk where RiskCode = a.RiskCode) RiskName, "
        + " (select max(occupationtype) from lcinsured where grpcontno='"+tGrpContNo+"') Mult, "
        + " a.Amnt, "
        + " a.Prem, "
        + " (select case (select state from lcreinsurtask where uwno=(select max(uwno) from lcreinsurtask where polno=a.grppolno) and polno=a.grppolno) when '00' then '待再保回复' when '01' then '已回复' when '02' then '办结' end from dual) state, "
        + " a.grppolno "
        + " from lcgrppol a "
        + " where a.grpcontno ='" + tGrpContNo +"' and a.grppolno = (select distinct grppolno from LCGUWError where grpcontno='"+tGrpContNo+"')"
        + " ) as temp "
        + " group by GrpName, RiskCode, RiskName, Mult, State, GrpPolNo ";
    turnPage1.queryModal(mSQL, RiskInfoGrid);
}

//查询再保审核任务 	
function QueryReInsureAudit()
{
  var mGrpContNo = fm.GrpContNo.value;	
  
  var sql = "select (select riskname from lmrisk where riskcode=b.riskcode),b.riskcode,a.uwno,a.uwoperator,a.uwidea,char(a.makedate) || ' ' || char(a.maketime), "
          + "  a.adjunctpath,(select case when a.adjunctpath is null then '无附件' else '有附件' end from dual)"
          + "from lcreinsuruwidea a ,lcgrppol b "
          + "where a.polno=b.grppolno "
          + "   and b.grpcontno = '" + mGrpContNo + "' "
          + "union "
          + "select (select riskname from lmrisk where riskcode=b.riskcode),b.riskcode,a.uwno,a.operator,a.uwidea,char(a.makedate) || ' ' || char(a.maketime), "
          + "   a.adjunctpath,(select case when a.adjunctpath is null then '无附件' else '有附件' end from dual) "
          + "from lcreinsuridea a , lbgrppol b "
          + "where a.polno = b.grppolno "
          + "   and b.grpcontno = '" + mGrpContNo + "' "
          + "order by uwno";	
  turnPage1.queryModal(sql,ReInsureAuditGrid);
}
function AutoReInsure()
{   
	  var ttGrpContNo = fm.GrpContNo.value;	
	  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fm.action = "./GrpAutoReInsureChk.jsp?GrpContNo="+ttGrpContNo;
    fm.submit();
} 	
function 	ReInsureAudit(){
  var tGrpContNo = fm.GrpContNo.value;
  mSQL = "select (select grpname from lcgrpcont where grpcontno=a.grpcontno),(select riskcode from lcgrppol where grppolno=a.grppolno),a.uwerror "
        + "from LCGUWError a "
        + "where a.grpcontno = '" + tGrpContNo +"'  "
        + "   and a.uwrulecode  in (select uwcode from lmuw where passflag='R')";
	turnPage.queryModal(mSQL, ReInsureGrid);
}

//响应发送再保审核按钮事件
function SendUWReInsure()
{
  var row = RiskInfoGrid.getSelNo();
  if(row == null || row == 0)
  {
    alert("请选择险种");
    return false;
  }
  
  var	GrpPolNo = RiskInfoGrid.getRowColData(row - 1, 8);
	var tsql ="select state from lcreinsurtask where uwno=(select max(uwno) from lcreinsurtask where polno='" +GrpPolNo + "') and polno='" +GrpPolNo + "'";
	var arr=easyExecSql(tsql);	
	if(tsql != null && arr == '00')
	{
    alert("该任务为待回复状态不能再发送再保审核!");
    return false;
	}
	
  ReInsureUpload();
}
			
//上载附件
function ReInsureUpload() 
{
  var i = 0;

  var tImportFile = fm.all('FileName').value;

  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./UpLodeSave.jsp";
  fm.submit();
}

//下载
function DownLoad()
{
  var row = ReInsureAuditGrid.getSelNo();
  if(row == null || row == 0)
  {
    alert("请选择险种");
    return false;
  }
  
  var FilePath = ReInsureAuditGrid.getRowColData(row - 1, 7); 
  //alert("下载路径"+FilePath) ;
  if (FilePath=="")
  {
    alert("没有附件,不能进行下载操作！")	
    return false;
  }   
 // alert(FilePath);
  //var showStr="正在下载数据……";
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./DownLoadSave.jsp?FilePath="+FilePath;
  fm.submit();
}
//临分说明下载
function LFDownLoad(){
var ttsql ="select adjunctpath from LCReInsurUWIdea where PolNo='"+fm.GrpContNo.value+"' and uwno =(select max(uwno) from LCReInsurUWIdea where  PolNo='"+fm.GrpContNo.value+"')";
var arr=easyExecSql(ttsql);		
if(arr){
	var FilePath = arr[0][0]; 
	}
  if (FilePath=="")
  {
    alert("没有附件,不能进行下载操作！")	
    return false;
  }   
  fm.action = "./DownLoadSave.jsp?FilePath="+FilePath;
  fm.submit();	
	}
//确认
function ApplyChk()
{
	
 ReInsureUpload1();
}
//
function ReInsureUpload1()
{
  var tImportFile = fm.all('FileName1').value;
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./UpLodeSave1.jsp";	
	fm.submit();
	}
//办结确认
function BJChk(){
  var tGrpContNo = fm.GrpContNo.value;
  var tBJRemark = fm.BJRemark.value;
  var tLFStateNo = fm.LFStateNo.value;
  var showStr="正在保存数据";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./SendGrpBJReInsureChk.jsp?GrpContNo="+tGrpContNo+"&BJRemark="+tBJRemark+"&LFStateNo="+tLFStateNo;
  fm.submit();	
	}
//再保审核任务办结
function ReInsureOver()
{
  var row = RiskInfoGrid.getSelNo();
	if(row == 0)
	{
	  alert("请选择险种");
	  return false;
	}
	
  var	State = RiskInfoGrid.getRowColData(row - 1, 7);
  
//  if (State!='已回复')
//  {
//    alert("该险种处于"+State+"状态不能进行办结");
//    return false;
//  }
  
  if(!confirm("该险种处于"+State+"状态,是否办结结束所选险种的再保审核？"))
  {
    return false;
  }
  
  fm.GrpPolNo.value = RiskInfoGrid.getRowColDataByName(row - 1, "GrpPolNo");
  fm.action = "./ReInsureSave.jsp?GrpPolNo="+fm.GrpPolNo.value;
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
  fm.submit();
}
 		
function afterSubmit( FlagStr, content )
{ 

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initReInsureGrid();
    var tGrpContNo = fm.GrpContNo.value;
//    alert(tGrpContNo);
    ReInsureAudit();
    QueryRiskInfo();
    QueryReInsureAudit();   
  }
}


function UpLodeReInsure(FilePath,FileName){
//alert(FilePath);
	  for (i=0; i<RiskInfoGrid.mulLineCount; i++)
  {
  ImportFile = fm.all('FileName').value;
    if (RiskInfoGrid.getSelNo(i))
    {
      checkFlag = RiskInfoGrid.getSelNo();
      break;
    }
  }
  var	GrpPolNo = RiskInfoGrid.getRowColData(checkFlag - 1, 8);
  var tRemark = fm.Remark.value;  	
 fm.action = "./SendGrpUWReInsureChk.jsp?FilePath="+FilePath+"&FileName="+FileName+"&GrpPolNo="+GrpPolNo+"&Remark="+tRemark;
 fm.submit();
}

function UpLode(FilePath,FileName){
 var tGrpContNo = fm.GrpContNo.value;
 var tApplyRemark = fm.ApplyRemark.value;
 var tLFState1No = fm.LFState1No.value;
 fm.action = "./SendGrpLFReInsureChk.jsp?FilePath="+FilePath+"&FileName="+FileName+"&GrpContNo="+tGrpContNo+"&ApplyRemark="+tApplyRemark+"&LFState1No="+tLFState1No;
 fm.submit();
	
	}

//办结反馈提示
function afterSubmitFinish( FlagStr, content )
{ 

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    QueryRiskInfo();
  }
}

function afterCodeSelect( cCodeName, Field ) {

  //alert("uwstate" + cCodeName + Field.value);
  if( cCodeName == "LFState" ) {
      DoUWStateCodeSelect(Field.value);//loadFlag在页面出始化的时候声明
    }
}