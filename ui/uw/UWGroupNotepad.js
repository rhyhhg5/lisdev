//该文件中包含客户端需要处理的函数和事件
//程序名称：团单记事本
//程序功能：记录保单纪录各个环节的备注信息
//创建日期：2006-9-21 18:40
//创建人  ：Wulg
//更新记录：  更新人    更新日期     更新原因/内容
//               该文件中包含客户端需要处理的函数和事件

var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	if (trim(fm.Content.value) == "")
	{
    alert("必须填写记事本内容！");
    return;
  }
  if (fm.Content.value.length > 500)
  {
  	alert("记事本内容填写过长，请精简！"); 
    return;
  }
  fm.GrpContNo.value = GrpContNo;
  fm.PrtNo.value = PrtNo;
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  ChangeDecodeStr();
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {   
  UnChangeDecodeStr();
  showInfo.close(); 
 window.focus();       
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}         
         
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  if(!verifyInput2())
	  return false;
  var strSql = "select PrtNo, NotePadCont, Operator, MakeDate, MakeTime "
  						 + "from LCNotePad "
  						 + "where GrpContNo = '" + GrpContNo + "'"
  						 + getWherePart('operator', 'operator')
  						 + getWherePart('MakeDate', 'MakeDate');
   turnPage.queryModal(strSql, UWGroupNotepadGrid);
   fm.Content.value = ""; 
}           

function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
	var tSelNo = UWGroupNotepadGrid.getSelNo();
	var tData = UWGroupNotepadGrid.getRowColData(tSelNo-1,2);
	fm.Content.value = tData;
}          
