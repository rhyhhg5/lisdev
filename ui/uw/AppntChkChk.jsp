<%@page contentType="text/html;charset=GBK"%> 
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：AppntChkChk.jsp
//程序功能：投保人查重
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>  
<%
//1-得到所有纪录，显示纪录值
  int index=0;
  int TempCount=0;
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String flag = "0";
  String Content = "请选择保单";
  LCContSchema tLCContSchema = new LCContSchema();
  
  //得到radio列的数组
  System.out.println("---4----");  
  	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  
  String tProposalNo = request.getParameter("ProposalNo");
  String tFlag = request.getParameter("Flag");
  
  String tSel[] = request.getParameterValues("InpPolGridSel");
  String tCustomerNo[] = request.getParameterValues("PolGrid1");
  String tOldCustomerNo[] = request.getParameterValues("OPolGrid1");
  int count = tSel.length;
  System.out.println("count:"+count);
  
  
//保单表 
try
{
  if(tProposalNo!=null&&tFlag!=null)//如果不是空纪录	
  {
  		System.out.println("----3-----");
		System.out.println("polno="+tProposalNo);
		System.out.println("flag="+tFlag);
  		
  		if (count > 0)
  		{
  			for (int i = 0;i<count;i++)
  			{
  				System.out.println("sel:"+tSel[i]);
  				if(tSel[i].equals("1")&&!tCustomerNo[i].equals(""))
  				{
  					flag = "1";
  					tLCContSchema.setAppntNo(tCustomerNo[i]);
  					tLCContSchema.setInsuredNo(tOldCustomerNo[0]);
					tLCContSchema.setContNo(tProposalNo);
					tLCContSchema.setAppFlag(tFlag);
					VData tVData = new VData();
					tVData.add(tLCContSchema);
					tVData.add(tG);
					
					PersonChkSureUI tPersonChkSureUI = new PersonChkSureUI();
					if(tPersonChkSureUI.submitData(tVData,"INSERT") == false)
					{
						FlagStr = "Fail";
						Content = tPersonChkSureUI.mErrors.getError(0).errorMessage;
					}
					else
					{					
						FlagStr = "Succ";
						Content = "成功!";
					}
  				}
  			}
  			if(flag.equals("0"))
  			{
				FlagStr = "Fail";
				Content = "请选择一条信息!";  			
  			}
  		}	
		else
		{
			FlagStr = "Fail";
			Content = "请选择一条信息!";
		}
		
		//查询并显示单条信息    							
  }
  else
  {
  	FlagStr = "Fail";
  	Content = "保单号传输失败!";
  }
  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";	
}  
%>  
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
