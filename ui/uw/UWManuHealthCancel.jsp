<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWManuHealthCancel.jsp
//程序功能：删除体检件
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.workflowengine.*"%>
<%
//输出参数
CErrors tError = null;
CErrors cError = null;
String FlagStr = "";
String Content = "";
String mOperate="DELETE||MAIN";

GlobalInput tGlobalInput = new GlobalInput();
tGlobalInput=(GlobalInput)session.getValue("GI");
UWHealthCancelUI tUWHealthCancelUI   = new UWHealthCancelUI();
if(tGlobalInput == null) {
  out.println("session has expired");
  return;
}


// 投保单列表
LCPENoticeSchema tLCPENoticeSchema = new LCPENoticeSchema();
LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();


String tPrtseq = request.getParameter("strPrtseq");
tLOPRTManagerSchema.setPrtSeq(tPrtseq);

try{
  // 准备传输数据 VData
  VData tVData = new VData();
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("Prtseq",tPrtseq);

  tVData.add(tGlobalInput);
  tVData.add(tTransferData);
  tVData.add(tLOPRTManagerSchema);
  tVData.add(tLCPENoticeSchema);
  // 数据传输

  tUWHealthCancelUI.submitData(tVData,"DELETE");
}
catch(Exception ex)
{
  Content = "操作失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
  tError = tUWHealthCancelUI.mErrors;
  if (!tError.needDealError())
  {

    Content = " 操作成功! ";
    FlagStr = "Success";
  }
  else
  {
    Content = " 操作失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}
System.out.println("%%%%%%%%%%%%%%"+Content);
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
