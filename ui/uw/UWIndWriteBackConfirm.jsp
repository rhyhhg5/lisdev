<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWIndWriteBackConfirm.jsp
//程序功能：
//创建日期：2006-6-8 16:21
//创建人  ：Yangming
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
<%
		String FlagStr="";
		String Content = "";
		CErrors tError = null;
		GlobalInput tG = (GlobalInput)session.getValue("GI");
		TransferData mTransferData = new TransferData();
		mTransferData.setNameAndValue("ProposalContNo", request.getParameter("ProposalContNo"));
		mTransferData.setNameAndValue("MissionID", request.getParameter("MissionID"));
		mTransferData.setNameAndValue("SubMissionID", request.getParameter("SubMissionID"));
		VData tVData	= new VData();
    tVData.add(mTransferData);
    tVData.add(tG);
    System.out.println("-------------------start workflow---------------------");
    TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
	  if( !tTbWorkFlowUI.submitData( tVData, "0000001200" ) ) {
	      Content = " 处理失败，原因是: " + tTbWorkFlowUI.mErrors.getError(0).errorMessage;
	      FlagStr = "Fail";
	  }else {
	      Content = " 处理成功！";
	      FlagStr = "Succ";
	  }
 System.out.println("-------------------end workflow---------------------");
%>
<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>