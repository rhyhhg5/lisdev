<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalBnfInfoModifyChk.jsp
//程序功能：新契约被保险人信息修改
//创建日期：2002-06-19 11:10:36
//创建人  ：zhangxing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
 String FlagStr = "Fail";
 String Content = "";
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");  
 if(tG == null) {
		out.println("session has expired");
		return;
	}	
	
	LCBnfSet tLCBnfSet = new LCBnfSet();

  	//接收信息
  	
  TransferData tTransferData = new TransferData();
  
	String tSerialNo[]          = request.getParameterValues("BnfGridNo");
	String tName[]              = request.getParameterValues("BnfGrid3");
	String tIDType[]            = request.getParameterValues("BnfGrid4");
	String tIDNo[]              = request.getParameterValues("BnfGrid5");
  String tRelationToInsured[] = request.getParameterValues("BnfGrid6");
	String tBnfLot[]            = request.getParameterValues("BnfGrid7");
	String tBnfGrade[]          = request.getParameterValues("BnfGrid8");	
	String tBnfType[]           = request.getParameterValues("BnfGrid9");
	String tPolNo[]           = request.getParameterValues("BnfGrid10");
  String tContNo              = request.getParameter("ContNo");
	String tAppntNo             = request.getParameter("AppntNo");
	String tInsuredNo           = request.getParameter("InsuredNo");
	
	int ChkCount = 0;
	boolean flag = true;
	if(tSerialNo != null)
	{		
		ChkCount = tSerialNo.length;
	}
	
	  if (ChkCount > 0)
	  {
	    		for (int i = 0; i < ChkCount; i++)
		    	{
				       if (!tName[i].equals(""))
				       {		  		
				            LCBnfSchema tLCBnfSchema = new LCBnfSchema();	  
				            tLCBnfSchema.setPolNo(tPolNo[i]);      
		  			        tLCBnfSchema.setName(tName[i]);
		  			        System.out.println("tName[i]:"+tName[i]);
					          tLCBnfSchema.setIDType( tIDType[i]);
					          tLCBnfSchema.setIDNo( tIDNo[i]);
					          tLCBnfSchema.setRelationToInsured(tRelationToInsured[i]);	
					          tLCBnfSchema.setBnfLot(tBnfLot[i]);
					          tLCBnfSchema.setBnfGrade(tBnfGrade[i]);   	    
					          tLCBnfSchema.setBnfType(tBnfType[i]);
			    	        tLCBnfSet.add( tLCBnfSchema );			             
			   		        flag = true;
				      }
		   	 }
		   	 System.out.println("&&&&&&&&&&&limain");
		 }
		
		   	 System.out.println("tLCBnfSet:"+tLCBnfSet.size());
		   	 tTransferData.setNameAndValue("ContNo", tContNo);
         tTransferData.setNameAndValue("AppntNo", tAppntNo);
    		 tTransferData.setNameAndValue("InsuredNo", tInsuredNo);
    		 tTransferData.setNameAndValue("LCBnfSet", tLCBnfSet); 	
	       System.out.println("flag:"+flag);

try
{           
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData);
		tVData.add( tG );
		
		// 数据传输
		EasyEdorTbUI tEasyEdorTbUI   = new EasyEdorTbUI();	
		
		if (!tEasyEdorTbUI.submitData(tVData,BQ.EASYEDORTYPE_BNF))
		{
		//	int n = tEasyEdorTbUI.getErrorCount();
			Content = " 新契约修改受益人失败，原因是: " + tEasyEdorTbUI.getErrors().getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tEasyEdorTbUI.getErrors();
		    if (!tError.needDealError())
		    {                          
		    	Content = " 新契约修改受益人成功,请确认是否重打合同! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 新契约修改受益人失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		     }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
