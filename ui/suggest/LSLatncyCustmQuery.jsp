<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<title>客户信息查询</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<script src="./LSLatncyCustmQuery.js"></script> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LSLatncyCustmQueryInit.jsp"%>

</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm target=fraSubmit method=post>
    <table  class= common align=center>
      	<TR  class= common>
          
      <TD  class= title> 潜在客户号码： </TD>
          <TD  class= input>
            <Input class= common name=CustomerNo >
          </TD>
          <TD  class= title>
          姓名：
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
       </TR>             
      	<TR  class= common>
          <TD  class= title>
          出生日期：
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Birthday >            
          </TD>
          <TD  class= title>
          证件类型：
          </TD>
          <TD  class= input>
            <Input class="code" name=IDType ondblclick="return showCodeList('IDType',[this]);">            
          </TD>
       </TR>   
      	<TR  class= common>
          <TD  class= title>
          证件号码：
          </TD>
          <TD  class= input>
            <Input class= common name=IDNo >
          </TD>
       </TR>   
   </Table>  
      <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="返回" TYPE=button onclick="returnParent();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLSLatncyCustm1);">
    		</TD>
    		<TD class= titleImg>
    			 客户信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divLSLatncyCustm1" style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanPersonGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 
 </Div>					

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>
