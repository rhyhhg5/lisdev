<%
//程序名称：LSModelInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：wuwei程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单信息部分
    fm.all('SugModelCode').value = '';
    fm.all('SugModelName').value = '';
    fm.all('SugPrvtPrpty').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
  }
  catch(ex)
  {
    alert("在LSModelInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}


function initForm()
{
	var loadFlag = "0";
	try
	{
		if( top.opener.mShowPolDetail == "APPROVE" ) loadFlag = "1";
	}
	catch(ex1){}
	try
	{
		initInpBox();
		initItemGrid();
		
		// 初始化时查询明细
		if( loadFlag == "1" )
		{
			fm.all( 'SugModelCode' ).value = <%=tSugModelCode%>;
			queryModelDetail();
		}	
	}
	catch(re)
	{
		alert(re);
	}
}


// item信息列表的初始化
function initItemGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="项目编码";    	     	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=30;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="项目名称";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="属性";	         		//列名
      iArray[3][1]="160px";            		//列宽
      iArray[3][2]=80;            			//列最大值
      iArray[3][3]=0 ;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="代理人代码";         		//列名
      iArray[4][1]="140px";            		//列宽
      iArray[4][2]=80;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      ItemGrid = new MulLineEnter( "fm" , "ItemGrid" ); 
      //这些属性必须在loadMulLine前
      ItemGrid.mulLineCount = 2;   
      ItemGrid.displayTitle = 1;
      ItemGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //ItemGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>