<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：SugpolSave.jsp
//程序功能：
//创建日期：2002-10-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.suggest.*"%>

<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	String tAction = "";
	String tOperate = "";
	SugpolUI tSugpolUI   = new SugpolUI();
	
	GlobalInput tG = new GlobalInput();
	
	tG.Operator = "Admin";
	tG.ComCode  = "001";
	tG.ManageCom = "00";
	session.putValue("GI",tG);
	tG = ( GlobalInput )session.getValue( "GI" );

	VData tVData = new VData();
	tAction = request.getParameter( "fmAction" );
//System.out.println("tAction:"+tAction);
	if( tAction.equals( "DELETE" ))
	{
		LSPolSchema tLSPolSchema   = new LSPolSchema();
	    tLSPolSchema.setPolNo( request.getParameter( "PolNo" ));

		// 准备传输数据 VData
		tVData.addElement( tLSPolSchema );
		tVData.addElement( tG );
	}
	else
	{
		//接收信息
		// 保单信息部分
		LSPolSchema tLSPolSchema   = new LSPolSchema();
		LSDutySchema tLSDutySchema = new LSDutySchema();
	  
	    tLSPolSchema.setPolNo(request.getParameter("PolNo"));
	    tLSPolSchema.setPrtNo(request.getParameter("PrtNo"));
	    tLSPolSchema.setManageCom(request.getParameter("ManageCom"));
	    tLSPolSchema.setSaleChnl(request.getParameter("SaleChnl"));
	    tLSPolSchema.setAgentCom(request.getParameter("AgentCom"));
	    tLSPolSchema.setAgentCode(request.getParameter("AgentCode"));
	    tLSPolSchema.setAgentGroup(request.getParameter("AgentGroup"));
	    tLSPolSchema.setHandler(request.getParameter("Handler"));
	    tLSPolSchema.setAgentCode1(request.getParameter("AgentCode1"));

	  	// 险种信息部分
	    tLSPolSchema.setRiskCode(request.getParameter("RiskCode"));
	    tLSPolSchema.setRiskVersion(request.getParameter("RiskVersion"));
	    tLSPolSchema.setCValiDate(request.getParameter("CValiDate"));
	    tLSPolSchema.setMult(request.getParameter("Mult"));
	    tLSPolSchema.setPrem(request.getParameter("Prem"));
	    tLSPolSchema.setAmnt(request.getParameter("Amnt"));
	    tLSDutySchema.setPayEndYear(request.getParameter("PayEndYear"));
	    tLSDutySchema.setPayEndYearFlag(request.getParameter("PayEndYearFlag"));
	    tLSDutySchema.setGetYear(request.getParameter("GetYear"));
	    tLSDutySchema.setGetYearFlag(request.getParameter("GetYearFlag"));
	    tLSDutySchema.setGetStartType(request.getParameter("GetStartType"));
	    tLSDutySchema.setInsuYear(request.getParameter("InsuYear"));
	    tLSDutySchema.setInsuYearFlag(request.getParameter("InsuYearFlag"));
System.out.println("end 险种信息...");
	
	    // 投保人信息部分
	    LSAppntIndSchema tLSAppntIndSchema   = new LSAppntIndSchema();
	
	    tLSAppntIndSchema.setCustomerNo(request.getParameter("AppntNo"));
	    tLSAppntIndSchema.setName(request.getParameter("AppntName"));
	    tLSAppntIndSchema.setSex(request.getParameter("AppntSex"));
	    tLSAppntIndSchema.setBirthday(request.getParameter("AppntBirthday"));
	    tLSAppntIndSchema.setIDType(request.getParameter("AppntIDType"));
	    tLSAppntIndSchema.setIDNo(request.getParameter("AppntIDNo"));
	    tLSAppntIndSchema.setRelationToInsured(request.getParameter("RelationToInsured"));
	    tLSAppntIndSchema.setPhone(request.getParameter("AppntPhone"));
	    tLSAppntIndSchema.setMobile(request.getParameter("AppntMobile"));
	    tLSAppntIndSchema.setPostalAddress(request.getParameter("AppntPostalAddress"));
	    tLSAppntIndSchema.setZipCode(request.getParameter("AppntZipCode"));
	    tLSAppntIndSchema.setEMail(request.getParameter("AppntEMail"));
System.out.println("end 投保人信息...");
	
		// 被保人信息
		LSInsuredSet tLSInsuredSet   = new LSInsuredSet();
		
		// 主被保人
		LSInsuredSchema tLSInsuredSchema   = new LSInsuredSchema();
	    tLSInsuredSchema.setInsuredGrade("M");
	
	    tLSInsuredSchema.setCustomerNo(request.getParameter("CustomerNo"));
	    tLSInsuredSchema.setName(request.getParameter("Name"));
	    tLSInsuredSchema.setSex(request.getParameter("Sex"));
	    tLSInsuredSchema.setBirthday(request.getParameter("Birthday"));
	    tLSInsuredSchema.setIDType(request.getParameter("IDType"));
	    tLSInsuredSchema.setIDNo(request.getParameter("IDNo"));
	    tLSInsuredSchema.setHealth(request.getParameter("Health"));
	    tLSInsuredSchema.setOccupationType(request.getParameter("OccupationType"));
	    tLSInsuredSchema.setMarriage(request.getParameter("Marriage"));
	
	    tLSInsuredSet.add(tLSInsuredSchema);
System.out.println("end 被保人信息...");
	    
		// 连带被保险人
		String tInsuredNum[] = request.getParameterValues("SubInsuredGridNo");
		String tInsuredCustomerNo[] = request.getParameterValues("SubInsuredGrid1");
		String tInsuredName[] = request.getParameterValues("SubInsuredGrid2");
		String tInsuredSex[] = request.getParameterValues("SubInsuredGrid3");
		String tInsuredBirthday[] = request.getParameterValues("SubInsuredGrid4");
		String tRelationToInsured[] = request.getParameterValues("SubInsuredGrid5");
		
		int InsuredCount = tInsuredNum.length;
		for (int i = 0; i < InsuredCount; i++)
		{
	  		if(tInsuredCustomerNo[i]==null || tInsuredCustomerNo[i].equals("")) break;
	  		
	  		tLSInsuredSchema   = new LSInsuredSchema();
		    tLSInsuredSchema.setInsuredGrade("S");
	
		    tLSInsuredSchema.setCustomerNo(tInsuredCustomerNo[i]);
		    tLSInsuredSchema.setName(tInsuredName[i]);
		    tLSInsuredSchema.setSex(tInsuredSex[i]);
		    tLSInsuredSchema.setBirthday(tInsuredBirthday[i]);
		    tLSInsuredSchema.setRelationToInsured(tRelationToInsured[i]);
		    
		    tLSInsuredSet.add(tLSInsuredSchema);
		}
	
		//是否有责任项
		String tNeedDutyGrid=request.getParameter("inpNeedDutyGrid");
		LSDutySet tLSDutySet=new LSDutySet();
		LSDutySchema tLSDutySchema1=new LSDutySchema();
		if (tNeedDutyGrid.equals("1"))
		{
			tLSDutySchema1 = new LSDutySchema();
			tLSDutySet=new LSDutySet();
			String tDutyCode[] =request.getParameterValues("DutyGrid1");
			String tDutyPrem1[] =request.getParameterValues("DutyGrid3");
			String tDutyGet1[] =request.getParameterValues("DutyGrid4");
			String tDutyPayEndYear[] =request.getParameterValues("DutyGrid5");
			String tDutyGetYear[] =request.getParameterValues("DutyGrid6");
			String tDutyChk[] = request.getParameterValues("InpDutyGridChk");
			int DutyCount=tDutyCode.length;
			for (int i = 0; i < DutyCount; i++)
			{
				if(tDutyCode[i]==null || tDutyCode[i].equals("")) break;
				
				tLSDutySchema1= new LSDutySchema();
				if(!tDutyCode[i].equals("") && tDutyChk[i].equals("1"))
				{
					tLSDutySchema1.setDutyCode(tDutyCode[i]);
					tLSDutySchema1.setPrem(tDutyPrem1[i]);
					tLSDutySchema1.setAmnt(tDutyGet1[i]);
					tLSDutySchema1.setPayEndYear(tDutyPayEndYear[i]);
					tLSDutySchema1.setGetYear(tDutyGetYear[i]);
					tLSDutySet.add(tLSDutySchema1);
				} // end of if
			} // end of for
		} // end of if
System.out.println("end 责任信息...");
System.out.println("get data ready!");
  
		// 准备传输数据 VData
		tVData.addElement(tLSPolSchema);
		tVData.addElement(tLSAppntIndSchema);
		tVData.addElement(tLSInsuredSet);
		tVData.addElement(tG);
		if (tNeedDutyGrid.equals("1"))
			tVData.addElement(tLSDutySet);
		else
			tVData.addElement(tLSDutySchema);
	} // end of if

	if( tAction.equals( "INSERT" )) tOperate = "INSERT||SUGGEST";
	if( tAction.equals( "UPDATE" )) tOperate = "UPDATE||SUGGEST";
	if( tAction.equals( "DELETE" )) tOperate = "DELETE||SUGGEST";
	
	System.out.println("wh tOperate = "+tOperate);
	// 数据传输
	if( !tSugpolUI.submitData( tVData, tOperate ))
	{
		Content = " 保存失败，原因是: " + tSugpolUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "")
	{
		tError = tSugpolUI.mErrors;
		if (!tError.needDealError())
		{                          
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}
		else                                                                           
		{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

System.out.println(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

