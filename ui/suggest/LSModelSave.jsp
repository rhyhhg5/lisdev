<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LSModelSave.jsp
//程序功能：
//创建日期：2002-10-9 11:10:36
//创建人  ：wuwei 程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.suggest.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	String tAction = "";
	String tOperate = "";
	ModelUI tModelUI   = new ModelUI();
	
	GlobalInput tG = new GlobalInput();
	
	tG.Operator = "Admin";
	tG.ComCode  = "001";
	session.putValue("GI",tG);
	tG = ( GlobalInput )session.getValue( "GI" );



	VData tVData = new VData();
	tAction = request.getParameter( "fmAction" );
System.out.println("tAction:"+tAction);
	if( tAction.equals( "DELETE" ))
	{
		LSModelSchema tLSModelSchema   = new LSModelSchema();
	    tLSModelSchema.setSugModelCode( request.getParameter( "SugModelCode" ));

		// 准备传输数据 VData
		tVData.addElement( tLSModelSchema );
		tVData.addElement( tG );
	}
	else
	{
		//接收信息
		
		LSModelSchema tLSModelSchema   = new LSModelSchema();
		
	  	// 主表信息部分
	    tLSModelSchema.setSugModelCode(request.getParameter("SugModelCode"));
	    tLSModelSchema.setSugModelName(request.getParameter("SugModeName"));
	    tLSModelSchema.setSugPrvtPrpty(request.getParameter("SugPrvtPrpty"));
	    tLSModelSchema.setAgentCode(request.getParameter("AgentCode"));
	    
	  		
		// 连接表信息
		LSModelLinkSet	tLSModelLinkSet	=	new LSModelLinkSet();
		
		//String tSugModelCode[] = request.getParameterValues("ItemGrid?");
		String tSugItemCode[] = request.getParameterValues("ItemGrid1");
		
		int tLinkCount = tSugItemCode.length;
		LSModelLinkSchema tLSModelLinkSchema;
		for (int i = 0; i < tLinkCount; i++)
		{
	  		if(tSugItemCode[i]==null || tSugItemCode[i].equals("")) break;
	
	  		tLSModelLinkSchema   = new LSModelLinkSchema();
	System.out.println("itemCode信息:"+tLSModelLinkSchema.getSugItemCode());
		    tLSModelLinkSchema.setSugModelCode(tLSModelSchema.getSugModelCode());
		    tLSModelLinkSchema.setSugItemCode(tSugItemCode[i]);
		    tLSModelLinkSet.add(tLSModelLinkSchema);
		}
System.out.println("end 连接表信息...");
	
		
		// 准备传输数据 VData
		tVData.addElement(tLSModelSchema);
		tVData.addElement(tLSModelLinkSet);
		tVData.addElement(tG);
		
	} // end of if

	if( tAction.equals( "INSERT" )) tOperate = "INSERT||LSModel";
	if( tAction.equals( "UPDATE" )) tOperate = "UPDATE||LSModel";
	if( tAction.equals( "DELETE" )) tOperate = "DELETE||LSModel";
	
	// 数据传输
	if( !tModelUI.submitData( tVData, tOperate ))
	{
		Content = " 保存失败，原因是: " + tModelUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "")
	{
		tError = tModelUI.mErrors;
		if (!tError.needDealError())
		{                          
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}
		else                                                                           
		{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

System.out.println(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

