<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ProposalQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  // 保单信息部分
  LSPolSchema tLSPolSchema   = new LSPolSchema();

    tLSPolSchema.setPolNo(request.getParameter("PolNo"));

  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLSPolSchema);

  // 数据传输
  SugpolQueryUI tSugpolQueryUI   = new SugpolQueryUI();
	if (!tSugpolQueryUI.submitData(tVData,"QUERY||DETAIL"))
	{
      Content = " 查询失败，原因是: " + tSugpolQueryUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tSugpolQueryUI.getResult();

		// 显示
		// 保单信息
		LSPolSchema mLSPolSchema = new LSPolSchema(); 
		mLSPolSchema.setSchema((LSPolSchema)tVData.getObjectByObjectName("LSPolSchema",0));
		%>
    	<script language="javascript">
    	 	parent.fraInterface.fm.all("PrtNo").value = "<%=mLSPolSchema.getPrtNo()%>";
    	 	parent.fraInterface.fm.all("PolNo").value = "<%=mLSPolSchema.getPolNo()%>";
    	 	parent.fraInterface.fm.all("ManageCom").value = "<%=mLSPolSchema.getManageCom()%>";
    	 	parent.fraInterface.fm.all("SaleChnl").value = "<%=mLSPolSchema.getSaleChnl()%>";
    	 	parent.fraInterface.fm.all("AgentCom").value = "<%=mLSPolSchema.getAgentCom()%>";
    	 	parent.fraInterface.fm.all("AgentCode").value = "<%=mLSPolSchema.getAgentCode()%>";
    	 	parent.fraInterface.fm.all("AgentGroup").value = "<%=mLSPolSchema.getAgentGroup()%>";
    	 	parent.fraInterface.fm.all("Handler").value = "<%=mLSPolSchema.getHandler()%>";
    	 	parent.fraInterface.fm.all("AgentCode1").value = "<%=mLSPolSchema.getAgentCode1()%>";
    	 	parent.fraInterface.fm.all("RiskCode").value = "<%=mLSPolSchema.getRiskCode()%>";
    	 	parent.fraInterface.fm.all("RiskVersion").value = "<%=mLSPolSchema.getRiskVersion()%>";
    	 	parent.fraInterface.fm.all("CValiDate").value = "<%=mLSPolSchema.getCValiDate()%>";
    	 	parent.fraInterface.fm.all("Mult").value = "<%=mLSPolSchema.getMult()%>";
    	 	parent.fraInterface.fm.all("Prem").value = "<%=mLSPolSchema.getPrem()%>";
    	 	parent.fraInterface.fm.all("Amnt").value = "<%=mLSPolSchema.getAmnt()%>";
    	</script>
		<%
		// 投保人信息
		LSAppntIndSchema mLSAppntIndSchema = new LSAppntIndSchema(); 
		mLSAppntIndSchema.setSchema((LSAppntIndSchema)tVData.getObjectByObjectName("LSAppntIndSchema",0));
		%>
    	<script language="javascript">
    	 	parent.fraInterface.fm.all("AppntCustomerNo").value = "<%=mLSAppntIndSchema.getCustomerNo()%>";
    	 	parent.fraInterface.fm.all("AppntName").value = "<%=mLSAppntIndSchema.getName()%>";
    	 	parent.fraInterface.fm.all("AppntSex").value = "<%=mLSAppntIndSchema.getSex()%>";
    	 	parent.fraInterface.fm.all("AppntBirthday").value = "<%=mLSAppntIndSchema.getBirthday()%>";
    	 	parent.fraInterface.fm.all("AppntIDType").value = "<%=mLSAppntIndSchema.getIDType()%>";
    	 	parent.fraInterface.fm.all("AppntIDNo").value = "<%=mLSAppntIndSchema.getIDNo()%>";
    	 	parent.fraInterface.fm.all("RelationToInsured").value = "<%=mLSAppntIndSchema.getRelationToInsured()%>";
    	 	parent.fraInterface.fm.all("AppntPhone").value = "<%=mLSAppntIndSchema.getPhone()%>";
    	 	parent.fraInterface.fm.all("AppntMobile").value = "<%=mLSAppntIndSchema.getMobile()%>";
    	 	parent.fraInterface.fm.all("AppntPostalAddress").value = "<%=mLSAppntIndSchema.getPostalAddress()%>";
    	 	parent.fraInterface.fm.all("AppntZipCode").value = "<%=mLSAppntIndSchema.getZipCode()%>";
    	 	parent.fraInterface.fm.all("AppntEMail").value = "<%=mLSAppntIndSchema.getEMail()%>";
    	</script>
		<%
		// 被保人信息
		LSInsuredSet mLSInsuredSet = new LSInsuredSet(); 
		mLSInsuredSet.set((LSInsuredSet)tVData.getObjectByObjectName("LSInsuredSet",0));
		int insuredCount = mLSInsuredSet.size();
		for (int i = 1; i <= insuredCount; i++)
		{
			LSInsuredSchema mLSInsuredSchema = mLSInsuredSet.get(i);
			if (mLSInsuredSchema.getInsuredGrade().equals("M"))
			{
			%>	
	    	<script language="javascript">
	    	 	parent.fraInterface.fm.all("CustomerNo").value = "<%=mLSInsuredSchema.getCustomerNo()%>";
	    	 	parent.fraInterface.fm.all("Name").value = "<%=mLSInsuredSchema.getName()%>";
	    	 	parent.fraInterface.fm.all("Sex").value = "<%=mLSInsuredSchema.getSex()%>";
	    	 	parent.fraInterface.fm.all("Birthday").value = "<%=mLSInsuredSchema.getBirthday()%>";
	    	 	parent.fraInterface.fm.all("IDType").value = "<%=mLSInsuredSchema.getIDType()%>";
	    	 	parent.fraInterface.fm.all("IDNo").value = "<%=mLSInsuredSchema.getIDNo()%>";
	    	 	parent.fraInterface.fm.all("Health").value = "<%=mLSInsuredSchema.getHealth()%>";
	    	 	parent.fraInterface.fm.all("OccupationType").value = "<%=mLSInsuredSchema.getOccupationType()%>";
	    	 	parent.fraInterface.fm.all("Marriage").value = "<%=mLSInsuredSchema.getMarriage()%>";
	    	</script>
	    	<%
	    		break;
	    	}
	    	break;
	    }
		
		// 连带被保人信息
		LSInsuredSet mLSSubInsuredSet = new LSInsuredSet(); 
		mLSSubInsuredSet.set((LSInsuredSet)tVData.getObjectByObjectName("LSInsuredSet",0));
		for (int i = 1; i <= insuredCount; i++)
		{
			LSInsuredSchema mLSSubInsuredSchema = mLSInsuredSet.get(i);
			int j = 0;
			if (mLSSubInsuredSchema.getInsuredGrade().equals("S"))
			{
			%>	
	    	<script language="javascript">
		   		parent.fraInterface.fm.SubInsuredGrid1[<%=j%>].value="<%=mLSSubInsuredSchema.getCustomerNo()%>";
		   		parent.fraInterface.fm.SubInsuredGrid2[<%=j%>].value="<%=mLSSubInsuredSchema.getName()%>";
		   		parent.fraInterface.fm.SubInsuredGrid3[<%=j%>].value="<%=mLSSubInsuredSchema.getSex()%>";
		   		parent.fraInterface.fm.SubInsuredGrid4[<%=j%>].value="<%=mLSSubInsuredSchema.getBirthday()%>";
		   		parent.fraInterface.fm.SubInsuredGrid5[<%=j%>].value="<%=mLSSubInsuredSchema.getRelationToInsured()%>";
	    	</script>
	    	<%
				j++;
	    	}
	    }
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tSugpolQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
//  out.println("<script language=javascript>");
//  out.println("showInfo.close();");
//  out.println("</script>");
%>
