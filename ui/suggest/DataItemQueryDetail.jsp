<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：DataItemQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：吴炜
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.suggest.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  // 数据信息部分
  	LSDataItemSchema tLSDataItemSchema   = new LSDataItemSchema();

    tLSDataItemSchema.setSugDataItemCode(request.getParameter("SugDataItemCode"));

  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.addElement(tLSDataItemSchema);
	
  // 数据传输
  	DataItemQueryUI tDataItemQueryUI   = new DataItemQueryUI();
  	if (!tDataItemQueryUI.submitData(tVData,"QUERY||DETAIL"))
	{
      Content = " 查询失败，原因是: " + tDataItemQueryUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tDataItemQueryUI.getResult();

		// 显示
		// 数据信息
		LSDataItemSchema mLSDataItemSchema = new LSDataItemSchema(); 
		mLSDataItemSchema.setSchema((LSDataItemSchema)tVData.getObjectByObjectName("LSDataItemSchema",0));
		%>
    	<script language="javascript">
    	 	parent.fraInterface.fm.all("SugDataItemCode").value = "<%=mLSDataItemSchema.getSugDataItemCode()%>";
    	 	parent.fraInterface.fm.all("SugDataItemName").value = "<%=mLSDataItemSchema.getSugDataItemName()%>";
    	 	parent.fraInterface.fm.all("SugPrvtPrpty").value = "<%=mLSDataItemSchema.getSugPrvtPrpty()%>";
    	 	parent.fraInterface.fm.all("AgentCode").value = "<%=mLSDataItemSchema.getAgentCode()%>";
    	 	parent.fraInterface.fm.all("DataLinkPrpty").value = "<%=mLSDataItemSchema.getDataLinkPrpty()%>";
    	 	parent.fraInterface.fm.all("dispFilePath").value = "<%=mLSDataItemSchema.getFilePath()%>";
    	</script>
		<%
  } // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tDataItemQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
//  out.println("<script language=javascript>");
//  out.println("showInfo.close();");
//  out.println("</script>");
%>
