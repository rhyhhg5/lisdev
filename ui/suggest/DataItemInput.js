//               该文件中包含客户端需要处理的函数和事件
//程序名称：DataItemInput.js
//程序功能：
//创建日期：2002-09-20 11:10:36
//创建人  ：吴炜
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var spanObj;
var mDebug = "1";
var mOperate = 0;
var mAction = "";
var arrResult = new Array();
window.onfocus = myonfocus;

/*********************************************************************
 *  保存数据项目信息的提交
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{

	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	mAction=fm.all('fmAction').value
	if( mAction == "INSERT" )
	{
		//showSubmitFrame(mDebug);
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		mAction = "INSERT";
		fm.all( 'fmAction' ).value = mAction;
		alert(fm.all( 'fmAction' ).value);
		fm.submit(); //提交
	}
}


/*********************************************************************
 *  保存个人投保单的提交后的操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		content = "保存成功！";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		showDiv(operateButton,"true");
		showDiv(inputButton,"false");
	}
	mAction = "";
}

/*********************************************************************
 *  "重置"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在DataItemInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

/*********************************************************************
 *  "取消"按钮对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelForm()
{
	mAction = "";
	fm.all( 'fmAction' ).value = mAction;
	showDiv(operateButton,"true");
    	showDiv(inputButton,"false");
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  Click事件，当点击增加图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addClick()
{
	//下面增加相应的代码
	fm.all('fmAction').value = "INSERT";
        if (fm.all('SugDataItemCode').value != null && fm.all('SugDataItemCode').value!="")
        {
          initForm();
        }
	showDiv( operateButton, "false" );
	showDiv( inputButton, "true" );
}

/*********************************************************************
 *  Click事件，当点击“查询”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryClick()
{
	if( mOperate == 0 )
	{
		mOperate = 1;
		showInfo = window.open("./DataItemQuery.html");
	}
}

/*********************************************************************
 *  Click事件，当点击“修改”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function updateClick()
{
	var tSugDataItemCode = "";
	tSugDataItemCode = fm.all( 'SugDataItemCode' ).value;
	if( tSugDataItemCode == null || tSugDataItemCode == "" )
		alert( "请先做投保单查询操作，再进行修改!" );
	else
	{
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			mAction = "UPDATE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}

/*********************************************************************
 *  Click事件，当点击“删除”图片时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteClick()
{
	var tSugDataItemCode = "";
	tSugDataItemCode = fm.all( 'SugDataItemCode' ).value;
	if( tSugDataItemCode == null || tSugDataItemCode == "" )
		alert( "请先做投保单查询操作，再进行删除!" );
	else
	{
		var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

		if( mAction == "" )
		{
			//showSubmitFrame(mDebug);
			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			mAction = "DELETE";
			fm.all( 'fmAction' ).value = mAction;
			fm.submit(); //提交
		}
	}
}



/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		if( mOperate == 1 )			// 查询保单明细
		{
			fm.all( 'SugDataItemCode' ).value 	= arrResult[0][0];
			fm.all( 'SugDataItemName' ).value 	= arrResult[0][1];
			fm.all( 'SugPrvtPrpty' ).value 		= arrResult[0][2];
			fm.all( 'AgentCode' ).value 		= arrResult[0][3];
			fm.all( 'DataLinkPrpty' ).value 	= arrResult[0][4];
			//com.sinosoft.utility.StrTool tStrTool = new com.sinosoft.utility.StrTool();
			fm.all( 'dispFilePath' ).value 		= arrResult[0][5];
			// 查询保单明细(已经返回明晰，不能查询blob类型）
			//queryDataItemDetail();
		}

	}
	mOperate = 0;		// 恢复初态
}

/*********************************************************************
 *  根据查询返回的信息查询投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryDataItemDetail()
{
	var cSugDataItemCode;
	cSugDataItemCode = fm.all( 'SugDataItemCode' ).value;

	//showSubmitFrame(mDebug);
//	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

//	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	parent.fraSubmit.window.location = "./DataItemQueryDetail.jsp?PolNo=" + cSugDataItemCode;
}

/*********************************************************************
 *  显示div
 *  参数  ：  第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
	if( cShow == "true" )
		cDiv.style.display = "";
	else
		cDiv.style.display = "none";
}

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

/********************************************************************
*  当点击显示数据时的处理
*  参数：无
*  返回：无
*********************************************************************
*/
function showBlob()
{
	var tSugDataItemCode;
	var mAction;
	tSugDataItemCode = fm.all( 'SugDataItemCode' ).value;
	var tdispFilePath = fm.all( 'dispFilePath' ).value;
	mAction = fm.all('fmAction').value;

	if ( tSugDataItemCode == null || tSugDataItemCode == "" || mAction == "INSERT" )
		alert ("请先做投保单查询操作，再进行删除!" );
	else
	{
		var urlStr = "./DataItemGetBlob.jsp?SugDataItemCode=" + tSugDataItemCode + "&FilePath=" + tdispFilePath;
		//showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.all('ifshowBlob').src = urlStr;
	}

}

function showdispFilePath()
{
	fm.all('dispFilePath').value=fm.all('FilePath').value;
}

