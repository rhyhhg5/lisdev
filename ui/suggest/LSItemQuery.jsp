<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LSItemQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LSItemQueryInit.jsp"%>
  <title>项目信息查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 项目信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            项目编码
          </TD>
          <TD  class= input>
			<Input class= common name=SugItemCode>
          </TD>
          <TD  class= title>
            项目名称
          </TD>
          <TD  class= input>
            <Input class= common name=SugItemName>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            项目属性
          </TD>
          <TD  class= input>
              <Input class="code" name=SugPrvtPrpty ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('SugPrvtPrpty',[this]);">
          </TD>
          <TD  class= title>
            代理人代码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCode>
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <!----
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLsDataItem);">
    		</td>
    		<td class= titleImg>
    			 数据项信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLsDataItem" style= "display: ''">
      	    <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLsDataItemGrid" >
  					</span> 
  			  	</td>
  			</tr>
	    </table>
	      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
	      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
	      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
	      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
  	--->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
