<html> 
<%
//程序名称：CollectivityClientInput.jsp
//程序功能：
//创建日期：2002-08-07 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="LatncyGrpInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LatncyGrpInit.jsp"%>
</head>
<body  onload="initForm();" ><!--在Init.jsp中初始化-->
  <form action="./LatncyGrpSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏CollectivityClient1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCollectivityClient1);">
      </td>
      <td class= titleImg>
        潜在客户信息
      </td>
    	</tr>
    </table>
    <Div  id= "divCollectivityClient1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            单位号码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpNo >
          </TD>
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class= common name=GrpName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位地址编码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpAddressCode >
          </TD>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input>
            <Input class= common name=GrpAddress >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位邮编
          </TD>
          <TD  class= input>
            <Input class= common name=GrpZipCode >
          </TD>
          <TD  class= title>
            单位电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位传真
          </TD>
          <TD  class= input>
            <Input class= common name=Fax >
          </TD>
          <TD  class= title>
            负责人
          </TD>
          <TD  class= input>
            <Input class= common name=Satrap >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            法人
          </TD>
          <TD  class= input>
            <Input class= common name=Corporation >
          </TD>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系人
          </TD>
          <TD  class= input>
            <Input class= common name=LinkMan >
          </TD>
          <TD  class= title>
            总人数
          </TD>
          <TD  class= input>
            <Input class= common name=Peoples >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            银行编码
          </TD>
          <TD  class= input>
            <Input class= common name=BankCode >
          </TD>
          <TD  class= title>
            银行帐号
          </TD>
          <TD  class= input>
            <Input class= common name=BankAccNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            行业分类
          </TD>
          <TD  class= input>
            <Input class="code" name=BusinessType ondblclick="return showCodeList('BusinessType',[this]);" onkeyup="return showCodeListKey('BusinessType',[this]);">                                                                                                        
          </TD>
          <TD  class= title>
            单位性质
          </TD>
          <TD  class= input>
            <Input class="code" name=GrpNature ondblclick="return showCodeList('GrpNature',[this]);" onkeyup="return showCodeListKey('GrpNature',[this]);">                                                                                  
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            成立日期
          </TD>
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="FoundDate" >
          </TD>
          <TD  class= title>
            客户组号码
          </TD>
          <TD  class= input>
            <Input class= common name=GrpGroupNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            状态
          </TD>
          <TD  class= input>
            <Input class= common name=State >
          </TD>
          <TD  class= title>
            备注
          </TD>
          <TD  class= input>
            <Input class= common name=Remark >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            黑名单标记
          </TD>
          <TD  class= input>
            <Input class= common name=BlacklistFlag >
          </TD>
          <TD  class= title>
            操作员代码	
          </TD>
          <TD  class= input>
            <Input class= common name=Operator >
          </TD>
        </TR>
            <input type=hidden name=Transact >
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
