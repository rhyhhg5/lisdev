<%
//程序名称：DataItemInit.jsp
//程序功能：
//创建日期：2002-09-19 11:10:36
//创建人  ：吴炜
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{

  try
  {
	// 数据信息部分
    fm.all('SugDataItemCode').value = '';
    fm.all('SugDataItemName').value = '';
    fm.all('SugPrvtPrpty').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
    fm.all('DataLinkPrpty').value = '';
    fm.all('dispFilePath').value = '';
    //fm.all('DataContnt').value = '';
  }
  catch(ex)
  {
    alert("在SugDataItemInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
	var loadFlag = "0";
	try
	{
		if( top.opener.mShowPolDetail == "APPROVE" ) loadFlag = "1";
	}
	catch(ex1){}
	try
	{
		initInpBox();

		// 初始化时查询数据明细
		if( loadFlag == "1" )
		{

			fm.all( 'SugDataItemCode' ).value = "<%=tSugDataItemCode%>";
			querySugDataItemDetail();
		}
	}
	catch(re)
	{
		alert(re);
		//alert("SugDataItemInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


</script>