<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="DataItemQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="DataItemQueryInit.jsp"%>
  <title>数据项目查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    
  <table  class= common align=center>
    <TR  class= common> 
      <TD  class= title> 数据项目代码</TD>
      <TD  class= input> <Input class= common name=SugDataItemCode > </TD>
      <TD  class= title> 数据项目名称</TD>
      <TD  class= input> <Input class= common name=SugDataItemName > </TD>
    </TR>
    <TR  class= common> 
      <TD  class= title> 数据项目属性</TD>
      <TD  class= input> <Input class="code" name=SugPrvtPrpty ondblclick="return showCodeList('SugPrvtPrpty',[this]);" onkeyup="return showCodeListKey('AgentCon',[this]);"> 
      </TD>
      <TD  class= title> 代理人编码</TD>
      <TD  class= input> <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCom',[this]);" onkeyup="return showCodeListKey('AgentCon',[this]);"> 
      </TD>
    </TR>
  </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLSDataItem);">
    		</td>
    		
      <td class= titleImg> 列表信息 </td>
    	</tr>
    </table>
  	<Div  id= "divLSDataItem" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanDataItemGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
