var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

/**********************添加单证信息 ************************/
function InsertCertifyCode()
{   
	if(!init()){
    	return false ;
    }
    
	if(!verifyInput()){
    	return false;
    }
    
    if(!chCertifyCode()){
    	return false;
    }
    
    if(!checkCertify()){
    	return false;
    }
    
	fm.OperateType.value = "INSERT";
 	var i = 0;
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); 
}


/**********************校验单证编码 ************************/
function checkCertify()
 {
	//单证编码不能含有中文字符
	if((/[\u4E00-\u9FA5]/g.test(fm.all('CertifyCode').value)))
	{
		alert("单证编码含有中文字符，请重新输入！！");
		return false;
	}
	var strSQL = "select 1 from LMCardDescription where CertifyCode = '" + fm.CertifyCode.value + "'";
	var arrResult = easyExecSql(strSQL);
	if (arrResult) 
	{
		alert("单证号码已存在，请重新输入！");
		return false;
	}
	if (fm.CertifyClass.value == "D") 
	{
		if (fm.SubCode.value == null || fm.SubCode.value == "") 
		{
			alert("单证类型为定额单证，必须录入两位单证类型码！！");
			return false;
		}
  		  
  		//单证类型码需为数字和大写字母
    	if(!(/^[A-Z0-9]+$/.test(fm.all('SubCode').value))||(trim(fm.all('SubCode').value).length!=2))
    	{
       		alert("两位单证类型码必需为数字和大写字母！");
        	return false;
   		}
   		
		var tSQL = "select 1 from LMCardDescription where CertifyCode != '" + fm.CertifyCode.value + "' and SubCode = '" + fm.SubCode.value + "'";
		var arrResult = easyExecSql(tSQL);
		if (arrResult)
		{
			alert("该单证类型码已使用！");
			return false;
		}
	}
	
	if(fm.ComCode.value=="86"&&fm.CertifyType.value!="00")
	{
		alert("总公司只能制定全国版单证！！！");
		return false;
	}
	if(fm.ComCode.value!="86"&&fm.CertifyType.value!="01")
	{
		alert("分公司只能制定地方版单证！！！");
		return false;
	}
	if(fm.ManageCom.value!="86"&&fm.CertifyType.value=="00")
	{
		alert("全国版单证的管理机构只能为总公司！！！");
		return false;
	}
	if(fm.ManageCom.value=="86"&&fm.CertifyType.value=="01")
	{
		alert("地方版单证的管理机构不能为总公司！！！");
		return false;
	}
	if(!(/^[0-9]*[1-9][0-9]*$/.test(fm.all('WarningNo').value)))
	{
		alert("预警线只能为正整数！！！");
		return false;
	}

	if (!isNaN(fm.Price.value))
	{   
		var tPrice =fm.Price.value *1
		if(!(tPrice>0))
		{
			alert("印刷单价不能为负数！！！");
			return false;
		}	
	}else{
		alert("印刷单价只能为数字类型！！！");
		return false;
	}
	
	if(!(/^[0-9]*[1-9][0-9]*$/.test(fm.all('CertifyLength').value)))
	{
		alert("单证号码长度只能为正整数！！！");
		return false;
	}
	return true;
}


/**********************查询单证信息 ************************/
function QueryCertifyCode()
{   
	if(!init()){
    	return false ;
    }
	var strSQL=" select  serialno ,certifycode ,certifyname ,subcode, comcode,WarningNo,printcode,(select printname from  lmprintcom where printcode =LMCardDescription.printcode ),"
			 + "CertifyType,(case CertifyType when '00' then '全国' when '01' then '地方' else '其他 ' end  ),price  from  LMCardDescription where 1=1  and comcode like'"+ fm.ComCode.value+"%'"
	         + getWherePart('CertifyCode','mCertifyCode')
	         + getWherePart('CertifyName','mCertifyName')  
	         " with ur"; 
  	turnPage.queryModal(strSQL, CardRiskGrid);
	
	if (!turnPage.strQueryResult) 
	{
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}
}

/**********************修改单证信息 ************************/
function UpdateCertifyCode(){
	
	if(!init()){
    	return false ;
    }
	
	var tRow = CardRiskGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    var tRowDatas = CardRiskGrid.getRowData(tRow);
    var comode = tRowDatas[4];
    
    if(fm.ComCode.value!=comode)
    {
     alert("登陆机构与单证所属管理机构不符，不能进行修改操作！！！");
	 return;
    }
    
    if(isNull(fm.tCertifyName.value)){
		alert("请录入单证名称！");
		return;
	}
	if(isNull(fm.tManageCom.value)){
		alert("请录入管理机构！");
		return;
	}
	if(isNull(fm.tWarningNo.value)){
		alert("请录入预警线！");
		return;
	}
	if(isNull(fm.tPrintCode.value)){
		alert("请录入印刷厂！");
		return;
	}
	if(isNull(fm.tCertifyType.value)){
		alert("请录入单证所属类型！");
		return;
	}
	if(isNull(fm.tPrice.value)){
		alert("请录入印刷单价！");
		return;
	}
	fm.OperateType.value = "UPDATE";
 	var i = 0;
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); 

}

/**********************显示修改明细 ************************/
function ShowDetail(){
	 
	 var tSel = 0;
	 tSel = CardRiskGrid.getSelNo();
		try
		{	
		  if (tSel !=0 )
		  	{
				divLLMainAskInput5.style.display = "";
			}else{
				 divLLMainAskInput5.style.display = "none";
		    }
		  
		  fm.all('SerialNo').value=CardRiskGrid.getRowColData(tSel-1,1);
		  fm.all('tCertifyCode').value=CardRiskGrid.getRowColData(tSel-1,2);
		  fm.all('tCertifyName').value=CardRiskGrid.getRowColData(tSel-1,3);
		  fm.all('tManageCom').value=CardRiskGrid.getRowColData(tSel-1,5); 
		  fm.all('tWarningNo').value=CardRiskGrid.getRowColData(tSel-1,6);
		  fm.all('tPrintCode').value=CardRiskGrid.getRowColData(tSel-1,7);
		  fm.all('tCertifyType').value=CardRiskGrid.getRowColData(tSel-1,9);
		  fm.all('tPrice').value=CardRiskGrid.getRowColData(tSel-1,11);
		  showAllCodeName();
		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
}

/**********************双击下拉选项 ************************/
function afterCodeSelect(cName, Filed)
{
    if(cName == 'CertifyClassListNew')
    {
        if(Filed.value == "D")
        {
            fm.all('divSubCodeName').style.display="";
            fm.all('divSubCode').style.display="";
        }
        if(Filed.value == "P")
        {
            fm.all('divSubCodeName').style.display="none";
            fm.all('divSubCode').style.display="none";
        }
    }else if(cName == 'HaveNumber'){
    	
    	if(Filed.value == "Y")
        {
            fm.CertifyLength.value="";
        }
        if(Filed.value == "N")
        {
            fm.CertifyLength.value="10";
            fm.CertifyLength.readOnly = true;
        }
    }
   
}
/**********************初始化 ************************/
function init()
{	
	if(fm.ComCode.value.length!=2 && fm.ComCode.value.length!= 4)
	{
		alert("您无权限进行该操作！！！");
        return false;
	}
	return true ;
}

/**********************校验单证编码全角半角符号************************/
function chCertifyCode()     
{   
	var str= fm.CertifyCode.value;
	for(var i=0 ;i<str.length;i++)  
	{     
		strCode=str.charCodeAt(i);     
		if((strCode>65248)||(strCode==12288))
		{     
			alert("单证编码含有全角字符，请重新输入！！！");  
            return false;   
		}  
	} 
	return true; 
}
/**********************公用为空校验************************/
function isNull(str){
	if(str == null || str.trim() == "" || str == "null"){
		return true;
	}
	return false;
}
/**********************提交后操作************************/
function afterSubmit(FlagStr,Content)
{  
	showInfo.close();
    window.focus; 
	if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "操作失败" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" +  "操作成功" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }     
	
}