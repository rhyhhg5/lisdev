<%@page contentType="text/html;charset=GBK"%>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="PrintingShopInput.js"></SCRIPT>
		<%@include file="PrintingShopInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./PrintingShopSave.jsp" method=post name=fm
			target="fraSubmit">
			<%@include file="../common/jsp/InputButton.jsp"%>
			<table>
				<TR>
					<TD>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLLMainAskInput);">
					</TD>
					<TD class=titleImg>
						添加印刷厂
					</TD>
				</TR>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>
						印刷厂编码
					</td>
					<td class=input>
						<input class=common name=PrintCode readonly >
					</td>
					<td class=title>
						印刷厂名称
					</td>
					<td class=input>
						<input class=common name=PrintName elementtype=nacessary>
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>印刷厂状态</td>
                	<td class=input><input class=codeno name=EffectiveState readonly CodeData="0|^0|有效^1|无效"
                    ondblClick="showCodeListEx('EffectiveState',[this,EffectiveStateName],[0,1],null,null,null,1);" onkeyup="showCodeListEx('EffectiveState',[this,EffectiveStateName],[0,1],null,null,null,1);" ><input name=EffectiveStateName class=codename verify="状态|NOTNULL" elementtype=nacessary >
                	</td>
					<td class="title">
						联系人
					</td>
					<td class="input">
						<input class="common" name="LinkMan">
					</td>
				</tr>
				<tr class="common">
					<td class="title">
						联系电话
					</td>
					<td class="input">
						<input class="common" name="Phone">
					</td>
					<td class="title">
						联系地址
					</td>
					<td class="input">
						<input class="common" name="PostalAddress">
					</td>
				</tr>
				<tr class="common">
					<td class="title">
						备注
					</td>
					<td class=input colspan="5">
						<textarea name="Remark"  cols="100%" rows="3"></textarea>
					</td>
				</tr>
			</table>

			<INPUT VALUE="增  加" class=cssButton TYPE=button
				onclick="InsertPrintCom();">
			<HR>
			<table>
				<TR>
					<TD>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLLMainAskInput3);">
					</TD>
					<TD class=titleImg>
						印刷厂查询及修改
					</TD>
				</TR>
			</table>
			<Div id="divLLMainAskInput3" style="display: ''">
				<table class=common>
					<TR class=common8>
						<TD class=title8 style="width: '10%'">
							印刷厂编码
						</TD>
						<TD class=input8>
							<input class="common" name=mPrintCode>
						</TD>
						<TD class=title8 style="width: '10%'">
							印刷厂名称
						</TD>
						<TD class=input8>
							<input class="common" name=mPrintName>
						</TD>
					</TR>
				</table>
				<INPUT VALUE="查  询" class=cssButton TYPE=button
					onclick="QueryPrintCom();">
				<INPUT VALUE="修  改" class=cssButton TYPE=button
					onclick="UpdatePrintCom();">
				<TR class=common8>
					<TD class=input8>
						<input class="common" name=mComCode TYPE=hidden>
					</TD>
				</TR>
				<br>
			</div>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCGrp1);">
					</td>
					<td class=titleImg>
						印刷厂信息
					</td>
				</tr>
			</table>
			<Div id="divLLMainAskInput4" align=center style="display: ''">
				<Table class=common>
					<TR class=common>
						<TD text-align: left colSpan=1>
							<span id="spanCardRiskGrid"></span>
						</TD>
					</TR>
				</Table>
				<INPUT CLASS=cssButton VALUE="首  页" TYPE=button
					onclick="turnPage.firstPage();">
				<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
					onclick="turnPage.previousPage();">
				<INPUT CLASS=cssButton VALUE="下一页" TYPE=button
					onclick="turnPage.nextPage();">
				<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button
					onclick="turnPage.lastPage();">
			</Div>
			<Div id="divLLMainAskInput5" style="display: 'none'">
				<table class=common>
					<tr class=common>
						<td class=title>
							印刷厂编码
						</td>
						<td class=input>
							<input class=common name=tPrintCode readonly>
						</td>
						<td class=title>
							印刷厂名称
						</td>
						<td class=input>
							<input class=common name=tPrintName>
						</td>
					</tr>
					<tr class=common>
						<td class=title>
							印刷厂状态
						</td>
                		<td class=input><input class=codeno name=tEffectiveState readonly CodeData="0|^0|有效^1|无效"
                    	ondblClick="showCodeListEx('CertifyStateList',[this,StateName],[0,1],null,null,null,1);" ><input name=StateName class=codename verify="状态|NOTNULL" elementtype=nacessary >
                		</td>
                		<td class="title">
						    联系人
						</td>
						<td class="input">
							<input class="common" name="tLinkMan" >
						</td>
					</tr>
					<tr class="common">
						<td class="title">
							联系电话
						</td>
						<td class="input">
							<input class="common" name="tPhone">
						</td>
						<td class="title">
							联系地址
						</td>
						<td class="input">
							<input class="common" name="tPostalAddress">
						</td>
					</tr>
					<tr>
						<td class=title>
							备注
						</td>
						<td class=input colspan="5">
						<textarea name="tRemark"  cols="100%" rows="3"></textarea>
						</td>
					</tr>
				</table>
			</Div>
			<input name=OperateType type=hidden class=common>
			<input name=ComCode type=hidden class=common>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
