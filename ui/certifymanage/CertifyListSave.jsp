
<%
//程序名称：
//程序功能：制定单证征订模版
//创建日期：2013-07-15
//创建人  ：WDK
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certifymanage.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK"%>
<%

   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
   
   
   GlobalInput tGI = (GlobalInput)session.getValue("GI");
   String tComCode = tGI.ComCode;
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {    	
	String mBatchNo = request.getParameter("BatchNo");
	String mOperate =request.getParameter("OperateType");
	String mTempTypes =request.getParameter("TempTypes");
	
	System.out.println("mOperate:" + mOperate);
	System.out.println("mTempTypes:" + mTempTypes);
	
	
	
	LZcertifyBatchStateSet  tLZcertifyBatchStateSet= new LZcertifyBatchStateSet();
	LZCertifyTemplateSet   tLZCertifyTemplateSet = new LZCertifyTemplateSet();
	   
  	     
	if ("INSERT".equals(mOperate))
	{   
		String tChk[]=request.getParameterValues("InpCertifyListChk");
  		String tCertifyCode[] = request.getParameterValues("CertifyList1");
  		String tPrintCode[] = request.getParameterValues("CertifyList6");
  		System.out.println(tChk.length);         
       for (int i = 0; i < tChk.length; i++) {   
			if (tChk[i]!= null &&tChk[i].equals("1")) {
				LZCertifyTemplateSchema  tLZCertifyTemplateSchema  = new  LZCertifyTemplateSchema();

			   	 tLZCertifyTemplateSchema.setCertifyCode(tCertifyCode[i].trim());          
				 tLZCertifyTemplateSchema.setBatchNo(mBatchNo);  
	  			 tLZCertifyTemplateSchema.setPrintCode(tPrintCode[i].trim());
				
				System.out.println("第"+i+"个单证号码" + tCertifyCode[i].trim());
				
				tLZCertifyTemplateSet.add(tLZCertifyTemplateSchema);
				 
			}
		}  
       
	}
    if ("DELETE".equals(mOperate)){
    
    	System.out.println("000000000000000000000000000000000");
    	String mChk[]=request.getParameterValues("InpCertifyStoreListChk");
    	String mSerialNo[] = request.getParameterValues("CertifyStoreList1");
    	String mCertifyCode[] = request.getParameterValues("CertifyStoreList3");  
    	System.out.println("长度》》》》》》》》》》》》》》"+mChk.length);    
       for (int i = 0; i < mChk.length; i++) {
			if (mChk[i]!= null &&mChk[i].equals("1")) {
			
				LZCertifyTemplateSchema  zLZCertifyTemplateSchema  = new  LZCertifyTemplateSchema();
			    
			    zLZCertifyTemplateSchema.setSerialNo(mSerialNo[i].trim());
			    zLZCertifyTemplateSchema.setBatchNo(mBatchNo);
				zLZCertifyTemplateSchema.setCertifyCode(mCertifyCode[i].trim()); 
				System.out.println("第"+i+"个流水号" + mSerialNo[i].trim()+" 单证号为"+ mCertifyCode[i].trim());
				tLZCertifyTemplateSet.add(zLZCertifyTemplateSchema);
			}
		}  
	}
	if ("UPDATE".equals(mOperate)){
	            
	            LZcertifyBatchStateSchema tLZcertifyBatchStateSchema = new LZcertifyBatchStateSchema();
				tLZcertifyBatchStateSchema.setBatchNo(mBatchNo);
				tLZcertifyBatchStateSet.add(tLZcertifyBatchStateSchema);
	}
    //全部增加
    if("ALLINSERT".equals(mOperate)){
    			String mAddSql =  request.getParameter("AddSql");
    			LMCardDescriptionDB tLMCardDescriptionDB = new LMCardDescriptionDB();
    			//查询全部单证
    			LMCardDescriptionSet tLMCardDescriptionSet=tLMCardDescriptionDB.executeQuery(mAddSql);
    			if(tLMCardDescriptionSet !=null && tLMCardDescriptionSet.size()>1){
    			 for(int i=1; i<=tLMCardDescriptionSet.size();i++){
    			 	LMCardDescriptionSchema tLMCardDescriptionSchema=tLMCardDescriptionSet.get(i);
    			 	LZCertifyTemplateSchema  tLZCertifyTemplateSchema  = new  LZCertifyTemplateSchema();
				   	tLZCertifyTemplateSchema.setCertifyCode(tLMCardDescriptionSchema.getCertifyCode());          
					tLZCertifyTemplateSchema.setBatchNo(mBatchNo);  
		  			tLZCertifyTemplateSchema.setPrintCode(tLMCardDescriptionSchema.getPrintCode());
					tLZCertifyTemplateSet.add(tLZCertifyTemplateSchema);
    			 }
    		}
    }
    //全部删除
    if ("AllDELETE".equals(mOperate)){
	            
	           LZCertifyTemplateSchema  zLZCertifyTemplateSchema  = new  LZCertifyTemplateSchema();
	           zLZCertifyTemplateSchema.setBatchNo(mBatchNo);
	           tLZCertifyTemplateSet.add(zLZCertifyTemplateSchema);
	}

   	VData aVData = new VData();
    aVData.add(tGI);
    aVData.add(tLZcertifyBatchStateSet);
    aVData.add(tLZCertifyTemplateSet);
    
    
    CertifyListUI tCertifyListUI = new CertifyListUI();
    System.out.println("I come in the FinBankAddUI ");
      if (!tCertifyListUI.submitData(aVData, mOperate)) 
      {  
          Content = "操作失败";
          System.out.println("添加失败");
          FlagStr = "Fail";    
      }
      else
      {
       Content = "操作成功";  

      }
  System.out.println("FlagStr : "+FlagStr);
  System.out.println("Content : "+Content);
%>
<html>
	<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>","<%=mOperate%>");
</script>
</html>
<%
}
%>
