
// 界面初始化
var turnPage = new turnPageClass();
var comTurnPage = new turnPageClass(); 
var detailTurnPage = new turnPageClass(); 


function init(){

	if(fm.ManageCom.value.length == 2){
		var query = "select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,comcode from LZcertifyBatchState cbs where batchstate='01' and temptypes='00'";
		turnPage.queryModal(query, CertifyBatchGrid);
	} else {
		var query = "select batchno,startdate,enddate,case temptypes when '00' then '全国版' else '地方版' end,case batchstate when '01' then '正在征订' when '02' then '征订结束' end,comcode from LZcertifyBatchState cbs where batchstate='01'";
		turnPage.queryModal(query, CertifyBatchGrid);
	}
}

function finishBatch(){
	
	if(!check())
	{
    	return false;
  	} 

	if(fm.BatchNo.value <= 0){
		alert("请点选待结束的征订批次！");
		return false;
	}
	
	if(!confirm("是否确认结束本次征订？")){
		return false;
	}
	
	var query = "select 1 from LZCertifyBatchMain where batchno='" + fm.BatchNo.value + "' and stateflag ='00'";
	var arrResult = easyExecSql(query);
	if(arrResult){
		if(!confirm("批次下存在尚未审核的导入批次，征订结束后会自动对未审核的批次进行审核，是否确认结束征订？")){
			return false;
		}
	}
	
	var query = "select 1 from LZCertifyBatchMain where batchno='" + fm.BatchNo.value + "'";
	var arrResult = easyExecSql(query);
	if(!arrResult){
		if(!confirm("不存在分公司导入的批次，是否确认结束征订？")){
			return false;
		}
	}
	
	fm.action = "CertifyBatchFinishSave.jsp";
	submitData();
}

// 审核确认
function comConfirm(){
	if(!check())
	{
    	return false;
  	} 

	var selNo = CertifyListGrid.getSelNo()
	
	if(selNo <= 0){
		alert("请点选待审核确认导入批次！");
		return false;
	}
	
	if(!checkData(CertifyBatchGrid.getRowColData(CertifyBatchGrid.getSelNo()-1 , 1))){
		return false;
	}
	
	fm.action = "CertifyBatchConfirmSave.jsp";
	fm.DealType.value = "ONLY";
	submitData();
}

// 全部审核
function allConfirm(){
	
	if(!check())
	{
    	return false;
  	} 
	if(!checkData()){
		return false;
	}

	fm.action = "CertifyBatchConfirmSave.jsp";
	fm.DealType.value = "BATCH";
	submitData();
}

function checkData(importBatchNo){
	if(isNull(fm.BatchNo.value)){
		alert("请选择待处理征订批次！");
		return false;
	}
	
	if(importBatchNo != null){
	
	}
	
	return true;
}

// 机构批次信息查询
function queryCertifyBatchList(batchNo){

	if(isNull(fm.BatchNo.value)){
		alert("请选择待处理征订批次！");
		return;
	}

	var query = "select ImportBatchNo,comcode,(select name from ldcom where comcode=bm.comcode),case stateflag when '00' then '未审核' when '01' then '已审核' end " 
			+ " from LZCertifyBatchMain bm"
			+ " where stateflag In ('00','01') and batchno='" + batchNo + "' "
			+ " and comcode like '" + fm.ManageCom.value + "%'"
			+ " order by stateflag,bm.comcode" ;
			
	comTurnPage.queryModal(query, CertifyListGrid);
}

// 机构明细信息查询
function queryCertifyComList(imputbatchNo){

	if(isNull(imputbatchNo)){
		alert("请选择待导入批次！");
		return;
	}

	var query = "select ImportBatchNo,CertifyCode,(select CertifyName from LMCardDescription where CertifyCode=bm.CertifyCode),serialno,SumCount,SumCountYW,(select name from ldcom where comcode=bm.comcode) " 
			+ " from LZCertifyBatchInfo bm"
			+ " where ImportBatchNo='" + imputbatchNo + "' ";
	detailTurnPage.queryModal(query, CertifyDetailGrid);
}

// 点击征订批次
function batchClick(){

	var batchNo = CertifyBatchGrid.getRowColData(CertifyBatchGrid.getSelNo()-1 , 1);
	fm.BatchNo.value = batchNo;
	
	CertifyDetailGrid.clearData();
	
	queryCertifyBatchList(batchNo);
}

// 点击机构批次
function comBatchClick(){
	var imputbatchNo = CertifyListGrid.getRowColData(CertifyListGrid.getSelNo()-1 , 1);
	queryCertifyComList(imputbatchNo);

}
// 公用为空校验
function isNull(str){
	if(str == null || str.trim() == "" || str == "null"){
		return true;
	}
	return false;
}

// 提交
function submitData(){
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

/**********************校验 ************************/
function check()
{	
	if(fm.ManageCom.value.length!=2 && fm.ManageCom.value.length!= 4)
	{
		alert("您无权限进行该操作！！！");
        return false;
	}
	
	return true ;
}


// 返回
function afterSubmit(FlagStr, Content, Type){

	showInfo.close();
	
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	
	if(Type == "finish"){
		init();
		fm.BatchNo.value = "";
		CertifyListGrid.clearData();
		CertifyDetailGrid.clearData();
	} else {
		batchClick();
	}
}