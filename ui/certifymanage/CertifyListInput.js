var mOperate="";
var showInfo;
var mPrintFlag="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

/**********************查询单证信息信息 ************************/
function queryCertify()
{   
   var strSQL="";
   if (fm.TempTypes.value ==00)
		{
			strSQL = "select CertifyCode,Certifyname,case HavePrice when 'Y' then '是' when 'N' then '否' else '其他' end , "
			 + " comcode, case CertifyType when '00' then '全国' when '01' then '地方' else '其他' end ,printcode ,"
			 + " (select  PrintName from  LMPrintCom where printcode= lmc.printcode)"
			 + " from LMCardDescription  lmc where ComCode = '"+fm.ManageCom.value+"' and CertifyType='00'"
			 +"  and  certifycode not in (select certifycode from LZCertifyTemplate  where batchno = '"+fm.BatchNo.value+"')  with ur";
		}
	if(fm.TempTypes.value ==01)
	   {
		strSQL = "select CertifyCode,Certifyname,case HavePrice when 'Y' then '是' when 'N' then '否' else '其他' end , "
			 + " comcode, case CertifyType when '00' then '全国' when '01' then '地方' else '其他' end ,printcode ,"
			 + " (select  PrintName from  LMPrintCom where printcode= lmc.printcode)"
			 + " from LMCardDescription lmc where ComCode = '"+fm.ManageCom.value+"' and CertifyType='01'  "
			 +"  and  certifycode not in (select certifycode from LZCertifyTemplate  where batchno = '"+fm.BatchNo.value+"')  with ur";
		}       
		
	turnPage.queryModal(strSQL, CertifyList);
}


/**********************添加到模版 ************************/
function addCertifyBatchList()
{	
	//校验是否选中
    var tChk=false;
	var lineCount = CertifyList.mulLineCount;
	for( i=0;i<lineCount;i++)
	{
	 tChk = CertifyList.getChkNo(i);
	 if(tChk)
	 {
	 	break;
	 }
  	}
  	if( tChk == false || tChk == null ){
  		alert("请您最少选择一条单证!");
  		return false;
  	}

	 if(!checkCertifyBatch())
	{
    	return false;
  	} 
     fm.all('OperateType').value ="INSERT";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}


/**********************添加全部到模版 ************************/
function addAllCertifyBatchList()
{
	if(!checkCertifyBatch())
	{
    	return false;
  	} 
  	
  	var strSQL="";
   if (fm.TempTypes.value ==00)
		{
			fm.AddSql.value ="select * "
			 + " from LMCardDescription  lmc where ComCode = '"+fm.ManageCom.value+"' and CertifyType='00'"
			 +"  and  certifycode not in (select certifycode from LZCertifyTemplate  where batchno = '"+fm.BatchNo.value+"')  with ur";
		}
		
	if(fm.TempTypes.value ==01)
	   {
		fm.AddSql.value = "select * "
			 + " from LMCardDescription lmc where ComCode = '"+fm.ManageCom.value+"' and CertifyType='01'  "
			 +"  and  certifycode not in (select certifycode from LZCertifyTemplate  where batchno = '"+fm.BatchNo.value+"')  with ur";
		}       
		
	 fm.all('OperateType').value ="ALLINSERT";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
	//turnPage.queryModal(strSQL, CertifyStoreList);

}


/**********************从模版中删除信息 ************************/
function delCertifyBatchList()
{	
	//校验是否选中
    var tChk=false;
	var lineCount = CertifyStoreList.mulLineCount;
	for( i=0;i<lineCount;i++)
	{
	 tChk = CertifyList.getChkNo(i);
	 if(tChk)
	 {
	 	break;
	 }
  	}
  	if( tChk == false || tChk == null ){
  		alert("请您最少选择一条单证!");
  		return false;
  	}
	

	if(!checkCertifyBatch())
	{
    	return false;
  	} 
     fm.all('OperateType').value ="DELETE";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}


/**********************从模版中删除全部信息 ************************/
function delAllCertifyBatchList()
{
	if(!checkCertifyBatch())
	{
    	return false;
  	} 
     fm.all('OperateType').value ="AllDELETE";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交


}
/**********************模版确认信息 ************************/
function updateCertifyBatchList()
{	
	if(!checkCertifyBatch())
	{
    	return false;
  	} 
     fm.all('OperateType').value ="UPDATE";
     var i = 0;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
     fm.submit(); //提交
    	
}

/**********************查询已添加模版信息 ************************/
function queryCertifyBatchList()
{   
   var strSQL="";
 
	strSQL = "select SerialNo, CertifyCode,(select distinct Certifyname from   LMCardDescription  where certifycode= lct.certifycode fetch first 1 rows only ) "
		  + " from LZCertifyTemplate  lct where batchno = '"+fm.BatchNo.value+"' with ur"	
	
	turnPage1.queryModal(strSQL, CertifyStoreList);
}


/**********************检查模版是否确认 ************************/
function checkCertifyBatch()
{   
  	var strSQL = "select 1 from LZCertifyBatchState where batchno = '"  + fm.BatchNo.value + "' and BatchState ='00'";
    var arrResult = easyExecSql(strSQL);
    if(!arrResult)
    {   
        alert("该批次模版已确认,您不能进行该操作！！！");
        return false;
    }
    
     return true;
}
/********************** 提交后续操作************************/
function afterSubmit(FlagStr,code,operate)
{  
	if (operate =="INSERT")
	{ 
	  queryCertify();
	  queryCertifyBatchList();
	   
	}
	
	if ( operate =="DELETE")
	{
	  queryCertifyBatchList();
	  queryCertify();
	 
	}
	
	if (operate =="ALLINSERT")
	{ 
	  initCertifyList();
      initCertifyStoreList();
	  queryCertify();
	  queryCertifyBatchList();
	   
	}
	
	if ( operate =="AllDELETE")
	{
	   initCertifyList();
       initCertifyStoreList();
	   queryCertifyBatchList();
	   queryCertify();
	 
	}
	
	showInfo.close();
    window.focus; 
	if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "操作失败" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" +  "操作成功" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }     
	
}
/**********************返回界面 ************************/
function goBack()
{	
    window.location.replace("./CertifyTemplateBatchApply.jsp");
}

