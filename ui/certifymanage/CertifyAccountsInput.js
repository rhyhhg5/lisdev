
// 界面初始化
var turnPage = new turnPageClass();
var comTurnPage = new turnPageClass(); 


// 下载
function download(){
	
	printAllGrpPDFBatch();	
	//if(fm.BatchNo.value== null || fm.BatchNo.value=="")
	//{
	//	alert("征订批次号不能为空！！！")
	//	return false;
	//}
	
	
	submitData();
}

// 提交
function submitData(){
	var showStr="正在处理报表信息，在下载完毕前，请勿再次点击【下载清单】";
	fm.submit();
	alert(showStr);
}

// 返回
function afterSubmit(FlagStr, Content, Type){

}


/************ 查询所选时间内的批次显示到Grid中 ******************/
function queryCertifyBatchList()
{	
    
    var tStrSql = ""
                + " select  lzp.Batchno,lci.Certifycode,lmd.certifyname,lmd.price,operatingtype,sendoutcom,'',receivecom, "
                +" '', totalprice, lci.sumcount, lci.startno, lci.endno, lzp.makedate "
                +" from LZcertifyInventoryTrace lci, LZPrintList lzp ,LZCertifyBatchMain lbm ,LMCardDescription  lmd "
				+" where (lci.certifycode = lzp.certifycode  and lbm.batchno = lzp.batchno "
				+ " and lci.startno = lzp.startno and lci.endno = lzp.endno "
                + " and lci.OperatingType='00' and lmd.certifycode = lci.certifycode "
                + getWherePart('lzp.BatchNo','BatchNo')
                + getWherePart('lci.CertifyCode','CertifyCode')
		        + getWherePart('lzp.MakeDate', 'endDate', '<=')
		        + getWherePart('lzp.MakeDate', 'startDate', '>=')
                + ") "
                +" group by lzp.Batchno,lmd.certifyname ,lci.Certifycode,lmd.price,operatingtype,sendoutcom,receivecom, "
				+" totalprice, lci.sumcount, lci.startno, lci.endno, lzp.makedate "
              	+ " union all "
              	+ " select lzp.Batchno,lci.Certifycode,lmd.certifyname,lmd.price,operatingtype,sendoutcom,'',receivecom, "
                +" '', totalprice, lci.sumcount, lci.startno, lci.endno, lzp.makedate  "
				 +" from LZcertifyInventoryTrace lci, LZPrintList lzp ,LZCertifyBatchMain lbm ,LMCardDescription  lmd "
				+ " where '1'='1'  " 
				+ " and  ( "
 				+" lci.certifycode = lzp.certifycode  and lbm.batchno = lzp.batchno "
				+ " and lzp.startno is null "
 				+ " and lzp.endno is null   "
				 + " and lci.OperatingType='00' and lmd.certifycode = lci.certifycode and lzp.serialno = lci.prtno )  "
				+ getWherePart('lzp.BatchNo','BatchNo')
                + getWherePart('lci.CertifyCode','CertifyCode')
		        + getWherePart('lzp.MakeDate', 'endDate', '<=')
		        + getWherePart('lzp.MakeDate', 'startDate', '>=')
				 +" group by lzp.Batchno,lmd.certifyname ,lci.Certifycode,lmd.price,operatingtype,sendoutcom,receivecom, "
				+ " totalprice, lci.sumcount, lci.startno, lci.endno, lzp.makedate "
		        + " with ur ";
    turnPage.pageDivName = "divImportBatchListGridPage";
    turnPage.queryModal(tStrSql, ImportBatchListGrid);
    
    if (!turnPage.strQueryResult)
    {
        alert("未查询到批次信息！");
        return false;
    }
    
    return true;
}

function printAllGrpPDFBatch()
{
	var count = 0;
	for(var i = 0; i < ImportBatchListGrid.mulLineCount; i++ )
	{
		if( ImportBatchListGrid.getChkNo(i) == true )
		{
			count ++;
		}
	}
	
	if(count == 0){
		alert("请选择一张保单后，再进行打印操作！");
		return;
	}
    fm.target = "fraSubmit";
    fm.action = "./CertifyAccountsSave.jsp";
    return true;
}