
<%
	// 防止IE缓存页面
	response.setHeader("Pragma", "No-cache");
	response.setHeader("Cache-Control", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<%@page contentType="text/html;charset=GBK"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="CertifyListInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="CertifyListInit.jsp"%>

	</head>
	<body onload="initForm()" style="behavior:url(#default#clientCaps)"
		id="oClientCaps">
		<form action="./CertifyListSave.jsp" method="post" name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;" />
					</td>
					<td class="titleImg">
						征订模版制定
					</td>
				</tr>
			</table>

			<table class="common">
				<tr class="common">
					<td class="title">
						批次号
					</td>
					<td class="input">
						<input class="readonly" name="BatchNo" readonly="readonly" />
					</td>
					<td>
						&nbsp;
					</td>
					<td>
						&nbsp;
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
			</table>

			<hr>
			<!-- 单证列表 -->
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divCertifyList);">
					</td>
					<td class=titleImg>
						单证列表
					</td>
				</tr>
			</table>

			<div id="divCertifyList">
				<table class="common">
					<tr class="common">
						<td text-align: left colSpan=1>
							<span id="spanCertifyList"></span>
						</td>
					</tr>
				</table>
			</div>
			 <div align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage.lastPage();" />                     
            </div>
			<tr>

				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="添  加" onclick="addCertifyBatchList();" />
							<input class="cssButton" type="button" value="全部添加" onclick="addAllCertifyBatchList();" />
						</td>
					</tr>

				</table>
				<hr>
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divShowStore);">
						</td>
						<td class=titleImg>
							已添加单证信息
						</td>
					</tr>
				</table>


				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="删  除" onclick="delCertifyBatchList();" />
							<input class="cssButton" type="button" value="全部删除" onclick="delAllCertifyBatchList();" />
						</td>
					</tr>
				</table>



				<div id="divShowStore" style="display:''">
					<table class="common">
						<tr class="common">
							<td text-align: left colSpan=1>
								<span id="spanCertifyStoreList"></span>
							</td>
						</tr>
					</table>
				</div>
				 <div align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>

				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="模版确认"
								onclick="updateCertifyBatchList();" />
						</td>
						<td class="common">
							<input class="cssButton" type="button" value="返  回"
								onclick="goBack();" />
						</td>
					</tr>
				</table>

				<input type=hidden name="ManageCom" value="<%=strCom%>">
				<input type=hidden name=OperateType class=common>
				<input type=hidden name=TempTypes class=common>
				<input type=hidden name=AddSql class=common>
		</form>

		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>

	</body>
</html>
