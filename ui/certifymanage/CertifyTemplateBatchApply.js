//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryCertifyBatchList()
{	
	if(!init()){
    	return false ;
    }
    if(!verifyInput2())
    {
        return false;
    }
    
    var tStrSql = ""
        + " select BatchNo, makedate, BatchState,(case BatchState when '00'then '正在制定模版' when '01' then '开始征订'  when '02' then ' 征订结束' else '其他' end ), "
        + " ComCode,TempTypes,(case TempTypes when '00' then '全国版' when '01'then '地方版' else '其他' end  ), Operator  "
        + " from LZcertifyBatchState a "
        + " where 1 = 1  and BatchState='00' and ComCode like'"+fm.ComCode.value+"%' "
        + getWherePart("BatchNo", "BatchNo")
        + getWherePart("ComCode", "ManageCom")
        + getWherePart('a.MakeDate', 'startDate', '>=')
        + getWherePart('a.MakeDate', 'endDate', '<=')
        + " with ur "
        ;
    
    turnPage1.pageDivName = "divImportBatchListGridPage";
    turnPage1.queryModal(tStrSql, ImportBatchListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("未查询到批次信息！");
        return false;
    }
    
    return true;
}

/**
 * 进入模版导入界面
 */
function importBatch()
{	
	if(!init())
    {
    	return false ;
    }
    
    if(!checkComCode()){
    	return false ;
    }
    
    var tRow = ImportBatchListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = ImportBatchListGrid.getRowData(tRow);
    
    var tBatchNo = tRowDatas[0];
    var tBatchState = tRowDatas[3];
    var tTempTypes = tRowDatas[5];
   
    var tStrUrl = "./CertifyListInput.jsp"
        + "?BatchNo=" + tBatchNo
        + "&TempTypes=" + tTempTypes
        ;

    window.location = tStrUrl;
}

/**
 * 申请导入批次号。
 */
function applyImportBatch()
{   
	if(!init())
    {
    	return false ;
    }
	var strSQL = "select 1 from LZCertifyBatchState where batchno = '"  + fm.BatchNo.value + "'";
    var arrResult = easyExecSql(strSQL);
    if(arrResult)
    {   
        alert("该批次号已存在，请重新申请！！！");
        return false;
    }
    fm.all('OperateType').value ="APPLY";
    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}
/**********************初始化 ************************/
function init()
{	
	if(fm.ComCode.value.length!=2 && fm.ComCode.value.length!= 4)
	{
		alert("您无权限进行该操作！！！");
        return false;
	}
	return true ;
}

/*****************校验管理机构************************/
function checkComCode()
{   
 	var tBatchNo = ImportBatchListGrid.getRowColData(ImportBatchListGrid.getSelNo()-1 , 1);
 	var tSQL=" select comcode from  LZCertifyBatchState  where batchno='"+tBatchNo+"'";
 	
 	var arrResult = easyExecSql(tSQL);
  	
 	if(fm.ComCode.value!=arrResult[0][0]){
 		
 		alert("您没有权限对该批次进行操作，请重新选择！！！");
    	return false;
 	
 	}
	return true;
}


/**
 * 申请导入批次号提交后动作。
 */
function afterApplyCertifyTemplateSubmit(FlagStr, Content, cBatchNo)
{   
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("申请批次失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        fm.BatchNo.value = cBatchNo;
        queryCertifyBatchList()
    }
}

