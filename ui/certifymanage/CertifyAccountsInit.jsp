
<%
	//Creator :刘岩松
	//Date :2003-04-18
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
     fm.MagCom.value=managecom;
  }
  catch(ex)
  {
    alert("1在CardMaxInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    //initInpBox();
    //initCertifyMaxGrid();
    initImportBatchListGrid();
  }
  catch(re)
  {
    alert("3CertifyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}




function initImportBatchListGrid()
{			
	var iArray = new Array();
    
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="批次号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="150";        		  //列宽
      iArray[1][2]=180;          			//列最大值
      iArray[1][3]=1;              		//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[2]=new Array();
      iArray[2][0]="编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="80";        			//列宽
      iArray[2][2]=80;          			//列最大值
      iArray[2][3]=1;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="单证名称";    	  //列名
      iArray[3][1]="80";            	//列宽
      iArray[3][2]=180;            		//列最大值
      iArray[3][3]=1;              		//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="单价";    	  //列名
      iArray[4][1]="80";            	//列宽
      iArray[4][2]=180;            		//列最大值
      iArray[4][3]=1;              		//是否允许输入,1表示允许，0表示不允许

       
     
      iArray[5]=new Array();
      iArray[5][0]="操作类型";    	      //列名
      iArray[5][1]="0";            	//列宽
      iArray[5][2]=50;   //列最大值
      iArray[5][3]=1;              		//是否允许输入,1表示允许，0表示不允许

      
      iArray[6]=new Array();
      iArray[6][0]="出库机构";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[6][1]="120";            	//列宽
      iArray[6][2]=50;            		//列最大值
      iArray[6][3]=1;              		//是否允许输入,1表示允许，0表示不允许  

	  iArray[7]=new Array();
      iArray[7][0]="出库机构名";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[7][1]="0";            	//列宽
      iArray[7][2]=50;            		//列最大值
      iArray[7][3]=1;              		//是否允许输入,1表示允许，0表示不允许  
      
      
      iArray[8]=new Array();
      iArray[8][0]="入库机构";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[8][1]="80";            	//列宽
      iArray[8][2]=50;            		//列最大值
      iArray[8][3]=0;
      
      iArray[9]=new Array();
      iArray[9][0]="入库机构名";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[9][1]="0";            	//列宽
      iArray[9][2]=50;            		//列最大值
      iArray[9][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      
      iArray[10]=new Array();
      iArray[10][0]="总价";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[10][1]="80";            	//列宽
      iArray[10][2]=50;            		//列最大值
      iArray[10][3]=1;                    		//是否允许输入,1表示允许，0表示不允许  
      
	  iArray[11]=new Array();
      iArray[11][0]="数量";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[11][1]="80";            	//列宽
      iArray[11][2]=50;            		//列最大值
      iArray[11][3]=1;
      
      
      iArray[12]=new Array();
      iArray[12][0]="起始号";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[12][1]="80";            	//列宽
      iArray[12][2]=50;            		//列最大值
      iArray[12][3]=1;
     
      iArray[13]=new Array();
      iArray[13][0]="终止号";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[13][1]="80";            	//列宽
      iArray[13][2]=50;            		//列最大值
      iArray[13][3]=1;
      
       iArray[14]=new Array();
      iArray[14][0]="操作时间";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[14][1]="80";            	//列宽
      iArray[14][2]=50;            		//列最大值
      iArray[14][3]=1;
      
      ImportBatchListGrid = new MulLineEnter( "fm" , "ImportBatchListGrid" );
      //这些属性必须在loadMulLine前
      ImportBatchListGrid.displayTitle = 1;
      ImportBatchListGrid.mulLineCount = 0;   
      ImportBatchListGrid.hiddenPlus = 1;        
      ImportBatchListGrid.hiddenSubtraction = 1; 
      ImportBatchListGrid.canChk = 1;     
      ImportBatchListGrid.loadMulLine(iArray);
     

    }
    catch(ex)
    {
        alert("初始化ImportBatchListGrid时出错：" + ex);
    }
}







function initCertifyMaxGrid()
{
	var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许      

      iArray[1]=new Array();
      iArray[1][0]="年";    	//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=150;            			//列最大值
      iArray[1][3]=1;     



      iArray[2]=new Array();
      iArray[2][0]="月";         			//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=250;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="日";         			//列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=1;

      iArray[4]=new Array();
      iArray[4][0]="单证编码";         			//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=1;

      iArray[5]=new Array();
      iArray[5][0]="单证名称";         			//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=1;
      
      iArray[6]=new Array();
      iArray[6][0]="类别";         			//列名
      iArray[6][1]="40px";            		//列宽
      iArray[6][2]=60;            			//列最大值
      iArray[6][3]=1;
      
      iArray[7]=new Array();
      iArray[7][0]="出/入库";         			//列名
      iArray[7][1]="40px";            		//列宽
      iArray[7][2]=60;            			//列最大值
      iArray[7][3]=1;
      
      iArray[8]=new Array();
      iArray[8][0]="出库部门";         			//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=60;            			//列最大值
      iArray[8][3]=1;
      
      iArray[9]=new Array();
      iArray[9][0]="入库金额";         			//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=60;            			//列最大值
      iArray[9][3]=1;
      
      iArray[10]=new Array();
      iArray[10][0]="出/入库数量";         			//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=60;            			//列最大值
      iArray[10][3]=1;
      
      iArray[11]=new Array();
      iArray[11][0]="出库起始号码";         			//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=60;            			//列最大值
      iArray[11][3]=1;
      
      
      iArray[12]=new Array();
      iArray[12][0]="出库终止号码";         			//列名
      iArray[12][1]="80px";            		//列宽
      iArray[12][2]=60;            			//列最大值
      iArray[12][3]=1;
      

      CertifyMaxGrid = new MulLineEnter( "fm" , "CertifyMaxGrid" );
      CertifyMaxGrid.mulLineCount = 0;
      CertifyMaxGrid.displayTitle = 1; 
      CertifyMaxGrid.unlocked = 1;
      CertifyMaxGrid.hiddenPlus = 0; 
      CertifyMaxGrid.hiddenSubtraction =0;  
      CertifyMaxGrid.loadMulLine(iArray);
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
