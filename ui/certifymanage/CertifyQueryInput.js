
// 界面初始化
var turnPage = new turnPageClass();
var detailTurnPage = new turnPageClass();

// 初始化
function init(){
	fm.ComCode.value = fm.ManageCom.value;
	fm.ComCodeName.value = fm.ManageComName.value;
}

function queryInventory(){
	
	var query = "select " 
		+ " certifycode,(select CertifyName from LMCardDescription where CertifyCode=ci.certifycode),sum(sumcount),comcode " 
		+ " from LZCertifyInventory ci " 
		+ " where comcode like '" + fm.ManageCom.value + "%'"
		+ " and comcode = '" + fm.ComCode.value + "'"
		+ getWherePart("certifycode","CertifyCode","like")
		+ " group by certifycode,comcode";
	turnPage.queryModal(query, InventoryGrid);
	
	CertifyListGrid.clearData();
}

// 点击查询明细
function inventoryClick(){

	var selRowNo = InventoryGrid.getSelNo() - 1;
	var certify = InventoryGrid.getRowColDataByName(selRowNo,"CertifyCode");
	var comcode = InventoryGrid.getRowColDataByName(selRowNo,"ComCode");
	
	var query = "select " 
		+ " certifycode,(select CertifyName from LMCardDescription where CertifyCode=ci.certifycode),startno,endno,sumcount " 
		+ " from LZCertifyInventory ci " 
		+ " where comcode like '" + fm.ManageCom.value + "%'"
		+ " and comcode = '" + comcode + "'"
		+ " and certifycode='" + certify + "'";
	detailTurnPage.queryModal(query, CertifyListGrid);
}

// 公用为空校验
function isNull(str){
	if(str == null || str.trim() == "" || str == "null"){
		return true;
	}
	return false;
}
