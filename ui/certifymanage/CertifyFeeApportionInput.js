
// 界面初始化
var turnPage = new turnPageClass();
var comTurnPage = new turnPageClass(); 
var detailTurnPage = new turnPageClass(); 


function init(){
	query();
}

// 下载
function download(){

	var selNo = CertifyBatchGrid.getSelNo()
	
	if(selNo <= 0){
		alert("请点选待征订批次！");
		return false;
	}
	
	var batchNo = CertifyBatchGrid.getRowColData(selNo - 1 , 1);
	
	fm.action = "CertifyFeeDetailSave.jsp?BatchNo=" + batchNo;
	submitData();
}

// 批次信息查询
function query(){
	var query = "select batchno,startdate,case temptypes when '00' then '全国版' else '地方版' end,"
		+ "case when exists (select 1 from LZPrintList where batchno=cbs.batchno and dealstate='01') then '尚未完成入库'when exists (select 1 from LZPrintList where batchno=cbs.batchno and dealstate='02') then '入库完成'  else '其他' end,"
		+ "case when exists (select 1 from LZSendOutList where batchno=cbs.batchno and dealstate='01') then '尚未完成出库' when exists (select 1 from LZSendOutList where batchno=cbs.batchno and dealstate='02') then '出库完成' else '其他' end,'征订结束', "
		
		+ "case when exists (Select 1 From Lzcertifyinventorytrace Cit Inner Join Lzsendoutlist Sol On Sol.Certifycode = Cit.Certifycode And Sol.Startno = Cit.Startno And Sol.Endno = Cit.Endno Where  Cit.Operatingtype = '01' and  Sol.batchno= cbs.batchno ) then '已分摊' else '未分摊' end  "
		//+ "case when exists (select ComCode,(select name from ldcom where comcode=sol.comcode),sum(TotalPrice) from LZSendOutList sol where batchno= cbs.batchno group by comcode order by comcode  ) then '已分摊' else '未分摊' end  "
		+ "from LZcertifyBatchState cbs "
		+ "where batchstate='02'"
		+ getWherePart("BatchNo","BatchNo")
		+ getWherePart("StartDate","StartDate")
		+ " order by startdate desc"
	turnPage.queryModal(query, CertifyBatchGrid);
}

// 公用为空校验
function isNull(str){
	if(str == null || str.trim() == "" || str == "null"){
		return true;
	}
	return false;
}

// 提交
function submitData(){
	fm.submit();
}

// 返回
function afterSubmit(FlagStr, Content, Type){
}