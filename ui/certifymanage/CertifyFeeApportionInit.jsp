<%
//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
    	fm.all('ManageCom').value = '<%=strManageCom%>';
    	
    	initCertifyBatchGrid();
        init();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

function initCertifyBatchGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="征订批次号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="征订日期";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="全国/地方版";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="入库情况";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="出库情况";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="征订状态";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="分摊状态";
        iArray[7][1]="50px";
        iArray[7][2]=100;
        iArray[7][3]=0;

        CertifyBatchGrid = new MulLineEnter("fm", "CertifyBatchGrid");

        CertifyBatchGrid.mulLineCount = 0;   
        CertifyBatchGrid.displayTitle = 1;
        CertifyBatchGrid.canSel = 1;
        CertifyBatchGrid.hiddenSubtraction = 1;
        CertifyBatchGrid.hiddenPlus = 1;
        CertifyBatchGrid.canChk = 0;
        CertifyBatchGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}

</script>

