<%@page contentType="text/html;charset=GBK" %>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certifymanage.InventoryIncomeBL"%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%
String FlagStr = "Fail";
String Content = "";

String tBatchNo = request.getParameter("BatchNo");
String tDealType = request.getParameter("DealType");

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{    
    VData tVData = new VData();
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("BatchNo", tBatchNo);
    
    String tChk[] = request.getParameterValues("InpCertifyListGridChk");
  	String tSerialno[] = request.getParameterValues("CertifyListGrid9");

	LZPrintListSet tLZPrintListSet = new LZPrintListSet();

	if(tChk != null && tSerialno != null){
		for(int i = 0; i < tChk.length; i++){
			if(tChk[i]!=null && tChk[i].equals("1")){
				LZPrintListSchema tLZPrintListSchema = new LZPrintListSchema();
				tLZPrintListSchema.setSerialNo(tSerialno[i]);
			
				tLZPrintListSet.add(tLZPrintListSchema);
			}
		}
	}
	
    
    tVData.add(tTransferData);
    tVData.add(tGI);
    tVData.add(tLZPrintListSet);
    
    InventoryIncomeBL tInventoryIncomeBL = new InventoryIncomeBL();
    if(!tInventoryIncomeBL.submitData(tVData, tDealType))
    {
    	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
        System.out.println(tInventoryIncomeBL.getErrors().getFirstError());
        Content = "入库失败，原因是: " + tInventoryIncomeBL.getErrors().getFirstError() + "_" + time;
        FlagStr = "Fail";
    }
    else
    {
        Content = "处理成功！";
        FlagStr = "Succ";
    }
}
catch (Exception ex)
{
	String time = PubFun.getCurrentDate2() + PubFun.getCurrentTime2();
    Content = " 入库失败：" + ex.getMessage() + "_" +  time;
    FlagStr = "Fail";
    ex.printStackTrace();
}

%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>" , "Income");
</script>
</html>
