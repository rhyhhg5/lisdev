<%
//程序名称：CertifySendOutInit.jsp
//程序功能：发放管理
//创建日期：2002-08-07
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strOperator = globalInput.Operator;
	String strCurTime = PubFun.getCurrentDate();
	String strCom = globalInput.ComCode;
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）

function initForm()
{
  try
  { 
  	fm.all('ComCode').value = '<%=strCom%>';
  	initCertifyBatchGrid();
    initCertifyList();
    initCertifyStoreList();
    if(!init())
	{
    	return false;
  	}
    queryBatchInfo();
    
  }
  catch(re)
  {
    alert("CertifySendOutInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCertifyBatchGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="征订批次号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="征订日期";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="征订结束日期";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="全国/地方版";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="征订状态";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="征订机构";
        iArray[6][1]="0px";
        iArray[6][2]=100;
        iArray[6][3]=3;

        CertifyBatchGrid = new MulLineEnter("fm", "CertifyBatchGrid");

        CertifyBatchGrid.mulLineCount = 0;   
        CertifyBatchGrid.displayTitle = 1;
        CertifyBatchGrid.canSel = 1;
        CertifyBatchGrid.hiddenSubtraction = 1;
        CertifyBatchGrid.hiddenPlus = 1;
        CertifyBatchGrid.canChk = 0;
        CertifyBatchGrid.loadMulLine(iArray);
        CertifyBatchGrid.selBoxEventFuncName = "batchClick";
    }
    catch(ex)
    {
        alert("初始化CertifyListGrid时出错：" + ex);
    }
}

// 起始单号、终止单号信息列表的初始化
function initCertifyList()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="征订批次号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="0";        		  //列宽
      iArray[1][2]=180;          			//列最大值
      iArray[1][3]=1;              		//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[2]=new Array();
      iArray[2][0]="单证编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="80";        			//列宽
      iArray[2][2]=80;          			//列最大值
      iArray[2][3]=1;
      iArray[2][3]=2;              		//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="newcertifycode";     //是否引用代码:null||""为不引用
      iArray[2][5]="2|3";             //引用代码对应第几列，'|'为分割符
      iArray[2][6]="0|1";             //上面的列中放置引用代码中第几位值
      iArray[2][9]="单证编码|newcertifycode&NOTNULL";
      iArray[2][18]=300;
      iArray[2][19]=1;   		//1是需要强制刷新.            		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="单证名称";    	  //列名
      iArray[3][1]="60";            	//列宽
      iArray[3][2]=180;            		//列最大值
      iArray[3][3]=1;              		//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="单价";    	  //列名
      iArray[4][1]="80";            	//列宽
      iArray[4][2]=180;            		//列最大值
      iArray[4][3]=1;              		//是否允许输入,1表示允许，0表示不允许

       
     
      iArray[5]=new Array();
      iArray[5][0]="寄发数量";    	      //列名
      iArray[5][1]="80";            	//列宽
      iArray[5][2]=50;   //列最大值
      iArray[5][3]=1;              		//是否允许输入,1表示允许，0表示不允许

      
      iArray[6]=new Array();
      iArray[6][0]="寄发机构编码";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[6][1]="80";        			//列宽
      iArray[6][2]=80;          			//列最大值
      iArray[6][3]=2;              		//是否允许输入,1表示允许，0表示不允许
      iArray[6][4]="sendcomcode";     //是否引用代码:null||""为不引用
      iArray[6][5]="6|7";             //引用代码对应第几列，'|'为分割符
      iArray[6][6]="0|1";             //上面的列中放置引用代码中第几位值
      iArray[6][9]="机构代码|sendcomcode&NOTNULL";
      iArray[6][18]=300;
      iArray[6][19]=1;   		//1是需要强制刷新.
      
	  iArray[7]=new Array();
      iArray[7][0]="寄发机构代码";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[7][1]="100";            	//列宽
      iArray[7][2]=50;            		//列最大值
      iArray[7][3]=1;              		//是否允许输入,1表示允许，0表示不允许  
      
      
      iArray[8]=new Array();
      iArray[8][0]="印刷厂编码";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[8][1]="80";            	//列宽
      iArray[8][2]=50;            		//列最大值
      iArray[8][3]=2;
      iArray[8][4]="printcode";     //是否引用代码:null||""为不引用
      iArray[8][5]="8|9";             //引用代码对应第几列，'|'为分割符
      iArray[8][6]="0|1";             //上面的列中放置引用代码中第几位值
      iArray[8][9]="机构代码|printcode&NOTNULL";
      iArray[8][18]=300;
      iArray[8][19]=1;   		//1是需要强制刷新.             		//是否允许输入,1表示允许，0表示不允许 
      
      iArray[9]=new Array();
      iArray[9][0]="印刷厂名称";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[9][1]="80";            	//列宽
      iArray[9][2]=50;            		//列最大值
      iArray[9][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      
      iArray[10]=new Array();
      iArray[10][0]="寄发起始号";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[10][1]="80";            	//列宽
      iArray[10][2]=50;            		//列最大值
      iArray[10][3]=1;              		//是否允许输入,1表示允许，0表示不允许 
      
      iArray[11]=new Array();
      iArray[11][0]="寄发终止号";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[11][1]="80";            	//列宽
      iArray[11][2]=50;            		//列最大值
      iArray[11][3]=1;
      iArray[11][7] = "checkSum";              		//是否允许输入,1表示允许，0表示不允许  
      iArray[11][21] = "endNo"; 
       
       
      
      
      
      CertifyList = new MulLineEnter( "fm" , "CertifyList" );
      //这些属性必须在loadMulLine前
      CertifyList.displayTitle = 1;
      CertifyList.mulLineCount = 0;   
      CertifyList.hiddenPlus = 0;        
      CertifyList.hiddenSubtraction = 0; 
      CertifyList.canChk = 1;     
      CertifyList.loadMulLine(iArray);
      
      //CertifyList.delBlankLine("CertifyList");

    } catch(ex) {
      alert(ex);
    }
}

function initCertifyStoreList()
{
    var iArray = new Array();

    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="50";        			//列宽
      iArray[0][2]=50;          			//列最大值
      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="流水号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="0";        		  //列宽
      iArray[1][2]=180;          			//列最大值
      iArray[1][3]=1;              		//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[2]=new Array();
      iArray[2][0]="征订批次号";     		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="0";        			//列宽
      iArray[2][2]=80;          			//列最大值
      iArray[2][3]=1;              		//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="单证编码";    	  //列名
      iArray[3][1]="60";            	//列宽
      iArray[3][2]=180;            		//列最大值
      iArray[3][3]=1;              		//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="单证名称";    	  //列名
      iArray[4][1]="80";            	//列宽
      iArray[4][2]=180;            		//列最大值
      iArray[4][3]=1;              		//是否允许输入,1表示允许，0表示不允许

       
     
      iArray[5]=new Array();
      iArray[5][0]="单价";    	      //列名
      iArray[5][1]="60";            	//列宽
      iArray[5][2]=50;   //列最大值
      iArray[5][3]=1;              		//是否允许输入,1表示允许，0表示不允许

      
      iArray[6]=new Array();
      iArray[6][0]="总价";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[6][1]="60";            	//列宽
      iArray[6][2]=50;            		//列最大值
      iArray[6][3]=1;              		//是否允许输入,1表示允许，0表示不允许  

	  iArray[7]=new Array();
      iArray[7][0]="寄发数量";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[7][1]="80";            	//列宽
      iArray[7][2]=50;            		//列最大值
      iArray[7][3]=1;              		//是否允许输入,1表示允许，0表示不允许  
       
      
      
      iArray[8]=new Array();
      iArray[8][0]="寄发机构代码";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[8][1]="80";            	//列宽
      iArray[8][2]=50;            		//列最大值
      iArray[8][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      
      
      iArray[9]=new Array();
      iArray[9][0]="寄发机构名称";     	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[9][1]="80";            	//列宽
      iArray[9][2]=50;            		//列最大值
      iArray[9][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      
      iArray[10]=new Array();
      iArray[10][0]="印刷厂编码";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[10][1]="80";            	//列宽
      iArray[10][2]=50;            		//列最大值
      iArray[10][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      
      iArray[11]=new Array();
      iArray[11][0]="印刷厂名称";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[11][1]="80";            	//列宽
      iArray[11][2]=50;            		//列最大值
      iArray[11][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      
      iArray[12]=new Array();
      iArray[12][0]="寄发起号";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[12][1]="80";            	//列宽
      iArray[12][2]=50;            		//列最大值
      iArray[12][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      
      iArray[13]=new Array();
      iArray[13][0]="寄发止号";    	    //列名 为了防止直接点“发放单证”时将mulLine清空
      iArray[13][1]="80";            	//列宽
      iArray[13][2]=50;            		//列最大值
      iArray[13][3]=0;              		//是否允许输入,1表示允许，0表示不允许  
      


      CertifyStoreList = new MulLineEnter( "fm" , "CertifyStoreList" );
      //这些属性必须在loadMulLine前
      CertifyStoreList.displayTitle = 1;
      CertifyStoreList.mulLineCount = 0;   
      CertifyStoreList.hiddenPlus = 1;        
      CertifyStoreList.hiddenSubtraction = 1; 
      CertifyStoreList.canChk = 1; 
      CertifyStoreList.unlocked=0;   
      CertifyStoreList.loadMulLine(iArray);
      
      //CertifyList.delBlankLine("CertifyList");

    } catch(ex) {
      alert(ex);
    }
}

</script>