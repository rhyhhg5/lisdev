
<%
//程序名称：BatchPaySave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.certifymanage.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.finfee.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK"%>
<%

   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
   
   
   GlobalInput tGI = (GlobalInput)session.getValue("GI");
   String mComCode = tGI.ComCode;
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {    	
	String mOperate =request.getParameter("OperateType");
	String sBatchNo =request.getParameter("BatchNo");
	
	System.out.println("mOperate--------:" + mOperate);
	
	
	
	LZSendOutListSet  tLZSendOutListSet= new LZSendOutListSet();
	   
	if ("INSERT".equals(mOperate))
	{   
		String tChk[]=request.getParameterValues("InpCertifyListChk");
		String tBatchNo[] = request.getParameterValues("CertifyList1");
  		String tCertifyCode[] = request.getParameterValues("CertifyList2");
  		String tPrice[] = request.getParameterValues("CertifyList4");
  		String tSumCount[] = request.getParameterValues("CertifyList5");
  		String tComCode[] = request.getParameterValues("CertifyList6");
  		String tPrintCode[] = request.getParameterValues("CertifyList8");
  		String tStartNo[] = request.getParameterValues("CertifyList10");
  		String tEndNo[] = request.getParameterValues("CertifyList11");
  		System.out.println(tChk.length);         

  		
       for (int i = 0; i < tChk.length; i++) {   
			if (tChk[i]!= null &&tChk[i].equals("1")) {
				LZSendOutListSchema  tLZSendOutListSchema  = new  LZSendOutListSchema();
                 
				tLZSendOutListSchema.setBatchNo(tBatchNo[i].trim());
				tLZSendOutListSchema.setCertifyCode(tCertifyCode[i].trim()); 
				tLZSendOutListSchema.setPrice(tPrice[i].trim());
				tLZSendOutListSchema.setSumCount(tSumCount[i].trim());
				tLZSendOutListSchema.setComCode(tComCode[i].trim());
				tLZSendOutListSchema.setStartNo(tStartNo[i].trim());
				tLZSendOutListSchema.setEndNo(tEndNo[i].trim());
				tLZSendOutListSchema.setPrintCode(tPrintCode[i].trim());
				
				System.out.println("第"+i+"个单证号码" + tCertifyCode[i].trim()+"印刷厂" +tPrice[i].trim()+"起始号"+tStartNo[i].trim()+"终止号"+tEndNo[i].trim()+"数量"+tSumCount[i].trim());
				
				tLZSendOutListSet.add(tLZSendOutListSchema);			 
			}
		}
		
	}
    if ("DELETE".equals(mOperate)){
    	String mChk[]=request.getParameterValues("InpCertifyStoreListChk");
    	String mSerialNo[] = request.getParameterValues("CertifyStoreList1");
    	String mBatchNo[] = request.getParameterValues("CertifyStoreList2");  
    	String mCertifyCode[] = request.getParameterValues("CertifyStoreList3");  
    	
    	System.out.println("长度》》》》》》》》》》》》》》"+mChk.length);    
       for (int i = 0; i < mChk.length; i++) {
			if (mChk[i]!= null &&mChk[i].equals("1")) {
			
			LZSendOutListSchema  zLZSendOutListSchema  = new  LZSendOutListSchema();
			    
			    zLZSendOutListSchema.setSerialNo(mSerialNo[i].trim());
			    zLZSendOutListSchema.setBatchNo(mBatchNo[i].trim());
				zLZSendOutListSchema.setCertifyCode(mCertifyCode[i].trim()); 
				System.out.println("第"+i+"个流水号" + mSerialNo[i].trim()+" 单证号为"+ mCertifyCode[i].trim());
				
				tLZSendOutListSet.add(zLZSendOutListSchema);
				
			}
		}  
	}
	if ("UPDATE".equals(mOperate)){
	            
	    LZSendOutListSchema  mLZSendOutListSchema  = new  LZSendOutListSchema();
		mLZSendOutListSchema.setBatchNo(sBatchNo);
		tLZSendOutListSet.add(mLZSendOutListSchema);
	}
	
   	VData aVData = new VData();
    aVData.add(tGI);
    aVData.add(tLZSendOutListSet);
    
    CertifySendListUI tCertifySendListUI = new CertifySendListUI();
    System.out.println("I come in the FinBankAddUI ");
      if (!tCertifySendListUI.submitData(aVData, mOperate)) 
      {  
          Content = "操作失败,原因是:"+ tCertifySendListUI.mErrors.getFirstError();
          System.out.println("添加失败");
          FlagStr = "Fail";    
      }
      else
      {
       Content = "操作成功";  

      }
  System.out.println("FlagStr : "+FlagStr);
  System.out.println("Content : "+Content);
%>
<html>
	<script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mOperate%>");
</script>
</html>
<%
}
%>
