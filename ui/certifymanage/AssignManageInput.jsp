<%@ page contentType="text/html;charset=GBK"%>

<html>
	<head>
		<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
		<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
		<script src="../common/javascript/Common.js"></script>
		<script src="../common/cvar/CCodeOperate.js"></script>
		<script src="../common/javascript/MulLine.js"></script>
		<script src="../common/Calendar/Calendar.js"></script>
		<script src="../common/javascript/VerifyInput.js"></script>

		<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

		<script src="AssignManageInput.js"></script>
		<%@include file="AssignManageInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="" method="post" name="fm" target="fraSubmit">
			<div id="divSendCom" style="display:''">
				<table>
					<tr>
						<td class="common">
							<img src="../common/images/butExpand.gif" style="cursor:hand;" />
						</td>
						<td class="titleImg">
							机构发放信息
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td class="title">
							发放机构
						</td>
						<td class="input">
							<input class="codeNo" name="CSendCom" ondblclick="return showCodeList('certifymanagesend',[this,CSendComName],[0,1]);" onkeyup="return showCodeListKey('certifymanagesend',[this,CSendComName],[0,1]);"/><input class="codename" name="CSendComName" readonly="readonly" elementtype="nacessary" />
						</td>
						<td class="title">
							接收机构
						</td>
						<td class="input">
							<input class="codeNo" name="CReceiveCom" ondblclick="return showCodeList('certifymanagesend',[this,CReceiveComName],[0,1]);" onkeyup="return showCodeListKey('certifymanagesend',[this,CReceiveComName],[0,1]);"/><input class="codename" name="CReceiveComName" readonly="readonly" elementtype="nacessary" />
						</td>
					</tr>
				</table>
				<br>
			</div>
			
			<table>
				<tr>
					<td class="common">
						<img src="../common/images/butExpand.gif" style="cursor:hand;"
							onclick="showPage(this, divInventoryGrid)" />
					</td>
					<td class="titleImg">
						库存信息
					</td>
				</tr>
			</table>
			<div id="divInventoryGrid" style="display: ''">
				<table class="common">
					<tr class="common">
						<td class="title">
							单证编码
						</td>
						<td class="input">
							<input class="common" name="queryCertify"/>
						</td>
						<td class="title">
							单证名称
						</td>
						<td class="input">
							<input class="common" name="queryCertifyName"/>
						</td>
					</tr>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="查  询" onclick="queryInventory();" />
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanInventoryGrid"></span>
						</td>
					</tr>
				</table>
				<div align="center">
					<input type="button" class="cssButton" value="首  页" onclick="turnPage.firstPage();" />
					<input type="button" class="cssButton" value="上一页" onclick="turnPage.previousPage();" />
					<input type="button" class="cssButton" value="下一页" onclick="turnPage.nextPage();" />
					<input type="button" class="cssButton" value="尾  页" onclick="turnPage.lastPage();" />
				</div>
				<br>
				<hr>
			</div>
			
			<div id="divCertifyListGrid" style="display: ''">
				<table>
					<tr>
						<td class="common">
							<img src="../common/images/butExpand.gif" style="cursor:hand;"/>
						</td>
						<td class="titleImg">
							单证列表
						</td>
					</tr>
				</table>
				<table class="common">
					<tr class="common">
						<td>
							<span id="spanCertifyListGrid"></span>
						</td>
					</tr>
				</table>
				<table>
					<tr>
						<td class="common">
							<input class="cssButton" type="button" value="调  拨" onclick="sendCom();" />
						</td>
					</tr>
				</table>
			</div>
			
			<input type=hidden name=ManageCom>
			<input type=hidden name=ManageComName>
			<input type=hidden name=DealType>
			
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>
