
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
<%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
 	String CurrentDate= PubFun.getCurrentDate();   
            	               	
 %>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('PrintCode').value = '';
    fm.all('PrintName').value = '';   
    fm.all('EffectiveState').value = '';
    fm.all('LinkMan').value = '';     
    fm.all('Phone').value = '';
    fm.all('PostalAddress').value = '';   
    fm.all('Remark').value = '';
  	fm.all('ComCode').value = '<%=Comcode%>';    
    
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}
;



function initForm()
{
  try
  {
    initInpBox();
    initCardRiskGrid();
    
    if(!init())
    {
    	return false ;
    }
  }
  catch(re)
  {
    alert("3CertifyDescInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCardRiskGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="印刷厂编码";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=1;

    iArray[2]=new Array();
    iArray[2][0]="印刷厂名称";         			//列名
    iArray[2][1]="200px";            	//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许  
    iArray[2][21]="Prem";

    iArray[3]=new Array();
    iArray[3][0]="印刷厂状态代码";         	//列名
    iArray[3][1]="0px";            	//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=0;  
    
    iArray[4]=new Array();
    iArray[4][0]="印刷厂状态";         	//列名
    iArray[4][1]="100px";            	//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
                			//是否允许输入,1表示允许，0表示不允许

    
    iArray[5]=new Array();
    iArray[5][0]="备注";         	//列名
    iArray[5][1]="100px";            	//列宽
    iArray[5][2]=60;            			//列最大值
    iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

	iArray[6]=new Array();
    iArray[6][0]="联系人";         	//列名
    iArray[6][1]="100px";           //列宽
    iArray[6][2]=60;            	//列最大值
    iArray[6][3]=0; 				//
    
    iArray[7]=new Array();
    iArray[7][0]="联系电话";         	//列名
    iArray[7][1]="100px";            	//列宽
    iArray[7][2]=60;            		//列最大值
    iArray[7][3]=0;						//
  
  	iArray[8]=new Array();
    iArray[8][0]="联系地址";         	//列名
    iArray[8][1]="100px";            	//列宽
    iArray[8][2]=60;            		//列最大值
    iArray[8][3]=0;						//
  
    CardRiskGrid = new MulLineEnter("fm", "CardRiskGrid");
    CardRiskGrid.mulLineCount = 0;
    CardRiskGrid.displayTitle = 1;
    CardRiskGrid.hiddenPlus = 1;
    CardRiskGrid.hiddenSubtraction = 1;
    CardRiskGrid.canSel = 1;
    CardRiskGrid.canChk = 0;
    CardRiskGrid.loadMulLine(iArray);
    CardRiskGrid.selBoxEventFuncName = "ShowDetail";
  }
  catch(ex)
  {
    alert("初始化时出错2003-05-21"+ex);
  }
}

function getComName(comC)
{
	var strSQL="select name from LDCom where comcode= '"+ comC +"' ";
	var comN = easyExecSql(strSQL);	
	
	if (comN)
	{	
		fm.all("ManageComName").value= comN[0][0];
		
	}
}


</script>


