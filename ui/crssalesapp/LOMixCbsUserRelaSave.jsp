<!-- 
<!--%@include file="../common/jsp/UsrCheck.jsp"%>
 -->
<%
//程序名称：LOMixCbsUserRelaSave.jsp
//程序功能：
//创建日期：2005-11-01 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>
  <%@page import="com.sinosoft.lis.crssale.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
System.out.println("into ... ...");
  //接收信息，并作校验处理。
  //输入参数
  LOMCommissionSchema tLOMCommissionSchema   = new LOMCommissionSchema();
  LOMixCbsUserRelaSchema tLOMixCbsUserRelaSchema   = new LOMixCbsUserRelaSchema();
  LOMixCbsUserRelaUI tLOMixCbsUserRelaUI = new LOMixCbsUserRelaUI();

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRearStr = "";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";



  System.out.println("begin LOMCommission schema...");
  //取得代理人信息(增加加密信息1111)
    //LisIDEA tLisIdea = new LisIDEA();
  tLOMCommissionSchema.setoperatorcode(request.getParameter("operatorcode"));
  tLOMCommissionSchema.setcomp_cod(request.getParameter("comp_cod"));
  tLOMCommissionSchema.setcomp_nam(request.getParameter("comp_nam"));
  tLOMCommissionSchema.setman_org_cod(request.getParameter("man_org_cod"));
  tLOMCommissionSchema.setman_org_nam(request.getParameter("man_org_nam"));
  //System.out.println("UserType"+request.getParameter("UserType"));
  tLOMCommissionSchema.setsales_nam(request.getParameter("sales_nam"));
  //System.out.println("AgentId"+request.getParameter("AgentId"));
  tLOMCommissionSchema.setdate_birthd(request.getParameter("date_birthd"));
  //tLOMCommissionSchema.setsex_cod(request.getParameter("sex_cod"));
  //tLOMCommissionSchema.setidtyp_cod(request.getParameter("idtyp_cod"));
  //tLOMCommissionSchema.setpaperworkid(request.getParameter("paperworkid"));
   
  tLOMCommissionSchema.settelephone(request.getParameter("telephone"));
  tLOMCommissionSchema.setsales_mob(request.getParameter("sales_mob"));
  tLOMCommissionSchema.setsales_mail(request.getParameter("sales_mail"));
  tLOMCommissionSchema.setstatus_cod(request.getParameter("status_cod"));
  tLOMCommissionSchema.setstatus(request.getParameter("status"));
  
  tLOMCommissionSchema.setfunction(request.getParameter("function"));
  tLOMCommissionSchema.setentrant_date(request.getParameter("entrant_date"));
  tLOMCommissionSchema.setdimission_date(request.getParameter("dimission_date"));
  tLOMCommissionSchema.setdata_upd_typ(request.getParameter("data_upd_typ"));
  tLOMCommissionSchema.setprt_entrust_orgid(request.getParameter("prt_entrust_orgid"));
  tLOMCommissionSchema.setprt_entrust_orgname(request.getParameter("prt_entrust_orgname"));
  tLOMCommissionSchema.setli_entrust_orgid(request.getParameter("li_entrust_orgid"));
  tLOMCommissionSchema.setli_entrust_orgname(request.getParameter("li_entrust_orgname"));
  tLOMCommissionSchema.sethi_entrust_orgid(request.getParameter("hi_entrust_orgid"));
  tLOMCommissionSchema.sethi_entrust_orgname(request.getParameter("hi_entrust_orgname"));
  tLOMCommissionSchema.setprt_check_date(request.getParameter("prt_check_date"));
  tLOMCommissionSchema.setli_check_date(request.getParameter("li_check_date"));
  tLOMCommissionSchema.sethi_check_date(request.getParameter("hi_check_date"));
  //System.out.print(request.getParameter("RuleState"));

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.addElement(tLOMCommissionSchema);

System.out.println("tOperate:"+tOperate+tVData);
  try
  {
    System.out.println("this will save the data!!!");
    tLOMixCbsUserRelaUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLOMixCbsUserRelaUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理
System.out.println("out ... ...");
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
