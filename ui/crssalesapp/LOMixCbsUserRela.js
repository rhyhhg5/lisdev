//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if(mOperate=="")
	{
		addClick();
	}
	//alert(mOperate);
  //if (!beforeSubmit())
    //return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	

	mOperate="";
	//alert(mOperate);
 //initForm();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    //alert(content);
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

 //   showDiv(operateButton,"true"); 
 //   showDiv(inputButton,"false"); 
    //执行下一步操作
  }  
}

function beforeSubmit()
{
  //if (!verifyInput()) return false;
 // alert(12);
  //var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value))
  // alert(13);
//  if (strChkIdNo != "")
//  {
//    alert(strChkIdNo);
//    return false;           xianchun  2005-12-19 不需要录入个人信息
//  }
   // alert(14);xianchun
 //   alert(fm.all('PasswordConfirm').value);

  return true;
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击保存图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("operatorcode").value==null)||(trim(fm.all("operatorcode").value)=='' 
       || fm.all("comp_cod").value==null)||(trim(fm.all("comp_cod").value)==''
       || fm.all("man_org_cod").value==null)||(trim(fm.all("man_org_cod").value)==''))
    alert("请确定要修改的人员！");
  else
  { 
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
   //   alert(1);
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LOMixCbsUserRelaQueryHtml.jsp");
} 

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("operatorcode").value==null)||(trim(fm.all("operatorcode").value)=='' 
       || fm.all("comp_cod").value==null)||(trim(fm.all("comp_cod").value)==''
       || fm.all("man_org_cod").value==null)||(trim(fm.all("man_org_cod").value)==''))
    alert("请确定要删除的记录！");
  else
  {
    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";  
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

  function changeIDNo()
  {
     if (getWherePart('IDNo')=='')
     {
       return false;
}
     var strSQL = "";
     strSQL = "select * from lacompersonal where 1=1 "
 + getWherePart('IDNo');
       //alert(strSQL);
     var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
     if (strQueryResult)
     {
      alert('该身份证号已存在!');
      fm.all('IDNo').value = '';
      return false;
     }
     return true;
  }

function afterQuery(arrQueryResult)
{	
	//alert(1);
      var arrResult = new Array();

      resetForm();
      if( arrQueryResult != null )
      {

      arrResult = arrQueryResult;
      fm.all('AgentCode').value = arrResult[0][0];
      fm.all('Name').value = arrResult[0][1];
      fm.all('AgentCom').value = arrResult[0][2];
      fm.all('AgentComName').value = arrResult[0][3];
      fm.all('IDNo').value = arrResult[0][4];
    
      
      //fm.all('Password').value = arrResult[0][5];
      fm.all('LoginName').value = arrResult[0][6];
      fm.all('Sex').value = arrResult[0][7];
      fm.all('HeadShip').value = arrResult[0][8];  
      fm.all('Birthday').value = arrResult[0][9];
      fm.all('HomeAddress').value = arrResult[0][10];
      fm.all('Phone').value = arrResult[0][11];
   
      fm.all('EmployDate').value = arrResult[0][12];
      fm.all('OutWorkDate').value = arrResult[0][13];
      fm.all('Operator').value = arrResult[0][14];
      fm.all('Noti').value = arrResult[0][15];
      fm.all('Mobile').value = arrResult[0][16];
      fm.all('ManageCom').value = arrResult[0][17];
      document.getElementById('LoginName').readOnly=true;
      document.getElementById('LoginName').className='readonly';
      showOneCodeNametoAfter('sex','Sex','SexName'); 
      
   	}
}
      
  //function afterQueryCom(arrQueryResult)
  //{	
  	//alert(1);
        //var arrResult = new Array();

       
        //if( arrQueryResult != null )
        //{
        //arrResult = arrQueryResult;
        //fm.all('AgentCom').value = arrResult[0][0];
        //fm.all('AgentComName').value = arrResult[0][2];
     	//}
  //}