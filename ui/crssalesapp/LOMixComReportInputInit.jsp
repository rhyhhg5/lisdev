<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initAgentComGrid();
	divPage.style.display="";                 
    fm.all('ManageCom').value = ManageCom;    
}
var AgentComGrid;
function initAgentComGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	//列名
    iArray[0][1]="30px";         	//列名    
    iArray[0][3]=0;         		//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="销售机构编码";         	 //列名
    iArray[1][1]="50px";            //列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="销售机构名称";         	
    iArray[2][1]="60px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="反馈信息";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="生成时间";      //列名
    iArray[4][1]="120px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
     
    AgentComGrid = new MulLineEnter("fm", "AgentComGrid"); 
    //设置Grid属性
    AgentComGrid.mulLineCount = 0;
    AgentComGrid.displayTitle = 1;
    AgentComGrid.locked = 1;
    AgentComGrid.canSel = 0;
    AgentComGrid.canChk = 0;
    AgentComGrid.hiddenSubtraction = 1;
    AgentComGrid.hiddenPlus = 1;
 	AgentComGrid.selBoxEventFuncName = "";
    AgentComGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
                                      

</script>
