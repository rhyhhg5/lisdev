//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
//提交，确认按钮对应操作

function submitForm()
{
  if(!beforeSubmit()){
  	return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLCasePrescriptionSave.jsp";
 	//fm.target='_blank';
  fm.submit(); //提交
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();  
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
   	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
 	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 }

}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(re)
  {
  	alert("在CaseReceipt.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  var MaxNum = LLCasePrescriptionGrid.mulLineCount;
	
  //添加操作  
	//最大用药天数 根据门诊诊断类型依次为3天，7天，14天，30天
  //获取参数值
  var CaseNo = fm.CaseNo.value;//分案号
  if(CaseNo.trim()==""){
  	alert("理赔号为空");
  	return false;
  }
  var DiagnoseType = fm.DiagnoseType.value;	//门诊诊断类型
  var MaxDays = 0;   //最大用药天数
  if(DiagnoseType=="0"){
    MaxDays=3;
  }
  if(DiagnoseType=="1"){
  	MaxDays=7;
  }
  if(DiagnoseType=="2"){
  	MaxDays=14;
  }
  if(DiagnoseType=="3"){
  	MaxDays=30;
  }
  if(MaxDays!=3&&MaxDays!=7&&MaxDays!=14&&MaxDays!=30){
  	alert("请选择门诊诊断类型")
  	return false;
  }
  var DrugName;			//药名
  var DrugQuantity;		//药品剂量
  var TimesPerDay;		//日次数
  var QuaPerTime;		//次用量
  for(var i=0;i<MaxNum;i++){
  	
  	//获取mulline中的参数值
  	DrugName = LLCasePrescriptionGrid.getRowColDataByName(i,"DrugName");	//药名
  	DrugQuantity = LLCasePrescriptionGrid.getRowColDataByName(i,"DrugQuantity");	//药品剂量
    TimesPerDay = LLCasePrescriptionGrid.getRowColDataByName(i,"TimesPerDay");   	//日次数
    QuaPerTime = LLCasePrescriptionGrid.getRowColDataByName(i,"QuaPerTime"); 		//次用量
    
    //验证数据类型
    if(DrugName.trim()==""){
      alert("药名为空");
      return false;
    }
   
    if(DrugQuantity.trim()==""){
      alert("药品剂量为空");
      return false;
    	
    }else if(!isNumeric(DrugQuantity)){
      alert("第"+(i+1)+"行“药品剂量”不为数字，请重新输入！");
      return false;
    }
    
    if(TimesPerDay.trim()==""){
      alert("日次数为空");
      return false;
    }else if(!isNumeric(TimesPerDay)){
      alert("第"+(i+1)+"行“日次数”不为数字，请重新输入！");
      return false;
    }
    
    if(QuaPerTime.trim()==""){
      alert("次用量为空");
      return false;
    }else if(!isNumeric(QuaPerTime)){
      alert("第"+(i+1)+"行“次用量”不为数字，请重新输入！");
      return false;
    }
    
    //校验计算规则：（日次数*次用量*最大用药天数  < 药品剂量 ）时 属于超量
    if((TimesPerDay*QuaPerTime*MaxDays)<DrugQuantity){
    	if(confirm("药品" + DrugName + " 的用药剂量已超过" + fm.DiagnoseTypeName.value + "用药剂量标准，请确认是否继续操作？")){
    		continue;
    	}else{
    		return false;
    	}
    }
    //账单种类为1-门诊时 #2695 
    if(fm.FeeType.value=="1"){
    	//药品名是否在“门诊限制药品”清单中 #2695 
      var strSQL="SELECT P.CodeName"
  	  + " FROM ldcode P WHERE P.CodeType ='RestrictedDrug' and P.CodeName like '" + DrugName + "'";
      var arrResult = easyQueryVer3(strSQL, 1, 0, 1);
	  if(arrResult)
	  { 
	  	if(confirm("药品"+ DrugName +"为门诊限制药品，请确认继续操作？")){
    		continue;
    	}else{
    		return false;
    	}
	  } 
    }
  }
  return true;
	
	
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}



/*function easyQuery()
{
	var strSQL = "select codename('detailstype',a.chargetype),a.chargecode,a.chargename, "
		+" a.chargedetail,a.unitprice,a.amount,a.chargefee,codename('detailsgivetype',a.givetype), "
		+" (select b.receiptno from llfeemain b where b.mainfeeno = a.mainfeeno),a.givetype,a.chargetype "
		+" from LLCaseChargeDetail a where 1=1 and mainfeeno is not null "
        +" and caseno='"+fm.CaseNo.value+"'"
        +" and MainFeeNo='"+fm.MainFeeNo.value+"'";
    arrResult = easyExecSql(strSQL);
    if (arrResult){
   		displayMultiline(arrResult,CaseDrugGrid);
    }
}*/



function getCaseRemark()
{
  var tCaseNo = fm.all('CaseNo').value;
  if(tCaseNo!=null&&tCaseNo!="")
  {
    var sql = "Select Remark from LLCase where caseno = '"+tCaseNo+"'";
    var strQueryResult  = easyQueryVer3(sql, 1, 1, 1);
	if (strQueryResult)
	  {
			var arr = decodeEasyQueryResult(strQueryResult);
			fm.all('Remark').value =arr[0][0];
	  }
  }
}


