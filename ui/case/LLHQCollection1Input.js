//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

var yy = "";
var mm = "";

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
	
	//年份校验
	yy = fm.all('Year').value;//alert(parseInt(yy).length);
	if(yy.length != 4 || parseInt(yy) > 2100 || parseInt(yy) < 2000)
	{
		alert("请输入正确年份");
		return false;
	}
	//月份校验
	mm = fm.all('Month').value;//alert(mm);alert(parseInt(mm));
	if( mm.length != 1 )
	{
		if( mm.length >2 || parseInt(mm) > 12 || parseInt(mm) < 10)
		{
			alert("请输入正确月份");
			return false;
		}
	}
	else
	{
		if(parseInt(mm) > 9 || parseInt(mm) < 1)
		{
			alert("请输入正确月份");
			return false;
		}
	}
	//统计方式校验
	if(fm.all('Choose').value != "1" && fm.all('Choose').value != "2")
	{
		alert("请输入正确统计方式");
		return false;
	}
	
	if(fm.Choose.value=="1")
	{ 
	month=parseInt(fm.Month.value);
	year=fm.Year.value;
	days=getDays(month, year);
	fm.MStartDate.value=year+"-"+month+"-"+"01";
	fm.MEndDate.value=year+"-"+month+"-"+days;
	fm.YStartDate.value=year+"01"+"01";
	fm.YEndDate.value=year+"12"+"31";
	}
  else
  { 
  
//  	month=fm.Month.value;
		year=fm.Year.value;
//		days=getDays(month, year);
		fm.MStartDate.value=year+"-"+"01"+"-"+"01";
		fm.MEndDate.value=year+"-"+"12"+"-"+"31";
		fm.YStartDate.value=year+"01"+"01";
		fm.YEndDate.value=year+"12"+"31";
  }
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "HQCollection1";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function getDays(month, year) 
{
	var daysInMonth = new Array(0,31, 28, 31, 30, 31, 30, 31, 31,30, 31, 30, 31);
	 if (2 == month)
     return ((0 == year % 4) && (0 != (year % 100))) ||(0 == year % 400) ? 29 : 28;
  else
     return daysInMonth[month];
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == "choose") 
	{
		if(Field.value == "2")
		{
			fm.Month.style.display='none';
			fm.Month.value = "";
			fm.Month.verify="";
		}
	  else
	  {
	  		fm.Month.style.display='';
	  		fm.Month.verify="统计月份|notnull&INT";
	  }
	}
}


function getCurrentDate()
{
		d = new Date();
		fm.all('Year').value = d.getYear();
		fm.all('Month').value = d.getMonth();
}

function result()
{
	fm.action="./LLHQCollection1.jsp" ;
	fm.target = "HQCollection1";
	submitForm();
	
}

function efficient()
{
	fm.action="./LLHQCollection2.jsp" ;
	fm.target = "HQCollection2";
	submitForm();
}