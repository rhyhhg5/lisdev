//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var arrDataSet;
var ImportPath;
var ImportState = "no";
window.onbeforeunload = beforeAfterInput;
window.onunload= AfterInput;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm() {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
  initForm();
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
    parent.fraMain.rows = "0,0,50,82,*";
  } else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


function CaseListUpload() {
  if ( ImportState =="Succ") {
    alert("该条款文件已上载，如要更新，请先删除原来的文件") ;
      return false ;
  }
  var i = 0;
  getImportPath();
  ImportFile = fm.all('FileName').value;
  var tprtno = ImportFile;
  if ( tprtno.indexOf("\\")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("\\")+1);
  if ( tprtno.indexOf("/")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("/")+1);
  if ( tprtno.indexOf(".")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("."));
  if(fm.isBasic.checked){
    if(tprtno!=fm.RiskCode.value){
      alert("导入文件名与基本条款编号不一致，请检查输入！");
      return;
    }
    if(fm.RiskName.value==''){
      alert("请录入基本条款名称！");
      return;
    }
    fm.FileType.value='JB';
  }
  else{
    if(tprtno.substring(0,1)!='t'&&tprtno.substring(0,1)!='f'){
      alert("导入文件名不合规则，请仔细阅读注意事项，在进行导入操作！");
      return;
    }
    if(tprtno.substring(0,1)=='t')
      fm.FileType.value='TK';
    if(tprtno.substring(0,1)=='f')
      fm.FileType.value='FL';
    tprtno=tprtno.substring(1);
    tSql = "select riskcode,riskname from lmrisk where riskcode='"+tprtno+"'";
    var arr = easyExecSql(tSql);
    if (arr){
      fm.RiskCode.value = arr[0][0];
      fm.RiskName.value = arr[0][1];
    }
    else{
      alert("请用产品编码作为文件名");
      return false;
    }
  }
      var showStr="正在上载数据……";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.action = "./ImportRiskStatementSave.jsp?ImportPath="+ImportPath+"&RiskCode="+fm.RiskCode.value+"&FileType="+fm.FileType.value+"&RiskName="+fm.RiskName.value;
      fm.submit(); //提交

}

function getImportPath () {

  var strSQL = "";
  strSQL = "select SysvarValue from ldsysvar where sysvar ='ProductPdfPath'";
  var arr = easyExecSql(strSQL);

  ImportPath = arr[0][0];
}

function AfterInput() {
  if ( ImportState=="Importing" ) {
    return false;
  }
}

function beforeAfterInput() {
  if ( ImportState=="Importing" ) {
    alert("磁盘投保尚未完成，请不要离开!");
    return false;
  }
}

function QueryRSList()
{
	var strSql = "select code,codename,case when codealias='1' then '有' else '无' end, "+
	"case when othersign='1' then '有' else '无' end from LDCODE1 WHERE CODETYPE='llriskfile' ";
	turnPage.queryModal(strSql, RiskStatementGrid);
}

function QueryRSUList()
{
	var strSql = "select riskcode,riskname from lmrisk where riskcode not in (select code from LDCODE1 WHERE CODETYPE='llriskfile' )";
	turnPage2.queryModal(strSql, RiskGrid);
}

function showTKPDF(a)
{
	var RiskCode = fm.all(a).all('RiskStatementGrid1').value;
	var flag = fm.all(a).all('RiskStatementGrid3').value;
	if(flag=='有'){
  var PdfURL="../ppdf/t"+RiskCode+".pdf";
 		var newWindow = OpenWindowNew(PdfURL,"审核","left");
 	}
 	else{
 	  alert("该产品的条款尚未导入");
 	}
}

function showFLPDF(a)
{
	var RiskCode = fm.all(a).all('RiskStatementGrid1').value;
	var flag = fm.all(a).all('RiskStatementGrid4').value;
	if(flag=='有'){
  var PdfURL="../ppdf/f"+RiskCode+".pdf";
 		var newWindow = OpenWindowNew(PdfURL,"审核","left");
 	}
 	else{
 	  alert("该产品的费率表尚未导入");
 	}
}

function showRow()
{
  if(fm.isBasic.checked)
    divRiskName.style.display = '';
  else
    divRiskName.style.display = 'none';
}