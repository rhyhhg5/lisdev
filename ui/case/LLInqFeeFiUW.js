var showInfo;
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var tSaveType="";
//提交，保存按钮对应操作
function submitForm(){
	if (confirm("您确实想保存该记录吗?")){
		if(fm.FeeType.value=='1'){
			fm.fmtransact.value = "FI||CONFIRM";
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:360px");
			fm.action = "./LLSurveyFeeSave.jsp";
			fm.submit(); //提交
		}
		else{
			fm.fmtransact.value = "CONFIRM";
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:360px");
			fm.action = "./LLAddSurvFeeSave.jsp";
			fm.submit(); //提交
		}
	}
	else{
		alert("您取消了修改操作！");
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
	showInfo.close();
	if (FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		if(fm.FeeType.value=='1'){
			var selno = SurveyGrid.getSelNo();
			SurveyGrid.setRowColData(selno-1,8,"已审核");
			ShowFeeDetail();
		}
		if(fm.FeeType.value=='2'){
			queryIndFee();
		}
	}
}

function QueryOnKeyDown(){
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13"){
		updateCaseGrid();
		queryIndFee();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm(){
	try{
		initForm();
	}
	catch(re){
		alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//提交前的校验、计算
function beforeSubmit(){
}

function queryClick(){
}

function showDiv(cDiv,cShow){
	if (cShow=="true"){
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

function afterCodeSelect( cCodeName, Field ) {
	try	{
		if(cCodeName=="sfeetype"){
			if(fm.FeeType.value=='1')
			showZJ();
			if(fm.FeeType.value=='2')
			showJJ();
		}
	}
	catch(ex){
		alert(ex.message);
	}
}

function getstr(){
	str="1 and comcode like #"+fm.ComCode.value+"%#";
}

function easyQuery(){
	if(fm.FeeType.value=='1'){
		queryInqFee();
	}
	else{
		queryIndFee();
	}
}

function queryInqFee(){
	strSQL = "SELECT distinct A.otherno,substr(A.surveyno,18,1),B.surveyoperator,"
	+"(select username from lduser c where usercode=B.surveyoperator),B.confer,"
	+"(select username from lduser c where usercode=B.confer),B.mngcom,'"
	+fm.UWTypeName.value+"',B.surveyno "
	+ "FROM LLINQFEE A, LLSURVEY B WHERE 1=1 and A.SURVEYNO=B.SURVEYNO"
	+ getWherePart("B.otherno","CaseNo")
	+ getWherePart("B.surveystartdate","RgtDateS",">=")
	+ getWherePart("B.surveystartdate","RgtDateE","<=")
	+ getWherePart("B.MngCom","ComCode","like")
	+ getWherePart("B.surveyoperator","Replyer")
	+ getWherePart("B.confer","Inspector")
	;
	if(fm.UWType.value=='1'){
		strSQL += " and a.uwstate in ('0','1')"
	}
	if(fm.UWType.value=='2'){
		strSQL += " and a.uwstate in ('2','3')"
	}
	turnPage.queryModal(strSQL, SurveyGrid);
}

function queryIndFee(){
	if(fm.ComCode.value==''){
		alert("请选择要审核间接调查费的机构！")
		return;
	}
	month=fm.Month.value/1;
	year= fm.Year.value/1;
	days=getDays(month, year);
	feedate = year+"-"+month+"-"+days;
	fm.MEndDate.value = feedate;
	strSQL = "select a.otherno,(select count(b.surveyno) from llsurvey b where b.otherno=a.otherno),feesum "
	+ " from llcaseindfee a where feedate='"+feedate+"'"+getWherePart("MngCom","ComCode","like");
	turnPage2.pageLineNum = 100;
	turnPage2.queryModal(strSQL,SurveyCaseGrid);
	feeSql = "select a.codename,a.code,b.feesum,"
	+" (select c.codename from ldcode c where c.codetype='llindfeeuw' and c.code=b.uwstate)"
	+" from llindirectfee b,ldcode a "
	+" where b.feeitem=a.code and a.codetype='llindsurvfee' and b.feedate='"+feedate+"' "
	+getWherePart("b.MngCom","ComCode","like");
	turnPage4.queryModal(feeSql,AddFeeGrid);
	strSql = "select sum(FeeSum),min(FeeSum),operator from LLCaseIndFee where feedate='"
	+fm.MEndDate.value+"'  and Mngcom like '"+fm.ComCode.value+"%%' group by operator";

	arr = easyExecSql(strSql);
	if(arr){
		fm.IndFeeSum.value=arr[0][0];
		fm.Average.value=arr[0][1];
		usersql = "select username from lduser where usercode='"+arr[0][2]+"'"
		urr=easyExecSql(usersql);
		if(urr){
			fm.Handler.value = urr[0][0];
		}
	}
	fm.IndFeeSum.value=fm.IndFeeSum.value=='null'?'':fm.IndFeeSum.value;
	fm.Average.value=fm.Average.value=='null'?'':fm.Average.value;
	fm.Handler.value=fm.Handler.value=='null'?'':fm.Handler.value;
}

function showJJ(){
	divZJ.style.display='none';
	divJJ.style.display='';
}

function showZJ(){
	divZJ.style.display='';
	divJJ.style.display='none';
}


function Dispatch(a){
	if (a=="1"){
		Str="1 and surveyflag=#1# and stateflag=#1# and comcode=#86#";
	}
	else{
		Str="1 and surveyflag =#2# and stateflag=#1# and comcode like substr(#"+fm.ComCode.value+"#,1,4)||#%#";
	}
	Str1="1 and surveyflag=#1# and stateflag=#1# and comcode<>#86#";
}

function ShowFeeDetail(){
	var selno = SurveyGrid.getSelNo();
	fm.SurveyNo.value='';
	if(selno>0)
	fm.SurveyNo.value = SurveyGrid.getRowColData(selno-1,9);

	strSql= "select b.codename,a.FeeItem,a.FeeSum,a.inqdept,"
	+"(select codename from ldcode c where c.codetype='llindfeeuw' and c.code=a.uwstate),"
	+"a.surveyno from llinqfee a,ldcode b where b.codetype='llsurveyfee' and b.code=a.feeitem and a.surveyno='"
	+fm.SurveyNo.value+"'";
	turnPage3.queryModal(strSql,InqFeeDetailGrid);
	strSql="select sum(FeeSum) from llinqfee where surveyno='"+fm.SurveyNo.value+"'";
	var arr=easyExecSql(strSql);
	if(arr){
		fm.SumInqFee.value=arr[0][0]=='null'?'0':arr[0][0];
	}
	strSql += " and uwstate='2'";
	var brr=easyExecSql(strSql);
	if(brr){
		fm.UWInqFee.value=brr[0][0]=='null'?'0':brr[0][0];
	}
}
