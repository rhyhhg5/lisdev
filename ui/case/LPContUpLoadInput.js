var showInfo;
var ImportPath;
//var turnPage = new turnPageClass();
//window.onfocus=myonfocus;

function uploadFile() {
	//获取上传文件的路径，此处为/temp_lp/
	var strSQL = "";
	strSQL = "select SysvarValue from ldsysvar where sysvar ='LPXmlPath'";
	var arr=easyExecSql(strSQL);
	if(arr) {
		ImportPath = arr[0][0];
	}else {
		alert("获取上传路径异常，文件上传失败!");
		return false;
	}
	//上载的文件名
	if(fm.all("FileName").value=="" || fm.all("FileName").value==null) {
		alert("请选择要上载的文件");
		return false;
	}
	var showStr="正在上载数据,请您稍等......";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./LPContListBatchSave.jsp?ImportPath="+ImportPath;
	fm.submit();
}

function afterSubmit(FlagStr,Content) {
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail" ){             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  }else{ 
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  }
}