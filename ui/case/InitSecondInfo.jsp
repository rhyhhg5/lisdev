<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
  * Name    ：SecondShowInfo.jsp
  * Function：理算－审核中显示要进行二次核保的信息
  * Author  ：LiuYansong
  * Date    : 2004-2-27
 */
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String CustomerNo = request.getParameter("InsuredNo");
  String mOperateFlag=request.getParameter("OperateFlag");
  String mOperate="";

  LCPolSet mLCPolSet = new LCPolSet();

  VData tVData = new VData();

  System.out.println("开始执行InitSecondInfo.jsp");
  tVData.addElement(CustomerNo);

	SecondUWUI mSecondUWUI = new SecondUWUI();
	if(mOperateFlag.equals("UW"))
  {
    mOperate = "INIT";
  }
  if(mOperateFlag.equals("END"))
  {
    mOperate = "INITEND";
  }
  if(!mSecondUWUI.submitData(tVData,mOperate))
  {
    FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = mSecondUWUI.getResult();
    mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
    System.out.println("mLCPolSet的个数是"+mLCPolSet.size());
    int m = mLCPolSet.size();
    if(mLCPolSet.size()>0)
    {
      %>
      <script language="javascript">
        parent.fraInterface.SecondUWGrid.clearData();
      </script>
      <%
      for(int i=1;i<=mLCPolSet.size();i++)
      {
      	System.out.println("2004-3-1开始赋值");
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(mLCPolSet.get(i));
        %>
        <script language="javascript">
          parent.fraInterface.fm.all("UWRegister").value = "<%=tLCPolSchema.getRemark()%>";
          parent.fraInterface.SecondUWGrid.addOne("SecondUWGrid");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,1,"<%=tLCPolSchema.getPolNo()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,2,"");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,3,"<%=tLCPolSchema.getRiskCode()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,4,"<%=tLCPolSchema.getAppntName()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,5,"<%=tLCPolSchema.getInsuredName()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,6,"<%=tLCPolSchema.getAmnt()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,7,"<%=tLCPolSchema.getMult()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,8,"");
          parent.fraInterface.emptyUndefined();
        </script>
        <%
      }
    }
  }
%>