<html>
	<%
	//Name:LLRegisterInput.jsp
	//Function：个人立案界面的初始化程序
	//Date：2008-06-03 17:44:28
	//Author ：MN
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%
 		String CurrentDate= PubFun.getCurrentDate();   
    String CurrentTime= PubFun.getCurrentTime();
  %>
	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="LLEasyRegisterInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLEasyRegisterInit.jsp"%>
    <script language="javascript">
      function initDate(){
        fm.OpStartTime.value="<%=CurrentTime%>";
        fm.OpStartDate.value="<%=CurrentDate%>";
      }
      var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";	
    </script>
	</head>
	<body  onload="initForm();initElementtype();">
		<form action="" method=post name=fm target="fraSubmit">

    <%@include file="LLRemark.jsp"%>
	  <%@include file="CaseTitle.jsp"%>
			
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerSearch);">
				</td>
				<td class= titleImg>
					客户查询
				</td>
			</tr>
		</table>
		<div id="divCustomerSearch" style="display:''">
			<table  class= common>
				<TR  class= common8>
					<TD  class= title8>客户姓名</TD><TD  class= input8><Input class=common name="CustomerName"  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>客户号码</TD><TD  class= input8><Input class=common name="CustomerNo" onkeydown="QueryOnKeyDown()"></TD>
		      <TD  class= title8>保单号码</TD><TD  class= input8><Input class=common name="ContNo" onkeydown="QueryOnKeyDown()" ></TD>
				</tr>
				<TR  class= common8>
					<TD  class= title8>证件号码</TD><TD  class= input8><Input class=common name="tIDNo"  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>证件类型</TD><TD  class= input8><Input  onClick="showCodeList('idtype',[this,tIDTypeName],[0,1]);" onkeyup="showCodeListKeyEx('idtype',[this,tIDTypeName],[0,1]);" class=codeno name="tIDType" verify="证件类型|code:idtype&INT"><Input class= codename name=tIDTypeName onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>出生日期</TD><TD  class= input8><Input class=common name="CBirthday"  ></TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>社保号码</TD><TD  class= input8><Input class=common name="OtherIDNo"  onkeydown="QueryOnKeyDown()"></TD>
				</TR>
			</table>
		</div>
		
		<div id="div1" style="display:''">
			<table>
				<TR>
					<td>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRegisterInfo);">
					</td>
					<td class= titleImg>
						个人申请信息
					</TD>
				</TR>
			</table>
		</div>
		<Div id= "divRegisterInfo" style="display: ''">
			<table class=common>
				<TR class=common >
					<TD  class= title8>申请人与被保人关系</TD>
					<TD  class= input8><Input class="codeno" name=Relation onClick="return showCodeList('llRelation',[this,RelationName],[0,1]);" onkeyup="showCodeListKeyEx('llRelation',[this,RelationName],[0,1]);"  verify="申请人与被保人关系|code:llRelation&INT&notnull"><Input class="codename"  elementtype=nacessary name=RelationName></TD>
					<TD  class= title8>受理方式</TD>
					<TD  class= input8><Input class=codeno  name=RgtType onClick="showCodeList('LLAskMode',[this,RgtTypeName],[0,1]);" onkeyup="showCodeListKeyEx('LLAskMode',[this,RgtTypeName],[0,1]);" verify="受理方式|notnull&code:LLAskMode&INT"><Input class=codename name= RgtTypeName elementtype=nacessary ></TD>
					<TD  class= title8>回执发送方式</TD>
					<TD  class= input8><Input class= codeno name=ReturnMode onClick="showCodeList('LLreturnMode',[this,ReturnModeName],[0,1]);" onkeyup="showCodeListKeyEx('LLreturnMode',[this,ReturnModeName],[0,1]);" verify="回执发送方式|code:LLreturnMode&INT"><Input class= codename name=ReturnModeName ></TD>
				</TR>
				<TR  class= common>
					<TD  class= title8>申请人姓名</TD>
					<TD  class= input8><Input class= common name=RgtantName elementtype=nacessary verify="申请人姓名|notnull"></TD>
					<TD class= title8>申请人证件号码</TD>
					<TD class= input8><Input class= common name=IDNo></TD>
					<TD class= title8>申请人证件类型</TD>
					<TD class= input8> <Input class="codeno" name=IDType onclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this，IDTypeName],[0,1]);"  verify="证件类型|code:idtype&INT"><Input class="codename"  name=IDTypeName ></TD>
				</TR>
				<TR  class= common>
					<TD  class= title8>申请人电话</TD>
					<TD  class= input8><Input class= common name=RgtantPhone verify="电话号码|num&&len<=30"></TD>
					<TD  class= title8>申请人手机</TD>
					<TD  class= input8><Input class= common name=Mobile verify="手机号码|num&&len>=8"></TD>
					<TD  class= title8>申请人电子邮箱</TD>
					<TD  class= input8><Input class= common name=Email verify="电子邮箱|Email"></TD>
				</TR>
				<TR>
					<TD  class= title8>申请人地址</TD>
					<TD  class= input8 colspan=3><Input class= common name=RgtantAddress style=width:97% ></TD>
					<TD  class= title8>邮政编码</TD>
					<TD  class= input8><Input class= common name="PostCode" verify="邮政编码|zipcode&INT">
					</TD>
				</tr>
			</table>
		</Div>
		<Div id= "divGetMode" style="display: ''">
			<table class=common>
				<TR >
					<TD class= title8>受益金领取方式</TD>
					<TD class= input8><Input class="codeno" name=paymode onclick="return showCodeList('paymode',[this,paymodename],[0,1],null,str,'1');" onkeyup="return showCodeListKey('paymode',[this,paymodename],[0,1],null,str,'1');" verify="受益金领取方式|code:paymode&INT&notnull"><Input class="codename" name=paymodename  elementtype=nacessary ></TD>
					<TD class= title8 id=tiAppMoney style= "display: 'none'">本次理赔申报金额</TD>
					<TD class= input8 id=idAppMoney style= "display: 'none'"><Input class= common name="ApplyMoney"  ></TD>
					<TD class= title8 id=titemp0></TD>
					<TD id=idtemp0 class= input8><Input class= common name="sql" type=hidden></td>
					<TD class= title8></TD>
					<TD class= input8><Input class= common name="temp1" type=hidden></td>
				</TR>
				<table class=common id='divBankAcc'>
				<TR  class= common8 >
					<TD class= title8>银行编码</TD>
					<TD class= input8><Input class="codeno"  name=BankCode onclick="return showCodeList('llbank',[this,BankName,SendFlag],[0,1,2]);" onkeyup="return showCodeListKey('llbank',[this,BankName,SendFlag],[0,1,2]);" verify="银行编码|code:bank"><Input class="codename"  elementtype=nacessary name=BankName ></TD>
          <TD class= title8>签约银行</TD> 
          <TD class= input8><Input class="codename" name=SendFlag readonly="true" ></TD>
          <TD class= title8></TD>
          <TD class= input8></TD>				
				</TR>
				<TR>	
					<TD class= title8>银行账号</TD>
					<TD class= input8><input class= common name="BankAccNo" elementtype=nacessary></TD>
					<TD class= title8>银行账户名</TD>
					<TD class= input8><input class= common name="AccName" elementtype=nacessary></TD>
					<TD class= title8></TD>
          <TD class= input8></TD>	
				</TR>
				</table>
			</table>
		</div>

		<table style="display: 'none'">
			<TR>
				<td>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEventInfo);">
				</td>
				<td class= titleImg>
					申请原因
				</TD>
			</TR>
		</table>
		<Div id= "divEventInfo" style="display: 'none'">
		  <table class=common>
		  	<TR  class= common8>
		  		<TD  class= title8>事故者现状</TD>
		  		<TD  class= input8><Input class=codeno name="CustStatus" onClick="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);" onkeyup="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);" verify="事故者现状|code:llcuststatus&INT"><Input class= codename name="CustStatusName"  ></TD>
		  		<TD  class= title8>死亡日期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="DeathDate" verify="死亡日期|date"></TD>
		  		<TD  class= title8></TD>
		  		<TD  class= input8></TD>
		  	</TR>
		  	<TR  class= common8>
		  	  <td colspan=6>
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="01">门诊费用
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="02">住院费用
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="03">医疗津贴
		        <INPUT TYPE="checkBox" NAME="appReasonCode" value="04">重大疾病
	          <INPUT TYPE="checkBox" NAME="appReasonCode" value="05">身 故
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="06">护 理
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="07">失 能
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="08">伤 残
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="09">特 需
            <INPUT TYPE="checkBox" NAME="appReasonCode" value="10" checked >门诊大额
          </td>
		  	</TR>
		  </table>
    </div>

    <Table>
    	<TR>
    		<TD class=common>
    			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg>
    			客户事件信息
    		</TD>
    	</TR>
    </Table>
    <Div  id= "divLDPerson1" style= "display: ''" align = center>
    	<Table  class= common style= "display: 'none'" >
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanEventGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    	<Table class=common>
        <TR class=common>
          <TD class=title8>发生日期</TD>
          <TD class=input8>
          <Input class="coolDatePicker" dateFormat="short" name="AccDate" elementtype=nacessary verify="出险日期|notnull" verify="出险日期|date">
          </TD>
          <TD class=title8 style="display: 'none'">事件号</TD>
          <TD class=input8 style="display: 'none'">
            <Input class=common name="SubReportNo">
          </TD>
          <TD class=title8>事故地点</TD>
          <TD class=input8>
            <Input class=common name="AccPlace" value="汇总医院">
          </TD>
          <TD class=title8 style="display: 'none'">事件信息</TD>
          <TD class=input8 style="display: 'none'">
            <Input class=common name="AccDesc" value="门诊">
          </TD>
          <TD class=title8 style="display: 'none'">事件类型</TD>
          <TD class=input8 style="display: 'none'">
            <Input onClick="showCodeList('llacctype',[this,AccTypeName],[0,1]);" onkeyup="showCodeListKeyEx('llacctype',[this,AccTypeName],[0,1]);" class=codeno name="AccType" value='1'>
            <Input class=codename name=AccTypeName>
          </TD>
        </TR>
       </Table>
    </Div>

    <div style="display: 'none'">
    	<table class= common>
    		<tr class= common8>
    			<TD  class= title>
    			</TD>
    			<TD  class= input>
    			  <INPUT TYPE="checkBox" NAME="SimpleCase" value="02" checked > 受理人处理
    		  </TD>
    		  <td class=title>
    		  </td>
    		  <TD  class= title>
    		  	材料齐备日期
    		  </TD>
    		  <TD  class= input>
    		  	<Input class=readonly readonly name=AffixGetDate >
    		  </TD>
    	  </tr>
      </table>
    </div>
    <br>
    <Div  id= "aa" style= "display: ''" align= right>
    	<input class=cssButton style='width:80px;' id="IdAppConf" type=button name='IdAppConf' value="申请确认" onclick="submitForm()">
    </div>
    <hr>
    <Div  id= "divnormalquesbtn" style= "display: ''" align= right>
    	<input class=cssButton style='width:80px;' type=button value="撤  件" onclick="DealCancel()">
    	<input class=cssButton style='width:80px;' type=button value="申请材料选择" onclick="openAffix()">
    	<input class=cssButton style='width:80px;' type=hidden value="账户修改" onclick="OpenBnf()" name=BankAccM>
    	<input class=cssButton style='width:80px;' type=hidden value="提起调查" onclick="submitFormSurvery()">
    	<input class=cssButton style='width:80px;' type=button value="理赔受理回执" onclick="PrintPage()">
    	<input class=cssButton type=hidden value="账户更替" onclick="BnfModify()">
    	<input class=cssButton type=hidden value="事件信息" onclick="OpenEvent()">
    	<INPUT class=cssButton type=hidden value="查  询"  onclick="return queryClick();">
    	<input class=cssButton type=hidden value="申诉处理" id='AppealDeal'  onclick="OnAppeal()">
    </Div>
    <Div  id= "divRgtFinish" style= "display: 'none'" align= right>
      <hr>
    	<input class=cssButton style='width:80px;' type=button  value="团体申请完毕" onclick="RgtFinish()">
    	<input class=cssButton style='width:80px;' type=button  value="返  回" onclick="top.close()">
    </Div>
    <table>
      <tr>
        <td>
          <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divICaseCure);">
        </td>
        <td class=titleImg>账单信息</td>
      </tr>
    </table>
<div id="divICaseCure" >
  <table class=common>
    <TR class=common>
      <TD class=title8>性别</TD>
      <TD class=input8>
        <input class="code" name="CustomerSex" type=hidden>
        <input class="common4" readonly name=CustomerSexName>
      </TD>
      <TD class=title8>年龄</TD>
      <TD class=input8>
        <input class=common4 name=Age>
      </TD>
      <TD class=title8>参保人员类别</TD>
      <TD class=input8>
        <input class=codeno name=InsuredStat onclick="return showCodeList('insustat',[this,InsuredStatName],[0,1]);" onkeyup="return showCodeListKey('insustat',[this,InsuredStatName],[0,1]);"><input class=codename name=InsuredStatName>
      </TD>
    </TR>
    <TR>
      <TD class=title8>账单属性</TD>
      <TD class=input8>
        <input class="fcodeno" name="FeeAtti" value="4" CodeData="0|2^2|手工报销^4|简易社保账单" onclick="return showCodeListEx('FeeAtti',[this,FeeAttiName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeAtti',[this,FeeAttiName],[0,1]);"><input class=fcodename name=FeeAttiName value="简易社保账单" elementtype=nacessary verify="账单属性|notnull">
      </TD>
      <TD  class= title8>账单种类</TD>
      <TD  class= input8><input class="fcodeno" name="FeeType" value="1"  onclick="return showCodeList('llfeetype',[this,FeeTypeName],[0,1]);" onkeyup="return showCodeListKey('llfeetype',[this,FeeTypeName],[0,1]);"><input class=fcodename name=FeeTypeName value="门诊"  elementtype=nacessary verify="账单种类|notnull"></TD>
      <TD class=title8>帐单类型</TD>
      <TD class=input8>
        <input class="fcodeno" name="FeeAffixType" CodeData="0|2^0|原件^1|复印件" value="0" onclick="return showCodeListEx('FeeAffixType',[this,FeeAffixTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeAffixType',[this,FeeAffixTypeName],[0,1]);"><input class=fcodename name=FeeAffixTypeName value="原件" >
      </TD>
    </TR>
  </table>
</div>
<div id=commonZD style="display:''">
  <table>
    <tr>
      <td class=common>
        <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divCaseReceipt);">
      </td>
      <td class=titleImg>账单信息明细</td>
    </tr>
  </table>
  <Div id="divEasySecu" style="display: ''">
    <table class=common>
      <tr class=common>
        <td class=title8 text-align: center colSpan=1>
          <span id="spanEasySecuGrid" align=center> </span>
        </td>
      </tr>
    </table>
  </div>
  <Div id="divSecurity" style="display: 'none'">
    <table class=common>
      <tr class=common>
        <td class=title8 text-align: center colSpan=1>
          <span id="spanSGSecurityGrid" align=center> </span>
        </td>
      </tr>
    </table>
  </div>
</div>
      <div id="divFeeConfirm" align='right'>
        <INPUT VALUE="账单保存" style='width:60px;' class=cssButton TYPE=button name="FeeSave" onclick="saveFee();">
        <INPUT VALUE="扣除明细" style='width:60px;' class=cssButton TYPE=button onclick="openDrug();">
        <INPUT VALUE="录入完毕" class=cssButton style='width:60px;' TYPE=button name="FeeComplete" onclick="zdComplete();">
      </div>
      <table>
				<tr>
					<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimResult);">
					</td>
					<td class= titleImg> 理算结果</td>
				</tr>
			</table>
			<Div  id= "divClaimResult" style= "display: ''">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanClaimResultGrid" ></span>
						</td>
					</tr>
				</table>
			</Div>
			<Div style= "display: 'none'>
				<input class=CssButton style='width: 60px;' type=button name="ReCalculate" value="重  算" onclick="ReCalPay()">
			</div>
			<table>
				<tr>
					<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divDetailGrid);">
					</td>
					<td class= titleImg> 保单责任明细</td>
				</tr>
			</table>
			<Div  id= "divDetailGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanClaimDetailGrid" >
							</span>
						</td>
					</tr>
				</table>
			</Div>

			<table>
				<tr>
					<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimTotal);">
					</td>
					<td class= titleImg> 计算赔付汇总</td>
				</tr>
			</table>
			<div  id= "divClaimTotal" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>先期给付</TD><TD  class= input8><Input class= readonly name="PreGiveAmnt" readonly></TD>
						<TD  class= title8>自付金额</TD><TD  class= input8><Input class= readonly name="SelfGiveAmnt" readonly></TD>
						<TD  class= title8>退休补充给付</TD><TD  class= input8><Input class= readonly name="RetireAddFee" readonly></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>不合理费用</TD><TD  class= input8><Input class= readonly name="RefuseAmnt" readonly></TD>
						<TD  class= title8>理算金额</TD><TD  class= input8><Input class= readonly name="StandPay" readonly></TD>
						<TD  class= title8>实赔金额</TD><TD  class= input8><Input class= readonly name="RealPay" readonly></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>实赔天数</TD><TD  class= input8><Input class= readonly name="RealHospDate" readonly></TD>
						<TD  class= title8>赔付金额</TD>
						<TD  class= input8><Input class= readonly name="AllGiveAmnt" readonly></TD>
						<TD  class= title8>赔付结论</TD>
						<TD  class= input8><Input class= readonly name="AllGiveType" readonly></TD>
					</TR>
				</table>
			</div>
			<div id=divconfirm align='right'>
		     <input class=CssButton style='width: 60px;' type=button value="理算确认" name="SubCalculate" onclick="submitPayForm();">
		     <input class=CssButton style='width: 60px;' type=button value="拒赔原因" name="Decline" onclick="ClaimDecline();">
		     <input class=cssButton style='width: 60px;' type=button value="结  案" name="Btn_ManChk" onclick="Claim_SPSD()">
		     <input class=cssButton style='width: 60px;' type=button value="给付确认" name="Btn_GiveEn" onclick="GiveEnsure()">
	    </div>
	    
	    <input type=hidden name="fmtransact">
	    <input type=hidden name="Reason">
      <Input type=hidden name=AppAmnt >
      <Input type=hidden name=Sex>
	    <input type=hidden name="ReContNo">
	    <input type=hidden name="LoadFlag" value="1">
	    <input type=hidden name="AppealFlag" value="0">
	    <input type=hidden name="operate">
	    <input type=hidden name="rgtflag">
	    <input type=hidden name="realpeoples">
	    <input type=hidden name="AccFlag">
      <input type=hidden name="CaseRelaNo">
	    <input type=hidden name="OpStartDate">
	    <input type=hidden name="OpStartTime">
	    <input type=hidden name="OtherIDType">
	    <input type=hidden name="LoadC">
	    <input type=hidden name="RiskCode">
	    <Input type=hidden name="MainFeeNo">
	    <Input type=hidden name="ReceiptNo">
	    <Input type=hidden name="HospitalCode">
	    <Input type=hidden name="HospitalName">
	    <Input type=hidden name="FeeDate">
	    <Input type=hidden name="Case_RgtState">
	    <Input type=hidden name="RealHospDate" value="1">
	    <Input type=hidden name="RemarkSP" value="简易理赔">
	    
	    <input type=hidden name="OpType">
	    <input type=hidden name="Opt">
	    <input type=hidden name="cOperate">
	    <input type=hidden name="MngCom">
	    
	    
	    <input type=hidden name="SMSContent">
	    <input type=hidden name="EmailContent">
	    
	    
	  <span id="spanCode"  style="display: none; position:absolute; slategray">
	  </span>
  </form>


  </body>
</html>