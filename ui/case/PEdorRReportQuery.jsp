<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：RReportQuery.jsp
//程序功能：生存调查报告查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head >
<title> </title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="PEdorRReportQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEdorRReportQueryInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
  	 <table> 
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 契调待回销队列
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCRReport" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanLCRReportGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    </div>

    <div id="divHidden" style="display :'none'">
	 <table class= common align=center>
    	<tr class= common>
			<td  class= title> 姓名 </td>
			<td  class= input>
				<Input class=codeNo name=CustomerName ondblclick="showCodeListEx('CustomerName',[this,CustomerNameName,CustomerNo],[0,1,2],'', '', '', true);" onkeyup="showCodeListKeyEx('CustomerName',[this,CustomerNameName,CustomerNo],[0,1,2],'', '', '', true);"><input class=codename name=CustomerNameName readonly=true>  </td>   	
			<td  class= title> 客户号 </td>
			<td  class= input><Input readonly class=common name=CustomerNo>  </td>   	
		</tr>
    </table>
  </div>
  
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 契约调查项目明细报告
    		</td>
    	</tr>
    </table>
    <Div  id= "divUWSpec1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  				<span id="spanQuestGrid">
  				</span> 
  		  	</td>
  		</tr>
    	</table>
    </div>
<Div  id= "divAddDelButton" style= "display: 'none'" align=left>
    <input type =hidden name="addRReportButton" class=cssButton value="保  存" onclick="addRecord();"></TD>
    <input type =button name="modRReportButton" class=cssButton value="修  改" onclick="modifyRecord();"></TD>
    <input type =button name="delRReportButton" class=cssButton value="删  除" onclick="delRecord();"></TD>
</DIV>
<BR>
    <table>
     <table class=common>
         <TR  class= common> 
           <TD  class= common> 其他契调信息 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="Contente" cols="120" rows="3" class="common" ></textarea>
           </TD>
         </TR>
      </table>
       <table class=common>
         <TR  class= common> 
           <TD  class= common> 契调结论 </TD>
         </TR>
         <TR  class= common>
           <TD  class= common>
             <textarea name="ReplyContente" cols="120" rows="3" class="common" ></textarea>
           </TD>
         </TR>
      </table>
  <p> 
    <!--读取信息-->
    <input type= "hidden" name= "Flag" value="">
    <input type= "hidden" name= "AppntNo" value="">
    <input type= "hidden" name= "EdorNo" value="">
    <input type= "hidden" name= "ContNo" value= "">
    <input type= "hidden" name= "Type" value="">
    <input type= "hidden" name= "PrtSeq" value="">
    <input type= "hidden" name= "InsuredNo" value="">   
    <input type= "hidden" name= "InsuredName" value="">
    <input type= "hidden" name= "GrpProposalNo" value="">
    <input type= "hidden" name= "RiskCode" value="">
    <input type=hidden id="fmAction" name="fmAction">
    <INPUT  type= "hidden" class= Common name=RReportItemCode>
    <INPUT  type= "hidden" class= Common name=RReportItemName>
    <INPUT  type= "hidden" class= Common name=RRItemContent>
    <INPUT  type= "hidden" class= Common name=RRItemResult>
    <INPUT  type= "hidden" class= Common name=LoadFlag>
  </p>
  <input value="契调信息结论保存" class=cssButton type=button onclick="saveRReport();" > 
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>