//程序名称：LLExemption.js
//程序功能：保费豁免
//创建日期：2013-12-02 17:00:00
//创建人  ：Houyd
//更新记录：  更新人    更新日期     更新原因/内容
//该文件中包含客户端需要处理的函数和事件


var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
//用于初始化保费豁免页面的mulline
function SearchExemption(){	
	    var ClaimPolGridNum = fm.ClaimPolGridNum.value;
		var CaseNo = fm.CaseNo.value;
	    if(CaseNo.substring(0,1) == 'R' || CaseNo.substring(0,1) == 'S')
	    {
	      var SQLCaseNo = "select caseno from LLAppeal where appealno ='"+CaseNo+"'";
  	      var JCCaseNo = easyExecSql(SQLCaseNo);
  	      CaseNo = JCCaseNo[0][0];
	    }
	    //查询备注信息
	    var tRemarkQuerySql = "select remark from llexemption where caseno = '"+CaseNo+"' fetch first 1 rows only";
	    var tRemarkAry = easyExecSql(tRemarkQuerySql);
	    if(tRemarkAry){
	    	fm.all('Remark').value = tRemarkAry[0][0];
	    }
			
			var SQL = "select d.contno,d.riskcode,e.riskname,(select FreeStartDate from llexemption where polno=d.polno and caseno='"+CaseNo+"'),d.payenddate,(select FreePried from llexemption where polno=d.polno and caseno='"+CaseNo+"'),f.codename,d.polno,d.contno,d.payintv, "+
				"(select case when state='1' then '申诉纠错' when state='0' then '已豁免' when state='2' then '失效' when state='temp' then '待审批' else '' end from llexemption where polno=d.polno and caseno='"+CaseNo+"')" +
						" from lcpol d,lmrisk e,ldcode f " + 
				"where d.riskcode = e.riskcode and d.payintv = f.code and f.codetype = 'payintv' " + 
				"and d.contno in( select ContNo from llclaimdetail where caseno='"+CaseNo+"' )and d.riskcode in(select distinct a.code1 from ldcode1 a,ldcode1 b " +
						"where a.code1=b.code and b.code1=d.riskcode and b.codetype='checkexemptionrisk' " +
						"and a.codetype='checkappendrisk' union select distinct a.code from ldcode1 a,ldcode1 b " +
						"where a.code1=b.code and b.code1=d.riskcode and b.codetype='checkexemptionrisk' " +
						"and a.codetype='checkappendrisk' union select distinct b.code1 from ldcode1 a,ldcode1 b " +
						"where a.code1=b.code and b.code1=d.riskcode and b.codetype='checkexemptionrisk' and " +
						"a.codetype='checkappendrisk' union select distinct code1 from ldcode1 where " +
						"(code1=d.riskcode or code=d.riskcode) and codetype='checkappendrisk' union select distinct code " +
						"from ldcode1 where (code1=d.riskcode or code=d.riskcode) and codetype='checkappendrisk' " +
						"union select riskcode from lmriskapp where risktype8='8' and riskcode= D.RISKCODE " +
						"union select riskcode from lmriskapp where risktype8='9' and kindcode='S' and riskcode=d.riskcode)";
			var result = easyExecSql(SQL);
			if(!result){
				alert("豁免信息查询失败");
			}
		                              
			turnPage.queryModal(SQL, ExemptionGrid);
			
			//若开始时间为空，为其赋初值
			var tSQL = "select max(accdate) from LLSubReport where subrptno in( "+
		                    "select subrptno from llcaserela where caseno='"+CaseNo+"')";
		    var tAccAry = easyExecSql(tSQL); 
		    var tAccDate = tAccAry[0][0];
			for(i=0;i<ExemptionGrid.mulLineCount;i++){
				if(ExemptionGrid.getRowColData(i,4)==""||ExemptionGrid.getRowColData(i,4)==null){
				
					ExemptionGrid.setRowColData(i,4,tAccDate);
				}
			}
			
		ExemptionGrid.checkBoxAll ();
}

//点击不可取消选择
function selectAll(i){
	for(j=0;j<ExemptionGrid.mulLineCount;j++){
		if(ExemptionGrid.getChkNo(j)!=true){
			 ExemptionGrid.checkBoxSel(j+1);
		}
	}
}

//提交，保存按钮对应操作
function submitForm(k)
{
	//只有案件原处理人，才能进行豁免操作star
	var tOperator =fm.cOperator.value;
//	alert(tOperator);
	var caseno = fm.CaseNo.value;
	var tSql = "select 1 from llcase where caseno ='"+caseno+"'and handler='"+tOperator+"' with ur";
	var result = easyExecSql(tSql);
	if(result != 1){
		
		alert("您不是案件原处理人，无法进行豁免操作");
		return false;
	}
	var tSQL0 = "select rgtstate from llcase where caseno='"+caseno+"'";
	var result = easyExecSql(tSQL0);
	if(result != '03'){	//只有检录状态可以进入
		
		alert("案件："+caseno+"不处于检录状态，无法豁免保费，请回退！");
	    return false;
	}
	//只有案件原处理人，才能进行豁免操作end
	if(k == 0){//确认豁免保费
		fm.fmtransact.value = "SAVE";
	}else if(k == 1){//取消豁免保费
		fm.fmtransact.value = "CANCEL";
	}
	var Count=ExemptionGrid.mulLineCount;
	if(Count==0){
	  	alert("没有案件信息!");
	  	return false;
	 }
  	var chkFlag=false;
  	for (i=0;i<Count;i++){
    	if(ExemptionGrid.getChkNo(i)==true){
    		if(ExemptionGrid.getRowColData(i,2)==""||ExemptionGrid.getRowColData(i,2)==null){
		    	alert("第"+(i+1)+"行险种号为空!");	
				return false;
			}else if(ExemptionGrid.getRowColData(i,4)==""||ExemptionGrid.getRowColData(i,4)==null){
				alert("第"+(i+1)+"行保费豁免开始时间为空！");
				ExemptionGrid.setFocus(i,4);
				return false;
			}else if(!isDate(ExemptionGrid.getRowColData(i,4))){
				alert("第"+(i+1)+"行保费豁免开始时间格式错误！");
				ExemptionGrid.setFocus(i,4);
				return false;
			}else if(ExemptionGrid.getRowColData(i,4) > ExemptionGrid.getRowColData(i,5)){
				alert("第"+(i+1)+"行保费豁免开始时间大于保费豁免结束时间！");
				ExemptionGrid.setFocus(i,4);
				return false;
			}
			//判断豁免开始时间
			var CaseNo = fm.CaseNo.value;
		    if(CaseNo.substring(0,1) == 'R' || CaseNo.substring(0,1) == 'S')
		    {
		      var SQLCaseNo = "select caseno from LLAppeal where appealno ='"+CaseNo+"'";
	  	      var JCCaseNo = easyExecSql(SQLCaseNo);
	  	      CaseNo = JCCaseNo[0][0];
		    }
			var tSQL = "select min(accdate) from LLSubReport where subrptno in( "+
		                    "select subrptno from llcaserela where caseno='"+CaseNo+"')";
		    var tAccAry = easyExecSql(tSQL); 
		    var tAccDate = tAccAry[0][0];
			if(ExemptionGrid.getRowColData(i,4) < tAccDate){
				alert("第"+(i+1)+"行保费豁免开始时间早于案件下事件出险时间！");
				ExemptionGrid.setFocus(i,4);
				return false;
			}
      		chkFlag=true;
      		
    	}
  	}
  	
    if (chkFlag==false){
        alert("请选择要处理的案件!");
        return false;
    }

    var showStr="正在执行操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LLExemptionSave.jsp";
    fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  SearchExemption();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
     parent.fraMain.rows = "0,0,0,0,*";
 	}
}

function querySuggest()
{
	sql = "select Remark,RemarkType from LLRenewOpinion where caseno = '"+fm.CaseNo.value+"' and customerno = '"+fm.CustomerNo.value+"'";

		arrResult = easyExecSql(sql);
		if(arrResult)
		{
		}
}
