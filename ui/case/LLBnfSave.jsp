<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLBnfSave.jsp
//程序功能：
//创建日期：2005-2-26 15:06
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="java.util.*"%>
  <%
  	System.out.println("<-Star SavePage->");
    LLBnfSet tLLBnfSet= new LLBnfSet();
    LLBnfBL tLLBnfBL= new LLBnfBL();
    VData tVData = new VData();
    
      CErrors tError = null;
	  String tRela  = "";                
	  String FlagStr = "";
	  String Content = "";
	  String transact = "";
	  GlobalInput tG = new GlobalInput(); 
	  tG=(GlobalInput)session.getValue("GI");
	//跳转标记  
	String tLoadFlag=request.getParameter("LoadFlag");
	System.out.println("跳转标记 ："+tLoadFlag);
	  	
    String tContNo[] = request.getParameterValues("BnfGrid1"); //合同号
    String tRiskName[] = request.getParameterValues("BnfGrid2"); //险种名称
    String tRiskCode[] = request.getParameterValues("BnfGrid20"); //险种编码
	String tName[] = request.getParameterValues("BnfGrid3"); //姓名
	String tSex[] = request.getParameterValues("BnfGrid21"); //性别
	String tBirthday[] = request.getParameterValues("BnfGrid5"); //出生日期
	String tBnfGrade[] = request.getParameterValues("BnfGrid22");//受益顺序
	String tBnfLot[] = request.getParameterValues("BnfGrid7");	//受益份额
	String tCustomerNo[] = request.getParameterValues("BnfGrid8");		//客户号
	String tIDType[] = request.getParameterValues("BnfGrid23");//证件类型
	String tIDNo[] = request.getParameterValues("BnfGrid10");
	String tPolNo[] = request.getParameterValues("BnfGrid11");
	String tRelationToInsured[] = request.getParameterValues("BnfGrid12");//与被保险人关系
	
	
	String tAccName[] = request.getParameterValues("BnfGrid14");  //帐户名  
	String tGetMoney[] = request.getParameterValues("BnfGrid15");
	String tBankCode[] = request.getParameterValues("BnfGrid16");
	String tBankAccNo[] = request.getParameterValues("BnfGrid17");
	String tCaseGetMode[] = request.getParameterValues("BnfGrid18");
	
	String tNativePlace[];
	String tOccupationCode[];
	String tPostalAddress[];
	String tPhone[];
	String tMobilePhone[];
	String tIDStartDate[];
	String tIDEndDate[];
	
	if("2".equals(tLoadFlag)){//从“审批审定”页面进入
		String tModiReasonDesc[] = request.getParameterValues("BnfGrid24");
		String tModiReasonCode[] = request.getParameterValues("BnfGrid25");
		tNativePlace = request.getParameterValues("BnfGrid29");
		tOccupationCode = request.getParameterValues("BnfGrid31");
		tPostalAddress = request.getParameterValues("BnfGrid32");
		tPhone = request.getParameterValues("BnfGrid33");
		tMobilePhone = request.getParameterValues("BnfGrid34");
		tIDStartDate = request.getParameterValues("BnfGrid35");
		tIDEndDate = request.getParameterValues("BnfGrid36");
	
	}else{
		tNativePlace = request.getParameterValues("BnfGrid25");
		tOccupationCode = request.getParameterValues("BnfGrid27");
		tPostalAddress = request.getParameterValues("BnfGrid28");
		tPhone = request.getParameterValues("BnfGrid29");
		tMobilePhone = request.getParameterValues("BnfGrid30");
		tIDStartDate = request.getParameterValues("BnfGrid31");
		tIDEndDate = request.getParameterValues("BnfGrid32");
	}
	
	                                                     
	  
	transact = request.getParameter("fmtransact");
	System.out.println("操作符："+transact);
	
	if( transact.equals("INSERT||MAIN"))
	{
			int bnfcount = 0;
			String tBnfGridNo[] = request.getParameterValues("BnfGrid1");
			bnfcount=tBnfGridNo.length;
		 for(int i=0;i<bnfcount;i++)
		 {
			   	System.out.println("i : "+i);
			    LLBnfSchema tLLBnfSchema = new LLBnfSchema();
			    tLLBnfSchema.setContNo(tContNo[i]);	  			       
			    tLLBnfSchema.setPolNo(tPolNo[i]);
			    tLLBnfSchema.setBnfType("1");
			    tLLBnfSchema.setBnfGrade(tBnfGrade[i]);			    
			    tLLBnfSchema.setInsuredNo(request.getParameter("InsuredNo"));			
			    tLLBnfSchema.setRelationToInsured(tRelationToInsured[i]);
			    tLLBnfSchema.setBnfLot(tBnfLot[i]);
			    tLLBnfSchema.setCustomerNo(tCustomerNo[i]);
			    tLLBnfSchema.setName(tName[i]);
			    tLLBnfSchema.setSex(tSex[i]);
			    tLLBnfSchema.setBirthday(tBirthday[i]);
			    tLLBnfSchema.setIDType(tIDType[i]);
			    tLLBnfSchema.setIDNo(tIDNo[i]);
			    tLLBnfSchema.setCaseNo(request.getParameter("CaseNo"));
			    tLLBnfSchema.setCaseGetMode(tCaseGetMode[i]);
			    tLLBnfSchema.setGetMoney(tGetMoney[i]);
			    //tLLBnfSchema.setBankCode(tBankCode[i]);
			    //tLLBnfSchema.setBankAccNo(tBankAccNo[i]);
			    //tLLBnfSchema.setAccName(tAccName[i]);
			    tLLBnfSchema.setNativePlace(tNativePlace[i]);
			    tLLBnfSchema.setOccupationCode(tOccupationCode[i]);
			    tLLBnfSchema.setPostalAddress(tPostalAddress[i]);
			    tLLBnfSchema.setPhone(tPhone[i]);
			    tLLBnfSchema.setMobilePhone(tMobilePhone[i]);
			    tLLBnfSchema.setIDStartDate(tIDStartDate[i]);
			    tLLBnfSchema.setIDEndDate(tIDEndDate[i]);
			    			    
			    tLLBnfSet.add(tLLBnfSchema);   
			    System.out.println("准备数据");    
		            
		 }
	  }
	  if( transact.equals("UPDATE||MAIN"))
	  {
	  	 String tSel[] = request.getParameterValues("InpBnfGridSel");
	  	 
	  	 if ( tSel!=null )
	  	 for (int i=0; i<tSel.length;i++)
         {
         System.out.println("选中行数："+tSel[i]+"");
         
	         if(tSel[i].equals("1"))
	         {
	         	System.out.println("i : "+i);
			    LLBnfSchema tLLBnfSchema = new LLBnfSchema();
			    tLLBnfSchema.setContNo(tContNo[i]);	 
			   
			    tLLBnfSchema.setPolNo(tPolNo[i]);
			    tLLBnfSchema.setBnfType("1");
			    tLLBnfSchema.setBnfGrade(tBnfGrade[i]);			    
			    tLLBnfSchema.setInsuredNo(request.getParameter("InsuredNo"));			
			    tLLBnfSchema.setRelationToInsured(tRelationToInsured[i]);
			    tLLBnfSchema.setBnfLot(tBnfLot[i]);
			    tLLBnfSchema.setCustomerNo(tCustomerNo[i]);
			    tLLBnfSchema.setName(tName[i]);
			    tLLBnfSchema.setSex(tSex[i]);			    
			    tLLBnfSchema.setBirthday(tBirthday[i]);
			    tLLBnfSchema.setIDType(tIDType[i]);
			    tLLBnfSchema.setIDNo(tIDNo[i]);
			    tLLBnfSchema.setCaseNo(request.getParameter("CaseNo"));
			    tLLBnfSchema.setCaseGetMode(tCaseGetMode[i]);
			    tLLBnfSchema.setGetMoney(tGetMoney[i]);
			    //tLLBnfSchema.setBankCode(tBankCode[i]);
			    //tLLBnfSchema.setBankAccNo(tBankAccNo[i]);
			    //tLLBnfSchema.setAccName(tAccName[i]);
			    
			    tLLBnfSchema.setNativePlace(tNativePlace[i]);
			    tLLBnfSchema.setOccupationCode(tOccupationCode[i]);
			    tLLBnfSchema.setPostalAddress(tPostalAddress[i]);
			    tLLBnfSchema.setPhone(tPhone[i]);
			    tLLBnfSchema.setMobilePhone(tMobilePhone[i]);
			    tLLBnfSchema.setIDStartDate(tIDStartDate[i]);
			    tLLBnfSchema.setIDEndDate(tIDEndDate[i]);  
			    
			    tLLBnfSet.add(tLLBnfSchema);
	         }
         } 

	  }
	 
	 try
	  {
	  // 准备传输数据 VData
		tVData.add(tLLBnfSet);
	  	tVData.add(tG);
	  	System.out.println("begin");
	    tLLBnfBL.submitData(tVData,transact);
	    System.out.println("end");
	  }
	catch(Exception ex)
	  {
	    Content = "保存失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLLBnfBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>