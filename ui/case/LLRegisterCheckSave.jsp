<%
/*******************************************************************************
 * Name     :LLRegisterCheckSave.jsp
 * Function :检录确认程序，变化案件状态，调用自动理算
 * Authorm  :wujs
 * Date     :2005-2-21 20:40
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page contentType="text/html;charset=GBK" %>
<%
  
  CaseCheckUI tCaseCheckUI   = new CaseCheckUI();
  
  CErrors tError = null;
  String transact = request.getParameter("fmtransact");
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  
  String strCaseNo=request.getParameter("CaseNo");
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI"); 
  LLCaseSchema tLLCaseSchema  = new LLCaseSchema();
  tLLCaseSchema.setCaseNo(strCaseNo);
  
  VData tVData = new VData();
  VData tVData2 = new VData();
	tVData.addElement(tG);
	tVData.addElement(tLLCaseSchema);

  try
  {
  //账单关联保存
  	if ( "FEERELASAVE".equals(transact))
  	{
  	  	String CaseRelaNo = request.getParameter("CaseRelaNo");
  	  	String[] tChk = request.getParameterValues("InpICaseCureGridChk");
  	  	String[] strPolNo=request.getParameterValues("ICaseCureGrid5");
  	  	LLFeeMainSet tLLFeeMainSet= new LLFeeMainSet();
		  	if ( tChk!=null )
		  	{
				  int intLength=tChk.length;
				  for(int i=0;i<intLength;i++)
				  {
				    if(tChk[i].equals("0")) //未选
				      continue;
				
				    LLFeeMainSchema tLLFeeMainSchema=new LLFeeMainSchema();
				    tLLFeeMainSchema.setCaseNo(strCaseNo);
				    tLLFeeMainSchema.setMainFeeNo( strPolNo[i]);
				    tLLFeeMainSchema.setCaseRelaNo(CaseRelaNo);
				    tLLFeeMainSet.add(tLLFeeMainSchema);
				  }
		  	}
  	  	tVData.addElement(tLLFeeMainSet);
   	}
	  tCaseCheckUI.submitData(tVData,transact);
	  System.out.println("transact:::::::::"+transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCaseCheckUI.mErrors;
    if (!tError.needDealError())
    {
      Content = "保存成功";
      FlagStr = "Succ";
/**
保单关联
*/
  LLCasePolicySet tLLCasePolicySet=new LLCasePolicySet();
  CasePolicyUI tCasePolicyUI   = new CasePolicyUI();

  tError = null;
  String transact1 = "INSERT";
	//读取立案明细信息
  String strCustomerNo = request.getParameter("CustomerNo");
  
  String strSQL0 = "select contno,InsuredName,riskcode,amnt,cvalidate,polno from ";
  String wherePart = " where insuredno='"+ strCustomerNo +"' and appflag='1' and stateflag<>'4' ";
  if(request.getParameter("RiskCode")=="1605")
  {
  	wherePart+=" and RiskCode='"+request.getParameter("RiskCode")+"'";
  }
  String strSQL = strSQL0+" lcpol "+wherePart+" union "+strSQL0+" lbpol "+wherePart;
  SSRS aSSRS = new ExeSQL().execSQL(strSQL) ; 

  String strRgtNo  = request.getParameter("RgtNo");
//  String[] strPolNo=request.getParameterValues("CasePolicyGrid6");
  String CaseRelaNo = request.getParameter("CaseRelaNo");

  int intLength=aSSRS.MaxRow ;
  System.out.println("保存LCPOL的信息");
  System.out.println(intLength);
  for(int i=0;i<intLength;i++)
  {
    LLCasePolicySchema tLLCasePolicySchema=new LLCasePolicySchema();
    tLLCasePolicySchema.setCaseNo(strCaseNo);
    tLLCasePolicySchema.setRgtNo(strRgtNo);
    System.out.println(aSSRS.GetText(i+1,6));
    tLLCasePolicySchema.setPolNo(aSSRS.GetText(i+1,6));
    tLLCasePolicySchema.setCaseRelaNo(CaseRelaNo);
    tLLCasePolicySet.add(tLLCasePolicySchema);
  }

    GlobalInput tG1 = new GlobalInput();
  	tG1=(GlobalInput)session.getValue("GI");

    try
    {
      VData tVData1 = new VData();
      tVData1.addElement(strCustomerNo);
      tVData1.addElement(strCaseNo);
      tVData1.addElement(tLLCasePolicySet);
      tVData1.addElement(tG1);
      if(tCasePolicyUI.submitData(tVData1,transact1)){
        ClaimCalBL tClaimCalBL=new ClaimCalBL();
        try
        {
		    	tVData2.addElement(tLLCaseSchema);
          tVData2.addElement(tG1);
          System.out.println("......tClaimCalBL");
          tClaimCalBL.submitData(tVData2,"autocal");
        }catch(Exception ex)
        {
          Content = transact1+"失败，原因是:" + ex.toString();
          FlagStr = "Fail";
        }
      }
    }
    catch(Exception ex)
    {
      Content = transact1+"失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
    if (FlagStr=="")
    {
      tError = tCasePolicyUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 保存成功";
        FlagStr = "Succ";
      }
      else
      {
        Content = " 保存失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }  
    }
    else
    {
      Content = "保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>