
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>

<%@page import="java.io.*"%>

<%
System.out.println("start：LLCaseStatisticsReport 团个险分险种-个、团");
 
String flag = "0";
String FlagStr = "";
String Content = "";
String Con="";
GlobalInput tG = new GlobalInput();
tG = (GlobalInput)session.getValue("GI");
	
String operator=tG.Operator;

String ManageCom = request.getParameter("ManageCom");
System.out.println(ManageCom);
String StartDate = request.getParameter("StartDate");
System.out.println(StartDate);
String EndDate = request.getParameter("EndDate");
System.out.println(EndDate);
String RiskRate= request.getParameter("RiskRate");
System.out.println(RiskRate);
String ContType = request.getParameter("ContType");
System.out.println(ContType);
String Operate = request.getParameter("Operate");
System.out.println("Operate:"+operator);
String StatsType = request.getParameter("StatsType");//统计类型
System.out.println("StatsType:"+StatsType);

System.out.println("@团个险分险种统计报表@");
//------------------------------
RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);
//-------------------------------

String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");
if(sd.compareTo(ed) > 0)
{
	flag = "1";
	FlagStr = "Fail";
	Content = "操作失败，原因是:统计止期比统计统计起期早";
}
if(ContType.equals("1"))
{
	ContType="I";
	Con="个险";
}else{
	ContType="G";
	Con="团险";
}
  	StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
  	EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");
  	String CurrentDate = PubFun.getCurrentDate();
  	CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	tVData.addElement(StartDate);
	tVData.addElement(EndDate);
  	String fileNameB = tG.Operator + "_" + FileQueue.getFileName()+".vts";	
  
  	TransferData tTransferData= new TransferData();
  	tTransferData.setNameAndValue("tFileNameB",fileNameB);
  
  	tVData.addElement(ManageCom);
  	tVData.addElement(operator);
  	tVData.addElement(ReportPubFun.getMngName(ManageCom));
  	tVData.addElement(ContType);
  	tVData.addElement(Con);
  	tVData.addElement(StatsType);
  	tVData.addElement(tTransferData);
  
  	XmlExport txmlExport = new XmlExport();
  	//vts打印的工具类
	//CombineVts tcombineVts = null;
	//CError cError = new CError( );
	//CErrors tError = null;
  	LLCaseStatisticsUI tLLCaseStatisticsUI = new LLCaseStatisticsUI();
  	System.out.println("Start 后台处理...");
  
   	if(!tLLCaseStatisticsUI.submitData(tVData,Operate))
   	{
      	Content=tLLCaseStatisticsUI.mErrors.getFirstError().toString(); 
      	%>
  		<%=Content%>  
  		<%return;
   	}else{
   		System.out.println("取数完毕...准备打印报表..."); 
		mResult = tLLCaseStatisticsUI.getResult();			
  		txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
  		if(txmlExport==null)
  		{
   			Content="没有得到要显示的数据文件";	
   			%>   		
   			<%=Content%>  
   			<%return;
  		}
   	}
   	String date=PubFun.getCurrentDate().replaceAll("-","");  
	String time=PubFun.getCurrentTime3().replaceAll(":","");
   
	System.out.println("开始打开报表!");
	Readhtml rh=new Readhtml();
	rh.XmlParse(txmlExport.getInputStream()); //相当于XmlExport.getInputStream();
	  
	String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
	String temp=realpath.substring(realpath.length()-1,realpath.length());
	if(!temp.equals("/"))
	{
		realpath=realpath+"/"; 
	}
	String templatename=rh.getTempLateName();//模板名字
	String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
	System.out.println("*********************templatepathname= " + templatepathname);
	System.out.println("************************realpath="+realpath);
	  
	String outname="CaseStatisticsReport"+tG.Operator+date+time+".xls";
	
	 //当前日期
	  String[] tCurrentDate = PubFun.getCurrentDate().split("-");
	  //当前日期  年
	  String tYear = tCurrentDate[0];
	  //当前日期  月
	  String tMonth = tCurrentDate[0]+tCurrentDate[1];
	  //当前日期   日
	  String tDay = tCurrentDate[0]+tCurrentDate[1]+tCurrentDate[2];
	  //原路径
	  String tFilePath = realpath+"vtsfile/";
	  //年文件
	  File yFile = new File(tFilePath+tYear);
	  //月文件
	  File mFile = new File(tFilePath+tYear+"/"+tMonth);
	  //日文件
	  File dFile = new File(tFilePath+tYear+"/"+tMonth+"/"+tDay); 

	  if(!yFile.exists()||!mFile.exists()||!dFile.exists())
	  {
		  dFile.mkdirs();
	  }
	  //最终文件存放路径
	  tFilePath = tFilePath+tYear+"/"+tMonth+"/"+tDay+"/";
	  
	String outpathname=tFilePath+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	//*******************************
	rpt.updateReportMetaData(outname);
	//**********************************
 
	//*********************************
	rh.setReadFileAddress(templatepathname);
	rh.setWriteFileAddress(outpathname);
	rh.start("vtsmuch");
	try{
		outname = java.net.URLEncoder.encode(outname, "UTF-8");
		outname = java.net.URLEncoder.encode(outname, "UTF-8");
	  	//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	  	//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
    }catch(Exception ex)
    {
     	ex.printStackTrace();
    }
    String[] InputEntry = new String[1];
    InputEntry[0] = outpathname;
    String tZipFile = tFilePath+ StrTool.replace(outname,".xls",".zip");
	System.out.println("tZipFile == " + tZipFile);
    rh.CreateZipFile(InputEntry, tZipFile);

%>

<html>
<%@page contentType="text/html;charset=GBK" %>
<a href="../f1print/download.jsp?filename=<%=StrTool.replace(outname,".xls",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>
	  	