/** 
 * 程序名称：ClientCaseQueryInput.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-2-21 13:03
 * 创建人  ：YangMing
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
/*	
	var strSql = "select a.GetNoticeNo, a.otherno, b.name, a.PayDate, a.SumDuePayMoney, a.bankcode, a.bankaccno, a.accname "
	           + " from LJSPay a, ldperson b where a.appntno=b.customerno and OtherNoType='2' "
          	 + getWherePart("a.GetNoticeNo", "GetNoticeNo2")
          	 + getWherePart("a.OtherNo", "OtherNo")
          	 + getWherePart("a.BankCode", "BankCode")
          	 + getWherePart("a.SumDuePayMoney", "SumDuePayMoney");
  
  if (fm.AppntName.value != "") strSql = strSql + " and a.appntno in (select c.customerno from ldperson c where name='" + fm.AppntName.value + "')";
  if (fm.PrtNo.value != "") strSql = strSql + " and a.otherno in (select polno from lcpol where prtno='" + fm.PrtNo.value + "')";
*/  			    
	var strSql = "select RgtNo,CaseNo,RgtType,CustomerNo,CustomerName,AccidentType,PostalAddress,Phone from LLCase where 1=1 "
    + getWherePart("CaseNo", "CaseNo")
    + getWherePart("RgtNo", "RgtNo")
    + getWherePart("RgtType", "RgtType")
    + getWherePart("RgtState", "RgtState")
    + getWherePart("CustomerNo", "CustomerNo")
    + getWherePart("CustomerName", "CustomerName")
    + getWherePart("AccidentType", "AccidentType")
    + getWherePart("ReceiptFlag", "ReceiptFlag")
    + getWherePart("HospitalFlag", "HospitalFlag")
    + getWherePart("SurveyFlag", "SurveyFlag")
    + getWherePart("AffixGetDate", "AffixGetDate")
    + getWherePart("FeeInputFlag", "FeeInputFlag")
    + getWherePart("InHospitalDate", "InHospitalDate")
    + getWherePart("OutHospitalDate", "OutHospitalDate")
    + getWherePart("InvaliHosDays", "InvaliHosDays")
    + getWherePart("InHospitalDays", "InHospitalDays")
    + getWherePart("DianoseDate", "DianoseDate")
    + getWherePart("PostalAddress", "PostalAddress")
    + getWherePart("Phone", "Phone")
    + getWherePart("AccStartDate", "AccStartDate")
    + getWherePart("AccidentDate", "AccidentDate")
    + getWherePart("AccidentSite", "AccidentSite")
    + getWherePart("DeathDate", "DeathDate")
    + getWherePart("CustBirthday", "CustBirthday")
    + getWherePart("CustomerSex", "CustomerSex")
    + getWherePart("CustomerAge", "CustomerAge")
    + getWherePart("IDType", "IDType")
    + getWherePart("IDNo", "IDNo")
    + getWherePart("Handler", "Handler")
    + getWherePart("UWState", "UWState")
    + getWherePart("Dealer", "Dealer")
    + getWherePart("AppealFlag", "AppealFlag")
    + getWherePart("GetMode", "GetMode")
    + getWherePart("GetIntv", "GetIntv")
    + getWherePart("CalFlag", "CalFlag")
    + getWherePart("UWFlag", "UWFlag")
    + getWherePart("DeclineFlag", "DeclineFlag")
    + getWherePart("EndCaseFlag", "EndCaseFlag")
    + getWherePart("EndCaseDate", "EndCaseDate")
    + getWherePart("MngCom", "MngCom")
    + getWherePart("Operator", "Operator")
    + getWherePart("MakeDate", "MakeDate")
    + getWherePart("MakeTime", "MakeTime")
    + getWherePart("ModifyDate", "ModifyDate")
    + getWherePart("ModifyTime", "ModifyTime")
  ;
	turnPage.queryModal(strSql, ClientCaseGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = ClientCaseGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				var rgtNo = arrReturn[0][0];
				var caseNo = arrReturn[0][1];
				top.opener.ClientafterQuery(rgtNo, caseNo);
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ClientCaseGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = ClientCaseGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
