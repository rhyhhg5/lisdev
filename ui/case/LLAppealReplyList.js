var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";
//提交，保存按钮对应操作
function submitForm()
{
  
  if ( fm.LogNo.value!="")
      {
      	alert("你不能执行改操作");
      	return false;
      	}
    if (confirm("您确实想保存该记录吗?"))
    {
      tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      
        fm.fmtransact.value="INSERT||MAIN";
        fm.submit(); //提交
        tSaveFlag ="0";

      
    }
    else
    {
      alert("您取消了修改操作！");
    }

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 
    if (fm.fmtransact.value=="DELETE||MAIN")
    {
    	fm.reset();
    	initCustomerGrid();
    }
 //   showDiv(operateButton,"true");
 //   showDiv(inputButton,"false");
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
 function addClick()
{

  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (confirm("您确实想修改该记录吗?"))
 	{
 		
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //showSubmitFrame(mDebug);
      fm.fmtransact.value = "UPDATE||MAIN"
      fm.submit(); //提交
    }//end of else
 
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{

  window.open("./LLMainAskQuery.html");
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function deleteClick()
{
	  if ( fm.LogNo.value=="")
      {
      	alert("请先查询数据");
      	return false;
    }
    
	if (confirm("删除记录会删除所有咨询记录，您确实想删除该记录吗?"))
 	{	
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      showSubmitFrame(mDebug);
      fm.fmtransact.value = "DELETE||MAIN"
      fm.submit(); //提交
 }//end of else 
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function submitForm1()
{
  tSaveFlag = "0";
	if((fm.PeopleType.value=="")||(fm.PeopleType.value=="null"))
	{
		alert("请您录入事故者类型！！！");
		return;
	}
	if((fm.RptObj.value=="")||(fm.RptObj.value=="null"))
	{
		alert("请您录入号码类型！！！");
		return;
	}
	if (fm.RptObjNo.value=="")
	{
		alert("请您收入要号码！");
 		return ;
  }
  if(fm.RptObj.value=="0")
  {
  	if(fm.PeopleType.value!="0")
  	{
  		alert("号码类型是团单，事故者类型只能是被保险人，请重新录入事故者类型");
  		return;
  	}
  	else
  	{
  		var i = 0;
   		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  		initSubReportGrid();
  		fm.action = "./ReportQueryOut1.jsp";
  		fm.submit(); //提交
  	}
  }
  else
  {
  	var i = 0;
   	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	initSubReportGrid();
  	fm.action = "./ReportQueryOut1.jsp";
  	fm.submit(); //提交
  }
}
//完成按照”客户号“在lcpol中进行查询，显示该客户的保单明细
function showInsuredLCPol()
{
  var row;
	var a_count=0;//判断选中了多少行
  var t = SubReportGrid.mulLineCount;//得到Grid的行数
  for(var i=0;i<t;i++)
  {
    varCount = SubReportGrid.getChkNo(i);
    if(varCount==true)
    {
       a_count++;
       row=i;
    }
  }
  if(a_count>1)
  {
    alert("您只能选中一行记录！");
    return;
  }
  else if(a_count<1)
  {
    alert("请您选中一条分案记录!");
    return;
  }
  else
  {
    var varInsuredNo;
    var varCount;
    varInsuredNo=SubReportGrid.getRowColData(row,1);
    if ((varInsuredNo=="null")||(varInsuredNo==""))
    {
      alert("客户号为空，不能进行查询操作！");
      return;
    }
    var varSrc = "&InsuredNo=" + SubReportGrid.getRowColData(row,1);
    var newWindow = window.open("./FrameMainReportLCPol.jsp?Interface=ReportLCPolInput.jsp"+varSrc,"ReportLCPolInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}

function showCustomerInfo()
{
	window.open("../sys/FrameCQPersonQuery.jsp");
}

function afterCodeSelect( cCodeName, Field ) {
	try	{	 
	    if ( cCodeName =="AskType")
	    {
	    //	if ( Field.value =="0" )
	    //	{
	    //		fm.NoticeIn.style.display='none';
	    //		fm.AskIn.style.display='';	
	    //	}else if ( Field.value =="1" ){
	    //		fm.AskIn.style.display='none';	
	    //		fm.NoticeIn.style.display='';
	    //	}else 
	   
	    if ( Field.value=="0")
	    {
             showConsult();
   	          fm.NoticeIn.style.display='none';
			fm.AskIn.style.display='none';
	    }
	      if ( Field.value=="1")
	    {
	    	showNotice();
	    	fm.NoticeIn.style.display='none';
			fm.AskIn.style.display='none';
	    }
	    if ( Field.value =="2" ){
	    		fm.NoticeIn.style.display='';
	    		fm.AskIn.style.display='';
	    	}
	    
	    }
	    
	    
	   
	   }catch(ex)
	   {
	   	alert(ex.message);
	}
}
function showConsult()
{
		    	DivComCusInfo.style.display='';
	    	DivCusInfo.style.display='';
	    	divEventInfo.style.display='';
	    	AnswerInfo.style.display='';
	    	//
	    	DivNoticeInfo.style.display='none';
	    	
}
function showNotice()
{
	DivComCusInfo.style.display='';
	DivCusInfo.style.display='none';	    	
	AnswerInfo.style.display='none';
	//
	divEventInfo.style.display='';
	DivNoticeInfo.style.display='';
	
 }
function showButton(para)
{
	if ( para==0)
	{
		showConsult();
	}else
		{
			showNotice();
			}
	
}
function inputNotice()
{
}
function inputConsult()
{
	
	if ( fm.LogNo.value=="")
	{
		alert("请先保存登记");
		return ;
	}
	
	
	parent.fraInterface.window.location="LLConsultInput.jsp?loadflag=1&logNo="+fm.LogNo.value;

}

function onSelSelected(parm1,parm2)
{
	alert();
	/*
	  var len =CustomerGrid.getSelNo() - 1;	
	   
	  var i = 0;
	  var logno= fm.LogNo.value ;
	  if (logno=="" ) 
	  {
	  	alert("请先保存登记");
	  	return ;
	  	}
      var ConsultNo =	CustomerGrid.getRowColData(len,4);
      var CustomerNo =	CustomerGrid.getRowColData(len,1);
      var CustomerName =CustomerGrid.getRowColData(len,2);
      if ( fm.AskType.value=="0")
      {
      	tSaveType="Consult";
 	  window.open("LLConsultMain.html?logNo="+logno+"&ConsultNo="+ConsultNo+"&CustomerNo="+CustomerNo+"&AskType="+fm.AskType.value+"&CustomerName="+CustomerName,"咨询录入",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
 	}
 	if ( fm.AskType.value=="1")
      {
      	tSaveType="Notice"
      	alert("待完成");
 	  //window.open("LLConsultMain.html?logNo="+logno+"&ConsultNo="+ConsultNo+"&CustomerNo="+CustomerNo,"咨询录入",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
 	}
 	*/
  
}




function afterQueryLL(arr)
{
/**
*由客户查询返回调用
*/
	
	if ( arr)
	{
		var len = CustomerGrid.mulLineCount;
		if (len<=0) len = 0;
		for ( i=0;i<arr.length;i++)
		{
			row =len+i;
			
			CustomerGrid.addOne();			
			CustomerGrid.setRowColData(row, 1,arr[i][0]);
			CustomerGrid.setRowColData(row, 2,arr[i][1]);
		}
	}
}

function afterQuery(arr)
{
	if ( tSaveType=="EventQuery")
 	{
 	for ( i=0;i<arrReturn.length;i++)
 	{
 		len = SubReportGrid.mulLineCount;
 	
 		SubReportGrid.addOne();                          
 		
 		SubReportGrid.setRowColData(len,1,arrReturn[i][0]);
 		SubReportGrid.setRowColData(len,2,arrReturn[i][1]);
 		SubReportGrid.setRowColData(len,3,arrReturn[i][2]);
 		SubReportGrid.setRowColData(len,4,arrReturn[i][3]);
 				
 	}
 	  return;
 	}
	try{
	var logno = arr[0][0] ;
	
	fm.LogNo.value = logno ;
	var strSQL = "select * from LLMainAsk where logno='"+ logno +"'";
		var qryResult = easyQueryVer3(strSQL,1,1,1);
	if ( !qryResult )
	{
	  alert("没有符合条件的记录");
	  return ;
	}
	var de = decodeEasyQueryResult(qryResult);
	if ( de )
	{
		setMainAskInfo(de)
		strSQL ="select customerno,customername,'咨询',consultno from llconsult where logno='"+ logno +"'"
		    +" union select customerno,customername,'通知',noticeno from llnotice where logno='"+ logno +"'";
	
		turnPage.queryModal(strSQL, CustomerGrid);
	}else
		{
			alert("查询错误!");
		}
	}catch(ex)
        {
        	alert( ex.message );
        }
        
        
	

//	var de = decode
//	if ( arr)
//	{
//		var len = CustomerGrid.mulLineCount;
//		if (len<=0) len = 0;
//		for ( i=0;i<arr.length;i++)
//		{
//			row =len+i;
//			
//			CustomerGrid.addOne();			
//			CustomerGrid.setRowColData(row, 1,arr[i][0]);
//			CustomerGrid.setRowColData(row, 2,arr[i][1]);
//		}
//	}
}

function setMainAskInfo(arrQueryResult)
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		
		 try{fm.all('LogNo').value=arrResult[0][0]}catch(ex){alert(ex.message+"LogNo")}
		try{fm.all('LogState').value=arrResult[0][1]}catch(ex){alert(ex.message+"LogState")}
		try{fm.all('AskType').value=arrResult[0][2]}catch(ex){alert(ex.message+"AskType")}
		try{fm.all('OtherNo').value=arrResult[0][3]}catch(ex){alert(ex.message+"OtherNo")}
		try{fm.all('OtherNoType').value=arrResult[0][4]}catch(ex){alert(ex.message+"OtherNoType")}
		try{fm.all('AskMode').value=arrResult[0][5]}catch(ex){alert(ex.message+"AskMode")}
		try{fm.all('CustomerNo').value=arrResult[0][6]}catch(ex){alert(ex.message+"CustomerNo")}
		try{fm.all('LogName').value=arrResult[0][7]}catch(ex){alert(ex.message+"LogName")}
		try{fm.all('LogComp').value=arrResult[0][8]}catch(ex){alert(ex.message+"LogComp")}
		try{fm.all('LogCompNo').value=arrResult[0][9]}catch(ex){alert(ex.message+"LogCompNo")}
		try{fm.all('LogDate').value=arrResult[0][10]}catch(ex){alert(ex.message+"LogDate")}
		try{fm.all('LogTime').value=arrResult[0][11]}catch(ex){alert(ex.message+"LogTime")}
		try{fm.all('Phone').value=arrResult[0][12]}catch(ex){alert(ex.message+"Phone")}
		try{fm.all('Mobile').value=arrResult[0][13]}catch(ex){alert(ex.message+"Mobile")}
		try{fm.all('PostCode').value=arrResult[0][14]}catch(ex){alert(ex.message+"PostCode")}
		try{fm.all('AskAddress').value=arrResult[0][15]}catch(ex){alert(ex.message+"AskAddress")}
		try{fm.all('Email').value=arrResult[0][16]}catch(ex){alert(ex.message+"Email")}
		try{fm.all('AnswerType').value=arrResult[0][17]}catch(ex){alert(ex.message+"AnswerType")}
		try{fm.all('AnswerMode').value=arrResult[0][18]}catch(ex){alert(ex.message+"AnswerMode")}
		//try{//fm.all('SendFlag').value=arrResult[0][19]}catch(ex){alert(ex.message+"SendFlag")}
		try{fm.all('SwitchCom').value=arrResult[0][20]}catch(ex){alert(ex.message+"SwitchCom")}
		try{fm.all('SwitchDate').value=arrResult[0][21]}catch(ex){alert(ex.message+"SwitchDate")}
		try{fm.all('SwitchTime').value=arrResult[0][22]}catch(ex){alert(ex.message+"SwitchTime")}
		try{fm.all('ReplyFDate').value=arrResult[0][23]}catch(ex){alert(ex.message+"ReplyFDate")}
		try{fm.all('DealFDate').value=arrResult[0][24]}catch(ex){alert(ex.message+"DealFDate")}
		//try{//fm.all('Remark').value=arrResult[0][25]}catch(ex){alert(ex.message+"Remark")}
		try{fm.all('AvaiFlag').value=arrResult[0][26]}catch(ex){alert(ex.message+"AvaiFlag")}
		try{fm.all('NotAvaliReason').value=arrResult[0][27]}catch(ex){alert(ex.message+"NotAvaliReason")}
		try{fm.all('Operator').value=arrResult[0][28]}catch(ex){alert(ex.message+"Operator")}
		try{fm.all('MngCom').value=arrResult[0][29]}catch(ex){alert(ex.message+"MngCom")}
		try{fm.all('MakeDate').value=arrResult[0][30]}catch(ex){alert(ex.message+"MakeDate")}
		try{fm.all('MakeTime').value=arrResult[0][31]}catch(ex){alert(ex.message+"MakeTime")}
		try{fm.all('ModifyDate').value=arrResult[0][32]}catch(ex){alert(ex.message+"ModifyDate")}
		try{fm.all('ModifyTime').value=arrResult[0][33]}catch(ex){alert(ex.message+"ModifyTime")}
        
	}
}

  function RelaQuery()
  {
  	var paramNo;
  	if ( fm.AskType.value=="0")
  	{
  		paramNo= fm.ConsultNo.value;
  		
  	}
  	else
  	{
  			//paramNo = fm.NoticeNo.value ;
  			paramNo="";
  	}
  	 tSaveType="EventQuery"
  	 window.open("./LLSubReportQuery.jsp?AskType="+fm.AskType.value+"&ParamNo="+paramNo,"客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  	}
  	
  	 function ReplySave()
 {
   	 if ( fm.ConsultNo.value =="")
	{
	 alert("没有咨询信息");
	 return false;
	}
 
  if ( fm.SerialNo.value =="") 
  {
	fm.fmtransact.value="INSERT||MAIN";
 }else
 	{
 		fm.fmtransact.value="UPDATE||MAIN";
 		}
	
	
	fm.action= "./LLAnswerInfoSave.jsp";

	tSaveType=="Answer"
	//0submitForm(); 	
 }
 
 function DelRela()
{
 if ( fm.ConsultNo.value =="")
	{
	 alert("没有咨询信息");
	 return false;
	}
 if ( 	SubReportGrid.mulLineCount<=0)
 {
 	alert("没有要删除的关联事件");
 	return false;
 	}
	fm.fmtransact.value="DELETE||RELA";
	
	alert( fm.fmtransact.value );
	fm.action= "./LLSubReportSave.jsp";

	tSaveType=="Event"
	//submitForm(); 	
	alert("")
}


 
 function insertCust()
{
	updateCustomer(CustomerGrid.mulLineCount);
	tSaveType="Consult"
	/*
	if ( fm.LogNo.value=="")
	{
		alert("没有登记号");
	}
	if ( fm.ConsultNo.value =="")
	{
		fm.fmtransact.value="INSERT||MAIN";
	}else
		{
			fm.fmtransact.value="UPDATE||MAIN";
		}
		alert( fm.fmtransact.value );
	fm.action= "./LLConsultSave.jsp";
	tSaveType="Consult"
	submitForm();
	*/
}
function updateCustomer()
{

	 var len =CustomerGrid.mulLineCount - 1;	
	 if (len<0) len = 0;
	 CustomerGrid.addOne();
	 consultNo="no"+ (len +1);
	  if ( len>=0)
	  {
	  	 	CustomerGrid.setRowColData(len,1,fm.CustomerNo.value);
	  		CustomerGrid.setRowColData(len,2,fm.CustomerName.value);
	  		 	CustomerGrid.setRowColData(len,4,consultNo); 
	    consultNo
	  	if ( consultNo=="") {
	  	
	  		CustomerGrid.setRowColData(len,3,"");
	  		
	  		
	  		}
		else
		{
			
	  		if ( tSaveType=="Consult") CustomerGrid.setRowColData(len,3,"咨询"); 
	  		
	  		if (tSaveType=="Notice") CustomerGrid.setRowColData(len,3,"通知"); 
	  	}
	  	
	 
	  }
	  
}
function 	ClientQuery()
{
	
	window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function EventSave()
{
	//alert(1);
	
	tSaveType="Event";
	/*
	if ( fm.ConsultNo.value =="")
	{
		alert("请先保存咨询信息");
		return 
	}
	if ( trim(fm.SubRptNo.value) !="")
	{
		if ( !confirm("确定要增加新事件吗?"))
		{
			return;
		}
    }
	
		fm.fmtransact.value="INSERT||MAIN";
	
	fm.action= "./LLSubReportSave.jsp";
	submitForm();
	*/
	fm.fmtransact.value="INSERT||MAIN";
	 updateEventGrid()
}

function updateEventGrid()
{

	if (fm.fmtransact.value=="DELETE||RELA")
	{
		
		SubReportGrid.delCheckTrueLine();
		
	}
	else if (fm.fmtransact.value=="INSERT||MAIN")
	{
		 //  alert(0);
    		var len = SubReportGrid.mulLineCount;
	    	if (len<=0) len = 0;
	    	SubReportGrid.addOne();
	    	SubReportGrid.setRowColData(len,1,fm.SubRptNo.value);
	    	SubReportGrid.setRowColData(len,2,fm.AccDate.value);
	    	SubReportGrid.setRowColData(len,3,fm.AccPlace.value);
	    	SubReportGrid.setRowColData(len,4,fm.AccidentType.value);
	    	SubReportGrid.setRowColData(len,5,Conversion(fm.AccSubject.value));
	    	
	    	
    }

	
}

function NoticeSave()
{
	tSaveType=="Notice"
	updateCustomer()
}

function Reply()
{
	var selno = CheckGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择要审核的个人受理");
	      return ;
		}
		
		//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		var varSrc="";
		showInfo = window.open("./FrameMain.jsp?Interface=LLAppealReplyInput.jsp"+varSrc,"审核",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}