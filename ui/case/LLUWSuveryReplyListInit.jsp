<%
//Name:LLSuveryReplyInit.jsp
//function：调查员回复列表（调查员信箱）初始化页面
//author:Xx
//Date:2005-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
	function initInpBox(){
		try{

		}
		catch(ex){
			alter("在LLSuveryReplyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}

	function initSelBox(){
		try{
		}
		catch(ex){
			alert("在LLSuveryReplyInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
		}
	}

	function initForm(){
		try{
			initInpBox();
			initINPUTValue();
			initCheckGrid();
			SurveyQuery();
		}
		catch(re){
			alter("在LLSuveryReplyInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

	function initINPUTValue(){
		fm.OtherNo.value="<%=request.getParameter("caseNo")==null?"":request.getParameter("caseNo") %>";
		}
	
	function initCheckGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array("序号","30px","10","0");
			iArray[1]=new Array("案件号","90px","30","0");
			iArray[2]=new Array("调查序号","60px","30","0");
			iArray[3]=new Array("调查类别","60px","60","0");
			iArray[4]=new Array("提起日期","60px","30","0");;
			iArray[5]=new Array("提调人","60px","50","0");
			iArray[6]=new Array("案件状态","0px","30","3");
			iArray[7]=new Array("调查类别代码","0px","30","3");
			iArray[8]=new Array("调查项目号","0px","20","3");
			iArray[9]=new Array("调查报告状态","120px","50","0");
			iArray[10]=new Array("调查号","0px","30","3");
			iArray[11]=new Array("涉案金额","60px","30","0");
			CheckGrid = new MulLineEnter("fm","CheckGrid");
			CheckGrid.mulLineCount =10;
			CheckGrid.displayTitle = 1;
			CheckGrid.locked = 1;
			CheckGrid.canSel =1;
			CheckGrid.hiddenPlus=1;
			CheckGrid.hiddenSubtraction=1;
			CheckGrid.selBoxEventFuncName = "DealSurvey";
			CheckGrid.loadMulLine(iArray);

		}
		catch(ex){
			alter(ex.message);
		}
	}
</script>