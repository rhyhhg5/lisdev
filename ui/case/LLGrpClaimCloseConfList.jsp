<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-60";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLGrpClaimCloseConfList.js"></SCRIPT>
   <%@include file="LLGrpClaimCloseConfListInit.jsp"%>
   <script language="javascript">
   function initDate(){
   fm.RgtDateStart.value="<%=afterdate%>";
   fm.RgtDateEnd.value="<%=CurrentDate%>";
   var usercode="<%=Operator%>";
   fm.ManageCom.value="<%=Comcode%>";
  }
   </script>
 </head>

 <body  onload="initDate();initForm();" >
   <form action="./LLGrpClaimCloseConfListSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegisterSearch);">
         </TD>
         <TD class= titleImg>
         团体给付确认工作列表
         </TD>
       </TR>
      </table>

	    <div id= "divGrpRegisterSearch" style= "display: ''" >
		<table  class= common>
		<TR  class= common8>
		<TD  class= title8>团体批次号</TD>
		<TD  class= input8><Input class= common name="srRgtNo"  onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>团体客户号</TD>
		<TD  class= input8><Input class=common name="srCustomerNo" onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>单位名称</TD>
		<TD  class= input8><Input class=common name="srGrpName" onkeydown="QueryOnKeyDown();"></TD>
	
		</TR>
		<tr>
		<TD  class= title8>申请人</TD>
		<TD  class= input8><input class= common name="srRgtantName" onkeydown="QueryOnKeyDown();"></TD>	
		<TD  class= title8>申请日期 起始</TD>
		<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateStart onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>结束</TD>
		<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateEnd onkeydown="QueryOnKeyDown();"></TD>			
		</tr>
		</table>
	    </div>
      <table  class= common>
				<TR  class= common8>
					<TD  class= title8 colspan=3>
						<!--	<input type="radio" name="StateRadio"  value="6" onclick="SearchGrpRegister()">待处理-->
							<input type="radio" name="StateRadio"  value="1" onclick="SearchGrpRegister()">登记 
							<input type="radio" name="StateRadio"  value="2" onclick="SearchGrpRegister()">团体受理完毕
							<input type="radio" name="StateRadio"  value="3" onclick="SearchGrpRegister()">团体结案
							<input type="radio" name="StateRadio"  value="4" checked onclick="SearchGrpRegister()">通知给付
							<input type="radio" name="StateRadio"  value="5" onclick="SearchGrpRegister()">付迄 
							<input type="radio" name="StateRadio"  value="0" onclick="SearchGrpRegister()">全部批次 
				 	</TD>
				</TR>
	    </table>

   <input name="AskIn" style="display:'none'"  class=cssButton type=button value="查 询" onclick="SearchGrpRegister()">
   

    <Div  id= "divCustomerInfo" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpRegisterGrid" >
            </span>
          </TD>
        </TR>
      </table>
         <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 	
    </Div>
     <table  class= common>
			<TR  class= common8>
    			<TD  class= input8><INPUT TYPE="checkBox" NAME="PrePaidFlag" value="01"> 回销预付赔款 </TD>
    		</TR>
    </table>
   <hr>
        
<input name="AskIn" style="display:''" style='width: 80px;' class=cssButton type=button value="撤   件" onclick="DealCancel()">
<input   style="display:''" style='width: 80px;' class=cssButton type=button name='RgtGiveEnsure1' value="团体给付确认" onclick="RgtGiveEnsure()">
<input name="AskIn" style="display:''" style='width: 80px;' class=cssButton type=hidden value="团体汇总明细" onclick="GrpColPrt()">
<input name="AskIn" style="display:''" style='width: 80px;' class=cssButton type=button value="团体汇总明细" onclick="NewGrpColPrt()">
<input name="AskIn" style="display:''" style='width: 80px;' class=cssButton type=hidden value="团体给付凭证" onclick="GPrint()">
<input name="AskIn" style="display:''" style='width: 80px;' class=cssButton type=button value="团体给付凭证" onclick="NewGPrint()">
<input name="AskIn" style="display:''" style='width: 80px;' class=cssButton type=button value="给付方式修改" onclick="ChangePayMode()">
<input name="AskIn" style="display:'none'"  class=cssButton type=button value="团体给付通知" onclick="UWClaim()">
<input class="codeno" name="GRprt" CodeData="0|2^1|团体赔付报告1^2|团体赔付报告2" onclick="return showCodeListEx('grprint',[this,GRprtName],[0,1]);" onkeyup="return showCodeListKeyEx('grprint',[this,GRprtName],[0,1]);"><input class=codename  name=GRprtName>
  
     <hr>
<Div  id= "divGrpCaseInfo" style= "display: 'none'">
	  <table>
	    	<tr> 
	        <td class=common>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCaseGrid);">
	    	</td>
	    	<td class= titleImg>
	    	个人客户信息
	    	</td>
	    </tr>
	    </table>
    <Div  id= "divGrpCaseGrid" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpCaseGrid" >
            </span>
          </TD>
        </TR>
      </table>	
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">      
    </Div> 
</Div>  
     
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="operate" >
     <Input type="hidden" class= common name="ManageCom" >
      <input type=hidden id="ApplyerType" name="ApplyerType" value="0">
     <Input type="hidden" class= common name="RgtNo" >

     <Input type="hidden" class= common name="Mobile" >
     <Input type="hidden" class= common name="Email" >
     
     <Input type="hidden" class= common name="EmailContent" >
     <Input type="hidden" class= common name="SMSContent" >
     <Input type="hidden" class= common name="SendFlag" >
      
     
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
