<%
//Name：LLRegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

String LoadFlag= request.getParameter("LoadFlag");
LoadFlag= LoadFlag==null?"":LoadFlag;
String RgtNo= request.getParameter("RgtNo") ;

String LoadC="";
if(request.getParameter("LoadC")!=null)
{
LoadC = request.getParameter("LoadC");
}
%>

<script language="JavaScript">
  var loadFlag="<%=LoadFlag%>";
  var RgtNo = '<%=RgtNo%>';
  RgtNo= (RgtNo=='null'?"":RgtNo);
 
  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initForm(){
    try{
      <%GlobalInput mG = new GlobalInput();
      mG=(GlobalInput)session.getValue("GI");
      %>
      fm.Handler.value = "<%=mG.Operator%>";
      fm.ModifyDate.value = getCurrentDate();
      
      var strsql = "select caseno,rgtstate from llcase where rgtno='"+RgtNo +"'";
      var arrResult = easyExecSql(strsql);

    if ( arrResult ){
      var CaseNo= arrResult[0][0];
      var RgtState= arrResult[0][1];
    }
    
  CaseNo= (CaseNo=='null'?"":CaseNo);
  fm.RgtNo.value=RgtNo;
  fm.CaseNo.value=CaseNo;
  fm.RgtState.value=RgtState;
  initQuery();
  initAccountGrid();
  queryLLSprint();
 }
    catch(re){
      alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
    }
  }


  function initQuery(){
   
    var tRgtNo = fm.RgtNo .value;
    var tCaseNo = fm.CaseNo.value;
    ClientafterQuery(tRgtNo,tCaseNo);

  }

  function initAccountGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("团体保单号","140px","100","0");
      iArray[3]=new Array("批次号","140px","100","0");
      iArray[4]=new Array("分案号","140px","100","0");
      iArray[2]=new Array("单位名称","140px","0","0");
      iArray[5]=new Array("应付金额","140px","0","0");
      iArray[6]=new Array("实付金额","140px","0","0");
      iArray[7]=new Array("拨付金额","140px","0","0");
      iArray[8]=new Array("拨付日期","140px","0","0");
      AccountGrid = new MulLineEnter("fm","AccountGrid");
      AccountGrid.mulLineCount =3;
      AccountGrid.displayTitle = 1;
      AccountGrid.locked = 1;
      AccountGrid.canSel =0;
      AccountGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      AccountGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
      //  CaseGrid. selBoxEventFuncName = "onSelSelected";
      AccountGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alter(ex);
    }
  }

</script>