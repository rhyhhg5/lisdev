//               该文件中包含客户端需要处理的函数和事件


var showInfo;
var mDebug="1";

//提交，保存按钮对应操作
function submitForm()
{
	if(fm.RemarkType.value=='')
	{
		alert("续保建议类型不能为空");
		return false;
	}
	if(fm.Remark.value=='')
	{
		alert("续保建议不能为空");
		return false;
	}
  	fm.fmtransact.value = "INSERT||RENEWS";
    var showStr="正在执行操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LLRenewSuggestSave.jsp";
    fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
     parent.fraMain.rows = "0,0,0,0,*";
 	}
}

function querySuggest()
{
	sql = "select Remark,RemarkType from LLRenewOpinion where caseno = '"+fm.CaseNo.value+"' and customerno = '"+fm.CustomerNo.value+"'";

		arrResult = easyExecSql(sql);
		if(arrResult)
		{
			fm.Remark.value=arrResult[0][0];
			fm.RemarkType.value=arrResult[0][1];
			if(fm.RemarkType.value==1)
				fm.RemarkTypeName.value="道德危险建议";
			else
				fm.RemarkTypeName.value="疾病现状描述";
		}
}
