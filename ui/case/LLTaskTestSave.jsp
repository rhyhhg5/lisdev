<%
//程序名称：ClaimSave.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：wujs
//更新记录：  更新人    更新日期     更新原因/内容

%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page import=" java.text.*"%>
<%@page import= "java.util.*"%>


<%
//接收信息，并作校验处理。
//输入参数
String oper = request.getParameter("Opt");
String FlagStr = "";
String Content = "";

	//#3120，深圳提数FTP服务器的相关软件开发，声明引用变量 start
	LLSZTransFtpTask mLLSZTransFtpTask = new LLSZTransFtpTask();
	// #3120 end 
	//#3366,深圳FTP理赔数据每10分钟一次 start
	LLSZLPTransFtpTask mLLSZLPTransFtpTask = new LLSZLPTransFtpTask();
	//#3366,深圳FTP理赔数据每10分钟一次 end
  LLWBRgtTask tLLWBRgtTask   = new LLWBRgtTask();
  LLWBCaseTask tLLWBCaseTask   = new LLWBCaseTask();
  LLWBImageTask tLLWBImageTask   = new LLWBImageTask();
  LLWBCroeResultTask tLLWBCroeResultTask = new LLWBCroeResultTask();
  LLWBCorePast tLLWBCorePast = new LLWBCorePast();
  LLCustomerMsgTaskTest tLLCustomerMsgTaskTest= new LLCustomerMsgTaskTest();
  
//#3120，深圳提数FTP服务器的相关软件开发，对应的if语句 start
if (oper.equals("ftp")) {
	mLLSZTransFtpTask.run();
	FlagStr = mLLSZTransFtpTask.getOpr();
}
// #3120 end
//#3366 理赔数据上报 start
if(oper.equals("ftplp")){
	mLLSZLPTransFtpTask.run();
	FlagStr = mLLSZLPTransFtpTask.getOpr();
}
//#3366 理赔数据上报 end
if(oper.equals("testMessage")){
	tLLCustomerMsgTaskTest.run();
	FlagStr =tLLCustomerMsgTaskTest.getOpr();
}

if(oper.equals("TYImage")){
	LLTYDealImage tLLTYDealImage = new LLTYDealImage();
	tLLTYDealImage.run();
	FlagStr =tLLTYDealImage.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}


//#3265，河南分公司向保监每日报送数据报表调整
LPMeiriTask mLPMeiriTask = new LPMeiriTask();
if(oper.equals("gsd")) {
	mLPMeiriTask.run();
	FlagStr = mLPMeiriTask.getOpr();
}

 if(oper.equals("rgt")) {
	 tLLWBRgtTask.run();
 FlagStr=tLLWBRgtTask.getOpr();}
 if(oper.equals("case")) {
	 tLLWBCaseTask.run();
 FlagStr=tLLWBRgtTask.getOpr();}
 if(oper.equals("image")) {
	 tLLWBImageTask.run();
 FlagStr=tLLWBRgtTask.getOpr();
 }
 if(oper.equals("result")) 
	 tLLWBCroeResultTask.run();
 if(oper.equals("cpresult")) 
	 tLLWBCorePast.run();
if(FlagStr.equals("true"))
	 Content="成功";
else
	 Content="失败";
 if(oper.equals("onecard")){
	 LLClaimOneCardTask oct=new LLClaimOneCardTask();
	 oct.run();
	 FlagStr=oct.getOpr();
 }
if(oper.equals("onecard")){
	 if("true".equals(FlagStr) || FlagStr=="true" ){
		 Content="处理成功";
	 }else{
		 Content="处理失败，详细需要查看日志或返回报文";
	 }
}
//医保通天津社保理赔批处理
if(oper.equals("ybtTJSB")){
	LLTJSSRegisterTask ybtTJSB=new LLTJSSRegisterTask();
	ybtTJSB.run();
	FlagStr=ybtTJSB.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
//税优批处理
if(oper.equals("uploadClaim")){
	LLUploadClaimTask uploadClaim=new LLUploadClaimTask();
	uploadClaim.run();
	FlagStr=uploadClaim.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
//浙江
if(oper.equals("zjwb1")){
	LLZWRgtTask tLLZWRgtTask=new LLZWRgtTask();
	tLLZWRgtTask.run();
	FlagStr=tLLZWRgtTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("zjwb2")){
	LLZBCaseTask tLLZBCaseTask=new LLZBCaseTask();
	tLLZBCaseTask.run();
	FlagStr=tLLZBCaseTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
 //上海start
if(oper.equals("shhw1")){
	LLHWRgtTask tLLHWRgtTask=new LLHWRgtTask();
	tLLHWRgtTask.run();
	FlagStr=tLLHWRgtTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}

if(oper.equals("shhw2")){
	LLHWCaseTask tLLHWCaseTask=new LLHWCaseTask();
	tLLHWCaseTask.run();
	FlagStr=tLLHWCaseTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("shhwsmj")){
	LLHWImageTask tLLHWImageTask=new LLHWImageTask();
	tLLHWImageTask.run();
	FlagStr=tLLHWImageTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("shhw3")){
	LLHWCroeResultTask tLLHWCroeResultTask=new LLHWCroeResultTask();
	tLLHWCroeResultTask.run();
	FlagStr=tLLHWCroeResultTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}

if(oper.equals("shhw4")){
	LLHWCorePast tLLHWCorePast=new LLHWCorePast();
	tLLHWCorePast.run();
	FlagStr=tLLHWCorePast.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
//上海end 
if(oper.equals("zjsmj")){//未知
	LLZBImageTask tLLZBImageTask=new LLZBImageTask();
	tLLZBImageTask.run();
	FlagStr=tLLZBImageTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("zjwb3")){
	LLZBCroeResultTask tLLZBCroeResultTask=new LLZBCroeResultTask();
	tLLZBCroeResultTask.run();
	FlagStr=tLLZBCroeResultTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("zjwb4")){
	LLZBCorePast tLLZBCorePast=new LLZBCorePast();
	tLLZBCorePast.run();
	FlagStr=tLLZBCorePast.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
 
if(oper.equals("sztb")){
	WBCBXmlTrans tWBCBXmlTrans=new WBCBXmlTrans();
	tWBCBXmlTrans.run();
	 FlagStr=tWBCBXmlTrans.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 } 
}

if(oper.equals("szbq")){
	LpOutSendBQSolveData tLpOutSendBQSolveData=new LpOutSendBQSolveData();
	tLpOutSendBQSolveData.run();
	 FlagStr=tLpOutSendBQSolveData.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 } 
}


if(oper.equals("zjtb")){
	ZBCBXmlTrans tZBCBXmlTrans=new ZBCBXmlTrans();
	tZBCBXmlTrans.run();
	FlagStr=tZBCBXmlTrans.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("zjbq")){
	ZLpOutSendBQSolveData tZLpOutSendBQSolveData=new ZLpOutSendBQSolveData();
	tZLpOutSendBQSolveData.run();
	FlagStr=tZLpOutSendBQSolveData.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("jiean")){
	LLHWCorePastTask tLLHWCorePastTask=new LLHWCorePastTask();
	tLLHWCorePastTask.run();
	FlagStr=tLLHWCorePastTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
 if(oper.equals("tytb")){
	TYWBCBXmlTrans tTYWYCBXmlTrans=new TYWBCBXmlTrans();
	tTYWYCBXmlTrans.run();
	FlagStr=tTYWYCBXmlTrans.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("tybq")){
	TYLpOutSendBQSolveData tTYpOutSendBQSolveData=new TYLpOutSendBQSolveData();
	tTYpOutSendBQSolveData.run();
	FlagStr=tTYpOutSendBQSolveData.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("tyjiean")){
	LLTYCorePastTask tLLTYCorePastTask=new LLTYCorePastTask();
	tLLTYCorePastTask.run();
	FlagStr=tLLTYCorePastTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
} 
//3827 成都数联易康密钥接口
if(oper.equals("scslgetkey")){
	LLHttpClientHandleTask tLLHttpClientHandleTask=new LLHttpClientHandleTask();
	tLLHttpClientHandleTask.run();
	FlagStr=tLLHttpClientHandleTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
//3827 成都数联易康诊疗信息接口
if(oper.equals("scslgetdata")){
	LLHttpClientHandleContentTask tLLHttpClientHandleContentTask=new LLHttpClientHandleContentTask();
	tLLHttpClientHandleContentTask.run();
	FlagStr=tLLHttpClientHandleContentTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
} 


//沛合批次多事件3866

if(oper.equals("phtp1")){
	LLTPRgtTask tLLTPRgtTask=new LLTPRgtTask();
	tLLTPRgtTask.run();
	FlagStr=tLLTPRgtTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}

if(oper.equals("phtp2")){
	LLTPCaseTask tLLTPCaseTask=new LLTPCaseTask();
	tLLTPCaseTask.run();
	FlagStr=tLLTPCaseTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("phtp3")){
	LLTPCroeResultTask tLLTPCroeResultTask=new LLTPCroeResultTask();
	tLLTPCroeResultTask.run();
	FlagStr=tLLTPCroeResultTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}

if(oper.equals("phtp4")){
	LLTPCorePast tLLTPCorePast=new LLTPCorePast();
	tLLTPCorePast.run();
	FlagStr=tLLTPCorePast.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
//3866

// 2194 天津大病 start
if(oper.equals("tjdb1")){
	LLTPRgtTJTask tLLTPRgtTJTask=new LLTPRgtTJTask();
	tLLTPRgtTJTask.run();
	FlagStr=tLLTPRgtTJTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("tjdb2")){
	LLTPCaseTJTask tLLTPCaseTJTask=new LLTPCaseTJTask();
	tLLTPCaseTJTask.run();
	FlagStr=tLLTPCaseTJTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}
if(oper.equals("tjdb3")){
	LLTPCaseDiseasesTJTask tLLTPCaseDiseasesTJTask=new LLTPCaseDiseasesTJTask();
	tLLTPCaseDiseasesTJTask.run();
	FlagStr=tLLTPCaseDiseasesTJTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}

// 2194 天津大病 end
//线上理赔状态推送
if(oper.equals("OCPush")){
	LLOnlinCaseStatePushTask tLLOnlinCaseStatePushTask=new LLOnlinCaseStatePushTask();
	tLLOnlinCaseStatePushTask.run();
	FlagStr=tLLOnlinCaseStatePushTask.getOpr();
	if("true".equals(FlagStr)){
		 Content="流程成功完成";
	 }else{
		 Content="处理过程中出现错误";
	 }
}

%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
