<%
//Name：RegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
		String LoadC = "";
		if(request.getParameter("LoadC")!=null)
		{
			LoadC = request.getParameter("LoadC");
		}
%>

<script language="JavaScript">

function initInpBox( )
{
  try {
     fm.reset();
     fm.CaseNo.value ="<%=request.getParameter("CaseNo")%>"
  } catch(ex) {
//    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!"+ex.message);
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
//    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常yyyyyyy:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
//    initInpBox();
    ////出险内容
		initClientGrid();
    initDiseaseGrid();
//    initDeformityGrid();
//    initSeriousDiseaseGrid();
//    initDegreeOperationGrid();
 }
  catch(ex)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

function initDiseaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         		//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              	 //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array("临床诊断","150px","100",1);
    iArray[2]=new Array("疾病名称","150px","100",1);     //,"diseasname","2|3","0|1"
    
    iArray[3]=new Array();
    iArray[3][0]="疾病代码";  //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="45px";      //列宽
    iArray[3][2]=100;         //列最大值
    iArray[3][3]=0;           //是否允许输入,1表示允许，0表示不允许3表示隐藏
    
    iArray[1]=new Array("案件号","100px","100",0);
    iArray[6]=new Array("理赔金额","60px","100",0);
    iArray[5]=new Array("手术信息","80px","100",0);
    iArray[7]=new Array("备注","0px","100",3);
    
    
    
    DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" );
    DiseaseGrid.mulLineCount = 0;
    DiseaseGrid.displayTitle = 1;
    DiseaseGrid.hiddenPlus=1;
    DiseaseGrid.hiddenSubtraction=1;
    DiseaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}
function initSeriousDiseaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="重疾名称";         			//列名
    iArray[1][1]="240px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;
   
    iArray[2]=new Array();
    iArray[2][0]="重疾代码";         			//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=160;            			//列最大值
    iArray[2][3]=0;
    
     iArray[3]=new Array("重疾责任依据","80px","100",0);
     iArray[4]=new Array("诊治医院","0px","100",3);
     iArray[5]=new Array("治疗医生","0px","100",3);
	   iArray[6]=new Array("重症监护天数","0px","4",3);
    
    SeriousDiseaseGrid = new MulLineEnter( "fm" , "SeriousDiseaseGrid" );
    SeriousDiseaseGrid.mulLineCount = 0;
    SeriousDiseaseGrid.displayTitle = 1;
    SeriousDiseaseGrid.hiddenPlus=1;
    SeriousDiseaseGrid.hiddenSubtraction=1;
    SeriousDiseaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}

//意外信息
function initDeformityGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;

     iArray[1]=new Array("意外名称","340px","100",0);
     iArray[2]=new Array("意外代码","80px","100",0);
     iArray[3]=new Array("疾病等级","60px","100",0);
     iArray[4]=new Array("责任依据","250px","100",0);
     iArray[5]=new Array("确认依据","0px","100",3);
     iArray[6]=new Array("鉴定日期","0px","100",3);
     iArray[7]=new Array("合法认证机构","0px","20",3);
     iArray[8]=new Array("合法认证机构代码","0px","0",3);
     
    DeformityGrid = new MulLineEnter( "fm" , "DeformityGrid" );
    DeformityGrid.mulLineCount = 0;
    DeformityGrid.displayTitle = 1;
    DeformityGrid.hiddenPlus=1;
    DeformityGrid.hiddenSubtraction=1;
    DeformityGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}

//初始化按档次录入初始化信息
function initDegreeOperationGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         //列宽
    iArray[0][2]=10;          	 //列最大值
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="手术名称";    //列名
    iArray[1][1]="240px";       //列宽
    iArray[1][2]=100;           //列最大值
    iArray[1][3]=0;


    iArray[2]=new Array();
    iArray[2][0]="手术代码";    //列名
    iArray[2][1]="80px";       //列宽
    iArray[2][2]=100;           //列最大值
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="手术费用";    //列名
    iArray[3][1]="0px";        //列宽
    iArray[3][2]=100;           //列最大值
    iArray[3][3]=3;

    iArray[4]=new Array();
    iArray[4][0]="手术等级";    //列名
    iArray[4][1]="80px";       //列宽
    iArray[4][2]=100;           //列最大值
    iArray[4][3]=1 ;

    DegreeOperationGrid = new MulLineEnter( "fm" , "DegreeOperationGrid" );
    DegreeOperationGrid.mulLineCount = 0;
    DegreeOperationGrid.displayTitle = 1;
    DegreeOperationGrid.canSel = 0;
    DegreeOperationGrid.hiddenPlus=1;
    DegreeOperationGrid.hiddenSubtraction=1;
    DegreeOperationGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在DegreeOperation中出错");
    alert(ex);
  }
}

function initClientGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         //列宽
    iArray[0][2]=10;          	 //列最大值
    iArray[0][3]=0;


    iArray[1]=new Array();
    iArray[1][0]="姓名";    //列名
    iArray[1][1]="50px";       //列宽
    iArray[1][2]=100;           //列最大值
    iArray[1][3]=0;


    iArray[2]=new Array();
    iArray[2][0]="身份证号";    //列名
    iArray[2][1]="0px";       //列宽
    iArray[2][2]=100;           //列最大值
    iArray[2][3]=3;

    iArray[3]=new Array();
    iArray[3][0]="性别";    //列名
    iArray[3][1]="25px";        //列宽
    iArray[3][2]=100;           //列最大值
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="年龄";    //列名
    iArray[4][1]="25px";       //列宽
    iArray[4][2]=100;           //列最大值
    iArray[4][3]=0 ;

    iArray[5]=new Array();
    iArray[5][0]="事件号";    //列名
    iArray[5][1]="0px";       //列宽
    iArray[5][2]=500;           //列最大值
    iArray[5][3]=3 ;
                                          
    iArray[6]=new Array();                
    iArray[6][0]="IDNo";    //列名        
    iArray[6][1]="0px";       //列宽     
    iArray[6][2]=20;           //列最大值
    iArray[6][3]=3 ;                        
    
    iArray[7]=new Array("理赔号","110px","30",0);
    iArray[8]=new Array("事件信息","150px","30",0);
    iArray[9]=new Array("发生日期","70px","30",0); 
    iArray[10]=new Array("发生地点","70px","30",0); 
    iArray[11]=new Array("入院日期","70px","30",0); 
    iArray[12]=new Array("出院日期","70px","30",0); 
                                          
    ClientGrid = new MulLineEnter( "fm" , "ClientGrid" );
    ClientGrid.mulLineCount = 1;
    ClientGrid.displayTitle = 1;
    ClientGrid.canSel = 1;
    ClientGrid.hiddenPlus=1;
    ClientGrid.hiddenSubtraction=1;
    ClientGrid.loadMulLine(iArray);
    ClientGrid. selBoxEventFuncName = "initFM";
  }
  catch(ex)
  {
    alert("在DegreeOperation中出错");
    alert(ex);
  }
}
</script>