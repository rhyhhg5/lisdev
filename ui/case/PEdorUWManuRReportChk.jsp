<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuRReportChk.jsp
//程序功能：保全人工核保生存调查报告录入
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%
  String flag;
  String content;
  
  GlobalInput gi = (GlobalInput)session.getValue("GI");	 
	String tSerialNo[] = request.getParameterValues("InvestigateGridNo");
	String tRReportItemCode[] = request.getParameterValues("InvestigateGrid1");
	String tRReportItemName[] = request.getParameterValues("InvestigateGrid2");
	String tRRItemContent[] = request.getParameterValues("InvestigateGrid3");
  int ChkCount = 0;
  if(tSerialNo != null)
  {		
    ChkCount = tSerialNo.length; 
  }
  
  LPRReportSchema tLPRReportSchema = new LPRReportSchema();
  tLPRReportSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPRReportSchema.setContNo(request.getParameter("ContNo"));
  tLPRReportSchema.setCustomerNo(request.getParameter("InsuredNo"));
  tLPRReportSchema.setName(request.getParameter("InsuredName"));
  tLPRReportSchema.setContente(request.getParameter("Contente"));
  
  LPRReportItemSet tLPRReportItemSet = new LPRReportItemSet();
  for (int i = 0; i < ChkCount; i++)
  {
    LPRReportItemSchema tLPRReportItemSchema = new LPRReportItemSchema();
    tLPRReportItemSchema.setRReportItemCode(tRReportItemCode[i]);
    tLPRReportItemSchema.setRReportItemName(tRReportItemName[i]);
    tLPRReportItemSchema.setRRItemContent(tRRItemContent[i]);
    tLPRReportItemSet.add(tLPRReportItemSchema);
  }
  
  VData data = new VData();
  data.add(gi);
  data.add(tLPRReportSchema);
  data.add(tLPRReportItemSet);
  PEdorUWManuRReportUI tPEdorUWManuRReportUI = new PEdorUWManuRReportUI();
  if (!tPEdorUWManuRReportUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tPEdorUWManuRReportUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！契调通知书号为" + tPEdorUWManuRReportUI.getPrtNo()+"\\n"+"保单批次号为" + tPEdorUWManuRReportUI.getContPrtNo();
  }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>
