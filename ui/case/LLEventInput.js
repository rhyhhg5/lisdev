var showInfo;
var mDebug="1";
var tSaveFlag = "0";
//提交，保存按钮对应操作
function submitForm()
{
  if(tSaveFlag=="1")
  {
    alert("您不能执行保存操作！");
    return;
  }
  else
  {
    if (confirm("您确实想保存该记录吗?"))
    {
      tSaveFlag = "1";

      //if (fm.RptNo.value!="")
      //{
      //  alert("以有该报案记录,不能进行保存操作!");
      //  return ;
      //}
      //if ((fm.RptObj.value=="")||(fm.RptObj.value=="null"))
      //{
      //  alert ("请您输入号码类型!");
      //  return;
      //}
      if ((fm.CustomerNo.value=="")||(fm.CustomerNo.value=="null"))
      {
        alert ("请您输入号码!");
        return;
      }
      if ((fm.CustomerName.value=="")||(fm.CustomerName.value=="null"))
      {
        alert ("请您输入报案人姓名!");
        return;
      }
      if((fm.AccDate.value=="")||(fm.AccDate.value=="null"))
      {
        alert("请您录入出险日期");
        return;
      }
      else
      {
        var i = 0;
        if( verifyInput2() == false ) return false;
        fm.fmtransact.value="INSERT||MAIN";
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        showSubmitFrame(mDebug);
        fm.action = './LLEventSave.jsp';
        fm.submit(); //提交
        tSaveFlag ="0";

      }
    }
    else
    {
      alert("您取消了修改操作！");
    }
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (confirm("您确实想修改该记录吗?"))
 	{
 	
 		//if ((fm.RptObj.value=="")||(fm.RptObj.value=="null"))
	  	//{
	  	//	alert ("请您输入号码类型!");
	  	//	return;
	  	//}
  	//if ((fm.RptObjNo.value=="")||(fm.RptObjNo.value=="null"))
	//  	{
	//  		alert ("请您输入号码!");
	//    	return;
	//  	}
  	//if ((fm.RptorName.value=="")||(fm.RptorName.value=="null"))
	//  	{
	//  		alert ("请您输入报案人姓名!");
	//  		return;
	//  	}
  	//if ((fm.RptDate.value=="")||(fm.RptDate.value=="null"))
	//  	{
	//  		alert ("请您输入报案日期!");
	//  		return;
	   var i = 0;
      
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      showSubmitFrame(mDebug);
       if( verifyInput2() == false ) return false;
      fm.fmtransact.value = "UPDATE||MAIN"
      fm.action = './LLEventSave.jsp';
      fm.submit(); //提交
    //end of else
  }//end of if
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{

  window.open("./LLEventQueryInput.jsp");
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function deleteClick()
{
	if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.action = './LLEventSave.jsp';
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function submitForm1()
{
  tSaveFlag = "0";
	if((fm.PeopleType.value=="")||(fm.PeopleType.value=="null"))
	{
		alert("请您录入事故者类型！！！");
		return;
	}
	if((fm.RptObj.value=="")||(fm.RptObj.value=="null"))
	{
		alert("请您录入号码类型！！！");
		return;
	}
	if (fm.RptObjNo.value=="")
	{
		alert("请您收入要号码！");
 		return ;
  }
  if(fm.RptObj.value=="0")
  {
  	if(fm.PeopleType.value!="0")
  	{
  		alert("号码类型是团单，事故者类型只能是被保险人，请重新录入事故者类型");
  		return;
  	}
  	else
  	{
  		var i = 0;
   		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  		initSubReportGrid();
  		fm.action = "./ReportQueryOut1.jsp";
  		fm.submit(); //提交
  	}
  }
  else
  {
  	var i = 0;
   	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	initSubReportGrid();
  	fm.action = "./ReportQueryOut1.jsp";
  	fm.submit(); //提交
  }
}
//完成按照”客户号“在lcpol中进行查询，显示该客户的保单明细
function showInsuredLCPol()
{
  var row;
	var a_count=0;//判断选中了多少行
  var t = SubReportGrid.mulLineCount;//得到Grid的行数
  for(var i=0;i<t;i++)
  {
    varCount = SubReportGrid.getChkNo(i);
    if(varCount==true)
    {
       a_count++;
       row=i;
    }
  }
  if(a_count>1)
  {
    alert("您只能选中一行记录！");
    return;
  }
  else if(a_count<1)
  {
    alert("请您选中一条分案记录!");
    return;
  }
  else
  {
    var varInsuredNo;
    var varCount;
    varInsuredNo=SubReportGrid.getRowColData(row,1);
    if ((varInsuredNo=="null")||(varInsuredNo==""))
    {
      alert("客户号为空，不能进行查询操作！");
      return;
    }
    var varSrc = "&InsuredNo=" + SubReportGrid.getRowColData(row,1);
    var newWindow = window.open("./FrameMainReportLCPol.jsp?Interface=ReportLCPolInput.jsp"+varSrc,"ReportLCPolInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}

function showCustomerInfo()
{
	window.open("../sys/FrameCQPersonQuery.jsp");
}
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		var strSQL="select * from LLSubReport where SubRptNo='"+arrQueryResult[0][0]+"'";
		
		arrResult =easyExecSql(strSQL);
		
        fm.all('SubRptNo').value= arrResult[0][0];
        fm.all('CustomerNo').value= arrResult[0][1];
        fm.all('CustomerName').value= arrResult[0][2];
        fm.all('CustomerType').value= arrResult[0][3];
        fm.all('AccSubject').value= arrResult[0][4];
        fm.all('AccidentType').value= arrResult[0][5];
        fm.all('AccDate').value= arrResult[0][6];
        fm.all('AccEndDate').value= arrResult[0][7];
        fm.all('AccDesc').value= arrResult[0][8];
        //fm.all('CustSituation').value= arrResult[0][9];
        fm.all('AccPlace').value= arrResult[0][10];
        fm.all('HospitalCode').value= arrResult[0][11];
        fm.all('HospitalName').value= arrResult[0][12];
        fm.all('InHospitalDate').value= arrResult[0][13];
        fm.all('OutHospitalDate').value= arrResult[0][14];
        //fm.all('Remark').value= arrResult[0][15];
        fm.all('SeriousGrade').value= arrResult[0][16];
        //fm.all('SurveyFlag').value= arrResult[0][17];
        //fm.all('Operator').value= arrResult[0][18];
        //fm.all('MngCom').value= arrResult[0][19];
        fm.all('MakeDate').value= arrResult[0][20];
        fm.all('MakeTime').value= arrResult[0][21];
        //fm.all('ModifyDate').value= arrResult[0][22];
        //fm.all('ModifyTime').value= arrResult[0][23];
	}
}