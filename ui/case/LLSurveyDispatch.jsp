<html>
	<%
	//Name：LLSurveyDispatch.jsp
	//Function：调查分配
	//Date：2004-12-23 16:49:22
	//Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLSurveyDispatch.js"></SCRIPT>
		<%@include file="LLSurveyDispatchInit.jsp"%>
	</head>
	<body  onload="initForm();">
		<form action="./LLSurveyDispatchSave.jsp" method=post name=fm target="fraSubmit">

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSurveyBox);">
					</TD>
					<TD class= titleImg>
						调查主管信箱
					</TD>
				</TR>
			</table>

			<Div  id= "divSurveyBox" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD class= title8>调查机构</TD>
						<TD class= input8><Input class="codeno" name=OrganCode onclick="return showCodeList('comcode',[this,OrganName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,OrganName],[0,1],null,null,null,1);"><input class=codename name=OrganName></TD>
						<TD class= title8>调查主管</TD>
						<TD class= input8><input class= codeno name="Inspector" onclick="Dispatch();return showCodeList('optname',[this,InspectorName], [0,1],null,Str1,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,InspectorName], [0,1],null,Str1,'1');"  onkeydown="QueryOnKeyDown()"><input class=codename name= InspectorName></TD>
						<TD class= title8>调查员</TD>
						<TD class= input8><input class= codeno name="Replyer" onclick="Dispatch();return showCodeList('optname',[this,ReplyerName], [0,1],null,Str,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,ReplyerName], [0,1],null,Str,'1');"  onkeydown="QueryOnKeyDown()"><input class=codename name= ReplyerName></TD>
					</TR>
					<TR  class= common8>
						<TD class= title8>案件号</TD>
						<TD class= input8><input class= common name="OtherNo" onkeydown="QueryOnKeyDown()"></TD>
						<TD class= title8>受理日期从</TD>
						<TD class= input8><input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
						<TD class= title8>到</TD>
						<TD class= input8><input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
					</TR>
				</table>
				<table  class= common id=divConfCondition style="display:'none'">
					<TR  class= common8>
						<TD class= title8>调查类别</TD>
						<TD class= input8><input class= codeno name="SurveyType" CodeData= "0|^0|即时调查^1|一般调查^2|再次调查" onClick="showCodeListEx('SurveyType',[this,SurveyTypeName],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('SurveyType',[this,SurveyTypeName],[0,1],'', '', '', 1);"><input class=codename name=SurveyTypeName></TD>
						<TD class= title8>提起日期从</TD>
						<TD class= input8><input class= 'coolDatePicker' dateFormat="Short" name="SurveyStartDate" onkeydown="SurveyQuery();"></TD>
						<TD class= title8>到</TD>
						<TD class= input8><input class= 'coolDatePicker' dateFormat="Short" name="SurveyEndDate" onkeydown="SurveyQuery();"></TD>
					</TR>
					<TR  class= common8>
						<TD class= title8>调查状态</TD>
						<TD class= input8><Input class= "codeno"  name=SurveyFlag CodeData= "0|^1|未回复^2|已回复" onkeydown="QueryOnKeyDown();" onclick="return showCodeListEx('surveyflag',[this,SurveyFlagName], [0,1]);" onkeyup="return showCodeListKeyEx('surveyflag', [this,SurveyFlagName], [0,1]);" ><input class= codename name='SurveyFlagName'></TD>
						<TD class= title8>分配日期从</TD>
						<TD class= input8><input class= 'coolDatePicker' dateFormate="Short" name="DispatchStartDate" ></TD>
						<TD class= title8>到</TD>
						<TD class= input8><input class= 'coolDatePicker' dateFormate="Short" name="DispatchEndDate"></TD>
					</TR>
				</table>
				<table class= common>
					<TR class= common>
						<TD  class= title8 colspan=2>
							<input type="radio" checked name="typeRadio"  value="1" onclick="easyQuery()" >待分配
							<input type="radio" name="typeRadio"  value="2" onclick="easyQuery()" >已分配
							<input type="radio" name="typeRadio"  value="3" onclick="easyQuery()" >未复核
							<input type="radio" name="typeRadio"  value="4" onclick="easyQuery()" >已复核
							<input type="radio" name="typeRadio"  value="0" onclick="easyQuery()" >全部调查案件
						</TD>
					</TR>
				</table>
			</Div>
			
			<Div  id= "divSurveyList" align=center style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left>
							<span id="spanCheckGrid" >
							</span>
						</TD>
					</TR>
				</table>
				<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();">
				<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
				<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
				<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
			</Div>
				<br>

				<!--新增加的下拉框-->
				<div id="divDispatch" align=left>
					<Div  id= "divDealer" style= "display: ''">
						<table  class= common>
							<TR  class= common  id=idbranch>
								<TD  class= title8 >处理</TD>
								<TD  class= input8 > <input class=codeno CodeData="0|3^0|下发^1|上报"  name=DealWith onclick="return showCodeListEx('DealType',[this,DealWithName],[0,1]);" onkeyup="return showCodeListKeyEx('DealType',[this,DealWithName],[0,1]);" readonly><input class=codename name=DealWithName></TD>
								<TD  class= title8>处理人</TD>
								<TD><Input class= "codeno"  name=Surveyer ondblclick="Dispatch();return showCodeList('optname',[this,SurveyerName], [0,1],null,Str,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,SurveyerName], [0,1],null,Str,'1');" readonly ><Input class=codename  name="SurveyerName"></TD>
								<TD  class= title8 name=titleQuan>未处理案件</TD>
								<TD><Input class= readonly  name=Quantity readonly ></TD>
							</tr>
							<TR  class= common id=idhq>
								<TD  class= title8 >调查机构</TD>
								<TD  class= input8 > <input class=codeno  name=ComCode onclick="return showCodeList('comcode',[this,ComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1]);" readonly><input class=codename name=ComName></TD>
								<TD  class= title8>处理人</TD>
								<TD><Input class= "codeno"  name=Dealer ondblclick="Dispatch();return showCodeList('optname',[this,DealerName], [0,1],null,Str1,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,DealerName], [0,1],null,Str1,'1');" readonly ><Input class=codename  name="DealerName"></TD>
							</tr>
						</table>
					</div>
				</Div>
				<div id="divconf" align=left style="display:'none'">
					<input class=cssButton style="width:70px"  type=button value= "审  核" name="AskIn" onclick="SurveyReply()">
					<input class=cssButton style="width:70px"  type=button value="直接调查费" name="InqFee" onclick="SurveyFee()">
					<input class=cssButton style="width:70px"  type=button value="间接调查费" name="Additional" onclick="AddFee()">
					<input class=cssButton style="width:70px"  type=button value="调查费汇总" name="FeeGive" onclick="SumFee()">
					<input class=cssButton style="width:70px"  type=button value="调查费结算" name="SumUp" onclick="GiveEnsure()">
				</div>
						<table>
						   <TR>
						   	<TD>
						   	 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRemark);">
						   	</TD>
						   	<TD class= titleImg>
						   	   调查指导意见
						   	</TD>
						   </TR>
						 </table>
						 <div id="divRemark" style= "display: ''">
						<table  class= common>
						     <TR  class= common>
							       <TD  class= title>留言记录</TD>
						     </TR>
						     <TR  class= common>
							       <TD  class= input>
								          <textarea name="result" cols="100%" rows="5" witdh=25% class="common" readonly="true" ></textarea>
							       </TD>
						     </TR>
						     <TR  class= common>
							       <TD  class= title>新发言</TD>
						     </TR>
						     <TR  class= common>
							       <TD  class= input>
								          <textarea name="newResult" cols="100%" rows="2" witdh=25% class="common"></textarea>
							       </TD>
						     </TR>
					  </table>
					  </Div>
					<br>
					<div id="divComfirm" align=left style="display:''">
					<INPUT VALUE="确  认" class= cssButton name='comfirm1' TYPE=button onclick="submitForm();">
					<INPUT VALUE="返  回" class= cssButton TYPE=button onclick="top.close();">
					</div>

			<!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
			<!--隐藏域-->
			<Input type="hidden" class= common name="fmtransact" >
			<Input type="hidden" class= common name="selno" >
			<Input type="hidden" class= common name="hdlComCode" >
			<Input type="hidden" class= common name="tSurveyNo" >
			<Input type="hidden" class= common name="RgtNo" >
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
