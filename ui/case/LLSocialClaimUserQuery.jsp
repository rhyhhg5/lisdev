<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	GlobalInput GI = new GlobalInput();
	GI = (GlobalInput) session.getValue("GI");
%>
<script>
  var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
  var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="LLSocialClaimUserQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LLSocialClaimUserQueryInit.jsp"%>
<title>理赔社保业务用户信息查询</title>
<script language="javascript">
   function initDate()
   {
      fm.Station.value=manageCom;
   }
   </script>
</head>
<body onload="initForm();initDate();initElementtype();">
<form action="" method=post name=fm target="fraSubmit">
<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLLReport1);"></td>
		<td class=titleImg>请您输入查询条件：</td>
	</tr>
</table>

<Div id="divLLReport1" style="display: ''">
<table class=common>
	<TR>
		<TD class=title>用户机构代码</TD>
		<TD class=input><Input class="code" name=Station
			verify="机构代码|notnull&code:ComCode"
			ondblclick="return showCodeList('ComCode',[this, StationName], [0, 1]);"
			onkeyup="return showCodeListKey('ComCode', [this, StationName], [0, 1]);" elementtype=nacessary>
		</TD>
		<TD class=title>用户机构名称</TD>
		<TD class=input><Input class="readonly" readonly name=StationName>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>用户代码</TD>
		<TD class=input><Input class="common" name=UserCode></TD>
		<TD class=title>用户名称</TD>
		<TD class=input><Input class="common" name=UserName></TD>
	</TR>
	<TR>
		<TD class=title>参加案件分配</TD>
		<TD class=input><Input class=codeno name=HandleFlag
			CodeData="0|^0|否^1|是"
			ondblClick="showCodeListEx('HandleFlag_1',[this,HandleFlagName],[0,1]);"
			onkeyup="showCodeListKeyEx('HandleFlag_1',[this,HandleFlagName],[0,1]);"><Input
			class=codename name=HandleFlagName ></TD>
	</TR>
	<TR class=common>
	<TD>	<input type=hidden id="fmtransact" name="fmtransact"></TD>
	</TR>
</table>
</Div>
<INPUT class=cssButton VALUE="查  询" TYPE=button onclick="submitForm();return false;"> 
<INPUT class=cssButton VALUE="返  回" TYPE=button onclick="returnParent();">
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol1);"></td>
		<td class=titleImg>理赔社保业务用户信息</td>
	</tr>
</table>
<Div id="divLLReport2" style="display: ''">
<table class=common>
	<tr class=common>
		<td text-align: left colSpan=1><span id="spanRegisterGrid">
		</span></td>
	</tr>
</table>
<INPUT VALUE="首  页" TYPE=button class=cssButton onclick="turnPage.firstPage();">
<INPUT VALUE="上一页" TYPE=button class=cssButton onclick="turnPage.previousPage();">
<INPUT VALUE="下一页" TYPE=button class=cssButton onclick="turnPage.nextPage();">
<INPUT VALUE="尾  页" TYPE=button class=cssButton onclick="turnPage.lastPage();">
</div>
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
