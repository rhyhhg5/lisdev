<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%> 
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<html>
<head>
<script>
function initDate(){
   fm.RgtDateS.value="<%=afterdate%>";
   fm.RgtDateE.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
   fm.Handler.value="<%=tG1.Operator%>";
   var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.ManageCom.value+"'");                           
   var arrResult1 = easyExecSql("select username from llclaimuser where usercode = '"+fm.Handler.value+"'");                        
   if(arrResult) fm.all('ComName').value=arrResult[0][0];                   
   if(arrResult1)fm.all('HandlerName').value=arrResult1[0][0];
  }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="LLContDealMail.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LLContDealMailInit.jsp"%>
<title>合同处理案件</title>
</head>
<body  onload="initDate();initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContDealMail);">
         </TD>
         <TD class= titleImg>合同处理案件
         </TD>
       </TR>
      </table>
    <Div id = "divContDealMail">
    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <TD  class= title>管理机构</TD>
        <TD  class= input>
           <Input class="codeno" name=ManageCom readonly verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,180);" ><input class=codename name=ComName></TD>
        <TD  class= title>理赔案件号</TD>
        <TD  class= input><input class="common" name="CaseNo"></TD> 
        <TD  class= title>案件处理人</TD>
        <TD  class= input>
           <input class="codeno" name=Handler ondblclick="return showCodeList('optnameunclass',[this,HandlerName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('optnameunclass',[this,HandlerName],[0,1]);"><input class=codename name=HandlerName></TD> 
      </tr>
      <tr class="common">
        <td class="title">客户号码</td>
        <td class="input"><input class="common" name="CustomerNo"></td>
        <td class="title">客户姓名</td>
        <td class="input"><input class="common" name="CustomerName"></td>
        <td class="title" id="divIDNot"></td>
        <td class="input" id="divIDNo"></TD>
      </tr>
      <tr class="common" id="divConfirm" style="display:''" >
        <td class="title">受理起期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="RgtDateS"></td>
        <td class="title">受理止期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="RgtDateE"></td>
        <TD  class= title></TD>
        <TD  class= input></TD> 
      </tr>
      <TR  class= common>
		 <td  colspan='3'>			 
			 <input type="radio" value="01" checked name="ContDealFlag" onclick="searchCase();">全部待处理</input>
			 <input type="radio" value="02" name="ContDealFlag" onclick="searchCase();">已申请</input>
			 <input type="radio" value="03" name="ContDealFlag" onclick="searchCase();">待审批</input>
			 <input type="radio" value="04" name="ContDealFlag" onclick="searchCase();">已完成</input>
			 <input type="radio" value="05" name="ContDealFlag" onclick="searchCase();">未申请</input>
		 </TD>                                             
	  </TR>	     
    </table>
 	 <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseInfo);">
         </TD>
         <TD class= titleImg>案件信息
         </TD>
       </TR>
      </table>
  	<div id="divCaseInfo" align = center style="display: ''">	
      <table class="common rgtht">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanCaseInfo"></span></td></tr>
      </table>

      <input VALUE="首  页" TYPE="button" class="cssButton" onclick="turnPage.firstPage();">
      <input VALUE="上一页" TYPE="button" class="cssButton" onclick="turnPage.previousPage();">
      <input VALUE="下一页" TYPE="button" class="cssButton" onclick="turnPage.nextPage();">
      <input VALUE="尾  页" TYPE="button" class="cssButton" onclick="turnPage.lastPage();">
  	</div>
  	<br>
  	<table>
      <tr>
        <td  id="divSubmitForm" >
          	<input value="合同处理" type="button" class="cssButton" onclick="ContDeal();">
        </td>
      </tr>
    </table>
    </Div>
    <br>
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		<input type=hidden name="SearchSQL" value="">
		<input type=hidden name="Operate" value="">
		<input type=hidden name="CaseNoT" value="">
		<input type=hidden name="CustomerNoT" value="">
		<Div id=DivFileDownload style="display:'none'">
      <A id=fileUrl href=""></A>
    </Div>
  </form>
</body>
</html>
