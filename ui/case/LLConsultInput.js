var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var tSaveType ="";
var turnPage = new turnPageClass();     
//提交，保存按钮对应操作
function submitForm()
{

 
    if (confirm("您确实想保存该记录吗?"))
    {
      tSaveFlag = "1";
     
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //  showSubmitFrame(mDebug);
      
        fm.submit(); //提交
        tSaveFlag ="0";

    
    }
    else
    {
      alert("您取消了修改操作！");
    }
 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

   updateConsultCust();
  }
  
  
 
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (confirm("您确实想修改该记录吗?"))
 	{
 		var rowNum=SubReportGrid.mulLineCount;
  	for(var j=0;j<rowNum;j++)
  	{
  		var tValue = SubReportGrid.getRowColData(j,1);
  		if ((tValue=="")||(tValue=="null"))
  		{
  			alert("请您录入客户号，若没有该用户请将分报案录入框中的该条记录取消！");
  			return;
  		}
  	}
 		if ((fm.RptObj.value=="")||(fm.RptObj.value=="null"))
  	{
  		alert ("请您输入号码类型!");
  		return;
  	}
  	if ((fm.RptObjNo.value=="")||(fm.RptObjNo.value=="null"))
  	{
  		alert ("请您输入号码!");
    	return;
  	}
  	if ((fm.RptorName.value=="")||(fm.RptorName.value=="null"))
  	{
  		alert ("请您输入报案人姓名!");
  		return;
  	}
  	if ((fm.RptDate.value=="")||(fm.RptDate.value=="null"))
  	{
  		alert ("请您输入报案日期!");
  		return;
    }
    else
    {
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      showSubmitFrame(mDebug);
      fm.fmtransact.value = "UPDATE||MAIN"
      fm.action = './ReportUpdate.jsp';
      fm.submit(); //提交
    }//end of else
  }//end of if
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{

  window.open("./FrameReportQuery.jsp");
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function deleteClick()
{
	alert("您无法进行删除操作！！！");
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function submitForm1()
{
  tSaveFlag = "0";
	if((fm.PeopleType.value=="")||(fm.PeopleType.value=="null"))
	{
		alert("请您录入事故者类型！！！");
		return;
	}
	if((fm.RptObj.value=="")||(fm.RptObj.value=="null"))
	{
		alert("请您录入号码类型！！！");
		return;
	}
	if (fm.RptObjNo.value=="")
	{
		alert("请您收入要号码！");
 		return ;
  }
  if(fm.RptObj.value=="0")
  {
  	if(fm.PeopleType.value!="0")
  	{
  		alert("号码类型是团单，事故者类型只能是被保险人，请重新录入事故者类型");
  		return;
  	}
  	else
  	{
  		var i = 0;
   		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  		initSubReportGrid();
  		fm.action = "./ReportQueryOut1.jsp";
  		fm.submit(); //提交
  	}
  }
  else
  {
  	var i = 0;
   	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	initSubReportGrid();
  	fm.action = "./ReportQueryOut1.jsp";
  	fm.submit(); //提交
  }
}
//完成按照”客户号“在lcpol中进行查询，显示该客户的保单明细
function showInsuredLCPol()
{
  var row;
	var a_count=0;//判断选中了多少行
  var t = SubReportGrid.mulLineCount;//得到Grid的行数
  for(var i=0;i<t;i++)
  {
    varCount = SubReportGrid.getChkNo(i);
    if(varCount==true)
    {
       a_count++;
       row=i;
    }
  }
  if(a_count>1)
  {
    alert("您只能选中一行记录！");
    return;
  }
  else if(a_count<1)
  {
    alert("请您选中一条分案记录!");
    return;
  }
  else
  {
    var varInsuredNo;
    var varCount;
    varInsuredNo=SubReportGrid.getRowColData(row,1);
    if ((varInsuredNo=="null")||(varInsuredNo==""))
    {
      alert("客户号为空，不能进行查询操作！");
      return;
    }
    var varSrc = "&InsuredNo=" + SubReportGrid.getRowColData(row,1);
    var newWindow = window.open("./FrameMainReportLCPol.jsp?Interface=ReportLCPolInput.jsp"+varSrc,"ReportLCPolInput",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}

function showCustomerInfo()
{
	window.open("../sys/FrameCQPersonQuery.jsp");
}

function insertCust()
{
	
	if ( fm.LogNo.value=="")
	{
		alert("没有登记号");
	}
	if ( fm.ConsultNo.value =="")
	{
		fm.fmtransact.value="INSERT||MAIN";
	}else
		{
			fm.fmtransact.value="UPDATE||MAIN";
		}
	fm.action= "./LLConsultSave.jsp";
	tSaveType="Consult"
	submitForm();
}
function deleteCust()
{
	
	if ( fm.LogNo.value=="")
	{
		alert("没有登记号");
	}
	if ( fm.ConsultNo.value =="")
	{
		alert("没有客户咨询信息，请先录入或查询");
		return ;
	}else
		{
		
			fm.fmtransact.value="DELETE||MAIN";
		}
    if ( confirm("删除客户咨询信息会删除相应的事件关联，确定要继续吗？"))
    {
	    fm.action= "./LLConsultSave.jsp";
		tSaveType="Consult"
		submitForm();
	}
}

function initCustomer()
{
	 
	 if ( fm.ConsultNo.value =="" ) 
     {
     	return ;
    }
    //查询页面元素   
  	var strSQL = "select CustomerName,CustomerType,DiseaseCode,ExpertFlag,ExpertName"
  	 +",ExpRDate,CSubject,ConsultNo,AskGrade,CContent,CustStatus "
  	 +"from llconsult where consultno='" + fm.ConsultNo.value +"'"
				
	var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	if (!strQueryResult) 
	{
		alert("错误");
		return "";
	}
	var statment = getStatment(strSQL );
	var  deResult= decodeEasyQueryResultV2(statment,strQueryResult);
   // alert(deResult[0].length);
	for( i=0;i<statment.length ;i++)
	{
	 
	 try {
	 	fm.all(statment[i]).value =Conversion( deResult[0][statment[i]] ) ;
	 }catch(ex)
	 {
	 	alert("没找到页面元素:"+ statment[i])
	 } ;
	  
	}
	
	//查询事件关联
	strSQL = "select SubRptNo,AccDate,AccPlace,AccidentType,AccSubject "
	 +"from LLSubReport where SubRptNo in "
	 +" ( select distinct SubRptNo from LLAskRela where consultno='" + fm.ConsultNo.value + "')";
	 
	 turnPage.queryModal(strSQL, SubReportGrid);
 //  strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
 //  if ( strQueryResult )
 //  {
 //  	 var arr = decodeEasyQueryResult(strQueryResult );
 //  	 displayEasyQu
 //  	}
  	//execEasyQuery( strSQL );
  //	strSQL = "select ";
	 
	 
}
function setCustomerInfo( arr )
{
	
	 fm.CustomerNo.value	 =	arr['CustomerNo'];
//	 fm.CustomerName.value	=  arr[3];
//	 fm.CustomerType.value	 =	arr[4];
//	 fm.DiseaseCode.value	=	arr[];
//	 fm.ExpertFlag.value	=	arr[];
//	 fm.ExpertName.value	=	arr[];
//	 fm.ExpRDate.value		  =	arr[];
//	 fm.CSubject.value	      =arr[];
//	 fm.ConsultNo.value		  =	arr[];	 
//	 fm.AskGrade.value	      =arr[];
//	 fm.CContent.value	      =arr[];
	// fm.CustStatus.value	      =arr[];
}
function updateConsultCust()
{
	
	//事件相关
	if ( tSaveType=="Event")
	{
	   //top.opener.updateCustomer( fm.ConsultNo.value );
	   updateEventGrid();
	 }else 
	 	if ( tSaveType=="Consult") //咨询相关
	 {
	 	if ( fm.fmtransact.value=="DELETE||MAIN" )
	 	{
	 		//删除
	 		
	 		top.opener.updateCustomer();
	 		initSubReportGrid();
	 		fm.reset();
	 		top.close();
	 		
	 		
		}
		else if (fm.fmtransact.value=="INSERT||MAIN")
	 	{
	 		top.opener.updateCustomer( fm.ConsultNo.value );
	 	}
	 }else if ( tSaveType =="Answer")
	 {
	 	}

//  if ( fm.fmtransact.value=="INSERT||MAIN")
//  {
//  	CustomerGrid.addOne();
//  	var len = CustomerGrid.mulLineCount-1;
//  	CustomerGrid.setRowColData(len, 1, fm.CustomerName.value);
//  	CustomerGrid.setRowColData(len, 2, fm.CustomerType.value);
//  	CustomerGrid.setRowColData(len, 3, fm.DiseaseCode.value);
//  	CustomerGrid.setRowColData(len, 4, fm.ExpertFlag.value);
//  	CustomerGrid.setRowColData(len, 5, fm.ExpertName.value);
//  	CustomerGrid.setRowColData(len, 6, fm.ExpRDate.value);
//  	CustomerGrid.setRowColData(len, 7, escape(fm.CSubject.value));
//  	CustomerGrid.setRowColData(len, 8, fm.ConsultNo.value);
//  	CustomerGrid.setRowColData(len, 9, fm.CustomerNo.value);
//  	
//  	CustomerGrid.setRowColData(len, 10, fm.AskGrade.value);
//  	CustomerGrid.setRowColData(len, 11,escape(fm.CContent.value));
//  	CustomerGrid.setRowColData(len, 12,  escape(fm.CustStatus.value));
//  }
}

function EventSave()
{
	//alert(1);
	tSaveType="Event";
	if ( fm.ConsultNo.value =="")
	{
		alert("请先保存咨询信息");
		return 
	}
	if ( trim(fm.SubRptNo.value) !="")
	{
		if ( !confirm("确定要增加新事件吗?"))
		{
			return;
		}
    }
	
		fm.fmtransact.value="INSERT||MAIN";
	
	fm.action= "./LLSubReportSave.jsp";
	submitForm();
	
}

function updateEventGrid()
{

	if (fm.fmtransact.value=="DELETE||RELA")
	{
		
		SubReportGrid.delCheckTrueLine();
		
	}
	else if (fm.fmtransact.value=="INSERT||MAIN")
	{
		 //  alert(0);
    		var len = SubReportGrid.mulLineCount;
	    	if (len<=0) len = 0;
	    	SubReportGrid.addOne();
	    	SubReportGrid.setRowColData(len,1,fm.SubRptNo.value);
	    	SubReportGrid.setRowColData(len,2,fm.AccDate.value);
	    	SubReportGrid.setRowColData(len,3,fm.AccPlace.value);
	    	SubReportGrid.setRowColData(len,4,fm.AccidentType.value);
	    	SubReportGrid.setRowColData(len,5,Conversion(fm.AccSubject.value));
	    	
	    	
    }

	
}


function DelRela()
{
 if ( fm.ConsultNo.value =="")
	{
	 alert("没有咨询信息");
	 return false;
	}
 if ( 	SubReportGrid.mulLineCount<=0)
 {
 	alert("没有要删除的关联事件");
 	return false;
 	}
	fm.fmtransact.value="DELETE||RELA";
	
	alert( fm.fmtransact.value );
	fm.action= "./LLSubReportSave.jsp";

	tSaveType=="Event"
	submitForm(); 	
}

function ShowRela()
{
	var arrResult = new Array();
	var selno= SubReportGrid.getSelNo();
	var subrptno = SubReportGrid.getRowColData(selno-1,1);
	
	var strSQL = "select AccidentType,AccDate,AccEndDate,AccDesc"
	      +",AccPlace,HospitalCode,HospitalName,InHospitalDate,OutHospitalDate"
	      +",SeriousGrade,AccSubject from llsubreport where subrptno='" + subrptno +"'";
	if (EasyShowForm(strSQL)==false)
	{
		alert("没有符合条件的记录");
		return false;
	}
	else
	{
		divLLLLEventInput1.style.display='';
		
		arrResult=easyExecSql(strSQL);
        try{fm.all('AccidentType').value=arrResult[0][0]}catch(ex){alert(ex.message+"AccidentType")}                             
		try{fm.all('AccDate').value=arrResult[0][1]}catch(ex){alert(ex.message+"AccDate")}                             
		try{fm.all('AccEndDate').value=arrResult[0][2]}catch(ex){alert(ex.message+"AccEndDate")}                             
		try{fm.all('AccDesc').value=arrResult[0][3]}catch(ex){alert(ex.message+"AccDesc")}
		try{fm.all('AccPlace').value=arrResult[0][4]}catch(ex){alert(ex.message+"AccPlace")}                             
		//try{fm.all('DiseaseCode').value=arrResult[0][0]}catch(ex){alert(ex.message+"DiseaseCode")}                             
		try{fm.all('HospitalCode').value=arrResult[0][5]}catch(ex){alert(ex.message+"HospitalCode")}                             
		try{fm.all('HospitalName').value=arrResult[0][6]}catch(ex){alert(ex.message+"HospitalName")}                             
		try{fm.all('InHospitalDate').value=arrResult[0][7]}catch(ex){alert(ex.message+"InHospitalDate")}                              
		try{fm.all('OutHospitalDate').value=arrResult[0][8]}catch(ex){alert(ex.message+"OutHospitalDate")}
		try{fm.all('SeriousGrade').value=arrResult[0][9]}catch(ex){alert(ex.message+"SeriousGrade")}
		try{fm.all('AccSubject').value=arrResult[0][10]}catch(ex){alert(ex.message+"AccSubject")}
               
	}                     
		                  
                          
}                  
  function RelaQuery()
  {
  	var paramNo;
  	if ( fm.AskType.value=="0")
  	{
  		paramNo= fm.ConsultNo.value;
  		
  	}
  	else
  	{
  			paramNo = fm.NoticeNo.value ;
  	}
  	var row=CustomerGrid.getSelNo()-1;
  	if(row<0)
  	{
  		alert("请选择一条事件！");
  		return false;
  	}
  	
  	 showInfo= window.open("./LLSubReportQuery.jsp?CustomerNo="+fm.CustomerNo.value+"&CustomerName="+fm.CustomerName.value+"&AskType="+fm.AskType.value+"&ParamNo="+paramNo,"客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  	}
  	
 function afterQuery( arrReturn )
 {
 	
 	for ( i=0;i<arrReturn.length;i++)
 	{
 		len = SubReportGrid.mulLineCount;
 	
 		SubReportGrid.addOne();                          
 		
 		SubReportGrid.setRowColData(len,1,arrReturn[i][0]);
 		SubReportGrid.setRowColData(len,2,arrReturn[i][1]);
 		SubReportGrid.setRowColData(len,3,arrReturn[i][2]);
 		SubReportGrid.setRowColData(len,4,arrReturn[i][3]);
 				
 	}
 }
 
 function ReplySave()
 {
   	 if ( fm.ConsultNo.value =="")
	{
	 alert("没有咨询信息");
	 return false;
	}
 
  if ( fm.SerialNo.value =="") 
  {
	fm.fmtransact.value="INSERT||MAIN";
 }else
 	{
 		fm.fmtransact.value="UPDATE||MAIN";
 		}
	
	
	fm.action= "./LLAnswerInfoSave.jsp";

	tSaveType=="Answer"
	submitForm(); 	
 }