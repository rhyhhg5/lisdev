<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%> 
<html>    
<%
//程序名称：LLClaimCollectionInput.jsp
//程序功能：F1报表生成
//创建日期：2005-08-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLUserGroupInput.js"></SCRIPT>
		<%@include file="LLUserGroupInit.jsp"%>   
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>

<body onload="initForm();">    
  <form action="" method=post name=fm target="f1print">
    <br>
		<Div  id= "divType" style= "display: ''" >
		<table>
    	<TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBusiness);">
         </TD>
         <TD class= titleImg>
         分组配置
         </TD>
       </TR>
   	</table>
		<Div  id= "divBusiness" style= "display: ''" align = left>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanBusinessGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    	<INPUT VALUE="查  询" class="cssButton" TYPE="button" onclick="GrpTypeSearch()" > 	
      <INPUT VALUE="保  存" class="cssButton" TYPE="button" onclick="GrpTypeSave(1)" > 	
      <INPUT VALUE="删  除" class="cssButton" TYPE="button" onclick="GrpTypeSave(2)" > 	
    </Div>
    <table>
    	<TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBusinessGrpCont);">
         </TD>
         <TD class= titleImg>
         分组用户配置
         </TD>
       </TR>
   	</table>
   	<Div  id= "divBusinessGrpCont" style= "display: ''" align = left>
   	  <table class= common border=0 width=100%>
      	<TR  class= common>          
					<TD  class= title>组</TD><TD  class= input>
    			<Input class= "codeno"  name=GrpType  ondblclick="return showCodeList('llusergroup',[this,GrpTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('llusergroup',[this,GrpTypeName],[0,1],null,null,null,1);" ><Input class=codename  name=GrpTypeName></TD>
          <TD class= title8>用户</TD>
		  		<TD class= input8><Input class= "codeno"  name=ClaimUserCode  ondblclick="return showCodeList('llusercode',[this,ClaimUserName],[0,1],null,'1','stateflag',1);" onkeyup="return showCodeListKey('llusercode',[this,ClaimUserName],[0,1],null,'1','stateflag',1);" ><Input class=codename  name=ClaimUserName></TD>
		  		<TD><INPUT VALUE="查  询" class="cssButton" TYPE="button" onclick="GrpContSearch()"></TD>
		  		<TD></TD>
        </TR>
      </Table>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanBusinessGrpContGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    	<INPUT VALUE="保  存" class="cssButton" TYPE="button" onclick="GrpContTypeSave(1)">
		  <INPUT VALUE="删  除" class="cssButton" TYPE="button" onclick="GrpContTypeSave(2)"> 	
    </Div>
    </Div>
		<input type= hidden name=StartDate>
		<input type= hidden name=EndDate>
		<input type= hidden name=MngName>
		<input type= hidden name=fmtransact>
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 