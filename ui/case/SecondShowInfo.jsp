<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
  * Name    ：SecondShowInfo.jsp
  * Function：理算－审核中显示要进行二次核保的信息
  * Author  ：LiuYansong
  * Date    : 2003-8-4
 */
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String CustomerNo = request.getParameter("InsuredNo");
  LCPolSet mLCPolSet = new LCPolSet();
  LBPolSet mLBPolSet = new LBPolSet();


  VData tVData = new VData();
  tVData.addElement(CustomerNo);
	SecondUWUI mSecondUWUI = new SecondUWUI();
  if(!mSecondUWUI.submitData(tVData,"SHOW"))
  {
    FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = mSecondUWUI.getResult();
    mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
    mLBPolSet.set((LBPolSet)tVData.getObjectByObjectName("LBPolSet",0));
    System.out.println("mLCPolSet的个数是"+mLCPolSet.size());
    System.out.println("mLBPolSet的个数是"+mLBPolSet.size());
    int m = mLCPolSet.size();
    if(mLCPolSet.size()>0)
    {
      %>
      <script language="javascript">
        parent.fraInterface.SecondUWGrid.clearData();
      </script>
      <%
      for(int i=1;i<=mLCPolSet.size();i++)
      {
        LCPolSchema tLCPolSchema = new LCPolSchema();
        tLCPolSchema.setSchema(mLCPolSet.get(i));
        %>
        <script language="javascript">
          parent.fraInterface.SecondUWGrid.addOne("SecondUWGrid");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,1,"<%=tLCPolSchema.getPolNo()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,2,"<%=tLCPolSchema.getRemark()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,3,"<%=tLCPolSchema.getRiskCode()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,4,"<%=tLCPolSchema.getAppntName()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,5,"<%=tLCPolSchema.getContNo()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,6,"<%=tLCPolSchema.getAmnt()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,7,"<%=tLCPolSchema.getPrem()%>");
          parent.fraInterface.SecondUWGrid.setRowColData(<%=i-1%>,8,"");
          parent.fraInterface.emptyUndefined();
        </script>
        <%
      }
    }
    if(mLBPolSet.size()>0)
    {
       for( int j=1;j<=mLBPolSet.size();j++)
       {
         LBPolSchema tLBPolSchema = new LBPolSchema();
         tLBPolSchema.setSchema(mLBPolSet.get(j));
         %>
         <script language="javascript">
           parent.fraInterface.SecondUWGrid.addOne("SecondUWGrid");
           parent.fraInterface.SecondUWGrid.setRowColData(<%=m+j-1%>,1,"<%=tLBPolSchema.getPolNo()%>");
           parent.fraInterface.SecondUWGrid.setRowColData(<%=m+j-1%>,2,"<%=tLBPolSchema.getRemark()%>");
           parent.fraInterface.SecondUWGrid.setRowColData(<%=m+j-1%>,3,"<%=tLBPolSchema.getRiskCode()%>");
           parent.fraInterface.SecondUWGrid.setRowColData(<%=m+j-1%>,4,"<%=tLBPolSchema.getAppntName()%>");
           parent.fraInterface.SecondUWGrid.setRowColData(<%=m+j-1%>,5,"<%=tLBPolSchema.getInsuredName()%>");
           parent.fraInterface.SecondUWGrid.setRowColData(<%=m+j-1%>,6,"<%=tLBPolSchema.getAmnt()%>");
           parent.fraInterface.SecondUWGrid.setRowColData(<%=m+j-1%>,7,"<%=tLBPolSchema.getMult()%>");
           parent.fraInterface.SecondUWGrid.setRowColData(<%=m+j-1%>,8,"");
           parent.fraInterface.emptyUndefined();
         </script>
         <%
      }
    }
  }
%>