<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ClaimQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LLClaimSchema tLLClaimSchema   = new LLClaimSchema();
  tLLClaimSchema.setClmNo(request.getParameter("ClmNo1"));
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLLClaimSchema);
  // 数据传输
  ClaimUI tClaimUI   = new ClaimUI();
	if (!tClaimUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tClaimUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tClaimUI.getResult();
		System.out.println(tVData.size());
		// 显示
		// 保单信息
		LLClaimSchema mLLClaimSchema = new LLClaimSchema(); 
		LLClaimSet mLLClaimSet=new LLClaimSet();
		mLLClaimSet=(LLClaimSet)tVData.getObjectByObjectName("LLClaimBLSet",0);
		mLLClaimSchema=mLLClaimSet.get(1);
		%>
    	<script language="javascript">
    	 	parent.fraInterface.fm.all("ClmNo").value = "<%=mLLClaimSchema.getClmNo()%>";
    	 	parent.fraInterface.fm.all("CasePolNo").value = "<%=mLLClaimSchema.getCasePolNo()%>";
    	 	parent.fraInterface.fm.all("CaseNo").value = "<%=mLLClaimSchema.getCaseNo()%>";
    	 	parent.fraInterface.fm.all("ContNo").value = "<%=mLLClaimSchema.getContNo()%>";
    	 	parent.fraInterface.fm.all("GrpPolNo").value = "<%=mLLClaimSchema.getGrpPolNo()%>";
    	 	parent.fraInterface.fm.all("PolNo").value = "<%=mLLClaimSchema.getPolNo()%>";
    	 	parent.fraInterface.fm.all("KindCode").value = "<%=mLLClaimSchema.getKindCode()%>";
    	 	parent.fraInterface.fm.all("RiskVer").value = "<%=mLLClaimSchema.getRiskVer()%>";
    	 	parent.fraInterface.fm.all("RiskCode").value = "<%=mLLClaimSchema.getRiskCode()%>";
    	 	parent.fraInterface.fm.all("PolMngCom").value = "<%=mLLClaimSchema.getPolMngCom()%>";
    	 	parent.fraInterface.fm.all("SaleChnl").value = "<%=mLLClaimSchema.getSaleChnl()%>";
    	 	parent.fraInterface.fm.all("ValidDate").value = "<%=mLLClaimSchema.getValidDate()%>";
    	 	parent.fraInterface.fm.all("PolState").value = "<%=mLLClaimSchema.getPolState()%>";
    	 	parent.fraInterface.fm.all("AgentCode").value = "<%=mLLClaimSchema.getAgentCode()%>";
    	 	parent.fraInterface.fm.all("AgentGroup").value = "<%=mLLClaimSchema.getAgentGroup()%>";
    	 	
    	 	parent.fraInterface.fm.all("RealPay").value = "<%=mLLClaimSchema.getStandPay()%>";
    	 	parent.fraInterface.fm.all("ClmUWer").value = "<%=mLLClaimSchema.getClmUWer()%>";
    	 	parent.fraInterface.fm.all("Operator").value = "<%=mLLClaimSchema.getOperator()%>";
    	 	parent.fraInterface.fm.all("ClmState").value = "<%=mLLClaimSchema.getClmState()%>";
    	 	parent.fraInterface.fm.all("MngCom").value = "<%=mLLClaimSchema.getMngCom()%>";
    	 	parent.fraInterface.fm.all("MakeDate").value = "<%=mLLClaimSchema.getMakeDate()%>";

    	</script>
		<%		
	}
 // end of if
 
 
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tClaimUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  //out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  //out.println("top.close();");
  out.println("</script>");
%>
