<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLBusiInput.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
 
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLBusiInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form  method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
    </table>
    <hr>
		<br>
    <input type="hidden" name=op value="">
    <table class=common>
    	<tr><td style="width:5%"></td>
					<TD  class= input8 style="width:18%"> <input type=button class=cssButton value="分销售渠道理赔统计" onclick="SaleClaim()"></TD>
					<TD  class= input8 style="width:16%"> <input type=button class=cssButton value="分销售渠道案件结果统计" style="width:70%" onclick="SaleResult()"></TD>
					<TD  class= input8> <input type=button class=cssButton value="分核保方式理赔统计" onclick="UWType()"></TD>
					
			</tr>
		</table>
		<table class=common>					
			<tr></tr><tr></tr><tr></tr>
			<tr>
					<TD  class= input8> <input type=button class=cssButton value="分核保决定理赔统计" onclick="UWDecision()"></TD>
					<TD  class= input8> <input type=button class=cssButton value="分缴费方式理赔统计" onclick="ChargeType()"></TD>
					<TD  class= input8> <input type=button class=cssButton value="分申请方式理赔统计" onclick="AppType()"></TD>
					<TD  class= input8> <input type=button class=cssButton value="分调查方式理赔统计" onclick="Investigation()"></TD>
				</tr>
		</table>
	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 