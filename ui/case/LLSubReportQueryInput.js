/** 
 * 程序名称：LLSubReportInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-01-12 09:36:37
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}

// 查询按钮
function easyQueryClick() {
			    
	var strSql = "select SubRptNo,AccDate,AccPlace,AccidentType,AccSubject,HospitalName,AccDesc from LLSubReport where 1=1 "
    + getWherePart("SubRptNo", "SubRptNo")
    + getWherePart("CustomerNo", "CustomerNo")
    + getWherePart("CustomerName", "CustomerName")
    + getWherePart("CustomerType", "CustomerType")
    + getWherePart("AccSubject", "AccSubject")
    + getWherePart("AccidentType", "AccidentType")
    + getWherePart("AccDate", "AccDate")
    + getWherePart("AccEndDate", "AccEndDate")
   
    + getWherePart("CustSituation", "CustSituation")
    + getWherePart("AccPlace", "AccPlace")
    + getWherePart("DiseaseCode", "DiseaseCode")
    + getWherePart("HospitalCode", "HospitalCode")
    + getWherePart("HospitalName", "HospitalName")
    + getWherePart("InHospitalDate", "InHospitalDate")
    + getWherePart("OutHospitalDate", "OutHospitalDate")
    
    + getWherePart("SeriousGrade", "SeriousGrade")
    + getWherePart("SurveyFlag", "SurveyFlag")
   
  ;
  //alert(strSql);
	turnPage.queryModal(strSql, LLSubReportGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  
}

function returnParent()
{
      var arrReturn = new Array();
	
			try
			{	
				
				
				arrReturn = getQueryResult();
				//alert(arrReturn[0]);
			//	alert(arrReturn.length);
				if ( arrReturn==null || arrReturn.length<=0)
				{
					alert("请选中要关联的事件");
					return;
				}
		    
				top.opener.afterQuery(arrReturn);
				top.close();
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex.message );
			}
			
		
	
}
function getQueryResult()
{
	
	var arrSelected = new Array();
	var count = 0;
	
	for (i=0;i<LLSubReportGrid.mulLineCount;i++)
	{
	
		if ( LLSubReportGrid.getChkNo(i)==true)
		{
		
			
			arrSelected[count] = new Array();
			arrSelected[count][0] = LLSubReportGrid.getRowColData(i,1);
			arrSelected[count][1] = LLSubReportGrid.getRowColData(i,2);
			arrSelected[count][2] = LLSubReportGrid.getRowColData(i,3);
			arrSelected[count][3] = LLSubReportGrid.getRowColData(i,4);
			arrSelected[count][4] = LLSubReportGrid.getRowColData(i,5);
			count++;
	
		}
	}
	
	/*var len =LLSubReportGrid.getSelNo() - 1;	
	if ( len>0)
	{
	var arrSelected = new Array();
	var count = 0;
	arrSelected[count] = new Array();
	arrSelected[count][0] = LLSubReportGrid.getRowColData(i,1);
	arrSelected[count][1] = LLSubReportGrid.getRowColData(i,2);
	arrSelected[count][2] = LLSubReportGrid.getRowColData(i,3);
	arrSelected[count][3] = LLSubReportGrid.getRowColData(i,4);
	}
	*/
	
	
	return arrSelected;
}



function submitForm()
{

  //alert(fm.ParamNo.value);
  //if ( fm.ParamNo.value =="")
	//{
	// alert("没有咨询信息");
	// top.close();
	// return false;
	//}
  
  var count =0;
	for (i=0;i<LLSubReportGrid.mulLineCount-1;i++)
	{
	
		if ( LLSubReportGrid.getChkNo(i)==true)
		{
			count++;
     }
     }

	if ( count<0) 
	{
		alert("请选择要关联的事件");
		return false;
	}
	
 
    if (confirm("您确实想保存该记录吗?"))
    {
   
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

      
       fm.fmtransact.value ="INSERT||RELA"
      
       fm.submit(); //提交
      
     
    
    }
    else
    {
      alert("您取消了修改操作！");
    }
 
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    returnParent();
  }
  
  
 
}