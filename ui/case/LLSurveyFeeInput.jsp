<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<title>直接调查费录入</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./CaseCommon.js"></SCRIPT>
  <script src="./LLSurveyFeeInput.js"></script> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLSurveyFeeInit.jsp"%>
</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form action="./LLSurveyFeeSave.jsp" name=fm target=fraSubmit method=post>
<Div  id= "divBnfLive" style= "display: ''" align = center>
	<Div align="left">
  	<Table>
    	<TR>
        <TD class=common>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg >
    			基本信息
    		</TD>
    	</TR>
    </Table>    	
	</div>
	<div id='divLDPerson1'>
		<table  class= common>
	    <TR  class= common8>
				<TD  class= title8>案件号</TD><TD  class= input8><input class= common name="CaseNo" onkeydown="QueryOnKeyDown()" onchange="initInqFeeDetailGrid();"></TD>
				<TD  class= title8>调查序号</TD><TD  class= input8><input class= common name="SurveyNo" onkeydown="QueryOnKeyDown()"></TD>     
			</TR>
			<TR  class= common8>
				<TD  class= title8>客户姓名</TD><TD  class= input8><input class= common name="CustomerName"  onkeydown="QueryOnKeyDown()"></TD>
				<TD  class= title8>客户号</TD><TD  class= input8><input class= common name="CustomerNo"  onkeydown="QueryOnKeyDown()"></TD>	
			</TR>
		</table>
	</div>
</div>
<Div  id= "divInqFee" style= "display: ''" align = center>
  <Div align="left">
    <Table>
    	<TR>
        <TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBnfList);">
    		</TD>
    		<TD class= titleImg>
    			 直接调查费用明细
    		</TD> 
    	</TR>
    </Table>    	
  </div>
  <Div  id= "divInqFeeDetailList" align = center style= "display: ''">
  	 <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanInqFeeDetailGrid" ></span> 
  			</TD>
      </TR>
    </Table>
  </div>
  <div id="div1" align=left>
    <INPUT VALUE="合  计" class= cssButton TYPE=button onclick="sumFeeItem();">
  </div>			
</Div>	
<br>
<Div align="left">
	<table >
      <tr >
        <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divAccountList);"></td>
        <td class= titleImg> 直接调查费用</td>
      </tr>
	</table>
</Div>
<Div  id= "divAccountList" align = center style= "display: ''">
   <table  class= common >
     <TR  class= common>
        <TD text-align: left colSpan=1>
          <span id="spanInqFeeGrid" >
          </span>
        </TD>
     </TR>					
   </table>
</Div> 
  <div id="div1" align=left>
    <INPUT VALUE="审  核" class= cssButton TYPE=button id="butConfirm" onclick="submitConf();">
    <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="top.close();">
  </div>			

 							
<input type="hidden" id="LoadFlag" name="LoadFlag">
<input type=hidden id="fmtransact" name="fmtransact">

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>

</html>
