/******************************************************************************
 * Name    ：RegisterQuery.js
 * Function:立案－查询的处理程序
 * Author  :LiuYansong
 */
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
function submitForm()
{
  initRegisterGrid();
  var strSQL = "select UserCode,UserName,ComCode,UpUserCode,ClaimPopedom "
             +" from LLClaimUser where ComCode like '"+fm.Station.value+"%%'"+
               	getWherePart( 'UserCode','UserCode' )+
               getWherePart('UserName','UserName')+
               //getWherePart('ComCode','Station')+
               getWherePart('UpUserCode','UpUserCode')+
               getWherePart('ClaimPopedom','ClaimPopedom')+
               getWherePart('HandleFlag','HandleFlag')+
               getWherePart('CheckFlag','CheckFlag')+
               getWherePart('RgtFlag','RgtFlag')+
               getWherePart('SurveyFlag','ServeyFlag')+
               getWherePart('StateFlag','StateFlag')+
               getWherePart('ClaimDeal','ClaimDeal')
               " order by UserCode ";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
    alert("在该管理机构下没有满足条件的立案信息记录");
    return "";
  }

  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.pageDisplayGrid = RegisterGrid;
  turnPage.strQuerySql     = strSQL;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayQueryResult(strResult)
{
  var filterArray          = new Array(0,1,2,3,4);
  turnPage.strQueryResult  = strResult;
  var tArr                 = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.useSimulation   = 1;
  turnPage.pageDisplayGrid = RegisterGrid;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
  }
}
function returnParent()
{
	var closeflag = 0;
	fm.fmtransact.value = "QUERY";
  var tRow=RegisterGrid.getSelNo();
  if(tRow==0)
  {
    top.close();
  }
  else{
  tCol=1;
	var tUserCode = RegisterGrid.getRowColData(tRow-1,tCol);
	//fm.UserCode.value=tUserCode;
	
	try
	{
		top.opener.afterQuery( tUserCode );
			
	}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口:" + ex.message );
		}
		top.close();
	}
 // fm.action = "./ClaimUserSave.jsp";
 // fm.submit(); //提交
  
}

