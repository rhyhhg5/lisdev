  <%
  //Name:LLRegisterCheckInput.jsp
  //Function：检录页面
  //Date：2002-07-21 17:44:28
  //Author ：wujs,Xx
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <head >
  	<SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLRegisterCheckInput.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LLRegisterCheckInit.jsp"%>
  </head>
  <body  onload="initForm();">
    <form action="./LLRegisterCheckSave.jsp" method=post name=fm target="fraSubmit">
      <%@include file="LLRemark.jsp"%>
      <%@include file="CaseTitle.jsp"%>
	<table  class= common>
    <TR  class= common8>
      <TD  class= title8>客户号</TD><TD  class= input8><Input class= readonly name="CustomerNo" readonly></TD>
      <TD  class= title8>客户姓名</TD><TD  class= input8><Input class= readonly name="CustomerName" readonly></TD>
      <TD  class= title8>客户性别</TD><TD  class= input8><Input class= readonly name="Sex" readonly></TD>
    </TR>
	</table>
      <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,InsuredEvent);">
          </td>
          <td class= titleImg>
            事件信息
          </td>
    			<TD  class=common style=display:'none'>
    			  <INPUT TYPE="checkBox" NAME="AllEvent" checked OnClick="queryEventInfo();"> 显示出险人所有事件
    		  </TD>
        </tr>
      </table>
      <Div  id= "InsuredEvent" style= "display: ''">
        <table  class= common>
          <tr  class= common>
            <td text-align: left >
              <span id="spanInsuredEventGrid" >
              </span>
            </td>
          </tr>
        </table>
        
         <div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage(); shouCheck();">      
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage(); shouCheck();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage(); shouCheck(); ">       
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage(); shouCheck();">       
     </div> 	
        
        <input class=cssButton type=button style='width: 60px;' name='divconfevent' value="保存事件" onclick="EventInsert()" id = "sub6">
      </div>

      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divReceipt);">
          </td>
          <td class= titleImg>
            账单信息
          </td>
        </tr>
      </table>
      <div id='divReceipt' style= "display: 'none'">
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
              <span id="spanICaseCureGrid" >
              </span>
            </td>
          </tr>
        </table>
      </div>

      <!------------------------->

      <Table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divCheckInfo);">
          </TD>
          <TD class=titleImg>
            出险内容
          </TD>
        </TR>
      </Table>
      <Div id = "divCheckInfo" style = "display: 'none'">
        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDisease);">
            </TD>
            <TD class=titleImg>
              疾病信息
            </TD>
          </TR>
        </Table>
        <Div id = "divDisease" style = "display: 'none'">
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanDiseaseGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>

        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divSeDisease);">
            </TD>
            <TD class=titleImg>
              重大疾病责任信息
            </TD>
          </TR>
        </Table>
        <Div id = "divSeDisease" style = "display: 'none'" >
          <div id="divSDProof"  style="display:'none' ; position:absolute; slategray">
            <Table class= common>
              <TR class= common>
                <TD text-align: left >
                  <span id="spanSDiseaseProofGrid" >
                  </span>
                </TD>
              </TR>
              <TR onclick=backMain()>
                <TD align=right>
                  <input type=button class=cssbutton name='backmain1' value="确  认" onclick="backMain();">
                </TD>
              </TR>
            </Table>
          </div>
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanSeriousDiseaseGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>

        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divOperation);">
            </td>
            <td class= titleImg>
              手术信息
            </td>
          </tr>
        </table>
        <Div  id= "divOperation" style= "display: 'none'">
          <table  class= common>
            <tr  class= common>
              <td text-align: left colSpan=1>
                <span id="spanDegreeOperationGrid" >
                </span>
              </td>
            </tr>
          </table>
        </div>

        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divAccident);">
            </td>
            <td class= titleImg>
              意外信息
            </td>
          </tr>
        </table>
        <Div  id= "divAccident" style= "display: 'none'">
          <table  class= common>
            <tr  class= common>
              <td text-align: left colSpan=1>
                <span id="spanAccidentGrid" >
                </span>
              </td>
            </tr>
          </table>
        </div>

        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDeformity);">
            </TD>
            <TD class=titleImg>
              残疾信息
            </TD>
          </TR>
        </Table>
        <Div id = "divDeformity" style = "display: 'none'">
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanDeformityGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>
        
        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divInvalidism);">
            </TD>
            <TD class=titleImg>
              伤残信息
            </TD>
          </TR>
        </Table>
        <Div id = "divInvalidism" style = "display: 'none'">
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanInvalidismGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>
        
        <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDisability);">
            </TD>
            <TD class=titleImg>
              失能信息
            </TD>
          </TR>
        </Table>
        <Div id = "divDisability" style = "display: 'none'">
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanDisabilityGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>

        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divOtherInfo);">
            </td>
            <td class= titleImg>
              其他录入要素
            </td>
          </tr>
        </table>

        <Div  id= "divOtherInfo" style= "display: 'none'">
          <table  class= common>
            <tr  class= common>
              <td text-align: left colSpan=1>
                <span id="spanOtherFactorGrid" >
                </span>
              </td>
            </tr>
          </table>
        </div>
        
         <Table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divMDisease);">
            </TD>
            <TD class=titleImg>
              轻症疾病责任信息
            </TD>
          </TR>
          </Table>
        <Div id = "divMDisease" style = "display: 'none'" >
          <div id="divMDProof"  style="display:'none' ; position:absolute; slategray">
            <Table class= common>
              <TR class= common>
                <TD text-align: left >
                  <span id="spanMDiseaseProofGrid" >
                  </span>
                </TD>
              </TR>
              <TR onclick=backMDMain()>
                <TD align=right>
                  <input type=button class=cssbutton name='backmain1' value="确  认" onclick="backMDMain();">
                </TD>
              </TR>
            </Table>
          </div>
          <Table class= common>
            <TR class= common>
              <TD text-align: left colSpan=1>
                <span id="spanMildGrid" >
                </span>
              </TD>
            </TR>
          </Table>
        </Div>

      </Div>
    <!----------------hidden----------------->
    	<Div  id= "divPol" style= "display: 'none'">
    	  <table  class= common>
    	    <tr  class= common>
    	      <td text-align: left colSpan=1>
    	        <span id="spanCasePolicyGrid" >
    	        </span>
    	      </td>
    	    </tr>
    	  </table>
    	</div>

  		<div id="divconfirm" align='left'>
  		  <INPUT VALUE="保  存" class=cssButton style='width: 60px;' TYPE=button name=mysave onclick="submitForm();" id ="sub1">
  		  <INPUT VALUE="返  回" class=cssButton style='width: 60px;' TYPE=button onclick="top.close();">
  		</div>

  <br>

  <div align='right' id= Contral>
  	<input class=cssButton style='width:80px;' type=button value="下发问题件" name='LowerQuestion' onclick="OnLower()" id = "sub10" >
    <input class=cssButton type=hidden value="影像查询" onclick="ImageQuery()">
    <input class=cssButton type=button style='width: 80px;' value="案件备注信息" onclick="openCaseRemark()" >
    <input class=cssButton type=hidden style='width: 80px;' value="疾病统计信息" onclick="DiseaseStatistics()" >
    <input class=cssButton type=button style='width: 60px;' value="补充材料" onclick="Affix()" id ="sub2">
    <input class=cssButton type=button style='width: 60px;' value="提起调查" onclick="submitFormSurvery()" id ="sub3">
    <input class=cssButton type=button style='width: 60px;' value="调查报告" onclick="ShowSurveyReply()" id ="sub4">
    <input class=cssButton style='width:80px;' type=button value="理赔二核" onclick="submitLLUnderWrit()" id = "sub6">
    <input class=cssButton type=button style='width: 60px;' value=" 确  认 " name=confirm onclick="submitFormCheck()" id ="sub5">
  </div>
  <hr>
  <div align='right' id=ConStep>
    <INPUT VALUE="上一步" class=cssButton style='width: 60px;' TYPE=button onclick="preDeal();" id ="sub7">
    <INPUT VALUE="下一步" class=cssButton style='width: 60px;' TYPE=button onclick="nextDeal();" id ="sub8">
    <INPUT VALUE="返  回" class=cssButton style='width: 60px;' TYPE=button onclick="backDeal();">
  </div>

  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="CaseRelaNo" name="CaseRelaNo">
  <input type=hidden id="Case_RgtState" name="Case_RgtState">
  <input type=hidden id="RiskCode" name="RiskCode">
  <input type=hidden id="RowNo" name="RowNo">
  <input type=hidden id="SDLoadFlag" name="SDLoadFlag">
  <input type=hidden id="MDLoadFlag" name="MDLoadFlag">
  <input type=hidden id="cOperator" name="cOperator">
  <input name ="LoadC" type="hidden">
  <input name ="LoadD" type="hidden">
  <input type=hidden name="ShowCaseRemarkFlag">
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>