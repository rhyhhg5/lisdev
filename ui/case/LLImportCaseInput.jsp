<html>
  <%
  //程序名称：LLImportCaseInput.jsp
  //程序功能：理赔批量导入
  //创建日期：2006-01-27 17:39:01
  //创建人  ：Xx
  //更新记录：  更新人    更新日期     更新原因/内容
  %>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>
  <%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String Operator=tG.Operator;
  String Comcode=tG.ManageCom;
  String CurrentDate= PubFun.getCurrentDate();
  String CurrentTime= PubFun.getCurrentTime();
  %>
  <head >
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="LLImportCaseInput.js"></SCRIPT>
    <script>
      function initDate(){
        fm.HandleDate.value="<%=CurrentDate%>";
        fm.Handler.value="<%=Operator%>";
      }
    </script>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LLImportCaseInit.jsp"%>
  </head>
  <body  onload="initDate();initForm();" >
    <form action="./LLImportCaseSave.jsp" method=post name=fm target="fraSubmit" >
      <div id="titleRgtInfo" style="display:''">
        <table>
          <tr>
            <td>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRgtInfo);">
            </td>
            <td class= titleImg>
              团体批次信息
            </td>
          </tr>
        </table>
      </div>
      <div id="divRgtInfo" style="display:''">
        <table class = common >
          <TR class = common>
            <TD class=title>团体批次号</TD><TD><input class=readonly readonly name=RgtNo></TD>
            <TD class=title>申请人数</TD><TD><input class=readonly readonly name=AppPeople></TD>
            <TD class=title>处理人</TD><TD><input class=readonly readonly name=Handler></TD>
            <TD class=title>处理时间</TD><TD><input class=readonly readonly name=HandleDate></TD>
          </TR>
        </table>
      </div>
        <iframe name="UploadiFrame" src="UpLoadInput.jsp" frameborder="0" scrolling="no" height="40px" width="100%"></iframe>
        <table class = common >
          <TR  >
            <TD  width='80%'>
              <INPUT class="cssbutton" style='width:80' TYPE=button VALUE="理   算" onclick = "CalClaim();" >
              <Input  type="hidden"　width="100%" name="insuredimport" value ="1">
            </TD>
          </TR>
        </table>

        <div id="divInsuredInfo" style="display: ''">
          <table class=common>
            <tr class=common >
              <TD  class= title>被保人总数</TD>
              <TD  class= input><Input class=readonly readonly name=SumInsured ></TD>
              <TD  class= title>待导入人数</TD>
              <TD  class= input><Input class=readonly readonly name=HoldInsured ></TD>
              <TD  class= title>导入成功数</TD>
              <TD  class= input><Input class=readonly readonly name=SuccInsured ></TD>
              <TD  class= title>导入失败数</TD>
              <TD  class= input><Input class=readonly readonly name=FailInsured ></TD>
            </TR>
            <tr class=common >
              <TD  class= title>社保号</TD>
              <TD  class= input><Input class=common name=SecurityNo onkeydown="queryOnKeyDown();"></TD>
              <TD  class= title>客户姓名</TD>
              <TD  class= input><Input class=common name=CustomerName onkeydown="queryOnKeyDown();"></TD>
              <TD  class= title>客户号</TD>
              <TD  class= input><Input class=common name=CustomerNo onkeydown="queryOnKeyDown();"></TD>
              <TD  class= title>单位编号</TD>
              <TD  class= input><Input class=common name=GrpNO onkeydown="queryOnKeyDown();"></TD>
            </TR>
          </table>
        </div>

        <div id="divCustomerSearch" style="display:''">
          <table>
            <tr>
              <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCase);">
              </td>
              <td class= titleImg>
                导入名单
              </td>
            </tr>
          </table>
          <Div  id= "divGrpCase" style= "display: ''" align= center>
            <Table  class= common>
              <TR  class= common>
                <TD text-align: left colSpan=1>
                  <span id="spanGrpCaseGrid">
                  </span>
                </TD>
              </TR>
            </Table>
            <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();">
            <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();">
            <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();">
            <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">
          </Div>

          <Div  id= "divDiskErr" style= "display: ''">
            <INPUT class=cssButton style='width:80' TYPE=button VALUE="打印错误清单" onclick="PrtErrList();">
          </Div>
          
        <div id="divDuplication" style="display:''">
          <table>
            <tr>
              <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDupCase);">
              </td>
              <td class= titleImg>
                重复导入客户名单
              </td>
            </tr>
          </table>
          <Div  id= "divDupCase" style= "display: ''" align= center>
            <Table  class= common>
              <TR  class= common>
                <TD text-align: left colSpan=1>
                  <span id="spanDiskErrQueryGrid" ></span>
                </TD>
              </TR>
            </Table>
            <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();">
            <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();">
            <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();">
            <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">
          </div>
        </div>
          <Div  id= "divDiskErr" style= "display: ''">
            <INPUT VALUE="删  除" class=cssButton style='width:80' TYPE=button onclick="deleteClick();">
          </Div>
          <Div  id= "divRgtFinish" style= "display: ''" align=right >
            <INPUT class=cssButton style='width:80' type=button value="团体申请完毕" onclick="RgtFinish();">
            <input class=cssButton style='width:80' type=button value="返  回" onclick="top.close()">
          </Div>

        <input type=hidden name=ImportFile>
        <input type=hidden id="" name="fmtransact">
        <input type=hidden id="" name="fmAction">
        <input type=hidden name="BatchCaseNo">
        <input type=hidden id="GrpContNo" name="GrpContNo">
        <input type=hidden id="LoadFlag" name="LoadFlag" value="1">
        <input name ="RiskCode" type="hidden">
        <input name ="realpeoples" type="hidden">
        <input name ="LoadC" type="hidden">
      </form>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </body>
  </html>