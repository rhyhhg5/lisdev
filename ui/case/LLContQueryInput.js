//程序名称：LLSecondUW.js
//程序功能：合同审查
//创建日期：2006-01-01 15:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;
var mEdorNo;//合同下最近的一次保全工单号

//显示保全信息，此处为“补充告知”
//wdx增加了方法
function afterSubmitM(index,money){
	
	document.getElementsByName("PolGrid14")[index].value=money;
}
function showEdorInfo()
{

	document.all("EdorInfo").src = "../bq/ShowEdorInfo.jsp?EdorNo=" + mEdorNo + "&Operate=PRINT";
	document.all("EdorInfo").height = "200";
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content); 
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    alert(content);
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
    cDiv.style.display="";
  else
    cDiv.style.display="none";  
}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	k++;
	var tInsuredNo = fm.InsuredNo.value;
  var strSQL ="select ContNo,(select AppntName from lccont where contno=p.contno)," 
		    +" (case when grpcontno='00000000000000000000' then (select case max(HighestEvaLevel) when 1 then '低' when 2 then '中' when 3 then '高' else '' end  from fx_FXQVIEW where AOGTYPE='1' and " +
			" AOGNAME=(select appntname from lcappnt where contno=p.contno)and " +
			" APPNTIDTYPE=(select idtype from lcappnt where contno=p.contno) and  " +
			" APPNTIDNO=(select idno from lcappnt where contno=p.contno) and " +
			" APPNTBIRTHDAY=(select APPNTBIRTHDAY from lcappnt where contno=p.contno) and " +
			" APPNTSEX=(select APPNTSEX from lcappnt where contno=p.contno) fetch first row only )" +
			"else (select case max(HighestEvaLevel) when 1 then '低' when 2 then '中' when 3 then '高' else '' end   from fx_FXQVIEW where AOGNAME=p.AppntName and AOGTYPE='2' fetch first row only) end ), "
	  		+" ManageCom,grpcontno,prtno," 
  			+" cvalidate,"
  			+" date(enddate)-1 day,"
  			+" conttype,proposalcontno,"
  			+" (case when db2inst1.CodeName('polstate', p.PolState) is not null then db2inst1.CodeName('polstate', p.PolState) else db2inst1.CodeName('stateflag', p.stateflag) end),"
  			+" enddate,"
  			+" (select riskname from lmrisk m where m.riskcode=p.riskcode )," 
  			//wdx注释了原来的字段
//  			+ " case when p.riskcode='162401' then ( cast(round( DB2INST1.CALMONEYVALUE( p.ContNo  , p.polno ,  p.riskcode  )  ,2)   as   numeric(30,2)) ) else (select nvl(Money,0) from LcInsureAccTrace where 1=1 and polno=p.polno ) end , "
  	 		+" ' ',"
  			+" (select codename from ldcode where codetype='contplancode' and code = p.contplancode), "
  			+" (select codename from ldcode where codetype = 'occupationtype' and code = p.OccupationType ), "
  	 		// wdx增加一列
  	 		+" '',"
  			+" polno " 
  			+" from LCPol p where appflag='1' "
	        +" and InsuredNo = '"+tInsuredNo+"'"
	        +" and (p.StateFlag is null or p.StateFlag in ('1'))"
	        +" union select ContNo,(select AppntName from lccont where contno=p.contno)," 
	        +" (case when grpcontno='00000000000000000000' then (select case max(HighestEvaLevel) when 1 then '低' when 2 then '中' when 3 then '高' else '' end   from fx_FXQVIEW where AOGTYPE='1' and " +
				" AOGNAME=(select appntname from lcappnt where contno=p.contno)and " +
				" APPNTIDTYPE=(select idtype from lcappnt where contno=p.contno) and  " +
				" APPNTIDNO=(select idno from lcappnt where contno=p.contno) and " +
				" APPNTBIRTHDAY=(select APPNTBIRTHDAY from lcappnt where contno=p.contno) and " +
				" APPNTSEX=(select APPNTSEX from lcappnt where contno=p.contno) fetch first row only )" +
				"else (select case max(HighestEvaLevel) when 1 then '低' when 2 then '中' when 3 then '高' else '' end   from fx_FXQVIEW where AOGNAME=p.AppntName and AOGTYPE='2' fetch first row only) end ), "
	        +
	        		"ManageCom,grpcontno,prtno,"
  			+" cvalidate,"
  			+" date(enddate)-1 day,"
  			+" conttype,proposalcontno,"
  			+" (case when db2inst1.CodeName('polstate', p.PolState) is not null then db2inst1.CodeName('polstate', p.PolState) else db2inst1.CodeName('stateflag', p.stateflag) end),"
  			+" paytodate,"
  			+" (select riskname from lmrisk m where m.riskcode=p.riskcode )," 
  			//wdx注释了原来的字段
//  			+ " case when p.riskcode='162401' then ( cast(round( DB2INST1.CALMONEYVALUE( p.ContNo  , p.polno ,  p.riskcode  )  ,2)   as   numeric(30,2)) ) else (select nvl(Money,0) from LcInsureAccTrace where 1=1 and polno=p.polno ) end , "
  	 		+" ' ',"
  			+" (select codename from ldcode where codetype='contplancode' and code = p.contplancode), "
  			+" (select codename from ldcode where codetype = 'occupationtype' and code = p.OccupationType ), "
  	 		// wdx增加一列
  	 		+" '',"
  			+" polno "
  			+" from LCPol p where appflag='1' "
	        +" and InsuredNo = '"+tInsuredNo+"'"
	        +" and p.StateFlag in ('2','3')";
  	 strSQL +=" union select p.ContNo,(select AppntName from lbcont where contno=p.contno union select appntname from lccont where contno=p.contno), "
  		+" (case when grpcontno='00000000000000000000' then (select case max(HighestEvaLevel) when 1 then '低' when 2 then '中' when 3 then '高' else '' end  from fx_FXQVIEW where AOGTYPE='1' and " +
			" AOGNAME=(select appntname from lcappnt where contno=p.contno union all select appntname from lbappnt where contno=p.contno)and " +
			" APPNTIDTYPE=(select idtype from lcappnt where contno=p.contno union all select idtype from lbappnt where contno=p.contno) and  " +
			" APPNTIDNO=(select idno from lcappnt where contno=p.contno union all select idno from lbappnt where contno=p.contno) and " +
			" APPNTBIRTHDAY=(select APPNTBIRTHDAY from lcappnt where contno=p.contno union all select APPNTBIRTHDAY from lbappnt where contno=p.contno) and " +
			" APPNTSEX=(select APPNTSEX from lcappnt where contno=p.contno union all select APPNTSEX from lbappnt where contno=p.contno) fetch first row only )" +
			"else (select case max(HighestEvaLevel) when 1 then '低' when 2 then '中' when 3 then '高' else '' end  from fx_FXQVIEW where AOGNAME=p.AppntName and AOGTYPE='2' fetch first row only) end ), "
  	 		+" p.ManageCom,p.grpcontno,p.prtno,p.cvalidate,"
  	 		+" date(p.enddate)-1 day,"
  	 		+" p.conttype,p.proposalcontno,"
  	 		//#2166 理赔页面终止时间页面调整、理算有效期判断逻辑调整
  	 		+" db2inst1.codeName('stateflag', StateFlag),"
  	 		//+ " (case (select 1 from llcontdeal where edorno=p.edorno) when 1 then (select case edortype when 'CT' then '解约' when 'HZ' then '合同终止' when 'CD' then '合同终止退费' when 'RB' then '解约回退' end from llcontdeal where edorno=p.edorno) "
      		//+ " else (select min(EdorName) from LMEdorItem where EdorCode = b.EdorType) end) , "
  	 		+" (CASE WHEN (SELECT date(EDORVALIDATE)-1 day FROM LPEDORITEM WHERE EDORNO = P.EDORNO "
  	 		+" AND EDORTYPE !='XB' and contno = p.contno order by modifydate desc fetch first  row only) is null "
  	 		+" then p.enddate else (SELECT EDORVALIDATE FROM LPEDORITEM WHERE EDORNO = P.EDORNO AND EDORTYPE !='XB' "
  	 		+" and contno = p.contno order by modifydate desc fetch first row only) end),"
  	 		+" (select riskname from lmrisk m where m.riskcode=p.riskcode ) ," 
  			//wdx注释了原来的字段
//  	 		+ " case when p.riskcode='162401' then ( cast(round( DB2INST1.CALMONEYVALUE( p.ContNo  , p.polno ,  p.riskcode  )  ,2)   as   numeric(30,2)) ) else (select nvl(Money,0) from LbInsureAccTrace where 1=1 and polno=p.polno ) end , "
  	 		+" ' ',"
  	 		+" (select codename from ldcode where codetype='contplancode' and code = p.contplancode), "
  	 		+" (select codename from ldcode where codetype = 'occupationtype' and code = p.OccupationType ), "
  	 		// wdx增加一列
  	 		+" '',"
  	 		+" p.polno "
  	 		+" from LBPol p where p.appflag='1'"
	        +" and p.InsuredNo = '"+tInsuredNo+"' with ur";
	//strSQL += " union select ContNo,(select AppntName from lccont where contno=p.contno union select appntname from lbcont where contno=p.contno),ManageCom,grpcontno,prtno,cvalidate,(CASE WHEN (SELECT date(EDORVALIDATE)-1 day FROM LPEDORITEM WHERE EDORNO = P.EDORNO AND EDORTYPE !='XB' and contno = p.contno order by modifydate desc fetch first  row only) is null then p.enddate else (SELECT EDORVALIDATE FROM LPEDORITEM WHERE EDORNO = P.EDORNO AND EDORTYPE !='XB' and contno = p.contno order by modifydate desc fetch first  row only) end),conttype,proposalcontno,(select riskshortname from lmrisk m where m.riskcode=p.riskcode ) from LBPol p where appflag='1' "
	       //    + " and InsuredNo = '"+tInsuredNo+"'";
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  // wdx增加补充医疗保险费率折扣
  Supdiscountfactor("page");
  
  //wdx调用了提交表单方法
  fm.submit(); //提交
  return true;
}
function Supdiscountfactor(strSQL){
	var tInsuredNo = fm.InsuredNo.value;
 	
		
	 
if( strSQL=="" || strSQL==null || tInsuredNo=="" || tInsuredNo==null || tInsuredNo=="undefined"){
	return ;
}
var  SQL=strSQL;
var contnos; //如果是一行，  .value有值。
var length=0;
	if(SQL=="page"){
		//点击下一页等的操作。
		var contval=fm.PolGrid1.value;
		if(contval!="undefined" && contval!=null){ //一条
			length=1;
			contnos=contval;
		}else{
		contnos=fm.PolGrid1;
		}
	}
//	else{
//		//录入理赔号后回车。
//		contnos = easyExecSql(SQL);
//	}

	if(contnos==false ||  contnos=="" || contnos==null || contnos=="undefined"){
		return ;
	}
if(length!=1){
	if(contnos.length>10){
		length=10;
	}else{
		length=contnos.length
	}
}
	for ( var a = 0; a < length; a++) {
		document.getElementsByName("PolGrid17")[a].value="";
//		return ;
	}
	
	for ( var int = 0; int < length; int++) {
		var contno;
		if(length==1){ //页面只有一条数据
			contno=contnos;
		}else{ //页面是多条数据
//			if(SQL=="page"){
				contno=contnos[int].value;
//			}else{
//				contno=contnos[int][0];
//			}
		}
		
		var sql=" select distinct prtno,riskcode from lcpol where 1=1 "
			+ "  and (StateFlag is null or StateFlag in ('1')) "
			+ "  and appflag='1' "
			+ "  and InsuredNo='"+tInsuredNo+"' "
			+ "  and contno='"+contno+"' "
			+ "  and riskcode in (select riskcode from lmriskapp where 1=1  and taxoptimal='Y') "
			;
//	sql="  select a.prtno from lcpol a where 1=1    and a.prtno in ('0201609010807','0201609011101','0201609011000','1000000000102','1000000000113','1000000000203','1000010000305','1000010000406' ) " ;
		
		var prtnos=easyExecSql(sql);  //
	
	if(prtnos==false || prtnos=="" || prtnos==null || prtnos=="undefined"){
		// 没查询到就为空
		document.getElementsByName("PolGrid17")[int].value="";
		continue;
	}
	var prt= prtnos;
	var bool=false;
		for ( var int2 = 0; int2 < prt.length; int2++) {
			
			var prtno=prt[int2][0];
			var riskcode=prt[int2][1];
			if(riskcode=="122601" || riskcode=="122901"){
				var sql=" select distinct Supdiscountfactor from lccontsub where 1=1 and prtno='"+prtno+"' ";					
			}else{
				var sql=" select distinct PremMult from lccontsub where 1=1 and prtno='"+prtno+"' ";					
			}
			
			 var Supdiscountfactors=easyExecSql(sql);
			if( Supdiscountfactors==false || Supdiscountfactors=="" || Supdiscountfactors==null || Supdiscountfactors=="undefined" ){
				// 如果为空，暂时定位 已享受，待IT确认
				// document.getElementsByName("PolGrid17")[int].value="";
				continue;
			} else{
			var Sf=Supdiscountfactors;
			
			 
			for ( var int3 = 0; int3 < Sf.length; int3++) {
				
				var Supdiscountfactor=Sf[int3][0];
		 	var re = /^[0-9]+.?[0-9]*$/;   // 判断字符串是否为数字 //判断正整数
											// /^[1-9]+[0-9]*]*$/
		     if (!re.test(Supdiscountfactor)) {
//		        alert("该保单("+contno+")补充医疗保险折扣因子为("+Supdiscountfactor+"),不为数字！");
		   continue;
		     }
		      Supdiscountfactor=parseFloat(Supdiscountfactor);
		     if(Supdiscountfactor>=0 && Supdiscountfactor<1){
		    // document.getElementsByName("PolGrid17")[int].value="已享受";
		    	 bool=true;
		    	 continue;
		     }else if(Supdiscountfactor==2){
		    	 bool=true;
		    	 continue;
		     }
		}
		}
		
	}	
		if(bool){  
			document.getElementsByName("PolGrid17")[int].value="已享受";
			continue;
		}else{
			document.getElementsByName("PolGrid17")[int].value="未享受";
			continue;
		}
	
	}
	// 书写SQL查询折扣率，最后转换成。
	// 若险种名称为税优险种，则系统判断该险种对应保单的“补充医疗保险折扣因子”数值（从承保信息获取）是否小于100%，小于100%，则显示“已享受”，等于100%，则显示“未享受”。
}
function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult != null )
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
			CheckGrid.setRowColData( i, j+1, arrResult[i][j] );
			}
		}
		//alert("result:"+arrResult);
	}
}

function Make()
{
  var i = 0;
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function onSelSelected()
{
	var row=PolGrid.getSelNo()-1;
	var ContNo="";
	var proposalcontno="";
	//fm.UWRemark.value = "";
	if(row>=0)
	{

		fm.ContNo.value = PolGrid.getRowColData(row,5);//团体合同号
		fm.ContNoA.value=PolGrid.getRowColData(row,1);//个单合同号
		proposalcontno = PolGrid.getRowColData(row,10);//投保单号码
		
		//查询合同下最近的一次保全工单号
		var tQueryEdorNo = "select edorno from lpedoritem where contno='"+PolGrid.getRowColData(row,1)+"'" +
			" order by makedate desc ,maketime desc fetch first 1 rows only with ur";
		var tResultEdorNo = easyExecSql(tQueryEdorNo);
		if(tResultEdorNo){
			fm.EdorNo.value = tResultEdorNo[0][0];
		}else{
			fm.EdorNo.value = "";
		}
		//fm.EdorNo.value = '20130606000006';
		this.mEdorNo = fm.EdorNo.value;//合同下最近的一次保全工单号
		
		fm.Remark.value="";
		var tsql = "select remark from lcgrpcont where grpcontno='"+fm.ContNo.value+"'";
		brr = easyExecSql(tsql);
		if(brr){
		  fm.Remark.value = brr[0][0]+"\n";
		}
		tsql = "select speccontent from lcspec a where a.contno='"+proposalcontno+"'";
		crr = easyExecSql(tsql);
		if(crr){
		  for(i=0;i<crr.length;i++){
		    fm.Remark.value += crr[i][0]+"\n";
		  }
		}
		
		//TODO:理赔二核核保信息
		//ContNo = PolGrid.getRowColData(row,1);
		//var PolNo = PolGrid.getRowColData(row,13);
		//var tUWIdeaSql = "select uwidea from lpuwmaster where contno='"+ContNo+"' and polno='"+PolNo+"'";
		//var tUWIdea = easyExecSql(tUWIdeaSql);
		//if(tUWIdea){
		//	for(i=0;i<tUWIdea.length;i++){
		//	fm.UWRemark.value += tUWIdea[i][0]+"\n";
		//}
		//document.getElementById('UWRemark').setAttribute('readOnly',true);
		//}
		//显示“补充告知”
		showEdorInfo();
	}
	
}

function DealCont()
{
	var selno = PolGrid.getSelNo()-1;
	if (selno >=0)
	{
		cContNo = PolGrid.getRowColData(selno,1);
		cPrtNo = PolGrid.getRowColData(selno,6);
		
	}
  
  window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+cPrtNo+"&ContNo="+cContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");  	  
}

function RenewSuggest()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.InsuredNo.value+"&CustomerName="+fm.CustomerName.value;
	showInfo = window.open("./FrameMainRenewSuggest.jsp?Interface=LLRenewSuggestInput.jsp"+varSrc,"续保建议",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function DealExemption()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.InsuredNo.value+"&CustomerName="+fm.CustomerName.value;
	showInfo = window.open("./FrameMainExemption.jsp?Interface=LLExemptionInput.jsp"+varSrc,"续保建议",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}


//将加费的信息显示出来；
function ShowAddFeeInfo()
{
		var strSql = "select prem,round(rate,2),paystartdate,payenddate from lcprem where ContNo='"
		+fm.ContNoA.value+"' and substr(payplancode,1,6)='000000' ";
		turnPage.queryModal(strSql, AddFeeGrid);
}

//将免责的信息显示出来；
function ShowSpecInfo()
{
		var strSql = "select SpecCode,SpecContent,'','',SerialNo from LCSpec where ContNo='"
		+fm.ContNoA.value+"'";
		turnPage.queryModal(strSql, SpecGrid);
}

function QueryOnKeyDown()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    var tsql = "select CustomerName,CustomerNo from llcase where CaseNo='"+fm.CaseNo.value+"'";
    var arr = easyExecSql(tsql);
    if(arr){
      fm.CustomerName.value = arr[0][0];
      fm.InsuredNo.value = arr[0][1];
    }
    easyQueryClick();
  }
}

function QueryOnEnter()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    var tsql = "select CustomerNo,Name from ldperson where 1=1"
    +getWherePart("Name","CustomerName")
    +getWherePart("CustomerNo","InsuredNo");
    var arr = easyExecSql(tsql);
    if (arr)
    {
      try
      {
        if(arr.length==1)
        {
          afterLLRegister(arr);
        }
        else{
          var varSrc = "&CustomerNo=" + fm.InsuredNo.value;
          varSrc += "&CustomerName=" + fm.CustomerName.value;
          varSrc += "&ContNo=";
          varSrc += "&IDType=";
          varSrc += "&IDNo=" ;
          showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
        }
      }catch(ex)
      {
        alert("错误:"+ ex.message);
      }
    }
  }
}
 
function afterLLRegister(arrReturn)
{
  fm.InsuredNo.value=arrReturn[0][0];
  fm.CustomerName.value=arrReturn[0][1];
      easyQueryClick();
}

function ScanQuery() {
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else {
	    var prtNo = PolGrid.getRowColData(tSel - 1,6);
	    var conttype = PolGrid.getRowColData(tSel-1,9);				
		
		if (prtNo == "") return;
		    
//		  window.open("../sys/ClaimGetQueryMain.jsp?PolNo=" + cPolNo);		
    if(conttype=='1')
		  window.open("../sys/ProposalEasyScan.jsp?prtNo="+prtNo+"&SubType=TB1001&BussType=TB&BussNoType=11", "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
		else
	    window.open("../sys/ProposalEasyScan.jsp?prtNo="+ prtNo + "&SubType=TB1002&BussType=TB&BussNoType=12" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");					
	}	     
}
