var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var tSaveType="";

//提交，保存按钮对应操作
function submitForm()
{

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  showInfo.close();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    if (fm.fmtransact.value=="DELETE||MAIN")
    {
      fm.reset();
      initCustomerGrid();
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1"){
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else{
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

function UWClaim()
{
  var selno = GrpRegisterGrid.getSelNo();
  if (selno <=0)
  {
    alert("请选择一条团体案件！");
    return ;
  }

  //window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  var varSrc="";
  var newWindow = window.open("./FrameMain.jsp?Interface=PayAffirmInput.jsp"+varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SearchGrpRegister()
{

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  // 书写SQL语句
  var strSql = " SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO, R.RGTANTNAME, R.RGTDATE, R.APPPEOPLES, b.codename,R.rgtstate " +
  " FROM LLREGISTER R , ldcode b " +
  " WHERE R.RGTOBJ = '0' AND R.RGTCLASS = '1' AND R.declineflag is null " +
  " and b.codetype='llgrprgtstate' and b.code = r.rgtstate "
  + getWherePart("r.MngCom","ManageCom","like")

  if (fm.all('srRgtNo').value != '')	//团体批次号
  strSql = strSql + " AND R.RGTNO = '" + fm.all('srRgtNo').value + "'";

  if (fm.all('srCustomerNo').value != '')	//客户号
  strSql = strSql + " AND R.CUSTOMERNO = '" + fm.all('srCustomerNo').value + "'";
  if (fm.all('srGrpName').value != '')	//客户姓名
  strSql = strSql + " AND A.GRPNAME = '" + fm.all('srGrpName').value + "'";
  if (fm.all('srRgtantName').value != '')	//申请人
  strSql = strSql + " AND R.RGTANTNAME = '" + fm.all('srRgtantName').value + "'";

  //申请日期区间查询
  var blDateStart = !(fm.all('RgtDateStart').value == null ||
  fm.all('RgtDateStart').value == "");
  var blDateEnd = !(fm.all('RgtDateEnd').value == null ||
  fm.all('RgtDateEnd').value == "");

  if(blDateStart && blDateEnd){
    strSql = strSql + " AND R.RGTDATE BETWEEN '" +
    fm.all('RgtDateStart').value + "' AND '" +
    fm.all('RgtDateEnd').value + "'";
  }
  else if(blDateStart){
    strSql = strSql + " AND R.RGTDATE > '" + fm.all('RgtDateStart').value + "'";
  }
  else if(blDateEnd){
    strSql = strSql + " AND R.RGTDATE < '" + fm.all('RgtDateEnd').value + "'";
  }
  strSql += " order by r.rgtno desc"
  turnPage.queryModal(strSql,GrpRegisterGrid);
  showInfo.close();
}

function CaseEnsure()
{
  var CaseCount=GrpCaseGrid.mulLineCount;
  var chkFlag=false;
  for (i=0;i<CaseCount;i++){
    if(GrpCaseGrid.getChkNo(i)==true){
      chkFlag=true;
    }
  }
  if (chkFlag==false){
    alert("请选中审批通过的案件");
    return false;
  }

  fm.all('operate').value	= "GIVE|ENSURE";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交

}

function queryGrpCaseGrid()
{
  var selno = GrpRegisterGrid.getSelNo();
  if (selno <= 0)
  {
    return ;
  }
  var jsRgtNo = GrpRegisterGrid.getRowColData(selno - 1, 1);
  fm.RgtNo.value = jsRgtNo;
  if(jsRgtNo == null || jsRgtNo == "")
  {
    alert("团体批次号为空！");
    return;
  }
  var StateRadio="";
  for(i = 0; i <fm.StateRadio.length; i++){
    if(fm.StateRadio[i].checked){
      StateRadio=fm.StateRadio[i].value;
      break;
    }
  }

  var strSql = " SELECT C.CASENO, C.OTHERIDNO,C.CUSTOMERNAME, C.CUSTOMERNO, C.RGTDATE, B.APPLYAMNT"
  +" FROM LLCASE C, LLSECURITYRECEIPT b  WHERE C.RGTNO = '"+ jsRgtNo + "' and b.caseno =c.caseno";
  switch(StateRadio){
    case "0":
    sqlpart = " ORDER BY C.CASENO ";
    break;
    case "1":
    sqlpart = " and c.rgtstate = '03' ORDER BY C.CASENO ";
    break;
    case "2":
    strSql = " SELECT C.CASENO, C.OTHERIDNO,C.CUSTOMERNAME, C.CUSTOMERNO, C.RGTDATE, B.APPLYAMNT,"
      +"d.reason,d.remark "
      +" FROM LLCASE C, LLSECURITYRECEIPT b, llcaseback d WHERE C.RGTNO = '"+ jsRgtNo 
      + "' and b.caseno =c.caseno and d.caseno = c.caseno and d.aviflag= 'Y' ";
    sqlpart = " and c.rgtstate='01' ORDER BY C.CASENO ";
    break;
    case "3":
    sqlpart = " and c.rgtstate='09' ORDER BY C.CASENO ";
  }
  initGrpCaseGrid(StateRadio);
  strSql+=sqlpart;
  turnPage2.queryModal(strSql,GrpCaseGrid);
  divGrpCaseInfo.style.display = '';
}

function QueryOnKeyDown()
{
  var keycode = event.keyCode;

  if(keycode=="13")
  {
    SearchGrpRegister();
  }
}

function afterCodeSelect( cCodeName, Field )
{
}

function CaseInfo()
{
  var selno = GrpCaseGrid.getSelNo();
  if (selno <= 0)
    return ;
  var aCaseNo = GrpCaseGrid.getRowColData(selno - 1, 1);
  varSrc = "";
  var varSrc= "&RgtNo="+fm.RgtNo.value+"&CaseNo="+aCaseNo;
  switch(fm.RgtNo.value.substring(1,3)){
    case "94":
    pathStr="./FrameMainSim.jsp?Interface=LLQDSimpleClaimInput.jsp"+varSrc;
    break;
    case "53":
    pathStr="./FrameMainSim.jsp?Interface=LLYNSimpleClaimInput.jsp"+varSrc;
    break;
    default:
    pathStr="./FrameMainSim.jsp?Interface=LLRegisterInput.jsp"+varSrc;
  }
  var newWindow=OpenWindowNew(pathStr,"简易录入","left");
}

