//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var interval = 1000;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm() {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  window.focus();
  queryGrpCaseGrid();
  QueryErrList();
  queryList();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
  initForm();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
  try {
    initForm();
  } catch(re) {
    alert("在LCInuredList.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm() {
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
    parent.fraMain.rows = "0,0,50,82,*";
  } else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function deleteClick() {
  //下面增加相应的删除代码
  if (confirm("您确实想删除这些记录吗?")) {
    getBatchCaseNo();
      fm.fmtransact.value = "DELETE||MAIN";
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.action ="./CaseDeleteSave.jsp?RgtNo="+fm.RgtNo.value+"&BatchCaseNo="+fm.BatchCaseNo.value;
      fm.submit(); //提交
  } else {
    alert("您取消了删除操作！");
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow) {
  if (cShow=="true") {
    cDiv.style.display="";
  } else {
    cDiv.style.display="none";
  }
}

function queryList(){
  var strSql = "select peoples2 from lcgrpcont where grpcontno = '"+fm.GrpContNo.value+"'";
  var arr = easyExecSql(strSql);
  if(arr)
    fm.SumInsured.value = arr[0][0];
  strSql = "select count(CASENO) from llcase where RGTNO = '"+fm.RgtNo.value+"'";
  var crr = easyExecSql(strSql);
  if(crr)
    fm.SuccInsured.value = crr[0][0];
  fm.HoldInsured.value = fm.AppPeople.value-fm.SuccInsured.value;
}

function Scroll() {
  strSql = "select count(CASENO) from llcase where RGTNO = '"+fm.RgtNo.value+"' and calflag='1'";
  var crr = easyExecSql(strSql);
  if(crr)
    fm.SuccInsured.value = crr[0][0];

      window.setTimeout("Scroll();", interval );interval=1000
    }
