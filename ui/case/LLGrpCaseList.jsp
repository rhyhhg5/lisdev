<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
<%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
  <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLGrpCaseList.js"></SCRIPT>
   <%@include file="LLGrpCaseListInit.jsp"%>
   
      <script language="javascript">
   function initDate(){
   		fm.RgtDateStart.value="<%=afterdate%>";
   		fm.RgtDateEnd.value="<%=CurrentDate%>";
   		var usercode="<%=Operator%>";
   		var comcode="<%=Comcode%>";
//   		fm.Operator.value=usercode;
//   		fm.OrganCode.value=comcode;
//   		
//   		
//   		var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
//   		var arrResult = easyExecSql(strSQL);
//			if(arrResult != null)
//			{
//			         fm.optname.value= arrResult[0][0];
// 			}
   }
   </script>
 </head>

 <body  onload="initDate();initForm();" >
   <form action="./LLGrpCaseListSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegisterSearch);">
         </TD>
         <TD class= titleImg>
         团体给付确认工作列表
         </TD>
       </TR>
      </table>
      

	    <div id= "divGrpRegisterSearch" style= "display: ''" >
		<table  class= common>
		<TR  class= common8>

      <TD  class= title id='titleCaseNo'> 管理机构</TD><TD  class= input>
  <Input class= "codeno"  name=MngCom  ondblclick="return showCodeList('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,MngComName],[0,1],null,null,null,1);" onkeydown="QueryOnKeyDown();"><Input class=codename  name=MngComName>
	 </TD>
		<TD  class= title8>团体客户号</TD>
		<TD  class= input8><Input class=common name="srCustomerNo" onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>单位名称</TD>
		<TD  class= input8><Input class=common name="srGrpName" onkeydown="QueryOnKeyDown();"></TD>
	
		</TR>
		<tr>
		<TD  class= title8>申请日期 起始</TD>
		<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateStart onkeydown="QueryOnKeyDown();"></TD>
		<TD  class= title8>结束</TD>
		<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateEnd onkeydown="QueryOnKeyDown();"></TD>			
		</tr>
		</table>
	    </div>

   <input name="AskIn" style="display:'none'"  class=cssButton type=button value="查 询" onclick="SearchGrpRegister()">
   

    <Div  id= "divCustomerInfo" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpRegisterGrid" >
            </span>
          </TD>
        </TR>
      </table>
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 	
    </Div>
    <!--
        <hr>
        
<input name="AskIn" style="display:''"  class=cssButton type=button value="团体给付确认" onclick="RgtGiveEnsure()">
<input name="AskIn" style="display:'none'"  class=cssButton type=button value="团体给付通知打印" onclick="UWClaim()">
<input name="AskIn" style="display:''"  class=cssButton type=button value="团体汇总明细打印" onclick="GrpColPrt()">
<input name="AskIn" style="display:''"  class=cssButton type=button value="团体给付凭证打印" onclick="GPrint()">
      -->    
			<br>
	    <hr>
	  	<table>
	    	<tr>
	        <td class=common>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCaseGrid);">
	    	</td>
	    	<td class= titleImg>
	    	统计信息 
	    </td><td style="width:100px"  align=right><input type=button class=cssButton value="统计计算" OnClick="ShowInfo();"></td>
	    </tr>
	    </table>
    <Div  id= "divGrpCaseGrid" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpCaseGrid" >
            </span>
          </TD>
        </TR>
      </table>	
    	<Div align=center>
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage2.lastPage();">      
   	  </Div> 
		</Div>  
     
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="operate" >
     <Input type="hidden" class= common name="RgtNo" >
     <Input type="hidden" class= common name="ComCode"  value="<%=Comcode%>">
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
