<%
//程序名称：ProposalPrintInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
  
  var tIntlFlag = "<%=request.getParameter("IntlFlag")%>";
  var tSaleChnlCode = ("1" == tIntlFlag ? "salechnlall" : "LCSaleChnl");
  
// 输入框的初始化（单记录部分）
function initInpBox()
{
	try
	{
		fm.reset();
		
		if("1" == tIntlFlag)
		{
		  fm.all("PrintStateTRID").style.display = "";
		  fm.all("downloadCustomerCardButton").style.display = "";
		  fm.all("downloadCustomerCardButtonExcel").style.display = "";
		}
	}
	catch(ex)
	{
		alert("在YBKPrintInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm()
{
    try
    {
        initInpBox();
        initContGrid();
       
    }
    catch(re)
    {
        alert("YBKPrintInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
}

//定义为全局变量，提供给displayMultiline使用
var ContGrid;

//保单信息列表的初始化
function initContGrid()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";	//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";	//列宽
		iArray[0][2]=10;	//列最大值
		iArray[0][3]=0;	//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="保险合同号";
		iArray[1][1]="100px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="ContNo";

		iArray[2]=new Array();
		iArray[2][0]="印刷号";
		iArray[2][1]="100px";           
		iArray[2][2]=100;            	
		iArray[2][3]=0;
		iArray[2][21]="PrtNo";

		iArray[3]=new Array();
		iArray[3][0]="被保人客户号";
		iArray[3][1]="80px";
		iArray[3][2]=200;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="身份证号";
		iArray[4][1]="80px";
		iArray[4][2]=200;
		iArray[4][3]=0;


		ContGrid = new MulLineEnter( "fm" , "ContGrid" );
		//这些属性必须在loadMulLine前
		ContGrid.mulLineCount = 0;
		ContGrid.displayTitle = 1;
		ContGrid.hiddenPlus = 1;
		ContGrid.hiddenSubtraction = 1;
		ContGrid.canSel = 1;
		ContGrid.canChk = 0;
		ContGrid.loadMulLine(iArray);
		
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>