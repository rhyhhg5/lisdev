<%
//Name：RegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
 
  String LoadFlag= request.getParameter("LoadFlag");
  LoadFlag= LoadFlag==null?"":LoadFlag;
  String RgtNo= request.getParameter("RgtNo") ;
 
  //RgtNo=RgtNo== null ?"":RgtNo;
  String CaseNo= request.getParameter("CaseNo");
  //CaseNo=CaseNo==null?"":CaseNo;
  String LoadC="";
		if(request.getParameter("LoadC")!=null)
		{
			LoadC = request.getParameter("LoadC");
		}
%>

<script language="JavaScript">
var loadFlag="<%=LoadFlag%>";
var RgtNo = '<%=RgtNo%>';
 RgtNo= (RgtNo=='null'?"":RgtNo);
var CaseNo = '<%=CaseNo %>';
CaseNo= (CaseNo=='null'?"":CaseNo);

function initInpBox( )
{
  try {
     fm.reset();
     	fm.CustomerNoQ.value="";
        fm.LoadFlag.value = loadFlag;
        fm.RgtNo.value = RgtNo;
        fm.CaseNo.value = CaseNo;
        fm.LoadC.value="<%=LoadC%>";
    if (fm.LoadC.value=='2')
    {
 //    alert(fm.LoadC.value);
    aa.style.display='none';}

  } catch(ex) {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常aaaaaaaaa:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常yyyyyyy:初始化界面错误!");
  }
}

function initForm()
{
  try
  {

	titleGrp.style.display='none';
	inputGrp.style.display='none';
 
	if(loadFlag=="0")
	{
		titleGrp.style.display='';
		inputGrp.style.display='';
		div1.style.display='none';
		divRegisterInfo.style.display='none';
		fm.Relation.value='05';
		fm.RgtType='9';
	}   
	
    initInpBox();
   // initCaseGrid();

   // initAffixGrid();
   // initBnfGrid();
   // initInsuredEventGrid();
   //initAppReasonGrid();     
   initEventGrid();
   checkRgtFlag();
   initQuery();
 	  <%GlobalInput mG = new GlobalInput();
  	mG=(GlobalInput)session.getValue("GI");
  	%>
     fm.Handler.value = "<%=mG.Operator%>";
     fm.ModifyDate.value = getCurrentDate();
   //QueryApplyReason();
  }
  catch(re)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
  }
}

// 保单信息列表的初始化
function initEventGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="事件号";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="发生日期";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][9]="发生日期|notnull&date";
    
    iArray[3]=new Array();
    iArray[3][0]="发生地点";
    iArray[3][1]="150px";
    iArray[3][2]=60;
    iArray[3][3]=0;
  
   iArray[4] = new Array("事故类型","80px","0","3");
   iArray[5] = new Array("事故主题","200px","0","3");
   iArray[6] = new Array("医院名称","200px","0","3");
   
   iArray[7]=new Array();
   iArray[7][0]="入院日期";
   iArray[7][1]="80px";
   iArray[7][2]=10;
   iArray[7][3]=0;
   iArray[7][9]="入院日期|date";
   
   iArray[8]=new Array();
   iArray[8][0]="出院日期";
   iArray[8][1]="80px";
   iArray[8][2]=10;
   iArray[8][3]=0;
   iArray[8][9]="出院日期|date";
   
   iArray[9] = new Array("事件信息","200px","1000","0");
   iArray[10] = new Array("事件类型","60px","10","0")

   iArray[11] = new Array("事件类型","60px","10","3")
   
    EventGrid = new MulLineEnter("fm","EventGrid");
    EventGrid.mulLineCount = 0;
    EventGrid.displayTitle = 1;
    EventGrid.locked = 0;
    EventGrid.canChk =1	;
    EventGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    EventGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    EventGrid.loadMulLine(iArray);
    //SubReportGrid.selBoxEventFuncName = "ShowRela";
  }
  catch(ex)
  {
    alter(ex);
  }
}

//立案附件信息
function initAffixGrid()
{
    var iArray = new Array();

      try
      {
      
      
          iArray[0]=new Array("序号","30px","0",1);
          iArray[1]=new Array("材料类型","80px","0","1");
	      iArray[2]=new Array("材料代码","80px","0",0);
	      iArray[3]=new Array("材料名称","180px","0",0);
	      iArray[4]=new Array("提供日期","100px","0",1);
	      iArray[5]=new Array("有无标记","60px","0",2);
	      iArray[6]=new Array("附件号码","60px","0",3);
	
      AffixGrid = new MulLineEnter( "fm" , "AffixGrid" );
      //这些属性必须在loadMulLine前
      AffixGrid.mulLineCount = 1;
      AffixGrid.displayTitle = 1;
      AffixGrid.hiddenPlus=1;   
      AffixGrid.hiddenSubtraction=1; 
      AffixGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //AffixGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }

function initBnfGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("受益人客户名称","80px","0",1);
   iArray[2]=new Array("与被保险人关系","80px","0",2,"Relaction");
   iArray[3]=new Array("受益人顺序","60px","0",1);
   iArray[4]=new Array("受益份额","60px","0",1);
   iArray[5]=new Array("领款方式","60px","0",2);
   iArray[6]=new Array("银行代码","60px","0",2);
   iArray[7]=new Array("账号","100px","0",1);
   iArray[8]=new Array("户名","100px","0",1);

    BnfGrid = new MulLineEnter("fm","BnfGrid");
    BnfGrid.mulLineCount = 1;
    BnfGrid.displayTitle = 1;
    BnfGrid.locked = 0;
    BnfGrid.canChk =1;
    BnfGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    BnfGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //   BnfGrid. selBoxEventFuncName = "onSelSelected";
    BnfGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initInsuredEventGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("事件号","100px","0",0);
   iArray[2]=new Array("发生日期","100px","0",0);
    iArray[3]=new Array("发生地点","100px","0",0);
   iArray[4]=new Array("事件类型","100px","0",0);
   iArray[5]=new Array("主题","300px","0",0);
   

    InsuredEventGrid = new MulLineEnter("fm","InsuredEventGrid");
    InsuredEventGrid.mulLineCount = 1;
    InsuredEventGrid.displayTitle = 1;
    InsuredEventGrid.locked = 0;
    InsuredEventGrid.canChk =0;
    InsuredEventGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    InsuredEventGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    InsuredEventGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initAppReasonGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0","0");
    iArray[1]=new Array("原因代码","100px","2","2");
    iArray[2]=new Array("申请原因","300px","0","0");
    
    iArray[1]=new Array();
    iArray[1][0]="原因代码";         			//列名
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=2;            			//列最大值
    iArray[1][3]=2;
    iArray[1][4]="llrgtreason";
    iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
    iArray[1][9]="原因代码|NOTNULL";

    iArray[2]=new Array();
    iArray[2][0]="申请原因";         			//列名
    iArray[2][1]="300px";            		//列宽
    iArray[2][2]=0;            			//列最大值
    iArray[2][3]=0;
    iArray[2][4]="codename"
   

    AppReasonGrid = new MulLineEnter("fm","AppReasonGrid");
    AppReasonGrid.mulLineCount = 1;
    AppReasonGrid.displayTitle = 1;    
    AppReasonGrid.canSel =0;
    AppReasonGrid.canChk =1;
    AppReasonGrid.hiddenPlus=0;   
    AppReasonGrid.hiddenSubtraction=0; 
    //AppReasonGrid.selBoxEventFuncName = "onSelSelected";
 
    AppReasonGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
//立案分案信息
function initCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="分案号";    	//列名
    iArray[1][1]="0px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="事故者客户号";         			//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="事故者名称";         			//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=60;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="性别";         		//列名
    iArray[4][1]="40px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=2;
    iArray[4][4]="Sex";
    //是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[5]= new Array();
    iArray[5][0]="证件类型";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=2;
    iArray[5][4]="IDType";

    iArray[6]= new Array();
    iArray[6][0]="证件号码";
    iArray[6][1]="150px";
    iArray[6][2]=100;
    iArray[6][3]=1;

    iArray[7]= new Array();
    iArray[7][0]="年龄";
    iArray[7][1]="40px";
    iArray[7][2]=100;
    iArray[7][3]=1;

    iArray[8]=new Array();
    iArray[8][0]="事故类型";
    iArray[8][1]="50px";
    iArray[8][2]=100;
    iArray[8][3]=0;

    iArray[9]=new Array();
    iArray[9][0]="分报案号码";
    iArray[9][1]="0px";
    iArray[9][2]=100;
    iArray[9][3]=0;

    CaseGrid = new MulLineEnter( "fm" , "CaseGrid" );
    //这些属性必须在loadMulLine前
    CaseGrid.mulLineCount = 0;
    CaseGrid.displayTitle = 1;
    CaseGrid.canChk =1;
    CaseGrid.hiddenPlus=1;
    CaseGrid.locked = 1;
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}

function initQuery()
{

        var tRgtNo = fm.RgtNo .value;
        var tCaseNo = fm.CaseNo.value;	
	ClientafterQuery(tRgtNo,tCaseNo);
	
}

function checkRgtFlag()
{
	strSQL=" select rgtclass, apppeoples from LLRegister where rgtno = '" + fm.all('RgtNo').value + "'";
	arrResult=easyExecSql(strSQL);
	fm.all('rgtflag').value = '0';
	if(arrResult != null)
	{
		fm.all('AppNum').value = arrResult[0][1];
		var RgtClass;
		try{RgtClass = arrResult[0][0]} catch(ex) {alert(ex.message + "RgtClass")}
	  strSQL1 = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
	  arrResult1 = easyExecSql(strSQL1);	
		fm.all('ClientNo').value=arrResult1[0][0]/1+1;
		if (RgtClass == '1') 
		{
			titleGrp.style.display='';
			inputGrp.style.display='';			
			divRgtFinish.style.display='';	
			titleAppNum.style.display='';
			inputAppNum.style.display='';
			titleClientNo.style.display='';
			inputClientNo.style.display='';
			fm.all('rgtflag').value = "1";
			//RgtFinish.style.display='';
			div1.style.display='none';
		        divRegisterInfo.style.display='none';		
		}	
	}
}

</script>