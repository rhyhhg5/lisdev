<%
/******************************************************************************
 * Name     ：FrameEndAgreement.jsp(分案保单明细的窗口)
 * Function ：显示＂解约暂停明细窗口＂
 * Date     ：203-08-05
 * Author   ：LiuYansong
 */
%>
<!--Root="../../" -->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
<title>进行解约暂停</title>
<script language="javascript">
function focusMe()
{
  window.focus();
}
</script>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" frameborder="no" border="1" framespacing="0" cols="*">
	<frame name="VD" src="../common/cvar/CVarData.html">
	<frame name="EX" src="../common/cvar/CExec.jsp">
	<frame name="fraSubmit"  scrolling="yes" noresize src="about:blank" >
	<frame name="fraTitle"  scrolling="no" noresize src="about:blank" >
	<frameset name="fraSet" cols="0%,*,0%" frameborder="no" border="1" framespacing="0" rows="*">
		<frame name="fraMenu" scrolling="yes" noresize src="about:blank">
		<%
			String szSrc = request.getParameter("Interface");
			szSrc += "?CaseNo=" + request.getParameter("CaseNo");
			szSrc += "&InsuredNo=" + request.getParameter("InsuredNo");
			szSrc += "&CustomerName=" + StrTool.unicodeToGBK(request.getParameter("CustomerName"));
      szSrc += "&RgtNo=" + request.getParameter("RgtNo");
      szSrc += "&Type=" + request.getParameter("Type");
			System.out.println("aaaaaaaaaa"+szSrc);
		%>
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="<%= szSrc %>">
    	<frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff" onblur="focusMe();>
	</body>
</noframes>
</html>
