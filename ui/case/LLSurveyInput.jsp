<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<html>    
<%
//程序名称：LAMarketInput.jsp
//程序功能：F1报表生成
//创建日期：2007-11-13
//创建人  ：xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*" %>
<%
    GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
  String CurrentDate= PubFun.getCurrentDate();   
             	               	
  String AheadDays="-30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
%>
 <script>
   var msql=" 1 and   char(length(trim(comcode)))<=#4# ";
 var msql1=" 1 and   branchtype=#2#  and branchtype2=#01# and endflag=#N#";

</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLSurveyInput.js"></SCRIPT>
<script>
	function initDate(){
	  fm.StartDate.value="<%=afterdate%>";
	  fm.EndDate.value="<%=CurrentDate%>";
	  fm.ManageCom.value="<%=tG.ManageCom%>";
	}
  </script>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body onload="initElementtype();initDate();" >    
  <form action="./LLSurveySave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			机构调查案件明细
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>
      	<TR  class= common>
              <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|code:ComCode"   
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,1,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,1,1);"
            ><Input  class='codename' name=ManageComName  > 
          </TD> 
        </TR>
        <TR  class= common>
          <TD  class= title>
             起期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=StartDate  verify="起期|DATE"   >  
          </TD>
          <TD  class= title>
             止期
          </TD>
          <TD  class= input>
           <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="止期|DATE"  >  
          </TD>
    
        </TR>     
       
    </table>

    <input type="hidden" name=op><INPUT VALUE="打 印" class="cssButton" TYPE="button" onclick="submitForm()">
    
    <input type="hidden" name=op><INPUT VALUE="下载(Excel)" class="cssButton" TYPE="button" onclick="submitForm2()">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->   </form>
<table>
 <TR>
   <TD>
     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
   </TD>
	<TD class= titleImg>
		   字段说明：
    </TD>
 </TR>
</table>
		<Div  id= "divCare" >
		  <table class= common border=0 width=100%>
		    <tr class="common"><td class="common">管理机构：</td><td class="common">调查的完成机构</td></tr>
		    <tr class="common"><td class="common">统计时间：</td><td class="common">结案、撤件日期在统计时间内的申请、申诉、纠错案件的调查，受理日期在统计时间内的咨询、通知案件的调查</td></tr>
		 	<tr class="common"><td class="common">案件号:</td><td class="common">调查的案件号</td></tr>
		 	<tr class="common"><td class="common">调查机构编码：</td><td class="common">最终完成调查的机构编码</td></tr>     
			<tr class="common"><td class="common">调查机构：</td><td class="common">最终完成调查的机构</td></tr>     
			<tr class="common"><td class="common">案件机构编码：</td><td class="common">案件所属机构编码</td></tr>
			<tr class="common"><td class="common">案件机构：</td><td class="common">案件所属机构</td></tr>
			<tr class="common"><td class="common">调查序号：</td><td class="common">调查的顺序号</td></tr>
			<tr class="common"><td class="common">中介机构编码：</td><td class="common">该保单所属的中介机构编码</td></tr>     
			<tr class="common"><td class="common">中介机构名称：</td><td class="common">该保单所属的中介机构的名称</td></tr>
			<tr class="common"><td class="common">所属营业单位编码：</td><td class="common">该保单业务员所属营业单位编码</td></tr>     
			<tr class="common"><td class="common">所属营业部：</td><td class="common">该保单业务员所属营业单位名称</td></tr>     
			<tr class="common"><td class="common">业务员编码：</td><td class="common">该保单的业务员编码</td></tr>     
			<tr class="common"><td class="common">业务员姓名：</td><td class="common">该保单的业务员姓名</td></tr>     
			<tr class="common"><td class="common">投保人客户号：</td><td class="common">该保单的投保人客户号</td></tr>     
			<tr class="common"><td class="common">投保人姓名：</td><td class="common">该保单的投保人</td></tr>     
			<tr class="common"><td class="common">险种类型：</td><td class="common">最终结案案件赔付的险种类型（团/个）</td></tr>     
			<tr class="common"><td class="common">被保险人客户号：</td><td class="common">案件的被保险人客户号</td></tr>     
			<tr class="common"><td class="common">被保险人姓名：</td><td class="common">案件的被保险人姓名</td></tr>     
			<tr class="common"><td class="common">险种编码：</td><td class="common">最终结案案件赔付的险种编码</td></tr>     
			<tr class="common"><td class="common">首期保费：</td><td class="common">最终结案案件赔付的险种的首期保费</td></tr>     
			<tr class="common"><td class="common">缴费频次：</td><td class="common">最终结案案件赔付的险种的缴费频次</td></tr>     
			<tr class="common"><td class="common">生效日期：</td><td class="common">该保单的生效日期</td></tr>     
			<tr class="common"><td class="common">出险日期：</td><td class="common">案件的出险日期，取第一条</td></tr>
			<tr class="common"><td class="common">调查开始日期：</td><td class="common">调查的提出日期</td></tr>     
			<tr class="common"><td class="common">调查结束日期：</td><td class="common">调查的完成日期</td></tr>
			<tr class="common"><td class="common">结案/撤件日期：</td><td class="common">案件的结案日期或撤件日期</td></tr>     
			<tr class="common"><td class="common">赔付结论：</td><td class="common">最终结案案件的赔付结论</td></tr>     
			<tr class="common"><td class="common">赔款：</td><td class="common">最终结案案件赔付的险种的赔付金额</td></tr>     
			<tr class="common"><td class="common">疾病编码：</td><td class="common">案件录入的疾病编码，取第一条</td></tr>     
			<tr class="common"><td class="common">疾病名称：</td><td class="common">疾病编码对应的疾病名称</td></tr>
			<tr class="common"><td class="common">意外编码：</td><td class="common">案件录入的意外编码，取第一条</td></tr>     
			<tr class="common"><td class="common">意外名称：</td><td class="common">意外编码对应的意外名称</td></tr>
			<tr class="common"><td class="common">调查人：</td><td class="common">该调查的完成人</td></tr>     
			<tr class="common"><td class="common">直接调查费：</td><td class="common">该次调查录入的直接调查费，已审核通过的</td></tr>     
			<tr class="common"><td class="common">间接调查费：</td><td class="common">该案件录入的间接调查费，已审核通过的</td></tr>
			<tr class="common"><td class="common">医院代码：</td><td class="common">案件录入账单的医院编码，取第一条</td></tr>     
			<tr class="common"><td class="common">医院名称：</td><td class="common">医院编码对应的医院名称</td></tr>     
			<tr class="common"><td class="common">住院天数：</td><td class="common">案件录入的账单住院天数合计</td></tr>     
		  </table>
		</Div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
