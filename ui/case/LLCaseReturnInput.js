//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
	var state = fm.RgtState.value;
	if(state=='11'||state=='12'||state=='14'||state=='07'||state=='13')//新增延迟状态不能回退 by zhangzhonghao
	{
		alert("此案件状态不能回退!");
		return false;
	}
	
		if(state == '16'){
		alert("此案件处于理赔二核中，不可以案件回退");
		return false;
	}
	
	var lltjSql ="select 1 from llhospcase where apptranno like 'TJ%' and hospitcode ='00000000' and caseno ='"+ fm.CaseNo.value +"'";
	var lltjarr = easyExecSql(lltjSql);
	if(lltjarr != null)
	{
		alert("天津居民意外案件不能进行回退！");
		return false;
	} 
	if(fm.BackState.value.trim()=='')
	{
		alert("回退状态不能为空");
		return false;
	}
	if(fm.llusercode.value.trim()=='')
	{
		alert("回退理赔员不能为空");
		return false;
	}
	if(fm.returnReason.value.trim()=='')
	{
		alert("回退原因不能为空");
		return false;
	}
  var strsql = "select usercode ,username from Llclaimuser where usercode='"+fm.Operator.value+"' union select usercode,username from llsocialclaimuser where usercode='"+fm.Operator.value+"' ";
  var arr = new Array();
  arr = easyExecSql(strsql);
  if(arr==null||arr.length<=0)
  {
  	alert("对不起，你的权限不够");
  	return false;
  }
  //#2967 抽检案件只能由抽检上级回退。
//  alert(state);
  if(state=='10'){
	 var strupuser = "select appclmuwer from LLClaimUWMain where caseno='"+fm.CaseNo.value+"'  with ur" 
	 var arr = new Array();
	 arr = easyExecSql(strupuser);
	 var usercode = fm.Operator.value;
//	 alert(arr[0][0])
	 if(arr!=null&&usercode!=arr[0][0]){
		 alert("该案件被抽检，只能由上级审批人"+arr[0][0]+"进行回退！");
		 return false;
	 }
  }
  fm.fmtransact.value = "INSERT||MAIN";
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //fm.action ="./LLCaseReturnSave.jsp?InsuredNo="+fm.InsuredNo.value+"&CaseNo="+fm.CaseNo.value;
  fm.action ="./LLCaseReturnSave.jsp";  
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    top.opener.initTitle();
    //top.opener.afterAffix( ShortCountFlag,SupplyDateResult );
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function displayEasyResult( arrQueryResult )
{
	var i, j, m, n;
	var arrSelected = new Array();
	var arrResult = new Array();

	if( arrQueryResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPersonGrid();
		PersonGrid.recordNo = (currPageIndex - 1) * MAXSCREENLINES;
		PersonGrid.loadMulLine(PersonGrid.arraySave);

		arrGrid = arrQueryResult;
		// 转换选出的数组
		arrSelected = getSelArray();
		arrResult = chooseArray( arrQueryResult, arrSelected );
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PersonGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//PersonGrid.delBlankLine();
	} // end of if
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PersonGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterLLRegister( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PersonGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	


	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = PersonGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
function afterQuery()
{	

	if(fm.LoadFlag.value=="1")
	{
		var strSQL="select Name,Sex,Birthday,BnfGrade,BnfLot,"
					+"CustomerNo,IDType,IDNo,PolNo,BnfType,ContNo,"
					+"RelationToInsured from LCBnf "
					+"where InsuredNo='"+fm.InsuredNo.value+"'";
					//alert(strSQL);
					turnPage.queryModal(strSQL, BnfGrid);
	}
	if(fm.LoadFlag.value=="2")
	{

		var strSQL="select Name,Sex,Birthday,BnfGrade,BnfLot,"
				+"CustomerNo,IDType,IDNo,PolNo,BnfType,ContNo,"
				+"RelationToInsured,accname,GetMoney,BankCode,"
				+"BankAccNo,CaseGetMode from LLBnf "
				+"where CaseNo='"+fm.CaseNo.value+"'";
				var arr=easyExecSql(strSQL);
			//	alert(arr);
				turnPage.queryModal(strSQL, BnfGrid);
			//	alert(strSQL);
	} 	
	//未查询到，填充默认值

	if (BnfGrid.mulLineCount<=0)
	{
		 var givemoney = fm.givemoney.value;
		
		 strSQL ="select InsuredName,InsuredSex,InsuredBirthday,'1','1',InsuredNo,'','',polno,'1',contno,'','','"+ givemoney +"' from "
		  +" LLCasePolicy where InsuredNo='" + fm.InsuredNo.value + "' and caseno='"+ fm.CaseNo.value +"'"; 
		  
		 turnPage.queryModal(strSQL, BnfGrid);
		 
	}
}

////////
function afterQuery1()
{	
	var strSQL="select  distinct CustomerNo,AccName,bankcode,bankaccno,getmode"
					+" from llregister where 1=1 and getmode = '4' and CustomerNo='"+fm.InsuredNo.value+"'";
					//alert(strSQL);
					
					turnPage.queryModal(strSQL, AccountGrid);
}
//////////

function Update()
{
//	var len =BnfGrid.getSelNo() - 1;

//	  if(len<0)
//	  {
//	  	alert("请选择申请原因！")
//	  	return false;
////	  }
  fm.fmtransact.value = "UPDATE||MAIN";
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action ="./LLBnfSave.jsp?InsuredNo="+fm.InsuredNo.value+"&CaseNo="+fm.CaseNo.value;
  fm.submit(); //提交
}
function modify()
{
	if(fm.paymode.value =='')
	{
		alert("必须选择领取方式！");
		return;
	}
	if(fm.paymode.value == 4)
	{
		if(fm.BankCode.value==""){alert("银行编码不能为空");        return ;}
		if(fm.BankAccNo.value==""){alert("银行账户不能为空");       return ;}
 		if(fm.AccName.value==""){alert("银行账户名不能为空");       return ;}
    if(fm.ModifyReason.value==""){alert("修改原因不能为空");    return ;}
  }
  try
  {     
  fm.fmtransact.value = "UPDATE";
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action ="./LLBnfLiveSave.jsp";
  fm.submit(); //提交
   }
  catch(ex)
  {
    alert("错误!");
  }   
}  
  
function giveConfirm()
{ 
  ////
  var arrResult  = new Array();
	var strSQL = "select RgtClass,TogetherFlag from LLRegister "
    +"where rgtNo='"+fm.RgtNo.value+"'"; 
    arrResult = easyExecSql(strSQL);
    if(arrResult!=null)
    {
    if(arrResult[0][0]==1&&arrResult[0][1]==3)
    {
    alert("团单给付方式为统一给付，这里不能做给付确认！");
    return;
    }
    }
  
	if (fm.RgtState.value != "09" )
	{
		alert("案件在当前状态下不能做给付确认！");
		return;		
	}
  ////
	if(fm.paymode.value =='')
	{
		alert("必须选择领取方式！");
		return;
	}
		if(fm.paymode.value == 4)
	{
		if(fm.BankCode.value==""){alert("银行编码不能为空");return;}
	else 
		if(fm.BankAccNo.value==""){alert("银行账户不能为空");return;}
  else
 		if(fm.AccName.value==""){alert("银行账户名不能为空");return;}
	}
	 try
  {     
  fm.fmtransact.value = "INSERT";
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action ="./LLBnfGiveConfirm.jsp";
  fm.submit(); //提交
   }
  catch(ex)
  {
    alert("错误!");
  } 
}  
  
