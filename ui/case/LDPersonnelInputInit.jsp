<%
//程序名称：LDPersonnelInput.jsp
//程序功能：
//创建日期：2005-04-13 23:19:03
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = "";
    fm.all('Name').value = "";
    fm.all('Sex').value = "";
    fm.all('Birthday').value = "";
    fm.all('IDType').value = "";
    fm.all('IDNo').value = "";
    fm.all('Password').value = "";
    fm.all('NativePlace').value = "";
    fm.all('Nationality').value = "";
    fm.all('RgtAddress').value = "";
    fm.all('Marriage').value = "";
    fm.all('MarriageDate').value = "";
    fm.all('Health').value = "";
    fm.all('Stature').value = "";
    fm.all('Avoirdupois').value = "";
    fm.all('Degree').value = "";
    fm.all('CreditGrade').value = "";
    fm.all('OthIDType').value = "";
    fm.all('OthIDNo').value = "";
    fm.all('ICNo').value = "";
    fm.all('GrpNo').value = "";
    fm.all('JoinCompanyDate').value = "";
    fm.all('StartWorkDate').value = "";
    fm.all('Position').value = "";
    fm.all('Salary').value = "";
    fm.all('OccupationType').value = "";
    fm.all('OccupationCode').value = "";
    fm.all('WorkType').value = "";
    fm.all('PluralityType').value = "";
    fm.all('DeathDate').value = "";
    fm.all('SmokeFlag').value = "";
    fm.all('BlacklistFlag').value = "";
    fm.all('Proterty').value = "";
    fm.all('Remark').value = "";
    fm.all('State').value = "";
    fm.all('VIPValue').value = "";
    fm.all('GrpName').value = "";
    fm.all('speciality').value = "";
    fm.all('ManageCom').value = "";
    fm.all('OccupationGrade').value = "";
    fm.all('UserCode').value = "";
    fm.all('Operator').value = "";
    fm.all('MakeDate').value = "";
    fm.all('MakeTime').value = "";
    fm.all('ModifyDate').value = "";
    fm.all('ModifyTime').value = "";
    //3340 新增字段
    fm.all('DocBack').value="";
    fm.all('WorkAge').value="";
    fm.all('certificationType').value="";
    fm.all('stateflag').value="";
  }
  catch(ex)
  {
    alert("在LDPersonnelInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
  }
  catch(ex)
  {
    alert("在LDPersonnelInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initFamilyDisGrid();
  }
  catch(re)
  {
    alert("LDPersonnelInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//直系亲属附件信息
function initFamilyDisGrid()  
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
	    iArray[0][0]="序号";    	           //列名
	    iArray[0][1]="30px";            		//列宽
	    iArray[0][2]=100;            			//列最大值
	    iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
	    
	    iArray[1]=new Array();
	    iArray[1][0]="亲属姓名";    	           //列名
	    iArray[1][1]="30px";            		//列宽
	    iArray[1][2]=100;            			//列最大值
	    iArray[1][3]=1; 						 //是否允许输入,1表示允许，0表示不允许
	    
	    iArray[2]=new Array();
	    iArray[2][0]="亲属身份证号";    	           //列名
	    iArray[2][1]="40px";            		//列宽
	    iArray[2][2]=100;            			//列最大值
	    iArray[2][3]=1; 						 //是否允许输入,1表示允许，0表示不允许
	    
	    iArray[3]=new Array();
	    iArray[3][0]="与员工关系";    	         //列名
	    iArray[3][1]="20px";            		//列宽
	    iArray[3][2]=100;            			//列最大值
	    iArray[3][3]=2; 						 //是否允许输入,1表示允许，0表示不允许 
	    iArray[3][10]="FamType";
	    iArray[3][11]="0|^0|本人|^1|夫妻|^2|父母|^3|子女" 
	    iArray[3][12]="3|4"
	    iArray[3][13]="1|0"
	    

	    iArray[4] = new Array("与员工关系","60px","10","3")
	    
	    iArray[5] = new Array("流水号","60px","10","3")
	    
	    FamilyDisGrid = new MulLineEnter("fm","FamilyDisGrid");
	    
	    FamilyDisGrid.mulLineCount = 0;
	    FamilyDisGrid.displayTitle = 1;
	    FamilyDisGrid.locked = 0;
	    FamilyDisGrid.canChk =0	;
	    FamilyDisGrid.hiddenPlus=0;
	    FamilyDisGrid.hiddenSubtraction=0;
	    FamilyDisGrid.loadMulLine(iArray);
	    
	}catch(ex)
	{
	   alert(ex);	
	}
}


</script>
