<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%> 
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<html>    
<%
//程序名称：LLClaimConfigureInput.jsp
//程序功能：F1报表生成
//创建日期：2005-08-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLClaimCollectionInput.js"></SCRIPT>
		<%@include file="LLClaimConfigureInit.jsp"%>   
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<script>
	function initDate(){
   fm.EndCaseDateS.value="<%=afterdate%>";
   fm.EndCaseDateE.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
  }
 </script>
</head>
 
<body onload="initForm();initDate();">    
  <form action="./LLClaimCollectionRpt.jsp" method=post name=fm target="f1print">
    <br>
    <!-- 
    <table>
    	<TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,Info);">
         </TD>
         <TD class= titleImg>
         条件选择
         </TD>
       </TR>
   	</table>
    <Div id=Info>
    <table class= common border=0 width=100%>
      	<TR  class= common>          
				<TD  class= title>统计机构</TD>
				<TD  class= input>
    			<Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return organname('comcode',[this,ManageComName],[0,1],null,null,null,1);" verify="统计机构|notnull"><Input class=codename  name=ManageComName></TD>
          		<TD  class= title8>保单生效起期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="CvaliDateS" verify="生效起期|date&notnull"></TD>
          		<TD  class= title8>保单生效止期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="CvaliDateE" verify="生效止期|date&notnull"></TD>
        </TR>
        <TR  class= common> 
        		<TD  class= title8>结案起期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="EndCaseDateS" verify="结案起期|date&notnull"></TD>
          		<TD  class= title8>结案止期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="EndCaseDateE" verify="结案止期|date&notnull"></TD>
		  		<TD class= title8>单位名称</TD>
            	<TD class= input8><Input class=common name="GrpName" ></TD>
        </TR>
        <TR  class= common style="display:'none'">   
				<TD  class= title>销售渠道</TD>
				<TD  class= input> <input class=codeno CodeData="0|^02|团单直销^03|中介^07|职团开拓^08|其他 "   name=SaleChnl ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName></TD>
          <TD  class= title>排序方式</TD>
				<TD  class= input> <input class=codeno CodeData="0|^1|保单生效时间 ^4|保费(高到低)^5|保费(低到高)^6|参保人数^7|失效日期 "  name=Sort ondblclick="return showCodeListEx('Sort',[this,SortName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Sort',[this,SortName],[0,1],null,null,null,1);"><input class=codename name=SortName></TD>
        </TR>
        <TR  class= common8>
            	<TD  class= title>参保人数</TD>
    	        <TD  class= input>>=<Input class=common name=Person> </TD> 
	            <TD class= title8>保单号码</TD>
    	        <TD class= input8><Input class=common name="GrpContNo" ></TD>
        	    <TD class= title>重点业务</TD>
		    	<TD class= input><Input class= "codeno"  name=GrpTypeS  ondblclick="return showCodeList('llgrptypes',[this,GrpTypeNameS],[0,1],null,null,null,1);" onkeyup="return organname('llgrptypes',[this,GrpTypeNameS],[0,1],null,null,null,1);" ><Input class=codename  name=GrpTypeNameS></TD>
         </TR>
         <TR  class= common>          
          		<TD  class= title style="display:'none'">赔付率</TD>
				<TD  class= input style="display:'none'" colspan=2>> 
				<Input class=common style="display:'none'" style=" width:50px"  name=ClaimRatioStd >% </TD> 
         </TR>
         <TR  class= common>          	          
        	  <TD  class= title>保费规模</TD>
          	  <TD  class= input>>=<Input class=common  name=Prem > </TD> 
          	  <TD  class= title>险种编码</TD>
              <TD  class= input><Input class=codeno  name="RiskCode" onClick="showCodeList('riskcode',[this,RiskName],[0,1],null,null,null,1,400);" onkeyup="showCodeListKeyEx('riskcode',[this,RiskName],[0,1],null,null,null,1,400);" ><Input class=codename name= RiskName></TD> 
              <TD  class= title></TD>
              <TD  class= title></TD>
         </TR>
    </table>
		<br>
		<INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="submitForm()">
		<Div  id= "divType" style= "display: ''" >
		-->
    <table>
    	<TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBusiness);">
         </TD>
         <TD class= titleImg>
         重点业务配置
         </TD>
       </TR>
   	</table>   
		<Div  id= "divBusiness" style= "display: ''" align = left>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanBusinessGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    	<INPUT VALUE="查  询" class="cssButton" TYPE="button" onclick="GrpTypeSearch()" > 	
      <INPUT VALUE="保  存" class="cssButton" TYPE="button" onclick="GrpTypeSave(1)" > 	
      <INPUT VALUE="修  改" class="cssButton" TYPE="button" onclick="GrpTypeSave(3)">
      <INPUT VALUE="删  除" class="cssButton" TYPE="button" onclick="GrpTypeSave(2)" > 	
    </Div>
    <table>
    	<TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBusinessGrpCont);">
         </TD>
         <TD class= titleImg>
         重点业务保单配置
         </TD>
       </TR>
   	</table>
   	<Div  id= "divBusinessGrpCont" style= "display: ''" align = left>
   	  <table class= common border=0 width=100%>
      	<TR  class= common>          
					<TD  class= title>重点业务</TD><TD  class= input>
    			<Input class= "codeno"  name=GrpType  ondblclick="return showCodeList('llgrptype',[this,GrpTypeName],[0,1],null,null,null,1);" onkeyup="return organname('llgrptype',[this,GrpTypeName],[0,1],null,null,null,1);" ><Input class=codename  name=GrpTypeName></TD>
          <TD class= title8>团体保单号</TD>
		  		<TD class= input8><Input class=common name=ContNo ></TD>
		  		<TD><INPUT VALUE="查  询" class="cssButton" TYPE="button" onclick="GrpContSearch()"></TD>
		  		<TD></TD>
        </TR>
      </Table>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanBusinessGrpContGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    	<INPUT VALUE="保  存" class="cssButton" TYPE="button" onclick="GrpContTypeSave(1)">
    	<INPUT VALUE="修  改" class="cssButton" TYPE="button" onclick="GrpContTypeSave(3)">
		  <INPUT VALUE="删  除" class="cssButton" TYPE="button" onclick="GrpContTypeSave(2)"> 	
    </Div>
<!--  
<table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
		 	<tr class="common"><td class="title">机构编码:保单所属机构编码</td></tr><br>
			<tr class="common"><td class="title">机构名称:保单所属机构名称</td></tr><br>
			<tr class="common"><td class="title">投保人:保单投保人名称</td></tr><br>
			<tr class="common"><td class="title">团体客户号：团体保单单位号</td></tr><br>
			<tr class="common"><td class="title">保单号：团体保单号</td></tr><br>
			<tr class="common"><td class="title">中介机构编码：团体保单的中介机构编码</td></tr><br>
			<tr class="common"><td class="title">中介机构名称：团体保单的中介机构名称</td></tr><br>
			<tr class="common"><td class="title">业务员编码：团体保单的业务员编码</td></tr><br>
			<tr class="common"><td class="title">业务员姓名：团体保单的业务员姓名</td></tr><br>
			<tr class="common"><td class="title">市场类型：团体保单承保的市场类型</td></tr><br>
			<tr class="common"><td class="title">项目属性：团体保单是否为大项目</td></tr><br>
			<tr class="common"><td class="title">险种代码：保单承保险种编码</td></tr><br>
			<tr class="common"><td class="title">险种名称：保单承保险种名称</td></tr><br>
			<tr class="common"><td class="title">承保日期：保单的签单日期</td></tr><br>
			<tr class="common"><td class="title">生效日期：保单的生效日期</td></tr><br>
			<tr class="common"><td class="title">失效日期：保单的失效日期</td></tr><br>
			<tr class="common"><td class="title">首期保费：保单承保险种的每期应缴保费</td></tr><br>
			<tr class="common"><td class="title">缴费频次：保单承保险种的缴费频次</td></tr><br>
			<tr class="common"><td class="title">保费交至日期：保单下该险种的保费交至日期</td></tr><br>
			<tr class="common"><td class="title">承保保费：保单承保险种的总应缴保费</td></tr><br>
			<tr class="common"><td class="title">经过保费：保单承保险种在统计期内的有效保费</td></tr><br>
			<tr class="common"><td class="title">实收保费：保单承保险种截止结案止期财务实收保费</td></tr><br>
			<tr class="common"><td class="title">被保险人数：保单承保险种下实际承保人数</td></tr><br>
			<tr class="common"><td class="title">承保退休人数：保单承保时录入的承保退休人数</td></tr><br>
			<tr class="common"><td class="title">承保在职人数：保单承保时录入的承保在职人数</td></tr><br>
			<tr class="common"><td class="title">赔付件数：保单承保险种下结案日期在统计期内的已结案案件数</td></tr><br>
			<tr class="common"><td class="title">理赔结案人数：保单承保险种下结案日期在统计期内的已结案人数</td></tr><br>
			<tr class="common"><td class="title">理赔退休人数：保单承保险种下结案日期在统计期内的已结案的退休人数（人员状态已当前状态为准）</td></tr><br>
			<tr class="common"><td class="title">理赔在职人数：保单承保险种下结案日期在统计期内的已结案的在职人数（人员状态已当前状态为准）</td></tr><br>
			<tr class="common"><td class="title">结案赔款：保单承保险种下结案日期在统计期内的已结案赔款</td></tr><br>
			<tr class="common"><td class="title">财务实付赔款：保单承保险种下结案日期在统计期内的已给付赔款</td></tr><br>
		</Div>
		-->
		<input type= hidden name=StartDate>
		<input type= hidden name=EndDate>
		<input type= hidden name=MngName>
		<input type= hidden name=fmtransact>
		<input type= hidden name=zdywlx>
		<input type= hidden name=grpcontno>
		<input type= hidden name=zdywcode>
		
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 