<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  LCPolSet mLCPolSet = new LCPolSet();
  CErrors tError = null;
  System.out.println("开始执行保存操作");
  String transact = "INSERT";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String OperateFlag = request.getParameter("OperateFlag");

  String RgtNo = request.getParameter("RgtNo");
  String UWRegister = request.getParameter("UWRegister");
  String[] tChk = request.getParameterValues("InpSecondUWGridChk");
  String[] strNumber=request.getParameterValues("SecondUWGridNo");
  String[] strPolNo=request.getParameterValues("SecondUWGrid1");
  SecondUWUI tSecondUWUI = new SecondUWUI();

  int intLength=strNumber.length;
  for(int i=0;i<intLength;i++)
  {
    if(tChk[i].equals("0")) //未选
      continue;
    LCPolSchema tLCPolSchema = new LCPolSchema();
    tLCPolSchema.setPolNo(strPolNo[i]);
    mLCPolSet.add(tLCPolSchema);
  }

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  try
  {
    VData tVData = new VData();
    tVData.addElement(RgtNo);
    tVData.addElement(UWRegister);
    tVData.addElement(OperateFlag);
    tVData.addElement(tG);
    tVData.addElement(mLCPolSet);
    tSecondUWUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (FlagStr=="")
  {
    tError = tSecondUWUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>