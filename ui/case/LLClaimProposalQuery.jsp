
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	String tDisplay = "";
	String tContNo="";
	try
	{
		tDisplay = request.getParameter("display");
	}
	catch( Exception e )
	{
		tDisplay = "";
	}
%> 

<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   
  String Operator=tG.Operator;
  String Comcode=tG.ManageCom;
   System.out.println("管理机构-----"+tG.ComCode);
%>  

<%
 String tInsuredNo = "";
 String tInsuredName = "";
 
 	try
	{
		 tInsuredNo += request.getParameter("InsuredNo");
		 if(tInsuredNo.equals("null"))
		 {
		 	tInsuredNo="";
		 }

 		 tInsuredName = new String(request.getParameter("InsuredName").getBytes("ISO8859_1"),"GBK");
	}
	catch( Exception e )
	{
		tInsuredNo = "";
		tInsuredName="";
	}

%>

<script language="JavaScript">
function initDisplayButton()
{
	tDisplay = <%=tDisplay%>;
	if (tDisplay=="1"||tDisplay=="2")
	{
		fm.Return.style.display='';
	}
	else if (tDisplay=="0")
	{
		fm.Return.style.display='none';
	}
}
</script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LLClaimProposalQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="LLClaimProposalQueryInit.jsp"%>
  <title>保单查询 </title>
</head>
<body  onload="initForm('<%=tInsuredNo%>','<%=tInsuredName%>');" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common style  = "display:'none'">
          
          <TD  class= title> 集体保单号码</TD>
          <TD  class= input> <Input class= common name=GrpContNo > </TD>
        
        </TR>
        <TR  class= common>
        <TD  class= title>保单号码 </TD>
          <TD  class= input> <Input class= common name=ContNo onkeyup=" if(event.keyCode ==13) easyQueryClick();"> </TD>
          <TD  class= title>被保人客户号 </TD>
          <TD  class= input> <Input class= common name=InsuredNo onkeyup="if(event.keyCode==13) easyQueryClick();"> </TD>
          <TD  class= title> 被保人姓名</TD>
          <TD  class= input> <Input class= common name=InsuredName onkeyup="if(event.keyCode==13) easyQueryClick();"> </TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 身份证号码 </TD>
          <TD  class= input> <Input class= common name=IDNo onkeyup="if(event.keyCode==13) easyQueryClick();"> </TD> 
          <TD  class= title> 业务员代码 </TD>
          <TD  class= input> <Input class= common name=AgentCode onkeyup="if(event.keyCode==13) easyQueryClick();"> </TD>
          <TD  class= title> 业务员姓名</TD>
          <TD  class= input> <Input class= common name=AgentName onkeyup="if(event.keyCode==13) easyQueryClick();"></TD> 
        </TR>
        <!-- TR  class= common style  = "display:'none'">
          <TD  class= title>  </TD>
          <TD  class= input> </TD>
          
          <TD  class= title> 代理人编码</TD>
          <TD  class= input> <Input class="code" name=AgentCode verify="代理人编码|notnull&code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2]);" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2]);"> </TD>
          <TD  class= title> 代理人组别 </TD>
          <TD  class= input> <Input class=common name=AgentGroup verify="代理人组别|notnull&len<=12" > </TD>
          <TD  class= title> 管理机构 </TD>
          <TD  class= input> <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);"> </TD>
        </TR>
        
        <TR  class= common style  = "display:'none'">
          <TD  class= title> </TD>
          <TD  class= input> <Input class= common name=ProposalNo > </TD>
          <TD  class= title> 投保人姓名 </TD>
          <TD  class= input> <Input class= common name=AppntName > </TD>
        </TR> -->       
    </table>
          <INPUT VALUE="查  询" class = CssButton TYPE=button onclick="easyQueryClick();">				          
          <!-- INPUT VALUE="返  回" name=Return class = CssButton TYPE=button STYLE="display:none" onclick="returnParent();"> --> 					
	<!-- 客户信息 --------------------------------Begin------------>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 客户信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>

      <INPUT VALUE="首  页" class = CssButton TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT VALUE="上一页" class = CssButton TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT VALUE="下一页" class = CssButton TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT VALUE="尾  页" class = CssButton TYPE=button onclick="turnPage1.lastPage();">				
  	</div>
  	<!-- 客户信息 --------------------------------End------------>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保障信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>

      <INPUT VALUE="首  页" class = CssButton TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT VALUE="上一页" class = CssButton TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT VALUE="下一页" class = CssButton TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT VALUE="尾  页" class = CssButton TYPE=button onclick="turnPage2.lastPage();">				
  	</div>
  	
	 <!-- 理赔信息 --------------------------------Begin------------>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 理赔记录
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCaseGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>

      <INPUT VALUE="首  页" class = CssButton TYPE=button onclick="turnPage3.firstPage();"> 
      <INPUT VALUE="上一页" class = CssButton TYPE=button onclick="turnPage3.previousPage();"> 					
      <INPUT VALUE="下一页" class = CssButton TYPE=button onclick="turnPage3.nextPage();"> 
      <INPUT VALUE="尾  页" class = CssButton TYPE=button onclick="turnPage3.lastPage();">				
  	</div>
  	<!-- 理赔信息 --------------------------------End------------>
	<hr>
  	 <INPUT VALUE="保单明细" class = CssButton  TYPE=button onclick="getQueryContDetail();" >
  	 <INPUT VALUE="投保书扫描件" class = CssButton  TYPE=button onclick="ScanQuery();" >
  	 <INPUT VALUE="理赔调查记录" class = CssButton  TYPE=button onclick="getQuerySurvey();" >  	 
  	 <INPUT VALUE="事件记录" class = CssButton  TYPE=button onclick="getQueryAccident();" >  	 
  	 <INPUT VALUE="出险信息记录" class = CssButton  TYPE=button onclick="getQueryDetail();" >  	 
  	 <INPUT VALUE="黑名单查询" class = CssButton TYPE=button onclick="getQueryBlack();">
  	 <INPUT VALUE="合同处理查询" class = CssButton TYPE=button onclick="getQueryContDeal();">  
  	 <p>	 
  	 <INPUT VALUE="万能账户查询" class = CssButton TYPE=button onclick="getWNQuery();">  
  	 <INPUT VALUE="问题件查询" class = CssButton TYPE=button onclick="QuestBack();"> 
  	 <INPUT VALUE="体检件查询" class = CssButton TYPE=button onclick="showHealthQ();"> 
  	 
  	 <!-- INPUT VALUE="基本信息查询" class = CssButton TYPE=button onclick="FunderInfo();">
  	 <INPUT VALUE="投保信息查询" class = CssButton TYPE=button onclick="PersonBiz();">
  	 <INPUT VALUE="保单保障项目查询" class = CssButton TYPE=hidden onclick="PolPolicy();">
  	 <INPUT VALUE="健康管理查询" class = CssButton TYPE=button onclick="HMQuery();">
  	 <INPUT VALUE="Call Center信息查询" class = CssButton TYPE=button onclick=""> -->
     <div>
     <p></p>
      <TR  class= common>
	<TD  class= title8>处理</TD>
	<TD  class= input8> <input class=codeno CodeData="0|8^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定^7|撤件^8|材料退回登记 "  name=DealWith ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"><input class=codename name=DealWithName></TD>
	 </TR>
	 </div>
  	 <Input type="hidden" class= common name="ComCode"  value="<%=Comcode%>">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
