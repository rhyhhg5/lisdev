<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
  <%
  //程序名称：
  //程序功能：
  //创建日期：2005-04-13 23:19:03
  //创建人  ：CrtHtml程序创建
  //更新记录：  更新人    更新日期     更新原因/内容
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LDPersonnelInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LDPersonnelInputInit.jsp"%>
  </head>
  <body  onload="initForm();initElementtype();" >
    <form action="./LDPersonnelSave.jsp" method=post name=fm target="fraSubmit">
      <%@include file="../common/jsp/OperateButton.jsp"%>
      <%@include file="../common/jsp/InputButton.jsp"%>
      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
          </td>
          <td class= titleImg>
            业务人员信息表基本信息
          </td>
        </tr>
      </table>
      <Div  id= "divLDPersonnel1" style= "display: ''">
        <table  class= common align='center' >
          <TR  class= common>
            <TD  class= title>所属机构</TD>
            <TD  class= input>
							<Input class="codeno" name=ManageCom verify="机构代码|notnull&code:ComCode" onclick="return showCodeList('ComCode',[this, ManageComName], [0, 1]);" onkeyup="return showCodeListKey('ComCode', [this, ManageComName], [0, 1]);"><Input class=codename name= ManageComName elementtype=nacessary>
            </TD>

			    <TD  class= title>
			      用户编码
			    </TD>
			    <TD  class= input>
						<Input class= "codeno"  name=UserCode verify="用户编码|notnull&code:UserCode" ondblclick="return showCodeList('UserCode',[this, UserName], [0, 1]);" onkeyup="return showCodeListKey('UserCode', [this, UserName], [0, 1]);" ><Input class= 'codename' name=UserName elementtype=nacessary>
			    </TD>            
            <TD  class= title>员工姓名</TD>
            <TD  class= input><Input class= 'common' name=Name elementtype=nacessary verify="员工姓名|notnull&len<=24"></TD>
          </TR>
          <TR  class= common>
            <TD  class= title>员工号码</TD>
            <TD  class= input><Input class= 'common' name=CustomerNo verify="员工号码|notnull&len<=24" elementtype=nacessary></TD>
            <TD  class= title>员工性别</TD>
            <TD  class= input><Input class= 'codeno' name=Sex CodeData="0|2^0|男^1|女" onclick="return showCodeListEx('Sex',[this,SexName],[0,1]);" onkeyup="return showCodeListKeyEx('Sex',[this,SexName],[0,1]);" verify="员工性别|code:Sex&&notnull"><input class=codename  name=SexName elementtype=nacessary>
            </TD>
            <TD  class= title>出生日期</TD>
            <TD  class= input>
              <Input class= coolDatePicker dateFromat="Short" name=Birthday verify="员工出生日期|date&notnull" elementtype=nacessary>
            </TD>
          </TR>
          <TR  class= common>
            <TD  class= title>证件类型</TD>
            <TD  class= input8><Input  onClick="showCodeList('idtype',[this,IDTypeName],[0,1]);" onkeyup="showCodeListKeyEx('idtype',[this,IDTypeName],[0,1]);" class=codeno name="IDType" verify="证件类型|code:idtype"><Input class= codename name=IDTypeName ></TD>
            <TD  class= title>证件号码</TD>
            <TD  class= input><Input class= 'common' name=IDNo onblur="checkidtype();getBirthdaySexByIDNo(this.value);" ></TD>
            <TD  class= title>学历专业</TD>
            <TD  class= input><Input class= 'codeno' name=speciality onClick="showCodeList('hmedulevelcode',[this,specialityName],[0,1],null,null,null,1,170);" onkeyup="showCodeListKeyEx('hmedulevelcode',[this,specialityName],[0,1],null,null,null,1,170);" verify="学历专业|code:hmedulevelcode"><Input class= codename name=specialityName></TD>
          </TR>
          <TR  class= common>
            <TD  class= title>业务职级</TD>
            <TD  class= input>
							<Input class="codeno" name=OccupationGrade onClick="showCodeList('llclaimpopedom',[this,OccupationGradeName],[0,1]);"  onkeyup="showCodeListKey('llclaimpopedom',[this,OccupationGradeName],[0,1]);" readonly verify="业务职级|code:llclaimpopedom"><Input class="codename" name=OccupationGradeName readonly>
            </TD>
            <TD  class= title8>电话号码</TD>
            <TD  class= input8><Input class= common name=Phone verify="电话号码|num&&len<=30"></TD>
            <TD  class= title8>手机号码</TD>
            <TD  class= input8><Input class= common name=Mobile verify="手机号码|num&&len>=8"></TD>
          </TR>
          <TR  class= common>
            <TD  class= title8>电子邮箱</TD>
            <TD  class= input8><Input class= common name=Email verify="电子邮箱|Email"></TD>
			<!-- 3340 -->
          	<TD class=title8>是否有医学背景 </TD>
          	<TD class=input8><Input class='codeno' name=DocBack CodeData="0|2^0|是^1|否" onclick="return showCodeListEx('DocBack',[this,DocBackName],[0,1]);" onkeyup="return showCodeListKeyEx('DocBack',[this,DocBackName],[0,1]);"verify="是否有医学背景|DocBack"><input class=codename name=DocBackName></TD>
          	<TD class=title8>从事理赔相关工作年限</TD>
          	<TD class=input8><Input class= common name=WorkAge verify="从事理赔相关工作年限|WorkAge"></TD>
          </TR>
 		  <TR class =common>
          	<TD class=title8>理赔人员评聘</TD>
          	<TD class=input8>
          		<Input class='codeno' name=certificationType verify="理赔员评聘|code:certificationType&INT" onclick="return showCodeList('certificationType',[this,certificationName],[0,1]);"onkeyup="showCodeListEx('certificationType',[this,certificationName],[0,1]);"><Input class=codename name=certificationName>	
            </TD>
            <TD class =title8>用户有效状态</TD>
            <TD><Input readonly class=common name=stateflag></TD>
          </TR>
        </table>
      </Div>
	<br>
	  <table>
          <TR class=common>
            <TD class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFamilyDis);">
            </TD>
            <td class= titleImg>
                             直系亲属配置
            </TD>
          </TR>
       </table>
	   <Div  id= "divFamilyDis" align=center style= "display: ''">
          <table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanFamilyDisGrid" >
                </span>
              </TD>
            </TR>
          </table>
       </Div>	
	
      <div id="div1" style="display: 'none'">
        <table >
          <TR  class= common>
            <TD  class= title>备注</TD>
            <TD  class= input><Input class= 'common' name=Remark ></TD>
            <TD  class= title>状态</TD>
            <TD  class= input><Input class= 'common' name=State ></TD>
            <TD  class= title>VIP值</TD>
            <TD  class= input><Input class= 'common' name=VIPValue ></TD>
            <TD  class= title>单位名称</TD>
            <TD  class= input><Input class= 'common' name=GrpName ></TD>
            <TD  class= title>入司日期</TD>
            <TD  class= input><Input class= coolDatePicker dateFromat="Short" name=JoinCompanyDate ></TD>
            <TD  class= title>参加工作日期</TD>
            <TD  class= input><Input class= coolDatePicker dateFromat="Short" name=StartWorkDate ></TD>
            <TD  class= title>职位</TD>
            <TD  class= input><Input class= 'common' name=Position ></TD>
            <TD  class= title>工资</TD>
            <TD  class= input><Input class= 'common' name=Salary ></TD>
            <TD  class= title>职业类别</TD>
            <TD  class= input><Input class= 'common' name=OccupationType ></TD>
            <TD  class= title>职业代码</TD>
            <TD  class= input><Input class= 'common' name=OccupationCode ></TD>
            <TD  class= title>密码</TD>
            <TD  class= input><Input class= 'common' name=Password ></TD>
            <TD  class= title>国籍</TD>
            <TD  class= input><Input class= 'common' name=NativePlace ></TD>
            <TD  class= title>民族</TD>
            <TD  class= input><Input class= 'common' name=Nationality ></TD>
            <TD  class= title>户口所在地</TD>
            <TD  class= input><Input class= 'common' name=RgtAddress ></TD>
            <TD  class= title>婚姻状况</TD>
            <TD  class= input><Input class= 'common' name=Marriage ></TD>
            <TD  class= title>结婚日期</TD>
            <TD  class= input><Input class=coolDatePicker dateFromat="Short" name=MarriageDate ></TD>
            <TD  class= title>健康状况</TD>
            <TD  class= input><Input class= 'common' name=Health ></TD>
            <TD  class= title>身高</TD>
            <TD  class= input><Input class= 'common' name=Stature ></TD>
            <TD  class= title>体重</TD>
            <TD  class= input><Input class= 'common' name=Avoirdupois ></TD>
            <TD  class= title>学历</TD>
            <TD  class= input><Input class= 'common' name=Degree ></TD>
            <TD  class= title>信用等级</TD>
            <TD  class= input><Input class= 'common' name=CreditGrade ></TD>
            <TD  class= title>其它证件类型</TD>
            <TD  class= input><Input class= 'common' name=OthIDType ></TD>
            <TD  class= title>其它证件号码</TD>
            <TD  class= input><Input class= 'common' name=OthIDNo ></TD>
            <TD  class= title>ic卡号</TD>
            <TD  class= input><Input class= 'common' name=ICNo ></TD>
            <TD  class= title>单位编码</TD>
            <TD  class= input><Input class= 'common' name=GrpNo ></TD>
            <TD  class= title>职业（工种）</TD>
            <TD  class= input><Input class= 'common' name=WorkType ></TD>
            <TD  class= title>兼职（工种）</TD>
            <TD  class= input><Input class= 'common' name=PluralityType ></TD>
            <TD  class= title>死亡日期</TD>
            <TD  class= input><Input class= coolDatePicker dateFromat="Short" name=DeathDate ></TD>
            <TD  class= title>是否吸烟标志</TD>
            <TD  class= input><Input class= 'common' name=SmokeFlag ></TD>
            <TD  class= title>黑名单标记</TD>
            <TD  class= input><Input class= 'common' name=BlacklistFlag ></TD>
            <TD  class= title>属性</TD>
            <TD  class= input><Input class= 'common' name=Proterty ></TD>
            <TD  class= title>操作员代码</TD>
            <TD  class= input><Input class= 'common' name=Operator ></TD>
            <TD  class= title>入机日期</TD>
            <TD  class= input><Input class= 'common' name=MakeDate ></TD>
            <TD  class= title>入机时间</TD>
            <TD  class= input><Input class= 'common' name=MakeTime ></TD>
            <TD  class= title>最后一次修改日期</TD>
            <TD  class= input><Input class= 'common' name=ModifyDate ></TD>
            <TD  class= title>最后一次修改时间</TD>
            <TD  class= input><Input class= 'common' name=ModifyTime ></TD>
          </TR>
        </TABLE>
      </DIV>
      <input type=hidden id="fmtransact" name="fmtransact">
      <input type=hidden id="fmAction" name="fmAction">
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>
