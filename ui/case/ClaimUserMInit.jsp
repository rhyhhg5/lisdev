<%
//Name：RegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">

function initInpBox( )
{
  try 
  {

    
  } catch(ex) {
    alert("在RegisterInputInit.jsp-->initInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    fm.saveButton.disabled=true;
    fm.deleteButton.disabled=true;
    initCaseDisGrid();
  }
  catch(re)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCaseDisGrid()
{
   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("机构代码","30px","0");
    iArray[1][3]=2;
    iArray[1][4]= "comcode";
    iArray[1][5]= "1|2";
    iArray[1][6]= "0|1";
    iArray[2]=new Array("机构名称","30px","0",0);
    iArray[3]=new Array();
    iArray[3][0]="个/团险";  
    iArray[3][1]="10px";
    iArray[3][2]=100;   
    iArray[3][3]=2;
    iArray[3][10]="RiskType";
    iArray[3][11]="0|^1|个险|^2|团险"; 

    CaseDisGrid = new MulLineEnter( "fm" , "CaseDisGrid" );
    
    //CaseDisGrid.mulLineCount = 5;
    //CaseDisGrid.hiddenPlus = 0;
    //CaseDisGrid.hiddenSubtraction=0;
    CaseDisGrid.displayTitle = 1;
    CaseDisGrid.canChk =0; 
    CaseDisGrid.canSel =0;
    CaseDisGrid.locked = 0;
    CaseDisGrid.loadMulLine(iArray);
   } 
  catch(ex)
  {
    alert(ex);
  }    
}

//立案附件信息
</script>