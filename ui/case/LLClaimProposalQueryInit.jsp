<%
//程序名称：LLClaimProposalQueryInit.jsp
//程序功能：
//创建日期：2010-08-17 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//返回按钮初始化
var str = "";
function initQuery()
{
    try
    {
        var tContNo = top.opener.fm.all('ContNo').value;
	    if (tContNo!=""&&tContNo!=null)
	    {
	    	fm.all('ContNo').value = tContNo; 
	    	easyQueryClick();
	    }
	 }
	 catch(ex)
	 {
	 }
}
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                  
	// 保单查询条件
    fm.all('ContNo').value = '';
    fm.all('InsuredNo').value = '';
    fm.all('InsuredName').value = '';
    fm.all('IDNo').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
    fm.all('GrpContNo').value = '';
    
  }
  catch(ex)
  {
    alert("在LLClaimProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {   
  }
  catch(ex)
  {
    alert("在LLClaimProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm(tInsuredNo,tInsuredName)
{
  try
  { 
    initInpBox();
    initSelBox();   
    initInsuredGrid(); 
	initPolGrid();
	initCaseGrid();
	
	fm.all('InsuredNo').value = tInsuredNo;
    fm.all('InsuredName').value = tInsuredName;
    var tsql="select name from ldperson where CustomerNo='"+fm.InsuredNo.value+"'";
    var xrr = easyExecSql(tsql);
    if(xrr){
    	fm.InsuredName.value = xrr[0][0];
    }
    easyQueryClick();  
	//initQuery();
	
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//客户信息列表的初始化
function initInsuredGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="客户号";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[2]=new Array();
      iArray[2][0]="客户姓名";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[3]=new Array();
      iArray[3][0]="性别";         		//列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
      iArray[4]=new Array();
      iArray[4][0]="身份证号";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="出生日期";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
      InsuredGrid = new MulLineEnter( "fm" , "InsuredGrid" ); 
      //这些属性必须在loadMulLine前
      InsuredGrid.mulLineCount = 1;   
      InsuredGrid.displayTitle = 1;
      InsuredGrid.locked = 1;
      InsuredGrid.canSel = 1;
      InsuredGrid.hiddenPlus = 1;
      InsuredGrid.hiddenSubtraction = 1;
      InsuredGrid.selBoxEventFuncName = "queryCustomer"; 
      InsuredGrid.loadMulLine(iArray);       
      }
      catch(ex)
      {
        alert(ex);
      }
}

//保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="被保人客户号";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[2]=new Array();
      iArray[2][0]="被保人姓名";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="保单号";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
           
      iArray[4]=new Array();
      iArray[4][0]="保单生效日";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="保单终止日";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="保单状态";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="保单所属机构";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 
      
      iArray[8]=new Array();
      iArray[8][0]="业务员姓名";         		//列名
      iArray[8][1]="70px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 
      
      iArray[9]=new Array();
      iArray[9][0]="累计实赔金额";         		//列名
      iArray[9][1]="80px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0; 
      
      iArray[10]=new Array();
      iArray[10][0]="累计住院天数";         		//列名
      iArray[10][1]="80px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0; 
      
      iArray[11]=new Array();
      iArray[11][0]="保单类型";         		//列名
      iArray[11][1]="80px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=3; 
      
      iArray[12]=new Array();
      iArray[12][0]="印刷号";         		//列名
      iArray[12][1]="80px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=3; 
      
      iArray[13]=new Array();
      iArray[13][0]="表类型";         		//列名
      iArray[13][1]="80px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=3; 
      
      iArray[14]=new Array();
      iArray[14][0]="管理机构";         		//列名
      iArray[14][1]="80px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=3; 
      
      iArray[15]=new Array();
      iArray[15][0]="总单投保单号码";         		//列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=3; 
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 1;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray); 
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

//理赔信息列表的初始化
function initCaseGrid()
  {                               
    var iArray = new Array();
      
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="理赔号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[2]=new Array();
      iArray[2][0]="处理人代码";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="处理人姓名";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
           
      iArray[4]=new Array();
      iArray[4][0]="受理日期";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="结案日期";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="案件状态";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="实赔金额";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 
      
      CaseGrid = new MulLineEnter( "fm" , "CaseGrid" ); 
      //这些属性必须在loadMulLine前
      CaseGrid.mulLineCount = 1;   
      CaseGrid.displayTitle = 1;
      CaseGrid.locked = 1;
      CaseGrid.canSel = 1;
      CaseGrid.hiddenPlus = 1;
      CaseGrid.hiddenSubtraction = 1;
      CaseGrid.loadMulLine(iArray); 
      
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>