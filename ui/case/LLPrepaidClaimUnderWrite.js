//该文件中包含客户端需要处理的函数和事件

//程序名称：LLPrepaidClaimUnderWrite.js
//程序功能：预付赔款审批
//创建日期：2010-11-30
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();


//查询预付赔款
function easyQuery()
{
	initPreClaimGrid();
	initPreClaimDetailGrid();
	if(fm.ManageCom.value==""||fm.ManageCom.value==null
	  ||fm.RgtDateS.value==""||fm.RgtDateS.value==null
	  ||fm.RgtDateE.value==""||fm.RgtDateE.value==null)
	  {
	  	alert("管理机构、申请起期、申请止期不能为空!");
	  	return false;
	  }
	var preSql="select ManageCom,PrepaidNo,GrpContNo,GrpName,RgtDate,case RgtType when '1' then '申请类' else '回收类' end,SumMoney,"
		+" codename('llprepaidstate',RgtState),Handler ,RgtState"
		+" from LLPrepaidClaim "
		+" where 1=1 and ManageCom like '"+fm.ManageCom.value+"%'"
		+" and RgtDate>='"+fm.RgtDateS.value+"' and RgtDate<='"+fm.RgtDateE.value+"'"
		+ getWherePart("GrpName","GrpName")
		+ getWherePart("GrpContNo","GrpContNo")
		+ getWherePart("PrepaidNo","PrepaidNo")
		+ getWherePart("RgtState","RgtState")
		+ getWherePart("Handler","Handler")
		+ getWherePart("RgtType","RgtType")
		preSql +=" order by GrpContNo,PrepaidNo";
		
	turnPage.queryModal(preSql,PreClaimGrid);
	
}
//查询预付赔款明细
function queryPreDetail()
{
	var selNo = PreClaimGrid.getSelNo();
	var tPreNo= PreClaimGrid.getRowColDataByName(selNo-1,"PrepaidNo");	

	var preDetailSql="select llde.RiskCode,(select riskname from lmrisk where riskcode=llde.riskcode), "
		+" ( SELECT (decimal(NVL((SELECT SUM(sumactupaymoney) FROM ljapaygrp WHERE endorsementno IS NULL "
        + " AND grpcontno=a.grpcontno AND grppolno=a.grppolno and paytype<>'YF'),0) + "
        + " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno AND grppolno=a.grppolno),0),12,2))"       
        + " FROM lcgrppol a, lmrisk b, llprepaidgrppol c"
        + " WHERE a.grppolno=c.grppolno AND a.riskcode=b.riskcode "
        + " AND a.GrpPolNo=llde.GrpPolNo),"
		+" decimal(nvl(sum(llpreg.ApplyAmount+llpreg.RegainAmount),0),12,2), "
		+ " (SELECT NVL(SUM(n.realpay),0) FROM llcase m,llclaimdetail n WHERE m.caseno=n.caseno "
        + " AND m.rgtstate in ('09','11','12') AND n.grpcontno=llde.grpcontno AND n.grppolno=llde.grppolno), "
		+ " nvl(llde.Money,0),llde.grppolno"
		+" from LLPrepaidClaimDetail llde,LLPrepaidGrpPol llpreg"
		+" where llde.GrpPolNo=llpreg.GrpPolNo "
		+" and llde.PrepaidNo='"+ tPreNo+"' group by llde.RiskCode,llde.GrpContNo,llde.GrpPolNo,llpreg.SumPay,llde.Money order by llde.RiskCode";
		
	turnPage.queryModal(preDetailSql,PreClaimDetailGrid);	
}
//查看扫描件
function queryScan()
{
    if( PreClaimGrid.getSelNo()==0 )
    {
	  alert("请选择一条预付赔款信息！");
      return false;
    }
  	var selNo = PreClaimGrid.getSelNo();
	var tPreNo= PreClaimGrid.getRowColDataByName(selNo-1,"PrepaidNo");	
	var docId = "";
	var sql = " select docid from es_doc_main where Busstype='LP' and subtype='LP03' and DocCode='" 
							+ tPreNo + "' ";
	var arr = easyExecSql(sql);
	if(!arr) 
	{
		window.alert("数据库中无该扫描件!");
		return false;
	} 
	else 
	{
		docId = arr[0][0];
	}

	var url="./FrameMainPrepaid.jsp?DocID="+docId+"&DocCode=" + tPreNo ;
	window.open(url);
}

//审批通过
function ClaimSPSD()
{
	if( PreClaimGrid.getSelNo()==0 )
	{
		alert("请选择一条预付赔款信息！");
	    return false;
	}
	var selNo = PreClaimGrid.getSelNo();
	var tRgtState= PreClaimGrid.getRowColDataByName(selNo-1,"RgtState");	
	fm.tPrepaidNo.value = PreClaimGrid.getRowColDataByName(selNo-1,"PrepaidNo");	

	if(tRgtState!="01"&&tRgtState!="02"&&tRgtState!="03")
	{
		alert("预付案件只有在申请扫描审批状态才能进行审批操作！");
		return false;
	}
	else{
		var DealerSql="select Dealer from LLPrepaidClaim where PrepaidNo='"+ fm.tPrepaidNo.value+"' ";
		var arrResult = easyExecSql( DealerSql );	 
		if(arrResult && arrResult[0][0]!=fm.Operator.value)
		{
			alert("预付案件的审批人为"+arrResult[0][0]);
			return false;
		}		
	}
	
	var ClaimCount=PreClaimDetailGrid.mulLineCount;
	  var chkFlag = false;
	  for (var i=0; i<ClaimCount; i++){
		  var money = PreClaimDetailGrid.getRowColDataByName(i,"money");
	      var pay = PreClaimDetailGrid.getRowColDataByName(i,"pay");
	      var grppolno = PreClaimDetailGrid.getRowColDataByName(i,"grppolno");
	      var riskcode = PreClaimDetailGrid.getRowColDataByName(i,"riskcode");

	      var checkSQL = " select " + pay + "-(" + money
						+ ") - (a.PrepaidBala) -" 
						+ " nvl((select sum(pay) from ljagetclaim where grppolno=a.grppolno and othernotype='5'),0) " 
						+ " from LLPrepaidGrpPol a where a.grppolno='"
						+ grppolno + "'";
		  var balaArr = easyExecSql(checkSQL);
		  
		  if (balaArr) {
			  if (balaArr[0][0]<0) {
				  if(!confirm("险种"+riskcode+"本次预付金额超过实收保费扣除 该险种理赔结案通知赔款与未回销预付赔款 之和，是否继续？")){
				      return false;
				  }		  
			  }		  
		  } else {
			  alert("险种"+riskcode+"预付信息查询失败，请与开发人员联系");
			  return false;
		  }
	  }
	
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all('fmtransact').value="SPSDUW";
	fm.action = "./LLPrepaidClaimUnderWriteSave.jsp";
	fm.target="fraSubmit";
	fm.submit();
}

//审批回退
function ClaimBack()
{
	if( PreClaimGrid.getSelNo()==0 )
	{
		alert("请选择一条预付赔款信息！");
	    return false;
	}
	var selNo = PreClaimGrid.getSelNo();
	var tRgtState= PreClaimGrid.getRowColDataByName(selNo-1,"RgtState");	
	fm.tPrepaidNo.value = PreClaimGrid.getRowColDataByName(selNo-1,"PrepaidNo");
	if(tRgtState!="03"&&tRgtState!="04")
	{
		alert("预付案件只有在审批审定状态才能进行审批回退！");
		return false;
	}
	
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all('fmtransact').value="BACK";
	fm.action = "./LLPrepaidClaimUnderWriteSave.jsp";
	fm.target="fraSubmit";
	fm.submit();
}


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
    easyQuery();  
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}