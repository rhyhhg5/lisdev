//               该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var contno;
var prtno;
var Insuredno;
var IdNo;
var manageCom;


//提交，保存按钮对应操作
function submitForm()
{
	// 判定是否有选择修改数据
	if(ContGrid.getSelNo()==0)
	{
		alert("请先选择一条记录，再修改邮箱！");
		return false;
	}
	var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.Opt.value=ContGrid.getSelNo();
	fm.action="./UpdateEmailSave.jsp";
	fm.submit(); // 提交
}

// 查询按钮
function easyQueryClick()
{
	
	var IdNo = fm.IDNo.value;
    if(fm.IDNo.value == "")
    {
        alert("身份证号码为空！");
        return false;
    }
    
	var strSQL = "select distinct contno,prtno,Insuredno,idno " +
			"from lcInsured " +
			"where idNo = '" + IdNo + "' and (prtno like 'YBK%' or prtno like 'YWX%')";
	var strQueryResult = easyExecSql(strSQL);
	if(!strQueryResult){
		alert("未查询到数据！");
		return false;
	}
	turnPage1.queryModal(strSQL, ContGrid);
}

function sendEmail(){
	if(ContGrid.getSelNo()==0)
	{
		alert("请先选择一条记录，再发送保单！");
		return false;
	}
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.Opt.value=ContGrid.getSelNo();
	fm.action="./UpdateEmailSendSave.jsp";
	fm.submit();
}

function sendEmailByDate(){
	fm.action="./EmailSendByDateSave.jsp";
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	// disable发送按钮，防止用户重复提交
	//fm.all("sendButton1").disabled=true;
	fm.submit();
}

function afterSubmit(FlagStr,Content,customerno,addressno){
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail" ) {
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		var emailQuery = "select email from lcaddress where customerno='"+customerno+"' and addressno='"+addressno+"' ";
		var result = easyExecSql(emailQuery);
		if(result){
			document.getElementById("newemail").value=result;
		}
	}
}
function afterSubmitNew(FlagStr,Content){
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail" ) {
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}