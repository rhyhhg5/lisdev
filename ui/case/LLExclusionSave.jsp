<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：InsuredUWInfoChk.jsp
//程序功能：免责
//创建日期：2005-08-19 11:10:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";
String Flag = request.getParameter("flag");

GlobalInput tG = new GlobalInput();

tG=(GlobalInput)session.getValue("GI");

if(tG == null) {
    out.println("session has expired");
    return;
  }
System.out.println("Flag : "+Flag);
if(Flag.equals("StopInsured")) {
    LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();

    tLCInsuredSchema.setContNo(request.getParameter("ContNo"));
    tLCInsuredSchema.setInsuredNo(request.getParameter("InsuredNo"));
    tLCInsuredSchema.setInsuredStat("1");    //1-暂停 0-未暂停

    VData tVData = new VData();
    FlagStr="";

    tVData.add(tG);
    tVData.add(tLCInsuredSchema);

    StopInsuredUI tStopInsuredUI = new StopInsuredUI();

    try {
        System.out.println("this will save the data!!!");
        tStopInsuredUI.submitData(tVData,"");
      } catch(Exception ex) {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
      }

    if (!FlagStr.equals("Fail")) {
        tError = tStopInsuredUI.mErrors;
        if (!tError.needDealError()) {
            Content = " 保存成功! ";
            FlagStr = "Succ";
          } else {
            Content = " 保存失败，原因是:" + tError.getFirstError();
            FlagStr = "Fail";
          }
      }
  } 
  else if(Flag.equals("risk")) {
    LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();

    String tPolNo=request.getParameter("PolNo");
    String tUWIdea=request.getParameter("UWIdea");

  //String tAddFeeValue = request.getParameter("AddFeeValue");
  //String tAddFeeRate = request.getParameter("AddFeeRate");
  //String tAddFeeStartDate = request.getParameter("AddFeeStartDate");
  //String tAddFeeEndDate = request.getParameter("AddFeeEndDate");

    String tPassFlag=request.getParameter("uwstate");
    String tSugUWFlag = request.getParameter("SugUWFlag");
    String tLoadFlag = request.getParameter("LoadFlag");
    
    System.out.println("===============------"+tLoadFlag);
    tLCUWMasterSchema.setPolNo(tPolNo);
    tLCUWMasterSchema.setProposalNo(tPolNo);
    tLCUWMasterSchema.setPassFlag(tPassFlag);
    tLCUWMasterSchema.setUWIdea(tUWIdea);
    tLCUWMasterSchema.setSugPassFlag(tSugUWFlag);

    TransferData mTransferData = new TransferData();
    //如果是加费的话，要区别健康加费与职业加费
    //核保结论取消加费，将加费作为一个标志进行处理，
    //核保结论统一为正常通过，变更承保，拒保
    
    //免责信息
    LCSpecSet tLCSpecSet = new LCSpecSet();
    String tSpecCode[] = request.getParameterValues("SpecGrid1");
		String tSpecInfo[] = request.getParameterValues("SpecGrid2");            //告知版别
		String tSpecStartDate[] = request.getParameterValues("SpecGrid3");           //告知编码
		String tSpecEndDate[] = request.getParameterValues("SpecGrid4");           //告知编码
		String tSerialNo[] = request.getParameterValues("SpecGrid5");
		String tSpecNo[] = request.getParameterValues("SpecGridNo");
    //封装加费信息
    LCPremSet tLCPremSet = new LCPremSet();
    String tAddFeeValue[] = request.getParameterValues("AddFeeGrid1");
		String tAddFeeRate[] = request.getParameterValues("AddFeeGrid2");            //告知版别
		String tPayStartDate[] = request.getParameterValues("AddFeeGrid3");           //告知编码
		String tPayEndDate[] = request.getParameterValues("AddFeeGrid4");           //告知编码
		String tNo[] = request.getParameterValues("AddFeeGridNo");
		//降档信息
		LCDutySet tLCDutySet = new LCDutySet();
		String tMult = request.getParameter("inputMult");
		int n = 0;
		if(tNo != null )
			 n = tNo.length ;
    if(tLoadFlag.equals("L")) {
    	if(n>0)
    	{
    		for(int i=0 ; i<n ; i++ )
    		{
	    		LCPremSchema tLCPremSchema = new LCPremSchema();
	    		tLCPremSchema.setPrem(tAddFeeValue[i]);
	    		tLCPremSchema.setRate(tAddFeeRate[i]);
	    		tLCPremSchema.setPayStartDate(tPayStartDate[i]);
	    		tLCPremSchema.setPayEndDate(tPayEndDate[i]);
	    		tLCPremSet.add(tLCPremSchema);
    		}
    	}
    	if(n == 0)
    	{
    		//如果进行加费操作而未填加费数据
    		//表示取消加费
    	}
        mTransferData.setNameAndValue("AddFeeFlag",tLoadFlag);
        
      } 
      //封装免责信息
      else if(tLoadFlag.equals("E")) {
      	int m = 0;
				if(tSpecNo != null )
					 m = tSpecNo.length ;
        for(int i=0 ; i<m ; i++ )
    		{
	    		LCSpecSchema tLCSpecSchema = new LCSpecSchema();
	    		tLCSpecSchema.setSpecCode(tSpecCode[i]);
	    		tLCSpecSchema.setSpecContent(tSpecInfo[i]);
	    		tLCSpecSchema.setSerialNo(tSerialNo[i]);
	    	//tLCSpecSchema.setPayStartDate(tSpecStartDate[i]);
	    	//tLCSpecSchema.setPayEndDate(tSpecEndDate[i]);
	    		tLCSpecSet.add(tLCSpecSchema);
    		}
    		mTransferData.setNameAndValue("SpecFlag",tLoadFlag);
      }
      else if(tLoadFlag.equals("M")) {
				LCDutySchema tLCDutySchema = new LCDutySchema();
    		mTransferData.setNameAndValue("LoadFlag",tLoadFlag);
      }
    // 准备传输数据 VData
    VData tVData = new VData();
    FlagStr="";

    tVData.add(tG);
    tVData.add(tLCUWMasterSchema);
    tVData.add(mTransferData);
    tVData.add(tLCPremSet);
    tVData.add(tLCSpecSet);
    
    ManuUWRiskSaveUI tManuUWRiskSaveUI = new ManuUWRiskSaveUI();
    try {
        System.out.println("this will save the data!!!");
        tManuUWRiskSaveUI.submitData(tVData,"");
      } catch(Exception ex) {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
      }

    if (!FlagStr.equals("Fail")) {
        tError = tManuUWRiskSaveUI.mErrors;
        if (!tError.needDealError()) {
            Content = " 保存成功! ";
            FlagStr = "Succ";
          } else {
            Content = " 保存失败，原因是:" + tError.getFirstError();
            FlagStr = "Fail";
          }
      }
  } else if(Flag.equals("Insured")) {

    LCIndUWMasterSchema tLCIndUWMasterSchema = new LCIndUWMasterSchema();

    String tContNo=request.getParameter("ContNo");
    String tInsuredNo = request.getParameter("InsuredNo");
    String tUWIndIdea=request.getParameter("UWIndIdea");
    String tIndPassFlag=request.getParameter("uwindstate");
    String tSugUWIndIdea=request.getParameter("SugIndUWIdea");
    String tSugIndPassFlag=request.getParameter("SugIndUWFlag");

    tLCIndUWMasterSchema.setContNo(tContNo);
    tLCIndUWMasterSchema.setPassFlag(tIndPassFlag);
    tLCIndUWMasterSchema.setUWIdea(tUWIndIdea);
    tLCIndUWMasterSchema.setSugPassFlag(tSugIndPassFlag);
    tLCIndUWMasterSchema.setSugUWIdea(tSugUWIndIdea);

    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("ContNo",tContNo);
    tTransferData.setNameAndValue("InsuredNo",tInsuredNo);
    tTransferData.setNameAndValue("LCIndUWMasterSchema",tLCIndUWMasterSchema);
    // 准备传输数据 VData
    VData tVData = new VData();
    FlagStr="";

    tVData.add(tG);
    tVData.add(tTransferData);

    LCInsuredUWUI tLCInsuredUWUI = new LCInsuredUWUI();

    try {
        System.out.println("this will save the data!!!");
        tLCInsuredUWUI.submitData(tVData,"submit");
      } catch(Exception ex) {
        Content = "保存失败，原因是:" + ex.toString();
        FlagStr = "Fail";
      }

    if (!FlagStr.equals("Fail")) {
        tError = tLCInsuredUWUI.mErrors;
        if (!tError.needDealError()) {
            Content = " 保存成功! ";
            FlagStr = "Succ";
          } else {
            Content = " 保存失败，原因是:" + tError.getFirstError();
            FlagStr = "Fail";
          }
      }
  }
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
