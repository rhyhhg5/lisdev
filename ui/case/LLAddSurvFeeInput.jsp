<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：LLAddSurvFeeInit.jsp
//程序功能：间接调查费录入
//创建日期：2005-6-26 12:07
//创建人  ：Xx
%>
  <head>
    <title>间接调查费录入</title>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="./CaseCommon.js"></SCRIPT>
    <script src="./LLAddSurvFeeInput.js"></script>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LLAddSurvFeeInit.jsp"%>

  </head>
  <body  onload="initForm();">
    <form name=fm target=fraSubmit action="./LLAddSurvFeeSave.jsp" method=post>
      <Div  id= "divInqFee" style= "display: ''" align = center>
        <table class= common border=0 width=100%>
          <TR  class= common>
            <TD  class= title>统计年份</TD>
            <TD class= input> <Input class=common name= Year onkeydown="QueryOnKeyDown();"></TD>
            <TD  class= title>统计月份</TD>
            <TD  class= input> <Input name=Month class=code CodeData="0|^1|一月|M^2|二月|M^3|三月|M^4|四月|M^5|五月|M^6|六月|M^7|七月|M^8|八月|M^9|九月|M^10|十月|M^11|十一月|M^12|十二月|M" onClick="showCodeListEx('Month',[this],[0]);" onkeyup="showCodeListKeyEx('Month',[this],[0]);" verify="统计月份|notnull" elementtype=nacessary></TD>
          </TR>
        </table>
        <Div align="left">
          <Table>
            <TR>
              <TD class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseList);">
              </TD>
              <TD class= titleImg>
                本月调查案件
              </TD>
            </TR>
          </Table>
        </div>
        <Div  id= "divCaseList" align = center style= "display: ''">
          <Table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanSurveyCaseGrid" ></span>
              </TD>
            </TR>

          </Table>
        </div>
      </Div>
      <br>
      <Div align="left">
        <table >
          <tr >
            <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divIndFeeDetail);">
            </td >
            <td class= titleImg> 间接调查费用</td>
          </tr>
        </table>
      </Div>
      <Div  id= "divIndFeeDetail" align = center style= "display: ''">
        <table  class= common >
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanAddFeeGrid" >
              </TR>
            </span>
          </TD>
        </TR>
      </table>
    </Div>
    <br>
    <div id='divResult'>
      <table  class= common>
        <TR  class= common8>
          <TD  class= title8>合  计</TD><TD  class= input8><input class= readonly name="Sum" readonly></TD>
          <TD  class= title8>案件平均</TD><TD  class= input8><input class= readonly name="Average" readonly></TD>
          <TD  class= title8>录入人</TD><TD  class= input8><input class= readonly name="Handler" readonly></TD>
        </TR>
      </table>
    </div>
    <hr>
    <div id="divConfirm" align=left>
      <INPUT VALUE="保  存" class= cssButton TYPE=button onclick="submitForm();">
      <INPUT VALUE="审  核" class= cssButton TYPE=button onclick="submitConf();">
      <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="top.close();">
    </div>

    <input type="hidden" id="LoadFlag" name="LoadFlag">
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="MStartDate" name="MStartDate">
    <input type=hidden id="MEndDate" name="MEndDate">
    <input type=hidden id="Operator" name="Operator">
    <input type=hidden id="BatchNo" name="BatchNo">
    <Input type="hidden" class= common name="comcode" >
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </Form>
</body>

</html>
