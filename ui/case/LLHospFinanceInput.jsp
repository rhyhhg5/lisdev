<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%> 
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<html>
<head>
<script>
	function initDate(){
   fm.AppDateS.value="<%=afterdate%>";
   fm.AppDateE.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
  }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="LLHospFinanceInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LLHospFinanceInit.jsp"%>
<title>医保通批量给付确认</title>
</head>
<body  onload="initForm();initDate();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>医保通批量给付确认
         </TD>
       </TR>
      </table>
    <table class="common" align="center" id="tbInfo" name="tbInfo">
      <tr class="common">
        <TD  class= title>管理机构</TD>
        <TD  class= input>
           <Input class="codeno" name=ManageCom readonly verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,180);" ><input class=codename name=ComName></TD>
        <TD  class= title>结算批次号</TD>
        <TD  class= input><input class="common" name="HCNo"></TD> 
        <TD  class= title></TD>
        <TD  class= input>
        </TD> 
      </tr>
      <tr class="common" id="divConfirm" style="display:''" >
        <td class="title">申请日期起期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="AppDateS"></td>
        <td class="title">申请日期止期</td>
        <td class="input"><input class="coolDatePicker" dateFormat="short" name="AppDateE"></td>
        <TD  class= title></TD>
        <TD  class= input></TD> 
      </tr>
      <tr class="common">
        <TD  class= title>医院代码</TD>
        <TD  class= input> <input class=code  name=HospitalCode onclick="return showCodeList('llhosp',[this,HospitalName],[0,1],null,fm.HospitalName.value,'Hospitname',1,240);" onkeyup="return showCodeListKeyEx('llhosp',[this,HospitalName],[0,1],null,fm.HospitalName.value,'Hospitname',1,240);" elementtype=nacessary verify="医院代码|notnull"></TD>
        <TD  class= title8>医院名称</TD>
        <TD  class= input> <input class=common4  name=HospitalName ></TD>
        <td class="title" id="divIDNot">给付状态</td>
        <td class="input" id="divIDNo"><input class="codeno" CodeData="0|2^0|未确认^1|已确认" name=State ondblclick="return showCodeListEx('State',[this,StateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('State',[this,StateName],[0,1]);"><input class=codename name=StateName></TD>
      </tr>
	</table>
	  <input value="查  询" type="button" class="cssButton" onclick="search();">
	  <input value="申  请" type="button" class="cssButton" onclick="applyHC();">
    <br>
    
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCaseInfo);"></td>
    	<td class= titleImg>结算批次信息</td></tr>
    </table>

  	<div id="divCaseInfo" style="display: ''">
      <table class="common">
        <tr class="common">
      	  <td text-align: left colSpan=1><span id="spanHospGetInfo"></span></td></tr>
      </table>

      <input VALUE="首  页" TYPE="button" class="cssButton" onclick="turnPage.firstPage();">
      <input VALUE="上一页" TYPE="button" class="cssButton" onclick="turnPage.previousPage();">
      <input VALUE="下一页" TYPE="button" class="cssButton" onclick="turnPage.nextPage();">
      <input VALUE="尾  页" TYPE="button" class="cssButton" onclick="turnPage.lastPage();">
  	</div>
  	<br>
  	<table>
      <tr>
        <td  id="divSubmitForm" >
          	<input value="开始给付" type="button" class="cssButton" onclick="deal()">
        </td>
        <td  id="divSubmitForm" >
          	<input value="打印给付凭证" type="button" class="cssButton" onclick="printPDF()">
        </td>
        <td  id="divSubmitForm" >
          	<input value="撤销批次" type="button" class="cssButton" onclick="cancel()">
        </td>
      </tr>
    </table>
    <br>
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		<input type=hidden name="SearchSQL" value="">
		<input type=hidden name="Operate" value="">
		<input type=hidden name="HCNoT" value="">
		<input type=hidden name="fmtransact" value="">
		<Div id=DivFileDownload style="display:'none'">
      <A id=fileUrl href=""></A>
    </Div>
  </form>
</body>
</html>
