//该文件中包含客户端需要处理的函数和事件
//程序名称：EdorList.js
//程序功能：
//创建日期：2007-3-18 21:54
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();


//----------------------全局变量区------------------------------

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();


//----------------------初始化程序区-----------------------------

//为业务类型下拉赋值
function setWorkTypeData()
{
  var temp = "";  //业务类型类别
  if(tAppObj == "I")
  {
    temp = "030";
  }
  else
  {
    temp = "031";
  }
  
  var sql = "select WorkTypeNo, WorkTypeName "
            + "from LGWorkType where WorkTypeNo like '" + temp + "%' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    var codes = "0|";
    for(var i = 0; i < rs.length; i++)
    {
      codes = codes + "^" + rs[i][0] + "|" + rs[i][1];
    }
    fm.WorkTypeNo.CodeData = codes;
  }
}

//查询保全清单
function queryEdorList()
{
  var edorType = "";
  var wherePart = "";
  var contNoFieldType = "";
  if(tAppObj == "G")
  {
    edorType = " LPGrpEdorItem ";
    contNoFieldType = "GrpContNo";
  }
  else
  {
    edorType = " LPEdorItem ";
    contNoFieldType = "ContNo";
  }
  
  var sql = "select a.CustomerNo, b.EdorType, (select min(EdorName) from LMEdorItem where EdorCode = b.EdorType), "
            + " b.EdorNo, case when b.GetMoney >=0 then b.GetMoney else 0 end, "
            + " case when b.GetMoney <=0 then b.GetMoney else 0 end, "
            + " (select Name from LDCom where ComCode = (select ComCode from LDUser where UserCode = a.AcceptorNo)), "
            + " (select UserName from LDUser where UserCode = a.Operator), a.AcceptDate, "
            + " (select ConfDate from LPEdorApp where EdorAcceptNo = b.EdorNo) ConfDate, "
            + " case b.EdorState when '0' "
            + " then (select Count(1) from LCInsuredList where EdorNo = b.EdorNo and GrpContNo = b.GrpContNo) "//增加减少人数
            + " else (select count(distinct InsuredNo) from LJAGetEndorse where EndorsementNo = b.EdorNo and GrpContNo = b.GrpContNo and FeeOperationType = b.EdorType and b.EdorType = 'NI') "
            + " end, "
            + " (select count(1) from LPInsured where EdorNo = b.EdorNo and EdorType = b.EdorType and GrpContNo = b.GrpContNo and b.EdorType in ('ZT', 'CT','XT','WT')), "
            + " b.EdorState, CodeName('itemedorstate', b.EdorState) "
            + " from LGWork a, " + edorType + " b "
            + " where a.WorkNo = b.EdorNo "
            + " and b." + contNoFieldType + " = '" + tContNo + "' "
            +" and not exists (select 1 from llcontdeal where edorno=a.workno )"
            + getWherePart("a.TypeNo", "WorkTypeNo")
            + getWherePart("a.AcceptDate", "ApplyDateStart", ">=")
            + getWherePart("a.AcceptDate", "ApplyDateEnd", "<=")
            + " order by ConfDate ";
  
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(sql, EdorListGrid);
  
  fm.all("divPage2").style.display = "";
  
  fm.EdorListSql.value = sql;
}
//查询理赔合同处理明细
function queryClaimList()
{
	var sql ="select a.caseno, (select min(EdorName) from LMEdorItem where EdorCode = a.EdorType),a.edorno,"
		+" a.appoperator,(select username from lduser where usercode=a.appoperator),"
		+" b.endcasedate ,b.mngcom,'已申请'"
		+" from llcontdeal a,llcase b,LPEdorItem c where a.caseno=b.caseno "
		+" and a.EdorNo= c.EdorNo"
		+" and a.contno='"+ tContNo +"'"
		+ getWherePart("a.appdate", "RgtDateStart",">=")
		+ getWherePart("a.appdate", "RgtDateEnd","<=")
	turnPage1.queryModal(sql, ContDealDetailGrid);		
}
function viewClaimInfo()
{
	var selno = ContDealDetailGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选中一条理赔合同处理信息!");
	    return false;
	}	
    var EdorAcceptNo = ContDealDetailGrid.getRowColData(selno - 1, 3);
    fm.EdorAcceptNo.value =EdorAcceptNo;
	var strSQL=" select 'x' from lpedoritem where edoracceptno='"+ EdorAcceptNo +"' and EdorState not in ('0','2')";
	var arrResult = easyExecSql(strSQL);
	if(arrResult)
	{
	  alert("本次申请，未理算，不能进行查看明细！");	
	  return false;
	}
	fm.action = "../f1print/AppEndorsementF1PJ1.jsp?type=";
	fm.target="f1print";		 	
	fm.submit();
}
//------------------事件响应区----------------
function printList()
{
  if(EdorListGrid.mulLineCount == 0)
  {
    alert("没有需要打印的数据");
    return false;
  }
  
  fm.action = "../sys/PrintEdorListSave.jsp";
	fm.target = "_blank";
	fm.submit();
  
  return true;
}

//察看工单
function viewEdorInfo()
{
  var row = EdorListGrid.getSelNo();
  
  if(row == null || row == 0)
  {
    alert("请选择一条记录");
    return false;
  }
  
  var WorkNo = EdorListGrid.getRowColDataByName(row - 1, "EdorNo");
  var CustomerNo = EdorListGrid.getRowColDataByName(row - 1, "CustomerNo");
  
	var width = screen.availWidth - 10;
  var height = screen.availHeight - 28;
	win = window.open("../task/TaskViewMain.jsp?WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + CustomerNo, 
					  "view", "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
	win.focus();
}