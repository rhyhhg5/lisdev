<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%>
<html>    
<%
//程序名称：LLCustomerInfoInput.jsp
//程序功能：F1报表生成
//创建日期：2010-12-02
//创建人  ：zzh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLCustomerInfoInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<script>
	function initDate(){
   fm.EndCaseDateS.value="<%=afterdate%>";
   fm.EndCaseDateE.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
   var arrResult=easyExecSql("select Name from ldcom where comcode='<%=tG1.ManageCom%>'");
   fm.ManageComName.value=arrResult[0][0];
  }
 </script>
<body onload="initDate();">
  <form action="" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
    	<TR  class= common>          
				<TD  class= title>统计机构</TD>
				<TD  class= input>
    			<Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return organname('comcode',[this,ManageComName],[0,1],null,null,null,1);" verify="统计机构|notnull"><Input class=codename  name=ManageComName></TD>
          		<TD  class= title8>结案起期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="EndCaseDateS" verify="生效起期|date&notnull"></TD>
          		<TD  class= title8>结案止期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="EndCaseDateE" verify="生效止期|date&notnull"></TD>
        </TR>
      	
    </table>
    <table class=common>
    	<tr class=common>
          <TD><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="CustomerinfoRPT()"></TD>
		</tr>
	</table>
	<table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
	 <Div  id= "divCare" style= "display: ''">
		 	<tr class="common"><td class="title">统计机构默认为登录机构，必录</td></tr><br>
		 	<tr class="common"><td class="title">结案起期、结案止期必录，无时间间隔限制</td></tr><br>
		 	<tr class="common"><td class="title">一次最多打印3万条</td></tr><br>
	</Div>
	<INPUT  type= hidden name="fmtransact" value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 