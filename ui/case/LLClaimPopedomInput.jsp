<html>
	<%
	//Name：LLClaimPopedomInput.jsp
	//Function：登记界面的初始化
	//Date：2005-8-23 16:49:22
	//Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.llcase.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLClaimPopedomInput.js"></SCRIPT>
		<%@include file="LLClaimPopedomInit.jsp"%>
	</head>

	<body  onload="initForm();" >
		<form action="./LLClaimPopedomSave.jsp" method=post name=fm target="fraSubmit">

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class= titleImg>
						理赔员权限配置
					</TD>
				</TR>
			</table>

			<Div  id= "divLLLLMainAskInput1" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD class= title>
							用户核赔权限
						</TD>
						<TD class=input>
							<Input class="codeno" name=ClaimPopedom ondblClick="showCodeList('llclaimpopedom',[this,ClaimPopedomName],[0,1],null,null,null,'1');"  onkeyup="showCodeListKey('llclaimpopedom',[this,ClaimPopedomName],[0,1],null,null,null,'1');" ><Input class="codename" name=ClaimPopedomName elementtype=nacessary verify="理赔师权限|notnull" >
						</TD>
						<TD class= title>

						</TD>
						<TD class=input>
							<Input class="readonly" name=ClaimPopedom readonly>
						</TD>
					</TR>
					<TR>
						<TD class= titleImg>
							医疗险、失能险、护理险类给付
						</TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>给付金额</TD><TD  class= input8><input class= common name="PayAmntH"></TD>
						<TD  class= title8>拒付金额</TD><TD  class= input8><input class= common name="RefuseAmntH" ></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>通融给付金额</TD><TD  class= input8><input class= common name="AccomAmntH"></TD>
						<TD  class= title8>协议给付金额</TD><TD  class= input8><input class= common name="ConsultAmntH" ></TD>
					</TR>
					<TR>
						<TD class= titleImg colspan=2>
							重疾险、意外险类给付
						</TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>给付金额</TD><TD  class= input8><input class= common name="PayAmntA"></TD>
						<TD  class= title8>拒付金额</TD><TD  class= input8><input class= common name="RefuseAmntA" ></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>通融给付金额</TD><TD  class= input8><input class= common name="AccomAmntA"></TD>
						<TD  class= title8>协议给付金额</TD><TD  class= input8><input class= common name="ConsultAmntA" ></TD>
					</TR>
					<TR>
						<TD class= titleImg colspan=2>
							其他权限
						</TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>豁免类（保费期数）</TD><TD  class= input8><input class= common name="Exemption"></TD>
						<TD  class= title8>案件解约权限</TD>
						<TD  class= input8>
							<Input class="codeno" name=ContCancel CodeData="0|2^0|无^1|有" onclick="return showCodeListEx('llcontcancel',[this,ContCancelName],[0,1]);" onkeyup="return showCodeListKeyEx('llcontcancel',[this,ContCancelName],[0,1]);"><Input class="codename" name=ContCancelName>
						</TD>
					</TR>
				</table>

			</DIV>
			<input name="AskIn" style="display:''"  class=cssButton type=button value="保 存" onclick="submitForm()">
			<input name="AskIn" style="display:''"  class=cssButton type=button value="修 改" onclick="updateClick()">
			<input name="AskIn" style="display:''"  class=cssButton type=button value="删 除" onclick="deleteClick()">
		</br>

	</Div>

	<!--隐藏域-->
	<Input type="hidden" class= common name="fmtransact" >
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
