<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLOperationInputInit.jsp
//程序功能：赔付维护页面初始化

%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
function initForm(){
  try{
    initOperationOneGrid();
    initOperationTwoGrid();
    initOperationRisk1Grid();
    initOperationRisk2Grid();
	
    //initTitle();
    <%
    GlobalInput mG = new GlobalInput();
    mG=(GlobalInput)session.getValue("GI");
    %>
	fm.ModifyDate.value = getCurrentDate();
	//QueryPolicyDetails("init");

  }
  catch(re)
  {
    alert("LLOperationInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
  }
}

/*险种明细查询的初始化*/
function initOperationRisk1Grid()
{
	var iArray = new Array();
	try
	  {
	    iArray[0]=new Array("序号","30px","100","0");
	    iArray[1]=new Array("个单合同号","80px","10","0");
	    iArray[2]=new Array("险种代码","80px","10","0");
	    iArray[3]= new Array("赔付结论","60px","100","0");
	    iArray[4]= new Array("赔付结论代码","80px","100","0");
	    iArray[4][21] = "GiveType";
	    iArray[5]= new Array("赔付结论依据","80px","100","0");
	    iArray[6]= new Array("赔付结论依据代码","80px","100","0");
	    iArray[6][21] = "GiveReason";
	    iArray[7]= new Array("保单号","80px","100","3");
	    iArray[8]= new Array("赔案号","80px","100","3");
	    iArray[9]= new Array("受理事故号","80px","100","3");
	    iArray[10]= new Array("给付责任类型","80px","100","3");
	    OperationRisk1Grid = new MulLineEnter( "fm" , "OperationRisk1Grid" );
	    OperationRisk1Grid.mulLineCount = 1;
	    OperationRisk1Grid.displayTitle = 1;

	    OperationRisk1Grid.canSel=0;
	    OperationRisk1Grid.canChk=0;
	    OperationRisk1Grid.hiddenPlus=1;
	    OperationRisk1Grid.hiddenSubtraction=1;

	    OperationRisk1Grid.loadMulLine(iArray);

	  }
	  catch(ex)
	  {
	    alert(ex);
	  }
}

/*险种明细修改的初始化*/
function initOperationRisk2Grid()
{
	var iArray = new Array();
	try
	  {
	    iArray[0]=new Array("序号","30px","100","0");
	    iArray[1]=new Array("个单合同号","80px","10","0");
	    iArray[2]=new Array("险种代码","80px","10","0");
	    iArray[3]= new Array("赔付结论","60px","100","2","llclaimdecision","3|4","1|0");
	    iArray[4]= new Array("赔付结论代码","80px","100","0");
	    iArray[4][21] = "GiveType";
	    iArray[5]= new Array("赔付结论依据","80px","100","2","llclaimdecision_1", "5|6","1|0");
	    iArray[5][15]="赔付结论代码"
	    iArray[5][17]="4"
	    iArray[6]= new Array("赔付结论依据代码","80px","100","0");
	    iArray[6][21] = "GiveReason";
	    iArray[7]= new Array("保单号","80px","100","3");
	    iArray[8]= new Array("赔案号","80px","100","3");
	    iArray[9]= new Array("受理事故号","80px","100","3");
	    iArray[10]= new Array("给付责任类型","80px","100","3");
	    
	    OperationRisk2Grid = new MulLineEnter( "fm" , "OperationRisk2Grid" );
	    OperationRisk2Grid.mulLineCount = 1;
	    OperationRisk2Grid.displayTitle = 1;

	    OperationRisk2Grid.canSel=0;
	    OperationRisk2Grid.canChk=0;
	    OperationRisk2Grid.hiddenPlus=1;
	    OperationRisk2Grid.hiddenSubtraction=1;

	    OperationRisk2Grid.loadMulLine(iArray);

	  }
	  catch(ex)
	  {
	    alert(ex);
	  }
}

/*保单责任明细查询的初始化*/
function initOperationOneGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array("序号","30px","100","0");
    iArray[1]=new Array("个单合同号","80px","10","0");
    iArray[2]=new Array("险种代码","80px","10","0");
    iArray[3]= new Array("给付责任","80px","100","0");
    iArray[4]= new Array("给付责任类型","100px","100","0");
    iArray[5]= new Array("给付责任编码","100px","100","0");
    iArray[6]= new Array("赔付结论","80px","100","0");
    iArray[7]= new Array("赔付结论代码","100px","100","0");
    iArray[7][21] = "GiveType";
    iArray[8]= new Array("赔付结论依据","100px","100","0");
    iArray[9]= new Array("赔付结论依据代码","120px","100","0");
    iArray[9][21] = "GiveReason";
    iArray[10]= new Array("保单号","80px","100","3");
    iArray[11]= new Array("赔案号","80px","100","3");
    iArray[12]= new Array("受理事故号","80px","100","3");
    iArray[13]= new Array("实赔金额","80px","100","0");
    
    OperationOneGrid = new MulLineEnter( "fm" , "OperationOneGrid" );
    OperationOneGrid.mulLineCount = 1;
    OperationOneGrid.displayTitle = 1;

    OperationOneGrid.canSel=0;
    OperationOneGrid.canChk=0;
    OperationOneGrid.hiddenPlus=1;
    OperationOneGrid.hiddenSubtraction=1;

    OperationOneGrid.loadMulLine(iArray);

  }
  catch(ex)
  {
    alert(ex);
  }
}

/*保单责任明细修改的初始化*/
function initOperationTwoGrid()
{
  var iArray = new Array();
  try
  {
	iArray[0]=new Array("序号","30px","100","0");
	iArray[1]=new Array("个单合同号","80px","10","0");
	iArray[2]=new Array("险种代码","80px","10","0");
	iArray[3]= new Array("给付责任","80px","100","0");
	iArray[4]= new Array("给付责任类型","100px","100","0");
	iArray[5]= new Array("给付责任编码","100px","100","0");
	iArray[6]= new Array("赔付结论","80px","100","2","llclaimdecision","6|7","1|0");
	iArray[7]= new Array("赔付结论代码","100px","100","0");
	iArray[7][21] = "GiveType";
	iArray[8]= new Array("赔付结论依据","100px","100","2","llclaimdecision_1", "8|9","1|0");
	iArray[8][15]="赔付结论代码"
	iArray[8][17]="7"
	iArray[9]= new Array("赔付结论依据代码","120px","100","0");
	iArray[9][21] = "GiveReason";
	iArray[10]= new Array("保单号","80px","100","3");
	iArray[11]= new Array("赔案号","80px","100","3");
    iArray[12]= new Array("受理事故号","80px","100","3");
    iArray[13]= new Array("实赔金额","80px","100","0");
    
    OperationTwoGrid = new MulLineEnter( "fm" , "OperationTwoGrid" );
    OperationTwoGrid.mulLineCount = 1;
    OperationTwoGrid.displayTitle = 1;

    OperationTwoGrid.canSel=0;
    OperationTwoGrid.canChk=0;
    OperationTwoGrid.hiddenPlus=1;
    OperationTwoGrid.hiddenSubtraction=1;

    OperationTwoGrid.loadMulLine(iArray);

  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>
