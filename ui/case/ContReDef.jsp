<html>
	<%
	//Name：ContReDef.jsp
	//Function：非标准业务配置界面
	//Date：2006-12-10 16:49:22
	//Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="ContReDef.js"></SCRIPT>
		<%@include file="ContReDefInit.jsp"%>
	</head>
	<body  onload="initForm();" >

		<form action="./LLSecondUWSave.jsp" method=post name=fm target="fraSubmit">
			<div id=BaseCondition>
				<Table>
					<TR>
						<TD class=common>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBaseInfo);">
						</TD>
						<TD class= titleImg >客户信息</TD>
					</TR>
				</Table>
				<div id='divBaseInfo'>
					<table  class= common>
						<TR  class= common8>
							<TD  class= title8>团体客户号</TD><TD  class= input8><input class= common name="GrpNo" onkeydown="QueryOnKeyDown();" ></TD>
							<TD  class= title8>团体名称</TD><TD  class= input8><input class= common name="GrpName" onkeydown="QueryOnEnter();"></TD>
							<TD  class= title8>团体保单号</TD><TD  class= input8><input class= common name="GrpContNo" onkeydown="QueryOnEnter();"></TD>
						</TR>
						<TR  class= common>	
            			<TD  class= input8>管理机构</TD>
            			<TD  class= input8><Input class="fcodeno" name=MngCom verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,180);"><input class=fcodename name=ComName></TD>
							<TD  class= title8>录入日期从</TD>
							<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
							<TD  class= title8>到</TD>
							<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
						</TR>
					</table>
				</div>

				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCont);">
						</TD>
						<TD class= titleImg>
							非标准业务保单列表
						</TD>
					</TR>
				</table>
				<Div  id= "divCont" align= center style= "display: ''">
					<table  class= common>
						<TR  class= common>
							<TD text-align: left colSpan=1>
								<span id="spanContGrid" ></span>
							</TD>
						</TR>
					</table>
				</Div>
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRemark);">
						</TD>
						<TD class= titleImg>
							特别约定
						</TD>
					</TR>
				</table>
				<div id="divRemark" align=center style="display:''">
					<table class=common>
						<TR  class= common>
							<TD  class= input>
								<textarea name="Remark" cols="100%" rows="2" witdh=25% class="common"></textarea>
							</TD>
						</tr>
					</table>
				</div>
				<hr>
				<Div  id= "divPol" align= left style= "display: 'none'">
					<table>
						<TR>
							<TD>
								<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
							</TD>
							<TD class= titleImg>
								险种列表
							</TD>
						</TR>
					</table>
					<Div  id= "divPolGrid" align= center style= "display: ''">
						<table  class= common>
							<TR  class= common>
								<TD text-align: left colSpan=1>
									<span id="spanPolGrid" ></span>
								</TD>
							</TR>
						</table>
					</Div>
				</Div>
				<Div  id= "divPlan" align= left style= "display: 'none'">
					<table>
						<TR>
							<TD>
								<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPlanGrid);">
							</TD>
							<TD class= titleImg>
								保障计划列表
							</TD>
						</TR>
					</table>
					<Div  id= "divPlanGrid" align= center style= "display: ''">
						<table  class= common>
							<TR  class= common>
								<TD text-align: left colSpan=1>
									<span id="spanPlanGrid" ></span>
								</TD>
							</TR>
						</table>
					</Div>
				</Div>
				<br>
				<input name="ScanBut" style="width: 100px;"  class=cssButton type=button value="承保协议图像" onclick="ScanQuery()">
				<input name="TranBut" style="width: 100px;"  class=cssButton type=button value="险种层配置" onclick="RiskPage()">
				<input name="Suggest" style="width: 100px;"  class=cssButton type=button value="责任层配置" onclick="DutyPage()">
			</div>
			<div id=divBasePage style="display:'none'">
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBaseRisk);">
						</TD>
						<TD class= titleImg>
							保单基本信息
						</TD>
					</TR>
				</table>
				<div id=divBaseRisk style="display:''">
					<table  class= common>
						<TR  class= common8>
							<TD  class= title8>团体名称</TD><TD  class= input8><input class= readonly name="selGrpName" readonly ></TD>
							<TD  class= title8>团体保单号</TD><TD  class= input8><input class= readonly name="selGrpContNo" readonly ></TD>
							<TD  class= title8>险种名称</TD><TD  class= input8><input class= readonly name="RiskName" readonly ></TD>
						</TR>
						<TR  class= common8>
							<TD  class= title8>保障计划</TD><TD  class= input8><input class= readonly name="PlanCode" readonly ></TD>
							<TD  class= title8>人员类别</TD><TD  class= input8><input class= readonly name="InsuType" readonly ></TD>
							<TD  class= title8>参保人数</TD><TD  class= input8><input class= readonly name="People" readonly ></TD>
						</TR>
					</table>
				</div>
			</div>
			<div id=divRiskPage style="display:'none'">
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRiskFactor);">
						</TD>
						<TD class= titleImg>
							险种配置要素
						</TD>
					</TR>
				</table>
				<div id=divRiskFactor style="display:''">
					<table  class= common>
						<TR  class= common8>
							<TD  class= title8>限额</TD><TD  class= input8><input class= common name="RiskPeak"></TD>
							<TD  class= title8>保额</TD><TD  class= input8><input class= common name="RiskAmnt"></TD>
							<TD  class= title8>团体公共保额</TD><TD  class= input8><input class= common name="GrpAmnt"></TD>
						</TR>
						<TR  class= common8>
							<TD class= title8>生效日期</TD><TD  class= input8><input class=common name="PlanCode"></TD>
							<TD class= input8 colSpan=4>
							</TD>
						</TR>
					</table>
				<input name="ExportRH" class=cssButton type=button value="推荐医院导出" onclick="SubmitForm()">
				<input name="ImportRH" class=cssButton type=button value="推荐医院导入" onclick="BackMain()">
				<input name="ExportOH" class=cssButton type=button value="指定医院导出" onclick="SubmitForm()">
				<input name="ImportOH" class=cssButton type=button value="指定医院导入" onclick="BackMain()">
				</div>
				<hr>
				<input name="EnsureBut" class=cssButton type=button value="确  认" onclick="SubmitForm()">
				<input name="BackBut" class=cssButton type=button value="返  回" onclick="BackMain()">
			</div>
			<div id=divDutyPage style="display:'none'">
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butCollapse.gif" style= "cursor:hand;" OnClick= "showPage(this,divDutyGrid);">
						</TD>
						<TD class= titleImg>
							显示原始责任信息
						</TD>
					</TR>
				</table>
				<Div  id= "divDutyGrid" align= center style= "display: 'none'">
					<table  class= common>
						<TR  class= common>
							<TD text-align: left colSpan=1>
								<span id="spanDutyGrid" ></span>
							</TD>
						</TR>
					</table>
				</Div>
				<hr>
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,spanGetDutyGrid);">
						</TD>
						<TD class= titleImg>
							基础配置
						</TD>
					</TR>
				</table>
				<Div  id= "divGetDutyGrid" align= center style= "display: ''">
					<table  class= common>
						<TR  class= common>
							<TD text-align: left colSpan=1>
								<span id="spanGetDutyGrid" ></span>
							</TD>
						</TR>
					</table>
				</Div>
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,spanGetDutyGrid);">
						</TD>
						<TD class= titleImg>
							责任要素特例配置
						</TD>
					</TR>
				</table>
				<table  class= common>
					<tr>
						<td>
							<input type="checkbox" value="1" name="chkGetRate" onclick="showGetRate()">给付比例
							<input type="checkbox" value="2" name="chkFeeItem" onclick="showFeeItem()">费用项目
							<input type="checkbox" value="3" name="chkSubsidy" onclick="showSubsidy ()">日额津贴
							<input type="checkbox" value="4" name="chkWaitPeriod" onclick="showWaitPeriod()">等待期
						</td>
					</tr>
				</table>
				<div id=divGetRate style="display:'none'">
					<table  class= common>
						<TR  class= common8>
							<TD  class= title8>给付比例类型</TD>
							<TD  class= input8><input class= codeno name="GetRateType" onclick="return showCodeList('grtype',[this,GetRateTypeName],[0,1]);" onkeyup="return showCodeListKey('grtype',[this,GetRateTypeName],[0,1]);" ><input class= codename name="GetRateTypeName"  ></TD>
							<TD  class= input8 style="width=60%">
							</TD>
						</TR>
					</table>
					<Div  id= "divGetRateGrid" align= center style= "display: ''">
						<table  class= common>
							<TR  class= common>
								<TD text-align: left colSpan=1>
									<span id="spanGetRateGrid" ></span>
								</TD>
							</TR>
						</table>
					</Div>
				</div>
				<div id=divFeeItem align= center style="display:'none'">
					<table  class= common>
						<TR  class= common>
							<TD text-align: left colSpan=1>
								<span id="spanFeeItemGrid" ></span>
							</TD>
						</TR>
					</table>
				</div>
				<div id=divSubsidy style="display:'none'">
					<table  class= common>
						<TR  class= common8>
							<TD  class= title8>档次</TD>
							<TD  class= input8><input class= common readonly name="Mult"  ></TD>
							<TD  class= title8>免赔天数</TD>
							<TD  class= input8><input class= common name="FreeDays"  ></TD>
							<TD  class= title8>每日津贴</TD>
							<TD  class= input8><input class= common name="Subsidy"  ></TD>
						</TR>
					</table>
				</div>
				<div id=divWaitPeriod style="display:'none'">
					<table  class= common>
						<TR  class= common8>
							<TD  class= title8>普通病等待期</TD>
							<TD  class= input8><input class= common readonly name="OWaitPeriod"  ></TD>
							<TD  class= title8>慢性病等待期</TD>
							<TD  class= input8><input class= common readonly name="CWaitPeriod"  ></TD>
							<TD  class= input8 style="width=30%">
							</TD>
						</TR>
					</table>
				</div>
				<br>
				<input name="EnsureBut" class=cssButton type=button value="确  认" onclick="SubmitForm()">
				<input name="BackBut" class=cssButton type=button value="返  回" onclick="BackMain()">
			</div>
			<input type=hidden name="sql" >
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
