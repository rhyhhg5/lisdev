<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLComCaseReportInput.jsp
//程序功能：F1报表生成
//创建日期：2012-03-12
//创建人  ：YJ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%
   GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-30";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>

<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLComCaseReportInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script>
	function initDate(){
   fm.StartDate.value="<%=afterdate%>";
   fm.EndDate.value="<%=CurrentDate%>";
   
   
//  try
//  {   
//	  
//	    fm.all('ManageCom').value = '<%=tG1.ManageCom%>';  
//        var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
//            //显示代码选择中文
//            if (arrResult != null) {
//            fm.all('ComName').value=arrResult[0][0];
//           }  
 
//  }
//  catch(ex)
//  {
//    alert("在AllPBqQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
//  } 
   
//  }
</script>
</head>
<body onload="initDate();">    
  <form action="./LLComCaseReportSave.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode4&NOTNULL&INT" ondblclick="return showCodeList('comcode4',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode4',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          </TR>
          <TR  class= common>      
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR  class= common>
          <TD  class= title>业务类型</TD>
          <TD  class= input> <input class="codeno" value="3" CodeData="0|3^1|商业保险^2|社会保险^3|全部"  verify="业务类型|notnull" elementtype=nacessary name=StatsType ondblclick="return showCodeListEx('StatsType',[this,StatsTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StatsType',[this,StatsTypeName],[0,1]);"><input class=codename value="全部" name=StatsTypeName></TD> 
        </TR>
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="SubmitPrint()"></TD>
			</tr>
		</table>

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 