var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var tSaveType="";
var arrDataSet ;

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


function UWClaim()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择一条团体案件！");
	      return ;
	}
		
		//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		var varSrc="";
		var newWindow = window.open("./FrameMain.jsp?Interface=PayAffirmInput.jsp"+varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SearchGrpRegister()
{
	var comCode = fm.ComCode.value;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var anow = getCurrentDate();
  var MngCom=	fm.all('MngCom').value;
  // 书写SQL语句
  var strSql = " SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO,R.RGTDATE, R.ENDCASEDATE , b.codename,"  
           		+" CASE r.endcaseflag WHEN '1' then to_char(to_date( r.endcasedate)-to_date(r.rgtdate)) else to_char(to_date('" +anow +"' )- to_date(R.RGTDATE))  end case "
  	       		+" FROM LLREGISTER R , ldcode b "  
  	       		+" WHERE R.RGTOBJ = '0' AND R.RGTCLASS = '1' " 
  	       		+" and b.codetype='llgrprgtstate' and b.code = r.rgtstate "
  	       	  + getWherePart("R.mngcom","MngCom","like")
 	     				+" and r.mngcom like '"+comCode+"%%'" 
  						;

  if (fm.all('srCustomerNo').value != '')	//客户号
  	strSql = strSql + " AND R.CUSTOMERNO = '" + fm.all('srCustomerNo').value + "'";
  if (fm.all('srGrpName').value != '')	//客户姓名
  	strSql = strSql + " AND R.GRPNAME = '" + fm.all('srGrpName').value + "'";
 	 	       
  //申请日期区间查询
  
  var blDateStart = !(fm.all('RgtDateStart').value == null || 
  			fm.all('RgtDateStart').value == "");
  var blDateEnd = !(fm.all('RgtDateEnd').value == null ||
  			fm.all('RgtDateEnd').value == "");
  
  if(blDateStart && blDateEnd) {
  	strSql = strSql + " AND R.RGTDATE BETWEEN '" + 
  			fm.all('RgtDateStart').value + "' AND '" +
  			fm.all('RgtDateEnd').value + "'";
  } else if (blDateStart) {
  	strSql = strSql + " AND R.RGTDATE > '" + fm.all('RgtDateStart').value + "'";
  } else if (blDateEnd) {
  	strSql = strSql + " AND R.RGTDATE < '" + fm.all('RgtDateEnd').value + "'";
  }

  turnPage.queryModal(strSql,GrpRegisterGrid);
  

	
	//turnPage.useSimulation = 1;
    turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);
    //判断是否查询成功
    if (turnPage.strQueryResult){
    //查询成功则拆分字符串，返回二维数组
    arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
    
    	for( i=0; i<arrDataSet.length; i++)
 		{
 		
				idate = eval(arrDataSet[i][7]);
		
				idate = idate + 1;
				iidate = ''+ idate;
				arrDataSet[i][7]= iidate;
 		}
 		
    turnPage.arrDataCacheSet = arrDataSet;
    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    turnPage.pageDisplayGrid = GrpRegisterGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSql;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    turnPage.pageLineNum =10;
    //在查询结果数组中取出符合页面显示大小设置的数组
    var tArr = new Array();
    tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 10);
    //调用MULTILINE对象显示查询结果
    displayMultiline(tArr, turnPage.pageDisplayGrid);
   
    }
    
    
  
  showInfo.close();	
}


function queryGrpCaseGrid()
{
	var comCode = fm.ComCode.value;
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <= 0)
	{				
	      return ;
	}
	var jsRgtNo = GrpRegisterGrid.getRowColData(selno - 1, 1);	

	if(jsRgtNo == null || jsRgtNo == "") 
	{
		alert("团体批次号为空！");
		return;
	}
	 	
	  var strSql = " SELECT C.CASENO, C.CUSTOMERNO, C.CUSTOMERNAME, C.RGTDATE, b.codename" + 
	  	       " FROM LLCASE C, ldcode b  " + 
	  	       " WHERE C.RGTNO = '" + jsRgtNo + "'" +
	  	       " and b.codetype='llrgtstate' and b.code = c.rgtstate " +
	  	       " and c.rgtstate = '09' " +
	  	       " and c.mngcom like '"+comCode+"%%'" 
	  	       " ORDER BY C.CASENO ";
	
	  turnPage.queryModal(strSql,GrpCaseGrid);
	  showPage(this,divGrpCaseInfo);   	       
}

function QueryOnKeyDown()
{
	var keycode = event.keyCode;

	if(keycode=="13")
	{
		SearchGrpRegister();		
	}
}

function ShowInfo()
{
	var comCode = fm.ComCode.value;
		var sql = " select a.grpname, decimal(a.sumprem,12,2), a.peoples2, count(b.caseno) as num, " 
		 				+ " decimal( decimal(count(b.caseno))*100 / decimal((select count(caseno) a from llcase where endcasedate is not null)), 12,2),"
		 				+ " sum(d.realpay), decimal(sum(d.realpay)*100/decimal((select sum(realpay) from llclaim) ),12,2) "
						+ " from   lcgrpcont a, llclaim d, "
	    			+ " (select distinct caseno, grpcontno from llclaimpolicy where  grpcontno <> '00000000000000000000') as b "
						+ " where  a.grpcontno = b.grpcontno "
						+ " and b.caseno = d.caseno "
						+" and d.mngcom like '"+comCode+"%%'" 
						+ " group by a.grpname, a.sumprem, a.peoples2 "
						+ " order by num DESC "
						;
		turnPage2.queryModal(sql,GrpCaseGrid);
}