<%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page contentType="text/html;charset=GBK" %>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
			 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
 %>
<html>
<head>
<title>调查费用汇总</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./CaseCommon.js"></SCRIPT>
  <script src="./LLSurvFeeSum.js"></script> 

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLSurvFeeSumInit.jsp"%>
   <script language="javascript">
   function initDate(){
   fm.Month.value="<%=tCurrentMonth%>"/1-1;
   fm.Year.value="<%=tCurrentYear%>";
  		fm.Operator.value="<%=Operator%>"
  		 	fm.comcode.value="<%=Comcode%>";
   }
   </script>

</head>
<body  onload="initDate();initForm();">
<!--登录画面表格-->
<form name=fm target=fraSubmit method=post>
<Div  id= "divInqFee" style= "display: ''" align = center>
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>统计年份</TD>
          <TD class= input> <Input class=common name= Year onkeydown="QueryOnKeyDown();">
          <TD  class= title>统计月份</TD>
          <TD  class= input> <Input name=Month class=codeno CodeData="0|^1|一月|M^2|二月|M^3|三月|M^4|四月|M^5|五月|M^6|六月|M^7|七月|M^8|八月|M^9|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('Month',[this,MonthName],[0,1]);" onkeyup="showCodeListKeyEx('Month',[this,MonthName],[0,1]);" verify="统计月份|notnull" elementtype=nacessary><input class=codename name=MonthName> </TD> 
        </TR>
    </table>
  <Div align="left">
    <Table>
    	<TR>
        <TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBnfList);">
    		</TD>
    		<TD class= titleImg>
    			 调查费用汇总
    		</TD> 
    	</TR>
    </Table>    	
  </div>
  <Div  id= "divInqFeeDetailList" align = center style= "display: ''">
  	 <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanSurveyCaseGrid" ></span> 
  			</TD>
      </TR>
    </Table>
  </div>
</Div>	
  <br>
		<Div  align=center style= "display: '' ">
		<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
		<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
		<br>
<hr>
  <div id="div1" align=left>
    <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="top.close();">
  </div>			
 							
<input type="hidden" id="RgtNo" name="RgtNo">

<input type="hidden" id="LoadFlag" name="LoadFlag">
<input type=hidden id="fmtransact" name="fmtransact">
<input type=hidden id="MStartDate" name="MStartDate">
<input type=hidden id="MEndDate" name="MEndDate">
<input type=hidden id="Operator" name="Operator">
<Input type="hidden" class= common name="comcode" >
<input type=hidden id="RgtState" name="RgtState">
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>

</html>
