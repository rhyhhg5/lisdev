<%
//Name：LLRegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

String LoadFlag= request.getParameter("LoadFlag");
LoadFlag= LoadFlag==null?"":LoadFlag;
String RgtNo= request.getParameter("RgtNo") ;

//RgtNo=RgtNo== null ?"":RgtNo;
String CaseNo= request.getParameter("CaseNo");
//CaseNo=CaseNo==null?"":CaseNo;
String LoadC="";
if(request.getParameter("LoadC")!=null)
{
LoadC = request.getParameter("LoadC");
}
String tShowCaseRemarkFlag = "";//
tShowCaseRemarkFlag = (String)session.getAttribute("ShowCaseRemarkFlag");
System.out.println("tShowCaseRemarkFlag:"+tShowCaseRemarkFlag);
%>

<script language="JavaScript">
  var loadFlag="<%=LoadFlag%>";
  var RgtNo = '<%=RgtNo%>';
  RgtNo= (RgtNo=='null'?"":RgtNo);
  var CaseNo = '<%=CaseNo %>';
  CaseNo= (CaseNo=='null'?"":CaseNo);

  function initInpBox( )
  {
    try {
      fm.reset();
      fm.CustomerNo.value="";
//      fm.CaseOrder.value="1";
//      fm.CaseOrderName.value="普通";
      fm.LoadFlag.value = loadFlag;
      fm.RgtNo.value = RgtNo;
      fm.CaseNo.value = CaseNo;
      fm.LoadC.value="<%=LoadC%>";
      fm.RiskCode.value = "<%=request.getParameter("RiskCode")%>"
      if(fm.RiskCode.value=='1605'){
        tiAppMoney.style.display='';
        idAppMoney.style.display='';
        titemp0.style.display='none';
        idtemp0.style.display='none';
      }
      else{
        tiAppMoney.style.display='none';
        idAppMoney.style.display='none';
        titemp0.style.display='';
        idtemp0.style.display='';
      }
      if (fm.LoadC.value=='2'){
        //			fm.CaseNo.focus();
        aa.style.display='none';
        divnormalquesbtn.style.display='none';
        //    	CaseChange();
      }

    }
    catch(ex) {
      alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在RegisterInputInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initForm(){
    try{
      titleGrp.style.display='none';
      inputGrp.style.display='none';
      if(loadFlag=="0")
      {
        titleGrp.style.display='';
        inputGrp.style.display='';
        div1.style.display='none';
        divRegisterInfo.style.display='none';
        fm.Relation.value='05';
        fm.RgtType.value='9';
      }
      initInpBox();
      fm.ShowCaseRemarkFlag.value = "<%=tShowCaseRemarkFlag%>"; 
      initEventGrid();
      initAppealGrid();
      checkRgtFlag();
      initQuery();
      initDate();    
      showCaseRemark();//#1769 案件备注信息的录入和查看功能 add by Houyd 初始化页面弹出
      
      <%GlobalInput mG = new GlobalInput();
      mG=(GlobalInput)session.getValue("GI");
      %>
      fm.Handler.value = "<%=mG.Operator%>";
      fm.ModifyDate.value = getCurrentDate();
    }
    catch(re){
      alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
    }
  }

  // 保单信息列表的初始化
  function initEventGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","0px","10","0");
      
        iArray[1]=new Array();
        iArray[1][0]="事件号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        iArray[1][21]="Subno";

      iArray[2]=new Array("发生日期","80px","100","1");
      iArray[2][9]="发生日期|notnull&date";

      iArray[3]=new Array("发生地点(省)","90px","100","2");
      iArray[3][4]="province1";
      iArray[3][5]="3|15";
      iArray[3][6]="1|0";
      
      iArray[4] =new Array("发生地点(市)","90px","100","2");
      iArray[4][4]="city1";
      iArray[4][5]="4|16";
      iArray[4][6]="1|0";
      iArray[4][15]="code1";
      iArray[4][17]="15";
      
      iArray[5] = new Array("发生地点(县)","90px","100","2");
      iArray[5][4]="county1";
      iArray[5][5]="5|17";
      iArray[5][6]="1|0";
      iArray[5][15]="code1";
      iArray[5][17]="16";
		
      iArray[6] = new Array("发生地点","150px","100","1")
      
      iArray[7] = new Array("事故类型","80px","0","3");
      iArray[8] = new Array("事故主题","200px","0","3");
      iArray[9] = new Array("医院名称","200px","0","3");

      iArray[10]=new Array("入院日期","80px","100","1");
      iArray[10][9]="入院日期|date";

      iArray[11]=new Array("出院日期","80px","100","1");
      iArray[11][9]="出院日期|date";

      iArray[12] = new Array("事件信息","200px","1000","1");

      iArray[13] = new Array("事件类型","60px","10","2")
      iArray[13][10]="AccType";
      iArray[13][11]="0|^1|疾病|^2|意外"
      iArray[13][12]="13|14"
      iArray[13][13]="1|0"

      iArray[14] = new Array("事件类型","60px","10","3")
      iArray[15] = new Array("发送地点省编码","60px","10","3")
      iArray[16] = new Array("发生地点市编码","60px","10","3")
	  iArray[17] = new Array("发生地点县编码","60px","10","3")  
      
      EventGrid = new MulLineEnter("fm","EventGrid");
      EventGrid.mulLineCount = 0;
      EventGrid.displayTitle = 1;
      EventGrid.locked = 0;
      EventGrid.canChk =1	;
      EventGrid.hiddenPlus=0;
      EventGrid.hiddenSubtraction=1;
      EventGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }
  
  function initAppealGrid(){
    var iArray = new Array();
    try{
      iArray[0] = new Array("序号","30px","10","0");
      iArray[1] = new Array("案件号","100px","100","0");
      iArray[2] = new Array("案件状态","50","100","0");
      iArray[3] = new Array("赔款","40px","100","0");
      iArray[4] = new Array("类型","40px","100","0");
      iArray[5] = new Array("原因","60px","100","0");
      iArray[6] = new Array("备注","200px","100","0");

      AppealGrid = new MulLineEnter("fm","AppealGrid");
      AppealGrid.mulLineCount = 0;
      AppealGrid.displayTitle = 1;
      AppealGrid.locked = 0;
      AppealGrid.canChk = 0;
      AppealGrid.hiddenPlus= 1;
      AppealGrid.hiddenSubtraction= 1;
      AppealGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }

  function initQuery(){

    var tRgtNo = fm.RgtNo .value;
    var tCaseNo = fm.CaseNo.value;
    ClientafterQuery(tRgtNo,tCaseNo);

  }

  function checkRgtFlag(){
    strSQL=" select rgtclass, apppeoples from LLRegister where rgtno = '" + fm.all('RgtNo').value + "'";
    arrResult=easyExecSql(strSQL);
    fm.all('rgtflag').value = '0';
    if(arrResult){
      fm.all('AppNum').value = arrResult[0][1];
      var RgtClass;
      try{RgtClass = arrResult[0][0]} catch(ex) {alert(ex.message + "RgtClass")}
      strSQL1 = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
      arrResult1 = easyExecSql(strSQL1);
      if (RgtClass == '1')
      {
        titleGrp.style.display='';
        inputGrp.style.display='';
        divRgtFinish.style.display='';
        titleAppNum.style.display='';
        inputAppNum.style.display='';
        fm.all('rgtflag').value = "1";
        div1.style.display='none';
        divRegisterInfo.style.display='none';
      }
    }
  }

</script>