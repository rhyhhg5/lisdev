<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";

//获取页面数据

String tsOperate = request.getParameter("operate");
String tsOperateCN = "";
if (tsOperate.equals("INSERT"))
tsOperateCN = "保存";
if (tsOperate.equals("UPDATE"))
tsOperateCN = "修改";
if (tsOperate.equals("DELETE"))
tsOperateCN = "删除";

//初始化
LLFeeMainSchema mFeeMainSchema = new LLFeeMainSchema();
LLSecurityReceiptSchema mLLSecurityReceiptSchema = new LLSecurityReceiptSchema();
LLCaseReceiptSet mReceiptSet = new LLCaseReceiptSet();
LLCaseReceiptSchema mReceiptSchema = null;
LLFeeOtherItemSet mFeeOtherItemSet = new LLFeeOtherItemSet();
LLFeeOtherItemSchema mFeeOtherItemSchema = null;

//账单主信息
String tsMainFeeNo = request.getParameter("MainFeeNo");
String tsCustomerName = StrTool.unicodeToGBK(request.getParameter("CustomerName"));
String tsFeeAtti = request.getParameter("FeeAtti");
String tsMngCom = request.getParameter("MngCom");
mFeeMainSchema.setMainFeeNo(tsMainFeeNo);
mFeeMainSchema.setCaseNo(request.getParameter("CaseNo"));
mFeeMainSchema.setRgtNo(request.getParameter("RgtNo"));
mFeeMainSchema.setCustomerNo(request.getParameter("CustomerNo"));
mFeeMainSchema.setCustomerName(tsCustomerName);
mFeeMainSchema.setCustomerSex(request.getParameter("CustomerSex"));
mFeeMainSchema.setHospitalCode(request.getParameter("HospitalCode"));
mFeeMainSchema.setHospitalName(request.getParameter("HospitalName"));
mFeeMainSchema.setReceiptNo(request.getParameter("ReceiptNo"));
mFeeMainSchema.setFeeType(request.getParameter("FeeType"));
mFeeMainSchema.setFeeAtti(tsFeeAtti);
mFeeMainSchema.setFeeAffixType(request.getParameter("FeeAffixType"));
mFeeMainSchema.setFeeDate(request.getParameter("FeeDate"));
mFeeMainSchema.setRealHospDate(request.getParameter("RealHospDate"));
mFeeMainSchema.setAge(request.getParameter("Age"));
mFeeMainSchema.setSecurityNo(request.getParameter("OtherIDNo"));
mFeeMainSchema.setInsuredStat(request.getParameter("InsuredStat"));
mFeeMainSchema.setMngCom(request.getParameter("MngCom"));

//分案收据明细
if (tsFeeAtti.equals("4")){
   mLLSecurityReceiptSchema.setRgtNo(request.getParameter("RgtNo"));
   mLLSecurityReceiptSchema.setCaseNo(request.getParameter("CaseNo"));
   String tApplyAmnt[] = request.getParameterValues("EasySecuGrid6");
   String tSelfPay1[] = request.getParameterValues("EasySecuGrid7");
   String tSmallDoorPay[] = request.getParameterValues("EasySecuGrid8");
   String tHighDoorAmnt[] = request.getParameterValues("EasySecuGrid9");
   String tSelfAmnt[] = request.getParameterValues("EasySecuGrid10");
  
   String tRadio[] = request.getParameterValues("InpEasySecuGridSel");  

   for (int index=0; index< tRadio.length;index++){
     if(tRadio[index].equals("1")){
       if (tApplyAmnt[index]!= null && !tApplyAmnt[index].equals("")){
           mFeeOtherItemSchema = new LLFeeOtherItemSchema();
           mFeeOtherItemSchema.setItemCode("301");
           mFeeOtherItemSchema.setDrugName("合计金额");
           mFeeOtherItemSchema.setAvliReason("ApplyAmnt");
           mFeeOtherItemSchema.setFeeMoney(tApplyAmnt[index]);
           mFeeOtherItemSchema.setAvliFlag("0");
           mFeeOtherItemSet.add(mFeeOtherItemSchema);
       }
       if (tSelfPay1[index]!= null && !tSelfPay1[index].equals("")){
           mFeeOtherItemSchema = new LLFeeOtherItemSchema();
           mFeeOtherItemSchema.setItemCode("307");
           mFeeOtherItemSchema.setDrugName("自付一");
           mFeeOtherItemSchema.setAvliReason("SelfPay1");
           mFeeOtherItemSchema.setFeeMoney(tSelfPay1[index]);
           mFeeOtherItemSchema.setAvliFlag("0");
           mFeeOtherItemSet.add(mFeeOtherItemSchema);
       }
       if (tSmallDoorPay[index]!= null && !tSmallDoorPay[index].equals("")){
           mFeeOtherItemSchema = new LLFeeOtherItemSchema();
           mFeeOtherItemSchema.setItemCode("314");
           mFeeOtherItemSchema.setDrugName("小额门急诊");
           mFeeOtherItemSchema.setAvliReason("SmallDoorPay");
           mFeeOtherItemSchema.setFeeMoney(tSmallDoorPay[index]);
           mFeeOtherItemSchema.setAvliFlag("0");
           mFeeOtherItemSet.add(mFeeOtherItemSchema);
       }
       if (tHighDoorAmnt[index] != null && !tHighDoorAmnt[index].equals("")){
           mFeeOtherItemSchema = new LLFeeOtherItemSchema();
           mFeeOtherItemSchema.setItemCode("315");
           mFeeOtherItemSchema.setDrugName("大额门急诊");
           mFeeOtherItemSchema.setAvliReason("HighDoorAmnt");
           mFeeOtherItemSchema.setFeeMoney(tHighDoorAmnt[index]);
           mFeeOtherItemSchema.setAvliFlag("0");
           mFeeOtherItemSet.add(mFeeOtherItemSchema);
       }
       if (tSelfAmnt[index] != null && !tSelfAmnt[index].equals("")){
           mFeeOtherItemSchema = new LLFeeOtherItemSchema();
           mFeeOtherItemSchema.setItemCode("305");
           mFeeOtherItemSchema.setDrugName("自费");
           mFeeOtherItemSchema.setAvliReason("SelfAmnt");
           mFeeOtherItemSchema.setFeeMoney(tSelfAmnt[index]);
           mFeeOtherItemSchema.setAvliFlag("0");
           mFeeOtherItemSet.add(mFeeOtherItemSchema);
       }
    }
  }
}


//社保手工报销账单
if (tsFeeAtti.equals("2")){
  String tApplyAmnt[] = request.getParameterValues("SGSecurityGrid6");
  String tHighDoorAmnt[] = request.getParameterValues("SGSecurityGrid7");
  String tSelfPay1[] = request.getParameterValues("SGSecurityGrid8");
  String tSelfPay2[] = request.getParameterValues("SGSecurityGrid9");
  String tSelfAmnt[] = request.getParameterValues("SGSecurityGrid10");
  
  String tRadio[] = request.getParameterValues("InpSGSecurityGridSel");  

  for (int index=0; index< tRadio.length;index++){
    if(tRadio[index].equals("1")){
    	mFeeMainSchema.setSumFee(request.getParameter(tApplyAmnt[index]));
    	mLLSecurityReceiptSchema.setRgtNo(request.getParameter("RgtNo"));
    	mLLSecurityReceiptSchema.setCaseNo(request.getParameter("CaseNo"));
    	mLLSecurityReceiptSchema.setApplyAmnt(tApplyAmnt[index]);
    	mLLSecurityReceiptSchema.setSupDoorFee(tHighDoorAmnt[index]);    	
    	mLLSecurityReceiptSchema.setSelfPay2(tSelfPay2[index]);
    	mLLSecurityReceiptSchema.setSelfAmnt(tSelfAmnt[index]);
      if(request.getParameter("InsuredStat").equals("2")){
        mLLSecurityReceiptSchema.setRetireAddFee(tSelfPay1[index]);
      }else{
        mLLSecurityReceiptSchema.setSelfPay1(tSelfPay1[index]);
      }
    }
  }
}

System.out.println("UI will begin ");

ICaseCureUI tICaseCureUI = new ICaseCureUI();
VData tVData = new VData();
try
{
  // 准备传输数据 VData
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  tVData.add(tGI);
  tVData.add(mFeeMainSchema);
  tVData.add(mReceiptSet);
  tVData.add(mFeeOtherItemSet);
  tVData.add(mLLSecurityReceiptSchema);
  tICaseCureUI.submitData(tVData,tsOperate);
}
catch(Exception ex)
{
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}

if ("".equals(FlagStr))
{
  tError = tICaseCureUI.mErrors;
  if (!tError.needDealError())
  {
    tVData =  tICaseCureUI.getResult();
    mFeeMainSchema = (LLFeeMainSchema) tVData.getObjectByObjectName(
    "LLFeeMainSchema", 0);
    String tipword = "";
    TransferData mTransferData = new TransferData();
    mTransferData=(TransferData) tVData.getObjectByObjectName("TransferData",0);
    if (mTransferData!=null)
    tipword = (String) mTransferData.getValueByName("tipword");
    tsMainFeeNo = mFeeMainSchema.getMainFeeNo();
    System.out.println("aftersubmit " + tipword);
    Content = "账单信息" + tsOperateCN + "成功"+tipword;
    FlagStr = "Succ";
  }
  else
  {
    System.out.println("fail" + tError.getFirstError());
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=tsMainFeeNo%>");
</script>
</html>