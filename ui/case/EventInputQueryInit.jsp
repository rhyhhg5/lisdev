<%
//程序名称：LDPersonQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
String InsuredNo = request.getParameter("InsuredNo");

String Name = StrTool.unicodeToGBK(request.getParameter("Name"));

String CaseNo = request.getParameter("CaseNo");
  
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
	// 将传送过来的被保险人号码填入页面
      fm.InsuredNo.value = '<%=InsuredNo %>';
      fm.Name.value = '<%=Name%>';
      fm.CaseNo.value = '<%=CaseNo%>';
  }
  catch(ex)
  {
    alert("在LDPersonQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LDPersonQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();  
    initEventGrid();
    afterQuery();
  }
  catch(re)
  {
    alert("LDPersonQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initEventGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="事件号";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="发生日期";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="发生地点";
    iArray[3][1]="150px";
    iArray[3][2]=60;
    iArray[3][3]=0;
  



   iArray[4] = new Array("事故类型","80px","0","3");
   iArray[5] = new Array("事故主题","200px","0","3");
   iArray[6] = new Array("医院名称","200px","0","0");
   iArray[7] = new Array("事故描述","200px","0","0");
   
    EventGrid = new MulLineEnter("fm","EventGrid");
    EventGrid.mulLineCount = 0;
    EventGrid.displayTitle = 1;
    EventGrid.locked = 1;
    EventGrid.canSel =0	;

    EventGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    EventGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    EventGrid.loadMulLine(iArray);
    //SubReportGrid.selBoxEventFuncName = "ShowRela";
  }
  catch(ex)
  {
    alter(ex);
  }
}

</script>