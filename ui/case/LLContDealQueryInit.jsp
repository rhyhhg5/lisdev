<%
//Name：LLContDealQueryInit.jsp
//Function：
//Date：2010-08-18 17:44:28
//Author  ：
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
function initForm()
{
  try
  { 
  	initInpBox();
    initSelBox();   
    initEdorListGrid();
    initContDealDetailGrid();
    setWorkTypeData();
	
  }
  catch(re)
  {
    alert("LLContDealQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                               
  }
  catch(ex)
  {
    alert("在LLContDealQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 下拉框的初始化
function initSelBox()
{  
  try                 
  {   
  }
  catch(ex)
  {
    alert("在LLContDealQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
//有效保单信息
 function initEdorListGrid()
 {
    var iArray = new Array();
    try
    {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=3;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="客户号";         		//列名
    iArray[1][1]="50px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21]="CustomerNo";
    
    iArray[2]=new Array();
    iArray[2][0]="业务类型代码";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="EdorType";

    iArray[3]=new Array();
    iArray[3][0]="业务类型";         		//列名
    iArray[3][1]="100px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="EdorName";

    iArray[4]=new Array();
    iArray[4][0]="对应受理号";         		//列名
    iArray[4][1]="100px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="EdorNo";
    
    iArray[5]=new Array();
    iArray[5][0]="收费金额";         		//列名
    iArray[5][1]="60px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="GetMoney";
    
    iArray[6]=new Array();
    iArray[6][0]="退费金额";         		//列名
    iArray[6][1]="60px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="GetMoney2"; 
    
    iArray[7]=new Array();
    iArray[7][0]="受理机构";         		//列名
    iArray[7][1]="100px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]="AcceptCom";
    
    iArray[8]=new Array();
    iArray[8][0]="经办人";         		//列名
    iArray[8][1]="100px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]="Operator";
    
    iArray[9]=new Array();
    iArray[9][0]="受理日期";         		//列名
    iArray[9][1]="70px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[9][21]="";
    
    iArray[10]=new Array();
    iArray[10][0]="结案日期";         		//列名
    iArray[10][1]="70px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[10][21]="";
    
    iArray[11]=new Array();
    iArray[11][0]="增加人数";         		//列名
    iArray[11][1]="60px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[11][21]="";
    
    iArray[12]=new Array();
    iArray[12][0]="减少人数";         		//列名
    iArray[12][1]="60px";            		//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[12][21]="";
    
    iArray[13]=new Array();
    iArray[13][0]="处理状态代码";         		//列名
    iArray[13][1]="35px";            		//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[13][21]="EdorState";
    
    iArray[14]=new Array();
    iArray[14][0]="处理状态";         		//列名
    iArray[14][1]="60px";            		//列宽
    iArray[14][2]=200;            			//列最大值
    iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[14][21]="";
    
    EdorListGrid = new MulLineEnter("fm","EdorListGrid");
    EdorListGrid.mulLineCount = 1;
    EdorListGrid.displayTitle = 1;
    EdorListGrid.locked = 0;
    EdorListGrid.canSel = 1	;
    EdorListGrid.hiddenPlus=1;
    EdorListGrid.hiddenSubtraction=1;
    EdorListGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }
  //理赔合同处理明细
 function initContDealDetailGrid()
 {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","40px","10","0");
      iArray[1]=new Array("理赔号","100px","100","0");
      iArray[2]=new Array("合同处理项目","80px","100","0");
      iArray[3]=new Array("合同处理号","80px","100","0");
      iArray[4] = new Array("操作人代码","60px","0","0");
      iArray[5] = new Array("操作人姓名","80px","0","0");
      iArray[6] = new Array("结案日期","80px","0","0");
      iArray[7]=new Array("受理机构","80px","100","0");
      iArray[8]=new Array("处理状态","60px","100","0");

      ContDealDetailGrid = new MulLineEnter("fm","ContDealDetailGrid");
      ContDealDetailGrid.mulLineCount = 1;
      ContDealDetailGrid.displayTitle = 1;
      ContDealDetailGrid.locked = 0;
      ContDealDetailGrid.canSel =1	;
      ContDealDetailGrid.hiddenPlus=1;
      ContDealDetailGrid.hiddenSubtraction=1;
      ContDealDetailGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }
</script>