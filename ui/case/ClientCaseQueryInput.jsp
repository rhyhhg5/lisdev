<%
//程序名称：CHTMLSpanInput.jsp
//程序功能：功能描述
//创建日期：2005-02-21 09:37:43
//创建人  ：ctrHTML
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="ClientCaseQueryInput.js"></SCRIPT> 
  <%@include file="ClientCaseQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divCHTMLSpanGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      分案号(个人理赔号)
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CaseNo >
    </TD>
    <TD  class= title>
      立案号(申请登记号)
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RgtNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      案件类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RgtType >
    </TD>
    <TD  class= title>
      案件状态
    </TD>
    <TD  class= input>
      <Input class= 'common' name=RgtState >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      出险人客户号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerNo >
    </TD>
    <TD  class= title>
      出险人名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerName >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      出险类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccidentType >
    </TD>
    <TD  class= title>
      收据信息标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ReceiptFlag >
    </TD>
  </TR>
  
  
</table>
  </Div>
  <div id="div1" style="display: 'none'">
		      
		      <TR  class= common>
    <TD  class= title>
      医院信息标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HospitalFlag >
    </TD>
    <TD  class= title>
      调查报告标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SurveyFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      材料齐备日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AffixGetDate >
    </TD>
    <TD  class= title>
      账单录入标记
    </TD>
    <TD  class= input>
      <Input class= 'common' name=FeeInputFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      入院日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InHospitalDate >
    </TD>
    <TD  class= title>
      出院日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=OutHospitalDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      无效住院天数
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InvaliHosDays >
    </TD>
    <TD  class= title>
      实际住院天数
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InHospitalDays >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      确诊日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DianoseDate >
    </TD>
    <TD  class= title>
      联系地址
    </TD>
    <TD  class= input>
      <Input class= 'common' name=PostalAddress >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      联系电话
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Phone >
    </TD>
    <TD  class= title>
      出险开始日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccStartDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      出险日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccidentDate >
    </TD>
    <TD  class= title>
      出险地点
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccidentSite >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      死亡日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DeathDate >
    </TD>
    <TD  class= title>
      出险人生日
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustBirthday >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      出险人性别
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerSex >
    </TD>
    <TD  class= title>
      出险人年龄
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CustomerAge >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      证件类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDType >
    </TD>
    <TD  class= title>
      证件号码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=IDNo >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      审核人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Handler >
    </TD>
    <TD  class= title>
      审核状态
    </TD>
    <TD  class= input>
      <Input class= 'common' name=UWState >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      当前处理人
    </TD>
    <TD  class= input>
      <Input class= 'common' name=Dealer >
    </TD>
    <TD  class= title>
      申诉标志
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AppealFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      赔付金领取方式
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GetMode >
    </TD>
    <TD  class= title>
      赔付金领取间隔
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GetIntv >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      核算标记
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CalFlag >
    </TD>
    <TD  class= title>
      核赔标记
    </TD>
    <TD  class= input>
      <Input class= 'common' name=UWFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      拒赔标记
    </TD>
    <TD  class= input>
      <Input class= 'common' name=DeclineFlag >
    </TD>
    <TD  class= title>
      结案标记
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndCaseFlag >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      结案日期
    </TD>
    <TD  class= input>
      <Input class= 'common' name=EndCaseDate >
    </TD>

  </TR>
		      <TD  class= title>
			      管理机构
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=MngCom >
			    </TD>
		  <TR  class= common>
		    <TD  class= title>
		      操作员
		    </TD>
		    <TD  class= input>
		      <Input class= 'common' name=Operator >
		    </TD>
		    <TD  class= title>
		      入机日期
		    </TD>
		    <TD  class= input>
		      <Input class= 'common' name=MakeDate >
		    </TD>
		  </TR>
		  <TR  class= common>
		    <TD  class= title>
		      入机时间
		    </TD>
		    <TD  class= input>
		      <Input class= 'common' name=MakeTime >
		    </TD>
		    <TD  class= title>
		      最后一次修改日期
		    </TD>
		    <TD  class= input>
		      <Input class= 'common' name=ModifyDate >
		    </TD>
		  </TR>
		  <TR  class= common>
		    <TD  class= title>
		      最后一次修改时间
		    </TD>
		    <TD  class= input>
		      <Input class= 'common' name=ModifyTime >
		    </TD>
		  </TR>
  	</div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT VALUE="查询"   TYPE=button   class=cssbutton onclick="easyQueryClick();">
          <INPUT VALUE="返回" TYPE=button   class=cssbutton onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCHTMLSpan1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divCHTMLSpan1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanClientCaseGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
