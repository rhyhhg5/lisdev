<html>
  <%
  //Name：LLMainAskInput.jsp
  //Function：登记界面的初始化
  //Date：2005-4-23 16:49:22
  //Author：Xx
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@page import = "com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
  <%@page import = "com.sinosoft.lis.vschema.*"%>
  <%@page import = "com.sinosoft.lis.llcase.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String Operator=tG.Operator;
  String Comcode=tG.ManageCom;

  String CurrentDate= PubFun.getCurrentDate();
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);
  String AheadDays="-30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
  %>

  <head>
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLFeeInputList.js"></SCRIPT>
    <%@include file="LLFeeInputListInit.jsp"%>
    <script language="javascript">
      function initDate(){
        fm.RgtDateS.value="<%=afterdate%>";
        fm.RgtDateE.value="<%=CurrentDate%>";
        var usercode="<%=Operator%>";
        var comcode="<%=Comcode%>";
        fm.Operator.value=usercode;
        fm.OrganCode.value=comcode;
        //   fm.OrganCode.value=comcode;
        var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
        var arrResult = easyExecSql(strSQL);
        if(arrResult != null)
        {
          fm.optname.value= arrResult[0][0];
        }
        
        
      }
    </script>
  </head>
  <body  onload="initDate();initForm();">
    <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
          </TD>
          <TD class= titleImg>
            录入人员信箱
          </TD>
        </TR>
      </table>

      <Div  id= "divLLLLMainAskInput1" style= "display: ''">
        <table  class= common>
          <TR  class= common8>
            <TD  class= title8>
              <input type="radio" name="StateRadio"  value="0" onclick="classify()">全部案件
              <input type="radio" name="StateRadio"  value="1" checked onclick="classify()">未录入（受理）
              <input type="radio" name="StateRadio"  value="4" checked onclick="classify()">未录入（扫描）
              <input type="radio" name="StateRadio"  value="2" onclick="classify()">已录入
              <input type="radio" name="StateRadio"  value="3" onclick="classify()">账单回退
            </TD>
          </TR>
        </table>
      </DIV>

      <Div  id= "divLLLLMainAskInput2" style= "display: ''">
        <table  class= common>
          <TR  class= common8>
            <TD class= title>管理机构</TD>
            <TD class= input><Input class= "code8"  name=OrganCode onkeydown="QueryOnKeyDown();" ondblclick="getstr();return showCodeList('comcode',[this], [0],null,str,'1');" onkeyup="getstr();return showCodeListKey('comcode', [this], [0],null,str,'1');" ></TD>
            <TD class= title>录入人员</TD>
            <TD class= input><Input class= "codeno"  name=Operator onkeydown="QueryOnKeyDown();" ondblclick="return showCodeList('llinputer',[this, optname], [0, 1]);" onkeyup="return showCodeListKey('llinputer', [this, optname], [0, 1]);" ><Input class=codename readonly name=optname></TD>
            <TD class= title>团体批次号</TD>
            <TD class= input><Input class=common name=RgtNo onkeydown="QueryOnKeyDown()"></TD>
          </TR>
          <TR  class= common>
          	<TD class= title>理赔号码</TD>
          	<TD class= input><Input class=common8 name=CaseNo onkeydown="QueryOnKeyDown()"></TD>
            <TD class= title>客户号</TD>
            <TD class= input><Input class=common name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>
            <TD class= title>客户名称</TD>
            <TD class= input><Input class=common name=CustomerName onkeydown="QueryOnKeyDown()"></TD>
          </TR>
          <TR  class= common>
          	<TD  class= title8>受理日期从</TD>
          	<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
          	<TD  class= title8>到</TD>
          	<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
          	<TD  class= title>险种类型</TD>
            <TD  class= input> <input class="codeno" CodeData="0|2^1|个险^2|团险"  verify="险种类型|notnull" elementtype=nacessary name=ContType ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeydown="QueryOnKeyDown();" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);"><input class=codename name=ContTypeName></TD> 
     
	        </TR>
        </table>
      </Div>
      <!--  <input style="display:''"  class=cssButton type=button value="查 询" onclick="easyQuery()"> -->
      <Div  id= "divCustomerInfo" style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanCheckGrid" >
              </span>
            </TD>
          </TR>
        </table>

   <Div id="divPage1" align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
	</Div>   
        <!--新增加的下拉框-->
        <Div  id= "divLLLLMainAskInput2" style= "display: ''">
          <table  class= common>
            <TR  class= common>
            <input style="display:''"  class=cssButton type=button value="录 入" onclick="DealFeeInput()">        </TR>
          </table>
        </Div>

      </Div>
      <hr>
      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQuantity);">
          </TD>
          <TD class= titleImg>
            录入人员工作量
          </TD>
        </TR>
      </table>
      <div id= "divQuantity" style= "display: ''">
        <table  class= common>
          <TR  class= common>
          	<TD class= title>已录入案件数</TD>
          	<TD class= input><Input class=readonly readonly name=CompleteCase ></TD>
            <TD class= title>已录入账单数</TD>
            <TD class= input><Input class=readonly readonly name=CompleteReceipt ></TD>
            <TD class= title>未录入案件数</TD>
            <TD class= input><Input class=readonly readonly name=ReadyCase ></TD>
          </TR>
        </table>
      </div>
      <!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
      <!--隐藏域-->
      <Input type=hidden class= common name="fmtransact" >
      <Input type=hidden name=AllOperator>
      <input type=hidden name=sql>
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>
