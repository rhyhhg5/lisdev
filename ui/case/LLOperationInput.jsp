<%
//Name：LLOperationInput.jsp
//Function：赔付结论运维 
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<head >
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="./CaseCommon.js"></SCRIPT>
		<SCRIPT src="LLOperationInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLOperationInputInit.jsp"%>
	</head>

	<body  onload="initForm();" >
		<form action="./LLOperationSave.jsp" method=post name=fm target="fraSubmit">
		<%@include file="LLOperationTitle.jsp" %>
		<table  class= common>
		    <TR  class= common8>
		      <TD  class= title8>客户号</TD><TD  class= input8><Input class= readonly name="CustomerNo" readonly></TD>
		      <TD  class= title8>客户姓名</TD><TD  class= input8><Input class= readonly name="CustomerName" readonly></TD>
		      <TD  class= title8>客户性别</TD><TD  class= input8><Input class= readonly name="Sex" readonly></TD>
		    </TR>
		</table>
		<table  class= common>
		    <TR  class= common8>
		      <TD  class= title8>赔付结论查询</TD><TD  class= input8><Input class= readonly name="GiveType1" readonly><Input class=readonly  name=GiveTypeDesc1 readonly></TD>
		      <TD  class= title8>赔付结论修改</TD><TD  class= input8><Input class= codeno name="GiveType2" ondblclick="return showCodeList('llclaimdecision',[this,GiveTypeDesc2],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('llclaimdecision',[this,GiveTypeDesc2],[0,1],null,null,null,1);" readonly><Input class=codename  name=GiveTypeDesc2 readonly></TD>
		      <TD  class= title8><input name=ClmNo style="display:none"></TD>
		    </TR>
		</table>
		<table>
			<tr>
				<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divDetailGrid);">
				</td>
				<td class= titleImg> 险种明细查询</td>
			</tr>
		</table>
		<Div  id= "divDetailGrid" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanOperationRisk1Grid" >
						</span>
					</td>
				</tr>
			</table>
		</Div>
		<table>
			<tr>
				<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divDetailGrid);">
				</td>
				<td class= titleImg> 险种明细修改</td>
			</tr>
		</table>
		<Div  id= "divDetailGrid" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanOperationRisk2Grid" >
						</span>
					</td>
				</tr>
			</table>
		</Div>
		<table>
			<tr>
				<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divDetailGrid);">
				</td>
				<td class= titleImg> 保单责任明细查询</td>
			</tr>
		</table>
		<Div  id= "divDetailGrid" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanOperationOneGrid" >
						</span>
					</td>
				</tr>
			</table>
		</Div>
		<table>
			<tr>
				<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divDetailGrid);">
				</td>
				<td class= titleImg> 保单责任明细修改</td>
			</tr>
		</table>
		<Div  id= "divDetailGrid" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<td text-align: left colSpan=1>
						<span id="spanOperationTwoGrid" >
						</span>
					</td>
				</tr>
			</table>
		</Div>
		<div id=divconfirm>
			<input class=CssButton style='width: 100px;' type=button value="保 存" name="SubCalculate" onclick="submitForm();" id = "sub3">
		</div>
		<Input type=hidden name="Opt">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
