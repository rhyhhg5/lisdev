<%
/*******************************************************************************
 * Name     :CaseAccidentSave.jsp
 * Function :事故/伤残信息的保存程序
 * Authorm  :LiuYansong
 * Date     :2003-7-16
 */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.llcase.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //String Type = request.getParameter("AccitentType");//判断是意外还是疾病；
  //System.out.println("类型是2003-7-28"+Type);
  String DisplayFlag = request.getParameter("DisplayFlag");//判断是在立案中进入的还是在理算中进入的；（0立案；1理算）
  
  LLCaseInfoSet tLLCaseInfoSet=new LLCaseInfoSet();
  LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
  LLOperationSet tLLOperationSet = new LLOperationSet();
  LLOtherFactorSet tLLOtherFactorSet = new LLOtherFactorSet();
  LLAccidentSet tLLAccidentSet = new LLAccidentSet();
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  CaseInfoUI tCaseInfoUI   = new CaseInfoUI();
  LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
  mLLCaseInfoSchema.setCaseNo(request.getParameter("CaseNo"));

  CErrors tError = null;
  String transact = "INSERT";
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String strCaseNo=request.getParameter("CaseNo");
  String CaseRelaNo = request.getParameter("CaseRelaNo");
  String RgtNo = request.getParameter("RgtNo");
  tLLCaseSchema.setCaseNo(strCaseNo);
  tLLCaseSchema.setRgtNo(RgtNo);
    
    //疾病信息
  	{ 
  	 
	  String[] Dianose=request.getParameterValues("DiseaseGrid1");
	  String[] DiseaseName=request.getParameterValues("DiseaseGrid3");
	  System.out.println("疾病名称 : "+DiseaseName[0]);
	  String[] DiseaseCode=request.getParameterValues("DiseaseGrid4");
	  System.out.println("疾病代码 : "+DiseaseCode[0]);
	  String[] HospitalCode=request.getParameterValues("DiseaseGrid5");
	  System.out.println("医院代码 : "+HospitalCode[0]);
	  String[] HospitalName=request.getParameterValues("DiseaseGrid6");
	  System.out.println("医院名称 : "+HospitalCode[0]);
	  String[] DoctorName= request.getParameterValues("DiseaseGrid7");
	  System.out.println("疾病名称 : "+HospitalCode[0]);
	  String[] DoctorNo= request.getParameterValues("DiseaseGrid9");
	  System.out.println("疾病名称 : "+HospitalCode[0]);
	 
	 int intLength =0;
     if (DiseaseName!=null )
	 { 
	   	 intLength = DiseaseName.length ;		
		 for(int i=0;i<intLength;i++) {
        if ( DiseaseName[i].length()<=0)
        {
          System.out.println(">>>>>>>>>>>>");
         continue ;
         }
		 LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
		
		 tLLCaseCureSchema.setCaseNo(strCaseNo);
		 tLLCaseCureSchema.setCaseRelaNo(CaseRelaNo);
		 tLLCaseCureSchema.setDiseaseName(DiseaseName[i]);
		 tLLCaseCureSchema.setDiseaseCode(DiseaseCode[i]);
		 tLLCaseCureSchema.setDiagnose(Dianose[i]);
		 tLLCaseCureSchema.setHospitalCode(HospitalCode[i]);
		 tLLCaseCureSchema.setHospitalName(HospitalName[i]);
		 tLLCaseCureSchema.setSeriousFlag("0"); //一般疾病
		 tLLCaseCureSchema.setDoctorName( DoctorName[i] );
		 tLLCaseCureSchema.setDoctorNo( DoctorNo[i] );
		 
		 tLLCaseCureSet.add(tLLCaseCureSchema);
		  }
	 }
	 }
	 
	 
	  //重疾疾病信息
	 {
	
	  String[] DiseaseName=request.getParameterValues("SeriousDiseaseGrid1");
	  String[] DiseaseCode=request.getParameterValues("SeriousDiseaseGrid2");
	  String[] HospitalCode=request.getParameterValues("SeriousDiseaseGrid4");
	  String[] HospitalName=request.getParameterValues("SeriousDiseaseGrid3");
	  String[] DoctorName= request.getParameterValues("SeriousDiseaseGrid5");
	  String[] SeriousWard= request.getParameterValues("SeriousDiseaseGrid7");
	  String[] DoctorNo= request.getParameterValues("SeriousDiseaseGrid8");
	  
	  int intLength =0;
     if (DiseaseName!=null )
	 { 
	   	 intLength = DiseaseName.length ;		
		 for(int i=0;i<intLength;i++) {
         
        if ( DiseaseName[i].length()<=0)
        {
          System.out.println("<<<<<<<<:"+CaseRelaNo);
         continue ;
         } 
		 LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
		 tLLCaseCureSchema.setCaseNo(strCaseNo);
		 tLLCaseCureSchema.setCaseRelaNo(CaseRelaNo);
		 tLLCaseCureSchema.setDiseaseName(DiseaseName[i]);
		 tLLCaseCureSchema.setDiseaseCode(DiseaseCode[i]);
		 //tLLCaseCureSchema.setDianose(Dianose[i]);
		 tLLCaseCureSchema.setHospitalCode(HospitalCode[i]);
		 tLLCaseCureSchema.setHospitalName(HospitalName[i]);
		 tLLCaseCureSchema.setDoctorName(DoctorName[i]);
		 tLLCaseCureSchema.setSeriousFlag("1"); //重疾疾病
		 //tLLCaseCureSchema.setSeriousWard(SeriousWard[i]); //一般疾病
		 tLLCaseCureSchema.setDoctorNo( DoctorNo[i] );

		
		 if ( i==0 ) 
		 	tLLCaseCureSchema.setMainDiseaseFlag("1");//主要疾病
		  else
		   	tLLCaseCureSchema.setMainDiseaseFlag("0");//主要疾病
		   	
		 tLLCaseCureSet.add(tLLCaseCureSchema);
		  }
	 }   
  }
  
   //残疾信息
	 {
	  String[] Name=request.getParameterValues("DeformityGrid1");
	  String[] Code=request.getParameterValues("DeformityGrid2");
	  String[] DeformityGrade=request.getParameterValues("DeformityGrid3");
	  String[] DiagnoseDesc=request.getParameterValues("DeformityGrid4");
	  String[] JudgeDate=request.getParameterValues("DeformityGrid5");
	  String[] JudgeOrganName=request.getParameterValues("DeformityGrid6");
	  String[] JudgeOrgan=request.getParameterValues("DeformityGrid7");
	
	  int intLength =0;
     if (Code!=null )
	 { 
	   	 intLength = Code.length ;		
		 for(int i=0;i<intLength;i++) {

		 LLCaseInfoSchema tLLCaseInfoSchema = new LLCaseInfoSchema();
		 
		  if ( Code[i].length()<=0)
        {
          System.out.println("<<<<<<<<");
         continue ;
         } 
         
		 tLLCaseInfoSchema.setCaseNo(strCaseNo);
		 tLLCaseInfoSchema.setCaseRelaNo(CaseRelaNo);
		 tLLCaseInfoSchema.setCode(Code[i]);
		 tLLCaseInfoSchema.setName(Name[i]);
		 //tLLCaseInfoSchema.setDianose(Dianose[i]);
		 tLLCaseInfoSchema.setDeformityGrade(DeformityGrade[i]);
		 tLLCaseInfoSchema.setDiagnoseDesc(DiagnoseDesc[i]);
		 tLLCaseInfoSchema.setJudgeDate(JudgeDate[i]);
		 tLLCaseInfoSchema.setJudgeOrganName(JudgeOrganName[i]);
		 tLLCaseInfoSchema.setJudgeOrgan(JudgeOrgan[i]);
		 tLLCaseInfoSet.add(tLLCaseInfoSchema);
		 }
	 }   
  }
  
  //手术信息
	 {
	
	  String[] OperationCode=request.getParameterValues("DegreeOperationGrid2");
	  String[] OperationName=request.getParameterValues("DegreeOperationGrid1");
	  String[] OpLevel=request.getParameterValues("DegreeOperationGrid4");
	 
	
	  int intLength =0;
     if (OperationCode!=null )
	 { 
	   	 intLength = OperationCode.length ;		
		 for(int i=0;i<intLength;i++) 
		 {
		 	if ( OperationCode[i].length()<=0)
        	{
         		 System.out.println("<<<<<<<<");
         		continue ;
         	}
         	LLOperationSchema tLLOperationSchema = new LLOperationSchema();
		 
		 tLLOperationSchema.setCaseNo(strCaseNo);
		 tLLOperationSchema.setCaseRelaNo(CaseRelaNo);
		 tLLOperationSchema.setOperationCode(OperationCode[i]);
		 tLLOperationSchema.setOperationName(OperationName[i]);
		 tLLOperationSchema.setOpLevel(OpLevel[i]);
		
		 tLLOperationSet.add(tLLOperationSchema);
		}
	 }   
  }
  
  //其他录入要素
	 {
	  String[] num=request.getParameterValues("OtherFactorGridNo");
	  String[] FactorType=request.getParameterValues("OtherFactorGrid6");
	  String[] FactorCode=request.getParameterValues("OtherFactorGrid2");
	  String[] FactorName=request.getParameterValues("OtherFactorGrid3");
	  String[] Value=request.getParameterValues("OtherFactorGrid4");
	  String[] Remark=request.getParameterValues("OtherFactorGrid5");
	
	  int intLength =0;
     if (FactorType!=null )
	 { 
	   	 intLength = FactorType.length ;		
		 for(int i=0;i<intLength;i++) 
		 {
		 	if ( FactorType[i].length()<=0)
        	{
         		 System.out.println("<<<<<<<<");
         		continue ;
         	}
         	LLOtherFactorSchema tLLOtherFactorSchema = new LLOtherFactorSchema();
		 
		 tLLOtherFactorSchema.setFactorType(FactorType[i]);
		 tLLOtherFactorSchema.setFactorCode(FactorType[i]);
		 tLLOtherFactorSchema.setCaseNo(request.getParameter("CaseNo"));
		 tLLOtherFactorSchema.setCaseRelaNo(CaseRelaNo);
		 tLLOtherFactorSchema.setSubRptNo("");
		 tLLOtherFactorSchema.setValue(Value[i]);
		 tLLOtherFactorSchema.setFactorName(FactorName[i]);
		 tLLOtherFactorSchema.setRemark(Remark[i]);
	
		 tLLOtherFactorSet.add(tLLOtherFactorSchema);
		}
	 }   
  }
  //意外信息
	 {
	  String[] num=request.getParameterValues("AccidentGridNo");
	  String[] AccidentNo=request.getParameterValues("AccidentGrid1");
	  String[] Type=request.getParameterValues("AccidentGrid2");
	  String[] TypeName=request.getParameterValues("AccidentGrid3");
	  String[] Code=request.getParameterValues("AccidentGrid5");
	  String[] Name=request.getParameterValues("AccidentGrid6");
	  String[] ReasonCode=request.getParameterValues("AccidentGrid7");
	  String[] Reason=request.getParameterValues("AccidentGrid8");
	  String[] Remark=request.getParameterValues("AccidentGrid9");
	
	  int intLength =0;
     if (AccidentNo!=null )
	 { 
	   	 intLength = AccidentNo.length ;		
		 for(int i=0;i<intLength;i++) 
		 {
		 	if ( Name[i].length()<=0)
        	{
         		 System.out.println("<<<<<<<<");
         		continue ;
         	}
         	 LLAccidentSchema tLLAccidentSchema = new LLAccidentSchema();
		 
		 //tLLAccidentSchema.setAccidentNo("3"); 	//以后添加
		 tLLAccidentSchema.setType(Type[i]);
		 
		 tLLAccidentSchema.setCaseNo(request.getParameter("CaseNo"));
		 tLLAccidentSchema.setCaseRelaNo(CaseRelaNo);
		 tLLAccidentSchema.setTypeName(TypeName[i]);
		
		 tLLAccidentSchema.setCode(Code[i]);
		 tLLAccidentSchema.setName(Name[i]);
		 
		 tLLAccidentSchema.setReasonCode(ReasonCode[i]);
		 tLLAccidentSchema.setReason(Reason[i]);
		 tLLAccidentSchema.setRemark(Remark[i]);
		 tLLAccidentSet.add(tLLAccidentSchema);
		}
	 }   
  }
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  try
  {
    VData tVData = new VData();
    tVData.addElement(tG);
    tVData.addElement(CaseRelaNo);
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tLLCaseCureSet);
    tVData.addElement(tLLCaseInfoSet);
    tVData.addElement(tLLOtherFactorSet);
    tVData.addElement(tLLOperationSet);
    tVData.addElement(tLLAccidentSet);
    tCaseInfoUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCaseInfoUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>