<%
//程序名称：ProposalQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//返回按钮初始化
var str = "";
function initDisplayButton()
{
	tDisplay = <%=tDisplay%>;
	//alert(tDisplay);
	if (tDisplay=="1"||tDisplay=="2")
	{
		fm.Return.style.display='';
	}
	else if (tDisplay=="0")
	{
		fm.Return.style.display='none';
	}
}
function initQuery()
{
    try
    {
        //alert("asdfsdaf"+top.opener.fm.all('ContNo'));
        var tContNo = top.opener.fm.all('ContNo').value;
	    //alert(tContNo);
	    if (tContNo!=""&&tContNo!=null)
	    {
	    	fm.all('ContNo').value = tContNo; 
	    	easyQueryClick();
	    }
	 }
	 catch(ex)
	 {
	 }
}
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('PrtNo').value = '';
    fm.all('ContNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('GrpContNo').value = '';
    fm.all('ProposalNo').value = '';
    fm.all('AppntName').value = '';
    
  }
  catch(ex)
  {
    alert("在AllProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initQuery();   
		initPolGrid();
		initContGrid();
		initClaimGrid();
		initDisplayButton();
		// easyQueryClick();
		
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 团单客户信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="团体客户号";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[2]=new Array();
      iArray[2][0]="投保单位";         		//列名
      iArray[2][1]="200px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      
      iArray[3]=new Array();
      iArray[3][0]="联系人";         		//列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[4]=new Array();
      iArray[4][0]="电话";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();                                                 
      iArray[5][0]="地址";         		//列名                                 
      iArray[5][1]="180px";            		//列宽                             
      iArray[5][2]=200;            			//列最大值                           
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 2;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;
      PolGrid.loadMulLine(iArray); 
      
      PolGrid. selBoxEventFuncName ="showGrid1"
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initContGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号码";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[2]=new Array();
      iArray[2][0]="参保单位";         		//列名
      iArray[2][1]="200px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
      
      iArray[3]=new Array();
      iArray[3][0]="客户号";         		//列名
      iArray[3][1]="0px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[4]=new Array();
      iArray[4][0]="投保人数";         		//列名
      iArray[4][1]="40px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();                                                 
      iArray[5][0]="总保费";         		//列名                               
      iArray[5][1]="40px";            		//列宽                             
      iArray[5][2]=200;            			//列最大值                           
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();                                                  
      iArray[6][0]="投保时间";         		//列名                                
      iArray[6][1]="55px";            		//列宽                              
      iArray[6][2]=200;            			//列最大值                            
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
      
      iArray[7]=new Array();                                                  
      iArray[7][0]="生效时间";         		//列名                                
      iArray[7][1]="55px";            		//列宽                              
      iArray[7][2]=200;            			//列最大值                            
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[8]=new Array();                                                  
      iArray[8][0]="失效时间";         		//列名                                
      iArray[8][1]="55px";            		//列宽                              
      iArray[8][2]=200;            			//列最大值                            
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[9]=new Array();                                                 
      iArray[9][0]="状态";         		//列名                             
      iArray[9][1]="25px";            		//列宽                             
      iArray[9][2]=200;            			//列最大值                           
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
      //这些属性必须在loadMulLine前
      ContGrid.mulLineCount = 0;   
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      ContGrid.canSel = 1;
      ContGrid.hiddenPlus = 1;
      ContGrid.hiddenSubtraction = 1;
      ContGrid.loadMulLine(iArray); 
      
      ContGrid. selBoxEventFuncName ="showGrid2"
      //这些操作必须在loadMulLine后面
      //ContGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


function initClaimGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

                                                                                   
      iArray[1]=new Array();                                                       
      iArray[1][0]="人数";         		//列名                                       
      iArray[1][1]="50px";            		//列宽                                   
      iArray[1][2]=100;            			//列最大值                                 
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
                                                                                   
      iArray[2]=new Array();
      iArray[2][0]="件数";         		//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="理赔金额(元)";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[4]=new Array();
      iArray[4][0]="件均赔额(元)";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();                                                 
      iArray[5][0]="保单赔付率(%)";         		//列名                               
      iArray[5][1]="70px";            		//列宽                             
      iArray[5][2]=200;            			//列最大值                           
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
//      iArray[6]=new Array();                                                  
//      iArray[6][0]="客户姓名";         		//列名                                
//      iArray[6][1]="50px";            		//列宽                              
//      iArray[6][2]=200;            			//列最大值                            
//      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
//      
//      iArray[7]=new Array();                                                  
//      iArray[7][0]="受理日期";         		//列名                                
//      iArray[7][1]="70px";            		//列宽                              
//      iArray[7][2]=200;            			//列最大值                            
//      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
//      
//      iArray[8]=new Array();                                                  
//      iArray[8][0]="结案日期";         		//列名                                
//      iArray[8][1]="70px";            		//列宽                              
//      iArray[8][2]=200;            			//列最大值                            
//      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
//      
//      iArray[9]=new Array();                                                 
//      iArray[9][0]="处理人";         		//列名                             
//      iArray[9][1]="40px";            		//列宽                             
//      iArray[9][2]=200;            			//列最大值                           
//      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[10]=new Array();                                                 
//      iArray[10][0]="案件状态";         		//列名                             
//      iArray[10][1]="50px";            		//列宽                             
//      iArray[10][2]=200;            			//列最大值                           
//      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
//      
//      iArray[11]=new Array();                                                 
//      iArray[11][0]="给付结论";         		//列名                             
//      iArray[11][1]="50px";            		//列宽                             
//      iArray[11][2]=200;            			//列最大值                           
//      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      ClaimGrid = new MulLineEnter( "fm" , "ClaimGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimGrid.mulLineCount = 0;   
      ClaimGrid.displayTitle = 1;
      ClaimGrid.locked = 1;
      ClaimGrid.canSel = 0;
      ClaimGrid.hiddenPlus = 1;
      ClaimGrid.hiddenSubtraction = 1;
      ClaimGrid.loadMulLine(iArray); 
      
//      ClaimGrid. selBoxEventFuncName ="showGrid2"
      //这些操作必须在loadMulLine后面
      //ClaimGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>