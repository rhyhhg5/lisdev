//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPersonGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
//    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

/*
function display(indexNum) 
{
//得到被选中的个人信息字符串
 str=fm.all("personInformation"+indexNum).value;
 arrRecord = str.split("|");  //拆分字符串，形成返回的数组
// alert(str);

 //前面只是得到部分字段的值，待完善，其他字段暂时赋空值
    top.opener.fm.all('CustomerNo').value=arrRecord[0];
    top.opener.fm.all('Password').value = arrRecord[1];    
    top.opener.fm.all('Name').value=arrRecord[2];
    top.opener.fm.all('Sex').value = arrRecord[3];
    top.opener.fm.all('Birthday').value = arrRecord[4];
    top.opener.fm.all('NativePlace').value = arrRecord[5];  
    top.opener.fm.all('Nationality').value = arrRecord[6];
    top.opener.fm.all('Marriage').value = arrRecord[7];
    top.opener.fm.all('MarriageDate').value = arrRecord[8];
    top.opener.fm.all('OccupationType').value = arrRecord[9];
    top.opener.fm.all('StartWorkDate').value = arrRecord[10];
    top.opener.fm.all('Salary').value = arrRecord[11];       //double    
    top.opener.fm.all('Health').value = arrRecord[12];
    top.opener.fm.all('Stature').value = arrRecord[13];      //double 
    top.opener.fm.all('Avoirdupois').value = arrRecord[14];   //double
    
    top.opener.fm.all('CreditGrade').value = arrRecord[15];
    top.opener.fm.all('IDType').value = arrRecord[16];
    top.opener.fm.all('Proterty').value = arrRecord[17];
    top.opener.fm.all('IDNo').value = arrRecord[18];
    top.opener.fm.all('OthIDType').value = arrRecord[19];
    top.opener.fm.all('OthIDNo').value = arrRecord[20];
    top.opener.fm.all('ICNo').value = arrRecord[21];
    top.opener.fm.all('HomeAddressCode').value = arrRecord[22];
    top.opener.fm.all('HomeAddress').value = arrRecord[23];
    top.opener.fm.all('PostalAddress').value = arrRecord[24];
    top.opener.fm.all('ZipCode').value = arrRecord[25];
    top.opener.fm.all('Phone').value = arrRecord[26];
    top.opener.fm.all('BP').value = arrRecord[27];
    top.opener.fm.all('Mobile').value = arrRecord[28];
    top.opener.fm.all('EMail').value = arrRecord[29];
    top.opener.fm.all('BankCode').value = arrRecord[30];
    top.opener.fm.all('BankAccNo').value = arrRecord[31];
    top.opener.fm.all('JoinCompanyDate').value = arrRecord[32];
    top.opener.fm.all('Position').value = arrRecord[33];
    top.opener.fm.all('GrpNo').value = arrRecord[34];
    top.opener.fm.all('GrpName').value = arrRecord[35];
    top.opener.fm.all('GrpPhone').value = arrRecord[36];
    top.opener.fm.all('GrpAddressCode').value = arrRecord[37];
    top.opener.fm.all('GrpAddress').value = arrRecord[38];
    top.opener.fm.all('DeathDate').value = arrRecord[39];
    top.opener.fm.all('Remark').value = arrRecord[40];
    top.opener.fm.all('State').value = arrRecord[41];
    top.opener.fm.all('BlacklistFlag').value = arrRecord[42];
    top.opener.fm.all('Operator').value = arrRecord[43];
 
}
*/


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPersonGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select * from LDPerson where 1=1 "
				 + getWherePart( 'CustomerNo' )
				 + getWherePart( 'Name' )
				 + getWherePart( 'Birthday' )
				 + getWherePart( 'IDType' )
				 + getWherePart( 'IDNo' );
//alert(strSQL);
	execEasyQuery( strSQL );
}

//选择页面上查询的字段对应于"select *"中的位置
function getSelArray()
{
	var arrSel = new Array();
	
	arrSel[0] = 0;
	arrSel[1] = 2;
	arrSel[2] = 3;
	arrSel[3] = 4;
	arrSel[4] = 16;
	arrSel[5] = 18;

	return arrSel;
}

function displayEasyResult( arrQueryResult )
{
	var i, j, m, n;
	var arrSelected = new Array();
	var arrResult = new Array();

	if( arrQueryResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPersonGrid();
		PersonGrid.recordNo = (currPageIndex - 1) * MAXSCREENLINES;
		PersonGrid.loadMulLine(PersonGrid.arraySave);

		arrGrid = arrQueryResult;
		// 转换选出的数组
		arrSelected = getSelArray();
		arrResult = chooseArray( arrQueryResult, arrSelected );
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PersonGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//PersonGrid.delBlankLine();
	} // end of if
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PersonGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterQuery( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PersonGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	return arrSelected;
}

