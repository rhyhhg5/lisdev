<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>



<%
  LLCasePrescriptionSchema tLLCasePrescriptionSchema   = new LLCasePrescriptionSchema();
  LLCasePrescriptionSet tLLCasePrescriptionSet=new LLCasePrescriptionSet();
  //LLFeeMainSchema aLLFeeMainSchema = new LLFeeMainSchema();
  //LLCaseChargeDetailBL tLLCaseChargeDetailBL   = new LLCaseChargeDetailBL();
  LLCasePrescriptionBL tLLCasePrescriptionBL = new LLCasePrescriptionBL();
  
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "INSERT";

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
	//读取立案明细信息
  

  String strCaseNo=request.getParameter("CaseNo");
  String strRgtNo=request.getParameter("RgtNo");
  //String strMainFeeNo=request.getParameter("MainFeeNo");
  System.out.println("分案号======="+strCaseNo);
  
  String[] strNumber=request.getParameterValues("LLCasePrescriptionGridNo");
  
  
  String strDiagnoseType = request.getParameter("DiagnoseType");
  String[] strDrugName=request.getParameterValues("LLCasePrescriptionGrid1");
  String[] strDrugQuantity=request.getParameterValues("LLCasePrescriptionGrid2");
  String[] strTimesPerDay=request.getParameterValues("LLCasePrescriptionGrid3");
  String[] strQuaPerTime=request.getParameterValues("LLCasePrescriptionGrid4");
  
  //String[] strRemark=request.getParameterValues("CaseDrugGrid9");
  
  
  int intLength=0;
  if(strNumber!=null){
  	    intLength +=strNumber.length;
  		System.out.println("数据容量："+intLength);
  }

  for(int i=0;i<intLength;i++)
  {
	  System.out.println("strDrugName:"+strDrugName);
    if(!strDrugName.equals("")){
		
   	  tLLCasePrescriptionSchema=new LLCasePrescriptionSchema();
   	  tLLCasePrescriptionSchema.setCaseNo(strCaseNo);
   	  tLLCasePrescriptionSchema.setRgtNo(strRgtNo);
   	  tLLCasePrescriptionSchema.setDiagnoseType(strDiagnoseType);
   	  tLLCasePrescriptionSchema.setDrugName(strDrugName[i]);
   	  tLLCasePrescriptionSchema.setDrugQuantity(Double.parseDouble(strDrugQuantity[i]));
   	  tLLCasePrescriptionSchema.setTimesPerDay(Integer.parseInt(strTimesPerDay[i]));
   	  tLLCasePrescriptionSchema.setQuaPerTime(Double.parseDouble(strQuaPerTime[i]));
  	  
  	  
  	  //tLLCaseChargeDetailSchema.setRemark(strRemark[i]);
  	  
      tLLCasePrescriptionSet.add(tLLCasePrescriptionSchema);
    }
  }
  System.out.println("从save页面传到BL之前intLength："+intLength);
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLLCasePrescriptionSet);
   tVData.addElement(tG);
   tVData.addElement(strCaseNo);
   tLLCasePrescriptionBL.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("3");
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLLCasePrescriptionBL.mErrors;
    if (!tError.needDealError())
    {
        Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

