<%
//Name：CaseAffixListInit.jsp
//Function：申请材料选择的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%String CaseNo= request.getParameter("CaseNo");

String RgtNo= request.getParameter("RgtNo");
String LoadFlag= request.getParameter("LoadFlag");
System.out.println(LoadFlag);
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
	var turnPage = new turnPageClass();
	function initForm(){
		try{
			initAffixGrid();
			initAddAffixGrid();
			fm.RgtNo.value="<%=RgtNo%>";
			fm.LoadFlag.value="<%=LoadFlag%>"
		}
		catch(re){
			alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
		initList();
	}
	//立案附件信息
	function initAffixGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array("序号","30px","0",1);
			iArray[1]=new Array("材料类型","120px","0");

			iArray[2]=new Array();
			iArray[2][0]="材料代码";
			iArray[2][1]="50px";
			iArray[2][2]=10;
			iArray[2][3]=3;

			iArray[3]=new Array("材料名称","180px","20",0);
			iArray[4]=new Array("提供日期","100px","20",1);
			iArray[5]=new Array("材料属性","60px","1",3);
			iArray[6]=new Array("份数","60px","4",1);
			iArray[7]=new Array("缺少件数","60px","4",1);
			iArray[8]=new Array("材料号码","60px","0",3);
			iArray[9]=new Array("SerialNo","60px","0",3);
			iArray[10]=new Array("材料类型代码","60px","0",3);

			AffixGrid = new MulLineEnter( "fm" , "AffixGrid" );
			//这些属性必须在loadMulLine前
			AffixGrid.mulLineCount = 5;
			AffixGrid.displayTitle = 1;
			AffixGrid.canChk = 1;
			AffixGrid.hiddenPlus=1;
			AffixGrid.hiddenSubtraction=1;
			AffixGrid.loadMulLine(iArray);
		}
		catch(ex){
			alert(ex);
		}
	}

	//立案附件信息
	function initAddAffixGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array("序号","30px","0",1);
			iArray[1]=new Array("材料类型","140px","10","2");
			iArray[1][4]="llmaffixtype"
			iArray[1][5]="1|10";              	                //引用代码对应第几列，'|'为分割符
			iArray[1][6]="1|0";

			iArray[2]=new Array();
			iArray[2][0]="材料代码";
			iArray[2][1]="50px";    
			iArray[2][2]=10;        
			iArray[2][3]=3;    
			     
			iArray[3]=new Array("材料名称","180px","20",2);
			iArray[3][4]="llmaffix"
			iArray[3][5]="3|2|1|10";
			iArray[3][6]="1|0|3|2";
			iArray[3][15]="AffixTypecode";
			iArray[3][17]="10";
			
			iArray[4]=new Array("提供日期","100px","20",1);
			iArray[5]=new Array("材料属性","60px","1",3);
			iArray[6]=new Array("份数","60px","4",1);
			iArray[7]=new Array("缺少件数","60px","4",1);
			iArray[8]=new Array("材料号码","60px","0",3);
			iArray[9]=new Array("SerialNo","60px","0",3);
			iArray[10]=new Array("材料类型代码","60px","0",3);

			AddAffixGrid = new MulLineEnter( "fm" , "AddAffixGrid" );
			//这些属性必须在loadMulLine前
			AddAffixGrid.mulLineCount = 0;
			AddAffixGrid.displayTitle = 1;
			AddAffixGrid.canChk = 0;
			AddAffixGrid.hiddenPlus=0;
			AddAffixGrid.hiddenSubtraction=0;
			AddAffixGrid.loadMulLine(iArray);

		}
		catch(ex)
		{
			alert(ex);
		}
	}

</script>