//该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();


//#2642 理赔审核门诊药品超量校验规则 add by zqs
function openPrescription(){
  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&LoadFlag=1" ;
  varSrc += "&CustomerNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&FeeType=" + fm.FeeType.value;
  pathStr="./FrameMainLLprescription.jsp?Interface=LLCasePrescriptionInput.jsp"+varSrc;
      showInfo = OpenWindowNew(pathStr,"LLCasePrescriptionInput","middle",800,330);
}

function openChargeDetails(){
    var varSrc = "&CaseNo=" + fm.CaseNo.value;
      varSrc += "&RgtNo=" + fm.RgtNo.value;
      varSrc += "&MainFeeNo="+fm.MainFeeNo.value;
      varSrc += "&LoadFlag=1" ;
      varSrc += "&CustomerNo=" + fm.CustomerNo.value;
      varSrc += "&CustomerName=" + fm.CustomerName.value;
      
      pathStr="./FrameMainChargeDetails.jsp?Interface=CaseChargeDetailsInput.jsp"+varSrc;
      showInfo = OpenWindowNew(pathStr,"CaseChargeDetailsInput","middle",800,330);
}

//#1769 案件备注信息的录入和查看功能 add by Houyd
function showCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}
	//#1769 案件备注信息的录入和查看功能 自动弹出页面只弹一次
	//获取session中的ShowCaseRemarkFlag
	var tShowCaseRemarkFlag = fm.ShowCaseRemarkFlag.value;
	if(tShowCaseRemarkFlag == "" || tShowCaseRemarkFlag == null || tShowCaseRemarkFlag == "null"){
		//tShowCaseRemarkFlag = "1";//准备存入session的标记:0-未弹出；1-已弹出
		//alert(tShowCaseRemarkFlag);		
	}else{
		return ;
	}	
	
	var tSql = "select 1 from llcaseremark where caseno='"+aCaseNo+"' with ur";
	var tResult = easyExecSql(tSql);
	if(tResult != null){
		pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  		showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
  	}	
	
	var LowerStr = "select rgttype from llregister where 1=1 and rgtno = '" + aCaseNo+"' with ur";
	var tLowerStr = easyExecSql(LowerStr);
	if(tLowerStr) {
		var ttRgtType = tLowerStr[0][0];
		if(ttRgtType == "13") {
			fm.LowerQuestion.disabled=false;
		}else {
			fm.LowerQuestion.disabled=true;
		}
	}
}
function openCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}

	pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  	showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
}

//账单重置
function zdReset(){
  initInpBox();
  initIFeeCaseCureGrid(fm.FeeAtti.value);
  initQDSecuGrid();
  initSecuSpanGrid();
  initSecuAddiGrid();
	querySecuDetail();
	initSecuItemGrid();
	showSecuItem();
	initFeeItemGrid();
	showFeeItem();   
}

//下一张账单
function zdNext()
{
  QueryZDInfo(2);
}

//上一张账单
function zdPrevious()
{
   QueryZDInfo(1);
}

//显示第一张帐单
function initQueryZD()
{
  QueryZDInfo(0);
}

//弹出窗口下发问题件
function OnLower(){
  var pathStr="./LLOnlinClaimQuestionMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&CustomerName="+fm.CustomerName.value+"&Handler="+fm.cOperator.value;
  showInfo=OpenWindowNew(pathStr,"EventInput","middle");
}

function initQuery()
{
  var strSql = " SELECT C.RGTNO, C.CUSTOMERNO, C.CUSTOMERNAME, C.CUSTOMERSEX ,C.CUSTOMERAGE,C.IDNO " +
  ",c.AccdentDesc,c.caseno FROM LLCASE C WHERE C.CASENO = '" + fm.all('CaseNo').value + "'";
  var arrResult = easyExecSql(strSql );
	var Remark="";
  if ( arrResult!=null )
  {
    fm.RgtNo.value =arrResult[0][0];
    fm.CustomerNo.value = arrResult[0][1];
    fm.CustomerName.value = arrResult[0][2];
    fm.CustomerSex.value = arrResult[0][3];
    if(arrResult[0][3]==0)
    fm.CustomerSexName.value = "男";
    if(arrResult[0][3]==1)
    fm.CustomerSexName.value = "女";
    if(arrResult[0][3]==2)
    fm.CustomerSexName.value = "不详";
    fm.Age.value = arrResult[0][4];
    fm.IDNo.value = arrResult[0][5];   
    fm.ContRemark.value = arrResult[0][6];
    fm.CaseNo.value = arrResult[0][7];
    strSql="select operator from llfeemain where caseno='"+fm.CaseNo.value+"'";
    var irr = easyExecSql(strSql);
    if (irr!=null)
    {
      irr[0][0]==null||irr[0][0]=='null'?'0':fm.cOperator.value=irr[0][0];
    }
    
    strSql="select rgtstate from llcase where caseno='"+fm.CaseNo.value+"'";
    var mrr = easyExecSql(strSql);
    if (mrr!=null)
    {
      mrr[0][0]==null||mrr[0][0]=='null'?'0':fm.Case_RgtState.value=mrr[0][0];
    }
    strSql = "select insuredstat, case insuredstat when '1' then '在职' when '2' then '退休' when '3' then '老职工' else '' end from lcinsured where insuredno = '"+fm.CustomerNo.value+"' order by makedate desc";
    var prr = easyExecSql(strSql);
    if (prr!=null)
    {
      fm.InsuredStat.value = prr[0][0];
      fm.InsuredStatName.value = prr[0][1];
      
    }
  var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
							+fm.CustomerNo.value+"'";
	var	crrResult1 = easyExecSql(strSQLx);
	if(crrResult1)
	{
	alert("请务必查询相关客户信息");
	
	}
    initDutyGrid();
  }
  else{
      fm.RgtNo.value ="";
      fm.CustomerNo.value = "";
      fm.CustomerName.value = "";
      fm.CustomerSex.value = "";
      fm.Case_RgtState.value= "";
      fm.cOperator.value= "";
      fm.MakeDate.value= "";
      initIFeeCaseCureGrid(fm.FeeAtti.value);
    }
  }

function QueryZDInfo(type)
{
  var strSql="select f.hospitalcode,f.hospitalname,f.receiptno,f.feetype,f.feeatti,"
  + "f.feeaffixtype,f.feedate,f.hospstartdate,f.hospenddate,f.realhospdate,f.mainfeeno ,"
  +"(select b.codename from ldcode b,ldhospital a where b.codetype='llhospiflag' and b.code=a.associateclass and a.hospitcode =f.hospitalcode),"
  +"f.inhosno,f.hosgrade,f.firstinhos,f.compsecuno,f.securityno,f.sumfee,f.hosdistrict,f.InsuredStat, "
  +"(select c.codename from ldcode c where c.codetype='llfeeatti' and c.code=f.feeatti), "
  +"(select c.codename from ldcode c where c.codetype='llfeetype' and c.code=f.feetype), "
  +"(select c.codename from ldcode c where c.codetype='insustat' and c.code=f.insuredstat)," +
  		" f.ReceiptType,(select c.codename from ldcode c where codetype='receipttype' and c.code=f.ReceiptType), " +
  		" f.IssueUnit,(select c.codename from ldcode c where codetype='issueunit' and c.code=f.IssueUnit), "
  +"f.medicaretype,(select c.codename from ldcode c where c.codetype='medicaretype' and c.code=f.medicaretype)"   // #3473 医保类型
  + " from llfeemain f  where f.caseno='" + fm.all('CaseNo').value
  + "' ";
  p=0;
  if(type==0)
    strSql+="order by f.mainfeeno";
  if(type==1)
    strSql+=" and trim(f.mainfeeno) < '" +fm.all('MainFeeNo').value+"' order by f.mainfeeno ";
  if(type==2)
    strSql+=" and trim(f.mainfeeno) > '" +fm.all('MainFeeNo').value+"' order by f.mainfeeno";
  if(type==3)
    strSql+=" and f.mainfeeno = '" +fm.all('MainFeeNo').value+"' order by f.mainfeeno";
  var arr=easyExecSql(strSql);
  if(type==1){
    if(arr==null||arr.length==0)
    {
      alert("已经到达第一张");
      return;
    }
    else
    {
      p=arr.length-1;
    }
  }
  if(type==2){
    if(arr==null||arr.length==0)
    {
      alert("已经到达最后一张");
      return;
    }
  }
  if(arr)
  {
    fm.HospitalCode.value=arr[p][0];
    fm.HospitalName.value=arr[p][1];
    fm.ReceiptNo.value=arr[p][2];
    fm.FeeType.value=arr[p][3];
    fm.FeeAtti.value=arr[p][4];
    fm.FeeAffixType.value=arr[p][5];
    fm.FeeDate.value=arr[p][6];
    fm.HospStartDate.value=arr[p][7];
    fm.HospEndDate.value=arr[p][8];
    fm.RealHospDate.value=arr[p][9];
    fm.FeeStart.value = fm.HospStartDate.value
    fm.FeeEnd.value = fm.HospEndDate.value
    fm.FeeDays.value = fm.RealHospDate.value
    fm.MainFeeNo.value=arr[p][10];
    fm.FixFlag.value=arr[p][11];
    fm.FeeAttiName.value=arr[p][20];
    fm.FeeTypeName.value=arr[p][21];
    fm.ReceiptType.value=arr[p][23];
    fm.ReceiptTypeName.value=arr[p][24];
    fm.IssueUnit.value=arr[p][25];
    fm.IssueUnitName.value=arr[p][26];
    fm.MedicareType.value=arr[p][27]; 
    fm.MedicareTypeName.value=arr[p][28];// #3473 医保类型
    if(fm.FeeAffixType.value=="0")
      fm.FeeAffixTypeName.value="原件";
    else
      fm.FeeAffixTypeName.value="复印件";
    fm.inpatientNo.value=arr[p][12];
    fm.HosGrade.value=arr[p][13];
    temsql="select codename from ldcode where codetype='hospitalclass' and code='"+fm.HosGrade.value+"'";
    var xrr = easyExecSql(temsql);
    if(xrr)
      fm.HosGradeName.value = xrr[0][0];
    fm.FirstInHos.value=arr[p][14];
    fm.CompSecuNo.value=arr[p][15];
    fm.SecurityNo.value=arr[p][16];
    fm.SumFee.value = arr[p][17];
    fm.InsuredStat.value = arr[p][19];
    fm.InsuredStatName.value = arr[p][22];
    fm.UrbanFlag.value = arr[p][18];
      if(fm.UrbanFlag.value=='0')
        fm.UrbanFlagName.value = '非城区';
      if(fm.UrbanFlag.value=='1')
        fm.UrbanFlagName.value = '城区';
    afterCodeSelect('FeeType',fm.FeeType.value);
    //填充账单项目明细
    if (fm.FeeAtti.value=="0"||fm.FeeAtti.value=="5")   //普通帐单
    {
      strSql="select FeeItemCode,FeeItemName,Fee, SelfAmnt,PreAmnt,RefuseAmnt,avaliflag from LLCaseReceipt where MainFeeNo='"
      +fm.all('MainFeeNo').value+"' order by FeeItemCode";
      turnPage.pageLineNum = 100;
      turnPage.queryModal(strSql,IFeeCaseCureGrid);
  
      if (IFeeCaseCureGrid.mulLineCount<=0)//防止没有对应数据,则显示默认的显示状态
        initIFeeCaseCureGrid(fm.FeeAtti.value);
    }
    if (fm.FeeAtti.value=="1")
    {
      if(fm.MngCom.value=='8694'||fm.MngCom.value=='8637')
        queryQDZD();
      fillHos_SecuReceipt_BJ();
      querySecuDetail();
    }
    if (fm.FeeAtti.value=="2")
    {
      fillManualReceipt();
      querySecuDetail();
    }
    if (fm.FeeAtti.value=="3")//填充特需医疗帐单
    {
      strSql = "select SumFee,SocialPlanAmnt,OtherOrganAmnt, b.remnant "+
      " from llsecurityreceipt a,llfeemain b where a.mainfeeno=b.oldmainfeeno and "+
      " b.mainfeeno='"+fm.all('MainFeeNo').value+"' union "+
      "select SumFee,SocialPlanAmnt,OtherOrganAmnt, remnant from llfeemain where mainfeeno='"+
      fm.all('MainFeeNo').value+"'";
      turnPage.queryModal(strSql,SecuAddiGrid);
  
      if (SecuAddiGrid.mulLineCount<=0)//防止没有对应数据,则显示默认的显示状态
      {
        initSecuAddiGrid();
      }
    }
    if (fm.FeeAtti.value=="4")
    {
      fillHos_SecuReceipt_BJ();

    }
  }
  else
    initInpBox();
}

function fillHos_SecuReceipt_BJ(){

      initSecuItemGrid();

      initFeeItemGrid();

  var strSQL = "select codename,code,codealias,(select sum(feemoney) from llfeeotherItem where mainfeeno='"
  +fm.MainFeeNo.value+"' and ItemCode=code) from ldcode where codetype='llsecufeeitem' and comcode='"
  if(fm.FeeAtti.value=='4'){
  	strSQL+="86' order by code";
  }
  else{
  	strSQL+=fm.MngCom.value+"' order by code";



  }
  var arr = easyExecSql(strSQL);
  if(arr){
    var x = 1;
    var y = 0;
    for(var j=0;j<arr.length;j++){
      SecuItemGrid.setRowColData(y,x,arr[j][0]);
      SecuItemGrid.setRowColData(y,x+1,arr[j][1]);
      SecuItemGrid.setRowColData(y,x+2,arr[j][2]);
      var tsfee = arr[j][3]=='null'?'':arr[j][3];
      SecuItemGrid.setRowColData(y,x+3,tsfee);
      x += 4;
      if(x==17&&j<arr.length-1){
        SecuItemGrid.addOne();
        x=1;
        y+=1;
      }
    }
  }
  
  var feetype = '2';
  if (fm.FeeType.value!='')
    feetype=fm.FeeType.value;
  var strSQL = "select codename,code,(select sum(fee) from llcasereceipt where mainfeeno='"
  +fm.MainFeeNo.value+"' and FeeItemCode=code) from ldcode where codetype='llfeeitemtype' "
  +"and code like '"+feetype+"%%' order by code";
  initFeeItemGrid();
  var brr = easyExecSql(strSQL);
  if(brr){
    var x = 1;
    var y = 0;
    for(var k=0;k<brr.length;k++){
      FeeItemGrid.setRowColData(y,x,brr[k][0]);
      FeeItemGrid.setRowColData(y,x+1,brr[k][1]);
      var tfee = brr[k][2]=='null'?'':brr[k][2];
      FeeItemGrid.setRowColData(y,x+2,tfee);
      x += 3;
      if(x==13&&k<brr.length-1){
        FeeItemGrid.addOne();
        x=1;
        y+=1;
      }
    }
  }
}

//填充社保手工报销单
function fillManualReceipt()
{
      strSql="select * from llsecurityreceipt where MainFeeNo='"+fm.all('MainFeeNo').value+"'";
      var drr=easyExecSql(strSql);
  
      if(drr)
      {
        fm.ApplyAmnt.value=drr[0][4];
        fm.FeeInSecu.value=drr[0][5];
        fm.FeeOutSecu.value=drr[0][6];
        fm.TotalSupDoorFee.value=drr[0][7];
        fm.TotalApplyAmnt.value=drr[0][4];
        fm.PayReceipt.value=drr[0][8];
        fm.RefuseReceipt.value=drr[0][9];
        fm.SupDoorFee.value=drr[0][10];
        fm.YearSupDoorFee.value=drr[0][11];
        fm.PlanFee.value=drr[0][12];
        fm.YearPlayFee.value=drr[0][13];
        fm.SupInHosFee.value=drr[0][14];
        fm.YearSupInHosFee.value=drr[0][15];
        fm.SecurityFee.value=drr[0][16];
        fm.SelfPay1.value=drr[0][19];
        fm.SelfPay2.value=drr[0][20];
        fm.SelfAmnt.value=drr[0][22];
        fm.SecuRefuseFee.value=drr[0][21];
        if(drr[0][38]=="null"){
        	fm.RetireAddFee.value="0";
        	}else{
        		fm.RetireAddFee.value=drr[0][38];
        		}
       if(drr[0][39]=="null"){
        	fm.YearRetireAddFee.value="0";
        	}else{
        		 fm.YearRetireAddFee.value=drr[0][39];
        		}
        
        
       
      }
      strSql="select feeitemcode,Fee, RefuseAmnt from LLCaseReceipt where substr(feeitemcode,1,1)='4' and MainFeeNo='"
      +fm.all('MainFeeNo').value+"' order by feeitemcode";
      var err=easyExecSql(strSql);
  
      if(err)
      {
        rcount = err.length;
        fm.DrugFee.value=err[0][1];
        fm.RefuseDrugFee.value=err[0][2];
        for(var i=1;i<rcount;i++){
          if(err[i][0]=='402'){
            fm.ExamFee.value=err[i][1];
            fm.RefuseExamFee.value=err[i][2];
          }
          if(err[i][0]=='403'){
            fm.OtherFee.value=err[i][1];
            fm.RefuseOtherFee.value=err[i][2];
          }
          if(err[i][0]=='404'){
            fm.MaterialFee.value=err[i][1];
            fm.RefuseMaterialFee.value=err[i][2];
          }
        }
      }
}
function verifyHospital()
{
	
	
	
}


//帐单保存前校验
function saveFee(){		
	    var cont="";
		if(fm.FeeAtti.value=="2"){
		    cont=" and FeeAtti ='2' and hospstartdate='"+fm.FeeStart.value+
		         "' and CustomerNo='"+fm.CustomerNo.value+"'";
		}
		var sqlx="select distinct caseno from llfeemain where HospitalCode ='"+fm.HospitalCode.value+
			     "' and caseno <> '"+fm.CaseNo.value+"' and ReceiptNo='"+fm.ReceiptNo.value+"'"+cont;
		var arrResult = easyExecSql(sqlx);	
		if(!arrResult){
		}else{
		    if(!confirm("该帐单在"+arrResult+"中有理赔记录，您确定要继续吗？"))
		    return false;
		}
		if(fm.FeeAtti.value!="2"&&fm.FeeDate.value==''){
		    alert("结算日期不能为空！");
		    return false;
		}
		//3633 结算日期
		if(!llcheckInputDate(fm.FeeDate.value)){
			alert("请核实结算日期录入是否正确");
			return false;
		}
		
    //TODO:临时校验，社保案件不可保存检录信息
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}

		//TODO:新增效验，理赔二核时不可账单保存
	var sql = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstate=easyExecSql(sql);
	if (rgtstate!=null){
	     if(rgtstate[0][0]=='16'){
	       alert("理赔二核中不可账单保存，必须等待二核结束！");
	       return false;
	     }
	}
	if( verifyInput2() == false ) return false;
	
	
	var rowNum=IFeeCaseCureGrid.mulLineCount ;
  var count=0; 
    for (count=0;count<rowNum;count++)
  {
  	if(
  	IFeeCaseCureGrid.getRowColData(count,3)<0||
  	IFeeCaseCureGrid.getRowColData(count,4)<0||
  	IFeeCaseCureGrid.getRowColData(count,5)<0||
  	IFeeCaseCureGrid.getRowColData(count,6)<0 ){
  	alert("金额必须大于0");
  	return false;
        }
  	
  	}
  
  
  if(fm.FeeAtti.value=='1'&&(fm.MngCom.value=='8694'||fm.MngCom.value=='8637')){
    if(checkQDZD()==false)
      return;
  }
  if(fm.FeeAtti.value=='2')
  {
    if(!checkSGZD()){
      return false;
    }
    fm.HospStartDate.value = fm.FeeStart.value;
    fm.HospEndDate.value = fm.FeeEnd.value;
    fm.RealHospDate.value = fm.FeeDays.value;
    if(fm.ReceiptNo.value=='')
      fm.ReceiptNo.value = "000000";
    fm.FeeAffixType.value = "0";
    fm.FeeDate.value = fm.FeeEnd.value;
  }

  if( verifyInput2() == false ) return false;
  //3558
  if (fm.FeeType.value=='2') {
	  var stDate=fm.HospStartDate.value;
	  var enDate=fm.HospEndDate.value;
      if(stDate.length!=0&&enDate.length!=0){
    	  stDate=modifydate(stDate);
    	  enDate=modifydate(enDate);
    	  if(isDate(stDate)&&isDate(enDate)){
    		  //3633入出院日期
    		  if(!llcheckInputDate(stDate)){
    			  alert("请核实入院日期录入是否正确");
    			  return false;
    		  }
    		  if(!llcheckInputDate(enDate)){
    			  alert("请核实出院日期录入是否正确");
    			  return false;
    		  }
    		  if(dateDiff(stDate,enDate,"D")<0){
    			  alert("请核实被保险人入出院日期");
            	  return false;
    		  }
    	  }else{
    		  alert("请核实被保险人入出院日期");
        	  return false;
    	  }
      }else{
    	  alert("请核实被保险人入出院日期");
    	  return false;
      }
   }
  //3633 治疗开始日期 治疗结束日期
  if(fm.FeeType.value=='1'){
	  if(!llcheckInputDate(fm.HospStartDate.value)){
		  alert("请核实治疗开始日期录入是否正确");
		  return false;
	  }
	  if(!llcheckInputDate(fm.HospEndDate.value)){
		  alert("请核实治疗结束日期录入是否正确");
		  return false;
	  }
  }
  //3558
  if (fm.HospEndDate.value.length != 0)
  {
    if(!checkDate(fm.HospStartDate.value,fm.HospEndDate.value)){
      if (fm.FeeType.value=='1') {
         alert("请核实治疗结束日期是否正确");
      } else {
          alert("请核实被保险人入出院日期");//3558
      }
      return false;
    }
  }
  if(!checkDate(fm.HospStartDate.value,fm.FeeDate.value)){
    alert("结算日期必须晚于住院开始日期或治疗开始日期");
    return false;
  }
  //#2694住院天数录入校验功能
  var HospDate= fm.RealHospDate.value;
  var FeeType =fm.FeeType.value;
  if(FeeType==2){
	  if(!HospDateReasonable(HospDate)){
	      return false;
	  }
  }
  

  fm.all('cOperate').value = "INSERT";
	fm.action ="./ICaseCureSave.jsp";
  submitForm();
}



//删除帐单，该功能目前不使用
  function deleteFee()
  {
    if(fm.all('MainFeeNo').value == null ||
    fm.all('MainFeeNo').value == '')
    {
      alert("账单信息尚未保存！");
      return;
    }
    fm.all('cOperate').value = "DELETE";
    submitForm();
  }

//数据提交
function submitForm()
{
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
}

//提交数据后操作
function afterSubmit(FlagStr, content, tMainFeeNo)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    fm.all('MainFeeNo').value = tMainFeeNo;
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    querySecuDetail();
    QueryZDInfo(3);
  }
  if (fm.all('cOperate').value == "DELETE")
  {
    initInpBox();
    initIFeeCaseCureGrid(fm.FeeAtti.value);
  }
}

//录入理赔号后回车查询相应的案件帐单信息
function QueryOnKeyDown()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    initQuery();
    showCaseRemark();//#1769 案件备注信息的录入和查看功能 add by Houyd 初始化页面弹出
    QueryZDInfo(0);
    
  }
}

//跳转到检录处理
function nextDeal()
{
  var caseNo = fm.all('CaseNo').value;

  var varSrc="&CaseNo="+ caseNo;
  parent.window.location = "./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+varSrc
}

//返回理赔人信箱
function backDeal()
{
  top.close();
}

//选择下拉列表后响应函数
function afterCodeSelect( cCodeName, Field )
{
  if(cCodeName=="llhospiquery"){
    if(fm.UrbanFlag.value=='0')
      fm.UrbanFlagName.value = '非城区';
    if(fm.UrbanFlag.value=='1')
      fm.UrbanFlagName.value = '城区';
    var tssql0 = "select codename from ldcode where ";
    sqlpart1=" codetype = 'llhospiflag' and code='"+fm.FixFlag.value+"'";
    tssql = tssql0+sqlpart1;
    urr = easyExecSql(tssql);
    if(urr){
      fm.FixFlag.value=urr[0][0];
    }
    sqlpart2 = " codetype='hospitalclass' and code='"+fm.HosGrade.value+"'";
    tssql = tssql0+sqlpart2;
    vrr = easyExecSql(tssql);
    if(vrr)
      fm.HosGradeName.value = vrr[0][0];
  }
  if( cCodeName=="llfeeatti"){
    initZDStyle();
  }
  if( cCodeName == "FeeType")
  {
    initZDStyle();
    caldate();
    strsqlinit="1 and code like #"+fm.all('FeeType').value+"%#";
    initIFeeCaseCureGrid(fm.FeeAtti.value);
  }
  if(cCodeName == "comcode"){
    initZDStyle();
  }
  if( cCodeName == "llfeeitemtype")
  {
    if (fm.FeeDutyFlag.value=="0")
       alert("该客户没有费用险责任");
  }
}

//计算实际住院天数
function caldate()
{
  if(fm.HospStartDate.value!=""&&fm.HospEndDate.value!="")
  {
    fm.HospStartDate.value=modifydate(fm.HospStartDate.value);
    fm.HospEndDate.value=modifydate(fm.HospEndDate.value);
    //	var a=dateDiff(fm.HospStartDate.value,fm.HospEndDate.value,"D")-1;
    var strSql= "select to_char(to_date('"+ fm.HospEndDate.value + "') - to_date('" + fm.HospStartDate.value +"')) from dual"
    //alert(strSql );
    var mrr = easyExecSql(strSql);
    if (mrr!=null)
    {
      a= mrr[0][0] ;
      if(fm.FeeType.value=="1")
        a=a/1+1;
      if ( a<0) 
        fm.RealHospDate.value =0 ;
      else
        fm.RealHospDate.value= a;
    }
  }
}

//不合理入院时间校验
function HospDateReasonable(HospDate){

  //一、校验单次住院天数超过30天
  if(HospDate>30){
    if(!confirm("住院天数大于30天，请确认继续操作？")){
      return false;
    }
  }

  return true;
}

//筛选门诊的开始时间和结束时间
function fillDate()
{
  if (fm.FeeType.value=="1")
  {
    var StartDate=modifydate(fm.FeeDate.value);
    var EndDate=modifydate(fm.FeeDate.value);
    var strSql="select feedate from llfeemain where caseno='" + fm.all('CaseNo').value
    +"' and FeeType='1'";
    var arr=easyExecSql(strSql);
    if (arr!=null)
    {
      var count=arr.length;
      for(i=0;i<count;i++){
        if(compareDate(StartDate,arr[i][0])==1)
          StartDate=arr[i][0];
        if(compareDate(EndDate,arr[i][0])==2)
          EndDate=arr[i][0];
      }
      fm.HospStartDate.value=StartDate;
      fm.HospEndDate.value=EndDate;
    }
    else
    {
      fm.HospStartDate.value=fm.FeeDate.value;
      fm.HospEndDate.value=fm.FeeDate.value;
    }
   }
}
         
//校验录入日期         
function checkDate(begin,end)
{
  begin=modifydate(begin);
  end=modifydate(end);
  if(begin<=end)
    return true;
  else
    return false;
}

//选择帐单显示界面
function initZDStyle()
{
    tsql = "select code,codename,'','','','',codealias from ldcode where codetype='llfeeitemtype' and "
    +" (substr(code,1,1) ='"+fm.FeeType.value+"' ";
  if( fm.FeeType.value=="1")
  {
    titleStart.style.display='';
    titleEnd.style.display='';
    titleMZ.style.display='';
    titleFeeStart.style.display='';
    titleApply.style.display='';
    titleConDays.style.display='';
    titleInpatient.style.display='none';
    titleOutpatient.style.display='none';
    titleZY.style.display='none';
    titleInHos.style.display='none';
    titleOutHos.style.display='none';
    titleDays.style.display='none';
  }
  if( fm.FeeType.value=="2")
  {
    titleInpatient.style.display='';
    titleOutpatient.style.display='';
    titleZY.style.display='';
    titleInHos.style.display='';
    titleOutHos.style.display='';
    titleDays.style.display='';
    titleStart.style.display='none';
    titleEnd.style.display='none';
    titleMZ.style.display='none';
    titleFeeStart.style.display='none';
    titleApply.style.display='none';
    titleConDays.style.display='none';
  }
  if(fm.FeeAtti.value=="0"||fm.FeeAtti.value=="5")
  {
    titleC1.style.display='';
    fm.InsuredStat.verify="" ;
    titleC2.style.display='';
    commonZD.style.display='';
    titleS1.style.display='none';
    titleS2.style.display='none';
    divsecurity.style.display='none';
    secuspan.style.display='none'
    SecuAddiZD.style.display='none';
    QDZD.style.display='none';
    divSecu.style.display='none';
    divSecuItem2.style.display='none';
  }
  if(fm.FeeAtti.value=="1")
  {
    initFeeItemGrid();
    initSecuItemGrid();
    showFeeItem();
    showSecuItem();
    titleS1.style.display='';
    fm.InsuredStat.verify="参保人员类别|notnull" ;
    titleS2.style.display='';
    titleC1.style.display='';
    titleC2.style.display='';
    secuspan.style.display=''
    divsecurity.style.display='none';
    SecuAddiZD.style.display='none';
    if(fm.MngCom.value.substr(0,4)=='8694'||fm.MngCom.value.substr(0,4)=='8637')
    {
      QDZD.style.display='';
      commonZD.style.display='none';
      divSecu.style.display='none';
      divSecuItem2.style.display='none';
    }
    else
    {
      QDZD.style.display='none';
      commonZD.style.display='none';
      divSecu.style.display='';
      divSecuItem2.style.display='';
    }
    querySecuDetail();
  }
  if( fm.FeeAtti.value=="2"){
    titleS1.style.display='';
    fm.InsuredStat.verify="参保人员类别|notnull" ;
    titleS2.style.display='';
    secuspan.style.display=''
    divsecurity.style.display='';
    titleC1.style.display='';
    titleC2.style.display='none';
    commonZD.style.display='none';
    SecuAddiZD.style.display='none';
    QDZD.style.display='none';
    querySecuDetail();
    divSecu.style.display='none';
    divSecuItem2.style.display='none';
    if(fm.MngCom.value.substr(0,4)=='8611'&&fm.InsuredStat.value=="2"){
      divRetireAddFee.style.display='';
    }
  }
  if(fm.FeeAtti.value=="3"){
    titleS1.style.display='';
    fm.InsuredStat.verify="" ;
    titleS2.style.display='';
    titleC1.style.display='';
    titleC2.style.display='';
    SecuAddiZD.style.display='';
    divSecuAddiReceipt.style.display='';
    divsecurity.style.display='none';
    commonZD.style.display='none';
    QDZD.style.display='none';
    secuspan.style.display='none'
    divSecu.style.display='none';
    divSecuItem2.style.display='none';
  }
  if(fm.FeeAtti.value=="4"){
    initSecuItemGrid();
    showSecuItem();
    titleS1.style.display='';
    titleS2.style.display='';
    titleC1.style.display='';
    titleC2.style.display='';
    commonZD.style.display='none';
    secuspan.style.display='none'
    divsecurity.style.display='none';
    SecuAddiZD.style.display='none';
    divSecu.style.display='';
    divSecuItem2.style.display='';
  }
}

//填充社保分段信息
function querySecuDetail(){  
  if(fm.MainFeeNo.value==null||fm.MainFeeNo.value=='')
    SDSql = "select '大额封顶线以上','','','','','','88' from dual union select '合计金额','','','','','','99' from dual";
  else{
    var SDSql = "(select to_char(downlimit),to_char(uplimit),char(decimal(planpayrate,3,2)),planfee,"+
    "selfpay,planfee+selfpay,serialno a from llsecudetail where MainFeeNo='"+fm.MainFeeNo.value+
    "' and uplimit>0 union select '大额封顶线以上','',char(decimal(planpayrate,3,2)),planfee,selfpay,"+
    "planfee+selfpay,'88' a from llsecudetail where uplimit=0 and MainFeeNo='"+fm.MainFeeNo.value+
//   " union select '退休人员补充费用','',char(decimal(planpayrate,3,2)),planfee,selfpay, " +
//  " planfeeselfpay,'8611' a from llsecudetail where uplimit=1 and MainFeeNo='"+fm.MainFeeNo.value+" '"+ 
    "' union select '合计金额','','',case when sum(planfee) is null then 0 else sum(planfee) "+
    "end,case when sum(selfpay) is null then 0 else sum(selfpay) end,case when (sum(planfee)is"+
    " null or sum(selfpay) is null)  then 0 else sum(planfee)+sum(selfpay) end, '99' a "
    +"from llsecudetail where MainFeeNo='"+fm.MainFeeNo.value+"') order by a";
  }
  turnPage.queryModal(SDSql,SecuSpanGrid);
}

//显示过滤出的保单责任
function initDutyGrid(){
  fm.FeeDutyFlag.value='0';
  var strSql="select distinct a.getdutycode,b.getdutyname,a.contno,(select CValiDate from lcpol where polno=c.polno),c.amnt,c.mult from lltoclaimduty a,lmdutygetclm b,lcduty c where c.polno=a.polno and b.getdutycode=a.getdutycode and a.caseno='"+fm.CaseNo.value+"' order by getdutycode";
  turnPage.queryModal(strSql,DutyRelaGrid);
  strSql="select '1' from lcpol a,lmriskapp b where b.riskcode = a.riskcode and b.risktype1 not in ('2','5') and insuredno='"
  +fm.CustomerNo.value+"' union select '1' from lbpol a,lmriskapp b where b.riskcode = a.riskcode and b.risktype1 not in ('2','5') and insuredno='"+fm.CustomerNo.value+"'";
  var brr=easyExecSql(strSql);
  if (brr!=null){
    fm.FeeDutyFlag.value=brr[0][0];
  }
}

function queryQDZD(){
  var initSql = "select code,codename, "
  +"coalesce((select wholeplanfee from llcasereceipt where feeitemcode=code and caseno='"+fm.CaseNo.value+"' and MainFeeNo='"+fm.MainFeeNo.value+"'),0),"
  +"coalesce((select partplanfee from llcasereceipt where feeitemcode=code and caseno='"+fm.CaseNo.value+"' and MainFeeNo='"+fm.MainFeeNo.value+"'),0),"
  +"coalesce((select selfpay2 from llcasereceipt where feeitemcode=code and caseno='"+fm.CaseNo.value+"' and MainFeeNo='"+fm.MainFeeNo.value+"'),0),"
  +"coalesce((select selffee from llcasereceipt where feeitemcode=code and caseno='"+fm.CaseNo.value+"' and MainFeeNo='"+fm.MainFeeNo.value+"'),0),"
  +"coalesce((select fee from llcasereceipt where feeitemcode=code and caseno='"+fm.CaseNo.value+"' and MainFeeNo='"+fm.MainFeeNo.value+"'),0) "
  +" from ldcode where codetype='llfeeitemtype' and substr(code,1,2)='QD' order by code";
  turnPage.queryModal(initSql,QDSecuGrid);
}

function checkQDZD(){
  var rowNum=QDSecuGrid.mulLineCount ;
  var xx=0; 
  var tWPlanFee=0;
  var tPPlanFee=0;
  var tSelfPay2=0;
  var tSelfFee=0;
  var tFee=0;
  for (xx=0;xx<rowNum;xx++)
  {
    tFeeItemName = QDSecuGrid.getRowColData( xx, 2);
    WPlanFee = QDSecuGrid.getRowColData(xx,3)==''?0:QDSecuGrid.getRowColData(xx,3)/1;
    PPlanFee = QDSecuGrid.getRowColData(xx,4)==''?0:QDSecuGrid.getRowColData(xx,4)/1;
    SelfPay2 = QDSecuGrid.getRowColData(xx,5)==''?0:QDSecuGrid.getRowColData(xx,5)/1;
    SelfFee  = QDSecuGrid.getRowColData(xx,6)==''?0:QDSecuGrid.getRowColData(xx,6)/1;
    Fee      = QDSecuGrid.getRowColData(xx,7)==''?0:QDSecuGrid.getRowColData(xx,7)/1;
    tempFee = WPlanFee+PPlanFee+SelfPay2+SelfFee;
    if(Fee < 0.01){
      Fee = tempFee;
      QDSecuGrid.setRowColData(xx,7,(Fee+""));
    }
    else{
      if(tempFee>0.01&&Math.abs(Fee-tempFee)>0.01 ){
        alert(tFeeItemName+"费用录入有误");
        return false;
      }
    }
    if (xx==rowNum-1)
      break;
      tWPlanFee +=WPlanFee;
      tPPlanFee += PPlanFee;
      tSelfPay2 += SelfPay2;
      tSelfFee += SelfFee;
      tFee += Fee; 
  }
  WPlanFee=(WPlanFee<0.01)?tWPlanFee:WPlanFee;
  PPlanFee = (PPlanFee<0.01)?tPPlanFee:PPlanFee;
  SelfPay2 = (SelfPay2<0.01)?tSelfPay2:SelfPay2;
  SelfFee = (SelfFee<0.01)?tSelfFee:SelfFee;
  Fee = (Fee<0.01)?tFee:Fee;
  QDSecuGrid.setRowColData(xx,3,WPlanFee+"");
  QDSecuGrid.setRowColData(xx,4,PPlanFee+"");
  QDSecuGrid.setRowColData(xx,5,SelfPay2+"");
  QDSecuGrid.setRowColData(xx,6,SelfFee+"");
  QDSecuGrid.setRowColData(xx,7,Fee+"");
  if (tWPlanFee>0.01&&Math.abs(tWPlanFee-WPlanFee)>0.01){
    alert("全额统筹合计有误");
    return false;
  }
  if (tPPlanFee>0.01&&Math.abs(tPPlanFee-PPlanFee)>0.01){
    alert("部分统筹合计有误");
    return false;
  }
  if (tSelfPay2>0.01&&Math.abs(tSelfPay2-SelfPay2)>0.01){
    alert("部分统筹自负部分合计有误");
    return false;
  }
  if (tSelfFee>0.01&&Math.abs(tSelfFee-SelfFee)>0.01){
    alert("全额自费合计有误");
    return false;
  }
  if (tFee>0.01&&Math.abs(tFee-Fee)>0.01){
    alert("合计有误");
    return false;
  }
}

function checkSGZD(){
  var tApplyAmnt = fm.ApplyAmnt.value==''?0:fm.ApplyAmnt.value/1;
  var tFeeInSecu = fm.FeeInSecu.value==''?0:fm.FeeInSecu.value/1;
  var tFeeOutSecu = fm.FeeOutSecu.value==''?0:fm.FeeOutSecu.value/1;
  var tTotalSupDoorFee = fm.TotalSupDoorFee.value==''?0:fm.TotalSupDoorFee.value/1;
  
  var tSupDoorFee = fm.SupDoorFee.value==''?0:fm.SupDoorFee.value/1;
  var tYearSupDoorFee = fm.YearSupDoorFee.value==''?0:fm.YearSupDoorFee.value/1
  var tPlanFee = fm.PlanFee.value==''?0:fm.PlanFee.value/1;
  var tYearPlayFee = fm.YearPlayFee.value==''?0:fm.YearPlayFee.value/1;
  var tSupInHosFee = fm.SupInHosFee.value==''?0:fm.SupInHosFee.value/1;
  var tYearSupInHosFee = fm.YearSupInHosFee.value==''?0:fm.YearSupInHosFee.value/1
  var tSecurityFee = fm.SecurityFee.value==''?0:fm.SecurityFee.value/1;

  var tSelfPay1 = fm.SelfPay1.value==''?0:fm.SelfPay1.value/1;
  var tSelfPay2 = fm.SelfPay2.value==''?0:fm.SelfPay2.value/1;
  var tSelfAmnt = fm.SelfAmnt.value==''?0:fm.SelfAmnt.value/1;
  var tSecuRefuseFee = fm.SecuRefuseFee.value==''?0:fm.SecuRefuseFee.value/1;
  
  var tDrugFee = fm.DrugFee.value==''?0:fm.DrugFee.value/1;
  var tRefuseDrugFee = fm.RefuseDrugFee.value==''?0:fm.RefuseDrugFee.value/1;
  var tExamFee = fm.ExamFee.value==''?0:fm.ExamFee.value/1;
  var tRefuseExamFee = fm.RefuseExamFee.value==''?0:fm.RefuseExamFee.value/1;
  var tOtherFee = fm.OtherFee.value==''?0:fm.OtherFee.value/1;
  var tRefuseOtherFee = fm.RefuseOtherFee.value==''?0:fm.RefuseOtherFee.value/1;
  var tMaterialFee = fm.MaterialFee.value==''?0:fm.MaterialFee.value/1;
  var tRefuseMaterialFee = fm.RefuseMaterialFee.value==''?0:fm.RefuseMaterialFee.value/1;

  if(tApplyAmnt<=0){
    tApplyAmnt = tFeeInSecu+tFeeOutSecu;
    fm.ApplyAmnt.value = tApplyAmnt.toFixed(2);
  }
  else{
    if(tFeeInSecu>0&&tFeeOutSecu>0&&Math.abs(tApplyAmnt-tFeeInSecu-tFeeOutSecu)>0.01){
      alert("申报费用不等于医保支付金额与医保拒付金额之和");
      return false;
    }
  }
  if(tSecurityFee<=0){
    tSecurityFee = tSupDoorFee+tPlanFee+tSupInHosFee;
    fm.SecurityFee.value = tSecurityFee.toFixed(2);
  }
  else{
    if(Math.abs(tSecurityFee-tSupDoorFee-tPlanFee-tSupInHosFee)>0.01){
      alert("医保支付金额合计有误");
      return false;
    }
  }
  if(tSecuRefuseFee<=0){
    tSecuRefuseFee = tSelfPay1+tSelfPay2+tSelfAmnt;
    fm.SecuRefuseFee.value = tSecuRefuseFee.toFixed(2);
  }
  else{
    if(Math.abs(tSecuRefuseFee-tSelfPay1-tSelfPay2-tSelfAmnt)>0.01){
      alert("医保不予支付金额合计有误");
      return false;
    }
  }
  if(tFeeInSecu<=0){
    tFeeInSecu = tSecurityFee+tSelfPay1;
    fm.FeeInSecu.value = tFeeInSecu.toFixed(2);
  }
  else{
    if(Math.abs(tFeeInSecu - tSecurityFee-tSelfPay1)>0.01){
      alert("社保内费用不等于医保支付金额与自负一金额之和");
      return false;
    }
  }
  if(tFeeOutSecu<=0){
    tFeeOutSecu = tSelfPay2+tSelfAmnt;
    fm.FeeOutSecu.value = tFeeOutSecu.toFixed(2);
  }
  else{
    if(tSelfPay2>0&&tSelfAmnt>0&&Math.abs(tFeeOutSecu - tSelfPay2-tSelfAmnt)>0.01){
      alert("社保外费用不等于自负二金额与自费金额之和");
      return false;
    }
    else{
      if(tSelfPay2>0){
        tSelfAmnt=tFeeOutSecu-tSelfPay2;
        fm.SelfAmnt.value = tSelfAmnt.toFixed(2);
      }
      else{
        tSelfPay2 = tFeeOutSecu-tSelfAmnt;
        fm.SelfPay2.value = tSelfPay2.toFixed(2);
      }
    }
  }
  if(tSelfAmnt<0){
    alert("自费金额有误！");
    return false;
  }
  if(tSelfPay2<0){
    alert("自负二金额有误！");
    return false;
  }
  if(tRefuseDrugFee>tDrugFee){
    alert("药费的医保不支付金额不能大于总金额");
    return false;
  }
  if(tRefuseExamFee>tExamFee){
    alert("检查、治疗、化验费用的医保不支付金额不能大于总金额");
    return false;
  }
  if(tRefuseMaterialFee>tMaterialFee){
    alert("材料费用的医保不支付金额不能大于总金额");
    return false;
  }
  if(tRefuseOtherFee>tOtherFee){
    alert("其他费用的医保不支付金额不能大于总金额");
    return false;
  }
  return true;
}

function getSGBXD(){
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.action ="./ICaseSecuZD.jsp";
			fm.submit();
}

function openDrug(){
    var varSrc = "&CaseNo=" + fm.CaseNo.value;
      varSrc += "&RgtNo=" + fm.RgtNo.value;
      varSrc += "&MainFeeNo="+fm.MainFeeNo.value;
      varSrc += "&LoadFlag=1" ;
      varSrc += "&CustomerNo=" + fm.CustomerNo.value;
      varSrc += "&CustomerName=" + fm.CustomerName.value;
      
      pathStr="./FrameMainDrug.jsp?Interface=CaseDrugInput.jsp"+varSrc;
      showInfo = OpenWindowNew(pathStr,"CaseDrugInput","middle",800,330);
}

function showBlack(){

	var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
							+fm.CustomerNo.value+"'";
	var	crrResult1 = easyExecSql(strSQLx);
	if(crrResult1){
	alert("客户："+crrResult1[0][1]+"为黑名单客户!");
	
	}
}

function zdComplete(){

    //TODO:临时校验，社保案件不可保存检录信息
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
	
			//TODO:新增效验，理赔二核时不可确认账单录入
	var sql = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstate=easyExecSql(sql);
	if (rgtstate!=null){
	     if(rgtstate[0][0]=='16'){
	       alert("理赔二核中不可确认账单录入，必须等待二核结束！");
	       return false;
	     }
	}
	
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./ICaseCureSaveAll.jsp";
    fm.submit(); //提交
}

function showSecuItem(){
  var strSQL = "select codename,code,codealias from ldcode where codetype='llsecufeeitem' "
  +"and comcode='";
  if(fm.FeeAtti.value=='4'){
  		strSQL+="86' order by code"
  }
  else{
  	strSQL+=fm.MngCom.value+"' order by code";
  }
  var arr = easyExecSql(strSQL);
  if(arr){
    var x = 1;
    var y = 0;
    for(var j=0;j<arr.length;j++){
      SecuItemGrid.setRowColData(y,x,arr[j][0]);
      SecuItemGrid.setRowColData(y,x+1,arr[j][1]);
      SecuItemGrid.setRowColData(y,x+2,arr[j][2]);
      x += 4;
      if(x==17&&j<arr.length-1){
        SecuItemGrid.addOne();
        x=1;
        y+=1;
      }
    }
  }
}

function showFeeItem(){
  var feetype = '2';
  if (fm.FeeType.value!='')
    feetype=fm.FeeType.value;
  var strSQL = "select codename,code from ldcode where codetype='llfeeitemtype' "
  +"and code like '"+feetype+"%%' order by code";
  initFeeItemGrid();
  var arr = easyExecSql(strSQL);
  if(arr){
    var x = 1;
    var y = 0;
    for(var j=0;j<arr.length;j++){
      FeeItemGrid.setRowColData(y,x,arr[j][0]);
      FeeItemGrid.setRowColData(y,x+1,arr[j][1]);
      x += 3;
      if(x==13&&j<arr.length-1){
        FeeItemGrid.addOne();
        x=1;
        y+=1;
      }
    }
  }
}

//填充社保分段信息
function querySecuSpan(){  
    SDSql = "select CODENAME from LDCODE WHERE CODETYPE ='llsecuspan' order by code";
	  turnPage.queryModal(SDSql,SecuSpanGrid);
	  FeeSql = "select lowamnt,midamnt,highamnt1,planfee,SupInHosFee,SuperAmnt,SupInHosFee+SuperAmnt,SelfPay1,ApplyAmnt from "
	  +" llsecurityreceipt where MainFeeNo='"+fm.MainFeeNo.value+"'"
	  var frr = easyExecSql(FeeSql);
	  if(frr){
	  	SecuSpanGrid.setRowColData(0,4,"0");
	  	SecuSpanGrid.setRowColData(0,5,frr[0][0]);
	  	SecuSpanGrid.setRowColData(1,4,frr[0][3]);
	  	SecuSpanGrid.setRowColData(1,5,frr[0][1]);
	  	SecuSpanGrid.setRowColData(2,4,frr[0][4]);
	  	SecuSpanGrid.setRowColData(2,5,frr[0][2]);
	  	SecuSpanGrid.setRowColData(3,4,"0");
	  	SecuSpanGrid.setRowColData(3,5,frr[0][5]);
	  	SecuSpanGrid.setRowColData(4,4,frr[0][6]);
	  	SecuSpanGrid.setRowColData(4,5,frr[0][7]);
	  }
}