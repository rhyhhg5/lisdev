<html>
<%
//Name：LLCustomerMail.jsp
//Function：客户信箱页面
//Date：2004-12-23 16:49:22
//Author：Xx
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 		String CurrentDate= PubFun.getCurrentDate();   
    String AheadDays="-30";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
 %>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLCustomerMail.js"></SCRIPT>
   <%@include file="LLCustomerMailInit.jsp"%>
   <script language="javascript">
   function initDate(){
      fm.RgtDateS.value="<%=afterdate%>";
      fm.RgtDateE.value="<%=CurrentDate%>";
      var usercode="<%=Operator%>";
      var comcode="<%=Comcode%>";
      fm.ManageCom.value=comcode;
      fm.Operator.value="<%=Operator%>";
     
   }
   </script>
 </head>
<body  onload="initDate();initForm();" >
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         理赔案件信箱
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
       <table  class= common>
	<TR  class= common8>
	    
		<TD  class= common8>
			<input type="radio" name="typeRadio"  value="0"  onclick="easyQuery()">咨询类 
			<input type="radio" name="typeRadio"   value="1"  onclick="easyQuery()">通知类  
			<input type="radio" name="typeRadio"  checked value="3" onclick="easyQuery()">申请类
			<input type="radio" name="typeRadio" value="4" onclick="easyQuery()">申诉类
			<input type="radio" name="typeRadio" value="5" onclick="easyQuery()">纠错类	
		 </td>
	 </tr>
	 </table>
	 </DIV>
	 <Div  id= "divLLcaseRgtstate" style= "display: ''">
	 <table  class= common>
	 <TR  class= common8>
	     
		 <td class= common8>			 
			 <input type="radio" value="01" checked name="RgtState" onclick="easyQuery()">未结案</input>
			 <input type="radio" value="02" name="RgtState" onclick="easyQuery()">结案</input>
			 <input type="radio" value="03" name="RgtState" onclick="easyQuery()">通知</input>
			 <input type="radio" value="04" name="RgtState" onclick="easyQuery()">所有案件</input>
		 </TD>
	</TR>
	</table>
	</DIV>
       
     </DIV>

     <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput2);">
         </TD>
         <TD class= titleImg>
         客户信箱
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
	<TR  class= common8>
			<Input type=hidden name=OrganCode ondblClick="showCodeList('station',[this],[0]);" onkeyup="showCodeListKey('station',[this],[0]);" onkeydown="QueryOnKeyDown()" >
			<TD  class= title>客户姓名</TD><TD  class= input> 
			<Input class=common8 name=CustomerName onkeydown="QueryOnKeyDown()" ></TD>	
			<TD  class= title>客户号</TD><TD  class= input> 
			<Input class=common8 name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>
			<TD  class= title>证件号码</TD><TD  class= input> 
			<Input class=common8 name=IDNo onkeydown="QueryOnKeyDown()"></TD>			
	</TR>
	<TR  class= common8>
			<TD  class= title>团体批次号</TD><TD  class= input> 
				<Input class=common8 name=RgtNo onkeydown="QueryOnKeyDown()"></TD>
			<TD  class= title>理赔号</TD><TD  class= input> 
				<Input class=common8 name=CaseNo onkeydown="QueryOnKeyDown()">
			</TD>
				
	
	</TR>
	<TR  class= common8>
		<TD  class= title>管理机构</TD>
        <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          	
		<TD  class= title8>受理日期从</TD>
		<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
		<TD  class= title8>到</TD>
		<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
	</tr>
	<TR  class= common8>
	          	
	<TD  class= title>险种类型</TD>
        <TD  class= input> <input class="codeno" CodeData="0|2^1|个险^2|团险"  verify="险种类型|notnull" elementtype=nacessary name=ContType ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeydown="QueryOnKeyDown();" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);"><input class=codename name=ContTypeName></TD> 
    <TD  class= title>受理类型</TD>
        <TD  class= input> <input class="codeno" CodeData="0|8^0|全部^1|常规^2|批次导入^3|医保通^4|意外险平台^5|社保补充平台^6|上海医保卡平台^7|微医平台^8|外包^9|微信（PICC健康生活）线上受理 "  name=RgtType ondblclick="return showCodeListEx('RgtType',[this,RgtTypeName],[0,1],null,null,null,1);" onkeydown="QueryOnKeyDown();" onkeyup="return showCodeListKeyEx('RgtType',[this,RgtTypeName],[0,1]);" value="0"><input class=codename name=RgtTypeName value="全部"></TD>        
     
		
	</tr>
       </table>
     </Div>

    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>
      <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
        <TR  class= common>
	<TD  class= title8>处理</TD>
	<TD  class= input8><!--<input class=codeno CodeData="0|8^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定^7|撤件^8|材料退回登记^9|理赔人变更^10|理赔二核 "  name=DealWith ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);">  --> <input class=codeno CodeData="0|8^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定^7|撤件^8|材料退回登记^9|理赔人变更 "  name=DealWith ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"><input class=codename name=DealWithName></TD>
	<TD  class= title8>查询</TD>
	<TD  class= input8> <input class=codeno CodeData="0|8^1|通知咨询^2|理赔申请^3|理赔帐单^4|疾病记录^5|案件理算^6|理赔给付^7|历史赔付^8|待处理人查询"  name=Query ondblclick="return showCodeListEx('Query',[this,QueryName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Query',[this,QueryName],[0,1],null,null,null,1);"><input class=codename name=QueryName></TD>
	<TD  class= title8>打印</TD>
	<TD  class= input8> <input class=codeno CodeData="0|6^0|给付通知书打印^1|给付明细表打印^2|给付凭证打印^4|补充材料打印^5|拒付通知书打印^6|账户对账单"  name=Printf ondblclick="return showCodeListEx('Printf',[this,PrintfName],[0,1]);" onkeyup="return showCodeListKeyEx('Printf',[this,PrintfName],[0,1]);"><input class=codename name=PrintfName></TD>
        </TR>	
        <TR  class= common>
	
        </TR>
       </table>
     </Div>
      
    </Div>
      
	<Div id= "divPage" align=center style= "display: 'none' ">
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
	</Div>

    
 <Div style= "display: 'none' ">   
 <hr/>       
</Div>
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="ComCode" >
			<Input type="hidden" class= common name="GrpRgtNo">
			<Input type="hidden" class= common name="Notice" >
			<Input type="hidden" class= common name="Detail" >
			<Input type="hidden" class= common name="GrpDetail" >
			<Input type="hidden" class= common name="SingleCaseNo" >
			<Input type="hidden" class= common name="selno" >
			<Input type="hidden" class= common name="StartCaseNo">
			<Input type="hidden" class= common name="EndCaseNo">
			<Input type="hidden" class= common name="GetDetail">
			<Input type="hidden" class= common name="Operator">
			<Input type="hidden" class= common name="CaseRelaNo">
  	</form>
  	<iframe name="printfrm" src="" width=0 height=0></iframe>
  	<form method=post id=printform target="printfrm" action="">
      		<input type="hidden" name=filename value=""/>
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
