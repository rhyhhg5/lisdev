<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<title>受益人信息</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./CaseCommon.js"></SCRIPT>
  <script src="./LLBnfInput.js"></script> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLBnfInputInit.jsp"%>
 <script type="text/javascript"> 
  var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";
 
 </script>
</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm target=fraSubmit method=post>
<Div  id= "divBnfLive" style= "display: ''" align = center>
<Div align="left">
  <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg >
    	受益人
    		</TD>
    	</TR>
    </Table>    	
</div>
<div id='divLDPerson1'>
<table  class= common>
   
     <TR  class= common8>
<TD  class= title8>理赔号</TD><TD  class= input8><input class= common name="CaseNo"></TD>   
<TD  class= title8>客户号</TD><TD  class= input8><input class= common name="CustomerNo"></TD>
<TD  class= title8>客户姓名</TD><TD  class= input8><input class= common name="CustomerName"></TD>

</TR>
</table>

</div>
<Div  id= "divpaymode" style= "display: 'none'">
<table class= common>
	<TR  class= common8> 
	 	<TD  class= title8>案件赔款</TD><TD  class= input8><Input class="readonly"  name="RealPay"></TD>
	 	<TD  class= title8></TD><TD  class= input8><INPUT TYPE="checkBox" NAME="PrePaidFlag" value="01"> 回销预付赔款</TD>
	</TR>
	<TR  class= common8> 
		 <TD  class= title8>回销预付赔款金额</TD><TD  class= input8><Input class="readonly"  name="Prepaid"></TD>
		 <TD  class= title8>实际给付金额</TD><TD  class= input8 ><Input class="readonly"  name="Pay"></TD>
		 <TD class= title8>领取人证件类型</TD>
            <TD class= input8><Input class=codeno name=DrawerIDType onclick="return showCodeList('IDType',[this,DrawerIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,DrawerIDTypeName],[0,1]);"  elementtype="" verify="" ><Input class="codename"  name=DrawerIDTypeName ></TD>				
	 </TR>
	 <TR  class= common8> 
		 <TD  class= title8>领取方式</TD><TD  class= input8><Input class="codeNo"  elementtype=nacessary name=paymode ondblclick="return showCodeList('paymode',[this,paymodeName],[0,1],null,str,'1');" onkeyup="return showCodeListKey('paymode',[this,paymodeName],[0,1],null,str,'1');" ><input class=codename name=paymodeName></TD>
		 <TD  class= title8>领取人</TD><TD  class= input8 ><Input class="common"  name=Drawer ></TD>
		 <TD  class= title8>领取人证件号码</TD><TD  class= input8 ><Input class="common"  name=DrawerID ></TD>
	 </TR>
</table>
</div>

<div id='divBankAcc'>
		<table  class= common>
				<TR  class= common8>
				  
						<TD  class= title8>银行编码</TD><TD  class= input8><Input class="codeNo"  elementtype=nacessary name=BankCode ondblclick="return showCodeList('llbank',[this,BankName],[0,1]);" onkeyup="return showCodeListKey('llbank',[this,BankName],[0,1]);" ><input class=codename name=BankName></TD>
						<TD  class= title8>账号</TD><TD  class= input8><input class= common name="BankAccNo"></TD>
						<TD  class= title8>账户名</TD><TD  class= input8><input class= common name="AccName"></TD>
				
				</TR>
				
				<TR  class= common>
					<TD  class= title colspan="6">更改账户原因</TD>
				</TR>
				<TR  class= common>
					<TD  class= input colspan="6">
					    <textarea name="ModifyReason" cols="100%" rows="3" witdh=25% class="common"></textarea>
					</TD>
				</TR>
		</table>
</div>
<Div align="left">
  <input name="Modify" style="display:''"  class=cssButton type=button value="修 改" onclick="modify();">
  <input name="Return" style="display:''"  class=cssButton type=button value="返 回" onclick="top.close();">     
  <input name="AskIn" style="display:''"  class=cssButton type=button style='width: 70px;' value="查看扫描件" onclick="ScanQuery()">
  <input name="GiveConfirm" style="display:''"  class=cssButton type=button value="给付确认" onclick="giveConfirm();">     
  </div>
</div>
 <Div  id= "divBnf" style= "display: ''" align = center>
  <Div align="left">
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBnfList);">
    		</TD>
    		<TD class= titleImg>
    			 身故受益人
    		</TD> 
    	</TR>
    </Table>    	
   </div>
 <Div  id= "divBnfList" align = center style= "display: ''">
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanBnfGrid" ></span> 
  	</TD>
      </TR>
    </Table>
    <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
  </div>
    <div id="div1" align=left>
    <INPUT VALUE="保  存" class= cssButton TYPE=button onclick="submitForm();">
    <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="top.close();">
    </div>			
   
        	
 </Div>	

<Div align="left">
<table >
      <tr >
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divAccountList);">
        </td>
        <td class= titleImg> 过往理赔帐户</td>
      </tr>
</table>
</Div>
<Div  id= "divAccountList" align = center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanAccountGrid" >
            </span>
          </TD>
        </TR>					
      </table>
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 		  
	
    </Div>
 
</div>
 							
<input type="hidden" id="InsuredNo" name="InsuredNo">
<input type="hidden" id="RgtNo" name="RgtNo">

<input type="hidden" id="LoadFlag" name="LoadFlag">
<input type="hidden" id="GridFlag" name="GridFlag" >
<input type=hidden id="fmtransact" name="fmtransact">
<input type=hidden id="givemoney" name="givemoney">
<input type="hidden" id="IDEndDate" name="IDEndDate" >
	<input type=hidden id="AnswerMode" name="AnswerMode">
	<input type=hidden id="Mobile" name="Mobile">
	<input type=hidden id="Email" name="Email">
	<input type=hidden id="SMSContent" name="SMSContent">
	<input type=hidden id="EmailContent" name="EmailContent">
<input type=hidden id="RgtState" name="RgtState">
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>

</html>
