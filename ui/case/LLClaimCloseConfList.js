var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";
var State="";

//#2269 （需求编号：2014-216）理赔处理中通知给付界面增加查看扫描件功能需求
//#2018 审批审定页面增加扫描件阅览功能
function ScanQuery() {
   var Count=CaseGrid.mulLineCount;
   
   if(Count==0) {
      alert("没有待展示的案件！");
	  return false;
   }
   
   var m = 0;
   for(i=0;i<Count;i++) {
 	   if(CaseGrid.getChkNo(i)==true) {
 	   	  	m++;
 	   }
   }
   if(m>1){
      alert("请只选择一个案件进行查看！");
	  return false;
   }else if(m==0){
      alert("请选择一个案件进行查看！");
	  return false;
   }
   
   for(i=0;i<Count;i++) {
 	   if(CaseGrid.getChkNo(i)==true) {
			var pathStr="./ClaimUnderwriteEasyScan.jsp?RgtNo="+CaseGrid.getRowColData(i,1)+"&SubType=LP1001&BussType=LP&BussNoType=21";
   			var newWindow=OpenWindowNew(pathStr,"查看扫描件","left");	
 	   }
   }
   
}

function easyQueryClick()
{
  var startDate = fm.RgtDateStart.value;
  var endDate = fm.RgtDateEnd.value;
  if (startDate==""||startDate==null||endDate==""||endDate==null){
  	alert("请输入受理日期");
  	return false;
  }
  
  var rgtno=fm.RgtNo.value;
  for(i = 0; i <fm.RgtState.length; i++){
	  if(fm.RgtState[i].checked){
		 State=fm.RgtState[i].value;
		 break;
	  }
  }
  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  if(State=='09'){
    var strSql = "select c.caseno,c.customername, c.customerno, m.realpay, "
           + " (select codename from ldcode where codetype='paymode' and code=c.casegetmode), "
           + " c.accname,(CASE c.accname WHEN c.customername THEN c.IDNo ELSE '' END),c.bankcode,c.bankaccno,c.accname,c.casegetmode,c.rgtno, "//	#2807
           + " case when c.prepaidflag='1' then '是' else '否' end ,"
           + " 0,(case when c.rgtstate in('11','12')  then m.realpay else 0 end) ,case when c.prepaidflag='1' then '1' else '0' end"
           + " from llcase c, llclaim m,llregister r "
  	       + " where  c.caseno=m.caseno and c.rgtstate='"+State+"' and c.rgtno=r.rgtno and (r.togetherflag is null or r.togetherflag not in ('3','4')) " 
  	       + getWherePart("c.mngcom","Comcode","like")
  	       + getWherePart("c.rgtno","RgtNo")
  	       + getWherePart("c.caseno","CaseNo")
  	       + getWherePart("r.grpname","RgtName","like")
  	       + getWherePart("r.RgtantName","RgtantName")
  	       + getWherePart("c.CustomerName","CustomerName")
  	       + getWherePart("c.CustomerNo","CustomerNo")
  	       + getWherePart("c.rgtdate","RgtDateStart",">=") 
 		   + getWherePart("c.rgtdate","RgtDateEnd","<=")
  	       + getWherePart("c.casegetmode","paymode")
  	       + " order by c.caseno";
  }else{
    var strSql = "select c.caseno,c.customername, c.customerno, m.realpay,"
           + "(select codename from ldcode where codetype='paymode' and code=j.paymode),"
           + " j.drawer, j.drawerid, j.bankcode, j.bankaccno, j.accname, j.paymode, c.rgtno, "
           + " (case when (select nvl(sum(-pay),0) from ljagetclaim where othernotype='Y' and actugetno=j.actugetno) <>0 then '是' else '否' end),"
           + " (select nvl(sum(-pay),0) from ljagetclaim where othernotype='Y' and actugetno=j.actugetno ), "
           + " (select nvl(sum(pay),0) from ljagetclaim where othernotype in ('5','Y') and actugetno=j.actugetno ) "
           + " from llcase c, llclaim m,llregister r, ljaget j "
  	       + " where  c.caseno=m.caseno and c.rgtstate='"+State+"' and c.rgtno=r.rgtno AND j.otherno=c.caseno "
  	       + " AND j.othernotype='5' AND (j.finstate IS NULL or j.finstate<>'FC') and (r.togetherflag is null or r.togetherflag not in ('3','4')) " 
  	       + getWherePart("c.mngcom","Comcode","like")
  	       + getWherePart("c.rgtno","RgtNo")
  	       + getWherePart("c.caseno","CaseNo")
  	       + getWherePart("r.grpname","RgtName","like")
  	       + getWherePart("r.RgtantName","RgtantName")
  	       + getWherePart("c.CustomerName","CustomerName")
  	       + getWherePart("c.CustomerNo","CustomerNo")
  	       + getWherePart("c.rgtdate","RgtDateStart",">=") 
 		   + getWherePart("c.rgtdate","RgtDateEnd","<=")
  	       + getWherePart("c.casegetmode","paymode")
  	       + " order by c.caseno";
  }
  turnPage.queryModal(strSql,CaseGrid);
  showInfo.close();

  if(rgtno!="") {
	var sql2 = "select rgtno from llregister where rgtno='"+rgtno+"' and togetherflag='3'";
	if (easyExecSql(sql2)!=null) {
	    alert("该批次为团体统一给付");
        return false;
    }
    var sql3 = "select rgtno from llregister where rgtno='"+rgtno+"' and togetherflag='4'";
	if (easyExecSql(sql3)!=null) {
	    alert("该批次为团体统一给付");
        return false;
    }
    var sql1 = "select caseno from llcase where rgtno='"+rgtno+"' and rgtstate not in('09','12','11','14')";
    if(easyExecSql(sql1)!=null) {
       alert("该批次下有未结案件");
       return false;
    }
  }
    rgtno = fm.RgtNo.value;
	var caseno = fm.CaseNo.value;
	if (caseno != "") {
		var sql4 = "select caseno from llcase a where a.caseno='"+caseno+"'" +
				" and exists (select 1 from llregister where rgtno=a.rgtno and togetherflag  in('3','4'))";
		if (easyExecSql(sql4) != null) {
			alert("请在【团体结案】页面进行给付确认。");
			initCaseGrid();
		}
	}
}


function UWClaim()
{
	var selno = CaseGrid.getSelNo();
	if (selno <= 0)
	{
	      alert("请选择要审批审定的理赔案件");
	      return ;
	}
	 	
	var CaseNo = CaseGrid.getRowColData(selno - 1, 1);  
	var varSrc = "&CaseNo=" + CaseNo;
	var newWindow = window.open("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function ClaimGetPrint()
{
	
	var Count=CaseGrid.mulLineCount;
  if(Count==0){
			alert("没有案件!");
			  return false;
					}
  var chkFlag=false;
  for (i=0;i<Count;i++){
    if(CaseGrid.getChkNo(i)==true){
      chkFlag=true;
    }
  }
  if (chkFlag==false){
    alert("请选择要打印的案件!");
    return false;
  }

		 fm.all('Operator').value = "PRINT";
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	   fm.action ="./LLBatchPrtConfSave.jsp";
	   fm.submit();
	
	
	
}


//#2684关于理赔环节黑名单监测规则修改的需求  added by zqs
function checkBlacklist(checkName,checkId){
	//需要验证的证件号与人名都为空
	if((checkId==""||checkId==null)&&(checkName==""||checkName==null)){
		return true;
	}
	//需要验证的证件号为空
	if(checkId==""||checkId==null){
		var strblack="select 1 from lcblacklist where trim(name)in('"+checkName+"') and (idno='' or idno is null) with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   	   	  		//alert("若“证件号码”在黑名单库中为空");
   	   	  		if(!confirm("客户或者领取人"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      		return false;
   	   	    	}
		}
		return true;
	}
	//需要验证的姓名为空
	if(checkName==""||checkName==null){
		var strblack=" select 1 from lcblacklist where trim(idno)in('"+checkId+"')with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   			//alert("姓名”不匹配为空，“证件号码”匹配");
   			if(!confirm("客户或者领取人为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		return true;
	}
	
	//1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
	var strblack=" select name,idno from lcblacklist where trim(name)in('"+checkName+"') or trim(idno)in('"+checkId+"') with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   		var strblack1=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and trim(idno)in('"+checkId+"') with ur";
   		var arrblack1=easyExecSql(strblack1);
   		if(arrblack1){
   			alert("客户或者领取人"+checkName+"为黑名单客户");
   	   	  	return false;
   		}
   		var strblack2=" select 1 from lcblacklist where trim(idno)in('"+checkId+"') with ur";
   		var arrblack2=easyExecSql(strblack2);
   		if(arrblack2){
   			if(!confirm("客户或者领取人"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		var strblack3=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and (trim(idno)in('') or idno is null) with ur";
   		var arrblack3=easyExecSql(strblack3);
   		if(arrblack3){
   			if(!confirm("客户或者领取人"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
     }
     return true;
}


function giveGrpConfirm()
{
  var rgtno=fm.RgtNo.value;

  if(rgtno=="") {
     alert("请输入批次号！");
     return false;
  }
  
  if(fm.IfConfirm.value!='OK') {
     alert("请先查询确认！");
     return false;
  }
  
  var sql1 = "select caseno from llcase where rgtno='"+rgtno+"' and rgtstate not in('09','12','11','14')";
  if(easyExecSql(sql1)!=null) {
     alert("该批次下有未结案件");
     return false;
  }
  
  var Count=CaseGrid.mulLineCount;
  if(Count==0) {
  	 alert("该批次下无给付案件");
  	 return false;
  }
   
  for(i=0;i<Count;i++){
       //556反洗钱黑名单客户的理赔监测功能
       //#2407 理赔模块黑名单控制调整
  	   /*var strblack=" select 1 from lcblacklist where trim(name)in('"+CaseGrid.getRowColData(i,2)+"','"+CaseGrid.getRowColData(i,6)+"') with ur";
   	   var crrblack=easyExecSql(strblack);
   	   if(crrblack){
    	   if(!confirm("客户'"+CaseGrid.getRowColData(i,2)+"'或者领取人'"+CaseGrid.getRowColData(i,6)+"'为黑名单客户,是否继续处理此案件?原程序验证")){
    	       return false;
    	   }   	   
       }*/
       //细化#2407 理赔模块黑名单控制调整==》#2684关于理赔环节黑名单监测规则修改的需求
       var checkName1 = CaseGrid.getRowColData(i,2);
       var checkcaseno1 = CaseGrid.getRowColData(i,1);
       var checkId1 =0;
       var strblack=" select IDNo from llcase where caseno= '"+ checkcaseno1 +"' with ur";
   	   var arrblack=easyExecSql(strblack);
   	   if(arrblack){
   	   	checkId1 = arrblack[0][0].trim();
   	   }
   	   if(!checkBlacklist(checkName1,checkId1)){
       	return false;
       }
       var checkName2 = CaseGrid.getRowColData(i,6);
       var checkId2 = CaseGrid.getRowColData(i,7);
       if(checkName1!=checkName2){
       	if(!checkBlacklist(checkName2,checkId2)){
       	return false;
       	}
       }

     
  	  if(CaseGrid.getRowColData(i,11)==""||CaseGrid.getRowColData(i,11)==null) {
  	     alert("第"+(i+1)+"行给付方式错误，给付方式不能为空!");	
  	     return false;
  	  }
  	  
  	  if(CaseGrid.getRowColData(i,11)=="1"||CaseGrid.getRowColData(i,11)=="2") {
  	     if(CaseGrid.getRowColData(i,6)=="") {
  	     	alert("第"+(i+1)+"行是现金支付，领款人不能为空!");
  	     	return false;
  	     }
  	     if(CaseGrid.getRowColData(i,7)=="") {
  	     	alert("第"+(i+1)+"行是现金支付，领款人身份证不能为空!");
  	     	return false;
  	     }
  	  }else{
  	     if(CaseGrid.getRowColData(i,8)=="") {
  	     	alert("第"+(i+1)+"行是银行支付，银行编码不能为空!");
  	     	return false;
  	     }
  	     
  	     if(CaseGrid.getRowColData(i,9)=="") {
  	     	alert("第"+(i+1)+"行是银行支付，银行账号不能为空!");
  	     	return false;
  	     }
		 if(CaseGrid.getRowColData(i,10)=="") {
		 	alert("第"+(i+1)+"行是银行支付，账户名不能为空!");
		 	return false;
		 }					
	  }	
	  
	  		//---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
		   var PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a " 
		   		+" where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and caseno='"+ CaseGrid.getRowColData(i,1) +"')"
		   var tPrepaidBala=easyExecSql(PrepaidBalaSql);

		if ((CaseGrid.getRowColData(i, 4) > 0 && tPrepaidBala != ""
				&& tPrepaidBala != null && tPrepaidBala > 0 && CaseGrid
				.getRowColData(i, 16) == "0")
				|| (CaseGrid.getRowColData(i, 4) < 0 && tPrepaidBala != ""
						&& tPrepaidBala != null && tPrepaidBala >= 0 && CaseGrid
						.getRowColData(i, 16) == "0")) {
			if (!confirm("确定案件" + CaseGrid.getRowColData(i, 1) + "不回销预付赔款?")) {
				return false;
			}
		}
		   if((tPrepaidBala == null || tPrepaidBala == "")&& CaseGrid.getRowColData(i,16)=="1")
		   {
		   		alert("案件"+CaseGrid.getRowColData(i,1)+"的被保险人投保的保单不存在预付未回销赔款!");
		   		return false;
		   }
		   //----------------End---
		   	var tPreSql="";
		    if(CaseGrid.getRowColData(i,16)=="1")
		    {
		    	 tPreSql ="select coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0),"
		    		+" (SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE X.realpay END),0) "
			 		+" FROM (SELECT caseno,grpcontno,grppolno,sum(realpay) realpay FROM llclaimdetail "
			 		+" GROUP BY caseno,grpcontno,grppolno) AS X, "
			 		+" llprepaidgrppol Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno AND X.caseno=r.caseno), "
			 		+" (select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno)- "
			 		+" (SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE X.realpay END),0) "
			 		+" FROM (SELECT caseno,grpcontno,grppolno,sum(realpay) realpay FROM llclaimdetail "
			 		+" GROUP BY caseno,grpcontno,grppolno) AS X, "
			 		+" llprepaidgrppol Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno AND X.caseno=r.caseno) "
		    		+" from llcase r where caseno='"+ CaseGrid.getRowColData(i,1) +"'"
		    }else if(CaseGrid.getRowColData(i,16)=="0" ||CaseGrid.getRowColData(i,16)== null ||CaseGrid.getRowColData(i,16)==""){
		        tPreSql ="select coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0),"
		    		+" '0',coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0) "
		    		+" from llcase r where caseno='"+ CaseGrid.getRowColData(i,1) +"'"  
		    }
		    var tPrepaidArr = easyExecSql(tPreSql);
		   	var tRealpay = tPrepaidArr[0][0];
			var tPrepaid = tPrepaidArr[0][1];
			var tPay = tPrepaidArr[0][2];
		    if (tPrepaid != 0) {
		      //if (!confirm("案件"+CaseGrid.getRowColData(i,1)+"赔款"+tRealpay+",回销预付赔款"+tPrepaid+",实际给付"+tPay+"。是否继续")) {
		      //    return false;
		      //}
		      if (!confirm("案件"+CaseGrid.getRowColData(i,1)+"赔款"+tRealpay+",回销预付赔款"+tPrepaid+"。是否继续?")) {
		          return false;
		      }
		    }		
  }
  
  if(confirm("确认对该批次下所有案件进行给付确认？")) {
     fm.all('Operator').value =  "INSERT||MAIN";
     fm.givegrpconfire1.disabled=true;
     var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	 fm.action="./LLClaimCloseGrpConfListSave.jsp"; 
	 fm.submit();
  }
}

function giveConfirm()
{
   var Count=CaseGrid.mulLineCount;
   
   if(Count==0) {
      alert("没有待给付确定的案件");
	  return false;
   }
   
   if(State!="09") {
   	  alert("案件已确认给付!");
   	  return false;
   }
	
   var chkFlag=false;
   for(i=0;i<Count;i++) {
 	   if(CaseGrid.getChkNo(i)==true) {
 	   	  	chkFlag=true;
 	   	  	if(CaseGrid.getRowColData(i,11)==""||CaseGrid.getRowColData(i,11)==null) {
 	   	     	alert("给付方式不能为空!");	
 	   	     	return false;
		  	}
		  
		  	if(CaseGrid.getRowColData(i,11)=="1"||CaseGrid.getRowColData(i,11)=="2") {
		     	if(CaseGrid.getRowColData(i,6)=="") {
		     		alert("现金支付，领款人不能为空!");
					return false;
			 	}
			 
			 	if(CaseGrid.getRowColData(i,7)=="") {
					alert("现金支付，领款人身份证不能为空!");
					return false;
			 	}
		  	}else{
		     	if(CaseGrid.getRowColData(i,8)=="") {
					alert("银行支付，银行编码不能为空!");
					return false;
			 	}
			 	if(CaseGrid.getRowColData(i,9)=="") {
					alert("银行支付，银行账号不能为空!");
					return false;
			 	}
			 	if(CaseGrid.getRowColData(i,10)=="") {
					alert("银行支付，账户名不能为空!");
					return false;
			 	}					
		  	}
		  //#3312 校验赔款领款人与被保人 start
		  	var CaseSql = "select CHECKGRPCONT(rgtobjno) from llregister where rgtno = (select rgtno from llcase where caseno='"+CaseGrid.getRowColData(i,1)+"') with ur";
		  	var tCase=easyExecSql(CaseSql);
		  	if(tCase[0][0]!="Y") {		  		
		  		if(CaseGrid.getRowColData(i,11)=="4") {
		  			var customerName = CaseGrid.getRowColData(i,2);
		  			var Drawer = CaseGrid.getRowColData(i,6);
		  			if(customerName!=Drawer) {
		  				alert("当赔款领款人和被保险人非同一人时，给付方式不能选择银行转账!");
		  				return false;
		  			}
		  		}
		  	}
		  //#3312 校验赔款领款人与被保人 end
		    //---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
		   var PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a " 
		   		+" where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and caseno='"+ CaseGrid.getRowColData(i,1) +"')"
		   var tPrepaidBala=easyExecSql(PrepaidBalaSql);
		
			if ((CaseGrid.getRowColData(i, 4) > 0 && tPrepaidBala != ""
					&& tPrepaidBala != null && tPrepaidBala > 0 && CaseGrid
					.getRowColData(i, 16) == "0")
					|| (CaseGrid.getRowColData(i, 4) < 0 && tPrepaidBala != ""
							&& tPrepaidBala != null && tPrepaidBala >= 0 && CaseGrid
							.getRowColData(i, 16) == "0")) {
				if (!confirm("确定案件" + CaseGrid.getRowColData(i, 1) + "不回销预付赔款?")) {
					return false;
				}
			}
		   if((tPrepaidBala == null || tPrepaidBala == "")&& CaseGrid.getRowColData(i,16)=="1")
		   {
		   		alert("案件"+CaseGrid.getRowColData(i,1)+"的被保险人投保的保单不存在预付未回销赔款!");
		   		return false;
		   }
		   //----------------End---
		   	var tPreSql="";
		    if(CaseGrid.getRowColData(i,16)=="1")
		    {
		    	 tPreSql ="select coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0),"
		    		+" (SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE X.realpay END),0) "
			 		+" FROM (SELECT caseno,grpcontno,grppolno,sum(realpay) realpay FROM llclaimdetail "
			 		+" GROUP BY caseno,grpcontno,grppolno) AS X, "
			 		+" llprepaidgrppol Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno AND X.caseno=r.caseno), "
			 		+" (select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno)- "
			 		+" (SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE X.realpay END),0) "
			 		+" FROM (SELECT caseno,grpcontno,grppolno,sum(realpay) realpay FROM llclaimdetail "
			 		+" GROUP BY caseno,grpcontno,grppolno) AS X, "
			 		+" llprepaidgrppol Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno AND X.caseno=r.caseno) "
		    		+" from llcase r where caseno='"+ CaseGrid.getRowColData(i,1) +"'"
		    }else if(CaseGrid.getRowColData(i,16)=="0" ||CaseGrid.getRowColData(i,16)== null ||CaseGrid.getRowColData(i,16)==""){
		        tPreSql ="select coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0),"
		    		+" '0',coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0) "
		    		+" from llcase r where caseno='"+ CaseGrid.getRowColData(i,1) +"'"  
		    }
		    var tPrepaidArr=easyExecSql(tPreSql);
		   	var tRealpay = tPrepaidArr[0][0];
			var tPrepaid = tPrepaidArr[0][1];
			var tPay = tPrepaidArr[0][2];
		    if (tPrepaid != 0) {
		      //if (!confirm("案件"+CaseGrid.getRowColData(i,1)+"赔款"+tRealpay+",回销预付赔款"+tPrepaid+",实际给付"+tPay+"。是否继续")) {
		      //   return false;
		      //}
		      if (!confirm("案件"+CaseGrid.getRowColData(i,1)+"赔款"+tRealpay+",回销预付赔款"+tPrepaid+"。是否继续?")) {
		          return false;
		      }
		    }
   	  //556反洗钱黑名单客户的理赔监测功能
   	  //#2407 理赔模块黑名单控制调整
  	  /* var strblack=" select 1 from lcblacklist where trim(name)in('"+CaseGrid.getRowColData(i,2)+"','"+CaseGrid.getRowColData(i,6)+"') with ur";
   	   var crrblack=easyExecSql(strblack);
   	   if(crrblack){
    	   if(!confirm("该客户或者领取人为黑名单客户,是否继续处理此案件?原程序校验")){
    	       return false;
    	   }  	   
    	}  */
    	//细化#2407 理赔模块黑名单控制调整==》#2684关于理赔环节黑名单监测规则修改的需求
       var checkName1 = CaseGrid.getRowColData(i,2);
       var checkcaseno1 = CaseGrid.getRowColData(i,1);
       var checkId1 =0;
       var strblack=" select IDNo from llcase where caseno= '"+ checkcaseno1 +"' with ur";
   	   var arrblack=easyExecSql(strblack);
   	   if(arrblack){
   	   	checkId1 = arrblack[0][0].trim();
   	   }
   	   if(!checkBlacklist(checkName1,checkId1)){
       	return false;
       }
       var checkName2 = CaseGrid.getRowColData(i,6);
       var checkId2 = CaseGrid.getRowColData(i,7);
       if(checkName1!=checkName2){
       	if(!checkBlacklist(checkName2,checkId2)){
       	return false;
       	}
       }

    	//TODO:临时校验，社保案件不可给付确认
//		var tSocial = CaseGrid.getRowColData(i,1);
//		if(!checkSocial('',tSocial,'')){
//			return false;
//		}
	
		//TODO:新增效验，理赔二核时不可给付确认
		var sqlUW = "select rgtstate from llcase where caseno='"+CaseGrid.getRowColData(i,1)+"' with ur";
  		var rgtstateUW=easyExecSql(sqlUW);
		if (rgtstateUW!=null){
	     	if(rgtstateUW[0][0]=='16'){
	       		alert("理赔二核中不可给付确认，必须等待二核结束！");
	       		return false;
	     	}
		}
    	 	
		//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求

	  	if(CaseGrid.getRowColData(i,5)!="现金" && CaseGrid.getRowColData(i,5)!="现金支票"){
	  		var rdraSrc=" select RelaDrawerInsured,DrawerIDType from llcaseext where 1=1 and caseno ='"+ CaseGrid.getRowColData(i,1) +"' ";
	  		var trCase=easyExecSql(rdraSrc);
	  	  	if( (trCase[0][0] != "28") && (trCase[0][1]=="0" || trCase[0][1]=="5") ) {  
	  				var strChkIdNo=checkIdNo(trCase[0][1],CaseGrid.getRowColData(i,7),"","");
	  		  		if(strChkIdNo!=""){
	  		  			 alert("领款人证件号码不符合规则，请核实") ;
	  		  			 return false;
	  		  		}
	  			}  			 
	  		}//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
		
	   }
	}
	if (chkFlag==false) {
		alert("请选择要给付确定的案件!");
		return false;
	}
	
	fm.all('Operator').value =  "INSERT||MAIN";
	fm.giveconfirm1.disabled=true;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
}

function afterSubmit( FlagStr, content )
{
fm.giveconfirm1.disabled=false;
 fm.givegrpconfire1.disabled=false;
 fm.IfConfirm.value!='NULL';
  showInfo.close();
 	easyQueryClick();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      

  }
}

function QueryOnKeyDown(){
  var keycode = event.keyCode;

  if(keycode=="13"){
    easyQueryClick();
  }
}
function QueryOnKeyDown1(){
  var keycode = event.keyCode;

  if(keycode=="13"){
  fm.IfConfirm.value='OK';
    easyQueryClick();
  }
}