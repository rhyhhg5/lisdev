var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";

function easyQueryClick()
{
	  	
  initCaseGrid();
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  // 书写SQL语句
  
  var strSql = " SELECT C.CASENO, C.CUSTOMERNO, C.CUSTOMERNAME, R.RGTDATE, C.CALFLAG ,b.codename " + 
  	       " FROM LLCASE C, LLRegister R,ldcode b " + 
  	       " WHERE C.RGTNO = R.RGTNO and b.codetype='llrgtstate' and b.code = c.rgtstate and c.RGTSTATE in ('05','04')"
  if (fm.all('RgtNo').value != '')	//团体批次号 
  	strSql = strSql + " AND R.RGTNO = '" + fm.all('RgtNo').value + "'";  	       
  if (fm.all('CaseNo').value != '')	//理赔号
  	strSql = strSql + " AND C.CASENO = '" + fm.all('CaseNo').value + "'";
  if (fm.all('CustomerNo').value != '')	//客户号
  	strSql = strSql + " AND C.CUSTOMERNO = '" + fm.all('CustomerNo').value + "'";
  if (fm.all('CustomerName').value != '')	//客户姓名
  	strSql = strSql + " AND C.CUSTOMERNAME = '" + fm.all('CustomerName').value + "'";
  if (fm.all('RgtantName').value != '')	//申请人
  	strSql = strSql + " AND R.RGTANTNAME = '" + fm.all('RgtantName').value + "'";  	
	 	       
  //申请日期区间查询
  var blDateStart = !(fm.all('RgtDateStart').value == null || 
  			fm.all('RgtDateStart').value == "");
  var blDateEnd = !(fm.all('RgtDateEnd').value == null ||
  			fm.all('RgtDateEnd').value == "");
  
  if(blDateStart && blDateEnd) {
  	strSql = strSql + " AND R.RGTDATE BETWEEN '" + 
  			fm.all('RgtDateStart').value + "' AND '" +
  			fm.all('RgtDateEnd').value + "'";
  } else if (blDateStart) {
  	strSql = strSql + " AND R.RGTDATE > '" + fm.all('RgtDateStart').value + "'";
  } else if (blDateEnd) {
  	strSql = strSql + " AND R.RGTDATE < '" + fm.all('RgtDateEnd').value + "'";
  }

//strSql = strSql + " AND C.RGTSTATE = '03' "; //** 只筛选出已经理算过的理培案件 **
    
    strSql = strSql + " ORDER BY C.CASENO "
    
  turnPage.queryModal(strSql,CaseGrid);
  showInfo.close();

}

function UWClaim()
{
	var selno = CaseGrid.getSelNo();
	if (selno <= 0)
	{
	      alert("请选择要审批审定的理赔案件");
	      return ;
	}
	 	
	var CaseNo = CaseGrid.getRowColData(selno - 1, 1);  
	var varSrc = "&CaseNo=" + CaseNo;
	var newWindow = window.open("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function ClaimUWPrint()
{
	
	var selno = CaseGrid.getSelNo();
	if (selno <= 0)
	{
	      alert("请选择要审批审定的理赔案件");
	      return ;
	}
	 	
	var CaseNo = CaseGrid.getRowColData(selno - 1, 1);  		  	
  	var newWindow = window.open("../f1print/ClaimUWPrintChk.jsp?hiddenCaseNo="+CaseNo );
}