
var showInfo;

var turnPage = new turnPageClass();


//***************************************************
//* 验证必填项 验证药品名中是否有重复名称的药品
//***************************************************
function checkValid()
{
  //1、新填入药品与数据库库中药品是否有相同药品名
	var rowNum=RestrictedDrugGrid.mulLineCount;
	//alert("rowNum  "+rowNum);
	for(i=0;i<rowNum;i++){
	  //每行的药品名
	  RTDrugName=RestrictedDrugGrid.getRowColDataByName(i,"RestrictedDrugCodeName");
	  if(RTDrugName==""){
	  	 alert("第"+ (i+1) +"行药品名为空");
	  	 return false;
	  }
	  var strSQL="SELECT P.CodeName"
  	  + " FROM ldcode P WHERE P.CodeType ='RestrictedDrug' and P.CodeName like '" + RTDrugName + "'";
      var arrResult = easyQueryVer3(strSQL, 1, 0, 1);
      //alert("arrResult"+ arrResult);
	  if(arrResult)
	  {  
	    alert("已存在限制或禁用药品"+RTDrugName+"!"); 
	    return false;
	  }
	  //2、新填入药品中有至少一对药品名相同
	  if(rowNum>1){
		  for(j=i+1;j<rowNum;j++){
		  	RTDrugName2=RestrictedDrugGrid.getRowColDataByName(j,"RestrictedDrugCodeName");
		  	if(RTDrugName2 ==RTDrugName){
		      alert("输入信息中有重复限制或禁用药品信息"+RTDrugName2+"与"+RTDrugName); 
		      return false;
		  	}
		  }
	  }
	}

	return true;
}


//***************************************************
//* 点击‘保存’进行的操作
//***************************************************
function submitForm()
{
  
  if(!checkValid()){
    return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT";
  fm.submit(); //提交
  //alert("chenggong 3");
  
  return true;
}


//***************************************************
//* 单击“修改”进行的操作
//***************************************************

/*function updateClick()
{
  //下面增加相应的代码
  
  if (fm.all('BlacklistCode').value==null || fm.all('BlacklistCode').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('BlacklistCode').value="";
 	return;
}
  
  if (fm.all('Name').value==null || fm.all('Name').value=='')
 { 
 	alert("请先查询出要修改的数据！");
 	fm.all('Name').value="";
 	return;
}
 
  if (confirm("您确实想修改该条记录?"))
  {
 
  //showSubmitFrame(mDebug);
  
   //alert("xiu1");
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    alert("您取消了修改操作！");
  }
}*/           
//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content,aBlacklistCode)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  //fm.all('BlacklistCode').value  = aBlacklistCode;
  initForm();
}
/*function initUpdate()
{
	if(transact == "update"){
		if(tBlacklistCode == null || tBlacklistCode == ""){
			alert("修改操作时，获取黑名单人员编码失败！");
			return false;
		}
		var tSQL = "select BlacklistCode,Name,Name1,Nationality,IDNoType,(select codename from ldcode where codetype = 'idtype' and code = IDNoType),"
		+ " IDNo,Name8,Name9,Name10,Remark"
	    + " FROM LCBlackList where  1=1  and type = '0' and BlacklistCode = '"+tBlacklistCode+"'";
		
		//执行查询并返回结果
		var strQueryResult = easyExecSql(tSQL);
		if(!strQueryResult){
			alert("修改操作时，根据黑名单人员编码获取人员信息失败！");
			return false;
		}
		fm.all('BlacklistCode').value = strQueryResult[0][0];
	    fm.all('Name').value = strQueryResult[0][1];
	    fm.all('Name1').value = strQueryResult[0][2];
	    fm.all('Nationality').value = strQueryResult[0][3];
	    fm.all('IDNoType').value = strQueryResult[0][4];
	    fm.all('IDNoTypeName').value = strQueryResult[0][5];
	    fm.all('IDNo').value = strQueryResult[0][6];
	    fm.all('Name8').value = strQueryResult[0][7];
	    fm.all('Name9').value = strQueryResult[0][8];
	    fm.all('Name10').value = strQueryResult[0][9];
	    fm.all('Remark').value = strQueryResult[0][10];
	}
	return true;
}*/