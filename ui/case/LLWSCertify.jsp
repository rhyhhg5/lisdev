<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>

<%
//程序名称：
//程序功能：
//创建日期：2009-05-04
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html>
<head>
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <script src="LLWSCertify.js"></script>
    <%@include file="LLWSCertifyInit.jsp" %>
</head>

<body onload="initForm();">
	<form action="" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="titleImg">查询条件</td>
            </tr>
        </table>
        
        <div id="divCondCode" style="display:''">
            <table class="common">
                <tr class="common">
                   <!--  <td class="title8">管理机构</td>
                    <td class="input8">
                        <input class="codeNo" name="ManageCom" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" /><input class="codename" name="ManageComName" readonly="readonly" />
                    </td> -->
                    <td class="title8">结算单号</td>
                    <td class="input8">
                        <input class="common" name="PrtNo" verify="结算单号|len=11" maxlength="11" />
                    </td>
                    <td class="title8">生效日期起</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="StartCValidate" verify="生效日期起|date" />
                    </td>
                    <td class="title8">生效日期止</td>
                    <td class="input8">
                        <input class="coolDatePicker" dateFormat="short" name="EndCValidate" verify="生效日期止|date" />
                    </td>
                </tr>
                <tr class="common">
                                       
                    <TD  class= title8>客户姓名</TD>
                    <TD  class= input8>
                        <Input class=common name="CustomerName" >
                    </TD>
                      <td class="title8">证件类型</td>
                    <td class="input8">
                        <Input  onClick="showCodeList('idtype',[this,tIDTypeName],[0,1]);" onkeyup="showCodeListKeyEx('idtype',[this,tIDTypeName],[0,1]);" class=codeno name="tIDType" ><Input class= codename name=tIDTypeName >
                    </td>
                    <td class="title8">证件号码</td>
                    <td class="input8">
                        <Input class=common name="tIDNo" >
                    </td>
                </tr>
                <tr class="common">
                  
                     <td class="title8">保险卡号</td>
                    <td class="input8">
                        <input class="common" name="CardNo" verify="保险卡号|len>=11&len<=12" maxlength="12" />
                    </td>
                    <TD  class= title8></TD>
                    <TD  class= input8></TD>
                </tr>
            </table>
        </div>
    
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" value="查  询" onclick="queryCertifyList();" /> 	
                </td>
            </tr>
            
        </table>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, divImportBatchListGrid);" />
                </td>
                <td class="titleImg">卡折清单列表</td>
            </tr>
        </table>
        <div id="divCertifyListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertifyListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divCertifyListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />                     
            </div>
        </div>        
        
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnWSCertify" value="确  认" onclick="dealWSCertify();" />   
                </td>
            </tr>
        </table>
        <br>
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">被保人列表</td>
            </tr>
        </table>
        <div id="divCertInsuListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanCertInsuListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divCertInsuListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage2.firstPage();" /> 
                <input type="button" class="cssButton" value="上一页" onclick="turnPage2.previousPage();" />                  
                <input type="button" class="cssButton" value="下一页" onclick="turnPage2.nextPage();" /> 
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage2.lastPage();" />                     
            </div>
        <table>
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnWSCertify" value="返  回" onclick="returnParent();" />   
                </td>
            </tr>
        </table>
        </div>
         <br>
        <table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
					</TD>
					<TD class= titleImg>
						注意事项：
					</TD>
				</TR>
		</table>
		<Div  id= "divCare" style= "display: ''">
		 	<tr class="common"><td class="title">激活卡处理流程：</td></tr><br>
			<tr class="common"><td class="title">1、契约人员将已销售的卡信息导入核心系统。</td></tr><br>
			<tr class="common"><td class="title">2、客户在网站进行激活。</td></tr><br>
			<tr class="common"><td class="title">3、系统自动将网站激活信息导入核心系统（每日凌晨导入前一日激活数据）。</td></tr><br>
			<tr class="common"><td class="title">4、理赔人员在卡折查询处进行确认。</td></tr><br>
 			<br>
			<tr class="common"><td class="title">手撕卡处理流程：</td></tr><br>
			<tr class="common"><td class="title">1、契约人员将已销售的卡信息及被保险人信息导入核心系统。</td></tr><br>
			<tr class="common"><td class="title">2、理赔人员在卡折查询处进行确认。</td></tr><br>
            <br>
            <tr class="common"><td class="title">特殊处理：</td></tr><br>
			<tr class="common"><td class="title">如果查询到卡折信息，有激活时间，但没有被保险人列表。该情况为核心激活失败，可以直接确认，系统自动进行补激活。</td></tr><br>
		</Div>
	  <input type="hidden" name="BCardNo" >
	  <input type="hidden" name="Flag" >
    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
