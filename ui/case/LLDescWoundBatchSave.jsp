<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：LLDescWoundBatchSave.jsp
	//程序功能：
	//创建日期：
	//创建人  ：Houyd
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
	//输入参数

	//输出参数
	String FlagStr = "Fail";
	String Content = "";
	
	String FileName = "";
	int count = 0;
	String ImportPath = request.getParameter("ImportPath");
	//得到excel文件的保存路径	
	System.out.println("ImportPath: "+ImportPath);
	
	//通过操作符判断是导入数据还是修改数据
	String Operate = request.getParameter("operate");
	System.out.println("Operate: "+Operate);
	if("DELETE".equals(Operate) || "UPDATE".equals(Operate)){
		System.out.println("本次操作是更新信息");
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
	  	boolean res = true;
	  	
	  	// 准备传输数据 VData
    	VData tVData = new VData();
    	FlagStr="";
    	System.out.println("Operate:"+Operate);
    	LDCodeSchema tLDCodeSchema = new LDCodeSchema();
    	tLDCodeSchema.setCodeType("desc_wound");
    	tLDCodeSchema.setCode(request.getParameter("mCode"));
    	tLDCodeSchema.setCodeName(request.getParameter("mCodeName"));
    	tLDCodeSchema.setCodeAlias(request.getParameter("mCodeName"));
    	tLDCodeSchema.setComCode(request.getParameter("mComCode"));
    	tLDCodeSchema.setOtherSign(request.getParameter("mOtherSign"));

		tVData.add(tLDCodeSchema);
    	tVData.add(tG);
		
		LLDescWoundBatchBL tLLDescWoundBatchBL = new LLDescWoundBatchBL();
		try{
			if(!tLLDescWoundBatchBL.submitData(tVData,Operate)){
				Content = "保存失败，原因是:"+tLLDescWoundBatchBL.mErrors.getFirstError() ;
		        FlagStr = "Fail";
		        res= false;
			}
		
		}catch(Exception ex){
	      		Content = "保存失败，原因是:" + ex.toString();
	      		FlagStr = "Fail";
	    }
	    if (res){
	    	Content = " 提交成功!";
	    	FlagStr = "Succ";
	  	}
	  	else{
	    	Content = " 保存失败，原因是:" + Content;
	    	FlagStr = "Fail";
	  	}
		
	}else{
		System.out.println("本次操作是批次上载信息");
		String path = application.getRealPath("").replace('\\','/')+'/';  //application.getRealPath("")取到的路径是用"\"分隔的
		System.out.println("path: "+path);
	
		DiskFileUpload fu = new DiskFileUpload();
		// 设置允许用户上传文件大小,单位:字节
		fu.setSizeMax(10000000);
	
		// 设置最多只允许在内存中存储的数据,单位:字节
		fu.setSizeThreshold(4096);
	
		// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
		fu.setRepositoryPath(path+"temp");
		//开始读取上传信息
		List fileItems = null;
	
		try{
	  		fileItems = fu.parseRequest(request);
		}
		catch(Exception ex){
	  		ex.printStackTrace();
		}
		// 依次处理每个上传的文件
		Iterator iter = fileItems.iterator();
	
		while (iter.hasNext()) {
	  		FileItem item = (FileItem) iter.next();
	  		//忽略其他不是文件域的所有表单信息
	  		if (!item.isFormField()) {
	    		String name = item.getName();
	    		long size = item.getSize();
	    		if((name==null||name.equals("")) && size==0)
	      			continue;
	    		ImportPath= path + ImportPath;
	    		FileName = name.substring(name.lastIndexOf("\\") + 1);
	    		System.out.println("importpath："+ImportPath + FileName);
	
	    		//保存上传的文件到指定的目录
	    		try {
	      			item.write(new File(ImportPath + FileName));
	      			count = 1;
	    		}
	    		catch(Exception e) {
	      		System.out.println("upload file error ...");
	    		}
	  		}
		}
  		System.out.println("upload successfully");
  		//上传完毕，开始逻辑处理
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
	
		TransferData tTransferData = new TransferData();
	  	boolean res = true;
	  
	  	LLDescWoundBatchBL tLLDescWoundBatchBL = new LLDescWoundBatchBL(ImportPath,FileName,tG); 
	  	if (count >0)
	  	{
	    	// 准备传输数据 VData
	    	VData tVData = new VData();
	    	FlagStr="";
	    	tTransferData.setNameAndValue("FileName", FileName);
	    	tTransferData.setNameAndValue("FilePath", ImportPath);
	    	System.out.println("FileName:"+FileName);
	    	System.out.println("FilePath:"+ImportPath);
	
	    	tVData.add(tTransferData);
	    	tVData.add(tG);
	    	try{
		      	if(!tLLDescWoundBatchBL.doAdd(ImportPath, FileName)){    //提取案件信息
		        	Content = "保存失败，原因是:"+tLLDescWoundBatchBL.mErrors.getFirstError() ;
		        	FlagStr = "Fail";
		        	res= false;
		      	}
	    	}
	    	catch(Exception ex){
	      		Content = "保存失败，原因是:" + ex.toString();
	      		FlagStr = "Fail";
	    	}
	    	System.out.println("submitData Finished");
	  	}
	  	else{
	    	Content += "上载文件失败! ";
	    	FlagStr = "Fail";
	  	} 
	  	if (FlagStr.equals("Fail")){
	    	res=false;
	  	}
	
	 	//输出导如成功了多少条记录
	 	int tSuccNum = tLLDescWoundBatchBL.getSuccNum();
	 	System.out.println("tSuccNum:"+tSuccNum);
	  	if (res){
	    	Content = " 提交成功!共导入： "+tSuccNum+"条记录。";
	    	FlagStr = "Succ";
	  	}
	  	else{
	    	Content = " 保存失败，原因是:" + Content;
	    	FlagStr = "Fail";
	  	}
	  	//添加各种预处理
	}

  %>
<html>
<script language="javascript">
	if("DELETE"=="<%=Operate%>" || "UPDATE"=="<%=Operate%>"){
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}else{
		parent.fraInterface.UploadiFrame.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
 	
</script>
</html>