<%
//Name：SubmittedAffixListInit.jsp
//Function：退回材料清单初始化
//Date：2005-04-05
//Author  ：DongXin
%>
<!--用户校验类-->
<%String CaseNo= request.getParameter("CaseNo");

  String RgtNo= request.getParameter("RgtNo");
  String LoadFlag= request.getParameter("LoadFlag");
  System.out.println(LoadFlag);
  String LoadD = "";
	if(request.getParameter("LoadD")!=null){
		LoadD = request.getParameter("LoadD");
	}
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<script language="JavaScript">
var turnPage = new turnPageClass(); 
function initForm()
{
  try
  {
      fm.LoadD.value="<%=LoadD%>";
       if(fm.LoadD.value=='1')
     {
       document.getElementById('sub1').disabled = true;
       document.getElementById('sub2').disabled = true;
     }
  initAffixGrid();
  fm.CaseNo.value="<%=CaseNo%>";
  fm.RgtNo.value="<%=RgtNo%>";
  fm.LoadFlag.value="<%=LoadFlag%>"
   
  }
  catch(re)
  {
    alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
   initList();
}
//立案附件信息
function initAffixGrid()
{
    var iArray = new Array();

      try
      {
      
      
          iArray[0]=new Array("序号","30px","0",1);
          iArray[1]=new Array("材料类型","120px","0");
	      
	      iArray[2]=new Array();
      	  iArray[2][0]="材料代码";         //列名（此列为顺序号，列名无意义，而且不显示）
          iArray[2][1]="50px";         			//列宽
          iArray[2][2]=10;          			//列最大值
          iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
          iArray[2][4]="llmaffix"
          iArray[2][5]="2|3";              	                //引用代码对应第几列，'|'为分割符
          iArray[2][6]="0|1";
          iArray[2][15]="affixtypecode"
          iArray[2][17]="1"
	      
	      iArray[3]=new Array("材料名称","180px","20",0);
	      iArray[4]=new Array("提供日期","100px","20",1);
	      iArray[5]=new Array("材料属性","60px","1",3);
	      iArray[6]=new Array("份数","60px","2",1);
	      iArray[7]=new Array("缺少件数","60px","2",1);
	      iArray[8]=new Array("材料号码","60px","0",3);
	      iArray[9]=new Array("SerialNo","60px","0",3);
	      iArray[10]=new Array("材料类型代码","60px","0",3);
	
      AffixGrid = new MulLineEnter( "fm" , "AffixGrid" );
      //这些属性必须在loadMulLine前
      AffixGrid.mulLineCount = 0;
      AffixGrid.displayTitle = 1;
      AffixGrid.canChk = 1;
      AffixGrid.hiddenPlus=1;   
      AffixGrid.hiddenSubtraction=1; 
      AffixGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //AffixGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }


</script>