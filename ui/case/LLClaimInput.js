//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var Action;
var tRowNo=0;
var mSaveType="";
var turnPage = new turnPageClass(); 

//#1769 案件备注信息的录入和查看功能 add by Houyd
function showCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}
	//#1769 案件备注信息的录入和查看功能 自动弹出页面只弹一次
	//获取session中的ShowCaseRemarkFlag
	var tShowCaseRemarkFlag = fm.ShowCaseRemarkFlag.value;
	if(tShowCaseRemarkFlag == "" || tShowCaseRemarkFlag == null || tShowCaseRemarkFlag == "null"){
		//tShowCaseRemarkFlag = "1";//准备存入session的标记:0-未弹出；1-已弹出
		//alert(tShowCaseRemarkFlag);		
	}else{
		return ;
	}	
	var tSql = "select 1 from llcaseremark where caseno='"+aCaseNo+"' with ur";
	var tResult = easyExecSql(tSql);
	if(tResult != null){
		pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  		showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
  	}	
	
	var LowerStr = "select rgttype from llregister where 1=1 and rgtno = '" + aCaseNo+"' with ur";
	var tLowerStr = easyExecSql(LowerStr);
	if(tLowerStr) {
		var ttRgtType = tLowerStr[0][0];
		if(ttRgtType == "13") {
			fm.LowerQuestion.disabled=false;
		}else {
			fm.LowerQuestion.disabled=true;
		}
	}
}
function openCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}

	pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  	showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
}

function OnLower(){
	  var pathStr="./LLOnlinClaimQuestionMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&CustomerName="+fm.CustomerName.value+"&Handler="+fm.Handler.value;
	  showInfo=OpenWindowNew(pathStr,"EventInput","middle");
	}
   
function showFeeInfo()
{
    var varSrc ="";
    var newWindow = window.open("./FrameMainFAZL.jsp?Interface=ICaseCureCalInput.jsp"+varSrc,"ICaseCureInput",'top=0,left=0,width=800,height=550,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,status=0');
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  if (fm.ClmNo.value!="")
  {
  	alert("请保存修改信息");
  	return;
  }
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if (confirm("您确实想修改该记录吗?"))
  {
    //下面增加相应的代码
    if (fm.ClmNo.value=="")
    {
      alert("请用新增保存");
      return;
    }
    Action = "UPDATE";
    showDiv(operateButton,"false");
    showDiv(inputButton,"true");
  }
  else
  {
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  Action = "QUERY";
  //window.showModelessDialog("./ProposalQuery.jsp",window,"dialogWidth=15cm;dialogHeight=12cm");
  initForm();
  window.open("./FrameClaimQuery.jsp");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  alert("您不能执行删除操作！！！");
  return;

}
//Click事件，当点击“选择责任”按钮时触发该函数
function chooseDuty()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  if(cPolNo=="")
  {
     alert("您必须先录入投保单号才能修改该投保单的责任项。");
     return false
  }
//  showModalDialog("./FrameMain.jsp?Interface=ChooseDutyInput.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
  window.open("./FrameMain.jsp?Interface=ChooseDutyInput.jsp&PolNo="+cPolNo);
  return true
}

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  if(cPolNo=="")
  {
     alert("您必须先录入投保单号才能察看该投保单的责任项。");
     return false
  }
  showModalDialog("./ProposalDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
//  window.open("./ProposalDuty.jsp?PolNo="+cPolNo);
}

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  //showModalDialog("./ProposalFee.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");
  window.open("./ProposalFee.jsp?PolNo="+cPolNo);

}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function submitFormSurvery()
{
}

function checkData(){
	var rowNum = ClaimPolGrid.mulLineCount ;
	
	if(rowNum<=0){
		alert("请选择赔付保单明细！");
		return false;
	}
	
	var i = 0 ;
	for(i ; i < rowNum ; i++){
		var cn = ClaimPolGrid.getRowColData(i,1);
		var rn = ClaimPolGrid.getRowColData(i,2);
		var dc = ClaimPolGrid.getRowColData(i,3);  
		                 
		if( cn.trim()=="" || cn==null || cn.trim()==''){
			alert("请选择相应的保单号，保单号不能为空！") ;
			return false ;
		}
		
		if(rn.trim()=="" || rn==null || rn.trim()==''){
			alert("请选择保单"+cn+"对应的险种名称！") ;
			return false ;
		}
		
		if(dc.trim()=="" || dc==null || dc.trim()==''){
			alert("请选择保单"+cn+"下的"+rn+"险种对应的给付责任类型！") ;
			return false;
		}
		
		//TODO：普通案件，不可添加社保保单
		var tSocial = fm.RgtNo.value;
		if(!checkSocial(cn,tSocial,'normal')){
			return false;
		}
		
		//伤残	add by Houyd #1777 新伤残评定标准及代码系统定义
		//对于新增的【伤残信息】，需要作为赔付意外伤残责任的必录校验项目，
		//赔付意外伤残责任时，检录页面的【伤残信息】和【残疾信息】二者必录其一
//		if(dc.trim()=="300" || dc.trim()=="301" || dc.trim()=="302" || dc.trim()=="303"){
//			var strSQL3="select 1 from llcaseinfo where caseno='"+ fm.CaseNo.value +"' and caserelano is not null with ur";
//	 		var crr = easyExecSql(strSQL3 );
//	 		if ( !crr )
//	 		{
//	 			alert("赔付残疾责任时，检录页面的【伤残信息】和【残疾信息】二者必录其一！");
//	 			return false;
//	 		}
//		}	
		
	}
	
	//TODO:临时校验，社保案件不可理赔计算
//	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
	
	//TODO:新增效验，理赔二核时不可理赔计算
	var sqlUW = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstateUW=easyExecSql(sqlUW);
	if (rgtstateUW!=null){
	     if(rgtstateUW[0][0]=='16'){
	       alert("理赔二核中不能处理，必须等待二核结束！");
	       return false;
	     }
	}
	
	return true;
}
/***************************************/
function CalPay()
{
	if(!checkData()){
		return false ;
	}
	//#2995 xuyunpeng add 添加对121101险种的特殊处理
	preCheckFeeDate("121101");
	//#2995 xuyunpeng add 添加对121101险种的特殊处理
	
	//return false;
	
	// # 3337 管理式补充医疗保险B款 **start**
	
	for(var rowNum=0;rowNum<ClaimPolGrid.mulLineCount;rowNum++){
        var arr=ClaimPolGrid.getRowData(rowNum);
        var contno=arr[0];
//        alert(contno);
        var riskname=arr[1];
        var riskcode=arr[10];  //险种代码
        var getdutykind=arr[2]; //保单给付类型
        if(riskcode=="162501"){
        	var tDeathSQL ="select deathdate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
        	var tDeathDate=easyExecSql(tDeathSQL);
        	if(tDeathDate!=null && tDeathDate !="" && getdutykind !="501"){
        		alert(riskname+"业务案件，一个案件下不能同时理赔医疗险责任、身故责任。");
        		return false;
        	}else if((tDeathDate==null || tDeathDate =="") && getdutykind=="501"){
        		alert(riskname+"业务案件，一个案件下不能同时理赔医疗险责任、身故责任。");
        		return false;
        	}
        }
	}
	// #3337 团体管理式补充医疗保险B款**end**
	
	fm.Opt.value = "cal";
   
   mSaveType = "calpay";
   
    var i = 0;
  	var showStr="正在赔付计算，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	fm.submit(); //提交
}
/***************************************/
function ReCalPay()
{
  if (confirm("重算后该案件会被视为非标准业务，您确定要重算吗?"))
  {
	  fm.Opt.value = "recal";
   
    mSaveType = "calpay";
   
    var i = 0;
  	var showStr="正在赔付计算，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	fm.submit(); //提交
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  fm.Calculate.disabled=false;
  showInfo.close();
  fm.SubCalculate.disabled=false;
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    if ( mSaveType=="calpay" || mSaveType=="savepay"  )
    {
    	queryCalPay();
    }
    //执行下一步操作
  }
  initTitle();
}

//页面查询
function queryCalPay(flag)
{
	 // add new 新增字段 这地方需要做相应调整
	var strSQL = "select contno,RiskCode,b.getdutyname,a.TabFeeMoney,a.DeclineAmnt,a.pregiveAmnt,"
	   + "a.ApproveAmnt,OutDutyAmnt,OverAmnt,a.GiveTypeDesc,a.GiveReasonDesc,a.ClaimMoney,a.StandPay,a.RealPay,"
	   +"a.polno,a.clmno,a.getdutycode,a.getdutykind,a.dutycode,a.GiveType,a.GiveReason,a.caserelano "
	   +" from LLClaimdetail a, LMDutyGetClm b where  a.caseno='"+ fm.CaseNo.value +"'"
	   +" and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind"

	var arr = easyExecSql(strSQL );
	var arr1=arr;
	if (arr) displayMultiline( arr, ClaimDetailGrid);
	if ( arr!=null && arr.length>0 && (flag='init' || mSaveType=="savepay" ) )
	{ 
		ClaimDetailGrid.checkBoxAll ();
	}
	else
	{
	 	initClaimDetailGrid();	
	}
	
	var strSQL = "select contno,RiskCode,b.getdutyname,a.TabFeeMoney,a.DeclineAmnt,"
	   + "a.OutDutyRate,OutDutyAmnt,'',a.ClaimMoney,a.StandPay,a.RealPay,"
	   +"a.polno,a.clmno,a.getdutycode,a.getdutykind,a.dutycode,a.caserelano,"
	   +"case when InpFlag='3' then '否' else '是' end ,"
	   +"(select amnt from lcduty where polno=a.polno and dutycode=a.dutycode union select amnt from lbduty where polno=a.polno and dutycode=a.dutycode),(select  coalesce(sum(realpay),0)  from LLClaimdetail  l,llcase c where c.caseno=l.caseno and l.polno=a.polno and l.getdutycode=a.getdutycode and c.rgtstate in('09','11','12'))"
	   +" from LLClaimdetail a, LMDutyGetClm b where  a.caseno='"+ fm.CaseNo.value +"'"
	   +" and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind"

	var err = easyExecSql(strSQL );
	if (err) displayMultiline( err, ClaimResultGrid);
	if ( err!=null && err.length>0 && (flag='init' || mSaveType=="savepay" ) )
	{ 
		ClaimResultGrid.checkBoxAll ();
	}
	else
	{
	 	initClaimResultGrid();	
	}

       var strSQL = " select distinct contno,b.riskname,a.GetDutyKind,''," +
	            " a.StandPay,a.RealPay,a.polno,a.clmno," +
	            " a.GiveTypeDesc,a.GiveType,a.riskcode,(select amnt from lcpol where polno=a.polno union select  amnt from lbpol where polno=a.polno) "+
	            " from LLClaimpolicy a, lmrisk b " +
	            " where caseno='"+ fm.CaseNo.value +"'" +
	            " and a.riskcode = b.riskcode " +
	            " order by a.polno";
	    
	var drr = easyExecSql(strSQL );
	if (drr) displayMultiline( drr, ClaimPolGrid);
	else
	{

	 	initClaimPolGrid();	  
	}
	
	var strSQL2 ="select sum(PreGiveAmnt),sum(SelfGiveAmnt),sum(RefuseAmnt),sum(claimmoney),sum(RealPay)  "
	 +" from llclaimdetail where clmno in (select clmno from LLClaimPolicy where  caseno='"+ fm.CaseNo.value +"')";
	var brr = easyExecSql(strSQL2 );
	if ( brr )
	{
		brr[0][0]==null||brr[0][0]=='null'?'0':fm.PreGiveAmnt.value  =brr[0][0];
		brr[0][1]==null||brr[0][1]=='null'?'0':fm.SelfGiveAmnt.value =brr[0][1];
		brr[0][2]==null||brr[0][2]=='null'?'0':fm.RefuseAmnt.value   =brr[0][2];
		brr[0][3]==null||brr[0][3]=='null'?'0':fm.StandPay.value     =brr[0][3];
		brr[0][4]==null||brr[0][4]=='null'?'0':fm.RealPay.value      =brr[0][4];
		
	
	 var strSQL3="select sum(RealHospDate) from llfeemain where   caseno='"+ fm.CaseNo.value +"' and caserelano is not null and feetype='2'";
	 var crr = easyExecSql(strSQL3 );
	 if ( crr )
	 {
	 	fm.RealHospDate.value =crr[0][0];
	 }
	 var strSQL4="select RealPay,GiveTypeDesc from LLClaim where caseno='"+fm.CaseNo.value+"'";
	 var arr = easyExecSql(strSQL4);
	 if (arr!=null)
	 {
	 	arr[0][0]==null||arr[0][0]=='null'?'0':fm.AllGiveAmnt.value=arr[0][0];
	 	arr[0][1]==null||arr[0][1]=='null'?'0':fm.AllGiveType.value=arr[0][1];
	}
	var strSQL5="select coalesce(sum(retireaddfee),0) from llsecurityreceipt where  caseno='"+ fm.CaseNo.value +"'";
	var srr = easyExecSql(strSQL5 );
	if (srr!=null)
	{
	
	fm.RetireAddFee.value =srr[0][0];
	}
	
   }
   else
	{
		fm.PreGiveAmnt.value  = "";
		fm.SelfGiveAmnt.value = "";
		fm.RefuseAmnt.value = "";
		fm.StandPay.value= "";
		fm.RealPay.value= "";
		fm.RealHospDate.value = "";	
		fm.AllGiveAmnt.value = "";
		fm.AllGiveType.value = "";	
		fm.RetireAddFee.value = "";	
		
	}
	
	var strSQL6 = "select caseprop from llcase where caseno='"+ fm.CaseNo.value +"'" ;
	var arrResult6 = easyExecSql(strSQL6);
  	if(arrResult6!=null){
  		if(arrResult6[0][0]=="09"){
  			fm.EasyCase.checked = "true";
  		}
  	}
  	
  	showCaseRemark();	//#1769 案件备注信息的录入和查看功能 add by Houyd
  	//#2366 关于理赔案件处理时保单红利处理  "给付未领取保单红利  "字段添加查询**start**新增代码
  	var StrSQL7 = "select sum(pay) from LJSGetclaim where otherno='"+ fm.CaseNo.value +"' and othernotype='H'"
  	var StrSQL8 = "select sum(pay) from LJAGetclaim where otherno='"+ fm.CaseNo.value +"' and othernotype='H'"
  	var arrResult7 = easyExecSql(StrSQL7);
  	var arrResult8 = easyExecSql(StrSQL8);
  	if(arrResult7!='null'&&arrResult7!=''&&arrResult7!=null){
  		fm.BDHL.value = arrResult7[0][0];
  	}else if(arrResult8!='null'&&arrResult8!=''&&arrResult8!=null){
  		fm.BDHL.value = arrResult8[0][0];
  	}else{
  		fm.BDHL.value ="";
  	}
  	//#2366 关于理赔案件处理时保单红利处理  "给付未领取保单红利  "字段添加查询**end**新增代码
  	
	// wdx新增
	Supdiscountfactor(arr1);
}

function Supdiscountfactor(contnos) {
	if (contnos == false || contnos == "" ||contnos == null || contnos=="undefined") {
		return;
	}
	var CustomerNo = document.getElementById("CustomerNo").value;
	document.getElementById("styleRed").style.display = "none";
	var bool = false;
	// 循环每一行数据
	for ( var int = 0; int < contnos.length ; int++) {
		var contno = contnos[int][0];
		
		var sql = " select distinct prtno,riskcode from lcpol where 1=1 "
				+ "  and (StateFlag is null or StateFlag in ('1')) "
				+ "  and appflag='1' "
				+ "  and contno='"
				+ contno
				+ "' "
				+ "  and riskcode in (select riskcode from lmriskapp where 1=1  and taxoptimal='Y') ";
	if(CustomerNo!="undefined"  && CustomerNo!=null){
	sql=sql+"  and InsuredNo='"+CustomerNo+"' ";
	}
		var prtnos = easyExecSql(sql); //

		if (prtnos == false || prtnos == "" || prtnos == null || prtnos=="undefined") {
			// 没查询到就为空
			continue;
		}
		var prt = prtnos;
		
		for ( var int2 = 0; int2 < prt.length; int2++) {

			var prtno = prt[int2][0];
			var riskcode = prt[int2][1];
			if(riskcode=="122601" || riskcode=="122901"){
				// 查询到了，便判断因子数值是否大于100%
				var sql = " select distinct Supdiscountfactor from lccontsub where 1=1 and prtno='"+ prtno+"' ";
			}else{
				// 判断被保险人风险保险费档次
				var sql=" select distinct PremMult from lccontsub where 1=1 and prtno='"+prtno+"' ";					
			}	
							
			var Supdiscountfactors = easyExecSql(sql);
			if (Supdiscountfactors == false || Supdiscountfactors == ""
					|| Supdiscountfactors == null) {
				continue;
			} else {
				var Sf = Supdiscountfactors;
				for ( var int3 = 0; int3 < Sf.length; int3++) {
					var Supdiscountfactor = Sf[int3][0];
					var re = /^[0-9]+.?[0-9]*$/; // 判断字符串是否为数字 //判断正整数
													// /^[1-9]+[0-9]*]*$/
					if (!re.test(Supdiscountfactor)) {
//						alert("该保单(" + contno + ")补充医疗保险折扣因子为("
//								+ Supdiscountfactor + "),不为数字！");
					continue;
					}
					Supdiscountfactor = parseFloat(Supdiscountfactor);
					if (Supdiscountfactor >= 0 && Supdiscountfactor < 1) {
						bool = true;
						document.getElementById("styleRed").style.display = "block";
						continue;
					}else if(Supdiscountfactor==2){
						bool = true;
						document.getElementById("styleRed2").style.display = "block";
						continue;
					}	
				}
			}

		}

	}

}
//提交，保存按钮对应操作
function submitForm()
{ 
	//#3062  始
	   var xzSQL ="select riskcode from llclaimdetail where caseno='"+ fm.CaseNo.value +"'";
	   var arr2 = easyExecSql(xzSQL);
	   for(var j=0;j<arr2.length;j++){
		   var sxSQL ="select taxoptimal from lmriskapp where riskcode='"+arr2[j]+"'";
		   var arr5 = easyExecSql(sxSQL);
		   var syxz=arr5[0][0];
		   if(syxz == "Y"){
		   var zd2SQL ="select  MainFeeNo from llfeemain  where  caseno= '"+ fm.CaseNo.value +"'  ";
		   var arr1 = easyExecSql(zd2SQL);
		   for(var i=0;i<arr1.length;i++){
		   var zdSQL ="select ReceiptType from llfeemain where caseno='"+ fm.CaseNo.value +"' and MainFeeNo='"+arr1[i]+"'";
		   var arr6 = easyExecSql(zdSQL);
		   var sjlx=arr6[0][0];
		   var zxSQL ="select FeeAtti from llfeemain  where caseno='"+ fm.CaseNo.value +"' and MainFeeNo='"+arr1[i]+"'";
		   var arr7 = easyExecSql(zxSQL);
		   var zdsx = arr7[0][0]
//		收据类型：为必录项。
		   if(sjlx == null || sjlx == ""){
			   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
			   return false;
		   }
//	   2、费用项目代码：当收据类型为医保住院、生育住院、新农合-住院、普通住院时，该字段必传。
		   if(sjlx=="03" || sjlx=="06" || sjlx=="07" || sjlx=="09"){
			   var xmSQL ="select distinct 1 from LLCaseReceipt  where caseno='"+ fm.CaseNo.value +"' and MainFeeNo='"+arr1[i]+"'" ;
			   var xmfy = easyExecSql(xmSQL);
			   if(xmfy != "1"){
				   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
				   return false;
			   }

		   }	 
//	   3、账单属性：当收据类型为医保门诊、医保特殊病、医保住院、新农合住院时，账单属性 对应 简易社保结算。 
//	   当收据类型为医保审批表时，账单属性 对应 手工报销。
//	   当收据类型为分割单时， 账单属性 对应 医院 或者简易社保结算。
		   if(sjlx=="01" || sjlx=="02" || sjlx=="03" || sjlx=="07"){
			   if(zdsx != "4"){
				   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
				   return false;
			   }

		   }
		   if(sjlx=="04"){
			   if(zdsx != "2"){
				   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
				   return false;
		   }
			   
		   } 
		   if(sjlx=="10"){
			   if(zdsx != "0"  && zdsx != "4"){
				   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
				   return false;
			   }
			   var cjSQL="select IssueUnit from llfeemain where  caseno='"+ fm.CaseNo.value +"' and MainFeeNo='"+arr1[i]+"'";
			   var cjdw = easyExecSql(cjSQL);
			   if(cjdw == null || cjdw == ""){
				   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
				   return false;
			   }
		   }	
			   if(zdsx == "4"){
				   if(sjlx == "01" || sjlx == "02" || sjlx == "03" ){
						   var csSQL ="select count(distinct ItemCode) from LLFeeOtherItem where  caseno= '"+ fm.CaseNo.value +"' and ItemCode in ('301','302','303','304','313','305','306','307') and mainfeeno='"+arr1[i]+"' ";
						   var csjg = easyExecSql(csSQL);
						   if(csjg != "8"){
							   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
							   return false;
						   }
				   }
				   if(sjlx == "07" ){
					   var csSQL ="select  distinct 1 from LLFeeOtherItem where  caseno= '"+ fm.CaseNo.value +"' and ItemCode ='302' and mainfeeno='"+arr1[i]+"' ";
					   var csjg = easyExecSql(csSQL);
					   if(csjg != "1"){
						   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
						   return false;
					   }

				   }
				   if(sjlx == "10" ){
						   var csSQL ="select count(distinct ItemCode) from LLFeeOtherItem where  caseno= '"+ fm.CaseNo.value +"' and ItemCode in ('319','318') and mainfeeno='"+arr1[i]+"' ";
						   var csjg = easyExecSql(csSQL);
						   if(csjg != "2"){
							   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
							   return false;
						   }

				   }
			   } 			  
			   if(zdsx == "0"){
				   if(sjlx == "10" ){
					   var csSQL ="select  sum(preamnt)  from LLCaseReceipt where  caseno= '"+ fm.CaseNo.value +"'  and MainFeeNo='"+arr1[i]+"'" ;
					   var csjg = easyExecSql(csSQL);
					   if(csjg <= 0){
						   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
						   return false;
					   }
				   } 
			   }
			   if(zdsx == "2"){
				   if(sjlx == "04" ){
					   var csSQL ="select  distinct 1 from LLSecurityReceipt where  caseno= '"+ fm.CaseNo.value +"'  and FeeDetailNo='"+arr1[i]+"'";
					   var csjg = easyExecSql(csSQL);
					   if(csjg != "1"){
						   alert("案件赔付的险种为税优险，请按照要求录入完整信息");
						   return false;
					   }
				   }
			   }
		   }
		   }
	   }
	//#3062	终
	
   var tAccSql ="select distinct accdate from llcaserela a,llsubreport b where a.subrptno=b.subrptno and a.caseno='"+ fm.CaseNo.value +"'";
   var tAccDate = easyExecSql(tAccSql);
   if(tAccDate == null || tAccDate =="")
   {
   	 alert("出险日期查询失败");
   	 return false;
   }
   var HangUpNo;
   for(var rowNum=0;rowNum<ClaimDetailGrid.mulLineCount;rowNum++)
   {
	   if(ClaimDetailGrid.getChkNo(rowNum))
	   {
		 //3558
		      var claimRes=ClaimDetailGrid.getRowColData(rowNum,10);
		      var claimResDesc=ClaimDetailGrid.getRowColData(rowNum,11);
		      var claimMon=ClaimDetailGrid.getRowColData(rowNum,14);
		      var claimDet=ClaimDetailGrid.getRowColData(rowNum,7); //
//		      alert(claimDet);
		      if(claimMon!="" && claimMon !=null && claimDet!="" && claimDet!=null){
		    	  var claimMonValue=claimMon*1;
		    	  var claimDetValue=claimDet*1;
		    	  if(claimRes=="协议给付" && claimDetValue == 0){
			    	  alert("该案件为协议案件，请核实协议给付金额");
			    	  return false;
			      }
		    	  if(claimRes=="全额拒付"&&""==claimResDesc){
		    		  alert("当前理赔案件为全额拒付案件，请核实赔付结论依据");
			    	  return false;
		    	  }
		      }
		      
		 //3558
		   
		  
	      var tPolNo=ClaimDetailGrid.getRowColData(rowNum,15);
	      //出险日期在保费交至日期范围外，提示“出险日期YYYYMMDD在保费交至日YYYYMMDD之外，是否继续理赔”	      
		  var paySql ="select a.paytodate from lcpol a,lmriskapp b where a.riskcode=b.riskcode and b.riskprop='G' and a.polno='"+ tPolNo +"' "
		  	+" union select a.paytodate from lbpol a,lmriskapp b where a.riskcode=b.riskcode and b.riskprop='G' and a.polno='"+ tPolNo +"' ";
		  var arr = easyExecSql(paySql);
		  if(arr)
		  {
		  	var tPaytoDate = arr[0][0];		 	  	
		  	if(tAccDate >= tPaytoDate)
			{
			  	if(confirm("出险日期"+tAccDate +"在保费交至日"+ tPaytoDate +"之外，是否继续理赔")){}
				else {return;}
			}
		  }		  
		  
		  var tContNo=ClaimDetailGrid.getRowColData(rowNum,1);
		  var strsql2="select a.contno from LCContHangUpState a where a.contno='"+tContNo+"' and a.HangUpType='2' and a.state<>'0' ";   
          if(easyExecSql(strsql2))
          {HangUpNo= HangUpNo+" "+easyExecSql(strsql2);     	   }
	  }
    }
    if(HangUpNo!=""&&HangUpNo!=null)
     {
       alert("该保单"+HangUpNo.substring(10)+"被保全挂起！");
       return false;
     }    
     
     //新增校验，对于赔付正在进行结余返还操作以及已完成结余返还项目的保单，在理算确认时校验并阻断
     //add by GY 2013-1-8
     //1.先根据页面上的保单号查询该保单号对应的grpcontno
     //var GrpContSQL="select grpcontno from lccont where contno='"+tContNo+"' and grpcontno not in ('00149574000001','00092763000002','00149574000002')";
     //    add by lyc 2014-6-11 改为配置
	 var GrpContSQL="select distinct grpcontno from llclaimdetail where rgtno='"+fm.all('RgtNo').value+"' and grpcontno not in (select code from ldcode where codetype='lp_jyfh_pass')"; 
	 var mGrpContNo = easyExecSql(GrpContSQL);
     //2.根据grpcontno查询该单是否正在进行结余返回操作
     var CheckBQSQL="select 1 from lpgrpedoritem where grpcontno='"+mGrpContNo+"' and edortype = 'BJ'";
     var CheckBQResult = easyExecSql(CheckBQSQL);
     //3.判断上述sql是否存在数据，是阻断否通过
     if(CheckBQResult)
       {
        alert("该保单"+mGrpContNo+"正在进行结余返还操作或者已完成结余返还项目，不能理算确认");
        return false;
       }
     
     //#2781 xuyunpeng add 针对税优险住院天数是否超过180天的校验 start
     if(!CheckSY()){
 		return false;
 	}
     //# 2781 xuyunpeng add 针对税优险住院天数是否超过180天的校验 end
    // #2781 税优险种判断账单种类为门诊时，赔付责任**start**
     if(!checkGetDuty()){
    	 return false;
     } 
     // #2781 税优险种判断账单种类为门诊时，赔付责任**end**
     
    //对于欠缴保费，保单处于中止状态,提示并阻止进行理算
    //TODO: 该校验只针对#48 《康利人生两全保险（分红型）》
    for(var rowNum=0;rowNum<ClaimPolGrid.mulLineCount;rowNum++){
        var arr=ClaimPolGrid.getRowData(rowNum);
        var contno=arr[0];
        var riskcode=arr[10];  //险种代码
        var polno=arr[6];  //保单险种号
        
		//TODO：普通案件，不可添加社保保单
		var tSocial = fm.RgtNo.value;
		if(!checkSocial(contno,tSocial,'normal')){
			return false;
		}	
		//#2366分红险理算处理--校验
//		var sql3 = "select 1 from dual where exists(select riskcode from lmriskapp where risktype4 = '2' and riskcode='" + riskcode +"')";
//        var result3 = easyExecSql(sql3);
//        if(result3=="1"){
//        	if (confirm("保单"+ contno +"存在未领取保单红利,请确认是否与赔款一并给付？")){
//        	}else{
//	        	return false;
//	        }
//	    	//#2366若申请日期在保全领取分红日期之前提示不处理分红
//	        var sqlapp = "select appdate from llregister where rgtno='"+fm.CaseNo.value+"' ";
//	        var appdate = easyExecSql(sqlapp);
//	        if(appdate == null || appdate == ""){
//	        	alert("案件申请日期为空,无法计算分红!");
//	 	    	return false;
//	        }
//	        //最后一次领取分红的时间
//	        var sqllj = "select max(getdate) from ljabonusget where otherno='"+contno+"'  with ur";
//	        var lastdate1 = easyExecSql(sqllj);
//	        if(lastdate1 != null && lastdate1 != ""){
//	        	//申请日期早于最后分红领取时间
//	        	if(lastdate1>=appdate){
//	        		if (confirm("客户"+fm.CustomerName.value+"最后一次领取日是"+lastdate1+"，本次案件申请日期是"+appdate+"，申请日期在领取日期之前，本次案件不再计算分红信息，请确认是否继续 ？")){
//	        		}else{
//	        			return false;
//	        		}
//	        	}
//	        }
//        }
        
        
        if(riskcode == "730101" || riskcode == "332401") {
	        var sql = "select 1 from lccont where stateflag='2' and contno='" + contno +"'";
	        var result = easyExecSql(sql);
	        //alert(result);
	        
	        
	        var sql2 = "select 1 from lcpol where polno= '" + polno + "' "
	                   +"and (stateflag in ('1','2') or StateFlag is null) and AppFlag = '1' and  paytodate<payenddate "
	                   +"and paytodate<=(select accdate from LLSubReport where subrptno in( "
	                   +"select subrptno from llcaserela where caseno='"+fm.CaseNo.value+"'))";
	        var arr = easyExecSql(sql2);
	        if(arr != null && arr != ""){
	        	alert("保单" + contno + "有欠缴保费不能进行理赔！");
	        	return false;
	        }
	        if(result != null && result != ""){
	        	if(confirm("保单" + contno + "处于中止状态请确认是否继续进行理赔！")){
	        		
	        	}else{
	        	return false;
	        	}
	        }
        }
    }  
//  税优一个案件不能同时赔付健管及医疗#2869 add Alex by 2016-6-24
	  var tSYJGType = "";
	  var tSYYJType = "";
	  var tGetDutyNameJG = ": ";
	  var tGetDutyNameYL = ": ";
	for(var SYrowNum=0;SYrowNum<ClaimDetailGrid.mulLineCount;SYrowNum++)
	{
		   if(ClaimDetailGrid.getChkNo(SYrowNum))
		   {
		      var tRiskCode=ClaimDetailGrid.getRowColData(SYrowNum,2);
		      if(tRiskCode=="122901"){
		    	  var tGetDutyName=ClaimDetailGrid.getRowColData(SYrowNum,3);
		    	  if(tGetDutyName=="恶性肿瘤第二诊疗意见服务"||tGetDutyName=="恶性肿瘤门诊挂号预约服务"||tGetDutyName=="健康档案"||tGetDutyName=="健康咨询"||tGetDutyName=="健康评估"){
		    		  tSYJGType="JGTYPE";
		    		  tGetDutyNameJG = tGetDutyNameJG + tGetDutyName + " ";
		    	  }else{
		    		  tSYYJType="YJTYPE";
		    		  tGetDutyNameYL = tGetDutyNameYL + tGetDutyName + " ";
		    	  }
		      }
		      
		  }
	}
	if(tSYJGType=="JGTYPE" && tSYYJType=="YJTYPE"){
		 alert("税优产品医疗责任"+tGetDutyNameYL+"与 健康管理服务/增值服务"+tGetDutyNameJG+"不能同时理赔！");
		 return false;
	}

//1500康乐人生赔偿重大疾病后不能进行身故赔偿并且重大疾病保证金终止  
	var Caseno=fm.CaseNo.value;
	var firFi = Caseno.substring(0,1);
	if(firFi=='S'||firFi=='R'){
//		alert("111");
	}else{
		for(var KLrowNum=0;KLrowNum<ClaimDetailGrid.mulLineCount;KLrowNum++){
			if(ClaimDetailGrid.getChkNo(KLrowNum)){
				var tContNo=ClaimDetailGrid.getRowColData(KLrowNum,1);
				var tRiskCode=ClaimDetailGrid.getRowColData(KLrowNum,2);
				// 3081 **start**
				var tPolNo=ClaimDetailGrid.getRowColData(KLrowNum,15);
				// 3081 **end**
				//alert("tPolNo===="+tPolNo);
				if(tRiskCode=="231701"){
					var sql11="select l.getdutycode from llclaimdetail l ,llcase c where l.caseno=c.caseno " 
					+" and l.contno='"+tContNo+"' and l.riskcode='"+tRiskCode+"' and l.getdutykind='400' " 
					+" and l.polno='"+tPolNo+"' and  c.rgtstate in ('11','12') with ur";   //3081
					var tGetDutyCode = easyExecSql(sql11);
					if(tGetDutyCode=="350201" || tGetDutyCode=="350203"){
						var tRiskName=ClaimDetailGrid.getRowColData(KLrowNum,3);
						if(tRiskName=="重大疾病保险金"){
							alert("康乐人生个人重大疾病保险（A款）:"+tRiskName+"责任终止不能再次理赔！");
							return false;
						}else if (tRiskName=="身故保险金"){
							alert("康乐人生个人重大疾病保险（A款）:"+tRiskName+"责任终止不能再次理赔！");
							return false;
						}
				       }
			        }
	            // #3545 守望健康A险种下，仅有体检责任的 star 
				if(tRiskCode=="122101"){
					var customerno = fm.CustomerNo.value;
					var sqlCheckDuty="select 1 from lcpol a, lcduty b where a.polno=b.polno and a.contno='"+tContNo+"' and b.dutycode!='698001' and a.riskcode='122101' union select 1 from lbpol a, lbduty b where a.polno=b.polno and a.contno='"+tContNo+"' and b.dutycode!='698001' and a.riskcode='122101' with ur";
					var CheckDuty = easyExecSql(sqlCheckDuty);
					if(CheckDuty==null){ //仅有体检责任
						//是否已理赔终止
						var sqlClaimEnd="select 1 from llcaseext a,llclaimdetail b where a.caseno=b.caseno and " +
								"b.contno='"+tContNo+"' and a.claimend='1' " +
								"and exists(select 1 from llcase where caseno=a.caseno and rgtstate in('11','12')) with ur";
						var CheckClaimEnd =	easyExecSql(sqlClaimEnd);
						if(CheckClaimEnd!=null){
							var customername = fm.CustomerName.value;
							alert("被保险人:"+customername+"《健康守望补充医疗保险（A款）》已失效，不能再次理赔");
							return false;
						}
						var sqlCase="select a.caseno from llcase a,llclaimdetail b where a.caseno=b.caseno and b.contno='"+tContNo+"' and b.riskcode='122101' " +
								"and a.rgtstate not in('11','12') and a.caseno !='"+Caseno+"' " +
								"and exists(select 1 from llcaseext where caseno=a.caseno and claimend='1') with ur ";
						var CheckCase = easyExecSql(sqlCase);
						if(CheckCase!=null){ //本案件之外的所有案件
							var CheckCaseNo = CheckCase[0][0];
							alert("该保单有正在赔付《健康守望补充医疗保险（A款）》险种的理赔案件，案件号为:"+CheckCaseNo+"待其结案并通知给付后再处理本案件");
							return false;
						}
						//通过批次导入的案件
						var sqlCase2="select a.caseno from llcase a,llregister b where a.rgtno=b.rgtno and a.customerno='"+customerno+"' " +
									 "and b.rgtobjno=(select grpcontno from lcinsured where contno='"+tContNo+"' and insuredno='"+customerno+"' union select grpcontno from lbinsured where contno='"+tContNo+"' and insuredno='"+customerno+"') " +
									 "and a.caseno!='"+Caseno+"'" +
									 "and a.rgtstate not in('11','12','14') " +
									 "and exists(select 1 from llcaseext where caseno=a.caseno and claimend='1') with ur"
						var CheckCase2 = easyExecSql(sqlCase2);
						if(CheckCase2!=null){
							var CheckCaseNo2 = CheckCase2[0][0];
							alert("该保单有正在赔付《健康守望补充医疗保险（A款）》险种的理赔案件，案件号为:"+CheckCaseNo2+"待其结案并通知给付后再处理本案件");
							return false;
						}
					}
				}
				// #3545 守望健康A险种下，仅有体检责任的 end 
				// 管理式补充医疗保险（A B款）以及补充团体医疗保险（A款）赔付身故责任之后 该整个保单终止，申诉纠错除外
				   var endsql="select codename, code1 from ldcode1 where code='"+tRiskCode+"' and codetype='lpdutyend' with ur";
				    var dutyend=easyExecSql(endsql);
//					alert (dutyend[0][0]);
//					alert (dutyend[0][1]);
					if(dutyend!=null){
						var sql13="select  l.getdutycode from llclaimdetail l ,llcase c where l.caseno=c.caseno " 
							+" and l.contno='"+tContNo+"' and l.riskcode='"+tRiskCode+"' and l.getdutykind='"+dutyend[0][0]+"' " 
							+" and l.polno='"+tPolNo+"' and  c.rgtstate in ('11','12') with ur";  
							var eGetDutyCode = easyExecSql(sql13);
							if(eGetDutyCode!=null){
								if(eGetDutyCode[0][0]==dutyend[0][1]){
									var risksql="select riskname from lmriskapp where riskcode='"+tRiskCode+"' with ur";
									var riskname=easyExecSql(risksql);
										alert ("该被保险人的"+riskname+":疾病身故保险金责任终止不能再次理赔！");
										return false ;
									  }
								}
							}
					}
				if(tRiskCode=="123701"){
					var checkSQL = "select 1 from lcpol where polno = '"+tPolNo+"' and enddate ";
					var EndDateSQL = "select enddate from lcpol where polno = '"+tPolNo+"' union select enddate from lbpol where polno = '"+tPolNo+"' with ur"; 
					var DSdateSQL =  "select min(DiagnoseDate) from LLCaseCure where caseno = '"+Caseno+"' and DiseaseCode in(select code from ldcode where codetype='llserialsdiease') and SeriousFlag = '1' with ur";
					var HospStarDateSQL = "select min(hospStartdate) from llfeemain where caseno='"+Caseno+"' and feetype='2' with ur";
					var HospEndDateSQL = "select max(hospenddate) from llfeemain where caseno='"+Caseno+"' and feetype='2' with ur";
					var EndDate = easyExecSql(EndDateSQL);
					var DSdate = easyExecSql(DSdateSQL);
					var HospStarDate = easyExecSql(HospStarDateSQL);
					var HospEndDate = easyExecSql(HospEndDateSQL);
					if(EndDate[0][0]!=null&&DSdate[0][0]!=null&&HospStarDate[0][0]!=null&&HospEndDate[0][0]!=null){
						var days1 = dateDiff(DSdate[0][0],EndDate[0][0],"D") //确诊日期与失效日期
						var days2 = dateDiff(HospStarDate[0][0],EndDate[0][0],"D") //入院日期与失效日期
						var days3 =  dateDiff(HospEndDate[0][0],EndDate[0][0],"D")//出院日期与失效日期
						if(days1>0&&days1<180&&days2>0&&days2<180&&days3<0){
							if (!confirm("此出险案件仅承担被保险人自该次住院开始至当日起（含住院当日）的180天内(含第180天)所发生的同一次住院的保险责任范围内的医疗费用")){
								return false;
							}
						}
					}
				}
				}

			}
	// #3207 管理式补充医疗保险(A款)**start**
	for (var mRowNum=0;mRowNum<ClaimDetailGrid.mulLineCount;mRowNum++){
		if(ClaimDetailGrid.getChkNo(mRowNum)){
			var tContNo=ClaimDetailGrid.getRowColData(mRowNum,1);
			var tRiskCode=ClaimDetailGrid.getRowColData(mRowNum,2);
		    var tPolNo=ClaimDetailGrid.getRowColData(mRowNum,15);
		    var risksql="select riskname from lmriskapp where riskcode='"+tRiskCode+"' with ur";
			var riskname=easyExecSql(risksql);
		    if(tRiskCode=="162401" || tRiskCode=="162501"){
		    	var sql12="select a.caseno from llcase a, llclaimdetail b where a.caseno=b.caseno "
		    		+"and a.rgtstate not in ('11','12','14') and b.riskcode='"+tRiskCode+"' and b.polno='"+tPolNo+"' " +
		    		" and b.contno='"+tContNo+"' and a.caseno<>'"+fm.CaseNo.value+"' with ur";
		    	var tCaseno=easyExecSql(sql12);
		    	if(tCaseno!=null){
		    		alert("该被保险人该保单有正在赔付"+riskname+"险种的理赔案件，案件号为"+tCaseno+"，待其结案并通知给付后再处理本案件。");
		    		return false;
		    	}
		    }
		    if(tRiskCode=="162501"){
		    	var tGetDutyKind=ClaimDetailGrid.getRowColData(mRowNum,18);
//		    	alert(tGetDutyKind);
		    	var tDeathSQL ="select deathdate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
	        	var tDeathDate=easyExecSql(tDeathSQL);
	        	if(tDeathDate!=null && tDeathDate !="" && tGetDutyKind !="501"){
	        		alert(riskname+"业务案件，一个案件下不能同时理赔医疗险责任、身故责任。");
	        		return false;
	        	}else if((tDeathDate==null || tDeathDate =="") && tGetDutyKind=="501"){
	        		alert(riskname+"业务案件，一个案件下不能同时理赔医疗险责任、身故责任。");
	        		return false;
	        	}
		    }
		}
	}

	// #3207 管理式补充A**end**
	
  if (confirm("您确实想保存该记录吗?"))
  {
  	strSQL = "select caseno from llcase where rgtstate not in ('14','09','12','11') and customerno='"
           +fm.all('CustomerNo').value+"' and caseno <> '"+fm.CaseNo.value+"'" ;
    crrResultTemp = easyExecSql(strSQL);

    if(crrResultTemp){
      alert("该客户有案件"+crrResultTemp+"未结案，请核实免赔额");
    }
    
    if (ClaimDetailGrid.mulLineCount==0)
    {
      alert("请作赔付明细计算"); 
      return;
    }   
    
    //TODO:临时校验，社保业务案件,不支持理算
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	} 
    
    //TODO:新增效验，理赔二核时不可理算确认
	var sql = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var rgtstate=easyExecSql(sql);
	if (rgtstate!=null){
	     if(rgtstate[0][0]=='16'){
	       alert("理赔二核中不可理算确认，必须等待二核结束！");
	       return false;
	     }
	}

    var i = 0;
    var tRealPay = 0;
    var tCalPay=0;
    var tTempPolPay=0;
	  mSaveType="savepay" ;
	  fm.SubCalculate.disabled=true;
	  
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //  showSubmitFrame(mDebug);
    fm.Opt.value = "save"
    fm.action="./ClaimSave.jsp";
    fm.submit(); //提交

  }
  else
  {
    return;
  }
}

function preDeal() 
{
    var caseNo = fm.all('CaseNo').value;
	var varSrc="&CaseNo="+ caseNo;
	parent.window.location = "./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+
	varSrc    
}
function nextDeal()
{
	var caseNo = fm.all('CaseNo').value;
	var rgtStateSql = "select rgtstate from llcase where caseno='"
	  				+ caseNo +"'";
	var tRgtState = easyExecSql(rgtStateSql);
	var riskCodeSql = "select riskcode from LLClaimPolicy where caseno='"
				  	+ caseNo +"'";
	var riskcode = easyExecSql(riskCodeSql);
	//医保卡保单添加立案上报校验
	if(tRgtState == "04"&& riskcode == "123202"||riskcode == "220602"){
		var noticesql = "select ConsultNo from LLCaseExt where caseno='"+caseNo+"' with ur";
		var noticeno = easyExecSql(noticesql);
		if(noticeno){
			var sql = "select 1 from YBK_C01_LIS_ResponseInfo where caseno= '"
				 + noticeno
				 +"' and prtno='02' and requesttype='C01'";
			var result = easyExecSql(sql);
			if(!result){
				if(confirm("是否上报立案信息？")){
					fm.NoticeNo.value=noticeno;
					fm.action="./LLClaimLAUploadSave.jsp";
					beforeLAUpload();
					fm.submit();
				}else{
					return;
				}
			}else{
				var varSrc = "&CaseNo=" + caseNo;
				var newWindow = OpenWindowNew("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + 
				varSrc,"审批","left");
			}
		}else{
			alert("未关联报案号，案件无法上报!");
			return;
		}
	//非医保卡理赔
	}else{
		var varSrc = "&CaseNo=" + caseNo;
		window.location = "./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + 
		varSrc;
			}
}
function beforeLAUpload(){
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
}
function afterLAUpload(FlagStr, content, caseNo){
	showInfo.close();
	if(FlagStr == "Fail"){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}else{
		var varSrc = "&CaseNo=" + caseNo;
		//window.location = "./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + 
		//varSrc;
		var newWindow = OpenWindowNew("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + 
				varSrc,"审批","left");
	}
}
function backDeal()
{
    top.close();
}

function CaseChange()
{
	initClaimPolGrid();
	initTitle();
    if(fm.CaseNo.value!='')   
	
	if (fm.RgtState.value == null ||
		fm.RgtState.value =="")
	{
		
		queryClear();
		
	}
	else
	{
		queryCalPay();
	}
	
}

function queryClear()
{
		initClaimDetailGrid();
		initClaimPolGrid();
		fm.PreGiveAmnt.value  = "";
		fm.SelfGiveAmnt.value = "";
		fm.RefuseAmnt.value = "";
		fm.StandPay.value= "";
		fm.RealPay.value= "";
		fm.RealHospDate.value = "";	
		fm.AllGiveAmnt.value = "";
		fm.AllGiveType.value = "";	
		fm.RetireAddFee.value = "";	
	
}

function ClaimDecline()
{
	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&InsuredNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	pathStr="./FrameMainClaimDecline.jsp?Interface=LLClaimDeclineInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"ClaimDeclineInput","middle",800,330);
}

//TODO:保费豁免页面的跳转
function Exemption(){	
	if(checkData()){
		var CaseNo = fm.CaseNo.value;//理赔案件号
		var CustomerNo = fm.CustomerNo.value;//客户号
		var CustomerName = fm.CustomerName.value;//客户姓名
		//判断案件状态
//		var tSQL0 = "select rgtstate from llcase where caseno='"+CaseNo+"'";
//		var result = easyExecSql(tSQL0);
//		if(result == '03'){	//只有检录状态可以进入
//
//		}else{
//			alert("案件："+CaseNo+"不处于检录状态，无法豁免保费，请回退！");
//		    return false;
//		}
		//从mulline中获取保单号
		for(var rowNum=0;rowNum<ClaimPolGrid.mulLineCount;rowNum++){
	        var arr = ClaimPolGrid.getRowData(rowNum);
	        var ContNo=arr[0];//保单号 
	        var polno=arr[6];  //保单险种号
	        var riskcode=arr[10];  //险种代码
	        //判断保单下的险种是否有豁免险，对于豁免投保人的情况，会在理赔后进入备份表，无法再次查看豁免记录，针对
	        //此特殊情况进行处理
	        var tSQL = "select 1 from lcpol d where d.contno='"+ContNo+"' " +
        		"and d.riskcode in(select riskcode from lmriskapp where risktype8='8' and riskcode= D.RISKCODE " +
				"union select riskcode from lmriskapp where risktype8='9' and kindcode='S' and riskcode=d.riskcode) "+
				"union select 1 from lbpol d where d.contno='"+ContNo+"' " +
				"and d.riskcode in(select riskcode from lmriskapp where risktype8='8' and riskcode= D.RISKCODE " +
				"union select riskcode from lmriskapp where risktype8='9' and kindcode='S' and riskcode=d.riskcode) with ur";
				
        	var result = easyExecSql(tSQL);
        	if(result == null){
				alert("保单下：" + ContNo + "没有豁免险种，请重新选择保单并计算！");
				return false;
	    	}
        }
        //附加路径内容
        var varSrc = "&CaseNo=" + CaseNo;
        varSrc += "&CustomerNo=" + CustomerNo;
        varSrc += "&CustomerName=" + CustomerName;
        varSrc += "&ClaimPolGridNum=" + ClaimPolGrid.mulLineCount;
        pathStr="./FrameMainLLExemption.jsp?Interface=LLExemptionInput.jsp"+varSrc;
		showInfo = OpenWindowNew(pathStr,"LLExemptionInput","left");
		
    }	
}


function ContQuery()
{
	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&InsuredNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	pathStr="./FrameMainContQuery.jsp?Interface=LLContQueryInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLContQueryInput","middle",800,330);
}

function searchAccValue()
{
	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&InsuredNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	pathStr="./FrameMainSearchAccValue.jsp?Interface=LLSearchAccValueInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"SearchAccValueInput","left");
}

//#2133 增加保单现金价值查询功能
function searchCashValue()
{
	var rowNum = ClaimPolGrid.mulLineCount ;	
	var contno = ClaimPolGrid.getRowColData(0,1);//“赔付保单明细”中的“保单号”存在多条，只显示第一条保单的信息
	var tAccSql ="select distinct accdate from llcaserela a,llsubreport b where a.subrptno=b.subrptno and a.caseno='"+ fm.CaseNo.value +"' order by accdate fetch first 1 rows only";
    var tAccDate = easyExecSql(tAccSql);
    if(tAccDate == null || tAccDate =="")
    {
		tAccDate='';
    }
	
	var varSrc = "&contNo=" + contno;
	varSrc += "&accDate=" + tAccDate;
	pathStr="./FrameMainSearchCashValue.jsp?Interface=LLCashValuePEdorBudget.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLCashValuePEdorBudget","left");
}
//xuyunpeng add 
//#2995若账单治疗开始时间在事件发生日180天以后，提示账单不予赔付 start
function preCheckFeeDate(riskCode){
	var tips;
	var rowNum = ClaimPolGrid.mulLineCount ;
	var i = 0 ;
	for(i ; i < rowNum ; i++){
		var cn = ClaimPolGrid.getRowColData(i,11);
		if(riskCode == cn){
			var tip = checkFeeDate();
			if(tip != ""&&tip != null){
				if(tips!=""&&tips != null){
					tips=tips+"、"+tip;
				}else{
					tips = tip;
				}
				
			}
			
		}
	}	
	
	if(tips!=""&&tips != null){
		alert("账单("+tips+")治疗时间在事故发生日期180天外，不予理赔。");
	}
}
//xuyunpeng add 险种121101专用 #2995
function checkFeeDate(){
	var tips;
	var strSQL = "select CaseRelaNo from LLCaseRela where caseno='"+ fm.CaseNo.value +"'" ;
	
	var CaseRelaNos = easyExecSql(strSQL);
	
  	if(CaseRelaNos!=null){
  		for (var i = 0;i<CaseRelaNos.length;i++){
  			//得到事件发生的日期
  			var srSQL = "select a.accdate from llsubreport a,llcaserela b "
				+ " where a.subrptno=b.subrptno and b.caserelano='"
				+ CaseRelaNos[i][0] + "' and b.caseno='" + fm.CaseNo.value + "'";
  			var mAccDate = easyExecSql(srSQL);  			
  			if(mAccDate !=null && mAccDate !=""){
  				
  				//得到本事件发生180天后的账单
  				var sql = "select ReceiptNo "
  					+ "from LLFeeMain where  caseno='" + fm.CaseNo.value
  					+ "' and days(HospStartDate)-days('" + mAccDate[0][0]
  				+ "')>180 and caserelano='" + CaseRelaNos[i][0]+"'";
  				
  				var FeeNo = easyExecSql(sql);
  				if(FeeNo != null){
  					for (var j = 0;j<FeeNo.length;j++){
  						if(tips!=""&&tips!=null){
  							tips = tips+"、"+FeeNo[j][0];
  						}else{
  							tips = FeeNo[j][0];
  						}
  						
  					}
  				}
  			}
  			
  		}
  		
  		/*if(arrResult6[0][0]=="09"){
  			
  		}*/
  	}

    
  	return tips;
}
//#2995 若账单治疗开始时间在事件发生日180天以后，提示账单不予赔付 end



//#2781 xuyunpeng 税优险进入此逻辑！判断住院天数是否超过了180天，超过给出提示并阻断 start
function CheckSY(){
	var tips="";
	var rowNum = ClaimPolGrid.mulLineCount ;
	var i = 0 ;
	var tContno="";
	for(i ; i < rowNum ; i++){	
		
		var riskcode = ClaimPolGrid.getRowColData(i,11);
		var mContNo= ClaimPolGrid.getRowColData(i,1);
		var srSQL = "select taxoptimal from lmriskapp where riskcode='"+riskcode+"'";
		var syPoint = easyExecSql(srSQL);
		if(syPoint!=null&&syPoint!=""){
		if("Y"==syPoint[0][0]){	
			if("100"!=ClaimPolGrid.getRowColData(i,3)){
				continue;
			}
			
			 if(tContno.indexOf(mContNo)>=0){
				 continue;
			 }
			 tContno+=mContNo;
				var days = 0;
				var qDate = "select CValiDate,EndDate from lcpol where contno='"+mContNo+"' and riskcode='"+riskcode+"'";
				var sDate = easyExecSql(qDate);
				if(sDate!=null&&sDate!=""){
					var startDate = sDate[0][0];
					var endDate = sDate[0][1];
					//alert(endDate);
					var caseInfoStr = "select HospStartDate,HospEndDate,RealHospDate "
										+"from llfeemain c "
										+"where FeeType = '2' "
										+"and caseno in "
										+"(select a.caseno "
										+"from llClaimpolicy a "
										+"where contno = '"+mContNo+"' "
										+"and exists "
										+"(select 1 "
										+"from LLSubReport "
										+"where subrptno in "
										+"(select subrptno "
										+"from llcaserela b "
										+"where b.caseno =a.caseno and accdate>=date'"+startDate+"' and accdate < date '"+endDate+"' )) and exists (select 1 from llcase where caseno = a.caseno and RgtState in ('09','11','12'))) "
										+"and exists "
										+"(select 1 from LLCaseRela "
										+"where CaseRelano = "
										+"c.CaseRelano and "
										+"caseno=c.caseno) union select HospStartDate,HospEndDate,RealHospDate from llfeemain d  where FeeType = '2' and caseno = '"+fm.CaseNo.value+"'  and exists (select 1  from LLCaseRela where CaseRelano = d.CaseRelano and caseno = d.caseno)";
					var dateInfoDays = easyExecSql(caseInfoStr);
					if(dateInfoDays!=null &&dateInfoDays!=""){
						for(var k=0;k<dateInfoDays.length;k++){
							var HospStartDate = dateInfoDays[k][0];
							var HospEndDate  = dateInfoDays[k][1];
							if((dateDiff(startDate,HospStartDate,"D")>=0)
									&&(dateDiff(HospStartDate,endDate,"D")>=0)
									&&(dateDiff(startDate,HospEndDate,"D")>=0)
									&&(dateDiff(HospEndDate,endDate,"D")>=0)){
								days+=parseInt(dateInfoDays[k][2]);
							}else{
								if((dateDiff(HospEndDate,startDate,"D")>=0)||(dateDiff(endDate,HospStartDate,"D")>=0)){
									return true;
								}
								
								if((dateDiff(HospStartDate,startDate,"D")>=0)){
									HospStartDate = startDate;
								}
								if((dateDiff(endDate,HospEndDate,"D")>=0)){
									HospEndDate = endDate;
								}
								
								var day = dateDiff(HospStartDate,HospEndDate,"D");
								days+=day;
							}
							
						}
					}
				}
					//alert(days);
				if(days>180){
					if(tips!=""){
						
						tips = tips+"、"+mContNo;
					}else{
						tips = mContNo;
					}
				}
			}
		}		
		
	}
	
	if(tips!=""){
		
		alert("保单"+tips+"，在保险期间内，被保险人年度累计住院天数已超180天");
		return false;
	}
	return true;		
}

//#2781 xuyunpeng 税优险进入此逻辑！判断住院天数是否超过了180天，超过给出提示并阻断 end

//#2781 税优险种判断账单种类为门诊时，赔付责任**start**
function checkGetDuty(){
	var aCaseNo =fm.CaseNo.value;
//	alert(aCaseNo);
	var aCount=0;
	for(var aSYrowNum=0; aSYrowNum<ClaimDetailGrid.mulLineCount; aSYrowNum++){
		if(ClaimDetailGrid.getChkNo(aSYrowNum)){
		var sYRiskCode= ClaimDetailGrid.getRowColData(aSYrowNum,2);
		//判断是否为税优险种
		var sYSQL="select taxoptimal from lmriskapp where riskcode='"+sYRiskCode+"'";
		var sYPoint = easyExecSql(sYSQL); 
		if(sYPoint != null && sYPoint !=""){
			if(sYPoint[0][0]=='Y'){
			//	alert("税优险种");
				var tRiskName=ClaimDetailGrid.getRowColData(aSYrowNum,3);
				 var tRiskResult =ClaimDetailGrid.getRowColData(aSYrowNum,10);
				if((tRiskName=="住院前后门诊费用保险金" || tRiskName=="特定门诊治疗费用保险金" || tRiskName=="慢性病门诊治疗费用保险金") &&
				    tRiskResult != "全额拒付"){
				aCount++;
				var sYFeeSQL="select mainfeeno from llfeemain where caseno='"+aCaseNo+"'";
				var sYFeeNum=easyExecSql(sYFeeSQL);
				for(var i=0;i<sYFeeNum.length;i++){
					//判断账单种类和账单属性
					var sYTypeSQL="select feetype,feeatti from llfeemain where caseno='"+aCaseNo+"' and mainfeeno='"+sYFeeNum[i]+"'";
					var sY = easyExecSql(sYTypeSQL);
				//	alert(sY[0][0]);
				//	alert(sY[0][1]);
					if(sY !=null){
						if(sY[0][0]=='1'&& sY[0][1]=='4'){
							//判断事件信息个数
							var sYSubSQL="select count(distinct b.subrptno) from llcaserela a,llsubreport b where a.caseno ='"+aCaseNo+"' " +
									" and b.subrptno=a.subrptno with ur";
							var sYCount =easyExecSql(sYSubSQL);
//							alert(aCount);
//							alert(sYCount);
// 						    alert(aCount > sYCount);
							if( aCount > sYCount){
								alert("住院前后门诊费用保险金、特定门诊治疗费用保险金、慢性病门诊治疗费用保险金三项责任不能重复理赔!");
								return false;
						 }
					  }
					}
						
				}
			 }
		   }
		}
	}
  }
	return true;
}
// # 2781  税优险种判断账单种类为门诊时，赔付责任**end**