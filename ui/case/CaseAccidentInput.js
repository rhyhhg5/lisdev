/*******************************************************************************
 * Name     :CaseAccidentInput.js
 * Function :立案－事故伤残明细的函数处理程序
 * Author   :LiuYansong
 * Date     :2003-7-16
 */

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var rate_flag = "1";
/*******************************************************************************
 * Name     :initDisplayFlag()
 * Function : 判断残疾明细是否显示
 * Date     :2003-7-25
 * Author   :LiuYansong
 */
function initDisplayFlag()
{
  var Flag = fm.DisplayFlag.value;
  if(Flag==0)
  {
    //fm.all("divDisplayFlag_c").style.display="none";
    fm.all("divDeformityInfo").style.display="none";
  }
  if(Flag==1)
  {
   // fm.all("divDisplayFlag_c").style.display="";
    fm.all("divDeformityInfo").style.display="";
    fm.all("divAccidentInfo").style.display="none";
    fm.all("divDiseaseInfo").style.display="none";
  }
}

function afterCodeSelect( cName, Filed)
{
  if(cName=='AccidentTypeList')
  {
    displayAccidentInfo();
  }
  if(cName=='DeformityTypeList')
  {
    displayDeformityInfo()
  }
}
function displayAccidentInfo()
{
  if(fm.DisplayFlag.value=="0")
  {
    var displayFlag = fm.AccitentType.value;
    if(displayFlag=="YW")//意外事故明细
    {
      fm.all('divAccidentInfo').style.display = "";
      fm.all('divDiseaseInfo').style.display  = "none";
    }
    if(displayFlag=="JB")
    {
      fm.all('divAccidentInfo').style.display = "none";
      fm.all('divDiseaseInfo').style.display  = "";
    }
  }
  else
  {
    fm.all('divAccidentInfo').style.diaplay = "none";
    fm.all('divDiseaseInfo').style.display = "none";
  }
}

function displayDeformityInfo()
{
  var displayFlag = fm.DeformityType.value;
  if(displayFlag==null)
  {
    fm.all('divDeformityInfo').style.display = "none";
  }
  else
  {
    fm.all('divDeformityInfo').style.display = "";
  }
}




//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
   {
  		parent.fraMain.rows = "0,0,0,0,*";
   }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
//初始化显示的程序
function showCaseInfo()
{
  fm.action = "./showcaseinfo.jsp";
  fm.submit();
}
function query_disease()
{
  window.open("./FrameQueryDisease.jsp");
}
/*******************************************************************************
 * Name          :deal_DeformityRate
 * Author        :LiuYansong
 * CreateDate    :2003-12-18
 * Function      :判断理算中的实际赔付金额的函数
 */
function deal_DeformityRate(j)
{
  rate_flag = "1";//初始化标志
  var DeformityGrade = DeformityGrid.getRowColData(j,1);
  var AppDeformityRate =DeformityGrid.getRowColData(j,5);
  var DeformityRate = DeformityGrid.getRowColData(j,6);
  var Remark = DeformityGrid.getRowColData(j,7);
  if(DeformityRate==""||DeformityRate=="null")
  {
    alert("请您录入实际给付比例");
    rate_flag = "0";
    return;
  }

  if(DeformityGrade=="Y03")
  {
    if(DeformityRate>0.5)
    {
      alert("重要器官切除的给付比例不能超过50%！");
      rate_flag = "0"
      return;
    }
  }

  if(DeformityGrade=="Y01"||DeformityGrade=="Y02"||DeformityGrade=="Y04")
  {
    if(DeformityRate>1)
    {
      alert("给付比例不能超过100%！");
      rate_flag = "0";
      return;
    }
  }

  if(rate_flag==1)
  {
    if(!(DeformityRate==AppDeformityRate))
    {
      if(Remark==""||Remark=="null")
      {
        alert("给付比例与实际给付比例不相等，请在备注中录入不相等的原因！！");
        rate_flag = "0";
        return;
      }
    }
    else
      rate_flag = "1";
  }
}


/*****************************************************/
//提交，保存按钮对应操作
function submitForm()
{
 
 
  
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./CaseAccidentSave.jsp";
    //showSubmitFrame(mDebug);
    fm.submit(); //提交
 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
   	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
 	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 }
}

function initFM()
{

	//一般疾病
	var strSQL ="select Diagnose,DiseaseName,DiseaseName,DiseaseCode,HospitalCode,HospitalName,DoctorName,SerialNo,DoctorNo  from llcasecure  where caseno='" 
	+ fm.CaseNo.value +"' and CaseRelaNo='" + fm.CaseRelaNo.value +"' "  
	  +" and SeriousFlag='0' order by MainDiseaseFlag desc";	//根据MainDiseaseFlag判断主要疾病,放在第一位
	var arr = easyExecSql(strSQL );
	
	if (arr) displayMultiline( arr, DiseaseGrid);
	
	//重疾
	strSQL ="select DiseaseName,DiseaseCode,HospitalName,HospitalCode,DoctorName,SerialNo,'',DoctorNo from llcasecure  where caseno='" + fm.CaseNo.value +"' and CaseRelaNo='" + fm.CaseRelaNo.value +"'"
	  +" and SeriousFlag='1' order by MainDiseaseFlag desc" ; //根据MainDiseaseFlag判断主要疾病,放在第一位
	var brr = easyExecSql(strSQL );
	if ( brr )displayMultiline( brr, SeriousDiseaseGrid);  
	//意外残疾
	strSQL ="select AccidentNo,Type,TypeName,'',Code,Name,ReasonCode,Reason,Remark from llaccident  where caseno='" + fm.CaseNo.value +"' and CaseRelaNo='" + fm.CaseRelaNo.value +"'";
	var crr = easyExecSql(strSQL );
	if ( crr )displayMultiline( crr, AccidentGrid);  
	//手术	
    strSQL ="select OperationName,OperationCode,OpFee,OpLevel,SerialNo from lloperation  where caseno='" + fm.CaseNo.value +"' and CaseRelaNo='" + fm.CaseRelaNo.value +"'"
	 ;
	var drr = easyExecSql(strSQL );
	if ( drr )displayMultiline( drr, DegreeOperationGrid);  
		//其它录入要素
	
    strSQL ="select a.codename,b.FactorCode,b.FactorName,b.Value,b.remark,b.FactorType from LLOtherFactor b, ldcode a "
     +"   where caseno='" + fm.CaseNo.value +"' and CaseRelaNo='" + fm.CaseRelaNo.value +"'"
      +" and  a.codetype='llotherfactor' and a.code=b.factortype ";
	var drr = easyExecSql(strSQL );
//	alert(drr);
	if ( drr )displayMultiline( drr, OtherFactorGrid);  
	
}
