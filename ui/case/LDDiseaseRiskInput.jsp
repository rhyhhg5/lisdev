<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%> 
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");

%>
<html>    
<%
//程序名称：LDDiseaseRiskInput.jsp
//程序功能：疾病风险库配置	
//创建日期：2010-11-18
//创建人  ：张仲豪
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LDDiseaseRiskInput.js"></SCRIPT>
		<%@include file="LDDiseaseRiskInit.jsp"%>   
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<script>
	
 </script>
</head>

<body onload="initForm();">
  <form action="" method=post name=fm target="">
    <br>
    
    <Div id=Info>
		
		
    <table>
    	<TR>
         <TD class= titleImg>
         疾病风险库配置
         </TD>
       </TR>
   	</table>
   	<Div  id= "divDiseaseRisk" style= "display: ''" align = left>
   	  <table class= common border=0 width=100%>
      	<TR  class= common8>
			<TD  class= title8>疾病分类代码</TD>
			<TD  class= input><Input class= 'common' name=ICDCode elementtype=nacessary verify="疾病分类代码|len<=20">
			<TD  class= title8>疾病分类名称</TD>
			<TD  class= input><Input class= 'common' name=ICDName elementtype=nacessary verify="疾病分类名称|len<=50"></TD>
        </TR>
      </Table>
    	<Table  class= common>
    		<TR  class= common>
    			<TD text-align: left colSpan=1>
    				<span id="spanDiseaseRiskGrid" ></span>
    			</TD>
    		</TR>
    	</Table>
    	<Div id="divPage" align=center style="display: 'none' ">
					<INPUT CLASS=cssButton VALUE="首页" TYPE=button
						onclick="turnPage.firstPage();">
					<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
						onclick="turnPage.previousPage();">
					<INPUT CLASS=cssButton VALUE="下一页" TYPE=button
						onclick="turnPage.nextPage();">
					<INPUT CLASS=cssButton VALUE="尾页" TYPE=button
						onclick="turnPage.lastPage();">
				</Div>
    	<INPUT VALUE="查  询" class="cssButton" TYPE="button" onclick="ICDSearch()">
    	<INPUT VALUE="保  存" class="cssButton" TYPE="button" onclick="DiseaseRiskSave()">
    </Div>

		<input type= hidden name=StartDate>
		<input type= hidden name=EndDate>
	</Div>	
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 