<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：Save.jsp
//程序功能：
//创建日期：
//创建人  ：cc
//更新记录：  更新人    更新日期     更新原因/内容
System.out.println("Auto-begin:");
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String tFlag = request.getParameter("tFlag");
  String AskOrAnswer = request.getParameter("AskOrAnswer");
  String opstring = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  if(tG == null) 
  {
	System.out.println("session has expired");
	return;
   }

  VData tVData = new VData();
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("InputDate",PubFun.getCurrentDate());
  tTransferData.setNameAndValue("Operator",tG.Operator);
  tTransferData.setNameAndValue("IssueNo",request.getParameter("IssueNo"));
  System.out.println("IssueNo====="+request.getParameter("IssueNo"));
  
  LLIssuePolSchema tLLIssuePolSchema = new LLIssuePolSchema();
  tLLIssuePolSchema.setCaseNo(request.getParameter("CaseNo"));
  tLLIssuePolSchema.setRgtNo(request.getParameter("RgtNo"));
  tLLIssuePolSchema.setMessageType(AskOrAnswer);
  tLLIssuePolSchema.setRequrieMan(tG.Operator);
  tLLIssuePolSchema.setSubject(request.getParameter("Subject"));
  tLLIssuePolSchema.setMessageCont(request.getParameter("Content"));
  tLLIssuePolSchema.setReplyer(request.getParameter("llusercode"));
  tLLIssuePolSchema.setBackRgtState(request.getParameter("BackState"));
  tLLIssuePolSchema.setState("0");
  tLLIssuePolSchema.setOperator(tG.Operator);
  tLLIssuePolSchema.setManageCom(tG.ManageCom);
  
         
	tVData.add( tTransferData );
  tVData.add( tLLIssuePolSchema );	
	tVData.add( tG ); 
	try
	{
		  LLIssueUI tLLIssueUI = new LLIssueUI();
		  if(AskOrAnswer.equals("1"))
		  {
		    opstring = "INSERT||NEW";
		  }
		  else 
		  {
		    opstring = "INSERT||ADD";
		  }
		if (tLLIssueUI.submitData(tVData,opstring) == false)
				{
					int n = tLLIssueUI.mErrors.getErrorCount();
					System.out.println("n=="+n);
					for (int j = 0; j < n; j++)
					System.out.println("Error: "+tLLIssueUI.mErrors.getError(j).errorMessage);
					Content = " 失败，原因是: " + tLLIssueUI.mErrors.getError(0).errorMessage;
					FlagStr = "Fail";
				}
		  				//如果在Catch中发现异常，则不从错误类中提取错误信息
				if (!FlagStr.equals("Fail"))
				{
				    tError = tLLIssueUI.mErrors;
				    //tErrors = tLLIssueUI.mErrors;
				    Content = " 保存成功! ";
				    if (!tError.needDealError())
				    {                          
				    	int n = tError.getErrorCount();
		    			if (n > 0)
		    			{    			      
					      for(int j = 0;j < n;j++)
					      {
					        //tError = tErrors.getError(j);
					        Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
					      }
					    }
		
				    	FlagStr = "Succ";
				    }
				    else                                                                           
				    {
				    	int n = tError.getErrorCount();
		    			if (n > 0)
		    			{
					      for(int j = 0;j < n;j++)
					      {
					        //tError = tErrors.getError(j);
					        Content = Content.trim() +j+". "+ tError.getError(j).errorMessage.trim()+".";
					      }
							}
				    	FlagStr = "Fail";
				    }
				}		
		

	}
	catch(Exception ex)
	{
			ex.printStackTrace();
			Content = Content.trim() +" 提示:异常退出.";
	}
%>                      
<html>
<script language="javascript">

parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
