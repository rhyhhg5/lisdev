<%
//程序名称：LLBnfInputInit.jsp
//程序功能：
//创建日期：2005-2-26 12:07
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
var turnPage=new turnPageClass();
function initInpBox()
{ 
  try
  {       
	  
  }
  catch(ex)
  {
    alert("在LDPersonQueryInit.jsp-->InitInpBox1函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LDPersonQueryInit.jsp-->InitSelBox2函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {

  initInpBox(); 
  initSurveyCaseGrid();
  updateCaseGrid();
  
  }
  catch(re)
  {
    alert("LDPersonQueryInit.jsp-->InitForm函数中发生异常3:初始化界面错误!"+re.message);
  }
}

// 保单信息列表的初始化
function initSurveyCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="案件号";
    iArray[1][1]="80px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="调查序号";
    iArray[2][1]="40px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="直接调查费";
    iArray[3][1]="40px";
    iArray[3][2]=100;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="间接调查费";
    iArray[4][1]="40px";
    iArray[4][2]=100;
    iArray[4][3]=0;

    iArray[5]=new Array();
    iArray[5][0]="总调查费";
    iArray[5][1]="40px";
    iArray[5][2]=100;
    iArray[5][3]=0;

    iArray[6]=new Array();
    iArray[6][0]="调查人";
    iArray[6][1]="40px";
    iArray[6][2]=60;
    iArray[6][3]=1;
    
    iArray[7]=new Array();
    iArray[7][0]="审核人";
    iArray[7][1]="40px";
    iArray[7][2]=60;
    iArray[7][3]=1;

    SurveyCaseGrid = new MulLineEnter("fm","SurveyCaseGrid");
    SurveyCaseGrid.mulLineCount =3;
    SurveyCaseGrid.displayTitle = 1;
    SurveyCaseGrid.locked = 1;
    SurveyCaseGrid.canChk =0;
    SurveyCaseGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    SurveyCaseGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    SurveyCaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initAttr(divID)
{
	fm.all("spanAddFeeGrid" + AddFeeGrid.maxSpanID).all('AddFeeGrid5').value="间接调查费";
}


</script>
