<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLClaimerCaseList.js"></SCRIPT>
   <%@include file="LLClaimerCaseListInit.jsp"%>
   <script language="javascript">
   function initDate(){
   fm.RgtDateS.value="<%=afterdate%>";
   fm.RgtDateE.value="<%=CurrentDate%>";
   var usercode="<%=Operator%>";
   var comcode="<%=Comcode%>";
   fm.Operator.value=usercode;
//   fm.OrganCode.value=comcode;
   

   var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
   var arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
		         fm.optname.value= arrResult[0][0];

    		}
   }
   </script>
 </head>
<body  onload="initDate();initForm();">
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         理赔案件信箱
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
       <table  class= common>
<TR  class= common8>
	<TD  class= title8>
	<input type="radio" name="StateRadio"  value="0" onclick="classify()">全部案件 
	<input type="radio" name="StateRadio"  value="3" checked onclick="classify()">待处理案件 
	<input type="radio" name="StateRadio"  value="1" onclick="classify()">未结案  
	<input type="radio" name="StateRadio"  value="2" onclick="classify()">已结案
	<input type="radio" name="StateRadio"  value="4" onclick="classify()">无影像
 	</TD>
</TR>
<TR  class= common8>
<TD  class= title8>
<input type="radio" name="typeRadio"  value="0">咨询类 
<input type="radio" name="typeRadio"   value="1">通知类  
<input type="radio" name="typeRadio" checked  value="3">申请类
<input type="radio" name="typeRadio" value="4">申诉类
<input type="radio" name="typeRadio" value="5">错误处理类
<!--<input type="checkbox" value="1">申诉类  
 <input type="checkbox" value="1">错误处理类-->
 </td>
 </tr>
 <TR  class= common8>
 <td>
 <input type="radio" value="01" name="RgtState" onclick="easyQuery()">受理</input>
 <input type="radio" value="02" name="RgtState" onclick="easyQuery()">扫描</input>
 <input type="radio" value="03" name="RgtState" onclick="easyQuery()">检录</input>
 <input type="radio" value="04" name="RgtState" onclick="easyQuery()">理算</input>
 <input type="radio" value="05" name="RgtState" onclick="QueryUW()">审批</input>
 <input type="radio" value="06" name="RgtState" onclick="easyQuery()">审定</input>
 <input type="radio" value="07" name="RgtState" onclick="easyQuery()">调查</input>
 <input type="radio" value="08" name="RgtState" onclick="easyQuery()">查讫</input>
 <input type="radio" value="09" name="RgtState" onclick="easyQuery()">结案</input>
 <input type="radio" value="10" name="RgtState" onclick="QueryUW()">抽检</input>
 <input type="radio" value="11" name="RgtState" onclick="easyQuery()">通知</input>
 <input type="radio" value="12" name="RgtState" onclick="easyQuery()">给付</input>
 <input type="radio" value="13" name="RgtState" onclick="easyQuery()">延迟</input>
 <input type="radio" value="14" name="RgtState" onclick="easyQuery()">撤件</input>
 </TD>                                             
</TR>
<tr>
   <td>
   	<input type="checkbox" value="1" name="llclaimdecision" onclick="PayQuery()">正常给付</input>
   	<input type="checkbox" value="2" name="llclaimdecision" onclick="PayQuery()">部分给付</input>
   	<input type="checkbox" value="3" name="llclaimdecision" onclick="PayQuery()">全额拒付</input>
   	<input type="checkbox" value="4" name="llclaimdecision" onclick="PayQuery()">协议给付</input>
   	<input type="checkbox" value="5" name="llclaimdecision" onclick="PayQuery()">通融给付</input>
   </td>
</tr>
</table>

</DIV>

<table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput2);">
         </TD>
         <TD class= titleImg>
         理赔员信箱
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
	<TR  class= common8>
  <TD  class= title> 管理机构</TD><TD  class= input>
  <Input class= "codeno"  name=OrganCode onkeydown="QueryOnKeyDown();"  ondblclick="getstr();return showCodeList('comcode',[this,OrganName],[0,1],null,null,null,1);" onkeyup="getstr();return showCodeListKey('comcode',[this,OrganName],[0,1],null,null,null,1);" ><Input class=codename  name=OrganName>
	
	<TD  class= title>理赔员姓名</TD><TD  class= input> 
	<Input class= "code8"  name=optname onkeydown="QueryOnKeyDown();" ondblclick="getRights();return showCodeList('optname',[this, Operator], [1, 0],null,Str,'1');" onkeyup="getRights();return showCodeListKey('optname', [this, Operator], [1, 0],null,Str,'1');" >
	
	<TD  class= title>理赔员代码</TD><TD  class= input> 
	<Input class="readonly" readonly name=Operator></TD>
	
<!--	<TD  class= title>理赔号码</TD><TD  class= input> 
	<Input class=common8 name=CaseNo  onkeydown="QueryOnKeyDown()"></TD>-->
	<TR  class= common>	
<!--	<TD  class= title>客户号</TD><TD  class= input> -->
	<Input type=hidden name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>
	<!--<TD  class= title>客户名称</TD><TD  class= input> 
	<Input class=common8 name=CustomerName></TD>-->
	<!--
		<TR  class= common>	
	</tr>
		<TD  class= title>团体批次号</TD><TD  class= input> -->
		<Input type=hidden name=RgtNo onkeydown="QueryOnKeyDown()"></TD>
		<TD  class= title8>受理日期从</TD>
		<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
		<TD  class= title8>到</TD>
		<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
		
	</TR>
       </table>
     </Div>
 <!--  <input style="display:''"  class=cssButton type=button value="查 询" onclick="easyQuery()"> -->
    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>
       
 <!--新增加的下拉框-->
 <Div  id= "divLLLLMainAskInput2" style= "display: 'none'">
       <table  class= common>
        <TR  class= common>
	<TD  class= title8>处理</TD>
	<TD  class= input8> <input class=code CodeData="0|7^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定^7|调查分配 "  name=DealWith ondblclick="return showCodeListEx('DealWith',[this]);" onkeyup="return showCodeListKeyEx('DealWith',[this]);"></TD>
	<TD  class= title8>查询</TD>
	<TD  class= input8> <input class=code CodeData="0|7^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定"  name=Query ondblclick="return showCodeListEx('Query',[this]);" onkeyup="return showCodeListKeyEx('Query',[this]);"></TD>
	<TD  class= title8>统计</TD>
	<TD  class= input8> <input class=code CodeData="0|2^1|门诊^2|住院"  name=FeeType ondblclick="return showCodeListEx('FeeType',[this]);" onkeyup="return showCodeListKeyEx('FeeType',[this]);"></TD>
        </TR>	
        <TR  class= common>
	<TD  class= title8>打印</TD>
	<TD  class= input8> <input class=code CodeData="0|5^0|给付通知书打印^1|给付明细表打印^2|给付凭证打印^3|调查报告打印^4|补充材料打印"  name=Printf ondblclick="return showCodeListEx('Printf',[this]);" onkeyup="return showCodeListKeyEx('Printf',[this]);"></TD>
        </TR>
       </table>
     </Div>
      
    </Div>
   <Div  align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div> 
	
	<hr>
	<table>
  	<TR>
    	<TD>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClaimerInfo);">
      </TD>
      <TD class= titleImg>
         理赔员统计
      </TD>
      <TD class=common>&nbsp&nbsp&nbsp&nbsp<input type=button class=cssButton value="统计计算" OnClick="ClaimerCal(); "></TD>
   	</TR>
	</table>   
	
	 <Div  id= "divClaimerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanClaimerGrid" >
            </span>
          </TD>
        </TR>
      </table>
	</Div>
	<Div  align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</Div> 
	
  <Div style= "display: 'none' ">  
 <hr/>       
 <input  class=cssButton type=button value="咨询通知" onclick="DealConsult()">
<input  class=cssButton type=button value="受理申请" onclick="DealApp()">
<input  class=cssButton type=button value="受理申请(团体)" onclick="DealAppGrp()">
<input  class=cssButton type=button value="账单录入" onclick="DealFeeInput()">
<input  class=cssButton type=button value="检录" onclick="DealCheck()">
<input  class=cssButton type=button value="理算" onclick="DealClaim()">
<input  class=cssButton type=button value="审批审定" onclick="DealApprove()">
 <hr/>
<input  class=cssButton type=button value="给付通知书打印" onclick="ClaimGetPrint()">
<input  class=cssButton type=button value="给付明细表打印" onclick="ClaimDetailPrint()">
<input  class=cssButton type=button value="给付凭证打印" onclick="GPrint()">
<input  class=cssButton type=button value="调查报告打印" onclick="SurveyPrint()">
<input  class=cssButton type=button value="补充材料打印" onclick="AffixPrint()">
</div>
<!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type=hidden name=AllOperator>
     <input type=hidden name=sql>
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
