<%
  //程序名称：LLPrepaidClaimInputInit.jsp
  //程序功能：预付赔款录入
  //创建日期：2010-11-25
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initForm()
{
	try{
		initGrpContGrid();
		initPrepaidClaimGrid();
	}
	catch(re){
		alert("LLPrepaidClaimInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initGrpContGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="管理机构";
		iArray[1][1]="40px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="团体保单号";
		iArray[2][1]="70px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		iArray[2][21]="grpcontno";

		iArray[3]=new Array();
		iArray[3][0]="投保单位名称";
		iArray[3][1]="100px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="生效日期";
		iArray[4][1]="50px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="失效日期";
		iArray[5][1]="50px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="预付状态编码";
		iArray[6][1]="0px";
		iArray[6][2]=100;
		iArray[6][3]=3;
		iArray[6][21]="state";
		
		iArray[7]=new Array();
		iArray[7][0]="预付状态";
		iArray[7][1]="50px";
		iArray[7][2]=100;
		iArray[7][3]=0;
		
		iArray[8]=new Array();
		iArray[8][0]="已预付金额";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="预付赔款余额";
		iArray[9][1]="60px";
		iArray[9][2]=100;
		iArray[9][3]=0;

		GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" ); 

		GrpContGrid.mulLineCount=1;
		GrpContGrid.displayTitle=1;
		GrpContGrid.canSel=1;
		GrpContGrid.canChk=0;
		GrpContGrid.hiddenPlus=1;
		GrpContGrid.hiddenSubtraction=1;
		GrpContGrid.selBoxEventFuncName = "onSelGrpInfo";

		GrpContGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

function initPrepaidClaimGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="险种编码";
		iArray[1][1]="50px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="riskcode";

		iArray[2]=new Array();
		iArray[2][0]="险种名称";
		iArray[2][1]="120px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="实收保费";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;
		iArray[3][21]="pay";

		iArray[4]=new Array();
		iArray[4][0]="已预付赔款";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="结案赔款";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="本次预付金额";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=1;
		iArray[6][9]="本次预付金额|NOTNULL&value>0";
		iArray[6][21]="money";
		
		iArray[7]=new Array();
		iArray[7][0]="团体险种号";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=3;
		iArray[7][21]="grppolno";

		PrepaidClaimGrid = new MulLineEnter( "fm" , "PrepaidClaimGrid" ); 

		PrepaidClaimGrid.mulLineCount=1;
		PrepaidClaimGrid.displayTitle=1;
		PrepaidClaimGrid.canSel=0;
		PrepaidClaimGrid.canChk=1;
		PrepaidClaimGrid.hiddenPlus=1;
		PrepaidClaimGrid.hiddenSubtraction=1;

		PrepaidClaimGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

</script>
