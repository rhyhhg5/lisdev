<%
//Name    ：LLClaimPopedomSave.jsp
//Function：理赔权限配置保存
//Author   :Xx
//Date：2005-8-26@民生
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%

  LLClaimPopedomSet tLLClaimPopedomSet = new LLClaimPopedomSet();

  CErrors tError = null;
  String transact = request.getParameter("fmtransact");
  System.out.println("LLClaimPopedomSave.jsp本次执行的操作是"+transact);

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  
    String mClaimPopedomName = StrTool.unicodeToGBK(request.getParameter("ClaimPopedomName")); 
  	LLClaimPopedomSchema tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("1");
    tLLClaimPopedomSchema.setGetDutyType("1");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("PayAmntH"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);
    
  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("1");
    tLLClaimPopedomSchema.setGetDutyType("2");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("PayAmntH"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);

  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("1");
    tLLClaimPopedomSchema.setGetDutyType("3");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("RefuseAmntH"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);

  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("1");
    tLLClaimPopedomSchema.setGetDutyType("4");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("ConsultAmntH"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);

  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("1");
    tLLClaimPopedomSchema.setGetDutyType("5");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("AccomAmntH"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);

  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("2");
    tLLClaimPopedomSchema.setGetDutyType("1");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("PayAmntA"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);
    
  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("2");
    tLLClaimPopedomSchema.setGetDutyType("2");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("PayAmntA"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);

  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("2");
    tLLClaimPopedomSchema.setGetDutyType("3");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("RefuseAmntA"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);
                                              
  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("2");
    tLLClaimPopedomSchema.setGetDutyType("4");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("ConsultAmntA"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);
                                              
  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("2");
    tLLClaimPopedomSchema.setGetDutyType("5");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("AccomAmntA"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);
                                              
  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("3");
    tLLClaimPopedomSchema.setGetDutyType("0");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("Exemption"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);
                                              
  	tLLClaimPopedomSchema = new LLClaimPopedomSchema();
    tLLClaimPopedomSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    tLLClaimPopedomSchema.setPopedomName(request.getParameter("ClaimPopedomName"));
    tLLClaimPopedomSchema.setGetDutyKind("4");
    tLLClaimPopedomSchema.setGetDutyType("0");
    tLLClaimPopedomSchema.setLimitMoney(request.getParameter("ContCancel"));
    tLLClaimPopedomSet.add(tLLClaimPopedomSchema);
                                              
    GlobalInput tG = new GlobalInput();       
    tG=(GlobalInput)session.getValue("GI");   
                                              
    VData tVData = new VData();               
    LLClaimPopedomUI tClaimPopedomUI = new LLClaimPopedomUI();
    try                                       
    {                                         
      tVData.addElement(tLLClaimPopedomSet);  
      tVData.addElement(tG);                  
      tClaimPopedomUI.submitData(tVData,transact);
    }                                         
    catch(Exception ex)                       
    {                                         
      Content = transact+"失败，原因是:" + ex.toString();
      FlagStr = "Fail";                       
    }                                         
                                              
    if (FlagStr=="")                          
    {                                         
      tError = tClaimPopedomUI.mErrors;       
      if (!tError.needDealError())            
      {                                       
        Content = " 操作成功！";              
        FlagStr = "Succ";                     
      }                                       
      else                                    
      {                                       
        Content = " 操作失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";                     
      }                                       
    }                                         
                                              
  //处理转意字符                              
%>                                            
<html>                                        
<script language="javascript">                
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>                                     
</html>                                       
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              
                                              