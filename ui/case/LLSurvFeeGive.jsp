<%
//程序名称：LLSurvFeeInit.jsp
//程序功能：调查费用确认给付
//创建日期：2005-2-26 12:07
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page contentType="text/html;charset=GBK" %>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String Operator=tG.Operator;
String Comcode=tG.ManageCom;
String CurrentDate= PubFun.getCurrentDate();
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
%>
<html>
	<head>
		<title>调查费结算</title>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="./CaseCommon.js"></SCRIPT>
		<SCRIPT src="./LLSurvFeeGive.js"></script>

		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLSurvFeeGiveInit.jsp"%>
		<script language="javascript">
			function initDate(){
				fm.Month.value="<%=tCurrentMonth%>"/1-1;
				fm.Year.value="<%=tCurrentYear%>";
				fm.Operator.value="<%=Operator%>"
				fm.comcode.value="<%=Comcode%>";
			}
		</script>
	</head>
	<body  onload="initDate();initForm();">
		<!--登录画面表格-->
		<form name=fm target=fraSubmit method=post>
			<br>
			<Table>
				<TR>
					<TD class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divIndirectFee);">
					</TD>
					<TD class= titleImg>调查费结算</TD>
				</TR>
			</Table>
			<table class= common border=0 width=60%>
				<TR  class= common>
					<TD  class= title>统计年份</TD>
					<TD class= input> <Input class=common name= Year onkeydown="QueryOnKeyDown();"></TD>
					<TD  class= title>统计月份</TD>
					<TD  class= input> <Input name=Month class=codeno CodeData="0|^1|一月|M^2|二月|M^3|三月|M^4|四月|M^5|五月|M^6|六月|M^7|七月|M^8|八月|M^9|九月|M^10|十月|M^11|十一月|M^12|十二月|M" ondblClick="showCodeListEx('Month',[this,MonthName],[0,1]);" onkeyup="showCodeListKeyEx('Month',[this,MonthName],[0,1]);" verify="统计月份|notnull" elementtype=nacessary><input class=codename name=MonthName> </TD>
					<TD  class= title>理赔号</TD>
					<TD  class= input> <Input name=CaseNo class=common > </TD>
				</TR>
			</table>
			<INPUT VALUE="查  询" class= cssButton TYPE=button onclick="queryAllFee();">
			<hr>
			
			<Table>
				<TR>
					<TD class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInqFee);">
					</TD>
					<TD class= titleImg>直接调查费</TD>
				</TR>
			</Table>
			<Div  id= "divInqFee" style= "display: ''" >
				<Table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanInqFeeGrid" ></span>
						</TD>
					</TR>
				</Table>
			</div>
			<hr>
			<Table>
				<TR>
					<TD class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divIndFee);">
					</TD>
					<TD class= titleImg>间接调查费</TD>
				</TR>
			</Table>
			<Div  id= "divIndFee" style= "display: ''">
				<Table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanIndirectFeeGrid" ></span>
						</TD>
					</TR>
				</Table>
				<br>
    		<table  class= common>
					<TR  class= common8>
						<TD  class= title8>间接调查费总额</TD><TD  class= input8><Input class= readonly name="IndFeeSum" readonly></TD>
						<TD  class= title8>均摊案件数</TD><TD  class= input8><Input class= readonly name="CaseCount" readonly></TD>
					</TR> 
				</table>
			</Div>
			<hr>
			<div id="div1" align=left>
				<INPUT VALUE="给付确认" name="GiveConfirm" class= cssButton TYPE=button onclick="submitForm();">
			</div>
			<hr>
			<Table>
				<TR>
					<TD class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGiveFee);">
					</TD>
					<TD class= titleImg>既往结算信息</TD>
				</TR>
			</Table>
			<Div  id= "divGiveFee" style= "display: ''" >
				<Table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanGiveFeeGrid" ></span>
						</TD>
					</TR>
				</Table>
				<INPUT VALUE="返  回" class= cssButton TYPE=button onclick="top.close();">
				<INPUT VALUE="费用明细打印" class= cssButton TYPE=button onclick="SurvFeePrt();">
			</div>
			<hr>

			<input type=hidden class= common name="fmtransact">
			<input type=hidden class= common name="MStartDate">
			<input type=hidden class= common name="MEndDate">
			<input type=hidden class= common name="Operator">
			<Input type=hidden class= common name="comcode" >
			<Input type=hidden class= common name="BatchNo" >
			<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		</Form>
	</body>

</html>
