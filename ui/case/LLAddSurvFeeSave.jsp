<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLAddSurvFeeSave.jsp
//程序功能：间接调查费录入
//创建日期：2005-6-26 12:07
//创建人  ：Xx
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
LLIndirectFeeSet tLLIndirectFeeSet   = new LLIndirectFeeSet();
LLCaseIndFeeSet tLLCaseIndFeeSet = new LLCaseIndFeeSet();
TransferData tTransferData = new TransferData();
LLIndirectFeeUI tLLIndirectFeeUI   = new LLIndirectFeeUI();
//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";
String transact = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

transact = request.getParameter("fmtransact");
String tsFeeDate = request.getParameter("MEndDate");
tTransferData.setNameAndValue("FeeDate",tsFeeDate);

//if(transact.equals("CONFIRM")){
//String tRNum[] = request.getParameterValues("InpAddFeeGridChk");
//String tsFeeItem[] = request.getParameterValues("AddFeeGrid2");//调查费用项目
//String tsFee[] = request.getParameterValues("AddFeeGrid3");	//金额
//
//if(tsFeeItem!=null){
//	for (int i = 0; i < tsFeeItem.length; i++){
//	  LLIndirectFeeSchema tLLIndirectFeeSchema = new LLIndirectFeeSchema();
//	  tLLIndirectFeeSchema.setFeeItem(tsFeeItem[i]);
//	  tLLIndirectFeeSchema.setFeeSum(tsFee[i]);
//	  tLLIndirectFeeSchema.setFeeDate(tsFeeDate);
//		if ( "1".equals( tRNum[i]) ){
//		  tLLIndirectFeeSchema.setUWState("2");
//	  }
//	else{
//		  tLLIndirectFeeSchema.setUWState("3");
//		}
//	  if(tLLIndirectFeeSchema.getFeeSum()>0){
//	    tLLIndirectFeeSet.add(tLLIndirectFeeSchema);
//	  }
//	}
//}
//
//String tsCaseno[] = request.getParameterValues("SurveyCaseGrid1");
//if (tsCaseno != null){
//	int CaseCount = tsCaseno.length; 
//  for (int i = 0; i < CaseCount; i++){
//      	LLCaseIndFeeSchema tLLCaseIndFeeSchema = new LLCaseIndFeeSchema();
//      	tLLCaseIndFeeSchema.setOtherNo(tsCaseno[i]);
//      	tLLCaseIndFeeSchema.setIndirectSN("0000000000");
//      	tLLCaseIndFeeSchema.setFeeDate(tsFeeDate);
//      	tLLCaseIndFeeSet.add(tLLCaseIndFeeSchema);
//  }
//}
//}
if(transact.equals("INSERT")){
	String tsFeeItem[] = request.getParameterValues("AddFeeGrid2");//调查费用项目
	String tsFee[] = request.getParameterValues("AddFeeGrid3");	//金额
	int feeItemCount = tsFeeItem.length;
	
	for (int i = 0; i < feeItemCount; i++){
	  try{
	    LLIndirectFeeSchema tLLIndirectFeeSchema = new LLIndirectFeeSchema();
	    tLLIndirectFeeSchema.setFeeItem(tsFeeItem[i]);
	    tLLIndirectFeeSchema.setFeeSum(tsFee[i]);
	    tLLIndirectFeeSchema.setFeeDate(tsFeeDate);
	    tLLIndirectFeeSchema.setUWState("0");
	    if(tLLIndirectFeeSchema.getFeeSum()>0){
	      System.out.println("费用项金额=="+tLLIndirectFeeSchema.getFeeSum());
	      tLLIndirectFeeSet.add(tLLIndirectFeeSchema);
	    }
	    System.out.println("费用项金额个数=="+tLLIndirectFeeSet.size());
	  }
	  catch (Exception ex){
	    System.out.println("第"+(i+1)+"项金额不是数字。");
	  }
	}
	
	String tRNum[] = request.getParameterValues("InpSurveyCaseGridChk");
	String tsCaseno[] = request.getParameterValues("SurveyCaseGrid1");
	if (tsCaseno != null){
		int CaseCount = tsCaseno.length; 
	  for (int i = 0; i < CaseCount; i++){
			 if ("1".equals(tRNum[i]) ){
	      	LLCaseIndFeeSchema tLLCaseIndFeeSchema = new LLCaseIndFeeSchema();
	      	tLLCaseIndFeeSchema.setOtherNo(tsCaseno[i]);
	      	tLLCaseIndFeeSchema.setIndirectSN("0000000000");
	      	tLLCaseIndFeeSchema.setFeeDate(tsFeeDate);
	      	tLLCaseIndFeeSet.add(tLLCaseIndFeeSchema);
	      }
	  }
	}
}
else if(transact.equals("CONFIRM"))
{

	String tsFeeItem[] = request.getParameterValues("AddFeeGrid2");	//调查费用项目
	String tIndirectSN[] = request.getParameterValues("AddFeeGrid5");	//录入批次流水
	String tsFee[] = request.getParameterValues("AddFeeGrid3");	//金额
	int feeItemCount = tsFeeItem.length;
	
	for (int i = 0; i < feeItemCount; i++)
	{
	  try
	  {
	    LLIndirectFeeSchema tLLIndirectFeeSchema = new LLIndirectFeeSchema();
	    tLLIndirectFeeSchema.setFeeItem(tsFeeItem[i]);
	    tLLIndirectFeeSchema.setIndirectSN(tIndirectSN[i]);
	    tLLIndirectFeeSchema.setFeeSum(tsFee[i]);
	    tLLIndirectFeeSchema.setFeeDate(tsFeeDate);
	    tLLIndirectFeeSchema.setUWState("1");  //0-未审核 1-审核
	    if(tLLIndirectFeeSchema.getFeeSum()>0)
	    {
	    	tLLIndirectFeeSet.add(tLLIndirectFeeSchema);
	    }    	
  	    System.out.println("间接调查费用录入批次流水号======"+tIndirectSN[i]);
  	    System.out.println("tLLIndirectFeeSet.size()======"+tLLIndirectFeeSet.size());
	   
	  }
	  catch (Exception ex)
	  {
	    System.out.println("第"+(i+1)+"项金额不是数字。");
	  }
	}
	
	String tRNum[] = request.getParameterValues("InpSurveyCaseGridChk");
	String tsCaseno[] = request.getParameterValues("SurveyCaseGrid1");
	String tCount[] = request.getParameterValues("SurveyCaseGrid2");
	if (tsCaseno != null)
	{
	  int CaseCount = tsCaseno.length; 
	  for (int i = 0; i < CaseCount; i++)
	  {		
		if ("1".equals(tRNum[i]) )
		{
			LLCaseIndFeeSchema tLLCaseIndFeeSchema = new LLCaseIndFeeSchema();
	      	tLLCaseIndFeeSchema.setOtherNo(tsCaseno[i]);	//案件号
	      	tLLCaseIndFeeSchema.setIndirectSN(tCount[i]); 	//暂时存调查次数
	      	tLLCaseIndFeeSet.add(tLLCaseIndFeeSchema);
		}      		  
	  }
	}
}

try{
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tLLIndirectFeeSet);
  tVData.add(tLLCaseIndFeeSet);
  tVData.add(tTransferData);
  tVData.add(tG);
  tLLIndirectFeeUI.submitData(tVData,transact);
}
catch(Exception ex){
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}

//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr==""){
  tError = tLLIndirectFeeUI.mErrors;
  if (!tError.needDealError()){
    Content = " 保存成功! ";
    FlagStr = "Success";
  }
  else{
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

//添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
