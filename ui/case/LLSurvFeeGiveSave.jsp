<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLSurvFeeGiveSave.jsp
//程序功能：调查费用给付
//创建日期：2005-08-19 11:10:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//输出参数
CErrors tError = null;
String FlagStr = "Fail";
String Content = "";

GlobalInput tG = new GlobalInput();

tG=(GlobalInput)session.getValue("GI");

if(tG == null) {
	out.println("session has expired");
	return;
}

String aYear = request.getParameter("Year");
String aMonth = request.getParameter("Month");
String aBatchNo = request.getParameter("BatchNo");
TransferData aTransferData = new TransferData();
aTransferData.setNameAndValue("Year",aYear);
aTransferData.setNameAndValue("Month",aMonth);
aTransferData.setNameAndValue("BatchNo",aBatchNo);

LLInqFeeStaSet aLLInqFeeStaSet = new LLInqFeeStaSet();

String tRNum[] = request.getParameterValues("InpInqFeeGridChk");
String tOtherNo[] = request.getParameterValues("InqFeeGrid1");
String tSurvFee[] = request.getParameterValues("InqFeeGrid3");
String tSurveyNo[] = request.getParameterValues("InqFeeGrid6");
if(tRNum!=null){
  	for (int i = 0; i < tRNum.length; i++){
			if ( "1".equals( tRNum[i]) ){
				LLInqFeeStaSchema tLLInqFeeStaSchema = new LLInqFeeStaSchema();
				tLLInqFeeStaSchema.setOtherNo(tOtherNo[i]);
				tLLInqFeeStaSchema.setSurveyNo(tSurveyNo[i]);
				System.out.println(tLLInqFeeStaSchema.getSurveyNo());
				tLLInqFeeStaSchema.setSurveyFee(tSurvFee[i]);
				tLLInqFeeStaSchema.setInqFee(tSurvFee[i]);
				tLLInqFeeStaSchema.setIndirectFee(0);
				aLLInqFeeStaSet.add(tLLInqFeeStaSchema);
			}
	}
}
String tNo[] = request.getParameterValues("IndirectFeeGridChk");
String ttOtherNo[] = request.getParameterValues("IndirectFeeGrid1");
String tIndFee[] = request.getParameterValues("IndirectFeeGrid2");
if(tNo!=null){
	for(int i=0 ; i<tNo.length ; i++ ){
		System.out.println("..................."+i);
		LLInqFeeStaSchema tLLInqFeeStaSchema = new LLInqFeeStaSchema();
		tLLInqFeeStaSchema.setOtherNo(ttOtherNo[i]);
		tLLInqFeeStaSchema.setSurveyNo(ttOtherNo[i]);
		System.out.println(tLLInqFeeStaSchema.getSurveyNo());
		tLLInqFeeStaSchema.setSurveyFee(tIndFee[i]);
		tLLInqFeeStaSchema.setIndirectFee(tIndFee[i]);
		tLLInqFeeStaSchema.setInqFee(0);
		aLLInqFeeStaSet.add(tLLInqFeeStaSchema);
	}
}

// 准备传输数据 VData
VData tVData = new VData();
FlagStr="";

tVData.add(tG);
tVData.add(aLLInqFeeStaSet);
tVData.add(aTransferData);

LLInqGiveEnsureUI tLLInqGiveEnsureUI = new LLInqGiveEnsureUI();
try {
	System.out.println("this will save the data!!!");
	tLLInqGiveEnsureUI.submitData(tVData,"GIVE||ENSURE");
}
catch(Exception ex) {
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}

if (!FlagStr.equals("Fail")) {
	tError = tLLInqGiveEnsureUI.mErrors;
	if (!tError.needDealError()) {
		Content = " 保存成功! ";
		FlagStr = "Succ";
	}
	else {
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
