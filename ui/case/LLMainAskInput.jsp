<html>
<%
  //Name：LLMainAskInput.jsp
  //Function：登记界面的初始化
  //Date：2004-12-23 16:49:22
  //Author：wujs
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LLMainAskInput.js"></SCRIPT>
	<%@include file="LLMainAskInputInit.jsp"%>
<%
  String CurrentDate = PubFun.getCurrentDate();
  String AheadDays = "-60";
  FDate tD = new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate), Integer.parseInt(AheadDays), "D", null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString(AfterDate);
%>
<script language="javascript">
   function initDate(){
   fm.CDateS.value="<%=afterdate%>";
   fm.CDateE.value="<%=CurrentDate%>";
  }
</script>
</head>
<body onload="initDate();initForm();initElementtype();">
<form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
<%@include file="ConsultTitle.jsp"%>
<table>
  <TR>
    <TD>
      <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divClientInfo);">
    </TD>
    <TD class=titleImg>客户信息</TD>
  </TR>
</table>
<Div id="divClientInfo" style="display: ''">
  <table class=common>
    <TR class=common8>
      <TD class=title8>客户姓名</TD>
      <TD class=input8>
        <input class=common name="LogName" elementtype=nacessary verify="客户姓名|notnull&len<=20"  onkeydown="QueryOnKeyDown()"></TD>
						<TD  class= title8>客户号码</TD>
						<TD  class= input8><input class= common name="LogerNo" elementtype=nacessary verify="客户号码|notnull&len<=20" onkeydown="QueryOnKeyDown()"></TD>
						<TD  class= title8>证件号码</TD>
						<TD  class= input8><input class= common name="IDNo" verify="身份证号|len<=20" onkeydown="QueryOnKeyDown()"></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>电话号码</TD>
						<TD  class= input8><input class= common name="Phone" verify="电话号码|num&&len<=20"></TD>
						<TD  class= title8>手机号码</TD>
						<TD  class= input8><input class= common name="Mobile" verify="手机号码|num&&len>=8">
      </TD>
      <TD class=title8>电子邮箱</TD>
      <TD class=input8>
        <input class=common name="Email" verify="电子邮箱|Email">
      </TD>
    </TR>
    <TR class=common8>
      <TD class=title8>单位名称</TD>
      <TD class=input8>
        <input class=common name="LogComp" verify="单位名称|len<=20"></TD>
						<TD class= title8>通讯地址</TD>
						<TD class= input8><input class= common name="AskAddress" verify="通信地址|len<=20"></TD>
						<TD class= title8>登记方式</TD>
						<TD class= input8><input class=codeno name="AskMode" onClick="showCodeList('LLAskMode',[this,AskModeName],[0,1]);" onkeyup="showCodeListKeyEx('LLAskMode',[this,AskModeName],[0,1]);" ><input class= codename name="AskModeName" ></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>回复方式</TD>
						<TD  class= input8><Input class="codeno" name="AnswerMode" onClick="showCodeList('LLreturnMode',[this,AnswerModeName],[0,1]);" onkeyup="showCodeListKeyEx('LLreturnMode',[this,AnswerModeName],[0,1]);"  ><Input class= codename name=AnswerModeName ></TD>
						<TD  class= title8>客户现状</TD>
						<TD  class= input8><input class="codeno" name="CustStatus" onClick="showCodeList('llcuststatus',[this,CustStatusName],[0,1]);" onkeyup="showCodeListKeyEx('llcuststatus',[this,CustStatusName],[0,1]);" ><Input class= codename name="CustStatusName"></TD>
						<TD  class= title8>出险类别</TD>
						<TD  class= input8><input class= codeno  name="AccType" CodeData="0|2^1|疾病^2|意外" onClick="return showCodeListEx('llacctype',[this,AccTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('llacctype',[this,AccTypeName],[0,1]);"><Input class= codename name="AccTypeName"></TD>
					</TR>
				</table>
			</DIV>

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divConsultInfo);">
					</TD>
					<TD class= titleImg>
						咨询通知信息
					</TD>
				</TR>
			</table>
			<div id="divConsultInfo" style="display:''">
				<table  class= common>
					<TR  class= common>
						<TD  class= title colspan="6">信息内容</TD>
					</TR>
					<TR  class= common>
						<TD  class= input colspan="6">
							<textarea name="CContent" cols="100%" rows="3" witdh=25% class="common"></textarea>
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title colspan="6">回复内容</TD>
					</TR>
					<TR  class= common>
						<TD  class= input colspan="6">
							<textarea name="Answer" cols="100%" rows="3" witdh=25% class="common"></textarea>
						</TD>
					</TR>
					<Input type=hidden class= common name="ReplyState">
				</table>
			</div>

			<Table>
				<TR class=common>
					<TD class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHospiInfo);"></TD>
					<TD class= titleImg>就诊医院</TD>
				</TR>
			</Table>
			<Div  id= "divHospiInfo" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanCommHospitalGrid">
							</span>
						</TD>
					</TR>
				</table>
			</Div>

			<Table>
				<TR class=common>
					<TD class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEventInfo);"></TD>
					<TD class= titleImg>关联事件</TD>
				</TR>
			</Table>
			<Div  id= "divEventInfo" style= "display: ''">
				<Table class=common>
					<tr>
						<TD  class= title8>起始日期</TD>
						<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=CDateS onkeydown="QueryOnKeyDown()"></TD>
						<TD  class= title8>终止日期</TD>
						<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=CDateE onkeydown="QueryOnKeyDown()"></TD>
					</TR>
					<TR  class= common>
						<TD text-align: left colSpan=4>
							<span id="spanSubReportGrid"></span>
						</TD>
					</TR>
				</table>
			</div>

			<Div  id= "divLLLLEventInput1" style= "display: 'none'">
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>重大事件标志</TD><TD  class= input8><Input class= code8 name="SeriousGrade1" CodeData= "0|^1|重大事件^2|一般事件" ondblClick="showCodeListEx('SeriousGrade',[this],[0]);" onkeyup="showCodeListKeyEx('SeriousGrade',[this],[0]);"></TD>
						<TD  class= title8>发生日期</TD><TD class=input8><Input class= coolDatePicker  name="AccDate1"></TD>
						<TD  class= title8>终止日期</TD><TD class=input8><Input class= coolDatePicker  name="AccEndDate1"></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>事故地点</TD><TD class=input8><Input class= common name="AccPlace1"></TD>
						<TD  class= title8>医院代码</TD><TD class=input8><Input class= code name="HospitalCode" ondblclick="getHospitCode();return showCodeListEx('GetHospitCode',[this,HospitalName],[0,1],'', '', '', true);" onkeyup="getHospitCode();return showCodeListKeyEx('GetHospitCode',[this,HospitalName],[0,1],'', '', '', true);"></TD>
						<TD  class= title8>医院名称</TD><TD class=input8><Input class= common name="HospitalName" ></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>客户现状</TD><TD class= input8><Input class= code name="CustSituation1"  ondblClick="showCodeList('llcuststatus',[this],[0]);" onkeyup="showCodeListKeyEx('llcuststatus',[this],[0]);" ></TD>
						<TD  class= title8>入院日期</TD><TD class= common8><Input class= 'coolDatePicker' dateFormat="Short"  name="InHospitalDate"></TD>
						<TD  class= title8>出院日期</TD><TD class= common8><Input class= 'coolDatePicker' dateFormat="Short"  name="OutHospitalDate"></TD>
					</TR>
					<TR  class= common>
						<TD  class= title colspan="6">事故描述</TD>
						<TD  class= title8>登记类型</TD><TD  class= input8><input class= code8 name="AskType" CodeData= "0|^0|咨询^1|通知^2|咨询通知" ondblClick="showCodeListEx('AskType',[this,''],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('AskType',[this,''],[0,1],'', '', '', 1);" elementtype=nacessary verify="登陆类型|len<=1"></TD>
					</TR>
					<TR  class= common>
						<TD  class= input colspan="6">
							<textarea name="AccDesc1" cols="100%" rows="3" witdh=25% class="common"></textarea>
						</TD>
					</TR>
				</table>
				<input class=cssButton type=button value="添加事件" onclick="EventSave()">
			</div>
			<hr>
			<div id="divconfirm" align='right'>
				<input class=cssButton style='width: 60px;' type=button value="确  认" onclick="submitForm()" id= "sub1">
				<input class=cssButton style='width: 60px;' type=button value="提起调查" onclick="submitFormSurvery()" id = "sub2">
        <input class=cssButton style='width: 90px;' type=button value="咨询回执(VTS)" onclick="printPage()" id= "sub3">
        <input class=cssButton style='width: 90px;' type=button value="咨询回执(PDF)" onclick="printPDF()" id= "sub4">
			</div>
			<!--隐藏域-->
			<input name ="fmtransact" type="hidden">
			<input name ="CustomerNo" type="hidden">
			<input name ="CustomerName" type="hidden">
			<input name ="LogNo" type="hidden">
			<input name ="ConsultNo1" type="hidden">
			<input name ="CNNo" type="hidden">
			<input name = "PostCode" type="hidden">
			<input name ="NoticeNo" type="hidden">
			<input name ="EmailContent" type="hidden">
			<input name ="SMSContent" type="hidden">
			<input name ="SaveFlag" value="0" type="hidden">
			<input name ="LoadC" type="hidden">
			<input name ="LoadD" type="hidden">
			<input name ="LogDate" type="hidden">
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>