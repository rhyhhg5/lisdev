<html>
<%
//Name：LLMainAskInput.jsp
//Function：审定审批案件查询
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLClaimConfList.js"></SCRIPT>
   <%@include file="LLClaimConfListInit.jsp"%>
 </head>

 <body  onload="initForm();" >
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         审批审定工作列表
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
       <table  class= common>
<TR  class= common8>
	<TD  class= title8>团体批次号</TD>
	<TD  class= input8><input class= common name="RgtNo" ></TD>
	<TD  class= title8>理赔号</TD>
	<TD  class= input8><input class= common name="CaseNo" ></TD>
	<TD  class= title8>申请人</TD>
	<TD  class= input8><input class= common name="RgtantName"></TD>
</TR>

<TR  class= common8>
	<TD  class= title8>客户号</TD>
	<TD  class= input8><input class= common name="CustomerNo" ></TD>
	<TD  class= title8>客户姓名</TD>
	<TD  class= input8><input class= common name="CustomerName"></TD>
</TR>

<TR  class= common8>
	<TD  class= title8>申请日期 起始</TD>
	<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateStart ></TD>
	<TD  class= title8>结束</TD>
	<TD  class= input8><Input class="coolDatePicker"  dateFormat="short" name=RgtDateEnd ></TD>
</TR>

</table>
</DIV>

   <input name="AskIn" style="display:''"  class=cssButton type=button value="查 询" onclick="easyQueryClick()">
   

    <Div  id= "divCaseList" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCaseGrid" >
            </span>
          </TD>
        </TR>
        
        <INPUT type="hidden" class=Common name=hiddenCaseNo value="">
      </table>
         <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 		  
	
    </Div>
    
        <br>
        
	<input name="AskIn" style="display:''"  class=cssButton type=button value="审批审定" onclick="UWClaim()">
	<!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
