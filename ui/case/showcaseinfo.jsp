<%@page contentType="text/html;charset=GBK" %>
<%
/*******************************************************************************
  * Name    ：showcaseinfo.jsp
  * Function：显示立案或者上理赔中的事故明细/伤残明细
  * Author  ：LiuYansong
  * Date    : 2003-7-28
 */
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  String DisplayFlag = request.getParameter("DisplayFlag");
  LLCaseSchema mLLCaseSchema = new LLCaseSchema();
  mLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.addElement(DisplayFlag);
  tVData.addElement(mLLCaseSchema);
	tVData.addElement(tG);
  // 数据传输
  CaseInfoUI mCaseInfoUI = new CaseInfoUI();

  if(!mCaseInfoUI.submitData(tVData,"SHOW"))
	{
    FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = mCaseInfoUI.getResult();
		System.out.println(tVData.size());
    LLCaseInfoSet tLLCaseInfoSet  = new LLCaseInfoSet();
    tLLCaseInfoSet.set((LLCaseInfoSet)tVData.getObjectByObjectName("LLCaseInfoSet",0));
    System.out.println("所得到的结果集是"+tLLCaseInfoSet.size());
    int count=tLLCaseInfoSet.size();
    if(tLLCaseInfoSet.size()>0)
    {
      if(DisplayFlag.equals("0"))
      {
        %>
        <script language="javascript">
          parent.fraInterface.CaseGrid.clearData();
          parent.fraInterface.DiseaseGrid.clearData();
        </script>
        <%
          LLCaseInfoSet ywLLCaseInfoSet = new LLCaseInfoSet();
        LLCaseInfoSet jbLLCaseInfoSet = new LLCaseInfoSet();
        String yw="0";
        String jb="0";
        for(int j=1;j<=count;j++)
        {
          LLCaseInfoSchema tLLCaseInfoSchema = new LLCaseInfoSchema();
          tLLCaseInfoSchema.setSchema(tLLCaseInfoSet.get(j));
          String yw_jb = tLLCaseInfoSchema.getAffixSerialNo();
          if(yw_jb.substring(0,2).equals("YW"))
          {
            ywLLCaseInfoSet.add(tLLCaseInfoSchema);
            yw="1";
          }
          if(yw_jb.substring(0,2).equals("JB"))
          {
            jbLLCaseInfoSet.add(tLLCaseInfoSchema);
            jb="1";
          }
        }

        if(ywLLCaseInfoSet.size()>0)
        {
          for(int yw_count=1;yw_count<=ywLLCaseInfoSet.size();yw_count++)
          {
            %>
            <script language="javascript">
              parent.fraInterface.CaseGrid.addOne("CaseGrid");
              parent.fraInterface.CaseGrid.setRowColData(<%=yw_count-1%>,1,"<%=ywLLCaseInfoSet.get(yw_count).getCode()%>");
              parent.fraInterface.CaseGrid.setRowColData(<%=yw_count-1%>,2,"<%=ywLLCaseInfoSet.get(yw_count).getName()%>");
            </script>
            <%
          }
        }
        if(jbLLCaseInfoSet.size()>0)
        {
          for(int jb_count=1;jb_count<=jbLLCaseInfoSet.size();jb_count++)
          {
            %>
            <script language="javascript">
              parent.fraInterface.DiseaseGrid.addOne("DiseaseGrid");
              parent.fraInterface.DiseaseGrid.setRowColData(<%=jb_count-1%>,1,"<%=jbLLCaseInfoSet.get(jb_count).getCode()%>");
              parent.fraInterface.DiseaseGrid.setRowColData(<%=jb_count-1%>,2,"<%=jbLLCaseInfoSet.get(jb_count).getName()%>");
            </script>
            <%
          }
        }
        if(yw.equals("1")&&jb.equals("1"))
        {
          %>
          <script language="javascript">
            parent.fraInterface.fm.all("AccitentType").value = "JB和YW";
          </script>
          <%
        }
        if(yw.equals("0")&&jb.equals("1"))
        {
          %>
          <script language="javascript">
            parent.fraInterface.fm.all("AccitentType").value = "JB";
          </script>
          <%
        }
        if(yw.equals("1")&&jb.equals("0"))
        {
          %>
          <script language="javascript">
            parent.fraInterface.fm.all("AccitentType").value = "YW";
          </script>
          <%
        }
      }
      if(DisplayFlag.equals("1"))
      {
        %>
        <script language="javascript">
          parent.fraInterface.DeformityGrid.clearData();
        </script>
        <%
          //判断该伤残结果是有疾病导致的还上由意外导致的
          String jb_1 = "0";
          String yw_1 = "0";
          for(int sc_count=1;sc_count<=tLLCaseInfoSet.size();sc_count++)
          {
            LLCaseInfoSchema scLLCaseInfoSchema = new LLCaseInfoSchema();
            scLLCaseInfoSchema.setSchema(tLLCaseInfoSet.get(sc_count));
            if(scLLCaseInfoSchema.getType().equals("YW"))
              yw_1="1";
            if(scLLCaseInfoSchema.getType().equals("JB"))
              jb_1="1";
            %>
            <script language="javascript">
              parent.fraInterface.DeformityGrid.addOne("DeformityGrid");
              parent.fraInterface.DeformityGrid.setRowColData(<%=sc_count-1%>,1,"<%=tLLCaseInfoSet.get(sc_count).getDeformityGrade()%>");
              parent.fraInterface.DeformityGrid.setRowColData(<%=sc_count-1%>,2,"<%=tLLCaseInfoSet.get(sc_count).getGradeName()%>");
              parent.fraInterface.DeformityGrid.setRowColData(<%=sc_count-1%>,3,"<%=tLLCaseInfoSet.get(sc_count).getCode()%>");
              parent.fraInterface.DeformityGrid.setRowColData(<%=sc_count-1%>,4,"<%=tLLCaseInfoSet.get(sc_count).getName()%>");
              parent.fraInterface.DeformityGrid.setRowColData(<%=sc_count-1%>,5,"<%=tLLCaseInfoSet.get(sc_count).getAppDeformityRate()%>");
              parent.fraInterface.DeformityGrid.setRowColData(<%=sc_count-1%>,6,"<%=tLLCaseInfoSet.get(sc_count).getDeformityRate()%>");
              parent.fraInterface.DeformityGrid.setRowColData(<%=sc_count-1%>,7,"<%=tLLCaseInfoSet.get(sc_count).getDiagnosis()%>");
            </script>
            <%
          }
          if(yw_1.equals("1")&&jb_1.equals("1"))
          {
            %>
            <script language="javascript">
              parent.fraInterface.fm.all("AccitentType").value = "JB和YW";
            </script>
            <%
          }
          if(yw_1.equals("0")&&jb_1.equals("1"))
          {
            %>
            <script language="javascript">
              parent.fraInterface.fm.all("AccitentType").value = "JB";
            </script>
            <%
          }
          if(yw_1.equals("1")&&jb_1.equals("0"))
          {
            %>
            <script language="javascript">
              parent.fraInterface.fm.all("AccitentType").value = "YW";
            </script>
            <%
          }
        }
      }
	}
%>