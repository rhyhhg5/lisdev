<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%
//程序名称：PEdorUWManuConfirmSave.jsp
//程序功能：保全个单人工核保核保完毕确认
//创建日期：2005-09-07
//创建人  ：QiuYang
//更新记录：更新人    更新日期     更新原因/内容
%>
<%
		String flag = "";
	  String content = "";
	  
		GlobalInput gi = (GlobalInput)session.getValue("GI");
		
		TransferData td = new TransferData();
		td.setNameAndValue("EdorNo", request.getParameter("EdorNo"));
    td.setNameAndValue("MissionId", request.getParameter("MissionId"));
    td.setNameAndValue("SubMissionId", request.getParameter("SubMissionId"));
    td.setNameAndValue("ActivityId", request.getParameter("ActivityId"));    
    td.setNameAndValue("EdorType", request.getParameter("EdorType"));
    td.setNameAndValue("ContNo", request.getParameter("ContNo"));
    td.setNameAndValue("PolNo", request.getParameter("PolNo"));
    td.setNameAndValue("InsuredNo", request.getParameter("InsuredNo"));
    td.setNameAndValue("PassFlag", request.getParameter("PassFlag"));
    td.setNameAndValue("UWIdea", request.getParameter("UWIdea"));
    td.setNameAndValue("fmtransact", request.getParameter("fmtransact"));

		LLSecondLPUWManuUI tLLSecondLPUWManuUI = new LLSecondLPUWManuUI(gi, td);
		if (!tLLSecondLPUWManuUI.submitData())
		{
			flag = "Fail";
			content = "数据保存失败！原因是：" + tLLSecondLPUWManuUI.getError();
		}
		else
		{
			flag = "Succ";
			content = "数据保存成功！";
		}
		content = PubFun.changForHTML(content);
%>
<SCRIPT>
		parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</SCRIPT>
