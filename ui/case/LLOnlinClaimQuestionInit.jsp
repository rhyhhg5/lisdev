<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">
<%
	String Caseno = request.getParameter("CaseNo");
	String InsuredNo = request.getParameter("InsuredNo");
	String operatorName = request.getParameter("Handler");
	String CustomerName = new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");
	System.out.println(CustomerName);
	System.out.println("operatorName:" + operatorName);
%>

function initForm(){
  try{
	  	initInpBox();
    	initPastQuestionGrid();
    	SearchLowerQuestion();
  }
  catch(ex){
    alter("在GrpContInvaliedate.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initInpBox() {
	
	fm.Caseno.value = '<%=Caseno%>';
	fm.Customerno.value = '<%=InsuredNo%>';
	fm.Lowerpeople.value= '<%=operatorName%>';
	var Caseno = '<%=Caseno%>';
	
	var LowerSQL = "select (case state when '1' then '已下发' when '2' then '已回复' end),OPERATOR from LLCASEPROBLEM where 1=1 and caseno = '" + Caseno + "' order by makedate desc ,maketime desc with ur";
	var tLowerSQL = easyExecSql(LowerSQL);
	if(tLowerSQL) {
		fm.all('Lowerstate').value= tLowerSQL[0][0];      
	}
	var customerStr = "select customername from llcase where 1=1 and caseno = '" + Caseno + "' with ur";
	var tCustomerStr = easyExecSql(customerStr);
	if(tCustomerStr) {
		fm.all('Customername').value= tCustomerStr[0][0];      
	}
}

function initPastQuestionGrid(){
   var iArray = new Array();
   try
   {
	iArray[0]=new Array("序号","30px","10","0");
	iArray[1]=new Array("案件号","150px","0",0);	
    iArray[2]=new Array("案件状态","150px","0",0);
    iArray[3]=new Array("下发原因","150px","0",0);
    iArray[4]=new Array("下发状态","150px","0",0);
    iArray[5]=new Array("下发人员","150px","0",0);
    iArray[6]=new Array("下发日期","150px","0",0);
    iArray[7]=new Array("备注信息","150px","0",0);
    iArray[8]=new Array("回复信息","150px","0",0);
    
    PastQuestionGrid = new MulLineEnter("fm","PastQuestionGrid");
    PastQuestionGrid.mulLineCount = 1;
    PastQuestionGrid.displayTitle = 1;
    PastQuestionGrid.locked = 0;
    PastQuestionGrid.canChk =0;
    PastQuestionGrid.canSel =1;
    PastQuestionGrid.hiddenPlus=1;
    PastQuestionGrid.hiddenSubtraction=1;
    PastQuestionGrid.selBoxEventFuncName = "SearchLowerQuestion";
    PastQuestionGrid.loadMulLine(iArray);
      
  } catch(ex){
    alert(ex);
  }    
}

</script>