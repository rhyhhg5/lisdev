
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();



//***************************************************
//* 单击“查询”进行的操作
//***************************************************

function easyQueryClick()

{  
	
	var strSQL="SELECT P.CodeName"
  	+ " FROM ldcode P WHERE P.CodeType ='RestrictedDrug' and P.CodeName like '%" + fm.RestrictedDrugCodeName.value + "%'";

	
	
    var arrResult = easyQueryVer3(strSQL, 1, 0, 1);
	if(!arrResult)
	{  alert("没有符合条件的信息！"); 
		turnPage.queryModal(strSQL, EvaluateGrid); 
		return false;
	}
    else turnPage.queryModal(strSQL, EvaluateGrid);
  /*if( verifyInput() == false )   
  	return false;  
	var tSQL = "";

	tSQL = "select BlacklistCode,Name,Name1,Nationality,"
	+ " (select codename from ldcode where codetype = 'idtype' and code = IDNoType),"
	+ " IDNo, "
	+ " Name8,"  //风险等级
	+ " Name9,"  //发布机构
	+ " Name10,"  //类别
	+ " Remark"
    + " FROM "
    + " LCBlackList" 
    + " where  1=1  and type = '0' "
	+ getWherePart('BlacklistCode', 'BlacklistCode')
	+ getWherePart('Name', 'Name')
    + " ORDER BY BlacklistCode" ;
	
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, EvaluateGrid); 
		return false;
	}
    else turnPage.queryModal(tSQL, EvaluateGrid);*/
}

function gotoAdd()
{
  //下面增加相应的代码
  //showInfo=window.open("./RestrictedDrugMain.jsp?transact=insert","RestrictedDrug","middle",800,330);
   showInfo = OpenWindowNew("./RestrictedDrugMain.jsp?transact=insert","RestrictedDrugMain","middle",800,330);
}
function gotoUpdate()
{
  var checkFlag = 0;
  for (i=0; i<EvaluateGrid.mulLineCount; i++)
  {
    if (EvaluateGrid.getSelNo(i))
    {
      checkFlag = EvaluateGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	var	tBlacklistCode = EvaluateGrid.getRowColData(checkFlag - 1, 1);
  	showInfo=OpenWindowNew("./RestrictedDrugMain.jsp?transact=update&BlacklistCode="+tBlacklistCode,"RestrictedDrugMain","middle",800,330);
  }else{
  	alert("请选择需要修改的限制药品信息！");
  	return false;
  }
  return true;
}
//***************************************************
//* 点击“删除”进行的操作
//***************************************************
function deleteClick()
{
  var checkFlag = 0;
  for (i=0; i<EvaluateGrid.mulLineCount; i++)
  {
    if (EvaluateGrid.getSelNo(i))
    {
      checkFlag = EvaluateGrid.getSelNo();
      break;
    }
  }
  if(checkFlag){
  	 var tRestrictedDrug = EvaluateGrid.getRowColData(checkFlag - 1, 1);
  	 if(tRestrictedDrug== null || tRestrictedDrug == ""){
  	 	alert("删除时，获取限制或禁用药品失败！");
  		return false;
  	 }
  	 
  	 if (confirm("您确实想删除该记录吗?"))
	  {
	  var i = 0;
	  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.fmtransact.value = "DELETE";
	  fm.action = "./RestrictedDrugQuerySave.jsp?RestrictedDrug="+tRestrictedDrug;
	  fm.submit(); //提交
	  //fm.action="";
	  }
	  else
	  {
	    alert("您取消了删除操作！");
	  }
  }else{
  	alert("请选择需要删除的信息！");
  	return false;
  }
}
//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  easyQueryClick();
} 
