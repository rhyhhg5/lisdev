var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";
//提交，保存按钮对应操作
function submitForm()
{
  
  if ( fm.LogNo.value!="")
      {
      	alert("你不能执行改操作");
      	return false;
      	}
    if (confirm("您确实想保存该记录吗?"))
    {
      tSaveFlag = "1";
     
      
        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);
        
      
        fm.fmtransact.value="INSERT||MAIN";
        fm.submit(); //提交
        tSaveFlag ="0";

      
    }
    else
    {
      alert("您取消了修改操作！");
    }

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 
    if (fm.fmtransact.value=="DELETE||MAIN")
    {
    	fm.reset();
    	initCustomerGrid();
    }
 //   showDiv(operateButton,"true");
 //   showDiv(inputButton,"false");
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
 function addClick()
{

  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (confirm("您确实想修改该记录吗?"))
 	{
 		
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      //showSubmitFrame(mDebug);
      fm.fmtransact.value = "UPDATE||MAIN"
      fm.submit(); //提交
    }//end of else
 
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{

  window.open("./LLMainAskQuery.html");
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function deleteClick()
{
	  if ( fm.LogNo.value=="")
      {
      	alert("请先查询数据");
      	return false;
    }
    
	if (confirm("删除记录会删除所有咨询记录，您确实想删除该记录吗?"))
 	{	
      var i = 0;
      var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      showSubmitFrame(mDebug);
      fm.fmtransact.value = "DELETE||MAIN"
      fm.submit(); //提交
 }//end of else 
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function afterQuery(arr)
{
	if ( tSaveType=="EventQuery")
 	{
 	for ( i=0;i<arrReturn.length;i++)
 	{
 		len = SubReportGrid.mulLineCount;
 	
 		SubReportGrid.addOne();                          
 		
 		SubReportGrid.setRowColData(len,1,arrReturn[i][0]);
 		SubReportGrid.setRowColData(len,2,arrReturn[i][1]);
 		SubReportGrid.setRowColData(len,3,arrReturn[i][2]);
 		SubReportGrid.setRowColData(len,4,arrReturn[i][3]);
 				
 	}
 	  return;
 	}
	try{
	var logno = arr[0][0] ;
	
	fm.LogNo.value = logno ;
	var strSQL = "select * from LLMainAsk where logno='"+ logno +"'";
		var qryResult = easyQueryVer3(strSQL,1,1,1);
	if ( !qryResult )
	{
	  alert("没有符合条件的记录");
	  return ;
	}
	var de = decodeEasyQueryResult(qryResult);
	if ( de )
	{
		setMainAskInfo(de)
		strSQL ="select customerno,customername,'咨询',consultno from llconsult where logno='"+ logno +"'"
		    +" union select customerno,customername,'通知',noticeno from llnotice where logno='"+ logno +"'";
	
		turnPage.queryModal(strSQL, CustomerGrid);
	}else
		{
			alert("查询错误!");
		}
	}catch(ex)
        {
        	alert( ex.message );
        }
        
        
	

//	var de = decode
//	if ( arr)
//	{
//		var len = CustomerGrid.mulLineCount;
//		if (len<=0) len = 0;
//		for ( i=0;i<arr.length;i++)
//		{
//			row =len+i;
//			
//			CustomerGrid.addOne();			
//			CustomerGrid.setRowColData(row, 1,arr[i][0]);
//			CustomerGrid.setRowColData(row, 2,arr[i][1]);
//		}
//	}
}

/**************************************************/
function easyQuery()
{

	 strSQL ="select a.CustomerNo,a.CustomerName,a.CaseNo,a.AffixGetDate,a.RgtDate,b.codename from llcase a,ldcode b where 1=1 and b.codetype='llrgtstate' and b.code = a.rgtstate and a.RGTSTATE = '03'"
	         +getWherePart("CaseNo","CaseNo")
	         +getWherePart("CustomerNo","CustomerNo")
	         +getWherePart("CustomerName","CustomerName")
	         +getWherePart("RgtDate","RgtDate1",">=")
	         +getWherePart("RgtDate","RgtDate2","<=")
	         +getWherePart("HandleDate","HandleDate1",">=")
	         +getWherePart("HandleDate","HandleDate2","<=");
	     
	 strSQL +=" order by CaseNo";
	 turnPage.queryModal(strSQL, CheckGrid);
}
function calClaim()
{
	var selno = CheckGrid.getSelNo()-1;
	if (selno <0)
	{
	      alert("请选择要理算的个人受理");
	      return ;
	}
	var caseno = CheckGrid.getRowColData(selno,3);
		
		//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		var varSrc="&CaseNo=" + caseno ;
		var newWindow = window.open("./FrameMainCaseClaim.jsp?Interface=LLClaimInput.jsp"+varSrc,"理算",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

