//程序名称：PEdorUWManuHealth.js
//程序功能：保全人工核保体检资料录入
//创建日期：2006-02-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();


//提交数据后执行的操作
function afterSubmit(flag, content)
{
	showInfo.close();
	window.focus();
	if (flag == "Fail") 
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

//查询被保人信息
function queryInsured()
{
  var sql = "select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday, " +
            "       a.IDType, a.IDNo, '', c.PrtSeq, " +
            "       case when c.PEState is null then '体检通知书未录入' " +
            "            when c.PEState = '01' then '体检通知书已录入' " +
            "            when c.PEState = '02' then '体检通知书已打印' end " +
            "from LCInsured a left join LPPENotice c " +
            "     on a.InsuredNo = c.CustomerNo " +
            "     and c.EdorNo = '" + fm.EdorNo.value + "', " + 
            "     LCCont b " +
            "where a.ContNo = b.ContNo " +
            "and a.AppntNo = '" + fm.AppntNo.value + "' " +
            "and b.AppFlag = '1'";
  turnPage.pageLineNum = 100;
  turnPage.queryModal(sql, LCInsuredGrid);
}


function showInsuredInfo()
{
  var contNo = "00000000202";
  var insuredNo = "000000002";
 // parent.fraInterface.window.location = "./ContInsuredInput.jsp?cInsuredNo=" + insuredNo + "&ContNo=" + contNo + "&ContType=2";
  //var pageUrl = "../sys/PolDetailQueryMain.jsp?ContNo=" + mContNo + 
  //              "&IsCancelPolFlag=0&ContType=2";
  //var pageUrl =  "../sys/ContInsuredInput.jsp?cInsuredNo=" + insuredNo + "&ContNo=" + contNo + "&ContType=2";
  //window.open(pageUrl);
}

function onClickedInsured()
{
  var selNo = LCInsuredGrid.getSelNo() - 1;
  fm.ContNo.value = LCInsuredGrid.getRowColData(selNo, 1);
  fm.InsuredNo.value = LCInsuredGrid.getRowColData(selNo, 2);
  fm.InsuredName.value = LCInsuredGrid.getRowColData(selNo, 3);
  fm.PrtSeq.value = LCInsuredGrid.getRowColData(selNo, 9);
  fm.PEState.value = LCInsuredGrid.getRowColData(selNo, 10);
  var sql = "select PEAddress, PEDate, Remark from LPPENotice " +
            "where EdorNo = '" + fm.EdorNo.value + "' " +
            "and CustomerNo = '" + fm.InsuredNo.value + "'";
  var result = easyExecSql(sql);
  if (result != null)
  {
    fm.Hospital.value = result[0][0];
    fm.EDate.value = result[0][1]
    fm.Note.value = result[0][2];
  }
  queryHealth();
}

//查询体检项目
function queryHealth()
{
  fm.EDate.value = getCurrentDate();
  var sql = "select PEItemCode, PEItemName, FreePE from LPPENoticeItem " +
            "where EdorNo = '" + fm.EdorNo.value + "' " +
            "and PrtSeq = '" + fm.PrtSeq.value + "' ";
  turnPage2.pageDivName = "divPage2";
  turnPage2.pageLineNum = 100 ; 
  turnPage2.queryModal(sql, HealthGrid);
}

//套餐反显
function afterCodeSelect(CodeName,field)
{
  if (CodeName == "testgrpname")
  {
    var strSql = " select distinct a.testitemcode, b.testgrpname, "
                +" (select distinct c.othersign from ldtestgrpmgt c where c.testgrpcode = a.testitemcode) "
                +" from   ldtestgrpmgt a, ldtestgrpmgt b"
                +" where  a.testgrptype = '4'"
                +" and    a.testgrpcode = '"+field.value+"'"
    				  	+" and    b.testgrptype = '3' "
                +" and    a.testitemcode = b.testgrpcode " 
                +" order by a.testitemcode";
    turnPage2.pageLineNum = 100 ; 
    turnPage2.queryModal(strSql, HealthGrid);
  }
}


function submitForm()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action= "./PEdorUWManuHealthSave.jsp";
  fm.submit();
}
