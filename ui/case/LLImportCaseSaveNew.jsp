<%
//程序名称：LLImportCaseSave.jsp
//程序功能：导入提交页面
//创建日期：2005-08-21 09:25:18
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%
  String FlagStr = "Fail";
  String Content = "";
  boolean tSecurityFlag =false;

String FileName = "";
int count = 0;




//得到批次导入的版本号 add by Houyd 20140122
String tVersionType = request.getParameter("VersionType");
//归档之前，记录解析配置文件xml的路径 add by Houyd 20140122
String XmlPath = "";

System.out.println("VersionType: "+tVersionType);

//得到excel文件的保存路径
String ImportPath = request.getParameter("ImportPath");
System.out.println("ImportPath: "+ImportPath);
String path = application.getRealPath("").replace('\\','/')+'/';  //application.getRealPath("")取到的路径是用"\"分隔的
System.out.println("path: "+path);

DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
fu.setSizeMax(10000000);

// 设置最多只允许在内存中存储的数据,单位:字节
fu.setSizeThreshold(4096);

// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(path+"temp");
//开始读取上传信息
List fileItems = null;

try{
  fileItems = fu.parseRequest(request);

}
catch(Exception ex){
  ex.printStackTrace();
}

// 依次处理每个上传的文件
Iterator iter = fileItems.iterator();

while (iter.hasNext()) {
  FileItem item = (FileItem) iter.next();
  //忽略其他不是文件域的所有表单信息
  if (!item.isFormField()) {
    String name = item.getName();
    long size = item.getSize();
    System.out.println("+++++name++++++"+name+",+++size+++"+size);
    if((name==null||name.equals("")) && size==0)
      continue;
    ImportPath= path + ImportPath;
    
    
    XmlPath = ImportPath;
    //对上传的Excel文件归档，makeby Houyd 2014-01-22
    if(!PubDocument.save(ImportPath)){
    	FlagStr = "Fail";
        Content = "文件归档失败";
        break;
    }
    ImportPath = PubDocument.getSavePath(ImportPath);//实际归档的Excel文件路径
    
    FileName = name.substring(name.lastIndexOf("\\") + 1);
    System.out.println("Importpath+FileName:"+ImportPath + FileName);
    File file = new File(ImportPath + FileName);
    if (file.exists()) {
        FlagStr = "Fail";
        Content = "该文件已经导入过！";
        break;
    }

    //保存上传的文件到指定的目录
    try {
      item.write(new File(ImportPath + FileName));
      count = 1;
    }
    catch(Exception e) {
      System.out.println("upload file error ...");
    }
  }
}
  System.out.println("upload successfully");

  //输出参数
  String mRgtNo = request.getParameter("RgtNo");
  System.out.println("RgtNo:"+mRgtNo);
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  TransferData tTransferData = new TransferData();
  boolean res = true;
  
  LLImportCaseInfoNew tLLImportCaseInfoNew = new LLImportCaseInfoNew();

  if (count >0)
  {
    // 准备传输数据 VData
    VData tVData = new VData();
    FlagStr="";
    tTransferData.setNameAndValue("FileName", FileName);
    tTransferData.setNameAndValue("FilePath", ImportPath);
    tTransferData.setNameAndValue("RgtNo",mRgtNo);
    tTransferData.setNameAndValue("VersionType",tVersionType);
    tTransferData.setNameAndValue("XmlPath",XmlPath);

    tVData.add(tTransferData);
    tVData.add(tG);
    try{
      if(!tLLImportCaseInfoNew.submitData(tVData, "")){    //提取案件信息
        Content = "保存失败，原因是:"+tLLImportCaseInfoNew.mErrors.getFirstError() ;
        FlagStr = "Fail";
        res= false;
      }
    }
    catch(Exception ex){
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
    System.out.println("submitData Finished");
  }
  else{
    Content += "上载文件失败! ";
    FlagStr = "Fail";
  }

  // #3684 社保阻断提示
 	String tSocialSql="Select CHECKGRPCONT(rgtobjno) from llregister where rgtno ='"+mRgtNo+"' with ur";
    ExeSQL tExeSQL = new ExeSQL();
   
	String  aSocialValue= tExeSQL.getOneValue(tSocialSql);
	System.out.println("a:"+Content);
	if("".equals(Content)||Content==null||Content=="null"){
		if("Y".equals(aSocialValue)){
			TransferData aTransferData = new TransferData();
			aTransferData = (TransferData)tLLImportCaseInfoNew.getResult().getObjectByObjectName("TransferData", 0);
			System.out.println("c:"+aTransferData);
			if(aTransferData!=null){
				String eErrorTitle =(String) aTransferData.getValueByName("ErrorTitle");
				System.out.println("c:"+eErrorTitle);
				int aErrorTitle=0;
				if(eErrorTitle==null || eErrorTitle=="null" ||"".equals(eErrorTitle)){
					eErrorTitle="0";
					aErrorTitle=Integer.parseInt(eErrorTitle);
				}else{
					aErrorTitle=Integer.parseInt(eErrorTitle);
				}
				System.out.println("a:"+aErrorTitle);
				if(aErrorTitle >0){
						 tSecurityFlag = true;
					 	Content ="该批次错误数据有："+eErrorTitle+"个，请核实错误清单。";
				 	 }
			}
		}
	}

  if (FlagStr.equals("Fail")){
    res=false;
 	}

	 if(res && tSecurityFlag){
		 FlagStr = "Succ";
	 }
	 else if (res){
	   Content = " 提交成功! ";
	   FlagStr = "Succ";
	 }
	 else{
	   Content = " 保存失败，原因是:" + Content;
	   FlagStr = "Fail";
	  }
  //添加各种预处理

  %>
  <html>
  <script language="javascript">
  parent.fraInterface.UploadiFrame.afterSubmit("<%=FlagStr%>","<%=Content%>");
  </script>
  </html>


