<%
//Name：RegisterInit.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author  ：LiuYansong
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
String LoadFlag= request.getParameter("LoadFlag");
LoadFlag= LoadFlag==null?"":LoadFlag;
String RgtNo= request.getParameter("RgtNo") ;
String CaseNo= request.getParameter("CaseNo");
String GrpContNo = request.getParameter("GrpContNo");
String LoadC="";
if(request.getParameter("LoadC")!=null)
{
LoadC = request.getParameter("LoadC");
}
%>

<script language="JavaScript">
  var loadFlag="<%=LoadFlag%>";
  var RgtNo = '<%=RgtNo%>';
  RgtNo= (RgtNo=='null'?"":RgtNo);
  var CaseNo = '<%=CaseNo %>';
  CaseNo= (CaseNo=='null'?"":CaseNo);

  function initInpBox( )
  {
    try {
      fm.reset();
      fm.CustomerNo.value="";
      fm.LoadFlag.value = loadFlag;
      fm.RgtNo.value = RgtNo;
      fm.CaseNo.value = CaseNo;
      fm.GrpContNo.value = "<%=GrpContNo%>";
      fm.LoadC.value="<%=LoadC%>";
      fm.RiskCode.value = "<%=request.getParameter("RiskCode")%>"
      if (fm.LoadC.value=='2')
      {
        aa.style.display='none';
        divnormalquesbtn.style.display='none';
        //    	CaseChange();
      }

    }
    catch(ex) {
      alert(ex.message);
    }
  }

  function initSelBox()
  {
    try{
    }
    catch(ex)
    {
      alert(ex.message);
    }
  }

  function initForm()
  {
    try
    {
      titleGrp.style.display='none';
      inputGrp.style.display='none';

      if(loadFlag=="0")
      {
        titleGrp.style.display='';
        inputGrp.style.display='';
        divRegisterInfo.style.display='none';
        fm.Relation.value='05';
        fm.RgtType='9';
      }
      initInpBox();
      initFeeGrid();
      initGrpCaseGrid();
      initEventGrid();
      checkRgtFlag();
      queryGrpCaseGrid();
      initQuery();
      initDate();
      <%GlobalInput mG = new GlobalInput();
      mG=(GlobalInput)session.getValue("GI");
      %>
      fm.Handler.value = "<%=mG.Operator%>";
      fm.ModifyDate.value = getCurrentDate();
      //QueryApplyReason();
    }
    catch(re)
    {
      alert("RegisterInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
    }
  }

  //事件列表的初始化
  function initEventGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","0px","10","0");
      iArray[1]=new Array("事件号","100px","100","0");
      iArray[2]=new Array("发生日期","80px","100","1");
      iArray[2][9]="发生日期|notnull&date";

      iArray[3]=new Array("发生地点","150px","60","1");
      iArray[4] = new Array("事故类型","80px","0","3");
      iArray[5] = new Array("事故主题","200px","0","3");
      iArray[6] = new Array("医院名称","200px","0","3");
      iArray[7]=new Array("入院日期","80px","100","1");
      iArray[7][9]="入院日期|date";

      iArray[8]=new Array("出院日期","80px","100","1");
      iArray[8][9]="出院日期|date";

      iArray[9] = new Array("事件信息","200px","1000","1");

      iArray[10] = new Array("事件类型","60px","10","2")
      iArray[10][10]="AccType";
      iArray[10][11]="0|^1|疾病|^2|意外"
      iArray[10][12]="10|11"
      iArray[10][13]="1|0"

      iArray[11] = new Array("事件类型","60px","10","3")

      EventGrid = new MulLineEnter("fm","EventGrid");
      EventGrid.mulLineCount = 0;
      EventGrid.displayTitle = 1;
      EventGrid.locked = 0;
      EventGrid.canChk =1	;
      EventGrid.hiddenPlus=0;
      EventGrid.hiddenSubtraction=1;
      EventGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alter(ex);
    }
  }

  // 账单信息列表的初始化
  function initFeeGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","0px","10","1");

      iArray[1]=new Array("总额","80px","100","1");

      iArray[2]=new Array("全自费部分","80px","100","3");

      iArray[3]=new Array("挂钩自费部分","80px","60","3");

      iArray[4] = new Array("纳入统筹额","80px","100","1");

      iArray[5] = new Array("统筹支付额","80px","100","1");

      iArray[6] = new Array("大病支付额","80px","100","1");

      iArray[7]=new Array("现金支付额","80px","100","1");

      FeeGrid = new MulLineEnter("fm","FeeGrid");
      FeeGrid.mulLineCount = 1;
      FeeGrid.displayTitle = 1;
      FeeGrid.locked = 0;
      FeeGrid.canChk = 0;
      FeeGrid.hiddenPlus=1;
      FeeGrid.hiddenSubtraction=1;
      FeeGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alter(ex);
    }
  }

  // 案件清单
function initGrpCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;
    
    iArray[1]=new Array("理赔号", "90px", "0", "0");
    iArray[2]=new Array("社保号", "100px", "0", "0");
    iArray[3]=new Array("客户姓名", "100px", "0", "0");
    iArray[4]=new Array("客户号", "100px", "0", "0");
    iArray[5]=new Array("申请日期", "100px", "0", "0");
    iArray[6]=new Array("申请金额", "100px", "0", "0");
    GrpCaseGrid = new MulLineEnter("fm","GrpCaseGrid");
    GrpCaseGrid.mulLineCount =5;
    GrpCaseGrid.displayTitle = 1;
    GrpCaseGrid.locked = 1;
    GrpCaseGrid.canSel =1;
    GrpCaseGrid.hiddenPlus=1;  
    GrpCaseGrid.hiddenSubtraction=1; 
    GrpCaseGrid.loadMulLine(iArray);
    GrpCaseGrid.selBoxEventFuncName = "ShowCase";
  }
  catch(ex)
  {
    alter(ex);
  }
}

  function initQuery()
  {

    var tRgtNo = fm.RgtNo .value;
    var tCaseNo = fm.CaseNo.value;
    ClientafterQuery(tRgtNo,tCaseNo);

  }

  function checkRgtFlag()
  {
    strSQL=" select rgtclass, apppeoples from LLRegister where rgtno = '" + fm.all('RgtNo').value + "'";
    arrResult=easyExecSql(strSQL);
    fm.all('rgtflag').value = '0';
    if(arrResult != null)
    {
      fm.all('AppNum').value = arrResult[0][1];
      var RgtClass;
      try{RgtClass = arrResult[0][0]} catch(ex) {alert(ex.message + "RgtClass")}
      strSQL1 = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
      arrResult1 = easyExecSql(strSQL1);
      fm.all('ClientNo').value=arrResult1[0][0]/1+1;
      if (RgtClass == '1')
      {
        titleGrp.style.display='';
        inputGrp.style.display='';
        divRgtFinish.style.display='';
        titleAppNum.style.display='';
        inputAppNum.style.display='';
        titleClientNo.style.display='';
        inputClientNo.style.display='';
        fm.all('rgtflag').value = "1";
        //RgtFinish.style.display='';
      }
    }
  }

</script>