<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：
//程序功能：
//创建日期：2003-06-16
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {

  }
  catch(ex)
  {
    alert("在LLclaimpastQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {  	
  }
  catch(ex)
  {
    alert("在LLclaimpastQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox(); 
    initSelBox(); 
	initCaseInfo();
	//initCardListInfo();
  }
  catch(re)
  {
    alert("LLclaimpastQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 个人客户信息列表的初始化
function initCaseInfo()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="团体批次号";         		//列名
		iArray[1][1]="80px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="理赔号";          		//列名
		iArray[2][1]="70px";            		//列宽
		iArray[2][2]=85;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="客户姓名";        		  //列名
		iArray[3][1]="70px";            		//列宽
		iArray[3][2]=85;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="客户号";          		//列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            		 	  //列最大值
		iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="受理日期";         		  //列名
		iArray[5][1]="80px";            		//列宽
		iArray[5][2]=100;            			  //列最大值
		iArray[5][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="审批日期";          		  //列名
		iArray[6][1]="80px";            		//列宽
		iArray[6][2]=60;            			  //列最大值
		iArray[6][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="结案日期";          		  //列名
		iArray[7][1]="80px";            		//列宽
		iArray[7][2]=60;            			  //列最大值
		iArray[7][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		CaseInfo = new MulLineEnter("fm" , "CaseInfo");
		// 这些属性必须在loadMulLine前
		CaseInfo.mulLineCount = 10;
		CaseInfo.displayTitle = 1;
		CaseInfo.locked = 1;
		CaseInfo.canSel = 1;
  	    CaseInfo.hiddenPlus = 1;
 	    CaseInfo.hiddenSubtraction = 1;
		CaseInfo.selBoxEventFuncName = "SelectCaseInfo";
		CaseInfo.loadMulLine(iArray);		
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>