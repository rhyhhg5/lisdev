var showInfo;
var mDebug="0";
var Action;
var tRowNo=0;
var mSaveType="";
var turnPage = new turnPageClass(); 

function CaseChange()
{
	initTitle();
   if(fm.CaseNo.value!='')   
	   
	if (fm.RgtState.value == null ||
		fm.RgtState.value =="")
	{
		
		queryClear();
		
	}
	else
	{
		QueryPolicyDetails();
	}
	
}

function queryClear()
{
	initOperationOnoGrid();
	initOperationTwoGrid();
	initOperationRisk1Grid();
	initOperationRisk2Grid();
}

//页面显示查询
function QueryPolicyDetails(flag)
{
	var str2SQL = "select GiveTypeDesc,GiveType,ClmNo from LLClaim where caseno='"+ fm.CaseNo.value +"'";
	var arr2 = easyExecSql(str2SQL);
	//alert(arr2[0][2]);
	if(arr2)
	{
		fm.GiveTypeDesc1.value = arr2[0][0];
		fm.GiveType1.value = arr2[0][1];
		fm.GiveTypeDesc2.value = arr2[0][0];
		fm.GiveType2.value = arr2[0][1];
		fm.ClmNo.value = arr2[0][2];
	}
	var str1SQL = "select contno,RiskCode,GiveTypeDesc,GiveType,GiveReasonDesc,GiveReason,PolNo,ClmNo,CaseRelaNo,GetDutyKind " 
			+" from LLClaimPolicy where caseno='"+ fm.CaseNo.value +"'";
	var arr1 = easyExecSql(str1SQL);
	//显示险种查询   
	if(arr1) displayMultiline( arr1, OperationRisk1Grid);
	if ( arr1==null || arr1.length<=0)
	{ 
		initOperationRisk1Grid();	
	}
	//显示险种修改 && (flag='init') 
	if(arr1) displayMultiline( arr1, OperationRisk2Grid);
	if ( arr1==null || arr1.length<=0)
	{ 
		initOperationRisk2Grid();	
	}
	
	var strSQL = "select contno,RiskCode,b.getdutyname,a.GetDutyKind,a.GetDutyCode,a.GiveTypeDesc,"
		   +"a.GiveType,a.GiveReasonDesc,a.GiveReason,a.PolNo,a.ClmNo,a.CaseRelaNo,a.RealPay "
		   +" from LLClaimdetail a, LMDutyGetClm b where  a.caseno='"+ fm.CaseNo.value +"'"
		   +" and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind"
		   
		var arr = easyExecSql(strSQL );
		//显示保单责任查询
		if (arr) displayMultiline( arr, OperationOneGrid);
		if ( arr==null || arr.length<=0)
		{ 
			initOperationOnoGrid();
		}
		
		//显示保单责任修改 
		if (arr) displayMultiline( arr, OperationTwoGrid);

		if ( rr==null || arr.length<=0)
		{ 
			initOperationTwoGrid();	
		}
}

//提交,保存按钮对应的操作
function submitForm() 
{
	var tRgtState = fm.RgtState.value;
	//alert(tRgtState);
	//判断案件是否为理算状态，理算状态："04"才可以修改
	if(tRgtState != "04")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "只有理算状态下的案件才可以修改！" ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		return;
	}
	
	var tContNo = OperationTwoGrid.getRowColData(0,1);
	if(tContNo == "")
	{
		 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "数据为空，请先查询数据！" ;
		 showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		 return;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  	fm.Opt.value="UPDATE";
  	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		QueryPolicyDetails();
		//执行下一步操作
	}
	initTitle();
}










