<%
//程序名称：PayAffirmInit.jsp
//程序功能：给付确认的初始化
//创建日期：
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%

%>

<script language="JavaScript">
  function initInpBox()
  {
    try
    {
      fm.all('CaseNo').value = "<%=request.getParameter("CaseNo")%>";
      <%
        GlobalInput mG = new GlobalInput();
        mG=(GlobalInput)session.getValue("GI");
      %>
      fm.all('Surveyer').value = "<%=mG.Operator%>";
      fm.all('InsuredNo').value = "<%=request.getParameter("InsuredNo")%>";
      <% String CustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");
      %>
       fm.all('CustomerName').value = "<%=CustomerName%>";
       easyQuery();
      //fm.all('Content').value = '';
    }
      catch(ex)
      {
        alter("在LLRgtSurveyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
      }
  }

function initForm()
{
  try
  {
    initInpBox();
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>