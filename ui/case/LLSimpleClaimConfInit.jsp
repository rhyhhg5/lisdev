<%
//Name:LLSimpleClaimConfInit.jsp
//function：批量审批
//author:ZJL 
//Date:2013-07-02
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">


function initForm()
{
  try
  {
    initCheckGrid();
  }  
  catch(ex)
  {
    alter("LLSimpleClaimConfInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=3;

    iArray[1]=new Array("批次号","120px","0","0");   
    iArray[2]=new Array("理赔号","120px","0","0");
    iArray[3]=new Array("客户姓名","80px","0","0");
    iArray[4]=new Array("客户号","80px","0","0");
    iArray[5]=new Array("受理日期","80px","0","0");
    iArray[6]=new Array("当前录入人","80px","0","0");;
    iArray[7]=new Array("案件状态","100px","0","0");
    iArray[8]=new Array("案件状态代码","80px","0","3");

    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =8;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canChk =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CheckGrid. selBoxEventFuncName = "onSelSelected";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}


 </script>