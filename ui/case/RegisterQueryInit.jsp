<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
 
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    initInpBox();
    initCheckGrid();
    initEventGrid();
    easyQuery();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initEventGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="事件号";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;

    iArray[2]=new Array();
    iArray[2][0]="发生日期";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][9]="发生日期|notnull&date";
    
    iArray[3]=new Array();
    iArray[3][0]="发生地点";
    iArray[3][1]="150px";
    iArray[3][2]=60;
    iArray[3][3]=0;
  
   iArray[4] = new Array("事故类型","80px","0","3");
   iArray[5] = new Array("事故主题","200px","0","3");
   iArray[6] = new Array("医院名称","200px","0","3");
   
   iArray[7]=new Array();
   iArray[7][0]="入院日期";
   iArray[7][1]="80px";
   iArray[7][2]=10;
   iArray[7][3]=0;
   iArray[7][9]="入院日期|date";
   
   iArray[8]=new Array();
   iArray[8][0]="出院日期";
   iArray[8][1]="80px";
   iArray[8][2]=10;
   iArray[8][3]=0;
   iArray[8][9]="出院日期|date";
   
   iArray[9] = new Array("事件信息","200px","1000","0");
   iArray[10] = new Array("事件类型","60px","10","0")

   iArray[11] = new Array("事件类型","60px","10","3")
   
    EventGrid = new MulLineEnter("fm","EventGrid");
    EventGrid.mulLineCount = 0;
    EventGrid.displayTitle = 1;
    EventGrid.locked = 0;
    EventGrid.canChk =1	;
    EventGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    EventGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    EventGrid.loadMulLine(iArray);
    //SubReportGrid.selBoxEventFuncName = "ShowRela";
  }
  catch(ex)
  {
    alter(ex);
  }
}


function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=1;
      
    iArray[1]=new Array("团体批次号","120px","0","0");
    iArray[2]=new Array("理赔号","125px","0","0");
    iArray[3]=new Array("姓名","60px","0","0");
    iArray[4]=new Array("案件状态代码","0px","0","3");
    iArray[5]=new Array("身份号","0px","0","3");
    iArray[6]=new Array("受理日期","80px","0","0");
    iArray[7]=new Array("处理人","0px","0","3");;
    iArray[8]=new Array("案件状态","60px","0","0");
    iArray[9]=new Array("案件结果","60px","0","0");
    iArray[10]=new Array("处理时效(天)","80px","0","3");
    iArray[11]=new Array("IDNo","0px","0","3");
		iArray[13]=new Array("操作员姓名","70px","20","0");
    iArray[14]=new Array("操作员代码","70px","20","0");
    iArray[12]=new Array("管理机构","60px","20","0");
    iArray[15]=new Array("团个标志","0px","20","3");

    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =5;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canSel =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
   
    CheckGrid. selBoxEventFuncName = "DealApp";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

 </script>