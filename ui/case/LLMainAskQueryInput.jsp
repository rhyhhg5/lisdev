<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator	=tG.Operator;
  	String Comcode	=tG.ManageCom;
  
 				String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLMainAskQueryInput.js"></SCRIPT>

   <%@include file="LLMainAskQueryInit.jsp"%>
   <script language="javascript">
   function initDate(){
   		fm.RgtDateS.value="<%=afterdate%>";
   		fm.RgtDateE.value="<%=CurrentDate%>";
   		var usercode="<%=Operator%>";
   		var comcode="<%=Comcode%>";
//   		fm.Operator.value=usercode;
//   		fm.OrganCode.value=comcode;
//   		
//   		
//   		var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
//   		var arrResult = easyExecSql(strSQL);
//			if(arrResult != null)
//			{
//			         fm.optname.value= arrResult[0][0];
// 			}
   }
   </script>
 </head>
<body  onload="initDate();initForm();">
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">

	 <table>
        <TR>
	         <TD class= titleImg>
	         请录入查询条件
	         </TD>
       </TR>
   </table>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
       	<TR  class= common>	
					<TD  class= title>客户姓名</TD>                                                             
       		<TD  class= input><Input class=common8 name=CustomerName onkeydown="FindCustomer()"></TD> 
       		<TD  class= title>客户号</TD>                                                               
       		<TD  class= input><Input class=common name=CustomerNo onkeydown="FindCustomer()"></TD>    
					<TD  class= title>身份证号</TD>
					<TD  class= input><Input class=common name=IDNo onkeydown="FindCustomer()"></TD>
				</TR>
       	<TR class = common>
       		<TD  class= title style="display:'none'">咨询通知号</TD>
					<TD  class= input style="display:'none'"> <Input class=common8 name=ConsultNo  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>保单号</TD>
					<TD  class= input><Input class=common8 name=ContNo onkeydown="QueryOnKeyDown()"></TD>
       		<TD  class= title>类型</TD>
	        <TD  class= input8><Input  CodeData="0|2^0|咨询^1|通知" onClick="return showCodeListEx('asktype',[this,AskTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('asktype',[this,AskTypeName],[0,1]);" class=codeno name="AskType" onkeydown="QueryOnKeyDown()"><Input class= codename name=AskTypeName onkeydown="QueryOnKeyDown()"></TD>
	        <TD  class= title8></TD>
					<TD  class= input8></TD>
       	</TR>
				<TR  class= common8 style="display:'none'">
					<TD  class= title>管理机构</TD>
					<TD  class= input><Input class= "code8"  name=OrganCode onkeydown="QueryOnKeyDown();" onclick="getstr();return showCodeList('stati',[this], [0],null,str,'1');" onkeyup="getstr();return showCodeListKey('stati', [this], [0],null,str,'1');" >
					<TD  class= title>操作员姓名</TD>
					<TD  class= input><Input class= "code8"  name=optname onkeydown="QueryOnKeyDown();" onclick="return showCodeList('optname',[this, Operator], [0, 1],null,fm.optname.value,'username','1');" onkeyup="return showCodeListKey('optname', [this, Operator], [0, 1],null,Str,'1');" >
					<TD  class= title>操作员代码</TD>
					<TD  class= input><Input class="readonly" readonly name=Operator></TD>
				</TR>
				<TR>	
					<TD  class= title8>受理日期从</TD>
					<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>到</TD>
					<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8></TD>
					<TD  class= input8></TD>
				</TR>
       </table>
     </Div>
     <br>
     <hr>
     <Table>
						<TR class=common>
							<TD class=common>
								<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondUWInfo);">
							</TD>
							<TD class= titleImg>
								基本信息
							</TD>
						</TR>
					</Table>
     <Table class=common style="display:'none'">
					</tr>
					<TD  class= title8>起始日期</TD>
					<TD  class= input8> <input readOnly class="coolDatePicker"  dateFormat="short"  name=CDateS onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>终止日期</TD>
					<TD  class= input8 > <input readOnly class="coolDatePicker"  dateFormat="short"  name=CDateE onkeydown="QueryOnKeyDown()"></TD>
				</TR>
			</Table>
    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1 align=center>
            <span id="spanCheckGrid" ></span>
          </TD>
        </TR>
      </table>       
    </Div>
  </br>
   <Div  align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>    
	<hr>
	<div id="DivCusInfo" style="display:''">
							<table>
								<TR>
									<TD>
										<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
									</TD>
									<TD class= titleImg>
										详细信息
									</TD>
								</TR>
							</table>
	</DIV>
	<Div  id= "divLLLLMainAskInput1" style= "display: ''">
	<Div>
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>客户姓名</TD><TD  class= input8><input readOnly class= common name="LogName" ></TD>
						<TD  class= title8></TD><TD  class= input8><input readOnly style="display:'none'" class= common name="LogerNo"  ></TD>
						<TD  class= title8></TD><TD  class= input8><input readOnly style="display:'none'" class= common name="IDNo_R"  ></TD>
					</TR>
					<TR  class= common8 style="display:'none'">
							<TD  class= title8>电话号码</TD><TD  class= input8><input readOnly class= common name="Phone"  ></TD>
							<TD  class= title8>手机号码</TD><TD  class= input8><input readOnly class= common name="Mobile" ></TD>
							<TD  class= title8>电子邮箱</TD><TD  class= input8><input readOnly class= common name="Email" ></TD>
					</TR>
					<TR  class= common8 style=" display:'none'">
								<TD  class= title8>单位名称</TD><TD  class= input8><input readOnly class= common name="LogComp" ></TD>
								<TD  class= title8>通讯地址</TD><TD  class= input8><input readOnly class= common name="AskAddress"  ></TD>
								<TD  class= title8>回复方式</TD><TD  class= input8><input readOnly class="codeno" name="AnswerMode" ><input readOnly class= codename name=AnswerModeName ></TD>
					</TR>
					<TR  class= common8>
	 							<TD  class= title8>登记方式</TD><TD  class= input8><input readOnly class=codeno name="AskMode"  ><input readOnly class= codename name="AskModeName" ></TD>
								<TD  class= title8>客户现状</TD><TD  class= input8><input readOnly class="codeno" name="CustStatus" ><input readOnly class= codename name="CustStatusName"></TD>
								<TD  class= title8>出险类别</TD><TD  class= input8><input readOnly class= codeno  name="AccType" ><input readOnly class= codename name="AccTypeName"></TD>
					</TR>
			</table>
	</DIV>
						
	<div id="ConsultInfo1" style="display:''">
		<table  class= common>
				<TR  class= common>
						<TD  class= title colspan="6">信息内容</TD>
				   	<TD  class= title colspan="6">回复内容</TD>
				</TR>
				<TR  class= common>
					<TD  class= input colspan="6">
						<textarea readOnly name="CContent" cols="50%" rows="3" witdh=25% class="common"></textarea>
					</TD>
					<TD  class= input colspan="6">
						<textarea readOnly name="Answer" cols="54%" rows="3" witdh=100% class="common"></textarea>
					</TD>
				</TR>
				<TR  class= common8>
					<Input type=hidden class= common name="ReplyState"></TD>
				</TR>
	  </table>
	</div>

			<Div  id= "" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanSubReportGrid">
							</span>
						</TD>
					</TR>
				</table>
			</div>
		</Div>
		<hr>
		<input type=button class=cssButton value="通知咨询查询" OnClick="ShowInfoPage();">
		
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="ComCode"  value="<%=Comcode%>">
     <Input type="hidden" class= common name="Operator" value="<%=Operator%>">
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
