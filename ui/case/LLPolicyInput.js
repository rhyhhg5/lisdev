//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作

function RiskCase()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	if(fm.DealWith1.value=='5')
	{
		fm.RiskRate.value='Y';
	}
	fm.target = "RiskCase";
	fm.action = "LLRiskCaseReport.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function DutyCase()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "DutyCase";
	fm.action = "LLDutyCaseReport.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function CasePolicy()
{
	if (verifyInput() == false)
    return false;
    if(fm.StartDate.value==null||fm.StartDate.value=='')
    {
    	if(fm.EndDate.value!='')//结案起始日期没有而截止日期有时候
    	{
    		alert("请补全结案起始日期！");
    		return;
    	}
    }
    else
    {
    	if(fm.EndDate.value==null||fm.EndDate.value=='')//结案起始日期有而截止日期没有时候
    	{
    		alert("请补全结案截止日期！");
    		return;
    	}
    }
    
    if(fm.TZStartDate.value==null||fm.TZStartDate.value=='')
    {
    	if(fm.TZEndDate.value!='')//结案起始日期没有而截止日期有时候
    	{
    		alert("请补全通知起始日期！");
    		return;
    	}
    }
    else
    {
    	if(fm.TZEndDate.value==null||fm.TZEndDate.value=='')//结案起始日期有而截止日期没有时候
    	{
    		alert("请补全通知截止日期！");
    		return;
    	}
    }
    
    if(fm.JFStartDate.value==null||fm.JFStartDate.value=='')
    {
    	if(fm.JFEndDate.value!='')//给付起始日期没有而给付截止日有时候
    	{
    		alert("请补全给付起始日期！");
    		return;
    	}
    }
    else
    {
    	if(fm.JFEndDate.value==null||fm.JFEndDate.value=='')//给付起始日期有而给付截止日没有时候
    	{
    		alert("请补全给付截止日期！");
    		return;
    	}
    }
    if((fm.StartDate.value==null||fm.StartDate.value=='')&&(fm.JFStartDate.value==null||fm.JFStartDate.value=='')&&(fm.TZStartDate.value==null||fm.TZStartDate.value==''))//当结案和给付起止日期都为空时
    {
    	alert("结案、给付和通知起止日期至少输入一项！");
    	return;
    }
  var i = 0;

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	fm.target = "BranchResult";
	fm.action = "LLCasePolicySave.jsp"
	fm.op.value = "PRINT||BRANCH";
	fm.submit();
	showInfo.close();
}

function CasePolicyCSV()
{
	if (verifyInput() == false)
    return false;
    if(fm.StartDate.value==null||fm.StartDate.value=='')
    {
    	if(fm.EndDate.value!='')//结案起始日期没有而截止日期有时候
    	{
    		alert("请补全结案起始日期！");
    		return;
    	}
    }
    else
    {
    	if(fm.EndDate.value==null||fm.EndDate.value=='')//结案起始日期有而截止日期没有时候
    	{
    		alert("请补全结案截止日期！");
    		return;
    	}
    }
    
    if(fm.TZStartDate.value==null||fm.TZStartDate.value=='')
    {
    	if(fm.TZEndDate.value!='')//结案起始日期没有而截止日期有时候
    	{
    		alert("请补全通知起始日期！");
    		return;
    	}
    }
    else
    {
    	if(fm.TZEndDate.value==null||fm.TZEndDate.value=='')//结案起始日期有而截止日期没有时候
    	{
    		alert("请补全通知截止日期！");
    		return;
    	}
    }
    
    if(fm.JFStartDate.value==null||fm.JFStartDate.value=='')
    {
    	if(fm.JFEndDate.value!='')//给付起始日期没有而给付截止日有时候
    	{
    		alert("请补全给付起始日期！");
    		return;
    	}
    }
    else
    {
    	if(fm.JFEndDate.value==null||fm.JFEndDate.value=='')//给付起始日期有而给付截止日没有时候
    	{
    		alert("请补全给付截止日期！");
    		return;
    	}
    }
    if((fm.StartDate.value==null||fm.StartDate.value=='')&&(fm.JFStartDate.value==null||fm.JFStartDate.value=='')&&(fm.TZStartDate.value==null||fm.TZStartDate.value==''))//当结案和给付起止日期都为空时
    {
    	alert("结案、通知日期和给付起止日期至少输入一项！");
    	return;
    }
  var i = 0;

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	fm.target = "BranchResult";
	fm.action = "LLCasePolicySaveCSV.jsp"
	fm.op.value = "PRINT||BRANCH";
	fm.submit();
	showInfo.close();
}




function Mult()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "Mult";
	fm.action = "LLMultSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function CaseStatistics()
{
	if (verifyInput() == false)
    return false;
   var i = 0;
   var result = fm.result.value;
   var isWeek = fm.isweek.value;
   if(isWeek!='Y'){
       if(result=='Y'){
  	       var tStartDate = fm.StartDate.value;
           var tEndDate = fm.EndDate.value;
           if(dateDiff(tStartDate,tEndDate,"M")>3){
              // alert("上班时间，统计期最多为三个月！");
               //return false;
           }
       }
   }
   
  if(fm.ContType.value=='3' && fm.StatsType.value!='3'){
  	alert("银保渠道险种暂不支持统计‘商业保险’、‘社会保险’");
  	return false;
  } 
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.target = "CaseStatistics";
	if (fm.ContType.value=='3'){
	  fm.action = "LLCaseStatisticsBankReport.jsp";
  }else{
    fm.action = "LLCaseStatisticsReport.jsp";
  }
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function ComCase()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var result = fm.result.value;
  var isWeek = fm.isweek.value;
  if(isWeek!='Y'){
      if(result=='Y'){
  	      var tStartDate = fm.StartDate.value;
          var tEndDate = fm.EndDate.value;
          if(dateDiff(tStartDate,tEndDate,"M")>3){
             // alert("上班时间，统计期最多为三个月！");
              //return false;
          }
       }
  }
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.target = "ComCase";
	if(fm.ContType.value=='3'){
	fm.action="LLComCaseBankReport.jsp";
	}else{
	fm.action = "LLComCaseReport.jsp";
	}
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}



function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

			function afterCodeSelect( cCodeName, Field )
			{
				//选择了处理
				if( cCodeName == "DealWith1")
				{
					switch(fm.all('DealWith1').value){

						case "1":
						RiskCase();               //产品理赔状况
         //   alert("该报表需求已改变，尚处于开发阶段。")
						break;
						case "2":
						DutyCase();               //责任理赔状况
						break;
						case "3":
						Mult();               //保障级别
						break;
						case "4":
						CasePolicy();
						break;
						case "5":
						RiskCase(); 
						break;
						case "6":
						CaseStatistics();
					  break;
					  case "7":
					  ComCase();
					}
				}
			}
