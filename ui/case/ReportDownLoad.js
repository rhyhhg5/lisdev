 /*=============================================
		  Name：LLRptDownLoad.js
		  Function： 打印报表下载
		  Date：2010-8-3
		  Author：ZhangXiaolei
=============================================*/
var showInfo;
var turnPage = new turnPageClass();
window.onfocus = myonfocus;


function query()
{

if ( verifyInput() == false )　return;

    strSQL = "select inf.ManageCom,inf.UserName, dll.Location ,inf.RptName ,inf.TempName,dll.ReportName,inf.MagStartDate,inf.MagEndDate,inf.MakeDate,(case inf.MakeStatus when '0' then '正在打印' when '1' then '打印完成' end ) ,inf.Number,db2inst1.ReportCondition(inf.Number,inf.RptName)" 
           + " from LDRInfor inf ,LDDLLoc dll where dll.FileName = inf.RptName";

    strSQL2=" and inf.UserName= '" + fm.UserCode.value + "' and inf.MakeDate >= date('"+fm.StartDate.value+"') and inf.MakeDate <= date('"+fm.EndDate.value+"') and inf.ManageCom like '" + fm.ManageCom.value + "%'" ;
 
    if(fm.ReportNo.value=="" || fm.ReportNo.value==null)
        strSQL += strSQL2;
    else
      strSQL+=" and dll.remark1='" +fm.ReportNo.value +"' "+strSQL2;//注意留空格
   //alert(strSQL);
  // return;
  strSQL+=" order by makedate desc "
    turnPage.queryModal(strSQL, ReportMetaData);
 if (!turnPage.strQueryResult) {
    alert("没有查询到指定条件的记录！");
   
    }
 
}


function downLoadFile()

{
	var selno = ReportMetaData.getSelNo() - 1;
	if (selno >= 0) {	     
	   var filePath =  ReportMetaData.getRowColData (selno,3);	    
	   var fileName =  ReportMetaData.getRowColData (selno,5);	
	   var Status =	 ReportMetaData.getRowColData (selno,10);  
	   //alert(Status);
	   if( filePath == null || filePath == '' || fileName == null || fileName == '' ) 
	   {
	   		alert("系统未找到下载文件!");
		    return false;  
	   } 	
	   if(Status=="正在打印")
	   {
	      alert("报表正在打印无法下载！");
	      return false; 
	   }
	  // fm.target = "fraSubmit";
//	   alert("filePath:"+filePath+";fileName:"+fileName);
	   fm.target = "f1print";	  
	   fm.action = "./ReportDownLoadSave.jsp?filePath=" + filePath + "&fileName=" + fileName ;	    	   
	   fm.submit();
	   	    
	} else {
		alert("请选中要下载的报表！");
		return false;  
	}
	
}


