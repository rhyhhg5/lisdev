<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：LLBranchCaseReportCSV.jsp
	//程序功能：分案件类型工作时效
	//创建日期：2012-05-28
	//创建人  ：zhangyige
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page import="java.io.*"%>
<%
	System.out.println("start");
	CError cError = new CError();
	boolean operFlag = true;

	//直接获取页面的值
	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	String MngCom = request.getParameter("ManageCom");  //管理机构
	String CaseType = request.getParameter("CaseType"); //案件类型
	String StartDate = request.getParameter("StartDate"); //统计起期
	String EndDate = request.getParameter("EndDate"); //统计止期
	String StatsType = request.getParameter("StatsType");//统计类型
	System.out.println("StatsType:"+StatsType);

	TransferData Para = new TransferData();

	Para.setNameAndValue("MngCom", MngCom);
	Para.setNameAndValue("CaseType", CaseType);
	Para.setNameAndValue("StartDate", StartDate);
	Para.setNameAndValue("EndDate", EndDate);
	Para.setNameAndValue("StatsType", StatsType);

	//  ------------------------------
	RptMetaDataRecorder rpt = new RptMetaDataRecorder(request);
	//   	-------------------------------
	String date = PubFun.getCurrentDate().replaceAll("-", "");
	String time = PubFun.getCurrentTime3().replaceAll(":", "");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String fileNameB = tG.Operator + "_" + FileQueue.getFileName()
			+ ".vts";
	Para.setNameAndValue("tFileNameB", fileNameB);
	String operator = tG.Operator;
	System.out.println("@分案件类型工作时效@");
	System.out.println("Operator:" + operator);
	System.out.println("ManageCom:" + tG.ManageCom);
	System.out.println("Operator:" + tG.Operator);
	Para.setNameAndValue("tOperator", operator);
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	String CSVFileName = "";

	tVData.addElement(tG);
	tVData.addElement(Para);

	LLBranchCaseReportCSVBL tLLBranchCaseReportCSVBL = new LLBranchCaseReportCSVBL();

	if (!tLLBranchCaseReportCSVBL.submitData(tVData, "PRINT")) {
		operFlag = false;
		Content = tLLBranchCaseReportCSVBL.mErrors.getFirstError()
		.toString();
%>
<%=Content%>
<%
	return;
	} else {
		System.out.println("--------成功----------");
		CSVFileName = tLLBranchCaseReportCSVBL.getFileName() + ".csv";

		if ("".equals(CSVFileName)) {
			operFlag = false;
			Content = "没有得到要显示的数据文件";
%>

<%=Content%>
<%
		return;
		}
	}

	Readhtml rh = new Readhtml();

	String realpath = application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
	String temp = realpath.substring(realpath.length() - 1, realpath
			.length());
	if (!temp.equals("/")) {
		realpath = realpath + "/";
	}
	System.out.println(realpath);
	rpt.updateReportMetaData(StrTool.replace(CSVFileName, ".csv", ".zip"));
	
	String outpathname=realpath+"vtsfile/"+CSVFileName;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	//outpathname = ""; //	本地调试用
	System.out.println("outpathname="+outpathname);
	try {
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		CSVFileName = java.net.URLEncoder.encode(CSVFileName, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
		//outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	} catch (Exception ex) {
		ex.printStackTrace();
	}
	String[] InputEntry = new String[1];
	InputEntry[0] = outpathname;
	System.out.println(InputEntry[0]+"sdfsfsdfsdfsd");
	String tZipFile = StrTool.replace(outpathname, ".csv", ".zip");
	System.out.println("tZipFile == " + tZipFile);
	rh.CreateZipFile(InputEntry, tZipFile);
%>

<html>
<%@page contentType="text/html;charset=GBK"%>
<a
	href="../f1print/download.jsp?filename=<%=StrTool.replace(CSVFileName,".csv",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>

