<html>
	<%
	//Name：LLSurveyReply.jsp
	//Function：调查报告保存
	//Date：2004-12-23 16:49:22
	//Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLSurveyReply.js"></SCRIPT>
		<%@include file="LLSurveyReplyInit.jsp"%>
	</head>

	<body  onload="initForm();" >
		<form action="./LLSurveyReplySave.jsp" method=post name=fm target="fraSubmit">

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divReport);">
					</TD>
					<TD class= titleImg>
						调查报告
					</TD>
				</TR>
			</table>
			<div id=divReport>
				<table  class= common style=border: 1px #799AE1 solid;>
					<TR  class= common>
						<TD  class= title>理赔号</TD>
						<TD  class= input><Input class= "readonly" readonly name=OtherNo ></TD>
						<TD  class= title>客户号</TD>
						<TD  class= input colspan=2><Input class= "readonly" readonly name=CustomerNo ></TD>
						<TD  class= title>客户姓名</TD>
						<TD  class= input><Input class= "readonly" readonly name=CustomerName ></TD>
					</TR>
					<TR  class= common>
						<TD  class= title>身份证号</TD>
						<TD  class= input><Input class= "readonly" readonly name=IDNo></TD>
						<TD  class= title>职  业</TD>
						<TD  class= input colspan=2><Input class= "readonly" readonly name=Occupation></TD>
						<TD  class= title>工作单位</TD>
						<TD  class= input><Input class= "readonly" readonly name=Company></TD>
					</TR>
					<TR  class= common>
						<TD  class= title>单位地址</TD>
						<TD  class= input colspan=2><Input class= "readonly" readonly name=CompAddress style=width:100%></TD>
						<TD  class= title>家庭地址</TD>
						<TD  class= input colspan=3><Input class= "readonly" readonly name=HomeAddress style=width:100%></TD>
					</TR>
					<TR  class= common>
						<TD  class= title>联系电话</TD>
						<TD  class= input><Input class= "readonly" readonly name=Phone ></TD>
						<TD  class= title>申请人</TD>
						<TD  class= input colspan=2><Input class= "readonly" readonly name=Appnt ></TD>
						<TD  class= title>联系方式</TD>
						<TD  class= input8><Input class="codeno" name="AnswerMode" onClick="showCodeList('LLreturnMode',[this,AnswerModeName],[0,1]);" onkeyup="showCodeListKeyEx('LLreturnMode',[this,AnswerModeName],[0,1]);"  ><Input class= codename name=AnswerModeName ></TD>
					</TR>
					<TR  class= common>
						<TD  class= title>提调序号</TD>
						<TD  class= input><Input class= "readonly" readonly name=SerialNo ></TD>
						<TD  class= title>调查类型</TD>
						<TD  class= input colspan=2><Input class="readonly" readonly  name="SurveyTypeName"></TD>
						<TD  class= title8>提起日期</TD>
						<TD  class= input8><input class= readonly readonly name="SurveyStartDate"></TD>
					</TR>
					<TR  class= common>
						<TD  class= title colspan="7">调查内容</TD>
					</TR>
					<TR  class= common>
						<TD  class= input colspan="7">
						<textarea name="Content" cols="100%" rows="6" witdh=25% class="common"></textarea>
						</TD>
					</TR>
					<TR  class= common8 style = "display:'none'">
						<TD  class= title >提调人</TD>
						<TD  class= input><Input class= "readonly" readonly name=StartMan type=hidden></TD>
					</TR>
					<TR  class= common>
						<TD  class= title colspan="7">事件信息</TD>
					</TR>
					<TR  class= common>
						<TD  class= input colspan="7">
							<textarea name="AccDesc" cols="100%" rows="3" witdh=25% class="common"></textarea>
						</TD>
					</TR>
				</table>
				<div id=divreply>
				<table  class= common>
						     <TR  class= common>
							       <TD  class= title>调查指导意见</TD>
						     </TR>
						     <TR  class= common>
							       <TD  class= input>
								          <textarea name="resultlala" cols="100%" rows="3" witdh=25% class="common"></textarea>
							       </TD>
						     </TR>
					  </table>
					<table class=common>
						<TR  class= common>
							<TD  class= title>院内调查信息</TD>
						</TR>
						<TR  class= common>
							<TD  class= input>
								<textarea name="result" cols="100%" rows="6" witdh=25% class="common"></textarea>
							</TD>
						</TR>
						<TR  class= common>
							<TD  class= title>院外调查信息</TD>
						</TR>
						<TR  class= common>
							<TD  class= input>
								<textarea name="OHresult" cols="100%" rows="6" witdh=25% class="common"></textarea>
							</TD>
						</TR>
					</table>
				</div>
			</div>
			<table >
				<tr >
					<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divAccountList);">
					</td >
					<td class= titleImg> 调查材料</td>
				</tr>
			</table>
			<Div  id= "divAccountList" align = center style= "display: ''">
				<table  class= common >
					<TR  class= common>
						<TD text-align: left>
							<span id="spanAffixGrid"></span>
						</TD>
					</TR>
				</table>
			</Div>
			<br>
			<Div align="left" >
				<input name="AskIn" style="display:''" id=sbutton class=cssButton type=button value="回复保存" onclick="SurveyReplySave()">
				<input name="Return" style="display:''"  class=cssButton type=button value="返 回" onclick="CloseWindows()">
			</div>

			<!--隐藏域-->
			<Input type="hidden" class= common name="fmtransact" >
			<Input type="hidden" class= common name="SurveyNo" >
			<Input type="hidden" class= common name="OtherNoType" >
			<Input type="hidden" class= common name="SurveyFlag" >
			<Input type="hidden" class= common name="SurveyType" >
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
