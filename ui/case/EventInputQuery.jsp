<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<title>客户事件查询</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <script src="./EventInputQuery.js"></script> 

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="EventInputQueryInit.jsp"%>

</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm  action= "./LLCaseEventSave.jsp" target=fraSubmit method=post>
  

  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLLSubReportGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class= readonly name=InsuredNo readonly >
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= readonly name=Name readonly >
    </TD>
  </TR>
  <TR  class= common  style= "display: 'none'" >
    <TD  class= title>
      事故类型
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AccidentType >
    </TD>
   
    <TD  class= title>
      事件主题
    </TD>
    <TD  class= input colspan="3">
      <Input class= 'common' name=AccSubject >
    </TD>
    </TR>
  
</table>
  </Div>
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg>
    			 客户事件信息
    		</TD>
    	</TR>
    </Table>    	

 <Div  id= "divLDPerson1" style= "display: ''" align = center>
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanEventGrid" ></span> 
  	</TD>
      </TR>
    </Table>
  </Div>	
      <Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="getLastPage();"> 
    </div>
<hr>
<input class=cssButton type=button value="生成事件" onclick="showPage(this,divLLLLEventInput1);">								
<input class=cssButton type=button value="返 回" onclick=" top.close()">
 <Div  id= "divLLLLEventInput1" style= "display: 'none'">
     <table  class= common>
<TR  class= common8>
<TD  class= title8>重大事件标志</TD><TD  class= input8><Input class= code8 name="SeriousGrade1" CodeData= "0|^1|重大事件^2|一般事件" ondblClick="showCodeListEx('SeriousGrade',[this],[0]);" onkeyup="showCodeListKeyEx('SeriousGrade',[this],[0]);"></TD>
<TD  class= title8>发生日期</TD><TD  class= input8><Input class= coolDatePicker name="AccDate1"></TD>
<TD  class= title8>终止日期</TD><TD  class= input8><Input class= coolDatePicker name="AccEndDate1"></TD>
</TR>
<TR  class= common8>
<TD  class= title8>事故地点</TD><TD  class= input8><Input class= common name="AccPlace1"></TD>
<TD  class= title8>客户现状</TD><TD  class= input8><Input class= code name="CustSituation1"  ondblClick="showCodeList('llcuststatus',[this],[0]);" onkeyup="showCodeListKeyEx('llcuststatus',[this],[0]);" ></TD>
<TD  class= title8>医院代码</TD><TD  class= input8><Input class= code name="HospitalCode1" ondblclick="getHospitCode();return showCodeListEx('GetHospitCode',[this,HospitalName1],[0,1],'', '', '', true);" onkeyup="getHospitCode();return showCodeListKeyEx('GetHospitCode',[this,HospitalName1],[0,1],'', '', '', true);"></TD>
</TR><TR  class= common8>
<TD  class= title8>医院名称</TD><TD  class= input8><Input class= common name="HospitalName1" ></TD>

<TD  class= title8>入院日期</TD><TD  class= common8><Input class= 'coolDatePicker' dateFormat="Short" name="InHospitalDate1"></TD>
<TD  class= title8>出院日期</TD><TD  class= common8><Input class= 'coolDatePicker' dateFormat="Short" name="OutHospitalDate1"></TD>
</TR>


<TR  class= common>
	<TD  class= title colspan="6">事故描述</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="AccDesc1" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>


</table>
 <div align='left'>
	<input class=cssButton type=button value="事件保存" onclick="EventSave()">
	
 </div> 

</DIV>
	<input type=hidden id="CaseNo" name="CaseNo">	
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
	
</html>
