<html>
<%
//Name：LLEventInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：LiuYansong，陈海强
%>
 <%@page contentType="text/html;charset=GBK" %>
 <!--用户校验类-->
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLEventInput.js"></SCRIPT>
   <%@include file="LLEventInputInit.jsp"%>
 </head>

 <body  onload="initForm();initElementtype();" >
   <form action="./LLSubReportSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLEventInput1);">
         </TD>
         <TD class= titleImg>
         登记信息
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLEventInput1" style= "display: ''">
     <table  class= common>
<TR  class= common8>
	<TD  class= title8>
	客户号码
	</TD>
	<TD  class= input8>
	<Input class= common name="CustomerNo" elementtype=nacessary verify="客户号码|notnull&len<=20">
	</TD>
	<TD  class= title8>
	客户名称
	</TD>
	<TD  class= input8>
	<Input class= common name="CustomerName" verify="客户名称|len<=20">
	</TD>
	<TD  class= title8>
	客户类型
	</TD>
	<TD  class= input8>
	<Input class= code8 name="CustomerType" verify="客户类型|len<=20" ondblClick="showCodeList('CaseCustomer',[this],[0]);" onkeyup="showCodeList('CaseCustomer',[this],[0]);">
	</TD>
</TR>
<TR  class= common8>
	<TD  class= title8>
	事故类型
	</TD>
	<TD  class= input8>
	<Input class= code name="AccidentType" verify="事故类型|len<=1" CodeData= "0|^0|死亡^1|重大疾病^2|其他^3|意外" ondblClick="showCodeListEx('AccidentType',[this],[0]);" onkeyup="showCodeListKeyEx('AccidentType',[this],[0]);">
	</TD>
	<TD  class= title8>
	发生日期
	</TD>
	<TD  class= input8>
	<Input class= 'coolDatePicker' dateFormat="Short" name="AccDate">
	</TD>
	<TD  class= title8>
	终止日期</TD>
	<TD  class= input8>
	<Input class= 'coolDatePicker' dateFormat="Short" name="AccEndDate">
	</TD>
</TR>
<TR  class= common8>
	
	<TD  class= title8>
	事故地点
	</TD>
	<TD  class= input8>
	<Input class= common name="AccPlace" verify="事故地点|len<=100">
	</TD>
	<TD  class= title8>
	医院代码
	</TD>
	<TD  class= input8>
	<Input class= common name="HospitalCode" verify="医院代码|len<=10">
	</TD>
	<TD  class= title8>
	医院名称
	</TD>
	<TD  class= input8>
	<Input class= common name="HospitalName" verify="医院名称|len<=60">
	</TD>
</TR>
<TR  class= common8>
	
	<TD  class= title8>
	入院日期
	</TD>
	<TD  class= input8>
	<Input class= 'coolDatePicker' dateFormat="Short" name="InHospitalDate">
	</TD>
	<TD  class= title8>
	出院日期
	</TD>
	<TD  class= input8>
	<Input class= 'coolDatePicker' dateFormat="Short" name="OutHospitalDate">
	</TD>
	<TD  class= title8>
	重大事件标志
	</TD>
	<TD  class= input8>
	<Input class= code8 name="SeriousGrade" verify="重大事件标志|len<=1" CodeData= "0|^1|重大事件^2|一般事件" ondblClick="showCodeListEx('SeriousGrade',[this],[0]);" onkeyup="showCodeListKeyEx('SeriousGrade',[this],[0]);">
	</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">事件主题</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">	
		<textarea name="AccSubject" cols="100%" rows="2" witdh=25% class="common"></textarea>
	</TD>
</TR>
<TR  class= common>
	<TD  class= title colspan="6">事故描述</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="AccDesc" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>

   
</table>
</DIV>
<div id="div1" style="display: 'none' ">
	<table>
		 <TR  class= common>
	    <TD  class= title>
	      入机日期
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MakeDate >
	    </TD>
	    <TD  class= title>
	      入机时间
	    </TD>
	    <TD  class= input>
	      <Input class= 'common' name=MakeTime >
	    </TD>
	  </TR>
	<TD  class= title>
      分报案号(事件号)
    </TD>
    <TD  class= input>
      <Input class= 'common' name=SubRptNo >
    </TD>
	</table>
</div>
     <!--隐藏域-->
    
    <input type=hidden id="fmtransact" name="fmtransact">
     
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>