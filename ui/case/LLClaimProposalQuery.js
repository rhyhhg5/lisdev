//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var tDisplay;
var QuestQueryFlag ;
var arrConttype;
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,0,0,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 保单明细查询
function getQueryContDetail()
{  
	var selno = PolGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选中一条保障信息");
	    return false;
	}	
    var cContNo = PolGrid.getRowColData(selno - 1, 3);
    var tContType = PolGrid.getRowColData(selno - 1, 11);
  	var cPrtNo = PolGrid.getRowColData(selno - 1, 12);
  	var tTableType = PolGrid.getRowColData(selno - 1, 13);
  	var cManageCom = PolGrid.getRowColData(selno - 1, 14);
  	
  	if(tTableType == "C")
  	{
  	  tTableType = '1';
  	}
  	else
  	{
  	  tTableType = '2';
  	}
	if (tContType == '2')
	{

    	window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ cContNo+"&ContType="+tTableType);
	}
	else if (tContType == '1')
	{
		window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0"+"&ContType="+tTableType);	
	}
}
//扫描件查询
function ScanQuery()
{
  var selno = PolGrid.getSelNo();
  if (selno <= 0)
	{
		alert("请选中一条保障信息!");
	    return false;
	}	 
  var tContNo = PolGrid.getRowColData(selno - 1, 3);
  var tContType = PolGrid.getRowColData(selno - 1, 11);
  var cPrtNo = PolGrid.getRowColData(selno - 1, 12);
  
  var arrReturn = new Array();
  var arrReturn1 = new Array();
  var tBussNoType = "";
  var tBussType = "";
  var tSubType = "";

  //查询团单记录(撤保或退保)
  var strSql = "select PrtNo from LCGrpCont where GrpContNo='" + tContNo + "'";
  var arrReturn = new Array();
  arrReturn = easyExecSql(strSql);
  
  //查询团单记录(撤保或退保)
    var strSql1 = "select PrtNo from LBGrpCont where GrpContNo='" + tContNo + "'";
    var arrReturn1 = new Array();
    arrReturn1 = easyExecSql(strSql1);
  
  if (arrReturn == null && arrReturn1 ==null ) {
  
    var arrReturn2 = new Array();
  	var strSql2 = "select PrtNo from LCCont where ContNo='" + tContNo + "'";
  	arrReturn2 = easyExecSql(strSql2);
  
          //查询团单记录(撤保或退保)
  	var strSql3 = "select PrtNo from LBCont where ContNo='" + tContNo + "'";
  	var arrReturn3 = new Array();
  	arrReturn3 = easyExecSql(strSql3);
          
  	//ContLoadFlag: 1 个单; 2 团单
  	//ContType: 1 签单; 2 撤保/退保
  	if (arrReturn2 == null && arrReturn3 ==null ) {
  		alert("没有该合同号，请重新输入");
  	} 
  	var strPrtNo = "";
  	if(arrReturn2== null && arrReturn3 !=null){
  	    strPrtNo = arrReturn3[0][0];
  	}
  	if(arrReturn2 != null&& arrReturn3 ==null){
  	    strPrtNo = arrReturn2[0][0];
  	}
  	var strSQL = "select a.docid,a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+strPrtNo+"%%'"
				  + " and a.busstype='TB'"
				  + " and b.busstype='TB'";	
	var arrReturn4 = new Array();
  	arrReturn4 = easyExecSql(strSQL);
  	if(arrReturn4 != null)
  	{   
  	    var cDocID = arrReturn4[0][0];
  	    var cDocCode = arrReturn4[0][1];
  	    var	cBussTpye = "TB" ;
  	    var cSubTpye = arrReturn4[0][2];
  	    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
  	}else{
  	   alert("该印刷号没有投保书影像件！");
  	}	
  }else{
    var strPrtNo1 = "";
  	if(arrReturn== null && arrReturn1 !=null){
  	    strPrtNo1 = arrReturn1[0][0];
  	}
  	if(arrReturn != null&& arrReturn1 ==null){
  	    strPrtNo1 = arrReturn[0][0];
  	}
  	var strSQL1 = "select a.docid,a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+strPrtNo1+"%%'"
				  + " and a.busstype='TB'"
				  + " and b.busstype='TB'";	
	var arrReturn5 = new Array();
  	arrReturn5 = easyExecSql(strSQL1);
  	if(arrReturn5 != null)
  	{   
  	    var cDocID = arrReturn5[0][0];
  	    var cDocCode = arrReturn5[0][1];
  	    var	cBussTpye = "TB" ;
  	    var cSubTpye = arrReturn5[0][2];
  	    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
  	}else{
  	   alert("该印刷号没有投保书影像件！");
  	}
  }		
}
//合同处理查询
function getQueryContDeal()
{
	var selno = PolGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选中一条保障信息!");
	    return false;
	}	
	var cContNo = PolGrid.getRowColData(selno - 1, 3);
	if(cContNo == null || cContNo == "") return false;	
	window.open("../case/LLContDealQueryInput.jsp?ContNo=" + cContNo + "&AppObj=I");
}
//万能账户查询
function getWNQuery()
{
	var selno = InsuredGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选中一条客户信息");
	    return false;
	}	
	var CustomerNo = InsuredGrid.getRowColData(selno - 1, 1);
	var CustomerName = InsuredGrid.getRowColData(selno - 1, 2);
	var CustomerSex = InsuredGrid.getRowColData(selno - 1, 3);
	if(CustomerNo == null || CustomerNo == "") return false;
	
	var varSrc = "&InsuredNo=" + CustomerNo;
	varSrc += "&CustomerName=" + CustomerName;
	pathStr="./FrameMainSearchAccValue.jsp?Interface=LLSearchAccValueInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"SearchAccValueInput","left");
}
//出险信息记录
function getQueryDetail()
{	
	var selno = InsuredGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选中一条客户信息");
	    return false;
	}	
	var CustomerNo = InsuredGrid.getRowColData(selno - 1, 1);
	var CustomerName = InsuredGrid.getRowColData(selno - 1, 2);
	var CustomerSex = InsuredGrid.getRowColData(selno - 1, 3);
	if(CustomerNo == null || CustomerNo == "") return false;
	
	window.open("../case/LLDetailInput.jsp?InsuredNo="+CustomerNo+"&CustomerName="+CustomerName+"&CustomerSex="+CustomerSex+"");	
}



// 保单明细查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cContNo = PolGrid.getRowColData(tSel - 1,1);				
		if (cContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0");	
	}
}

// 基本信息查询
function FunderInfo()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cInsuredNo = PolGrid.getRowColData(tSel - 1,4);	
		var cGrpContNo = PolGrid.getRowColData(tSel - 1,5);		    	
		if (cInsuredNo == "")
		  return;				
	    window.open("CQPersonMain.jsp?CustomerNo="+cInsuredNo+"&GrpContNo="+cGrpContNo);	
	}
}
//个人业务--投保信息查询
function PersonBiz()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cContNo = PolGrid.getRowColData(tSel - 1,1);				
	    var cPrtNo = PolGrid.getRowColData(tSel - 1,2);	
	    var cCustomerNo = PolGrid.getRowColData(tSel - 1,4);
	    var cGrpContNo  = PolGrid.getRowColData(tSel - 1,5); 
		if (cContNo == "")
		    return;	
	    window.open("PolDetailQueryMain.jsp?ContNo="+cContNo+"&PrtNo="+cPrtNo+"&CustomerNo="+cCustomerNo+"&GrpContNo="+cGrpContNo+"&IsCancelPolFlag=0"+"&IsCancelPolFlag=0");	
	}
}

//保单保障项目查询
function PolPolicy()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var ContNo = PolGrid.getRowColData(tSel - 1,1);		
	    var CustomerNo = PolGrid.getRowColData(tSel - 1,4);		
		if (ContNo == "" || CustomerNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../case/PolInfoQueryMain.jsp?ContNo="+ContNo+"&CustomerNo="+CustomerNo+"&IsCancelPolFlag=0");	
	}
}

//理赔调查记录信息查询
function getQuerySurvey()
{
	var selno = InsuredGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选中一条客户信息");
	    return false;
	}	
	var CustomerNo = InsuredGrid.getRowColData(selno - 1, 1);
	
	var CustomerName = InsuredGrid.getRowColData(selno - 1, 2);
	if(CustomerNo == null || CustomerNo == "") return false;

	if (CaseGrid.mulLineCount =0)
	{
		alert("被保险人"+CustomerName+"没有调查信息!");
	    return false;
	}
	window.open("../case/RgtSurveyInput.jsp?InsuredNo="+CustomerNo+"&CustomerName="+CustomerName+"&Type=2");	
}

//事件记录信息查询
function getQueryAccident()
{
	var selno = InsuredGrid.getSelNo();
	if (selno <= 0)
	{
		alert("请选中一条客户信息");
	    return false;
	}	
	var CustomerNo = InsuredGrid.getRowColData(selno - 1, 1);
	var CustomerName = InsuredGrid.getRowColData(selno - 1, 2);
	if(CustomerNo == null || CustomerNo == "") return false;
	
	window.open("../case/LLAcciDentInput.jsp?InsuredNo="+CustomerNo+"&CustomerName="+CustomerName+"");	
}
//问题件回销
function QuestBack()
{
  var selno = PolGrid.getSelNo();
  if (selno <= 0)
  {
		alert("请选中一条保障信息!");
	    return false;
  }	
  var cContNo = PolGrid.getRowColData(selno - 1, 3);
  var cProposalContNo = PolGrid.getRowColData(selno - 1, 15);
  if(cProposalContNo == null || cProposalContNo == "") return false;
  var Str="select 1 from lcissuepol where proposalcontno='"+ cProposalContNo +"' ";
  var ARR = easyExecSql(Str);
  if(!ARR) {
      alert("未录入问题件!");
      return false;
    }	
  window.open("../uw/UWManuQuestQMain.jsp?ContNo="+cProposalContNo+"&Flag=1" ,"window2");
}

//体检件回销
function showHealthQ()
{
  var selno = PolGrid.getSelNo();
  if (selno <= 0)
  {
		alert("请选中一条保障信息!");
	    return false;
  }	
  var cContNo = PolGrid.getRowColData(selno - 1, 3);
  var cPrtNo = PolGrid.getRowColData(selno - 1, 12);
  var cProposalContNo = PolGrid.getRowColData(selno - 1, 15);
  if(cProposalContNo == null || cProposalContNo == "") return false;
  var Str="select 1 from LCpenotice where proposalcontno='"+ cProposalContNo +"' ";
  var ARR = easyExecSql(Str);
  if(!ARR) {
      alert("未录入体检件!");
      return false;
    }	  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  window.open("../uw/UWManuHealthQMain.jsp?ContNo="+cProposalContNo+"&PrtNo="+cPrtNo+"&LoadFlag=1","window1");
  showInfo.close();
}

//【查询】按钮
function easyQueryClick()
{
	var comCode = fm.ComCode.value;
	initInsuredGrid();
	initPolGrid();
	initCaseGrid();	
	if ((fm.ContNo.value==null||fm.ContNo.value=="")
	    &&(fm.InsuredNo.value==null||fm.InsuredNo.value=="")
	    &&(fm.InsuredName.value==null||fm.InsuredName.value=="")
	    &&(fm.AgentCode.value==null||fm.AgentCode.value=="")
	    &&(fm.IDNo.value==null||fm.IDNo.value=="")
	    &&(fm.AgentName.value==null||fm.AgentName.value=="")) 
	{
		alert("请输入查询条件！");
		return false;
	}
	if(!fm.ContNo.value==null||!fm.ContNo.value=="")
	{
		//var strContNo =" select distinct conttype from lccont where appflag='1' and contno='"+fm.ContNo.value+"' or grpcontno='"+fm.ContNo.value+"'"
		//	+" union all select distinct conttype from lbcont where appflag='1' and contno='"+fm.ContNo.value+"' or grpcontno='"+fm.ContNo.value+"' with ur";
		var sql = "select distinct conttype "
		        + "from lccont "
		        + "where appflag = '1' "
		        + "   and contno = '" + fm.ContNo.value+"' "  //C表个单
		        + " union all "
		        + "select distinct conttype "
		        + "from lccont "
		        + "where grpcontno = '" + fm.ContNo.value + "' "  //C表团单
			      + " union all "
			      + "select distinct conttype "
			      + "from lbcont "
			      + "where appflag = '1' "
			      + "   and contno = '" + fm.ContNo.value + "' " //B表个单
			      + " union all "
			      + "select distinct conttype "
			      + "from lbcont "
			      + "where grpcontno = '" + fm.ContNo.value + "' "  //B表团单
			      + "with ur";
		var arrResult = easyExecSql(sql);
		arrConttype = arrResult[0][0];
	}
	queryInsuredGrid();	//查询客户信息
	queryPolGrid();		//查询保障信息
	queryCaseGrid();	//查询理赔记录	
}
function queryInsuredGrid()
{
	//查询客户信息
	var strInsuredSQL = "select distinct a.InsuredNo,a.Name,codename('sex',a.Sex),a.IDNo,a.Birthday from lcinsured a where 1=1"
     + getWherePart("a.InsuredNo", "InsuredNo")
     + getWherePart("a.Name", "InsuredName")
     + getWherePart("a.IDNo","IDNo")
     + getWherePart("(select distinct getUniteCode(agentcode) from LCCont b where b.ContNo = a.ContNo)","AgentCode")
     + getWherePart("(select distinct c.Name from LCCont b, LAAgent c where b.agentcode =c.agentcode and b.ContNo = a.ContNo)","AgentName")
	if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='1')
	{
		//strInsuredSQL = strInsuredSQL+" and exists (select 1 from lccont where contno=a.contno and insuredno=a.insuredno "
		//+" and contno='"+fm.ContNo.value+"' )"
		strInsuredSQL = strInsuredSQL + " and a.ContNo = '" + fm.ContNo.value + "' ";
	}else if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='2')
	{
		//strInsuredSQL = strInsuredSQL+" and exists (select 1 from lccont where contno=a.contno and insuredno=a.insuredno "
		//+" and grpcontno='"+fm.ContNo.value+"' )"
		strInsuredSQL = strInsuredSQL+" and a.GrpContNo = '" + fm.ContNo.value + "' ";
	}	  		 
	  strInsuredSQL = strInsuredSQL +" union select distinct a.InsuredNo,a.Name,codename('sex',a.Sex),a.IDNo,a.Birthday from lbinsured a where 1=1 "
	   + getWherePart("a.InsuredNo", "InsuredNo")
       + getWherePart("a.Name", "InsuredName")
       + getWherePart("a.IDNo","IDNo")
       + getWherePart("(select distinct getUniteCode(agentcode) from LBPol b where b.ContNo = a.ContNo)","AgentCode")
       + getWherePart("(select distinct c.Name from LBPol b, LAAgent c where b.agentcode =c.agentcode and b.ContNo = a.ContNo)","AgentName")
	if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='1')
	{
		//strInsuredSQL = strInsuredSQL+" and exists (select 1 from lbcont where contno=a.contno and insuredno=a.insuredno "
		//+" and contno='"+fm.ContNo.value+"' )"
		strInsuredSQL = strInsuredSQL+" and a.ContNo = '" + fm.ContNo.value + "' ";
	}
	else if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='2')
	{
		//strInsuredSQL = strInsuredSQL+" and exists (select 1 from lbcont where contno=a.contno and insuredno=a.insuredno "
		//+" and grpcontno='"+fm.ContNo.value+"' )"
			strInsuredSQL = strInsuredSQL+" and a.GrpContNo = '" + fm.ContNo.value + "' ";
	} 
	turnPage1.queryModal(strInsuredSQL, InsuredGrid);
}
function queryPolGrid()
{
	//查询保障信息
	var strPolSQL = "select a.InsuredNo,a.Name,case when c.grpcontno='00000000000000000000' then a.ContNo else c.grpcontno end,"
		  +" c.cvalidate,c.CInValiDate,codeName('stateflag', c.StateFlag),c.managecom,(select name from laagent where agentcode=c.agentcode),"
		  +" (select coalesce(sum(realpay),0) from llclaimdetail de,llcase ca where de.caseno=ca.caseno and de.contno=c.contno and ca.rgtstate in ('09','11','12')),"
		  +" (select coalesce(sum(RealHospDate),0) from llclaimdetail e,LLFeeMain f where e.caseno=f.caseno and e.contno=c.contno),"
		  +" c.conttype,c.prtno,'C',c.managecom,c.ProposalContNo"
		  //+" from LCInsured a, LCCont c  where 1=1 and a.ContNo = c.ContNo and a.insuredno=c.insuredno"
		  +" from LCInsured a, LCCont c  where 1=1 and a.ContNo = c.ContNo "
		  +" and c.appflag='1' "
		  + getWherePart("a.InsuredNo", "InsuredNo")
	      + getWherePart("a.Name", "InsuredName")
	      + getWherePart("a.IDNo","IDNo")
	      + getWherePart("getUniteCode(c.agentcode)","AgentCode")
	      + getWherePart("(select b.Name from LAAgent b where b.agentcode = c.agentcode)","AgentName")
	      
	    if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='1')
		{
			strPolSQL = strPolSQL+" and c.contno='"+fm.ContNo.value+"'"
		}
		else if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='2')
		{
			strPolSQL =strPolSQL+" and c.grpcontno='"+fm.ContNo.value+"'"
		} 			  
		  
		  strPolSQL = strPolSQL + " union " + strPolSQL.replace("LCInsured", "LBInsured"); //上面是全在C表的情况，本语句查的是合同在C被保人在B的情况
		  strPolSQL =strPolSQL +" union "
		  +" select a.InsuredNo,a.Name,case when c.grpcontno='00000000000000000000' then a.ContNo else c.grpcontno end,"
		  +" c.cvalidate,c.CInValiDate,codeName('stateflag', c.StateFlag),c.managecom,(select name from laagent where agentcode=c.agentcode),"
		  +" (select coalesce(sum(realpay),0) from llclaimdetail de,llcase ca where de.caseno=ca.caseno and de.contno=c.contno and ca.rgtstate in ('09','11','12')),"
		  +" (select coalesce(sum(RealHospDate),0) from llclaimdetail e,LLFeeMain f where e.caseno=f.caseno and e.contno=c.contno),"
		  +" c.conttype,c.prtno,'B',c.managecom,c.ProposalContNo"
		  //+" from LbInsured a, LbCont c  where 1=1 and a.ContNo = c.ContNo and a.insuredno=c.insuredno"
		  +" from LbInsured a, LbCont c  where 1=1 and a.ContNo = c.ContNo "
		  +" and c.appflag='1' "
		  + getWherePart("a.InsuredNo", "InsuredNo")
	      + getWherePart("a.Name", "InsuredName")
	      + getWherePart("a.IDNo","IDNo")
	      + getWherePart("getUniteCode(c.agentcode)","AgentCode")
	      + getWherePart("(select b.Name from LAAgent b where b.agentcode =c.agentcode)","AgentName")
	   
 		if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='1')
		{
			strPolSQL = strPolSQL+" and c.contno='"+fm.ContNo.value+"'"
		}
		else if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='2')
		{
			strPolSQL =strPolSQL+" and c.grpcontno='"+fm.ContNo.value+"'"
		} 	
	turnPage2.queryModal(strPolSQL, PolGrid);
	
}

function queryCaseGrid()
{		
   //查询理赔记录
	var strCaseSQL = "select a.caseno,a.Handler,(select username from lduser where usercode=a.Handler),"
		+" a.rgtdate,a.endcasedate,codename('llrgtstate',a.rgtstate),"
		+" (select coalesce(sum(de.realpay),0) from llclaimdetail de,llcase ll where de.caseno=a.caseno and ll.caseno=de.caseno and ll.rgtstate in ('09','11','12'))"
		+" from llcase a where 1=1"
		+ getWherePart("a.customerno", "InsuredNo")
	    + getWherePart("a.customerName", "InsuredName")
	    + getWherePart("a.IDNo","IDNo")
	    //+ getWherePart("(select agentcode from lccont where insuredno=a.customerno and appflag='1' union select agentcode from lbcont where insuredno=a.customerno and appflag='1')","AgentCode")
	    //+ getWherePart("(select agentname from lccont where insuredno=a.customerno and appflag='1' union select agentname from lbcont where insuredno=a.customerno and appflag='1')","AgentName")
	    
	    + getWherePart("(select distinct getUniteCode(agentcode) from lccont where insuredno=a.customerno and appflag='1')","AgentCode")
	    + getWherePart("(select distinct Name from lccont b, laagent c where b.agentcode =c.agentcode and b.insuredno=a.customerno and appflag='1')", "AgentName")
	    
	//if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='1')
	//{		
	//	strCaseSQL +=" and exists (select 1 from lccont where insuredno=a.customerno and contno='"+ fm.ContNo.value +"' "
	//		+" union select 1 from lbcont where insuredno=a.customerno and contno='"+ fm.ContNo.value +"')"
	//}
	//else if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='2')
	//{
	//	strCaseSQL +=" and exists (select 1 from lccont where insuredno=a.customerno and grpcontno='"+ fm.ContNo.value +"' "
	//	    +" union select 1 from lbcont where insuredno=a.customerno and grpcontno='"+ fm.ContNo.value +"')"
	//	
	//}   
	//
	//strCaseSQL =strCaseSQL+" with ur";
	//turnPage3.queryModal(sql, CaseGrid);
	
	var tContCdtn = "";  //录入保单号时生成保单号的查询条件
	if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='1')
	{		
		//tContCdtn += " and exists (select 1 from lccont where insuredno=a.customerno and contno='" + fm.ContNo.value + "') ";
		tContCdtn += " and exists (select 1 from LLClaimDetail where CaseNO = a.CaseNO and ContNo = '" + fm.ContNo.value + "') ";
	}
	else if((!fm.ContNo.value==null||!fm.ContNo.value=="")&& arrConttype =='2')
	{
		//tContCdtn += " and exists (select 1 from lccont where insuredno=a.customerno and grpcontno='" + fm.ContNo.value + "' )";
		tContCdtn += " and exists (select 1 from LLClaimDetail where CaseNO = a.CaseNO and GrpContNo = '" + fm.ContNo.value + "') ";
	} 
	
  var tRegExp = /lccont/g;  //正则表达式
	strCaseSQL = strCaseSQL + tContCdtn;  //c表
	strCaseSQL = strCaseSQL
	           + " union "
	           + strCaseSQL.replace(tRegExp,"lbcont")  //b表。用正则表单时把所有lccont替换成lbcong
	           + " with ur"; 
	turnPage3.queryModal(strCaseSQL, CaseGrid);
}
function queryCustomer()
{
	var selno = InsuredGrid.getSelNo();
	if (selno <= 0)
	{
	      return false;
	}
	var CustomerNo = InsuredGrid.getRowColData(selno - 1, 1);
	if(CustomerNo == null || CustomerNo == "") return false;

	 var strSql1 = "select distinct a.InsuredNo,a.Name,case when c.grpcontno='00000000000000000000' then a.ContNo else c.grpcontno end,"
		  +" c.cvalidate,c.CInValiDate,codeName('stateflag', c.StateFlag),c.managecom,(select name from laagent where agentcode=c.agentcode),"
		  +" (select coalesce(sum(pay),0) from ljagetclaim where contno=c.contno),"
		  +" (select coalesce(sum(RealHospDate),0) from llclaimdetail e,LLFeeMain f where e.caseno=f.caseno and e.contno=c.contno),"
		  +" c.conttype,c.prtno,'C',c.managecom,c.ProposalContNo"
		  +" from LCInsured a, LCCont c  where 1=1 and a.ContNo = c.ContNo and a.insuredno=c.insuredno"
		  +" and c.appflag='1' and a.insuredno='"+CustomerNo+"'"
		  +" union "
		  +" select distinct a.InsuredNo,a.Name,case when c.grpcontno='00000000000000000000' then a.ContNo else c.grpcontno end,"
		  +" c.cvalidate,c.CInValiDate,codeName('stateflag', c.StateFlag),c.managecom,(select name from laagent where agentcode=c.agentcode),"
		  +" (select coalesce(sum(pay),0) from ljagetclaim where contno=c.contno),"
		  +" (select coalesce(sum(RealHospDate),0) from llclaimdetail e,LLFeeMain f where e.caseno=f.caseno and e.contno=c.contno),"
		  +" c.conttype,c.prtno,'C',c.managecom,c.ProposalContNo"
		  +" from LbInsured a, LbCont c  where a.ContNo = c.ContNo and a.insuredno=c.insuredno"
		  +" and c.appflag='1' and a.insuredno='"+CustomerNo+"' with ur";
	 turnPage2.queryModal(strSql1,PolGrid);
	 
	 var strSql2 = "select a.caseno,a.Handler,(select username from lduser where usercode=a.Handler),"
		+" a.rgtdate,a.endcasedate,codename('llrgtstate',a.rgtstate),"
		+" (select coalesce(sum(de.realpay),0) from llclaimdetail de,llcase ll where de.caseno=a.caseno and ll.caseno=de.caseno and ll.rgtstate in ('09','11','12'))"
		+" from llcase a where 1=1 and a.customerno='"+CustomerNo+"' with ur";	
	 turnPage3.queryModal(strSql2,CaseGrid);
}
// 数据返回父窗口
function returnParent()
{
	if (tDisplay=="1")
	{
	    var arrReturn = new Array();
	    var tSel = PolGrid.getSelNo();
	    //alert(tSel);
	    if( tSel == 0 || tSel == null )
	    	alert( "请先选择一条记录，再点击返回按钮。" );
	    else
	    {
	    	try
	    	{
	    		arrReturn = getQueryResult();
	    		
	    		top.opener.afterQuery( arrReturn );
	    		
	    		top.close();
	    	}
	    	catch(ex)
	    	{
	    		alert( "请先选择一条非空记录，再点击返回按钮。");
	    		
	    	}
	    	
	    }
	 }
	 else
	 {
	    top.close(); 
	 }
}

function getQueryResult()
{
	var arrSelected = null;
	var tRow = PolGrid.getSelNo();
	
	if( tRow == 0 || tRow == null || arrDataSet == null )
		      return arrSelected;
	
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	
	return arrSelected;
}
//查询黑名单
function getQueryBlack() 
{
	var tSel = InsuredGrid.getSelNo();
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录!" );
		return false;
	} 
	else 
	{
		var CustomerNo = InsuredGrid.getRowColData(tSel - 1,1);
		var CustomerName = InsuredGrid.getRowColData(tSel - 1,2);
		var varSrc = "?CustomerNo=" + CustomerNo;
		varSrc += "&CustomerName=" + CustomerName;
	    varSrc += "&CaseNo=" ;
		pathStr="./FrameMainBlackList.jsp?Interface=BlackListInput.jsp"+varSrc+"&LoadC=2";
		showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
	}
}

// 查询返回
function afterCodeSelect( cCodeName, Field )
{
  //选择了处理
  if( cCodeName == "DealWith")
  {
    LoadC="1";
    LoadD="1";
    switch(fm.all('DealWith').value){
      case "0":
      DealConsult();             //咨询通知
      window.focus();              //咨询通知
      break;
      case "1":
      DealApp();                 //受理申请
      window.focus();              //咨询通知
      break;
      case "2":
      DealAppGrp();              //团体受理申请
      window.focus();              //咨询通知
      break;
      case "3":
      DealFeeInput();            //帐单录入
      window.focus();              //咨询通知
      break;
      case "4":
      DealCheck();               //检录
      window.focus();              //咨询通知
      break;
      case "5":
      DealClaim();               //理算
      window.focus();              //咨询通知
      break;
      case "6":
      DealApprove();             //审批审定
      window.focus();              //咨询通知
      break;
      case "7":
      DealCancel();							 //撤件
      window.focus();              //咨询通知
      break;
 	  case "8":
      openSubmittedAffix();					//材料退回登记
      window.focus();  
       break;
    }
  }
}

//咨询通知 -- ok
function DealConsult()
{
 
  var caseNo="";

 var customerno=InsuredGrid.getRowColData(0,1);
 strsql = "select consultno from LLMainAsk a,LLAnswerinfo b where a.LogNo=b.LogNo and a.logerno='"+customerno+"' fetch first 1 rows only";
 arrResult=easyExecSql(strsql);
   if(arrResult != null)
  {
    caseNo = arrResult[0][0];
  }
  var varSrc="&CaseNo="+ caseNo;// 为了保证接口一致性，做了些调整
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseNote.jsp?Interface= LLMainAskInput.jsp"+varSrc,"咨询通知","left");

}

//受理申请 --ok
function DealApp()
{

  var selno = CaseGrid.getSelNo()-1;
  var caseNo="";
  var RgtNo="";
  if (selno >=0)
  {
    caseNo = CaseGrid.getRowColData( selno, 1);
  }
  strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }
  var varSrc= "&RgtNo="+ RgtNo + "&CaseNo="+ caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseRequest.jsp?Interface=LLRegisterInput.jsp"+varSrc,"","left");

}

//团体受理申请 --ok
function DealAppGrp()
{
  var selno = CaseGrid.getSelNo()-1;
  var caseNo="";
  var RgtNo="";
  if (selno >=0)
  {
    caseNo = CaseGrid.getRowColData( selno, 1);
  }
  
  strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
  arrResult = easyExecSql(strSQL);

  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }

  var varSrc="&RgtNo="+ RgtNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainGrpCheck.jsp?Interface=LLGrpRegisterInput.jsp"+varSrc,"","left");

}

//帐单录入 --ok
function DealFeeInput()
{	
  var CaseNo="";
  var selno = CaseGrid.getSelNo()-1;
  if (selno>=0)
  {
    CaseNo = CaseGrid.getRowColData(selno, 1);
  }
  var varSrc = "&CaseNo=" + CaseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow =OpenWindowNew("./FrameMainZD.jsp?Interface=ICaseCureInput.jsp" + varSrc,"","left");

}

//检录 --ok
function DealCheck()
{
  var caseNo ="";
  var selno = CaseGrid.getSelNo()-1;
  if (selno >=0)
  {
    caseNo = CaseGrid.getRowColData( selno, 1);
  }
  var varSrc="&CaseNo="+ caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+
  varSrc,"检录","left");

}

//理算 --OK
function DealClaim()
{
  var caseno = '';
  var selno = CaseGrid.getSelNo()-1;
  if (selno >=0)
  {
    caseno = CaseGrid.getRowColData(selno,1);
  }
  var varSrc="&CaseNo=" + caseno ;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseClaim.jsp?Interface=LLClaimInput.jsp"+
  varSrc,"理算","left");
}

//审批审定 --ok
function DealApprove()
{
  var selno = CaseGrid.getSelNo()-1;
  var caseNo = "";
  if (selno>=0)
  {
    caseNo = CaseGrid.getRowColData( selno, 1);
  }

  var varSrc = "&CaseNo=" + caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + varSrc,"","left");

}

//撤件 --ok
function DealCancel()
{
  var caseno = '';
  var selno = CaseGrid.getSelNo()-1;
  if (selno >=0)
  {
    caseno = CaseGrid.getRowColData(selno,1);
    strSQL="select CustomerNo,CustomerName from llcase where caseno='"+caseno+"'";
    arrResult = easyExecSql(strSQL);
    if(arrResult != null)
     {
      CustomerNo = arrResult[0][0];
      CustomerName=arrResult[0][1];
     }
     var varSrc="&CaseNo=" + caseno +"&CustomerNo="+CustomerNo+"&CustomerName="+CustomerName;
     varSrc += "&LoadD="+LoadD;
  showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }else{
    alert("请至少选择一条理赔记录！");
    return false;
  }
  
}

//材料退回登记 --ok
function openSubmittedAffix()
{
   var caseno = '';
   var selno = CaseGrid.getSelNo()-1;
   if (selno >=0)
  {
    caseno = CaseGrid.getRowColData(selno,1);
  }
  strSQL="select rgtno,rgtstate from llcase where caseno='"+caseno+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
    RgtState=arrResult[0][1];
  }
  if(RgtState=="09"||RgtState=="12")
  {
	  //showInfo = window.open("./SubmittedAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value);
	  pathStr="./SubmittedAffixMain.jsp?CaseNo="+caseno+"&RgtNo="+RgtNo+"&LoadD="+LoadD;
	  showInfo = OpenWindowNew(pathStr,"","middle");
	}else
		{
			alert("未结案，不能回退");
		}
}

