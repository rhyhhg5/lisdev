<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLClaimCollecionTypeSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.config.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDCodeSet tLDCodeSet   = new LDCodeSet();
  OLDCodeSetUI tOLDCodeSetUI   = new OLDCodeSetUI();
  //输出参数
  CErrors tError = null;
  String zdywcode  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  TransferData td=new TransferData();
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  //transact = "DELETE&INSERT";
  transact=request.getParameter("fmtransact");
  zdywcode=request.getParameter("zdywcode");
  String tRadio[] = request.getParameterValues("InpBusinessGridSel");
  String tCode[] = request.getParameterValues("BusinessGrid1");
  String tCodeName[] = request.getParameterValues("BusinessGrid2");
  td.setNameAndValue("zdywcode",request.getParameter("zdywcode"));//add by zzh 101112
  for (int index=0;index<tRadio.length;index++) {
	  if ("1".equals(tRadio[index])) {
	    LDCodeSchema tLDCodeSchema = new LDCodeSchema();
	    tLDCodeSchema.setCodeType("llgrptype");
	    tLDCodeSchema.setCode(tCode[index]);
	    tLDCodeSchema.setCodeName(tCodeName[index]);
	    tLDCodeSet.add(tLDCodeSchema);
	  }
  }
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLDCodeSet);
  	tVData.add(td);
  	tOLDCodeSetUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDCodeSetUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success1";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
