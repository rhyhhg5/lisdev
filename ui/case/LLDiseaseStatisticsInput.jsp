<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="java.util.*"%> 
<html>
<%
  //程序名称：LLDiseaseStatisticsInput
  //程序功能：按疾病平均花费统计
  //创建日期：2014-4-9
  //创建人  ：Houyd
  //更新记录：  更新人    更新日期     更新原因/内容
%>

<%	
	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
%>
<head>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/CommonTools.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./LLDiseaseStatisticsInput.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="LLDiseaseStatisticsInit.jsp"%>
</head>

<body onload="initForm();">
  <form action="./LLDiseaseStatisticsSave.jsp" method="post" name="fm" target="fraSubmit">
    <hr/>
    <table class="common">
      <TR class= common>
		<TD class= titleImg>查询条件：</TD>
	  </tr>
      <tr class="common">
      	<TD  class= title>疾病名称</TD>
        <TD  class= input>
        	<Input class=code name="DiseaseName" CodeData="" ondblClick="queryDisease()" onkeyup="">
        </TD>	
        <TD  class= title>医院名称</TD>
        <TD  class= input>
        	<Input class=code name="HospitalName" CodeData="" ondblClick="" onkeyup="">
        </TD>	
        <TD  class= title>地区</TD>
        <TD  class= input>
        	<Input class=code name="Area" CodeData="" ondblClick="" onkeyup="">
        </TD>	    	 	
      </tr>
	  <tr class=common>
        <TD>
        	<INPUT VALUE="查询" class="cssButton" TYPE="button" onclick="DiseaseStatisticsQuery()"/>
        </TD>
	  </tr>
    </table>

    <div id= "divRateGrid" style= "display: ''">
      <table>
        <tr>
          <td class="titleImg">
    	       报表信息列表
          </td>
        </tr>
      </table>

      <table class="common">
        <tr>
    	  <td colSpan="1">
	         <span id="spanReportInformationGrid" ></span>
		  </td>
	    </tr>
     </table>
     
     <div  id= "divPage" align=center style= "display: 'none' ">
	   <input CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
	   <input CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	   <input CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	   <input CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
	 </div>
    </div>
    <hr/>
    
    <input type=hidden id="fmtransact" name="fmtransact" > 
    <input type=hidden id="Operator" name="Operator" value='<%=Operator%>'>
  </form>
</html>