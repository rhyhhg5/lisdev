<html>
	<%
	//Name:LLRegisterInput.jsp
	//Function：个人立案界面的初始化程序
	//Date：2005-02-16 17:44:28
	//Author ：Xx
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%
 		String CurrentDate= PubFun.getCurrentDate();   
    String CurrentTime= PubFun.getCurrentTime();
  %>
	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="LLStingInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLStingInit.jsp"%>
    <script language="javascript">
      function initDate(){
        fm.OpStartTime.value="<%=CurrentTime%>";
        fm.OpStartDate.value="<%=CurrentDate%>";
      }
    </script>
	</head>
	<body  onload="initForm();initElementtype();">
		<form action="./LLStringSave.jsp" method=post name=fm target="fraSubmit">
	  <%@include file="CaseTitle.jsp"%>
			
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerSearch);">
				</td>
				<td class= titleImg>
					客户查询
				</td>
			</tr>
		</table>
		<div id="divCustomerSearch" style="display:''">
			<table  class= common>
				<TR  class= common8>
					<TD  class= title8>单位名称</TD><TD  class= input8><Input class=common name="CustomerName" ></TD>
		      <TD  class= title8>保单号码</TD><TD  class= input8><Input class=common name="ContNo"></TD>
				</tr>
				<TR  class= common8>
					<TD  class= title8>应付金额</TD><TD  class= input8><Input class=common name="ShouldGive" readonly></TD>
		      <TD  class= title8>实付金额</TD><TD  class= input8><Input class=common name="RealGive" readonly ></TD>
				</tr>
				<TR  class= common8>
					<TD  class= title8>历史拨付金额</TD><TD  class= input8><Input class=common name="HistoryGive"  readonly></TD>
		      <TD  class= title8>差额</TD><TD  class= input8><Input class=common name="Balance"  readonly></TD>
		      
				</tr>
				<TR  class= common8>
					<TD  class= title8>本次拨付金额</TD><TD  class= input8><Input class=common name="RealPay"  ></TD>
					<TD  class= title8>险种代码</TD> <TD  class= input8><Input class=codeno name="RiskCode" readonly><Input class=codename name=RiskName></TD>
				</tr>
			</table>
		</div>
		<Div  id= "RgtFinish" style= "display: ''" align= right>
		<input class=cssButton style='width:80px;' type=button  value="拨付确认" onclick="RgtFinish()">
		<input class=cssButton style='width:80px;' type=button  value="拨付审核" onclick="RgtUw()">
    	<input class=cssButton style='width:80px;' type=button  value="返  回" onclick="top.close()">
    
    <Div  id= "divRgtFinish" style= "display: 'none'" align= right>
      <hr>
    	<input class=cssButton style='width:80px;' type=button  value="团体申请完毕" onclick="RgtFinish()">
    	<input class=cssButton style='width:80px;' type=button  value="返  回" onclick="top.close()">
    </Div>
<hr>
<Div align="left">
        <table >
          <tr >
            <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divAccountList);">
            </td>
            <td class= titleImg> 拨付信息</td>
          </tr>
        </table>
      </Div>
<Div  id= "divAccountList" align = center style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanAccountGrid" >
              </span>
            </TD>
          </TR>
        </table>
        <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
        <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();">

      </Div>
	    
	  <span id="spanCode"  style="display: none; position:absolute; slategray">
	  </span>
  </form>


  </body>
</html>