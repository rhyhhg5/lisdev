//              该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//更加选中的OperationTypeList进行初始化
function afterCodeSelect( cName, Filed)
{
  if(cName=='OperationTypeList')
  {
    displayOperationInfo();
  }
}
//displayOperationInfo()显示手术信息的函数
function displayOperationInfo()
{
  if(fm.OperationType.value=="")
  {
    fm.all('divFeeOperation').style.display="none";
    fm.all('divDegreeOperation').style.display="none";
  }
  if(fm.OperationType.value!="")
  {
    if(fm.OperationType.value=="0")
    {
      //显示按费用给付的手术信息
      fm.all('divFeeOperation').style.display="";
      fm.all('divDegreeOperation').style.display="none";
    }
    if(fm.OperationType.value=="1")
    {
      //只显示按档次进行赔付的手术信息
      fm.all('divFeeOperation').style.display="none";
      fm.all('divDegreeOperation').style.display="";
    }
  }
}


//提交，保存按钮对应操作
function submitForm()
{
 // alert("");
  if(fm.OperationType.value=="0和1")
  {
    alert("请您从新选中手术赔付种类！！！");
    return;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  showSubmitFrame(mDebug);
  fm.action="./ICaseCureSave.jsp";
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function showCaseCureInfo()
{
  fm.action = './ShowCaseCure.jsp';
  fm.submit();
}