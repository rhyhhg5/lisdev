<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLSubReportSave.jsp
//程序功能：
//创建日期：2005-01-12 09:36:37
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLAskRelaSchema tLLAskRelaSchema   = new LLAskRelaSchema(); //咨询关联事件
  LLNoticeRelaSchema tLLNoticeRelaSchema   = new LLNoticeRelaSchema();
  LLSubReportSchema tLLSubReportSchema   = new LLSubReportSchema();
  LLAskRelaSet tLLAskRelaSet = null;
  LLNoticeRelaSet tLLNoticeRelaSet = null;
  OLLSubReportUI tOLLSubReportUI   = new OLLSubReportUI();
  TransferData tTransferData = new TransferData();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  int j=0;
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
   System.out.println("**********"+transact);
   String Path = application.getRealPath("config//Conversion.config");
    tLLSubReportSchema.setSubRptNo(request.getParameter("SubRptNo"));
    String askType = request.getParameter("AskType");
    
    String ConsultNo = request.getParameter("ConsultNo");
    String NoticeNo = request.getParameter("NoticeNo");
    String CNNo = request.getParameter("CNNo");
    System.out.println("wowzhizheliqutonghzihao"+NoticeNo);
  
  if (  transact.equals("INSERT||MAIN"))
  {
  	tLLSubReportSchema.setSubRptNo("");
    tLLSubReportSchema.setCustomerNo(request.getParameter("LogerNo"));
    tLLSubReportSchema.setCustomerName(request.getParameter("CustomerName"));
    tLLSubReportSchema.setCustSituation(request.getParameter("CustSituation"));
    tLLSubReportSchema.setAccSubject(request.getParameter("AccSubject"));
    tLLSubReportSchema.setAccidentType(request.getParameter("AccidentType"));
    tLLSubReportSchema.setAccDate(request.getParameter("AccDate"));
    tLLSubReportSchema.setAccEndDate(request.getParameter("AccEndDate"));
    tLLSubReportSchema.setAccDesc(StrTool.Conversion(request.getParameter("AccDesc"),Path));
    tLLSubReportSchema.setAccPlace(request.getParameter("AccPlace"));
    tLLSubReportSchema.setHospitalCode(request.getParameter("HospitalCode"));
    tLLSubReportSchema.setHospitalName(request.getParameter("HospitalName"));
    tLLSubReportSchema.setInHospitalDate(request.getParameter("InHospitalDate"));
    tLLSubReportSchema.setOutHospitalDate(request.getParameter("OutHospitalDate"));
    tLLSubReportSchema.setSeriousGrade(request.getParameter("SeriousGrade"));
    //用于事件关联,默认添加事件自动关联
    if("0".equals( askType))
    {
    	tLLAskRelaSchema.setSubRptNo("");
    	tLLAskRelaSchema.setConsultNo(request.getParameter("ConsultNo"));
    }
    if("1".equals( askType))
    {
    	tLLNoticeRelaSchema.setSubRptNo("");
    	tLLNoticeRelaSchema.setNoticeNo(request.getParameter("NoticeNo"));
    }
    if("2".equals( askType))
    {
    	tLLAskRelaSchema.setSubRptNo("");
    	tLLAskRelaSchema.setConsultNo(request.getParameter("CNNo"));
    	tLLNoticeRelaSchema.setSubRptNo("");
    	tLLNoticeRelaSchema.setNoticeNo(request.getParameter("CNNo"));
    }

 }
 
 if ( transact.equals("DELETE||RELA") || transact.equals("INSERT||RELA"))
 {
   	String[] tChk = request.getParameterValues("InpSubReportGridSel");
	  String[] EveNo=request.getParameterValues("SubReportGrid1");
	  int intLength =0;
	//if (ParamNo==null ) ParamNo = ConsultNo;
	tTransferData.setNameAndValue("tEventNo",request.getParameter("tEventNo"));
	System.out.println("tEventNo : "+request.getParameter("tEventNo"));
   if ("0".equals( askType))
   {
   	System.out.println("咨询");
    if (EveNo!=null )
	  { 
	   	 intLength = EveNo.length ;
		 tLLAskRelaSet = new LLAskRelaSet(); 
		 for(int i=0;i<intLength;i++)
		 {
		   
		    if(tChk[i].equals("0")) //未选
		      continue;
		
		 tLLAskRelaSchema.setConsultNo(ConsultNo);
		 tLLAskRelaSchema.setSubRptNo(EveNo[i]);
		 tLLAskRelaSet.add(tLLAskRelaSchema);
		  }
	 }
                                   
   }
   if ("1".equals( askType))
   {
   	System.out.println("通知");
     if (EveNo!=null )
	  { 
	   intLength = EveNo.length ;
		 tLLNoticeRelaSet = new LLNoticeRelaSet(); 
		 for(int m=0;m<intLength;m++)
		 {
		  
		    System.out.println("值是"+tChk[m]);
		    if(tChk[m].equals("0")) //checkbox 未选
		      continue;
		 
		 tLLNoticeRelaSchema.setNoticeNo(NoticeNo);
		 tLLNoticeRelaSchema.setSubRptNo(EveNo[m]);
		 tLLNoticeRelaSet.add(tLLNoticeRelaSchema);
		  }
	 }
	 
   }
   
   if("2".equals(askType))
   { 
   	System.out.println("咨询/通知");
    if (EveNo!=null )
	  { 
	   System.out.println("jinlaile");
	   intLength = EveNo.length ;
	   System.out.println(intLength);
		 tLLAskRelaSet = new LLAskRelaSet(); 
		 tLLNoticeRelaSet = new LLNoticeRelaSet();
		 for(int n=0;n<intLength;n++)
		 {
		    if(tChk[n].equals("0")) //未选
		      continue;
		System.out.println(CNNo);
		 tLLAskRelaSchema.setConsultNo(CNNo);
		 System.out.println(CNNo);
		 tLLAskRelaSchema.setSubRptNo(EveNo[n]);
		 System.out.println(EveNo[n]);
		 tLLAskRelaSet.add(tLLAskRelaSchema);
		 
		 tLLNoticeRelaSchema.setNoticeNo(CNNo);
		 System.out.println(CNNo);
		 tLLNoticeRelaSchema.setSubRptNo(EveNo[n]);
		 System.out.println(CNNo);
		 tLLNoticeRelaSet.add(tLLNoticeRelaSchema); 


		  }
		  System.out.println("结束传送");
     }
   } 
  
 }
 
    

        //工作流标记
  		TransferData mf = new TransferData();
  		mf.setNameAndValue("WFFLAG","0");
  		
  		//咨询号,通知号，咨询通知号
  		mf.setNameAndValue("AskType",askType);
  		mf.setNameAndValue("ConsultNo",request.getParameter("ConsultNo"));
  		mf.setNameAndValue("NoticeNo",request.getParameter("NoticeNo"));
      mf.setNameAndValue("CNNo",request.getParameter("CNNo"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLLSubReportSchema);
  	tVData.add(tG);
  	tVData.add(mf);
  	tVData.add(tTransferData);
  	tVData.add(tLLAskRelaSet);
  	tVData.add(tLLNoticeRelaSet);
    
    tOLLSubReportUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLLSubReportUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("transact:" + transact);
  String EventNo ="";
  LLSubReportSchema  rLLSubReportSchema = null;
  if ( FlagStr.equals("Success"))
  {
  
    VData res = tOLLSubReportUI.getResult();
    if ( res!=null &&  transact.equals("INSERT||MAIN"))
    {
    	System.out.println("res not null");
    	System.out.println( res.size() );
    	rLLSubReportSchema=(LLSubReportSchema)res.getObjectByObjectName("LLSubReportSchema",0);
       if (	rLLSubReportSchema!=null ){
       System.out.println("rLLSubReportSchema not null");
	    EventNo= rLLSubReportSchema.getSubRptNo();
	    System.out.println(EventNo+"    ********************");
	    }
    }
  }
  
  
  //添加各种预处理
%>                      

<html>
<script language="javascript">
	//parent.fraInterface.fm.SubRptNo.value = "<%=EventNo%>" ;
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	parent.fraInterface.updateEventGrid("<%=EventNo%>");
</script>
</html>
