<%
//程序名称：LLClaimFiledSub.jsp
//程序功能：理赔案卷存档清单
//创建日期：2006-03-06
//创建人  ：yanchao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
ReportUI
</title>
<head>
</head>
<body>
<%


String flag = "0";
String FlagStr = "";
String Content = "";
String com = " ";

//设置模板名称

String FileName ="LLClaimFiled";
GlobalInput tG = new GlobalInput();
tG = (GlobalInput)session.getValue("GI");
	
	String operator=tG.Operator;

String StartDate = request.getParameter("StartDate");
System.out.println(StartDate);

String EndDate = request.getParameter("EndDate");
System.out.println(EndDate);


String organcode = request.getParameter("organcode");
System.out.println(organcode);

String organname = request.getParameter("organname");
System.out.println(organname);


String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");
System.out.println("@理赔案卷存档清单统计报表@");
System.out.println("Operate:"+operator);

if(sd.compareTo(ed) > 0)
{
flag = "1";
FlagStr = "Fail";
Content = "操作失败，原因是:统计止期比统计统计起期早";
}


		
if(!(organcode==null||organcode.equals(""))){
			String  tManage="";
			if(organcode.length()>=4){
			  tManage = organcode.substring(0,4)+ "0000" ;
			  System.out.println("tManage="+tManage);
			  System.out.println("organcode="+organcode);
			}else{
			  tManage=organcode+ "0000" ;
			}
			
			if(organcode.equals(tManage)){
				if(organcode.equals("86000000")){
				   com="and a.MngCom like '86%' ";
				}else{
	 		     com="and a.MngCom ='"+tManage+"'";
	 		  }
	 	  }else{
	 		  com="and a.MngCom like '"+organcode+"%'";
	 		}
}
System.out.println(com);	


JRptList t_Rpt = new JRptList();
String tOutFileName = "";
String strVFPathName="";
if(flag.equals("0"))
{
t_Rpt.m_NeedProcess = false;
t_Rpt.m_Need_Preview = false;
t_Rpt.mNeedExcel = true;

StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");

String CurrentDate = PubFun.getCurrentDate();
CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");

t_Rpt.AddVar("StartDate", StartDate);
t_Rpt.AddVar("EndDate", EndDate);
t_Rpt.AddVar("com", com);



String YYMMDD = "";
YYMMDD = StartDate.substring(0, StartDate.indexOf("-")) + "年"
       + StartDate.substring(StartDate.indexOf("-") + 1,StartDate.lastIndexOf("-")) + "月"
       + StartDate.substring(StartDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("StartDateN", YYMMDD);
YYMMDD = "";
YYMMDD = EndDate.substring(0, EndDate.indexOf("-")) + "年"
       + EndDate.substring(EndDate.indexOf("-") + 1,EndDate.lastIndexOf("-")) + "月"
       + EndDate.substring(EndDate.lastIndexOf("-") + 1) + "日";
t_Rpt.AddVar("EndDateN", YYMMDD);


t_Rpt.Prt_RptList(pageContext,FileName);
tOutFileName = t_Rpt.mOutWebReportURL;
String strVFFileName = FileName+tOutFileName.substring(tOutFileName.indexOf("_"));
 String strRealPath = application.getRealPath("/web/Generated").replace('\\','/');
 strVFPathName =strRealPath +"/"+ strVFFileName;
System.out.println(strVFPathName);
System.out.println("=======Finshed in JSP===========");
System.out.println(tOutFileName);
}
%>
<form name="fm"  method="post"  action="../web/ShowF1Report.jsp" >
<input name="FileName" type="hidden" value="<%=tOutFileName%>">
<input name="RealPath" type="hidden" value="<%=strVFPathName%>">
</form>

</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;

if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >
