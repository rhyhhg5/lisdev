<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLCaseSave.jsp
//程序功能：
//创建日期：2005-02-23 11:53:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//接收信息，并作校验处理。
//输入参数
LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
String BatchCaseNo = "";
BatchClaimConfBL tBatchClaimConfBL = new BatchClaimConfBL();
//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
String transact = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
transact = request.getParameter("fmtransact");
tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
//String tCaseNo[] = request.getParameterValues("GrpCaseGrid1");
//if (tCaseNo!=null )
//{
//  for(int i=0;i<tCaseNo.length;i++) {
//    BatchCaseNo += "'"+tCaseNo[i]+"'," ;
//  }
//}
//BatchCaseNo = request.getParameter("BatchCaseNo");
tLLRegisterSchema.setRemark(BatchCaseNo);

 String tRNum[] = request.getParameterValues("InpDiskErrQueryGridChk");
 String tCaseNo[] = request.getParameterValues("DiskErrQueryGrid1");
 LLCaseSet tLLCaseSet = new LLCaseSet();
 for(int index=0;index<tRNum.length;index++) {
   if(tRNum[index].equals("1")) {
     LLCaseSchema tLLCaseSchema = new LLCaseSchema();
     tLLCaseSchema.setCaseNo(tCaseNo[index]);
     tLLCaseSet.add(tLLCaseSchema);
   }
 }


try
{
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tLLCaseSet);
  tVData.add(tLLRegisterSchema);
  tVData.add(tG);
  tBatchClaimConfBL.submitData(tVData,"DELETE||MAIN");
}
catch(Exception ex)
{
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}

//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
  tError = tBatchClaimConfBL.mErrors;
  if (!tError.needDealError())
  {
    Content = " 保存成功! ";
    FlagStr = "Success";
  }
  else
  {
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

//添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
