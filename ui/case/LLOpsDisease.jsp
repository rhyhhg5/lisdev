<html>
	<%
	//Name:LLOpsDisease.jsp
	//Function：手术疾病合理性知识库维护界面
	//Date：2016年1月19日16:55:43
	//Author ：Rs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%
 	String CurrentDate= PubFun.getCurrentDate();   
    String CurrentTime= PubFun.getCurrentTime();
  %>
	<head >
		<SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>	
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLOpsDiseaseInit.jsp"%>
		<SCRIPT src="LLOpsDisease.js"></SCRIPT>
	</head>
	<body onload="initForm();" >
	
		<form action="LLOpsDiseaseSave.jsp" method=post name=fm target="fraSubmit">
		<table>
    	<tr>
    		<td>
    		  <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGGroup1);">
    		</td>
        <td class= titleImg>
          请输入查询信息
        </td>   		 
    	</tr>
    </table>
    <Div  id= "divLGGroup1" style= "display: ''">
		<table  class= common align='center' >
      <TR  class= common>
        <TD  class= title>
          手术代码
        </TD>
        <TD  class= input>
  <Input class="codeNo"  name=ICDOPSCode  elementtype=nacessary  verify="手术代码|notnull"  style=" width:160px; " ondblclick="return showCodeList('lloperation',[this,ICDOPSName],[1,0],null,fm.ICDOPSName.value,'Icdopsname',1,240);" onkeyup="return showCodeListKeyEx('lloperation',[this,ICDOPSName],[1,0],null,fm.ICDOPSName.value,'Icdopsname',1,240);" /> 
        </TD>
        <TD  class= title>
          手术名称
        </TD>
        <TD  class= input>
        <Input class=common name=ICDOPSName  elementtype=nacessary verify="手术名称|notnull"  > 
        </TD>
      </TR>
        <TR  class= common>
        <TD  class= title>
          疾病代码
        </TD>
        <TD  class= input>
          <Input class="codeNo"  name=ICDCode elementtype=nacessary  verify="疾病代码|notnull"  style=" width:160px; " ondblclick="return showCodeList('lldiseas',[this,ICDName],[1,0],null,fm.ICDName.value,'ICDName',1,240);" onkeyup="return showCodeListKeyEx('lldiseas',[this,ICDName],[1,0],null,fm.ICDName.value,'ICDName',1,240);" /> 
        </TD>
        <TD  class= title>
          疾病名称
        </TD>
        <TD  class= input>
          <Input class= 'common' name=ICDName elementtype="nacessary" verify="小组名称|notnul" >
        </TD>
		</TR>
		</table>	
			</Div>
	    <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="queryClick();"> 
    	<INPUT VALUE="新 增" class= cssButton TYPE=button onclick="submitClick();">  
    	<INPUT VALUE="删 除" class= cssButton TYPE=button onclick="deleteClick();"> 
	<hr></hr><br/>
			<table>
    	<tr>
    		<td>
    		  <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGGroup1);">
    		</td>
        <td class= titleImg>
          查询信息
        </td>   		 
    	</tr>
    </table>
	
	     <table  class= common>
        <tr>
    	  	<td text-align: left colSpan=1>
	         <span id="spanLLOpsDiseaseGrid" ></span>
		</td>
	</tr>
     </table>
   <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="SerialNo" name="SerialNo">
    <span id="spanCode"  style="display: none; position:absolute; slategray">
	  </span>
		</form>
	
  </body>
</html>