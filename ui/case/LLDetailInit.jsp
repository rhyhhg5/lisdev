<%
//Name：LLDetailInit.jsp
//Function：
//Date：2010-08-19 17:44:28
//Author  ：kedy
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%GlobalInput mG = new GlobalInput();
  mG=(GlobalInput)session.getValue("GI");
  String tCustomerNo = request.getParameter("InsuredNo");
%>
<script language="JavaScript">
function initForm()
{
  try
  { 
    initInpBox();
    initSelBox();   
    initDiseaseGrid();
    initDeformityGrid();
    initSeriousDiseaseGrid();
    initDegreeOperationGrid();
    initAccidentGrid();
    initDisabilityGrid();
    initQuery(); 	
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
// 下拉框的初始化
function initSelBox()
{  
  try                 
  {   
  	var sql="select name ,codename('sex',sex) from ldperson where customerno='"+fm.CustomerNo.value+"' ";
  	var arrResult = easyExecSql(sql );
  	if(arrResult)
  	{
  		fm.CustomerName.value = arrResult[0][0];
 		fm.Sex.value = arrResult[0][1];
 	}
  }
  catch(ex)
  {
    alert("在LLClaimProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
 function initInpBox()
 {
    try {     
     fm.CustomerNo.value = '<%=tCustomerNo%>';
    }
    catch(ex) {
    alert("在LLRegisterCheckInit.jsp-->initInpBox函数中发生异常:初始化界面错误!"+ex.message);
   }
 }
 
//初始化疾病信息列表
 function initDiseaseGrid(){
   var iArray = new Array();
   try{
     iArray[0]=new Array("序号","30px","10","3");
     iArray[1]=new Array("临床诊断","100px","100",1);
     iArray[2]=new Array("疾病","80px","100",3);
     iArray[3]=new Array("疾病名称","120px","100",1);

     iArray[4]=new Array("疾病代码","80px","100",2);
     iArray[4][4]="lldiseas"
     iArray[4][5]="3|4";
     iArray[4][6]="0|1";
     iArray[4][15]="ICDName"
     iArray[4][17]="3"
     iArray[4][19]="1";

     iArray[5]=new Array("诊治医院","105px","100",2,"llhospiquery","5|6","0|1");
     iArray[5][15]="Hospitname";
     iArray[5][17]="6";
     iArray[5][19]=1;
     iArray[6]=new Array("诊治医院名称","140px","100",1);

     iArray[7]=new Array("治疗医生","105px","100",1);
     iArray[8]=new Array("序号","300px","0",3);
     iArray[9]=new Array("治疗医生代码","120px","100",3);

     iArray[10]=new Array("手术","50px","30",2);
     iArray[10][10]="IsOps";
     iArray[10][11]="0|^1|有|^0|无"
     iArray[10][12]="10|11"
     iArray[10][13]="1|0"
     iArray[11] = new Array("有无手术","60px","10","3")

     DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" );
     DiseaseGrid.mulLineCount = 0;
     DiseaseGrid.displayTitle = 1;
     DiseaseGrid.addEventFuncName="setHosInfo";
     DiseaseGrid.loadMulLine(iArray);
   }
   catch(ex){
     alert("在DiseaseGrid中出错"+ex.message);
   }
 }
 
 //初始化重疾列表
 function initSeriousDiseaseGrid(){
   var iArray = new Array();
   try{
     iArray[0]=new Array("序号","30px","10","3");
     iArray[1]=new Array("重疾名称","120px","100","1");
     iArray[2]=new Array("重疾代码","60px","160","2");
     iArray[2][4]="llseriousdiseas";
     iArray[2][5]="2|1";
     iArray[2][6]="0|1";
     iArray[2][9]="疾病代码|NOTNULL";
     iArray[2][15]="name"
     iArray[2][17]="1"
     iArray[2][19]="1";

     iArray[3]=new Array("诊治医院名称","120px","100",3,"hospital","3|4","1|0");
     iArray[4]=new Array("诊治医院代码","100px","100",3);
     iArray[5]=new Array("治疗医生","60px","100",1);
     iArray[6]=new Array("重疾责任依据编码","300px","0",3);
     iArray[7]=new Array("重症监护天数","80px","4",3);
     iArray[8]=new Array("重疾责任标志","20px","100",3);
     iArray[9]=new Array("重疾责任依据","320px","100",0);
     iArray[9][7]="OpenProof";
     iArray[10]=new Array("重疾责任","50","100",0);
     iArray[11]=new Array("确诊时间","100px","100",1);

     SeriousDiseaseGrid = new MulLineEnter( "fm" , "SeriousDiseaseGrid" );
     SeriousDiseaseGrid.mulLineCount = 0;
     SeriousDiseaseGrid.displayTitle = 1;
     SeriousDiseaseGrid.loadMulLine(iArray);
   }
   catch(ex){
     alert("在SeriousDiseaseGrid中出错"+ex.message);
   }
 }
 
  //初始化手术列表信息
 function initDegreeOperationGrid()
 {
   var iArray = new Array();
   try
   {
     iArray[0]=new Array("序号","30px","10","3");
     iArray[1]=new Array("手术名称","200px","100","1");

     iArray[2]=new Array("手术代码","150px","100","2");
     iArray[2][4]="lloperation";
     iArray[2][5]="2|1|4";
     iArray[2][6]="1|0|2";
     iArray[2][15]="Icdopsname";
     iArray[2][17]="1";
     iArray[2][19]=1

     iArray[3]=new Array("手术费用","90px","100","3");
     iArray[4]=new Array("手术等级","180px","100","1");
     iArray[5]=new Array("序号","300px","0",3);

     DegreeOperationGrid = new MulLineEnter( "fm" , "DegreeOperationGrid" );
     DegreeOperationGrid.mulLineCount = 0;
     DegreeOperationGrid.displayTitle = 1;
     DegreeOperationGrid.canSel = 0;
     DegreeOperationGrid.loadMulLine(iArray);
   }
   catch(ex){
     alert("在DegreeOperation中出错"+ex.message);
   }
 }
 
 //初始化意外信息列表
 function initAccidentGrid(){
   var iArray = new Array();
   try{
     iArray[0]=new Array("序号","30px","10",3);
     iArray[1]=new Array("意外序号","100px","10",3);
     iArray[2]=new Array("意外类型","100px","100",3);
     iArray[3]=new Array("意外类型名称","150px","100",3);
     iArray[4]=new Array("意外查询","80px","100",3);
     iArray[5]=new Array("意外名称","300px","100",1);

     iArray[6]=new Array("意外代码","90px","100",2);
     iArray[6][4]="llacci"
     iArray[6][5]="6|5";
     iArray[6][6]="0|1";
     iArray[6][15]="accname";
     iArray[6][17]="5";
     iArray[6][19]=1;

     iArray[7]=new Array("意外原因代码","80px","100",3);
     iArray[8]=new Array("意外原因名称","180px","100",3);
     iArray[9]=new Array("备注","155px","100",1);

     AccidentGrid = new MulLineEnter( "fm" , "AccidentGrid" );
     AccidentGrid.mulLineCount = 0;
     AccidentGrid.displayTitle = 1;
     AccidentGrid.canSel = 0;
     AccidentGrid.loadMulLine(iArray);
   }
   catch(ex){
     alert("在Accident中出错"+ex.message);
   }
 }
  //初始化残疾信息列表
 function initDeformityGrid(){
   var iArray = new Array();
   try{
     iArray[0]=new Array("序号","30px","10","3");
     iArray[1]=new Array("残疾名称","200px","100",1);

     iArray[2]=new Array("残疾代码","80px","100",2,"desc_wound","2|1|3|4","0|1|3|4");
     iArray[2][19]=1;

     iArray[3]=new Array("残疾级别","80px","100",0);
     iArray[4]=new Array("给付比例","80px","100",0);
     iArray[5]=new Array("确认依据","120px","100",3);
     iArray[6]=new Array("序号","300px","0",3);
     iArray[7]=new Array("鉴定日期","100px","100",1);
     iArray[8]=new Array("合法认证机构","110px","20",3);
     iArray[9]=new Array("合法认证机构代码","300px","0",3);

     DeformityGrid = new MulLineEnter( "fm" , "DeformityGrid" );
     DeformityGrid.mulLineCount = 0;
     DeformityGrid.displayTitle = 1;
     DeformityGrid.loadMulLine(iArray);
   }
   catch(ex){
     alert("在DeformityGrid中出错"+ex.message);
   }
 }
 //失能表
 function initDisabilityGrid(){
   var iArray = new Array();
   try{
     iArray[0]=new Array("序号","30px",10,0);
     iArray[1]=new Array("是否丧失日常生活能力","50px",10,2);
     iArray[1][10]="Type";
     iArray[1][11]="0|^0|否|^1|是"
     iArray[1][12]="1|2"
     iArray[1][13]="1|0"
     
     iArray[2]=new Array("code","30px",10,3);
     iArray[2][21]="DisabilityFlag";
     
     iArray[3]=new Array("失能日期","30px",10,1);
     iArray[3][9]="失能日期|notnull&date"
     iArray[3][21]="DisabilityDate";
     
     iArray[4]=new Array("观察期结束日期","30px",10,1);
     iArray[4][7]="getObservationDate";
     iArray[4][9]="观察期结束日期|notnull&date"
     iArray[4][21]="ObservationDate";
     
     
     DisabilityGrid = new MulLineEnter( "fm" , "DisabilityGrid" );
     DisabilityGrid.mulLineCount = 0;
     //DisabilityGrid.hiddenPlus=0;
     //DisabilityGrid.hiddenSubtraction=0;
     DisabilityGrid.loadMulLine(iArray);
   }
   catch(ex){
     alert("在DisabilityGrid中出错"+ex.message);
   }
 }
 
 
</script>