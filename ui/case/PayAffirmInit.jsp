<%
//程序名称：PayAffirmInit.jsp
//程序功能：给付确认的初始化
//创建日期：
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
%>                            

<script language="JavaScript">
function initForm()
{
  try
  {
		initClaimPolGrid();
		initBnfGrid();
  }
  catch(re)
  {
    alert("ProposalInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initClaimPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         			//列名
      iArray[1][1]="120px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[2]=new Array();
      iArray[2][0]="险种";         			//列名
      iArray[2][1]="60px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
  
      iArray[3]=new Array("给付责任","80px","0",0);
	   iArray[4]=new Array("核算赔付金额","60px","0",0);
	   iArray[5]=new Array("实际赔付金额","60px","0",0);
	  
  
      ClaimPolGrid = new MulLineEnter( "fm" , "ClaimPolGrid" ); 
      //这些属性必须在loadMulLine前
      ClaimPolGrid.mulLineCount = 1;   
      ClaimPolGrid.displayTitle = 1;
      ClaimPolGrid.hiddenPlus=1;  
	    ClaimPolGrid.hiddenSubtraction=1; 
      ClaimPolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initBnfGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("客户名称","100px","0",1);
   iArray[2]=new Array("与被保险人关系","80px","0",2,"Relaction");
   iArray[3]=new Array("受益人顺序","60px","0",1);
   iArray[4]=new Array("受益份额","60px","0",1);
   iArray[5]=new Array("领款方式","80px","0",2);
   iArray[6]=new Array("银行代码","80px","0",2);
   iArray[7]=new Array("账号","100px","0",1);
   iArray[8]=new Array("户名","100px","0",1);
   iArray[9]=new Array("赔付金额","80px","0",1);


    BnfGrid = new MulLineEnter("fm","BnfGrid");
    BnfGrid.mulLineCount = 1;
    BnfGrid.displayTitle = 1;
    BnfGrid.locked = 0;
    BnfGrid.canChk =1;
    BnfGrid.hiddenPlus=1;  
    BnfGrid.hiddenSubtraction=1; 
 //   BnfGrid. selBoxEventFuncName = "onSelSelected";
    BnfGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}


</script>