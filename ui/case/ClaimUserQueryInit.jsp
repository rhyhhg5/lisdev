<%
//Name    ：ClaimUserQueryInit.jsp
//Function：理赔人查询初始化程序
//Author  ：LiuYansong
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script language="JavaScript">
  //单击时查询
  function RegisterDetailClick(cObj)
  {
    var ex,ey;
    ex = window.event.clientX+document.body.scrollLeft;
    ey = window.event.clientY+document.body.scrollTop;
    divDetailInfo.style.left=ex;
    divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
  }

  // 输入框的初始化（单记录部分）
  function initInpBox()
  {
    try
    {
    }
    catch(ex)
    {
      alert("在RegisterQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
  }

  // 下拉框的初始化
  function initSelBox()
  {
    try
    {
    }
    catch(ex)
    {
      alert("在RegisterQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm()
  {
    try
    {
      initInpBox();
      initSelBox();
      initRegisterGrid();
    }
    catch(re)
    {
      alert("RegisterQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
  }

  // 报案信息列表的初始化
  function initRegisterGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";
      iArray[0][1]="30px";
      iArray[0][2]=10;
      iArray[0][3]=0;

      iArray[1]=new Array();
      iArray[1][0]="用户编码";
      iArray[1][1]="40px";
      iArray[1][2]=40;
      iArray[1][3]=0;

      iArray[2]=new Array();
      iArray[2][0]="用户姓名";
      iArray[2][1]="60px";
      iArray[2][2]=60;
      iArray[2][3]=0;

      iArray[3]=new Array();
      iArray[3][0]="机构编码";
      iArray[3][1]="60px";
      iArray[3][2]=60;
      iArray[3][3]=0;

      iArray[4]=new Array();
      iArray[4][0]="上级用户";
      iArray[4][1]="60px";
      iArray[4][2]=60;
      iArray[4][3]=0;

      iArray[5]=new Array();
      iArray[5][0]="核赔权限";
      iArray[5][1]="60px";
      iArray[5][2]=60;
      iArray[5][3]=0;

      RegisterGrid = new MulLineEnter( "fm" , "RegisterGrid" );
      RegisterGrid.mulLineCount = 10;
      RegisterGrid.displayTitle = 1;
      RegisterGrid.canSel=1;
      RegisterGrid.locked = 0;
      RegisterGrid.loadMulLine(iArray);
      RegisterGrid.hiddenPlus=1;
      RegisterGrid.hiddenSubtraction=1;
      RegisterGrid.detailInfo="单击显示详细信息";
      RegisterGrid.detailClick=RegisterDetailClick;
    }
    catch(ex)
    {
      alert(ex);
    }
  }
</script>