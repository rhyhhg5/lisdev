<% 
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称:NewClaimUnderWriteInput.jsp
//程序功能：
//创建日期：2002-11-20
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./NewClaimUnderwriteInput.js"></SCRIPT> 
   

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./NewClaimUnderwriteInit.jsp"%>
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
</head>

<body  onload="initForm();" >
	<form action="./NewClaimQueryOutUW.jsp" method=post name=fm1 target="fraSubmit">    
  	<TABLE class=common>
    	<TR  class= common> 
      	<TD  class= input width="26%"> 
        	<Input class= common type=Button value="赔案查询" onclick="submitFormFA()">
      	</TD>
    	</TR>
  	</TABLE> 
 	</form>
  
  <form action="./ClaimUnderwriteSave.jsp" method=post name=fm target="fraSubmit">
  	<table>
      <tr>
      	<td>
        	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
      	</td>
      	<td class= titleImg> 赔案信息 </td>
    	</tr>
    </table>
    <Div  id= "divLCPol1" style= "display: ''">
      
    <table  class= common>
      
      <TR  class= common> 
        <TD  class= title width="200">
        	 赔案号
        </TD>
        <TD  class= input width="200"> 
          <Input class= common name=ClmNo >
        </TD>
      </TR>
      
      <TR  class= common>
       	<TD  class= title width="200" >
       		赔案状态
       	</TD>
        <TD  class= input width="200" > 
          <input class=common name=ClmState >
        </TD>
      </TR>
      
      <TR  class= common>
      	<TD  class= title width="200">
      		 给付通知书号码
      	</TD>
        <TD  class= input width="200"> 
          <Input class= common name=GetNoticeNo >
        </TD>
      </TR> 

      <TR  class= common> 
      	<TD  class= input > 
        	<Input class= common type=Button value="给付确认" onclick="submitFormGF()">
      	</TD>
    	</TR>
    	
    	<TR  class= common> 
      	<TD  class= input > 
        	<Input class= common type=Button value="理赔批单打印" onclick="submitFormLPPDDY()">
      	</TD>
    	</TR>
    	
    	<TR  class= common> 
      	<TD  class= input width="26%"> 
        	<Input class= common type=Button value="二次核保通知书打印" onclick="submitFormTZSDY()">
      	</TD>
    	</TR>

    </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>