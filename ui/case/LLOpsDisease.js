/** 
 * 程序名称：LGGroupInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-02-22 17:32:49
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
// 查询按钮
function queryClick() {
	var sql = "select ICDOPSCode, ICDOPSName, ICDCode, ICDName, SerialNo  from LLOpsDisease  where 1=1"
		if(fm.ICDOPSCode.value !=null && fm.ICDOPSCode.value !=""){
			sql=sql+" and  ICDOPSCode like '%"+fm.ICDOPSCode.value+"%'";
		}
	if(fm.ICDOPSName.value !=null && fm.ICDOPSName.value !=""){
		sql=sql+" and  ICDOPSName like '%"+fm.ICDOPSName.value+"%'";
	}
	if(fm.ICDCode.value !=null && fm.ICDCode.value !=""){
		sql=sql+" and  ICDCode like '%"+fm.ICDCode.value+"%'";
	}
	if(fm.ICDName.value !=null && fm.ICDName.value !=""){
		sql=sql+" and  ICDName like '%"+fm.ICDName.value+"%'";
	}

	turnPage.pageLineNum = 5;			  
  turnPage.queryModal(sql, LLOpsDiseaseGrid);
}  		      
  
//提交，保存按钮对应操作
function submitClick()
{
	if((fm.ICDOPSCode.value == "" || fm.ICDOPSCode.value == null ) ){
		alert("手术代码不能为空");
		return false;
	}
	
	if((fm.ICDOPSName.value == "" || fm.ICDOPSName.value == null ) ){
		alert("手术名称不能为空");
		return false;
	}

	if((fm.ICDCode.value == "" || fm.ICDCode.value == null ) ){
		alert("疾病代码不能为空");
		return false;
	}

	if((fm.ICDName.value == "" || fm.ICDName.value == null ) ){
		alert("疾病名称不能为空");
		return false;
	}

	if (confirm("您确定增加该记录吗?"))
	{

		fm.fmtransact.value = "INSERT||MAIN" ;
		fm.submit(); //提交
		alert("增加成功！")
		initForm();
	}
	else
	{
		alert("您取消了增加操作！");
	}

}

function deleteClick()
{
	fm.all("SerialNo").value = LLOpsDiseaseGrid.getRowColData(LLOpsDiseaseGrid.getSelNo()-1, 5);
	if(LLOpsDiseaseGrid.getSelNo()==0){
		alert("没有选中的行！");
		return false;
		
	}

	//下面增加相应的删除代码
	if (confirm("您确实想删除该记录吗?"))
	{
		
		//showSubmitFrame(mDebug);
		fm.fmtransact.value = "DELETE||MAIN";
		fm.submit(); //提交
		alert("删除成功！");
		queryClick(); 
	}
	else
	{
		alert("您取消了删除操作！");
	}
}      
function afterSubmit(FlagStr, content) {
	window.focus();
showInfo.close();	
   
var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px"); 
	top.window.focus();
	queryClick();  
}


