// 该文件中包含客户端(协议退保)需要处理的函数和事件

var showInfo;
var mDebug="1";
var addrFlag="NEW"
var turnPage = new turnPageClass();//使用翻页功能，必须建立为全局变量
var hasULI = false;

//初始化页面

function edorTypeXTReturn()
{
	initForm();

}

//保存申请
function edorTypeXTSave()
{
	if (!checkRight()){
		return false;
	}
	if (!verifyInput2())
    {
     return false;
    }
	if(!checkData())
	{
	  return false;
	}
	
	//var getMoneyV1=fm.GetMoneyNum.value;
	//var getMoneyV2=fm.GetMoneyPro.value;
	//var premV=fm.Prem.value;
	//var x=mathRound(parseFloat(getMoneyV1));    
	//var y=mathRound(parseFloat(getMoneyV2)*(parseFloat(premV))/100); 
	//
	////判断数据的正确性
	//if(x > parseFloat(fm.Prem.value) * 1.1)
	//{
	//	alert("退费金额小于等于保费的110%!");
	//	fm.GetMoneyNum.focus();
	//	return;
	//}
	//
	//if(getMoneyV2 > 110)
	//{
	//	alert("退费金额比例应小于等于110％！");
	//	return;
	//}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmAction').value = "INSERT||EDORXT";
	fm.submit();
}

//查询客户的相关信息

function customerQuery()
{	
	window.open("./LCAppntQuery.html");
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.all('addrFlag').value=addrFlag;
	
	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit(); //提交
	
}

//数据提交前的校验
function checkData()
{
  var polSelectedCount = 0;
  for(var i = 0; i < PolGrid.mulLineCount; i++)
  {
    if(PolGrid.getChkNo(i))
    {
      polSelectedCount = polSelectedCount + 1;
      
      var insuredName = PolGrid.getRowColDataByName(i, "insuredName");
      var riskName = PolGrid.getRowColDataByName(i, "riskName");
      var getMoney = PolGrid.getRowColDataByName(i, "getMoney");
      var xtMoney = PolGrid.getRowColDataByName(i, "xtMoney");
      var xtProportion = PolGrid.getRowColDataByName(i, "xtProportion");
      
      if(getMoney == "")
      {
        alert("请先退保试算");
        return false;
      }
      
      if(xtMoney == "" || xtProportion == "")
      {
        alert("请先算费");
        return false;
      }
      
      xtMoney = parseFloat(xtMoney);
      if(xtMoney < 0)
      {
        alert("协议退费金额必须为正数");
        return false;
      }
      //if(xtProportion >= 1.1)
      //{
      //  alert("协议退保比例不能大于1.1");
      //  return false;
      //}
      //qulq 061205 modify 
      
      if(xtProportion == "-")
      {
        continue;
      }
      
      var xtProportionCheck = pointFour(parseFloat(xtMoney) / parseFloat(getMoney));
      var xtMoneyCheck = pointTwoNew(getMoney * parseFloat(xtProportion));
      if(xtProportion != xtProportionCheck && xtMoney != xtMoneyCheck)
      {
        alert(insuredName + " " + riskName + " 的算费结果被修改，请再次算费");
        return false;
      }
    }
  }
  if(polSelectedCount == 0)
  {
    alert("请选择险种");
    return false;
  }
  if (!verifyInput2())
  {
    return false;
  }
  if(hasULI && checkULIPrem())
  {
    return false;
  }
  
  return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content)
{
	showInfo.close();
	top.focus();
	
	if(FlagStr == "Success")
	{
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
        
        top.opener.focus();
        top.opener.getEdorItem();
        top.close();
	}
    else
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    }
    
}

//提交前的校验、计算

function afterCodeSelect( cCodeName, Field )
{
}

//提交前的校验、计算  

function beforeSubmit()
{
  //添加操作
}           
       
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";  
	}
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
}

/*********************************************************************
 *  改变退费文本框中的值
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
function changeValue1()
{    	
	/*
	if(!isNumber(this.value)){
		alert('请输入数字！');
		this.foucs();
		this.select();
		return;
	}*/
	var a=parseFloat(fm.all('GetMoneyNum').value);
	var b=parseFloat(fm.all('Prem').value);

	fm.all('GetMoneyPro').value=mathRound(a/b*100);   
}

function checknum(){
	if(!checkNumber(GetMoneyNum))
	return;
}
/*********************************************************************
 *  改变退费（按比例）文本框中的值
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
function changeValue2()
{ 
	/*   if(!isNumber(this.value)){
	alert('请输入数字！');
	this.foucs();
	this.select();
	return;
	}      
	*/
	var a=parseFloat(fm.all('GetMoneyPro').value);
	var b=parseFloat(fm.all('Prem').value);
	fm.all('GetMoneyNum').value=mathRound(a*b/100);
       	 
}

//返回主页面

function returnParent()
{
	top.opener.getEdorItem();
	top.opener.focus();
	top.close();
}

/*********************************************************************
 *  查询投保单位信息
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function getCont()
{
	var strSQL = "select * from LCCont  where ContNo = '"+ fm.ContNo.value + "'";
	var arrResult = easyExecSql(strSQL, 1, 0);
	//var strSQL1 = "select * from  lpedoritem where contno='"+fm.ContNo.value+"' and edortype='XT'";
	//var arrResult1 = easyExecSql(strSQL1,1,0);
	if (arrResult != null) 
	{
	    try {fm.all('AppntNo').value= arrResult[0][19]; } catch(ex) { }; //客户号码
	    try {fm.all('AppntName').value= arrResult[0][20]; } catch(ex) { }; //客户姓名
	    try {fm.all('SignCom').value= arrResult[0][37]; } catch(ex) { }; //签单机构
	    try {fm.all('SignDate').value= arrResult[0][38]; } catch(ex) { }; //签单日期
	    try {fm.all('CValiDate').value= arrResult[0][57]; } catch(ex) { }; //保单生效日期
	    try {fm.all('Prem').value= arrResult[0][51]; } catch(ex) { }; //总保费
	    try {fm.all('Amnt').value= arrResult[0][52]; } catch(ex) { }; //总保额
	}
	else
	{
	    alert('未查到该集体保单！');
	} 
}

function getPolInfo(tContNo)
{  
    // 书写SQL语句
    var strSQL = "  select a.polNo, a.riskSeqNo, a.riskCode, b.riskName, "
                 + "    a.insuredNo, a.insuredName, a.amnt, a.mult, a.prem, "
                 + "    a.cValiDate, a.endDate, a.payToDate "
                 + "from LCPol a, LMRiskApp b "
                 + "where a.riskCode = b.riskCode "
                 + "   and contNo = '" + tContNo + "' "
                 + "order by riskSeqNo ";
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    var arrResult = easyExecSql(strSQL, 1, 0);        
    //如果已经保存,查询已有的数据，并转换为正数
    getCont();       
    //查询成功则拆分字符串，返回二维数组    
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
    
    setField();
}

//为已知要素赋值
function setField()
{
  for(var i = 0; i < PolGrid.mulLineCount; i++)
  {
    var sql = "  select abs(getMoney) "
              + "from LPBudgetResult "
              + "where polNo = '" + PolGrid.getRowColDataByName(i, "polNo") + "' "
              + "   and edorNo = '" + fm.EdorNo.value + "' "
              + "   and edorType = '" + fm.EdorType.value + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
      PolGrid.setRowColDataByName(i, "getMoney", rs[0][0]);
    }
    
    //协议退保金
	  sql = "select abs(decimal(edorValue, 20, 2)) "
	        + "from LPEdorEspecialData "
	        + "where EdorNo = '" + fm.EdorNo.value + "' "
	        + "   and EdorType = '" + fm.EdorType.value + "' "
	        + "   and DetailType = 'XTFEE' "
	        + "   and PolNo = '" + PolGrid.getRowColDataByName(i, "polNo") + "' ";
	  rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    PolGrid.setRowColDataByName(i, "xtMoney", rs[0][0]);
	    
	    //若计算了协议退费，则选中险种
	    PolGrid.checkBoxSel(i + 1);
	  }
	  
	  //协议退保比例
	  sql = "select edorValue "
	        + "from LPEdorEspecialData "
	        + "where EdorNo = '" + fm.EdorNo.value + "' "
	        + "   and EdorType = '" + fm.EdorType.value + "' "
	        + "   and DetailType = 'XTFEERATEP' "
	        + "   and PolNo = '" + PolGrid.getRowColDataByName(i, "polNo") + "' ";
	  rs = easyExecSql(sql);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    PolGrid.setRowColDataByName(i, "xtProportion", rs[0][0]);
	  }
  }
}

//解约试算
function pEdorCalZTTest()
{
	if (!checkRight()){
		return false;
	}
	
  win = window.open("../case/LLContdealBudgetMain.jsp?contNo=" + fm.ContNo.value
                      + "&edorNo=" + fm.EdorNo.value
                      + "&edorType=" + fm.EdorType.value
                      + "&edorValiDate=" + fm.EdorValiDate.value,
                    "pEdorCalZTTest");
  win.focus();
}

//得到保全生效日期
function getEdorValiDate()
{
  var sql = "  select edorValiDate "
            + "from LPEdorItem "
            + "where edorNo = '" + fm.EdorNo.value + "' "
            + "   and edorType = '" + fm.EdorType.value + "' "
            + "   and contNo = '" + fm.ContNo.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    return result[0][0];
  }
  return "";
}

//设置退保试算结果，在退保试算页面PEdorBudget.jsp调用
function setBudgetResultToXTDetail()
{
  for(var i = 0; i < PolGrid.mulLineCount; i++)
  {
    var polNo = PolGrid.getRowColDataByName(i, "polNo");
    var sql = "  select getMoney "
              + "from LPBudgetResult "
              + "where polNo = '" + polNo + "' "
              + "   and edorNo = '" + fm.EdorNo.value + "' "
              + "   and edorType = '" + fm.EdorType.value + "' ";
    var result = easyExecSql(sql);
    if(result)
    {
      var money = Math.abs(result);
    	//退费为负，所以要转换
      PolGrid.setRowColDataByName(i, "getMoney", String(money));
    }
  }
}

//解约 算费 
function calFee()
{
	//判断权限
	if (!checkRight()){
		return false;
	}
	
  var polSelectedCount = 0;  //选择的险种
  for(var i = 0; i < PolGrid.mulLineCount; i++)
  {
    if(PolGrid.getChkNo(i))
    {
      polSelectedCount = polSelectedCount + 1;
      			
      var getMoney = PolGrid.getRowColDataByName(i, "getMoney");
      var xtMoney = PolGrid.getRowColDataByName(i, "xtMoney");
      var xtProportion = PolGrid.getRowColDataByName(i, "xtProportion");
      
      if(getMoney == "")
      {
        alert("请先进行解约试算!");
        return false;
      }
           
//      if(getMoney != "" && xtMoney != "" && xtProportion != "")
//      {
//        alert("只能填协议退费金额和协议退费比例其中一项");
//        return false;
//      }
      
      if(getMoney != "0" && xtMoney == "" && xtProportion == "")
      {
        alert("解约退费不等于0时，必须填写解约金和解约比例其中一项!");
        return false;
      }else if(getMoney=='0'&& xtMoney == ""){
      	alert("解约退费等于0时，必须填写解约金一项!");
        return false;
      }
      //解约金不能超过总保费的110%
      //得到险种总保费
      var sqlPrem = " select sum(sumactupaymoney) from ljapayperson where contno = '"
      						+	fm.all('ContNo').value
      						+ "' and polno ='"
      						+PolGrid.getRowColDataByName(i, "polNo")
      						+"'";
      var rs = easyExecSql(sqlPrem);
      //if(rs == null || rs[0][0] == "0")
      //{
      //  PolGrid.setRowColDataByName(i, "xtMoney", "0");
      //}
      //else if(xtMoney != "")
      if(xtMoney != "")
      {
        proportion = parseFloat(xtMoney) / parseFloat(rs[0][0]);
	      if(!hasULI && proportion >= 1.1)
	      {
	        alert("解约比例不能超过总保费的110%");
	        return false;
	      }
	    
	    if(getMoney == "0"){
        	PolGrid.setRowColDataByName(i, "xtProportion", "-");        	
      	}else{
      	 xtProportion = pointFour(parseFloat(xtMoney) / getMoney);
         PolGrid.setRowColDataByName(i, "xtProportion", String(xtProportion));
      	}  	   
      }
      else if(xtProportion != "")
      {
        var money = xtProportion * getMoney * 1.1;
        
      	if(!hasULI && money / rs[0][0] >= 1.1)
	      {
	        alert("解约比例不能超过总保费的110%");
	        return false;
	      }
        xtMoney = pointTwoNew(getMoney * parseFloat(xtProportion));
        //pointTwo 不是按照4舍5入来规格化 ，所以重写一个。qulq 061205 
       // PolGrid.setRowColDataByName(i, "xtMoney", pointTwo(xtMoney));
       PolGrid.setRowColDataByName(i, "xtMoney", String(xtMoney));
      }
    }
  }
  
  if(polSelectedCount == 0)
  {
    alert("请选择保单险种信息！");
    return false;
  }
  
  return true;
}
//规格化为两位小数，4舍5入
function pointTwoNew( s )
{
  var r = Math.round( parseFloat(s) * 100 )/100;
  var v = r.toString();
  var len = v.length;
  var index = v.indexOf(".");

  if( index==-1 )
  {
    v = v + ".00";
    return v;
  }
  else
  {
    if( len-index==3 )
    {
      return v;
    }
    else if( len-index==2 )
    {
      v = v +"0";
      return v;
    }
    else if( len-index==1 )
    {
      v = v + "00";
      return v;
    }
    else
    {
      return v.substring(0,index+3);
    }
  }
}

//初始化已存在页面数据
function initInpBox()
{ 
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
 try
	{    
		
		fm.all('EdorNo').value = top.opener.fm.all('EdorAcceptNo').value;
		fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
		fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
		fm.all('EdorTypeName').value = top.opener.fm.all('EdorTypeName').value;
		fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
		fm.all('ContType').value ='2';
		try {fm.all('AppntNo').value= ''; } catch(ex) { }; //客户号码
		try {fm.all('Peoples2').value= ''; } catch(ex) { }; //投保总人数
		try {fm.all('GrpName').value= ''; } catch(ex) { }; //单位名称
		try {fm.all('SignCom').value= ''; } catch(ex) { }; //签单机构
		try {fm.all('SignDate').value= ''; } catch(ex) { }; //签单日期
		try {fm.all('CValiDate').value= ''; } catch(ex) { }; //保单生效日期
		try {fm.all('Prem').value= ''; } catch(ex) { }; //总保费
		try {fm.all('Amnt').value= ''; } catch(ex) { }; //总保额
		
		try {fm.all('EdorValiDate').value= getEdorValiDate(); } catch(ex) { }; //总保额
		//showOneCodeName("EdorCode", "EdorTypeName"); 
	}
	catch(ex)
	{
		alert("在PEdorTypeXTInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}   
	
	setXTDetailInfo(); 
	hasULIRisk();
}   

//给XT明细录入元素赋值
function setXTDetailInfo()
{
  var sql = "select EdorValiDate, ReasonCode "
            + "from LPEdorItem "
            + "where EdorNo = '" + fm.EdorNo.value + "' "
            + "   and EdorType = '" + fm.EdorType.value + "' "
            + "   and ContNo = '" + fm.ContNo.value + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.EdorValiDate.value = rs[0][0];
    fm.reason_tb.value = rs[0][1];
    
    var sql = "select CodeName "
              + "from LDCode "
              + "where CodeType = 'reason_tb' "
            + "   and Code = '" + fm.reason_tb.value + "' ";
    var rs = easyExecSql(sql);
    if(rs && rs[0][0] != "" && rs[0][0] != "null")
    {
      fm.reason_tbName.value = rs[0][0];
    }
  }
}

//20081107 add by zhanggm 判断协议退保的险种是否包含万能险种
function hasULIRisk()
{
  var sql = "select 1 from LCPol a, LMRiskApp b where a.riskcode = b.riskcode "
          + "and a.ContNo = '" + fm.all('ContNo').value + "' "
          + "and b.RiskType4 = '4'";
  if(easyQueryVer3(sql))
  {
    hasULI = true;
    showULIPremInfo();
  }
  else
  {
    hasULI = false;  
  }
}
//20081107 add by zhanggm 显示万能险的费用信息，保费、部分领取、追加保费
function showULIPremInfo()
{
  var PremInfo = "保单"+fm.ContNo.value+"，";
  var sql = "select nvl(sum(SumActuPayMoney),0) From LJAPay where IncomeNo = '" +fm.ContNo.value+ "' with ur";
  var rs = easyExecSql(sql);
  if(rs && rs[0][0] != 0 && rs[0][0] != "0")
  {
    PremInfo += "缴纳保费："+rs[0][0]+"元，";
  }
  var sql1 = "select " + rs[0][0] ;
  
  sql = "select nvl(sum(SumActuPayMoney),0) from LJAPay a,LPEdorItem b,LPEdorApp c "
      + "where a.Incomeno = b.EdorAcceptNo and a.Incomeno = c.EdorAcceptNo "
      + "and c.EdorState = '0' "
      + "and b.EdorType = 'ZB' and b.ContNo = '" +fm.ContNo.value + "' with ur";
  var rs1 = easyExecSql(sql);
  if(rs1 && rs1[0][0] != 0 && rs1[0][0] != "0")
  {
    PremInfo += "追加保费："+rs1[0][0]+"元，";
  }
  
  sql = "select nvl(sum(Money),0) from LCInsureAccTrace "
      + "where ContNo = '" +fm.ContNo.value + "' and MoneyType = 'LX' with ur "
  var rs3 = easyExecSql(sql);
  if(rs3 && rs3[0][0] != 0 && rs3[0][0] != "0")
  {
    PremInfo += "结算利息："+rs3[0][0]+"元，";
  }
  
  sql = "select nvl(sum(SumGetMoney),0) from LJAGet a,LPEdorItem b,LPEdorApp c "
      + "where a.OtherNo = b.EdorAcceptNo and a.OtherNo = c.EdorAcceptNo "
      + "and c.EdorState = '0' "
      + "and b.EdorType = 'LQ' and b.ContNo = '" +fm.ContNo.value + "' with ur";
  var rs2 = easyExecSql(sql);
  if(rs2 && rs2[0][0] != 0 && rs2[0][0] != "0")
  {
    PremInfo += "部分领取："+rs2[0][0]+"元，";
  }
  
  var totalPrem = parseFloat(rs[0][0])+parseFloat(rs1[0][0])-parseFloat(rs2[0][0])+parseFloat(rs3[0][0]);
  PremInfo += "合计费用："+totalPrem+"元。";
  fm.all('TotalPremInfo').value = PremInfo;
  fm.all('TotalPrem').value = totalPrem;
}
//20081107 add by zhanggm 校验万能协议退保费用，不能高于用户合计交费金额。高于true;
function checkULIPrem()
{
  var sumprem = 0;
  var temp = 0;
  for(var i = 0; i < PolGrid.mulLineCount; i++)
  {
    if(PolGrid.getChkNo(i))
    {
      sumprem += parseFloat(PolGrid.getRowColDataByName(i, "xtMoney"));
      temp++;
    }
  }
  if(sumprem > fm.all('TotalPrem').value)
  {
    alert("协议退保金额不能大于合计交费金额"+fm.all('TotalPrem').value+"元");
    return true;
  }
  if(temp < PolGrid.mulLineCount)
  {
    alert("万能险只支持能两个险种同时协议解约。");
    return true;
  }
  return false;
}

function checkRight(){
	var strSQL = "select 1 from llcontdeal where edorno='"+fm.EdorAcceptNo.value+"' and appoperator='"+fm.Operator.value+"'";
	var arrResult = easyExecSql(strSQL);
	if (!arrResult) {
		alert("没有权限进行处理");
		return false;
	}
	return true;
}

    