<html>
  <%
  /*******************************************************************************
  * Name     :SecondUWInput.jsp
  * Function :初始化“案件审核”中的解约退费”的界面程序
  * Date     :2006-02-01
  * Author   :Xx
  */
  %>

  <%@page contentType="text/html;charset=GBK"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%
  %>
  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="EndAgreementInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="EndAgreementInit.jsp"%>
  </head>
  <body  onload="initForm();">
    <form  action='./EndAgreementSave.jsp' method=post name=fm target="fraSubmit">
      <table>
        <tr class=common>
          <td >
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
          </td>
          <td class= titleImg>
            解约暂停信息
          </td>
        </tr>
      </table>

      <Div  id= "divLLCase1" style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD  class= title>
              团体批次号
            </TD>
            <TD  class= input>
              <Input class= "readonly" readonly name=RgtNo >
            </TD>
            <TD  class= title>
              理赔号
            </TD>
            <TD  class= input>
              <Input class= "readonly" readonly name=CaseNo >
            </TD>
          </TR>

          <TR  class= common style='display:none'>
            <TD  class= title>
              案件类型
            </TD>
            <TD  class= input>
              <Input class= "readonly" readonly name=Type >
            </TD>
            <td class= common>
              <input class=common type=hidden value="查询所有保单" onclick="showUWInfo()">
            </td>
          </TR>

          <TR  class= common>
            <TD  class= title>
              客户号码
            </TD>
            <TD  class= input>
              <Input class= "readonly" readonly name=InsuredNo >
            </TD>
            <TD  class= title>
              客户姓名
            </TD>
            <TD  class= input>
              <Input class= "readonly" readonly name=CustomerName >
            </TD>
          </TR>
        </table>
      </Div>

      <Table >
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondUWInfo);">
          </TD>
          <TD class= titleImg>
            解约暂停明细信息
          </TD>
        </TR>
      </Table>

      <Div  id= "divSecondUWInfo" style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanSecondUWGrid" >
              </span>
            </TD>
          </TR>
        </table>
      </Div>

      <table class=common>
        <TR  class= common>
          <TD  class= title>
            解约暂停提示
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= input>
          <textarea name="UWRegister" cols="90%" rows="4" witdh=25% class="common"></textarea></TD>
          </TD>
        </TR>
      </table>

      <table class=common>
        <TR class=common>
          <TD class=common>
            <Input value="退保理算" type= button class=cssButton onclick= "CalZT()">
            <INPUT VALUE="返    回" type=button class=cssButton onclick="top.close();">
          </TD>
        </TR>
      </table>
      <hr>
      <Table >
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondUWInfo);">
          </TD>
          <TD class= titleImg>
            理算结果
          </TD>
        </TR>
      </Table>

      <Div  id= "divSecondUWInfo" style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanLoanValueGrid" >
              </span>
            </TD>
          </TR>
        </table>
        <br>
        <Input value="解约确认" type= button class=cssButton onclick= "EndAgreement()">

        <input type=hidden name="Opt">
        <input type=hidden name="StartPhase">
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
  <script language="javascript">

  </script>
</html>
