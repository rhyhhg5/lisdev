var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var tSaveType="";

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
	}
}

function easyQuery()
{
}

function submitForm()
{
	if(fm.llusercode.value==null||fm.llusercode.value=="")
	{
		alert("请选择理赔员！");
		return;
		}

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
    fm.submit(); //提交
}

function PCsubmitForm()
{	
	if(fm.PCLPYcode.value==null||fm.PCLPYcode.value=="")
	{
		alert("请选择理赔员！");
		return;
	}
	if(fm.RgtNo.value==null||fm.RgtNo.value=="")
	{
		alert("请填入要变更的批次！");
		return;
	}
	
	tstrSQL="select count(*) from llcase where rgtno = '"+fm.all('RgtNo').value+"' and rgtstate not in ('07','09','12','14','11','13')";
    tarrResult = easyExecSql(tstrSQL);
    if(tarrResult[0][0]==0)
  	{
  	  alert("此批次下没有可以变更的案件");
  	  return false;
  	}
  
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	fm.action="./LLCaseTransferRgtSave.jsp";
    fm.submit(); //提交
}


function submitFormAuto()
{


	
		if(!CheckGrid.getChkNo())
	{
		
		 if(!confirm("您确定要全部案件重新分配吗？"))
					    return false;
					    
		
	}

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	fm.action="./LLCaseTransferAutoSave.jsp?Handler="+Handler;
    fm.submit(); //提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		window.location.reload();
	/*	top.close();
		top.opener.location.reload();
		parent.opener.top.focus(); */
	}
} 
function classify(){
var contTypeSQL="";
var addString =" and a.mngcom like '%"+ManageCom+"%'";
var statePart="";
switch(RgtState){	   case "01":
			statePart =" and rgtstate<'09'";
			break;
			case "02":
			statePart =" and rgtstate in ('09','11','12')";
			break;
			case "03":
			statePart =" and rgtstate='11'";
			break;
			case "04":
			statePart =" and rgtstate<'15'";
			break;
		}
addString=addString+statePart;
if(StartDate!="")
addString=addString+" and a.rgtdate>= '"+StartDate+"'";
if(EndDate!="")
addString=addString+" and a.rgtdate<= '"+EndDate+"'";
if(RgtNo!="" && RgtNo!="null")
addString=addString+" and a.rgtno='"+RgtNo+"'";
if(CustomerName!="" && CustomerName!="null")
addString=addString+" and a.CustomerName='"+CustomerName+"'";
if(CustomerNo!="" && CustomerNo!="null")
addString=addString+" and a.CustomerNo='"+CustomerNo+"'";
if(CaseNo!="" && CaseNo!="null")
addString=addString+" and a.CaseNo='"+CaseNo+"'";
if(CaseNo!="" && CaseNo!="null")
addString=addString+" and a.CaseNo='"+CaseNo+"'";
if(ContType!="" && ContType!="null"){
   if(ContType=="1"){
     contTypeSQL=" and exists (select 1 from lcpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno=a.customerno union select 1 from lbpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno=a.customerno)";
    }
    if(ContType=="2"){
   contTypeSQL=" and not exists (select 1 from lcpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno=a.customerno union select 1 from lbpol where grpcontno='00000000000000000000' and appflag='1' and conttype='1' and insuredno=a.customerno ) and exists (select 1 from lcpol where conttype='2' and appflag='1' and insuredno=a.customerno union select 1 from lbpol where  conttype='2' and appflag='1' and insuredno=a.customerno)";
   }
   addString=addString+contTypeSQL;
  }
	var anow = getCurrentDate();
	var strSQL="select case when a.rgtno=a.caseno then '' else a.rgtno end xx,a.CaseNo,a.CustomerName,a.rgtstate,a.CustomerNo,a.rgtdate,a.endcasedate, "
							+" a.RgtType,a.operator,b.codename,(select givetypedesc from llclaimdetail where caseno=a.caseno fetch first 1 rows only), " 
							+" to_char(date('"+anow+"')-date(a.rgtdate)+1)  from llcase a, ldcode b where "
						    +" b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgttype='1' "+addString;

		turnPage.queryModal(strSQL, CheckGrid);
	
}


//调查分配
function DealSurveyAssign()
{
	var caseno = '';
	var newWindow = OpenWindowNew("./FrameMainSurveyDispatch.jsp?Interface=LLSurveyDispatch.jsp","调查分配","middle",800,330);
}

//理赔员统计
function ClaimerCal()
{

		var strSQL=" select bb,sum(aa)  from ( "
							+"	select count(distinct a.caseno) aa,a.handler bb from llcase a, llclaimuser b where "
							+"	b.usercode=a.handler and b.upusercode='"+Operator+"' and a.handler<>'"+Operator+"'  and a.rgtstate not in ('07','09','12','14','11','13')   group by a.handler "
							+"	union "
							+"	select count(distinct a.caseno) aa,c.appclmuwer bb from llcase a, llclaimuser b,llclaimuwmain c where  b.usercode=a.handler "
							+"	and a.rgtstate not in ('07','09','12','14','11','13') and b.upusercode='"+Operator+"' and c.appclmuwer<>'"+Operator+"' "
							+"	and rgtstate in ('05','10') and c.caseno=a.caseno  group by c.appclmuwer "
							+"	) AS X group by bb ";
				
		turnPage2.queryModal(strSQL,ClaimerGrid);
		
}   
    
    
    
    
    
    
    
    
    
    
    