<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="ClaimDeclineQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ClaimDeclineQueryInit.jsp"%>
  <title>赔案查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./ClaimDeclineQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 赔案信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLLReport1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            拒赔号
          </TD>
          <TD  class= input>
            <Input class= common name=DeclineNo >
          </TD>
        </TR>
        <TR class=common>
          <TD  class= title>
            拒赔类型
          </TD>
      <TD  class= input width="26%"> 
      	 <Input class=code name=DeclineType verify="拒赔类型" CodeData="0|^0|立案拒赔^1|分案拒赔^2|分案保单拒赔^3|赔案拒赔" ondblClick="showCodeListEx('DeclineQueryType',[this],[0,1,2,3]);" onkeyup="showCodeListKeyEx('DeclineQueryType',[this],[0,1,2,3]);">
      </TD>

        </TR>
        <TR  class= common>
          <TD  class= title>
            关联案件号
          </TD>
          <TD  class= input>
            <Input class= common name=RelationNo >
          </TD>
        </TR>
      </table>
    </Div>
          <INPUT VALUE="查询" TYPE=button onclick="submitForm();return false;"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 拒赔信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanClaimDeclineGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button > 
      <INPUT VALUE="上一页" TYPE=button > 					
      <INPUT VALUE="下一页" TYPE=button > 
      <INPUT VALUE="尾页" TYPE=button > 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
