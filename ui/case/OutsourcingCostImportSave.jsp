<%
//程序名称：OutsourcingCostImportSave.jsp
//程序功能：导入提交页面
//创建日期：2013-12-23
//创建人  ：LiuJian
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.lang.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="jxl.*"%>
<%
    
	CErrors tError = null;
	String flag="";
	String content="";
	//接收类（天津外包费用总额）
	LLOutsourcingTotalSchema tLLOutsourcingTotalSchema   = new LLOutsourcingTotalSchema();
	//	接收类（本保费分摊费用）
	LLOutcoucingAverageSchema tLLOutcoucingAverageSchema   = new LLOutcoucingAverageSchema();
	System.out.println("批次号要用的登录机构是----------");
	LLOutcoucingAverageSet tLLOutcoucingAverageSet= new LLOutcoucingAverageSet();
	OutsourcingCostImportUI tOutsourcingCostImportUI = new OutsourcingCostImportUI();
	
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String tOperate="";
	//批次号
	 String LogmanageCom= request.getParameter("LogmanageCom");
	 String mBatchNo = LogmanageCom.substring(0, 4);
	 System.out.println("批次号要用的登录机构是----------"+mBatchNo);
	 //mBatchNo = "TJ"+mBatchNo+PubFun1.CreateMaxNo("TJBatchNo" , 10);
	 mBatchNo = mBatchNo+PubFun1.CreateMaxNo("TJBatchNo" , 10);
	//险种编码
	String RiskCode= request.getParameter("RiskCode");
	//得到excel文件的保存路径
	String path = application.getRealPath("").replace('\\','/')+'/';  //application.getRealPath("")取到的路径是用"\"分隔的
	
	
	DiskFileUpload fu = new DiskFileUpload();
	//设置允许用户上传文件大小,单位:字节
	fu.setSizeMax(10000000);
	
	//设置最多只允许在内存中存储的数据,单位:字节
	fu.setSizeThreshold(4096);
	
	//设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
	fu.setRepositoryPath(path+"temp");
	//开始读取上传信息
	List fileItems = null;
	
	try{
		fileItems = fu.parseRequest(request);
	}
	catch(Exception ex){
		ex.printStackTrace();
	}
	
	//依次处理每个上传的文件
	Iterator iter = fileItems.iterator();
	List grpContnoList =new ArrayList();
	while (iter.hasNext()) {
		
		FileItem item = (FileItem) iter.next();
		
		  //忽略其他不是文件域的所有表单信息
		  if (!item.isFormField())
		  {
			  
			  //通过校验得到保单号集合
			  try{
				 
				 OutsourcingCostUpload outsourcingCostUpload = new OutsourcingCostUpload(); 
			   	 grpContnoList = outsourcingCostUpload.checkGrpcontnoList(item.getInputStream());
			   	 System.out.println("grpContnoList集合的长度---------"+grpContnoList.size());
			  }catch(Exception ex){
				  content="导入失败，请使用模板，不要改变模板格式！" ;
				   flag="Fail";
			  }
			  	
		  }
		    
	}
	//首先进入if语句进行grpContnoList的重复校验
	
	HashSet set = new HashSet(grpContnoList);
	/*
	进行批次号校验   
	*/
	String getcheckGrpContno = tOutsourcingCostImportUI.checkGrpContno(grpContnoList,RiskCode);
	if(null!=getcheckGrpContno){
		content="导入失败，原因是:导入的保单号"+getcheckGrpContno+"下没有险种"+RiskCode+"，请核查！";
		flag="Fail";
	}else if(!tOutsourcingCostImportUI.checkBatchNo(mBatchNo)){
		content="保存失败，原因是:该批次号已经导入过，请重新刷新页面再导入！";
		flag="Fail";
	}else if(set.size()!=grpContnoList.size()){
		content="导入失败，原因是：保单号有重复，请重新检查保单号！" ;
		flag="Fail";
	}else if(!tOutsourcingCostImportUI.checkBatchNoLength(grpContnoList)){
		String mCount=tOutsourcingCostImportUI.getMCount();
		content="导入失败，原因是：第"+mCount+"条保单号过长，请重新确认！" ;
		flag="Fail";
	}
	else{
		//只有校验成功才能进入这里
		//遍历grpContnoList保单号集合，然后把每一个值放到tLLOutcoucingAverageSchema中
		Iterator grpConno = grpContnoList.iterator();
		
		while(grpConno.hasNext()){
			LLOutcoucingAverageSchema mLLOutcoucingAverageSchema   = new LLOutcoucingAverageSchema();
			mLLOutcoucingAverageSchema.setBatchno(mBatchNo);
			mLLOutcoucingAverageSchema.setGrpcontno((String)grpConno.next());
			mLLOutcoucingAverageSchema.setP1("0");
			tLLOutcoucingAverageSet.add(mLLOutcoucingAverageSchema);
		}
		VData tVData = new VData();
		tVData.add(tG);
		tVData.add(tLLOutcoucingAverageSet);
		
		try{
			if(!tOutsourcingCostImportUI.submitData(tVData,tOperate)){
				content="导入失败，原因是："+tLLOutcoucingAverageSchema.mErrors.getFirstError() ;
				flag="Fail";
			}else{
			
				content="导入成功！其他信息填写完毕后，请点击保存！";
				flag="Succ";
			}
		}catch(Exception ex){
			content="导入失败，原因是："+ex.toString() ;
			flag="Fail";
		 }
	}
	System.out.println("upload successfully");
	
%>
<script language="javascript" >
	parent.fraInterface.fm.all('BatchNo').value = '<%=mBatchNo%>';
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
