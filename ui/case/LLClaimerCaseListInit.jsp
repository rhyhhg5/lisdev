<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
 
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('AllOperator').value = "<%=Operator%>";
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    initInpBox();
    initCheckGrid();
    initClaimerGrid();
    classify();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=1;
      
    iArray[1]=new Array("理赔号","120px","0","0");
    iArray[7]=new Array("案件类型","80px","0","0");
    iArray[3]=new Array("案件状态代码","80px","0","3");
    iArray[4]=new Array("客户号","80px","0","0");
    iArray[2]=new Array("客户姓名","80px","0","0");
    iArray[5]=new Array("受理日期","80px","0","0");
    iArray[6]=new Array("结案日期","80px","0","0");
    iArray[8]=new Array("处理人","80px","0","3");;
    iArray[9]=new Array("案件状态","100px","0","0");
    iArray[10]=new Array("给付结论","100px","0","0");
    iArray[11]=new Array("处理时效(天)","80px","0","0");


    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =0;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canSel =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CheckGrid. selBoxEventFuncName = "onSelSelected";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initClaimerGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=1;
      
    iArray[0]=new Array("序号","30px","0",0);
			iArray[1]=new Array("理赔员","160px","0",0);
			iArray[2]=new Array("理赔员代码","70px","0",0);
			iArray[3]=new Array("机构","0px","0",3);
			iArray[4]=new Array("理赔件数","60px","0",0);
			iArray[5]=new Array("件数占比(%)","80px","0",0);
			iArray[6]=new Array("理赔金额","80px","0",0);
			iArray[7]=new Array("金额占比(%)","80px","0",0);


    ClaimerGrid = new MulLineEnter("fm","ClaimerGrid");
    ClaimerGrid.mulLineCount =0;
    ClaimerGrid.displayTitle = 1;
    ClaimerGrid.locked = 1;
    ClaimerGrid.canSel =0;
    ClaimerGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ClaimerGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CheckGrid. selBoxEventFuncName = "onSelSelected";
    ClaimerGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}


 </script>