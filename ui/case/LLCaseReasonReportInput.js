//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	if(fm.all('ManageCom').value == '1')
	{
		alert("输入有误，可能是如下错误中的一个：\n管理机构输入不符合数据库规定的取值范围！请查阅或双击输入框选择！");
		return false;
	}
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "CaseReport";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}
function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}


function getCurrentDate()
{
		var d = new Date();
		var year = d.getYear();
		var month = d.getMonth();
		var day = d.getDay();
		
		fm.StartDate.value = year+"-"+(month-1)+"-"+day;
		fm.EndDate.value = year+"-"+month+"-"+day;
		
}