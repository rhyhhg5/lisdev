<%
//Name:ReportInit.jsp
//function：
//author:Xx
//Date:2005-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

  String CaseNo= request.getParameter("CaseNo");
  
%>
<script language="JavaScript">
var turnPage = new turnPageClass();
var CaseNo="<%=CaseNo%>";

function initForm()
{
  try
  {
    

    initCheckGrid();
		ClaimerCal();
  }  
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=3;

    iArray[1]=new Array("批次号","120px","0","0");    
    iArray[2]=new Array("理赔号","120px","0","0");
    iArray[3]=new Array("变更时案件状态","80px","0","0");
    iArray[4]=new Array("变更前理赔人","80px","0","0");
    iArray[5]=new Array("变更后理赔人","80px","0","0");
    iArray[6]=new Array("变更备注","120px","0","0");
    iArray[7]=new Array("变更人","80px","0","0");
    iArray[8]=new Array("变更日期","80px","0","0");


    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =10;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canChk =0;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CheckGrid. selBoxEventFuncName = "onSelSelected";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function ClaimerCal()
{


		var strSQL="select case when rgtno=caseno then '' else rgtno end,caseno,(select codename from ldcode where codetype ='llrgtstate' and code=beforstate),(select username from llclaimuser where usercode=ohandler),(select username from llclaimuser where usercode=nhandler),remark,(select username from llclaimuser where usercode=llcaseback.operator),modifydate from llcaseback where backtype='9'  "+
								"and caseno='"+CaseNo+"'";
	
		turnPage.queryModal(strSQL,CheckGrid);
		
}   
    



 </script>