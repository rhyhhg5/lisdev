<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLClaimInputInit.jsp
//程序功能：理算页面初始化
//创建日期：2002-06-19 11:10:36
//创 建 人：Wujs
//更 新 人：Xx

%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
		String LoadC = "";
		if(request.getParameter("LoadC")!=null){
			LoadC = request.getParameter("LoadC");
		}
     	String LoadD="";
		if(request.getParameter("LoadD")!=null){
			LoadD = request.getParameter("LoadD");
		}
		
		String tShowCaseRemarkFlag = "";//
		tShowCaseRemarkFlag = (String)session.getAttribute("ShowCaseRemarkFlag");
		System.out.println("tShowCaseRemarkFlag:"+tShowCaseRemarkFlag);
		
%>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox(){
  try{
    var tCaseNo= '<%=request.getParameter("CaseNo")%>';
    fm.all('CaseNo').value = tCaseNo=='null'?'':tCaseNo;
  }
  catch(ex){
    alert("在LLClaimInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(){
  try{
  	initInpBox();
    initClaimPolGrid();
    initClaimDetailGrid();
    initClaimResultGrid();
	
	fm.ShowCaseRemarkFlag.value = "<%=tShowCaseRemarkFlag%>";
    var LoadFlag = "<%=LoadC%>";
    if (LoadFlag =='2'){
      divconfirm.style.display='none';
      CalculateB.style.display='none';
    }
	fm.LoadD.value="<%=LoadD%>";
    if(fm.LoadD.value=='1')
     {
       document.getElementById('sub1').disabled = true;
       document.getElementById('sub2').disabled = true;
       document.getElementById('sub3').disabled = true;
       document.getElementById('sub4').disabled = true;
       // document.getElementById('sub5').disabled = true;
       document.getElementById('sub6').disabled = true;
       document.getElementById('sub7').disabled = true;
       document.getElementById('sub8').disabled = true;      
     }
    initTitle();
    <%
    GlobalInput mG = new GlobalInput();
    mG=(GlobalInput)session.getValue("GI");
    %>
	fm.Handler.value = "<%=mG.Operator%>";
	fm.ModifyDate.value = getCurrentDate();
	var strSQL3="select enddate,operator from llcaseoptime where rgtstate='04' and caseno='"+fm.CaseNo.value+"'order by sequance desc " ;
	var arrResult3 = easyExecSql(strSQL3);
		
	if(arrResult3 != null && arrResult3 !="" ){ 

	fm.ModifyDate.value = arrResult3[0][0];
	fm.Handler.value = arrResult3[0][1];
	
	}
    queryCalPay("init");
    
    showCaseRemark();//#1769 案件备注信息的录入和查看功能 add by Houyd 初始化页面弹出

  }
  catch(re)
  {
    alert("ProposalInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
  }
}

// 被保人信息列表的初始化
function initClaimPolGrid()
{
  var iArray = new Array();

  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";    	           //列名
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=100;            			//列最大值
    iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号";         			//列名
    iArray[1][1]="120px";         			//列宽
    iArray[1][2]=10;          			    //列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="llclaimpolicy"
    iArray[1][5]="1|2|3|7|12|11";    
		iArray[1][6]="0|1|2|3|4|5";
    iArray[1][15]="CaseNo"
    iArray[1][16]=fm.all("CaseNo").value

    iArray[2]= new Array("险种名称","150px","100","2","llclaimrisk","2|11|7","1|0|2");
    iArray[2][15]=fm.all("CaseNo").value
    iArray[2][17]="1"

    iArray[3]= new Array("给付责任类型","80px","100","2","llgetdutykind");
    iArray[3][15]="RiskCode"
    iArray[3][17]="11"

    iArray[4]= new Array("原理算赔付金额","80px","100","3");
    iArray[5]= new Array("核算赔付金额","80px","100","3");
    iArray[6]= new Array("实赔额","80px","100","3");
    iArray[7]= new Array("险种保单号","80px","100","3");
    iArray[8]= new Array("赔案号","80px","100","3");
    iArray[9]= new Array("赔付结论","80px","100","3");
    iArray[10]= new Array("赔付结论代码","80px","100","3");
    iArray[11]= new Array("险种代码","80px","100","3");
    iArray[12]= new Array("险种保额","80px","100","0");
    ClaimPolGrid = new MulLineEnter( "fm" , "ClaimPolGrid" );
    //这些属性必须在loadMulLine前
    ClaimPolGrid.mulLineCount = 1;
    ClaimPolGrid.displayTitle = 1;
    ClaimPolGrid.canSel=0;
    ClaimPolGrid.canChk=0;
    ClaimPolGrid.hiddenPlus=0;
    ClaimPolGrid.hiddenSubtraction=0;

    ClaimPolGrid.loadMulLine(iArray);

    //这些操作必须在loadMulLine后
    //SubInsuredGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert(ex);
  }
}

function initClaimDetailGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array("序号","30px","100","0");

    iArray[1]=new Array("保单号码","80px","10","0");

    iArray[2]=new Array("险种代码","50px","10","2");
    iArray[2][4]="llclaimrisk"
    iArray[2][15]="保单号码"
    iArray[2][17]="1"

    iArray[3]= new Array("给付责任","120px","100","0");

    iArray[4]= new Array("账单金额","50px","100","1");
    iArray[5]= new Array("拒付金额","50px","100","1");
    iArray[6]= new Array("先期给付","50px","100","1");
    iArray[7]= new Array("通融/协议给付比例","100px","100","1");
    iArray[8]= new Array("免赔额","50px","100","3");
    iArray[9]= new Array("溢额","50px","100","1");

    iArray[10]= new Array("赔付结论","60px","100","2","llclaimdecision","10|20","1|0");

    iArray[11]= new Array("赔付结论依据","80px","100","2","llclaimdecision_1", "11|21","1|0");
    iArray[11][15]="赔付结论代码"
    iArray[11][17]="20"

    iArray[12]= new Array("理算金额","60px","100","0");
    iArray[13]= new Array("核算赔付金额","80px","100","3");
    iArray[14]= new Array("实赔金额","60px","100","0");

    iArray[15]= new Array("险种保单号","80px","100","3");
    iArray[16]= new Array("赔案号","80px","100","3");
    iArray[17]= new Array("给付责任代码","80px","100","3");
    iArray[18]= new Array("给付责任类型","80px","100","3");
    iArray[19]= new Array("责任代码","80px","100","3");
    iArray[20]= new Array("赔付结论代码","80px","100","3");
    iArray[21]= new Array("赔付结论依据代码","80px","100","3");
    iArray[22]= new Array("受理事故号","80px","100","3");
    iArray[23]= new Array("比例/金额","50px","10","2")
    iArray[23][10]="IsRate";
    iArray[23][11]="0|^1|比例|^2|金额"
    iArray[23][12]="23"
    iArray[23][13]="0"

    ClaimDetailGrid = new MulLineEnter( "fm" , "ClaimDetailGrid" );
    ClaimDetailGrid.mulLineCount = 1;
    ClaimDetailGrid.displayTitle = 1;

    ClaimDetailGrid.canSel=0;
    ClaimDetailGrid.canChk=1;
    ClaimDetailGrid.hiddenPlus=1;
    ClaimDetailGrid.hiddenSubtraction=1;

    ClaimDetailGrid.loadMulLine(iArray);

  }
  catch(ex)
  {
    alert(ex);
  }
}

function initClaimResultGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array("序号","30px","100","0");

    iArray[1]=new Array("保单号码","80px","10","0");

    iArray[2]=new Array("险种代码","50px","10","2");
    iArray[2][4]="llclaimrisk"
    iArray[2][15]="保单号码"
    iArray[2][17]="1"

    iArray[3]= new Array("给付责任","120px","100","0");

    iArray[4]= new Array("账单金额","50px","100","1");
    iArray[5]= new Array("拒付金额","50px","100","1");
    iArray[6]= new Array("给付比例","100px","100","1");
    iArray[7]= new Array("免赔额","50px","100","1");
    iArray[8]= new Array("限额","50px","100","1");

    iArray[9]= new Array("理算金额","60px","100","0");
    iArray[10]= new Array("核算赔付金额","80px","100","3");
    iArray[11]= new Array("实赔金额","60px","100","0");

    iArray[12]= new Array("险种保单号","80px","100","3");
    iArray[13]= new Array("赔案号","80px","100","3");
    iArray[14]= new Array("给付责任代码","80px","100","3");
    iArray[15]= new Array("给付责任类型","80px","100","3");
    iArray[16]= new Array("责任代码","80px","100","3");
    iArray[17]= new Array("受理事故号","80px","100","3");
    iArray[18]= new Array("是否判断保额","80px","100","0");
    iArray[19]= new Array("保额","80px","100","0");
    iArray[20]= new Array("历史赔付","80px","100","0");

    ClaimResultGrid = new MulLineEnter( "fm" , "ClaimResultGrid" );
    ClaimResultGrid.mulLineCount = 1;
    ClaimResultGrid.displayTitle = 1;

    ClaimResultGrid.canSel=0;
    ClaimResultGrid.canChk=1;
    ClaimResultGrid.hiddenPlus=1;
    ClaimResultGrid.hiddenSubtraction=1;

    ClaimResultGrid.loadMulLine(iArray);

  }
  catch(ex)
  {
    alert(ex);
  }
}

function initClaimPayGrid()
  {
    var iArray = new Array();

      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="180px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="伤残给付";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="90px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="死亡给付";         			//列名
      iArray[3][1]="90px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="医疗给付";         			//列名
      iArray[4][1]="90px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="退保金";         			//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="退费";         			//列名
      iArray[6][1]="90px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="其他";         			//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="给付合计";         			//列名
      iArray[8][1]="0px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      ClaimPayGrid = new MulLineEnter( "fm" , "ClaimPayGrid" );
      ClaimPayGrid.mulLineCount = 1;
      ClaimPayGrid.displayTitle = 1;
      ClaimPayGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //SubInsuredGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function mlChange(parm1,parm2){

   var rangNum=ClaimPayGrid.mulLineCount;                                   // ClaimPayGrid的长度
   var rowNum=ClaimPolGrid.mulLineCount;                                   // claimPolGrid的长度
   var vHspfje=fm.all(parm1).all('ClaimDetailGrid3').value;               // claimDetailGrid的核算赔付金额
   var vSjpfje=fm.all(parm1).all('ClaimDetailGrid4').value;             // claimDetailGrid的实际赔付金额
   var vBdh=fm.all(parm1).all('ClaimDetailGrid1').value;                 // claimDetailGrid的保单号
   var vTjlx=fm.all(parm1).all('ClaimDetailGrid5').value;                 // claimDetailGrid的统计类型
   vHspfje=parseFloat(vHspfje);
   vSjpfje=parseFloat(vSjpfje);



   if(fm.all(parm1).all('InpClaimDetailGridChk').value=='1'){               // 选中
       for(i=0;i<rowNum;i++){
           var iArray=ClaimPolGrid.getRowData(i);
           if(iArray[0]==vBdh){
                                                                        // claimPolGrid, claimDetailGrid的保单号相等
                iArray[6]=parseFloat(iArray[6]);                        //claimPolGrid 核算赔付金额   字符---数值
                iArray[7]=parseFloat(iArray[7]);                        //claimPolGrid 实际赔付金额   字符---数值
                iArray[6]+=vHspfje;                                       // claimPolGrid核算赔付金额+claimDetailGrid核算赔付金额
                iArray[7]+=vSjpfje;                                     // claimPolGrid实际赔付金额+claimDetailGrid实际赔付金额
                iArray[6]+="";
                iArray[7]+="";                                         //数值----字符
                ClaimPolGrid.setRowColData(i,7,iArray[6]);
                ClaimPolGrid.setRowColData(i,8,iArray[7]);
           }
       }
       for(j=0;j<rangNum;j++){
            var jArray=ClaimPayGrid.getRowData(j);
            if(jArray[0]==vBdh){

                  if("SC"==vTjlx){
                      jArray[1]=parseFloat(jArray[1]);
                      jArray[1]+=vSjpfje;
                      jArray[1]+="";
                      ClaimPayGrid.setRowColData(j,2,jArray[1]);

                  }

                  if("SW"==vTjlx){
                      jArray[2]=parseFloat(jArray[2]);
                      jArray[2]+=vSjpfje;
                      jArray[2]+="";
                      ClaimPayGrid.setRowColData(j,3,jArray[2]);

                  }
                  if("YL"==vTjlx){
                      jArray[3]=parseFloat(jArray[3]);
                      jArray[3]+=vSjpfje;
                      jArray[3]+="";
                      ClaimPayGrid.setRowColData(j,4,jArray[3]);

                  }

           }
      }


   }
   if(fm.all(parm1).all('InpClaimDetailGridChk').value=='0'){
        for(i=0;i<rowNum;i++){
           var iArray=ClaimPolGrid.getRowData(i);
           if(iArray[0]==vBdh){

                iArray[6]=parseFloat(iArray[6]);                        //claimPolGrid 核算赔付金额   字符---数值
                iArray[7]=parseFloat(iArray[7]);                        //claimPolGrid 实际赔付金额   字符---数值
                iArray[6]-=vHspfje;
                iArray[7]-=vSjpfje;
                iArray[6]+="";
                iArray[7]+="";                                       // claimPolGrid核算赔付金额-claimDetailGrid核算赔付金额
                ClaimPolGrid.setRowColData(i,7,iArray[6]);                  //数值----字符
                ClaimPolGrid.setRowColData(i,8,iArray[7]);
           }
        }
        for(j=0;j<rangNum;j++){
            var jArray=ClaimPayGrid.getRowData(j);
            if(jArray[0]==vBdh){


                  if("SC"==vTjlx){                                                       //伤残
                      jArray[1]=parseFloat(jArray[1]);
                      jArray[1]-=vSjpfje;
                      jArray[1]+="";
                      ClaimPayGrid.setRowColData(j,2,jArray[1]);

                  }

                  if("SW"==vTjlx){                                                     //死亡
                      jArray[2]=parseFloat(jArray[2]);
                      jArray[2]-=vSjpfje;
                      jArray[2]+="";
                      ClaimPayGrid.setRowColData(j,3,jArray[2]);

                  }
                  if("YL"==vTjlx){                                                    //医疗
                      jArray[3]=parseFloat(jArray[3]);
                      jArray[3]-=vSjpfje;
                      jArray[3]+="";
                      ClaimPayGrid.setRowColData(j,4,jArray[3]);

                  }

             }
       }
   }
}

function mlChangeAll(parm1,parm2){

      var rNum=ClaimDetailGrid.mulLineCount;
      var rangNum=ClaimPayGrid.mulLineCount;                                   // ClaimPayGrid的长度
      var rowNum=ClaimPolGrid.mulLineCount;
      if(parm1){

         for(i=0;i<rNum;i++){
                var iArray=ClaimDetailGrid.getRowData(i);
                iArray[2]=parseFloat(iArray[2]);                        //claimPolGrid 核算赔付金额   字符---数值
                iArray[3]=parseFloat(iArray[3]);


                for(j=0;j<rowNum;j++){
                   var jArray=ClaimPolGrid.getRowData(j);
                   if(iArray[0]==jArray[0]){
                       jArray[6]=parseFloat(jArray[6]);                        //claimPolGrid 核算赔付金额   字符---数值
                       jArray[7]=parseFloat(jArray[7]);                        //claimPolGrid 实际赔付金额   字符---数值
                       jArray[6]+=iArray[2];
                       jArray[7]+=iArray[3];

                       jArray[6]+="";
                       jArray[7]+="";                                       // claimPolGrid核算赔付金额-claimDetailGrid核算赔付金额
                       ClaimPolGrid.setRowColData(j,7,jArray[6]);                  //数值----字符
                       ClaimPolGrid.setRowColData(j,8,jArray[7]);
                    }
                }
                for(k=0;k<rangNum;k++){
                   var kArray=ClaimPayGrid.getRowData(k);
                   if(iArray[0]==kArray[0]){                                  //伤残
                        if("SC"==iArray[4]){
                         kArray[1]=parseFloat(kArray[1]);
                         kArray[1]+=iArray[3];
                         kArray[1]+="";
                         ClaimPayGrid.setRowColData(k,2,kArray[1]);

                        }

                        if("SW"==iArray[4]){                                    //死亡
                         kArray[2]=parseFloat(kArray[2]);
                         kArray[2]+=iArray[3];
                         kArray[2]+="";
                         ClaimPayGrid.setRowColData(k,3,kArray[2]);

                        }
                        if("YL"==iArray[4]){                                     //医疗
                          kArray[3]=parseFloat(kArray[3]);
                          kArray[3]+=iArray[3];
                          kArray[3]+="";
                          ClaimPayGrid.setRowColData(k,4,kArray[3]);

                       } //end if
                  }//end if
              }//end for
          }//end for
      }
      else{
        for(i=0;i<rNum;i++){
                var iArray=ClaimDetailGrid.getRowData(i);
                iArray[2]=parseFloat(iArray[2]);                        //claimPolGrid 核算赔付金额   字符---数值
                iArray[3]=parseFloat(iArray[3]);

                for(j=0;j<rowNum;j++){
                   var jArray=ClaimPolGrid.getRowData(j);
                   if(iArray[0]==jArray[0]){
                       jArray[6]=parseFloat(jArray[6]);                        //claimPolGrid 核算赔付金额   字符---数值
                       jArray[7]=parseFloat(jArray[7]);                        //claimPolGrid 实际赔付金额   字符---数值
                       jArray[6]-=iArray[2];
                       jArray[7]-=iArray[3];
                       jArray[6]+="";
                       jArray[7]+="";                                       // claimPolGrid核算赔付金额-claimDetailGrid核算赔付金额
                       ClaimPolGrid.setRowColData(j,7,jArray[6]);                  //数值----字符
                       ClaimPolGrid.setRowColData(j,8,jArray[7]);
                    }
                }
                for(k=0;k<rangNum;k++){
                   var kArray=ClaimPayGrid.getRowData(k);
                   if(iArray[0]==kArray[0]){                                  //伤残
                        if("SC"==iArray[4]){
                         kArray[1]=parseFloat(kArray[1]);
                         kArray[1]-=iArray[3];
                         kArray[1]+="";
                         ClaimPayGrid.setRowColData(k,2,kArray[1]);

                        }

                        if("SW"==iArray[4]){                                    //死亡
                         kArray[2]=parseFloat(kArray[2]);
                         kArray[2]-=iArray[3];
                         kArray[2]+="";
                         ClaimPayGrid.setRowColData(k,3,kArray[2]);

                        }
                        if("YL"==iArray[4]){                                     //医疗
                          kArray[3]=parseFloat(kArray[3]);
                          kArray[3]-=iArray[3];
                          kArray[3]+="";
                          ClaimPayGrid.setRowColData(k,4,kArray[3]);

                       } //end if
                  }//end if
              }//end for
          }//end for

      }//end  else
}//end function

//-------------------------end --------------------------------------------------------------------------

</script>
