<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GB2312" %>
<%
//程序名称：ModifySave.jsp
//程序功能：
//创建日期：2005-04-15
//创建人  ：DongXin
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>

<!--用户校验类-->

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
		CErrors tError = null;
		String FlagStr = "";
		String Content = "";
		String transact = "";
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");	
		VData tVData = new VData();
		String  OpType=request.getParameter("OpType");

		if(OpType.equals("01")){
	  LLReasonAffixBL tLLReasonAffixBL = new LLReasonAffixBL();
		LLMAffixSet mLLMAffixSet = new LLMAffixSet();
		LLMAffixSet tLLMAffixSet = new LLMAffixSet();		
	  transact = request.getParameter("operate");
		String tsOperateCN = "";
		if (transact.equals("INSERT"))
		tsOperateCN = "保存";
		if (transact.equals("DELETE"))
		tsOperateCN = "删除";		
    String AffixTypeCode[]=request.getParameterValues("AffixBaseGrid1");
    String AffixTypeName[]=request.getParameterValues("AffixBaseGrid2");
    String AffixCode[]=request.getParameterValues("AffixBaseGrid3");
    String AffixName[]=request.getParameterValues("AffixBaseGrid4");    
    String tRNum[] = request.getParameterValues("InpAffixBaseGridChk");
   		for(int i=0;i<tRNum.length;i++){  
       LLMAffixSchema tLLMAffixSchema =new  LLMAffixSchema(); 
       tLLMAffixSchema.setAffixTypeCode(AffixTypeCode[i]);
       tLLMAffixSchema.setAffixTypeName(AffixTypeName[i]);
       tLLMAffixSchema.setAffixCode(AffixCode[i]);
       tLLMAffixSchema.setAffixName(AffixName[i]);
       tLLMAffixSchema.setManageCome("86");
   		 mLLMAffixSet.add(tLLMAffixSchema);	
   		 if(tRNum[i].equals("1")){
   		 tLLMAffixSet.add(tLLMAffixSchema);	
   		 }
   }

		try
			{
	  tVData.add(mLLMAffixSet);
	  tVData.add(tLLMAffixSet);  
	  tVData.add(tG);	
	  tLLReasonAffixBL.submitData(tVData,transact);
			}catch(Exception ex){
	  Content = tsOperateCN+"失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
					}
		if (FlagStr==""){
  	tError = tLLReasonAffixBL.mErrors;
  	if (!tError.needDealError()){
    Content = tsOperateCN+"成功! ";
    FlagStr = "Success";
  		}else{
    Content = tsOperateCN+"失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
 		       }
		}
	}else {
	
		PubSubmit ps = new PubSubmit();
		MMap mmap = new MMap();
		
	  String reasonCode=request.getParameter("codetype");
    String[] Code=request.getParameterValues("AffixGrid1");
 
   String sql="delete from LLMAppReasonAffix where reasoncode='"+reasonCode+"'";
   mmap.put(sql,"DELETE");
	 for(int i=0;Code!=null&&i<Code.length;i++)
	{		    
	   sql="insert into LLMAppReasonAffix values('"+reasonCode+"','"+Code[i]+"')" ;
	 mmap.put(sql,"INSERT");
	}
	tVData.add(mmap);

  try
  {
	ps.submitData(tVData,"UPDATE||MAIN");
  }catch(Exception ex){
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    
     tError = ps.mErrors;
   
    if (!tError.needDealError())
    {
    
      Content = "保存成功";
      FlagStr = "Succ";
    }
    else
    {
      Content = "保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

}
//添加各种预处理
%>
<html>
<script language="javascript">
 
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>