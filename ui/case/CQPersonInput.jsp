<%
//程序名称：LDPersonInput.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<head >
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="CQPersonInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CQPersonInit.jsp"%>
</head>
<body onload="initForm();easyQueryClick();">
  <form  method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏LDPerson1的信息 -->
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
	      </td>
	      <td class= titleImg>
	        个人客户信息
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPerson1" style= "display: ''">
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonBasic);">
	      </td>
	      <td class= titleImg>
	        客户基本信息
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPersonBasic" style= "display: ''">    
      <table  class= common>
        <TR  class= common>           
    <TD  class= title>
      客户号码
    </TD>
    <TD  class= input>
      <Input class="readonly" name=CustomerNo readonly >
    </TD>
    <TD  class= title>
      客户姓名
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=Name readonly >
    </TD>
    <TD  class= title>
      客户性别
    </TD>
    <TD  class= input>
      <Input class="readonly" name=Sex  readonly >
    </TD>
  </TR>
  <TR  class= common>
    
    <TD  class= title>
      客户出生日期
    </TD>
    <TD  class= input>
      <Input class="readonly" readonly name=Birthday >
    </TD>
    <TD  class= title>
      婚姻状况
    </TD>
    <TD  class= input>
      <Input class="readonly" name=Marriage readonly >
    </TD>
    <TD  class= title>
      身份证号码
    </TD>
    <TD  class= input>
      <Input class= readonly name=IDNo readonly >
    </TD>
  </TR>
  
  
    
  <TR  class= common>
    <TD  class= title>
      职业名称
    </TD>
    <TD  class= input>
       <Input class="readonly" name=OccupationName readonly >
    </TD>
     <TD  class= title>
      职业代码
    </TD>
    <TD  class= input>
			<Input class="readonly" name=OccupationCode readonly >
    </TD>
    <TD  class= title>
      工作单位
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=GrpNo readonly >
    </TD>
  </TR>
  
  <TR  class= common>
    <TD  class= title>
      单位地址
    </TD>
    <TD  class= input colSpan=5>
            <Input class="readonly" name=CompanyAddress readonly >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      VIP标志
    </TD>
    <TD  class= input>
       <Input class='readonly' name=VIPValue readonly>
    </TD>
    <TD  class= title>
      优质客户标志
    </TD>
    <TD  class= input>
       <Input class='readonly' name=GoodCustom  readonly>
    </TD>
    <TD  class= title>
      黑名单客户标志
    </TD>
    <TD  class= input>
       <Input class='readonly' name=BlacklistFlag readonly>
    </TD>
  </TR>
  <TR  class= common>
    
  </TR>                     
      </Table>
    </Div>
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonAddress);">
	      </td>
	      <td class= titleImg>
	        客户联系方式
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPersonAddress" style= "display: ''">    
      <table  class= common>
       <TR  class= common>               
          <TD  class= title>
            地址编码<br><font color=red size=1>(选此查看地址信息)</font>
          </TD>
          <TD  class= input>
            <Input class= code name=AddressNo ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this],[0],'', '', '', 1);">
          </TD>
        </TR>

  
			  <TR  class= common>
			    <TD  class= title>
			      手机
			    </TD>
			    <TD  class= input>
			      <Input class= 'readonly' name=Mobile readonly >
			    </TD>
			    <TD  class= title>
			      家庭电话
			    </TD>
			    <TD  class= input>
			      <Input class= 'readonly' name=HomePhone readonly >
			    </TD>
			    <TD  class= title>
			      e_mail
			    </TD>
			    <TD  class= input>
			      <Input class= 'readonly' name=EMail readonly >
			    </TD>
			  </TR>
			  <TR  class= common>
			    <TD  class= title>
			      家庭地址
			    </TD>
			    <TD  class= input >
			      <Input class= 'readonly' name=HomeAddress readonly colSpan=5>
			    </TD>

			  </TR>
     </table>
    </Div> 
    
    
      
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPersonAccount);">
	      </td>
	      <td class= titleImg>
	        客户保单信息
	      </td>
	  </tr>
    </table>

    <Div  id= "divLDPersonAccount" style= "display: ''">    
      <table  class= common>  
  <TR  class= common>      
    <TD  class= title>
      个险客户
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=PersonInsure readonly >
    </TD>
    <TD  class= title>
      团险客户
    </TD>    
    <TD  class= input>
      <Input  name=GroupInsure CLASS="readonly" MAXLENGTH=20 readonly >
    </TD>
    <TD  class= title>
      团个险用户
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=GroupPerson readonly >
    </TD>
  </TR>     
      
        
          <TD>
          <input type=hidden name=Transact >
          <input type=hidden name=GrpContNo>
          </TD>        
      </table>
    </Div>      
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
