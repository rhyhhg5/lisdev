<html>
  <%
  //Name：LLSecuPolicyInput.jsp
  //Function：社保政策配置
  //Date：2006-09-20
  //Author：Helga
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/V<u></u>erifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLTestCal.js"></SCRIPT>
    <%@include file="LLTestCalInit.jsp"%>
  </head>
  <body  onload="initForm();initElementtype();" >

    <form action="./LLTestCalSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBaseInfo);">
            <TD class= titleImg>
              基本分类信息
            </TD>
          </TR>
        </table>

        <Div  id= "divBaseInfo" style= "display: ''">
          <table  class= common>
            <TR  class= common8>
              <TD  class= title>管理机构</TD>
            <TD  class= input><Input class="codeno" name=ManageCom  verify="管理机构|code:comcode&NOTNULL&INT" onclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName elementtype=nacessary></TD>
              <TD  class= title>基金类型</TD>
              <TD  class= input><Input class="codeno" name=SecurityType  verify="基金类型|code:comcode&NOTNULL&INT" onclick="return showCodeList('llsecuritytype',[this,SecurityTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('llsecuritytype',[this,SecurityTypeName],[0,1],null,null,null,1);"><input class=codename name=SecurityTypeName elementtype=nacessary></TD>
              <TD  class= title8>费用类型</TD>
              <TD  class= input8><input class=codeno name=FeeType  CodeData="0|2^1|门诊^2|住院" onclick="return showCodeListEx('FeeType',[this,FeeTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeType',[this,FeeTypeName],[0,1]);"><input class=codename name=FeeTypeName  elementtype=nacessary verify="账单种类|notnull" elementtype=nacessary></TD>
            </TR>
            <TR  class= common8>

              <TD  class= title8>医院级别</TD>
              <TD  class= input><Input class=codeno name=LevelCode CodeData="0|3|^1|一级^2|二级^3|三级" onclick="return showCodeListEx('LevelCode',[this,LevelCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('LevelCode',[this,LevelCodeName],[0,1],null,null,null,1);"><input class=codename name=LevelCodeName ></TD>
              <TD  class= title8>参保人员性质</TD>
              <TD  class= input><Input class=codeno name=InsuredStat CodeData="0|2|^1|在职^2|退休" onclick="return showCodeListEx('InsuredStat',[this,InsuredStatName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('InsuredStat',[this,InsuredStatName],[0,1],null,null,null,1);"><input class=codename name=InsuredStatName ></TD>
              <TD  class= title8>住院次数</TD>
              <TD  class= input><Input class=codeno name=FirstInHos CodeData="0|3^1|第一次住院^2|第二次住院^9|其它" onclick="return showCodeListEx('FirstInHos',[this,FirstInHosName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('FirstInHos',[this,FirstInHosName],[0,1],null,null,null,1);"><input class=codename name=FirstInHosName ></TD>
              </TR>
              <TR  class= common8>

              <TD  class= title8>帐单金额</TD>
              <TD  class= input><Input class=common name=Amuont elementtype=nacessary></TD>
              <TD  class= title8>门诊累计帐单金额</TD>
              <TD  class= input><Input class=common name=Secufee ></TD>
              </TR>
          </table>
        </Div>


<input name="ModiSave" style="display:''"  class=cssButton type=button value="测 算" onclick="Calc()">

        <table>
          <TR>
            <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGetLimit);">
            </TD>
            <TD class= titleImg>
              测算结果
            </TD>
          </TR>
        </table>

        <Div  id= "divGetLimit" style= "display: ''">
          <table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanGetModeGrid" >
                </span>
              </TD>
            </TR>
          </table>
        </Div>
        
        <!--隐藏域-->
        <input type=hidden id="fmtransact" name="fmtransact">
        <input type=hidden id="operate" name="operate">

      </form>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </body>
  </html>
