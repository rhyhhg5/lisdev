//               该文件中包含客户端需要处理的函数和事件


var showInfo;
var mDebug="1";

//提交，保存按钮对应操作
function submitForm()
{
	//add lyc 2014-07-09 IT要求
//	if(fm.Content.value=='')
//	{
//		alert("拒赔原因不能为空");return false;}
  var i = 0;
  fm.fmtransact.value="INSERT"
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  showSubmitFrame(mDebug);
  fm.action = './LLClaimDeclineSave.jsp';
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
   // showDiv(inputButton,"false");
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
     parent.fraMain.rows = "0,0,0,0,*";
 	}
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  var varSrc = "&ClmCaseNo=" + fm.CaseNo.value;
  varSrc += "&Type=1";
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  var newWindow = window.open("./RgtSurveyQuery1.jsp?Interface=RgtSurveyQuery.jsp"+varSrc,"RgtSurveyQuery",'toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//完成按照”客户号“在lcpol中进行查询，显示该客户的保单明细
function showInsuredLCPol()
{
	var varInsuredNo;
	var varCount;
	varCount = SubReportGrid.getSelNo();
	varInsuredNo=SubReportGrid.getRowColData(varCount-1,1);
	if ((varInsuredNo=="null")||(varInsuredNo==""))
	{
		alert("客户号为空，不能进行查询操作！");
		return;
	}

	if (varCount==0)
    {
    		alert("请您选择一个客户号！");
    		return;
    }

	var varSrc = "&InsuredNo=" + SubReportGrid.getRowColData(SubReportGrid.getSelNo()-1,1);
	var newWindow = window.open("./FrameMainReportLCPol.jsp?Interface=ReportLCPolInput.jsp"+varSrc,"ReportLCPolInput");
}

function easyQuery(){
  var tsql = "select reason from llclaimdecline where caseno='"+fm.CaseNo.value+"'";
  var arrResult = easyExecSql(tsql);
  if(arrResult)
    fm.Content.value = arrResult[0][0];
}