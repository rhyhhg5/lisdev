<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="RegisterQuery.js"></SCRIPT>
   <%@include file="RegisterQueryInit.jsp"%>
   <script language="javascript">
   function initDate(){
   		fm.RgtDateS.value="<%=afterdate%>";
   		fm.RgtDateE.value="<%=CurrentDate%>";
   		var usercode="<%=Operator%>";
   		var comcode="<%=Comcode%>";
//   		fm.Operator.value=usercode;
//   		fm.OrganCode.value=comcode;
//   		
//   		
//   		var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
//   		var arrResult = easyExecSql(strSQL);
//			if(arrResult != null)
//			{
//			         fm.optname.value= arrResult[0][0];
// 			}
   }
   </script>
 </head>
<body  onload="initDate();initForm();">
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">

<table>
        <TR>
         <TD class= titleImg>
         请录入查询条件
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
       	<TR>
					<TD  class= title>客户姓名</TD>
					<TD  class= input><Input class=common8 name=CustomerName onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>客户号</TD>
					<TD  class= input><Input class=common name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>身份证号</TD>
					<TD  class= input><Input class=common name=IDNo onkeydown="QueryOnKeyDown()"></TD>
				</TR>
				<TR  class= common>	
					<TD  class= title>理赔号</TD>
					<TD  class= input> <Input class=common8 name=CaseNo  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>类型</TD> 
	        <TD  class= input8><Input  CodeData="0|2^0|未结案^1|已结案" onClick="return showCodeListEx('casestate',[this,CaseStateName],[0,1]);" onkeyup="return showCodeListKeyEx('casestate',[this,CaseStateName],[0,1]);" class=codeno name="CaseState" onkeydown="QueryOnKeyDown()"><Input class= codename name=CaseStateName onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8></TD><TD  class= input8 ></TD>
				</TR>
				<TR style="display:'none'">
					<TD  class= title>团体批次号</TD>
					<TD  class= input> <Input class=common8 name=RgtNo  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>操作员姓名</TD>
					<TD  class= input><Input class= "code8"  name=optname onkeydown="QueryOnKeyDown();" onclick="return showCodeList('optname',[this, Operator], [0, 1],null,fm.optname.value,'username','1');" onkeyup="return showCodeListKey('optname', [this, Operator], [0, 1],null,Str,'1');" >
					<TD  class= title>操作员代码</TD>
					<TD  class= input><Input class="readonly" readonly name=Operator></TD>
					<TD  class= title>管理机构</TD>
					<TD  class= input><Input class= "code8"  name=OrganCode onkeydown="QueryOnKeyDown();" onclick="getstr();return showCodeList('stati',[this], [0],null,str,'1');" onkeyup="getstr();return showCodeListKey('stati', [this], [0],null,str,'1');" >
				</TR>
				<TR>
					<TD  class= title8>受理日期从</TD>
					<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>到</TD>
					<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8></TD><TD  class= input8 ></TD>
				</TR>
       </table>
     </Div>
     <br>
     <hr>
 <!--  <input style="display:''"  class=cssButton type=button value="查 询" onclick="easyQuery()"> -->
    
    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1 align=center>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>     
    </Div>
		<Div align=center>  
 	  	<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
   <!--Div  align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div-->   
	<hr>
	<br>
	 
	<div id="div1" style="display:'none'">
		  <table>
		      <TR>
		        <td>
		          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerSearch);">
		        </td>
		        <td class= titleImg>
		          详细查询信息
		        </TD>
		    	</TR>
		    </table>
	</div>
	 <div id="divCustomerSearch" style="display:'none'">      
   <table  class= common>
			<TR  class= common8>
				<TD  class= title8 id='titleClientNo' style="display:'none'">客户序号</TD><TD  class= input8 id='inputClientNo' style="display:'none'"><Input readOnly class=readonly readonly name="ClientNo" ></TD>
				<TD  class= title8>客户姓名</TD><TD  class= input8><Input readOnly class=common name="CustomerTypeQ" ></TD>
				<TD  class= title8>客户号码</TD><TD  class= input8><Input readOnly class=common name="CustomerNoQ" ></TD>
				<TD  class= title8>性别</TD><TD  class = input8 ><Input readOnly class=common name="Sex" ></TD>
			</tr>
			<TR  class= common8>
				<TD  class= title8>证件号码</TD><TD  class= input8><Input readOnly class=common name="tIDNo" ></TD>
				<TD  class= title8>证件类型</TD><TD  class= input8><Input readOnly class=codeno name="tIDType" ><Input readOnly class= codename name=tIDTypeName></TD>	         
				<TD  class= title8>出生日期</TD><TD  class= input8><Input readOnly class=common name="CBirthday"  ></TD>
				<!--TD  class= title8><Input readOnly class=cssButton type=button value="客户查询" onclick="ClientQuery()"></td-->
			</TR>
			<TR  class= common8>
				<TD  class= title8>移动号码</TD><TD  class= input8><Input readOnly class=common name="Mobile" ></TD>
				<TD  class= title8>联系地址</TD><TD  class= input8 colSpan=3><Input readOnly class=common3 style=" width: 98%;" name="Address" ></TD>
			</TR>
   </table>
</div>
 <Table>
    <TR class= common >
       <TD class=common>
	         <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRegisterInfo);">
    	 </TD>
    	 <TD class= titleImg>
    			 申请人信息
    	 </TD>
    </TR>
</Table>    

 <Div id= "divRegisterInfo" style="display: ''">
		  <table class=common>
		    <TR class=common style="display:'none'" >
		     	<TD  class= title8>申请人与被保人关系</TD><TD  class= input8><Input readOnly class="codeno" name=Relation  onClick="return showCodeList('llRelation',[this,RelationName],[0,1]);" onkeyup="showCodeListKeyEx('llRelation',[this,RelationName],[0,1]);" ><Input readOnly class="codename"  elementtype=nacessary name=RelationName></TD>   
		     	<TD  class= title8>受理方式</TD><TD  class= input8><Input readOnly class=codeno  name=RgtType onClick="showCodeList('LLAskMode',[this,RgtTypeName],[0,1]);" onkeyup="showCodeListKeyEx('LLAskMode',[this,RgtTypeName],[0,1]);"><Input readOnly class=codename name= RgtTypeName elementtype=nacessary verify="受理方式|notnull" ></TD>  
			 		<TD  class= title8>回执发送方式</TD><TD  class= input8>
			  	<Input readOnly class= codeno name=ReturnMode onClick="showCodeList('LLreturnMode',[this,ReturnModeName],[0,1]);" onkeyup="showCodeListKeyEx('LLreturnMode',[this,ReturnModeName],[0,1]);"><Input readOnly class= codename name=ReturnModeName >
			  	</TD>
			  </TR>
		    <TR  class= common>
					   <TD  class= title8>申请人姓名</TD><TD  class= input8><Input readOnly class= common name=RgtantName elementtype=nacessary verify="申请人姓名|notnull"></TD>
				     <TD class= title8>申请人证件号码</TD><TD class= input8><Input readOnly class= common name=IDNo_A></TD>
				     <TD class= title8>申请人证件类型</TD><TD class= input8> <Input readOnly class="codeno" name=IDType ><Input readOnly class="codename"  name=IDTypeName ></TD>
		    </TR>
		    <TR  class= common style="display:'none'">
		     <TD  class= title8 >申请人电话</TD><TD  class= input8><Input readOnly class= common name=RgtantPhone ></TD>
		     <TD  class= title8 >申请人地址</TD><TD  class= input8><Input readOnly class= common name=RgtantAddress ></TD>
		     <TD  class= title8>邮政编码</TD><TD  class= input8><Input readOnly class= common name="PostCode" verify="zipcode">
		     </TD>
		    </tr>
		  </table>
  </Div>
	<Div id= "divGetMode" style="display: ''">
  <table class=common>
     <TR  class= common>
			 <TD  class= title8>受益金领取方式</TD><TD  class= input8><Input readOnly class="codeno" name=paymode ><Input readOnly class="codename" name=paymodename  elementtype=nacessary verify="受益金领取方式|notnull" ></TD>
     <TD  class= title8></TD><TD  class= input8><Input readOnly class= common name="temp0" type=hidden ></TD>
     <TD  class= title8></TD><TD  class= input8><Input readOnly class= common name="temp1" type=hidden></td>
     </TR>
    </table>
  </Div>
  
	<div id='divBankAcc' style="display:'none'">
		<table  class= common>
			<TR  class= common8>
				<TD  class= title8>银行编码</TD>
				<TD  class= input8><Input readOnly class="codeno"  name=BankCode ><Input readOnly class="codename"  elementtype=nacessary name=BankName ></TD>
				<TD  class= title8>银行账号</TD><TD  class= input8><Input readOnly class= common name="BankAccNo"></TD>
				<TD  class= title8>银行账户名</TD><TD  class= input8><Input readOnly class= common name="AccName"></TD>
			</TR>
		</table>
	</div> 
<Table>
    <TR>
       <TD class=common>
	         <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    	 </TD>
    	 <TD class= titleImg>
    			 客户事件信息
    	 </TD>
    </TR>
</Table> 
  <Div  id= "divLDPerson1" style= "display: ''" align = center>
   	<Table  class= common>
			 <TR  class= common>
			 		<TD class=title style="width:10%">历史申请次数 <input readOnly class=common name=TotalApp></TD>
			 </TR>
       <TR  class= common>
        	<TD text-align: left colSpan=1>
            <span id="spanEventGrid" ></span> 
  		 		</TD>
      </TR>
    </Table>
	</Div> 
	<hr>
		<input type=button class=cssButton style="align:right" value="理赔申请查询" OnClick="ShowInfoPage();">
		<input type=button class=cssButton value="保单查询" OnClick="ContInfoPage();">
<!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="ComCode" value="<%=Comcode%>" >
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
