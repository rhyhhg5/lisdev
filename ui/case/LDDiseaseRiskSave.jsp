<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDDiseaseRiskSave.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.config.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDDiseaseSet tLDDiseaseSet   = new LDDiseaseSet();
  LDDiseaseRiskUI tLDDiseaseRiskUI   = new LDDiseaseRiskUI();
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tRadio[] = request.getParameterValues("InpDiseaseRiskGridSel");
  String tIcdcode[] = request.getParameterValues("DiseaseRiskGrid1");
  String tRisklevel[] = request.getParameterValues("DiseaseRiskGrid3");
  String tRiskcontinueflag[] = request.getParameterValues("DiseaseRiskGrid4");
  for (int index=0;index<tRadio.length;index++) {
	  if ("1".equals(tRadio[index])) {
		LDDiseaseSchema tLDDiseaseSchema = new LDDiseaseSchema();
		tLDDiseaseSchema.setICDCode(tIcdcode[index]);
		tLDDiseaseSchema.setRiskLevel(tRisklevel[index]);
		tLDDiseaseSchema.setRiskContinueFlag(tRiskcontinueflag[index]);
		tLDDiseaseSet.add(tLDDiseaseSchema);
	  }
  }
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLDDiseaseSet);
  	tLDDiseaseRiskUI.submitData(tVData,"UPDATE");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDDiseaseRiskUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success1";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
