<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LLUserWorkSta.jsp
//程序功能：
//创建日期：2005-04-17
//创建人  ：JavaBean
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>


 <%GlobalInput tG = new GlobalInput();
 tG=(GlobalInput)session.getValue("GI");
 
 
 String CurrentDate	= PubFun.getCurrentDate();   
 String CurrentTime	= PubFun.getCurrentTime();
 String Operator   	= tG.Operator;
%>

<head>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="LLRiskPayInput.js"></SCRIPT> 
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css> 
</head>

<body onload="initElementtype();">    
  <form action="" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> 
          	<Input class="code" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" 
          	ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" 
          	onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
          </TD>          
          <TD  class= title>统计起期</TD>
          <TD  class= input> 
          	<Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> 
          </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> 
          	<Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> 
          </TD> 
        </TR>
    </table>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title>险   种</TD>
          <TD  class= input colspan=3> 
          	<Input class="code" name=riskCode style="width:20%" verify="险种|notnull"
          	ondblclick= "return showCodeList('grpriskcode',[this,grpRiskName],[0,1],null,null,null,1);"
          	onkeyup= "return showCodeListKey('grpriskcode',[this,grpRiskName],[0,1],null,null,null,1);"><input class=codename name=grpRiskName style="width:60%" elementtype=nacessary  > </TD> 
          <TD  class= title>统   计</TD>
					<TD  class= input8> 
						<input class=codeno CodeData="0|6^0|分险种赔付率分析表^1|分客户赔付率分析表"  name=DealWith
							ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);" 
							onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);"><input class=codename name=DealWithName>
					</TD>	
			</tr>
			<TR >
          <TD  class= title></TD>
          <TD  class= input> 
          </TD>          
          <TD  class= title></TD>
          <TD  class= input> 
          </TD> 
          <TD  class= title></TD>
          <TD  class= input> 
          </TD> 
        </TR>
        <input type="hidden" name="Handler" value="<%=tG.Operator%>">
        <input type="hidden" name="CurrentDate" value="<%=CurrentDate%>">
        <input type="hidden" name="CurrentTime" value="<%=CurrentTime%>">
		</table>
		   <hr>
    <table class=common>
    	<tr class=common>
          <Input type =button class=cssButton value="打  印" onclick="Print()">	
			</tr>
		</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
