<%
//程序名称：LLSurveyFeeInit.jsp
//程序功能：直接调查费录入/业务审核初始化
//创建日期：2005-2-26 12:07
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
String CaseNo=request.getParameter("CaseNo");
String SurveyNo=request.getParameter("SurveyNo");
String LoadFlag=request.getParameter("LoadFlag");
%>

<script language="JavaScript">

	// 输入框的初始化（单记录部分）
	var turnPage=new turnPageClass();
	function initInpBox(){
		try{
			fm.CaseNo.value="<%=CaseNo%>";
			fm.SurveyNo.value="<%=SurveyNo%>";
			var LoadFlag = "<%=LoadFlag%>";
			if(LoadFlag=='0'){
				fm.butConfirm.style.display='none';
			}
		}
		catch(ex){
			alert("在LLSurveyFeeInit.jsp-->InitInpBox1函数中发生异常:初始化界面错误!");
		}
	}

	// 下拉框的初始化
	function initSelBox(){
		try{
		}
		catch(ex){
			alert("在LLSurveyFeeInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
		}
	}

	function initForm(){
		try{
			initInpBox();
			ClientQuery();
			initInqFeeGrid();
			initInqFeeDetailGrid();
			SurveyQuery();
		}
		catch(re){
			alert("LDPersonQueryInit.jsp-->InitForm函数中发生异常3:初始化界面错误!"+re.message);
		}
	}

	// 保单信息列表的初始化
	function initInqFeeDetailGrid()
	{
		var iArray = new Array();
		var strsqlinit1="1 and surveyno= #"+fm.all('CaseNo').value+fm.all('SurveyNo').value+"#";
		try
		{
			iArray[0]=new Array("","30px","10","3");
			iArray[1]=new Array("调查序号","60px","50","2");
			iArray[1][4]="llsurvey";
			iArray[1][5]="1|6|8";
			iArray[1][6]="0|1|2";
			iArray[1][15]="1";
			iArray[1][16]=strsqlinit1;

			iArray[2]=new Array("费用项目","150px","100","2");
			iArray[2][4]="llsurveyfee";
			iArray[2][5]="2|3";
			iArray[2][6]="1|0";
			iArray[2][18]=300;

			iArray[3]=new Array("费用项目代码","0px","50","3");
			iArray[4]=new Array("金额","40px","100","1");
			iArray[5]=new Array("调查机构","0px","50","3");
			iArray[6]=new Array("费用类别","50px","50","0");
			iArray[7]=new Array("审核状态","65px","50","0");
			iArray[8] = new Array("调查号","20px","50","3");

			InqFeeDetailGrid = new MulLineEnter( "fm" , "InqFeeDetailGrid" );
			InqFeeDetailGrid.mulLineCount = 3;
			InqFeeDetailGrid.displayTitle = 1;
			InqFeeDetailGrid.locked = 0;
			InqFeeDetailGrid.canSel = 0;
			InqFeeDetailGrid.canChk = 0;
			InqFeeDetailGrid.hiddenPlus=0;
			InqFeeDetailGrid.addEventFuncName="initAttr";
			InqFeeDetailGrid.hiddenSubtraction=0;
			InqFeeDetailGrid.loadMulLine(iArray);
		}
		catch(ex){
			alert(ex);
		}
	}
	
	function initInqFeeGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array("序号","30px","10","0");
			iArray[1]=new Array("调查序号","60px","100","0");
			iArray[2]=new Array("合计金额","40px","100","0");
			iArray[3]=new Array("录入人","40px","60","1");
			iArray[4]=new Array("审核人","40px","0","1");;
			InqFeeGrid = new MulLineEnter("fm","InqFeeGrid");
			InqFeeGrid.mulLineCount =3;
			InqFeeGrid.displayTitle = 1;
			InqFeeGrid.locked = 1;
			InqFeeGrid.canSel =0;
			InqFeeGrid.hiddenPlus=1;
			InqFeeGrid.hiddenSubtraction=1;
			InqFeeGrid.loadMulLine(iArray);
		}
		catch(ex){
			alter(ex);
		}
	}

	function initAttr(divID){
		fm.all("spanInqFeeDetailGrid" + InqFeeDetailGrid.maxSpanID).all('InqFeeDetailGrid6').value="直接调查费";
	}


</script>
