/*******************************************************************************
* Name: LLRegisterInput.js
* Function:立案主界面的函数处理程序
* Author:Dongjf
*/
var showInfo;
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();
/*******************************************************************************
* Name        :main_init
* Author      :LiuYansong
* CreateDate  :2003-12-16
* ModifyDate  :
* Function    :初始化主程序
*/

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //fm.comfirm1.disabled=false;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try{
    initForm();
  }
  catch(re){
    alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
  if(cDebug=="1"){
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else{
    parent.fraMain.rows = "0,0,0,0,*";
  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";
  }
}


//用于数据的提交。选择机构下需要的配置项
function submitForm(){
	if(fm.ManageCom.value==""){
		alert("必须输入管理机构！");
		return false;
	}
	fm.comfirm1.disabled=true;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="./ClientReasonSaveaaa.jsp";
  fm.submit();
}

//用于数据的查询，查询处已配置好的
function ClientQuery(){
    if(fm.ManageCom.value==""){
		alert("请选择管理机构！");
		return false;
	}
	//查询前清理模板字段对勾(导入模板字段加减需要修改)
    var lis= new Array();
    lis=fm.claimReasonCode;
    if(lis==null || lis ==""){
    	alert("未获得页面字段");
    	return false;
    }
	for (m=0; m<lis.length ;m++){
		fm.claimReasonCode[m].checked = false;
	}

	var strSQL="select code1,codename from ldcode1 where codetype='llimportfeeitem' "
	          +"and code='"+fm.ManageCom.value+"'";
	var arrResult = easyExecSql(strSQL);
	if ( arrResult!=null ) {
		for (i=0;i< arrResult.length ;i++){
			var ind= eval(arrResult[i][0])-31;
			fm.claimReasonCode[ind].checked = true;
		}
	}
}

function ToExcel()
{
  if (fm.all('ManageCom').value != '')
  {  	 	
   	fm.action="./GEdorAccountExecelSaveaaa.jsp";
   	fm.submit(); //提交
  }
  else
  {  	
    alert("请输入团体机构编码!");
    return;
  }  
}

function makeXml()
{
  if (fm.all('ManageCom').value != '')
  {  	 	
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   	fm.action="./LLMakeXml.jsp";
   	fm.submit(); //提交
  }
  else
  {  	
    alert("请输入团体机构编码!");
    return;
  }  
}
function ToVts()
{
  if (fm.all('ManageCom').value != '')
  {  	 	
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   	fm.action="./LLMakeVts.jsp";
   	fm.submit(); //提交
  }
  else
  {  	
    alert("请输入团体机构编码!");
    return;
  }
  showInfo.close();  
}

//用于数据的提交，合并了申请确认和生成xml文件。选择机构下需要的配置项
function submitmakeXml(){
	if(fm.ManageCom.value==""){
		alert("必须输入管理机构！");
		return false;
	}
	//fm.comfirm1.disabled=true;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="./ClientReasonSaveMakeXml.jsp";
  fm.submit();
}