
var showInfo;
var turnPage = new turnPageClass();

function easyQueryClick() {

	if (fm.RgtDateE.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == null) {
		alert("	请录入管理机构！");
		return false;
	}
	if(fm.RgtDateS.value == "" || fm.RgtDateS.value == null || fm.RgtDateE.value == "" || fm.RgtDateE.value == null){
		if(fm.RgtDateSS.value == "" || fm.RgtDateSS.value == null || fm.RgtDateSE.value == "" || fm.RgtDateSE.value == null){
			alert("	请输入受理日期或审批日期！");
			return false;
		}
	}
	if (fm.Type.value == "" || fm.Type.value == null) {
		alert("请录入案件类型！");
		return false;
	}else if (fm.Type.value != "1" && fm.Type.value != "2") {
		alert("案件类型选择有误，请重新选择！");
		return false;
	}
	//审批审定状态下案件查询
	var spart1 = "";
	var strSql = "select a.rgtno,a.caseno,a.CustomerName,a.CustomerNo,a.RgtDate,a.UWDate,a.endcasedate " +
			"from llcase a, llcaseoptime b " +
			"where 1 = 1 and a.caseno = b.caseno and b.rgtstate in ('05','06') and b.operator = '" + fm.Handler.value + "' ";
			
	if(fm.RgtDateS.value != '' && fm.RgtDateE.value != ''){
		spart1 = spart1 + "and a.RgtDate between '" + fm.RgtDateS.value + "' and '" + fm.RgtDateE.value + "' ";
	}
	if(fm.RgtDateSS.value != '' && fm.RgtDateSE.value != ''){
		spart1 = spart1 + "and a.UWDate between '" + fm.RgtDateSS.value + "' and '" + fm.RgtDateSE.value + "' ";
	}
	strSql = strSql + spart1 + 
			"and MngCom like '" + fm.ManageCom.value + "%' ";
	//抽检案件查询
	var spart2 = "";
	var strSql2 = "select distinct(a.rgtno),a.caseno,a.CustomerName,a.CustomerNo,a.RgtDate,a.UWDate,a.endcasedate " +
			"from llcase a, llcaseoptime b " +
			"where 1 = 1 and a.caseno = b.caseno and b.rgtstate = '10' and b.operator = '" + fm.Handler.value + "' ";
			
	if(fm.RgtDateS.value != '' && fm.RgtDateE.value != ''){
		spart2 = spart2 + "and a.RgtDate between '" + fm.RgtDateS.value + "' and '" + fm.RgtDateE.value + "' ";
	}
	if(fm.RgtDateSS.value != '' && fm.RgtDateSE.value != ''){
		spart2 = spart2 + "and a.UWDate between '" + fm.RgtDateSS.value + "' and '" + fm.RgtDateSE.value + "' ";
	}
	strSql2 = strSql2 + spart2 + 
			"and MngCom like '" + fm.ManageCom.value + "%' ";

	if (fm.Type.value == "1") {
		turnPage.queryModal(strSql, CaseInfo);
		if (CaseInfo.mulLineCount == 0) {
			alert("未查到相关信息，请重新查询！");
			return false;
		}
	} else {
		if (fm.Type.value == "2") {
			turnPage.queryModal(strSql2, CaseInfo);
			if (CaseInfo.mulLineCount == 0) {
				alert("未查到相关信息，请重新查询！");
				return false;
			}
		}
	}
}

// 查询返回
function afterCodeSelect( cCodeName, Field )
{
  //选择了处理
  if( cCodeName == "DealWith")
  {
    LoadC="1";
    LoadD="1";
    switch(fm.all('DealWith').value){
      case "0":
      DealConsult();             //咨询通知
      window.focus();              //咨询通知
      break;
      case "1":
      DealApp();                 //受理申请
      window.focus();              //咨询通知
      break;
      case "2":
      DealAppGrp();              //团体受理申请
      window.focus();              //咨询通知
      break;
      case "3":
      DealFeeInput();            //帐单录入
      window.focus();              //咨询通知
      break;
      case "4":
      DealCheck();               //检录
      window.focus();              //咨询通知
      break;
      case "5":
      DealClaim();               //理算
      window.focus();              //咨询通知
      break;
      case "6":
      DealApprove();             //审批审定
      window.focus();              //咨询通知
      break;
      case "7":
      DealCancel();							 //撤件
      window.focus();              //咨询通知
      break;
 	  case "8":
      openSubmittedAffix();					//材料退回登记
      window.focus();  
       break;
    }
  }
}

//咨询通知 -- ok
function DealConsult()
{
 
  var caseNo="";

 var customerno=CaseInfo.getRowColData(0,4);
 strsql = "select consultno from LLMainAsk a,LLAnswerinfo b where a.LogNo=b.LogNo and a.logerno='"+customerno+"' fetch first 1 rows only";
 arrResult=easyExecSql(strsql);
   if(arrResult != null)
  {
    caseNo = arrResult[0][0];
  }
  var varSrc="&CaseNo="+ caseNo;// 为了保证接口一致性，做了些调整
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseNote.jsp?Interface= LLMainAskInput.jsp"+varSrc,"咨询通知","left");

}

//受理申请 --ok
function DealApp()
{

  var selno = CaseInfo.getSelNo()-1;
  var caseNo="";
  var RgtNo="";
  if (selno >=0)
  {
    caseNo = CaseInfo.getRowColData( selno, 2);
  }
  strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }
  var varSrc= "&RgtNo="+ RgtNo + "&CaseNo="+ caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseRequest.jsp?Interface=LLRegisterInput.jsp"+varSrc,"","left");

}

//团体受理申请 --ok
function DealAppGrp()
{
  var selno = CaseInfo.getSelNo()-1;
  var caseNo="";
  var RgtNo="";
  if (selno >=0)
  {
    caseNo = CaseInfo.getRowColData( selno, 2);
  }
  
  strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
  arrResult = easyExecSql(strSQL);

  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }

  var varSrc="&RgtNo="+ RgtNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainGrpCheck.jsp?Interface=LLGrpRegisterInput.jsp"+varSrc,"","left");

}

//帐单录入 --ok
function DealFeeInput()
{	
  var CaseNo="";
  var selno = CaseInfo.getSelNo()-1;
  if (selno>=0)
  {
    CaseNo = CaseInfo.getRowColData(selno, 2);
  }
  var varSrc = "&CaseNo=" + CaseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow =OpenWindowNew("./FrameMainZD.jsp?Interface=ICaseCureInput.jsp" + varSrc,"","left");

}

//检录 --ok
function DealCheck()
{
  var caseNo ="";
  var selno = CaseInfo.getSelNo()-1;
  if (selno >=0)
  {
    caseNo = CaseInfo.getRowColData( selno, 2);
  }
  var varSrc="&CaseNo="+ caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+
  varSrc,"检录","left");

}

//理算 --OK
function DealClaim()
{
  var caseno = '';
  var selno = CaseInfo.getSelNo()-1;
  if (selno >=0)
  {
    caseno = CaseInfo.getRowColData(selno,2);
  }
  var varSrc="&CaseNo=" + caseno ;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseClaim.jsp?Interface=LLClaimInput.jsp"+
  varSrc,"理算","left");
}

//审批审定 --ok
function DealApprove()
{
  var selno = CaseInfo.getSelNo()-1;
  var caseNo = "";
  if (selno>=0)
  {
    caseNo = CaseInfo.getRowColData( selno, 2);
  }

  var varSrc = "&CaseNo=" + caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + varSrc,"","left");

}

//撤件 --ok
function DealCancel()
{
  var caseno = '';
  var selno = CaseInfo.getSelNo()-1;
  if (selno >=0)
  {
    caseno = CaseInfo.getRowColData(selno,2);
    strSQL="select CustomerNo,CustomerName from llcase where caseno='"+caseno+"'";
    arrResult = easyExecSql(strSQL);
    if(arrResult != null)
     {
      CustomerNo = arrResult[0][0];
      CustomerName=arrResult[0][1];
     }
     var varSrc="&CaseNo=" + caseno +"&CustomerNo="+CustomerNo+"&CustomerName="+CustomerName;
     varSrc += "&LoadD="+LoadD;
  showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }else{
    alert("请至少选择一条理赔记录！");
    return false;
  }
  
}

//材料退回登记 --ok
function openSubmittedAffix()
{
   var caseno = '';
   var selno = CaseInfo.getSelNo()-1;
   if (selno >=0)
  {
    caseno = CaseInfo.getRowColData(selno,2);
  }
  strSQL="select rgtno,rgtstate from llcase where caseno='"+caseno+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
    RgtState=arrResult[0][1];
  }
  if(RgtState=="09"||RgtState=="12")
  {
	  //showInfo = window.open("./SubmittedAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value);
	  pathStr="./SubmittedAffixMain.jsp?CaseNo="+caseno+"&RgtNo="+RgtNo+"&LoadD="+LoadD;
	  showInfo = OpenWindowNew(pathStr,"","middle");
	}else
		{
			alert("未结案，不能回退");
		}
}

