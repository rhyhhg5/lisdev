//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var arrDataSet;



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {   
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");     
  }
  else
  { 
  	
	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModelessDialog(urlStr,window,"status:no;help:0;close:0;resizable:1;dialogWidth:550px;dialogHeight:350px");   
 	initForm();
  }
}

function addInfo(addFlag,error)
{
	if (addFlag=="Fail")
	{
		alert("向打印队列添加数据失败!原因是："+error);
	}else
	{
		alert("成功添加进打印队列!");
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function resetFm()
{
    fm.reset();

}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{   
	
	var Actugetno = fm.mActugetno.value;
    var otherNo = fm.mOtherno.value;
   
    if(isNull(Actugetno) && isNull(otherNo)){
		alert("实付号、业务号至少录入一项！");
  	return false;
}
	// 初始化表格
	initPolGrid();
	var sql="";
	sql = " select Actugetno,Otherno, InsBankCode,InsBankAccNo,EnterAccDate,paymode,case when (select count(1) from ljfiget b where a.actugetno = b.actugetno )>0 then '是' else '否' end isExist  from ljaget a  where 1=1  "
		  + getWherePart( 'Actugetno','mActugetno' )
	    + getWherePart( 'Otherno','mOtherno'  );
	
	turnPage.queryModal(sql,PolGrid);
	if (!turnPage.strQueryResult)
	  {
	  	alert('查询失败,不存在有效纪录！');
	  	return false;
	  }
	 
}

//显示修改明细
function ShowDetail(){
	 var tSel = 0;
	 tSel = PolGrid.getSelNo();
		try
		{
			if (tSel !=0 )
			   divLLMainAskInput5.style.display = "";
			else
				 divLLMainAskInput5.style.display = "none";
				 
		  fm.all('getbankcode').value=PolGrid.getRowColData(tSel-1,3);
		  fm.all('getbankaccno').value=PolGrid.getRowColData(tSel-1,4);
		  fm.all('PayMode').value=PolGrid.getRowColData(tSel-1,6);
		  fm.all('reActuGetNo').value=PolGrid.getRowColData(tSel-1,1);
		  fm.all('EnterAccDate').value=PolGrid.getRowColData(tSel-1,5);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的接口" + ex );
		}
}
 
//充正
function AddJaGet()
{ 
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.mOperate.value = "DOPOSITIVE";
	fm.submit();
 }
//充负
function MinLJaGet(){
   
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.mOperate.value = "DONEGATIVE";
	fm.submit();
}
//修改
function ModLJaGet()
{   
	if (fm.getbankcode.value == "" || fm.getbankcode.value == null)
	{   
		alert('本方银行不能为空！');
		return;
	}
	if (fm.getbankaccno.value == null || fm.getbankaccno.value == "")
	{
		alert('银行帐号不能为空！');
		return;
	}
	if (fm.PayMode.value == null || fm.PayMode.value == "")
	{
		alert('支付方式不能为空！');
		return;
	}
	var regetbankcode = fm.getbankcode.value;
	var regetbankaccno = fm.getbankaccno.value;
	var rePayMode = fm.PayMode.value;
	var mActuGetNo = fm.reActuGetNo.value;
	
	
	
	fm.mOperate.value = "REPAIR";
	submitForm();
}

//删除数据
function DelLJaGet()
{
	fm.mOperate.value = "DELETE";
		
	 if (confirm("您确实想删除该记录吗?"))
   {
      var i = 0;
      var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      //fm.fmtransact.value = "DELETE||MAIN";
      fm.submit(); //提交
      fm.reActugetno.value = '';
      fm.reOtherno.value = '' ;
      fm.reManageCom.value = '' ;
      fm.reAgentCom.value = '' ;
      easyQueryClick();
    }
    else
    {
       alert("您取消了删除操作！");
    } 
}

//添加支行
function ChildBankButt()
{
	
	if (fm.BankCode.value == null ||fm.BankCode.value == "")
	{
		alert('请选择总行代码');
		return;
	}
	if (fm.ManageCom.value == null || fm.ManageCom.value == "")
	{
		alert('请选择支行所属地区');
		return;
	}
	if (fm.ChildBankName.value == null ||fm.ChildBankName.value == "")
	{
		alert('请录入支行或分行名称');
		return;
	}
	var ManageCom = fm.ManageCom.value;
	var bankcode = fm.BankCode.value;
	if(!valProFlag(ManageCom,bankcode)){
		return false;
	}
	if(ManageCom.length == 4){
		fm.mProvincialBanch.value = '1';
	}else{
		fm.mProvincialBanch.value = '0';
	}
	fm.mOperate.value = "CHILD";
	submitForm();
	
}
//添加总行
function HeadBankButt()
{
	if (fm.HeadBankName.value == null ||fm.HeadBankName.value == "")
	{
		alert('请录入总行名称');
		return;
	}
	fm.mOperate.value = "HEAD";
	submitForm();
}

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput())
	{
  var i = 0;
  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
	}
}

//判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}
