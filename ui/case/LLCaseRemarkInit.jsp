<%
//程序名称：LLCaseRemarkInit.jsp
//程序功能：理赔提调的初始化
//创建日期：
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	GlobalInput mG = new GlobalInput();
	mG=(GlobalInput)session.getValue("GI");
	String CaseNo = request.getParameter("CaseNo");
	%>

<script language="JavaScript">
function initInpBox(){
	try{
		fm.CaseNo.value = '<%= CaseNo%>';

	}
	catch(ex){
		alter("在LLCaseRemarkInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}

function initForm(){
	try{
		initInpBox();
		initLLCaseRemarkGrid();
		EasyQuery();
	}
	catch(re){
		alter("在LLCaseRemarkInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initLLCaseRemarkGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array("序号","30px","10","0");
		iArray[1]=new Array("机构名称","50px","30","0");
		iArray[2]=new Array("人员编码","50px","30","0");
		iArray[3]=new Array("人员姓名","80px","50","0");
		iArray[4]=new Array("记录日期","80px","100","0");
		iArray[5]=new Array("记录内容","300px","500","0");

		LLCaseRemarkGrid = new MulLineEnter( "fm" , "LLCaseRemarkGrid" );
		//这些属性必须在loadMulLine前
		LLCaseRemarkGrid.mulLineCount = 3;
		LLCaseRemarkGrid.displayTitle = 1;
		LLCaseRemarkGrid.locked = 1;
		//LLCaseRemarkGrid.canSel = 0;
		LLCaseRemarkGrid.hiddenPlus = 1;
		LLCaseRemarkGrid.hiddenSubtraction = 1;
		LLCaseRemarkGrid.loadMulLine(iArray);
		//LLCaseRemarkGrid.selBoxEventFuncName = "ShowContent";
	}
	catch(ex){
		alert(ex);
	}
}
function initLLCaseBackGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array("序号","30px","10","0");
		iArray[1]=new Array("案件号","80px","30","0");
		iArray[2]=new Array("案件回退前状态","100px","30","0");
		iArray[3]=new Array("案件回退到状态","100px","50","0");
		iArray[4]=new Array("回退前案件处理人","120px","100","0");
		iArray[5]=new Array("回退后案件处理人","120px","100","0");
		iArray[6]=new Array("案件回退日期","100px","500","0");
		iArray[7]=new Array("案件回退时间","100px","100","0");
		iArray[8]=new Array("案件回退操作人","100px","100","0");
		iArray[9]=new Array("案件回退原因","300px","100","0");

		LLCaseBackGrid = new MulLineEnter( "fm" , "LLCaseBackGrid" );
		//这些属性必须在loadMulLine前
		LLCaseBackGrid.mulLineCount = 3;
		LLCaseBackGrid.displayTitle = 1;
		LLCaseBackGrid.locked = 1;
		LLCaseBackGrid.canSel = 1;
		LLCaseBackGrid.hiddenPlus = 1;
		LLCaseBackGrid.hiddenSubtraction = 1;
		LLCaseBackGrid.loadMulLine(iArray);
		//ContGrid.selBoxEventFuncName = "ShowContent";
	}
	catch(ex){
		alert(ex);
	}
}


</script>