//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();

function submitForm()
{
  var comlimit = "";
  var timelimit = "";
  if(fm.comcode.value.length==2)
    comlimit="000000";
  if(fm.comcode.value.length==4)
    comlimit=fm.comcode.value.substring(2)+"0000";
  if(fm.comcode.value.length==8)
    comlimit=fm.comcode.value.substring(2);
  if(fm.Month.value.length==1)
    timelimit = fm.Year.value+"0"+fm.Month.value;
  else
    timelimit = fm.Year.value+fm.Month.value;
  fm.BatchNo.value = comlimit+timelimit;
  var temsql = "select actugetno from ljaget where otherno in ('C"+fm.BatchNo.value+"','F"+fm.BatchNo.value+"')";
  var arr = easyExecSql(temsql);
  if(arr){
    if(!confirm("本月的查勘费用已结，您确认要修改吗？"))
      return false;
  }
  var rowNum=SurveyCaseGrid. mulLineCount ; 
  	fm.fmtransact.value = "GIVE||ENSURE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LLSurvFeeSumSave.jsp";
    fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


 
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		updateCaseGrid();
		queryIndFee();
	}
}

function SurveyQuery()
{
}

function makesql()
{
	strsqlinit1="1 and otherno= #"+fm.all('CaseNo').value+"#";
}

////////
function afterQuery1()
{	
}
//////////


  
function afterCodeSelect(ObjType,Obj)
{
	if(ObjType=='Month'){
		updateCaseGrid();
		queryIndFee();
	}
}

function updateCaseGrid()
{
	month=fm.Month.value/1;
	year= fm.Year.value/1;
	days=getDays(month, year);
	fm.MStartDate.value = year+"-"+month+"-"+"01";
	fm.MEndDate.value = year+"-"+month+"-"+days;
	strSql0 = "select a.otherno,substr(a.surveyno,18,1),a.inqfee,a.indirectfee," 
	          + "a.inqfee+a.indirectfee,a.inputer,a.confer from llinqfeesta a,";
	strSqlpart1 = "llcase b where a.otherno =b.caseno and b.rgtstate in ('11','12') ";
	strSqlpart2 = "llconsult b where a.otherno = b.consultno ";
	strSql1 = " and a.payed is null "+getWherePart("a.inqdept","comcode","like")
	+getWherePart("b.makedate","MEndDate","<=")
	+getWherePart("b.makedate","MStartDate",">=");
  strSql = strSql0 + strSqlpart1+strSql1 + " union " + strSql0 + strSqlpart2+strSql1;
	turnPage.queryModal(strSql,SurveyCaseGrid);
}

function SurvFeePrt()
{
  var comlimit = "";
  var timelimit = "";
  if(fm.comcode.value.length==2)
    comlimit="000000";
  if(fm.comcode.value.length==4)
    comlimit=fm.comcode.value.substring(2)+"0000";
  if(fm.comcode.value.length==8)
    comlimit=fm.comcode.value.substring(2);
  if(fm.Month.value.length==1)
    timelimit = fm.Year.value+"0"+fm.Month.value;
  else
    timelimit = fm.Year.value+fm.Month.value;
  fm.BatchNo.value = comlimit+timelimit;
  var newWindow =OpenWindowNew("SurvFeePrt.jsp?Year="+fm.Year.value+"&Month="+fm.Month.value+"&BatchNo="+fm.BatchNo.value,"SurvFeePrt","left" );
}

