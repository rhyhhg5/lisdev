<%
//Name：LLClaimInput.jsp
//Function：理算页面 
//Date：2004-12-23 16:49:22
//Author：Wujs,Xx
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<head >
		<SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="./CaseCommon.js"></SCRIPT>
		<SCRIPT src="LLTaskTest.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	</head>

	<body  onload="initForm();" > 
		<!--立案分案信息部分（列表） -->
		<form action="./LLTaskTestSave.jsp" method=post name=fm target="fraSubmit">

	<div id=divconfirm align='right'>
	    <!-- #3265，河南分公司向保监每日报送数据报表调整 -->
	    <input class=CssButton type=button value="河南每日报送数据" name="getSubmitData" onclick="submitData()">
		
		<!--#3302 短信调试  -->
		<input class=CssButton type=button value="短信调试测试" name="Message" onclick="testMessage();">
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divXSB);">
				</td>
				<td class= titleImg>
					新社保操作按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divXSB" style= "display: ''" align = left>
		<table align = left>
			<tr>
				<td class=common>
		<input class=CssButton style='width: 60px;' type=button value="获取报文" name="getSocialClaim" onclick="SocialClaim()" id = "sub12">
				</td>
			</tr>
		</table>
		</Div>
		<br>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTYJK);">
				</td>
				<td class= titleImg>
					统一接口的操作按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divTYJK" style= "display: ''" align = left>
			<input class=CssButton type=button value="统一-承保TB" name="ty-tb" onclick="tytb();" id = "sub24">
			<input class=CssButton type=button value="统一-保全BQ" name="ty-bq" onclick="tybq();" id = "sub25">
			<input class=CssButton type=button value="统一结案-批处理" name="tyjiean-pcl" onclick="tyjiean();" id = "sub26">
		</Div>
		<br/>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPCL);">
				</td>
				<td class= titleImg>
					批处理操作按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divPCL" style= "display: ''" align = left>
			<!-- 税优批处理-->
			<input class=CssButton style='width: 100px;' type=button value="税优批处理" name="getuploadClaim" onclick="uploadClaim()" id = "sub15">
			<!-- 医保通天津社保理赔批处理测试-->
			<input class=CssButton style='width: 180px;' type=button value="医保通天津社保理赔批处理测试" name="getybtTJSB" onclick="ybtTJSB()" id = "sub14">
			<!-- 一卡通批处理测试-->
			<input class=CssButton style='width: 160px;' type=button value="一卡通批处理测试" name="getOneCard" onclick="onecard()" id = "sub13">
			<input class=CssButton type=button value="结案-批处理" name="jiean-pcl" onclick="jiean();" id = "sub23">
			
		</Div>
		<br/>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSZ);">
				</td>
				<td class= titleImg>
					深圳操作按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divSZ" style= "display: ''" align = left>
			<!-- #3120，深圳提数FTP服务器的相关软件开发，新增按钮 start-->
			<input class=CssButton type=button value="深圳理赔提数至FTP服务器" name="ToTansFtp" onclick="transFtp();">
			<!-- #3120 end-->
			<!-- #3366, 深圳FTP理赔数据 star -->
			<input class=CssButton type=button value="深圳理赔提数实时更新" name="ToTansFtp" onclick="transFtpLP();">
			<!-- #3366, end -->
			<input class=CssButton style='width: 60px;' type=button value="批次" name="SubCalculate" onclick="submitForm();" id = "sub3">
			<input class=CssButton style='width: 100px;' type=button value="案件" onclick="openCaseRemark()" id = "sub9">		
			<input class=CssButton type=button value="扫描件" name="AccValue" onclick="searchAccValue();" id = "sub4">
			<input class=CssButton type=button value="理赔结果" name="result" onclick="lpresult();" id = "sub4">
			<input class=CssButton type=button value="既往理赔结果" name="result1" onclick="cpresult();" id = "sub4">
		</Div>
		<br/>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divZJ);">
				</td>
				<td class= titleImg>
					浙江操作按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divZJ" style= "display: ''" align = left>
			<input class=CssButton type=button value="浙江-WB01" name="zj-wb1" onclick="zjwb1();" id = "sub16">
			<input class=CssButton type=button value="浙江-WB02" name="zj-wb2" onclick="zjwb2();" id = "sub17">
			<input class=CssButton type=button value="浙江-扫描件" name="zj-smj" onclick="zjsmj();" id = "sub18">
			<input class=CssButton type=button value="浙江-WB03" name="zj-wb3" onclick="zjwb3();" id = "sub19">
			<input class=CssButton type=button value="浙江-WB04" name="zj-wb4" onclick="zjwb4();" id = "sub20">
			<input class=CssButton type=button value="浙江-承保TB" name="zj-tb" onclick="zjtb();" id = "sub21">
			<input class=CssButton type=button value="浙江-保全BQ" name="zj-bq" onclick="zjbq();" id = "sub22">
		</Div>
		<br/>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSH);">
				</td>
				<td class= titleImg>
					上海操作按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divSH" style= "display: ''" align = left>
			<input class=CssButton type=button value="上海-批次处理(团HW01)" name="sh-hw1" onclick="shhw1();" id = "sub27">
			<input class=CssButton type=button value="上海-分案处理(团HW02)" name="sh-hw2" onclick="shhw2();" id = "sub28">
			<input class=CssButton type=button value="上海-扫描件处理(团)" name="sh-hwsmj" onclick="shhwsmj();" id = "sub29">
			<input class=CssButton type=button value="上海-理赔完成反馈(团HW03)" name="sh-hw3" onclick="shhw3();" id = "sub30">
			<input class=CssButton type=button value="上海-既往理赔处理(团HW04)" name="sh-hw4" onclick="shhw4();" id = "sub31">
		</Div>
	
	
	<!-- 批次多事件 -->
	<br/>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSH);">
				</td>
				<td class= titleImg>
					批次多事件按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divSH" style= "display: ''" align = left>
			<input class=CssButton type=button value="沛合-批次处理(TP01)" name="ph-tp1" onclick="phtp1();" id = "sub32">
			<input class=CssButton type=button value="沛合-分案处理(TP02)" name="ph-tp2" onclick="phtp2();" id = "sub33">
			<input class=CssButton type=button value="沛合-理赔完成反馈(TP03)" name="ph-tp3" onclick="phtp3();" id = "sub34">
			<input class=CssButton type=button value="沛合-既往理赔处理(TP04)" name="ph-tp4" onclick="phtp4();" id = "sub35">
		</Div>
	
	<!-- 影像件处理 -->
	<br/>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSH);">
				</td>
				<td class= titleImg>
					影像件处理按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divSH" style= "display: ''" align = left>
			<input class=CssButton type=button value="影像件处理(Image01)" name="TY-tp2" onclick="TYImage();" id = "sub36">
		</Div>
				
	<!-- 天津大病 -->
	<br/>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTJ);">
				</td>
				<td class= titleImg>
					天津大病按钮
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divTJ" style= "display: ''" align = left>
			<input class=CssButton type=button value="天津-批次处理(DB01)" name="tj-db1" onclick="tjdb1();" id = "sub37">
			<input class=CssButton type=button value="天津-分案处理(DB02)" name="tj-db2" onclick="tjdb2();" id = "sub38">
			<input class=CssButton type=button value="天津-诊疗明细(DB03)" name="tj-db3" onclick="tjdb3();" id = "sub39">
		</Div>
	<!-- 线上理赔状态推送 -->
	<br/>
		<br>
		<hr>
		<br>
		<table align = left>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSH);">
				</td>
				<td class= titleImg>
					线上理赔状态推送
				</td>
			</tr>
		</table>
		<br/>
		<Div  id= "divOC" style= "display: ''" align = left>
			<input class=CssButton type=button value="理赔状态推送(OC)" name="OC" onclick="OCPush();" id = "sub40">
		</Div>
	<br/>		
	
	</div>
		
	<Input type=hidden name="Opt">
	<Input type=hidden name="LoadFlag" id="LoadFlag" value="2">
	<input name ="LoadC" type="hidden">
	<input name ="LoadD" type="hidden">
	<input type=hidden name="ShowCaseRemarkFlag">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
