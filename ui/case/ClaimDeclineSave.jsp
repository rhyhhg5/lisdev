<%
//程序名称：LLClaimSave.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  CErrors tError = null;
  String transact="INSERT";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  VData tVData = new VData();
  
  LLClaimDeclineSchema tLLClaimDeclineSchema   = new LLClaimDeclineSchema();
  LLClaimDeclineSet tLLClaimDeclineSet=new LLClaimDeclineSet();
  ClaimDeclineUI tClaimDeclineUI   = new ClaimDeclineUI();

  
 
  String strDeclineType=request.getParameter("DeclineType");
  String strRelationNo=request.getParameter("RelationNo");
  String strReason=request.getParameter("Reason");
  String strDeclineDate=request.getParameter("DeclineDate");
  String strArchiveNo=request.getParameter("ArchiveNo");
  String strHandler=request.getParameter("Handler");
  String strMngCom=request.getParameter("MngCom");
  String strRgtNo=request.getParameter("RgtNo");
  String strOpt=request.getParameter("Opt");
  
  
  tLLClaimDeclineSchema.setDeclineType(strDeclineType);
  tLLClaimDeclineSchema.setRelationNo(strRelationNo);
  tLLClaimDeclineSchema.setReason(strReason);
  tLLClaimDeclineSchema.setDeclineDate(strDeclineDate);
  tLLClaimDeclineSchema.setArchiveNo(strArchiveNo);
  tLLClaimDeclineSchema.setHandler(strHandler);
  tLLClaimDeclineSchema.setMngCom(strMngCom);
  tLLClaimDeclineSchema.setRgtNo(strRgtNo);
  tLLClaimDeclineSet.add(tLLClaimDeclineSchema);
  
  if (strOpt.equals("add"))
  	transact="INSERT";
  if (strOpt.equals("update"))
	transact="UPDATE";
  if (strOpt.equals("delete"))
	transact="DELETE";

  try
  {
  // 准备传输数据 VData
   //此处需要根据实际情况修改
   tVData.addElement(tLLClaimDeclineSet);    
   tVData.addElement(tG);
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tClaimDeclineUI.submitData(tVData,transact);

  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  System.out.println("start....");
  if (FlagStr=="")
  {
    tError = tClaimDeclineUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
      	tVData.clear();
	tVData = tClaimDeclineUI.getResult();
	LLClaimDeclineSet ttLLClaimDeclineSet = new LLClaimDeclineSet();
	ttLLClaimDeclineSet.set((LLClaimDeclineBLSet)tVData.getObjectByObjectName("LLClaimDeclineBLSet",0));
	String tDeclineNo = ttLLClaimDeclineSet.get(1).getDeclineNo();
	System.out.println("declineno==="+tDeclineNo);
	
	if (transact.equals("INSERT"))
	{
%>
  	<script language="javascript">
		parent.fraInterface.fm.all("DeclineNo").value="<%=tDeclineNo%>";
	</script>  	
<%
	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
//保存拒赔信息
%>            
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>