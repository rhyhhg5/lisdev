 <%
//Name:LLSecuPolicyInputInit.jsp
//function：社保政策配置初始化页面
//author:Xx
//Date:2006-08-21
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
  function initInpBox(){
    try{

    }
    catch(ex){
      alter("LLSecuPolicyInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
  }

  function initSelBox(){
     fm.all('RgtNo').value="<%=request.getParameter("RgtNo")%>";
     
  }

  function initForm(){
    try{
    	initSelBox();
      initGetModeGrid();
      getCon();
    }
    catch(re){
      alter("LLSecuPolicyInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
  }


  function getCon()
  {
    var sql = "select caseno,customerno,customername from llcase where rgtno = '"+fm.all('RgtNo').value+"'";
    turnPage.queryModal(sql,GetModeGrid);
  }
  
  
  
function initGetModeGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="理赔号";         		//列名
      iArray[1][1]="200px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="客户号";         		//列名                     
      iArray[2][1]="200px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[3]=new Array();                                                 
      iArray[3][0]="客户姓名";         		//列名                     
      iArray[3][1]="200px";            		//列宽                             
      iArray[3][2]=100;            			//列最大值                           
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              

      GetModeGrid = new MulLineEnter( "fm" , "GetModeGrid" ); 
      //这些属性必须在loadMulLine前
      GetModeGrid.mulLineCount = 5;   
      GetModeGrid.displayTitle = 1;
      GetModeGrid.locked = 1;
      GetModeGrid.canSel = 1;
      GetModeGrid.hiddenPlus = 1;
      GetModeGrid.hiddenSubtraction = 1;
      GetModeGrid.loadMulLine(iArray);
      GetModeGrid.selBoxEventFuncName ="ShowScan";
      
  
      
      //这些操作必须在loadMulLine后面
      //ContGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
  
  


</script>