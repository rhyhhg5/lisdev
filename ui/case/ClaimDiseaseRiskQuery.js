//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var tDisplay;

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function getQueryDetail()
{  
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = PolGrid.getRowColData(tSel - 1,2);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		
		if (cPolNo == "")
		    return;
		    
		var GrpPolNo = PolGrid.getRowColData(tSel-1,1);
                var prtNo = PolGrid.getRowColData(tSel-1,3);
        //alert("dfdf");
        if( tIsCancelPolFlag == "0"){
	    	if (GrpPolNo =="00000000000000000000") {
	    	 	window.open("./AllProQueryMain.jsp?LoadFlag=6&prtNo="+prtNo,"window1");	
		    } else {
			window.open("./AllProQueryMain.jsp?LoadFlag=4");	
		    }
		} else {
		if( tIsCancelPolFlag == "1"){//销户保单查询
			if (GrpPolNo =="00000000000000000000")   {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} else {
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	    } else {
	    	alert("保单类型传输错误!");
	    	return ;
	    }
	 }
 }
}

//销户保单的查询函数
function getQueryDetail_B()
{
	
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	  var cPolNo = PolGrid.getRowColData(tSel - 1,1);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		if (cPolNo == "")
			return;
		var GrpPolNo = PolGrid.getRowColData(tSel-1,6);
	    if (GrpPolNo =="00000000000000000000") 
	    {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} 
			else 
			{
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	}
}



// 保单明细查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cContNo = PolGrid.getRowColData(tSel - 1,1);				
		if (cContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0");	
	}
}

// 基本信息查询
function FunderInfo()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cInsuredNo = PolGrid.getRowColData(tSel - 1,4);				
		if (cInsuredNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/CQPersonMain.jsp?CustomerNo="+cInsuredNo);	
	}
}
//个人业务
function PersonBiz()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var ContNo = PolGrid.getRowColData(tSel - 1,1);				
		if (ContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&IsCancelPolFlag=0");	
	}
}
// 查询按钮
function easyQueryClick()
{
    
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select ICDCode,ICDName,DiseasDegree,Sex,Age,RiskKind,OPSFlag,UWResult,OPSNo,DiseasBTypeCode,DiseasType,DiseasSTypeCode,DiseasSType,RiskGrade,OPSName,Relapse,DisText,GestationRela,State,ObservedTime,JudgeStandard,Year from LDUWAddFee where 1=1"
//						+ getWherePart("ICDCode","MainDiseaseNo")+ getWherePart("ICDName","MainDiseaseKind"); 
					+" and ICDCode like '%%"+fm.all('MainDiseaseNo').value+"%%' and ICDName like '%%"+fm.all('MainDiseaseKind').value+"%%' ";
		
		
						
//  if(fm.InsuredNo.value.trim()!="")strSQL = strSQL + if(fm.InsuredNo.value.trim()!="")strSQL = strSQL + getWherePart( 'InsuredNo' );;
// else if(fm.IDNo.value.trim()!="")strSQL = strSQL + "and InsuredNo in (select InsuredeNo from LcinsuredNo where IDNo = '"+fm.IDNo.value.trim()+"' and IDType = 1)";
// strSQL = strSQL+ getWherePart( 'InsuredName' )        				 
//            + getWherePart( 'AgentName' )
//            + " and ManageCom like '" + comCode + "%%'"
//	            + "order by ContNo";
	           
	  //          t(turnPage);
	//if(turnPage==undefined)
	//    var turnPage = new turnPageClass();

	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
   
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //showCodeName();
}




// 数据返回父窗口
function returnParent()
{
	//alert(tDisplay);
	if (tDisplay=="1")
	{
	    var arrReturn = new Array();
	    var tSel = PolGrid.getSelNo();
	    //alert(tSel);
	    if( tSel == 0 || tSel == null )
	    	alert( "请先选择一条记录，再点击返回按钮。" );
	    else
	    {
	    	try
	    	{
	    		arrReturn = getQueryResult();
	    		
	    		top.opener.afterQuery( arrReturn );
	    		
	    		top.close();
	    	}
	    	catch(ex)
	    	{
	    		alert( "请先选择一条非空记录，再点击返回按钮。");
	    		
	    	}
	    	
	    }
	 }
	 else
	 {
	    top.close(); 
	 }
}

function getQueryResult()
{
	var arrSelected = null;
	var tRow = PolGrid.getSelNo();
	
	if( tRow == 0 || tRow == null || arrDataSet == null )
		      return arrSelected;
	
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	
	return arrSelected;
}