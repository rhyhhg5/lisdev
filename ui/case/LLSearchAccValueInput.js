//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 

var showInfo;
var mDebug="1";
var mPolNo = "";
var mInsuAccNo = "";

//提交，保存按钮对应操作
function submitForm()
{
	if (fm.CalDate.value=="" || fm.CalDate.value==null) {
		alert("查询日期不能为空");
		return false;
	}
	var tDays = -1;
	var sql = "select days(current date)-days('"+fm.CalDate.value+"') from dual with ur";
  var arrReturn = easyExecSql(sql);
  if(arrReturn != null) {
  	tDays = arrReturn[0][0];
  }

	if (tDays < 0) {
		alert("查询日期不能在今天之后！");
		return false;
	}
	
	if (fm.PolNo.value=="" || fm.PolNo.value==null 
	    || fm.InsuaccNo.value=="" || fm.InsuaccNo.value==null) {
	   alert("请选择保单");
	   return false;
	}
  var i = 0;
  fm.fmtransact.value="Query"
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  showSubmitFrame(mDebug);
  fm.action = './LLSearchAccValueSave.jsp';
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
   // showDiv(inputButton,"false");
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
     parent.fraMain.rows = "0,0,0,0,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function easyQuery(){
  if (fm.InsuredNo.value==""||fm.InsuredNo.value==null) {
  	alert("未获得客户号");
  	return false;
  }
  
  
	var strSQL = "select distinct contno,a.riskcode,b.riskname,a.cvalidate,a.enddate,a.polno,d.insuaccno "
	           + " from lcpol a,lmriskapp b,lmrisktoacc c,LMRiskInsuAcc d "
	           + " where a.appflag='1' and a.riskcode=b.riskcode and c.riskcode=a.riskcode"
	           + " and b.risktype4='4' and d.acctype='002' and c.insuaccno = d.insuaccno "
	           + getWherePart('a.insuredno','InsuredNo');
	turnPage.queryModal(strSQL,PolGrid);
}

function onSelSearchAcc() {
	var row=PolGrid.getSelNo()-1;
	if(row>=0)
	{
		fm.PolNo.value = PolGrid.getRowColData(row,6);;
		fm.InsuaccNo.value=PolGrid.getRowColData(row,7);
	}
  
	mPolNo = fm.PolNo.value;
	mInsuAccNo = fm.InsuaccNo.value;
	
	if (mPolNo!=""&&mPolNo!=null&&mInsuAccNo!=""&&mInsuAccNo!=null) {
		queryAcc();
		queryEndDate();
		queryAccTrace();
  }
}

//查询万能帐户信息 包括：账户建立时间、账户当前金额、最近结算月份、最近结算时间
function queryAcc()
{
  /*
  var sql = "select a.AccFoundDate, a.InsuAccBala, "
          + "    ltrim(rtrim(char(year(b.DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(b.DueBalaDate - 1 day)))), "
          + "    b.MakeDate,DueBalaDate "
          + "from LCInsureAcc a , LCInsureAccBalance b "
          + "    where a.PolNo = '" + mPolNo + "' "
          + "        and  a.InsuAccNo = '" + mInsuAccNo + "' "
          + "        and  b.PolNo=a.PolNo and b.InsuAccNo = a.InsuAccNo "
          + "union all select a.AccFoundDate, a.InsuAccBala, "
          + "    ltrim(rtrim(char(year(b.DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(b.DueBalaDate - 1 day)))), "
          + "    b.MakeDate,DueBalaDate  "
          + "from LBInsureAcc a , LCInsureAccBalance b "
          + "    where a.PolNo = '" + mPolNo + "' "
          + "        and  a.InsuAccNo = '" + mInsuAccNo + "' "
          + "        and  b.PolNo=a.PolNo and b.InsuAccNo = a.InsuAccNo "
          + "    order by DueBalaDate desc ";
  var arrReturn = easyExecSql(sql);
  fm.all('AccFoundDate').value = arrReturn[0][0];
  fm.all('InsuAccBala').value = arrReturn[0][1];
  fm.all('BalaMonth').value = arrReturn[0][2];
  fm.all('BalaDate').value = arrReturn[0][3];
  */
  //拆了
  var sql = "select a.AccFoundDate, a.InsuAccBala "
          + "from LCInsureAcc a "
          + "where a.PolNo = '" + mPolNo + "' "
          + "and  a.InsuAccNo = '" + mInsuAccNo + "' "
          + "union all select a.AccFoundDate, a.InsuAccBala "
          + "from LBInsureAcc a "
          + "where a.PolNo = '" + mPolNo + "' "
          + "and  a.InsuAccNo = '" + mInsuAccNo + "' ";
  var arrReturn = easyExecSql(sql);
  if(arrReturn != null)
  {
    fm.all('AccFoundDate').value = arrReturn[0][0];
    fm.all('InsuAccBala').value = arrReturn[0][1];
  }
  
      sql = "select "
          + "ltrim(rtrim(char(year(b.DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(b.DueBalaDate - 1 day)))), "
          + "b.MakeDate,DueBalaDate "
          + "from  LCInsureAccBalance b "
          + "where b.PolNo = '" + mPolNo + "' "
          + "and  b.InsuAccNo = '" + mInsuAccNo + "' "
          + "order by DueBalaDate desc ";
  var arrReturn1 = easyExecSql(sql);
  if(arrReturn1 != null)
  {        
    fm.all('BalaMonth').value = arrReturn1[0][0];
    fm.all('BalaDate').value = arrReturn1[0][1];
  }
  return true;
}

//查询帐户结束时间
function queryEndDate()
{
  var sql = "select b.EdorValiDate from LBPol a ,LPEdorItem b "
          + "    where a.PolNo = '" + mPolNo + "'"
          + "        and b.PolNo = a.PolNo and b.ContNo = a.ContNo ";
  var arrReturn = easyExecSql(sql);
  if(arrReturn != null)
  {
    fm.all('AccEndDate').value = arrReturn[0][0];
  }
  return true;
}

//查询帐户历史信息
function queryAccTrace()
{
  var sql = "select a.PayDate, a.MoneyType, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    (case when a.OtherType = '6' " 
          + "             then ltrim(rtrim(char(year(a.PayDate - 1 day))))||'-'||ltrim(rtrim(char(month(a.PayDate - 1 day)))) "
          + "          else '' end), "
          + "    a.OtherNo, "
          + "    varchar(nvl((case when a.Money >= 0 then a.Money end),0)), "
          + "    varchar(nvl((case when a.Money <= 0 then abs(a.Money) end),0)), "
          + "    varchar(nvl((select sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
          + "    a.SerialNo x "
          + "from LCInsureAccTrace a, LCInsureAcc b "
          + "where a.PolNo = '" + mPolNo + "'" 
          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType != '1' "
          + "    group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "
          
          + "union all select a.PayDate, a.MoneyType, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    (case when a.OtherType = '6' " 
          + "             then ltrim(rtrim(char(year(a.PayDate - 1 day))))||'-'||ltrim(rtrim(char(month(a.PayDate - 1 day)))) "
          + "          else '' end), "
          + "    a.OtherNo, "
          + "    varchar(nvl((case when a.Money >= 0 then a.Money end),0)), "
          + "    varchar(nvl((case when a.Money <= 0 then abs(a.Money) end),0)), "
          + "    varchar(nvl((select sum(Money) from LBInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
          + "    a.SerialNo x "
          + "from LBInsureAccTrace a, LBInsureAcc b "
          + "where a.PolNo = '" + mPolNo + "'" 
          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType != '1' "
          + "    group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "

          + "union all "
          
          //契约进帐户的轨迹
          + "select a.PayDate, a.MoneyType, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    '', "
          + "    a.OtherNo, "
          + "    varchar((select sum(Prem) from LCPrem where PolNo = a.PolNo)), "
          + "    varchar((select sum(Prem) from LCPrem where PolNo = a.PolNo) - a.Money), "
          + "    varchar(nvl((select sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
          + "    a.SerialNo x "
          + "from LCInsureAccTrace a, LCInsureAcc b "
          + "where a.PolNo = '" + mPolNo + "'" 
          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType = '1' "
          + "group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "
          //契约进帐户的轨迹，B表
          + "union all select a.PayDate, a.MoneyType, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    '', "
          + "    a.OtherNo, "
          + "    varchar((select sum(Prem) from LCPrem where PolNo = a.PolNo)), "
          + "    varchar((select sum(Prem) from LCPrem where PolNo = a.PolNo) - a.Money), "
          + "    varchar(nvl((select sum(Money) from LBInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
          + "    a.SerialNo x "
          + "from LBInsureAccTrace a, LCInsureAcc b "
          + "where a.PolNo = '" + mPolNo + "'" 
          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType = '1' "
          + "group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "
          
          + "order by x ";
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(sql, OmnipotenceAccTraceGrid);
  return true;
}