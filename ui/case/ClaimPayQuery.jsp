
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.llcase.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%
	String tDisplay = "";
	String tContNo="";
	try
	{
		tDisplay = request.getParameter("display");
	}
	catch( Exception e )
	{
		tDisplay = "";
	}
	
	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  String Operator=tG.Operator;
  String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
  String AheadDays="-30";
        FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
	System.out.println("管理机构-----"+tG.ComCode);
%>   

<script language="javascript">
  var comCode = <%=tG.ComCode%>
  function initDate(){
   		fm.RgtDateS.value="<%=afterdate%>";
   		fm.RgtDateE.value="<%=CurrentDate%>";
   		var usercode="<%=Operator%>";
   		var comcode="<%=Comcode%>";
//   		fm.Operator.value=usercode;
//   		fm.OrganCode.value=comcode;
   		
   		
//   		var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
//   		var arrResult = easyExecSql(strSQL);
//			if(arrResult != null)
//			{
//			         fm.optname.value= arrResult[0][0];
// 			}
   }
</script>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="ClaimPayQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <%@include file="ClaimPayQueryInit.jsp"%>
  
  <title>理赔给付记录查询 </title>
</head>
<script>
  var turnPage = new turnPageClass(); 
</script>
<body  onload="initForm(); initDate();ClaimPayQuery();" >
  <br>
  <form method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
        <TR  class= common>
          <TD  class= title>客户姓名 </TD>
          <TD  class= input> <Input class= common name=CustomerName onkeydown="QueryOnKeyDown()"> </TD>
          <TD  class= title> 客户号码</TD>
          <TD  class= input> <Input class= common name=CustomerNo onkeydown="QueryOnKeyDown()"> </TD>
          <TD  class= title> 身份证号码 </TD>
          <TD  class= input> <Input class= common name=IDNo onkeydown="QueryOnKeyDown()"> </TD>
        </TR>
         <TR  class= common style="display:''">
	        <TD  class= title>理赔号</TD>
          <TD  class= input> <Input class= common name=CaseNo onkeydown="QueryOnKeyDown()"> </TD>
          <TD  class= title8>受理日期从</TD>
			    <TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>到</TD>
					<TD  class= input8><input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
        </TR>
        <TR  class= common >
        	 <TD  class= title>领款人姓名</TD>
        	 <TD  class= input> <Input class= common name=Drawer onkeydown="QueryOnKeyDown()"> </TD>
         </TR>
        <TR  class= common style  = "display:'none'">          
          <TD  class= title> 代理人编码</TD>
          <TD  class= input> <Input class="code" name=AgentCode  > </TD>
          <TD  class= title> 代理人组别 </TD>
          <TD  class= input> <Input class=common name=AgentGroup verify="代理人组别|notnull&len<=12" > </TD>
          <TD  class= title> 管理机构 </TD>
          <TD  class= input> <Input class="code" name=ManageCom  > </TD>
        </TR>
        
        <TR  class= common style  = "display:'none'">
          <TD  class= title> </TD>
          <TD  class= input> <Input class= common name=ProposalNo ><Input class= common name=ContNo > </TD>
          <TD  class= title> 投保人姓名 </TD>
          <TD  class= input> <Input class= common name=AppntName > </TD>
        </TR>
       
    </table>
          <INPUT VALUE="查  询" class = CssButton TYPE=hidden onclick="easyQueryClick();">         
     <hr>
    
    <table style="display:'none'">
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLC2);">
    		</td>
    		<td class= titleImg>
    			 全体客户给付类型信息
    		</td>
    	</tr>
    </table>	
    <Div divLC2 style="display:'none'">
    <table class=common>
    	<tr class=common>
    		<td class= title style="width:30%">正常给付:
    			<input class=common1 style="width:55px" name=get1>件，&nbsp 占比:<input class=common_A1 name=rget1>%
    		</td>                                          
    		<td class= title style="width:30%">部分给付:                      
    			<input class=common1 style="width:55px" name=get2>件，&nbsp 占比:<input class=common_A1 name=rget2>%
    		</td>                                          
    		<td class= title style="width:30%">全额拒付:                      
    			<input class=common1 style="width:55px" name=get3>件，&nbsp 占比:<input class=common_A1 name=rget3>%
    		</td>                                          
    	</tr>                                            
    	<tr class=common>                                
    		<td class= title>协议给付:                      
	    		<input class=common1 style="width:55px" name=get4>件，&nbsp 占比:<input class=common_A1 name=rget4>%
    		</td>                                          
    		<td class= title>通融给付:                      
    			<input class=common1 style="width:55px" name=get5>件，&nbsp 占比:<input class=common_A1 name=rget5>%
    		</td>
    		<td class= title>
    		</td>

    	</tr>
    </table>
  </Div>
 
    <table class=common>
    	<tr class=common>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLC1);">&nbsp<b>查询结果<b></td>
    		<td class= title>历史赔付次数</td>
    		<td class= input><input class=common name=AllClaimNum></td>
    	</tr>
    </table>	
  	<Div  id= "divLC1" style= "display: ''" align = center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanClaimPayGrid" >
  					</span> 
  			  	</td>
  				</tr>
	    	</table>
      <INPUT VALUE="首  页" class = CssButton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class = CssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class = CssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class = CssButton TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  	
  	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLC2);">
    		</td>
    		<td class= titleImg>
    			 个人给付类型信息
    		</td>
    	</tr>
    </table>	
    <Div divLC2>
    <table class=common>
    	<tr class=common>
    		<td class= title>正常给付:
    			<input class=common1 style="width:55px" name=getP1>件，&nbsp 占比:<input class=common_A1 name=rgetP1>%
    		</td>                                          
    		<td class= title>部分给付:                      
    			<input class=common1 style="width:55px" name=getP2>件，&nbsp 占比:<input class=common_A1 name=rgetP2>%
    		</td>                                          
    		<td class= title>全额拒付:                      
    			<input class=common1 style="width:55px" name=getP3>件，&nbsp 占比:<input class=common_A1 name=rgetP3>%
    		</td>                                          
    	</tr>                                            
    	<tr class=common>                                
    		<td class= title>协议给付:                      
    			<input class=common1 style="width:55px" name=getP4>件，&nbsp 占比:<input class=common_A1 name=rgetP4>%
    		</td>                                          
    		<td class= title>通融给付:                      
    			<input class=common1 style="width:55px" name=getP5>件，&nbsp 占比:<input class=common_A1 name=rgetP5>%
    		</td>
    		<td class= title>
    		</td>

    	</tr>
    </table>
  </Div>
  	<hr>
		<input type=button class=cssButton style="align:right" value="审批审定查询" OnClick="ShowInfoPage();">
		<input type=button class=cssButton value="保单查询" OnClick="ContInfoPage();">  	
  	<p>
  	<div id="divQueryButt" style="display: 'none'" align=center>
  	 <INPUT VALUE="保单明细" class = CssButton TYPE=button onclick="PolClick();">
  	 <INPUT VALUE="基本信息查询" class = CssButton TYPE=button onclick="FunderInfo();">
  	  <INPUT VALUE="投保信息查询" class = CssButton TYPE=button onclick="PersonBiz();">
  	   <INPUT VALUE="保单保障项目查询" class = CssButton TYPE=button onclick="">
  	    <INPUT VALUE="健康管理查询" class = CssButton TYPE=button onclick="">
  	     <INPUT VALUE="Call Center信息查询" class = CssButton TYPE=button onclick="">
  	     <hr>
  	      <INPUT VALUE="合同管理查询" class = CssButton TYPE=button onclick="">
  	 </div>
  	 <Input type="hidden" class= common name="ComCode"  value="<%=Comcode%>">
  	 </p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
