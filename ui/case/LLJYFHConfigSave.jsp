<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：黑名单批量导入
	//程序功能：
	//创建日期：2014-01-07
	//创建人  ：Houyd
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	LDCodeSchema tLDCodeSchema = new LDCodeSchema();
	LDCodeUI tLDCodeUI = new LDCodeUI();
	//输出参数
	String FlagStr = "";
	String Content = "";
	GlobalInput tGI = new GlobalInput(); //repair:
	tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
	//后面要执行的动作：添加，修改，删除
    String transact=request.getParameter("Transact");
	if(tGI==null)
	{
	    System.out.println("页面失效,请重新登陆");   
	    FlagStr = "Fail";        
	    Content = "页面失效,请重新登陆";  
	}
	else
	{
		String Operator  = tGI.Operator ;  //保存登陆管理员账号
		String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
		CErrors tError = null;
	    String tBmCert = "";
	    
	    String newCode = request.getParameter("SpecialGroupPolicy");
	    System.out.println("transact:"+transact);
	    if("INSERT".equals(transact))
	    {
	    	tLDCodeSchema.setCodeType("lp_jyfh_pass");
			tLDCodeSchema.setCode(request.getParameter("SpecialGroupPolicy"));
			tLDCodeSchema.setCodeName("跳过结余返还检验");
			tLDCodeSchema.setCodeAlias("理赔");
			tLDCodeSchema.setComCode(Operator);
			tLDCodeSchema.setOtherSign(PubFun.getCurrentDate());
	    }
	    else if("DELETE".equals(transact))
	    {
	    	String tCode=request.getParameter("Code");
	    	tLDCodeSchema.setCodeType("lp_jyfh_pass");
			tLDCodeSchema.setCode(tCode);
	    }
	    else if("UPDATE".equals(transact))
	    {
	    	String tCode=request.getParameter("Code");
	    	System.out.println("tCode:"+tCode);
	    	tLDCodeSchema.setCodeType("lp_jyfh_pass");
			tLDCodeSchema.setCode(tCode);
			tLDCodeSchema.setComCode(Operator);
	    }
	    
		try{
		  // 准备传输数据 VData
		   VData tVData = new VData();
		   tVData.add(tLDCodeSchema);
		   tLDCodeUI.submitData(tVData,transact,newCode);
	  }
	  catch(Exception ex)
	  {
		    Content = "保存失败，原因是:" + ex.toString();
		    FlagStr = "Fail";
      }
	  //如果在Catch中发现异常，则不从错误类中提取错误信息
	  if (FlagStr=="")
	  {
	      tError = tLDCodeUI.mErrors;
	      if (!tError.needDealError())
	      {     
	        if("INSERT".equals(transact))
	  	    {
	        	Content ="保存成功！";
		  	    FlagStr = "Success";
	  	    }
	  	    else if("DELETE".equals(transact))
	  	    {
	  	    	Content ="删除成功！";
		  	    FlagStr = "Success";
	  	    }
	  	    else if("UPDATE".equals(transact))
	  	    {
	  	    	Content ="修改成功！";
		  	    FlagStr = "Success";
	  	    }
	      }
	      else                                                                           
	     {
	    	    if("INSERT".equals(transact))
		  	    {
	    	    	Content = "保存失败，原因是:" + tError.getFirstError();
	   	  	     	FlagStr = "Fail";
		  	    }
		  	    else if("DELETE".equals(transact))
		  	    {
		  	    	Content = "删除失败，原因是:" + tError.getFirstError();
			  	    FlagStr = "Fail";
		  	    }
		  	    else if("UPDATE".equals(transact))
		  	    {
		  	    	Content = "修改失败，原因是:" + tError.getFirstError();
			  	    FlagStr = "Fail";
		  	    }
	     }
	  }
	}
  %>
<html>
<script language="javascript">
 	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>