//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           



function getQueryResult()
{
	var arrSelected = null;
	tRow = CheckGrid.getSelNo();

	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = CheckGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

function initQuery()
{
	var strSQL="select b.OtherNo,substr(b.SurveyNo,18),"
	+ "(select c.codename from ldcode c where codetype='llsurveykind' and code=a.SurveyType),"
	+"a.SurveyStartDate,a.Startman,a.RgtState,a.SurveyType,b.inqno,"
	+ "(select c.codename from ldcode c where codetype='llsurveyflag' and code=a.surveyflag),"
	+ "b.SurveyNo,a.surveysite from LLSurvey a,LLInqApply b where 1=1 and a.surveyno=b.surveyno and not exists (select 1 from llcase where rgtstate='14' and caseno=b.otherno and not exists (select 1 from llcaseoptime where caseno=llcase.caseno and rgtstate='08')) "
	+" and b.OtherNo='"+fm.CaseNo.value+"' with ur";
				
	turnPage.queryModal(strSQL,CheckGrid);
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = CheckGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
	
			arrReturn = getQueryResult();
			top.opener.afterSimpleLLSurveyReply(arrReturn);
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterSimpleLLSurveyReply接口。" + ex.message );
		}
		top.close();
	}
}

