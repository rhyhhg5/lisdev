<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLPolicyInput.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
   String sql = "select case when current time>='08:00:00' and current time<'17:30:00' then 'Y' else 'N' end from dual"; 
   String sql1 = "select case when dayofweek(current date)=7 or dayofweek(current date)=1 then 'Y' else 'N' end from dual with ur";
   ExeSQL tExeSQL = new ExeSQL();
   String rel = tExeSQL.getOneValue(sql);
   String rel1 = tExeSQL.getOneValue(sql1);
%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLPolicyInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./LLPolicy.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title>险种类型</TD>
          <TD  class= input> <input class="codeno" CodeData="0|3^1|个险^2|团险^3|银保渠道"  verify="险种类型|notnull" elementtype=nacessary name=ContType ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);"><input class=codename name=ContTypeName></TD> 
         </TR>
    </table>
        <table class= common width=100%>
      	<TR  class= common>
		  <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
       	</TR>
       	<TR  class= common>
          <TD  class= title>业务类型</TD>
          <TD  class= input> <input class="codeno" value="3" CodeData="0|3^1|商业保险^2|社会保险^3|全部"  verify="业务类型|notnull" elementtype=nacessary name=StatsType ondblclick="return showCodeListEx('StatsType',[this,StatsTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StatsType',[this,StatsTypeName],[0,1]);"><input class=codename value="全部" name=StatsTypeName></TD> 
        </TR>
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="CaseStatistics()"></TD>
			</tr>
		</table>
	<Input type="hidden" class= common name="RiskRate" >
	<INPUT  type= hidden name="result" value=<%=rel %>>
	<INPUT  type= hidden name="isweek" value=<%=rel1 %>>
  </form>
  <table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
		 	<tr class="common"><td class="title">机构代码：案件受理机构</td></tr><br>
            <tr class="common"><td class="title">机构名称：案件受理机构名称</td></tr><br>
            <tr class="common"><td class="title">险种编码：案件对应赔款的险种编码</td></tr><br>
            <tr class="common"><td class="title">险种名称：案件对应赔款的险种名称</td></tr><br>
            <tr class="common"><td class="title">承保人数；机构下该险种在统计期间内的投保人数</td></tr><br>
            <tr class="common"><td class="title">新单首期保费；机构下该险种在统计期间内所缴纳保费总和</td></tr><br>
            <tr class="common"><td class="title">经过保费：机构下该险种在统计期间内的经过保费</td></tr><br>
            <tr class="common"><td class="title">理赔件数：在统计期间内发生的理赔案件数</td></tr><br>
            <tr class="common"><td class="title">理赔支出：在统计期间内发生的理赔额</td></tr><br>
		</Div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 