<%
//Name:ReportInit.jsp
//function：
//author:
//Date:2005-08-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
 GlobalInput tG = (GlobalInput)session.getValue("GI");
 String tHCNo = "";

 tHCNo = request.getParameter("HCNo");
 if(tHCNo.equals("null"))
  tHCNo="";

%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  	fm.HCNo.value = "<%=tHCNo%>";
  }
  catch(ex)
  {
    alter("在SecondUWInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在SecondUWInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initUnDoCaseGrid();
    initDoCaseGrid();
        
    HospGetSearch();
    //ContSearch();
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initUnDoCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="管理机构";
    iArray[1][1]="40px";
    iArray[1][2]=40;
    iArray[1][3]=0;
    iArray[1][21]="contno";
    
    iArray[2]=new Array();
    iArray[2][0]="医院代码";
    iArray[2][1]="40px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][21]="EdorAcceptNo";
    
    iArray[3]=new Array();
    iArray[3][0]="医院名称";
    iArray[3][1]="80px";
    iArray[3][2]=80;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="案件号";
    iArray[4][1]="60px";
    iArray[4][2]=100;
    iArray[4][3]=0;

    iArray[5]=new Array();
    iArray[5][0]="客户号";
    iArray[5][1]="30px";
    iArray[5][2]=100;
    iArray[5][3]=0;
    iArray[5][21]="operator";
    
    iArray[6]=new Array();
    iArray[6][0]="客户姓名";
    iArray[6][1]="40px";
    iArray[6][2]=100;
    iArray[6][3]=0;
    
    iArray[7]=new Array();
    iArray[7][0]="给付金额";
    iArray[7][1]="30px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    
    iArray[8]=new Array();
    iArray[8][0]="回销预付赔款金额";
    iArray[8][1]="30px";
    iArray[8][2]=100;
    iArray[8][3]=0;
    
    iArray[9]=new Array();
    iArray[9][0]="实际给付金额";
    iArray[9][1]="30px";
    iArray[9][2]=100;
    iArray[9][3]=0;

    UnDoCaseInfo = new MulLineEnter("fm","UnDoCaseInfo");
    UnDoCaseInfo.mulLineCount =0;
    UnDoCaseInfo.displayTitle = 1;
    UnDoCaseInfo.locked = 1;
    UnDoCaseInfo.canSel =0;
    UnDoCaseInfo.canChk = 1;
    UnDoCaseInfo.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    UnDoCaseInfo.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    UnDoCaseInfo.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initDoCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="管理机构";
    iArray[1][1]="40px";
    iArray[1][2]=40;
    iArray[1][3]=0;
    iArray[1][21]="contno";
    
    iArray[2]=new Array();
    iArray[2][0]="医院代码";
    iArray[2][1]="40px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][21]="EdorAcceptNo";
    
    iArray[3]=new Array();
    iArray[3][0]="医院名称";
    iArray[3][1]="80px";
    iArray[3][2]=80;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="案件号";
    iArray[4][1]="60px";
    iArray[4][2]=100;
    iArray[4][3]=0;

    iArray[5]=new Array();
    iArray[5][0]="客户号";
    iArray[5][1]="30px";
    iArray[5][2]=100;
    iArray[5][3]=0;
    iArray[5][21]="operator";
    
    iArray[6]=new Array();
    iArray[6][0]="客户姓名";
    iArray[6][1]="40px";
    iArray[6][2]=100;
    iArray[6][3]=0;
    
    iArray[7]=new Array();
    iArray[7][0]="给付金额";
    iArray[7][1]="30px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    
    iArray[8]=new Array();
    iArray[8][0]="回销预付赔款金额";
    iArray[8][1]="30px";
    iArray[8][2]=100;
    iArray[8][3]=0;
    
    iArray[9]=new Array();
    iArray[9][0]="实际给付金额";
    iArray[9][1]="30px";
    iArray[9][2]=100;
    iArray[9][3]=0;
    
    iArray[10]=new Array();
    iArray[10][0]="案件状态";
    iArray[10][1]="30px";
    iArray[10][2]=100;
    iArray[10][3]=0;

    DoCaseInfo = new MulLineEnter("fm","DoCaseInfo");
    DoCaseInfo.mulLineCount =0;
    DoCaseInfo.displayTitle = 1;
    DoCaseInfo.locked = 1;
    DoCaseInfo.canSel =0;
    DoCaseInfo.canChk = 1;
    DoCaseInfo.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    DoCaseInfo.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    DoCaseInfo.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
   
</script>