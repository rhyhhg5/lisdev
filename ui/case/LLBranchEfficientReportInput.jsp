<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLBranchCaseReportInput.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
   String sql = "select case when current time>='08:00:00' and current time<'17:30:00' then 'Y' else 'N' end from dual"; 
   String sql1 = "select case when dayofweek(current date)=7 or dayofweek(current date)=1 then 'Y' else 'N' end from dual with ur";
   ExeSQL tExeSQL = new ExeSQL();
   String rel = tExeSQL.getOneValue(sql);
   String rel1 = tExeSQL.getOneValue(sql1);
%>
<%
    
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLBranchCaseReportInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form action="./LLBranchCaseReport.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
        <TR  class= common>
          <TD  class= title>业务类型</TD>
          <TD  class= input> <input class="codeno" value="3" CodeData="0|3^1|商业保险^2|社会保险^3|全部"  verify="业务类型|notnull" elementtype=nacessary name=StatsType ondblclick="return showCodeListEx('StatsType',[this,StatsTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StatsType',[this,StatsTypeName],[0,1]);"><input class=codename value="全部" name=StatsTypeName></TD> 
        </TR>
        
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="BranchEfficient()"></TD>
			</tr>
		</table>
<INPUT  type= hidden name="result" value=<%=rel %>>
<INPUT  type= hidden name="isweek" value=<%=rel1 %>>
  </form>
  <table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
		 	<tr class="common"><td class="title">机构编码：案件对应的二级机构。</td></tr><br>
            <tr class="common"><td class="title">机构：案件对应的二级机构名称。</td></tr><br>
            <tr class="common"><td class="title">受理-扫描时间：该机构完成了扫描动作的案件在统计范围内从受理状态到扫描状态的平均天数，不包括没有扫描件的案件</td></tr><br>
            <tr class="common"><td class="title">扫描-账单录入时间：该机构完成了账单录入动作的案件在统计范围内从扫描状态到账单录入状态的平均天数，不包括没有扫描件的案件。</td></tr><br>
            <tr class="common"><td class="title">账单录入-检录时间：该机构完成了检录动作的案件在统计范围内从第一次保存账单的时间到检录状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">检录-审批时间：该机构完成了审批动作的案件在统计范围内从检录状态到审批状态的平均天数。不考虑案件回退情况，以及多层审批上报的情况，检录和审批状态，都是只记录第一次保存确定的时间。</td></tr><br>
            <tr class="common"><td class="title">审批-审定时间：该机构完成了审定动作的案件在统计范围内从第一次审批状态到第一次审定状态的平均天数。</td></tr><br>
			<tr class="common"><td class="title">调查时间：该机构完成了调查动作的调查案件在统计范围内从调查状态到查讫状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">理赔二核时间：该机构完成了理赔二核动作的案件在统计范围内的平均天数。</td></tr><br>
            <tr class="common"><td class="title">审定-结案时间：该机构完成了结案动作的案件在统计范围内从审定状态到结案状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">抽检-结案时间：该机构完成了抽检动作的案件在统计范围内从抽检状态到结案状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">结案-通知时间：该机构完成给付确认动作、变更为通知状态的案件在统计范围内从结案状态到通知状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">通知－给付时间：该机构完成财务给付步骤的案件在统计范围内从通知状态到给付状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">扫描－结案时间：该机构完成了结案动作的案件在统计范围内从扫描状态到结案状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">受理-通知时间（天）：该机构完成给付确认动作、变更为通知状态的案件在统计范围内从受理状态到通知状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">受理-结案时间（天）：该机构完成了结案动作的案件在统计范围内从受理状态到结案状态的平均天数。</td></tr><br>
            <tr class="common"><td class="title">合计：该报表查询出所有二级机构对应项的列合计。</td></tr><br>
		</Div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 