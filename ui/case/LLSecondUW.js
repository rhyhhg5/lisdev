//程序名称：LLSecondUW.js
//程序功能：合同审查
//创建日期：2006-01-01 15:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var k = 0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content); 
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    alert(content);
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
    cDiv.style.display="";
  else
    cDiv.style.display="none";  
}

// 查询按钮
function easyQueryClick()
{
	// 书写SQL语句
	k++;
	var tInsuredNo = fm.InsuredNo.value;
  var strSQL = "select ContNo,AppntName,ManageCom,'','','',prtno,proposalcontno,remark from LCCont where appflag='1' "
	           + " and InsuredNo = '"+tInsuredNo+"'";

  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult != null )
	{
		// 初始化表格
		initPolGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
			CheckGrid.setRowColData( i, j+1, arrResult[i][j] );
			}
		}
		//alert("result:"+arrResult);
	}
}

function Make()
{
  var i = 0;
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function onSelSelected()
{
	var row=PolGrid.getSelNo()-1;
	var ContNo="";
	if(row>0)
	{
		ContNo = PolGrid.getRowColData(row,8);
		fm.ContNo.value = ContNo;
		fm.Remark.value=PolGrid.getRowColData(row,9);
		fm.ContNoA.value=PolGrid.getRowColData(row,1);
	}
	
		divLCImpart0.style.display = '';
		divLCImpart2.style.display = '';
		DivAddFee.style.display = '';
 	 	clearImpart();    //清空
  	getImpartbox();   //保障状况告知※健康告知
  getImpartDetailInfo();
  ShowAddFeeInfo();
  ShowSpecInfo();
}

function DealCont()
{
	var selno = PolGrid.getSelNo()-1;
	if (selno >=0)
	{
		cContNo = PolGrid.getRowColData(selno,1);
		cPrtNo = PolGrid.getRowColData(selno,7);
	}
  
  window.open("../app/ProposalEasyScan.jsp?LoadFlag=6&prtNo="+cPrtNo+"&ContNo="+cContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");  	  
}

function RenewSuggest()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.InsuredNo.value+"&CustomerName="+fm.CustomerName.value;
	showInfo = window.open("./FrameMainRenewSuggest.jsp?Interface=LLRenewSuggestInput.jsp"+varSrc,"续保建议",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function DealExemption()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.InsuredNo.value+"&CustomerName="+fm.CustomerName.value;
	showInfo = window.open("./FrameMainExemption.jsp?Interface=LLExemptionInput.jsp"+varSrc,"续保建议",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function getImpartDetailInfo() {
  initImpartDetailGrid();
  var InsuredNo=fm.all("InsuredNo").value;
  var ContNo=fm.all("ContNo").value;
  //告知信息初始化
  if(InsuredNo!=null&&InsuredNo!="") {
    var strSQL ="select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where CustomerNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and CustomerNoType='I'";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
      return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = ImpartDetailGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL;
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }
}

//将加费的信息显示出来；
function ShowAddFeeInfo()
{
		var strSql = "select prem,round(rate,2),paystartdate,payenddate from lcprem where ContNo='"
		+fm.ContNoA.value+"' and substr(payplancode,1,6)='000000' ";
		turnPage.queryModal(strSql, AddFeeGrid);
}

//将免责的信息显示出来；
function ShowSpecInfo()
{
		var strSql = "select SpecCode,SpecContent,'','',SerialNo from LCSpec where ContNo='"
		+fm.ContNoA.value+"'";
		turnPage.queryModal(strSql, SpecGrid);
}

function DealAddFee()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value+"&ContNo="+fm.ContNoA.value +"&CustomerNo="+fm.InsuredNo.value+"&CustomerName="+fm.CustomerName.value;
	showInfo = window.open("./FrameMainAddFee.jsp?Interface=LLAddFeeInput.jsp"+varSrc,"保单加费",'width=700,height=400,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function DealCease()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value+"&ContNo="+fm.ContNoA.value +"&InsuredNo="+fm.InsuredNo.value+"&CustomerName="+fm.CustomerName.value;
	showInfo = window.open("./FrameEndAgreement.jsp?Interface=EndAgreementInput.jsp"+varSrc,"保单加费",'width=700,height=400,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function QueryOnKeyDown()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    var tsql = "select CustomerName,CustomerNo from llcase where CaseNo='"+fm.CaseNo.value+"'";
    var arr = easyExecSql(tsql);  
    if(arr){
      fm.CustomerName.value = arr[0][0];
      fm.InsuredNo.value = arr[0][1];
    }
    easyQueryClick();
  }
}

function QueryOnEnter()
{
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    var tsql = "select CustomerNo,Name from ldperson where 1=1"
    +getWherePart("Name","CustomerName")
    +getWherePart("CustomerNo","InsuredNo");
    var arr = easyExecSql(tsql);
    if (arr)
    {
      try
      {
        if(arr.length==1)
        {
          afterLLRegister(arr);
        }
        else{
          var varSrc = "&CustomerNo=" + fm.InsuredNo.value;
          varSrc += "&CustomerName=" + fm.CustomerName.value;
          varSrc += "&ContNo=";
          varSrc += "&IDType=";
          varSrc += "&IDNo=" ;
          showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
        }
      }catch(ex)
      {
        alert("错误:"+ ex.message);
      }
    }
  }
}

function afterLLRegister(arrReturn)
{
  fm.InsuredNo.value=arrReturn[0][0];
  fm.CustomerName.value=arrReturn[0][1];
      easyQueryClick();
}