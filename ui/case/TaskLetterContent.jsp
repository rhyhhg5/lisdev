<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskLetterContent.jsp
//程序功能：
//创建日期：2005-08-29
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.task.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
    CError cError = new CError( );
    boolean operFlag=true;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
	String letterInfo = "";
	  
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
	LGLetterSchema schema = new LGLetterSchema();
	schema.setEdorAcceptNo(request.getParameter("edorAcceptNo"));
	schema.setSerialNumber(request.getParameter("serialNumber"));
	schema.setLetterType(request.getParameter("letterType"));
	
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	
    tVData.add(tG);
    tVData.add(schema);

    PrtLetterContentUI tPrtLetterContentUI = new PrtLetterContentUI();
	XmlExport txmlExport = new XmlExport();    
    if(!tPrtLetterContentUI.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tPrtLetterContentUI.mErrors.getFirstError().toString();                 
    }
    else
    {    
		mResult = tPrtLetterContentUI.getResult();			
	  	txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	
	  	if(txmlExport == null)
	  	{
	   		operFlag = false;
	   		Content = "没有得到要显示的数据文件";	  
	  	}
	}
	
	if (operFlag==true)
	{
		session.putValue("PrintStream", txmlExport.getInputStream());
		
		response.sendRedirect("../f1print/GetF1Print.jsp?TemplateName=PrtLetterPreview.vts&showToolBar=false"
			+ "appletWidth=" + request.getParameter("appletWidth") 
			+ "&appletHeight=" + request.getParameter("appletHeight"));
	}
	else
	{
    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");	
</script>
</html>
<%
  	}

%>