//该文件中包含客户端需要处理的函数和事件

//程序名称：LLSMSGrpCont.js
//程序功能：短信发送团体保单配置
//创建日期：2010-03-31
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();


//查询按钮
function searchGrpCont()
{
  var mngcom=fm.ManageCom.value;
  var grpcontno=fm.grpContNo.value;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  // 书写SQL语句
  
  var strSql = " SELECT l.RGTNO,b.grpname,l.MngCom,l.operator,l.modifydate,l.modifytime" +
  " FROM LLAppClaimReason l,lcgrpcont b  "
  +"WHERE l.rgtno=b.grpcontno and reasontype='9' and reasoncode='97' " 
  + " and l.mngcom like '"+fm.ManageCom.value+"%'"
  + getWherePart("l.RGTNO","grpContNo")
  + " order by l.rgtno desc";
  
  turnPage.queryModal(strSql,Mul10Grid);
  showInfo.close();
}

//保存按钮
function saveGrpContNo()
{
	if(fm.grpContNo.value=='') {
		alert("请输入团体保单号");
    return false;
  }
   
  var sql1="select * from lcgrpcont where grpcontno='"+fm.grpContNo.value+"' and appflag='1' ";
    
  if(!easyExecSql(sql1)){
    var sql3="select * from lccont where contno='"+fm.grpContNo.value+"' ";
    
    if(easyExecSql(sql3)){
    	alert("该保单号为个单保单号");
      return false;
    } else {
    	alert("该保单号不存在");
    	return false;
    }
  }
  
  var sql="select * from LLAppClaimReason where rgtno='"+fm.grpContNo.value+"' and reasoncode='97' ";
  if(easyExecSql(sql)) {
  	alert("此保单已配置！");
  	return false;
  }
  
  fm.all("operate").value='insert';
  var showStr="正在保存数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

 	fm.action="./LLSMSGrpContSave.jsp";
	fm.submit();
}

//删除按钮
function deleteGrpContNo( )
{
	var rowsNo = Mul10Grid.mulLineCount;
	if ( rowsNo>0){
		var checkRc = false;
		var kk=0;
		for(kk=0;kk<rowsNo;kk++){
			if(Mul10Grid.getChkNo(kk)){
				checkRc=true;
			}
		}
	  	
    if(!checkRc){
      alert("请选择一个保单！");
      return false;
    }
  }
  
  fm.all('operate').value = 'del';
  var showStr="正在删除数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

 	fm.action="./LLSMSGrpContDelSave.jsp";
	fm.submit();
}


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
    searchGrpCont();
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

