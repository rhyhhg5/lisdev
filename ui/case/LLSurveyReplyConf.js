var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 
    if (fm.fmtransact.value=="DELETE||MAIN")
    {
    	fm.reset();
    	initCustomerGrid();
    }
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function showCustomerInfo()
{
	window.open("../sys/FrameCQPersonQuery.jsp");
}

function afterCodeSelect( cCodeName, Field ) {
	try	{	 
	   
	   }catch(ex)
	   {
	   	alert(ex.message);
	}
}

function onSelSelected(parm1,parm2)
{
  
}

 
function 	ClientQuery()
{
	
	window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}


function LLSurveyReplyConf()
{
  if(fm.SurveyFlag.value==''){
    alert("请录入审核结论！");
    return false;
  }
	var i = 0;
	fm.fmtransact.value="INSERT||CONF";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LLSurveyReplySave.jsp";
  	fm.submit();
}

function CloseWindows()
{
	top.close();
}

function submitBlackList()
{
	var varSrc = "?CustomerNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&CaseNo=" + fm.OtherNo.value;
	pathStr="./FrameMainBlackList.jsp?Interface=BlackListInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
}

function ShowConf(){

    	var strSQL = "select a.docid,a.doccode,a.subtype,b.subtypename,a.managecom,a.scanoperator,a.makedate,"
					+ " a.maketime,a.modifydate,a.modifytime,a.docid "
					+ " from es_doc_main a ,es_doc_def b where 1=1 "
					+ " and a.subtype = b.subtype "
				  + " and a.doccode like '"+fm.OtherNo.value+"%%'"
				  + " and a.busstype='LP'"
				  + " and b.busstype='LP'";	
	var arrReturn4 = new Array();
  	arrReturn4 = easyExecSql(strSQL);
  	if(arrReturn4 != null)
  	{   
  	    var cDocID = arrReturn4[0][0];
  	    var cDocCode = arrReturn4[0][1];
  	    var	cBussTpye = "LP" ;
  	    var cSubTpye = arrReturn4[0][2];
  	    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+cDocCode+"&BussTpye="+cBussTpye+"&SubTpye="+cSubTpye);        
  	}else{
  	   alert("该理赔号没有扫描件！");
  	}	

  }
