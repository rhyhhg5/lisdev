var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var tSaveType="";

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
	}
}

function easyQuery()
{
	var comCode = fm.ComCode.value;
	if(fm.RgtDateS.value == "" || fm.RgtDateE.value == "") 
	{ alert("请输入受理起止日期"); return false;}
	//咨询类
	
  	var	strSQL =" select c.ConsultNo,c.customername,c.CustomerNo,b.phone,b.mobile,b.askaddress,b.makedate, '',"
  					 +" case when b.asktype='0' then '咨询' else '通知' end ,d.IDNo, "
  					 +" (select k.name from ldcom k where c.Mngcom = k.comcode ), "
  					 +" (select h.username from lduser h where h.usercode = c.operator) ,c.operator"
  					 +" from LLConsult c, LLMainAsk b, LDPerson d where 1=1"
  					 +" and c.Customerno = d.customerno "
  					 +" and (b.AskType='0' or b.AskType='1')"
  					 + getWherePart("c.ConsultNo","ConsultNo") 
  					 + getWherePart("c.CustomerNo","CustomerNo")
  					 + "and C.CustomerName like '%%" + fm.CustomerName.value +"%%' "
  					 + getWherePart("b.asktype","AskType")
  					 + getWherePart("d.idno","IDNo")
  					 +" and b.logno=c.logno "
  					 +" and b.makedate <= '"+fm.all('RgtDateE').value+"'"
  					 +" and b.makedate >= '"+fm.all('RgtDateS').value+"'"
  					 +" and b.mngcom like '"+comCode+"%%'"
  					 ;
  					 
  					 //alert(strSQL);
  					 
//					 alert(strSQL);alert(easyExecSql(strSQL));
    turnPage.pageLineNum = 5;
		turnPage.queryModal(strSQL, CheckGrid);
}

//咨询通知

function DealConsult_origin()
{
	
	var LoadC="2";
	var selno = CheckGrid.getSelNo()-1;
	var caseNo;
	if (selno <0)
	{
		//   alert("请选择要咨询通知的个人受理");
		//   return false;
	}else
		{
			caseNo = CheckGrid.getRowColData( selno, 1);
		}

		var varSrc="&CaseNo="+ caseNo;
		varSrc += "&LoadC="+LoadC;
		var newWindow = OpenWindowNew("./FrameMainCaseNote.jsp?Interface= LLMainAskInput2.jsp"+varSrc,"审核","left");

}

//咨询通知本地页面

function DealConsult()
{
	
	ConsultChange();
	
	
}
function ConsultChange()
{
	
	var tConsultno  = CheckGrid.getRowColData( CheckGrid.getSelNo()-1, 1);
	var strSQL=  "select llmainask.LogName,llmainask.LogerNo,llmainask.Phone,"
							+"llmainask.Email,llmainask.LogComp,llmainask.AskAddress,llmainask.PostCode,"
							+"(select ldcode.codename from "
						  +"ldcode where ldcode.codetype='llaskmode'and ldcode.code=llmainask.AskMode),"
							+"(select ldcode.codename from "
						  +"ldcode where ldcode.codetype='llreturnmode'and ldcode.code=llmainask.AnswerMode),"
							+"llmainask.Mobile,llmainask.AskMode,llmainask.AnswerMode"
							+" from llmainask where llmainask.logno=(select LLConsult.LogNo from LLConsult where LLConsult.Consultno='"+tConsultno+"')";
	var arr = easyExecSql(strSQL);
	if (arr)
	{
		fm.LogName.value=arr[0][0];
		fm.LogerNo.value=arr[0][1];
		fm.Phone.value=  arr[0][2];
		fm.Email.value=  arr[0][3];
		fm.LogComp.value=arr[0][4];
		fm.AskAddress.value=arr[0][5];
//		fm.PostCode.value=  arr[0][6];
		fm.AskModeName.value=arr[0][7]; 
		fm.AnswerModeName.value=arr[0][8];
		fm.Mobile.value=arr[0][9];
		fm.AskMode.value=arr[0][10];
		fm.AnswerMode.value=arr[0][11];
		}
		//身份证号查询
		var strSQL="select idno from ldperson where customerno='"+fm.LogerNo.value+"'";
		var arr = easyExecSql(strSQL);
		if (arr)
		{
			fm.IDNo_R.value=arr[0][0];
		}
		//客户现状、出险类别查询
		var strSQL="select a.CustStatus,(select b.codename from ldcode b where b.codetype='llcuststatus' and b.code=a.CustStatus),a.acccode,case when a.acccode='1' then '意外' else '疾病' end from LLConsult a where a.ConsultNo='"+tConsultno+"'";
		var arr = easyExecSql(strSQL);
		if (arr)
		{
			fm.CustStatus.value=arr[0][0];
			fm.CustStatusName.value=arr[0][1];
			fm.AccType.value=arr[0][2];
			fm.AccTypeName.value=arr[0][3];
		}
		//信息回复查询
		var strSQL="select Answer from LLAnswerInfo where ConsultNo='"+tConsultno+"'";
		var arr = easyExecSql(strSQL);
		{
			fm.Answer.value=arr[0][0];
		}
		var strSQL="select CContent from LLConsult where ConsultNo='"+tConsultno+"'";
		var arr = easyExecSql(strSQL);
		{
			fm.CContent.value=arr[0][0];
		}
		UpdateGrid();
}

//进入原始页面
function ShowPage()
{
	
}

function UpdateGrid()
{
	if (fm.CDateE.value.length==0)
		fm.CDateE.value=getCurrentDate();
	if(fm.CDateS.value.length==0)
		fm.CDateS.value=newDate(fm.CDateE.value,-2);
		
	   var dpart=getWherePart("AccDate","CDateS",">=") + getWherePart("AccDate","CDateE","<=");
	var strSql="select SubRptNo,AccDate,AccPlace,InHospitalDate,OutHospitalDate,AccDesc "+
							"from LLSubReport where CustomerNo='"+fm.LogerNo.value+"'";
			strSql+=dpart;
			strSql+=" order by AccDate desc";
//			alert(strSql);
			turnPage.queryModal(strSql, SubReportGrid);
}	
	
	
	function getstr()
			{
				//alert("aaa");
				str="1 and code like #"+fm.OrganCode.value+"%#";
			}
			
	function getRights()
	{
			var strSQL="select claimpopedom from llclaimuser where usercode='"+fm.AllOperator.value+"'";
			var arrResult = easyExecSql(strSQL);
			if(arrResult != null)
			{
					var Rights = arrResult[0][0];
					//alert(Rights);

			}
			Str="1 and claimpopedom < #"+Rights+"#";
	}

//原始页面查询
function ShowInfoPage()
{
	var selno = CheckGrid.getSelNo();//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = CheckGrid.getRowColData(selno-1,1);
	OpenWindowNew("./LLMainAskInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");
}

//保单查询
function ContInfoPage()
{
		var selno = CheckGrid.getSelNo();//alert(CheckGrid.getSelNo());
		if(selno == "" || selno == null)
		{alert("请先选择一条信息");return;}
		var CaseNo = CheckGrid.getRowColData(selno-1,1);
		var ContNo = easyExecSql("select distinct grpcontno from llclaimpolicy where caseno = '"+CaseNo+"'");
		window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
}

function FindCustomer()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode == "13")
	{
		var sql = " select Name, CustomerNo, IDNo from LDPerson where 1=1 "
//						+ getWherePart("Name","CustomerName")
						+ " and Name like '%%"+fm.CustomerName.value+"%%' "
						+ getWherePart("CustomerNo","CustomerNo")
						+ getWherePart("IDNo","IDNo")
						;// alert(sql);
		var arrResult = easyExecSql(sql); 
//		if(arrResult.length > 3)
//		{
//			fm.CustomerName.showCodeList('',[this],[0]);
//		}
//		if(arrResult.length > 3 ){return;}		
//		fm.CustomerName.value = arrResult[0][0];
//		fm.CustomerNo.value = arrResult[0][1];
//		fm.IDNo.value = arrResult[0][2];
		easyQuery();
	}
}