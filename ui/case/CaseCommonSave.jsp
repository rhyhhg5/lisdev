<%
//程序名称：CaseCommonSave.jsp
//程序功能：理赔系统操作权限校验
//创建日期：2005-03-26 20:09:16
//创建人  ：zhangtao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
	CErrors CaseCommon_Error = null;
	boolean CaseCommon_RightFlag = false;
	String CaseCommon_Content = "";
		
	GlobalInput CaseCommon_GlobalInput = new GlobalInput();
  	CaseCommon_GlobalInput = (GlobalInput)session.getValue("GI"); 
  		
	String CaseCommon_JspFileName = request.getParameter("CaseCommon_JspFileName");
	String CaseCommon_ButtonName = request.getParameter("CaseCommon_ButtonName");
	String CaseCommon_CaseNo = request.getParameter("CaseNo");
	
	//System.out.println("== JspFileName = " + CaseCommon_JspFileName +" ==");
	//System.out.println("== ButtonName == " + CaseCommon_ButtonName +" ==");
	System.out.println("== CaseNo ====== " + CaseCommon_CaseNo +" ==");
	
	LLCaseSchema CaseCommon_LLCaseSchema = new LLCaseSchema();
	CaseCommon_LLCaseSchema.setCaseNo(CaseCommon_CaseNo);
	CaseCommon_LLCaseSchema.setButtonName(CaseCommon_ButtonName);
	CaseCommon_LLCaseSchema.setJspFileName(CaseCommon_JspFileName);
	
		CaseCommonRightCheckBL tCaseCommonRightCheckBL = new CaseCommonRightCheckBL();
		VData CaseCommon_VData = new VData();
		try
		{
			CaseCommon_VData.add(CaseCommon_GlobalInput);
			CaseCommon_VData.add(CaseCommon_LLCaseSchema);
			CaseCommon_RightFlag = 
				tCaseCommonRightCheckBL.submitData(CaseCommon_VData, "CHECK|RIGHT");
		}
		catch(Exception ex)
		{
			CaseCommon_RightFlag = false;
		    CaseCommon_Content = "用户权限校验失败！" + ex.toString();
		    System.out.println(CaseCommon_Content);	
		    	
		}
	
		if (CaseCommon_RightFlag)
		{
		    CaseCommon_Error = tCaseCommonRightCheckBL.mErrors;
		    if (CaseCommon_Error.needDealError())
		    {
				CaseCommon_RightFlag = false;
			    CaseCommon_Content = CaseCommon_Error.getFirstError();		    		
			    System.out.println(CaseCommon_Content);			      		
		    }
		    else
		    {				
				CaseCommon_Content = "恭喜，您可以执行当前操作...";
				System.out.println(CaseCommon_Content);		    	
		    }
		    	
		}
		else
		{	
		    CaseCommon_Error = tCaseCommonRightCheckBL.mErrors;
		    if (CaseCommon_Error.needDealError())
		    {

			    CaseCommon_Content = CaseCommon_Error.getFirstError();		    		
			    System.out.println(CaseCommon_Content);			      		
		    }					
				
		}
		
		if(!CaseCommon_RightFlag)
		{
		%>
			<script language="javascript">	
					parent.fraInterface.CaseCommon_NoRight("<%=CaseCommon_Content%>");
			</script>			
			
		<%
			return;
		}	
	
%>

