<%
//程序名称：OutsourcingCostSave.jsp
//程序功能：导入提交页面
//创建日期：2013-12-23
//创建人  ：LiuJian
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="jxl.*"%>
<%

	//错误处理类
	CErrors tError = null;
	String flag="Fail";
	String content="";
	//接收类（天津外包费用总额）
	LLOutsourcingTotalSchema tLLOutsourcingTotalSchema   = new LLOutsourcingTotalSchema();
	//	接收类（本保费分摊费用）
	LLOutcoucingAverageSchema tLLOutcoucingAverageSchema   = new LLOutcoucingAverageSchema();
	
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
	String BatchNo =request.getParameter("BatchNo");
	String OSTotal =request.getParameter("OSTotal");
	String RiskCode =request.getParameter("RiskCode");
	String BalanceStartDate =request.getParameter("BalanceStartDate");
	String BalanceEndDate =request.getParameter("BalanceEndDate");
	String PayYear =request.getParameter("PayYear");
	String ManageCom =request.getParameter("ManageCom");
	String Operator =request.getParameter("Operator");
	String OSAccount =request.getParameter("OSAccount");
	String OSAccountNo =request.getParameter("OSAccountNo");
	String Transfertype =request.getParameter("Transfertype");
	String OSBank =request.getParameter("OSBank");
	
	
	String tOperate =request.getParameter("operate");

	String AuditState = request.getParameter("auditState");
	
	tLLOutsourcingTotalSchema.setBatchno(BatchNo);
	tLLOutsourcingTotalSchema.setOstotal(OSTotal);
	tLLOutsourcingTotalSchema.setRiskcode(RiskCode);
	tLLOutsourcingTotalSchema.setBalancestartdate(BalanceStartDate);
	tLLOutsourcingTotalSchema.setBalanceenddate(BalanceEndDate);
	tLLOutsourcingTotalSchema.setPayyear(PayYear);
	tLLOutsourcingTotalSchema.setManagecom(ManageCom);
	tLLOutsourcingTotalSchema.setOperator(Operator);
	tLLOutsourcingTotalSchema.setOsaccount(OSAccount);
	tLLOutsourcingTotalSchema.setOsaccountno(OSAccountNo);
	tLLOutsourcingTotalSchema.setTransfertype(Transfertype);
	tLLOutsourcingTotalSchema.setOsbank(OSBank);
	
	
	System.out.println("==============="+AuditState+"========");
	//通过判断只有审核的时候，才将审核意见和审核状态放在tLLOutsourcingTotalSchema中
	
	if(AuditState==null||"".equals(AuditState)){
		tLLOutsourcingTotalSchema.setAuditstate("0");
	}else{
		tLLOutsourcingTotalSchema.setAuditstate(AuditState);
		tLLOutsourcingTotalSchema.setAuditopinion(request.getParameter("AuditOpinion"));
	}
	
	System.out.println("==============="+tOperate);
	
	OutsourcingCostUI aOutsourcingCostUI = new OutsourcingCostUI();
	try{
		VData tVData = new VData();
		tVData.add(tG);
		tVData.add(tLLOutsourcingTotalSchema);
		
		aOutsourcingCostUI.submitData(tVData,tOperate);
	} catch(Exception ex)
	  {
		content = "保存失败，原因是:" + ex.toString();
	    flag = "Fail";
	  }
	if(!aOutsourcingCostUI.mErrors.needDealError()){
		if("DELETE||MAIN".equals(tOperate)){
			content = "删除成功！";
		    flag = "Success";
		}else if("INSERT||MAIN".equals(tOperate)){
			content = "保存成功！";
		    flag = "Success";
		}else if("UPDATE||MAIN".equals(tOperate)){
			content = "审核成功！";
		    flag = "Success";
		}
		
	}else{
		content = "保存失败，原因是:" +aOutsourcingCostUI.mErrors.getFirstError();
	    flag = "Fail";
	}
	
	
%>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>