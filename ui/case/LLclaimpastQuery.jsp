<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.util.*"%>
<%
	GlobalInput tG1 = (GlobalInput) session.getValue("GI");
	String CurrentDate = PubFun.getCurrentDate();
	String tCurrentYear = StrTool.getVisaYear(CurrentDate);
	String tCurrentMonth = StrTool.getVisaMonth(CurrentDate);
	String tCurrentDate = StrTool.getVisaDay(CurrentDate);
	String AheadDays = "-90";
	FDate tD = new FDate();
	Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate), Integer
			.parseInt(AheadDays), "D", null);
	FDate fdate = new FDate();
	String afterdate = fdate.getString(AfterDate);
%>
<html>
	<head>
		<script>
function initDate(){
   fm.RgtDateE.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
   fm.Handler.value="<%=tG1.Operator%>";
   var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.ManageCom.value+"'");                           
   var arrResult1 = easyExecSql("select username from llclaimuser where usercode = '"+fm.Handler.value+"'");                        
   if(arrResult) fm.all('ComName').value=arrResult[0][0];                   
   if(arrResult1)fm.all('HandlerName').value=arrResult1[0][0];
  }
</script>

		<meta http-equiv="Content-Type" content="text/html; charset=GBK">

		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="LLclaimpastQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLclaimpastQueryInit.jsp"%>
		<title>已审批案件查询</title>
	</head>
	<body onload="initDate();initForm();">
		<form action="" method=post name=fm target="fraSubmit">
			<table>
				<TR>
					<TD>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divContDealMail);">
					</TD>
					<TD class=titleImg>
						已审批案件
					</TD>
				</TR>
			</table>
			<Div id="divContDealMail">
				<table class="common" align="center" id="tbInfo" name="tbInfo">
					<tr class="common">
					 <td  class= title>管理机构</td>
           				<td class= input><Input class="codeno" name=ManageCom readonly verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,180);" ><input class=codename name=ComName></td> 
           				<td  class= title>理赔员</td>
           				<td  class= input><input class="codeno" name=Handler ondblclick="return showCodeList('optnameunclass',[this,HandlerName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('optnameunclass',[this,HandlerName],[0,1]);"><input class=codename name=HandlerName></td> 
          				 <td class=title>案件类型</td>
 		 				  <td class=input><Input class=codeno name=Type verify="收付费标志|NOTNULL" CodeData="0|^1|审批^2|抽检" ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);" onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);"><input class=codename name=SXFlagName readonly=true elementtype="nacessary"></td>
		   				</tr>	
					<tr class="common" id="divConfirm" style="display:''">
						<td class="title">
							受理起期
						</td>
						<td class="input">
							<input class="coolDatePicker" dateFormat="short" name="RgtDateS">
						</td>
						<td class="title">
							受理止期
						</td>
						<td class="input">
							<input class="coolDatePicker" dateFormat="short" name="RgtDateE">
						</td>
						<TD class=title></TD>
						<TD class=input></TD>
					</tr>
					<tr class="common" id="divConfirm" style="display:''">
						<td class="title">
							审批起期
						</td>
						<td class="input">
							<input class="coolDatePicker" dateFormat="short" name="RgtDateSS">
						</td>
						<td class="title">
							审批止期
						</td>
						<td class="input">
							<input class="coolDatePicker" dateFormat="short" name="RgtDateSE">
						</td>
						<TD class=title></TD>
						<TD class=input></TD>
					</tr>
				</table>
				<input value="查  询" class="cssButton" type="button"
					onclick="easyQueryClick();">
				<table>
					<TR>
						<TD>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divCaseInfo);">
						</TD>
						<TD class=titleImg>
							案件信息
						</TD>
					</TR>
				</table>
				<div id="divCaseInfo" align=center style="display: ''">
					<table class="common rgtht">
						<tr class="common">
							<td text-align: left colSpan=1>
								<span id="spanCaseInfo"></span>
							</td>
						</tr>
					</table>

					<input VALUE="首  页" TYPE="button" class="cssButton"
						onclick="turnPage.firstPage();">
					<input VALUE="上一页" TYPE="button" class="cssButton"
						onclick="turnPage.previousPage();">
					<input VALUE="下一页" TYPE="button" class="cssButton"
						onclick="turnPage.nextPage();">
					<input VALUE="尾  页" TYPE="button" class="cssButton"
						onclick="turnPage.lastPage();">
				</div>
				<TR class=common>
					<TD class=title8>
						处理
					</TD>
					<TD class=input8>
						<input class=codeno
							CodeData="0|8^0|咨询通知^1|受理申请^2|团体受理申请^3|帐单录入^4|检录^5|理算^6|审批审定^7|撤件^8|材料退回登记 "
							name=DealWith
							ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1],null,null,null,1);">
						<input class=codename name=DealWithName>
					</TD>
				</TR>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
