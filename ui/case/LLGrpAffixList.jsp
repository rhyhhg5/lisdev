<html>
	<%
	//Name:CaseAffixList.jsp
	//Function：申请材料选择页面
	//Date：2002-07-21 17:44:28
	//Author ：LiuYansong
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLGrpAffixList.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLGrpAffixListInit.jsp"%>
	</head>
	<body  onload="initForm();">
		<form action="./LLAffixListSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,AppAffixList);">
					</td>
					<td class= titleImg>
						出险原因相关申请材料
					</td>
				</tr>
			</table>
			<Div  id= "AppAffixList" style= "display: ''">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanAffixGrid" ></span>
						</td>
					</tr>
				</table>
			</div>
			<table>
				<tr>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,AddAffixList);">
					</td>
					<td class= titleImg>
						其他申请材料
					</td>
				</tr>
			</table>
			<Div  id= "AddAffixList" style= "display: ''">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanAddAffixGrid" ></span>
						</td>
					</tr>
				</table>
			</div>
			<input class=cssButton name=confirm type=button value=" 保 存 " onclick="AffixSave()">
			<input class=cssButton type=hidden type=button value=" 修 改 " onclick="AffixUpdate()">
			<input class=cssButton  type=button value=" 返 回 " onclick="top.close()">
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="CaseNo" name="CaseNo">
			<input type=hidden id="RgtNo" name="RgtNo">
			<input type=hidden id="Reason" name="Reason">
			<input type=hidden id="ShortCountFlag" name="ShortCountFlag">
			<input type=hidden id="SupplyDateResult" name="SupplyDateResult">
			<input type=hidden id="AffixNo" name="AffixNo">
			<input type=hidden id="Mobile" name="Mobile">
			<input type=hidden id="Email" name="Email">
			<input type=hidden id="SMSContent" name="SMSContent">
			<input type=hidden id="EmailContent" name="EmailContent">
			<input type=hidden id="AffordCopies" name="AffordCopies">
			<input type=hidden id="ShortCopies" name="ShortCopies">
			<input type=hidden id="ShortAffName" name="ShortAffName">
			<input type=hidden id="AnswerMode" name="AnswerMode">
			<input name ="LoadC" type="hidden">
			<input type="hidden" id="LoadFlag" name="LoadFlag">
			<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		</form>


	</body>
</html>