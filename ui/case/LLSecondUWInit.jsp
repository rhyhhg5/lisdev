<%
//Name:ReportInit.jsp
//function：理赔二核
//author:Xx
//Date:2005-08-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>


<%
 String tCaseNo = "";
 String tInsuredNo = "";

 tCaseNo = request.getParameter("CaseNo");
 if(tCaseNo.equals("null"))
  tCaseNo="";
 tInsuredNo += request.getParameter("InsuredNo");
 if(tInsuredNo.equals("null"))
  tInsuredNo="";
String CustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alter("在SecondUWInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在SecondUWInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(tCaseNo,tInsuredNo)
{
  try
  {
    initInpBox();
    initPolGrid();
    initImpartDetailGrid();
    initAddFeeGrid();
    initSpecGrid();
    initHide(tCaseNo,tInsuredNo);
    var tsql = "select name from ldperson where customerno= '"+fm.InsuredNo.value+"'";
    var srr = easyExecSql(tsql);
    if(srr)
      fm.CustomerName.value = srr[0][0];
    easyQueryClick();
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="合同号码";
    iArray[1][1]="120px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="投保人名称";
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    

    iArray[3]=new Array();
    iArray[3][0]="管理机构";
    iArray[3][1]="100px";
    iArray[3][2]=120;
    iArray[3][3]=0;

    iArray[4]=new Array();
    iArray[4][0]="投保书健康告知栏询问号";
    iArray[4][1]="120px";
    iArray[4][2]=60;
    iArray[4][3]=3;
    
    iArray[5]=new Array();
    iArray[5][0]="体检健康告知栏询问号";
    iArray[5][1]="100px";
    iArray[5][2]=120;
    iArray[5][3]=3;

    iArray[6]=new Array();
    iArray[6][0]="对应未告知情况";
    iArray[6][1]="100px";
    iArray[6][2]=120;
    iArray[6][3]=3;
    
    iArray[7]=new Array();
    iArray[7][0]="投保书号";
    iArray[7][1]="120px";
    iArray[7][2]=100;
    iArray[7][3]=3;

    iArray[8]=new Array();
    iArray[8][0]="投保单号";
    iArray[8][1]="120px";
    iArray[8][2]=100;
    iArray[8][3]=3;

    iArray[9]=new Array();
    iArray[9][0]="特别约定";
    iArray[9][1]="120px";
    iArray[9][2]=1000;
    iArray[9][3]=3;

    PolGrid = new MulLineEnter("fm","PolGrid");
    PolGrid.mulLineCount =5;
    PolGrid.displayTitle = 1;
    PolGrid.locked = 1;
    PolGrid.canSel =1
    PolGrid.canChk = 0;;
    PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    PolGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    PolGrid. selBoxEventFuncName = "onSelSelected";
    PolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

// 告知明细信息列表的初始化
function initImpartDetailGrid() {                               
    var iArray = new Array();
      
    try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="告知版别";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="ImpartVer";
      iArray[1][9]="告知版别|len<=5";
      iArray[1][18]=300;

      iArray[2]=new Array();
      iArray[2][0]="告知编码";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="ImpartCode";
      iArray[2][5]="2|3";
      iArray[2][6]="0|1";
      iArray[2][9]="告知编码|len<=4";
      iArray[2][15]="ImpartVer";
      iArray[2][17]="1";
      iArray[2][18]=700;

      iArray[3]=new Array();
      iArray[3][0]="告知内容";         		//列名
      iArray[3][1]="250px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="疾病内容";         		//列名
      iArray[4][1]="250px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="开始时间";         		//列名
      iArray[5][1]="150px";            		//列宽
      iArray[5][2]=150;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[5][9]="开始时间|date";

      iArray[6]=new Array();
      iArray[6][0]="结束时间";         		//列名
      iArray[6][1]="90px";            		//列宽
      iArray[6][2]=90;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][9]="结束时间|date";
      
      iArray[7]=new Array();
      iArray[7][0]="证明机构或医生";         		//列名
      iArray[7][1]="90px";            		//列宽
      iArray[7][2]=90;            			//列最大值
      iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="目前情况或结果";         		//列名
      iArray[8][1]="90px";            		//列宽
      iArray[8][2]=90;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许


      iArray[9]=new Array();
      iArray[9][0]="能否证明";         		//列名
      iArray[9][1]="90px";            		//列宽
      iArray[9][2]=90;            			//列最大值
      iArray[9][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[9][4]="yesno";
    
 
      ImpartDetailGrid = new MulLineEnter( "fm" , "ImpartDetailGrid" ); 
      //这些属性必须在loadMulLine前
      ImpartDetailGrid.hiddenPlus = 1;
      ImpartDetailGrid.hiddenSubtraction = 1;
      ImpartDetailGrid.mulLineCount = 0;   
      ImpartDetailGrid.displayTitle = 1;
      ImpartDetailGrid.loadMulLine(iArray);  
      
    }
    catch(ex) {
      alert(ex);
    }
}

function initAddFeeGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="费用值";         	//列名
      iArray[1][1]="20px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="费用比例";         	//列名
      iArray[2][1]="20px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="加费起始日期";         		//列名
      iArray[3][1]="20px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="加费终止日期";         		//列名
      iArray[4][1]="20px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      

      AddFeeGrid = new MulLineEnter( "fm" , "AddFeeGrid" );
      //这些属性必须在loadMulLine前
      AddFeeGrid.mulLineCount = 1;
      AddFeeGrid.displayTitle = 1;
      AddFeeGrid.locked = 0;
      AddFeeGrid.canSel = 0;
      AddFeeGrid.hiddenPlus = 1;
      AddFeeGrid.hiddenSubtraction = 1;
      AddFeeGrid.addEventFuncName="checkAddfee";
      AddFeeGrid.loadMulLine(iArray);

      //RiskGrid. selBoxEventFuncName = "showRiskResult";
    } catch(ex) {
      alert(ex);
    }
}

function initSpecGrid() {
  var iArray = new Array();

  try {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="免责信息代码";         	//列名
      iArray[1][1]="20px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="speccode";              	//是否引用代码:null||""为不引用
      iArray[1][5]="1|2";              	    //引用代码对应第几列，'|'为分割符
      iArray[1][6]="0|1";              	 
      iArray[1][19]=1;

      iArray[2]=new Array();
      iArray[2][0]="免责信息";         	//列名
      iArray[2][1]="20px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="免责起始日期";         		//列名
      iArray[3][1]="20px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="免责终止日期";         		//列名
      iArray[4][1]="20px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[5]=new Array();
      iArray[5][0]="流水号";         		//列名
      iArray[5][1]="0px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许


      SpecGrid = new MulLineEnter( "fm" , "SpecGrid" );
      //这些属性必须在loadMulLine前
      SpecGrid.mulLineCount = 1;
      SpecGrid.displayTitle = 1;
      SpecGrid.locked = 0;
      SpecGrid.canSel = 0;
      SpecGrid.hiddenPlus = 1;
      SpecGrid.hiddenSubtraction = 1;
      SpecGrid.loadMulLine(iArray);

      //RiskGrid. selBoxEventFuncName = "showRiskResult";
    } catch(ex) {
      alert(ex);
    }
}

function initHide(tCaseNo,tInsuredNo)
{
	fm.all('CaseNo').value = tCaseNo;
  fm.all('InsuredNo').value = tInsuredNo;
	
}

 </script>