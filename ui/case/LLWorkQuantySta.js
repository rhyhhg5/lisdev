//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}

function Quantity()
{
	if (verifyInput() == false)
    return false;
	var tStartDate = fm.StartDate.value;
    var tEndDate = fm.EndDate.value;
    var result = fm.result.value;
    var isWeek = fm.isweek.value;
    if(isWeek!='Y'){
        if(result=='Y'){
            if(dateDiff(tStartDate,tEndDate,"M")>3){
 //               alert("上班时间，统计期最多为三个月！");
 //               return false;
            }
        }
     }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "Quantity";
	fm.action = "LLWorkQuantyStaSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function Quality()
{
	if (verifyInput() == false)
    return false;
	var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  if(dateDiff(tStartDate,tEndDate,"M")>3){
//    alert("统计期最多为三个月！");
//    return false;
  }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "Quality";
	fm.action = "LLWorkQualityStaSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function Efficiency()
{
	if (verifyInput() == false)
    return false;
	var tStartDate = fm.StartDate.value;
  var tEndDate = fm.EndDate.value;
  var result = fm.result.value;
  var isWeek = fm.isweek.value;
  if(isWeek!='Y'){
      if(result=='Y'){
          if(dateDiff(tStartDate,tEndDate,"M")>3){
//              alert("上班时间，统计期最多为三个月！");
 //             return false;
          }
      }
   }
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "Efficiency";
	fm.action = "LLWorkEfficientStaSub.jsp"
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

			function afterCodeSelect( cCodeName, Field )
			{
				//选择了处理
				if( cCodeName == "DealWith")
				{
					switch(fm.all('DealWith').value){
						case "0":
						Quantity();             //工作量统计
						break;
						case "1":
						Quality();                 //工作质量统计
						break;
						case "2":
						Efficiency();              //工作效率统计
						break;
						case "3":
						Efficiency();              //工作效率统计
					}
				}
			}
