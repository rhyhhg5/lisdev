<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：CaseQuery.jsp
//程序功能：查询分立案信息
//创建日期：2002-08-13 11:10:36
//创建人  ：lixy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	System.out.println("---hahahahah---");
  // 保单信息部分
  LLCaseSchema tLLCaseSchema   = new LLCaseSchema();
  
  tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
    
  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLLCaseSchema);

  // 数据传输
  CaseUI tCaseUI   = new CaseUI();
	if (!tCaseUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tCaseUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tCaseUI.getResult();
		
		// 显示
		LLCaseSet mLLCaseSet = new LLCaseSet(); 
		mLLCaseSet.set((LLCaseSet)tVData.getObjectByObjectName("LLCaseBLSet",0));
		int n = mLLCaseSet.size();
		System.out.println("get case "+n);
		LLCaseSchema mLLCaseSchema = mLLCaseSet.get(1);
		%>
		<script language="javascript">
		   top.opener.fm.CaseNo.value="<%=mLLCaseSchema.getCaseNo()%>";
		   top.opener.fm.SubReportNo.value="<%=mLLCaseSchema.getSubRptNo()%>";
		   top.opener.fm.CustomerNo.value="<%=mLLCaseSchema.getCustomerNo()%>";
		   top.opener.fm.CustomerName.value="<%=mLLCaseSchema.getCustomerName()%>";
		   top.opener.emptyUndefined();
		</script>
			<%
	} // end of else
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tCaseUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	top.close();
</script>
</html>

