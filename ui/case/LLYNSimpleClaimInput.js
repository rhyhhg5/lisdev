/*******************************************************************************
* Name: RegisterInput.js
* Function:立案主界面的函数处理程序
* Author:LiuYansong
*/
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var case_flag ;//判断是否选择了分案号,初始化时是1，若没有选中则是0。
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tSaveFlag = "0";
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
/*******************************************************************************
* Name        :main_init
* Author      :LiuYansong
* CreateDate  :2003-12-16
* ModifyDate  :
* Function    :初始化主程序
* Parameter   :A_jsp---fm.action;Target_Flag (0--无target，1有target),T_jsp--target 对应的页面 ;Info---提示信息
*
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info)
{
	var i = 0;
	var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action ="./"+A_jsp;
	//alert("target标志是"+Target_Flag);
	if(Target_Flag=="1")
	{
		fm.target = "./"+T_jsp;   
	}
else
	{
		fm.target = "fraSubmit";
	}
	//showSubmitFrame(mDebug);
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	FlagDel = FlagStr;
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
else
	{
//		var strsql = "select a.RgtState,b.codename,a.Operator,a.MakeDate from LLcase a, ldcode b where a.caseno='"+fm.CaseNo.value +"'"
//		+" and b.codetype='llrgtstate' and b.code=a.rgtstate"
//		arrResult = easyExecSql(strsql);
//
//		if ( arrResult )
//		{
//			fm.RgtState.value= arrResult[0][1];
//			fm.Handler.value= arrResult[0][2];
//			//fm.ModifyDate.value= arrResult[0][4];
//			fm.ModifyDate.value = getCurrentDate();
//
//			initTitle();
//			ClientafterQuery( fm.RgtNo.value,fm.CaseNo.value );
//		}

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//showDiv(operateButton,"true");
		//showDiv(inputButton,"false");
	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
else
	{
		cDiv.style.display="none";
	}
}

//打印理赔申请书的函数
function PLPsqs()
{
	main_init("PLPsqs.jsp","1","f1print","打印");
	showInfo.close();
}
//连接报案查询界面的函数
function RgtQueryRpt()
{
	tSaveFlag = "Rpt";
	showInfo=window.open("FrameReportQuery.jsp");
}

function ClientQuery()
{
  var  wherePart = getWherePart("a.InsuredNo", 'CustomerNo' )
  + getWherePart("a.name", 'CustomerName' )
  + getWherePart( 'a.GrpContNo','GrpContNo' )
  + getWherePart('a.othidno','SecurityNo');
  if ( wherePart == ""){
    alert("请输入查询条件");
    return false;
  }

  var strSQL0 ="select distinct a.insuredno,a.name,a.sex,a.birthday,a.idno,a.othidno,a.insuredstat,b.grpname ";
  var sqlpart1="from lcinsured a,ldperson b where b.CustomerNo=a.insuredNo ";
  var sqlpart2="from lbinsured a,ldperson b where b.CustomerNo=a.insuredNo ";
  var strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;
  //+ " and b.AddressNo= a.AddressNo ";
  var arr= easyExecSql( strSQL,1,0,1 );

  if ( arr==null ){
    alert("没有符合条件的数据");
    return false;
  }
  try
  {
    //如果查询出的数据是一条，显示在页面，如果是多条数据填
    //出一个页面，显示在mulline中
    if(arr.length==1){
      afterLLRegister(arr);
      QueryEvent();
    }
    else{
      var varSrc = "&CustomerNo=" + fm.CustomerNo.value;
      varSrc += "&CustomerName=" + fm.CustomerName.value;
      varSrc += "&GrpContNo=" + fm.GrpContNo.value;
      varSrc += "&IDNo=" + fm.IDNo.value;
      varSrc += "&SecurityNo=" + fm.SecurityNo.value;
      showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLSPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
    }
  }
  catch(ex){
    alert("错误:"+ ex.message);
  }
}

function afterLLRegister(arr)
{
  if(arr!=null)
  {
    fm.CustomerNo.value   =arr[0][0];
    var ttsql = "select * from llcase where CustomerNo = '"+fm.CustomerNo.value 
    + "' and RgtNo = '"+fm.RgtNo.value+"'";
    var xrr = easyExecSql(ttsql);
    if(xrr){
      if(!confirm("该批次下已经录入了该客户，要继续吗？"))
        return false;
    }
    fm.CustomerName.value =arr[0][1];
    fm.Sex.value          =arr[0][2];
    if(arrResult[0][2]==0)
    fm.SexName.value = "男";
    if(arrResult[0][2]==1)
    fm.SexName.value = "女";
    if(arrResult[0][2]==2)
    fm.SexName.value = "不详";
    fm.CBirthday.value =arr[0][3];
    fm.IDNo.value      =arr[0][4];
    fm.SecurityNo.value =arr[0][5];
    fm.InsuredStat.value =arr[0][6];
    if(arrResult[0][6]==1)
    fm.SexName.value = "在职";
    if(arrResult[0][6]==1)
    fm.SexName.value = "退休";
    fm.GrpName.value = arr[0][7];
  }
  showPRemark();
}

function openAffix()
{
	var varSrc ="";
	pathStr="./CaseAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value+"&LoadFlag=1&LoadC="+fm.LoadC.value;
	showInfo = OpenWindowNew(pathStr,"","middle");
}

function submitFormSurvery()
{
	strSQL="select otherno from LLSurvey where otherno='"+fm.CaseNo.value+"'";
	arrResult = easyExecSql(strSQL);
	if(arrResult){
		
		 if(!confirm("该案件提起过调查，您确定要继续吗？"))
		    return false;
		}else{
		 
		}
	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&InsuredNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	varSrc += "&StartPhase=0";
	varSrc += "&Type=1";
	pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
}

function submitForm()
{
    if( verifyInput2() == false )
     return false;
		if(!checkIdCard(fm.IDNo.value)){
		  if(!confirm("要继续吗？"))
		    return false;
		}

		if(fm.CustomerNo.value==null||fm.CustomerNo.value=="")
		{
			alert("请输入客户数据");
		}
		else
		{
		      //556反洗钱黑名单客户的理赔监测功能
            /*var strblack=" select 1 from lcblacklist where trim(name)='"+fm.CustomerName.value+"' with ur";
            var crrblack=easyExecSql(strblack);
            if(crrblack){
              alert(fm.CustomerName.value+"客户为黑名单客户，须请示上级处理。");
              return false ;
            }*/
			   var checkName1 = fm.CustomerName.value;
			   var checkId1 =fm.IDNo.value;
		   	   if(!checkBlacklist(checkName1,checkId1)){
		       	return false;
		       }
				fm.all('operate').value = "INSERT||MAIN";

				strSQL = " select AppPeoples,customerno,grpname,rgtstate from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
				arrResult = easyExecSql(strSQL);
				if(arrResult != null){
				  if (arrResult[0][3]!='01'){
				    alert("该批次已受理申请完毕");
				    return;
				  }
				  
					var AppPeoples;
					try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
					var appntno= arrResult[0][1];
					strSQL = "select InsuredNo from lcpol where appntno='"+appntno+"' and InsuredNo='"
					+fm.all('CustomerNo').value+"'";
					crrResult = easyExecSql(strSQL);
					if(crrResult){
					}
					else{
					  if(!confirm("团体客户"+arrResult[0][2]+"未给个人客户"+fm.CustomerName.value+"投保，您确定要继续吗？"))
					    return false;
					}
				}
				else{
					alert("团体案件信息查询失败！");
					return;
				}
				
				//校验各种保全项目
			   if(!checkBQ()){
			   		return false;
			   }

				strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
				arrResult = easyExecSql(strSQL);
				if(arrResult != null){
					var RealPeoples;
					try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
				}
				if ( (eval(AppPeoples) - eval(RealPeoples)) < 5 ){
					alert("团体申请人数：" + AppPeoples + "实际已录入人数：" + RealPeoples);
				}
				if ( eval(RealPeoples) >= eval(AppPeoples) ){
//					if (!confirm("团体申请人数：" + AppPeoples + ", 实际已录入人数：" + RealPeoples + "你确认要补加个人客户吗?"));
//					{
//						return;
//					}
				}
		    //客户尚有未结案校
	    strSQL = "select caseno from llcase where rgtstate not in ('14','09','12','11') and customerno='"
	    +fm.all('CustomerNo').value+"' and caseno <> '"+fm.CaseNo.value+"' and rgtno='"+fm.all('rgtno').value+"'" ;
	    crrResultTemp = easyExecSql(strSQL);
	
	    if(crrResultTemp){
	      if(!confirm("本批次该客户有案件"+crrResultTemp+"未结案，您确定要继续吗？"))
	      return false;
	    }
	    else{
	
	    }	
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.action ="./LLYNSimpleClaimSave.jsp";
			fm.submit();
		}
}


//ClientCaseQueryInput.js查询返回接口2005-2-21 13:09
function ClientafterQuery(RgtNo,CaseNo)
{
	//团体批次
	if ( RgtNo!=null && RgtNo.length>0)
	{
		var arrResult = new Array();
		var strSQL="select c.otheridno,c.CustomerName,c.CustomerNo,c.IDNo,c.CustomerSex,c.custbirthday,"
		+"f.InsuredStat,c.grpname,f.hosatti,f.hospitalname,f.hospitalcode,f.inhosno,f.realhospdate,"
		+"f.feetype,f.feedate,f.hospstartdate,f.hospenddate,c.custstate,c.deathdate "
		+"from llcase c,llfeemain f where f.caseno=c.caseno and c.caseno='"+fm.CaseNo.value+"'";
		arrResult = easyExecSql(strSQL);
		if (CaseNo!=null && CaseNo.length>0)
		{
			if( arrResult != null )
			{
				fm.SecurityNo.value   =arrResult[0][0];
				fm.CustomerName.value =arrResult[0][1];
				fm.CustomerNo.value   =arrResult[0][2];
				fm.IDNo.value =arrResult[0][3];
				fm.Sex.value  =arrResult[0][4];
				fm.CBirthday.value   =arrResult[0][5];
				fm.InsuredStat.value =arrResult[0][6];
				fm.GrpName.value = arrResult[0][7]
				fm.HosSecNo.value= arrResult[0][8];
				fm.HospitalName.value= arrResult[0][9];
				fm.HospitalCode.value= arrResult[0][10];
				fm.inpatientNo.value =arrResult[0][11] ;
		    fm.RealHospDate.value=arrResult[0][12];
			  fm.FeeType.value= arrResult[0][13];
			  fm.FeeDate.value= arrResult[0][14];
			  var cureSQL = "SELECT diseasecode,diseasename from llcasecure where caseno='"+fm.CaseNo.value+"'";
			  curResult = easyExecSql(cureSQL);
			  if(curResult){
			    fm.ICDCode.value=curResult[0][0];
			    fm.ICDName.value=curResult[0][1];
			  }
			  fm.HospStartDate.value = arrResult[0][15];
			  fm.HospEndDate.value = arrResult[0][16];
			  fm.CustStatus.value = arrResult[0][17];
			  fm.DeathDate.value = arrResult[0][18];
		}
		var feeSQL = "SELECT applyamnt,selfamnt,selfpay2,getlimit,midamnt,planfee,standbyamnt,"
		    +"SupInHosFee,highamnt1,OfficialSubsidy FROM LLSECURITYRECEIPT where caseno ='"
		    +fm.CaseNo.value+"'";
	  turnPage.queryModal(feeSQL,FeeGrid);
			strSQL="select ReasonCode,Reason from LLAppClaimReason where 1=1 "
			+"and rgtno='"+RgtNo+"' "
			+" and caseno='"+CaseNo+"'";
			arrResult = easyExecSql(strSQL);
			if ( arrResult!=null )
			{

				for (i=0;i< arrResult.length ;i++)
				{
					var ind= eval(arrResult[i][0])-1;
				}
			}
			//事件查询
			QueryEvent();
			fm.all("CaseNo").value = CaseNo;
			fm.all("RgtNo").value =  RgtNo;
			strSQL="select RgtState,Operator,ModifyDate,handler,codename from llcase,ldcode where caseno='"+CaseNo+"' and code=RgtState and codetype='llrgtstate'";
			arrResult = easyExecSql(strSQL);
			if(arrResult!=null)
			{
				fm.all("Handler").value = arrResult[0][1];
				fm.all("ModifyDate").value = arrResult[0][2];
				fm.RgtState.value=arrResult[0][4];
				if(arrResult[0][3].substring(0,3)=="bcm")
				{
					fm.SimpleCase.checked = "true";
				}
			}
		strSQL = "select appealtype,case appealtype  when '1' then '纠错类' when '0' then '申诉类' end ,appeanrcode,appealreason,appealrdesc from LLAppeal where appealno ='"+fm.CaseNo.value+"'";
				brrResult = easyExecSql(strSQL);
				if(brrResult){
				fm.all('AppealType').value=brrResult[0][0];
				fm.all('AppealTypeName').value=brrResult[0][1];
				fm.all('AppeanRCode').value= brrResult[0][2];
				fm.all('AppealReason').value= brrResult[0][3];
				fm.all('AppealRDesc').value= brrResult[0][4];

		}
			showPRemark();	
		}
	}
}

//判断查询输入窗口的案件类型是否是回车，
//如果是回车调用查询客户函数
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClientQuery();
	}
}

//弹出窗口显示复选框选择受益人
function OpenBnf()
{
	var pathStr="./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerName.value+"&RgtNo="+fm.RgtNo.value+"&paymode="+fm.paymode.value;
	showInfo=OpenWindowNew(pathStr,"EventInput","middle");
}

//打印接口
function PrintPage()
{
	if(fm.CaseNo.value=="")
	{
		alert("请先保存数据");
		return false;
	}
	showInfo = window.open("./PayAppPrt.jsp?CaseNo="+fm.CaseNo.value,"PrintPage",'width=600,height=400,top=100,left=60,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function afterCodeSelect( cCodeName, Field ) {
	if( cCodeName == "llRelation" && Field.value=="00")
	{
		fm.RgtantName.value = fm.CustomerName.value;
		fm.IDTypeName.value=fm.tIDTypeName.value;
		fm.IDType.value = fm.tIDType.value;
		fm.IDNo  .value=fm.IDNo.value;

		    addSQL = "select b.PostalAddress,b.zipcode,b.phone,b.mobile,b.Email from lcinsured a,lcaddress b where b.AddressNo=a.AddressNo and b.CustomerNo=a.InsuredNo and a.InsuredNo='"+fm.CustomerNo.value+"'";
		    crr = easyExecSql( addSQL , 1,0,1);
		    if (crr)
		    {
		    	fm.RgtantAddress.value=crr[0][0];
		    	fm.PostCode.value =crr[0][1];
		    	fm.RgtantPhone.value = crr[0][2];
		    	fm.Mobile.value=crr[0][3];
		    	fm.Email.value = crr[0][4];
		    }
	}
	if( cCodeName == "llgetmode")
	{
		if(Field.value=="1"||Field.value=="2")
		{
			divBankAcc.style.display='none';
			fm.BankAccM.disabled=true;
		}
	else
		{
			divBankAcc.style.display='';
			fm.BankAccM.disabled=false;
			var strSQL ="select distinct a.Bankcode,a.Bankaccno,a.Accname "
			+"from lcinsured a where InsuredNo='"+fm.CustomerNo.value+"'";
			var arr= easyExecSql( strSQL,1,0,1 );
				if (arr){
				fm.BankCode.value=arr[0][0];
				fm.BankAccNo.value=arr[0][1];
				fm.AccName.value=arr[0][2];
			}
		}
	}
}

function RgtFinish()
{
	strSQL = " select AppPeoples,rgtstate from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
	arrResult = easyExecSql(strSQL);

	if(arrResult != null)
	{
		if ( arrResult[0][1]!='01')
		{
			alert("团体案件状态不允许该操作！");
			return;
		}
		var AppPeoples;
		try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
	}
else
	{
		alert("团体案件信息查询失败！");
		return;
	}


	strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		var RealPeoples;
		try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
	}
	strSQL = " select customername from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
	brr = easyExecSql(strSQL)
	if(brr)
	{
		count = brr.length;
	}

	if ( RealPeoples < AppPeoples )
	{
		if(confirm("团体申请下个人申请尚未全部申请完毕！" + "团体申请人数：" + AppPeoples + "实际申请人数：" + RealPeoples+ "\n您确认要结束本团体批次吗?"))
		{
			fm.realpeoples.value=RealPeoples;
			alert("本批次申请人数为："+RealPeoples+"人")
		}
	else
		{
			return;
		}
	}

//	if ( RealPeoples == AppPeoples )
//	{
			fm.realpeoples.value=RealPeoples;
		fm.all('operate').value = "UPDATE||MAIN";
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

		fm.action = "./LLGrpRegisterSave.jsp";
		fm.submit();
//	}

}

function CaseChange()
{
	initTitle();
	ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}

function QueryEvent()
{
	if(fm.CustomerNo.value!="")
	{

		var strSQL="select SubRptNo,AccDate,AccPlace,AccidentType,AccSubject,HospitalName,InHospitalDate,OutHospitalDate,AccDesc,"+
					"case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end," +"AccidentType from LLSubReport where CustomerNo='"
		+ fm.CustomerNo.value +"' order by AccDate desc";
		//turnPage.queryModal(strSQL, EventGrid);
		arrResult = easyExecSql(strSQL);
		if ( arrResult )
		{
			displayMultiline(arrResult,EventGrid);

			//查询关联上的事件
			strSQL = "select subrptno from llcaserela where caseno='" + fm.CaseNo.value +"'";
			var br = easyExecSql ( strSQL );
			//对已关联事件打勾
			if ( br)
			{
				pos =0;
				for ( i=0;i<arrResult.length;i++)
				{
					for ( j=0;j<br.length ;j++)
					{
						if ( arrResult[i][0]==	br[j][0] )
						{
							EventGrid.checkBoxSel(i+1);

						}
					}
				}
			}
		}
	}
}

function DealCancel()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.CustomerNo.value+"&CustomerName="+fm.CustomerName.value;
  showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}



function showPRemark() 
{
	
	var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
								+fm.CustomerNo.value+"'";
	var	crrResult1 = easyExecSql(strSQLx);
	if(crrResult1)
	{
		alert("客户："+crrResult1[0][1]+"为黑名单客户，您确定要继续吗？");
	
	}
	var grpResult = new Array();
	var perResult = new Array();
	var arrResult = new Array();
	var polResult = new Array();
  var temp="";
	var str1SQL="select grpcontno from lccont where "+
							" insuredno='"+fm.CustomerNo.value+"' "+
							" and grpcontno in (select grpcontno from lcgrpcont where appflag='1')"+
							" and appflag='1'";
	grpResult=easyExecSql(str1SQL);
	if(grpResult != null&&grpResult != '')
	{
		for ( i = 0; i <= grpResult.length ;i++)
		{	
			var strSQL="select '保单'||grpcontno||'约定:'||remark from lcgrpcont "+
								 "where grpcontno='"+grpResult[i]+"'";
			arrResult=easyExecSql(strSQL);
				
			var polSQL="Select '保障计划'||CONTPLANCODE||'下险种'||RISKCODE||coalesce((select codename from ldcode where codetype='CalTitle' and code=c.calfactor),'')||'约定:'||remark FROM LCCONTPLANDUTYPARAM c "+
								" where grpcontno='"+grpResult[i]+"' AND CONTPLANCODE!='11' and remark !='' "+
								 "GROUP BY CONTPLANCODE,RISKCODE,remark,calfactor";
			polResult=easyExecSql(polSQL);								
			if(arrResult != null&&arrResult != '')
			{
				try
				{			
					temp += arrResult+"\n";													
				}
				catch(ex)
				{
					alert(ex.message)
				}
			}
			if(polResult !=null&&polResult != '')
			{
				try
				{
					temp += polResult+"\n";
				}
				catch(ex)
				{
					alert(ex.message)
				}
			}
		}		
	} 		
		var str2SQL="select contno from lccont where "+
							" insuredno='"+fm.CustomerNo.value+"' "+
							" and appflag='1'" ;
	  perResult=easyExecSql(str2SQL);		  
		if(perResult != null&&perResult != '')
	{
		for ( i = 0; i <= perResult.length ;i++)
		{	
			var str3SQL="select '保单'||contno||'下险种'||(select riskcode from lcpol p where p.polno=c.polno)||'约定:'||speccontent from lcpolspecrela c "+
									" where contno ='"+perResult[i]+"'";
					arrResult=easyExecSql(str3SQL);	
					
			if(arrResult != null&&arrResult != '')
			{		
					try
				{
					temp +=	arrResult+"\n";
				}
				catch(ex)
				{
					alert(ex.message)
				}
			}else{}								
	  }
	}
		fm.all('PerRemark').value=temp.replace(/\,/g,"\n");		
		if(temp=="")
		{
			fm.all('PerRemark').value="无特别约定，请依照条款处理";	
		}										    	
}

function ContQuery()
{

	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&InsuredNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	pathStr="./FrameMainContQuery.jsp?Interface=LLContQueryInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLContQueryInput","middle",800,330);

}

//计算实际住院天数
function caldate()
{
  if(fm.HospStartDate.value!=""&&fm.HospEndDate.value!="")
  {
    fm.HospStartDate.value=modifydate(fm.HospStartDate.value);
    fm.HospEndDate.value=modifydate(fm.HospEndDate.value);
    //	var a=dateDiff(fm.HospStartDate.value,fm.HospEndDate.value,"D")-1;
    var strSql= "select to_char(to_date('"+ fm.HospEndDate.value + "') - to_date('" + fm.HospStartDate.value +"')) from dual"
    //alert(strSql );
    var mrr = easyExecSql(strSql);
    if (mrr!=null)
    {
      a= mrr[0][0] ;
      if(fm.FeeType.value=="1")
        a=a/1+1;
      if ( a<0) 
        fm.RealHospDate.value =0 ;
      else
        fm.RealHospDate.value= a;
    }
  }
}

function queryGrpCaseGrid()
{
	var jsRgtNo = fm.all('RgtNo').value;
	 	
	  var strSql = " SELECT C.CASENO, C.OTHERIDNO,C.CUSTOMERNAME, C.CUSTOMERNO, C.RGTDATE, B.APPLYAMNT"
	  	        + " FROM LLCASE C, LLSECURITYRECEIPT b  WHERE C.RGTNO = '" 
	  	        + jsRgtNo + "' and b.CASENO= c.CASENO ORDER BY C.CASENO " ;
      turnPage1.pageLineNum = 3;
	  turnPage1.queryModal(strSql,GrpCaseGrid);
}

function ShowCase()
{
  var selno = GrpCaseGrid.getSelNo()-1;
  if (selno >=0)
  {
    fm.CaseNo.value = GrpCaseGrid.getRowColData( selno, 1);
  }
  ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}

function checkMoney(){
  
}

//by gzh 20120912
//整理对保全的校验
function checkBQ(){
	var tError = "";//非阻断错误
	var tZDError = "";//阻断错误
	var tTorG = "";//若无对应的保全项目，该变量仅赋值，不使用。
	var customerno=fm.CustomerNo.value;
	//正在进行中，提示不阻断
	var tIngSql = " select a.grpcontno,a.contno,b.edortype,b.edorno, "
			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
			+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
			+ " where a.grpcontno=b.grpcontno "
			+ " and a.insuredno='"+customerno+"' "
			+ " and a.grpcontno!='00000000000000000000' "
			+ " and c.edoracceptno=b.edorno"
			+ " and c.edorstate != '0' "
			+ " and b.edortype in ('RR','RS','AC','WJ','YS')"
			+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
			+ " union all "
			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
			+ " from lcinsured a,lpedoritem b,lpedorapp c "
			+ " where a.contno=b.contno "
			+ " and a.insuredno='"+customerno+"' "
			+ " and a.grpcontno='00000000000000000000' "
			+ " and c.edoracceptno=b.edorno"
			+ " and c.edorstate != '0' "
			+ " and b.edortype in ('ZF','PC','AD','YS','AE')"
			+ " group by a.grpcontno,a.contno,b.edortype,b.edorno ";
	var tIngArr = easyExecSql(tIngSql);
 
	if(tIngArr){//存在正在进行中的保全项目
		for(var i=0;i<tIngArr.length;i++){
			if(tIngArr[i][0] != null && tIngArr[i][0] != "" && tIngArr[i][0] != "00000000000000000000"){
				tTorG = "团单(保单号码："+tIngArr[i][0]+")";
				
			}else{
				tTorG = "个单(保单号码："+tIngArr[i][1]+")";
			}
			//提示不阻断
			tError +="该客户所在"+tTorG+"正在进行"+tIngArr[i][4]+"保全操作,工单号为："+tIngArr[i][3]+"。 \n";
		}
	}
	//正在进行中，提示且阻断
	var tIngSql1 = "select * from ( "
				+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    			+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
    			+ " where a.grpcontno=b.grpcontno "
    			+ " and a.insuredno='"+customerno+"' "
    			+ " and a.grpcontno!='00000000000000000000' "
    			+ " and c.edoracceptno=b.edorno"
    			+ " and c.edorstate != '0' "
    			+ " and b.edortype in ('WT','XT','GA','TQ','TA','SG','CT','TF','LQ','ZB') "//团单保全
    			+ " union all "
    			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    			+ " from lcinsured a,lpedoritem b,lpedorapp c "
    			+ " where a.contno=b.contno "
    			+ " and a.insuredno='"+customerno+"' "
    			+ " and a.grpcontno='00000000000000000000' "
    			+ " and c.edoracceptno=b.edorno"
    			+ " and c.edorstate != '0' "
    			+ " and b.edortype in ('PR','BP','BA','FC','FX','WT','GF','RF','LN','BF','XT','BC','CM','TB','NS','TF','CT','WX','ZB','LQ','RB')"//个单保全
    			+ " ) as temp "
    			+ " group by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname "
    			+ " order by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname ";
	var tIngArr1 = easyExecSql(tIngSql1);
	if(tIngArr1){
		for(var i=0;i<tIngArr1.length;i++){
			if(tIngArr1[i][0] != null && tIngArr1[i][0] != "" && tIngArr1[i][0] != "00000000000000000000"){
				tTorG = "团单(保单号码："+tIngArr1[i][0]+")";
				
			}else{
				tTorG = "个单(保单号码："+tIngArr1[i][1]+")";
			}
			tZDError +="该客户所在"+tTorG+"正在进行"+tIngArr1[i][4]+"保全操作,工单号为："+tIngArr1[i][3]+"。 \n";
		}
	}
	//该被保人所在分单的保全项目
	var tIngSql2 = " select a.grpcontno,a.contno,b.edortype,b.edorno, "
				+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
				+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
				+ " where a.grpcontno=b.grpcontno "
				+ " and a.insuredno='"+customerno+"' "
				+ " and a.grpcontno!='00000000000000000000' "
				+ " and c.edoracceptno=b.edorno"
				+ " and c.edorstate != '0' "
				+ " and b.edortype in ('LP','ZT','BC','CM','WD','JM')"
				+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
				+ " order by a.grpcontno,a.contno,b.edortype,b.edorno ";
    var tIngArr2 = easyExecSql(tIngSql2);
    if(tIngArr2){
    	for(var i=0;i<tIngArr2.length;i++){
    		tTorG = "团单(保单号码："+tIngArr2[i][0]+")";
    		var tempSql = " select 1 from lpinsured a "
					 + " where a.grpcontno='"+tIngArr2[i][0]+"' "
					 + " and a.contno = '"+tIngArr2[i][1]+"' "
					 + " and a.edortype = '"+tIngArr2[i][2]+"' "
					 + " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate<>'0') "
					 + " union all "
					 + " select 1 from lpbnf a "
					 + " where 1=1 "
					 + " and a.contno = '"+tIngArr2[i][1]+"' "
					 + " and a.edortype = '"+tIngArr2[i][2]+"' "
					 + " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate<>'0') "
					 + " with ur ";
			var tempArr = easyExecSql(tempSql);
			if(tempArr){
				if(tIngArr2[i][2] == "LP"){
					tError +="该客户正在"+tTorG+"中进行"+tIngArr2[i][4]+"保全操作,工单号为："+tIngArr2[i][3]+"。 \n";
				}else{
					tZDError +="该客户正在"+tTorG+"中进行"+tIngArr2[i][4]+"保全操作,工单号为："+tIngArr2[i][3]+"。 \n";
				}
			}
    	}
    }
	//保全处理完成
	var tDoneSql = "select * from ( "
			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
    		+ " where a.grpcontno=b.grpcontno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno!='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('RS','WT','XT','CT') "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lcinsured a,lpedoritem b,lpedorapp c "
    		+ " where a.contno=b.contno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('FC','WT','XT','CT') "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lbinsured a,lpgrpedoritem b,lpedorapp c"
    		+ " where a.grpcontno=b.grpcontno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno!='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('RS','WT','XT','CT') "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lbinsured a,lpedoritem b,lpedorapp c "
    		+ " where a.contno=b.contno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('FC','WT','XT','CT') "
    		+ " ) as temp "
    		+ " group by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname "
    		+ " order by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname ";
	var tDoneArr = easyExecSql(tDoneSql);
	if(tDoneArr){//存在已完成的保全项目
		for(var i=0;i<tDoneArr.length;i++){
			if(tDoneArr[i][0] != null && tDoneArr[i][0] != "" && tDoneArr[i][0] != "00000000000000000000"){
				tTorG = "团单(保单号码："+tDoneArr[i][0]+")";
				
			}else{
				tTorG = "个单(保单号码："+tDoneArr[i][1]+")";
			}
			//由20121115修改，需求调整，做完保全的，仅提示不阻断
			tError +="该客户已在"+tTorG+"中做过"+tDoneArr[i][4]+"保全操作，工单号为："+tDoneArr[i][3]+"。 \n";
		}
	}
	//没有保全号的保全项目(各种满期给付各种特殊处理)。。。。//0 给付完成；1 正在给付。
	//常无忧B满期给付
	var tCWMJSql = " select temp.ContNo,temp.edorstate "
			   + " from "
			   + " ( "
			   + " select a.contno," 
			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate "
			   + " from ljsgetdraw a where insuredno='"+customerno+"' and feefinatype='TF' and riskcode='330501' "
			   + " ) as temp"
			   + " group by temp.ContNo,temp.edorstate "
			   + " order by temp.ContNo,temp.edorstate ";
	var tCWMJArr = 	easyExecSql(tCWMJSql);
	if(tCWMJArr){
		for(var i=0;i<tCWMJArr.length;i++){
			tTorG = "个单(保单号码："+tCWMJArr[i][0]+")";
			if(tCWMJArr[i][1] == "1"){
				tZDError +="该客户所在"+tTorG+"正在进行常无忧B满期给付保全操作。 \n";
			}
		}
	}
	//个单满期给付
	var tGDMJSql = " select temp.ContNo,temp.edorstate "
			   + " from "
			   + " ( "
			   + " select a.contno," 
			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate "
			   + " from ljsgetdraw a where insuredno='"+customerno+"' and feefinatype='TF' and riskcode not in ('170206','330501') "
			   + " ) as temp"
			   + " group by temp.ContNo,temp.edorstate "
			   + " order by temp.ContNo,temp.edorstate ";
	var tGDMJArr = 	easyExecSql(tGDMJSql);
	if(tGDMJArr){
		for(var i=0;i<tGDMJArr.length;i++){
			tTorG = "个单(保单号码："+tGDMJArr[i][0]+")";
			if(tGDMJArr[i][1] == "0"){//需求调整，做完保全的仅提示不阻断
				tError +="该客户已在"+tTorG+"中做过个单满期给付保全操作。 \n";
			}else{
				tZDError +="该客户所在"+tTorG+"正在进行个单满期给付保全操作。 \n";
			}
		}
	}   
	var tMJSql = " select temp.GrpContNo,temp.edorName,temp.edorstate "
	           + " from "
	           + " ( "
	           + " select a.grpcontno GrpContNo,'团单满期给付' edorName," //团单满期给付
			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate"
			   + " from ljsgetdraw a,lcinsured b "
			   + " where a.grpcontno = b.grpcontno and b.grpcontno != '00000000000000000000' "
			   + " and a.feefinatype='TF' and a.riskcode='170206' "
			   + " and b.insuredno='"+customerno+"' "
//			   + " union all "
//			   + " select a.contno GrpContNo,'特需险满期给付' edorName,'1' edorstate" //特需险满期给付
//			   + " from lgwork a,lcinsured b " 
//			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
//			   + " and a.typeno='070015' "
//			   + " and b.insuredno='"+customerno+"' "
//			   + " and a.statusno != '5' "
//			   + " union all "
//			   + " select a.contno GrpContNo,'特需险满期给付' edorName,'0' edorstate" //特需险满期给付
//			   + " from lgwork a,lcinsured b " 
//			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
//			   + " and a.typeno='070015' "
//			   + " and b.insuredno='"+customerno+"' "
//			   + " and a.statusno = '5' "
			   + " ) as temp "
			   + " group by temp.GrpContNo,temp.edorName,temp.edorstate "
			   + " order by temp.GrpContNo,temp.edorName,temp.edorstate "
			   + " with ur "; 
	var tMJArr = easyExecSql(tMJSql);
	if(tMJArr){
		for(var i=0;i<tMJArr.length;i++){
			tTorG = "团单(保单号码："+tMJArr[i][0]+")";
			if(tMJArr[i][2] == "0"){//需求调整，做完保全的，仅提示不阻断
				tError +="该客户已在"+tTorG+"中做过"+tMJArr[i][1]+"保全操作。 \n";
			}else{
				tZDError +="该客户所在"+tTorG+"正在进行"+tMJArr[i][1]+"保全操作。 \n";
			}
		}
	}
	//所在团单正在定期结算
	var tDJSql = " select distinct a.contno "
			   + " from lgwork a,lcinsured b "
			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
			   + " and b.grpcontno not in (select code from ldcode where codetype='lp_dqjs_pass') "
			   + " and a.typeno like '06%' and a.statusno not in ('5','8') "
			   + " and b.insuredno='"+customerno+"' ";
	var tDJArr = 	easyExecSql(tDJSql);
	if(tDJArr){
		for(var i=0;i<tDJArr.length;i++){
			tTorG = "团单(保单号码："+tDJArr[i][0]+")";
			tZDError +="该客户所在"+tTorG+"正在进行定期结算保全操作。 \n";
		}
	}
	//若存在多个保单，则仅提示不阻断
	var tCSQL = "select count(1) from lcinsured where insuredno = '"+customerno+"' "
			  + " and exists (select 1 from lccont where prtno = lcinsured.prtno and appflag = '1') "
			  + "having count(1) >1 ";
	var tCArr = 	easyExecSql(tCSQL);
	if(tCArr){
		tError = tError + tZDError;
		tZDError = "";
	}
	if(tError != ""){//提示不阻断处理
		tError +="是否继续？";
		if(!confirm(tError)){
			return false;
		}
	}
	if(tZDError != ""){//提示不阻断处理
		alert(tZDError);
		return false;
	}
	return true;
}

//#2684关于理赔环节黑名单监测规则修改的需求  added by zqs
function checkBlacklist(checkName,checkId){
	//需要验证的证件号与人名都为空
	if((checkId==""||checkId==null)&&(checkName==""||checkName==null)){
		return true;
	}
	//需要验证的证件号为空
	if(checkId==""||checkId==null){
		var strblack="select 1 from lcblacklist where trim(name)in('"+checkName+"') and (idno='' or idno is null) with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   	   	  		//alert("若“证件号码”在黑名单库中为空");
   	   	  		if(!confirm("客户为黑名单客户,是否继续处理此案件?")){
   	   	      		return false;
   	   	    	}
		}
		return true;
	}
	//需要验证的姓名为空
	if(checkName==""||checkName==null){
		var strblack=" select 1 from lcblacklist where trim(idno)in('"+checkId+"')with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   			//alert("姓名”不匹配为空，“证件号码”匹配");
   			if(!confirm("客户为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		return true;
	}
	
	//1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
	var strblack=" select name,idno from lcblacklist where trim(name)in('"+checkName+"') or trim(idno)in('"+checkId+"') with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   		var strblack1=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and trim(idno)in('"+checkId+"') with ur";
   		var arrblack1=easyExecSql(strblack1);
   		if(arrblack1){
   			alert("客户为黑名单客户");
   	   	  	return false;
   		}
   		var strblack2=" select 1 from lcblacklist where trim(idno)in('"+checkId+"') with ur";
   		var arrblack2=easyExecSql(strblack2);
   		if(arrblack2){
   			if(!confirm("客户为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		var strblack3=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and (trim(idno)in('') or idno is null) with ur";
   		var arrblack3=easyExecSql(strblack3);
   		if(arrblack3){
   			if(!confirm("客户为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
     }
     return true;
}