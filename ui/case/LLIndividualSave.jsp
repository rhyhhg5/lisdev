<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ClienReasonSave.jsp
//程序功能：
//创建日期：2005-02-18 08:49:52
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	//声明Schema Set
	
  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);

	LICaseTimeRemindSchema tLICaseTimeRemindSchema = new LICaseTimeRemindSchema();
	LICaseTimeRemindSet tLICaseTimeRemindSet = new LICaseTimeRemindSet();
	PubFun pf = new PubFun();
	PubFun1 pf1 = new PubFun1();
	FDate fDate = new FDate();
	//声明VData
	VData tVData = new VData();
	//声明后台传送对象
	LLValidityBL tLLValidityBL = new LLValidityBL();
	//输出参数
	CErrors tError = null;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String EventFlag="";	//事件申请标记 为1 有客户存在事件，为0不存在事件
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	System.out.println("-------------------------"+tG);

	
    System.out.println("----------------校验证件的有效性");
    String toDate = pf.getCurrentDate();
    System.out.println("toDate:"+toDate);
    String endDate = request.getParameter("IDEndDate");
    System.out.println("endDate:"+endDate);
    String serialno = "LS"+pf.getCurrentDate2()+""+ pf1.CreateMaxNo("LSH_TimeRemind", 8);
    System.out.println("serialno:"+serialno);
    tLICaseTimeRemindSchema.setSerialNo(serialno);
    
    tLICaseTimeRemindSchema.setinsuredno(request.getParameter("CustomerNo"));
    System.out.println("CustomerNo:"+request.getParameter("CustomerNo"));
    
    tLICaseTimeRemindSchema.setinsuredname(request.getParameter("CustomerName"));
    System.out.println("CustomerName:"+request.getParameter("CustomerName"));
    
    String sql = "select code from ldcode where 1=1 and codetype='sex' and codename='" + request.getParameter("ShowSexName") + "' with ur";
    ExeSQL tExeSQL = new ExeSQL();
    String mSex = tExeSQL.getOneValue(sql);
    System.out.println("mSex:"+mSex);
    tLICaseTimeRemindSchema.setsex(mSex);

    tLICaseTimeRemindSchema.setbirthday(request.getParameter("CBirthday"));
    System.out.println("CBirthday:"+request.getParameter("CBirthday"));
    
    tLICaseTimeRemindSchema.setidtype(request.getParameter("tIDType"));
    System.out.println("tIDType:"+request.getParameter("tIDType"));
    
    tLICaseTimeRemindSchema.setidno(request.getParameter("tIDNo"));
    System.out.println("tIDNo:"+request.getParameter("tIDNo"));
    
    tLICaseTimeRemindSchema.setIDStartDate(request.getParameter("IDStartDate"));
    System.out.println("startDate:"+request.getParameter("IDStartDate"));
    
    tLICaseTimeRemindSchema.setIDEndDate(request.getParameter("IDEndDate"));
    System.out.println("endDate:"+request.getParameter("IDEndDate"));
    
    tLICaseTimeRemindSchema.setReminddate(pf.getCurrentDate());
    System.out.println("Date1:"+pf.getCurrentDate());
    
    tLICaseTimeRemindSchema.setRemindTime(pf.getCurrentTime());
    System.out.println("Date2:"+pf.getCurrentTime());
    
    tLICaseTimeRemindSchema.setaddress(request.getParameter("ShowPostalAddress"));
    System.out.println("ShowPostalAddress:"+request.getParameter("ShowPostalAddress"));
    
    tLICaseTimeRemindSchema.setnativeplace(request.getParameter("ShowNativePlace"));
    System.out.println("ShowNativePlace:"+request.getParameter("ShowNativePlace"));
    
    tLICaseTimeRemindSchema.setManageCom(request.getParameter("CaseRuleMng"));
    System.out.println("CaseRuleMng:"+request.getParameter("CaseRuleMng"));
    
    tLICaseTimeRemindSchema.setOperator(request.getParameter("Handler"));
    System.out.println("Handler:"+request.getParameter("Handler"));
    
    tLICaseTimeRemindSchema.setMakeDate(pf.getCurrentDate());
    tLICaseTimeRemindSchema.setMakeTime(pf.getCurrentTime());  
    tLICaseTimeRemindSchema.setmodifydate(pf.getCurrentDate());
    tLICaseTimeRemindSchema.setmodifytime(pf.getCurrentTime());
    
    tLICaseTimeRemindSet.add(tLICaseTimeRemindSchema);
    
  try                                 
  {                                   
  // 准备传输数据 VData               
	//VData传送
	System.out.println("<--Star Submit VData-->");
	tVData.add(tLICaseTimeRemindSet);

  	tVData.add(tG);
  	System.out.println("<--Into tClientRegisterBL-->");
  	tLLValidityBL.submitData(tVData,strOperate);
  } 
  catch(Exception ex)
  { 
    Content = "保存失败，原因是: " + ex.toString();
    FlagStr = "Fail";
  } 
    
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if ("".equals(FlagStr))
  { 
    tError = tLLValidityBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	tVData.clear();
    	tVData=tLLValidityBL.getResult();              
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("结束了");
  
  %>
                   
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>