<html>
	<%
		//Name：LLCustomerMail.jsp
		//Function：客户信箱页面
		//Date：2004-12-23 16:49:22
		//Author：Xx
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="com.sinosoft.utility.*"%>
	<%@page import="com.sinosoft.lis.schema.*"%>
	<%@page import="com.sinosoft.lis.vschema.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
	<%@page import="java.util.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
		String Operator = tG.Operator;
		String Comcode = tG.ManageCom;

		String CurrentDate = PubFun.getCurrentDate();
		String AheadDays = "-30";
		FDate tD = new FDate();
		Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate), Integer
				.parseInt(AheadDays), "D", null);
		FDate fdate = new FDate();
		String afterdate = fdate.getString(AfterDate);
	%>

	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLDownLoadInput.js"></SCRIPT>
		<%@include file="LLDownLoadInputInit.jsp"%>
		<script language="javascript">
   function initDate(){
     
     
   }
   </script>
	</head>
	<body onload="initDate();initForm();">
		<form action="./" method=post name=fm
			target="fraSubmit">
			<table>
				<TR>
					<TD>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLLLLMainAskInput1);">
					</TD>
					<TD class=titleImg>
						报表下载
					</TD>
				</TR>
			</table>
			<Div id="divLLLLMainAskInput2" style="display: ''">
				<table class=common>

					<TR class=common8>
						<TD class=title>
							用户名
						</TD>
						<TD class=input>
							<Input class=common8 name=UserCode onkeydown="QueryOnKeyDown()">
						</TD>
				   </tr>
				</table>
			</Div>

			<Div id="divCustomerInfo" style="display: ''">
				<table class=common>
					<TR class=common>
						<TD text-align: left colSpan=1>
							<span id="spanCheckGrid"> </span>
						</TD>
					</TR>
				</table>
				<Div id="divPage" align=center style="display: 'none' ">
					<INPUT CLASS=cssButton VALUE="首页" TYPE=button
						onclick="turnPage.firstPage();">
					<INPUT CLASS=cssButton VALUE="上一页" TYPE=button
						onclick="turnPage.previousPage();">
					<INPUT CLASS=cssButton VALUE="下一页" TYPE=button
						onclick="turnPage.nextPage();">
					<INPUT CLASS=cssButton VALUE="尾页" TYPE=button
						onclick="turnPage.lastPage();">
				</Div>
				<Div id="divLLLLMainAskInput2" style="display: ''">
					<table class=common>
						<TR>

							<TD width='80%'>

								<INPUT class="cssbutton" style='width:80' TYPE=button VALUE="报表预览"
									onclick="SaveFile()">
							</TD>
						</TR>
					</table>
				</Div>

			</Div>




			<Div style="display: 'none' ">
				<hr />
			</Div>
			<!--隐藏域-->
			<Input type="hidden" class=common name="operate">
		</form>
		<iframe name="printfrm" src="" width=0 height=0></iframe>
		<form method=post id=printform target="printfrm" action="">
			<input type="hidden" name=filename value="" />
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
