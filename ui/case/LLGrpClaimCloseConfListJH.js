var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var tSaveType="";
//提交，保存按钮对应操作
function submitForm()
{

  if ( fm.LogNo.value!="")
      {
      	alert("你不能执行改操作");
      	return false;
      	}
    if (confirm("您确实想保存该记录吗?"))
    {
      tSaveFlag = "1";


        var i = 0;
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
        //showSubmitFrame(mDebug);


        fm.fmtransact.value="INSERT||MAIN";
        fm.submit(); //提交
        tSaveFlag ="0";


    }
    else
    {
      alert("您取消了修改操作！");
    }

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
fm.RgtGiveEnsure1.disabled=false;
fm.BigRgtGiveEnsure1.disabled=false;

//fm.QDRgtGiveEnsure1.disabled=false;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    if (fm.fmtransact.value=="DELETE||MAIN")
    {
    	fm.reset();
    	initCustomerGrid();
    }
    //SearchGrpRegister();
    divGrpCaseInfo.style.display = 'none';
 //   showDiv(operateButton,"true");
 //   showDiv(inputButton,"false");

//    if(fm.all('SendFlag').value=="OK")
//    {//执行下一步操作(发送email、短信)
//  	var ExeSql = "select returnmode,rgtantmobile,email,rgtantname from llregister where rgtno='"+fm.RgtNo.value+"'";
//  	arr=easyExecSql(ExeSql);
//  	if(arr)
//  	{
//  	  //fm.AnswerMode.value=arr[0][0];
//  	  fm.Mobile.value=arr[0][1];
//  	  fm.Email.value=arr[0][2];
//
//  	  fm.SMSContent.value="尊敬的"+arr[0][3]+"先生/女士,您的理赔申请已受理。"
//  	                      +"如有疑问，敬请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
//  	                      +"发信人：中国人保健康"
//  	  fm.EmailContent.value="尊敬的"+arr[0][3]+"先生/女士,您的理赔申请已受理。"
//  	                      +"如有疑问，敬请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
//  	                      +"发信人：中国人保健康"
//  	  //alert(fm.Mobile.value);
//  	  //
//  	  alert(fm.Email.value);
//  	  fm.action="./GrpSendMail.jsp";
//  	  //alert();
//  	  fm.submit();
//  	  //alert();
//  	}
//
//   }

  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


function UWClaim()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择一条团体案件！");
	      return ;
	}

		//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
		var varSrc="";
		var newWindow = window.open("./FrameMain.jsp?Interface=PayAffirmInput.jsp"+varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SearchGrpRegister()
{
  var startDate = fm.RgtDateStart.value;
  var endDate = fm.RgtDateEnd.value;
  if(fm.srRgtNo.value=='' && fm.srCustomerNo.value=='' && fm.srGrpName.value=='' && fm.srRgtantName.value==''){
  if (startDate==""||startDate==null||endDate==""||endDate==null){
  	alert("请输入受理日期");
  	return false;
  }

  }
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  var StateRadio="";
  for(i = 0; i <fm.StateRadio.length; i++){
    if(fm.StateRadio[i].checked){
      StateRadio=fm.StateRadio[i].value;
      break;
    }
  }
  // 书写SQL语句

 var strSql = " SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO, R.RGTANTNAME, R.RGTDATE,"
 		+" (select count(caseno) from llcase where rgtno=R.rgtno), b.codename,R.rgtstate,"
 		+" coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where rgtno=r.rgtno  ),0)," 
 		+" (select codename from ldcode where r.TogetherFlag=code and codetype='lltogetherflag' ) ,"
 		+" (select codename from ldcode where r.CaseGetMode=code and codetype='paymode' ),r.RgtantName,"
 		+" r.BankCode,r.AccName,r.BankAccNo,"
 		+" (CASE WHEN r.rgtstate in ('04','05') THEN "
 		+" (SELECT NVL(SUM(-pay),0) FROM ljagetclaim m WHERE m.otherno=r.rgtno AND m.othernotype='Y' ) "
 		+" ELSE 0 END), "
 		+" (CASE WHEN r.rgtstate in ('04','05') THEN "
 		+" (SELECT NVL(SUM(sumgetmoney),0) FROM ljaget m where m.othernotype in ('Y','5') AND m.otherno=r.rgtno) "
 		+" ELSE 0 END) "
  	    +" FROM LLREGISTER R , ldcode b " 
  	    +" WHERE  R.RGTOBJ = '0' AND R.RGTCLASS = '1' and togetherflag='4' AND R.declineflag is null  "
  	    +" and r.MngCom like '8612%' and exists ( select 1 from llcase a ,llhospcase b where a.caseno=b.caseno and casetype='03' and hospitcode='JH02' and a.rgtno=r.rgtno) " 
 //+" and b.codetype='llgrprgtstate' and b.code = r.rgtstate and r.GRPNAME like '"+fm.srGrpName.value+"%'"
  	    +" and b.codetype='llgrprgtstate' and b.code = r.rgtstate "
  	    + getWherePart("r.GRPNAME","srGrpName","like")
  + getWherePart("r.MngCom","ManageCom","like")
  + getWherePart("R.RGTNO","srRgtNo")
 // + getWherePart("R.ApplyerType","ApplyerType")
  + getWherePart("R.CUSTOMERNO","srCustomerNo")
  //+ getWherePart("R.GRPNAME","srGrpName")
  + getWherePart("R.RGTANTNAME","srRgtantName","like")
  + getWherePart("R.RGTDATE","RgtDateStart",">=")
  + getWherePart("R.RGTDATE","RgtDateEnd","<=");
  switch(StateRadio){
    case "0":
    sqlpart = " ORDER BY r.rgtno ";
    break;
    case "1":
    sqlpart = " and r.rgtstate = '01' ORDER BY r.rgtno ";
    break;
    case "2":
    sqlpart = " and r.rgtstate='02' ORDER BY r.rgtno ";
    break;
    case "3":
    sqlpart = " and r.rgtstate='03' ORDER BY r.rgtno ";
    break;
    case "4":
    sqlpart = " and r.rgtstate='05' ORDER BY r.rgtno ";
    break;
    case "5":
    sqlpart = " and r.rgtstate='04' ORDER BY r.rgtno ";
    break;
    case "6":
    sqlpart = " and r.rgtstate in ('03','05') ORDER BY r.rgtno ";
  }
  strSql+=sqlpart;
  turnPage.queryModal(strSql,GrpRegisterGrid);
  showInfo.close();
}
//---团体给付确认---
function RgtGiveEnsure()
{
	fm.RgtGiveEnsure1.disabled=true;
	fm.all('SendFlag').value="OK";
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择一条团体案件！");
	      return ;
	}

	fm.all('RgtNo').value = GrpRegisterGrid.getRowColData(selno-1, 1); //批次号	
	var trgtstate = GrpRegisterGrid.getRowColData(selno-1, 9);   
	
	if(trgtstate=='04'||trgtstate=='05'){
	  alert("该团体案件已做完给付确认！");
	  return false;
	}
	if(trgtstate=='01'){
	  alert("当前状态下不能做该团体案件的给付确认！");
	  return false;
	}
	//--------避免给付方式领取方式选择错误，增加给付方式领取方式等信息提示----------Begin---------
	var tGrpName= GrpRegisterGrid.getRowColData(selno-1, 3); 		//投保单位
	var tTogetherFlag= GrpRegisterGrid.getRowColData(selno-1, 11); //给付方式
	var tCaseGetMode= GrpRegisterGrid.getRowColData(selno-1, 12); //领取方式
	var tRgtantName= GrpRegisterGrid.getRowColData(selno-1, 13);  //领取人
	var tBankCode= GrpRegisterGrid.getRowColData(selno-1, 14);    //银行代码
	var tAccName= GrpRegisterGrid.getRowColData(selno-1, 15);     //账户名
	var tBankAccNo= GrpRegisterGrid.getRowColData(selno-1, 16);   //账号	

   //556反洗钱黑名单客户的理赔监测功能
  	   /*var strblack=" (select 1 from lcblacklist where trim(name)in('"+tRgtantName+"')) union (select 1 from llcase where rgtno='"+fm.all('RgtNo').value+"' and exists (select 1 from lcblacklist where trim(name)=llcase.customername) )with ur";
   	   var crrblack=easyExecSql(strblack);
   	   if(crrblack){
    	   alert("该单位下出险人或者领取人为黑名单客户，须请示上级处理。");
    	   return false;
    	  }*/
	//#2684关于理赔环节黑名单监测规则修改的需求
	var rgtno1 = GrpRegisterGrid.getRowColData(selno-1, 1);
	var strblack=" select IDNo from LLRegister where rgtno= '"+ rgtno1 +"' with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   	  checkId1 = arrblack[0][0].trim();
   	}
   	
	if(!checkBlacklist(tRgtantName,checkId1)){
	   return false;
	}
	var strblack2 = "select customername,IDNO from llcase where rgtno='"+fm.all('RgtNo').value+"'";
	var arrblack2=easyExecSql(strblack2);
   	if(arrblack2){
   		for(var i =0;i<arrblack2.length;i++){
   	 		checkName2 = arrblack2[i][0].trim();
   	 		checkId2 = arrblack2[i][1].trim();
   	 		if(!checkBlacklist(checkName2,checkId2)){
       			return false;
       		}
   		}
   	}

	//-----------增加个人给付校验--Begin--
	if(tTogetherFlag=="个人给付")
	{
		alert("团体案件的给付方式为个人给付!团体批次号为："+fm.all('RgtNo').value);
		return false;		
	}
	//-----------增加个人给付校验--End--
	var ModeMsg="";
	var TogetherMsg="";
	if(tCaseGetMode=="银行转账"||tCaseGetMode=="银行汇款")//银行转账需要提示银行信息
	{
		ModeMsg="\n银行代码为"+tBankCode+",账户名为"+tAccName+",账号为"+tBankAccNo;
	}
	if(tTogetherFlag=="部分统一给付")//部分统一给付需要提示已经给付案件数，待给付案件数
	{
		var sql1="select count(1) from llcase where rgtno='"+fm.all('RgtNo').value+"' and rgtstate in ('11','12')";
		var sql2="select count(1) from llcase where rgtno='"+fm.all('RgtNo').value+"' ";
		var count1= easyExecSql(sql1);  //已经给付案件
		var count2= easyExecSql(sql2);  //全部案件数
		if(count2)
		{
			TogetherMsg="\n案件已经给付"+parseInt(count1)+"个,待统一给付"+parseInt(count2-count1)+"个";
		}
	}
	if (!confirm("请确认案件"+fm.all('RgtNo').value+"的给付方式为"+tTogetherFlag+"，领取方式为"+tCaseGetMode+"，领取人为"+tRgtantName+ModeMsg+TogetherMsg))
	{
		fm.RgtGiveEnsure1.disabled=false;
		return false;
	}
    //--------避免给付方式领取方式选择错误，增加给付方式领取方式等信息提示----------End---------
    	//--------校验青岛无名单是否已经结案------------Begin------------
	var tGrpContNo= GrpRegisterGrid.getRowColData(selno-1, 4);   //合同号	
	var QDRgt="select count(1) from llregister where rgtno='"+fm.all('RgtNo').value+"' and GrpName='青岛市医疗保险管理中心'";
	if(easyExecSql(QDRgt)>=1)
	{ 
		var sql1="select count(1) from ljsgetclaim where otherno ='"+fm.all('RgtNo').value+"'";
		if(easyExecSql(sql1)<=0)
		{			
			alert("请确认团体审批审定后是否进行了【团体结案】操作!" + "团体批次号：" + fm.all('RgtNo').value);
			fm.RgtGiveEnsure1.disabled=false;
			return false;
		}
	}
	 //---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
   var PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a " 
   		+" where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and grpcontno='"+ tGrpContNo +"')"
   var tPrepaidBala=easyExecSql(PrepaidBalaSql);

   if(tPrepaidBala!="" && tPrepaidBala!=null && tPrepaidBala > 0 && fm.PrePaidFlag.checked==false )
   {
	  if(!confirm("确定案件不回销预付赔款?"))
	  {
		  fm.RgtGiveEnsure1.disabled=false;
  		 return false;
      }	
   }
   if((tPrepaidBala == null || tPrepaidBala == "" || tPrepaidBala <=0)&& fm.PrePaidFlag.checked==true)
   {
   		alert("被保险人投保的保单不存在预付未回销赔款!");
   		fm.RgtGiveEnsure1.disabled=false;
   		return false;
   }
   //----------------End---
	var tPreSql="";
//    if(fm.PrePaidFlag.checked == true)
//    {
//    	 tPreSql ="select coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where rgtno=r.rgtno),0),"
//    		+" (SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE X.realpay END),0) "
//	 		+" FROM (SELECT rgtno,grpcontno,grppolno,sum(realpay) realpay FROM llclaimdetail "
//	 		+" GROUP BY rgtno,grpcontno,grppolno) AS X, "
//	 		+" llprepaidgrppol Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno AND X.rgtno=r.rgtno), "
//	 		+" (select coalesce(sum(realpay),0)  from llclaimdetail where rgtno=r.rgtno)- "
//	 		+" (SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE X.realpay END),0) "
//	 		+" FROM (SELECT rgtno,grpcontno,grppolno,sum(realpay) realpay FROM llclaimdetail "
//	 		+" GROUP BY rgtno,grpcontno,grppolno) AS X, "
//	 		+" llprepaidgrppol Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno AND X.rgtno=r.rgtno) "
//    		+" from llregister r where rgtno='"+ fm.all('RgtNo').value +"'"
//    }else if(fm.PrePaidFlag.checked == false){
//        tPreSql ="select coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where rgtno=r.rgtno),0),"
//    		+" '0',coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where rgtno=r.rgtno),0) "
//    		+" from llregister r where rgtno='"+ fm.all('RgtNo').value +"'"  
//    }


    if(fm.PrePaidFlag.checked==true ){
		tPreSql = "select nvl(Z.realpay,0),nvl(Z.prepaid,0), "
			+ " nvl(z.realpay,0)-nvl(Z.prepaid,0) "
			+ " from llregister r left join "
			+ " (select X.rgtno rgtno,sum(X.realpay) realpay, "
			+ " sum(case when X.realpay>db2inst1.nvl(Y.prepaidbala,0) then db2inst1.nvl(Y.prepaidbala,0) else X.realpay end) prepaid "
			+ " from ( "
			+ " select a.rgtno rgtno,b.grpcontno grpcontno,b.grppolno grppolno,db2inst1.nvl(sum(b.pay),0) realpay "
			+ " from llcase a,ljsgetclaim b where a.caseno=b.otherno and a.rgtno='" + fm.all('RgtNo').value
			+ "' group by a.rgtno,b.grpcontno,b.grppolno ) AS X left join llprepaidgrppol Y "
			+ " on X.grpcontno=Y.grpcontno and X.grppolno=Y.grppolno group by X.rgtno) AS Z "
			+ " on r.rgtno=Z.rgtno where r.rgtno='" + fm.all('RgtNo').value + "' ";
	    var tPrepaidArr=easyExecSql(tPreSql);
	   	var tRealpay = tPrepaidArr[0][0];
		var tPrepaid = tPrepaidArr[0][1];
		var tPay = tPrepaidArr[0][2];
		tPay = formatFloat(tPay,2);  
			
		if (tPrepaid > 0) {
		    //if (!confirm("批次赔款"+tRealpay+",回销预付赔款"+tPrepaid+",实际给付"+tPay+"。是否继续")) {
		    //	fm.RgtGiveEnsure1.disabled=false;
		    //    return false;
		    //}
		    if (!confirm("批次赔款"+tRealpay+",回销预付赔款"+tPrepaid+"。是否继续?")) {
		    	fm.RgtGiveEnsure1.disabled=false;
		        return false;
		    }
		}
	}
	//--------校验青岛无名单是否已经结案------------End------------
  	fm.all('operate').value	= "GIVE|ENSURE";
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action="./LLGrpClaimCloseConfListSave.jsp";
    fm.submit(); //提交

}
//小数点精度
function formatFloat(src, pos)
{
	var num = new Number(src);
	var result = num.toFixed(pos);
    return result;
}

//给付凭证打印
function GPrint()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印给付凭证的团体案件");
	      return ;
	}

 // fm.target="f1print";
 // fm.fmtransact.value="PRINT";

	rgtNo = GrpRegisterGrid.getRowColData( selno-1, 1);

		strSQL="select ActuGetNo from LJAGet where OtherNo='"+rgtNo+"'" +"and OtherNoType='5' and (finstate<>'FC' or finstate is null)";
		arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
		        ActuGetNo = arrResult[0][0];

    		}
    	if(ActuGetNo == null || ActuGetNo == "")
    	{
    		alert("实付号码为空，团体案件尚未给付确认！");
    		return;
    	}
        var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'lp008' and standbyflag2='"+ActuGetNo+"'");

	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{

		fm.action="./LLClaimMainListPrintSave.jsp?ActuGetNo="+ActuGetNo+"&RgtClass=1";
		fm.submit();
	}
	else
	{
		//window.open("../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ActuGetNo+"&PrtSeq="+PrtSeq);
		fm.action = "../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ ActuGetNo +"&PrtSeq="+PrtSeq;
		fm.submit();
	}
	//var newWindow = window.open("./LLClaimMainListPrintSave.jsp?ActuGetNo=" + ActuGetNo + "&RgtClass=1"  + "&fmtransact=PRINT");
}
function printPDF()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var caseNo;
	if (selno <= 0)
	{
		alert("请选择要打印给付凭证的理赔案件");
		return ;  
	}
	var rgtNo = GrpRegisterGrid.getRowColData( selno-1, 1);
        strSQL="select ActuGetNo from LJAGet where OtherNo='"+rgtNo+"'" +"and OtherNoType='5'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		ActuGetNo = arrResult[0][0];

	}
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'lp008' and standbyflag2='"+ActuGetNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		alert("提取数据失败-->PrtSeq为空");
		return false;
	}
	//window.open("../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ActuGetNo+"&PrtSeq="+PrtSeq );
	fm.action="../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ActuGetNo+"&PrtSeq="+PrtSeq;
	fm.submit();
}
// 打印汇总明细表
function GrpColPrt()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印给付明细的团体案件");
	      return ;
	}

	RgtNo = GrpRegisterGrid.getRowColData( selno-1, 1);
	strSQL="select rgtstate,togetherflag from llregister where rgtno='"+RgtNo+"'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
	      rgtstate = arrResult[0][0];
	      togetherflag = arrResult[0][1];

    }
  	if(rgtstate == "01" || rgtstate == "02")
  	{
  		alert("此状态下不能打印！");
  		return;
  	}
   var code;
   if(RgtNo.substr(1, 2)=="12"){
   	    code="lp011";
   }else if(RgtNo.substr(1, 2)=="11"){
   	    code="lp003bj";
   }else{
   	    code="lp003";
   }
   var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '"+code+"' and standbyflag2='"+RgtNo+"'");

	if(code =="lp003bj")
	{	
		alert(code);
	    var newWindow = window.open("PayColPrtBJ.jsp?RgtNo=" + RgtNo+ "&fmtransact=PRINT");
		
		
	}
	//else if(code =="lp011")
	//{	
	//    var newWindow = window.open("PayColPrtTJ.jsp?RgtNo=" + RgtNo+ "&fmtransact=PRINT");	
	//}	
	else if((PrtSeq==null || PrtSeq=="" || PrtSeq=="null")&&code !="lp003bj")
	{
		fm.action="./PayColPrt.jsp?RgtNo=" + RgtNo;
		fm.submit();
	}
	else 
	{	
		fm.action = "../uw/PrintPDFSave.jsp?Code=0"+code+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ RgtNo +"&PrtSeq="+PrtSeq;
		fm.submit();
	}
}
function GRPrint()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印赔付报告的团体保单");
	      return ;
	}
	if (fm.RgtDateStart.value==null||fm.RgtDateStart.value==''){
	  alert("请输入申请起始日期");
	  return;
	}
	if (fm.RgtDateEnd.value==null||fm.RgtDateEnd.value==''){
	  alert("请输入申请结束日期");
	  return;
	}
	GrpNo = GrpRegisterGrid.getRowColData( selno-1, 2);
	GrpContNo = GrpRegisterGrid.getRowColData( selno-1, 4);
  fm.target="GRPrt1";
  fm.fmtransact.value="PRINT";
  alert("正式环境暂时取消此处打印功能，请登录查询机，在'理赔案件-->理赔统计-->团体保单赔付报告'下继续操作");
  return false;
  //var newWindow = window.open("GrpReportPrt.jsp?GrpNo=" + GrpNo+ "&GrpContNo="+GrpContNo+"&fmtransact=PRINT&RgtDateStart="+fm.RgtDateStart.value+"&RgtDateEnd="+fm.RgtDateEnd.value);
}
function GRDPrint()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印赔付报告的团体保单");
	      return ;
	}

	GrpNo = GrpRegisterGrid.getRowColData( selno-1, 2);
	GrpContNo = GrpRegisterGrid.getRowColData( selno-1, 4);
  fm.target="GRPrt2";
  fm.fmtransact.value="PRINT";
  alert("正式环境暂时取消此处打印功能，请登录查询机，在'理赔案件-->理赔统计-->团体保单赔付报告'下继续操作");
  return false;
  //var newWindow = window.open("GrpReportPrtD.jsp?GrpNo=" + GrpNo+ "&GrpContNo="+GrpContNo+"&fmtransact=PRINT");
}

function queryGrpCaseGrid()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <= 0)
	{
	      return ;
	}
	var jsRgtNo = GrpRegisterGrid.getRowColData(selno - 1, 1);

	if(jsRgtNo == null || jsRgtNo == "")
		return;

	  var strSql = " SELECT C.CASENO, C.CUSTOMERNO, C.CUSTOMERNAME, C.RGTDATE, b.codename" +
	  	       " FROM LLCASE C, ldcode b  " +
	  	       " WHERE C.RGTNO = '" + jsRgtNo + "'" +
	  	       " and b.codetype='llrgtstate' and b.code = c.rgtstate " +
	  	       " ORDER BY C.CASENO ";

	  turnPage2.queryModal(strSql,GrpCaseGrid);
	  divGrpCaseInfo.style.display = '';
	  
	  var tPreSql="select prepaidflag from llregister where rgtno='"+ jsRgtNo +"'";
	  var tPreFlag= easyExecSql(tPreSql);
	  if(tPreFlag != null && tPreFlag!= "" && tPreFlag==1)
	  {
	  	fm.PrePaidFlag.checked = true;
	  }else{
	  	fm.PrePaidFlag.checked = false;
	  }
}

function QueryOnKeyDown()
{
	var keycode = event.keyCode;

	if(keycode=="13")
	{
		SearchGrpRegister();
	}
}

function DealCancel()
{
	var rgtno = '';
	var selno = GrpRegisterGrid.getSelNo()-1;
	if (selno >=0)
	{
		rgtno = GrpRegisterGrid.getRowColData(selno,1);
		GrpName = GrpRegisterGrid.getRowColData(selno,3);
		GrpNo = GrpRegisterGrid.getRowColData(selno,2);
	}
	var varSrc="&CaseNo=" + rgtno +"&CustomerNo="+GrpNo+"&CustomerName="+GrpName+"&LoadFlag=2";
showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=750,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

	function afterCodeSelect( cCodeName, Field )
	{
		if( cCodeName == "grprint" && Field.value=="1")
		{
			GRPrint();
		}
		if( cCodeName == "grprint" && Field.value=="2")
		{
			GRDPrint();
		}
	}

function QDRgtGiveEnsure()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择一条团体案件！");
	      return ;
	}

	fm.all('RgtNo').value = GrpRegisterGrid.getRowColData(selno-1, 1);
	var trgtstate = GrpRegisterGrid.getRowColData(selno-1, 9);
	if(trgtstate=='04'||trgtstate=='05'){
	  alert("该团体案件已做完给付确认！");
	  return false;
	}
	if(trgtstate=='01'){
	  alert("当前状态下不能做该团体案件的给付确认！");
	  return false;
	}
		//--------避免给付方式领取方式选择错误，增加给付方式领取方式等信息提示----------Begin---------
	var tGrpName= GrpRegisterGrid.getRowColData(selno-1, 3); 		//投保单位
	var tTogetherFlag= GrpRegisterGrid.getRowColData(selno-1, 11); //给付方式
	var tCaseGetMode= GrpRegisterGrid.getRowColData(selno-1, 12); //领取方式
	var tRgtantName= GrpRegisterGrid.getRowColData(selno-1, 13);  //领取人
	var tBankCode= GrpRegisterGrid.getRowColData(selno-1, 14);    //银行代码
	var tAccName= GrpRegisterGrid.getRowColData(selno-1, 15);     //账户名
	var tBankAccNo= GrpRegisterGrid.getRowColData(selno-1, 16);   //账号	
	var tRealpay = GrpRegisterGrid.getRowColData(selno-1, 10);
	var tPrepaid = GrpRegisterGrid.getRowColData(selno-1, 17);
	var tPay = GrpRegisterGrid.getRowColData(selno-1, 18);
	
	//556反洗钱黑名单客户的理赔监测功能
  	  /* var strblack=" (select 1 from lcblacklist where trim(name)in('"+tRgtantName+"')) union (select 1 from llcase where rgtno='"+fm.all('RgtNo').value+"' and exists (select 1 from lcblacklist where trim(name)=llcase.customername) )with ur";
   	   var crrblack=easyExecSql(strblack);
   	   if(crrblack){
    	   alert("该单位下出险人或者领取人为黑名单客户，须请示上级处理。");
    	   return false;
    	  }*/
	//#2684关于理赔环节黑名单监测规则修改的需求
	var rgtno1 = GrpRegisterGrid.getRowColData(selno-1, 1);
	var strblack=" select IDNo from LLRegister where rgtno= '"+ rgtno1 +"' with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   	  checkId1 = arrblack[0][0].trim();
   	}
   	
	if(!checkBlacklist(tRgtantName,checkId1)){
	   return false;
	}
	var strblack2 = "select customername,IDNO from llcase where rgtno='"+fm.all('RgtNo').value+"'";
	var arrblack2=easyExecSql(strblack2);
   	if(arrblack2){
   		for(var i =0;i<arrblack2.length;i++){
   	 		checkName2 = arrblack2[i][0].trim();
   	 		checkId2 = arrblack2[i][1].trim();
   	 		if(!checkBlacklist(checkName2,checkId2)){
       			return false;
       		}
   		}
   	}
	//-----------增加个人给付校验--Begin--
	if(tTogetherFlag=="个人给付")
	{
		alert("团体案件的给付方式为个人给付!团体批次号为："+fm.all('RgtNo').value);
		return false;		
	}
	//-----------增加个人给付校验--End--
	var ModeMsg="";
	var TogetherMsg="";
	if(tCaseGetMode=="银行转账"||tCaseGetMode=="银行汇款")//银行转账需要提示银行信息
	{
		ModeMsg="\n银行代码为"+tBankCode+",账户名为"+tAccName+",账号为"+tBankAccNo;
	}
	if(tTogetherFlag=="部分统一给付")//部分统一给付需要提示已经给付案件数，待给付案件数
	{
		var sql1="select count(1) from llcase where rgtno='"+fm.all('RgtNo').value+"' and rgtstate in ('11','12')";
		var sql2="select count(1) from llcase where rgtno='"+fm.all('RgtNo').value+"' ";
		var count1= easyExecSql(sql1);  //已经给付案件
		var count2= easyExecSql(sql2);  //全部案件数
		if(count2)
		{
			TogetherMsg="\n案件已经给付"+parseInt(count1)+"个,待统一给付"+parseInt(count2-count1)+"个";
		}
	}
	if (!confirm("请确认案件"+fm.all('RgtNo').value+"的给付方式为"+tTogetherFlag+"，领取方式为"+tCaseGetMode+"，领取人为"+tRgtantName+ModeMsg+TogetherMsg))
	{
		return false;
	}
    //--------避免给付方式领取方式选择错误，增加给付方式领取方式等信息提示----------End---------
	//--------校验青岛无名单是否已经结案------------Begin------------
	var tGrpContNo= GrpRegisterGrid.getRowColData(selno-1, 4);   //合同号	
	var QDRgt="select count(1) from llregister where rgtno='"+fm.all('RgtNo').value+"' and GrpName='青岛市医疗保险管理中心'";
	if(easyExecSql(QDRgt)>=1)
	{ 
		var sql1="select count(1) from ljsgetclaim where otherno ='"+fm.all('RgtNo').value+"'";
		if(easyExecSql(sql1)<=0)
		{			
			alert("请确认团体审批审定后是否进行了【团体结案】操作!" + "团体批次号：" + fm.all('RgtNo').value);
			return false;
		}
	}
	if (tPrepaid > 0) {
    	//if (!confirm("理算赔款"+tRealpay+",回销预付赔款"+tPrepaid+",实际给付"+tPay+"。是否继续")) {
        //	return false;
    	//}
    	if (!confirm("理算赔款"+tRealpay+",回销预付赔款"+tPrepaid+"。是否继续?")) {
        	return false;
    	}
	}
	//--------校验青岛无名单是否已经结案------------End------------
    //---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
   var PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a "
   		+" where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and grpcontno='"+ tGrpContNo +"')"
   var tPrepaidBala=easyExecSql(PrepaidBalaSql);

   if(tPrepaidBala!="" && tPrepaidBala!=null && tPrepaidBala > 0 && fm.PrePaidFlag.checked==false )
   {
	  if(!confirm("确定案件不回销预付赔款?"))
	  {
  		 return false;
      }	
   }
   if((tPrepaidBala == null || tPrepaidBala == "" || tPrepaidBala <=0)&& fm.PrePaidFlag.checked==true)
   {
   		alert("被保险人投保的保单不存在预付未回销赔款!");
   		return false;
   }
   //----------------End---
  	fm.all('operate').value	= "QDGIVE|ENSURE";
  	//fm.QDRgtGiveEnsure1.disabled=true;
  	fm.RgtGiveEnsure1.disabled=true;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交

}
function YNRgtGiveEnsure()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <=0)
	{
	      alert("请选择一条团体案件！");
	      return ;
	}

	fm.all('RgtNo').value = GrpRegisterGrid.getRowColData(selno-1, 1);
	var trgtstate = GrpRegisterGrid.getRowColData(selno-1, 9);
	if(trgtstate=='04'||trgtstate=='05'){
	  alert("该团体案件已做完给付确认！");
	  return false;
	}
	if(trgtstate=='01'){
	  alert("当前状态下不能做该团体案件的给付确认！");
	  return false;
	}
		//--------避免给付方式领取方式选择错误，增加给付方式领取方式等信息提示----------Begin---------
	var tGrpName= GrpRegisterGrid.getRowColData(selno-1, 3); 		//投保单位
	var tTogetherFlag= GrpRegisterGrid.getRowColData(selno-1, 11); //给付方式
	var tCaseGetMode= GrpRegisterGrid.getRowColData(selno-1, 12); //领取方式
	var tRgtantName= GrpRegisterGrid.getRowColData(selno-1, 13);  //领取人
	var tBankCode= GrpRegisterGrid.getRowColData(selno-1, 14);    //银行代码
	var tAccName= GrpRegisterGrid.getRowColData(selno-1, 15);     //账户名
	var tBankAccNo= GrpRegisterGrid.getRowColData(selno-1, 16);   //账号	

   //556反洗钱黑名单客户的理赔监测功能
  	  /* var strblack=" (select 1 from lcblacklist where trim(name)in('"+tRgtantName+"')) union (select 1 from llcase where rgtno='"+fm.all('RgtNo').value+"' and exists (select 1 from lcblacklist where trim(name)=llcase.customername) )with ur";
   	   var crrblack=easyExecSql(strblack);
   	   if(crrblack){
    	   alert("该单位下出险人或者领取人为黑名单客户，须请示上级处理。");
    	   return false;
    	  }*/
	//#2684关于理赔环节黑名单监测规则修改的需求
	var rgtno1 = GrpRegisterGrid.getRowColData(selno-1, 1);
	var strblack=" select IDNo from LLRegister where rgtno= '"+ rgtno1 +"' with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   	  checkId1 = arrblack[0][0].trim();
   	}
   	
	if(!checkBlacklist(tRgtantName,checkId1)){
	   return false;
	}
	var strblack2 = "select customername,IDNO from llcase where rgtno='"+fm.all('RgtNo').value+"'";
	var arrblack2=easyExecSql(strblack2);
   	if(arrblack2){
   		for(var i =0;i<arrblack2.length;i++){
   	 		checkName2 = arrblack2[i][0].trim();
   	 		checkId2 = arrblack2[i][1].trim();
   	 		if(!checkBlacklist(checkName2,checkId2)){
       			return false;
       		}
   		}
   	}


	//-----------增加个人给付校验--Begin--
	if(tTogetherFlag=="个人给付")
	{
		alert("团体案件的给付方式为个人给付!团体批次号为："+fm.all('RgtNo').value);
		return false;		
	}
	//-----------增加个人给付校验--End--
	var ModeMsg="";
	var TogetherMsg="";
	if(tCaseGetMode=="银行转账"||tCaseGetMode=="银行汇款")//银行转账需要提示银行信息
	{
		ModeMsg="\n银行代码为"+tBankCode+",账户名为"+tAccName+",账号为"+tBankAccNo;
	}
	if(tTogetherFlag=="部分统一给付")//部分统一给付需要提示已经给付案件数，待给付案件数
	{
		var sql1="select count(1) from llcase where rgtno='"+fm.all('RgtNo').value+"' and rgtstate in ('11','12')";
		var sql2="select count(1) from llcase where rgtno='"+fm.all('RgtNo').value+"' ";
		var count1= easyExecSql(sql1);  //已经给付案件
		var count2= easyExecSql(sql2);  //全部案件数
		if(count2)
		{
			TogetherMsg="\n案件已经给付"+parseInt(count1)+"个,待统一给付"+parseInt(count2-count1)+"个";
		}
	}
	if (!confirm("请确认案件"+fm.all('RgtNo').value+"的给付方式为"+tTogetherFlag+"，领取方式为"+tCaseGetMode+"，领取人为"+tRgtantName+ModeMsg+TogetherMsg))
	{
		return false;
	}
    //--------避免给付方式领取方式选择错误，增加给付方式领取方式等信息提示----------End---------
    //--------校验青岛无名单是否已经结案------------Begin------------
	var tGrpContNo= GrpRegisterGrid.getRowColData(selno-1, 4);   //合同号	
	var QDRgt="select count(1) from llregister where rgtno='"+fm.all('RgtNo').value+"' and GrpName='青岛市医疗保险管理中心'";
	if(easyExecSql(QDRgt)>=1)
	{ 
		var sql1="select count(1) from ljsgetclaim where otherno ='"+fm.all('RgtNo').value+"'";
		if(easyExecSql(sql1)<=0)
		{			 
			alert("请确认团体审批审定后是否进行了【团体结案】操作!" + "团体批次号：" + fm.all('RgtNo').value);
			return false;
		}
	}
	//--------校验青岛无名单是否已经结案------------End------------
	//---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
   var PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a "
   		+" where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and grpcontno='"+ tGrpContNo +"')"
   var tPrepaidBala=easyExecSql(PrepaidBalaSql);

   if(tPrepaidBala!="" && tPrepaidBala!=null && tPrepaidBala > 0 && fm.PrePaidFlag.checked==false )
   {
	  if(!confirm("确定案件不回销预付赔款?"))
	  {
  		 return false;
      }	
   }
   if((tPrepaidBala == null || tPrepaidBala == "" || tPrepaidBala <=0)&& fm.PrePaidFlag.checked==true)
   {
   		alert("被保险人投保的保单不存在预付未回销赔款!");
   		return false;
   }
   //----------------End---
   	var tRealpay = GrpRegisterGrid.getRowColData(selno-1, 10);
	var tPrepaid = GrpRegisterGrid.getRowColData(selno-1, 17);
	var tPay = GrpRegisterGrid.getRowColData(selno-1, 18);
	
	if (tPrepaid > 0) {
    	//if (!confirm("批次赔款"+tRealpay+",回销预付赔款"+tPrepaid+",实际给付"+tPay+"。是否继续")) {
        //	return false;
    	//}
    	if (!confirm("批次赔款"+tRealpay+",回销预付赔款"+tPrepaid+"。是否继续?")) {
        	return false;
    	}
	}
  	fm.all('operate').value	= "GIVE|ENSURE";
  	fm.RgtGiveEnsure1.disabled=true;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit(); //提交
}

function SendMail(){
	showInfo.close();
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "邮件发送成功！" ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
}
function SendMsg( ttext,tto,MsgFlag )
{
	showInfo.close();
		var Flag=MsgFlag;
  		if (MsgFlag) {
				   var tsname= "service@picchealth.com";
   				  var tlogin="理赔咨询回复";
   				  var tmobile=tto;//fm.all('to').value;

				var tUrl="http://211.100.6.183/TSmsPortal/smssend?dt="+tmobile+"&feecode=000000&feetype=01&svid=picch&spno=5467&msg="+ ttext +"&reserve=0000000000000000&type=0";
				var win=window.open(tUrl,"fraSubmit",'height=110,width=220,top=260,left=400,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');

				if (win.result==0){
			      var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "短信发送" ;
						showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
					 }
			 else{
						var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "短信发送成功！" ;
						showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
					  }
          }

}
function ChangePayMode()
{
	var rgtno = '';
	var selno = GrpRegisterGrid.getSelNo()-1;
	if (selno >=0){
		rgtno = GrpRegisterGrid.getRowColData(selno,1);
	}else{
		alert("请选择一个批次");
		return;
	}
	var varSrc="&RgtNo=" + rgtno +"&LoadFlag=2";
  showInfo = window.open("./FrameMainRgtChangePayMode.jsp?Interface=LLRgtChangePayMode.jsp"+varSrc,"团体给付方式修改",'width=750,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}
function NewGPrint()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印给付凭证的团体案件");
	      return ;
	}

 // fm.target="f1print";
 // fm.fmtransact.value="PRINT";

	rgtNo = GrpRegisterGrid.getRowColData( selno-1, 1);

		strSQL="select ActuGetNo from LJAGet where OtherNo='"+rgtNo+"'" +"and OtherNoType='5' and (finstate<>'FC' or finstate is null)";
		arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
		        ActuGetNo = arrResult[0][0];

    		}
    	if(ActuGetNo == null || ActuGetNo == "")
    	{
    		alert("实付号码为空，团体案件尚未给付确认！");
    		return;
    	}
    	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'lp008' and standbyflag2='"+ActuGetNo+"'");
	    var showStr="正在准备打印数据，请稍后...";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.target = "fraSubmit";
		fm.fmtransact.value="PRINT";
		fm.action = "../uw/PDFPrintSave.jsp?Code=lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+ ActuGetNo +"&PrtSeq="+PrtSeq+"&StandbyFlag3=1";
		fm.submit();
}
////PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	fm.BigNewGPrint1.disabled=false;
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
function NewGrpColPrt()
{
	var selno = GrpRegisterGrid.getSelNo();
	var ActuGetNo="";
	var rgtNo;
	if (selno <= 0)
	{
	      alert("请选择要打印给付明细的团体案件");
	      return ;
	}

	RgtNo = GrpRegisterGrid.getRowColData( selno-1, 1);
	strSQL="select rgtstate,togetherflag from llregister where rgtno='"+RgtNo+"'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
	      rgtstate = arrResult[0][0];
	      togetherflag = arrResult[0][1];

    }
  	if(rgtstate == "01" || rgtstate == "02")
  	{
  		alert("此状态下不能打印！");
  		return;
  	}
   var code;
   if(RgtNo.substr(1, 2)=="12"){
   	    code="lp011";
   }else if(RgtNo.substr(1, 2)=="11"){
   	    code="lp003bj";
   }else if(RgtNo.substr(1, 2)=="31"){
   	    code="lp003sh"
   }else{
   	    code="lp003";
   }
   var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '"+code+"' and standbyflag2='"+RgtNo+"'");

		if(code =="lp003bj"||code =="lp011"||code =="lp003sh")
	{	
	 var newWindow = window.open("PayColPrtBJ.jsp?RgtNo=" + RgtNo+ "&fmtransact=PRINT");		
	}
	
//	else if(code =="lp003sh")
//	{			   
//	    var newWindow = window.open("PayColPrtSH.jsp?RgtNo=" + RgtNo+ "&fmtransact=PRINT");							
//	}

	else 
	{	
		fm.action = "../uw/PDFPrintSave.jsp?Code="+code+"&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&OtherNo="+ RgtNo +"&PrtSeq="+PrtSeq;
		fm.submit();
	}
}

function BigRgtGiveEnsure(){

	fm.BigRgtGiveEnsure1.disabled=true;
	
	var strSql = " SELECT count(1) "
  	    +" FROM LLREGISTER R " 
  	    +" WHERE  R.RGTOBJ = '0' AND R.RGTCLASS = '1' AND R.declineflag is null and r.rgtstate = '03' and togetherflag='4' and r.MngCom like '8612%' " 
  	  +" and r.MngCom like '8612%' and exists ( select 1 from llcase a ,llhospcase b where a.caseno=b.caseno and casetype='03' and hospitcode='JH02' and a.rgtno=r.rgtno) "
  	    + getWherePart("r.GRPNAME","srGrpName","like")
  + getWherePart("r.MngCom","ManageCom","like")
  + getWherePart("R.RGTNO","srRgtNo")
  + getWherePart("R.CUSTOMERNO","srCustomerNo")
  + getWherePart("R.RGTANTNAME","srRgtantName","like")
  + getWherePart("R.RGTDATE","RgtDateStart",">=")
  + getWherePart("R.RGTDATE","RgtDateEnd","<=");
  
	arrResultBig = easyExecSql(strSql);
	if(arrResultBig != null)
	{
	        rgtCount = arrResultBig[0][0];

		}
	if (confirm("该操作共给付确认批次"+rgtCount+"个，您确实想保存该记录吗?")){
		
	  	fm.all('operate').value	= "GIVE|ENSURE";
	    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	    fm.action="./LLGrpClaimCloseConfListJHSave.jsp";
	    fm.submit(); //提交
		
	}else{
		alert("您取消了修改操作！");
		fm.BigRgtGiveEnsure1.disabled=false;
	}
  




}

function BigNewGPrint()
{

		
		fm.BigNewGPrint1.disabled=true;
		var strSql = " SELECT count(1) "
	  	    +" FROM LLREGISTER R " 
	  	    +" WHERE  R.RGTOBJ = '0' AND R.RGTCLASS = '1' AND R.declineflag is null and togetherflag='4' and r.rgtstate = '05' and r.MngCom like '8612%' " 
	  	  +" and r.MngCom like '8612%' and exists ( select 1 from llcase a ,llhospcase b where a.caseno=b.caseno and casetype='03' and hospitcode='JH02' and a.rgtno=r.rgtno) "
	  	    + getWherePart("r.GRPNAME","srGrpName","like")
	  + getWherePart("r.MngCom","ManageCom","like")
	  + getWherePart("R.RGTNO","srRgtNo")
	  + getWherePart("R.CUSTOMERNO","srCustomerNo")
	  + getWherePart("R.RGTANTNAME","srRgtantName","like")
	  + getWherePart("R.RGTDATE","RgtDateStart",">=")
	  + getWherePart("R.RGTDATE","RgtDateEnd","<=");
	  
		arrResultBig = easyExecSql(strSql);
		if(arrResultBig != null)
		{
		        rgtCount = arrResultBig[0][0];

			}
		if (confirm("该操作共打印给付凭证"+rgtCount+"个，您确认继续操作吗?")){
			
		
		    var showStr="正在准备打印数据，请稍后...";
	        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.target = "fraSubmit";
			fm.fmtransact.value="PRINT";
			fm.action = "../uw/PDFPrintSaveLLJH.jsp?Code=lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag3=1";
			fm.submit();
			
			
		}else{
			alert("您取消了修改操作！");
			fm.BigNewGPrint1.disabled=false;
		}
	  
}

//#2684关于理赔环节黑名单监测规则修改的需求
function checkBlacklist(checkName,checkId){
	//需要验证的证件号与人名都为空
	if((checkId==""||checkId==null)&&(checkName==""||checkName==null)){
		return true;
	}
	//需要验证的证件号为空
	if(checkId==""||checkId==null){
		var strblack="select 1 from lcblacklist where trim(name)in('"+checkName+"') and (idno='' or idno is null) with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   	   	  		//alert("若“证件号码”在黑名单库中为空");
   	   	  		if(!confirm("客户或者领取人"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      		return false;
   	   	    	}
		}
		return true;
	}
	//需要验证的姓名为空
	if(checkName==""||checkName==null){
		var strblack=" select 1 from lcblacklist where trim(idno)in('"+checkId+"')with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   			//alert("姓名”不匹配为空，“证件号码”匹配");
   			if(!confirm("客户或者领取人为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		return true;
	}
	
	//1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
	var strblack=" select name,idno from lcblacklist where trim(name)in('"+checkName+"') or trim(idno)in('"+checkId+"') with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   		var strblack1=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and trim(idno)in('"+checkId+"') with ur";
   		var arrblack1=easyExecSql(strblack1);
   		if(arrblack1){
   			alert("客户或者领取人"+checkName+"为黑名单客户");
   	   	  	return false;
   		}
   		var strblack2=" select 1 from lcblacklist where trim(idno)in('"+checkId+"') with ur";
   		var arrblack2=easyExecSql(strblack2);
   		if(arrblack2){
   			if(!confirm("客户或者领取人"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		var strblack3=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and (trim(idno)in('') or idno is null) with ur";
   		var arrblack3=easyExecSql(strblack3);
   		if(arrblack3){
   			if(!confirm("客户或者领取人"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
     }
     return true;
}
