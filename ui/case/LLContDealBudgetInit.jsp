<% 
//程序名称：PEdorBudgetInit.jsp
//程序功能：解约试算
//创建日期：2006-04-19
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String currentDate = PubFun.getCurrentDate();
  String contNo = request.getParameter("contNo");
  if(contNo == null || contNo.equals("null"))
  {
    contNo = "";
  }
  
  String edorNo = request.getParameter("edorNo");
  if(edorNo == null || edorNo.equals("null"))
  {
    edorNo = "";
  }
  
  String edorType = request.getParameter("edorType");
  if(edorType == null || edorType.equals("null"))
  {
    edorType = "";
  }
  
  String edorValiDate = request.getParameter("edorValiDate");
  if(edorValiDate == null || edorValiDate.equals("null"))
  {
    edorValiDate = "";
  }
%>

<script language="JavaScript">  
function initForm()
{
  fm.contNoTop.value = "<%= contNo%>";
  fm.edorNo.value = "<%=edorNo%>";
  fm.edorType.value = "<%=edorType%>";
  var edorValiDate = "<%=edorValiDate%>";
  if("<%=edorValiDate%>" != "")
  {
    fm.all("divEdorValiDate").style.display = "none";
    fm.all("divEdorValiDatePassIn").style.display = "";
  }
  else
  {
    fm.all("divEdorValiDate").style.display = "";
    fm.all("divEdorValiDatePassIn").style.display = "none";
  }
  
  var edorValiDate = "<%=edorValiDate%>";
  if(edorValiDate == "")
  {
    edorValiDate = "<%=currentDate%>";
  }
  fm.edorValiDate.value = edorValiDate;
  fm.edorValiDatePassIn.value = edorValiDate;
  
  initLCContGrid();
  initLCPolGrid();
  
  if(fm.edorType.value != "" || fm.contNoTop.value != "")
  {
    queryCont();
  }
}

//初始化保单列表
function initLCContGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=0;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";
    iArray[1][1]="80px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    iArray[1][21]="contNo";
    
    iArray[2]=new Array();
    iArray[2][0]="投保人姓名";
    iArray[2][1]="60px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][21]="appntName";
    
    iArray[3]=new Array();
    iArray[3][0]="客户号";
    iArray[3][1]="60px";
    iArray[3][2]=100;
    iArray[3][3]=0;
    iArray[3][21]="appntNo";
        
    iArray[4]=new Array();
    iArray[4][0]="投保日期";
    iArray[4][1]="60px";
    iArray[4][2]=100;
    iArray[4][3]=3;
    iArray[4][21]="polApplyDate";
    
    iArray[5]=new Array();
    iArray[5][0]="生效日期";
    iArray[5][1]="60px";
    iArray[5][2]=100;
    iArray[5][3]=0;
    iArray[5][21]="cValiDate";
    
    iArray[6]=new Array();
    iArray[6][0]="交至日期";
    iArray[6][1]="60px";
    iArray[6][2]=100;
    iArray[6][3]=3;
    iArray[6][21]="payToDate";
        
    iArray[7]=new Array();
    iArray[7][0]="满期日期";
    iArray[7][1]="60px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    iArray[7][21]="cInValiDate";
    
    iArray[8]=new Array();
    iArray[8][0]="缴费频次";
    iArray[8][1]="50px";
    iArray[8][2]=100;
    iArray[8][3]=3;
    iArray[8][21]="payIntv";
    
    iArray[9]=new Array();
    iArray[9][0]="期交保费";
    iArray[9][1]="50px";
    iArray[9][2]=100;
    iArray[9][3]=3;
    iArray[9][21]="prem";
    
    LCContGrid = new MulLineEnter( "fm" , "LCContGrid" ); 
    //这些属性必须在loadMulLine前
    LCContGrid.mulLineCount = 0;   
    LCContGrid.displayTitle = 1;
    LCContGrid.canSel=1;       
    //PolGrid.canChk=1;
    LCContGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    LCContGrid.hiddenSubtraction=1;
    LCContGrid.loadMulLine(iArray);
    LCContGrid.selBoxEventFuncName ="setContInfoOnClick" ; 
  
  }
  catch(ex)
  {
    alert(ex.message);
  }
}

//初始化险种列表
function initLCPolGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=0;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
    iArray[1]=new Array();
    iArray[1][0]="险种号";
    iArray[1][1]="0px";
    iArray[1][2]=100;
    iArray[1][3]=3;  
    iArray[1][21]="polNo";
    
    iArray[2]=new Array();
    iArray[2][0]="序号";
    iArray[2][1]="30px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][21]="riskSeqNo";
    
    iArray[3]=new Array();
    iArray[3][0]="被保人号";
    iArray[3][1]="70px";
    iArray[3][2]=100;
    iArray[3][3]=0;
    iArray[3][21]="insuredNo";
    
    iArray[4]=new Array();
    iArray[4][0]="被保人";
    iArray[4][1]="50px";
    iArray[4][2]=100;
    iArray[4][3]=0;
    iArray[4][21]="insuredName";
    
    iArray[5]=new Array();
    iArray[5][0]="代码";
    iArray[5][1]="50px";
    iArray[5][2]=100;
    iArray[5][3]=0;
    iArray[5][21]="riskCode";
    
    iArray[6]=new Array();
    iArray[6][0]="险种名称";
    iArray[6][1]="150px";
    iArray[6][2]=100;
    iArray[6][3]=0;
    iArray[6][21]="riskName";
        
    iArray[7]=new Array();
    iArray[7][0]="生效日期";
    iArray[7][1]="80px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    iArray[7][21]="cValiDate";
    
    iArray[8]=new Array();
    iArray[8][0]="保额";
    iArray[8][1]="50px";
    iArray[8][2]=100;
    iArray[8][3]=0;
    iArray[8][21]="amnt";
    
    iArray[9]=new Array();
    iArray[9][0]="档次";
    iArray[9][1]="30px";
    iArray[9][2]=100;
    iArray[9][3]=0; 
    iArray[9][21]="mult"; 
    
    iArray[10]=new Array();
    iArray[10][0]="期交保费";
    iArray[10][1]="60px";
    iArray[10][2]=100;
    iArray[10][3]=0; 
    iArray[10][21]="prem";
    
    iArray[11]=new Array();
    iArray[11][0]="总保费";
    iArray[11][1]="60px";
    iArray[11][2]=100;
    iArray[11][3]=0;  
    iArray[11][21]="sumPrem";
    
    iArray[12]=new Array();
    iArray[12][0]="满期日期";
    iArray[12][1]="75px";
    iArray[12][2]=100;
    iArray[12][3]=0;  
    iArray[12][21]="endDate";
    
    iArray[13]=new Array();
    iArray[13][0]="交费年期";
    iArray[13][1]="60px";
    iArray[13][2]=100;
    iArray[13][3]=0;  
    iArray[13][21]="payEndYear";    
    
    iArray[14]=new Array();
    iArray[14][0]="交费频次";
    iArray[14][1]="60px";
    iArray[14][2]=100;
    iArray[14][3]=0;  
    iArray[14][21]="payIntv";
    
    iArray[15]=new Array();
    iArray[15][0]="理赔金额";
    iArray[15][1]="60px";
    iArray[15][2]=100;
    iArray[15][3]=0;  
    iArray[15][21]="claimPay";
    
    iArray[16]=new Array();
    iArray[16][0]="理赔率";
    iArray[16][1]="50px";
    iArray[16][2]=100;
    iArray[16][3]=3;  
    iArray[16][21]="claimPayRate";
    
    iArray[17]=new Array(); 
    iArray[17][0]="试算退费";
    iArray[17][1]="60px";
    iArray[17][2]=100;
    iArray[17][3]=0;  
    iArray[17][21]="getMoney";
    
    iArray[18]=new Array();
    iArray[18][0]="备注";
    iArray[18][1]="80px";
    iArray[18][2]=100;
    iArray[18][3]=0;  
    iArray[18][21]="remark";
        
    LCPolGrid = new MulLineEnter( "fm" , "LCPolGrid" ); 
    //这些属性必须在loadMulLine前
    LCPolGrid.mulLineCount = 0;   
    LCPolGrid.displayTitle = 1;
    LCPolGrid.canSel=0;       
    LCPolGrid.canChk=1;
    LCPolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    LCPolGrid.hiddenSubtraction=1;
    LCPolGrid.loadMulLine(iArray);
    LCPolGrid.selBoxEventFuncName ="" ; 
  
  }
  catch(ex)
  {
    alert(ex.message);
  }
}
</script>