<%
//程序名称：FrameMainAddFee.jsp(理赔保单加费的窗口)
//程序功能：显示＂理赔保单加费的窗口＂
//创建日期：2005-09-21 09:20
//创建人  ：Xx
//更新记录：
//更新人：   
//更新日期:
//更新原因/内容:
%>
<!--Root="../../" -->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
<title>保单加费</title>
<script language="javascript">
  window.focus();

</script>
</head>
<!--<frameset rows="0,0,0,65,*" frameborder="no" border="1" framespacing="0" cols="*"> -->
<frameset name="fraMain" rows="0,0,0,0,*" frameborder="no" border="1" framespacing="0" cols="*">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="yes" noresize src="about:blank" >
	<frame name="fraTitle"  scrolling="no" noresize src="about:blank" >
	<frameset name="fraSet" cols="0%,*,0%" frameborder="no" border="1" framespacing="0" rows="*">
		<!--菜单区域-->
		<frame name="fraMenu" scrolling="yes" noresize src="about:blank">
		<!--交互区域-->
		<%
			String szSrc = request.getParameter("Interface");
			szSrc += "?CaseNo=" + request.getParameter("CaseNo");
			szSrc += "&ContNo=" + request.getParameter("ContNo");
			szSrc += "&CustomerNo=" + request.getParameter("CustomerNo");
			String aCustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");
			szSrc += "&CustomerName=" +aCustomerName;
			szSrc += "&LoadFlag=" + request.getParameter("LoadFlag");
		%>
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="<%= szSrc %>">
    	<!--下一步页面区域-->
    	<frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff" onblur="focusMe();">
	</body>
</noframes>
</html>
