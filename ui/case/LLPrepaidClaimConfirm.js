//该文件中包含客户端需要处理的函数和事件

//程序名称：LLPrepaidClaimUnderWrite.js
//程序功能：预付赔款审批
//创建日期：2010-11-30
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();


//查询
function easyQuery( )
{
	if(fm.ManageCom.value==""||fm.ManageCom.value==null
	  ||fm.RgtDateS.value==""||fm.RgtDateS.value==null
	  ||fm.RgtDateE.value==""||fm.RgtDateE.value==null)
  {
  	alert("管理机构、申请起期、申请止期不能为空!");
  	return false;
  }
  var preSql="select ManageCom,PrepaidNo,GrpContNo,GrpName,RgtDate,SumMoney,"
		+" codename('llprepaidstate',RgtState),PayMode,codename('paymode',PayMode),bankcode,"
		+" (select bankname from ldbank where bankcode=a.bankcode),"
		+" BankAccNo,AccName,RgtState"
		+" from LLPrepaidClaim a"
		+" where 1=1 and ManageCom like '"+fm.ManageCom.value+"%'"
		+" and RgtState in ('04','05','06') "
		+" and RgtDate>='"+fm.RgtDateS.value+"' and RgtDate<='"+fm.RgtDateE.value+"'"
		
		+ getWherePart("GrpName","GrpName")
		+ getWherePart("GrpContNo","GrpContNo")
		+ getWherePart("PrepaidNo","PrepaidNo")
		+ getWherePart("RgtState","RgtState")
		+ getWherePart("RgtType","RgtType")
		preSql +=" order by PrepaidNo";
		
	turnPage.queryModal(preSql,PreClaimGrid);
}

//通知给付
function giveConfirm()
{
	if( PreClaimGrid.getSelNo()==0 )
	{
		alert("请选择一条预付赔款信息！");
	    return false;
	}
	var selNo = PreClaimGrid.getSelNo();
	var tRgtState= PreClaimGrid.getRowColData(selNo-1,14);
	if(tRgtState!="04" )
	{
		alert("预付案件只有在审定状态才能进行给付确认!");
		return false;
	}
	var tPreNo= PreClaimGrid.getRowColDataByName(selNo-1,"PrepaidNo");
	var tPayMode= PreClaimGrid.getRowColDataByName(selNo-1,"PayMode");
	var tBankCode= PreClaimGrid.getRowColDataByName(selNo-1,"BankCode");
	var tBankAccNo= PreClaimGrid.getRowColDataByName(selNo-1,"BankAccNo");
	var tAccName= PreClaimGrid.getRowColDataByName(selNo-1,"AccName");
	var tRgtType= PreClaimGrid.getRowColDataByName(selNo-1,"RgtType");
	
	fm.tPrepaidNo.value = tPreNo;
	if (!checkDealPopedom()){
		return false;
	}
	if(tPayMode == null || tPayMode == "")
	{
		alert("收付方式不能为空");
		return false;
	}
	if(tRgtType == 1 && (tBankCode == null||tBankCode == ""
	||tBankAccNo==null||tBankAccNo==""||tAccName==null||tAccName==""))
	{
		alert("预付赔款时银行账户信息银行名称、银行账号、账户名称不能为空");
		return false;
	}
	if(tRgtType == 2 && (tPayMode ==3||tPayMode == 4||tPayMode == 11)&&(tBankCode == null||tBankCode == ""))
	{
		alert("收费方式为转账支票、银行转账或银行汇款时，银行名称不能为空");
		return false;
	}
	if(tRgtType == 2&&(tPayMode == 4||tPayMode == 11)&&(tBankAccNo == null||tBankAccNo == ""||tAccName == null||tAccName == ""))
	{
		alert("收费方式为银行转账或银行汇款时，银行账户信息银行账号、账户名称不能为空");
		return false;
	}
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all('fmtransact').value="CONFIRM";
	fm.action = "./LLPrepaidClaimConfirmSave.jsp";
	fm.target="fraSubmit";
	fm.submit();
}
//校验给付确认权限
function checkDealPopedom()
{
	var RegisterSQL = "select Register from LLPrepaidClaim where PrepaidNo='"+fm.tPrepaidNo.value+"'";
	var arrResult = easyExecSql(RegisterSQL);
	if (!arrResult) {
		alert("预付案件申请人查询失败");
		return false;
	}
	var handler = arrResult[0][0];	
	var strSQL = "select 1 From llclaimuser a where a.usercode='"+handler+"' and stateflag='1' "
		+"union select 1 From llsocialclaimuser a where a.usercode='"+handler+"' and stateflag='1' with ur";
    var mrr = easyExecSql(strSQL);

 	if (mrr) {
  		if (fm.Operator.value==handler) {
  			return true;
   	 	}
  	}
	var upUser = handler;	
	while (true) {
		var rightSQL = "select a.upusercode,a.stateFlag"
                 + " From llclaimuser a where a.usercode='"+handler+"' "
                 + " union select a.upusercode,a.stateflag from llsocialclaimuser a where a.usercode='"+handler+"' with ur";
    	var rightResult = easyExecSql(rightSQL);

    	if (!rightResult) {
    		alert("没有权限进行操作");
    		return false;
    	}
    
    	if ("1"==rightResult[0][1]) {
    		break;
    	}
            
    	upUser = rightResult[0][0];
    	handler = upUser;
   }
  
   if (fm.Operator.value==upUser) {
  		return true;
   }
   else{
  		alert("您没有预付案件给付通知权限，请用"+upUser+"进行给付确认");
  		return false;
   }
}

//凭证打印
function print()
{
	if(PreClaimGrid.getSelNo() ==0)
	{
		alert("请选择一条预付赔款信息！");
	    return false;
	}
	var selNo = PreClaimGrid.getSelNo();
	var tRgtState= PreClaimGrid.getRowColData(selNo-1,14);
	if(tRgtState!="05" )
	{
		alert("预付案件只有在通知状态才能打印凭证");
		return false;
	}
	var tPrepaidNo = PreClaimGrid.getRowColData(selNo-1, 2);
	var SQL="select  1 from   LLPrepaidClaim  where  PrepaidNo= '"+tPrepaidNo+"' and  RgtType='1' ";
	var Result = easyExecSql(SQL);
	if (Result != null)
	{
		var strSQL="select ActuGetNo from LJAGet where OtherNo='"+tPrepaidNo+"'" +"and OtherNoType='Y'";
		var arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
			ActuGetNo = arrResult[0][0];
		}else{
       		 	alert("实付号码为空，申请类预付赔款才能打印凭证！");
        		return;
    	}
    	
    	var showStr="正在准备打印数据，请稍后...";
    	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   	 	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.target = "fraSubmit";
		fm.action = "./LLprepaidClaimPrintSave.jsp?Code=lp006&OtherNo="+tPrepaidNo+"&StandbyFlag2="+ActuGetNo+"&StandbyFlag3=0&OtherNoType='Y'";
		fm.submit();	
	}else {
			var strSQL1="select GetNoticeNo from ljspay where OtherNo='"+tPrepaidNo+"'" +"and OtherNoType='Y'";
			var rResult = easyExecSql(strSQL1);
			if(rResult != null)
			{
				GetNoticeNo = rResult[0][0];
    		}
    	var showStr="正在准备打印数据，请稍后...";
    	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   	 	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.target = "fraSubmit";
		fm.action = "./LLprepaidClaimPrintSave.jsp?Code=lp009&OtherNo="+tPrepaidNo+"&StandbyFlag2="+GetNoticeNo+"&StandbyFlag3=0&OtherNoType='Y'";
		fm.submit();	
  					
		}
   	
}


//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    fm.RgtState.value = "05";
  	fm.RgtStateName.value = "通知状态";
	easyQuery();
    //执行下一步操作
  }
}
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

