<%
//程序名称：ClaimUnderwriteSave.jsp
//程序功能：审批审定前台递交页面
//创建日期：2002-07-21 20:09:16
//创建人  ：Xx
//更新记录：  Xx    更新日期 2006-11-1    更新原因:优化
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>

<%
//接收信息，并作校验处理。
//输入参数

CErrors tError = null;
String FlagStr = "";
String Content = "";
String strRgtNo = request.getParameter("RgtNo");	//立案号
String strCaseNo = request.getParameter("CaseNo");	//分案号
String[] strClmNos = request.getParameterValues("ClaimPolGrid16");
String strClmNo = strClmNos[0]; //赔案号

String strDecisionSP = "1"; //审批结论暂时去掉，没有意义
String strDecisionSD = request.getParameter("DecisionSD");
String strRemarkSP = request.getParameter("RemarkSP");
String strRemarkSD = request.getParameter("RemarkSD");
String cOperate = request.getParameter("cOperate");
String cOperateCN = "";

String ContDealFlag = "";
if (request.getParameterValues("ContDealFlag") != null){
 	ContDealFlag = "0";
}
System.out.println("ContDealFlag========="+ContDealFlag);
if (strRgtNo == null || strRgtNo.equals("")){
	Content = "立案号不能为空!";
	FlagStr = "Fail";
}
if (strCaseNo == null || strCaseNo.equals("")){
	Content = "分案号不能为空!";
	FlagStr = "Fail";
}
if (strClmNo == null || strClmNo.equals("")) {
	Content = "赔案号不能为空!";
	FlagStr = "Fail";
}

if (cOperate.equals("APPROVE|SP")){
	cOperateCN = "审批";
	if (strRemarkSP == null || strRemarkSP.equals("")) {
		Content = "审批意见不能为空!";
		FlagStr = "Fail";
	}
}
if (cOperate.equals("APPROVE|SD")){
	cOperateCN = "审定";
	if (strDecisionSD == null || strDecisionSD.equals("")){
		Content = "审定结论不能为空!";
		FlagStr = "Fail";
	}
//	if (strRemarkSD == null || strRemarkSD.equals("")){
//		Content = "审定意见不能为空!";
//		FlagStr = "Fail";
//	}
}

if (!FlagStr.equals("Fail")){
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
	LLCaseSchema tLLCaseSchema = new LLCaseSchema();
	tLLCaseSchema.setContDealFlag(ContDealFlag);

	LLClaimUWMainSchema tLLClaimUWMainSchema = new LLClaimUWMainSchema();

	tLLClaimUWMainSchema.setRgtNo(strRgtNo);
	tLLClaimUWMainSchema.setCaseNo(strCaseNo);
	tLLClaimUWMainSchema.setClmNo(strClmNo);

	tLLClaimUWMainSchema.setcheckDecision1(strDecisionSP);
	tLLClaimUWMainSchema.setRemark1(strRemarkSP);
	tLLClaimUWMainSchema.setcheckDecision2(strDecisionSD);
	tLLClaimUWMainSchema.setRemark2(strRemarkSD);

	ClaimUnderwriteUI tClaimUnderwriteUI = new ClaimUnderwriteUI();
	VData tVData = new VData();
	try{
		tVData.add(tGlobalInput);
		tVData.add(tLLClaimUWMainSchema);
		tVData.add(tLLCaseSchema);
		tClaimUnderwriteUI.submitData(tVData, cOperate);
	}
	catch(Exception ex){
		Content = " 执行失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (FlagStr.equals("")){
		tError = tClaimUnderwriteUI.mErrors;
		if (!tError.needDealError()){
			VData tResultData = tClaimUnderwriteUI.getResult();
			String strResult = (String) tResultData.getObjectByObjectName("String", 0);
			Content = strResult;
			FlagStr = "Succ";
		}
		else{
			Content = " 执行失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
}

%>

<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
