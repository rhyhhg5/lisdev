//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();


// 数据返回父窗口
function getQueryDetail()
{  
	var arrReturn = new Array();
	var tSel = ClaimPayGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = ClaimPayGrid.getRowColData(tSel - 1,2);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		
		if (cPolNo == "")
		    return;
		    
		var GrpPolNo = ClaimPayGrid.getRowColData(tSel-1,1);
                var prtNo = ClaimPayGrid.getRowColData(tSel-1,3);
        //alert("dfdf");
        if( tIsCancelPolFlag == "0"){
	    	if (GrpPolNo =="00000000000000000000") {
	    	 	window.open("./AllProQueryMain.jsp?LoadFlag=6&prtNo="+prtNo,"window1");	
		    } else {
			window.open("./AllProQueryMain.jsp?LoadFlag=4");	
		    }
		} else {
		if( tIsCancelPolFlag == "1"){//销户保单查询
			if (GrpPolNo =="00000000000000000000")   {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} else {
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	    } else {
	    	alert("保单类型传输错误!");
	    	return ;
	    }
	 }
 }
}


// 查询按回车
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClaimPayQuery();
//		UpdateGrid();
	}
}

function ClaimPayQuery()
{
	var comCode = fm.ComCode.value;
  if(fm.RgtDateS.value == "" || fm.RgtDateE.value == "") 
	{ alert("请输入受理起止日期"); return false;}  
	// 初始化表格
	//initClaimPayGrid();
	
	// 书写SQL语句
	var strSQL = "";

//  var dpart=getWherePart("a.makedate","RgtDateS",">=") + getWherePart("a.makedate","RgtDateE","<=");
	CalInfo();
	strSQL =  " select a.actugetno,a.otherno,b.Customername,b.customerno,a.contno,c.riskname,a.pay,"
					+ " (select h.username from lduser h where h.usercode = b.handler),"
					+ " a.makedate from Ljagetclaim a,llcase b,lmrisk c where 1=1 "
//			    + " and a.otherno like '" + comcode + "' "
          + getWherePart("a.ContNo", "ContNo")
          + getWherePart("a.otherno", "CaseNo")
          + getWherePart("b.IdNo", "IDNo")
		      + " and b.CustomerName like '%%"+fm.CustomerName.value+"%%'"
		      + getWherePart("b.customerno", "CustomerNo")
		      + " and b.endcasedate is not null "
		      + " and b.rgtdate <= '"+fm.all('RgtDateE').value+"'"
  				+ " and b.rgtdate >= '"+fm.all('RgtDateS').value+"'"	 
  				+" and b.mngcom like '"+comCode+"%%'" 
		      + " and b.caseno=a.otherno and c.riskcode=a.riskcode order by a.makedate DESC, b.customerno"
		      ;
//		      + dpart;
//      + " and b.otherno=a.caseno";
//    	var arr=easyExecSql(strSQL);
//    	var result=arr;
    	
    	turnPage.pageLineNum = 10;              
  		turnPage.queryModal(strSQL, ClaimPayGrid);

  //showCodeName();
}

//计算历史统计信息
function CalInfo()
{
		var strSQL = " select count(a.caseno), sum(a.realpay)"
								+" from llclaim a, (select distinct otherno from ljagetclaim) b"
								+" where a.caseno = b.otherno"
								+" and exists (select 1 from llcase where caseno=a.caseno"
								+" and rgtdate <= '"+fm.all('RgtDateE').value+"'"
  				       			+" and rgtdate >= '"+fm.all('RgtDateS').value+"'"
								+" and mngcom like '"+comCode+"%%') "
								 ;
		var arr = easyExecSql(strSQL);
		fm.TotalMoney.value = arr[0][1]=="null"?0:arr[0][1];
		fm.TotalNum.value = arr[0][0]=="null"?0:arr[0][0];
}

//Mulline单选框事件
function ShowInfo()
{
		var selno = ClaimPayGrid.getSelNo();
		var ContNo = ClaimPayGrid.getRowColData(selno-1,5);
		var CustomerNo = ClaimPayGrid.getRowColData(selno-1,4);
		var CaseNo = ClaimPayGrid.getRowColData(selno-1,2);
		var ClaimPay = ClaimPayGrid.getRowColData(selno-1,7);
		var re = easyExecSql(" select Amnt from LCCont where contno = '"+ContNo+"'");
		fm.ContNo_D.value = ContNo;
		fm.Amnt.value = (re=="0"?"":re);
		if(fm.Amnt.value == "" || fm.Amnt.value == null)
		{ fm.LeftAmnt.value =""; }
		else
		{ fm.LeftAmnt.value = fm.Amnt.value - ClaimPay; }
		var AccAmnt = easyExecSql("select sum(b.fee) from llcasereceipt b where b.caseno = '"+CaseNo+"' and b.feeitemcode not like '3%%'")
		fm.AccAmnt.value = AccAmnt;
		fm.GiveType.value = easyExecSql(" select GiveTypedesc from llclaim where CaseNo = '"+CaseNo+"'");
		
//		var sql = " select count(*) from llcase where customerno = '"+CustomerNo+"'";
//		fm.ClaimNum.value = easyExecSql(sql);
//		sql = " select sum(b.realpay) from llcase a, llclaim b where  a.customerno = '"+CustomerNo+"' and a.caseno = b.caseno "
//		fm.ClaimAmt.value = easyExecSql(sql);
		
		sql = "select count(b.caseno) from ljagetclaim a, llcase b where  a.otherno = b.caseno and b.customerno = '"+CustomerNo+"'";
		fm.PersonTNum.value = easyExecSql(sql);
		sql = "select sum(a.pay) from ljagetclaim a, llcase b where  a.otherno = b.caseno and b.customerno = '"+CustomerNo+"'";
		fm.PersonTAmnt.value = easyExecSql(sql);
		
		
}


// 数据返回父窗口
function returnParent()
{
	//alert(tDisplay);
	if (tDisplay=="1")
	{
	    var arrReturn = new Array();
	    var tSel = ClaimPayGrid.getSelNo();
	    //alert(tSel);
	    if( tSel == 0 || tSel == null )
	    	alert( "请先选择一条记录，再点击返回按钮。" );
	    else
	    {
	    	try
	    	{
	    		arrReturn = getQueryResult();
	    		
	    		top.opener.afterQuery( arrReturn );
	    		
	    		top.close();
	    	}
	    	catch(ex)
	    	{
	    		alert( "请先选择一条非空记录，再点击返回按钮。");
	    		
	    	}
	    	
	    }
	 }
	 else
	 {
	    top.close(); 
	 }
}

function getQueryResult()
{
	var arrSelected = null;
	var tRow = ClaimPayGrid.getSelNo();
	
	if( tRow == 0 || tRow == null || arrDataSet == null )
		      return arrSelected;
	
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = ClaimPayGrid.getRowData(tRow-1);
	
	return arrSelected;
}

function getstr()
{
	//alert("aaa");
	str="1 and code like #"+fm.OrganCode.value+"%#";
}
function getRights(){
	var strSQL="select claimpopedom from llclaimuser where usercode='"+fm.Operator.value+"'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		var Rights = arrResult[0][0];
		//alert(Rights);

	}
	Str="1 and claimpopedom < #"+Rights+"#";
}


//原始信息查询页面
function ShowInfoPage()
{
	var selno = ClaimPayGrid.getSelNo();//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = ClaimPayGrid.getRowColData(selno-1,2);
	OpenWindowNew("./ClaimUnderwriteInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");
}    

//保单查询
function ContInfoPage()
{
		var selno = ClaimPayGrid.getSelNo();//alert(CheckGrid.getSelNo());
		if(selno == "" || selno == null)
		{alert("请先选择一条信息");return;}
		var CaseNo = ClaimPayGrid.getRowColData(selno-1,2);
		var ContNo = easyExecSql("select distinct grpcontno from llclaimpolicy where caseno = '"+CaseNo+"'");
		window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
}