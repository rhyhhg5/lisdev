<%
//Name:LLCustomerMailInit.jsp
//function：客户信箱页面函数
//author:Xx
//Date:2005-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    
    initInpBox();
    initCheckGrid();
    easyQuery();

  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=3;

        
    iArray[1] = new Array("用户","130px","0","0");
    iArray[2] = new Array("后台报表名称","130px","0","0");     
    iArray[3] = new Array("报表生成时间","60px","0","0");    
    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =10;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canSel =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CheckGrid. selBoxEventFuncName = "onSelSelected";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

 </script>