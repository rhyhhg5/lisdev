<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：RgtSurveySave.jsp
//程序功能：
//创建日期：2005-02-23 11:53:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LLSurveySchema tLLSurveySchema   = new LLSurveySchema();
  LLInqCertificateSet tLLInqCertificateSet = new LLInqCertificateSet();
  RgtSurveyUI tRgtSurveyUI   = new RgtSurveyUI();
  LLCaseDB tLLCaseDB = new LLCaseDB();
  String tRgtState = "";
  //String tSurveyState = "";
  //输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  	
  tLLSurveySchema.setOtherNo(request.getParameter("CaseNo"));
  tLLSurveySchema.setSubRptNo("0000");
  tLLSurveySchema.setStartPhase("0");
    
  tLLCaseDB.setCaseNo(request.getParameter("CaseNo"));
  tLLCaseDB.getInfo();
  tRgtState = tLLCaseDB.getRgtState();

//案件提调时是什么状态，在调查表中就存为该状态，且在查讫时，统一将案件处理人修改为提调人
//add by Houyd #1738 社保调查
//  if ("01".equals(tRgtState)||"02".equals(tRgtState)||"07".equals(tRgtState)||"08".equals(tRgtState)){
//      tSurveyState = "0";
//    }else{
//      tSurveyState = "1";
//  }
  tLLSurveySchema.setRgtState(tRgtState);
  tLLSurveySchema.setCustomerNo(request.getParameter("InsuredNo"));
  tLLSurveySchema.setCustomerName(request.getParameter("CustomerName"));
  tLLSurveySchema.setSurveyType(request.getParameter("SurveyType"));
  tLLSurveySchema.setContent(request.getParameter("Content"));
  tLLSurveySchema.setSurveyFlag("0");/** 调查报告状态 */
  tLLSurveySchema.setSurveySite(request.getParameter("CasePay"));
  tLLSurveySchema.setSurveyClass(request.getParameter("SurvType"));

  try{
  	VData tVData = new VData();
	tVData.add(tLLSurveySchema);
	tVData.add(tLLInqCertificateSet);
  	tVData.add(tG);
    tRgtSurveyUI.submitData(tVData,transact);
  }
  catch(Exception ex){
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (FlagStr==""){
    tError = tRgtSurveyUI.mErrors;
    if (!tError.needDealError()){
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else{
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
