<html>
  <%
  //Name：LLMainAskInput.jsp
  //Function：登记界面的初始化
  //Date：2004-12-23 16:49:22
  //Author：wujs
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@page import = "com.sinosoft.lis.schema.*"%>
  <%@page import = "com.sinosoft.lis.vschema.*"%>
  <%@page import = "com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%>
  <%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String Operator=tG.Operator;
  String Comcode=tG.ManageCom;

  String CurrentDate= PubFun.getCurrentDate();
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);
  String AheadDays="-60";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
  %>

  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLQDBudgetList.js"></SCRIPT>
    <%@include file="LLQDBudgetListInit.jsp"%>
    <script language="javascript">
      function initDate(){
        var usercode="<%=Operator%>";
        fm.ManageCom.value="<%=Comcode%>";
      }
    </script>
  </head>

  <body  onload="initDate();initForm();" >
    <form action="./LLGrpClaimCloseConfListSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegisterSearch);">
          </TD>
          <TD class= titleImg>
            体给付确认工作列表
          </TD>
        </TR>
      </table>

      <div id= "divGrpRegisterSearch" style= "display: ''" >
        <table  class= common>
          <TR  class= common8>
            <TD  class= title8>团体客户号</TD>
            <TD  class= input8><Input class=common name="srCustomerNo" onkeydown="QueryOnKeyDown();"></TD>
            <TD  class= title8>单位名称</TD>
            <TD  class= input8><Input class=common name="srGrpName" onkeydown="QueryOnKeyDown();"></TD>
            <TD  class= title8>保单号码</TD>
            <TD  class= input8><Input class=common name="GrpContNo" onkeydown="QueryOnKeyDown();"></TD>
          </TR>
        </table>
      </div>

      <Div  id= "divCustomerInfo" align=center style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD text-align: left colSpan=1>
              <span id="spanGrpRegisterGrid" >
              </span>
            </TD>
          </TR>
        </table>
        <INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
        <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
      </Div>

      <hr>
    <div  id= "divClaimTotal" style= "display: ''">
      <table  class= common>
        <TR  class= common8>
          <TD  class= title8>拨付金额</TD>
          <TD  class= input8><Input class= readonly name="StandPay" value=300000 readonly></TD>
          <TD  class= input8><Input class= readonly readonly></TD>
          <TD  class= input8><Input class= readonly readonly></TD>
        </TR> 
      </table>
    </div>

      <input style="display:''"  class=cssButton type=button name='QDRgtGiveEnsure1' value="统一拨付" onclick="QDRgtGiveEnsure()">
      <input name="AskIn" style="display:'none'"  class=cssButton type=button value="团体给付通知打印" onclick="UWClaim()">
      <input name="AskIn" style="display:''"  class=cssButton type=button value="团体汇总明细打印" onclick="GrpColPrt()">
      <input name="AskIn" style="display:''"  class=cssButton type=button value="团体给付凭证打印" onclick="GPrint()">


      <!--隐藏域-->
      <Input type="hidden" class= common name="fmtransact" >
      <Input type="hidden" class= common name="operate" >
      <Input type="hidden" class= common name="ManageCom" >
      <Input type="hidden" class= common name="RgtNo" >
      <input type=hidden id="ApplyerType" name="ApplyerType" value="5">
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>
