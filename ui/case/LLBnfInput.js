//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();

//#2269 （需求编号：2014-216）理赔处理中通知给付界面增加查看扫描件功能需求
//#2018 审批审定页面增加扫描件阅览功能
function ScanQuery() {
 
   if(fm.all('CaseNo').value=='' || fm.all('CaseNo').value==null) {
      alert("没有待展示的案件！");
	  return false;
   }
   var pathStr="./ClaimUnderwriteEasyScan.jsp?RgtNo="+fm.all('CaseNo').value+"&SubType=LP1001&BussType=LP&BussNoType=21";
   var newWindow=OpenWindowNew(pathStr,"查看扫描件","left");	
   
   
}
//提交，保存按钮对应操作
function submitForm()
{
  var chk = 1;
  var i = 0;
  BnfGrid.delBlankLine();
  var arr = new Array();
  var arrlot = new Array(0,0,0,0,0,0,0);
  var row = BnfGrid.mulLineCount-1;
  var tLoadFlag = fm.LoadFlag.value;
  var idate=fm.IDEndDate.value;

  
  
  	//效验同一级别的份额总额是否等于1
  for(var m=0;m<=row;m++ )
  {
   		arr[m]=i;
   		for(var n=1;n<=6;n++)
   		{	
   		    if(eval(n)==eval(BnfGrid.getRowColData(m,22))) {  		
   				arrlot[n] = eval(arrlot[n])+eval(BnfGrid.getRowColData(m,7));   
   			}     
   	    }

   	    i=i+1;

   	    
   	    var tName = BnfGrid.getRowColData(m,3);
   	    if(tName == null || tName == '' || tName == 'null'){
   	    	alert("第"+(m+1)+"行，‘姓名’字段不可为空！");
   	    	return false;
   	    }
   	    var tSex = BnfGrid.getRowColData(m,4);
   	    if(tSex == null || tSex == '' || tSex == 'null' || (tSex !='男' && tSex !='女')){
   	    	alert("第"+(m+1)+"行，‘性别’字段不可为空或性别错误！");
   	    	return false;
   	    }
   	    var tIDType = BnfGrid.getRowColData(m,9);
   	    if(tIDType == null || tIDType == '' || tIDType == 'null'){
   	    	alert("第"+(m+1)+"行，‘证件类型’字段不可为空！");
   	    	return false;
   	    }
   	    var tIDNo = BnfGrid.getRowColData(m,10);
   	    if(tIDNo == null || tIDNo == '' || tIDNo == 'null'){
   	    	alert("第"+(m+1)+"行，‘证件号码’字段不可为空！");
   	    	return false;
   	    }
   	    
   		
   		if(BnfGrid.getRowColData(m,19)=="非投保指定"){
   			if(tIDType=="身份证" || tIDType=="户口本"){
   	   			var strChkIdNo=checkIdNo(tIDType=="身份证"?"0":"5",tIDNo,"",tSex=="男"?"0":"1");
   	 	  		if(strChkIdNo!=""){
   	 	  			alert("第"+(m+1)+"行，身故受益人身份证号不符合规则，请核实");
   	 	  			return false;
   	 	  		}
   	   		}
   			
   		}else {
   			
   			if(tIDType=="身份证" || tIDType=="户口本"){
   				
   	   			var strChkIdNo=checkIdNo(tIDType=="身份证"?"0":"5",tIDNo,"",tSex=="男"?"0":"1");
   	 	  		if(strChkIdNo!=""){
   	 	  			if(!confirm("第"+(m+1)+"行，身故受益人身份证号不符合规则，请核实")){
   	 	  				return false;
   	 	  			}
   	 	  		}
   	   		}
   			
   		}
	  		
	  		
  		
   	    if(tLoadFlag==1){
	   	    var tNativePlace = BnfGrid.getRowColData(m,24);
	   	    if(tNativePlace == null || tNativePlace == '' || tNativePlace == 'null'){
	   	    	alert("第"+(m+1)+"行，‘国籍’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tOccupation = BnfGrid.getRowColData(m,26);
	   	    if(tOccupation == null || tOccupation == '' || tOccupation == 'null'){
	   	    	alert("第"+(m+1)+"行，‘职业’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tPostalAddress = BnfGrid.getRowColData(m,28);
	   	    if(tPostalAddress == null || tPostalAddress == '' || tPostalAddress == 'null'){
	   	    	alert("第"+(m+1)+"行，‘联系地址’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tIDStartDate = BnfGrid.getRowColData(m,31);
	   	    if(tIDStartDate == null || tIDStartDate == '' || tIDStartDate == 'null'){
	   	    	alert("第"+(m+1)+"行，‘证件生效日期’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tIDEndDate = BnfGrid.getRowColData(m,32);
	   	    if(tIDEndDate == null || tIDEndDate == '' || tIDEndDate == 'null'){
	   	    	alert("第"+(m+1)+"行，‘证件失效日期’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tPhone = BnfGrid.getRowColData(m,29);
	   	    var tMobilePhone = BnfGrid.getRowColData(m,30);
	   	    if((tPhone == null || tPhone == '' || tPhone == 'null') && (tMobilePhone == null || tMobilePhone == '' || tMobilePhone == 'null')){
	   	    	alert("第"+(m+1)+"行，‘固定电话’和‘手机号码’二者必录一项！");
	   	    	return false;
	   	    }
	   	    var date1=BnfGrid.getRowColData(m,32);//获取页面的输入的日期
//	   		alert(date1);
	   		if(isDate(date1)){
	   		var date=new Date();//当前日期 因为这么new出来的是国际日期，所以在下行把年月日拼接起来
	   		var date2=date.getYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();//拼接成字符串
	   		var dateValue=compareDate(date1,date2);   //这个是
	   		if(dateValue==2){
	   			alert("身故受益人"+tName+"，证件已过期，请联系客户变更证件有效期。");
	   			return false;
	   			}
	   		}else{
	   			alert("您输入的日期非法！请输入yyyy-mm-dd格式的日期");
	   			return false;
	   		}
     
   	    }
   	    if(tLoadFlag==2){
   	    	var tNativePlace = BnfGrid.getRowColData(m,28);
	   	    if(tNativePlace == null || tNativePlace == '' || tNativePlace == 'null'){
	   	    	alert("第"+(m+1)+"行，‘国籍’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tOccupation = BnfGrid.getRowColData(m,30);
	   	    if(tOccupation == null || tOccupation == '' || tOccupation == 'null'){
	   	    	alert("第"+(m+1)+"行，‘职业’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tPostalAddress = BnfGrid.getRowColData(m,32);
	   	    if(tPostalAddress == null || tPostalAddress == '' || tPostalAddress == 'null'){
	   	    	alert("第"+(m+1)+"行，‘联系地址’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tIDStartDate = BnfGrid.getRowColData(m,35);
	   	    if(tIDStartDate == null || tIDStartDate == '' || tIDStartDate == 'null'){
	   	    	alert("第"+(m+1)+"行，‘证件生效日期’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tIDEndDate = BnfGrid.getRowColData(m,36);
	   	    if(tIDEndDate == null || tIDEndDate == '' || tIDEndDate == 'null'){
	   	    	alert("第"+(m+1)+"行，‘证件失效日期’字段不可为空！");
	   	    	return false;
	   	    }
	   	    var tPhone = BnfGrid.getRowColData(m,33);
	   	    var tMobilePhone = BnfGrid.getRowColData(m,34);
	   	    if((tPhone == null || tPhone == '' || tPhone == 'null') && (tMobilePhone == null || tMobilePhone == '' || tMobilePhone == 'null')){
	   	    	alert("第"+(m+1)+"行，‘固定电话’和‘手机号码’二者必录一项！");
	   	    	return false;
	   	    }
	   	    var date1=BnfGrid.getRowColData(m,36);//获取页面的输入的日期
//	   		alert(date1);
	   		if(isDate(date1)){
	   		var date=new Date();//当前日期 因为这么new出来的是国际日期，所以在下行把年月日拼接起来
	   		var date2=date.getYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();//拼接成字符串
	   		var dateValue=compareDate(date1,date2);   //这个是
	   		if(dateValue==2){
	   			alert("身故受益人"+tName+"，证件已过期，请联系客户变更证件有效期。");
	   			return false;
	   			}
	   		}else{
	   			alert("您输入的日期非法！请输入yyyy-mm-dd格式的日期");
	   			return false;
	   		}
   	    }
//  	     // 增加仅对身份证和户口本校验**start**
//   	    if(tIDType!=null){
//   	    	var tIdTypeSQL="select code from ldcode where codetype='idtype' and codename='"+ tIDType +"' with ur";
//   	    	var tIdType =easyExecSql(tIdTypeSQL);
//   	    	if(tIdType!=null){
//		   	    if(tIdType[0][0]=='0'|| tIdType[0][0]=='5'){
//		   	    	if(tSex !="" && tSex !=null){
//		   				var tSexSQL="select code from ldcode where codetype='sex' and codename='"+ tSex +"' with ur";
//		   				var aSex =easyExecSql(tSexSQL);
//		   			}
////		   	    	alert(tIdType[0][0]+"  "+tIDNo+"  "+aSex[0][0]);
//		   	    	var strChkIdNo=checkIdNo(tIdType[0][0],tIDNo,"",aSex[0][0]);
//		   			if(strChkIdNo != ""){
//		   				alert(strChkIdNo);
//		   				return false;
//		   			}	
//		   	    }
//   	    	}
//   	    }
//   	    // 增加仅对身份证和户口本校验**end** 
  		
  }
  if(eval(arrlot[1])!=1&&eval(arrlot[2])!=1&&eval(arrlot[3])!=1&&eval(arrlot[4])!=1&&eval(arrlot[5])!=1&&eval(arrlot[6])!=1)
  {
  	alert("同一级别的受益份额加起来应该等于1");
    return false;
  }
  if(arr.length==0)
  {
  	alert("请选择数据！");
  	return false;
  }

  fm.fmtransact.value = "INSERT||MAIN";
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action ="./LLBnfSave.jsp?InsuredNo="+fm.InsuredNo.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+fm.LoadFlag.value;
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.GiveConfirm.disabled=false;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    top.opener.initTitle();
    //top.opener.afterAffix( ShortCountFlag,SupplyDateResult );
  }
  if(fm.fmtransact.value == "INSERT")
  {
    sendMessage();
  }
  initForm();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

function displayEasyResult( arrQueryResult )
{
	var i, j, m, n;
	var arrSelected = new Array();
	var arrResult = new Array();

	if( arrQueryResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPersonGrid();
		PersonGrid.recordNo = (currPageIndex - 1) * MAXSCREENLINES;
		PersonGrid.loadMulLine(PersonGrid.arraySave);

		arrGrid = arrQueryResult;
		// 转换选出的数组
		arrSelected = getSelArray();
		arrResult = chooseArray( arrQueryResult, arrSelected );
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				PersonGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
		
		//PersonGrid.delBlankLine();
	} // end of if
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = PersonGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterLLRegister( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PersonGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	


	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = PersonGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
function afterQuery()
{	

//	var strSQL="select a.ContNo,c.riskname,a.Name,a.Sex,a.Birthday,a.BnfGrade,a.BnfLot,"
//					+"a.CustomerNo,a.IDType,a.IDNo,a.polno,a.BnfType,"
//					+"a.RelationToInsured from LCBnf a ,LCPol b,lmrisk c "
//					+" where a.InsuredNo='"+fm.InsuredNo.value+"' and a.polno = b.polno and b.riskcode=c.riskcode";				
//					
//		var strSQL1="select a.ContNo,c.riskname,a.Name,a.Sex,a.Birthday,a.BnfGrade,a.BnfLot,"
//				+"a.CustomerNo,a.IDType,a.IDNo,a.polno,a.BnfType,"
//				+"a.RelationToInsured,a.accname,a.GetMoney,a.BankCode,"
//				+"a.BankAccNo,a.CaseGetMode from LLBnf a,LcPol b ,lmrisk c "
//				+" where CaseNo='"+fm.CaseNo.value+"' and a.polno =b.polno and b.riskcode = c.riskcode";
				
				//查询契约处受益人
				var tLoadFlag = fm.LoadFlag.value;
				var tPartSQL = "";
				if(tLoadFlag=='2'){
					tPartSQL = " '','','','', ";
				}
				
				var strSQL="select a.ContNo,c.riskname,a.Name,"
						  +" CASE WHEN a.Sex = '0' THEN '男' when a.Sex='1' then '女' else '' END,"
						  +" a.Birthday,"
						  +" (select codename from ldcode where codetype = 'occupationtype' and code = a.BnfGrade),"
						  +" a.BnfLot,a.CustomerNo,"
						  +" (select codename from ldcode where codetype = 'idtype' and code= a.IDType),"
						  +" a.IDNo,a.polno,a.RelationToInsured,"
						  +" (select codename from ldcode where codetype = 'relation' and code=a.RelationToInsured ),"
						  //#2329 （需求编号：2014-229）客户基本信息要素录入需求
						  +" '','','','','','',b.riskcode,a.sex,a.bnfgrade,a.idtype, "
						  + tPartSQL
						  +" codename('nativeplace',a.nativeplace),a.nativeplace,codename('occupation',a.OccupationCode),a.OccupationCode,"
						  +" a.PostalAddress,a.Phone,'',a.IDStartDate,a.IDEndDate "
						  +" from LCBnf a ,LCPol b,lmrisk c "                                         	
						  +" where a.InsuredNo='"+fm.InsuredNo.value+"' and a.polno = b.polno and b.riskcode=c.riskcode and relationtoinsured <> '00'";	
				//查询理赔处受益人	，优先		                                                                                               	
				var strSQL1="select a.ContNo,c.riskname,a.Name,"
				    +" CASE WHEN a.Sex = '0' THEN '男' when a.Sex='1' then '女' else '' END,"
				    +" a.Birthday,"
				    +" (select codename from ldcode where codetype = 'occupationtype' and code = a.BnfGrade),"
				    +" a.BnfLot, a.CustomerNo,"
					+" (select codename from ldcode where codetype = 'idtype' and code= a.IDType),"
					+" a.IDNo,a.polno,a.RelationToInsured,"
					+" (select codename from ldcode where codetype = 'relation' and code=a.RelationToInsured ),"					
					+" a.accname,a.GetMoney,a.BankCode,"                                          	
					+" a.BankAccNo,a.CaseGetMode,'',b.riskcode,a.sex,a.bnfgrade,a.idtype, "
					+ tPartSQL
					//#2329 （需求编号：2014-229）客户基本信息要素录入需求
					+" codename('nativeplace',a.nativeplace),a.nativeplace,codename('occupation',a.OccupationCode),a.OccupationCode,"
					+" a.PostalAddress,a.Phone,a.mobilephone,a.IDStartDate,a.IDEndDate "
					+" from LLBnf a,LcPol b ,lmrisk c "                                     	
					+"  where CaseNo='"+fm.CaseNo.value+"' and a.polno =b.polno and b.riskcode = c.riskcode and relationtoinsured <> '00'"; 
						
				var arr=easyExecSql(strSQL);
				var arr1=easyExecSql(strSQL1);
				if(arr1!=null){
					turnPage.queryModal(strSQL1, BnfGrid);
				}
				else{
					turnPage.queryModal(strSQL, BnfGrid);
				}
				
			//	alert(strSQL);
	 	
	//未查询到，填充默认值

	//if (BnfGrid.mulLineCount<=0)
	//{
	//	 var givemoney = fm.givemoney.value;
	//	 strSQL ="select a.contno,c.riskname,a.InsuredName,a.InsuredSex,a.InsuredBirthday,'1','1',a.InsuredNo,'','','','','','1','"+ givemoney +"' from "
	//	  +" LLCasePolicy a,lcpol b,lmrisk c where a.InsuredNo='" + fm.InsuredNo.value + "' and caseno='"+ fm.CaseNo.value +"' and a.contno=b.contno and b.riskcode = c.riskcode"; 
	//
	//	 turnPage.queryModal(strSQL, BnfGrid);
	//	 
	//}
}

////////
function afterQueryBankMess()
{	
	var strSQL="select  distinct Rgtno,(select codename from ldcode where codetype = 'bank' and code=bankcode),AccName,bankaccno,'银行转帐'"
					+" from llregister where 1=1 and getmode = '4' and CustomerNo='"+fm.InsuredNo.value+"'";
					//alert(strSQL);
					
					turnPage.queryModal(strSQL, AccountGrid);
}
//////////

function Update()
{

  var selNo = BnfGrid.getSelNo()-1;
  if ( selNo<0) 
  {
  	alert("请选择数据");
  	return false;
  }
  fm.fmtransact.value = "UPDATE||MAIN";
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action ="./LLBnfSave.jsp?InsuredNo="+fm.InsuredNo.value+"&CaseNo="+fm.CaseNo.value;
  fm.submit(); //提交
}
function modify()
{
	if(fm.paymode.value =='')
	{
		alert("必须选择领取方式！");
		return;
	}
	if(fm.paymode.value == 4)
	{
		if(fm.BankCode.value==""){alert("银行编码不能为空");        return ;}
		if(fm.BankAccNo.value==""){alert("银行账户不能为空");       return ;}
 		if(fm.AccName.value==""){alert("银行账户名不能为空");       return ;}
    if(fm.ModifyReason.value==""){alert("修改原因不能为空");    return ;}
  }
  try
  {     
  fm.fmtransact.value = "UPDATE";
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action ="./LLBnfLiveSave.jsp";
  fm.submit(); //提交
   }
  catch(ex)
  {
    alert("错误!");
  }   
}  
  
function giveConfirm()
{ 
  ////

  var arrResult  = new Array();
	var strSQL = "select RgtClass,TogetherFlag from LLRegister "
    +"where rgtNo='"+fm.RgtNo.value+"'"; 
    arrResult = easyExecSql(strSQL);
    if(arrResult!=null)
    {
    if(arrResult[0][0]==1&&arrResult[0][1]==3)
    {
    alert("团单给付方式为统一给付，这里不能做给付确认！");
    return;
    }
    }
  
	if (fm.RgtState.value != "09" )
	{
		alert("案件在当前状态下不能做给付确认！");
		return;		
	}
	//---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
   var PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a "
   			+" where exists (select 1 from llclaimdetail where grpcontno=a.grpcontno and caseno='"+ fm.CaseNo.value +"')";      			
   var tPrepaidBala=easyExecSql(PrepaidBalaSql);

	if ((fm.RealPay.value > 0 && tPrepaidBala != "" && tPrepaidBala != null
			&& tPrepaidBala > 0 && fm.PrePaidFlag.checked == false)
			|| (fm.RealPay.value < 0 && tPrepaidBala != ""
					&& tPrepaidBala != null && tPrepaidBala >= 0 && fm.PrePaidFlag.checked == false)) {
		if (!confirm("确定案件不回销预付赔款?")) {
			return false;
		}
	}
   if((tPrepaidBala == null || tPrepaidBala == "" )&& fm.PrePaidFlag.checked==true)
   {
   		alert("被保险人投保的保单不存在预付未回销赔款!");
   		return false;
   }
    //--------对于全部统一给付和全部统计给付（医疗机构）的批次案件，在审批审定页面给付确认时提示阻断---------//
    //GY add by 2012-12-20 //
    rgtno = fm.RgtNo.value;
	var caseno = fm.CaseNo.value;
	if (caseno != "") {
		var sql4 = "select caseno from llcase where caseno='"+caseno+"'" +
				" and rgtno in (select rgtno from llregister where togetherflag ='3'"
				+ " or togetherflag='4')";
		if (easyExecSql(sql4) != null) {
			alert("请在【团体结案】页面进行给付确认。");
			return false;
		}
	}
   //----------------End---
   	var tPreSql="";
    if(fm.PrePaidFlag.checked == true)
    {
    	 tPreSql ="select coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0),"
    		+" (SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE X.realpay END),0) "
	 		+" FROM (SELECT caseno,grpcontno,grppolno,sum(realpay) realpay FROM llclaimdetail "
	 		+" GROUP BY caseno,grpcontno,grppolno) AS X, "
	 		+" llprepaidgrppol Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno AND X.caseno=r.caseno), "
	 		+" (select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno)- "
	 		+" (SELECT NVL(SUM(CASE WHEN X.realpay>Y.prepaidbala THEN Y.prepaidbala ELSE X.realpay END),0) "
	 		+" FROM (SELECT caseno,grpcontno,grppolno,sum(realpay) realpay FROM llclaimdetail "
	 		+" GROUP BY caseno,grpcontno,grppolno) AS X, "
	 		+" llprepaidgrppol Y WHERE X.grpcontno=Y.grpcontno AND X.grppolno=Y.grppolno AND X.caseno=r.caseno) "
    		+" from llcase r where caseno='"+ fm.all('CaseNo').value +"'"
    }else if(fm.PrePaidFlag.checked == false){
        tPreSql ="select coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0),"
    		+" '0',coalesce((select coalesce(sum(realpay),0)  from llclaimdetail where caseno=r.caseno),0) "
    		+" from llcase r where caseno='"+ fm.all('CaseNo').value +"'"  
    }
    var tPrepaidArr=easyExecSql(tPreSql);
   	var tRealpay = tPrepaidArr[0][0];
	var tPrepaid = tPrepaidArr[0][1];
	var tPay = tPrepaidArr[0][2];
	if (tPrepaid != 0) {
	    //if(!confirm("案件赔款"+tRealpay+",回销预付赔款"+tPrepaid+",实际给付"+ tPay +"。是否继续")) {
	    //    return false;
	    //}
	    if(!confirm("案件赔款"+tRealpay+",回销预付赔款"+tPrepaid+"。是否继续？")) {
	        return false;
	    }
	}
  ////
	if(fm.paymode.value =='')
	{
		alert("必须选择领取方式！");
		return;
	}
//	if(fm.DrawerID.value==""|| fm.DrawerID.value ==null){
//		alert("领款人身份证未录入，请录入后继续操作");
//		return false;
//	}
		
	
//	 var strSQL=" select name from lcinsured where 1=1 and insuredno in ( select customerno from llcase where 1=1 and caseno='"+fm.CaseNo.value+"' ) union  select name from lbinsured where 1=1 and insuredno in ( select customerno from llcase where 1=1 and caseno='"+fm.CaseNo.value+"' )";
//	  
//	  //3501
//	  var arrstrSQL = easyExecSql(strSQL);
//		if(arrstrSQL)
//		{ 
//			if(arrstrSQL[0][0]==fm.Drawer.value){//相同
//				var strChkIdNo=checkIdNo("0",fm.DrawerID.value,"","");
//				if(strChkIdNo!=""){
//					if(!confirm("领款人身份证号不符合规则，请核实")){
//						return false;
//					}
//				}
//			}else{//不同
//				var strChkIdNo=checkIdNo("0",fm.DrawerID.value,"","");
//				if(strChkIdNo!=""){
//					alert("领款人身份证号有误，请核对后修改");
//					return false;
//				}
//			}
//		}
//	
	
		if(fm.paymode.value != '1'&&fm.paymode.value != '2')
	{
		if(fm.BankCode.value==""){alert("银行编码不能为空");return;}
	else 
		if(fm.BankAccNo.value==""){alert("银行账户不能为空");return;}
  else
 		if(fm.AccName.value==""){alert("银行账户名不能为空");return;}
	}

	
	
	if(fm.paymode.value != '1'&&fm.paymode.value != '2'){
		  if(fm.AccName.value!=''&&fm.Drawer.value==''){
  			fm.Drawer.value=fm.AccName.value; 
  			} 	
		}
	//#3312 商保赔款领款人和被保险人非同一人时给付方式校验功能 start
	var CaseSql = "select CHECKGRPCONT(rgtobjno) from llregister where rgtno = (select rgtno from llcase where caseno='"+ fm.all('CaseNo').value +"') with ur";
  	var tCase=easyExecSql(CaseSql);
  	if(tCase[0][0] != "Y" ) {  		
  		var customerName = fm.CustomerName.value;
  		var drawerName = fm.Drawer.value;
  		if(customerName!="" && customerName!=null&&customerName!="null"&&drawerName!=""
  			&&drawerName!=null&&drawerName!="null"&&customerName!=drawerName) {
  			if(fm.paymode.value=='4') {
  				alert("当赔款领款人和被保险人非同一人时，给付方式不能选择银行转账");
  				return;
  			}
  		}
  	}
	//#3312 商保赔款领款人和被保险人非同一人时给付方式校验功能 end
  //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
  		
  	if(fm.DrawerIDType.value=="0" || fm.DrawerIDType.value=="5"){
  		var draSrc=" select RelaDrawerInsured from llcaseext where 1=1 and caseno ='"+ fm.all('CaseNo').value +"' ";
  		var tCase=easyExecSql(draSrc);
  	  	if(tCase[0][0] != "28" ) {  
  				var strChkIdNo=checkIdNo(fm.DrawerIDType.value,fm.DrawerID.value,"","");
  		  		if(strChkIdNo!=""){
  		  			 alert("领款人证件号码不符合规则，请核实") ;
  		  			 return false;
  		  		}
  			}  			 
  		}//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
  	
	 try
  {     
  fm.fmtransact.value = "INSERT";
  fm.GiveConfirm.disabled=true;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action ="./LLBnfGiveConfirm.jsp";
  fm.submit(); //提交
   }
  catch(ex)
  {
    alert("错误!");
  } 
}  
  
function afterCodeSelect(cCodeName, Field )
{
	if( cCodeName == "paymode"){
    if(Field.value=="1"||Field.value=="2"){
      divBankAcc.style.display='none';
    } else{
      divBankAcc.style.display='';
    }
  }
}
function SendMail(){
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "邮件发送成功！" ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	}
function SendMsg( ttext,tto,MsgFlag )
{
		var Flag=MsgFlag;
  		if (MsgFlag) {
				   var tsname= "service@picchealth.com";      
   				  var tlogin="理赔咨询回复";      
   				  var tmobile=tto;//fm.all('to').value;   
   				         
				var tUrl="http://211.100.6.183/TSmsPortal/smssend?dt="+tmobile+"&feecode=000000&feetype=01&svid=picch&spno=5467&msg="+ ttext +"&reserve=0000000000000000&type=0";
				var win=window.open(tUrl,"fraSubmit",'height=110,width=220,top=260,left=400,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no');
			
				if (win.result==0){					
			      var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "短信发送" ;  
						showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
					 }
			 else{		
						var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "短信发送成功！" ;  
						showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");    	           
					  }	 
          }
 
}

function sendMessage()
{
  	  var sql0 = "select customername,endcasedate from llcase where CaseNo='"+fm.CaseNo.value+"'";
  	brr=easyExecSql(sql0);
  	var customername = brr[0][0];
  	endcasedate=brr[0][1];
  	var ExeSql = "select returnmode,rgtantmobile,email,rgtantname from llregister where rgtno='"+fm.RgtNo.value+"'";
  	arr=easyExecSql(ExeSql);
  	
  	if(arr)
  	{
  	  fm.AnswerMode.value=arr[0][0];
  	  fm.Mobile.value=arr[0][1];
  	  fm.Email.value=arr[0][2];
  	  if(fm.paymode.value=='1')
  	  {
  	  fm.SMSContent.value="尊敬的"+arr[0][3]+"先生/女士,您关于"+customername+"先生/女士的理赔申请已于"
  	                      +endcasedate+"受理完毕,请领取人"+fm.Drawer.value+"于14携带有效证件到财务处领取保险金."
  	                      +"如有任何疑问，请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
  	  fm.EmailContent.value="尊敬的"+arr[0][3]+"先生/女士,您关于"+customername+"先生/女士的理赔申请已于"
  	                      +endcasedate+"受理完毕,请领取人"+fm.Drawer.value+"于14携带有效证件到财务处领取保险金."
  	                      +"如有任何疑问，请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
  	  }
  	  else
  	  {
  	  fm.SMSContent.value="尊敬的"+arr[0][3]+"先生/女士,您关于"+customername+"先生/女士的理赔申请已于"
  	                      +endcasedate+"受理完毕,请请于近期注意查询您的保险金接收帐户."
  	                      +"如有任何疑问，请拨打95518（北京）或4006695518（全国其他地区）进行咨询。";
  	  fm.EmailContent.value="尊敬的"+arr[0][3]+"先生/女士,您关于"+customername+"先生/女士的理赔申请已于"
  	                      +endcasedate+"受理完毕,请请于近期注意查询您的保险金接收帐户."
  	                      +"如有任何疑问，请拨打95518（北京）或4006695518（全国其他地区）进行咨询。";
  	  }
  	  fm.action="./SendMail.jsp";
  	  fm.submit();
  	}
}