<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLSurveyFeeSave.jsp
//程序功能：
//创建日期：2005-02-23 11:53:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//接收信息，并作校验处理。
//输入参数
LLInqFeeSet tLLInqFeeSet   = new LLInqFeeSet();
TransferData tTransferData = new TransferData();
LLInqFeeUI tLLInqFeeUI   = new LLInqFeeUI();
//输出参数
CErrors tError = null;
String FlagStr = "";
String Content = "";
String transact = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
transact = request.getParameter("fmtransact");
System.out.println("transact : "+transact);

if(transact.equals("FI||CONFIRM")){
	String tsSurveyNo = request.getParameter("SurveyNo");
	tTransferData.setNameAndValue("SurveyNo",tsSurveyNo);
	String tsFeeItemCode[] = request.getParameterValues("InqFeeDetailGrid2");	//费用项目代码
	String tsFeeSum[] = request.getParameterValues("InqFeeDetailGrid3");	//金额
	String tsInqDept[]= request.getParameterValues("InqFeeDetailGrid4");	//调查机构
	String tRNum[] = request.getParameterValues("InpInqFeeDetailGridChk");
	int feeItemCount = 0;
	if(tsFeeItemCode!=null)
	  feeItemCount = tsFeeItemCode.length;
	for (int i = 0; i < feeItemCount; i++){
	  if (tsFeeItemCode[i] != null && !tsFeeItemCode[i].equals("") && "1".equals( tRNum[i])){
	    LLInqFeeSchema tLLInqFeeSchema = new LLInqFeeSchema();
	    tLLInqFeeSchema.setFeeItem(tsFeeItemCode[i]);
	    tLLInqFeeSchema.setSurveyNo(tsSurveyNo);
    	tLLInqFeeSchema.setFeeSum(tsFeeSum[i]);
	    tLLInqFeeSchema.setContSN("1");
	    tLLInqFeeSchema.setInqDept(tsInqDept[i]);
	    tLLInqFeeSet.add(tLLInqFeeSchema);
	  }
	}
}
else{
	String tSurveyNo = request.getParameter("SurveyNo");
	tTransferData.setNameAndValue("SurveyNo",tSurveyNo);
	String tsOtherNo = request.getParameter("CaseNo");
	tTransferData.setNameAndValue("OtherNo",tsOtherNo);
	String tsSurveyNo[] = request.getParameterValues("InqFeeDetailGrid8");//调查号
	String tsFeeItemCode[] = request.getParameterValues("InqFeeDetailGrid3");	//费用项目代码
	String tsFeeSum[] = request.getParameterValues("InqFeeDetailGrid4");	//金额
	String tsInqDept[]= request.getParameterValues("InqFeeDetailGrid5");	//调查机构
	int feeItemCount = 0;
	if(tsFeeItemCode!=null)
	  feeItemCount = tsFeeItemCode.length;
	for (int i = 0; i < feeItemCount; i++){
	  if (tsFeeItemCode[i] != null && !tsFeeItemCode[i].equals("") &&
	  tsFeeSum[i] != null && !tsFeeSum[i].equals("")){
	    LLInqFeeSchema tLLInqFeeSchema = new LLInqFeeSchema();
	    tLLInqFeeSchema.setFeeItem(tsFeeItemCode[i]);
	    tLLInqFeeSchema.setSurveyNo(tsSurveyNo[i]);
	    try{
	    	tLLInqFeeSchema.setFeeSum(tsFeeSum[i]);
	    }
	    catch(Exception ex){
	    	Content = "保存失败，费用值不是有效的数字格式!";
	  		FlagStr = "Fail";
	    }
	    tLLInqFeeSchema.setOtherNo(tsOtherNo);
	    tLLInqFeeSchema.setContSN("1");
	    tLLInqFeeSchema.setInqDept(tsInqDept[i]);
	    tLLInqFeeSet.add(tLLInqFeeSchema);
	  }
	}
}

if(!FlagStr.equals("Fail")){
	try{
	  // 准备传输数据 VData
	  VData tVData = new VData();
	  tVData.add(tTransferData);
	  tVData.add(tLLInqFeeSet);
	  tVData.add(tG);
	  tLLInqFeeUI.submitData(tVData,transact);
	}
	catch(Exception ex){
	  Content = "保存失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
	}
}

//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr.equals("")){
  tError = tLLInqFeeUI.mErrors;
  if (!tError.needDealError()){
    Content = " 保存成功! ";
    FlagStr = "Success";
  }
  else{
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

//添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
