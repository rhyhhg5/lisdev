<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLCustomerInfoRpt.jsp
//程序功能：F1报表生成
//创建日期：2010-12-02
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>

<%@page import="java.io.*"%>
<%
		System.out.println("start");
  	CError cError = new CError();
  	boolean operFlag=true;
  	
  	//直接获取页面的值
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String strOperation = "";
  	String MngCom 				= request.getParameter("ManageCom");	
  	String ManageName 			= request.getParameter("ManageComName");
  	String EndCaseDateS 		= request.getParameter("EndCaseDateS");		//结案起期
  	String EndCaseDateE 		= request.getParameter("EndCaseDateE");		//结案止期

  	TransferData Para = new TransferData();
      
  	Para.setNameAndValue("MngCom",MngCom);
  	Para.setNameAndValue("ManageName",ManageName);
  	Para.setNameAndValue("EndCaseDateS",EndCaseDateS);
  	Para.setNameAndValue("EndCaseDateE",EndCaseDateE);
   	
//  ------------------------------
   	RptMetaDataRecorder rpt=new RptMetaDataRecorder(request);
//   	-------------------------------
  	String date=PubFun.getCurrentDate().replaceAll("-","");  
	String time=PubFun.getCurrentTime3().replaceAll(":","");
  	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	String fileNameB = tG.Operator + "_" + FileQueue.getFileName()+".vts";
	Para.setNameAndValue("tFileNameB",fileNameB);
	String operator = tG.Operator;
	System.out.println("@理赔客户信息报表@");
	System.out.println("Operator:"+operator);
	System.out.println("ManageCom:"+tG.ManageCom);
	System.out.println("Operator:"+tG.Operator);
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();

    tVData.addElement(tG);    
    tVData.addElement(Para);
 
    
    LLCustomerInfoUI tLLCustomerInfoUI = new LLCustomerInfoUI();
	XmlExport txmlExport = new XmlExport();    
    if(!tLLCustomerInfoUI.submitData(tVData,"PRINT"))
    {
       	operFlag=false;
       	Content=tLLCustomerInfoUI.mErrors.getFirstError().toString(); 
       	%>
   		<%=Content%>  
   		<%return;
    }
    else
    {  
    	System.out.println("--------成功----------");  
		mResult = tLLCustomerInfoUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	
	   		%>
	   		
	   		<%=Content%>  
	   		<%return;
	  	}
	}
    
    Readhtml rh=new Readhtml();
	  rh.XmlParse(txmlExport.getInputStream()); //相当于XmlExport.getInputStream();
	  
	  String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
	  String temp=realpath.substring(realpath.length()-1,realpath.length());
	  if(!temp.equals("/"))
	  {
		  realpath=realpath+"/"; 
	  }
	  String templatename=rh.getTempLateName();//模板名字
	  String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
	  System.out.println("*********************templatepathname= " + templatepathname);
	  System.out.println("************************realpath="+realpath);
	  
	  String outname="CustomerInfo"+tG.Operator+date+time+".xls";
	  String outpathname=realpath+"vtsfile/"+outname;//该文件目录必须存在,应该约定好,统一存放,便于定期做文件清理工作 Commented By Qisl At 2008.10.23
	  //*******************************
	  rpt.updateReportMetaData(outname);
	  //**********************************
	 
	  //*********************************
	  rh.setReadFileAddress(templatepathname);
	  rh.setWriteFileAddress(outpathname);
	  rh.start("vtsmuch");
	  try{
	  outname = java.net.URLEncoder.encode(outname, "UTF-8");
	  outname = java.net.URLEncoder.encode(outname, "UTF-8");
	  //outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
	  //outpathname = java.net.URLEncoder.encode(outpathname, "UTF-8");
    }catch(Exception ex)
    {
     ex.printStackTrace();
    }
      String[] InputEntry = new String[1];
      InputEntry[0] = outpathname;
      String tZipFile = realpath+"vtsfile/"+ StrTool.replace(outname,".xls",".zip");
		System.out.println("tZipFile == " + tZipFile);
      rh.CreateZipFile(InputEntry, tZipFile);



%>

<html>
<%@page contentType="text/html;charset=GBK" %>
<a href="../f1print/download.jsp?filename=<%=StrTool.replace(outname,".xls",".zip")%>&filenamepath=<%=tZipFile%>">点击下载</a>
</html>

