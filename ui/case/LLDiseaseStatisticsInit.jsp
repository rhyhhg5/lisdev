
<%
	//程序名称：LLDiseaseStatisticsInit.jsp
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容

%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="javascript">  

  function initReportInformationGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";       		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";        		//列宽
      iArray[0][2]=10;        			//列最大值
      iArray[0][3]=0;          			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="疾病名称";
      iArray[1][1]="100px";
      iArray[1][2]=100;
      iArray[1][3]=0;
      
      iArray[2]=new Array();
      iArray[2][0]="疾病编码";
      iArray[2][1]="100px";
      iArray[2][2]=100;
      iArray[2][3]=0;
      
      iArray[3]=new Array();
      iArray[3][0]="医院名称";
      iArray[3][1]="100px";
      iArray[3][2]=100;
      iArray[3][3]=0;
      
      iArray[4]=new Array();
      iArray[4][0]="地区";
      iArray[4][1]="100px";
      iArray[4][2]=100;
      iArray[4][3]=2;
      iArray[2][4]="llclaimrisk"
      
      iArray[5]=new Array();
      iArray[5][0]="次均住院费用";
      iArray[5][1]="100px";
      iArray[5][2]=100;
      iArray[5][3]=0;
      
      ReportInformationGrid = new MulLineEnter("fm" ,"ReportInformationGrid" ); 
      //这些属性必须在loadMulLine前
      ReportInformationGrid.mulLineCount = 3;   
      ReportInformationGrid.displayTitle = 1;
      ReportInformationGrid.locked = 1;
      ReportInformationGrid.canSel=1;
      ReportInformationGrid.canChk = 0;
      ReportInformationGrid.hiddenSubtraction=1;
      ReportInformationGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      ReportInformationGrid.selBoxEventFuncName="showDetails";
      ReportInformationGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
    }
    catch(ex)
    {
      alert("在LLDiseaseStatisticsInit.jsp-->initReportInformationGrid函数中发生异常:初始化界面错误!");
    }
  }

  function initForm()
  {
    try
    {
      initReportInformationGrid(); 
      
    }
    catch(re)
    {
      alert("初始化界面错误!");
    }
  }
</script>
