<%
//程序名称：LLAddSurvFeeInit.jsp
//程序功能：间接调查费录入
//创建日期：2005-6-26 12:07
//创建人  ：Xx
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String Operator=tG.Operator;
String Comcode=tG.ManageCom;
String CurrentDate= PubFun.getCurrentDate();
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
%>

<script language="JavaScript">

  var turnPage=new turnPageClass();

  function initInpBox(){
    try{
      fm.comcode.value="<%=Comcode%>";
      var Month="<%=tCurrentMonth%>"/1-1;
      var Year="<%=tCurrentYear%>"/1;
      if(Month==0){
      	Month=12;
      	Year-=1;
      }
      fm.Year.value=Year;
      fm.Month.value=Month;
      fm.Operator.value="<%=Operator%>"
    }
    catch(ex){
      alert("在LLAddSurvFeeInit.jsp-->InitInpBox1函数中发生异常:初始化界面错误!");
    }
  }

  // 下拉框的初始化
  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在LLAddSurvFeeInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm(){
    try{
      initInpBox();
      initAddFeeGrid();
      initSurveyCaseGrid();
      updateCaseGrid();
      queryIndFee();
    }
    catch(re){
      alert("在LLAddSurvFeeInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
    }
  }

  // 保单信息列表的初始化
  function initAddFeeGrid()
  {
    var iArray = new Array();
    try
    {
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("费用项目","50px","100","0");
      iArray[2]=new Array("费用项目代码","20px","10","3");
      iArray[3]=new Array("金额","20px","20","1");
      iArray[4]=new Array("审核状态","20px","10","0");
      iArray[5]=new Array("录入批次流水","20px","10","3");

      AddFeeGrid = new MulLineEnter( "fm" , "AddFeeGrid" );
      AddFeeGrid.mulLineCount = 0;
      AddFeeGrid.displayTitle = 1;
      AddFeeGrid.locked = 0;
      AddFeeGrid.canSel = 0;
      AddFeeGrid.canChk = 0;
      AddFeeGrid.hiddenPlus=1;
      AddFeeGrid.hiddenSubtraction=1;
      AddFeeGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert(ex);
    }
  }

  function initSurveyCaseGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10","0");
      iArray[1]=new Array("案件号","40px","30","0");
      iArray[2]=new Array("提调次数","20px","0","0");
      iArray[3]=new Array("案件状态","20px","0","0");
      iArray[4]=new Array("分配标记","0px","10","3");
      SurveyCaseGrid = new MulLineEnter("fm","SurveyCaseGrid");
      SurveyCaseGrid.mulLineCount =0;
      SurveyCaseGrid.displayTitle = 1;
      SurveyCaseGrid.locked = 1;
      SurveyCaseGrid.canSel =0;
      SurveyCaseGrid.canChk =1;
      SurveyCaseGrid.hiddenPlus=1;
      SurveyCaseGrid.hiddenSubtraction=1;
      SurveyCaseGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }

</script>
