<%
//Name:ReportInit.jsp
//function：
//author:MN
//Date:2008-07-25
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">

function initForm()
{

  try
  {
    initGrpContGrid();
  }
  catch(re)
  {
    alter("在GrpContInvaliedate.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initGrpContGrid()
{

   var iArray = new Array();
   try
   {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("团体保单号","80px","0",0);
    iArray[2]=new Array("单位名称","80px","0",0);
    iArray[3]=new Array("客户号","60px","0",0);
    iArray[4]=new Array("保障计划","40px","0",0);
    iArray[5]=new Array("险种代码","40px","0",0);
    iArray[6]=new Array("险种名称","120px","0",0);
    iArray[7]=new Array("险种保额","40px","100",1); 
    iArray[7][21] = "RiskAmnt"; 
    
    GrpContGrid = new MulLineEnter( "fm" , "GrpContGrid" );
    GrpContGrid.mulLineCount =5;
    GrpContGrid.displayTitle = 1;
    //GrpContGrid.locked = 1;
    GrpContGrid.canSel = 1;
    GrpContGrid.hiddenPlus=1;  
    GrpContGrid.hiddenSubtraction=1; 
    GrpContGrid.loadMulLine(iArray);
  } 
  catch(ex)
  {
    alert(ex);
  }    
}


 </script>