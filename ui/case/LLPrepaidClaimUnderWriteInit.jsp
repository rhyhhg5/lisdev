<%
  //程序名称：LLPrepaidClaimUnderWriteInit.jsp
  //程序功能：预付赔款审批
  //创建日期：2010-11-30
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
GlobalInput tG = (GlobalInput)session.getValue("GI");
String mCurrentDate= PubFun.getCurrentDate();  
FDate tFDate=new FDate();
String AfterDate = tFDate.getString(PubFun.calDate(tFDate.getDate(mCurrentDate),-90,"D",null));
%>
<script language="JavaScript">

function initForm()
{
	try{
		initInpBox();
		initPreClaimGrid();
		initPreClaimDetailGrid();
	}
	catch(re){
		alert("LLPrepaidClaimUnderWriteInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}
function initInpBox()
{
  try
  {
    fm.ManageCom.value = "<%=tG.ComCode%>";
  	fm.Operator.value = "<%=tG.Operator%>";
  	fm.RgtDateS.value = "<%=AfterDate%>";
  	fm.RgtDateE.value = "<%=mCurrentDate%>";

  }
  catch(ex)
  {
    alter("在LLPrepaidClaimUnderWriteInit.jsp-->initInpBox函数中发生异常:初始化界面错误!");
  }
}

function initPreClaimGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="管理机构";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="预付赔款号";
		iArray[2][1]="120px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		iArray[2][21]="PrepaidNo";

		iArray[3]=new Array();
		iArray[3][0]="团体保单号";
		iArray[3][1]="120px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="投保单位名称";
		iArray[4][1]="120px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="申请日期";
		iArray[5][1]="80px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="预付类型";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="预付金额";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="预付赔款状态";
		iArray[8][1]="80px";
		iArray[8][2]=100;
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="审批人";
		iArray[9][1]="60px"; 
		iArray[9][2]=100;
		iArray[9][3]=0;
		
		iArray[10]=new Array();
		iArray[10][0]="预付状态";
		iArray[10][1]="60px";
		iArray[10][2]=100;
		iArray[10][3]=3;
		iArray[10][21]="RgtState";
		
		PreClaimGrid = new MulLineEnter( "fm" , "PreClaimGrid" ); 

		PreClaimGrid.mulLineCount=1;
		PreClaimGrid.displayTitle=1;
		PreClaimGrid.canSel=1;
		PreClaimGrid.canChk=0;
		PreClaimGrid.hiddenPlus=1;
		PreClaimGrid.hiddenSubtraction=1;
		PreClaimGrid.selBoxEventFuncName = "queryPreDetail";

		PreClaimGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initPreClaimDetailGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="险种编码";
		iArray[1][1]="40px";
		iArray[1][2]=100;
		iArray[1][3]=0;
		iArray[1][21]="riskcode";

		iArray[2]=new Array();
		iArray[2][0]="险种名称";
		iArray[2][1]="100px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="实收保费";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;
		iArray[3][21]="pay";

		iArray[4]=new Array();
		iArray[4][0]="已预付赔款";
		iArray[4][1]="75px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="结案赔款";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="本次预付金额";
		iArray[6][1]="90px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		iArray[6][21]="money";
		
		iArray[7]=new Array();
		iArray[7][0]="团体险种号";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=3;
		iArray[7][21]="grppolno";

		PreClaimDetailGrid = new MulLineEnter( "fm" , "PreClaimDetailGrid" ); 

		PreClaimDetailGrid.mulLineCount=1;
		PreClaimDetailGrid.displayTitle=1;
		PreClaimDetailGrid.canSel=0;
		PreClaimDetailGrid.canChk=0;
		PreClaimDetailGrid.hiddenPlus=1;
		PreClaimDetailGrid.hiddenSubtraction=1;

		PreClaimDetailGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
