<%
//程序名称：LMInsuAccRateInit.jsp
//程序功能：给付比例和免赔额的修改
//创建日期：2013-4-15
//创建人  ：zlx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<!--用户校验类-->
<%
GlobalInput globalInput = (GlobalInput)session.getValue("GI");
String strManageCom = globalInput.ManageCom;
String strOperator =globalInput.Operator;
%>  
<script language="JavaScript" >
//输入框的初始化 
function initInpBox()	
{
  try
  {
  }
  catch(ex)
  {
    alert("在LMInsuAccRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();//把文本框设空
    initCaseGrid(); //初始化列名
  }
  catch(re) 
  {
    alert("在TestGroupListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//保单信息Mulline的初始化
function initCaseGrid(){
  var iArray = new Array();
  try{
    iArray[0]=new Array("序号","30px","10",0);
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="核保师";
    iArray[1][1]="30px";
    iArray[1][2]=100;
    iArray[1][3]=0;
  
	iArray[2]=new Array();
    iArray[2][0]="核保状态";
    iArray[2][1]="30px";
    iArray[2][2]=100;
    iArray[2][3]=0;

    CaseGrid = new MulLineEnter("fm","CaseGrid");
    CaseGrid.mulLineCount =0;
    CaseGrid.displayTitle = 1;
    CaseGrid.locked = 1;
    CaseGrid.canChk =1;
    CaseGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CaseGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CaseGrid. selBoxEventFuncName = "onSelSelected";
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex){
    alter(ex);
  }
}

</script>