<%@page import="com.sinosoft.httpclientybk.inf.N01"%>
<%@page import="com.sinosoft.httpclientybk.inf.U01"%>
<%@page import="com.sinosoft.httpclientybk.inf.YBU04"%>
<%@page import="com.sinosoft.httpclientybk.T01.YbPremInfoUp"%>
<%@page import="com.sinosoft.httpclientybk.T02.YbBankInfoUp"%>
<%
//程序名称：ClaimSave.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：wujs
//更新记录：  更新人    更新日期     更新原因/内容

%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page import=" java.text.*"%>
<%@page import= "java.util.*"%>


<%
//接收信息，并作校验处理。
//输入参数
String oper = request.getParameter("Opt");
String pNumber = request.getParameter("pNumber");
String FlagStr = "";
String Content = "";
/* VData tVData = new VData();
TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("PrtNo", pNumber); 
tVData.add(tTransferData);*/

//核保信息上传
//nLLUploadHbTask hbTask = new nLLUploadHbTask();
YBKU01Task hbTask = new YBKU01Task();
if(oper.equals("hb")) {
	hbTask.run();
	if("true".equals(hbTask.getOpr()))
		 Content="核保信息上传批处理成功!";
	else
		 Content="核保信息上传批处理失败!";
}
//银行卡缴费信息上传
//nLLUploadYhkTask yhkTask = new nLLUploadYhkTask();
YbBankInfoUpTask yhkTask = new YbBankInfoUpTask();
 if(oper.equals("yhk")) {
 	yhkTask.run();
 	if("true".equals(hbTask.getOpr()))
 		Content="银行卡缴费结果信息上传批处理成功!";
	else
		Content="银行卡缴费结果信息上传批处理失败!";
 }
 //承保信息上传
//nLLUploadCbTask cbTask = new nLLUploadCbTask();
 YBKN01Task cbTask = new YBKN01Task();
 if(oper.equals("cb")) {
	 cbTask.run();
 	 if("true".equals(cbTask.getOpr()))
 		 Content="承保信息上传批处理成功!";
	 else
		 Content="承保信息上传批处理失败!";
 }
 //保费信息上传
//nLLUploadBfTask bfTask = new nLLUploadBfTask();
 YbPremInfoUpTask bfTask = new YbPremInfoUpTask();
 if(oper.equals("bf")){
	 bfTask.run();
	 if("true".equals(bfTask.getOpr()))
		 Content="保费信息上传批处理成功";
	 else
		 Content="保费信息上传批处理失败";
}
 
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
