<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：CaseReceiptQueryOut.jsp
//程序功能：在"立案"模块中的分案诊疗明细中将按照"立案号"查询出的数据显示在"分案诊疗明细"界面上的控件上
//创建人  ：刘岩松
//创建日期：2002-11-15
//更新记录：
//更新人:刘岩松
//更新日期:
//
//更新原因/内容:
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>

<%
    //输出参数
    String Content = "";
    CErrors tError = null;
    String FlagStr = "Fail";

    LLCaseCureSchema tLLCaseCureSchema = new LLCaseCureSchema();
    tLLCaseCureSchema.setCaseNo(request.getParameter("CaseNo"));
    // 准备传输数据 VData
    VData tVData = new VData();
    tVData.addElement(tLLCaseCureSchema);

    CaseCureQueryUI tCaseCureQueryUI   = new CaseCureQueryUI();
    if (!tCaseCureQueryUI.submitData(tVData,"QUERY||MAIN"))
    {
    		Content = " 查询失败，原因是: " + tCaseCureQueryUI.mErrors.getError(0).errorMessage;
      		FlagStr = "Fail";
    }
    else
    {
		tVData.clear();
		tVData = tCaseCureQueryUI.getResult();

		LLCaseCureSet mLLCaseCureSet = new LLCaseCureSet();
		mLLCaseCureSet.set((LLCaseCureSet)tVData.getObjectByObjectName("LLCaseCureBLSet",0));
		LLCaseCureSchema mLLCaseCureSchema = mLLCaseCureSet.get(1);

		%>
			<script language="javascript">
					parent.fraInterface.fm.all("AccidentType").value = "<%=mLLCaseCureSchema.getAccidentType()%>";
    	</script>
		<%
     }
    tVData.clear();
    tVData.addElement(tLLCaseCureSchema);
     %>
<html>
<script language="javascript">
	</script>
</html>