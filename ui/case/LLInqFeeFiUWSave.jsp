<%
//程序名称：LLInqFeeFiUWSave.jsp
//程序功能：
//创建日期：2006-09-12 11:53:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//接收信息，并作校验处理。
//输入参数
LLInqDispatchBL tLLInqDispatchBL   = new LLInqDispatchBL();
VData tVData = new VData();
//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
String transact = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
tVData.add(tG);

String FeeType = request.getParameter("FeeType");
if (FeeType.equals("1")){
  //直接查勘费的save
  transact = "CONF||ZJ";
  LLInqFeeSet aLLInqFeeSet = new LLInqFeeSet();
  String tSuveyNo = request.getParameter("SurveyNo");
  String tFeeCode[] = request.getParameterValues("InqFeeDetailGrid1");
  String tInqDept[] = request.getParameterValues("InqFeeDetailGrid4");
	String tRNum[] = request.getParameterValues("InqFeeDetailGridChk");
	if (tRNum != null) 
	{
		System.out.println("<-tRNum-->"+ tRNum.length);
  	for (int i = 0; i < tRNum.length; i++)	
  	{   
			if ("1".equals( tRNum[i]))
			{
	  			System.out.println("<-eventno[i]-->"+ tRNum[i]);
	  			LLInqFeeSchema tLLInqFeeSchema = new LLInqFeeSchema();
		      tLLInqFeeSchema.setFeeItem(tFeeCode[i]);
			    tLLInqFeeSchema.setSurveyNo(tSuveyNo);
			    tLLInqFeeSchema.setContSN("1");
			    tLLInqFeeSchema.setInqDept(tInqDept[i]);
					
					aLLInqFeeSet.add(tLLInqFeeSchema);
			}
		}
	}
	tVData.add(aLLInqFeeSet);
}
else if(FeeType.equals("2")){
  //间接查勘费的save
  transact = "CONF||JJ";
}
else{
  Content="保存失败，请指定审核的查勘费类型！";
  FlagStr="Fail";
}
try
{
  // 准备传输数据 VData
  System.out.println("transact"+transact);
  tLLInqDispatchBL.submitData(tVData,transact);
}
catch(Exception ex)
{
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}

//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
  tError = tLLInqDispatchBL .mErrors;
  if (!tError.needDealError())
  {
    Content = " 保存成功! ";
    FlagStr = "Success";
  }
  else
  {
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

//添加各种预处理
%>
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
