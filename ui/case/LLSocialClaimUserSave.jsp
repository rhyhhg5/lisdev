<%
//Name    ：LLSocialClaimUserSave.jsp
//Function：对理赔岗位人员权限的分配执行后台调用程序
//Author   :Fanting
//Date：2014-4-9  民生
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%

  LLSocialClaimUserSchema mLLSocialClaimUserSchema = new LLSocialClaimUserSchema();
  LDSpotUWRateSchema mLDSpotUWRateSchema  = new LDSpotUWRateSchema();

  CErrors tError = null;
  String transact = request.getParameter("fmtransact");

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  
    // add check 2014-04-08 
    mLLSocialClaimUserSchema.setComCode(request.getParameter("Station"));//机构代码   
    mLLSocialClaimUserSchema.setUserCode(request.getParameter("UserCode"));//用戶编码    
    mLLSocialClaimUserSchema.setUserName(StrTool.unicodeToGBK(request.getParameter("UserName"))); //名称     
    mLLSocialClaimUserSchema.setSocialMoney(request.getParameter("SocialMoney"));//社保业务结案金额    
    mLLSocialClaimUserSchema.setUpUserCode(request.getParameter("UpUserCode"));//上级用户代码    
    mLLSocialClaimUserSchema.setRgtFlag(request.getParameter("RgtFlag"));// 參考配置程序，默認值為1     
    mLLSocialClaimUserSchema.setHandleFlag(request.getParameter("HandleFlag"));//参加案件分配  
    mLLSocialClaimUserSchema.setDispatchRate(request.getParameter("DispatchRate"));// 案件分配上限    
    mLLSocialClaimUserSchema.setSurveyFlag(request.getParameter("ServeyFlag"));//调查权限      
    mLLSocialClaimUserSchema.setPrepaidLimit(request.getParameter("PrepaidLimit"));//案件抽检金额
    mLLSocialClaimUserSchema.setStateFlag(request.getParameter("StateFlag"));//用戶有效状态    
    String Path = application.getRealPath("config//Conversion.config");
    mLLSocialClaimUserSchema.setRemark(StrTool.Conversion(request.getParameter("Remark"),Path));//备注
    
    mLLSocialClaimUserSchema.setPrepaidFlag(request.getParameter("PrepaidFlag"));//预付赔款标记
    mLLSocialClaimUserSchema.setPrepaidUpUserCode(request.getParameter("PrepaidUpUserCode"));//预付赔款上级
    mLLSocialClaimUserSchema.setPrepaidMoney(request.getParameter("PrepaidMoney"));//预付赔款金额
   
    mLDSpotUWRateSchema.setUserCode(request.getParameter("UserCode"));
    mLDSpotUWRateSchema.setUserName(StrTool.unicodeToGBK(request.getParameter("UserName")));
    mLDSpotUWRateSchema.setUWGrade("AAAA");
    mLDSpotUWRateSchema.setUWType("20000");
    mLDSpotUWRateSchema.setUWRate(request.getParameter("ClaimSpotRate"));

    
    
    


    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");

    VData tVData = new VData();
    LLSocialClaimUserOperateBL mLLSocialClaimUserOperateBL = new LLSocialClaimUserOperateBL();
    try
    {
      tVData.addElement(mLLSocialClaimUserSchema);
      tVData.addElement(mLDSpotUWRateSchema);
      tVData.addElement(tG);
      mLLSocialClaimUserOperateBL.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = transact+"失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }

    if (FlagStr=="")
    {
      tError = mLLSocialClaimUserOperateBL.mErrors;
      if (!tError.needDealError())
      {
        Content = " 操作成功！";
        FlagStr = "Succ";
      }
      else
      {
        Content = " 操作失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }
  
  //处理转意字符
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>