<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：LLHospFinaceInit.jsp
//程序功能：
//创建日期：2003-06-16
//创建人  ：Kevin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {

  }
  catch(ex)
  {
    alert("在LLHospFinaceInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLHospFinaceInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
		initHospGetInfo();
		//initCardListInfo();
  }
  catch(re)
  {
    alert("LLHospFinaceInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 个人客户信息列表的初始化
function initHospGetInfo()
{
	var iArray = new Array();

	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="批次号";         		//列名
		iArray[1][1]="100px";            		//列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="医院编码";          		//列名
		iArray[2][1]="60px";            		//列宽
		iArray[2][2]=85;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="医院名称";        		  //列名
		iArray[3][1]="70px";            		//列宽
		iArray[3][2]=85;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="申请日期";          		//列名
		iArray[4][1]="60px";            		//列宽
		iArray[4][2]=100;            		 	  //列最大值
		iArray[4][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="申请机构";         		  //列名
		iArray[5][1]="60px";            		//列宽
		iArray[5][2]=100;            			  //列最大值
		iArray[5][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="申请人";         		  //列名
		iArray[6][1]="60px";            		//列宽
		iArray[6][2]=100;            			  //列最大值
		iArray[6][3]=0; 
		
		iArray[7]=new Array();
		iArray[7][0]="给付金额";         		  //列名
		iArray[7][1]="60px";            		//列宽
		iArray[7][2]=100;            			  //列最大值
		iArray[7][3]=0; 

		iArray[8]=new Array();
		iArray[8][0]="给付状态";          		  //列名
		iArray[8][1]="60px";            		//列宽
		iArray[8][2]=60;            			  //列最大值
		iArray[8][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		HospGetInfo = new MulLineEnter("fm" , "HospGetInfo");
		// 这些属性必须在loadMulLine前
		HospGetInfo.mulLineCount = 10;
		HospGetInfo.displayTitle = 1;
		HospGetInfo.locked = 1;
		HospGetInfo.canSel = 1;
    HospGetInfo.hiddenPlus = 1;
    HospGetInfo.hiddenSubtraction = 1;
    HospGetInfo.selBoxEventFuncName = "SelectHospGetInfo";
		HospGetInfo.loadMulLine(iArray);		
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>