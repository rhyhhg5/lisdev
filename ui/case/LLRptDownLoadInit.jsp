<%
/*=============================================
Name：LLRptDownLoadInit.jsp
Function： 打印报表下载
Date：2010-8-3
Author：ZhangXiaolei
=============================================*/
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.util.*"%>
<%
	
    GlobalInput GI = new GlobalInput();
    GI = (GlobalInput)session.getValue("GI");
    String CurrentDate= PubFun.getCurrentDate();   
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	           	
    String AheadDays="-30";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
%>
	<script>
     
     var ComCode = "<%=GI.ComCode%>"; //记录登陆机构
     var Operator="<%=GI.Operator%>";//记录操作员
     var startDate= "<%=afterdate%>" ;
     var endDate="<%=CurrentDate%>";
    </script>

<script language="JavaScript">
function initInpBox()
{
  try
  {
   
    
  }
  catch(ex)
  {
    alter("在LLRptDownLoadInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initReportMetaData()
    fm.all('ManageCom').value=ComCode;
    fm.all('UserCode').value=Operator;
   // alert(Operator);
    fm.all( 'StartDate').value = startDate;
    fm.all( 'EndDate' ).value = endDate; 
    var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                           
    var arrResult1 = easyExecSql("select username from llclaimuser where usercode = '"+fm.all('UserCode').value+"'");                        
    fm.all('ManageComName').value=arrResult[0][0];                   
    fm.all('UserName').value=arrResult1[0][0];
    var claimuser = easyExecSql("select 1 from llclaimuser where usercode='"+Operator+"'");
    fm.all('claimoperator').value=claimuser[0][0];
  }
  catch(re)
  {
    alter("在LLRptDownLoadInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initReportMetaData()
{
  var iArray = new Array();
  try
  {  
    iArray[0] =  new Array("序号","30px","10","3");
    iArray[1] = new Array("统计机构","60px","0","0");
    iArray[2] = new Array("用户名","50px","0","0");   //0px是隐藏了
    iArray[3] = new Array("下载路径","0px","0","3");  
    iArray[4] = new Array("文件名","0px","0","3");
    iArray[5] = new Array("临时报表名","0px","0","3");
    iArray[6] = new Array("报表名称","100px","0","0");
    iArray[7] = new Array("统计起期","80px","0","0");     
    iArray[8] = new Array("统计止期","80px","0","0"); 
    iArray[9] = new Array("打印时间","80px","0","0");     
    iArray[10] = new Array("报表状态","60px","0","0");
    iArray[11] = new Array("报表编号","0px","0","3");
    iArray[12] = new Array("统计条件","370px","0","0");
    iArray[13] = new Array("原路径","0px","0","3");
    ReportMetaData = new MulLineEnter("fm","ReportMetaData");
    ReportMetaData.displayTitle = 1;
    ReportMetaData.locked = 1;
    ReportMetaData.canSel =1;
    ReportMetaData.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ReportMetaData.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    ReportMetaData.loadMulLine(iArray);
  }  
  catch(ex)
  {
    alter(ex);
  }
}

 </script>
