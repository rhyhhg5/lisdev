//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage2 = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage  = new turnPageClass();

var arrDataSet;
var ImportPath;
var ImportState = "no";
pageOpen = new Date();
window.onbeforeunload = beforeAfterInput;
window.onunload= AfterInput;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
  if(showInfo!=null) {
    try {
      showInfo.focus();
    } catch(ex) {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm() {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  window.focus();
  queryGrpCaseGrid();
  QueryErrList();
  queryList();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
  initForm();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
  try {
    initForm();
  } catch(re) {
    alert("在LCInuredList.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm() {
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
  if(cDebug=="1") {
    parent.fraMain.rows = "0,0,50,82,*";
  } else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function deleteClick() {
  //下面增加相应的删除代码
  if (confirm("您确实想删除这些记录吗?")) {
  	  //#2169 批次导入前后台数据同步问题方案
	  var arrResult = new Array();
	  var strSQL="select rgtstate from llregister where rgtno='"+fm.RgtNo.value+"' with ur";
	  arrResult=easyExecSql(strSQL);
	  if(arrResult != null){
	  		var tRgtState=arrResult[0][0];
	  		if(tRgtState=='06'){
	  			alert("批次号:"+fm.RgtNo.value+"还在处理中，请稍候再处理该批次的案件!");
	  			return ;
	  		}
	  }
  	
  	  var rowsNo = DiskErrQueryGrid.mulLineCount;
	  if (rowsNo>0){
	  	var checkRc = false;
	  	var kk=0;
	  	for(kk=0;kk<rowsNo;kk++){
	  		if(DiskErrQueryGrid.getChkNo(kk)){
	  			checkRc=true;
	  		}
	    }
      if(!checkRc){
        alert("请选择记录！");
        return false;
      }
    }
     //getBatchCaseNo();
      fm.fmtransact.value = "DELETE||MAIN";
      var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      fm.action ="./CaseDeleteSave.jsp";
      fm.submit(); //提交
  } else {
    alert("您取消了删除操作！");
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow) {
  if (cShow=="true") {
    cDiv.style.display="";
  } else {
    cDiv.style.display="none";
  }
}

function CaseListUpload() {
  //只允许在登记状态导入
  var strSQL = "select rgtno from llregister where rgtstate<>'01' and rgtno='"+fm.RgtNo.value+"'";
  var	arrResult = easyExecSql(strSQL);
  if(arrResult){
    alert("该批次已经申请完毕");
    return;
  }  
  
  if ( ImportState =="Succ") {
    if ( !confirm("确定您要再次磁盘投保吗?") )
      return false ;
  }
  var i = 0;
  getImportPath();
  ImportFile = fm.all('FileName').value;
  var tprtno = ImportFile;
  if ( tprtno.indexOf("\\")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("\\")+1);
  if ( tprtno.indexOf("/")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("/")+1);
  if ( tprtno.indexOf("_")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("_"));
  if ( tprtno.indexOf(".")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("."));

    var showStr="正在上载数据……";
    var urlStr="./ImportRateInfoInput.jsp?RgtNo="+fm.RgtNo.value ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LLBatchRImportSave.jsp?RgtNo="+fm.RgtNo.value+"&ImportPath="+ImportPath;
    fm.submit(); //提交
}

function getImportPath () {

  var strSQL = "";
  strSQL = "select SysvarValue from ldsysvar where sysvar ='LPXmlPath'";
  var arr = easyExecSql(strSQL);

  ImportPath = arr[0][0];
}

function AfterInput() {
  if ( ImportState=="Importing" ) {
    return false;
  }
}

function beforeAfterInput() {
  if ( ImportState=="Importing" ) {
    alert("磁盘投保尚未完成，请不要离开!");
    return false;
  }
}


function CalClaim()
{
  var strSQL = "select rgtno from llregister where rgtstate<>'01' and rgtno='"+fm.RgtNo.value+"'";
  var	arrResult = easyExecSql(strSQL);
  if(arrResult){
    alert("该批次已经申请完毕");
    return;
  } 
	fm.fmtransact.value="cal";
	var showStr="理算过程将会在后台处理,预计将会处理几分钟,此页面可关闭";
	var urlStr="./CalRateInfoInput.jsp?RgtNo="+fm.RgtNo.value ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./BatchClaimCal.jsp?RgtNo="+fm.RgtNo.value;
	fm.submit();
}

function queryGrpCaseGrid()
{
	var jsRgtNo = fm.all('RgtNo').value;
	if(jsRgtNo == null || jsRgtNo == "") 
		return;
	var strSql = "SELECT C.CaseNo,C.IDNo, C.CustomerName,c.CustomerNo, C.RgtDate " 
	           + " FROM LLCase C " + " WHERE C.RGTNO = '" + jsRgtNo 
	           + "'" + getWherePart("C.OtherIDNo","SecurityNo")
				     + getWherePart("C.CustomerNo","CustomerNo")
				     +getWherePart("C.CustomerName","CustomerName")
	  	       + " ORDER BY C.CASENO ";
	  	      
	  turnPage1.queryModal(strSql,GrpCaseGrid);
}

function queryList(){
  var strSql = "select peoples2 from lcgrpcont where grpcontno = '"+fm.GrpContNo.value+"'";
  var arr = easyExecSql(strSql);
  if(arr)
    fm.SumInsured.value = arr[0][0];
  strSql = "select count(CASENO) from llcase where RGTNO = '"+fm.RgtNo.value+"'";
  var crr = easyExecSql(strSql);
  if(crr)
    fm.SuccInsured.value = crr[0][0];
  fm.HoldInsured.value = fm.AppPeople.value-fm.SuccInsured.value;
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function QueryErrList()
{
  initDiskErrQueryGrid();
  // 书写SQL语句
  var strSql = "select c.caseno,c.IDNo,c.CUSTOMERNAME,c.CUSTOMERNO,c.otheridno,c.customersex,a.accdate,a.accplace,a.accdesc "
             + " from llcase c,llcaserela b,llsubreport a where b.caseno=c.caseno "
             + " and b.subrptno=a.subrptno and c.customername in "
             + " (select a from (select customername a,count(*) b from llcase "
             + " where rgtno = '"+fm.RgtNo.value+"' group by customername) as x where b>1) "
             + " and c.idno in (select a from (select idno a,count(*) b from llcase "
             + " where rgtno = '"+fm.RgtNo.value+"' group by idno) as x where b>1) "
             + " and c.customersex in (select a from (select customersex a,count(*) b from llcase "
             + " where rgtno = '"+fm.RgtNo.value+"' group by customersex) as x where b>1) "
             + " and c.RgtNo = '"+fm.RgtNo.value+"' order by customerno ";

//alert(strSql);
		turnPage2.queryModal(strSql, DiskErrQueryGrid);
}

function showErrList()
{
	var row = DiskErrQueryGrid.getSelNo()-1;
	if(row>=0)
	{
		fm.CustomerNo.value = DiskErrQueryGrid.getRowColData(row,4);
		queryGrpCaseGrid();
	}
}

function PrtErrList(){
  getImportPath();
  //#2169 批次导入前后台数据同步问题方案
  var arrResult = new Array();
  var strSQL="select rgtstate from llregister where rgtno='"+fm.RgtNo.value+"' with ur";
  arrResult=easyExecSql(strSQL);
  if(arrResult != null){
  		var tRgtState=arrResult[0][0];
  		if(tRgtState=='06'){
  			alert("批次号:"+fm.RgtNo.value+"还在处理中，请稍候再处理该批次的案件!");
  			return ;
  		}
  }
  var newWindow = OpenWindowNew("./ErrListPrt.jsp?RgtNo=" +fm.RgtNo.value+"&ImportPath="+ImportPath,"ClaimDetailPrint","left");
}

function getBatchCaseNo(){
  var CaseCount=DiskErrQueryGrid.mulLineCount;
  var count = 0;
  for (i=0;i<CaseCount;i++){
    if(DiskErrQueryGrid.getChkNo(i)==true){
      fm.BatchCaseNo.value += "'"+DiskErrQueryGrid.getRowColData(i,1)+"',";
    }
  }
  fm.BatchCaseNo.value += "''";
}

function queryOnKeyDown(){
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13")
  {
    queryGrpCaseGrid();
  }
}

function RgtFinish(){
  strSQL = " select AppPeoples,rgtstate from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
  arrResult = easyExecSql(strSQL);

  if(arrResult != null){
    if ( arrResult[0][1]=='06'){
      alert("批次号:"+fm.RgtNo.value+"还在处理中，请稍候再处理该批次的案件!");
      return;
    }
    if ( arrResult[0][1]!='01' && arrResult[0][1]!='07'){
      alert("团体案件状态不允许该操作！");
      return;
    }
    var AppPeoples;
    try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
  }
  else{
    alert("团体案件信息查询失败！");
    return;
  }


  strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null){
    var RealPeoples;
    try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
  }
  strSQL = " select customername,idno from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
  brr = easyExecSql(strSQL)
  if(brr){
    count = brr.length;
    for (i=0;i<count;i++){
      //556反洗钱黑名单客户的理赔监测功能
       /* var strblack=" select 1 from lcblacklist where trim(name)='"+brr[i][0]+"' with ur";
        var crrblack=easyExecSql(strblack);
        if(crrblack){
          alert(brr[i][0]+"客户为黑名单客户，须请示上级处理。");
           return false ;
       }*/
    	//#2684关于理赔环节黑名单监测规则修改的需求
    	var checkName1 = brr[i][0];
    	var checkId1 = brr[i][1];
       if(!checkBlacklist(checkName1,checkId1)){
       		return false;
       }
    }
  }
  if ( RealPeoples < AppPeoples ){
    if(confirm("团体申请下个人申请尚未全部申请完毕！" + "团体申请人数：" + AppPeoples + "实际申请人数：" + RealPeoples+ "\n您确认要结束本团体批次吗?")){
      fm.realpeoples.value=RealPeoples;
      alert("本批次申请人数为："+RealPeoples+"人")
    }
    else{
      return;
    }
  }
     //新增校验，对于赔付正在进行结余返还操作以及已完成结余返还项目的保单，在理算确认时校验并阻断
     //add by GY 2013-1-8
     //1.先根据页面上的批次号查询该批次号对应的grpcontno
     //var GrpContSQL="select distinct grpcontno from llclaimdetail where rgtno='"+fm.all('RgtNo').value+"' and grpcontno not in ('00149574000001','00092763000002','00149574000002')";
     //add by lyc 2014-6-11 改为配置
     var GrpContSQL="select distinct grpcontno from llclaimdetail where rgtno='"+fm.all('RgtNo').value+"' and grpcontno not in (select code from ldcode where codetype='lp_jyfh_pass')"; 
	 var mGrpContNo = easyExecSql(GrpContSQL);     
	 //2.根据grpcontno查询该单是否正在进行结余返回操作
     var CheckBQSQL="select 1 from lpgrpedoritem where grpcontno='"+mGrpContNo+"' and edortype = 'BJ'";
     var CheckBQResult = easyExecSql(CheckBQSQL);
     //3.判断上述sql是否存在数据，是阻断否通过
     if(CheckBQResult)
       {
        alert("该保单"+mGrpContNo+"正在进行结余返还操作或者已完成结余返还项目，不能理算确认");
        return false;
       }

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action = "./LLGrpRegisterSave.jsp?RgtNo="+fm.RgtNo.value+"&operate=UPDATE||MAIN&realpeoples="+RealPeoples;
  fm.submit();
}

function openAffix(){
  var varSrc ="";
  pathStr="./LLGrpAffixMain.jsp?RgtNo="+fm.RgtNo.value+"&LoadFlag=1&LoadC="+fm.LoadC.value;
  showInfo = OpenWindowNew(pathStr,"","middle");
}

//#2684关于理赔环节黑名单监测规则修改的需求 added by zqs
function checkBlacklist(checkName,checkId){
	//需要验证的证件号与人名都为空
	if((checkId==""||checkId==null)&&(checkName==""||checkName==null)){
		return true;
	}
	//需要验证的证件号为空
	if(checkId==""||checkId==null){
		var strblack="select 1 from lcblacklist where trim(name)in('"+checkName+"') and (idno='' or idno is null) with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   	   	  		//alert("若“证件号码”在黑名单库中为空");
   	   	  		if(!confirm("客户"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      		return false;
   	   	    	}
		}
		return true;
	}
	//需要验证的姓名为空
	if(checkName==""||checkName==null){
		var strblack=" select 1 from lcblacklist where trim(idno)in('"+checkId+"')with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   			//alert("姓名”不匹配为空，“证件号码”匹配");
   			if(!confirm("客户为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		return true;
	}
	
	//1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
	var strblack=" select name,idno from lcblacklist where trim(name)in('"+checkName+"') or trim(idno)in('"+checkId+"') with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   		var strblack1=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and trim(idno)in('"+checkId+"') with ur";
   		var arrblack1=easyExecSql(strblack1);
   		if(arrblack1){
   			alert("客户"+checkName+"为黑名单客户");
   	   	  	return false;
   		}
   		var strblack2=" select 1 from lcblacklist where trim(idno)in('"+checkId+"') with ur";
   		var arrblack2=easyExecSql(strblack2);
   		if(arrblack2){
   			if(!confirm("客户"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		var strblack3=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and (trim(idno)in('') or idno is null) with ur";
   		var arrblack3=easyExecSql(strblack3);
   		if(arrblack3){
   			if(!confirm("客户"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
     }
     return true;
}
