<%
//程序名称：PayAffirmInit.jsp
//程序功能：给付确认的初始化
//创建日期：
//创建人  ：刘岩松
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%

%>

<script language="JavaScript">
	function initInpBox()
	{
		try
		{
			fm.all('CaseNo1').value = "<%=request.getParameter("CaseNo")%>";
			strSQL="select count(*) from llsurvey where otherno='"+"<%=request.getParameter("CaseNo")%>"+"'";
			var arr=easyExecSql(strSQL);
			fm.all('SerialNo').value=arr[0][0];
			strSQL="SELECT CONTENT FROM LLSURVEY"
			<%GlobalInput mG = new GlobalInput();
			mG=(GlobalInput)session.getValue("GI");
			%>
			fm.all('Surveyer').value = "<%=mG.Operator%>";

			<% String CustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK");
			%>
		}
		catch(ex)
		{
			alter("在LLRgtSurveyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initForm()
	{
		try
		{
			initInpBox();
			initSurveyGrid();
			QueryGrid()
		}
catch(re)
		{
			alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}

function initSurveyGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="0px";
    iArray[0][2]=10;
    iArray[0][3]=1;
      
    iArray[1]=new Array("调查号","120px","0","0");
    iArray[2]=new Array("提调人","80px","0","0");
    iArray[3]=new Array("调查类型","80px","0","0");
    iArray[4]=new Array("指定调查员","80px","0","2");
    iArray[5]=new Array("上交总公司","80px","0","2");
    SurveyGrid = new MulLineEnter("fm","SurveyGrid");
    SurveyGrid.mulLineCount =3;
    SurveyGrid.displayTitle = 1;
    SurveyGrid.locked = 1;
    SurveyGrid.canSel =1;
    SurveyGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    SurveyGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    SurveyGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>