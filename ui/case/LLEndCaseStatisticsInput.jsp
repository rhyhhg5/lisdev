   <% 
  //Name:LLendcaseStatisticsInput.jsp
  //Function:结案案件时间分布统计报表
  //Date:2011-3-2 11:27
  //Author:Liqing
   %>

    <%@include file="../common/jsp/UsrCheck.jsp"%>


    <%@page contentType="text/html;charset=GBK" %>
    <%@include file="../common/jsp/UsrCheck.jsp"%>
    <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
    <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import = "com.sinosoft.utility.*"%>
    <%@page import="java.util.*"%> 
     <html>
    <head>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLEndCaseStatisticsInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
       <%
     GlobalInput tG1 = (GlobalInput)session.getValue("GI");
     String CurrentDate= PubFun.getCurrentDate();   
     String AheadDays="-90";
     FDate tD=new FDate();
     Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
     FDate fdate = new FDate();
     String afterdate = fdate.getString( AfterDate );
     %>
    <script>
	function initForm(){
	fm.ECaseDateS.value="<%=afterdate%>";
    fm.ECaseDateE.value="<%=CurrentDate%>";
    fm.ManageCom.value="<%=tG1.ManageCom%>";
    var arrResult=easyExecSql("select Name from ldcom where comcode='<%=tG1.ManageCom%>'");
    fm.ComName.value=arrResult[0][0];
    }
   </script>
    </head>
	
	<body onload="initForm();" >
	<form method=post name=fm target="fraSubmit">
	<table class= common border=0 width=100%>
	  <tr class=common>
		 <td class= titleImg>
			结案案件时间分布统计表
		 </td>
	  </tr>
	 </table>
	
	 <table class=common border=0 width=100%>
      <tr  class= common>
         <td  class= title>管理机构</td>
         <td  class= input> <input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>   
         <td  class=title> 统计时间类型 </td> 
         <td  class= input> <Input class= "codeNo" name=DateType CodeData="0|2^Y|年^YM|年月" ondblclick="return showCodeListEx('dateTypeValue',[this,DateTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DateTypeValue',[this,DateTypeName],[0,1]);" ><Input class= "codeName" name=DateTypeName ></TD>   
         
       </tr>
       <tr class=common> 
         <td  class= title>结案起期</td>
         <td class= input> <input name=ECaseDateS class='coolDatePicker' dateFormat='short' verify="结案起期|notnull&Date" elementtype=nacessary> </TD> 
         <td  class= title>结案止期</td>
         <td class= input> <input name=ECaseDateE class='coolDatePicker' dateFormat='short' verify="结案止期|notnull&Date" elementtype=nacessary> </TD> 
       </tr> 
       <tr class=common>
         <td  class= title>受理起期</td>
         <td  class= input> <input name=RgtDateS class='coolDatePicker' dateFormat='short' verify="受理起期|notnull&Date" elementtype=nacessary> </TD>     
         <td  class= title>受理止期</td>
         <td  class= input> <Input name=RgtDateE class='coolDatePicker' dateFormat='short' verify="受理止期|notnull&Date" elementtype=nacessary> </TD> 
        </tr>
        <tr class=common>
         <td  class= title>事件起期</td>
         <td  class= input> <input name=AccDateS class='coolDatePicker' dateFormat='short' verify="事件起期|notnull&Date" elementtype=nacessary> </TD>     
         <td  class= title>事件止期</td>
         <td  class= input> <input name=AccDateE  class='coolDatePicker' dateFormat='short' verify="事件止期|notnull&Date" elementtype=nacessary> </TD> 
        </tr>
   </table>
        <hr>
    <table class=common>
    	<tr class=common>
          <Input type =button class=cssButton value="打  印" onclick="Print()">	
			</tr>
		</table>
		 <input type= hidden name=StartDate>
		<input type= hidden name=EndDate>
		<input type= hidden name=fmtransact>
	    </form>
	    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   </body>

</html>