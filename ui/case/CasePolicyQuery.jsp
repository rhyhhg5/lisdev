<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="CasePolicyQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CasePolicyQueryInit.jsp"%>
  <title>立案查询 </title>
</head>



<body  onload="initForm();" >

  <form action="./CasePolicyQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 报案信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCasePolicy1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divCasePolicy1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            立案号
          </TD>
          <TD  class= input>
            <Input class= common name=RgtNo >
          </TD>
          
          <TD  class= title>
            事故者姓名
          </TD>
          <TD  class= input>
            <Input class= common name=CustomerName >
          </TD>
          
          
         </tr>
         <TR>
          <TD  class= title>
            号码类型
          </TD>
          <TD  class= input>
            <Input class= code name=RgtObj verify="号码类型|NOTNULL" CodeData="0|3^0|团体保单号^1|个人保单号^2|客户号" ondblClick="showCodeListEx('RgtObj',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('RgtObj',[this],[0,1,2]);" >
          </TD>
        
          <TD  class= title>
            号码
          </TD>
          <TD  class= input>
            <Input class= common name=RgtObjNo >
          </TD>
        </TR>
        
      </table>
    </Div>
          <input class=common type=button value="查询" onclick="submitForm();">  
      	  <input class=common type=button value="返回" onclick="returnParent();">  
      			
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCasePolicy2);">
    		</td>
    		<td class= titleImg>
    			 立案信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divCasePolicy2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanCasePolicyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
	<INPUT VALUE="首  页" TYPE=button class=common onclick="getFirstPage();"> 
      	<INPUT VALUE="上一页" TYPE=button class=common onclick="getPreviousPage();"> 					
      	<INPUT VALUE="下一页" TYPE=button class=common onclick="getNextPage();"> 
      	<INPUT VALUE="尾  页" TYPE=button class=common onclick="getLastPage();"> 					
  	 
  	  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
