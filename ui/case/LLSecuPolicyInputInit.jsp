<%
//Name:LLSecuPolicyInputInit.jsp
//function：社保政策配置初始化页面
//author:Xx
//Date:2006-08-21
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
  function initInpBox(){
    try{

    }
    catch(ex){
      alter("LLSecuPolicyInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("LLSecuPolicyInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm(){
    try{
      initInpBox();
      initGetLimitGrid();
      initGetRateGrid();
    }
    catch(re){
      alter("LLSecuPolicyInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
  }

  //立案附件信息
  function initGetLimitGrid(){
    var iArray = new Array();

    try{
      iArray[0]=new Array("序号","30px","0",0);
      iArray[1]=new Array("医院级别","80px","0",2);
      iArray[1][10]="HosGrade";
      iArray[1][11]="0|3^1|一级^2|二级^3|三级"
      iArray[1][12]="1|2"
      iArray[1][13]="1|0"
      iArray[2]=new Array("HosGrade","80px","20",3);
      iArray[3]=new Array("城区属性","80px","20",2);
      iArray[3][10]="UrbanOption";
      iArray[3][11]="0|2^0|非城区^1|城区"
      iArray[3][12]="3|4"
      iArray[3][13]="1|0"
      iArray[4]=new Array("UrbanOption","80px","20",3);
      iArray[5]=new Array("人员类型","80px","20",2);
      iArray[5][10]="InsuredStat";
      iArray[5][11]="0|2^1|在职^2|退休"
      iArray[5][12]="5|6"
      iArray[5][13]="1|0"
      iArray[6]=new Array("InsuredStat","80px","20",3);
      iArray[7]=new Array("住院次数","80px","20",2);
      iArray[7][10]="IsFirstInHos";
      iArray[7][11]="0|2^1|第一次住院^2|第二次住院^9|其它"
      iArray[7][12]="7|8"
      iArray[7][13]="1|0"
      iArray[8]=new Array("IsFirst","80px","20",3);
      iArray[9]=new Array("年龄下限","80px","20",1);
      iArray[10]=new Array("年龄上限","80px","20",1);
      iArray[11]=new Array("起付线","80px","20",1);
      if(!fm.cHosGrade.checked){
        iArray[1][3]=3
      }
      if(!fm.cUrbanOption.checked){
        iArray[3][3]=3
      }
      if(!fm.cInsuredStat.checked){
        iArray[5][3]=3
      }
      if(!fm.cIsFirst.checked){
        iArray[7][3]=3
      }
      if(!fm.cAge.checked){
        iArray[9][3]=3
        iArray[10][3]=3
      }

      GetLimitGrid = new MulLineEnter( "fm" , "GetLimitGrid" );
      GetLimitGrid.mulLineCount = 1;
      GetLimitGrid.displayTitle = 1;
      GetLimitGrid.canChk = 0;
      GetLimitGrid.hiddenPlus=0;
      GetLimitGrid.hiddenSubtraction=0;
      GetLimitGrid.loadMulLine(iArray);

    }
    catch(ex)
    {
      alert(ex);
    }
  }

  function initGetRateGrid(){
    var iArray = new Array();

    try{
      iArray[0]=new Array("序号","30px","0",0);
      iArray[1]=new Array("医院级别","80px","0",2);
      iArray[1][10]="HosGrade";
      iArray[1][11]="0|3^1|一级^2|二级^3|三级"
      iArray[1][12]="1|2"
      iArray[1][13]="1|0"
      iArray[2]=new Array("HosGrade","80px","20",3);
      iArray[3]=new Array("城区属性","80px","20",2);
      iArray[3][10]="UrbanOption";
      iArray[3][11]="0|2^0|非城区^1|城区"
      iArray[3][12]="3|4"
      iArray[3][13]="1|0"
      iArray[4]=new Array("UrbanOption","80px","20",3);
      iArray[5]=new Array("人员类型","80px","20",2);
      iArray[5][10]="InsuredStat";
      iArray[5][11]="0|2^1|在职^2|退休"
      iArray[5][12]="5|6"
      iArray[5][13]="1|0"
      iArray[6]=new Array("InsuredStat","80px","20",3);
      iArray[7]=new Array("住院次数","80px","20",2);
      iArray[7][10]="IsFirstInHos";
      iArray[7][11]="0|2^1|第一次住院^2|第二次住院^9|其它"
      iArray[7][12]="7|8"
      iArray[7][13]="1|0"
      iArray[8]=new Array("IsFirst","80px","20",3);
      iArray[9]=new Array("年龄下限","80px","20",1);
      iArray[10]=new Array("年龄上限","80px","20",1);
      iArray[11]=new Array("金额分段下限","80px","20",1);
      iArray[12]=new Array("金额分段上限","80px","20",1);
      iArray[13]=new Array("给付比例","80px","20",1);
     if(!fm.dHosGrade.checked){
       iArray[1][3]=3
     }
     if(!fm.dUrbanOption.checked){
       iArray[3][3]=3
     }
     if(!fm.dInsuredStat.checked){
       iArray[5][3]=3
     }
     if(!fm.dIsFirst.checked){
       iArray[7][3]=3
     }
     if(!fm.dAge.checked){
       iArray[9][3]=3
       iArray[10][3]=3
     }

      GetRateGrid = new MulLineEnter( "fm" , "GetRateGrid" );
      GetRateGrid.mulLineCount = 1;
      GetRateGrid.displayTitle = 1;
      GetRateGrid.canChk = 0;
      GetRateGrid.hiddenPlus=0;
      GetRateGrid.hiddenSubtraction=0;
      GetRateGrid.loadMulLine(iArray);

    }
    catch(ex)
    {
      alert(ex);
    }
  }

  function getCon()
  {
    //return " and affixtypecode=#"+fm.all('codetype').value+"#";


    alert(fm.all("codetype").value);
    return "#"+fm.all('codetype').value+"#"
  }
  
  


</script>