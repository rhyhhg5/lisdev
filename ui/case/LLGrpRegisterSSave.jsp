<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLAffixSave.jsp
//程序功能：
//创建日期：2005-02-5 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
  CErrors tError = null;
  GrpRegisterBL tGrpRegisterBL = new GrpRegisterBL();
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  VData tVData = new VData();
  
  //输出参数
  String FlagStr = "";
  String Content = "";

  String strOperate = request.getParameter("operate");
  
  System.out.println("==== strOperate == " + strOperate);
  LLRegisterSchema mLLRegisterSchema = new LLRegisterSchema();
  
System.out.println("SimpleCase:" + request.getParameterValues("SimpleCase")+";EasyCase:"+request.getParameterValues("EasyCase")+";FenCase:"+request.getParameterValues("FenCase")+";HeadCase:"+request.getParameterValues("HeadCase"));
    if (request.getParameterValues("SimpleCase") != null){
      tLLRegisterSchema.setRptFlag("1");
	}
	else if (request.getParameterValues("EasyCase") != null) {
	  tLLRegisterSchema.setRptFlag("2");
	}
	else if (request.getParameterValues("FenCase") != null) {
	  tLLRegisterSchema.setRptFlag("3");
	}
	else if (request.getParameterValues("HeadCase") != null) {
	  tLLRegisterSchema.setRptFlag("4");
	}else {
	  tLLRegisterSchema.setRptFlag("0");
	}
  
  System.out.println("====  " + tLLRegisterSchema.getRptFlag());
	if (request.getParameterValues("PrePaidFlag") != null) {
		tLLRegisterSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
	}
  
  if (strOperate.equals("INSERT||MAIN")) 
  {
	  tLLRegisterSchema.setRgtObj("0");			//号码类型
	  tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));	//申请类型
	  tLLRegisterSchema.setRgtType(request.getParameter("RgtType"));	//申请类型
	  tLLRegisterSchema.setRgtObjNo(request.getParameter("GrpContNo"));	//团单合同号
	  tLLRegisterSchema.setCustomerNo(request.getParameter("CustomerNo"));  //团体客户号
	  tLLRegisterSchema.setGrpName(request.getParameter("GrpName"));  	//单位名称
	  tLLRegisterSchema.setAppAmnt(request.getParameter("AppAmnt"));		//预计申请金额
	  tLLRegisterSchema.setAppPeoples(request.getParameter("AppPeoples"));		//申请人数
	  tLLRegisterSchema.setInputPeoples(request.getParameter("AppPeoples"));		//录入申请人数
	  tLLRegisterSchema.setAccidentCourse(request.getParameter("GrpRemark"));
	  tLLRegisterSchema.setTogetherFlag(request.getParameter("TogetherFlag"));	//统一给付标志
	  tLLRegisterSchema.setRgtClass("1");
	  tLLRegisterSchema.setPostCode(request.getParameter("PostCode"));			//邮政编码，对应表中立案人/申请人邮政编码“申请人邮政编码”
	  tLLRegisterSchema.setAppDate(request.getParameter("AppDate"));
	  System.out.println(tLLRegisterSchema.getAppDate()+"==$%^$%^&&======&&&&&&(((())))==========");
	  tLLRegisterSchema.setRgtantAddress(request.getParameter("RgtantAddress"));	//联系地址，对应表中“申请人地址”
	  tLLRegisterSchema.setRgtantPhone(request.getParameter("RgtantPhone"));	//联系电话，对应表中是“申请人电话”
	  tLLRegisterSchema.setRgtantName(request.getParameter("RgtantName"));		//联系人，对应表中是“申请人电话”  
	  tLLRegisterSchema.setBankCode(request.getParameter("BankCode"));
	  tLLRegisterSchema.setBankAccNo(request.getParameter("BankAccNo"));
	  tLLRegisterSchema.setAccName(request.getParameter("AccName")); 	  
	  tLLRegisterSchema.setIDType(request.getParameter("IDType")); 
	  tLLRegisterSchema.setIDNo(request.getParameter("IDNo")); 
	  tLLRegisterSchema.setGetMode(request.getParameter("CaseGetMode")); 
	  tLLRegisterSchema.setCaseGetMode(request.getParameter("CaseGetMode")); 
	  tLLRegisterSchema.setRemark(request.getParameter("Remark"));
    //tLLRegisterSchema.setApplyerType(request.getParameter("ApplyerType"));
      
    //2877
      tLLRegisterSchema.setIDStartDate(request.getParameter("IDStartDate"));
      tLLRegisterSchema.setIDEndDate(request.getParameter("IDEndDate"));

    tLLRegisterSchema.setRgtantMobile(request.getParameter("Mobile"));
    tLLRegisterSchema.setEmail(request.getParameter("Email"));
	  
	  
	   try
	   {		   
		   tVData.add(tLLRegisterSchema);
		   tVData.add(tG);		   
		  tGrpRegisterBL.submitData(tVData,strOperate);
	   }
	 catch(Exception ex)
	    {
	      Content = "保存失败，原因是:" + ex.toString();
	      System.out.println("aaaa"+ex.toString());
	      FlagStr = "Fail";
	    }
	    if (FlagStr=="")
	    {
		    tError = tGrpRegisterBL.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="保存成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tGrpRegisterBL.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "保存失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
	     }	     
	    mLLRegisterSchema.setSchema(( LLRegisterSchema )tVData.getObjectByObjectName( "LLRegisterSchema", 0 ));
  }		

  if (strOperate.equals("UPDATE||MAIN"))
  {
      tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
      tLLRegisterSchema.setAppPeoples(request.getParameter("realpeoples"));
      System.out.println("团体申请人数："+request.getParameter("realpeoples"));
      tLLRegisterSchema.setRgtState("02");  
      
	try
	{		   
		tVData.add(tLLRegisterSchema);
		tVData.add(tG);		   
		tGrpRegisterBL.submitData(tVData,strOperate);
	}
	catch(Exception ex)
	{
	      Content = "保存失败，原因是:" + ex.toString();
	      System.out.println("aaaa"+ex.toString());
	      FlagStr = "Fail";
	}
	if (FlagStr=="")
	{
		    tError = tGrpRegisterBL.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="保存成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tGrpRegisterBL.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "保存失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
	}      
  }
  %>

		   <script language="javascript">
		   if (parent.fraInterface.fm.all('operate').value == "INSERT||MAIN")
		   {
		    parent.fraInterface.fm.all("RgtNo").value = "<%=mLLRegisterSchema.getRgtNo()%>";
		    parent.fraInterface.fm.all("Handler").value = "<%=mLLRegisterSchema.getHandler1()%>";
		    parent.fraInterface.fm.all("ModifyDate").value = "<%=mLLRegisterSchema.getRgtDate()%>";
		   }
		   </script>
	<%=Content%> 		

   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
   
   
   
 