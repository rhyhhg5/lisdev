<%@page language="java" %><%@
page import="java.util.*" %><%@
page import="java.io.*" %><%@
page import="java.net.*" %><%@
page import="com.f1j.swing.JBook"%><%@
page import="com.sinosoft.xreport.util.*"%><%@
page contentType="text/plain;charset=gb2312" %><%@
include file="init.jsp"%><%
    /////////////////////////下载文件///////////////////////
    try
    {
        /**读取参数*/
        Hashtable queryParameters = javax.servlet.http.HttpUtils.parseQueryString(request.getQueryString());
        String fileParameters[] = (String[])queryParameters.get("file");
        String strFileName=fileParameters[0];
        String typeParameters[] = (String[])queryParameters.get("type");
        String strType=typeParameters[0];

        String fileName="";
        String branch="";

        /**参数检验*/
        if(null==strType||"".equals(strType))
        {
            out.println("err|parameter type should not be null");
            return;
        }
        if(null==strFileName||"".equals(strFileName))
        {
            out.println("err|parameter file should not be null");
            return;
        }

        /**读取报表定义*/
        if(strType.equals("define"))
        {
            StringTokenizer token=new StringTokenizer(strFileName,SysConfig.REPORTJOINCHAR);
            if(token.hasMoreTokens())
                branch=token.nextElement().toString();
            fileName=SysConfig.FILEPATH
                    +"define"
                    +SysConfig.FILESEPARATOR
                    +branch
                    +SysConfig.FILESEPARATOR
                    +strFileName;
        }

        /**读取报表数据*/
        if(strType.equals("data"))
        {
            StringTokenizer token=new StringTokenizer(strFileName,SysConfig.REPORTJOINCHAR);
            if(token.hasMoreTokens())
                branch=token.nextElement().toString();
            fileName=SysConfig.FILEPATH
                    +"data"
                    +SysConfig.FILESEPARATOR
                    +branch
                    +SysConfig.FILESEPARATOR
                    +strFileName;
        }

        /**读取配置文件*/
        if(strType.equals("conf"))
        {
            fileName=SysConfig.FILEPATH
                    +"conf"
                    +SysConfig.FILESEPARATOR
                    +strFileName;
        }

        BufferedInputStream file=new BufferedInputStream(new FileInputStream(fileName));
        OutputStream os=response.getOutputStream();
        BufferedOutputStream bos=new BufferedOutputStream(os);

        byte[] b=new byte[4096];
        int intLength;
        String strFile="";
        while((intLength=file.read(b))>0)
        {
            bos.write(b,0,intLength);
        }

        bos.flush();
        bos.close();
        }
        catch(Exception ex)
        {
            out.println("err|"+ex.getMessage());
        }
%>