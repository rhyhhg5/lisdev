<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：LLPrepaidClaimInput.jsp
 //程序功能：预付赔款录入
 //创建日期：2010-11-25
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  String Operator = tG.Operator;
  String ManageCom = tG.ManageCom;
%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="LLPrepaidClaimSettle.js"></SCRIPT>
  <%@include file="LLPrepaidClaimSettleInit.jsp"%>
  <script language="javascript">
   var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";
   function initData(){
      fm.ManageCom.value="<%=ManageCom%>";
      fm.Operator.value="<%=Operator%>";
   }
  </script>
</head>
<body  onload="initForm();initElementtype();initData();">
<form action="./LLPrepaidClaimSettleSave.jsp" method=post name=fm target="fraSubmit">
<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLPrepaid);">
    </td>
    <td class= "titleImg">保单查询</td>
  </tr>
</table>
<div id="divLLPrepaid">
<table class="common" >
  <tr class="common">
    <td class="title">管理机构</td>
    <td class="input"><Input class="codeno" name="ManageCom" ondblclick="return showCodeList('comcode4',[this,ManageName],[0,1],null,'1','sign',1);" onkeyup="return showCodeListKey('comcode4',[this,ManageName],[0,1],null,'1','sign',1);" readonly="true" ><input class="codename" name="ManageName" elementtype="nacessary"></TD>
    <td class="title">投保单位名称</td>
    <td class="input"><input class="common" name="SGrpName" ></td>  
    <td class="title">团体保单号</td>
    <td class="input"><input class="common" name="SGrpContNo" ></td>   
  </tr>
</table>

<input value="查  询"  onclick="searchGrpCont()" class="cssButton" type="button" >
</div>
<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPrepaidGrp);">
    </td>
    <td class="titleImg" >保单信息</td>
  </tr>
</table>
<div id="divPrepaidGrp" >
<table class="common">
  <tr class="common">
    <td text-align: left colSpan=1>
     <span id="spanGrpContGrid" >
     </span> 
    </td>
  </tr>
</table>
</div>
<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPrepaidPol);">
    </td>
    <td class="titleImg" >险种信息</td>
  </tr>
</table>
<div id="divPrepaidPol">
<table class="common">
  <tr class="common">
    <td text-align: left colSpan=1>
     <span id="spanPrepaidClaimGrid" >
     </span> 
    </td>
  </tr>
</table>
</div>
<div id= "divSubmit" style= "display: ''" align= right>
<input value="打印还款通知书"  onclick="printBackGetNotice()" class="cssButton" type="button" >
</div>
<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPrepaidInfo);">
    </td>
    <td class="titleImg" >预付清账</td>
  </tr>
</table>
<div id="divPrepaidInfo">
<table class="common" >
  <tr class="common" >
    <td class="title">预付赔款号</td>
    <td class="input"><Input class="input" name="PrepaidNo" onkeydown="searchPrepaid()" ></td>  
    <td class="title">收费方式</td>
    <td class="input"><Input class="codeno" name=PayMode onclick="return showCodeList('paymode',[this,PayModeName],[0,1],null,str,'1');" onkeyup="return showCodeListKey('paymode',[this,PayModeName],[0,1],null,str,'1');" verify="给付方式|code:llgetmode&INT&notnull"><input class="codename" name="PayModeName" readonly="true" elementtype="nacessary" ></td>  
  </tr>
  <tr class="common">
    <td class="title">银行编码</td>
    <td class="input"><Input class="codeno" name="BankCode" onclick="return showCodeList('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" onkeyup="return showCodeListKey('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" ><Input class="codename" name="BankName" ></td>  
    <td class="title">签约银行</td>
    <td class="input"><input class="readonly" name="SendFlag" readonly="true"></td>
  </tr>
  <tr class="common" >
    <td class="title">银行账号</td>
    <td class="input"><Input class="input" name="AccNo" ></td>  
    <td class="title">账户名</td>
    <td class="input"><Input class="input" name="AccName" ></td> 
  </tr>
</table>
</div>
<div id= "divSubmit" style= "display: ''" align= right>
<input value="清账结算" name="Apply" onclick="submitForm()" class="cssButton" type="button" >
<input value="撤  销" name="Cancel" onclick="cancel()" class="cssButton" type="button" >
<input value="预付赔款明细"  onclick="searchPrepaidList()" class="cssButton" type="button" >
</div>

<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPrepaidClaim);">
    </td>
    <td class="titleImg" >预付赔款明细</td>
  </tr>
</table>
<div id="divPrepaidClaim">
<table class="common">
  <tr class="common">
    <td text-align: left colSpan=1>
     <span id="spanPrepaidClaimDetailGrid" >
     </span> 
    </td>
  </tr>
</table>
</div>

<Input type="hidden" class="common" name="Operator">
<Input type="hidden" class="common" name="GrpContNo">
<Input type="hidden" class="common" name="RgtType" value="2" >
<Input type="hidden" class="common" name="fmtransact">

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
