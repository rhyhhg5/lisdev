//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var Action;
var tRowNo=0;
var mSaveType="";
var turnPage = new turnPageClass(); 

   
function showFeeInfo()
{
    var varSrc ="";
    var newWindow = window.open("./FrameMainFAZL.jsp?Interface=ICaseCureCalInput.jsp"+varSrc,"ICaseCureInput",'top=0,left=0,width=800,height=550,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,status=0');
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  if (fm.ClmNo.value!="")
  {
  	alert("请保存修改信息");
  	return;
  }
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if (confirm("您确实想修改该记录吗?"))
  {
    //下面增加相应的代码
    if (fm.ClmNo.value=="")
    {
      alert("请用新增保存");
      return;
    }
    Action = "UPDATE";
    showDiv(operateButton,"false");
    showDiv(inputButton,"true");
  }
  else
  {
    alert("您取消了修改操作！");
  }
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  Action = "QUERY";
  //window.showModelessDialog("./ProposalQuery.jsp",window,"dialogWidth=15cm;dialogHeight=12cm");
  initForm();
  window.open("./FrameClaimQuery.jsp");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  alert("您不能执行删除操作！！！");
  return;

}
//Click事件，当点击“选择责任”按钮时触发该函数
function chooseDuty()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  if(cPolNo=="")
  {
     alert("您必须先录入投保单号才能修改该投保单的责任项。");
     return false
  }
//  showModalDialog("./FrameMain.jsp?Interface=ChooseDutyInput.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
  window.open("./FrameMain.jsp?Interface=ChooseDutyInput.jsp&PolNo="+cPolNo);
  return true
}

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  if(cPolNo=="")
  {
     alert("您必须先录入投保单号才能察看该投保单的责任项。");
     return false
  }
  showModalDialog("./ProposalDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
//  window.open("./ProposalDuty.jsp?PolNo="+cPolNo);
}

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  //showModalDialog("./ProposalFee.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");
  window.open("./ProposalFee.jsp?PolNo="+cPolNo);

}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function submitFormSurvery()
{
}

//分案收据明细
function showCaseReceipt()
{
    if (CaseGrid.getSelNo()==0)
    {
    		alert("请您选择一个分案！");
    		return;
    }
    if ((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
    {
    	alert("请您先执行保存操作");
    	return;
    }
    else
    {
  			var varSrc = "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
  			varSrc += "&CustomerNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
  			varSrc +="&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
  			var newWindow = window.open("./FrameMainClaimFASJ.jsp?Interface=ClaimCaseReceiptInput.jsp"+varSrc,"CaseReceiptInput",'width=700,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  	}
}

//分案诊疗明细
function showICaseCure()
{
  if (CaseGrid.getSelNo()==0)
  {
    alert("请选择一个分案!");
    return;
  }
  if ((fm.RgtNo.value=="")||(fm.RgtNo.value==null))
  {
    alert ("请您先执行保存操作!");
    return ;
  }
  else
  {
			var varSrc = "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
      varSrc += "&CustomerNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
      varSrc += "&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
      var newWindow = window.open("./ClaimFrameMainFAZL.jsp?Interface=ClaimICaseCureInput.jsp"+varSrc,"ICaseCureInput",'width=700,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  }
}


function showAccident()
{
  var varSrc ="";
  var newWindow = window.open("./FrameMainAccident.jsp?Interface=CaseAccidentInput.jsp"+varSrc,"CaseAccidentInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}


/***************************************/
function CalPay()
{
	fm.Opt.value = "cal";
   
   mSaveType = "calpay";
   
    var i = 0;
  	var showStr="正在赔付计算，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	fm.submit(); //提交
}
/***************************************/
function ReCalPay()
{
	fm.Opt.value = "recal";
   
   mSaveType = "calpay";
   
    var i = 0;
  	var showStr="正在赔付计算，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    if ( mSaveType=="calpay" || mSaveType=="savepay"  )
    {
    	queryCalPay();
    }
    //执行下一步操作
  }
  initTitle();
}

//页面查询
function queryCalPay(flag)
{
	var strSQL = "select ContNo,RiskCode,b.getdutyname,a.TabFeeMoney,a.DeclineAmnt,a.pregiveAmnt,"
	   + "a.ApproveAmnt,OutDutyAmnt,OverAmnt,a.GiveTypeDesc,a.GiveReasonDesc,a.ClaimMoney,a.StandPay,a.RealPay,"
	   +"a.polno,a.clmno,a.getdutycode,a.getdutykind,a.dutycode,a.GiveType,a.GiveReason,a.caserelano "
	   +" from LLClaimdetail a, LMDutyGetClm b where  a.caseno='"+ fm.CaseNo.value +"'"
	   +" and b.getdutycode = a.getdutycode and b.getdutykind=a.getdutykind"

	var arr = easyExecSql(strSQL );
	if (arr) displayMultiline( arr, ClaimDetailGrid);
	if ( arr!=null && arr.length>0 && (flag='init' || mSaveType=="savepay" ) )
	{ 
		ClaimDetailGrid.checkBoxAll ();
	}
	else
	{

	 	initClaimDetailGrid();	
	}
	
       var strSQL = " select a.ContNo,b.riskname,a.GetDutyKind,''," +
	            " a.StandPay,a.RealPay,a.polno,a.clmno," +
	            " a.GiveTypeDesc,a.GiveType,a.riskcode"+
	            " from LLClaimpolicy a, lmrisk b " +
	            " where caseno='"+ fm.CaseNo.value +"'" +
	            " and a.riskcode = b.riskcode " +
	            " order by a.polno";
	    
	var drr = easyExecSql(strSQL );
	if (drr) displayMultiline( drr, ClaimPolGrid);
	else
	{

	 	initClaimPolGrid();	  
	}
	
	var strSQL2 ="select sum(PreGiveAmnt),sum(SelfGiveAmnt),sum(RefuseAmnt),sum(claimmoney),sum(RealPay)  "
	 +" from llclaimdetail where clmno in (select clmno from LLClaimPolicy where  caseno='"+ fm.CaseNo.value +"')";
	var brr = easyExecSql(strSQL2 );
	if ( brr )
	{
		brr[0][0]==null||brr[0][0]=='null'?'0':fm.PreGiveAmnt.value  =brr[0][0];
		brr[0][1]==null||brr[0][1]=='null'?'0':fm.SelfGiveAmnt.value =brr[0][1];
		brr[0][2]==null||brr[0][2]=='null'?'0':fm.RefuseAmnt.value   =brr[0][2];
		brr[0][3]==null||brr[0][3]=='null'?'0':fm.StandPay.value     =brr[0][3];
		brr[0][4]==null||brr[0][4]=='null'?'0':fm.RealPay.value      =brr[0][4];
		
	
	 var strSQL3="select sum(RealHospDate) from llfeemain where   caseno='"+ fm.CaseNo.value +"'";
	 var crr = easyExecSql(strSQL3 );
	 if ( crr )
	 {
	 	fm.RealHospDate.value =crr[0][0];
	 }
	 var strSQL4="select RealPay,GiveTypeDesc from LLClaim where caseno='"+fm.CaseNo.value+"'";
	 var arr = easyExecSql(strSQL4);
	 if (arr!=null)
	 {
	 	arr[0][0]==null||arr[0][0]=='null'?'0':fm.AllGiveAmnt.value=arr[0][0];
	 	arr[0][1]==null||arr[0][1]=='null'?'0':fm.AllGiveType.value=arr[0][1];
	}
	
   }
   else
	{
		fm.PreGiveAmnt.value  = "";
		fm.SelfGiveAmnt.value = "";
		fm.RefuseAmnt.value = "";
		fm.StandPay.value= "";
		fm.RealPay.value= "";
		fm.RealHospDate.value = "";	
		fm.AllGiveAmnt.value = "";
		fm.AllGiveType.value = "";	
	}
   
}

//提交，保存按钮对应操作
function submitForm()
{
  if (confirm("您确实想保存该记录吗?"))
  {
    if (ClaimDetailGrid.mulLineCount==0)
    {
      alert("请作赔付明细计算");
      return;
    }

    var i = 0;
    var tRealPay = 0;
    var tCalPay=0;
    var tTempPolPay=0;
	  mSaveType="savepay" ;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //  showSubmitFrame(mDebug);
    fm.Opt.value = "save"
   
    fm.submit(); //提交

  }
  else
  {

    alert("您取消了修改操作！");
  }
}

function preDeal() 
{
    var caseNo = fm.all('CaseNo').value;
	var varSrc="&CaseNo="+ caseNo;
	parent.window.location = "./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+
	varSrc    
}
function nextDeal()
{
	var caseNo = fm.all('CaseNo').value;
	var varSrc = "&CaseNo=" + caseNo;
	parent.window.location = "./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + 
	varSrc;
}

function backDeal()
{
    top.close();
}

function CaseChange()
{
	
	initTitle();
	
	if (fm.RgtState.value == null ||
		fm.RgtState.value =="")
	{
		
		queryClear();
		
	}
	else
	{
		queryCalPay();
	}
	
}

function queryClear()
{
		initClaimDetailGrid();
		initClaimPolGrid();
		fm.PreGiveAmnt.value  = "";
		fm.SelfGiveAmnt.value = "";
		fm.RefuseAmnt.value = "";
		fm.StandPay.value= "";
		fm.RealPay.value= "";
		fm.RealHospDate.value = "";	
		fm.AllGiveAmnt.value = "";
		fm.AllGiveType.value = "";	
	
}