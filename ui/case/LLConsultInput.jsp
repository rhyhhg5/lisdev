<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：LiuYansong，陈海强
%>
 <%@page contentType="text/html;charset=GBK" %>
 <!--用户校验类-->
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLConsultInput.js"></SCRIPT>
   <%@include file="LLConsultInputInit.jsp"%>
   
 </head>

 <body  onload="initForm();" >
   <form action="./LLConsultSave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         咨询客户信息
         </TD>
       </TR>
      </table>
<!--
     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
     <Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerInfo);">
          </TD>
          <TD class= titleImg>
            客户列表
          </TD>
        </TR>
      </Table>

    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCustomerGrid" >
            </span>
          </TD>
        </TR>
		  </table>
    </Div>
    <hr  width="88%" align='left'>
    -->
    <Div  id= "divLLLLMainAskInput1" style= "display: ''">
 <table  class= common>
<TR  class= common8>
<TD  class= title8>客户号码</TD><TD  class= input8><Input class= common name="CustomerNo"></TD>
<TD  class= title8>客户名称</TD><TD  class= input8><Input class= common name="CustomerName"></TD>
<TD  class= title8>客户类型</TD><TD  class= input8><Input class= code8 name="CustomerType" ondblClick="showCodeList('casecustomer',[this],[0]);" onkeyup="showCodeListKeyEx('casecustomer',[this],[0]);"></TD>
</TR><TR  class= common8>
<TD  class= title8>是否咨询专家</TD><TD  class= input8><Input class= common name="ExpertFlag"></TD>
<TD  class= title8>专家编号</TD><TD  class= input8><Input class= common name="ExpertNo"></TD>
<TD  class= title8>专家姓名</TD><TD  class= input8><Input class= common name="ExpertName"></TD>
</TR>
<TR  class= common8>
<TD  class= title8>疾病代码</TD><TD  class= input8><Input class= code8 name="DiseaseCode" ondblclick="showCodeList('diseascode',[this],[0]);" onkeyup="showCodeListEx('diseascode',[this],[0]);"></TD>
<TD  class= title8>	期待回复日期</TD><TD  class= input8><Input class= common name="ExpRDate"></TD>
</TR>
<TR  class= common8>
	<TD  class= title8>咨询级别</TD><TD  class= input8><Input class= common name="AskGrade"></TD>
	<TD  class= title8>	咨询有效标志</TD><TD  class= input8><Input class= common name="AvaiFlag"></TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">咨询主题</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">	
		<textarea name="CSubject" cols="100%" rows="2" witdh=25% class="common"></textarea>
	</TD>
</TR>
<TR  class= common>
	<TD  class= title colspan="6">咨询内容</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="CContent" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>

<TR  class= common>
	<TD  class= input colspan="6">客户现状</TD>
	
</TR>
<TR  class= common>
	<TD  class= input colspan="6">	
		<textarea name="CustStatus" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>

<TR  class= common8>
	<Input type=hidden class= common name="PeplyState"></TD>
</TR>
</table>
<div align='right' style="display: 'none'">
<input class=cssButton type=button value="保 存" onclick="insertCust()">
<input class=cssButton type=button value="删 除" onclick="deleteCust()">
</div>
</DIV>
 
  <Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecondUWInfo);">
          </TD>
          <TD class= titleImg>
            关联事件
          </TD>
        </TR>
      </Table>

    <Div  id= "divSecondUWInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanSubReportGrid">
            </span>
          </TD>
        </TR>
		  </table>
	<input class=cssButton type=button value="关联查询" onclick="RelaQuery()">
	<input class=cssButton type=button value="删除关联" onclick="DelRela()">

    </Div>
    
       

  <!--隐藏域-->
            <input name ="fmtransact" type="hidden">
            <input name ="LogNo" type="hidden">
            <input name ="ConsultNo" type="hidden">   
            <input name ="AskType" type="hidden">   
  
 
 
  <!--Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLEventInput1);">
          </TD>
          <TD class= titleImg>
            事件信息
          </TD>
        </TR>
      </Table-->
      
       <Div  id= "divLLLLEventInput1" style= "display: 'none'">
     <table  class= common>
     <!--
<TR  class= common8>
<TD  class= title8>客户号码</TD><TD  class= input8><Input class= common name="CustomerNo"></TD>
<TD  class= title8>客户名称</TD><TD  class= input8><Input class= common name="CustomerName"></TD>
<TD  class= title8>客户类型</TD><TD  class= input8><Input class= code8 name="CustomerType"></TD>
</TR>
-->
<TR  class= common8>
<TD  class= title8>事故类型</TD><TD  class= input8><Input class= common name="AccidentType"></TD>
<TD  class= title8>发生日期</TD><TD  class= input8><Input class= common name="AccDate"></TD>
<TD  class= title8>终止日期</TD><TD  class= input8><Input class= common name="AccEndDate"></TD>
</TR>
<TR  class= common8>
<TD  class= title8>事故地点</TD><TD  class= input8><Input class= common name="AccPlace"></TD>


<TD  class= title8>医院代码</TD><TD  class= input8><Input class= common name="HospitalCode"></TD>
</TR><TR  class= common8>
<TD  class= title8>医院名称</TD><TD  class= input8><Input class= common name="HospitalName"></TD>

<TD  class= title8>入院日期</TD><TD  class= input8><Input class= common name="InHospitalDate"></TD>
<TD  class= title8>出院日期</TD><TD  class= input8><Input class= common name="OutHospitalDate"></TD>
</TR><TR  class= common8>
<TD  class= title8>重大事件标志</TD><TD  class= input8><Input class= code8 name="SeriousGrade" CodeData= "0|^1|重大事件^2|一般事件" ondblClick="showCodeListEx('SeriousGrade',[this],[0]);" onkeyup="showCodeListKeyEx('SeriousGrade',[this],[0]);"></TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">事件主题</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">	
		<textarea name="AccSubject" cols="100%" rows="2" witdh=25% class="common"></textarea>
	</TD>
</TR>
<TR  class= common>
	<TD  class= title colspan="6">事故描述</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="AccDesc" cols="100%" rows="3" witdh=25% class="common"></textarea>
	</TD>
</TR>


</table>
<div align='right' style="display: 'none'">

 <input class=cssButton type=button value="新增事件" onclick="EventSave()">
 
  
 </div> 
</DIV>
<div id = "AnswerInfo">
<Table>
        <TR class=common>
          <TD class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAnswerInput1);">
          </TD>
          <TD class= titleImg>
            回复信息
          </TD>
        </TR>
      </Table>
<table>
<div id='divAnswerInput1'>
<Table>
<TR  class= common>
	<TD  class= title colspan="6">回复信息</TD>
</TR>
<TR  class= common>
	<TD  class= input colspan="6">
	    <textarea name="Answer" cols="100%" rows="3" witdh=25% class="common"></textarea>
	    <input type=hidden name="SerialNo">
	</TD>
</TR>    
</table>
<div align='right'>
<input class=cssButton type=button value="保存回复" onclick="ReplySave()">
</div>
</Div>
</div>
     <!--隐藏域-->
     <Input type="hidden" class= common name="SubRptNo" >
     </form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>