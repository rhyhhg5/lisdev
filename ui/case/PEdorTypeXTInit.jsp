<%
//PEdorTypeXTInit.jsp
//程序功能：
//创建日期：2005-4-6 
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
GlobalInput tG = (GlobalInput)session.getValue("GI");
%>
<script language="JavaScript">  
//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divLPAppntGrpDetail.style.left=ex;
  	divLPAppntGrpDetail.style.top =ey;
   	detailQueryClick();
}                                    

function initForm()
{
	try
	{
		fm.Operator.value = "<%=tG.Operator%>";
		initInpBox(); 
		initQuery(); 
		initPolGrid();
		getPolInfo(fm.all('ContNo').value);
		initElementtype();
	}
	catch(re)
	{
		alert("PEdorTypeXTInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initQuery()
{	
	try
	{
		getCont();
	}
	catch(re)
	{
		alert("PEdorTypeXTInit.jsp-->getCont函数中发生异常:初始化界面错误!");
	}
}

function initPolGrid()
{                               
  var iArray = new Array();
    
    try
    {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=0;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="险种号";
      iArray[1][1]="0px";
      iArray[1][2]=100;
      iArray[1][3]=3;
      iArray[1][21]="polNo";
      
      iArray[2]=new Array();
      iArray[2][0]="险种序号";
      iArray[2][1]="35px";
      iArray[2][2]=100;
      iArray[2][3]=0;
      iArray[2][21]="riskSeqNo";

      iArray[3]=new Array();
      iArray[3][0]="险种号码";
      iArray[3][1]="0px";
      iArray[3][2]=100;
      iArray[3][3]=3;
      iArray[3][21]="riskCode";
      
      iArray[4]=new Array();
      iArray[4][0]="险种名称";
      iArray[4][1]="100px";
      iArray[4][2]=100;
      iArray[4][3]=0;
      iArray[4][21]="riskName";
      
      iArray[5]=new Array();
      iArray[5][0]="被保人号码";
      iArray[5][1]="45px";
      iArray[5][2]=100;
      iArray[5][3]=0;   
      iArray[5][21]="insuredNo";     
      
      iArray[6]=new Array();
      iArray[6][0]="被保人";
      iArray[6][1]="50px";
      iArray[6][2]=100;
      iArray[6][3]=0;
      iArray[6][21]="insuredName";
      
      iArray[7]=new Array();
      iArray[7][0]="保额";
      iArray[7][1]="40px";
      iArray[7][2]=100;
      iArray[7][3]=0;
      iArray[7][21]="amnt";
      
      iArray[8]=new Array();
      iArray[8][0]="档次";
      iArray[8][1]="20px";
      iArray[8][2]=100;
      iArray[8][3]=0;
      iArray[8][21]="mult";
      
      iArray[9]=new Array();
      iArray[9][0]="保费";
      iArray[9][1]="40px";
      iArray[9][2]=100;
      iArray[9][3]=0;
      iArray[9][21]="prem";
      
      iArray[10]=new Array();
      iArray[10][0]="生效日期";
      iArray[10][1]="50px";
      iArray[10][2]=100;
      iArray[10][3]=0;
      iArray[10][21]="cValiDate";
      
      iArray[11]=new Array();
      iArray[11][0]="保险满期日";
      iArray[11][1]="50px";
      iArray[11][2]=100;
      iArray[11][3]=0;
      iArray[11][21]="endDate";
      
      iArray[12]=new Array();
      iArray[12][0]="交至日";
      iArray[12][1]="50px";
      iArray[12][2]=100;
      iArray[12][3]=0;
      iArray[12][21]="payToDate";
      
      iArray[13]=new Array();
      iArray[13][0]="退保退费";
      iArray[13][1]="35px";
      iArray[13][2]=100;
      iArray[13][3]=0;
      iArray[13][21]="getMoney";
      
      iArray[14]=new Array();
      iArray[14][0]="协议解约金";
      iArray[14][1]="50px";
      iArray[14][2]=100;
      iArray[14][3]=1;
      iArray[14][21]="xtMoney";
      
      iArray[15]=new Array();
      iArray[15][0]="协议解约比例";
      iArray[15][1]="50px";
      iArray[15][2]=100;
      iArray[15][3]=1;
      iArray[15][21]="xtProportion";
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.canChk=1;
      PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      PolGrid.hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex);
    }
}
</script>