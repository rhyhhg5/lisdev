<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：LLPrepaidClaimUnderWriteInput.jsp
 //程序功能：预付赔款审批
 //创建日期：2010-11-30
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="LLPrepaidClaimUnderWrite.js"></SCRIPT>
  <%@include file="LLPrepaidClaimUnderWriteInit.jsp"%>
</head>
<script>

</script>
<body  onload="initForm();initElementtype();">
<form action="./LLPrepaidClaimUnderWriteSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQuer);">
    </td>
    <td class="titleImg" >预付赔款查询</td>
  </tr>
</table>
<div id="divQuer" align=left>
<table  class="common" >
  <tr class="common">
    <td class="title">管理机构</td>
    <td class="input"><Input class="codeno" name="ManageCom" verify="管理机构|code:comcode&NOTNULL&INT" 
    ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" 
    onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);" readonly="true" ><input class="codename" name="ComName" elementtype="nacessary"></td>
    <td class="title">投保单位名称</td><td class="input"><input class="common" name="GrpName" ></td>  
    <td class="title">团体保单号</td><td class="input"><input class="common" name="GrpContNo" ></td>  
  </tr>
  <tr class="common">
    <td class="title">预付赔款号</td><td class="input"><input class="common" name="PrepaidNo" ></td>  
    <td class="title">预付赔款状态</td><td class="input"><Input class="codeno" name=RgtState 
	ondblClick="showCodeList('llprepaidstate',[this,RgtStateName],[0,1]);"
	onkeyup="showCodeListKey('llprepaidstate',[this,RgtStateName],[0,1]);"><input class="codename" name=RgtStateName></td>  
    <td class="title">审批人</td><td class="input"><input class="codeno" name="Handler" 
    ondblclick="showCodeList('optname',[this,HandlerName],[0,1],null,1,'PrepaidFlag');" 
    onkeyup="showCodeListKey('optname',[this,HandlerName],[0,1],null,1,'PrepaidFlag');" ><input class="codename" name="HandlerName" ></td>  
  </tr>
  <tr class="common">
    <td class="title">预付类型</td>
    <td class="input"><input class="codeno" name="RgtType" CodeData="0|^1|申请类^2|回收类"
	ondblClick="showCodeListEx('RgtType',[this,RgtTypeName],[0,1]);"
	onkeyup="showCodeListKeyEx('RgtType',[this,RgtTypeName],[0,1]);" ><input class="codename" name="RgtTypeName" ></td>  
	<td class="title">申请起期</td>
	<td class="input"><input class="coolDatePicker"  dateFormat="short"  name=RgtDateS elementtype="nacessary"></td>
	<td class="title">申请止期</td>
	<td class="input"><input class="coolDatePicker"  dateFormat="short"  name=RgtDateE elementtype="nacessary"></td>  
  </tr>
</table>
</div>
<div align=left>
<input value="查  询"  onclick="easyQuery()" class="cssButton" type="button" >
</div>
<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPreClaim);">
    </td>
    <td class="titleImg" >预付赔款信息</td>
  </tr>
</table>
<div id="divPreClaim" align=left>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanPreClaimGrid" >
     </span> 
      </td>
   </tr>
</table>
</div>
	<Div id= "divPage" align=center>
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
<div align=right>
<input value="查看扫描件"  onclick="queryScan()" class="cssButton" type="button" >
</div>
<br>
<table>
  <tr>
    <td>
      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPreClaimDetail);">
    </td>
    <td class="titleImg" >预付赔款明细</td>
  </tr>
</table>
<div id="divPreClaimDetail" align=left>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanPreClaimDetailGrid" >
     </span> 
      </td>
   </tr>
</table>
</div>
<div align=right>
<input value="审批通过"  onclick="ClaimSPSD()" class="cssButton" type="button" >
<input value="审批回退"  onclick="ClaimBack()" class="cssButton" type="button" >
</div>
<br>
<input type=hidden name="Operator">
<input type=hidden name="fmtransact">
<input type=hidden name="tPrepaidNo">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
