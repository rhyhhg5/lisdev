<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2005-08-19
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.task.*"%>


<%
    boolean operFlag=true;
	String FlagStr = "";
	String Content = "";
	String letterInfo = "";
	
	
	String showToolBar = "false";  //函件上是否显示按钮
	if(request.getParameter("showToolBar") != null)
	{
		showToolBar = "true";
	}
	  
	GlobalInput tG = new GlobalInput();	
	tG = (GlobalInput)session.getValue("GI");
	
	//如果是察看函件
	String loadFlag = request.getParameter("loadFlag");
	
	if(loadFlag != null && loadFlag.equals("SHOWLETTER"))
	{
		String sql = "select LetterInfo from LGLetter "
					 + "where EdorAcceptNo='" + request.getParameter("edorAcceptNo") + "' "
					 + "	and SerialNumber='" + request.getParameter("serialNumber") + "' ";
		ExeSQL e = new ExeSQL();
		SSRS s = e.execSQL(sql);
		
		if(s.getMaxRow() > 0)
		{
			letterInfo = s.GetText(1, 1);
		}
	}
	//预览函件
	else
	{
		letterInfo = (String) session.getAttribute("letterInfo");
		session.removeAttribute("letterInfo");
		//转换为GB2312
		letterInfo = new String(letterInfo.getBytes("ISO-8859-1"),"GB2312");
	}
	
	LGLetterSchema schema = new LGLetterSchema();
	schema.setEdorAcceptNo(request.getParameter("edorAcceptNo"));
	schema.setLetterType(request.getParameter("letterType"));
	schema.setAddressNo(request.getParameter("addressNo"));
	schema.setBackFlag(request.getParameter("backFlag"));
	schema.setBackDate(request.getParameter("backDate"));
	schema.setSerialNumber(request.getParameter("serialNumber"));
	schema.setLetterInfo(letterInfo);
	
	VData tVData = new VData();
	VData mResult = new VData();
	CErrors mErrors = new CErrors();
	
    tVData.add(tG);
    tVData.add(schema);

    PrtLetterUI tPrtLetterUI = new PrtLetterUI();
	XmlExport txmlExport = new XmlExport();    
    if(!tPrtLetterUI.submitData(tVData,"PRINT"))
    {
       	operFlag = false;
       	Content = tPrtLetterUI.mErrors.getFirstError().toString();                 
    }
    else
    {    
		mResult = tPrtLetterUI.getResult();			
	  	txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  	
	  	if(txmlExport==null)
	  	{
	   		operFlag=false;
	   		Content="没有得到要显示的数据文件";	  
	  	}
	}
	
	LDSysVarDB tLDSysVarDB = new LDSysVarDB();
  tLDSysVarDB.setSysVar("VTSFilePath");
  tLDSysVarDB.getInfo();
  String vtsPath = tLDSysVarDB.getSysVarValue();
  if (vtsPath == null)
  {
    vtsPath = "vtsfile/";
  }
  
  String filePath = application.getRealPath("/").replace('\\', '/') + "/" + vtsPath;
  String fileName = "TASK" + FileQueue.getFileName()+".vts";
  String realPath = filePath + fileName;
	
	if (operFlag==true)
	{
	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);
//    session.putValue("PrintVts", dataStream);
//		session.putValue("PrintStream", txmlExport.getInputStream());
//		System.out.println("put session value");
//		response.sendRedirect("../f1print/GetF1Print.jsp?TemplateName=PrtLetterPreview.vts&showToolBar=" + showToolBar);
			AccessVtsFile.saveToFile(dataStream, realPath);
			response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath=" + realPath);
	}
	else
	{
    	FlagStr = "Fail";

%>
<html>
<script language="javascript">	
	alert("<%=Content%>");	
</script>
</html>
<%
  	}

%>