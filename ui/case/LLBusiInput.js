//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

function SaleClaim()
{
			fm.action="./LLBranchCaseReport.jsp" ;
			fm.op.value = "PRINT||SALECHN";
			fm.target = "SaleClaim";
			submitForm();
}

function SaleResult()
{
			fm.action="./LLBranchRsuReport.jsp" ;
			fm.op.value = "PRINT||SALECHN";
			fm.target = "SaleResult";
			submitForm();
}

function UWType()
{
			fm.action="./LLUWTypeReport.jsp" ;
			fm.target = "UWType";
			submitForm();
}

function UWYear()
{
			fm.action="./LLInsureYearRpt.jsp" ;
			fm.target = "UWYear";
			submitForm();
}

function UWDecision()
{
			fm.action="./LLUWDecideReport.jsp" ;
			fm.target = "UWDecision";
			submitForm();
}

function ChargeType()
{
			fm.action="./LLPayModeReport.jsp" ;
			fm.target = "ChargeType";
			submitForm();
}

function AppType()
{
			fm.action="./LLRgtTypeReport.jsp" ;
			fm.target = "AppType()";
			submitForm();
}

function Investigation()
{
			fm.action="./LLSurveyCaseReport.jsp" ;
			fm.target = "Investigation";
			submitForm();
}



//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
//	fm.target = "f1print";
//	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}

function download()
{
	if (verifyInput() == false)
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = 'download';
	fm.submit();
	showInfo.close();
}
