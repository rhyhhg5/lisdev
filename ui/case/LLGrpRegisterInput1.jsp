<html>
  <%
  //Name:LLGrpRegisterInput.jsp
  //Function：团体立案界面的初始化程序
  //Date：2002-07-21 17:44:28
  //Author ：Xx
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String Comcode=tG.ManageCom;
  %>
  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="LLGrpRegisterInput1.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="LLGrpRegisterInit.jsp"%>
  </head>
  <script language="javascript">
   var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";
  
    function initDate(){
      fm.ManageCom.value="<%=Comcode%>";
    }
  </script>
  <body  onload="initDate();initForm();initElementtype();">
    <form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">

      <Div  id= "divUnclose" style= "display: ''">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegister);">
            </td>
            <td class= titleImg>
              未结团体批次
            </td>
          </tr>
        </table>
        <Div  id= "divGrpRegister" align=center style= "display: ''">
          <table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanGrpRegisterGrid" >
                </span>
              </TD>
            </TR>
          </table>
          <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();">
          <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();">
          <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();">
          <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">
        </Div>
      </Div>
      <HR>
      <table >
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpcontRemarkInfo);">
          </td>
          <td class= titleImg>
            团体保单特约信息
          </td>
          <TD >
            <input class=cssButton type=button value="查看扫描" onclick="ShowScan();">
          </TD>
        </tr>
      </table>

      <div id= "divGrpcontRemarkInfo" style= "display: ''" >
        <table  class= common>
          <TR  class= common8>
            <TD  class= input8>
              <textarea name="GrpRemark" cols="95%" rows="3" witdh=25% class="common"  ></textarea>
            </TD>
          </TR>
        </table>
      </div>
      <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClientInfoTitle);">
          </td>
          <td class= titleImg>
            团体案件信息
          </td>
        </tr>
      </table>

      <div id= "divClientInfoTitle" style= "display: ''" >
        <table  class= common>
          <TR  class= common8>
            <TD class= title8>团体批次号</TD>
            <TD class= input8><Input class= common name="RgtNo" onkeydown="QueryOnKeyDown_RgtNo()" ></TD>
            <TD class= title8>处理人</TD>
            <TD class= input8><Input class= readonly name="Handler" readonly ></TD>
            <TD class= title8>处理日期</TD>
            <TD class= input8><Input class= readonly name="ModifyDate" readonly ></TD>
          </TR>
        </table>
      </div>
      <table>
        <tr>
          <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,idClientQuery);">
          </td>
          <td class= titleImg>
            团体客户查询
          </td>
        </tr>
      </table>
      <div id ='idClientQuery'  style= "display: ''">
        <table  class= common>
          <TR  class= common8>
            <TD class= title8>团体号</TD>
            <TD class= input8><Input class=common name="CustomerNo" elementtype=nacessary verify="团体客户号|notnull" onkeydown="QueryOnKeyDown();"></TD>
            <TD class= title8>单位名称</TD>
            <TD class= input8><Input class=common name="GrpName" onkeydown="QueryOnKeyDown();" ></TD>
            <TD class= title8>保单号码</TD>
            <TD class= input8><Input class=common name="GrpContNo" onkeydown="QueryOnKeyDown();"></TD>
          </TR>
          <tr>
            <TD class= title8>投保人数</TD>
            <TD class= input8><Input class="common"  name=PeopleNo elementtype=nacessary verify="投保人数|notnull"></TD>
            <TD class= title8 id='idGrpAcc' style= "display: 'none'">团体帐户余额</TD>
            <TD class= input8 id='idGrpAccBala' style= "display: 'none'"><Input class="common"  name=AccBala ></TD>
          </tr>
        </table>
      </div>
      <table>
        <TR>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpRegisterInfo);">
          </td>
          <td class= titleImg>申请信息</TD>
        </TR>
      </table>

      <Div id= "divGrpRegisterInfo" style= "display: ''">
        <table class=common>
          <TR class= common>
            <TD class= title8>联系人</TD>
            <TD class= input8><Input class= common name=RgtantName ></TD>
            <TD class= title8>证件类型</TD>
            <TD class= input8><Input class="codeno" name=IDType onclick="return showCodeList('IDType',[this,IDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName],[0,1]);" verify="证件类型|code:IDType"><Input class="codename"  name=IDTypeName ></TD>
            <TD class= title8>证件号码</TD>
            <TD class= input8><Input class= common name=IDNo ></TD>
          </tr>
          <TR  class= common>
            <TD class= title8>联系电话</TD>
            <TD class= input8><Input class= common name=RgtantPhone verify="电话号码|num&&len<=30"></TD>
            <TD class= title8>联系地址</TD>
            <TD class= input8><Input class=common name=RgtantAddress ></TD>
            <TD class= title8>邮政编码</TD>
            <TD class= input8><Input class= common name=PostCode  verify="邮政编码|zipcode&INT"></TD>
          </tr>
          <TR  class= common>
            <TD class= title8>受理方式</TD>
            <TD class= input8><Input class=codeno name=RgtType onClick="showCodeList('LLAskMode',[this,RgtTypeName],[0,1]);" onkeyup="showCodeListKey('LLAskMode',[this,RgtTypeName],[0,1]);"  verify="受理方式|notnull&code:LLAskMode" ><Input class=codename name=RgtTypeName elementtype=nacessary></TD>
            <TD class= title8>联系手机</TD>
            <TD class= input8><Input class= common name=Mobile verify="手机号码|num&&len>=8"></TD>
            <TD class= title8>电子邮箱</TD>
            <TD class= input8><Input class= common name=Email verify="电子邮箱|Email"></TD>
          </tr>
          <TR class= common>
            <TD class= title8>申请人数</TD>
            <TD class= input8><Input class= common name=AppPeoples elementtype=nacessary verify="申请人数|notnull"></TD>
            <TD class= title8 >给付方式</TD>
            <TD class= input8><Input class=codeno name=TogetherFlag  onClick="showCodeList('lltogetherflag',[this,TogetherFlagName],[0,1]);" onkeyup="showCodeListKey('lltogetherflag',[this,TogetherFlagName],[0,1]);" verify="给付方式|notnull&code:lltogetherflag" ><Input class= codename name=TogetherFlagName elementtype=nacessary></TD>
            <TD class= title8 >赔款领取方式</TD>
            <TD class= input8><Input class= codeno name=CaseGetMode onClick="showCodeList('paymode',[this,CaseGetModeName],[0,1],null,str,'1');" onkeyup="showCodeListKey('paymode',[this,CaseGetModeName],[0,1],null,str,'1');" verify="保险金领取方式|code:llgetmode" ><Input class= codename name=CaseGetModeName ></TD>
          </TR>
          <tr class= common id='titleBank'>
            <TD class= title8>银行编码</TD>
            <TD class= input8><Input class="codeno"  name=BankCode onclick="return showCodeList('bank',[this,BankName],[0,1]);" onkeyup="return showCodeListKey('bank',[this,BankName],[0,1]);" ><Input class="codename"  name=BankName></TD>
            <TD class= title8>银行账户</TD>
            <TD class= input8><Input class= common  name=BankAccNo ></TD>
            <TD class= title8>账户名</TD>
            <TD class= input8><Input class= common  name=AccName ></TD>
          </tr>
          <TR  class= common id='idApplyAmnt' style="display:none">
            <TD class= title8>申报金额</TD>
            <TD class= input8><Input class= common name=AppAmnt ></TD>
          </TR>
        </TABLE>
        <TABLE class=common>
          <TR>
            <TD  class= title8 >备注</TD>
          </tr>
          <TR  class= common>
            <TD class= input><textarea name="Remark" cols="100%" rows="4" width=25% class="common"></textarea></TD>
          </tr>
        </table>
      </Div>
       <Div  id= "divGrpCaseInfo" style= "display: 'none'">
    <table>
      <tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpCaseGrid);">
        </td>
        <td class= titleImg>个人客户信息</td>
      </tr>
    </table>
    <Div  id= "divGrpCaseGrid" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanGrpCaseGrid" >
            </span>
          </TD>
        </TR>
      </table>
      <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
    </Div>
  </Div>
    </br>
    <Div  id= "divgrpconf" style= "display: ''" align= left>
      <input class=cssButton style='width:80px;' type=button name='asksave1' value="确  认" onclick="AskSave();">
      <input class=cssButton style='width:80px;' type=button value="撤  件" onclick="DealCancel();">
    </Div>
    <Div  id= "divnormalquesbtn" style= "display: ''" align= right>
      
      <input class=cssButton style='width: 80px;' type=button name='DiskImport1' value="  弹性拨付  " onclick="DiskImport();">
    </Div>

    <hr/>

  </Div>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="LoadFlag" name="LoadFlag" value="0">
  <input type=hidden id="operate" name="operate">
  <input type=hidden id="RiskCode" name="RiskCode">
  <input type=hidden id="ApplyerType" name="ApplyerType" value="0">
  <Input type=hidden id="ManageCom" name="ManageCom" >
  <input type=hidden name="LoadC" >
  <input type=hidden name="LoadD" >
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>