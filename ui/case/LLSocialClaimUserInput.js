/*******************************************************************************
 * Name: RegisterInput.js
 * Function:立案主界面的函数处理程序
 * Author:LiuYansong
 */
var showInfo;
var mDebug="0";
var tSaveFlag = "0";
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();
/*******************************************************************************
 * Name        :main_init
 * Author      :LiuYansong
 * CreateDate  :2003-12-16
 * ModifyDate  :
 * Function    :初始化主程序
 * Parameter   :A_jsp---fm.action;Target_Flag (0--无target，1有target),T_jsp--target 对应的页面 ;Info---提示信息;Operate---操作标识
 *
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info,Operate)
{
	
  var i = 0;
  var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action ="./"+A_jsp;
  if(Operate=="INSERT")
  {
    fm.fmtransact.value="INSERT";
    
  }
  if(Operate=="QUERY")
  {
    fm.fmtransact.value="QUERY";
  }
  if(Operate=="UPDATE")
  {
    fm.fmtransact.value="UPDATE";
  }
  if(Operate=="DELETE")
  {
    fm.fmtransact.value="DELETE";
  }
  if(Target_Flag=="1")
  {
    fm.target = "./"+T_jsp;
  }
  else
  {
    fm.target = "fraSubmit";
  }
  showSubmitFrame(mDebug);

  fm.submit(); //提交
}

//提交，保存按钮对应操作
//添加控制，不能对其点击两次的控制
function submitForm()
{

  if( verifyInput2() == false ) return false;
  if(tSaveFlag=="1")
  {
    alert("您不能执行保存操作！");
    return;
  }
  else
  {
    if (confirm("您确实想保存该记录吗?"))
    {

      tSaveFlag = "1";
      if(!checkdata())
        return ;
      else
      {
        main_init("LLSocialClaimUserSave.jsp","0","_blank","保存","INSERT");
        tSaveFlag = "0";
      }
    }
    else
    {
      alert("您取消了保存操作！");
    }
  }
}
//修改按钮对应的操作
function updateClick()
{
 
  if( verifyInput2() == false ) return false;
  if(tSaveFlag=="1")
  {
    alert("您不能执行修改操作！");
    return;
  }
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
        //解锁disable
      fm.all('UserCode').disabled = false;
      fm.all('HandleFlag').disabled = false;
      if( verifyInput2() == false ){
       return false;
      }
      if( checkUpdate() == false ){
       return false;
      }
      if(!checkdata())
        return;
      else
      {
        main_init("LLSocialClaimUserSave.jsp","0","_blank","修改","UPDATE");
        tSaveFlag = "0";
      }
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}
function deleteClick()
{
  if(tSaveFlag=="1")
  {
    alert("您不能执行删除操作！");
    return;
  }
  else
  {
    if (confirm("您确实想删除该记录吗?"))
    {
      //解锁disable
      fm.all('UserCode').disabled = false;
      fm.all('HandleFlag').disabled = false;
      tSaveFlag = "1";
      main_init("LLSocialClaimUserSave.jsp","0","_blank","删除","DELETE");
      tSaveFlag = "0";
    }
    else
    {
      alert("您取消了删除操作！");
    }
  }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  FlagDel = FlagStr;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}


//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  var pathStr="./FrameMain.jsp?Interface=LLSocialClaimUserQuery.jsp";
  var newWindow=OpenWindowNew(pathStr,"理赔岗位权限查询","left");
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function checkdata()
{
  //案件抽检比例介于0和100之间的数字
  if(fm.ClaimSpotRate.value/1<0||fm.ClaimSpotRate.value/1>100)
  {
    alert("抽检比例应为介于0和100之间的数字");
    tSaveFlag = "0";
    return false;
  }
  //案件分配上限为介于0和60000之间的数字
  if(fm.DispatchRate.value/1<0||fm.DispatchRate.value/1>60000)
  {
    alert("案件分配上限应为介于0和60000之间的数字");
    tSaveFlag = "0";
    return false;
  }
  //一家机构职能有一个调查主管
  var ServeyFlag = fm.ServeyFlag.value;
  if(ServeyFlag=='1')
  {
  	var sql_M="select 1 from LLSocialClaimUser where SurveyFlag='1' and stateflag='1' and comcode='"+fm.Station.value+"' and usercode<>'"+fm.UserCode.value+"'with ur";
    var arrR2 = easyExecSql(sql_M);
    if (!(arrR2 == null||arrR2==""))
     {
	   alert("该机构已经存在调查主管！");
	   tSaveFlag = "0";
       return false;
     }	
  }
  
  //用户状态为非有效时需要录入备注信息
  var tStateFlag = fm.all('StateFlag').value;
  if(tStateFlag=='1')
  {
    return true;
  }else{
   if(fm.all('Remark').value==null||fm.all('Remark').value=='')
   {
     alert("用户状态为非有效时请录入备注信息！");
     return false;
   }
  }
					
  return true;
}

function afterQuery(userCode )
{
	if(userCode==null||userCode=='')
	{
	   alert("用户编码信息获取失败,烦请重新操作");
	   return false;
	}
	var tUserCode = userCode;
	var strSQL = "Select ComCode,(Select name From ldcom where comcode = LLSocialClaimUser.comcode)"
	+" ,UserCode,UserName,SocialMoney,UpUserCode,(Select a.username From LLSocialClaimUser a where a.usercode =LLSocialClaimUser.upusercode  ),RgtFlag,HandleFlag,(case HandleFlag when '0' then '否' when '1' then '是' else '' end)"
	+" ,DispatchRate,SurveyFlag,(case SurveyFlag when '0' then '无调查权限' when '1' then '调查主管' when '2' then '普通调查员' else '' end ),PrepaidLimit,StateFlag,(case StateFlag when '1' then '有效' when '2' then '休假' when '3' then '离职' when '4' then '离岗' when '9' then '其他无效情况' else '' end),remark, "
	+" PrepaidFlag,PrepaidUpUserCode,(case when PrepaidMoney is null then 0 else PrepaidMoney end),(select UserName from  lduser where UserCode = PrepaidUpUserCode),"
	+" (case PrepaidFlag when '0' then '否' when '1' then '是' end ) "
	+" From LLSocialClaimUser where 1=1 "
	+" and UserCode = '"+tUserCode+"'"
	;
    var arrResult = easyExecSql(strSQL);
	if (arrResult != null)
	{
	   try{
            fm.all('Station').value = arrResult[0][0];
            fm.all('StationName').value = arrResult[0][1];
            fm.all('UserCode').value = arrResult[0][2];
            fm.all('UserName').value = arrResult[0][3];
            fm.all('SocialMoney').value = arrResult[0][4];
            fm.all('UpUserCode').value = arrResult[0][5];
            fm.all('UpUserName').value = arrResult[0][6];
            fm.all('RgtFlag').value = arrResult[0][7];
            fm.all('HandleFlag').value = arrResult[0][8];
            fm.all('HandleFlagName').value = arrResult[0][9];
            fm.all('DispatchRate').value = arrResult[0][10];
            fm.all('ServeyFlag').value = arrResult[0][11];
            fm.all('ServeyFlagName').value = arrResult[0][12];
            fm.all('PrepaidLimit').value = arrResult[0][13];
            fm.all('StateFlag').value = arrResult[0][14];
            fm.all('StateFlagName').value = arrResult[0][15];
            fm.all('Remark').value = arrResult[0][16];
            
            fm.PrepaidFlag.value = arrResult[0][17];
		    fm.PrepaidUpUserCode.value = arrResult[0][18];
		    fm.PrepaidMoney.value = arrResult[0][19];
		    fm.PrepaidUpUserCodeName.value = arrResult[0][20];
		    fm.PrepaidFlagName.value = arrResult[0][21];
            
            //当input属性为disable时，form不提交
            //用户编码置为失效
            fm.all('UserCode').disabled = true;
            //参加案件分配
            fm.all('HandleFlag').disabled = true;
	      }catch(ex)
		{
			alert(ex.message );
		}
    }
    var tSQL = "Select UWRate From LDSpotUWRate where Usercode = '"+tUserCode+"' and UWGrade ='AAAA' and UWType ='20000'";
    var arrResult1 = easyExecSql(tSQL);
    if (arrResult1 != null)
	{
	   try{
	   	   fm.all('ClaimSpotRate').value = arrResult1[0][0];
	   }catch(ex)
		{
			alert(ex.message );
		}
	}
}

function getUserName(cObj,cName)
{
  fm.all('UpUserName').value = '';
  var Usercode = fm.all('UserCode').value;
  if(Usercode==null||Usercode=='')
  {
  	alert("烦请先录入用户编码信息！");
    return false;
  }
  var ComCode = fm.all('Station').value;
  if(ComCode==null||ComCode=='')
  {
    alert("烦请先录入机构代码信息");
    return false;
  }
  var strsql ="1 and StateFlag not in (#3#,#4#,#9#) and usercode <>#"+fm.all('UserCode').value+"# ";
  showCodeList('llsocialcode',[cObj,cName],[0,1],null,strsql,1,1);

}

//用户状态为非有效时需要录入备注信息
function checkUpdate()
{
//  var tStateFlag = fm.all('StateFlag').value;
//  if(tStateFlag=='1')
//  {
//    return true;
//  }else{
//   if(fm.all('Remark').value==null||fm.all('Remark').value=='')
//   {
//     alert("用户状态为非有效时请录入备注信息！");
//     return false;
//   }
//  }
  return true;
} 
