//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var theFirstValue="";
var theSecondValue="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		EasyQuery();
	}
}
function checkData()
{
	if(fm.StartDate.value == null || fm.StartDate.value == "" 
		|| fm.EndDate.value == null||fm.EndDate.value=="")
	{
		alert("统计日期不能为空");
		
		return false;
	}
}

//查询函数
function EasyQuery()
{
//		if (checkData() == false)
//		{
//    return false;
//  	}
//		var SpanType=fm.SpanType.value;
//		var StartDate=fm.StartDate.value;
//		var EndDate=fm.EndDate.value;
		var MngCom=fm.MngCom.value;
		var rgtNo = fm.RgtNo.value;
		var caseNo = fm.CaseNo.value;
		var contNo = fm.ContNo.value;
		var customerNo = fm.CustomerNo.value;
//		if(SpanType=="1")
//		{
//			SpanType ="  and (bankonthewayflag = '1' or enteraccdate is not null) ";
//		}else
//			{
//			SpanType="  and (BankOnTheWayFlag = '0' or BankOnTheWayFlag is null) and EnterAccDate is null and ConfDate is null and BankAccNo is not null";
//			}
        var spart1 = "";
		var sql ="select g.managecom,g.actugetno,g.otherno,g.sumgetmoney,g.accname,(select bankname from ldbank where bankcode=g.bankcode) "+
							",case when (select cansendflag from ldbank where bankcode=g.bankcode)='1' then '是'else '否' end,"
							+"'待修改',g.enteraccdate,g.sendbankcount from ljaget g where g.paymode='4' and othernotype='5' and confdate is null and otherno not like 'P%'"
							+ getWherePart('g.actugetno','ActuGetNo')
							+ getWherePart('g.bankaccno','SearchAccNo')
							+ getWherePart('g.accname','SearchAccName')
							+ getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
							+" and g.managecom like '"+MngCom+"%%' and (bankonthewayflag <> '1' or bankonthewayflag is null) "
							+" and g.cansendbank = '1' ";
		if(caseNo!=''){
		    if(easyExecSql("select 1 from ljaget where otherno='"+caseNo+"' ")!=null)
		        spart1 = spart1+" and g.otherno='"+caseNo+"'";
		    else
		        spart1 = spart1+" and exists (select 1 from ljagetclaim where otherno='"+caseNo+"' and actugetno=g.actugetno)";
		}
		if(rgtNo!=''){
		    if(easyExecSql("select 1 from ljaget where otherno='"+rgtNo+"' ")!=null)
		        spart1 = spart1+"and g.otherno='"+rgtNo+"'";
		    else
		        spart1 = spart1+" and exists (select 1 from llcase where caseno=g.otherno and rgtno='"+rgtNo+"')";
		}
		if(contNo!=''){
		spart1 = spart1+" and exists (select 1 from llclaimdetail where (contno='"+contNo+"' or grpcontno='"+contNo+"') and (caseNo=g.otherno or rgtno=g.otherno))"
		}
		if(customerNo!=''){
		spart1 = spart1+" and exists (select 1 from llcase where customerNo='"+customerNo+"' and (caseNo=g.otherno or rgtno=g.otherno)";
		}					
		sql = sql+spart1+"order by managecom with ur";
						
		turnPage.pageLineNum = 10;
		turnPage.queryModal(sql,GetModeGrid);
		var arr = easyExecSql(sql );
		if(!arr)alert("没有数据");
		
}
function submitForm(k)
{ 
   var chkFlag=false;
   for (i=0;i<GetModeGrid.mulLineCount;i++){
       if(GetModeGrid.getSelNo(i)!='0'){
            chkFlag=true;
       }
  }
   if (chkFlag==false){
       alert("请选中待处理案件！");
       return false;
   }

	if(k==0)
	{  //alert(1);
	   fm.operate.value="MODIFY"	
		var casno=fm.all('PolNo').value;
		  if(casno.substring(0,1)!="P"){
			  if( fm.DrawerIDType.value=="0" || fm.DrawerIDType.value=="5" ){
				  var rstrChkIdNo=checkIdNo(fm.DrawerIDType.value,fm.DrawerID.value,"","");
					if(rstrChkIdNo!=""){
						alert("领款人证件号码不符合规则，请核实");
						return false;
			  		}
			  }
		  }
		  //如果为批次，就不一定是 身份证号了。
	}
  else
  {  //alert(2);
  	 fm.operate.value="CONFIRM"	
  }
	
	if(fm.ActuGetNo2.value=="")
	{
		alert("给付凭证号不能为空！")
		return; 
		}
	 if (fm.PolNo.value=="")
	 {
	 	alert("理赔号不能为空！");
    return false;
  	}
  var first = fm.PolNo.value.substr(0,1);
  //alert(first);
  if(first == "P")
  {
  	fm.grpflag.value = "MORE";
  	}
  else {
  	fm.grpflag.value = "ONE";
  	}
  //alert(fm.grpflag.value);
  //return;
  
  if (fm.PayMode.value == '') {
	  alert("请选择付费方式！");
	  return false;
  }
  
  if (fm.Drawer.value == '') {
	  alert("请录入领取人！");
	  return false;
  }
  
  if(fm.PayMode.value!='1'&&fm.PayMode.value!='2') {
	  if(fm.BankCode.value==''){
		  alert("请选择对方开户银行！");
	      return false;
	  }
	  if(fm.BankAccNo.value==''){
	      alert("请录入对方银行帐号！");
	      return false;
	  }
	  if(fm.AccName.value==''){
	      alert("请录入对方银行帐户名！");
	      return false;
	  }
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "fraSubmit";
  fm.action="./LLAccModifySave.jsp"
	fm.submit();
}

function getquery(k)
{
    var rgtNo = fm.RgtNo.value;
	var caseNo = fm.CaseNo.value;
	var contNo = fm.ContNo.value;
	var customerNo = fm.CustomerNo.value;
	var MngCom=fm.MngCom.value;
	var spart1 = "";
	var sql = "";
	if(k == 2)
	{
	    sql ="select g.managecom,g.actugetno,g.otherno,g.sumgetmoney,g.accname,(select bankname from ldbank where bankcode=g.bankcode) "+
							",case when (select cansendflag from ldbank where bankcode=g.bankcode)='1' then '是'else '否' end,"
							+"'待修改',g.enteraccdate,g.sendbankcount from ljaget g where g.paymode='4' and othernotype='5' and confdate is null "
							+ getWherePart('g.actugetno','ActuGetNo')
							+ getWherePart('g.bankaccno','SearchAccNo')
							+ getWherePart('g.accname','SearchAccName')
							+getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
							+ " and exists (select 1 from llcase where caseno=g.otherno "+getWherePart('rigister','Operator')+")"
							+" and g.managecom like '"+MngCom+"%%' and (bankonthewayflag = '0' or bankonthewayflag is null) "
							+" and g.cansendbank = '1' ";
	}
   if(k==3)
    {
       
   
		sql ="select g.managecom,g.actugetno,g.otherno,g.sumgetmoney,g.accname,(select bankname from ldbank where bankcode=g.bankcode) "+
							",case when (select cansendflag from ldbank where bankcode=g.bankcode)='1' then '是'else '否' end,"
							+"'待确认',g.enteraccdate,g.sendbankcount from ljaget g where g.paymode in ('4','11','3') and othernotype='5' and confdate is null "
							+ getWherePart('g.actugetno','ActuGetNo')
							+ getWherePart('g.bankaccno','SearchAccNo')
							+ getWherePart('g.accname','SearchAccName')
							+ getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
							+ " and exists (select 1 from llcase where caseno=g.otherno "
							+ " and exists (select 1 from llclaimuser where usercode=llcase.Rigister "+getWherePart('upusercode','Operator')+" union select 1 from llsocialclaimuser where usercode=llcase.Rigister "+getWherePart('upusercode','Operator')+"))"
							+" and g.managecom like '"+MngCom+"%%' and (bankonthewayflag = '0' or bankonthewayflag is null) "
							+" and g.cansendbank = 'M' ";
	
   }
   if(k==0){
       sql ="select g.managecom,g.actugetno,g.otherno,g.sumgetmoney,g.accname,(select bankname from ldbank where bankcode=g.bankcode) "+
							",case when (select cansendflag from ldbank where bankcode=g.bankcode)='1' then '是'else '否' end,"
							+"'待提盘',g.enteraccdate,g.sendbankcount from ljaget g where g.paymode='4' and othernotype='5' and confdate is null and otherno not like 'P%' "
							+ getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
							+ getWherePart('g.actugetno','ActuGetNo')
							+ getWherePart('g.bankaccno','SearchAccNo')
							+ getWherePart('g.accname','SearchAccName')
							+" and g.managecom like '"+MngCom+"%%' and (bankonthewayflag = '0' or bankonthewayflag is null) "
							+" and (g.cansendbank = '0' or g.cansendbank is null)  and  (g.BankAccNo is not null)  ";

   
   }
   if(k==1){
      sql ="select g.managecom,g.actugetno,g.otherno,g.sumgetmoney,g.accname,(select bankname from ldbank where bankcode=g.bankcode) "+
							",case when (select cansendflag from ldbank where bankcode=g.bankcode)='1' then '是'else '否' end,"
							+"'在途',g.enteraccdate,g.sendbankcount from ljaget g where g.paymode='4' and othernotype='5' and confdate is null and otherno not like 'P%' "
							+ getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
							+ getWherePart('g.actugetno','ActuGetNo')
							+ getWherePart('g.bankaccno','SearchAccNo')
							+ getWherePart('g.accname','SearchAccName')
							+" and g.managecom like '"+MngCom+"%%' and bankonthewayflag='1' "
							+" and (g.cansendbank = '0' or g.cansendbank is null) and g.paymode='4'";

   
   } 
   if(k==4){
      sql ="select g.managecom,g.actugetno,g.otherno,g.sumgetmoney,g.accname,(select bankname from ldbank where bankcode=g.bankcode) "+
							",case when (select cansendflag from ldbank where bankcode=g.bankcode)='1' then '是'else '否' end,"
							+"case when bankonthewayflag='1' then '在途' when (bankonthewayflag='0' or bankonthewayflag is null) and cansendbank='1' then '待修改' when (bankonthewayflag='0' or bankonthewayflag is null) and cansendbank='M' then '待确认' else '待提盘' end ,"
							+"g.enteraccdate,g.sendbankcount from ljaget g where g.paymode='4' and othernotype='5' and confdate is null and otherno not like 'P%' "
							+ getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
							+ getWherePart('g.actugetno','ActuGetNo')
							+ getWherePart('g.bankaccno','SearchAccNo')
							+ getWherePart('g.accname','SearchAccName')
							+" and g.managecom like '"+MngCom+"%%'"
							;
		   
   }
  if(k==5){
          sql ="select g.managecom,g.actugetno,g.otherno,g.sumgetmoney,g.accname,(select bankname from ldbank where bankcode=g.bankcode) "+
							",case when (select cansendflag from ldbank where bankcode=g.bankcode)='1' then '是'else '否' end,"
							+"case when paymode='4' then case when bankonthewayflag='1' then '在途' when (bankonthewayflag='0' or bankonthewayflag is null) and cansendbank='1' then '待修改' when (bankonthewayflag='0' or bankonthewayflag is null) and cansendbank='M' then '待确认' else '待提盘' end end,"
							+"g.enteraccdate,g.sendbankcount from ljaget g where  othernotype='5' and confdate is null and otherno not like 'P%' "
							+ getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
							+ getWherePart('g.actugetno','ActuGetNo')
							+ getWherePart('g.bankaccno','SearchAccNo')
							+ getWherePart('g.accname','SearchAccName')
							+" and g.managecom like '"+MngCom+"%%'";
														
			
   
   }
   if(k==6){
             sql ="select g.managecom,g.actugetno,g.otherno,g.sumgetmoney,g.accname,(select bankname from ldbank where bankcode=g.bankcode) "+
							",case when (select cansendflag from ldbank where bankcode=g.bankcode)='1' then '是'else '否' end,"
							+"'银行汇款',"
							+"g.enteraccdate,g.sendbankcount from ljaget g where  othernotype='5' and confdate is null and paymode='11' and (bankonthewayflag='0' or bankonthewayflag is null)"
							+ getWherePart("g.makedate","StartDate",">=") + getWherePart("g.makedate","EndDate","<=")
							+ getWherePart('g.actugetno','ActuGetNo')
							+ getWherePart('g.bankaccno','SearchAccNo')
							+ getWherePart('g.accname','SearchAccName')
							+" and g.managecom like '"+MngCom+"%%'";
   }
       if(caseNo!=''){
		        spart1 = spart1+" and g.otherno='"+caseNo+"'";
	   }
		if(rgtNo!=''){
		        spart1 = spart1+" and exists (select 1 from llcase where caseno=g.otherno and rgtno='"+rgtNo+"')";
		}
		if(contNo!=''){
		spart1 = spart1+" and exists (select 1 from llclaimdetail where (contno='"+contNo+"' or grpcontno='"+contNo+"') and (caseNo=g.otherno or rgtno=g.otherno))";
		}
		if(customerNo!=''){
		var rr = easyExecSql("select 1 from llcase where customerNo='"+customerNo+"'");
		if(rr!=null && rr!=""){
		spart1 = spart1+" and exists (select 1 from llcase where customerNo='"+customerNo+"' and (caseNo=g.otherno or rgtno=g.otherno))";
		}else{
		spart1 = spart1+" and exists (select 1 from ljagetclaim m where  actugetno=g.actugetno and exists (select 1 from lcgrpcont where appntno='"+customerNo+"' and grpcontno=m.grpcontno union select 1 from lbgrpcont where appntno='"+customerNo+"')) ";
		}
		}					
		sql = sql+spart1+"  with ur";
   turnPage.pageLineNum = 10;
   turnPage.queryModal(sql,GetModeGrid);
   var arr = easyExecSql(sql );
   if(!arr)alert("没有数据");
   return;
}
	
	
function query()
{
	  var tsel = GetModeGrid.getSelNo();
	  var otherno =GetModeGrid.getRowColData(tsel-1,2);
	  //alert(otherno);

		var sql ="select a.actugetno ,a.otherno,a.paymode,(select codename from ldcode "
		          +"where codetype  = 'paymode' and code = a.paymode),a.sumgetmoney,a.Drawer,"
		          +"a.DrawerID,a.BankCode,(select bankname from ldbank "
		          +"where bankcode=a.bankcode),a.BankAccNo,a.AccName, "
		          +"(select bank_reason(a.actugetno) from dual),"
		          +"paymode,bankonthewayflag,confdate,cansendbank "
		          +" from ljaget a where actugetno = '"+otherno+"'";

		var arr = easyExecSql(sql );
		if(arr)
		{ 
			fm.all('ActuGetNo2').value = arr[0][0];
			fm.all('PolNo').value = arr[0][1];
			fm.all('PayMode').value = arr[0][2];
			fm.all('PayModeName').value = arr[0][3];
			fm.all('GetMoney').value = arr[0][4];
			fm.all('Drawer').value = arr[0][5];
			fm.all('DrawerID').value = arr[0][6];
			fm.all('BankCode').value = arr[0][7];
			fm.all('BankCodeName').value = arr[0][8];
			fm.all('BankAccNo').value = arr[0][9];
			fm.all('AccName').value = arr[0][10];
			fm.all('BankError').value = arr[0][11];

			
			if( arr[0][1].substring(0,1)=="P"){
//				a1DrawerIDName.style.display='none'; //领取人证件号码
//				a2DrawerIDName.style.display='';
				fm.DrawerIDTypeName.value='';   //领取人证件类型 ,值为空
				fm.DrawerIDType.value='';
				b1DrawerIDName.style.display='none';//领取人证件类型 ，隐藏
				b2DrawerIDName.style.display='none';
				
			}else{
//				a1DrawerIDName.style.display='';
//				a2DrawerIDName.style.display='none';
				b1DrawerIDName.style.display=''; //领取人证件类型 ，显示
				b2DrawerIDName.style.display='';
				
				
		        var  DrawerIDT="select (select ext.DrawerIDType from llcaseext ext where 1=1 and ext.caseno=a.otherno ), "
		          +" (select codename from ldcode where 1=1 and codetype='idtype' and  code  in(select ext.DrawerIDType from llcaseext ext where 1=1 and ext.caseno=a.otherno )) "
		          +" from ljaget a where actugetno = '"+otherno+"'";
		          var arrDrawerIDT = easyExecSql(DrawerIDT );
		  		if(arrDrawerIDT){
		  			fm.all('DrawerIDType').value = arrDrawerIDT[0][0];
					fm.all('DrawerIDTypeName').value = arrDrawerIDT[0][1];
		  		}
				
			}
			
			if((arr[0][14]=="" || arr[0][14]==null) && GetModeGrid.getRowColData(tsel-1,8)!=""){
			    
			    if(arr[0][13]=='1'){
			        fm.ModifyButton.disabled=true;
                    fm.ConfirmButton.disabled=true;
                    
			    }else{
			        if(arr[0][12]=='4' || arr[0][12]=='3' || arr[0][12]=='11'){
			            if(arr[0][15]=='M'){
			                fm.ModifyButton.disabled=false;
                            fm.ConfirmButton.disabled=false;
                            
			            }else{
			                fm.ModifyButton.disabled=false;
                            fm.ConfirmButton.disabled=true;
                        }
			        }
			        
			    }
			}else{
			fm.ModifyButton.disabled=true;
            fm.ConfirmButton.disabled=true;
			}
			}
	    if(document.all.typeRadio(6).checked){
			fm.ModifyButton.disabled=true;
            fm.ConfirmButton.disabled=true;
		}
		sql = "select RgtObjNo from llregister where rgtno = '"+fm.all('PolNo').value+"'";
		var brr = easyExecSql(sql);
		if(brr)
		{
			fm.all('GrpContNo').value = brr[0][0];
			//alert(fm.all('GrpContNo').value);
		}
	   
	}	


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
 // EasyQuery();
}

//进行两次输入的校验
function confirmSecondInput1(aObject,aEvent)
{ 
	
	if(aEvent=="onkeyup")
	{
		var theKey = window.event.keyCode;
		if(theKey=="13")
		{
			if(theFirstValue!="")
			{
				theSecondValue = aObject.value;
				if(theSecondValue=="")
				{
					alert("请再次录入！");
					aObject.value="";
					aObject.focus();
					return;
				}
				if(theSecondValue==theFirstValue)
				{
					aObject.value = theSecondValue;
					return;
				}
				else
				{
					alert("两次录入结果不符，请重新录入！");
					theFirstValue="";
					theSecondValue="";
					aObject.value="";
					aObject.focus();
					return;
				}
			}
			else
			{
				theFirstValue = aObject.value;
				if(theFirstValue=="")
				{
					theSecondValue="";
					alert("请录入内容！");
					return;
				}
				aObject.value="";
				aObject.focus();
				return;
			}
		}
	}
	else if(aEvent=="onblur")
	{
		if(theFirstValue!="")
		{
			theSecondValue = aObject.value;
			if(theSecondValue=="")
			{
				alert("请再次录入！");
				aObject.value="";
				aObject.focus();
				return true;
			}
			if(theSecondValue==theFirstValue)
			{
				aObject.value = theSecondValue;
				theSecondValue="";
				theFirstValue="";
				fm.all('flag').value=true;
				return;
			}
			else
			{
				alert("两次录入结果不符，请重新录入！");
				theFirstValue="";
				theSecondValue="";
				aObject.value="";
				aObject.focus();
				return;
			}
		}
		else
		{
			theFirstValue = aObject.value;
			theSecondValue="";
			if(theFirstValue=="")
			{
				return;
			}
			aObject.value="";
			aObject.focus();
			return;
		}
	}
}


function ShowScan()
{
	  var Start = fm.all('PolNo').value.substring(0,1);
	  
	  if(Start=="P")
	  {
      var pathStr="./LLAccModifyMain.jsp?Interface=LLAccModifyChose.jsp&RgtNo="+fm.all('PolNo').value;
      var newWindow=OpenWindowNew(pathStr,"选择案件","left");
	
	  }
	  
	  else if(Start!=""&&Start!=null)
	  {
	  	var sql = "select docid from es_doc_main where doccode = '"+fm.all('PolNo').value+"' and busstype = 'LP'";
	  	var arr = easyExecSql(sql);
		  if(arr)
		  { 
		    window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+arr[0][0]+"&DocCode="+fm.all('PolNo').value+"&BussTpye=LP");
		  }
		  else
		  {
		   alert("未查找到影像信息！");
		   return;
		  }
	  }

    else
    {
      alert("没有符合条件的数据！")
    }
    
  
}
function printf(){
  fm.action='./LLAccModifyPrintf.jsp';
  var showStr="正在打开页面，请您稍候...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.target = "f1print";
  fm.submit();
  showInfo.close();
}