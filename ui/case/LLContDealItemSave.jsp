<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：AllPBqQuerySubmit.jsp
//程序功能：
//创建日期：2003-1-20
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
   <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //接收信息，并作校验处理。
  LPEdorAppSchema tLPEdorAppSchema = new LPEdorAppSchema();
  LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
  LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
  PGrpEdorCancelUI tPGrpEdorCancelUI = new PGrpEdorCancelUI();
  LLContDealSchema tLLContDealSchema = new LLContDealSchema();
  TransferData tTransferData = new  TransferData();  
        

  //输出参数
  String FlagStr = "";
  String Content = "";
  GlobalInput tGI = new GlobalInput(); 
  tGI=(GlobalInput)session.getValue("GI");  
  String Operator  = tGI.Operator ;  //保存登陆管理员账号
  String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom内存中登陆区站代码
  CErrors tError = null;
  String delReason = ""; //删除原因
  String reasonCode = ""; //原因编号
		

  //后面要执行的动作：添加，修改，删除
  String transact=request.getParameter("Transact");
  String DelFlag= request.getParameter("DelFlag");
  System.out.println("DelFlag"+DelFlag);
  System.out.println(transact);
 
  System.out.println("reasonCode="+reasonCode);    
  tLLContDealSchema.setEdorNo(request.getParameter("EdorNo"));  
  tLLContDealSchema.setEdorType(request.getParameter("EdorType"));  
  
  VData dVData = new VData();
  dVData.addElement(tGI);
  dVData.addElement(tLLContDealSchema);
  
  LLContCancelBL tLLContCancelBL = new LLContCancelBL();
  if(!tLLContCancelBL.submitData(dVData,""))
  {
	  Content = "保存失败，原因是:" + tLLContCancelBL.mErrors.getFirstError();
      FlagStr = "Fail";  
  }else{
	  if(DelFlag.equals("1")) //删除项目
		{       
					
			  tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("hEdorAcceptNo"));
			  tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
			  System.out.println(tLPEdorItemSchema.getEdorNo());
			  tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
			  tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
			  System.out.println("PEdorAppcancelSubmit->>>"+request.getParameter("EdorType"));
			  tLPEdorItemSchema.setEdorState(request.getParameter("EdorState"));
			  System.out.println(request.getParameter("EdorState"));  
			  tLPEdorItemSchema.setInsuredNo(request.getParameter("InsuredNo"));
			  System.out.println(request.getParameter("InsuredNo"));
			  tLPEdorItemSchema.setPolNo(request.getParameter("PolNo"));
			  tLPEdorItemSchema.setMakeDate(request.getParameter("MakeDate"));
		      tLPEdorItemSchema.setMakeTime(request.getParameter("MakeTime"));
		      
		      delReason = request.getParameter("delItemReason");
		      reasonCode = request.getParameter("CancelItemReasonCode");
		      System.out.println(delReason);   
		      	      
		      tTransferData.setNameAndValue("DelReason",delReason);
		      tTransferData.setNameAndValue("ReasonCode",reasonCode);
		     try
		     {
			    // 准备传输数据 VData
			     VData tVData = new VData();
			     tVData.addElement(tGI);
			     tVData.addElement(tLPEdorItemSchema);
			     tVData.addElement(tTransferData);
			     System.out.println("tLPEdorItemSchema");	     
		
			     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			      tPGrpEdorCancelUI.submitData(tVData,transact);
		   	  }
		     catch(Exception ex)
		     {
		       Content = "保存失败，原因是:" + ex.toString();
		       FlagStr = "Fail";
		     }
			  if (FlagStr=="")
			  {
				    tError = tPGrpEdorCancelUI.mErrors;
				    if (!tError.needDealError())
				    {
				      Content ="撤销成功！";
				    	FlagStr = "Succ";
				    }
				    else
				    {
				    	Content = "撤销失败，原因是:" + tError.getFirstError();
				    	FlagStr = "Fail";
				    }
			   }
		  }
	     if (DelFlag.equals("2"))
	     {  //删除批单
		     tLPEdorMainSchema.setEdorAcceptNo(request.getParameter("hEdorAcceptNo")); 
		     tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
		     tLPEdorMainSchema.setEdorState(request.getParameter("EdorMainState"));     
		     tLPEdorMainSchema.setContNo(request.getParameter("ContNo"));     
		     delReason = request.getParameter("delMainReason");
		     reasonCode = request.getParameter("CancelMainReasonCode");
		     System.out.println(delReason);  
		     
		     tTransferData.setNameAndValue("DelReason",delReason);
		     tTransferData.setNameAndValue("ReasonCode",reasonCode);
		     try
		     {
		    // 准备传输数据 VData
		     VData tVData = new VData();
		     tVData.addElement(tGI);
		     tVData.addElement(tLPEdorMainSchema);
		     tVData.addElement(tTransferData);
		     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
		     tPGrpEdorCancelUI.submitData(tVData,transact);
		    }
		    catch(Exception ex)
		    {
		      Content = "保存失败，原因是:" + ex.toString();
		      FlagStr = "Fail";
		    }
		   if (FlagStr=="")
		   {
			    tError = tPGrpEdorCancelUI.mErrors;
			    if (!tError.needDealError())
			    {
			      Content ="撤销成功！";
			    	FlagStr = "Succ";
			    }
			    else
			    {
			    	Content = "撤销失败，原因是:" + tError.getFirstError();
			    	FlagStr = "Fail";
			    }
		   }  
	  }  
	    if (DelFlag.equals("3"))
	    {  //删除申请
	      
		     tLPEdorAppSchema.setEdorAcceptNo(request.getParameter("hEdorAcceptNo"));     
		     tLPEdorAppSchema.setEdorState(request.getParameter("EdorAppState"));
		     
		     
		     delReason = request.getParameter("delAppReason");
		     reasonCode = request.getParameter("CancelAppReasonCode");
		     
		     tTransferData.setNameAndValue("DelReason",delReason);
		     tTransferData.setNameAndValue("ReasonCode",reasonCode);
		      
		     try
		     {
			    // 准备传输数据 VData
			     VData tVData = new VData();
			     tVData.addElement(tGI);
			     tVData.addElement(tLPEdorAppSchema);
			     tVData.addElement(tTransferData);
			
			
			     //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
			     System.out.println("hello");
			     tPGrpEdorCancelUI.submitData(tVData,transact);
		    }
		    catch(Exception ex)
		    {
		      Content = "保存失败，原因是:" + ex.toString();
		      FlagStr = "Fail";
		    }
		    if (FlagStr=="")
		   {
			    tError = tPGrpEdorCancelUI.mErrors;
			    if (!tError.needDealError())
			    {
			      Content ="撤销成功！";
			    	FlagStr = "Succ";
			    }
			    else
			    {
			    	Content = "撤销失败，原因是:" + tError.getFirstError();
			    	FlagStr = "Fail";
			    }
	   		}
	}
  }
	
%>                                       
<html>
<script language="javascript">
	var mLoadFlag = "<%=request.getParameter("LoadFlag")%>"
	if (mLoadFlag == "TASKFRAME")
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
	else
	{
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
</script>
</html>

