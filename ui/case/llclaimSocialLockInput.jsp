<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%>
<%	GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String CurrentDate= PubFun.getCurrentDate();
    String tCurrentYear=StrTool.getVisaYear(CurrentDate);
    String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
    String tCurrentDate=StrTool.getVisaDay(CurrentDate);
    String AheadDays="-90";
    FDate tD=new FDate();
    Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
    FDate fdate = new FDate();
    String afterdate = fdate.getString( AfterDate );
%>
<html>
<%
    //程序名称：llclaimSocialLockInput.jsp
//程序功能：赔付率预警报表
//创建日期：2018-06-01
//创建人  ：zg
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="llclaimSocialLockInput.js"></SCRIPT>
    <%@include file="llclaimSocialLockInit.jsp"%>
    <script>
        function initDate(){
            fm.EndCaseDateS.value="<%=afterdate%>";
            fm.EndCaseDateE.value="<%=CurrentDate%>";
            fm.ManageCom.value="<%=tG1.ManageCom%>";
        }
    </script>
</head>
<body onload="initDate();initForm()">
    <form action="./llclaimSocialLockSave.jsp" method=post name=fm target="f1print">
    <br>
    <table>
        <tr>
            <td>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,Info);">
            </td>
            <td class= titleImg>
                条件选择
            </td>
        </tr>
    </table>
    <div id=info>
        <table class= common border=0 width=100%>
            <tr class=common>
                <TD  class= title>统计机构</TD>
                <TD  class= input>
                    <Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return organname('comcode',[this,ManageComName],[0,1],null,null,null,1);" verify="统计机构|notnull"><Input class=codename  name=ManageComName></TD>
                <TD  class= title8>保单生效起期</TD>
                <TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="CvaliDateS" verify="生效起期|date&notnull"></TD>
                <TD  class= title8>保单生效止期</TD>
                <TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="CvaliDateE" verify="生效止期|date&notnull"></TD>
            </tr>
            <tr class= common>
                <TD  class= title8>结案起期</TD>
                <TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="EndCaseDateS" verify=""></TD>
                <TD  class= title8>结案止期</TD>
                <TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="EndCaseDateE" verify=""></TD>
            </tr>
            <tr class=common>
                <td clasee= title>案件申请起期</td>
                <td class=input><input clasee="coolDatePicker" dateFormat="short" name="RgtCaseDateS" verify="" elementtype=nacessary></td>
                <td clasee= title>案件申请止期</td>
                <td class=input><input clasee="coolDatePicker" dateFormat="short" name="RgtCaseDateS" verify="" elementtype=nacessary></td>
            </tr>
        </table>
    </div>
        <br>
    <div id=info2>
        <table class=common>
            <tr class=common>
                <TD  class= title>
                    <INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="CasePolicy()">
                </TD>
            </tr>
        </table>
    </div>
    <br>
    <div id=info3 >
        <table>
            <TR>
                <TD>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
                </TD>
                <TD class= titleImg>
                    统计字段说明：
                </TD>
            </TR>
        </table>
    </div>
    <Div  id= "divCare" style= "display: ''">
        <tr class="common"><td class="title">分公司:保单所属机构名称(四位)</td></tr><br>
        <tr class="common"><td class="title">三级机构名称:保单所属机构名称(八位)</td></tr><br>
        <tr class="common"><td class="title">项目编码:社保保单所属项目编码</td></tr><br>
        <tr class="common"><td class="title">项目名称：社保保单所属项目名称</td></tr><br>
        <tr class="common"><td class="title">保单号：团体保单号</td></tr><br>
        <tr class="common"><td class="title">投保单位：团单为投保单位名称</td></tr><br>
        <tr class="common"><td class="title">险种代码：保单承保险种编码</td></tr><br>
        <tr class="common"><td class="title">险种名称：保单承保险种名称</td></tr><br>
        <tr class="common"><td class="title">保费收入(含结余返还)：</td></tr><br>
        <tr class="common"><td class="title">实收保费：保单承保险种截止结案止期财务实收保费</td></tr><br>
        <tr class="common"><td class="title">90%实收保费：保单承保险种截止结案止期财务90%实收保费</td></tr><br>
        <tr class="common"><td class="title">95%实收保费：保单承保险种截止结案止期财务95%实收保费</td></tr><br>
        <tr class="common"><td class="title">结案赔款：保单案件通知给付赔款</td></tr><br>
        <tr class="common"><td class="title">未决赔款：保单案件已理算未给付赔款</td></tr><br>
        <tr class="common"><td class="title">负现金流：实收保费-结案赔款</td></tr><br>
        <tr class="common"><td class="title">预警差值：90%实收保费-结案赔款</td></tr><br>
        <tr class="common"><td class="title">停止录入：95%实收保费-结案赔款</td></tr><br>
    </Div>
    </form>

    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
