<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLConsultSave.jsp
//程序功能：
//创建日期：2005-01-12 16:10:51
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLConsultSchema tLLConsultSchema   = new LLConsultSchema();
  OLLConsultUI tOLLConsultUI   = new OLLConsultUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    String Path = application.getRealPath("config//Conversion.config");	
    tLLConsultSchema.setConsultNo(request.getParameter("ConsultNo"));
    tLLConsultSchema.setLogNo(request.getParameter("LogNo"));
    tLLConsultSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLLConsultSchema.setCustomerName(request.getParameter("CustomerName"));
    tLLConsultSchema.setCustomerType(request.getParameter("CustomerType"));
    tLLConsultSchema.setDiseaseDesc(request.getParameter("DiseaseDesc"));
    tLLConsultSchema.setAccCode(request.getParameter("AccCode"));
    tLLConsultSchema.setAccDesc(request.getParameter("AccDesc"));
    tLLConsultSchema.setHospitalCode(request.getParameter("HospitalCode"));
    tLLConsultSchema.setHospitalName(request.getParameter("HospitalName"));
    tLLConsultSchema.setInHospitalDate(request.getParameter("InHospitalDate"));
	tLLConsultSchema.setOutHospitalDate(request.getParameter("OutHospitalDate"));
    tLLConsultSchema.setCSubject(StrTool.Conversion(request.getParameter("CSubject"),Path));
    tLLConsultSchema.setCContent(StrTool.Conversion(request.getParameter("CContent"),Path));
    tLLConsultSchema.setReplyState("0");
    tLLConsultSchema.setAskGrade(request.getParameter("AskGrade"));
    tLLConsultSchema.setExpRDate(request.getParameter("ExpRDate"));
    tLLConsultSchema.setDiseaseCode(request.getParameter("DiseaseCode"));
    tLLConsultSchema.setCustStatus(StrTool.Conversion(request.getParameter("CustStatus"),Path));
    tLLConsultSchema.setExpertFlag(request.getParameter("ExpertFlag"));
    tLLConsultSchema.setExpertNo(request.getParameter("ExpertNo"));
    tLLConsultSchema.setExpertName(request.getParameter("ExpertName"));
    tLLConsultSchema.setAvaiFlag(request.getParameter("AvaiFlag"));
    tLLConsultSchema.setAvaliReason(request.getParameter("AvaliReason"));
    
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLLConsultSchema);
  	tVData.add(tG);
  	System.out.println(transact );
    tOLLConsultUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLLConsultUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 咨询信息保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  String ConsultNo =tLLConsultSchema.getConsultNo();
  LLConsultSchema rLLConsultSchema = null;
  if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact))
  {
  
    VData res = tOLLConsultUI.getResult();
    rLLConsultSchema=(LLConsultSchema)res.getObjectByObjectName("LLConsultSchema", 0);
    if ( rLLConsultSchema!=null )
    {
    	ConsultNo= rLLConsultSchema.getConsultNo();
    }
  }
  
  //添加各种预处理
  System.out.println("--End Save Page--");
%>                      

<html>
<script language="javascript">
  parent.fraInterface.fm.ConsultNo.value = "<%=ConsultNo%>" ;
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	//parent.fraInterface.updateCustomer("<%=ConsultNo%>");
</script>
</html>
