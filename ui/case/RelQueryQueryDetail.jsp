<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：RelQueryQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
<%
System.out.println("开始执行RelQueryQueryDetail.jsp");    
    CErrors tError = null;
    String FlagStr = "Fail";
    String Content = "";

    LDPersonSchema tLDPersonSchema = new LDPersonSchema();
    tLDPersonSchema.setCustomerNo(request.getParameter("CustomerNo"));
    VData tVData = new VData();
    tVData.addElement(tLDPersonSchema);
    System.out.println("CustomerNo===="+tLDPersonSchema.getCustomerNo());
     
    CQLDPersonUI tCQLDPersonUI = new CQLDPersonUI();
    if (!tCQLDPersonUI.submitData(tVData,"QUERY"))
    {
    		Content = " 查询失败，原因是: " + tCQLDPersonUI.mErrors.getError(0).errorMessage;
      		FlagStr = "Fail";
    }
    else
    {
		tVData.clear();
		tVData = tCQLDPersonUI.getResult();
		
		LDPersonSchema mLDPersonSchema = new LDPersonSchema(); 
		//LDPersonSet mLDPersonSet=new LDPersonSet();
		mLDPersonSchema=(LDPersonSchema)tVData.getObjectByObjectName("LDPersonSchema",0);
		%>
    	<script language="javascript">
    	parent.fraInterface.fm.all("CustomerNo").value = "<%=mLDPersonSchema.getCustomerNo()%>";
     	parent.fraInterface.fm.all("Name").value = "<%=mLDPersonSchema.getName()%>";
     	parent.fraInterface.fm.all("Password").value = "<%=mLDPersonSchema.getPassword()%>";
     	parent.fraInterface.fm.all("Sex").value = "<%=mLDPersonSchema.getSex()%>";
     	parent.fraInterface.fm.all("Birthday").value = "<%=mLDPersonSchema.getBirthday()%>";
     	parent.fraInterface.fm.all("NativePlace").value = "<%=mLDPersonSchema.getNativePlace()%>";
     	parent.fraInterface.fm.all("Nationality").value = "<%=mLDPersonSchema.getNationality()%>";
     	parent.fraInterface.fm.all("Marriage").value = "<%=mLDPersonSchema.getMarriage()%>";
     	parent.fraInterface.fm.all("MarriageDate").value = "<%=mLDPersonSchema.getMarriageDate()%>";
     	parent.fraInterface.fm.all("OccupationType").value = "<%=mLDPersonSchema.getOccupationType()%>";
     	parent.fraInterface.fm.all("StartWorkDate").value = "<%=mLDPersonSchema.getStartWorkDate()%>";
     	parent.fraInterface.fm.all("Salary").value = "<%=mLDPersonSchema.getSalary()%>";
     	parent.fraInterface.fm.all("Health").value = "<%=mLDPersonSchema.getHealth()%>";
     	parent.fraInterface.fm.all("Stature").value = "<%=mLDPersonSchema.getStature()%>";
     	parent.fraInterface.fm.all("Avoirdupois").value = "<%=mLDPersonSchema.getAvoirdupois()%>";
     	parent.fraInterface.fm.all("CreditGrade").value = "<%=mLDPersonSchema.getCreditGrade()%>";
     	parent.fraInterface.fm.all("IDType").value = "<%=mLDPersonSchema.getIDType()%>";
     	parent.fraInterface.fm.all("Proterty").value = "<%=mLDPersonSchema.getProterty()%>";
     	parent.fraInterface.fm.all("IDNo").value = "<%=mLDPersonSchema.getIDNo()%>";
     	parent.fraInterface.fm.all("OthIDType").value = "<%=mLDPersonSchema.getOthIDType()%>";
     	parent.fraInterface.fm.all("OthIDNo").value = "<%=mLDPersonSchema.getOthIDNo()%>";
     	parent.fraInterface.fm.all("ICNo").value = "<%=mLDPersonSchema.getICNo()%>";
     	parent.fraInterface.fm.all("HomeAddressCode").value = "<%=mLDPersonSchema.getHomeAddressCode()%>";
     	parent.fraInterface.fm.all("HomeAddress").value = "<%=mLDPersonSchema.getHomeAddress()%>";
     	parent.fraInterface.fm.all("PostalAddress").value = "<%=mLDPersonSchema.getPostalAddress()%>";
     	parent.fraInterface.fm.all("ZipCode").value = "<%=mLDPersonSchema.getZipCode()%>";
     	parent.fraInterface.fm.all("Phone").value = "<%=mLDPersonSchema.getPhone()%>";
     	parent.fraInterface.fm.all("BP").value = "<%=mLDPersonSchema.getBP()%>";
     	parent.fraInterface.fm.all("Mobile").value = "<%=mLDPersonSchema.getMobile()%>";
     	parent.fraInterface.fm.all("EMail").value = "<%=mLDPersonSchema.getEMail()%>";
     	parent.fraInterface.fm.all("BankCode").value = "<%=mLDPersonSchema.getBankCode()%>";
     	parent.fraInterface.fm.all("BankAccNo").value = "<%=mLDPersonSchema.getBankAccNo()%>";
     	parent.fraInterface.fm.all("JoinCompanyDate").value = "<%=mLDPersonSchema.getJoinCompanyDate()%>";
     	parent.fraInterface.fm.all("Position").value = "<%=mLDPersonSchema.getPosition()%>";
     	parent.fraInterface.fm.all("GrpNo").value = "<%=mLDPersonSchema.getGrpNo()%>";
     	parent.fraInterface.fm.all("GrpName").value = "<%=mLDPersonSchema.getGrpName()%>";
     	parent.fraInterface.fm.all("GrpPhone").value = "<%=mLDPersonSchema.getGrpPhone()%>";
     	parent.fraInterface.fm.all("GrpAddressCode").value = "<%=mLDPersonSchema.getGrpAddressCode()%>";
     	parent.fraInterface.fm.all("GrpAddress").value = "<%=mLDPersonSchema.getGrpAddress()%>";
     	parent.fraInterface.fm.all("DeathDate").value = "<%=mLDPersonSchema.getDeathDate()%>";
     	parent.fraInterface.fm.all("Remark").value = "<%=mLDPersonSchema.getRemark()%>";
     	parent.fraInterface.fm.all("State").value = "<%=mLDPersonSchema.getState()%>";
     	parent.fraInterface.fm.all("BlacklistFlag").value = "<%=mLDPersonSchema.getBlacklistFlag()%>";
     	
     	</script>
		<%		
	}
  
%>