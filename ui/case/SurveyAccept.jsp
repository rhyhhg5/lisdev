<%
//程序名称：LLReportInput.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
	LLSurveySchema mLLSurveySchema = new LLSurveySchema();
	LLSurveyAcceptUI mLLSurveyAcceptUI = new LLSurveyAccept();
	
  CErrors tError = null;
  String transact = "ACCEPT";
  
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

    mLLSurveySchema.setClmCaseNo(request.getParameter("ClmCaseNo"));
		mLLSurveySchema.setType(request.getParameter("Type"));
		mLLSurveySchema.setSerialNo(request.getParameter("SerialNo"));
    
        
//问题
//修改：将SubReportGrid6该成SubReportGrid7
//将SubReportGrid4该成SubReportGrid5
//System.out.println(" Debug inof");
//System.out.println(" Remark info : " + request.getParameterValues("SubReportGrid7")[0]);
//System.out.println(" InhospitalDate info : " + request.getParameterValues("SubReportGrid5")[0]);

	  String[] strNumber=request.getParameterValues("SubReportGridNo");
	  String[] strCustomerNo=request.getParameterValues("SubReportGrid1");
	  String[] strCustomerName=request.getParameterValues("SubReportGrid2");
	  String[] strHospitalCode=request.getParameterValues("SubReportGrid3");
	  String[] strHospitalName=request.getParameterValues("SubReportGrid4");
	  String[] strInHospitalDate=request.getParameterValues("SubReportGrid5");
	  String[] strOutHospitalDate=request.getParameterValues("SubReportGrid6");
	  String[] strRemark=request.getParameterValues("SubReportGrid7");
	  String[] strSubRptNo=request.getParameterValues("SubReportGrid8");
	  
	  int intLength=0;
	  
	  if(strNumber!=null)
	  intLength=strNumber.length;
	  for(int i=0;i<intLength;i++)
	  {
	  	tLLSubReportSchema=new LLSubReportSchema();
	  	tLLSubReportSchema.setCustomerNo(strCustomerNo[i]);
	  	tLLSubReportSchema.setCustomerName(strCustomerName[i]);
	  	tLLSubReportSchema.setHospitalCode(strHospitalCode[i]);
	  	tLLSubReportSchema.setHospitalName(strHospitalName[i]);
	  	tLLSubReportSchema.setInHospitalDate(strInHospitalDate[i]);
	  	tLLSubReportSchema.setOutHospitalDate(strOutHospitalDate[i]);
	  	tLLSubReportSchema.setRemark(strRemark[i]);
			tLLSubReportSet.add(tLLSubReportSchema);
		
		


System.out.println(" Debug inof");
System.out.println(" Remark info : " + strRemark[i]);
System.out.println(" InhospitalDate info : " + strInHospitalDate[i]);
	  }
	  GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");
	
	VData tVData = new VData();
  try
  {
  // 准备传输数据 VData

   //保存报案信息
   tVData.addElement(tLLReportSet);  
   tVData.addElement(tLLSubReportSet);
   tVData.addElement(tG); 
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tReportUI.submitData(tVData,transact);
   
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tReportUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    	tVData.clear();
    	tVData = tReportUI.getResult();
	    //反写报案号的语句
	    LLReportSchema ttLLReportSchema = new LLReportSchema();
	    LLReportBLSet yLLReportBLSet = new LLReportBLSet();
	    
	    yLLReportBLSet.set((LLReportBLSet)tVData.getObjectByObjectName("LLReportBLSet",0));
	    ttLLReportSchema.setSchema(yLLReportBLSet.get(1));	
	    
	    System.out.println("ReportNo==="+ttLLReportSchema.getRptNo());
	    %>
	    <script language="javascript">
	    parent.fraInterface.fm.all("RptNo").value="<%=ttLLReportSchema.getRptNo()%>";
			</script>
			<%
      
      //反写分报案号的语句
      LLSubReportSet yLLSubReportSet = new LLSubReportSet();
      yLLSubReportSet.set((LLSubReportBLSet)tVData.getObjectByObjectName("LLSubReportBLSet",0));
      int m = yLLSubReportSet.size();
      for (int i =0;i<m;i++)
      {
      	LLSubReportSchema ttLLSubReportSchema = new LLSubReportSchema();
      	ttLLSubReportSchema.setSchema(yLLSubReportSet.get(i+1));
      	
      System.out.println("SubRptNo=="+ttLLSubReportSchema.getSubRptNo());
      %>
      
	<script language="javascript">
		parent.fraInterface.SubReportGrid.setRowColData(<%=i%>,8,"<%=ttLLSubReportSchema.getSubRptNo()%>");
	</script>
<%
    }
    }
    
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>