//               该文件中包含客户端需要处理的函数和事件


var showInfo;
var mDebug="1";

//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput2() == false ) return false;
	if (fm.CaseGetMode.value==''){
			alert("请选择领取方式");
			return false;
		}
	if (fm.TogetherFlag.value!=1){
			if(fm.CaseGetMode.value!='1'&&fm.CaseGetMode.value!='2'){
			if(fm.BankCode.value==''){
				alert("请录入银行编码！");
				return false;
			}
			if(fm.BankAccNo.value==''){
				alert("请录入银行账号！");
				return false;
			}
			if(fm.AccName.value==''){
				alert("请录入银行账户名！");
				return false;
			}
		}
	}
	
  	fm.fmtransact.value = "UPDATE||RgtPayMode";
    var showStr="正在执行修改，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LLRgtChangePayModeSave.jsp";
    fm.submitButton.disabled=true;
    fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.submitButton.disabled=false;
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
     parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	alert("您不能执行修改操作");
	return;
}


//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{

}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function afterCodeSelect( cCodeName, Field ) {
  if( cCodeName == "lltogetherflag"){
    if (fm.CaseGetMode.value!="1"&&fm.CaseGetMode.value!='2'){
    	showDiv(titleBank,"true");
    }else{
    	showDiv(titleBank,"false");
    }
    if(Field.value=="2"||Field.value=="3"){
    	showDiv(titleCaseGetMode1,"true");
    	showDiv(titleCaseGetMode2,"true");
    	fm.BankCode.onclick="";
		fm.BankName.readOnly=true;
		fm.BankAccNo.readOnly=true;
		fm.AccName.readOnly=true;
		searchAcc();
    }else if (Field.value=="4") {
        showDiv(titleCaseGetMode1,"true");
    	showDiv(titleCaseGetMode2,"true");
    	fm.BankCode.onclick=bankcodename;
		fm.BankName.readOnly=false;
		fm.BankAccNo.readOnly=false;
		fm.AccName.readOnly=false;
    }else {
    	showDiv(titleCaseGetMode1,"true");
    	showDiv(titleCaseGetMode2,"true");
    	showDiv(titleBank,"false");
    }
  }
  
  if( cCodeName == "paymode"){
    if(Field.value=="1"||Field.value=="2" ||fm.TogetherFlag.value=="1"){
      showDiv(titleBank,"false");
    }else{
      showDiv(titleBank,"true");
      if (fm.TogetherFlag.value=="4") {
        fm.BankCode.onclick=bankcodename;
		fm.BankName.readOnly=false;
		fm.BankAccNo.readOnly=false;
		fm.AccName.readOnly=false;  
      } else {
        fm.BankCode.onclick="";
		fm.BankName.readOnly=true;
		fm.BankAccNo.readOnly=true;
		fm.AccName.readOnly=true;
      }
    }
  }
}

function bankcodename()
{
	return showCodeList('lllbank',[fm.BankCode,fm.BankName],[0,1],null,fm.BankName.value,'bankname',1);
	//return showCodeList('llbank',[fm.BankCode,fm.BankName,fm.SendFlag],[0,1,2]);
}

function searchAcc() {
   var tSQL = "SELECT a.claimbankcode,b.bankname,a.claimbankaccno,a.claimaccname FROM lcgrpappnt a,ldbank b WHERE a.claimbankcode=b.bankcode AND a.grpcontno = '"+fm.GrpContNo.value+"'";
   var tArrs = easyExecSql(tSQL);
   if (tArrs){
		fm.BankCode.value = tArrs[0][0];
		fm.BankName.value = tArrs[0][1];
		fm.BankAccNo.value = tArrs[0][2];
		fm.AccName.value = tArrs[0][3];
   } else {
        fm.BankCode.value = "";
		fm.BankName.value = "";
		fm.BankAccNo.value = "";
		fm.AccName.value = "";
   }
}
