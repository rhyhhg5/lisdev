<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLPolicyInput.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLPolicyInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script>
	function initDate(){
   fm.StartDate.value="<%=afterdate%>";
   fm.EndDate.value="<%=CurrentDate%>";
   fm.JFStartDate.value="<%=afterdate%>";
   fm.JFEndDate.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
   fm.ContType.value = "1";
   fm.ContTypeName.value = "个险";
  }
</script>
</head>
<body onload="initDate();" >    
  <form action="./LLPolicy.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title>险种类型</TD>
          <TD  class= input> <input class="codeno" CodeData="0|2^1|个险^2|团险"  verify="险种类型|notnull" elementtype=nacessary name=ContType ondblclick="return showCodeListEx('ContType',[this,ContTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('ContType',[this,ContTypeName],[0,1]);"><input class=codename name=ContTypeName></TD> 
		  <TD  class= title>险种编码</TD>
          <TD  class= input><Input class=codeno  name="RiskCode" onClick="showCodeList('riskprop',[this,RiskName],[0,1],null,fm.ContType.value,'riskprop',1,250);" onkeyup="showCodeListKeyEx('riskprop',[this,RiskName],[0,1],null,fm.ContType.value,'riskprop',1,250);" ><Input class=codename name= RiskName></TD> 
        </TR>
        <TR class = common>
          <TD  class= title>保单号</TD>
          <TD  class= input> <input class=common name=ContNo ></TD> 
          <TD  class= title>批次号</TD>
          <TD  class= input> <input class=common name=RgtNo ></TD> 
          <TD  class= title></TD>
          <TD  class= input></TD>
        </TR>
      	<TR  class= common>
      	  <TD  class= title>结案起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="" elementtype=nacessary> </TD> 
          <TD  class= title>结案止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="" elementtype=nacessary> </TD> 
       	  <TD  class= title> </TD>
          <TD  class= input> <Input name=td0 class=readonly readonly > </TD>
		 </TR>
		 <TR  class= common>
      	  <TD  class= title>通知起期</TD>
          <TD  class= input> <Input name=TZStartDate class='coolDatePicker' dateFormat='short' verify="" elementtype=nacessary> </TD> 
          <TD  class= title>通知止期</TD>
          <TD  class= input> <Input name=TZEndDate class='coolDatePicker' dateFormat='short' verify="" elementtype=nacessary> </TD> 
       	  <TD  class= title> </TD>
          <TD  class= input> <Input name=td2 class=readonly readonly > </TD>
		 <TR  class= common>
      	  <TD  class= title>给付起期</TD>
          <TD  class= input> <Input name=JFStartDate class='coolDatePicker' dateFormat='short' verify="" elementtype=nacessary> </TD> 
          <TD  class= title>给付止期</TD>
          <TD  class= input> <Input name=JFEndDate class='coolDatePicker' dateFormat='short' verify="" elementtype=nacessary> </TD> 
       	  <TD  class= title> </TD>
          <TD  class= input> <Input name=td1 class=readonly readonly > </TD>
		 </TR>
		 <TR  class= common>
          <TD  class= title>业务类型</TD>
          <TD  class= input> <input class="codeno" value="3" CodeData="0|3^1|商业保险^2|社会保险^3|全部"  verify="业务类型|notnull" elementtype=nacessary name=StatsType ondblclick="return showCodeListEx('StatsType',[this,StatsTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('StatsType',[this,StatsTypeName],[0,1]);"><input class=codename value="全部" name=StatsTypeName></TD> 
        </TR>
    </table>
    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title>
			 <INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="CasePolicy()">  
             <INPUT VALUE="csv模式打印" class="cssButton" TYPE="button" onclick="CasePolicyCSV()"> 
             </TD> 
			</tr>
		</table>
	 <table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
	 </table>
		<Div  id= "divCare" style= "display: ''">
			<tr class="common"><td class="title">管理机构代码：案件受理机构代码(八位)</td></tr><br>
		 	<tr class="common"><td class="title">管理机构：案件受理机构</td></tr><br>
			<tr class="common"><td class="title">投保单位：团单为投保单位名称，个单为投保人</td></tr><br>
			<tr class="common"><td class="title">投保客户号：投保单位或个人的客户号</td></tr><br>
			<tr class="common"><td class="title">保单号码：团单为团体保单号，个单为个人保单号</td></tr><br>
			<tr class="common"><td class="title">批次号：批次下案件显示批次号，个案受理的显示理赔号</td></tr><br>
			<tr class="common"><td class="title">理赔号：案件理赔号</td></tr><br>
			<tr class="common"><td class="title">案件状态：结案、通知、给付</td></tr><br>
			<tr class="common"><td class="title">客户号：被保险人客户号</td></tr><br>
			<tr class="common"><td class="title">客户姓名：被保险人姓名</td></tr><br>
			<tr class="common"><td class="title">出生年月：被保险人出生日期</td></tr><br>
			<tr class="common"><td class="title"><font color="red">年龄：被保险人案件受理时年龄</font></td></tr><br>
			<tr class="common"><td class="title">客户性别：被保险人性别</td></tr><br>
			<tr class="common"><td class="title">证件类型：被保险人受理时选择的证件类型</td></tr><br>
			<tr class="common"><td class="title">证件号码：被保险人受理时录入的证件号码</td></tr><br>
			<tr class="common"><td class="title">职业代码：被保险人职业代码</td></tr><br>
			<tr class="common"><td class="title">职业名称：被保险人职业名称</td></tr><br>
			<tr class="common"><td class="title">职业类别代码：被保险人职业类别代码</td></tr><br>
			<tr class="common"><td class="title">职业类别名称：被保险人职业类别名称</td></tr><br>
			<tr class="common"><td class="title">人员类别：已录入账单中第一张账单的参保人员类别（在职、退休、老职工），非社保类账单无此项。</td></tr><br>
			<tr class="common"><td class="title">治疗类型：已录入账单中第一张账单的账单种类（门诊、住院、门诊特殊病）</td></tr><br>
			<tr class="common"><td class="title">理赔险种：案件赔付险种</td></tr><br>
			<tr class="common"><td class="title">险种保额：该赔付险种的险种保额</td></tr><br>
			<tr class="common"><td class="title">保费：该赔付险种的首期保费</td></tr><br>
			<tr class="common"><td class="title">缴费频次：该赔付险种的缴费频次</td></tr><br>
			<tr class="common"><td class="title">套餐编码：案件赔付险种所属套餐的编码</td></tr><br>
			<tr class="common"><td class="title">套餐名称：案件赔付险种所属套餐的名称</td></tr><br> 
			<tr class="common"><td class="title">保单生效日：该赔付保单的生效日期</td></tr><br>
			<tr class="common"><td class="title">保单失效日：该赔付保单的失效日期</td></tr><br>
			<tr class="common"><td class="title">销售渠道：该赔付保单的销售渠道</td></tr><br>
			<tr class="common"><td class="title">核保结论：该赔付保单的核保结论</td></tr><br>
			<tr class="common"><td class="title">业务员代码：该赔付保单的业务员代码</td></tr><br>
			<tr class="common"><td class="title">业务员姓名：该赔付保单的业务员姓名</td></tr><br>
			<tr class="common"><td class="title">所属营业单位编码：该业务员所属营业单位编码</td></tr><br>
			<tr class="common"><td class="title">所属营业部：该业务员所属营业部名称</td></tr><br>
			<tr class="common"><td class="title">中介公司代码：该赔付保单的中介公司代码</td></tr><br>
			<tr class="common"><td class="title">中介公司名称：该赔付保单的中介公司名称</td></tr><br>
			<tr class="common"><td class="title">账单金额：案件下所有账单的合计金额</td></tr><br>
			<tr class="common"><td class="title">拒付金额：案件下该保单该险种的拒付金额</td></tr><br>
			<tr class="common"><td class="title"><font color="red">给付责任：案件赔付的给付责任</font></td></tr><br>
			<tr class="common"><td class="title"><font color="red">赔付金额：案件下该保单该险种给付责任的赔付金额</font></td></tr><br>
			<tr class="common"><td class="title" ><font color="red">赔付结论：该案件给付责任的赔付结论</font></td></tr><br>
			<tr class="common"><td class="title">出险日期：案件下录入的事件中出险日期最早的</td></tr><br>
			<tr class="common"><td class="title">就诊医院编码：已录入账单中第一张账单的就诊医院编码</td></tr><br>
			<tr class="common"><td class="title">就诊医院：已录入账单中第一张账单的就诊医院名称</td></tr><br>
			<tr class="common"><td class="title">医院等级：已录入账单中第一张账单的就诊医院的等级</td></tr><br>
			<tr class="common"><td class="title">合作级别：已录入账单中第一张账单的就诊医院的系统中当前合作级别</td></tr><br>
			<tr class="common"><td class="title">入院日期：已录入账单中第一张账单的入院日期</td></tr><br>
			<tr class="common"><td class="title">出院日期：已录入账单中第一张账单的出院日期</td></tr><br>
			<tr class="common"><td class="title">治疗天数：所有账单的住院天数总和</td></tr><br>
			<tr class="common"><td class="title">疾病名称：已录入的疾病信息中的第一个疾病</td></tr><br>
			<tr class="common"><td class="title">ICD-10：已录入的疾病信息中的第一个疾病ICD-10编码</td></tr><br>
			<tr class="common"><td class="title">意外信息：已录入的意外信息中的第一个意外信息</td></tr><br>
			<tr class="common"><td class="title">意外编码：已录入的意外信息中的第一个意外的编码</td></tr><br>
			<tr class="common"><td class="title">受理日期：案件的受理日期</td></tr><br>
			<tr class="common"><td class="title">受理人员：案件的受理人</td></tr><br>
			<tr class="common"><td class="title">扫描日期：案件的第一次扫描时间，只统计对案件的扫描</td></tr><br>
			<tr class="common"><td class="title">扫描人员：案件添加扫描件的操作人，只统计对案件的扫描</td></tr><br>
			<tr class="common"><td class="title">录入日期：案件账单的最后录入时间</td></tr><br>
			<tr class="common"><td class="title">录入人员：案件账单的最后录入人员</td></tr><br>
			<tr class="common"><td class="title">检录日期：案件的最后检录日期</td></tr><br>
			<tr class="common"><td class="title">检录人员：案件的最后检录人</td></tr><br>
			<tr class="common"><td class="title">理算日期：案件的最后理算确认日期</td></tr><br>
			<tr class="common"><td class="title">理算人员：案件的最后理算确认人</td></tr><br>
			<tr class="common"><td class="title">审批日期：案件的初次审批日期</td></tr><br>
			<tr class="common"><td class="title">审批人员：案件的初次审批人</td></tr><br>
			<tr class="common"><td class="title">审定日期：案件最后的审定日期</td></tr><br>
			<tr class="common"><td class="title">审定人员：案件最后的审定人</td></tr><br>
			<tr class="common"><td class="title">抽检日期：案件被抽检的日期</td></tr><br>
			<tr class="common"><td class="title">抽检人员：案件抽检审核上级</td></tr><br>
			<tr class="common"><td class="title">结案日期：案件结案日期</td></tr><br>
			<tr class="common"><td class="title">结案人员：案件的处理人</td></tr><br>
			<tr class="common"><td class="title">通知日期：案件给付确认的日期</td></tr><br>
			<tr class="common"><td class="title">通知人员：案件的给付确认人</td></tr><br>
			<tr class="common"><td class="title">给付日期：案件的实际给付日期</td></tr><br>   
            <tr class="common"><td class="title">是否核销预付赔款：判断此案件是否使用了预付赔款</td></tr><br> 
		</Div>



	<Input type="hidden" class= common name="RiskRate" >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 