/*******************************************************************************
* Name: RegisterInput.js
* Function:立案主界面的函数处理程序
* Author:LiuYansong
*/
var showInfo;
var mDebug="0";
var FlagDel;//在delete函数中判断删除的是否成功
var case_flag ;//判断是否选择了分案号,初始化时是1，若没有选中则是0。
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tSaveFlag = "0";
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
/*******************************************************************************
* Name        :main_init
* Author      :LiuYansong
* CreateDate  :2003-12-16
* ModifyDate  :
* Function    :初始化主程序
* Parameter   :A_jsp---fm.action;Target_Flag (0--无target，1有target),T_jsp--target 对应的页面 ;Info---提示信息
*
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info)
{
	var i = 0;
	var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action ="./"+A_jsp;
	//alert("target标志是"+Target_Flag);
	if(Target_Flag=="1")
	{
		fm.target = "./"+T_jsp;   
	}
else
	{
		fm.target = "fraSubmit";
	}
	//showSubmitFrame(mDebug);
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	FlagDel = FlagStr;
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
else
	{
//		var strsql = "select a.RgtState,b.codename,a.Operator,a.MakeDate from LLcase a, ldcode b where a.caseno='"+fm.CaseNo.value +"'"
//		+" and b.codetype='llrgtstate' and b.code=a.rgtstate"
//		arrResult = easyExecSql(strsql);
//
//		if ( arrResult )
//		{
//			fm.RgtState.value= arrResult[0][1];
//			fm.Handler.value= arrResult[0][2];
//			//fm.ModifyDate.value= arrResult[0][4];
//			fm.ModifyDate.value = getCurrentDate();
//
//			initTitle();
//			ClientafterQuery( fm.RgtNo.value,fm.CaseNo.value );
//		}

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//showDiv(operateButton,"true");
		//showDiv(inputButton,"false");
	}
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
else
	{
		parent.fraMain.rows = "0,0,0,0,*";
	}
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
else
	{
		cDiv.style.display="none";
	}
}

//打印理赔申请书的函数
function PLPsqs()
{
	main_init("PLPsqs.jsp","1","f1print","打印");
	showInfo.close();
}
//连接报案查询界面的函数
function RgtQueryRpt()
{
	tSaveFlag = "Rpt";
	showInfo=window.open("FrameReportQuery.jsp");
}

function ClientQuery()
{
  var  wherePart = getWherePart("a.InsuredNo", 'CustomerNo' )
  + getWherePart("a.name", 'CustomerName' )
  + getWherePart( 'a.GrpContNo','GrpContNo' )
  + getWherePart('a.othidno','SecurityNo');
  if ( wherePart == ""){
    alert("请输入查询条件");
    return false;
  }

  var strSQL0 ="select distinct a.insuredno,a.name,a.sex,a.birthday,a.idno,a.othidno,a.insuredstat,b.grpname ";
  var sqlpart1="from lcinsured a,ldperson b where b.CustomerNo=a.insuredNo ";
  var sqlpart2="from lbinsured a,ldperson b where b.CustomerNo=a.insuredNo ";
  var strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;
  //+ " and b.AddressNo= a.AddressNo ";
  var arr= easyExecSql( strSQL,1,0,1 );

  if ( arr==null ){
    alert("没有符合条件的数据");
    return false;
  }
  try
  {
    //如果查询出的数据是一条，显示在页面，如果是多条数据填
    //出一个页面，显示在mulline中
    if(arr.length==1){
      afterLLRegister(arr);
      QueryEvent();
    }
    else{
      var varSrc = "&CustomerNo=" + fm.CustomerNo.value;
      varSrc += "&CustomerName=" + fm.CustomerName.value;
      varSrc += "&GrpContNo=" + fm.GrpContNo.value;
      varSrc += "&IDNo=" + fm.IDNo.value;
      varSrc += "&SecurityNo=" + fm.SecurityNo.value;
      showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLSPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
    }
  }
  catch(ex){
    alert("错误:"+ ex.message);
  }
}

function afterLLRegister(arr)
{
  if(arr!=null)
  {
    fm.CustomerNo.value   =arr[0][0];
    var ttsql = "select * from llcase where CustomerNo = '"+fm.CustomerNo.value 
    + "' and RgtNo = '"+fm.RgtNo.value+"'";
    var xrr = easyExecSql(ttsql);
    if(xrr){
      if(!confirm("该批次下已经录入了该客户，要继续吗？"))
        return false;
    }
    fm.CustomerName.value =arr[0][1];
    fm.Sex.value          =arr[0][2];
    if(arrResult[0][2]==0)
    fm.SexName.value = "男";
    if(arrResult[0][2]==1)
    fm.SexName.value = "女";
    if(arrResult[0][2]==2)
    fm.SexName.value = "不详";
    fm.CBirthday.value =arr[0][3];
    fm.IDNo.value      =arr[0][4];
    fm.SecurityNo.value =arr[0][5];
    fm.InsuredStat.value =arr[0][6];
    if(arrResult[0][6]==1)
    fm.SexName.value = "在职";
    if(arrResult[0][6]==1)
    fm.SexName.value = "退休";
    fm.GrpName.value = arr[0][7];
  }
  showPRemark();
}

function openAffix()
{
	var varSrc ="";
	pathStr="./CaseAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value+"&LoadFlag=1&LoadC="+fm.LoadC.value;
	showInfo = OpenWindowNew(pathStr,"","middle");
}

function submitFormSurvery()
{
	strSQL="select otherno from LLSurvey where otherno='"+fm.CaseNo.value+"'";
	arrResult = easyExecSql(strSQL);
	if(arrResult){
		
		 if(!confirm("该案件提起过调查，您确定要继续吗？"))
		    return false;
		}else{
		 
		}
	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&InsuredNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	varSrc += "&StartPhase=0";
	varSrc += "&Type=1";
	pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
}

function submitForm()
{
    if( verifyInput2() == false )
     return false;
//    #3501	
    var strChkIdNo=checkIdNo('0',fm.IDNo.value,fm.CBirthday.value,fm.Sex.value);
    if(strChkIdNo!=""){
		alert("被保人身份证号不符合规则，请核实");
		return false;
		}
//		if(!checkIdCard(fm.IDNo.value)){
//		  if(!confirm("要继续吗？"))
//		    return false;
//		}
    

		if(fm.CustomerNo.value==null||fm.CustomerNo.value=="")
		{
			alert("请输入客户数据");
		}
		else
		{
				fm.all('operate').value = "INSERT||MAIN";

				strSQL = " select AppPeoples,customerno,grpname,rgtstate from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
				arrResult = easyExecSql(strSQL);
				if(arrResult != null){
				  if (arrResult[0][3]!='01'){
				    alert("该批次已受理申请完毕");
				    return;
				  }
		    //客户尚有未结案校
	    strSQL = "select caseno from llcase where rgtstate not in ('14','09','12','11') and customerno='"
	    +fm.all('CustomerNo').value+"' and caseno <> '"+fm.CaseNo.value+"' and rgtno='"+fm.all('rgtno').value+"'" ;
	    crrResultTemp = easyExecSql(strSQL);
	
	    if(crrResultTemp){
	      if(!confirm("本批次该客户有案件"+crrResultTemp+"未结案，您确定要继续吗？"))
	      return false;
	    }
	    else{
	
	    }			  
					var AppPeoples;
					try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
					var appntno= arrResult[0][1];
					strSQL = "select InsuredNo from lcpol where appntno='"+appntno+"' and InsuredNo='"
					+fm.all('CustomerNo').value+"'";
					crrResult = easyExecSql(strSQL);
					if(crrResult){
					}
					else{
					  if(!confirm("团体客户"+arrResult[0][2]+"未给个人客户"+fm.CustomerName.value+"投保，您确定要继续吗？"))
					    return false;
					}
				}
				else{
					alert("团体案件信息查询失败！");
					return;
				}

				strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
				arrResult = easyExecSql(strSQL);
				if(arrResult != null){
					var RealPeoples;
					try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
				}
				if ( (eval(AppPeoples) - eval(RealPeoples)) < 5 ){
					alert("团体申请人数：" + AppPeoples + "实际已录入人数：" + RealPeoples);
				}
				if ( eval(RealPeoples) >= eval(AppPeoples) ){
//					if (!confirm("团体申请人数：" + AppPeoples + ", 实际已录入人数：" + RealPeoples + "你确认要补加个人客户吗?"));
//					{
//						return;
//					}
				}

			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.action ="./LLSimpleClaimSave.jsp";
			fm.submit();
		}
}


//ClientCaseQueryInput.js查询返回接口2005-2-21 13:09
function ClientafterQuery(RgtNo,CaseNo)
{
	//团体批次
	if ( RgtNo!=null && RgtNo.length>0)
	{
		var arrResult = new Array();
		var strSQL="select c.otheridno,c.CustomerName,c.CustomerNo,c.IDNo,c.CustomerSex,c.custbirthday,"
		+"f.InsuredStat,c.grpname,f.hosatti,f.hospitalname,f.hospitalcode,f.inhosno,f.realhospdate,"
		+"f.feetype,f.feedate,f.hospstartdate,f.hospenddate,c.custstate,c.deathdate "
		+"from llcase c,llfeemain f where f.caseno=c.caseno and c.caseno='"+fm.CaseNo.value+"'";
		arrResult = easyExecSql(strSQL);
		if (CaseNo!=null && CaseNo.length>0)
		{
			if( arrResult != null )
			{
				fm.SecurityNo.value   =arrResult[0][0];
				fm.CustomerName.value =arrResult[0][1];
				fm.CustomerNo.value   =arrResult[0][2];
				fm.IDNo.value =arrResult[0][3];
				fm.Sex.value  =arrResult[0][4];
				fm.CBirthday.value   =arrResult[0][5];
				fm.InsuredStat.value =arrResult[0][6];
				fm.GrpName.value = arrResult[0][7]
				fm.HosSecNo.value= arrResult[0][8];
				fm.HospitalName.value= arrResult[0][9];
				fm.HospitalCode.value= arrResult[0][10];
				fm.inpatientNo.value =arrResult[0][11] ;
		    fm.RealHospDate.value=arrResult[0][12];
			  fm.FeeType.value= arrResult[0][13];
			  fm.FeeDate.value= arrResult[0][14];
			  var cureSQL = "SELECT diseasecode,diseasename from llcasecure where caseno='"+fm.CaseNo.value+"'";
			  curResult = easyExecSql(cureSQL);
			  if(curResult){
			    fm.ICDCode.value=curResult[0][0];
			    fm.ICDName.value=curResult[0][1];
			  }
			  fm.HospStartDate.value = arrResult[0][15];
			  fm.HospEndDate.value = arrResult[0][16];
			  fm.CustStatus.value = arrResult[0][17];
			  fm.DeathDate.value = arrResult[0][18];
		}
		var feeSQL = "SELECT applyamnt,selfamnt,selfpay2,getlimit,midamnt,planfee,standbyamnt,"
		    +"SupInHosFee,highamnt1,OfficialSubsidy FROM LLSECURITYRECEIPT where caseno ='"
		    +fm.CaseNo.value+"'";
	  turnPage.queryModal(feeSQL,FeeGrid);
			strSQL="select ReasonCode,Reason from LLAppClaimReason where 1=1 "
			+"and rgtno='"+RgtNo+"' "
			+" and caseno='"+CaseNo+"'";
			arrResult = easyExecSql(strSQL);
			if ( arrResult!=null )
			{

				for (i=0;i< arrResult.length ;i++)
				{
					var ind= eval(arrResult[i][0])-1;
				}
			}
			//事件查询
			QueryEvent();
			fm.all("CaseNo").value = CaseNo;
			fm.all("RgtNo").value =  RgtNo;
			strSQL="select RgtState,Operator,ModifyDate,handler,codename from llcase,ldcode where caseno='"+CaseNo+"' and code=RgtState and codetype='llrgtstate'";
			arrResult = easyExecSql(strSQL);
			if(arrResult!=null)
			{
				fm.all("Handler").value = arrResult[0][1];
				fm.all("ModifyDate").value = arrResult[0][2];
				fm.RgtState.value=arrResult[0][4];
				if(arrResult[0][3].substring(0,3)=="bcm")
				{
					fm.SimpleCase.checked = "true";
				}
			}
		strSQL = "select appealtype,case appealtype  when '1' then '纠错类' when '0' then '申诉类' end ,appeanrcode,appealreason,appealrdesc from LLAppeal where appealno ='"+fm.CaseNo.value+"'";
				brrResult = easyExecSql(strSQL);
				if(brrResult){
				fm.all('AppealType').value=brrResult[0][0];
				fm.all('AppealTypeName').value=brrResult[0][1];
				fm.all('AppeanRCode').value= brrResult[0][2];
				fm.all('AppealReason').value= brrResult[0][3];
				fm.all('AppealRDesc').value= brrResult[0][4];

		}
			showPRemark();	
		}
	}
}

//判断查询输入窗口的案件类型是否是回车，
//如果是回车调用查询客户函数
function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		ClientQuery();
	}
}

//弹出窗口显示复选框选择受益人
function OpenBnf()
{
	var pathStr="./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerName.value+"&RgtNo="+fm.RgtNo.value+"&paymode="+fm.paymode.value;
	showInfo=OpenWindowNew(pathStr,"EventInput","middle");
}

//打印接口
function PrintPage()
{
	if(fm.CaseNo.value=="")
	{
		alert("请先保存数据");
		return false;
	}
	showInfo = window.open("./PayAppPrt.jsp?CaseNo="+fm.CaseNo.value,"PrintPage",'width=600,height=400,top=100,left=60,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function afterCodeSelect( cCodeName, Field ) {
	if( cCodeName == "llRelation" && Field.value=="00")
	{
		fm.RgtantName.value = fm.CustomerName.value;
		fm.IDTypeName.value=fm.tIDTypeName.value;
		fm.IDType.value = fm.tIDType.value;
		fm.IDNo  .value=fm.IDNo.value;

		    addSQL = "select b.PostalAddress,b.zipcode,b.phone,b.mobile,b.Email from lcinsured a,lcaddress b where b.AddressNo=a.AddressNo and b.CustomerNo=a.InsuredNo and a.InsuredNo='"+fm.CustomerNo.value+"'";
		    crr = easyExecSql( addSQL , 1,0,1);
		    if (crr)
		    {
		    	fm.RgtantAddress.value=crr[0][0];
		    	fm.PostCode.value =crr[0][1];
		    	fm.RgtantPhone.value = crr[0][2];
		    	fm.Mobile.value=crr[0][3];
		    	fm.Email.value = crr[0][4];
		    }
	}
	if( cCodeName == "llgetmode")
	{
		if(Field.value=="1"||Field.value=="2")
		{
			divBankAcc.style.display='none';
			fm.BankAccM.disabled=true;
		}
	else
		{
			divBankAcc.style.display='';
			fm.BankAccM.disabled=false;
			var strSQL ="select distinct a.Bankcode,a.Bankaccno,a.Accname "
			+"from lcinsured a where InsuredNo='"+fm.CustomerNo.value+"'";
			var arr= easyExecSql( strSQL,1,0,1 );
				if (arr){
				fm.BankCode.value=arr[0][0];
				fm.BankAccNo.value=arr[0][1];
				fm.AccName.value=arr[0][2];
			}
		}
	}
}

function RgtFinish()
{
	strSQL = " select AppPeoples,rgtstate from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
	arrResult = easyExecSql(strSQL);

	if(arrResult != null)
	{
		if ( arrResult[0][1]!='01')
		{
			alert("团体案件状态不允许该操作！");
			return;
		}
		var AppPeoples;
		try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
	}
else
	{
		alert("团体案件信息查询失败！");
		return;
	}


	strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
	arrResult = easyExecSql(strSQL);
	if(arrResult != null)
	{
		var RealPeoples;
		try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
	}
	strSQL = " select customername from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
	brr = easyExecSql(strSQL)
	if(brr)
	{
		count = brr.length;
	}

	if ( RealPeoples < AppPeoples )
	{
		if(confirm("团体申请下个人申请尚未全部申请完毕！" + "团体申请人数：" + AppPeoples + "实际申请人数：" + RealPeoples+ "\n您确认要结束本团体批次吗?"))
		{
			fm.realpeoples.value=RealPeoples;
			alert("本批次申请人数为："+RealPeoples+"人")
		}
	else
		{
			return;
		}
	}

//	if ( RealPeoples == AppPeoples )
//	{
			fm.realpeoples.value=RealPeoples;
		fm.all('operate').value = "UPDATE||MAIN";
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

		fm.action = "./LLGrpRegisterSave.jsp";
		fm.submit();
//	}

}

function CaseChange()
{
	initTitle();
	ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}

function QueryEvent()
{
	if(fm.CustomerNo.value!="")
	{

		var strSQL="select SubRptNo,AccDate,AccPlace,AccidentType,AccSubject,HospitalName,InHospitalDate,OutHospitalDate,AccDesc,"+
					"case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end," +"AccidentType from LLSubReport where CustomerNo='"
		+ fm.CustomerNo.value +"' order by AccDate desc";
		//turnPage.queryModal(strSQL, EventGrid);
		arrResult = easyExecSql(strSQL);
		if ( arrResult )
		{
			displayMultiline(arrResult,EventGrid);

			//查询关联上的事件
			strSQL = "select subrptno from llcaserela where caseno='" + fm.CaseNo.value +"'";
			var br = easyExecSql ( strSQL );
			//对已关联事件打勾
			if ( br)
			{
				pos =0;
				for ( i=0;i<arrResult.length;i++)
				{
					for ( j=0;j<br.length ;j++)
					{
						if ( arrResult[i][0]==	br[j][0] )
						{
							EventGrid.checkBoxSel(i+1);

						}
					}
				}
			}
		}
	}
}

function DealCancel()
{
	var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.CustomerNo.value+"&CustomerName="+fm.CustomerName.value;
  showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}



function showPRemark() 
{
	
	var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
								+fm.CustomerNo.value+"'";
	var	crrResult1 = easyExecSql(strSQLx);
	if(crrResult1)
	{
		alert("客户："+crrResult1[0][1]+"为黑名单客户，您确定要继续吗？");
	
	}
	var grpResult = new Array();
	var perResult = new Array();
	var arrResult = new Array();
	var polResult = new Array();
  var temp="";
	var str1SQL="select grpcontno from lccont where "+
							" insuredno='"+fm.CustomerNo.value+"' "+
							" and grpcontno in (select grpcontno from lcgrpcont where appflag='1')"+
							" and appflag='1'";
	grpResult=easyExecSql(str1SQL);
	if(grpResult != null&&grpResult != '')
	{
		for ( i = 0; i <= grpResult.length ;i++)
		{	
			var strSQL="select '保单'||grpcontno||'约定:'||remark from lcgrpcont "+
								 "where grpcontno='"+grpResult[i]+"'";
			arrResult=easyExecSql(strSQL);
				
			var polSQL="Select '保障计划'||CONTPLANCODE||'下险种'||RISKCODE||coalesce((select codename from ldcode where codetype='CalTitle' and code=c.calfactor),'')||'约定:'||remark FROM LCCONTPLANDUTYPARAM c "+
								" where grpcontno='"+grpResult[i]+"' AND CONTPLANCODE!='11' and remark !='' "+
								 "GROUP BY CONTPLANCODE,RISKCODE,remark,calfactor";
			polResult=easyExecSql(polSQL);								
			if(arrResult != null&&arrResult != '')
			{
				try
				{			
					temp += arrResult+"\n";													
				}
				catch(ex)
				{
					alert(ex.message)
				}
			}
			if(polResult !=null&&polResult != '')
			{
				try
				{
					temp += polResult+"\n";
				}
				catch(ex)
				{
					alert(ex.message)
				}
			}
		}		
	} 		
		var str2SQL="select contno from lccont where "+
							" insuredno='"+fm.CustomerNo.value+"' "+
							" and appflag='1'" ;
	  perResult=easyExecSql(str2SQL);		  
		if(perResult != null&&perResult != '')
	{
		for ( i = 0; i <= perResult.length ;i++)
		{	
			var str3SQL="select '保单'||contno||'下险种'||(select riskcode from lcpol p where p.polno=c.polno)||'约定:'||speccontent from lcpolspecrela c "+
									" where contno ='"+perResult[i]+"'";
					arrResult=easyExecSql(str3SQL);	
					
			if(arrResult != null&&arrResult != '')
			{		
					try
				{
					temp +=	arrResult+"\n";
				}
				catch(ex)
				{
					alert(ex.message)
				}
			}else{}								
	  }
	}
		fm.all('PerRemark').value=temp.replace(/\,/g,"\n");		
		if(temp=="")
		{
			fm.all('PerRemark').value="无特别约定，请依照条款处理";	
		}										    	
}

function ContQuery()
{

	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&InsuredNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	pathStr="./FrameMainContQuery.jsp?Interface=LLContQueryInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLContQueryInput","middle",800,330);

}

//计算实际住院天数
function caldate()
{
  if(fm.HospStartDate.value!=""&&fm.HospEndDate.value!="")
  {
    fm.HospStartDate.value=modifydate(fm.HospStartDate.value);
    fm.HospEndDate.value=modifydate(fm.HospEndDate.value);
    //	var a=dateDiff(fm.HospStartDate.value,fm.HospEndDate.value,"D")-1;
    var strSql= "select to_char(to_date('"+ fm.HospEndDate.value + "') - to_date('" + fm.HospStartDate.value +"')) from dual"
    //alert(strSql );
    var mrr = easyExecSql(strSql);
    if (mrr!=null)
    {
      a= mrr[0][0] ;
      if(fm.FeeType.value=="1")
        a=a/1+1;
      if ( a<0) 
        fm.RealHospDate.value =0 ;
      else
        fm.RealHospDate.value= a;
    }
  }
}

function queryGrpCaseGrid()
{
	var jsRgtNo = fm.all('RgtNo').value;
	 	
	  var strSql = " SELECT C.CASENO, C.OTHERIDNO,C.CUSTOMERNAME, C.CUSTOMERNO, C.RGTDATE, B.APPLYAMNT"
	  	        + " FROM LLCASE C, LLSECURITYRECEIPT b  WHERE C.RGTNO = '" 
	  	        + jsRgtNo + "' and b.CASENO= c.CASENO ORDER BY C.CASENO " ;
      turnPage1.pageLineNum = 3;
	  turnPage1.queryModal(strSql,GrpCaseGrid);
}

function ShowCase()
{
  var selno = GrpCaseGrid.getSelNo()-1;
  if (selno >=0)
  {
    fm.CaseNo.value = GrpCaseGrid.getRowColData( selno, 1);
  }
  ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}

function checkMoney(){
  
}