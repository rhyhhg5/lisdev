<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import="java.util.*"%> 
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<html>    
<%
//程序名称：LLCardClaimCollectionInput.jsp
//程序功能：F1报表生成
//创建日期：2010-12-3
//创建人  ：zzh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLCardClaimCollectionInput.js"></SCRIPT>
		<%@include file="LLCardClaimCollectionInit.jsp"%>   
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<script>
	function initForm(){
	fm.EndCaseDateS.value="<%=afterdate%>";
   fm.EndCaseDateE.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
   var arrResult=easyExecSql("select Name from ldcom where comcode='<%=tG1.ManageCom%>'");
   fm.ManageComName.value=arrResult[0][0];
  }
 </script>
</head>

<body onload="initForm();">
  <form action="./LLCardClaimCollectionRpt.jsp" method=post name=fm target="f1print">
    <br>
    <table>
    	<TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,Info);">
         </TD>
         <TD class= titleImg>
         查询条件
         </TD>
       </TR>
   	</table>
    <Div id=Info>
    <table class= common border=0 width=100%>
      	<TR  class= common>          
				<TD  class= title>统计机构</TD>
				<TD  class= input>
    			<Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return organname('comcode',[this,ManageComName],[0,1],null,null,null,1);" verify="统计机构|notnull"><Input class=codename  name=ManageComName></TD>
          		<TD  class= title8>保单生效起期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="CvaliDateS" verify="生效起期|date&notnull"></TD>
          		<TD  class= title8>保单生效止期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="CvaliDateE" verify="生效止期|date&notnull"></TD>
        </TR>
        <TR  class= common> 
        		<TD  class= title8>结算起期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="JieSuanS" verify="结算起期|date&notnull"></TD>
          		<TD  class= title8>结算止期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="JieSuanE" verify="结算止期|date&notnull"></TD>
		  		<TD  class= title8></TD>
		  		<TD  class= input8></TD>
        </TR>
        <TR  class= common> 
        		<TD  class= title8>激活起期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="ActiveDateS" verify="激活起期|date&notnull"></TD>
          		<TD  class= title8>激活止期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="ActiveDateE" verify="激活止期|date&notnull"></TD>
		  		<TD  class= title8></TD>
		  		<TD  class= input8></TD>
        </TR>
        <TR  class= common> 
        		<TD  class= title8>结案起期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="EndCaseDateS" verify="结案起期|date&notnull"></TD>
          		<TD  class= title8>结案止期</TD>
		  		<TD  class= input8><Input class="coolDatePicker" dateFormat="short"  name="EndCaseDateE" verify="结案止期|date&notnull"></TD>
		  		<TD  class= title8></TD>
		  		<TD  class= input8></TD>
        </TR>
    </table>
		<hr>
		<INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="submitForm()">
	<table>
	 	<TR>
	 	 <TD>
	 	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCare);">
	 	 </TD>
		 <TD class= titleImg>
		   统计字段说明：
		 </TD>
		</TR>
		
	 </table>
	 <Div  id= "divCare" style= "display: ''">
		 	<tr class="common"><td class="title">统计机构默认为登录机构，必录</td></tr><br>
		 	<tr class="common"><td class="title">保单生效期间、结算期间、激活期间三个时间段只能一次选一个，不能多选，无时间间隔限制</td></tr><br>
		 	<tr class="common"><td class="title">查询条件为结算期间时，卡折套餐承保人数是结算时的卡折数</td></tr><br>
		 	<tr class="common"><td class="title">查询条件为激活期间时，卡折套餐承保人数是网站激活的被保险人数</td></tr><br>
		 	<tr class="common"><td class="title">查询条件为生效期间时，卡折套餐承保人数是网站激活时确认的生效日期在此期间的被保险人数</td></tr><br>
		 	<tr class="common"><td class="title">查询条件为结算期间时，卡折套餐承保保费是结算时的保费</td></tr><br>
		 	<tr class="common"><td class="title">查询条件为激活期间时，卡折套餐承保保费是网站激活的卡的保费</td></tr><br>
		 	<tr class="common"><td class="title">查询条件为生效期间时，卡折套餐承保保费是网站激活时确认的生效日期在此期间的卡的保费</td></tr><br>
		 	
	</Div>
		<input type= hidden name=StartDate>
		<input type= hidden name=EndDate>
		<input type= hidden name=MngName>
		<input type= hidden name=fmtransact>
		
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 