var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var showInfo;

//提交，保存按钮对应操作
function submitForm()
{
	var Count=CustomerInfo.mulLineCount;
  if(Count==0){
  	alert("没有客户信息!");
  	return false;
  }
  
  var chkFlag=false;
  for (i=0;i<Count;i++){
    if(CustomerInfo.getChkNo(i)==true){
    	if(CustomerInfo.getRowColData(i,8)==""||CustomerInfo.getRowColData(i,8)==null){
		    alert("第"+(i+1)+"行客户风险等级为空!");	
				return false;
			}
			if(CustomerInfo.getRowColData(i,10)==""||CustomerInfo.getRowColData(i,10)==null){
		    alert("第"+(i+1)+"行分类依据为空!");	
				return false;
			}
			if(CustomerInfo.getRowColData(i,11)==""||CustomerInfo.getRowColData(i,11)==null){
		    alert("第"+(i+1)+"行分类依据为空!");	
				return false;
			}
      chkFlag=true;
    }
  }
  
  if (chkFlag==false){
    alert("请选择要分类的客户!");
    return false;
  }
  
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="./LCCustomerRCConfirmSave.jsp"
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LCCustomerRCConfirm.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

// 事件响应函数，当用户改变CodeSelect的值时触发
function afterCodeSelect(cCodeName, Field)
{
	try {
		if( cCodeName == "ClassState"){
			if (fm.ClassState.value=="0"){
				divConfirm.style.display='none';
			} else {
				divConfirm.style.display='';
			}
		}
		if (cCodeName == "CustomerType") {
			initCustomerInfo();
			if (fm.CustomerType.value=="0") {
				divIDNo.style.display="none";
				divIDNot.style.display="none";
			} else {
				divIDNo.style.display="";
				divIDNot.style.display="";
			}
		}
	} catch(ex) {
		alert("在afterCodeSelect中发生异常");
	}
}


function searchCase()
{
	
	if (fm.RgtDateS.value==""||fm.RgtDateS.value==null
	    ||fm.RgtDateE.value==""||fm.RgtDateE.value==null
	    ||fm.ManageCom.value==""||fm.ManageCom.value==null) 
	    {
	    	alert("请录入受理日期和管理机构!");
	    	return false;
	    }
  var StateRadio="";
  for(i = 0; i <fm.ContDealFlag.length; i++)
  {
    if(fm.ContDealFlag[i].checked){
      ContDealFlag=fm.ContDealFlag[i].value;
      break;
    }
  }
	var contdeal="";
	var uwdeal="";
	var uwsql="";	
	if (ContDealFlag == "01") //全部待处理
	{
		contdeal += " and a.contdealflag in ('0','1')"
		uwdeal +=" ,llcontdeal b ";
		uwsql +=" and a.caseno=b.caseno and (a.handler='"+ fm.Handler.value +"'or b.dealer='"+ fm.Handler.value+"')";
	}
	else if (ContDealFlag == "02") //已申请
	{
		contdeal = " and a.contdealflag ='0' and a.handler='"+ fm.Handler.value +"'"
	}
	else if (ContDealFlag == "03") //待审批
	{
		contdeal = " and a.contdealflag ='1'"
		uwdeal +=" ,llcontdeal b ";
		uwsql +=" and a.caseno=b.caseno and (a.handler='"+ fm.Handler.value +"'or b.dealer='"+ fm.Handler.value+"')";
	}
	else if (ContDealFlag == "04") //已完成
	{
		contdeal = " and a.contdealflag ='2'"
		uwdeal +=" ,llcontdeal b ";
		uwsql +=" and a.caseno=b.caseno and (a.handler='"+ fm.Handler.value +"'or b.dealer='"+ fm.Handler.value+"')";
	}
	else if (ContDealFlag == "05") //未申请
	{
		contdeal = " and a.contdealflag is null and a.handler='"+ fm.Handler.value +"'"
	}
	var strSQL = "select distinct a.caseno,a.rgtdate,a.customerno,a.customername,a.handler,"
	         +" case when a.contdealflag='0' then '已申请' when a.contdealflag='1' then '待审批' when a.contdealflag='2' then '已完成' else '未申请' end "
	         +" from llcase a "+ uwdeal
	         +" where a.mngcom like '"+fm.ManageCom.value+"%' "
		     +" and a.rgtstate in ('11','12')" + contdeal + uwsql
		     +" and exists (select 1 from llclaimdetail where caseno=a.caseno and grpcontno='00000000000000000000')"
		     + getWherePart('a.caseno','CaseNo')
		     + getWherePart('a.rgtdate','RgtDateS','>=')
		     + getWherePart('a.rgtdate','RgtDateE','<=')
		     + getWherePart('a.customerno','CustomerNo')
		     + getWherePart('a.customername','CustomerName');
	turnPage.queryModal(strSQL,CaseInfo);
}

function SelectCaseInfo()
{
	var vRow = CaseInfo.getSelNo();

	if( vRow == null || vRow == 0 ) {
		return;
	}
	
	fm.CustomerNoT.value = CaseInfo.getRowColData(vRow-1,3);
	fm.CaseNoT.value = CaseInfo.getRowColData(vRow-1,1);
}

function ContDeal()
{
	if (fm.CaseNoT.value==null||fm.CaseNoT.value==""
	    ||fm.CustomerNoT.value==null||fm.CustomerNoT.value=="") 
	{
		alert("没有案件信息！");
		return false;
	}

	var varSrc = "&CaseNo=" + fm.CaseNoT.value;
	varSrc += "&InsuredNo=" + fm.CustomerNoT.value;
	pathStr="./FrameMainContDeal.jsp?Interface=LLContDealInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLContDealInput","left");
}

function closePage()
{
  top.close();
}

function afterPrint(outname,outpathname) {
	showInfo.close();
	fileUrl.href = "../f1print/download.jsp?filename="+outname+"&filenamepath="+outpathname;
  fileUrl.click();
}