<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：BatchClaimCal.jsp
//程序功能：
//创建日期：2005-02-5 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//接收信息，并作校验处理。
//输入参数


CErrors tError = null;

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
VData tVData = new VData();

BatchClaimCalBL tBatchClaimCalBL = new BatchClaimCalBL();
LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
//输出参数
String FlagStr = "";
String Content = "";

  tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
  try
  {
    tVData.add(tLLRegisterSchema);
    tVData.add(tG);
    tBatchClaimCalBL.submitData(tVData, "cal");
  }
  catch(Exception ex)
  {
    Content = "确认失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr=="")
  {
    tError = tBatchClaimCalBL.mErrors;
    if (!tError.needDealError())
    {
      Content ="确认成功！";
      FlagStr = "Succ";
      tVData.clear();
      tVData = tBatchClaimCalBL.getResult();
    }
    else
    {
      Content = "确认失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }

%>


<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>



