<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuHealthSave.jsp
//程序功能：保全人工核保体检资料录入
//创建日期：2006-02-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String flag;
  String content;

  GlobalInput gi = (GlobalInput)session.getValue("GI");	   
  
  String edorNo = request.getParameter("EdorNo");
  String contNo = request.getParameter("ContNo");
 	String customerNo = request.getParameter("InsuredNo");
 	String customerName = request.getParameter("InsuredName");
 	String prtSeq = request.getParameter("PrtSeq");
 	String replyContente = request.getParameter("ReplyContente");
 	String contente = request.getParameter("Contente");
 	
 	LPRReportSchema tLPRReportSchema = new LPRReportSchema();
 	tLPRReportSchema.setEdorNo(edorNo);
 	tLPRReportSchema.setContNo(contNo);
 	tLPRReportSchema.setProposalContNo(contNo);
 	tLPRReportSchema.setPrtSeq(prtSeq);
 	tLPRReportSchema.setContente(contente);
 	tLPRReportSchema.setReplyContente(replyContente);
 	tLPRReportSchema.setCustomerNo(customerNo);
 	
 	String serialNo[] = request.getParameterValues("QuestGridNo");
 	String reportItemCode[] = request.getParameterValues("QuestGrid1");
 	String reportResult[] = request.getParameterValues("QuestGrid5");
  int count = 0;
  if (serialNo != null)
  {		
    count = serialNo.length;
  }
  
  LPRReportItemSet tLPRReportItemSet = new LPRReportItemSet();
  for (int i = 0; i < count; i++)
  {
    LPRReportItemSchema tLPRReportItemSchema = new LPRReportItemSchema();
    tLPRReportItemSchema.setEdorNo(edorNo);
    tLPRReportItemSchema.setPrtSeq(prtSeq);
    tLPRReportItemSchema.setContNo(contNo);
    tLPRReportItemSchema.setRReportItemCode(reportItemCode[i]);
    tLPRReportItemSchema.setRRItemResult(reportResult[i]);
    tLPRReportItemSet.add(tLPRReportItemSchema);
  }
  
  LPRReportResultSet tLPRReportResultSet = new LPRReportResultSet();
  for (int i = 0; i < count; i++)
  {
   	LPRReportResultSchema tLPRReportResultSchema = new LPRReportResultSchema();
   	tLPRReportResultSchema.setEdorNo(edorNo);
   	tLPRReportResultSchema.setContNo(contNo);
   	tLPRReportResultSchema.setProposalContNo(contNo);
   	tLPRReportResultSchema.setPrtSeq(prtSeq);
   	tLPRReportResultSchema.setSerialNo(String.valueOf(i + 1));
   	tLPRReportResultSchema.setCustomerNo(customerNo);
   	tLPRReportResultSchema.setName(customerName);
   	tLPRReportResultSchema.setRReportResult(reportResult[i]);
   	tLPRReportResultSet.add(tLPRReportResultSchema);
  }
  
  VData data = new VData();
  data.add(gi);
  data.add(tLPRReportSchema);
  data.add(tLPRReportItemSet);
  data.add(tLPRReportResultSet);
  PEdorUWManuRReportQueryUI tPEdorUWManuRReportQueryUI = new PEdorUWManuRReportQueryUI();
  if (!tPEdorUWManuRReportQueryUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tPEdorUWManuRReportQueryUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！";
  }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>
 	
 	
 	