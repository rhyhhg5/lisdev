//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var mSwitch = parent.VD.gVSwitch;
var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}

// 数据返回父窗口
function getQueryDetail()
{  
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	    var cPolNo = PolGrid.getRowColData(tSel - 1,2);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		
		if (cPolNo == "")
		    return;
		    
		var GrpPolNo = PolGrid.getRowColData(tSel-1,1);
                var prtNo = PolGrid.getRowColData(tSel-1,3);
        //alert("dfdf");
        if( tIsCancelPolFlag == "0"){
	    	if (GrpPolNo =="00000000000000000000") {
	    	 	window.open("./AllProQueryMain.jsp?LoadFlag=6&prtNo="+prtNo,"window1");	
		    } else {
			window.open("./AllProQueryMain.jsp?LoadFlag=4");	
		    }
		} else {
		if( tIsCancelPolFlag == "1"){//销户保单查询
			if (GrpPolNo =="00000000000000000000")   {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} else {
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	    } else {
	    	alert("保单类型传输错误!");
	    	return ;
	    }
	 }
 }
}

//销户保单的查询函数
function getQueryDetail_B()
{
	
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录。" );
	else
	{
	  var cPolNo = PolGrid.getRowColData(tSel - 1,1);				
		parent.VD.gVSwitch.deleteVar("PolNo");				
		parent.VD.gVSwitch.addVar("PolNo","",cPolNo);
		if (cPolNo == "")
			return;
		var GrpPolNo = PolGrid.getRowColData(tSel-1,6);
	    if (GrpPolNo =="00000000000000000000") 
	    {
	    	    window.open("./AllProQueryMain_B.jsp?LoadFlag=6","window1");	
			} 
			else 
			{
				window.open("./AllProQueryMain_B.jsp?LoadFlag=7");	
			}
	}
}



// 保单明细查询
function PolClick()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cContNo = PolGrid.getRowColData(tSel - 1,1);				
		if (cContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo +"&IsCancelPolFlag=0");	
	}
}

// 基本信息查询
function FunderInfo()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cInsuredNo = PolGrid.getRowColData(tSel - 1,4);	
			var cGrpContNo = PolGrid.getRowColData(tSel - 1,5);	
	    	
		if (cInsuredNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("CQPersonMain.jsp?CustomerNo="+cInsuredNo+"&GrpContNo="+cGrpContNo);	
	}
}
//个人业务--投保信息查询
function PersonBiz()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var cContNo = PolGrid.getRowColData(tSel - 1,1);				
	    var cPrtNo = PolGrid.getRowColData(tSel - 1,2);	
	    var cCustomerNo = PolGrid.getRowColData(tSel - 1,4);
	    var cGrpContNo  = PolGrid.getRowColData(tSel - 1,5); 
		if (cContNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("PolDetailQueryMain.jsp?ContNo="+cContNo+"&PrtNo="+cPrtNo+"&CustomerNo="+cCustomerNo+"&GrpContNo="+cGrpContNo+"&IsCancelPolFlag=0"+"&IsCancelPolFlag=0");	
	}
}

//保单保障项目查询
function PolPolicy()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
	    var ContNo = PolGrid.getRowColData(tSel - 1,1);		
	    var CustomerNo = PolGrid.getRowColData(tSel - 1,4);		
		if (ContNo == "" || CustomerNo == "")
		    return;	
		    //alert("cContNo="+cContNo);	
		
	    window.open("../case/PolInfoQueryMain.jsp?ContNo="+ContNo+"&CustomerNo="+CustomerNo+"&IsCancelPolFlag=0");	
	}
}

//健康管理项目查询
function HMQuery()
{
	var arrReturn = new Array();
	var arr = new Array();
	var tSel = PolGrid.getSelNo();
	if(getQueryResult() == null)alert("请先查询并选择一条数据");
  else
	{
		arrReturn = getQueryResult();
	
	
		var Sql = " select a.logno ,b.name,'',b.IDNo,a.logdate "
									+ " from llmainask a,LDPerson b  "
									+ " where a.logerno = '"+arrReturn[0][3]+"'"
									+ " and b.Customerno = '"+arrReturn[0][3]+"'"
									;
				
					 arr = easyExecSql(Sql);
//		if( tSel == 0 || tSel == "null" )
//			alert( "请先选择一条记录，再点击返回按钮。" );
//		else
//		{	    
//	    alert(arr);
		    var tSel_health = HealthInfoGrid.getSelNo();
		    if( tSel_health == 0 || tSel_health == null )
				{//选择某一客户后，查询他有几次咨询
					
					      if (arr != null)
					      {
					      	divHealthInfo.style.display='';			//结果不为空时，显示Mulline
					      	turnPage.queryModal(Sql,HealthInfoGrid);
					      }
					      else
					      	{
					      		window.open("LHConsultInput.jsp?logno="+null+"&IsCancelPolFlag=0");	
					      	}
						
				}
		  	else
		  	{ 

			    	var logno_data = HealthInfoGrid.getRowColData(tSel_health-1,1);
		  	  	window.open("LHConsultInput.jsp?logno="+logno_data+"&IsCancelPolFlag=0");	
		  	  	initPolGrid();easyQueryClick();
		 		}
//		 }
	}
}

//合同查询
function EdorQuery()
{	  
	
	
	 
		var arrReturn = new Array();
		var tSel = PolGrid.getSelNo();
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击返回按钮。" );
		else
		{
		    var ContNo = PolGrid.getRowColData(tSel - 1,1);		
		    var GrpContNo = PolGrid.getRowColData(tSel - 1,5);	
		     var ContType= PolGrid.getRowColData(tSel - 1,7);	   	 
		    var AppFlag= PolGrid.getRowColData(tSel - 1,8);
			if (ContNo=="")
			    
			    alert("请选择保单!");	
			    
			  if(AppFlag!="1")
			  {
			  	 alert("无有效保单，没有保全信息!");	
			  	 return; 
			  	}  
		    window.open("EndorQurey.jsp?ContNo="+ContNo+"&GrpContNo="+GrpContNo+"&ContType="+ContType);
	}
	
}



// 查询按钮
function easyQueryClick()
{
  var comCode = fm.ComCode.value;
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	if ((fm.ContNo.value==null||fm.ContNo.value=="")
	    &&(fm.InsuredNo.value==null||fm.InsuredNo.value=="")
	    &&(fm.InsuredName.value==null||fm.InsuredName.value=="")
	    &&(fm.PrtNo.value==null||fm.PrtNo.value=="")
	    &&(fm.IDNo.value==null||fm.IDNo.value=="")
	    &&(fm.AgentName.value==null||fm.AgentName.value=="")) {
		alert("请输入查询条件！");
		return false;
	}
	
	strSQL = "select a.ContNo,a.PrtNo,a.Name,a.InsuredNo,a.GrpContNo,getUniteCode(c.agentcode),c.ContType,c.appflag "
				+ " from LCInsured a,  LCCont c  where 1=1 and a.ContNo = c.ContNo "// and c.AgentCode = b.AgentCode"
		    + getWherePart("a.ContNo", "ContNo")
		    + getWherePart("a.InsuredNo", "InsuredNo")
	      + getWherePart("a.Name", "InsuredName")
	      + getWherePart("a.IDNo","IDNo")
	      + getWherePart("c.agentcode","AgentName")
	      + getWherePart("c.PrtNo","PrtNo")
	      + " and c.managecom like '"+comCode+"%%' with ur" 
	      ;
	      // alert(strSQL);
//      + getWherePart( 'AgentName' )
//  if(fm.InsuredNo.value.trim()!="")strSQL = strSQL + if(fm.InsuredNo.value.trim()!="")strSQL = strSQL + getWherePart( 'InsuredNo' );;
// else if(fm.IDNo.value.trim()!="")strSQL = strSQL + "and InsuredNo in (select InsuredeNo from LcinsuredNo where IDNo = '"+fm.IDNo.value.trim()+"' and IDType = 1)";
// strSQL = strSQL+ getWherePart( 'InsuredName' )        				 
//            + " and ManageCom like '" + comCode + "%%'"
//	            + "order by ContNo";
	           
	  //          t(turnPage);
	//if(turnPage==undefined)
	//    var turnPage = new turnPageClass();

	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  divHealthInfo.style.display='none';
  initHealthInfoGrid();
   
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //showCodeName();
}




// 数据返回父窗口
function returnParent()
{
	//alert(tDisplay);
	if (tDisplay=="1")
	{
	    var arrReturn = new Array();
	    var tSel = PolGrid.getSelNo();
	    //alert(tSel);
	    if( tSel == 0 || tSel == null )
	    	alert( "请先选择一条记录，再点击返回按钮。" );
	    else
	    {
	    	try
	    	{
	    		arrReturn = getQueryResult();
	    		
	    		top.opener.afterQuery( arrReturn );
	    		
	    		top.close();
	    	}
	    	catch(ex)
	    	{
	    		alert( "请先选择一条非空记录，再点击返回按钮。");
	    		
	    	}
	    	
	    }
	 }
	 else
	 {
	    top.close(); 
	 }
}

function getQueryResult()
{
	var arrSelected = null;
	var tRow = PolGrid.getSelNo();
	
	if( tRow == 0 || tRow == null || arrDataSet == null )
		      return arrSelected;
	
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	
	return arrSelected;
}

function BlackQuery() {
	var tSel = PolGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
			alert( "请先选择一条记录。" );
	} else {
		customerno = PolGrid.getRowColData(tSel - 1,4);
		customername = PolGrid.getRowColData(tSel - 1,3);
		var varSrc = "?CustomerNo=" + customerno;
		varSrc += "&CustomerName=" + customername;
	  varSrc += "&CaseNo=" ;
		pathStr="./FrameMainBlackList.jsp?Interface=BlackListInput.jsp"+varSrc+"&LoadC=2";
		showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,330);
	}
}