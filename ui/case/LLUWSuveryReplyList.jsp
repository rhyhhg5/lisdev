<html>
	<%
	//Name：LLSuveryReplyList.jsp
	//Function：调查员回复列表（调查员信箱）
	//Date：2005-12-23 16:49:22
	//Author：wujs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@page import="java.util.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String Operator=tG.Operator;
	String Comcode=tG.ManageCom;

	String CurrentDate= PubFun.getCurrentDate();
	String AheadDays="-30";
	FDate tD=new FDate();
	Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
	FDate fdate = new FDate();
	String afterdate = fdate.getString( AfterDate );
	%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLUWSuveryReplyList.js"></SCRIPT>
		<%@include file="LLUWSuveryReplyListInit.jsp"%>
		<script language="javascript">
		</script>
	</head>

	<body  onload="initForm();" >
		<form action="" method=post name=fm target="fraSubmit">
			<%@include file="../common/jsp/InputButton.jsp"%>
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQueryPart);">
					</TD>
					<TD class= titleImg>
						调查工作查询条件
					</TD>
				</TR>
			</table>
			<Div  id= "divQueryPart" style= "display: ''">
				<table  class= common>
				<TR  class= common8>
						<TD  class= title8>案件号</TD><TD  class= input8><input class= common name="OtherNo" onkeydown="QueryOnKeyDown()"></TD>
					</TR>
				</table>
			</DIV>

			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSurveyList);">
					</TD>
					<TD class= titleImg>
						调查工作列表
					</TD>
				</TR>
			</table>
			<Div  id= "divSurveyList" align  = center style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanCheckGrid" >
							</span>
						</TD>
					</TR>
				</table>
				<INPUT VALUE="首   页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
				<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
				<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
				<INPUT VALUE="尾   页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
			</Div>
			<br>
			<input type=button class=cssButton style="width:70px" name="prtRpt" value="调查报告" onclick="SurveyPrint()">
			
			<!--隐藏域-->
			<Input type="hidden" class= common name="fmtransact" >
			<Input type="hidden" class= common name="tSurveyNo" >
			<Input type="hidden" class= common name="comcode" >
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
