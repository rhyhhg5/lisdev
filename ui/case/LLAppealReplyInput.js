//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var temp = 1;
          //使用翻页功能，必须建立为全局变量

var turnPage = new turnPageClass();

var tRowNo=0;

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");


  if (fm.ClmNo.value=="")
  {
  	alert("请先作赔案查询");
  	return;
  }

  showSubmitFrame(mDebug);
  fm.submit(); //提交

}

//提交，保存按钮对应操作
function submitForm1()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

//  showSubmitFrame(mDebug);
//  fm1.submit(); //提交
	window.open("./FrameClaimQueryUW.jsp?Flag=3");
}

//提交，保存按钮对应操作
function submitForm2()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

//  showSubmitFrame(mDebug);
//  fm1.submit(); //提交
    window.open("./FrameClaimQueryUW.jsp?Flag=1");

}

function submitForm3()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

//  showSubmitFrame(mDebug);
//  fm1.submit(); //提交
	window.open("./FrameClaimQueryUW.jsp?Flag=2");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //showDiv(operateButton,"false");
  //showDiv(inputButton,"true");
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModelessDialog("./FrameProposalQuery.jsp",window,"dialogWidth=15cm;dialogHeight=12cm");
  //window.open("./FrameClaimUnderwriteQuery.jsp");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}
//Click事件，当点击“选择责任”按钮时触发该函数
function chooseDuty()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  if(cPolNo=="")
  {
     alert("您必须先录入投保单号才能修改该投保单的责任项。");
     return false
  }
//  showModalDialog("./FrameMain.jsp?Interface=ChooseDutyInput.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
  window.open("./FrameMain.jsp?Interface=ChooseDutyInput.jsp&PolNo="+cPolNo);
  return true
}

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  if(cPolNo=="")
  {
     alert("您必须先录入投保单号才能察看该投保单的责任项。");
     return false
  }
  showModalDialog("./ProposalDuty.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=14cm");
//  window.open("./ProposalDuty.jsp?PolNo="+cPolNo);
}

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  cPolNo=fm.ProposalNo.value;
  //showModalDialog("./ProposalFee.jsp?PolNo="+cPolNo,window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");
  window.open("./ProposalFee.jsp?PolNo="+cPolNo);

}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function ShowErrorInfo()
{
	initClaimErrorGrid();

	// 书写SQL语句
	var strSQL = "";

	var tPolNo=ClaimUWDetailGrid.getRowColData(tRowNo-1,1);
	strSQL = "select PolNo,ClmNo,UWRuleCode,UWError,CurrValue from LLClaimError where PolNo='"+ tPolNo + "' "
				+ " and " + ++temp + "='" + temp + "'"
				 + getWherePart( 'ClmNo' );

	  turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	  //判断是否查询成功
	  if (!turnPage.strQueryResult) //未查询到信息
	  {
	    //查询成功则拆分字符串，返回二维数组
	  	//alert("无自动核赔信息");
	  	return false;

	  }
	  else
	  {

	  	arrGrid = turnPage.arrDataCacheSet;

	  	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	  	turnPage.pageDisplayGrid = ClaimErrorGrid;

	  	//保存SQL语句
	  	turnPage.strQuerySql     = strSQL;

	  	//设置查询起始位置
	  	turnPage.pageIndex       = 0;

	  	//在查询结果数组中取出符合页面显示大小设置的数组
	  	turnPage.arrDataCacheSet=decodeEasyQueryResult(turnPage.strQueryResult);
	  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	  	alert("bbb=="+arrDataSet);

	  	//调用MULTILINE对象显示查询结果
	  	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	  	ClaimUWDetailGrid.setRowColData(tRowNo-1,5,"0");

	   }
	return true;
}


function Auto_chk()
{
	fm.Opt.value="Autochk";
	tRowNo = ClaimUWDetailGrid.getSelNo();
	if (tRowNo==0)
	{
		alert("请选择需要自动核赔的保单");
		return;
	}

	if (ShowErrorInfo())
	{

		alert("已经自动核赔");
		return;
	}

	var i = 0;
  	var showStr="正在核赔数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//fm.target="_blank";
	fm.submit(); //提交

}

function Man_chk()
{
	fm.Opt.value="Manchk"
	tRowNo = ClaimUWDetailGrid.getSelNo();
	if (tRowNo==0)
	{
		alert("请选择需要人工核赔的保单");
		return;
	}

	var ManOpn=ClaimUWDetailGrid.getRowColData(tRowNo-1,6);
	if (ManOpn =="")
	{
		alert("请录入核赔意见");
		return;
	}

	ShowErrorInfo();

	var i = 0;
  	var showStr="正在核赔数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//fm.target="_blank";
	fm.submit(); //提交
}

function Gf_ensure()
{


	fm.Opt.value="Gf_ensure";
	var i = 0;
  	var showStr="正在核赔数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}

function Chk_ensure()
{

	fm.Opt.value="Chk_ensure";
	var i = 0;
  	var showStr="正在核赔数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}

function ReChk_ensure()
{

	fm.Opt.value="ReChk_ensure";
	var i = 0;
  	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交

}

function Chk_Query()
{
      varSrc = "&RgtNo=" + fm.RgtNo.value;
      var newWindow = window.open("./FrameMainFABD.jsp?Interface=QueryCheck.jsp"+varSrc,"QueryCheck",'width=800,height=500,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');

}


function submitFormSurvery()
{
  if (CaseGrid.getSelNo()==0)
  {
    alert("请您选择一个分案！");
    return;
  }

  if (CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1)=="")
  {
    alert("分案号为空");
    return;
  }
  var varSrc = "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
  varSrc += "&InsuredNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
  varSrc += "&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&StartPhase=2";
  varSrc += "&Type=1";
  var newWindow = window.open("./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc,"RgtSurveyInput",'toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

//进行二次核保函数
function SecondUW()
{
  if (CaseGrid.getSelNo()==0)
  {
    alert("请您选择一个分案！");
    return;
  }

  if (CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1)=="")
  {
    alert("分案号为空");
    return;
  }

  var varSrc = "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
  varSrc += "&InsuredNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
  varSrc += "&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&Type=1";
  var newWindow = window.open("./FraimSecondUW.jsp?Interface=SecondUWInput.jsp"+varSrc,"SecondUWInput",'width=700,height=500,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}
//解约暂停函数
function EndAgreement()
{
  if (CaseGrid.getSelNo()==0)
  {
    alert("请您选择一个分案！");
    return;
  }

  if (CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1)=="")
  {
    alert("分案号为空");
    return;
  }
  var varSrc = "&CaseNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,1);
  varSrc += "&InsuredNo=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,2);
  varSrc += "&CustomerName=" + CaseGrid.getRowColData(CaseGrid.getSelNo()-1,3);
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&Type=1";
  var newWindow = window.open("./FraimEndAgreement.jsp?Interface=EndAgreementInput.jsp"+varSrc,"EndAgreementInput",'width=700,height=500,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}