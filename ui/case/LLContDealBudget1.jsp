<html> 
<% 
//程序名称：LLContDealBudget1.jsp
//程序功能：合同处理解约试算
//创建日期：2013-02
//创建人  ：
//更新记录: 更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  String CaseNo=request.getParameter("CaseNo");
%>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  
  <SCRIPT src="../case/LLContDealBudget1.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLContDealBudgetInit.jsp"%>
  <title>终止退费试算</title>
</head>
<body  onload="initForm();" >
  <center><H2>合同终止退费试算<H2></center>
  <form action="./LLContDealBudgetSave.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 保单号</TD>
        <TD  class= input > 
          <Input class= common name=contNoTop onkeydown="queryContOnKeyDown();">
        </TD>
        <TD class = title > 投保人客户号 </TD>
        <TD class = input >
        	<Input class= common name=appntNoTop onkeydown="queryContOnKeyDown();">
        </TD>
        <TD class = title > 投保人姓名 </TD>
        <TD class = input >
        	<Input class= common name=appntNameTop onkeydown="queryContOnKeyDown();">
        </TD>
        <TD class = title >
          <Input type =button class=cssButton value="查  询" onclick="queryCont();">
        </TD>
      </TR>
    </TABLE>
    
    <table>
	  	<tr>
	        <td>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
	        </td>
	        <td class= titleImg> 有效保单信息 </td>
	  	</tr>
    </table> 
    <Div  id= "divContGrid" style= "display: ''">
      <table  class= common>
      	<tr  class= common>
        		<td text-align: left colSpan=1>
      			<span id="spanLCContGrid" >
      			</span> 
      	  	</td>
      	</tr>
      </table>	
      <Div id= "divPage" align="center" style= "display: 'none' ">	  		
        <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="turnPage1.firstPage();"> 
        <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
        <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="turnPage1.nextPage();"> 
        <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="turnPage1.lastPage();"> 
      </Div>
    </Div>
    
    <table>
    	<tr>
    		<td class= titleImg>
    			 保单信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divContInfo" style= "display: ''">
      <table  class= common>
        <TR>
          <TD  class= title > 保单号</TD>
          <TD  class= input > 
            <input class="readonly" readonly name=contNo >
          </TD>
          <TD  class= title>
            投保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" name=appntName readonly >
          </TD>  
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" name=appntNo readonly >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            投保日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=polApplyDate readonly >
          </TD>
          <TD  class= title>
            生效日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=cValiDate readonly >
          </TD>
      
          <TD  class= title>
            交至日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=payToDate readonly >
          </TD>      
        </TR>
        <TR  class= common>
          <TD  class= title>
            满期日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=cInValiDate readonly >
          </TD> 
<!--          <TD  class= title>
            缴费频次
          </TD>
          <TD  class= input>
            <Input class="readonly" name=payIntv readonly >
          </TD>  
-->          
          <TD  class= title>
            期交保费
          </TD>
          <TD  class= input>
            <Input class="readonly" name=prem readonly >
          </TD>     
        </TR>
        <TR class= common>
          <TD class= title colspan=6>
            
          </TD>    
        </TR>
        <TR class= common>
          <TD class= title>
            理赔情况
          </TD> 
          <TD class= title>
            <Input class="readonly" name=claimState readonly >
          </TD> 
          <TD class= title>
            保全情况
          </TD>
          <TD class= title colspan=3>
            <Input class="readonly3" name=edorState readonly >
          </TD>
        </TR>
      </table>
    </Div> 
     
    <table>
      <tr>
        <td class=common>
          <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCPol);">
        </td>
        <td class=titleImg>险种选择</td>
      </tr>
    </table>
    <Div id="divLCPol" style="display: ''">
      <table class=common>
        <tr class=common>
          <td text-align: left colSpan=1>
            <span id="spanLCPolGrid">  </span>
          </td>
        </tr>
      </table>
      <Div id= "divPage2" align="center" style= "display: 'none' ">	  		
        <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="turnPage2.lastPage();"> 
      </Div>
    </div>
    <div  id= "divUliInfo" style= "display: 'none'">
      <table  class= common>
       <tr class= common>
          <td  class= common >
            <font color="#FF0000">提示：因追加保费、部分领取等影响帐户价值的操作，可能会造成该试算结果与实际解约金额不同。</font>
          </td>  
        </tr>
       </table>
    </div>
     <table class=common>
      <TR  class= common> 
        <TD  class= title > 预计退费生效日期 </TD>
        <TD  class= input id="divEdorValiDate"> 
          <Input class= "coolDatePicker" dateFormat="short" name=edorValiDate >
        </TD>
        <TD  class= input id="divEdorValiDatePassIn" style="display: none"> 
          <Input class= "readonly" readonly name=edorValiDatePassIn >
        </TD>
        <TD class = title> 长期险预计交至日期 </TD>
        <TD class = input>
        	<Input class= "coolDatePicker" dateFormat="short" name=payToDateLongPol >
        </TD>
        <TD class = title >合计退费</TD>
       <!-- <TD class = input  dateFormat="short" name=GetMoney > -->
      <TD> <Input class="input" name=GetMoney readonly > </TD>
      </TR>
      <!--
      <tr>
      <TD CLASS=title>贷款银行</TD>
			<TD CLASS=input>
				<Input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=0  ondblclick="return showCodeList('bank',[this,BankCodeName], [0,1]);" onkeyup="return showCodeListKey('bank',[this,BankCodeName], [0,1]);" ><Input NAME=BankCodeName VALUE="" CLASS="codeName" readonly >
				<input name="BankCodeBak" type=hidden>
			</TD>
			<TD class= title >审核人</TD>
      <TD> <Input class="input" name=ApproveCode  > </TD>

      </tr>
      -->
    </TABLE>   
    
    <br>
    <Input type =button class=cssButton value="终止退费试算" onclick="edorBudget()">
    <Input class=cssButton type=Button value="万能保单事项" name="Omni" id ="Omni" onclick="showOmniInfo();">
    <Input type =button class=cssButton value="返  回" onclick="returnParent()">
    <input type=hidden id="CaseNo" name="CaseNo" value="<%=CaseNo%>">
    <input type=hidden id="edorNo" name="edorNo">
	<input type=hidden id="edorType" name="edorType">
	<div id = "test"></div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
