<%
//程序名称：
//程序功能：导入提交页面
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="java.net.*"%>
<%

String FlagStr = "Fail";
String Content = "";
int count = 0;
String FileName = "";
String OutPath ="";
//得到xml文件的保存路径
String ImportPath = request.getParameter("ImportPath");
System.out.println("ImportPath: "+ImportPath);
//获取到根目录
String path = application.getRealPath("").replace('\\','/')+'/';  //application.getRealPath("")取到的路径是用"\"分隔的
System.out.println("path: "+path);
//获取系统保存路径
	String str="select sysvarvalue from LDSysVar where sysvar='LLPXmlPath1'";
OutPath = new ExeSQL().getOneValue(str);
	System.out.println("OutPath:" + OutPath);
	OutPath = path + OutPath;
	
DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
fu.setSizeMax(10000000);

// 设置最多只允许在内存中存储的数据,单位:字节
fu.setSizeThreshold(4096);

// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(path+"temp");
//开始读取上传信息
List fileItems = null;

try{
	  fileItems = fu.parseRequest(request);
	}
	catch(Exception ex){
	  ex.printStackTrace();
	}

// 依次处理每个上传的文件
Iterator iter = fileItems.iterator();

while (iter.hasNext()) {
  FileItem item = (FileItem) iter.next();
  //忽略其他不是文件域的所有表单信息
  if (!item.isFormField()) {
    String name = item.getName();
    long size = item.getSize();
    if((name==null||name.equals("")) && size==0)
      continue;
    ImportPath= path + ImportPath;
    
    
    //对上传的XML文件归档
    if(!PubDocument.save(ImportPath)){
    	FlagStr = "Fail";
        Content = "文件归档失败";
        break;
    }

    ImportPath = PubDocument.getSavePath(ImportPath);//实际归档的xml文件路径
    OutPath = PubDocument.getSavePath(OutPath);
    
    FileName = name.substring(name.lastIndexOf("\\") + 1);
    System.out.println("Importpath+FileName:"+ImportPath + FileName);
    System.out.println("Outpath+FileName:"+OutPath + FileName);
    

    //保存上传的文件到指定的目录
    try {
      item.write(new File(ImportPath + FileName));
      count = 1;
    }
    catch(Exception e) {
      System.out.println("upload file error ...");
    }
  }
}
System.out.println("upload successfully");

//输出参数
GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	
	TransferData tTransferData = new TransferData();
boolean res = true;

testTJJHClaim  tTJJHClaim = new testTJJHClaim();

if(count >0){
	//准备传输数据 VData 
	VData tVData = new VData();
	FlagStr="";
	 tTransferData.setNameAndValue("FileName", FileName);
	 tTransferData.setNameAndValue("FilePath", ImportPath);
	 tTransferData.setNameAndValue("OutFilePath", OutPath);
	 tVData.add(tTransferData);
	 tVData.add(tG);
	 try{
		 if(!tTJJHClaim.submitData(tVData, "")){    //提取案件信息
		        Content = "保存失败，原因是:"+tTJJHClaim.mErrors.getFirstError() ;
		        FlagStr = "Fail";
		        res= false;
		      }
	 }catch(Exception ex){
		 Content = "保存失败，原因是:" + ex.toString();
	     FlagStr = "Fail"; 
	 }
}else{
	 Content += "上载文件失败! ";
	 FlagStr = "Fail";
}
if (FlagStr.equals("Fail")){
    res=false;
  }

  if (res){
    Content = " 提交成功! ";
    FlagStr = "Succ";
  }
  else{
    Content = " 保存失败，原因是:" + Content;
    FlagStr = "Fail";
  }
  
  if("Succ".equals(FlagStr)){
	  //用户下载到自己选定的目录
	  	BufferedOutputStream output = null;
		BufferedInputStream input = null;  
  		try
		{
			//filePath = new String(filePath.getBytes("ISO-8859-1"),"gbk");
			String  filePath = OutPath +FileName ;
			System.out.println("filePath :" + filePath);
			File file = new File(filePath);
			System.out.println("下载地址是----》》》"+filePath+"==|||"+file.getName());
			response.reset();
			response.setContentType("application/octet-stream"); 
			response.setHeader("Content-Disposition","attachment; filename=" + URLEncoder.encode(FileName,"GBK"));
			response.setContentLength((int) file.length());

			byte[] buffer = new byte[4096];  
			//写缓冲区
			output = new BufferedOutputStream(response.getOutputStream());
			input = new BufferedInputStream(new FileInputStream(file));

			int len = 0;
			while((len = input.read(buffer)) >0)
			{
				output.write(buffer,0,len);
			}
			Content = "下载成功！";
			out.clear();
			out = pageContext.pushBody();

			input.close();
			output.close(); 
		}
		catch(FileNotFoundException fe)
		{
		
			Content = "没有要下载的文件，请确认文件是否存在！";
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			System.out.println("下载失败！");
			
			Content = "下载失败";
		}
		finally {
			if (input != null) input.close();
			
			if (output != null) output.close();
		}  
}
%>
<html>
<script language="javascript">
  parent.fraInterface.UploadiFrame.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html> 