<html>
<%
//程序名称：LLExemptionInput.jsp
//程序功能：豁免保费
//创建日期：
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LLAddFeeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLAddFeeInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
  <form  action='./LLAddFeeSave.jsp' method=post name=fm target="fraSubmit">
    <table>
      <tr>
	      <td>
  	      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLCase1);">
    	  </td>
      	<td class= titleImg>
      	  案件基本信息
      	</td>
    	</tr>
    </table>

  <Div  id= "divLLCase1" style= "display: ''">
		<table  class= common>
			<TR  class= common id= "divRgt" style= "display: 'none'">
				<TD  class= title>
					团体批次号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=RgtNo >
				</TD>
				<TD  class= title>
					团体客户号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=GrpNo >
				</TD>
				<TD  class= title>
					单位名称
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=GrpName  >
				</TD>
			</TR>
			<TR  class= common id= 'divCase'>
				<TD  class= title>
					理赔号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=CaseNo >
				</TD>
				<TD  class= title>
					合同号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=ContNo >
				</TD>
			</TR>
			<TR>
				<TD  class= title>
					客户号
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=CustomerNo >
				</TD>
				<TD  class= title>
					客户姓名
				</TD>
				<TD  class= input>
					<Input class= "readonly" readonly name=CustomerName  >
				</TD>
			</TR>
     </table>
    </div>
<DIV id=DivLCPol STYLE="display:''">
    <table class= common border=0 width=100%>
    	 <tr>
	        <td class= titleImg align= center>险种信息：</td>
	     </tr>
    </table>
    <table  class= common>
       <tr  class= common>
      	 <td text-align: left colSpan=1>
  					<span id="spanRiskGrid" >
  					</span>
  			 </td>
  		 </tr>
    </table>
</DIV>
    <Div  id= "divClaimPay" style= "display: ''">
    <table style= "display: ''">
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimPay);">
        </td>
        <td class= titleImg> 加费信息</td>
      </tr>
    </table>
    <table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
				<span id="spanAddFeeGrid" >
				</span>
			</td>
		</tr>
    </table>
  </div>
		<table  class= common>
      <TR  class= common>
       <TD  class= title colspan=6>
         加费原因
       </TD>
      </tr>
      <TR  class= common>
       <TD  class= input >
				<textarea name="Remark" cols="90%" rows="4" width=25% class="common"></textarea></TD>
	  	 </TD>
      </tr>  
     </table>
    <INPUT VALUE="确  认" TYPE=button class=cssbutton onclick="submitForm();">
    <INPUT VALUE="送  核" TYPE=button class=cssbutton onclick="submitForm();">
    <INPUT VALUE="返  回" TYPE=button class=cssbutton onclick="top.close();"> 
    	<input type=hidden id="fmtransact" name="fmtransact">			
    	<input type=hidden id="LoadFlag" name="LoadFlag">
          </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script language="javascript">
 
</script>
</html>
