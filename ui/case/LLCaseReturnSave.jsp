<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLCaseSave.jsp
//程序功能：
//创建日期：2005-02-23 11:53:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLCaseSchema tLLCaseSchema   = new LLCaseSchema();
  LLCaseBackSchema tLLCaseBackSchema = new LLCaseBackSchema();
  LLCaseReturnUI tLLCaseReturnUI   = new LLCaseReturnUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");

    tLLCaseSchema.setCaseNo(request.getParameter("CaseNo"));
    tLLCaseSchema.setHandler(request.getParameter("llusercode"));
    System.out.println("llusercode"+request.getParameter("llusercode"));
    tLLCaseSchema.setRgtState(request.getParameter("BackState"));
    System.out.println("RgtState"+request.getParameter("RgtState"));
    System.out.println("returnReason"+request.getParameter("returnReason"));
    System.out.println("remark"+request.getParameter("remark"));
    System.out.println("transact"+request.getParameter("fmtransact"));
    
    tLLCaseBackSchema.setCaseNo(request.getParameter("CaseNo"));
    tLLCaseBackSchema.setBeforState(request.getParameter("RgtState"));
    tLLCaseBackSchema.setAfterState(request.getParameter("BackState"));
    tLLCaseBackSchema.setOHandler(request.getParameter("upusercode"));
    tLLCaseBackSchema.setNHandler(request.getParameter("llusercode"));
    tLLCaseBackSchema.setReason(request.getParameter("returnReason"));
    tLLCaseBackSchema.setRemark(request.getParameter("remark"));
 
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
		tVData.add(tLLCaseSchema);
		tVData.add(tLLCaseBackSchema);
  	tVData.add(tG);
    tLLCaseReturnUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLLCaseReturnUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
