<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ClientReasonSaveMakeXml.jsp
//程序功能：
//创建日期：2012-02-13 08:49:52
//创建人  ：YJ
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
  <%@page import="com.sinosoft.lis.agentprint.*"%>
  <%@page import="java.io.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	//声明Schema Set
	
  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);
  	
	LDCode1Set tLDCode1Set = new LDCode1Set();
	
	//声明VData
	VData tVData = new VData();
	//声明后台传送对象
	ClaimConfigBL tClaimConfigBL   = new ClaimConfigBL();
	//输出参数
	CErrors tError = null;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String EventFlag="";	//事件申请标记 为1 有客户存在事件，为0不存在事件
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");

   /******************************************************/
     //申请原因
	String tNum[] = request.getParameterValues("claimReasonCode");
	String appResonName[] = {"姓名","身份证号","医保号","人员类别","性别","医院名称","医院编码","就诊类别","账单号码","账单日期","入院日期","出院日期","住院天数","费用总额","起付线","帐户支付","统筹支付","统筹自负","大额救助支付","大额救助自负","个人自付","部分自负","全自费","统筹自负","第三方支付","疾病诊断","疾病编码"};

	int AppReasonNum = 0;
	if (tNum != null) 
	{
		AppReasonNum = tNum.length;
	}

	/*将原因信息填充*/
	for (int i = 0; i < AppReasonNum; i++)	
	{  
    LDCode1Schema tLDCode1Schema = new LDCode1Schema();                              
 		tLDCode1Schema.setCode1(tNum[i]);        //原因代码                                                         
		//tLLAppClaimReasonSchema.setCaseNo(appResonName[i]);                //申请原因                                             
		tLDCode1Schema.setCode(request.getParameter("ManageCom")); 
		tLDCode1Schema.setComCode(request.getParameter("ManageCom")); 
		tLDCode1Schema.setCodeType("llimportfeeitem");		
		tLDCode1Set.add(tLDCode1Schema);
	}
                                      
  try                                 
  {                                   
  // 准备传输数据 VData               
	//VData传送
		System.out.println("<--Star Submit VData-->");
		tVData.add(tLDCode1Set);		
  	tVData.add(tG);
  	System.out.println("<--Into tClaimConfigBL-->");
    tClaimConfigBL.submitData(tVData,strOperate);
  } 
  catch(Exception ex)
  { 
    Content = "保存失败，原因是: " + ex.toString();
    FlagStr = "Fail";
  } 
    
    
    System.out.println("aa:"+FlagStr);
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if ("".equals(FlagStr))
  { 
    tError = tClaimConfigBL.mErrors;
    if (!tError.needDealError())
    {         
    System.out.println("bb");                 
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	tVData.clear();
    	tVData=tClaimConfigBL.getResult();              
    }
    else                                                                           
    {
    System.out.println("CCC"); 
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("FlagStr:"+FlagStr);
if("Success".equals(FlagStr)){
   	System.out.println("start Make XML!");
 	
	Xmlexport tXmlexport=new Xmlexport();
	String ManageCom = request.getParameter("ManageCom");
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("ManageCom",ManageCom);
    tVData.clear();
	tVData.addElement(tTransferData);
  
  	try{
    	if(!tXmlexport.submitData(tVData,"")){
       	Content = "生成失败！";
       	FlagStr = "Fail";    
    	} else {
       	Content = "操作成功";
    	}
		}
		catch(Exception ex)
		{    	
    		FlagStr = "Fail";
  	}
}else{

}
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>