//程序名称：TaskLetterTop.js
//程序功能：工单管理查看顶部操作按钮
//创建日期：2005-08-25
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

//新建函件
function create()
{
	parent.fraInterface.create();
}

//下发函件
function sendLetter()
{
	parent.fraInterface.sendLetter();
}

//函件回销
function takeBack()
{
	parent.fraInterface.takeBack();
}


//强制回销
function forceTakeBack()
{
	parent.fraInterface.forceTakeBack();
}

//察看扫描件
function viewScan()
{
	parent.fraInterface.viewScan();
}

function printLetter()
{
    parent.fraInterface.printLetter();
}

function deleteLetter()
{
    parent.fraInterface.deleteLetter();
}