<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：LLPrepaidClaimUnderWriteSave.jsp
//程序功能：预付赔款审批
//创建日期：2010-11-30
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>

<%
 //接收信息，并作校验处理。
 
 //输入参数
 //××××Schema t××××Schema = new ××××Schema();
 LLPrepaidClaimConfirmUI tLLPrepaidClaimConfirmUI = new LLPrepaidClaimConfirmUI();
 LLPrepaidUWMainSchema tLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
 LLPrepaidClaimSchema tLLPrepaidClaimSchema = new LLPrepaidClaimSchema();
 
 //输出参数
 CErrors tError = null;             
 String FlagStr = "";
 String Content = "";
 String transact = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 
 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 transact = request.getParameter("fmtransact");
 System.out.println(request.getParameter("tPrepaidNo"));
 tLLPrepaidUWMainSchema.setPrepaidNo(request.getParameter("tPrepaidNo"));
 
 String tRadio[] = request.getParameterValues("InpPreClaimGridSel"); 
 for(int index =0 ; index<tRadio.length; index++)
 {
	 if(tRadio[index].equals("1"))
	 {
		 String tPayMode[] = request.getParameterValues("PreClaimGrid8");
		 String tBankCode[] = request.getParameterValues("PreClaimGrid10");
		 String tBankAccNo[] = request.getParameterValues("PreClaimGrid12");
		 String tAccName[] = request.getParameterValues("PreClaimGrid13");
		 String tPrepaidNo[] = request.getParameterValues("PreClaimGrid2");
		 System.out.println(tPrepaidNo[index]);
		 tLLPrepaidClaimSchema.setPayMode(tPayMode[index]);
		 tLLPrepaidClaimSchema.setBankCode(tBankCode[index]);
		 tLLPrepaidClaimSchema.setBankAccNo(tBankAccNo[index]);
		 tLLPrepaidClaimSchema.setAccName(tAccName[index]);
		 tLLPrepaidClaimSchema.setPrepaidNo(tPrepaidNo[index]);
	 }
 }
 try
 {
  //准备传输数据VData
  VData tVData = new VData(); 
  tVData.add(tG);
  tVData.addElement(tLLPrepaidUWMainSchema);
  tVData.addElement(tLLPrepaidClaimSchema);
  tLLPrepaidClaimConfirmUI.submitData(tVData,transact);
 }
 catch(Exception ex)
 {
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr=="")
 {
  tError = tLLPrepaidClaimConfirmUI.mErrors;
  if (!tError.needDealError())
  {                          
   Content = " 保存成功! ";
   FlagStr = "Success";
  }
  else                                                                           
  {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

