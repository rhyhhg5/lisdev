//程序名称：LLSecondUW.js
//程序功能：合同审查
//创建日期：2006-01-01 15:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	fm.GiveEnsure.disabled=false;
  showInfo.close();
  if (FlagStr == "Fail" )
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content); 
  } else { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    if (fm.Operate.value!="CONFIRM") {
    	UnDoCaseSearch();
    }
    HospGetSearch();
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
    cDiv.style.display="";
  else
    cDiv.style.display="none";  
}

function HospGetSearch() {
	var strSQL = "select a.managecom,(select name from ldcom where comcode=a.managecom),"
	           + "a.hospitcode,(select hospitname from ldhospital where hospitcode=a.hospitcode),"
	           + "a.paymode,a.bankcode,a.bankaccno,a.accname,"
	           + "(select codename from ldcode where codetype='llgetmode' and code=a.paymode),"
	           + "(select bankname From ldbank where bankcode=a.bankcode),"
	           + "(select case when cansendflag='1' then '是' else '否' end From ldbank where bankcode=a.bankcode)"
	           + " from llhospget a where hcno='"+fm.HCNo.value+"'";
	var drr= easyExecSql(strSQL);
	if (!drr) {
		alert("批次查询失败");
	}
	fm.ManageCom.value = drr[0][0];
	fm.ManageName.value = drr[0][1];
	fm.HospitalCode.value = drr[0][2];
	fm.HospitalName.value = drr[0][3];
	fm.paymode.value = drr[0][4];
	fm.BankCode.value = drr[0][5];
	fm.BankAccNo.value = drr[0][6];
	fm.AccName.value = drr[0][7];
	fm.paymodename.value = drr[0][8];
	fm.BankName.value = drr[0][9];
	fm.SendFlag.value = drr[0][10];
	
	if(fm.paymode.value=='1' || fm.paymode.value=='2') {
		divBankAcc.style.display='none';
  }
  	
	DoCaseSearch();
}

function UnDoCaseSearch()
{
	if(fm.EndCaseDataS.value=="" || fm.EndCaseDataE.value=="")
	{
		alert("查询未给付案件信息时必须录入结案起期和结案止期");
		return ;
	}
	var strSQL = "select b.mngcom,a.hospitcode,"
	           + "(select hospitname from ldhospital where hospitcode=a.hospitcode),"
	           + "a.caseno,b.customerno,b.customername,a.realpay,"
	           +" (CASE WHEN b.rgtstate in ('09') THEN "
	    	   +" (SELECT NVL(SUM(-pay),0) FROM ljagetclaim m WHERE m.otherno=b.caseno AND m.othernotype='Y' ) "
	    	   +" ELSE 0 END), "
	    	   +" (CASE WHEN b.rgtstate in ('09') THEN "
	    	   +" (SELECT NVL(SUM(sumgetmoney),0) FROM ljaget m where m.othernotype in ('Y','5') AND m.otherno=b.caseno) "
	    	   +" ELSE 0 END) "
	           + " from llhospcase a,llcase b where a.caseno=b.caseno"
	           + " and b.rgtstate='09' and (a.hcno is null or a.hcno = '')"
	           + " and a.hospitcode='"+fm.HospitalCode.value+"'"
	           + getWherePart('a.caseno','CaseNo')
	           + getWherePart('b.customerno','CustomerNo')
	           + getWherePart('b.customername','CustomerName')
	           + getWherePart('b.endcasedate','EndCaseDataS','>=')
	           + getWherePart('b.endcasedate','EndCaseDataE','<=');
	turnPage1.queryModal(strSQL, UnDoCaseInfo);
}

function DoCaseSearch()
{
	var strSQL = "select b.mngcom,a.hospitcode,"
	           + "(select hospitname from ldhospital where hospitcode=a.hospitcode),"
	           + "a.caseno,b.customerno,b.customername,a.realpay,"
	           +" (CASE WHEN b.rgtstate in ('09','11','12') THEN "
	    	   +" (SELECT NVL(SUM(-pay),0) FROM ljagetclaim m WHERE m.otherno=b.caseno AND m.othernotype='Y' ) "
	    	   +" ELSE 0 END), "
	    	   +" (CASE WHEN b.rgtstate in ('09','11','12') THEN "
	    	   +" (SELECT NVL(SUM(sumgetmoney),0) FROM ljaget m where m.othernotype in ('Y','5') AND m.otherno=b.caseno) "
	    	   +" ELSE 0 END), "
	           + "(select codename from ldcode where codetype='llrgtstate' and code=b.rgtstate)"
	           + " from llhospcase a,llcase b where a.caseno=b.caseno"
	           + " and b.rgtstate in ('09','11','12')"
	           + " and a.hcno='"+fm.HCNo.value+"'";
	
	turnPage2.queryModal(strSQL, DoCaseInfo);
	queryCaseGrid();
}

function addRecord() {
	var Count=UnDoCaseInfo.mulLineCount;
  if(Count==0){
  	alert("没有案件信息!");
  	return false;
  }
  
  var chkFlag=false;
  for (i=0;i<Count;i++){
    if(UnDoCaseInfo.getChkNo(i)==true){
    	if(UnDoCaseInfo.getRowColData(i,4)==""||UnDoCaseInfo.getRowColData(i,4)==null){
		    alert("第"+(i+1)+"行案件号为空!");	
				return false;
			}
      chkFlag=true;
    }
  }
  
  if (chkFlag==false){
    alert("请选择要添加的案件!");
    return false;
  }
  
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.Operate.value = "INSERT";
	fm.action="./LLHospGetDealSave.jsp"
  fm.submit(); //提交
}
function addAllRecord()
{
	if(fm.EndCaseDataS.value=="" || fm.EndCaseDataE.value=="")
	{
		alert("查询未给付案件信息时必须录入结案起期和结案止期");
		return ;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.Operate.value = "ALLINSERT";
	fm.action="./LLHospGetDealSave.jsp"
  	fm.submit(); //提交
}
function deleteRecord() {
	var Count=DoCaseInfo.mulLineCount;
  if(Count==0){
  	alert("没有案件信息!");
  	return false;
  }
  
  var chkFlag=false;
  for (i=0;i<Count;i++){
    if(DoCaseInfo.getChkNo(i)==true){
    	if(DoCaseInfo.getRowColData(i,4)==""||DoCaseInfo.getRowColData(i,4)==null){
		    alert("第"+(i+1)+"行案件号为空!");	
				return false;
			}
      chkFlag=true;
    }
  }
  
  if (chkFlag==false){
    alert("请选择要取消的案件!");
    return false;
  }
  
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.Operate.value = "DELETE";
	fm.action="./LLHospGetDealSave.jsp"
  fm.submit(); //提交
}

function caseConfirm() {
	var Count=DoCaseInfo.mulLineCount;
    if(Count==0){
  	    alert("没有案件信息!");
  	    return false;
    }
  
  //556反洗钱黑名单客户的理赔监测功能
  //#2407 理赔模块黑名单控制调整
  for (i=0;i<Count;i++){
    if(DoCaseInfo.getChkNo(i)==true){
		   var strblack=" select 1 from lcblacklist where trim(name)in('"+DoCaseInfo.getRowColData(i,3)+"') with ur";
   	       var crrblack=easyExecSql(strblack);
   	       if(crrblack){
    	      if(!confirm("领取医院为黑名单客户,是否继续处理此案件?")){
    	          return false;
    	      }
    	   }
    	//细化#2407 理赔模块黑名单控制调整==》#2684关于理赔环节黑名单监测规则修改的需求
       var checkName1 = DoCaseInfo.getRowColData(i,6);
       var checkcaseno1 = DoCaseInfo.getRowColData(i,4);
       var checkId1 =0;
       var strblack=" select IDNo from llcase where caseno= '"+ checkcaseno1 +"' with ur";
   	   var arrblack=easyExecSql(strblack);
   	   if(arrblack){
   	   	checkId1 = arrblack[0][0].trim();
   	   }
   	   if(!checkBlacklist(checkName1,checkId1)){
       	return false;
       }
    }
  }

  if (fm.paymode.value==null||fm.paymode.value=="") {
  	alert("领取方式不能为空!");
  	return false;
  }
  
  if (fm.paymode.value!="1" && fm.paymode.value!="2") {
  	if (fm.BankCode.value==null||fm.BankCode.value=="") {
  		alert("银行编码不能为空!");
  		return false;
  	}
  	
  	if (fm.BankAccNo.value==null||fm.BankAccNo.value=="") {
  		alert("银行账号不能为空!");
  		return false;
  	}
  	
  	if (fm.AccName.value==null||fm.AccName.value=="") {
  		alert("银行账户名不能为空!");
  		return false;
  	}
  }

  // start
  var PrepaidBalaSql="select sum(PrepaidBala) from LLPrepaidGrpCont a " 
 		+" where exists (select 1 from llclaimdetail " 
 		                + "where grpcontno=a.grpcontno " 
 		                + "and CaseNo in (select CaseNo from llhospcase where HCNo = '" + fm.HCNo.value + "'))";
  var tPrepaid=easyExecSql(PrepaidBalaSql);
  var tPrepaidBala;
  if(tPrepaid != null){
      tPrepaidBala = tPrepaid[0][0];
  }else{
	  tPrepaidBala = null;
  }
  
  if(fm.PrePaidFlag.checked == false && (tPrepaidBala !== "null" && tPrepaidBala > 0) )
  {
	  if(!confirm("确定案件不回销预付赔款?"))
	  {
		  fm.GiveEnsure.disabled=false;
		  return false;
      }	
  }  
  
  if(fm.PrePaidFlag.checked == true && (tPrepaidBala == "null" || tPrepaidBala <= 0) )
  {
	  alert("被保险人投保的保单不存在预付未回销赔款!");
 	  fm.GiveEnsure.disabled=false;
 	  return false;
  }
  
  if(fm.PrePaidFlag.checked == true )
  {
    var tRealpay = 0;
    var tPrepaid = 0;
    var tPay;
	var tGrpContSql="select distinct grpcontno "
                    + "from LLHospCase a, LLClaimDetail b "
                    + "where a.caseno = b.caseno and a.hcno = '"+ fm.HCNo.value +"' ";
	var tGrpCont=easyExecSql(tGrpContSql);
	//var tGrpContNo = tGrpCont[0][0]; //获取团单号
	
	for(j=0; j<tGrpCont.length; j++ ){
		  var Count=DoCaseInfo.mulLineCount;
		  var tRealpayTemp = 0;
		  var tPrepaidTemp = 0;
		  //var tPayTemp;
		  var tPrepaidbala
		  for (i=0; i<Count; i++){
			var strCaseno=DoCaseInfo.getRowColData(i,4);
		        var realpaySql = " select nvl(sum(b.realpay),0) realpay from llhospcase a, LLClaimDetail b " 
	                         + "where a.caseno = b.caseno and a.caseno = '"+ strCaseno +"' " 
	                         + "and b.grpcontno = '"+ tGrpCont[j] +"'";
		        var tRealpayArr = easyExecSql(realpaySql);
		        var tRealpayArrTemp;
		        if(tRealpayArr != null){
		        	tRealpayArrTemp = tRealpayArr[0][0];
		        }else{
		            tRealpayArrTemp = 0;
		        }		        
		        tRealpayTemp = parseFloat(tRealpayTemp) + parseFloat(tRealpayArrTemp);
		 }
		 
		 var prepaidSql = "select db2inst1.nvl(prepaidbala,0) prepaidbala from LLPrepaidGrpCont "
                         + "where grpcontno = '"+ tGrpCont[j] +"'";
		 var tprepaidbalaArr = easyExecSql(prepaidSql);
		 if(tprepaidbalaArr != null){
		     tPrepaidbala = tprepaidbalaArr[0][0];
		 }else{
			 tPrepaidbala = 0;
		 }
		 if(tRealpayTemp > tPrepaidbala){
			 tPrepaidTemp = tPrepaidbala;
		 }else{
			 tPrepaidTemp = tRealpayTemp
		 }
		 
		 tRealpay = parseFloat(tRealpay) + parseFloat(tRealpayTemp);
		 tPrepaid = parseFloat(tPrepaid) + parseFloat(tPrepaidTemp);
	}

    tPay = tRealpay - tPrepaid;
	tPay = formatFloat(tPay,2);
	tRealpay = formatFloat(tRealpay,2);
	tPrepaid = formatFloat(tPrepaid,2);
	if (tPrepaid > 0) {
	    if (!confirm("批次赔款"+tRealpay+",回销预付赔款"+tPrepaid+",实际给付"+tPay+"。是否继续")) {
	    	fm.RgtGiveEnsure1.disabled=false;
	        return false;
	    }
	}
  }
// end
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.Operate.value = "CONFIRM";
	fm.GiveEnsure.disabled=true;
	fm.action="./LLHospGetDealSave.jsp"
    fm.submit(); //提交
}

//小数点精度
function formatFloat(src, pos)
{
	var num = new Number(src);
	var result = num.toFixed(pos);
    return result;
}

//加载时判断是否勾选“回销预付赔款”
function queryCaseGrid() {
	var PrepaidBalaSql="select sum(PrepaidBala) from LLPrepaidGrpCont a " 
   		+" where exists (select 1 from llclaimdetail " 
   		                + "where grpcontno=a.grpcontno " 
   		                + "and CaseNo in (select CaseNo from llhospcase where HCNo = '" + fm.HCNo.value + "'))";
    var tPrepaid=easyExecSql(PrepaidBalaSql);
    var tPrepaidBala;
    if(tPrepaid != null){
        tPrepaidBala = tPrepaid[0][0];
    }else{
    	tPrepaidBala = null;
    }

    if(tPrepaidBala!="" && tPrepaidBala != null && tPrepaidBala > 0 && tPrepaidBala != "null")
    {
	    fm.PrePaidFlag.checked = true;	
    }else{
    	fm.PrePaidFlag.checked = false;
    }
}

function afterCodeSelect( cCodeName, Field ) {
  if( cCodeName == "paymode") {
    if(Field.value=="1"||Field.value=="2") {
      divBankAcc.style.display='none';
    } else {
      divBankAcc.style.display='';
    }
  }
}


//#2684关于理赔环节黑名单监测规则修改的需求
function checkBlacklist(checkName,checkId){
	//需要验证的证件号与人名都为空
	if((checkId==""||checkId==null)&&(checkName==""||checkName==null)){
		return true;
	}
	//需要验证的证件号为空
	if(checkId==""||checkId==null){
		var strblack="select 1 from lcblacklist where trim(name)in('"+checkName+"') and (idno='' or idno is null) with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   	   	  		//alert("若“证件号码”在黑名单库中为空");
   	   	  		if(!confirm("客户"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      		return false;
   	   	    	}
		}
		return true;
	}
	//需要验证的姓名为空
	if(checkName==""||checkName==null){
		var strblack=" select 1 from lcblacklist where trim(idno)in('"+checkId+"')with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   			//alert("姓名”不匹配为空，“证件号码”匹配");
   			if(!confirm("客户为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		return true;
	}
	
	//1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
	var strblack=" select name,idno from lcblacklist where trim(name)in('"+checkName+"') or trim(idno)in('"+checkId+"') with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   		var strblack1=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and trim(idno)in('"+checkId+"') with ur";
   		var arrblack1=easyExecSql(strblack1);
   		if(arrblack1){
   			alert("客户"+checkName+"为黑名单客户");
   	   	  	return false;
   		}
   		var strblack2=" select 1 from lcblacklist where trim(idno)in('"+checkId+"') with ur";
   		var arrblack2=easyExecSql(strblack2);
   		if(arrblack2){
   			if(!confirm("客户"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		var strblack3=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and (trim(idno)in('') or idno is null) with ur";
   		var arrblack3=easyExecSql(strblack3);
   		if(arrblack3){
   			if(!confirm("客户"+checkName+"为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
     }
     return true;
}

