<%
//Name:ReportInit.jsp
//function：
//author:Xx
//Date:2005-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">


function initForm()
{
  try
  {
    

    initCheckGrid();
//    classify();
    initClaimerGrid();
//		ClaimerCal();
  }  
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initCheckGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=3;

    iArray[1]=new Array("批次号","120px","0","0");   
    iArray[2]=new Array("理赔号","120px","0","0");
    iArray[3]=new Array("客户姓名","80px","0","0");
    iArray[4]=new Array("客户号","80px","0","0");
    iArray[5]=new Array("受理日期","80px","0","0");
    iArray[6]=new Array("当前录入人","80px","0","0");;
    iArray[7]=new Array("案件状态","100px","0","0");
    iArray[8]=new Array("案件状态代码","80px","0","3");
    iArray[9]=new Array("处理时效(天)","0px","0","3");

    CheckGrid = new MulLineEnter("fm","CheckGrid");
    CheckGrid.mulLineCount =8;
    CheckGrid.displayTitle = 1;
    CheckGrid.locked = 1;
    CheckGrid.canChk =1;
    CheckGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    CheckGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CheckGrid. selBoxEventFuncName = "onSelSelected";
    CheckGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

function initClaimerGrid()
{
  var iArray = new Array();
  try
  {
  	  iArray[0]=new Array();
      iArray[0][0]="序号";
  	  iArray[0][1]="0px";
  	  iArray[0][2]=10;
  	  iArray[0][3]=1;
      
  
			iArray[1]=new Array("录入人员代码","80px","0",0);
			iArray[2]=new Array("录入人员姓名","80px","0",3);
			iArray[3]=new Array("已录入案件数","60px","0",0);
			iArray[4]=new Array("已录入账单数","60px","0",0);
			iArray[5]=new Array("未录入案件数","60px","0",0);



    ClaimerGrid = new MulLineEnter("fm","ClaimerGrid");
    ClaimerGrid.mulLineCount =5;
    ClaimerGrid.displayTitle = 1;
    ClaimerGrid.locked = 1;
    ClaimerGrid.canSel =0;
    ClaimerGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ClaimerGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //  CheckGrid. selBoxEventFuncName = "onSelSelected";
    ClaimerGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}

 </script>