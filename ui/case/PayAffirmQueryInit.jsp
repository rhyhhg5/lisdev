<%
//程序名称：ReportQueryInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//单击时查询
function reportDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divDetailInfo.style.left=ex;
  	divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
}


function initForm()
{
  try
  {
		initInpBox();
		initPayAffirmGrid();
  }
  catch(re)
  {
    alert("PayAffirmQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initInpBox()
{ 

  try
  {                                   
	
	fm.all('MenuFlag').value = <%=request.getParameter("Flag")%>;
  }
  catch(ex)
  {
    alert("在ClaimQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 报案信息列表的初始化
function initPayAffirmGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="立案号";    	//列名
      iArray[1][1]="110px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="事故者姓名";    	//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="签批同意日期";         			//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      iArray[4]=new Array();
      iArray[4][0]="理赔结论";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      iArray[4][10]="CasePayType"; // 名字最好有唯一性
      iArray[4][11]= "0|^0|一般给付件|^1|短期给付件|^2|协议给付件|^3|责任免除件" ;

      iArray[5]=new Array();
      iArray[5][0]="审核人";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[6]=new Array();
      iArray[6][0]="管辖机构";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      PayAffirmGrid = new MulLineEnter( "fm" , "PayAffirmGrid" ); 
      //这些属性必须在loadMulLine前
      PayAffirmGrid.mulLineCount = 8;   
      PayAffirmGrid.displayTitle = 1;
      PayAffirmGrid.canSel=1;
      PayAffirmGrid.loadMulLine(iArray);  
      PayAffirmGrid.detailInfo="单击显示详细信息";
      PayAffirmGrid.detailClick=reportDetailClick;
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>