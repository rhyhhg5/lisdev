 
 
//使得从该窗口弹出的窗口能够聚焦 
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//磁盘导入
function importFile()
{

   var excelFile = fm.FileName.value;
      
       if(excelFile=='') {
       		alert("请选择需上传的文件!");
       		return false;
       	}
       if(excelFile.indexOf('.xls')==-1&&excelFile.indexOf('.xlsx')==-1){
       		alert("文件格式不正确，请选择正确的Excel文件(后缀名.xls)！");
       		return false;
       	}
       	
	document.all("Info").innerText = "上传文件导入中，请稍后...";
	submitForm();
}

//提交
function submitForm()
{
	fm.submit(); //提交
	
}

//执行导入后的处理
function afterSubmit(FlagStr, content)
{
   showInfo.close();
   window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    else
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		top.opener.focus();
		top.opener.location.reload();
		top.close();
		
    }
   	   
}

//模板下载
function downloadTemplate(){
	var	beforeAction =  fm.action;
    fm.action = "OutsourcingCostDownload.jsp";
    fm.submit();
   	fm.action=beforeAction;
}