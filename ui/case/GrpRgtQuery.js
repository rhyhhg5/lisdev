var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var tSaveType="";

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else
 	{
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}


function SearchGrpRegister()
{
	if ((fm.all('srRgtNo').value==null || fm.all('srRgtNo').value=="")
			&&(fm.all('srCustomerNo').value==null || fm.all('srCustomerNo').value=="")
			&&(fm.all('srGrpName').value.value==null || fm.all('srGrpName').value=="")
			&&(fm.all('srRgtantName').value==null || fm.all('srRgtantName').value=="")
			&&(fm.all('RgtDateStart').value==null || fm.all('RgtDateStart').value=="")
			&&(fm.all('RgtDateEnd').value==null || fm.all('RgtDateEnd').value=="")) {
		alert("请输入查询条件");
		return false;
	}
	
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  // 书写SQL语句
  var strSql = " SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO, R.RGTANTNAME, R.RGTDATE, R.APPPEOPLES, b.codename " + 
  	       " FROM LLREGISTER R , ldcode b " + 
  	       " WHERE R.RGTOBJ = '0' AND R.RGTCLASS = '1' " +
  	       " and b.codetype='llgrprgtstate' and b.code = r.rgtstate "
  	       
  if (fm.all('srRgtNo').value != '')	//团体批次号 
  	strSql = strSql + " AND R.RGTNO = '" + fm.all('srRgtNo').value + "'";  	       

  if (fm.all('srCustomerNo').value != '')	//客户号
  	strSql = strSql + " AND R.CUSTOMERNO = '" + fm.all('srCustomerNo').value + "'";
  if (fm.all('srGrpName').value != '')	//客户姓名
  	strSql = strSql + " AND A.GRPNAME = '" + fm.all('srGrpName').value + "'";
  if (fm.all('srRgtantName').value != '')	//申请人
  	strSql = strSql + " AND R.RGTANTNAME = '" + fm.all('srRgtantName').value + "'";  	
  if (fm.all('RgtState').value =='03')
  	strSql = strSql + " and r.rgtstate in ('03','04','05')";
	 	       
  //申请日期区间查询
  var blDateStart = !(fm.all('RgtDateStart').value == null || 
  			fm.all('RgtDateStart').value == "");
  var blDateEnd = !(fm.all('RgtDateEnd').value == null ||
  			fm.all('RgtDateEnd').value == "");
  
  if(blDateStart && blDateEnd) {
  	strSql = strSql + " AND R.RGTDATE BETWEEN '" + 
  			fm.all('RgtDateStart').value + "' AND '" +
  			fm.all('RgtDateEnd').value + "'";
  } else if (blDateStart) {
  	strSql = strSql + " AND R.RGTDATE > '" + fm.all('RgtDateStart').value + "'";
  } else if (blDateEnd) {
  	strSql = strSql + " AND R.RGTDATE < '" + fm.all('RgtDateEnd').value + "'";
  }

  turnPage.queryModal(strSql,GrpRegisterGrid);
  showInfo.close();	
}


// 将查询信息返回上一级页面
function returnParent()
{
  var tRow=GrpRegisterGrid.getSelNo();
  if(tRow==0)
  {
    alert("请您先选择要返回的数据！");
    return;
  }
	try
	{
		top.opener.document.all('GrpRgtNo').value=GrpRegisterGrid.getRowColData(tRow-1,1);
		top.opener.document.all('GrpName').value=GrpRegisterGrid.getRowColData(tRow-1,3);
//		top.opener.document.all('BatchNumber').value="1";
	}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口:" + ex.message );
		}
		top.close();
}

function queryGrpCaseGrid()
{
	var selno = GrpRegisterGrid.getSelNo();
	if (selno <= 0)
	{				
	      return ;
	}
	var jsRgtNo = GrpRegisterGrid.getRowColData(selno - 1, 1);	

	if(jsRgtNo == null || jsRgtNo == "") 
	{
		alert("团体批次号为空！");
		return;
	}
	 	
	  var strSql = " SELECT C.CASENO, C.CUSTOMERNO, C.CUSTOMERNAME, C.RGTDATE, b.codename" + 
	  	       " FROM LLCASE C, ldcode b  " + 
	  	       " WHERE C.RGTNO = '" + jsRgtNo + "'" +
	  	       " and b.codetype='llrgtstate' and b.code = c.rgtstate " +
	  	       " ORDER BY C.CASENO ";
	
	  turnPage2.queryModal(strSql,GrpCaseGrid);
	  showPage(this,divGrpCaseInfo);   	       
}

function QueryOnKeyDown()
{
	var keycode = event.keyCode;

	if(keycode=="13")
	{
		SearchGrpRegister();		
	}
}