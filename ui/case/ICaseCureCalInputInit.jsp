<%
/*******************************************************************************
 * Name     :ICaseInit.jsp
 * Function :初始化“立案－费用明细信息”的程序
 * Author   :LiuYansong
 * Date     :2003-7-23
 */
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  String LoadC="";
		if(request.getParameter("LoadC")!=null)
		{
			LoadC = request.getParameter("LoadC");
		}

%>
<script language="JavaScript">
  var turnPage = new turnPageClass();
function initForm()
{
  try
  {
   initICaseCureGrid();
  //initHospitalGrid();
        fm.LoadC.value="<%=LoadC%>";
    if (fm.LoadC.value=='2')
    {
     alert(fm.LoadC.value);
    divconfirm.style.display='none';}
  }  
    initFeeRelaGrid();
   
   // initSuccorGrid();
   // showCaseCureInfo();

  catch(re)
  {
    alert("ICaseCureInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


//门诊费用明细信息
function initFeeRelaGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="账单收据号";    	//列名
    iArray[1][1]="130px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="费用项目代码";    	//列名
    iArray[2][1]="90px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=2;
    iArray[2][4]="";

     iArray[3]= new Array("费用项目金额","80px","100","1");
     iArray[4]= new Array("先期给付金额","80px","100","1");
     iArray[5]= new Array("自费金额","80px","100","1");
     iArray[6]= new Array("不合理金额","80px","100","1");
     iArray[7]= new Array("实际赔付金额","80px","100","1");

    FeeRelaGrid = new MulLineEnter( "fm" , "FeeRelaGrid" );
    FeeRelaGrid.mulLineCount = 1;
    FeeRelaGrid.displayTitle = 1;
    FeeRelaGrid.canSel = 0;
    FeeRelaGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex.message);
  }
}





//门诊费用明细信息
function initICaseCureGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="账单收据号";    	//列名
    iArray[1][1]="130px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="医院代码";    	//列名
    iArray[2][1]="90px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=2;
    iArray[2][4]="FeeItemCode";

    iArray[3]=new Array();
    iArray[3][0]="费用项目类型代码";    	//列名
    iArray[3][1]="120px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="账单种类";    	//列名
    iArray[4][1]="120px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;

    iArray[5]=new Array();
    iArray[5][0]="账单属性";    	//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;

    iArray[6]=new Array();
    iArray[6][0]="账单类型";    	//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;
   
     iArray[7]= new Array("账单日期","80px","100","1");
    
    ICaseCureGrid = new MulLineEnter( "fm" , "ICaseCureGrid" );
    ICaseCureGrid.mulLineCount = 1;
    ICaseCureGrid.displayTitle = 1;
    ICaseCureGrid.canSel = 0;
    ICaseCureGrid.hiddenPlus=1;   
    ICaseCureGrid.hiddenSubtraction=1;
    ICaseCureGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex.message);
  }
}

//初始化按档次录入初始化信息
function initDegreeOperationGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";

    iArray[1]=new Array();
    iArray[1][0]="手术代码";    	//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;

    iArray[2]=new Array();
    iArray[2][0]="手术名称";    	//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=10;            			//列最大值
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]="手术费用";    	//列名
    iArray[3][1]="90px";            		//列宽
    iArray[3][2]=10;            			//列最大值
    iArray[3][3]=1;

    iArray[4]=new Array();
    iArray[4][0]="手术档次";    	//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=10;            			//列最大值
    iArray[4][3]=2;
    iArray[5] = new Array("受理事故号","80px","10","2");
    
    DegreeOperationGrid = new MulLineEnter( "fm" , "DegreeOperationGrid" );
    DegreeOperationGrid.mulLineCount = 1;
    DegreeOperationGrid.displayTitle = 1;
    DegreeOperationGrid.canSel = 0;
    DegreeOperationGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在DegreeOperation中出错");
    alert(ex);
  }
}


</script>
