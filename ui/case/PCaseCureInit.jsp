<%
//程序名称：LLPCaseCureInit.jsp
//程序功能：初始化“分案疾病伤残明细”
//创建日期：
//创建人  ：刘岩松
//更新记录：
//更新人:刘岩松
//更新日期:
//更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
var turnPage = new turnPageClass();
function initInpBox()
{ 
  try
  {                                   
//    fm.all('CaseNo').value = '';
//    fm.all('AffixSerialNo').value = '';
//    fm.all('HospitalCode').value = '';
//    fm.all('HospitalName').value = '';
//    fm.all('CustomerNo').value = '';
//    fm.all('CustomerName').value = '';
//    fm.all('AccidentType').value = '';
//    fm.all('CureNo').value = '';
//    fm.all('DiseaseCode').value = '';
//    fm.all('DiseaseName').value = '';
//    fm.all('Diagnosis').value = '';
    //fm.all('ClmFlag').value = '';
    //fm.all('MngCom').value = '';
    //fm.all('Operator').value = '';
    //fm.all('ReceiptNo').value = '';
    //fm.all('InHospitalDate').value = '';
    //fm.all('OutHospitalDate').value = '';
    //fm.all('InHospitalDays').value = '';

  }
  catch(ex)
  {
    alert("在PCaseInfoInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在PCaseInfoInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initPCaseInfoGrid();
    initPCaseInjureGrid();
    showCaseInfo();
  }
  catch(re)
  {
    alert("PCaseInfoInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//立案分案诊疗信息
function initPCaseInfoGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;                                   //是否允许输入,1表示允许，0表示不允许
      iArray[0][4]="SerialNo";              			

      iArray[1]=new Array();
      iArray[1][0]="疾病代码";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="DiseaseCode";
      iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
 			iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
 			iArray[1][9]="医院代码|NOTNULL";
 
      
      iArray[2]=new Array();
      iArray[2][0]="疾病名称";    	//列名
      iArray[2][1]="400px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="DiseaseName";

      PCaseInfoGrid = new MulLineEnter( "fm" , "PCaseInfoGrid" ); 
      //这些属性必须在loadMulLine前
      PCaseInfoGrid.mulLineCount = 1;   
      PCaseInfoGrid.displayTitle = 1;
      PCaseInfoGrid.canSel = 0;
      PCaseInfoGrid.loadMulLine(iArray);  
       }
      catch(ex)
      {
        alert(ex);
      }
  }

function initPCaseInjureGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;                                   //是否允许输入,1表示允许，0表示不允许
      iArray[0][4]="SerialNo";              			

      iArray[1]=new Array();
      iArray[1][0]="伤残代码";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][4]="cd_wound";
      iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
 			iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
 			iArray[1][9]="伤残代码|NOTNULL";
 
      
      iArray[2]=new Array();
      iArray[2][0]="伤残名称";    	//列名
      iArray[2][1]="400px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      PCaseInjureGrid = new MulLineEnter( "fm" , "PCaseInjureGrid" ); 
      //这些属性必须在loadMulLine前
      PCaseInjureGrid.mulLineCount = 1;   
      PCaseInjureGrid.displayTitle = 1;
      PCaseInjureGrid.canSel = 0;
      PCaseInjureGrid.loadMulLine(iArray);  
       }
      catch(ex)
      {
        alert(ex);
      }
  }
</script>