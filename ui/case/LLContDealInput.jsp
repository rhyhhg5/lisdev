<html>
	<%
	//Name：LLMainAskInput.jsp
	//Function：登记界面的初始化
	//Date：2004-12-23 16:49:22
	//Author：wujs
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.llcase.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLContDealInput.js"></SCRIPT>
		<%@include file="LLContDealInit.jsp"%>
  <script language="javascript">
   var str = "1 and code in (select code from ldcode where codetype=#llgetmode# )";
  </script>
	</head>
	<body  onload="initForm();initElementtype();" >

		<form action="./LLSecondUWSave.jsp" method=post name=fm target="fraSubmit">
			<Div align="left">
				<Table>
					<TR>
						<TD class=common>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContDeal);">
						</TD>
						<TD class= titleImg >
							合同处理
						</TD>
						<TD class= common >(
						  <input class=cssButton type=button style='width:80px;' value="合同处理删除" onclick="edorCancle();">
						  )
	    	    </TD>
					</TR>
				</Table>
			</div>
			<Div  id= "divContDeal" align= center style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanContDealGrid" >
							</span>
						</TD>
					</TR>
				</table>
			</Div>
			<br>
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCont);">
					</TD>
					<TD class= titleImg>
						客户保单信息
					</TD>
				</TR>
			</table>

			<Div  id= "divCont" align= center style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanContGrid" >
							</span>
						</TD>
					</TR>
				</table>
			</Div>
			<table class = common>
       <TR>
        <TD  class= input> 
        	<input name="ContDeal" style="display:''"  class=cssButton type=button value="新  建" onclick="saveDealCont()">
			  </TD>
       </TR>
      </table>
      <table>
      <tr>
        <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
        </td>
        <td class= titleImg>
            保单险种信息
        </td>
      </tr>
    </table>
    <Div  id= "divPolGrid" style= "display: ''">
      <table  class= common>
      	<tr  class= common>
      		<td text-align: left colSpan=1>
    			<span id="spanPolGrid" >
    			</span> 
    	  	</td>
      	</tr>
      </table>					
    </div>
			<Div  id= "divLPGrpPol" style= "display: ''">
			<table  class= common>
			<tr class=common>
				<td class=title >
				  合同处理号
				</td>
				<td class= Input colspan="5">
				  <Input type="input" class="readonly" readonly name=EdorAcceptNo>
				</td>
			</tr>
				<tr  class= common>
					<TD  class= title>合同处理项目</TD>
					<TD  class= input>
						<Input class= "codeNo" name=EdorTypeValue CodeData="0|3^CT|解约^HZ|合同终止^RB|解约回退^CD|合同终止退费" ondblclick="return showCodeListEx('EdorTypeValue',[this,EdorTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('EdorTypeValue',[this,EdorTypeName],[0,1]);" ><Input class= "codeName" name=EdorTypeName readonly >
					</TD>
					<TD class= title>
					    合同处理生效日期
					</TD>
					<TD  class= input>
					    <Input class= "coolDatePicker" dateFormat="short" name=EdorValiDate >
					</TD>
					<TD  class= title>
						操作员
					</TD>
					<TD  class= input>
						<Input class= "readonly" readonly name=Operator>
					</TD>
				</tr>	
			</table>
		</div>
		<table class= common> 
    	<tr>
    		<td> 
    			<input type =button class=cssButton value="添加合同处理项目" onclick="addRecord();">    			
    		</td>
    	</tr>
    </table>
    <br>
    <table>
	    <tr>
	     <td>
	      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEdorItemGrid);">
	     </td>
	     <td class= titleImg>
	      合同处理项目信息
	     </td>
	    </tr>
    </table>
    <Div  id= "divEdorItemGrid" style= "display: ''">
      <table  class= common>
      	<tr  class= common>
	    		<td text-align: left colSpan=1>
	  			<span id="spanEdorItemGrid" >
	  			</span> 
	  	  	</td>
      	</tr>
      </table>					   
    </div> 
    <table>
	    <tr>
	     <td>
	      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPayMode);">
	     </td>
	     <td class= titleImg>
	      收付方式信息
	     </td>
	    </tr>
    </table>
    <div id = "divPayMode" style = "display:''">
     <table class="common">
		  <tr class="common">
		  		<td class="title">收付方式</td>
		    	<td class="input"><Input class="codeno" name="PayMode" verify="收付方式|code:paymode&INT&notnull" onclick="return showCodeList('paymode',[this,PayModeName],[0,1],null,str,'1');" onkeyup="return showCodeListKey('paymode',[this,PayModeName],[0,1],null,str,'1');" ><input class=codename name=PayModeName elementtype="nacessary"></td>  
			    <td class="title">银行编码</td>
			    <td class="input"><Input class="code" name="BankCode" verify="银行编码|NOTNULL" onclick="return showCodeList('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" onkeyup="return showCodeListKey('lllbank',[this,BankName,SendFlag],[0,1,2],null,fm.BankName.value,'bankname',1);" ></td>  
			    <td class="title">银行名称</td>
			    <td class="input"><Input class="input" name="BankName" ></td> 
		  </tr>
		  <tr class="common" >
		  	<td class="title">签约银行</td>
			<td class="input"><input class="readonly" name="SendFlag" readonly="true"></td>
		    <td class="title">银行账号</td>
		    <td class="input"><Input class="input" name="AccNo" verify="银行账号|NOTNULL" ></td>  
		    <td class="title">账户名</td>
		    <td class="input"><Input class="input" name="AccName" verify="账户名|NOTNULL" ></td> 
		  </tr>
		  <tr class="common" >

		  </tr>
	 </table>
    </div>
    <div id = "divappconfirm" style = "display:''">
    <table class = common>
      <TR>
        <TD  class= input> 
        		<Input type =button class=cssButton value="合同处理项目明细" onclick="detailEdor();">	
            <Input class=cssButton type=Button value="合同处理理算" onclick="edorAppConfirm()">
            <input type="button" class=cssButton name="redo" id="redo" value="理算回退" onclick="reCal();">
            <Input class=cssButton type=Button value="合同处理查看" onclick="PrtEdor()">
            <input type =button class=cssButton value="删除合同处理项目" onclick="delRecord();"> 
            <Input class=cssButton type=Button value="合同处理确认" onclick="edorConfirm()">
            
        </TD>
      </TR>
    </table>
    </div>
			<input type=hidden name="CaseNo" value="">
			<input type=hidden name="InsuredNo" value="">
			<input type=hidden name="AppntNo" value="">
			<input type=hidden name="ContNo" value="">
			<input type=hidden name="GrpContNo" value="00000000000000000000">
			<input type=hidden name="fmtransact">
			<input type=hidden name="Transact">
			<input type=hidden name="EdorType">
			<input type=hidden name="CurrentOperator">
			<input type=hidden name="ContNoA" >
			<input type=hidden name="sql" >
			<input type=hidden name="Edorappdate" >
		</form>
				<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
			</body>
		</html>
