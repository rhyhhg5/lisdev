var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var tSaveType="";

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
	}
}

function easyQuery()
{
  var startDate = fm.RgtDateS.value;
  var endDate = fm.RgtDateE.value;
  if (startDate==""||startDate==null||endDate==""||endDate==null){
  	alert("请输入受理日期");
  	return false;
  }
  if(dateDiff(startDate,endDate,"M")>3){
      alert("统计期最多为三个月！");
      return false;
  }
	
	var anow = fm.RgtDateE.value;
	var strSQL0 = "select a.rgtno,a.caseno,a.CustomerName,a.CustomerNo,a.rgtdate,"
       + "a.claimer,b.codename "
       +" from llcase a, ldcode b where b.codetype='llrgtstate' and b.code=a.rgtstate and receiptflag in('0','3')"
       + getWherePart("a.rgtdate","RgtDateS",">=")
       + getWherePart("a.rgtdate","RgtDateE","<=")
       + getWherePart("a.claimer","Operator")
       + getWherePart("a.MngCom","OrganCode","like")
       + getWherePart("a.RgtNo","RgtNo")
       + getWherePart("a.CaseNo","CaseNo")
       + getWherePart("a.CustomerNo","CustomerNo")
       + getWherePart("a.CustomerName","CustomerName")
       ;
       strSQL1 ="and rgtstate in('01','02') order by a.rgtdate,a.caseno";
       strSQL = strSQL0+strSQL1;
		  if ( strSQL !="" )
		  {
		      var idate = 0;
		      var iidate = '';
		      turnPage.queryModal(strSQL, CheckGrid);
		  }
		  ClaimerCal();
}

function submitForm()
{
	if(fm.llusercode.value==null||fm.llusercode.value=="")
	{
		alert("请选择理赔员！");
		return;
		}
if(fm.llusercode.value==fm.Operator.value)
{
		alert("修改的理赔员不能与原理赔员相同！");
		return;
		}
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
    fm.submit(); //提交
}

function getstr()
{
  str="1 and comcode like #"+fm.OrganCode.value+"%#";
}

function submitFormAuto()
{


	
		if(!CheckGrid.getChkNo())
	{
		
		 if(!confirm("您确定要全部案件重新分配吗？"))
					    return false;
					    
		
	}

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  	fm.action="./LLCaseTransferAutoSave.jsp?Handler="+Handler;
   fm.submit(); //提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		window.location.reload();
	/*	top.close();
		top.opener.location.reload();
		parent.opener.top.focus(); */
	}
} 
function classify(){

	var anow = getCurrentDate();

	var strSQL="select case when a.rgtno=a.caseno then '' else a.rgtno end xx,a.CaseNo,a.CustomerName,a.rgtstate,a.CustomerNo,a.rgtdate,a.endcasedate, "
							+" a.RgtType,a.operator,b.codename,'', " 
							+" to_char(to_date('"+anow+"')-to_date(a.rgtdate)+1)  from llcase a, ldcode b where "
						  +" b.codetype='llrgtstate' and b.code=a.rgtstate and a.rgtstate not in  ('07','09','12','14','11','13')  and a.handler='"+Handler+"' "   
						  +" union select  case when a.rgtno=a.caseno then '' else a.rgtno end xx,a.CaseNo,a.CustomerName,a.rgtstate,a.CustomerNo,a.rgtdate,a.endcasedate, "
						  +" a.RgtType,a.operator,b.codename,'',to_char(to_date('"+anow+"')-to_date(a.rgtdate)+1) "
						  +" from llcase a, ldcode b,llclaimuwmain c where b.codetype='llrgtstate' "
						  +" and b.code=a.rgtstate and a.rgtstate not in ('07','09','12','14','11','13')  "
						  +" and rgtstate in ('05','10') and c.caseno=a.caseno and c.appclmuwer='"+Handler+"'order by xx with ur";
							

		turnPage.queryModal(strSQL, CheckGrid);
	
}


//调查分配
function DealSurveyAssign()
{
	var caseno = '';
	var newWindow = OpenWindowNew("./FrameMainSurveyDispatch.jsp?Interface=LLSurveyDispatch.jsp","调查分配","middle",800,330);
}

//理赔员统计
function ClaimerCal()
{

		var strSQL=" select a.claimer,11, "
							+"	(select count(*) from llcase where claimer=a.claimer and ReceiptFlag ='1'), "
							+"	(select count(*) from llfeemain b where exists(select 1 from llcase where caseno=b.caseno and claimer=a.claimer and ReceiptFlag ='1') ), "
							+"	(select count(*) from llcase where claimer=a.claimer and ReceiptFlag in('3','0') and rgtstate in('01','02')) "
							+"	from llcase a "
							+"	where exists(select 1 from lduser where usercode=a.claimer and  claimpopedom = '2') "
							+"	group by a.claimer ";
				
		turnPage2.queryModal(strSQL,ClaimerGrid);
		
}   
    
    
    
    
    
    
    
    
    
    
    