<%
	//程序名称：LDCodeInputInit.jsp
	//程序功能:初始化页面数据
	//创建日期：2017-12-19 17:07
	//创建人:XHW
	//更新记录:更新人   更新日期  更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script src="../common/javascript/Common.js">
</script>
<script language="JavaScript">
	function initInpBox() {
		try {
		//	fm.all("tph1").value="";
		//	fm.all("ComCode").value="";
		//	fm.all("tp1").value="";
		//	fm.all("Code").value="";
		//	fm.all("startDate").value = "";
		//	fm.all("endDate").value = "";
		//	fm.all("CodeType").value="";
		//	fm.all("Thrid").value="";
		}catch(ex) {
			alert("LPContInputInit.jsp--->initInpBox函数中发生异常：初始化页面错误!");
		}
		//fm.MakeDate.value = getCurrentDate();
	}
	function initForm() {
		try {
			initInpBox();
			initLDCodeGrid();
		}catch(re) {
			alert("LPContInputInit.jsp--->initForm函数中发生异常：初始化页面错误!");
		}
	}
	var LDCodeGrid;
	function initLDCodeGrid() {
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0]="序号";
			iArray[0][1]="50px";
			iArray[0][3]=0;
			
			iArray[1] = new Array();
			iArray[1][0]="保单机构";		//列名
			iArray[1][1]="50px";		//列宽
			iArray[1][2]=100;			//列最大值
			
			iArray[2] = new Array();
			iArray[2][0]="团单号";
			iArray[2][1]="50px"; 
			iArray[2][3]=1;
			
			iArray[3] = new Array();
			iArray[3][0]="操作时间";
			iArray[3][1]="50px";
			iArray[3][3]=1;
			
			iArray[4] = new Array();
			iArray[4][0]="投保单位";
			iArray[4][1]="50px";
			iArray[4][3]=1;
			
			iArray[5] = new Array();
			iArray[5][0]="保单承保人数";
			iArray[5][1]="50px"; 
			iArray[5][3]=1;
			
			iArray[6] = new Array();
			iArray[6][0]="操作人";
			iArray[6][1]="50px"; 
			iArray[6][3]=1;
			
			iArray[7] = new Array();
			iArray[7][0]="外包第三方";
			iArray[7][1]="50px"; 
			iArray[7][3]=1;
			
			LDCodeGrid = new MulLineEnter("fm","LDCodeGrid");
			
			//设置行数
			LDCodeGrid.mulLineCount = 5;
			//是否显示标题
			LDCodeGrid.displayTitle = 1;
			//++是否显示  1不显示0显示   默认不显示
			LDCodeGrid.hiddenPlus = 0;
			//--是否显示  1不显示0显示   默认为不显示
			LDCodeGrid.hiddenSubtraction = 0;
			//radio缺省值  1显示0隐藏
			LDCodeGrid.canSel = 0;
			//checkBox缺省值  1显示0隐藏
			LDCodeGrid.canChk = 1;
			
			LDCodeGrid.loadMulLine(iArray);
		}catch(ex) {
			alert(ex);
		}
		
	}
</script>