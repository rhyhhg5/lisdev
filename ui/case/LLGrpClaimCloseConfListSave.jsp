<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLAffixSave.jsp
//程序功能：
//创建日期：2005-02-5 08:49:52
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  
  
  CErrors tError = null;
  
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  VData tVData = new VData();
  
  //输出参数
  String FlagStr = "";
  String Content = "";
    
  String strOperate = request.getParameter("operate");
  System.out.println("==== strOperate == " + strOperate);

  if (strOperate.equals("GIVE|ENSURE")) 
  {
   GrpGiveEnsureBL tGrpGiveEnsureBL = new GrpGiveEnsureBL();
   LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
   tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
   
   if (request.getParameterValues("PrePaidFlag") != null) {
	   tLLRegisterSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
	}else{
	   tLLRegisterSchema.setPrePaidFlag("");  // 个案标记llcase 批次案件标记llregister
	}
	   try
	   {		   
		   tVData.add(tLLRegisterSchema);
		   tVData.add(tG);		   
		  tGrpGiveEnsureBL.submitData(tVData, strOperate);
	   }
	 catch(Exception ex)
	    {
	      Content = "确认失败，原因是:" + ex.toString();
	      System.out.println("aaaa"+ex.toString());
	      FlagStr = "Fail";
	    }
	    if (FlagStr=="")
	    {
		    tError = tGrpGiveEnsureBL.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="确认成功！"+tGrpGiveEnsureBL.getBackMsg();
		    	FlagStr = "Succ";
		    	tVData.clear();
				tVData = tGrpGiveEnsureBL.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "确认失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
	     }	     
  }		
  if (strOperate.equals("QDGIVE|ENSURE")) 
  {
	  GrpGiveEnsureBL tGrpGiveEnsureBL = new GrpGiveEnsureBL();
   LLRegisterSchema tLLRegisterSchema =new LLRegisterSchema();
   tLLRegisterSchema.setRgtNo(request.getParameter("RgtNo"));
   if (request.getParameterValues("PrePaidFlag") != null) {
	   tLLRegisterSchema.setPrePaidFlag("1");  // 0或null 不使用预付回销 1-预付回销
	}else{
	   tLLRegisterSchema.setPrePaidFlag("");  // 个案标记llcase 批次案件标记llregister
	}
	   try
	   {		   
		   strOperate="GIVE|ENSURE";
		   tVData.add(tLLRegisterSchema);
		   tVData.add(tG);		   
		   tGrpGiveEnsureBL.submitData(tVData, strOperate);
	   }
	 catch(Exception ex)
	    {
	      Content = "确认失败，原因是:" + ex.toString();
	      System.out.println("aaaa"+ex.toString());
	      FlagStr = "Fail";
	    }
	    if (FlagStr=="")
	    {
		    tError = tGrpGiveEnsureBL.mErrors;
		    if (!tError.needDealError())
		    {                          
		      Content ="确认成功！";
		    	FlagStr = "Succ";
		    	tVData.clear();
				  tVData = tGrpGiveEnsureBL.getResult();
		    }
		    else                                                                           
		    {
		    	Content = "确认失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
	     }	     
  }		
  

  %>

   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
   
   
   
 