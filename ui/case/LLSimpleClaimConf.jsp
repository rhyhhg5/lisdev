<html>
	<%
	//Name：LLSimpleClaimConf.jsp
	//Function：批量审批
	//Date：2013-07-02
	//Author：ZJL
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page import="java.util.*"%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String Operator=tG.Operator;
	String Comcode=tG.ManageCom; 
	String Handler= request.getParameter("Handler");
	String CurrentDate= PubFun.getCurrentDate();
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);
  String AheadDays="-30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
	%>

	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLSimpleClaimConf.js"></SCRIPT>
		<%@include file="LLSimpleClaimConfInit.jsp"%>
		
		 <script language="javascript">
   function initDate(){
   fm.RgtDateS.value="<%=afterdate%>";
   fm.RgtDateE.value="<%=CurrentDate%>";
   var usercode="<%=Operator%>";
   var comcode="<%=Comcode%>";
   fm.Operator.value=usercode;
   fm.OrganCode.value=comcode;

   var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
   var arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
      fm.optname.value= arrResult[0][0];
 		}
 		fm.Printf.value = '0'; 
 		fm.PrintfName.value = '审批';
   }
   </script>
	</head>
	<body  onload="initDate();initForm();">
		<form action="./LLSimpleClaimConfSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<TR>
					<TD><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQuery);"></TD>
					<TD class= titleImg>查询条件</TD>
				</TR>
			</table>
			<Div  id= "divQuery" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD  class= title>管理机构</TD><TD  class= input> 
						<Input class= "code8"  name=OrganCode ondblclick="getstr();return showCodeList('comcode',[this], [0],null,str,'1');" onkeyup="getstr();return showCodeListKey('comcode', [this], [0],null,str,'1');" >
	                    <TD  class= title>理赔员代码</TD><TD  class= input> 
	                    <Input class= "code8"  name=Operator ondblclick="getRights();return showCodeList('optname',[this, optname], [0, 1],null,Str,'1');" onkeyup="getRights();return showCodeListKey('optname', [this, optname], [0, 1],null,Str,'1');" >
	                    <TD  class= title>理赔员姓名</TD><TD  class= input> 
	                    <Input class="readonly" readonly name=optname></TD>
					</TR>
					<TR  class= common>
          	<TD  class= title8>受理日期从</TD>
          	<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
          	<TD  class= title8>到</TD>
          	<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
          	<TD  class= title8>处理类型</TD>
          	<TD  class= input8> <input class=codeno CodeData="0|2^0|审批^1|审定^2|抽检"  name=Printf ondblclick="return showCodeListEx('Printf',[this,PrintfName],[0,1]);" onkeyup="return showCodeListKeyEx('Printf',[this,PrintfName],[0,1]);"><input class=codename name=PrintfName></TD>
	        </TR>
				</table>
			</Div>
			<Div  align=left style= "display: '' ">
					<INPUT CLASS=cssButton VALUE="查询" TYPE=button onclick="easyQuery();">
				</Div>
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseList);">
					</TD>
					<TD class= titleImg>
						待处理案件列表
					</TD>
				</TR>
			</table>

			<Div  id= "divCaseList" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD text-align: left colSpan=1>
							<span id="spanCheckGrid" >
							</span>
						</TD>
					</TR>
				</table>
				
				<Div  align=left style= "display: '' ">
					<INPUT CLASS=cssButton VALUE="批量审批" TYPE=button onclick="submitForm();">
				</Div>

				<Div  align=center style= "display: '' ">
					<INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
					<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
					<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
					<INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
				</Div>
				
				<Input type=hidden  class=code name="Hand" >
			</form>
			<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

		</body>
	</html>
