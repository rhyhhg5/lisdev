<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：ClaimQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LLClaimUnderwriteSchema tLLClaimUnderwriteSchema   = new LLClaimUnderwriteSchema();
  tLLClaimUnderwriteSchema.setClmUWNo(request.getParameter("ClmUWNo"));
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLLClaimUnderwriteSchema);
  // 数据传输
  ClaimUnderwriteUI tClaimUnderwriteUI   = new ClaimUnderwriteUI();
	if (!tClaimUnderwriteUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tClaimUnderwriteUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tClaimUnderwriteUI.getResult();
		System.out.println(tVData.size());
		// 显示
		// 保单信息
		LLClaimUnderwriteSchema mLLClaimUnderwriteSchema = new LLClaimUnderwriteSchema(); 
		LLClaimUnderwriteSet mLLClaimUnderwriteSet=new LLClaimUnderwriteSet();
		mLLClaimUnderwriteSet=(LLClaimUnderwriteSet)tVData.getObjectByObjectName("LLClaimUnderwriteBLSet",0);
		mLLClaimUnderwriteSchema=mLLClaimUnderwriteSet.get(1);
		%>
    	<script language="javascript">

    	 	top.opener.fm.all("ClmUWer1").value = "<%=mLLClaimUnderwriteSchema.getClmUWer()%>";
    	 	top.opener.fm.all("ClmUWGrade").value = "<%=mLLClaimUnderwriteSchema.getClmUWGrade()%>";
    	 	top.opener.fm.all("ClmDecision").value = "<%=mLLClaimUnderwriteSchema.getClmDecision()%>";
    	 	top.opener.fm.all("MngCom1").value = "<%=mLLClaimUnderwriteSchema.getMngCom()%>";
    	 	top.opener.fm.all("MakeDate1").value = "<%=mLLClaimUnderwriteSchema.getMakeDate()%>";
    	 	top.opener.fm.all("CustomerNo").value = "<%=mLLClaimUnderwriteSchema.getInsuredNo()%>";
    	 	top.opener.fm.all("Name").value = "<%=mLLClaimUnderwriteSchema.getInsuredName()%>";
    	 	top.opener.fm.all("AppntCustomerNo").value = "<%=mLLClaimUnderwriteSchema.getAppntNo()%>";
    	 	top.opener.fm.all("AppntName").value = "<%=mLLClaimUnderwriteSchema.getAppntName()%>";
    	 	top.opener.emptyUndefined();
    	</script>
		<%		
	}
 // end of if
 
 //读取分表信息
 //保单信息部分
  LLClaimUWDetailSchema tLLClaimUWDetailSchema   = new LLClaimUWDetailSchema();
  tLLClaimUWDetailSchema.setClmUWNo(request.getParameter("ClmUWNo"));
  // 准备传输数据 VData
  tVData.removeAllElements();
	tVData.addElement(tLLClaimUWDetailSchema);
  // 数据传输
  ClaimUWDetailUI tClaimUWDetailUI   = new ClaimUWDetailUI();
	if (!tClaimUWDetailUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tClaimUWDetailUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tClaimUWDetailUI.getResult();
		System.out.println(tVData.size());
		// 显示
		LLClaimUWDetailSchema mLLClaimUWDetailSchema = new LLClaimUWDetailSchema(); 
		LLClaimUWDetailSet mLLClaimUWDetailSet=new LLClaimUWDetailSet();
		mLLClaimUWDetailSet=(LLClaimUWDetailSet)tVData.getObjectByObjectName("LLClaimUWDetailBLSet",0);
		for(int i=1;i<=mLLClaimUWDetailSet.size();i++)
		{
			mLLClaimUWDetailSchema=mLLClaimUWDetailSet.get(i);
			%>
    	<script language="javascript">
    	 	top.opener.fm.ClaimUWDetailGrid1[<%=i-1%>].value = "<%=mLLClaimUWDetailSchema.getClmUWNo()%>";
    	 	top.opener.fm.ClaimUWDetailGrid2[<%=i-1%>].value = "<%=mLLClaimUWDetailSchema.getGetDutyCode()%>";
    	 	top.opener.fm.ClaimUWDetailGrid3[<%=i-1%>].value = "<%=mLLClaimUWDetailSchema.getGetDutyKind()%>";
    	 	top.opener.fm.ClaimUWDetailGrid4[<%=i-1%>].value = "<%=mLLClaimUWDetailSchema.getStandPay()%>";
     		top.opener.fm.ClaimUWDetailGrid5[<%=i-1%>].value = "<%//=mLLClaimUWDetailSchema.getRealPay()%>";
     		top.opener.fm.ClaimUWDetailGrid6[<%=i-1%>].value = "<%=mLLClaimUWDetailSchema.getReceiptNo()%>";
     		top.opener.emptyUndefined();
     	</script>
		<%	
		} //end of for	
	} //end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tClaimUWDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");
%>
