<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDPersonnelSave.jsp
//程序功能：
//创建日期：2005-04-13 23:19:03
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
	<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDPersonnelSchema tLDPersonnelSchema   = new LDPersonnelSchema();
  OLDPersonnelUI tOLDPersonnelUI   = new OLDPersonnelUI();
  // # 3040 人员信息档案管理模块
  LLRelationClaimSet  mLLRelationClaimSet = new  LLRelationClaimSet();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    tLDPersonnelSchema.setCustomerNo(request.getParameter("CustomerNo"));
    tLDPersonnelSchema.setName(request.getParameter("Name"));
    tLDPersonnelSchema.setSex(request.getParameter("Sex"));
    tLDPersonnelSchema.setBirthday(request.getParameter("Birthday"));
    tLDPersonnelSchema.setIDType(request.getParameter("IDType"));
    tLDPersonnelSchema.setIDNo(request.getParameter("IDNo"));
    tLDPersonnelSchema.setPassword(request.getParameter("Password"));
    tLDPersonnelSchema.setNativePlace(request.getParameter("NativePlace"));
    tLDPersonnelSchema.setNationality(request.getParameter("Nationality"));
    tLDPersonnelSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLDPersonnelSchema.setMarriage(request.getParameter("Marriage"));
    tLDPersonnelSchema.setMarriageDate(request.getParameter("MarriageDate"));
    tLDPersonnelSchema.setHealth(request.getParameter("Health"));
    tLDPersonnelSchema.setStature(request.getParameter("Stature"));
    tLDPersonnelSchema.setAvoirdupois(request.getParameter("Avoirdupois"));
    tLDPersonnelSchema.setDegree(request.getParameter("Degree"));
    tLDPersonnelSchema.setCreditGrade(request.getParameter("CreditGrade"));
    tLDPersonnelSchema.setOthIDType(request.getParameter("OthIDType"));
    tLDPersonnelSchema.setOthIDNo(request.getParameter("OthIDNo"));
    tLDPersonnelSchema.setICNo(request.getParameter("ICNo"));
    tLDPersonnelSchema.setGrpNo(request.getParameter("GrpNo"));
    tLDPersonnelSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
    tLDPersonnelSchema.setStartWorkDate(request.getParameter("StartWorkDate"));
    tLDPersonnelSchema.setPosition(request.getParameter("Position"));
    tLDPersonnelSchema.setSalary(request.getParameter("Salary"));
    tLDPersonnelSchema.setOccupationType(request.getParameter("OccupationType"));
    tLDPersonnelSchema.setOccupationCode(request.getParameter("OccupationCode"));
    tLDPersonnelSchema.setEMail(request.getParameter("Email"));
    tLDPersonnelSchema.setMobile(request.getParameter("Mobile"));
    tLDPersonnelSchema.setPhone(request.getParameter("Phone"));
    tLDPersonnelSchema.setDeathDate(request.getParameter("DeathDate"));
    tLDPersonnelSchema.setSmokeFlag(request.getParameter("SmokeFlag"));
    tLDPersonnelSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));
    tLDPersonnelSchema.setProterty(request.getParameter("Proterty"));
    tLDPersonnelSchema.setRemark(request.getParameter("Remark"));
    tLDPersonnelSchema.setState(request.getParameter("State"));
    tLDPersonnelSchema.setVIPValue(request.getParameter("VIPValue"));
    tLDPersonnelSchema.setGrpName(request.getParameter("GrpName"));
    tLDPersonnelSchema.setspeciality(request.getParameter("speciality"));
    tLDPersonnelSchema.setManageCom(request.getParameter("ManageCom"));
    tLDPersonnelSchema.setOccupationGrade(request.getParameter("OccupationGrade"));
    tLDPersonnelSchema.setUserCode(request.getParameter("UserCode"));
    System.out.println("**********"+tLDPersonnelSchema.getUserCode());
    tLDPersonnelSchema.setOperator(request.getParameter("Operator"));
    tLDPersonnelSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDPersonnelSchema.setMakeTime(request.getParameter("MakeTime"));
    tLDPersonnelSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLDPersonnelSchema.setModifyTime(request.getParameter("ModifyTime"));
    System.out.println("**************3340 人员信息档案管理模块");
    tLDPersonnelSchema.setWorkAge(request.getParameter("WorkAge"));
    tLDPersonnelSchema.setDocBack(request.getParameter("DocBack"));
    tLDPersonnelSchema.setCertificationType(request.getParameter("certificationType"));
    
  //# 3340 人员信息档案管理模块**start**

  	String relaName[] =request.getParameterValues("FamilyDisGrid1");
  	String relaNo[] =request.getParameterValues("FamilyDisGrid2");
  	String FamType[]=request.getParameterValues("FamilyDisGrid4");
  	String SerialNo[]=request.getParameterValues("FamilyDisGrid5");
  	
  	if(relaName !=null){
  		System.out.println("tRNum==="+relaName.length);
  		for (int i=0;i<relaName.length;i++){
  			if(relaName[i]!=null && !relaName[i].equals("")){
  				LLRelationClaimSchema tLLRelationClaimSchema = new LLRelationClaimSchema();
  				tLLRelationClaimSchema.setRelationName(relaName[i]);
  				tLLRelationClaimSchema.setRelationIDNO(relaNo[i]);
  				tLLRelationClaimSchema.setRelation(FamType[i]);
  				tLLRelationClaimSchema.setSerialNo(SerialNo[i]);
  				System.out.println("=========="+SerialNo[i]);
  				mLLRelationClaimSet.add(tLLRelationClaimSchema);
  			}
  		}
  	}
  	
  	// # 3340 人员信息档案管理模块**end**
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLDPersonnelSchema);
  	tVData.add(tG);
  	tVData.add(mLLRelationClaimSet);    // #3340 人员信息档案管理模块
    tOLDPersonnelUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLDPersonnelUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
