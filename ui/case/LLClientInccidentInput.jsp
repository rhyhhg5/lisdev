<html>
<%
//Name:RegisterInput.jsp
//Function：立案界面的初始化程序
//Date：2002-07-21 17:44:28
//Author ：LiuYansong
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
     
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LLClientInccidentInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLClientInccidentInputInit.jsp"%>
</head>
<body  onload="initForm();">
  <form action="./RegisterSave.jsp" method=post name=fm target="fraSubmit">
   
  <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,GrpInfo);">
    	</td>
    	<td class= titleImg>
    	 案件信息 
    	</td>
    </tr>
      </table>
	<div id="GrpInfo" style="display: ''">
		<table  class= common>
			<TR  class= common8>
				<TD  class= title8>团体批次号</TD><TD  class= input8><Input class= readonly name="GrpRgtNo" readonly></TD>
				<TD  class= title8>案件号</TD><TD  class= input8><Input class= readonly name="CaseNo" readonly></TD>
				<TD  class= title8>受理日期</TD><TD  class= input8><Input class= readonly name="MakeData" readonly></TD>
			</TR>
		</table>
	</div>
	<table>
    	<tr>
	        <td class=common>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,Client);">
	    	</td>
	    	<td class= titleImg>
	    	 个人客户查询 
	    	</td>
    	</tr>
    </table>
      <div id="Client" style="display : ''">
		<table  class= common>
			<TR  class= common8>
				<TD  class= title8>客户号码</TD><TD  class= input8><Input class=common name="InsuredNo1" ></TD>
				<TD  class= title8>客户姓名</TD><TD  class= input8><Input class=common name="CustomerName1"  ></TD>
				<TD  class= title8>保单号码</TD><TD  class= input8><Input class=common name="ContNo" ></TD>
			</tr>
			<TR  class= common8>
				<TD  class= title8>证件类型</TD><TD  class= input8><Input class=code name="IDType"  ondblClick="showCodeList('idtype',[this],[0]);" onkeyup="showCodeList('idtype',[this],[0]);"></TD>
				<TD  class= title8>证件号码</TD><TD  class= input8><Input class=common name="IDNo"  ></TD>
				<TD  class= title8><input class=cssbutton type=button value="客户查询" onclick="ClientQuery()"></td>
			</TR>
		</table>
	  </div>
<table>
   	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,CustomerDiv);">
    	</td>
    	<td class= titleImg>
    	 出险人信息 
    	</td>
    </tr>
</table>
	<div id="CustomerDiv" style="display: ''">
		<table  class= common>
			<TR  class= common8>
				<TD  class= title8>客户号码</TD><TD  class= input8><Input class= readonly name="CustomerNo1" readonly></TD>
				<TD  class= title8>客户名称</TD><TD  class= input8><Input class= readonly name="CustomerName2" readonly></TD>
				<TD  class= title8>客户性别</TD><TD  class= input8><Input class= readonly name="Sex" readonly></TD>
			</TR>
			<TR  class= common8>    
	 			<TD  class= title8>出生日期</TD><TD  class= input8><input class=common name="CBirthday"  ></TD>        
				<TD  class= title8>证件类型</TD><TD  class= input8><Input class= code   name="CIDType" ondblClick="showCodeList('idtype',[this],[0]);" onkeyup="showCodeListKeyEx('idtype',[this],[0]);"></TD>   
	          	<TD  class= title8>证件号码</TD><TD  class= input8><Input class=common   name="CIDNo" ></TD>             
	       	</TR>
	    </table>
    </div>

<div id="TopDiv" style="display: ''">	
	<table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,EventDiv);">
    	</td>
    	<td class= titleImg>
    	 申请原因
    	</td>
		</tr>
	</table>
	<Div id= "EventDiv" style= "display: ''">
		<table class=common>
			<TR  class= common8>    
				<TD  class= title8>出险开始日期</TD><TD  class= input8><Input class="coolDatePicker" dateFormat="short"   name=AccStartDate ></TD>   
				<TD  class= title8>出险结束日期</TD><TD  class= input8><input class="coolDatePicker" dateFormat="short"  name="AccidentDate"></TD>        
				<TD  class= title8>死亡日期</TD><TD  class= input8><Input  class="coolDatePicker" dateFormat="short"  name="DeathDate"></TD>
			</TR>
			<TR  class= common8>
				<TD  class= title8>出险地点</TD><TD  class= input8><Input class= common8   name=AccidentSite ></TD>   
			</TR>
			<TR  class= common><TD  class= title colspan="6">事故经过描述</TD>
			</TR>
			<TR  class= common>
			<TD  class= input colspan="6">
				<textarea name="AccidentReason2" cols="100%" rows="3" witdh=25% class="common"></textarea>
			</TD>
			</TR>
			<input type=hidden id="" name="EventFlag" value="0">
			<input type=hidden id="" name="tCaseNo" value="0">
			<input type=hidden id="" name="tRgtNo" value="0">
			
		</table>
	</div>
</div>
<Div  id= "AppReason" style= "display: ''">
	<table  class= common>
		<tr  class= common>
			<td text-align: left colSpan=1>
				<span id="spanAppReasonGrid" ></span>
			</td>
		</tr>
	</table>
</div>
<div id="div1" style="display: ''">
	<table class= common>
		<td class=input><input class=cssButton type=button value="保存原因" onclick="ReasonSave()"></td>
	</table>
</div>

<table class= common>
	<tr class= common8>
		<TD  class= title>
		</TD>
		<TD  class= input>
		</TD>
		<td class=title>      
		</td>
		<TD  class= title>材料齐备日期</TD><TD  class= input><Input class="readonly" readonly name=AffixGetDate ></TD>
	    <td class=input><input class=cssButton type=button value="申请材料选择" onclick="openAffix()"></td>
	</tr>
</table>
	
		<table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,InsuredEvent);">
    	</td>
    	<td class= titleImg>
    	 近期事件信息
    	</td>
    </tr>
      </table>
      	<Div  id= "InsuredEvent" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanInsuredEventGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>

    <input class=cssButton type=hidden value="事件生成" onclick="alert('调用事件生成')">
</table>
	
    
    <Div  id= "divnormalquesbtn" style= "display: ''" align= left>
     <input class=cssButton type=button value="保 存" onclick="alert()">
     <input class=cssButton  type=button value="返 回" onclick="">
    </Div>

  

    </Div>
   
     <div id="dd" style="display:'none'">
     
    <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCase);">
    	</td>
    	<td class= titleImg>
    	 客户信息
    	</td>
    </tr>
      </table>
   
</div>
	<Div  id= "divCase" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanCaseGrid" >
					</span>
				</td>
			</tr>
		</table>
	</div>
    

 <input type=hidden id="fmtransact" name="fmtransact">
 <input type=hidden id="ShortCountFlag" name="ShortCountFlag">
 <input type=hidden id="SupplyDateResult" name="SupplyDateResult">
 <input type=hidden id="LoadFlag" name="LoadFlag" value="1">	
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>