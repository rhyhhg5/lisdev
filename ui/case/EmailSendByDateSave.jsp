<%@page import="com.sinosoft.lis.taskservice.YBKBYMailTask"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProposalPrintSave.jsp
//程序功能：
//创建日期：2002-11-26
//创建人  ：Kevin
//修改人  ：朱向峰
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%
CErrors tError = null;
String FlagStr = "";
String Content = "";
String tOperate = "";


//获取开始日期和截止日期
String startDate = request.getParameter("startDate").trim();
String endDate = request.getParameter("endDate").trim();

if("".equals(startDate)||startDate==null){
	FlagStr="Fail";
	Content="请选开始日期!";
}else if("".equals(endDate)||endDate==null){
	FlagStr="Fail";
	Content="请选择结束日期!";
}else if(java.sql.Date.valueOf(startDate).after(java.sql.Date.valueOf(endDate))){
	//起始日期大于结束日期 
	FlagStr="Fail";
	Content="开始日期大于结束日期,请重新选择!";
}else{
	//获得session中的人员喜讯你
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	YBKBYMailTask mailTask = new YBKBYMailTask(startDate,endDate);
	mailTask.run();
	FlagStr="true";
	Content="处理成功!";
}

%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmitNew("<%=FlagStr%>","<%=Content%>");
</script>
</html>