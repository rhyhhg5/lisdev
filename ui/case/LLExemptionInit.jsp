<%
//程序名称：LLExemptionInit.jsp
//程序功能：豁免保费
//创建日期：
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%

%> 

<script language="JavaScript">
	function initInpBox()
	{
		try
		{
				try
				{
					divCase.style.display = "";
					fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
					if(fm.CaseNo.value=="null")
						fm.CaseNo.value="";
		            fm.CustomerNo.value = '<%= request.getParameter("CustomerNo") %>';

					fm.ClaimPolGridNum.value = '<%= request.getParameter("ClaimPolGridNum") %>';
					var tSQL = "select Name from ldperson where CustomerNo='"+fm.CustomerNo.value+"'";
				    var xrr = easyExecSql(tSQL);
				    if(xrr!= null){
				  	  fm.CustomerName.value = xrr[0][0];
				    }
				}
				catch(ex)
				{
					alert(ex);
				}
				<%GlobalInput mG = new GlobalInput();
				mG=(GlobalInput)session.getValue("GI");
				%>
				fm.cOperator.value="<%=mG.Operator%>";
			}
			catch(ex)
			{
				alter("在LLExemptionInit.jspInitInpBox函数中发生异常:初始化界面错误!");
			}
		}
		function initForm()
		{
			try
			{
				initInpBox();
				initExemptionGrid();
				SearchExemption();
			}
			catch(re)
			{
				alter("在LLExemptionInit.jspInitForm函数中发生异常:初始化界面错误!");
			}
		}

		function initExemptionGrid()
		{
			
			var iArray = new Array();
			try
			{						
				iArray[0]=new Array();
      			iArray[0][0]="序号";  
      			iArray[0][1]="30px"; 
      			iArray[0][2]=20;   
      			iArray[0][3]=0; 
				
				iArray[1]=new Array();
      			iArray[1][0]="保单号码";  
      			iArray[1][1]="150px"; 
      			iArray[1][2]=10;   
      			iArray[1][3]=0; 
				
				iArray[2]=new Array();
      			iArray[2][0]="险种代码";  
      			iArray[2][1]="100px"; 
      			iArray[2][2]=10;   
      			iArray[2][3]=0; 
				
				iArray[3]=new Array();
      			iArray[3][0]="险种名称";  
      			iArray[3][1]="100px"; 
      			iArray[3][2]=100;   
      			iArray[3][3]=0; 		
				
				iArray[4]=new Array();
      			iArray[4][0]="保费豁免开始时间";  
      			iArray[4][1]="100px"; 
      			iArray[4][2]=100;   
      			iArray[4][3]=1; 
				
				iArray[5]=new Array();
      			iArray[5][0]="保费豁免结束时间";  
      			iArray[5][1]="100px"; 
      			iArray[5][2]=100;   
      			iArray[5][3]=0; 
      			
      			iArray[6]=new Array();
      			iArray[6][0]="豁免期数";  
      			iArray[6][1]="100px"; 
      			iArray[6][2]=100;   
      			iArray[6][3]=0; 
      			
      			iArray[7]=new Array();
      			iArray[7][0]="缴费间隔";  
      			iArray[7][1]="100px"; 
      			iArray[7][2]=100;   
      			iArray[7][3]=0; 
      			
      			iArray[8]=new Array();
      			iArray[8][0]="保单险种号";  
      			iArray[8][1]="100px"; 
      			iArray[8][2]=100;   
      			iArray[8][3]=3;
      			
      			iArray[9]=new Array();
      			iArray[9][0]="保单号";  
      			iArray[9][1]="100px"; 
      			iArray[9][2]=100;   
      			iArray[9][3]=3; 
      			
      			iArray[10]=new Array();
      			iArray[10][0]="缴费间隔";  
      			iArray[10][1]="100px"; 
      			iArray[10][2]=100;   
      			iArray[10][3]=3; 
      			
      			iArray[11]=new Array();
      			iArray[11][0]="豁免状态";  
      			iArray[11][1]="100px"; 
      			iArray[11][2]=100;   
      			iArray[11][3]=0;     			

				ExemptionGrid = new MulLineEnter( "fm" , "ExemptionGrid" );
				ExemptionGrid.mulLineCount = 1;
				ExemptionGrid.displayTitle = 1;
				ExemptionGrid.canSel=0;
				ExemptionGrid.canChk=1;
				
				ExemptionGrid.chkBoxAllEventFuncName = "selectAll";
				ExemptionGrid.chkBoxEventFuncName = "selectAll";
				
				ExemptionGrid.hiddenPlus=1;
				ExemptionGrid.hiddenSubtraction=1;
				ExemptionGrid.loadMulLine(iArray);
			}
			catch(ex)
			{
				alert(ex);
			}
		}

	</script>