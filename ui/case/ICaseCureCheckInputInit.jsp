<%
/*******************************************************************************
* Name     :ICaseCureInit.jsp
* Function :初始化“立案－费用明细信息”的程序
* Author   :Xx
* Date     :2005-7-23
*/
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//添加页面控件的初始化。
String LoadC = request.getParameter("LoadC")==null?"":request.getParameter("LoadC");
%>
<script language="JavaScript">

  var turnPage = new turnPageClass();

  function initInpBox(){
    try{
      fm.all('ReceiptNo').value = '';
      fm.all('FeeDate').value = '';
      fm.all('HospStartDate').value = '';
      fm.all('HospEndDate').value = '';
      fm.all('RealHospDate').value = '';
      fm.all('SeriousWard').value = '';
      fm.all('MainFeeNo').value = '';
      fm.all('inpatientNo').value='';

      fm.all('CaseNo').value = '<%= request.getParameter("CaseNo") %>'=='null'?'':'<%= request.getParameter("CaseNo") %>';

      <%
      GlobalInput mG = new GlobalInput();
      mG=(GlobalInput)session.getValue("GI");
      %>
      fm.cOperator.value = "<%=mG.Operator%>";

      fm.MakeDate.value = getCurrentDate();


      var strSQL1="select operator,startdate from llcaseoptime where caseno='"+fm.CaseNo.value
      +"' and rgtstate='03' order by sequance desc";
      var arrResult1 = easyExecSql(strSQL1);
      if(arrResult1){
          fm.cOperator.value = arrResult1[0][0];
          fm.MakeDate.value = arrResult1[0][1]
      }

      if(fm.all('CaseNo').value==''){
        fm.MngCom.value="<%=mG.ManageCom%>";
      }
      else{
        fm.MngCom.value="86"+fm.all('CaseNo').value.substr(1,2);
      }

      var strSQL2="select name from ldcom where comcode='"+fm.MngCom.value+"'";
      var arrResult2 = easyExecSql(strSQL2);
      if(arrResult2){
        fm.ComName.value= arrResult2[0][0];
      }
    }
    catch(ex){
      alert("在ICaseCureInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
    }
  }

  function initSelBox(){
    try{

    }
    catch(ex){
      alert("在ICaseCureInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
    }
  }

  function initForm(){
    try{
      initInpBox();
      initDutyRelaGrid();
      initIFeeCaseCureGrid();
      fm.LoadC.value="<%=LoadC%>";
      if (fm.LoadC.value=='2'){
        divconfirm.style.display='none';
        divCaseduty.style.display='none';
        Control.style.display = 'none';
        fm.FeeAtti.onclick = "";
        fm.FeeType.onclick = "";
        fm.FeeAffixType.onclick = "";
        fm.FirstInHos.onclick = "";
        fm.InsuredStat.onclick = "";
      }
      if(fm.LoadC.value=='3'){
        fm.idNext.style.display='none';
      }
      initQuery();

      initQueryZD();

    }
    catch(re){
      alert("ICaseCureInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
    }
  }


  function initDutyRelaGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","20","3");
      iArray[1]=new Array("责任编号","80px","50","0");
      iArray[2]=new Array("责任名称","150px","50","0");
      iArray[3]=new Array("合同号","100px","50","0");
      iArray[4]=new Array("给付起始日期","80px","50","0");
      iArray[5]=new Array("保额","80px","50","0");
      iArray[6]=new Array("档次","80px","5","0");

      DutyRelaGrid = new MulLineEnter( "fm" , "DutyRelaGrid" );
      DutyRelaGrid.mulLineCount = 3;
      DutyRelaGrid.displayTitle = 1;
      DutyRelaGrid.hiddenPlus=1;
      DutyRelaGrid.hiddenSubtraction=1;
      DutyRelaGrid.loadMulLine(iArray);
    }
    catch(ex){
      alert(ex.message);
    }
  }


</script>
