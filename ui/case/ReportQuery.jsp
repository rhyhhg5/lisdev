<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="ReportQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ReportQueryInit.jsp"%>
  <title>报案查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./ReportQueryOut.jsp" method=post name=fm target="fraTitle">
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLLReport1" style= "display: ''">
      <table  class= common>
      
        <TR  class= common>
          <TD  class= title>
            报案号
          </TD>
          <TD  class= input>
            <Input class= common name=RptNo >
          </TD>
          <TD  class= title>
            事故者姓名
          </TD>
          <TD  class= input>
            <Input class= common name=People >
          </TD>
          <TD  class= title>
            报案受理日期
          </TD>
          <TD  class= input>
            <input class="coolDatePicker" dateFormat="short" name="RptDate" >
          </TD>
        </TR>

        <TR  class= common>
          <TD  class= title>
            号码类型
          </TD>
          <TD  class= input>
            <Input class=code name=RptObj verify="号码类型|NOTNULL" CodeData="0|^0|团单^1|个单^2|客户" ondblClick="showCodeListEx('RptObjReport1',[this],[0,1,2]);" onkeyup="showCodeListKeyEx('RptObjReport1',[this],[0,1,2]);">
          </TD>
          <TD  class= title>
            号码
          <TD  class= input>
            <Input class= common name=RptObjNo >
          </TD>
          <Input class=common type = hidden name=People_Flag >
        </TR>
      </table>
    </Div>
          <INPUT class=cssButton VALUE="查  询" TYPE=button onclick="submitForm();return false;">
          <INPUT class=cssButton VALUE="返  回" TYPE=button onclick="returnParent();">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLReport2);">
    		</td>
    		<td class= titleImg>
    			 报案信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLLReport2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanReportGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
    <br>
      <INPUT VALUE="首  页" TYPE=button class=cssButton onclick="turnPage.firstPage();" >
      <INPUT VALUE="上一页" TYPE=button class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页" TYPE=button class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE="尾  页" TYPE=button class=cssButton onclick="turnPage.lastPage();">
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
