<html>
	<%
	//Name：LLReasonAffixInput.jsp
	//Function：
	//Date：2005-04-14
	//Author：AppleWood/Helga
	%>
	<%@page contentType="text/html;charset=GB2312" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LLReasonAffixInput.js"></SCRIPT>
		<%@include file="LLReasonAffixInputInit.jsp"%>
	</head>
	<body  onload="initForm();" >

		<form action="./LLReasonAffixSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<TR>
					<TD>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetupType);">
					</TD>
					<TD class= titleImg>
						材料配置类型
					</TD>
				</TR>
			</table>
			<Div  id= "divSetupType" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD>
							<input type="radio" value="01" name="OpType" onclick="easyQuery()">新增材料
							<input type="radio" value="02" name="OpType" checked onclick="easyQuery()">出险原因材料归属
						</TD>
					</TR>
				</table>
			</DIV>
			<div id="divNew" style = "display:'none'">
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAffixBaseList);">
						</TD>
						<TD class= titleImg>
							新增申请材料
						</TD>
					</TR>
				</table>
					<table  class= common>
						<TR  class= common8>
							<TD  class= title>材料类型</TD>
							<TD  class= input nowrap>
								<Input class=codeno name="AffixType" ondblClick="showCodeList('llmaffixtype',[this,AffixTypeName],[0,1],null,null,null,1);" onkeyup="showCodeListKey('llmaffixtype',[this,AffixTypeName],[0,1],null,null,null,1);"><Input class=codename name="AffixTypeName" >
							</td>
							<TD  class= title8 >
							<input name="QueryAll" style="display:''" class=cssButton type=button value="查  询" onclick="easyQueryBaseClick()">							</TD>
							<TD  class= input8></TD>
							<TD  class= title8 ></TD><TD  class= input8></TD>
						</tr>
					</table>
				<Div  id= "divAffixBaseList" style= "display: ''">
					<table  class= common>
						<TR  class= common>
							<TD text-align: left colSpan=1>
								<span id="spanAffixBaseGrid" >
								</span>
							</TD>
						</TR>
					</table>
				</Div>
			</div>

			<div id="divAttribute" style = "display:''">
				<table>
					<TR>
						<TD>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAffixReason);">
						</TD>
						<TD class= titleImg>
							出险原因材料归属
						</TD>
					</TR>
				</table>

				<Div  id= "divAffixReason" style= "display: ''">
					<table  class= common>
						<TR  class= common8>
							<TD  class= title>案件类型</TD>
							<TD  class= input nowrap>
								<Input class=codeno name="codetype" elementtype=nacessary ondblClick="showCodeList('llrgtreason',[this,codetypeName],[0,1]);" onkeyup="showCodeListKey('llrgtreason',[this,codetypeName],[0,1]);" ><Input class=codename type="" name="codetypeName" >
							</td>
							<TD  class= title8 ></TD><TD  class= input8></TD>
							<TD  class= title8 ></TD><TD  class= input8></TD>
						</tr>
					</table>

					<Div  id= "divAffixList" style= "display: ''">
						<table  class= common>
							<TR  class= common>
								<TD text-align: left colSpan=1>
									<span id="spanAffixGrid" >
									</span>
								</TD>
							</TR>
						</table>
					</Div>
				</div>
			</div>
					<input name="ModiSave" style="display:''" class=cssButton type=button value="保  存" onclick="ModifySave()">
					<input name="Delete" style="display:''" name="DelButton" id="DelButton" class=cssButton type=button value="删  除" onclick="DeleteClick()">

				<!--隐藏域-->
				<input type=hidden id="fmtransact" name="fmtransact">
				<input type=hidden id="operate" name="operate">

			</form>
			<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
		</body>
	</html>
