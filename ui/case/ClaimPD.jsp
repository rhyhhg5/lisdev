<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ClaimPD.jsp-"理赔批单打印"
//程序功能：将赔案号进行传递;
//创建人  ：刘岩松
//创建日期：2002-12-23
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.f1print.*"%>
<%
  System.out.println("开始执行打印操作");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Succ";

  	LLClaimSchema tLLClaimSchema = new LLClaimSchema();
    tLLClaimSchema.setClmNo(request.getParameter("ClmNo"));
    PayNoticeF1PUI tPayNoticeF1PUI   = new PayNoticeF1PUI();
    System.out.println("赔案号===="+tLLClaimSchema.getClmNo());
  //定义一个全局变量存储赔案号
    GlobalInput tG = new GlobalInput();
  	tG=(GlobalInput)session.getValue("GI");


  VData tVData = new VData();
  VData mResult = new VData();
  try
  {
    tVData.addElement(tG);
    tVData.addElement(tLLClaimSchema);
    //调用批单打印的类
    boolean bl = tPayNoticeF1PUI.submitData(tVData,"PRINT");
    if ( !bl)
	  {
	    String errMess = "不祥";
	    if ( tPayNoticeF1PUI.mErrors.needDealError() )
	    {
	         errMess = tPayNoticeF1PUI.mErrors.getFirstError();
	    }
	    
	    Content = "PRINT"+"失败，原因是:" + errMess;
	    FlagStr = "Fail";   
	  }
  }
  catch(Exception ex)
  {
    Content = "PRINT"+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
 
  if ( !FlagStr.equals("Fail") )
  {
	  mResult = tPayNoticeF1PUI.getResult();
	  XmlExport txmlExport = new XmlExport();
	  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	  if (txmlExport==null)
	  {
	    System.out.println("null");
	    Content = "打印失败";
	    FlagStr = "Fail";
	    
	    
	  }
	  else
	  {
	  	session.putValue("PrintStream", txmlExport.getInputStream());
	  	System.out.println("put session value");
	  	response.sendRedirect("../f1print/GetF1Print.jsp");
	  }
  }
  %>
  <html>
<script language="javascript">
	var  flagStr = "<%=FlagStr%>";
	if ( flagStr =="Fail")
	{
	   alert( "<%=Content%>" );
	}
	
	
</script>
</html>

