
<%
  /*******************************************************************************
   * Name     :LLRegisterCheckSaveAll.jsp
   * Function :事故/伤残/医疗信息的保存程序
   * Authorm  :MN
   * Date     :2008-6-10 20:40
   */
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK"%>
<%
  /**
     transact
    账单关联保存
     CHECKFINISH 审核完成
   **/
  EasyCaseCheckUI tCaseCheckUI = new EasyCaseCheckUI();
  AllCaseInfoUI tAllCaseInfoUI = new AllCaseInfoUI();
  ClaimCalBL aClaimCalAutoBL = new ClaimCalBL();
  CErrors tError = null;
  String tRela = "";
  String FlagStr = "";
  String Content = "";
  String transact = "INSERT";
  String strCaseNo = request.getParameter("CaseNo");
  String CustomerNo = request.getParameter("CustomerNo");
  String CaseRelaNo = request.getParameter("CaseRelaNo");
  String RgtNo = request.getParameter("RgtNo");
  System.out.println(strCaseNo + "aaa" + CaseRelaNo + "bbbb" + RgtNo);
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  tLLCaseSchema.setCaseNo(strCaseNo);
  tLLCaseSchema.setRgtNo(RgtNo);
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");
  VData tVData = new VData();
  VData tVData1 = new VData();
  VData tVData2 = new VData();
  tVData.addElement(tG);
  tVData1.addElement(tG);
  tVData1.addElement(tLLCaseSchema);
  tVData2.addElement(tLLCaseSchema);
  /////////////////////////////////////////////////////////////////////////////////////
  LLCaseInfoSet tLLCaseInfoSet = new LLCaseInfoSet();
  LLCaseCureSet tLLCaseCureSet = new LLCaseCureSet();
  LLOperationSet tLLOperationSet = new LLOperationSet();
  LLOtherFactorSet tLLOtherFactorSet = new LLOtherFactorSet();
  LLAccidentSet tLLAccidentSet = new LLAccidentSet();
  LLCaseInfoSchema mLLCaseInfoSchema = new LLCaseInfoSchema();
  LLSubReportSet tLLSubReportSet = new LLSubReportSet();
  mLLCaseInfoSchema.setCaseNo(request.getParameter("CaseNo"));

  //事件信息
  System.out.println("<--submit mulline-->");
  LLSubReportSchema tLLSubReportSchema = new LLSubReportSchema();
  tLLSubReportSchema.setSubRptNo(request.getParameter("SubReportNo"));
  tLLSubReportSchema.setCustomerNo(request.getParameter("CustomerNo"));
  tLLSubReportSchema.setCustomerName(request.getParameter("CustomerName"));
  tLLSubReportSchema.setAccDate(request.getParameter("AccDate"));
  tLLSubReportSchema.setAccDesc(request.getParameter("AccDesc"));
  tLLSubReportSchema.setAccPlace(request.getParameter("AccPlace"));
  tLLSubReportSet.add(tLLSubReportSchema);
  
  //其他录入要素

    LLOtherFactorSchema tLLOtherFactorSchema = new LLOtherFactorSchema();
    tLLOtherFactorSchema.setFactorType("9");
    tLLOtherFactorSchema.setFactorCode("9918");
    tLLOtherFactorSchema.setCaseNo(request.getParameter("CaseNo"));
    tLLOtherFactorSchema.setCaseRelaNo(CaseRelaNo);
    tLLOtherFactorSchema.setSubRptNo("");
    tLLOtherFactorSchema.setValue("");
    tLLOtherFactorSchema.setFactorName("社保补充手工报销审批表无疾病诊断理赔简易处理");
    tLLOtherFactorSchema.setRemark("");
    tLLOtherFactorSet.add(tLLOtherFactorSchema);

   try {
    tVData.addElement(tLLSubReportSet);
    tVData.addElement(CaseRelaNo);
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tLLCaseCureSet);
    tVData.addElement(tLLCaseInfoSet);
    tVData.addElement(tLLOtherFactorSet);
    tVData.addElement(tLLOperationSet);
    tVData.addElement(tLLAccidentSet);
    /////////////////////////////////////////////////////////////////////////////////////
    LLFeeMainSet tLLFeeMainSet = new LLFeeMainSet();   
    String tFeeAtti = request.getParameter("FeeAtti");
    if ("4".equals(tFeeAtti)) {
      String[] tChk4 = request.getParameterValues("EasySecuGridNo");
      String[] strFeeNo4 = request.getParameterValues("EasySecuGrid1");
      int intLength = tChk4.length;
      for (int i = 0; i < intLength; i++) {
          LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
          tLLFeeMainSchema.setCaseNo(strCaseNo);
          tLLFeeMainSchema.setMainFeeNo(strFeeNo4[i]);
          tLLFeeMainSchema.setCaseRelaNo(CaseRelaNo);
          tLLFeeMainSet.add(tLLFeeMainSchema);
      }
    }
    if ("2".equals(tFeeAtti)) {
      String[] tChk2 = request.getParameterValues("SGSecurityGridNo");
      String[] strFeeNo2 = request.getParameterValues("SGSecurityGrid1");
      int intLength = tChk2.length;
      for (int i = 0; i < intLength; i++) {
          LLFeeMainSchema tLLFeeMainSchema = new LLFeeMainSchema();
          tLLFeeMainSchema.setCaseNo(strCaseNo);
          tLLFeeMainSchema.setMainFeeNo(strFeeNo2[i]);
          tLLFeeMainSchema.setCaseRelaNo(CaseRelaNo);
          tLLFeeMainSet.add(tLLFeeMainSchema);
      }
    }
    tVData.addElement(tLLFeeMainSet);
    tAllCaseInfoUI.submitData(tVData, transact);
  }
  catch (Exception ex) {
    Content = transact + "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  
  if (FlagStr == "") {
    tError = tAllCaseInfoUI.mErrors;
    if (!tError.needDealError()) {
      try {
        if (tCaseCheckUI.submitData(tVData1, "CHECKFINISH")) {
          try {
            tVData2.addElement(tLLCaseSchema);
            tVData2.addElement(tG);
            System.out.println("......tClaimCalBL");
            aClaimCalAutoBL.submitData(tVData2, "autocal");
          }
          catch (Exception ex) {
            Content = transact + "失败，原因是:" + ex.toString();
            FlagStr = "Fail";
          }
        }
      }
      catch (Exception ex) {
        Content = transact + "失败，原因是:" + ex.toString();
        FlagStr = "Fail";
      }
      if (FlagStr=="")
    {
      tError = tCaseCheckUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 保存成功";
        FlagStr = "Succ";
      }
      else
      {
        Content = " 保存失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }
    }
  }
  else {
    Content = "保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
