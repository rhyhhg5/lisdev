<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：LLPrepaidClaimUnderWriteSave.jsp
//程序功能：预付赔款审批
//创建日期：2010-11-30
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>


<%
 //输出参数
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 
 CErrors tError = null;
 String FlagStr = "";
 String Content = "";
 String transact = "";
 
 LLPrepaidClaimUnderWriteUI tLLPrepaidClaimUnderWriteUI = new LLPrepaidClaimUnderWriteUI();
 LLPrepaidUWMainSchema tLLPrepaidUWMainSchema = new LLPrepaidUWMainSchema();
 
 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 transact = request.getParameter("fmtransact");

 System.out.println(request.getParameter("tPrepaidNo"));
 tLLPrepaidUWMainSchema.setPrepaidNo(request.getParameter("tPrepaidNo"));
 
 try
 {
  //准备传输数据VData
  VData tVData = new VData(); 
  tVData.add(tG);
  tVData.addElement(tLLPrepaidUWMainSchema);
  tLLPrepaidClaimUnderWriteUI.submitData(tVData,transact);
 }
 catch(Exception ex)
 {
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr=="")
 {
  tError = tLLPrepaidClaimUnderWriteUI.mErrors;
  if (!tError.needDealError())
  {                          
   VData tResultData = tLLPrepaidClaimUnderWriteUI.getResult();
   String strResult = (String) tResultData.getObjectByObjectName("String", 0);
   Content = strResult;
   FlagStr = "Success";
  }
  else                                                                           
  {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

