   <% 
  //Name:LLNOCaseStatisticsInput.jsp
  //Function:未结案案件案件时效报表
  //Date:2011-8-17 11:27
  //Author:yj
   %>

    <%@include file="../common/jsp/UsrCheck.jsp"%>


    <%@page contentType="text/html;charset=GBK" %>
    <%@include file="../common/jsp/UsrCheck.jsp"%>
    <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
    <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import = "com.sinosoft.utility.*"%>
    <%@page import="java.util.*"%> 
     <html>
    <head>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLNOCaseStatisticsInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
       <%
     GlobalInput tG1 = (GlobalInput)session.getValue("GI");
     String CurrentDate= PubFun.getCurrentDate();   
     String AheadDays="-90";
     FDate tD=new FDate();
     Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
     FDate fdate = new FDate();
     String afterdate = fdate.getString( AfterDate );
     %>
    <script>
	function initForm(){
    fm.ManageCom.value="<%=tG1.ManageCom%>";
    var arrResult=easyExecSql("select Name from ldcom where comcode='<%=tG1.ManageCom%>'");
    fm.ComName.value=arrResult[0][0];
    }
   </script>
    </head>
	
	<body onload="initForm();" >
	<form method=post name=fm target="fraSubmit">
	<table class= common border=0 width=100%>
	  <tr class=common>
		 <td class= titleImg>
			未结案案件时效报表
		 </td>
	  </tr>
	 </table>
	
	 <table class=common border=0 width=100%>
      <tr  class= common>
         <td  class= title>管理机构</td>
         <td  class= input> <input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>   
         <td  class= title>案件状态</td>
         <td  class= input> <input class="codeno" name=RgtState elementtype=nacessary  ondblclick="return showCodeList('rgtstate',[this,RgtStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('rgtstate',[this,RgtStateName],[0,1],null,null,null,1);"><input class=codename name=RgtStateName></TD>      
       </tr>  
       <tr  class= common>
         <TD  class= title >处理人</TD>
         <td  class= input> <Input class="codeno" name=UserCode elementtype=nacessary  verify="用户名|code:llusercode&NOTNULL" ondblclick="return showCodeList('llusercode',[this,UserName],[0,1],null,'1 and comcode like  #'+fm.ManageCom.value+'%#','1');" onkeyup="return showCodeListKey('llusecode',[this,UserName],[0,1],null,'1 and comcode like  #'+fm.ManageCom.value+'%#','1');" ><input class="codename" name=UserName elementtype=nacessary  ></td>          			
          <td  class= title>处理人上级</td>
         <td  class= input> <input class="codeno" name=UpUserCode elementtype=nacessary  ondblclick="return showCodeList('llupusercode',[this,UpUserName],[0,1],null,fm.ManageCom.value,'comcode',1);" onkeyup="return showCodeListKey('llupusercode',[this,UpUserName],[0,1],null,fm.ManageCom.value,'comcode',1);"><input class=codename name=UpUserName></TD>      
       </tr>
       <tr  class= common>
         <td  class= title>批次号</td>
         <TD  class= input> <input class=common name=PNo ></TD> 
         <td  class= title>理赔号</td>
         <TD  class= input> <input class=common name=CaseNo ></TD> 
       </tr>  
       <tr  class= common>
         <td  class= title>证件号码</td>
         <TD  class= input> <input class=common name=ZJNo ></TD> 
       </tr>  
       <tr  class= common>
         <td  class= title>时效天数起期</td>
         <TD  class= input> <input class=common name=StartDateNo ></TD> 
         <td  class= title>时效天数止期</td>
         <TD  class= input> <input class=common name=EndDateNo ></TD> 
       </tr>  
   </table>
        <hr>
    <table class=common>
    	<tr class=common>
          <Input type =button class=cssButton value="打  印" onclick="Print()">	
			</tr>
		</table>
		 <input type= hidden name=StartDate>
		<input type= hidden name=EndDate>
		<input type= hidden name=fmtransact>
	    </form>
	    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
   </body>

</html>