var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var turnPage5 = new turnPageClass();
var turnPage6 = new turnPageClass();
var mEdorNo;
var mEdorType;
var mContno;
var mFlag = 0; //核保完毕标志



//查询核保信息
function queryUWErr()
{
	var sql = "select '理赔二核' as  项目类型,Missionprop1 as 赔案号, " +
	          " missionId as 核保任务号,Missionprop3 as  被保人, " +
				" Missionprop12 as 送核原因,Missionprop10 as 工单号 ,Missionprop11,missionprop2 " + 
			" from LWMission where ActivityID='0000001182' ";
	if(fm.MissionId.value!=""){
		sql = sql+" and MissionId = '"+fm.MissionId.value+"' ";
	}
	if(fm.CaseNo.value!=""){
		sql = sql+" and Missionprop1 = '"+fm.CaseNo.value+"' ";
	}
	if(fm.EdorNo.value!=""){
		sql = sql+" and Missionprop10 = '"+fm.EdorNo.value+"' ";
	}
  turnPage.pageLineNum = 100 ;
	turnPage.queryModal(sql, UWErrGrid);
	if (UWErrGrid.mulLineCount > 0)
	{
		if (UWErrGrid.mulLineCount == 1)
		{
		  fm.UWErrGridSel.checked = "true";
		}
		else
		{
		  fm.UWErrGridSel[0].checked = "true";
		}
	}
	fm.EdorType.value = UWErrGrid.getRowColDataByName(0,"Missionprop11"); //工单类型 
}

//得到待核保的险种信息
function queryUWPol()
{
	var edorNo = fm.EdorNo.value;
	var sql = "SELECT '理赔二核' AS 项目类型,LCcont.CONTNO AS 保单号,LCcont.PRTNO AS 印刷号," + 
	" b.INSUREDNO AS 被保人客户号, b.NAME AS 被保人,LCcont.PREM AS 保费,LCcont.AMNT AS 保额,LCcont.MULT AS 档次," +
	" LCcont.CVALIDATE AS 保险起期, LCcont.CINVALIDATE AS 保险止期,LCcont.PAYINTV AS 交费间隔, " +
	" CASE WHEN (SELECT 1 FROM LCPOL WHERE '1388475130000' = '1388475130000' " +
	" AND LCPOL.CONTNO = LCCONT.CONTNO AND NOT EXISTS (SELECT 1  FROM LPUWMASTER WHERE EDORNO = '"+edorNo+"' " +
	" AND POLNO = LCPOL.POLNO AND EDORTYPE = 'SP' AND AUTOUWFLAG = '2') FETCH FIRST 1 ROWS ONLY)" +
	" IS NULL THEN '已核保' ELSE '未核保' END, 'SP' AS 工单类型 " + 
	" FROM LCCONT , LCINSURED b WHERE LCCONT.CONTNO = b.CONTNO and lccont.conttype='1' AND b.INSUREDNO = '"+fm.InsuredNo.value+"'" ;
	turnPage2.pageLineNum = 100 ;          
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, PolGrid);
	//选中第一行
	PolGrid. radioBoxSel (1);
	//ContNo和PrtNo保存第一个保单的信息
	fm.ContNo.value = PolGrid.getRowColData(0, 2); 
	fm.PrtNo.value = PolGrid.getRowColData(0, 3); 
}


//得到待核保的理赔信息
function queryUWCase()
{
	var selNo = PolGrid.getSelNo() - 1;
	if (selNo == -1)
	{
		return false;
	}
	this.mEdorNo = fm.EdorNo.value;
  this.mContno = PolGrid.getRowColData(selNo, 2);
	this.mEdorType = PolGrid.getRowColDataByName(selNo, "EdorType");	
	var caseNo = fm.CaseNo.value;
	
    var sql = " select '"+caseNo+"' as 理赔号, "
             +" insuredno as 客户号,insuredname as 客户名称,polno as 险种号, "
			 +" contno as 被保险人持有的保单,riskcode as 险种代码, "
			 +" (select riskname from lmriskapp where riskcode = lcpol.riskcode ) as 险种名称, "
			 +" CodeName('uwflag', (select uwflag from lccont where contno = lcpol.contno)) as 承保时的核保结论 , "
			 +" case when (select edorno from LPUWMaster where polno=lcpol.polno and EdorNo = '" + mEdorNo + "' )" 
			 +" is null then '未核保' else '已核保' end "
			 +" from lcpol where contno='"+mContno+"' and insuredno='"+fm.InsuredNo.value+"'"
			 +" with ur ";
	turnPage6.pageLineNum = 100 ;          
	turnPage6.pageDivName = "divPage6";
	turnPage6.queryModal(sql, CaseGrid);
//	//ContNo和PrtNo保存第一个保单的信息
//	fm.ContNo.value = PolGrid.getRowColData(0, 3); 
//	fm.PrtNo.value = PolGrid.getRowColData(0, 4); 
}



function initUWInfo()
{
	document.all("trAddCondition").style.display = "none";
	document.all("trAddFee").style.display = "none";
	document.all("trAddSpec").style.display = "none";
	document.all("trSubMult").style.display = "none";
	document.all("trSubAmnt").style.display = "none";
  document.all("tdDisagreeDeal1").style.display = "none";
	document.all("tdDisagreeDeal2").style.display = "none";
	fm.DisagreeDeal.verify = "";
	fm.AddFeeFlag.checked = false;
	fm.SpecFlag.checked = false;
	fm.SubMultFlag.checked = false;
	fm.SubAmntFlag.checked = false;
	fm.PassFlag.value = "1";
	fm.UWIdea.value = "";
}

//查询核保信息，选择每个项目时触发
function queryUWInfo()
{
	initCaseGrid(); 
	var selNo = PolGrid.getSelNo() - 1;
	if (selNo == 0)
	{
		return false;
	}	
	this.mEdorNo = fm.EdorNo.value;
  this.mContno = PolGrid.getRowColData(selNo, 2);
	this.mEdorType = PolGrid.getRowColDataByName(selNo, "EdorType");

	var sql = "select PassFlag, UWIdea, DisagreeDeal, AddPremFlag, " +
	          "       SpecFlag, SubMultFlag, SubAmntFlag, Mult, Amnt " +
	          "from LPUWMaster " +
	          "where EdorNo = '" + mEdorNo + "' " +
	          "and EdorType = '" + mEdorType + "' " +
	          "and PolNo = '" + mPolNo + "' " +
	          "and AutoUWFlag = '2'";
	var result = easyExecSql(sql);
	if (result)
	{
		try 
		{
			fm.PassFlag.value = result[0][0];
			fm.UWIdea.value = result[0][1];
			fm.DisagreeDeal.value = result[0][2];
			var addPremFlag = result[0][3];
			var specFlag = result[0][4];
			var subMultFlag = result[0][5];
			var subAmntFlag = result[0][6];
			fm.InputMult.value = result[0][7];
			fm.InputAmnt.value = result[0][8];
			if (fm.PassFlag.value == "4")
			{
				document.all("trAddCondition").style.display = "";
        document.all("tdDisagreeDeal1").style.display = "";
        document.all("tdDisagreeDeal2").style.display = "";
        fm.DisagreeDeal.verify = "客户不同意处理|notnull&code:DisagreeDeal";
				if (addPremFlag == "1")
				{
					queryPrem();
					fm.AddFeeFlag.checked = true;
        	document.all("trAddFee").style.display = "";
				}
				if (specFlag == "1")
				{
					querySpec();
					document.all("trAddSpec").style.display = "";
	        fm.SpecFlag.checked = true;
				}
				if (subMultFlag == "1")
				{
				  showSubMult();
					document.all("trSubMult").style.display = "";
					fm.SubMultFlag.checked = true;
				}
				if (subAmntFlag == "1")
				{
				  showSubAmnt();
					document.all("trSubAmnt").style.display = "";
					fm.SubAmntFlag.checked = true;
				}
			}
		}
		catch (ex){}
	}
	if(fm.PassFlag.value=="4"){
		
		fm.PassFlag1.value="2";
	}
	else if (fm.PassFlag.value=="5"){
		
		fm.PassFlag1.value="3";
	}
	else
	fm.PassFlag1.value = fm.PassFlag.value;
	
	fm.EdorNo.value = mEdorNo;
	fm.EdorType.value = mEdorType;
	fm.ContNo.value = PolGrid.getRowColData(selNo, 3);
	fm.PolNo.value = mPolNo;
	fm.InsuredNo.value = PolGrid.getRowColData(selNo, 7);
	queryImapart();
	showAllCodeName();
}



/*********************************************************************
 *  投保书查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function scanQuery()
{
	var arrReturn = new Array();
	var arrReturn1 = new Array();
	var tBussNoType = "";
	var tBussType = "";
    var tSubType = "";
	//获的个人保单印刷号
    
  var tContNo = fm.ContNo.value;
  if (tContNo == "")
  {
    tContNo= trim(fm.all('ContNo').value);
	}
	if (tContNo == "")
	{
	  alert("请选择一个保单！");
	  return false;
	}
		 
  //查询团单记录(撤保或退保)
    var strSql = "select PrtNo from LCGrpCont where GrpContNo='" + tContNo + "'";
  var arrReturn = new Array();
  arrReturn = easyExecSql(strSql);
  
  //查询团单记录(撤保或退保)
    var strSql1 = "select PrtNo from LBGrpCont where GrpContNo='" + tContNo + "'";
  var arrReturn1 = new Array();
  arrReturn1 = easyExecSql(strSql1);
  
  if (arrReturn == null && arrReturn1 ==null ) {
  
        var arrReturn2 = new Array();
  	var strSql2 = "select PrtNo from LCCont where ContNo='" + tContNo + "'";
  	arrReturn2 = easyExecSql(strSql2);
  
          //查询团单记录(撤保或退保)
  	var strSql3 = "select PrtNo from LBCont where ContNo='" + tContNo + "'";
  	var arrReturn3 = new Array();
  	arrReturn3 = easyExecSql(strSql3);
          
  	//ContLoadFlag: 1 个单; 2 团单
  	//ContType: 1 签单; 2 撤保/退保
  	if (arrReturn2 == null && arrReturn3 ==null ) {
  		alert("没有该合同号，请重新输入");
  	} else if(arrReturn3 == null ){	
  		
  		var tProNo = arrReturn2[0][0];
              window.open("../sys/LCProposalScanQuery.jsp?prtNo="+ tProNo + "&SubType=TB1001&BussType=TB&BussNoType=11" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");					
  
  	}else if(arrReturn2 == null ){	
  
  		var tProNo = arrReturn3[0][0];
              window.open("../sys/LCProposalScanQuery.jsp?prtNo="+ tProNo + "&SubType=TB1001&BussType=TB&BussNoType=11" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");				
  	}
  
    } else if(arrReturn == null){
  
  	var tProNo = arrReturn1[0][0];
          window.open("../sys/LCProposalScanQuery.jsp?prtNo="+ tProNo + "&SubType=TB1002&BussType=TB&BussNoType=12" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");				
  }else if(arrReturn1 == null ){	
  	var tProNo = arrReturn[0][0];
          window.open("../sys/LCProposalScanQuery.jsp?prtNo="+ tProNo + "&SubType=TB1002&BussType=TB&BussNoType=12" , "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");				
  }
}

//显示案件扫描影印件
function showEdorScan()
{  
	var caseNo = fm.CaseNo.value;
    var url = "./EsDocPagesQueryMain.jsp?DocCode=" + caseNo;
	window.open(url, "", "status:no;help:0;close:0");
}

//显示既往核保信息
function showUWInfo()
{
	var selNo = PolGrid.getSelNo();
	if (selNo == 0)
	{
		alert("请选择一个保单！");
		return false;
	}
	
	var sql = "select ProposalContNo from LCCont " + 
	          "where ContNo = '" + PolGrid.getRowColData(selNo - 1, 2) + "' ";
	var result = easyExecSql(sql);
	if (!result)
	{
		alert("未找到合同信息！");
		return false;
	}

	var tUWESelNo = UWErrGrid.getSelNo();
  	var varSrc = "&CaseNo=" + UWErrGrid.getRowColData(tUWESelNo-1, 2);
  	varSrc += "&InsuredNo=" + UWErrGrid.getRowColData(tUWESelNo-1, 8);
  	varSrc += "&CustomerName=" + UWErrGrid.getRowColData(tUWESelNo-1, 4);
  	pathStr="./FrameMainLLUnderWrit.jsp?Interface=LLUnderWritInput.jsp"+varSrc;
  	showInfo = OpenWindowNew(pathStr,"LLUnderWritInput","middle",800,500);
}

//显示调查报告信息
function showInvestInfo()
{
	var caseNo = fm.CaseNo.value;
	
	var url = "./LLUWSuveryReplyList.jsp?caseNo=" + caseNo ;
	window.open(url);
}

//显示保单信息
function showContInfo()
{
	var url = "../app/ProposalEasyScan.jsp?" +
	      "ContNo=" + fm.ContNo.value +
	      "&prtNo=" + fm.PrtNo.value;  //这里&prtNo中p是小写
        "&LoadFlag=6";
	window.open(url, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  return true;
}

//TODO:初始化核保结论
function initDecision(){
	fm.PassFlag1.value = "";
	fm.PassFlagName.value = "";
	fm.UWIdea.value = "";
	var selNo = CaseGrid.getSelNo() -1;
	var tEdorNo = fm.EdorNo.value;
	var tPolNo = CaseGrid.getRowColData(selNo, 4);
	var tQueryUWIdeaSql = "select passflag,uwidea from lpuwmaster where edorno='"+tEdorNo+"' and polno='"+tPolNo+"'";
	var tResultUWIdea = easyExecSql(tQueryUWIdeaSql);
	if(tResultUWIdea != null){
		fm.PassFlag1.value = tResultUWIdea[0][0];
		if(fm.PassFlag1.value == "1"){
			fm.PassFlagName.value = "标准承保"
		}else if(fm.PassFlag1.value == "2"){
			fm.PassFlagName.value = "变更承保"
		}else if(fm.PassFlag1.value == "3"){
			fm.PassFlagName.value = "拒保"
		}
		fm.UWIdea.value = tResultUWIdea[0][1];
	}
}

//保存核保结论
function saveDecision()
{
	var selNo = CaseGrid.getSelNo() -1;	
	if (selNo== -1)
	{
		alert("请选择需要核保的理赔信息！");
		return false ;
	} 

	//准备提交数据
	var edorType = "SP";
	var contNo = CaseGrid.getRowColData(selNo, 5);
	var insuredNo = CaseGrid.getRowColData(selNo, 2); 
	var polno = CaseGrid.getRowColData(selNo, 4); 
	fm.EdorType.value=edorType;
	fm.ContNo.value=contNo;
	fm.InsuredNo.value=insuredNo;
	fm.PolNo.value=polno;
	fm.fmtransact.value="saveUW";
	
	if(fm.PassFlag1.value =="2"){
		
		fm.PassFlag.value ="2";
	}
	else if (fm.PassFlag1.value =="3"){
		
		fm.PassFlag.value ="3";
	}
	else if (fm.PassFlag1.value =="1"){
		
		fm.PassFlag.value ="1";
	}
	
  if (!beforeSubmit())
  {
    return false;
  }
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./LLSecondLPUWManuSave.jsp";
	fm.submit(); //提交
}

//确认核保完毕
function confirmDecision()
{
  if (!confirm("确认核保完毕？"))
  {
    return false;
  }
  
	fm.fmtransact.value="finishUW";
	
  
  mFlag = 1;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./LLSecondLPUWManuSave.jsp";
	fm.submit(); //提交
}

//提交数据后执行的操作
function afterSubmit(flag, content)
{
	showInfo.close();
	window.focus();
	if (flag == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	if (mFlag == 0)
	{
	  queryUWPol(); 
	  queryUWCase();//更新险种核保状态
	}
  else
  {
  	if (flag == "Succ")
  	{
	  	top.window.close();
	    top.opener.window.focus();
	    top.opener.window.location.reload();
	  }
  }
}

//创建工作流节点
function makeWorkFlow()
{
	fm.action = "./PEdorUWManuWorkFlow.jsp";
  fm.submit();
}

//查询人工核保结论
function queryManuUW() 
{
	var sql = "select EdorType, EdorType, ContNo, PolNo, " +
	          "       (select RiskSeqNo from LCPol where PolNo = LPUWMaster.PolNo) as RiskSeqNo, " +
	          "       (select riskCode from LCPol where PolNo = LPUWMaster.PolNo), InsuredNo, InsuredName, " +
	          "       SugUWIdea, AddPremFlag, SpecFlag, SubMultFlag, UWIdea, MakeDate, " +
	          "       case when DisagreeDeal = '1' then '终止申请' when DisagreeDeal = '2' then '终止险种效力' end , " +
	          "       case when CustomerReply = '1' then '同意' when CustomerReply = '2' then '不同意' end "+
						"from LPUWMaster " +
						"where EdorNo = '" + mEdorNo + "' " +
						"and AutoUWFlag = '2'" +
						"order by RiskSeqNo";
	turnPage5.pageLineNum = 100 ; 
	turnPage5.pageDivName = "divPage5";
	turnPage5.queryModal(sql, ManuUWGrid);
}

//查询返回
function afterCodeSelect( cCodeName, Field )
{
  //选择了处理
  if( cCodeName == "DealWith")
  {
    LoadC="1";
    LoadD="1";
    switch(fm.all('DealWith').value){
      case "0":
      DealConsult(LoadC,LoadD);             //咨询通知
      window.focus();              //咨询通知
      break;
      case "1":
      DealApp(LoadC,LoadD);                 //受理申请
      window.focus();              //咨询通知
      break;
      case "2":
      DealAppGrp(LoadC,LoadD);              //团体受理申请
      window.focus();              //咨询通知
      break;
      case "3":
      DealFeeInput(LoadC,LoadD);            //帐单录入
      window.focus();              //咨询通知
      break;
      case "4":
      DealCheck(LoadC,LoadD);               //检录
      window.focus();              //咨询通知
      break;
      case "5":
      DealClaim(LoadC,LoadD);               //理算
      window.focus();              //咨询通知
      break;
      case "6":
      DealApprove(LoadC,LoadD);             //审批审定
      window.focus();              //咨询通知
      break;
      case "7":
      DealCancel(LoadC,LoadD);							 //撤件
      window.focus();              //咨询通知
      break;
 	  case "8":
      openSubmittedAffix(LoadC,LoadD);					//材料退回登记
      window.focus();  
       break;
    }
  }
}

//咨询通知 -- ok
function DealConsult(LoadC,LoadD)
{
 
  var caseNo= fm.CaseNo.value;

  var varSrc="&CaseNo="+ caseNo;// 为了保证接口一致性，做了些调整
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseNote.jsp?Interface= LLMainAskInput.jsp"+varSrc,"咨询通知","left");

}

//受理申请 --ok
function DealApp(LoadC,LoadD)
{

  var caseNo= fm.CaseNo.value; 
  var RgtNo="";
  strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }
  var varSrc= "&RgtNo="+ RgtNo + "&CaseNo="+ caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseRequest.jsp?Interface=LLRegisterInput.jsp"+varSrc,"","left");

}

//团体受理申请 --ok
function DealAppGrp(LoadC,LoadD)
{
  var caseNo=fm.CaseNo.value;
  var RgtNo="";

  strSQL="select rgtno from llcase where caseno='"+caseNo+"'";
  arrResult = easyExecSql(strSQL);

  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
  }

  var varSrc="&RgtNo="+ RgtNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainGrpCheck.jsp?Interface=LLGrpRegisterInput.jsp"+varSrc,"","left");

}

//帐单录入 --ok
function DealFeeInput(LoadC,LoadD)
{	
  var CaseNo= fm.CaseNo.value ;
  var varSrc = "&CaseNo=" + CaseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow =OpenWindowNew("./FrameMainZD.jsp?Interface=ICaseCureInput.jsp" + varSrc,"","left");

}

//检录 --ok
function DealCheck(LoadC,LoadD)
{
  var caseNo = fm.CaseNo.value;

  var varSrc="&CaseNo="+ caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseCheck.jsp?Interface=LLRegisterCheckInput.jsp"+
  varSrc,"检录","left");

}

//理算 --OK
function DealClaim(LoadC,LoadD)
{
  var caseno = fm.CaseNo.value;

  var varSrc="&CaseNo=" + caseno ;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainCaseClaim.jsp?Interface=LLClaimInput.jsp"+
  varSrc,"理算","left");
}

//审批审定 --ok
function DealApprove(LoadC,LoadD)
{
  var caseNo = fm.CaseNo.value;

  var varSrc = "&CaseNo=" + caseNo;
  varSrc += "&LoadC="+LoadC;
  varSrc += "&LoadD="+LoadD;
  var newWindow = OpenWindowNew("./FrameMainUnderWrite.jsp?Interface=ClaimUnderwriteInput.jsp?MenuFlag=0" + varSrc,"","left");

}

//撤件 --ok
function DealCancel(LoadC,LoadD)
{
  var caseno = fm.CaseNo.value;
    caseno = CaseInfo.getRowColData(selno,2);
    strSQL="select CustomerNo,CustomerName from llcase where caseno='"+caseno+"'";
    arrResult = easyExecSql(strSQL);
    if(arrResult != null)
     {
      CustomerNo = arrResult[0][0];
      CustomerName=arrResult[0][1];
     }
     var varSrc="&CaseNo=" + caseno +"&CustomerNo="+CustomerNo+"&CustomerName="+CustomerName;
     varSrc += "&LoadD="+LoadD;
  showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  
}

//材料退回登记 --ok
function openSubmittedAffix(LoadC,LoadD)
{
   var caseno = fm.CaseNo.value ;
  strSQL="select rgtno,rgtstate from llcase where caseno='"+caseno+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    RgtNo = arrResult[0][0];
    RgtState=arrResult[0][1];
  }
  if(RgtState=="09"||RgtState=="12")
  {
	  //showInfo = window.open("./SubmittedAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value);
	  pathStr="./SubmittedAffixMain.jsp?CaseNo="+caseno+"&RgtNo="+RgtNo+"&LoadD="+LoadD;
	  showInfo = OpenWindowNew(pathStr,"","middle");
	}else
		{
			alert("未结案，不能回退");
		}
}

