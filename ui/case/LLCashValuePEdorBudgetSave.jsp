<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<% 
//程序名称：LLCashValuePEdorBudgetSave.jsp
//程序功能：退保试算
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String flag = "Succ";
  String content = "现金价值试算成功";
  String remark = "无";
  long startTime = System.currentTimeMillis();
  
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  
  String contNo = request.getParameter("contNo");
  String edorType = request.getParameter("edorType");
  String edorNo = request.getParameter("edorNo");
  boolean needResultFlag = (edorNo != null && !edorNo.equals("") 
                            && edorType != null && !edorType.equals(""));
  System.out.println("\n" + needResultFlag);
  String edorValiDate = request.getParameter("edorValiDate");
  String payToDateLongPol = request.getParameter("payToDateLongPol");
  
  EdorCalZTTestBL tEdorCalZTTestBL = new EdorCalZTTestBL();
  tEdorCalZTTestBL.setEdorValiDate(edorValiDate);
  tEdorCalZTTestBL.setCurPayToDateLongPol(payToDateLongPol);
  tEdorCalZTTestBL.setNeedBugetResultFlag(needResultFlag);
  tEdorCalZTTestBL.setOperator(tGI.Operator);
  LPEdorItemDB tLPEdorItemDB = new LPEdorItemDB();
  
  String[] tChecks = request.getParameterValues("InpLCPolGridChk");
  String[] tPolNos = request.getParameterValues("LCPolGrid1");
  if(tChecks != null)
  {
  
    LPBudgetResultSet tLPBudgetResultSet = new LPBudgetResultSet();
    for(int i = 0; i < tChecks.length; i++)
    {
      if (!tChecks[i].equals("1"))
      {
        continue;
      }
      double getMoney = 0;  //退保退费
      
      
      LJSGetEndorseSchema tLJSGetEndorseSchema 
        = tEdorCalZTTestBL.budgetOnePol(tPolNos[i]);
      System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
      System.out.println(tEdorCalZTTestBL.mErrors.needDealError());
      System.out.println("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");
      if(tEdorCalZTTestBL.mErrors.needDealError())
      {
        flag = "Fail";
        content = tEdorCalZTTestBL.mErrors.getErrContent() ;
        content = content.replaceAll("退保","现金价值");
        content = content.replaceAll("保全","理赔");
        
        break;
      }
      LGErrorLogSet tLGErrorLogSet = tEdorCalZTTestBL.getOnePolLGErrorLogSet();
      System.out.println(tLGErrorLogSet.size());
      if(tLGErrorLogSet.size()>0)
      {
      	remark ="";
      	for(int j=1;j<=tLGErrorLogSet.size();j++)
      	{
      		remark +=tLGErrorLogSet.get(j).getDescribe();
      		System.out.println("计算过程出现错误："+tLGErrorLogSet.get(j).getDescribe());
      	}
      }
      if(tLJSGetEndorseSchema != null)
      {
        getMoney += tLJSGetEndorseSchema.getGetMoney();
        getMoney = PubFun.setPrecision(getMoney, "0.00");
        if(getMoney != 0)
        {
          getMoney = -getMoney;
        }
        
        LPBudgetResultSchema tLPBudgetResultSchema = tEdorCalZTTestBL.getOnePolLPBudgetResult();
        tLPBudgetResultSet.add(tLPBudgetResultSchema);
      }
%>      
<script language="javascript">
      parent.fraInterface.LCPolGrid.setRowColDataByName("<%=i%>", "getMoney", "<%=getMoney%>");
      parent.fraInterface.LCPolGrid.setRowColDataByName("<%=i%>", "remark", "<%=remark%>");      
</script>
<%      
      if(flag.equals("Fail"))
      {
        break;
      }
    }
    
    if(needResultFlag)
    {
      for(int i = 1; i <= tLPBudgetResultSet.size(); i++)
      {
        tLPBudgetResultSet.get(i).setEdorNo(edorNo);
        tLPBudgetResultSet.get(i).setEdorType(edorType);
        tLPBudgetResultSet.get(i).setOperator(tGI.Operator);
      }
      MMap map = new MMap();
      map.put("delete from LPBudgetResult "
        + "where edorNo = '" + edorNo 
        + "' and edorType = '" + edorType 
        + "' and contNo = '" + contNo + "' ", "DELETE");
      map.put(tLPBudgetResultSet, "INSERT");
      VData data = new VData();
      data.add(map);
      PubSubmit p = new PubSubmit();
      if(!p.submitData(data, ""))
      {
        flag = "Fail";
        content += ", 存储试算结果出错。";
      }
    }
  }
  System.out.println(System.currentTimeMillis() - startTime);
  content = PubFun.changForHTML(content);
  
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>

