/*******************************************************************************
* Name: LLRegisterInput.js
* Function:立案主界面的函数处理程序
* Author:LiuYansong
*/
var showInfo;
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();
/*******************************************************************************
* Name        :main_init
* Author      :LiuYansong
* CreateDate  :2003-12-16
* ModifyDate  :
* Function    :初始化主程序
*/

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try{
    initForm();
  }
  catch(re){
    alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
  if(cDebug=="1"){
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else{
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

//对页面上的关键信息进行查询
function ClientafterQuery(rgtNo,caseNo){
    //query grpcontno and grpname
	var strsql = "select rgtobjno,grpname from llregister where rgtno='"+RgtNo +"'";
      var arrResult = easyExecSql(strsql);

    if ( arrResult ){
      fm.CustomerName.value= arrResult[0][1];
      fm.ContNo.value= arrResult[0][0];
   }
   var tRiskSql ="select riskcode,(select riskname from lmrisk where riskcode=a.riskcode) from lcgrppol a where grpcontno='"+ fm.ContNo.value +"'"
   			+"  union select riskcode,(select riskname from lmrisk where riskcode=a.riskcode) from lbgrppol a where grpcontno='"+ fm.ContNo.value +"'";
	var arrRisk = easyExecSql(tRiskSql);
    if ( arrRisk ){
   	  fm.RiskCode.value = arrRisk[0][0];
   	  fm.RiskName.value = arrRisk[0][1];
    }else{
   	  alert("保单险种查询失败");
   	  return false;
    }
//查询应付金额指该保单已经给付确认案件账单的高段二金额
var strsq2 = "select nvl(sum(highamnt2),0) from llsecurityreceipt where caseno in (select a.caseno from llclaimdetail a,llcase b where a.caseno=b.caseno and a.grpcontno='"+fm.ContNo.value +"' and b.rgtstate in('11','12'))";
    var arrResult2 = easyExecSql(strsq2);
    if ( arrResult2 ){
      fm.ShouldGive.value= arrResult2[0][0];
   }
//实付金额指保单下已给付确认的批次案件在受理时录入的申报金额之和以及历史拨付金额的合计金额
var strsq3 = "select nvl(sum(pay),0) from ljagetclaim a where a.grpcontno='"+fm.ContNo.value +"' "
		   +" and a.otherno in (select rgtno from llcase where rgtno=a.otherno and rgtstate in ('11','12')) with ur";
    var arrResult3 = easyExecSql(strsq3);
    if ( arrResult3 ){
      fm.RealGive.value= arrResult3[0][0];
   }
   
//查询历史拨付金额指通过弹性拨付给出的金额
	var strsq4 = "select coalesce(sum(realPay),0) from LLSpringGiveResult where grpcontno='"+fm.ContNo.value +"'"; 
	var arrResult4 = easyExecSql(strsq4); 

//查询本次赔款是否已经结案
	var sqlPay = "select rgtstate from llcase where caseno='"+fm.CaseNo.value +"'"; 
	var arrResultPay = easyExecSql(sqlPay)
//查询本次赔款
	var sqlRealPay = "select coalesce(sum(realpay),0) from LLSpringGiveResult where caseno='"+fm.CaseNo.value +"'"; 
	var arrResultRealPay = easyExecSql(sqlRealPay)
	
	if(arrResultPay =='11')
	{
		fm.HistoryGive.value=arrResult4[0][0];
		fm.RealPay.value='0';
	}
    else 
    {
    	fm.HistoryGive.value=parseInt(arrResult4[0][0])-parseInt(arrResultRealPay[0][0]);
		fm.RealPay.value=arrResultRealPay[0][0];
    }
   var shouldGive=fm.ShouldGive.value;
   var realGive=fm.RealGive.value;
   fm.Balance.value=mathRound(shouldGive-realGive);
    
    
}
//提交拨付金额
function RgtFinish(){
	var strsq5 = "select handler,rgtstate from llcase where caseno='"+fm.CaseNo.value +"'"; 
    var arrResult5 = easyExecSql(strsq5);
    if ( arrResult5 ){
      var tHandler= arrResult5[0][0];
      var tRgtState= arrResult5[0][1];
      if(fm.Handler.value!=tHandler){
      	alert("对不起，您不是指定的处理人，指定处理人为"+tHandler);
      	return false;
      }
      if(tRgtState!='01'){
      	alert("对不起，该状态下不能执行此操作!");
      	return false;
      }
      
   }
	
	//对拨付金额的判断
	if((fm.Balance.value-fm.HistoryGive.value)<fm.RealPay.value)
	{
	  if(!confirm("实付金额大于差额，是否继续"))
	  {
      	return false;
      }
	}
		
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();	
}


function queryLLSprint()
{
  var strSQL="select  distinct grpcontno,grpname,rgtno,caseno,ShouldPay,ShouldRealPay,RealPay,uwdate"
  +" from LLSpringGiveResult where caseno='"+fm.CaseNo.value+"'";
  turnPage.queryModal(strSQL, AccountGrid);
}

function RgtUw(){
  if(fm.RgtNo.value==null||fm.RgtNo.value==""){
    alert("没有团体申请信息！");
    return ;
  }
  var varSrc= "&RgtNo="+fm.RgtNo.value+"&GrpContNo="+fm.ContNo.value;
  var pathStr="./FrameMainSim.jsp?Interface=LLStringUwInput.jsp"+varSrc;
  var newWindow=OpenWindowNew(pathStr,"拨付审定","left");
}