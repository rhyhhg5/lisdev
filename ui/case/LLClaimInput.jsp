<%
//Name：LLClaimInput.jsp
//Function：理算页面 
//Date：2004-12-23 16:49:22
//Author：Wujs,Xx
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<head >
		<SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="./CaseCommon.js"></SCRIPT>
		<SCRIPT src="LLClaimInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LLClaimInputInit.jsp"%>
	</head>

	<body  onload="initForm();" >
		<!--立案分案信息部分（列表） -->
		<form action="./ClaimSave.jsp" method=post name=fm target="fraSubmit">

			<%@include file="LLRemark.jsp"%>
			<%@include file="CaseTitle.jsp"%>
			<div id= "divICaseInfoA" style= "display: 'none'">
				<table>
					<tr>
						<td>
							<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divICaseInfo);">
						</td>
						<td class= titleImg>
							申诉，错误信息
						</td>
					</tr>
				</table>
				<Div  id= "divICaseInfo" style= "display: ''">
					<table  class= common>
						<TR  class= common>
							<TD class= title8>原案件号</TD><TD class= input8><Input class=readonly readonly name=RRgtNo></td>
							<TD class= title8>原理算员</TD><TD class= input8><input class=readonly readonly name=OSperNo></TD>
							<TD class= title8>原审核员</TD><TD class= input8><input class=readonly readonly name=OSperName></TD>
						</TR>
					</table>
					<input class=cssButton type=hidden value="原受理信息查看" onclick="">
				</div>
			</Div>

	<table  class= common>
    <TR  class= common8>
      <TD  class= title8>客户号</TD><TD  class= input8><Input class= readonly name="CustomerNo" readonly></TD>
      <TD  class= title8>客户姓名</TD><TD  class= input8><Input class= readonly name="CustomerName" readonly></TD>
      <TD  class= title8>客户性别</TD><TD  class= input8><Input class= readonly name="Sex" readonly></TD>
    </TR>
	</table>
			<table>
				<tr>
					<td class=common><img src="../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divPolGrid);"></td>
					<td class= titleImg> 赔付保单明细</td>
				</tr>
			</table>
			<Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanClaimPolGrid" ></span>
						</td>
					</tr>
				</table>
			</Div>
			<div id=CalculateB>
				<input class=CssButton style='width: 60px;' type=button name="Calculate" value="计  算" onclick="CalPay()" id = "sub1">
			</div>
			<hr>
			<!-- 赔付明细信息部分（列表） -->

			<table>
				<tr>
					<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimResult);">
					</td>
					<td class= titleImg> 理算结果</td>
				</tr>
			</table>
			<Div  id= "divClaimResult" style= "display: ''">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanClaimResultGrid" ></span>
						</td>
					</tr>
				</table>
			</Div>
			<Div>
				<input class=CssButton style='width: 60px;' type=button name="ReCalculate" value="重  算" onclick="ReCalPay()" id = "sub2">
			</div>
			<hr>

			<table>
				<tr>
					<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divDetailGrid);">
					</td>
					<td class= titleImg> 保单责任明细</td>
				</tr>
			</table>
			<Div  id= "divDetailGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
						<td text-align: left colSpan=1>
							<span id="spanClaimDetailGrid" >
							</span>
						</td>
					</tr>
				</table>
			</Div>

			<table>
				<tr>
					<td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimTotal);">
					</td>
					<td class= titleImg> 计算赔付汇总</td>
				</tr>
			</table>
			<div  id= "divClaimTotal" style= "display: ''">
				<table  class= common>
					<TR  class= common8>
						<TD  class= title8>先期给付</TD><TD  class= input8><Input class= readonly name="PreGiveAmnt" readonly></TD>
						<TD  class= title8>自付金额</TD><TD  class= input8><Input class= readonly name="SelfGiveAmnt" readonly></TD>
						<TD  class= title8>退休补充给付</TD><TD  class= input8><Input class= readonly name="RetireAddFee" readonly></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>不合理费用</TD><TD  class= input8><Input class= readonly name="RefuseAmnt" readonly></TD>
						<TD  class= title8>理算金额</TD><TD  class= input8><Input class= readonly name="StandPay" readonly></TD>
						<TD  class= title8>实赔金额</TD><TD  class= input8><Input class= readonly name="RealPay" readonly></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>实赔天数</TD><TD  class= input8><Input class= readonly name="RealHospDate" readonly></TD>
						<TD  class= title8>赔付金额</TD>
						<TD  class= input8><Input class= readonly name="AllGiveAmnt" readonly></TD>
						<TD  class= title8>赔付结论</TD>
						<TD  class= input8><Input class= readonly name="AllGiveType" readonly></TD>
					</TR>
					<TR  class= common8>
						<TD  class= title8>给付未领取保单红利</TD><TD  class= input8><Input class= readonly name="BDHL" readonly></TD>
					</TR>
				</table>
				<table class= common>
    		<tr class= common8>
    		  <TD  class= title>
    			</TD>
    			<TD  class= input align="center">
    			  <INPUT TYPE="checkBox" NAME="EasyCase" value="01"> 简易案件
    		  </TD> <!-- none 隐藏  block 显示 -->
    		  <td class="" align="left"  ><b name="styleRed" id="styleRed" style="display:none" ><font color="red" >该被保险人已享受补充医疗保险费率折扣，理赔时请根据《个人税优健康保险理赔顺序特别约定告知书》中约定的理赔顺序进行赔付</b>
    		  </td>
    		  <td class="" align="left"  ><b NAME="styleRed2" id="styleRed2" style="display:none"><font color="red" >该被保险人已有补充医疗保险费率投保，理赔时请根据条款约定的理赔顺序进行赔付</b>
    		  </td>
    		 <!--  <TD  class= "">
    		  </TD>
    		  <TD  class= "">
    		  </TD> -->
    	  </tr>
      </table>
			</div>
		<p>
	<div id=divconfirm align='right'>
		<input class=CssButton style='width: 60px;' type=button value="确  认" name="SubCalculate" onclick="submitForm();" id = "sub3">
		<input class=CssButton style='width: 100px;' type=button value="案件备注信息" onclick="openCaseRemark()" id = "sub9">		
		<input class=CssButton type=button value="万能账户查询" name="AccValue" onclick="searchAccValue();" id = "sub4">
		<input class=CssButton type=button value="现金价值查询" name="CashValue" onclick="searchCashValue();" id = "sub10">
		<input class=CssButton style='width: 60px;' type=button value="拒赔原因" name="Decline" onclick="ClaimDecline();" id = "sub5">	
		<input class=CssButton style='width: 60px;' type=button value="保费豁免" name="Decline" onclick="Exemption();" id = "sub8">	
		<input class=cssButton style='width:80px;' type=button value="下发问题件" name='LowerQuestion' onclick="OnLower()" id = "sub10" >
		<INPUT class=cssButton style='width: 60px;' type=button value="上一步" onclick="preDeal();" id = "sub6">
		<INPUT class=cssButton style='width: 60px;' type=button value="下一步" onclick="nextDeal();" id = "sub7">
		<INPUT class=cssButton style='width: 60px;' type=button value="返  回" onclick="backDeal();" >
	</div>

	<Input type=hidden name="Opt">
	<Input type=hidden name="LoadFlag" id="LoadFlag" value="2">
	<input name ="LoadC" type="hidden">
	<input name ="LoadD" type="hidden">
	<input type=hidden name="ShowCaseRemarkFlag">
	<input type=hidden name="NoticeNo">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
