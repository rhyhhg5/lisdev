var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var tSaveType="";
var arr_fm;

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
	}
}

function easyQuery()
{
	var comCode = fm.ComCode.value;
	if(fm.RgtDateS.value == "" || fm.RgtDateE.value == "") 
	{ alert("请输入受理起止日期"); return false;}
	//初始化界面
	fm.ClaimNum.value = "";
	fm.ClaimAmt.value = "";
	initAccountGrid();
	divAccDetail.style.display="none";
	HospitalFee.style.display="none";
	DiagFee.style.display="none"; 
	divsecurity.style.display="none";
  
	//查询语句
	var sqlpart="";
	var sqlAcc="";
	if (fm.CaseState.value=="0")
		sqlpart = " and a.endcasedate is null";
	if (fm.CaseState.value=="1")
		sqlpart = " and a.endcasedate is not null";
	//咨询类
	var strSQL ="";
	  switch (fm.AccType.value)
	  {
	  	case "1": sqlAcc=" and d.Feetype = '1' and d.Feeatti <> '2' ";break;
	  	case "2": sqlAcc=" and d.Feetype = '2' and d.Feeatti = '0' ";break;	  	
	  	case "3": sqlAcc=" and d.Feeatti = '2' ";break;	
	  	case "4": sqlAcc=" and d.Feetype = '2' and d.Feeatti = '1' ";break;	
	  	default : sqlAcc="";
	  }
	  
		strSQL ="select distinct case when a.rgtno=a.caseno then '' else a.rgtno end, "
		+ " a.CaseNo,a.CustomerName,a.rgtstate,a.CustomerNo,a.rgtdate,a.endcasedate,c.codename, '', e.idno, "
		+ " (select k.name from ldcom k where a.Mngcom = k.comcode ),(select h.username from lduser h where h.usercode = a.handler),a.handler "
		+ " from LLCase a, LLregister b, ldcode c ,llfeemain d, ldperson e where 1=1 and a.rgtno=b.rgtno and d.caseno=a.caseno and c.code=a.rgtstate and c.codetype='llrgtstate' "
		+" and a.Customerno = e.customerno "
		+ getWherePart("a.rgtno","RgtNo") 
		+ getWherePart("a.CaseNo","CaseNo") 
		+ getWherePart("a.IdNo","IDNo") 
		+ getWherePart("a.CustomerNo","CustomerNo")
		+ "and a.CustomerName like '%%" + fm.CustomerName.value +"%%' "
		+ getWherePart("e.idno","IDNo")
		+" and a.rgtdate <= '"+fm.all('RgtDateE').value+"'"
		+" and a.rgtdate >= '"+fm.all('RgtDateS').value+"'"	
		+" and a.mngcom like '"+comCode+"%%'"
		+sqlAcc+ sqlpart+" order by a.rgtdate DESC, a.CaseNo ";
		turnPage.pageLineNum = 5;
		turnPage.queryModal(strSQL, CheckGrid);
}

function DealFeeInput()
{
	clear_fm();
	divAccDetail.style.display="none";
	HospitalFee.style.display="none";
	DiagFee.style.display="none"; 
	divsecurity.style.display="none";
		
		
		CalNumAmt();
		var selno ="";
		selno = CheckGrid.getSelNo()-1;		
		var CaseNo = CheckGrid.getRowColData(selno, 2);
		
		// 填写诊断
		//得到帐单数,使诊断行数与帐单数一致
		var mainfeeno = easyExecSql("select mainfeeno from llfeemain where  caseno = '"+CaseNo+"' order by feedate desc");
		var diag = new Array();//诊断信息数组

		for(var m=0; m<mainfeeno.length; m++)
		{//拼写诊断信息
			var tempDiag = "";
			var diagnose = easyExecSql("select diagnose from llcasecure a,  (select distinct caserelano x from llfeemain where  caseno = '"+CaseNo+"' and mainfeeno = '"+mainfeeno[m]+"') b where  a.caserelano = b.x");

			if(!diagnose) break;
			for(var n=0; n<diagnose.length; n++)
			{
				tempDiag = tempDiag + diagnose[n] +",";
			}
			diag[m] = tempDiag;
		}
		
		var sql = " select case when a.FeeType='1' then '门诊' else '住院' end,a.feedate, "
				+ " (select sum(b.fee) from llcasereceipt b where b.mainfeeno = a.mainfeeno and b.feeitemcode not like '3%%'), '',"
		        + " (select c.accdesc from LLSubReport c, LLcaserela d where c.subrptno = d.subrptno and a.caseno = d.caseno and a.caserelano = d.caserelano), "
		        + " (select sum(f.realpay) from llclaimdetail f where a.caserelano = f.caserelano), a.mainfeeno, a.caserelano"
				+ " from   llfeemain a  " 
				+ " where  a.caseno = '"+CaseNo+"' order by a.feedate desc"
				;
		turnPage.pageLineNum = 50;
		turnPage.queryModal(sql, AccountGrid);
		
		//填入诊断信息
		for(var p=0; p<AccountGrid.mulLineCount; p++)
		{
				if(p>diag.length)
				break;
					
				AccountGrid.setRowColData(p, 4, diag[p]);
		}

}


function DealFeeInput_origin()
{	
	var LoadC="2";
	var CaseNo="";
	var selno = CheckGrid.getSelNo()-1;
	if (selno <0)
	{
		// alert("请选择要账单录入的个人受理");
	}
else
	{
		CaseNo = CheckGrid.getRowColData(selno, 2);
	}
	var varSrc = "&CaseNo=" + CaseNo;
	varSrc += "&LoadC="+LoadC;
	var newWindow =OpenWindowNew("./FrameMainZD.jsp?Interface=ICaseCureInput.jsp" + varSrc,"","left");

}
function getstr()
{
	str="1 and code like #"+fm.OrganCode.value+"%#";
}

function showDetail()
{
		initInpBox1();
		initInpBox2();

		fm.typeRadio[0].checked = true;
		var sel = CheckGrid.getSelNo()-1; 
		if(sel == "null" || sel == "-1")
				{alert("请先选择一条理赔信息"); return;}   
		var selno = AccountGrid.getSelNo()-1;     
		if(selno < 0){alert("请选择帐单");return;}     
		var AccountType = AccountGrid.getRowColData(selno, 1);
		var mainfeeno   = AccountGrid.getRowColData(selno, 7);
		selno = CheckGrid.getSelNo()-1;   
		var CaseNo = CheckGrid.getRowColData(selno, 2);
		var feeatti = easyExecSql("select feeatti from llfeemain where caseno = '"+CaseNo+"' and mainfeeno = '"+mainfeeno+"'"); 
		if(AccountType == "门诊" && feeatti != "2")
		{		
				divAccDetail.style.display="";
				HospitalFee.style.display="none";
				DiagFee.style.display=""; 
				divsecurity.style.display="none";
//				AfterCal.style.display="";     
				Calculation();
		}
		if(AccountType == "住院" && feeatti != "2")            
		{                                 
				divAccDetail.style.display="";
				HospitalFee.style.display="";
				DiagFee.style.display="none"; 
				divsecurity.style.display="none"; 
//				AfterCal.style.display="";      
				Calculation();
		}    
		if(feeatti == "2" )
		{
				divsecurity.style.display=""; 
				divAccDetail.style.display="none";
				HospitalFee.style.display="none";
				DiagFee.style.display="none";  
				CalZD();
		}
}

function Calculation()
{	
		//AcccoutGrid中的值
		var selno       = AccountGrid.getSelNo()-1;          
		var AccountType = AccountGrid.getRowColData(selno, 1);
		var mainfeeno   = AccountGrid.getRowColData(selno, 7);
		//CheckGrid中的值
		selno = CheckGrid.getSelNo()-1;                       
		var CaseNo = CheckGrid.getRowColData(selno, 2);
		//从数据库中查出数据
		var sql = "select feeitemcode, fee from llcasereceipt where caseno = '"+CaseNo+"' and mainfeeno = '"+mainfeeno+"'";
		arr_fm = easyExecSql(sql);//alert(sql);alert(arr_fm);
		var sum = easyExecSql("select sum(fee) from llcasereceipt where caseno = '"+CaseNo+"' and mainfeeno = '"+mainfeeno+"' and feeitemcode not like '3%%' ");

		//填充页面
		if(arr_fm != null)
		{
				for (var i = 0; i < arr_fm.length;i++ )
				{
						if(arr_fm[i][0] != null)
						{
							if( i > 0 && arr_fm[i][0] == arr_fm[i-1][0])
							{
								fm.all(arr_fm[i][0]).value = fm.all(arr_fm[i][0]).value*1 +	arr_fm[i][1]*1;//alert(fm.all(arr_fm[i][0]).value);
							}					
							
							else
							{
								fm.all(arr_fm[i][0]).value = arr_fm[i][1];//alert(fm.all(arr_fm[i][0]).value);
							}
							var a = fm.all(arr_fm[i][0]).value*100/sum ;
							fm.all(arr_fm[i][0]+"r").value = a.toString().substr(0,5);
						}//if
				}//for
		}//if
		fm.HospitalSum.value = sum;
		fm.DiagSum.value = sum;
}

//多张账单费用计算
function CalMulAcc()
{
		
		//错误校验
		var selno = CheckGrid.getSelNo()-1; 
		if(selno == "null" || selno == "-1")
				{alert("请先选择一条理赔信息"); return;} 
		var CaseNo = CheckGrid.getRowColData(selno, 2);
		var count_Acc = easyExecSql("select count(*) from llfeemain where caseno = '"+CaseNo+"' ");
		if(count_Acc == "1")
		{alert("此次理赔只有一张账单");return;}
		var mainfeeno   = AccountGrid.getRowColData(selno, 7);
		var AccountType = AccountGrid.getRowColData(selno, 1);
		var feeatti = easyExecSql("select feeatti from llfeemain where caseno = '"+CaseNo+"' and mainfeeno = '"+mainfeeno+"'"); 

		if(AccountType == "门诊")
		{	
				divAccDetail.style.display="";
				HospitalFee.style.display="none";
				DiagFee.style.display=""; 
				divsecurity.style.display="none";
		}
		if(AccountType == "住院")            
		{                  
				divAccDetail.style.display="";
				HospitalFee.style.display="";
				DiagFee.style.display="none"; 
				divsecurity.style.display="none"; 
		}  
		
		
//		HospitalFee.style.display='';
		if(arr_fm != null)
		{
				for (var i = 0; i < arr_fm.length;i++ )
				{
							fm.all(arr_fm[i][0]).value = "";	
							fm.all(arr_fm[i][0]+"r").value = "";	
				}
		}
	
		var sum = "";
		for(var j = 0; j < count_Acc; j++ )
		{
			sum = sum*1 + easyExecSql("select sum(fee) from llcasereceipt where caseno = '"+CaseNo+"' and mainfeeno = '"+AccountGrid.getRowColData(j, 7)+"' and feeitemcode not like '3%%' ")*1;
		}
		for(var j = 0; j < count_Acc; j++ )
		{//alert("第"+j+"行： "+AccountGrid.getRowColData(j, 7));
			var sql = "select feeitemcode, fee from llcasereceipt where caseno = '"+CaseNo+"' and mainfeeno = '"+AccountGrid.getRowColData(j, 7)+"'";
			arr_fm = easyExecSql(sql);//alert(arr_fm);
		
			if(arr_fm != null)
			{//alert(arr_fm);
					for (var i = 0; i < arr_fm.length;i++ )
					{
							if(arr_fm[i][0] != null)
							{
								fm.all(arr_fm[i][0]).value = (fm.all(arr_fm[i][0]).value*1 + arr_fm[i][1]*1).toString().substr(0,10);
								//alert(arr_fm[i][0]);alert(fm.all(arr_fm[i][0]).value);
								var a = fm.all(arr_fm[i][0]).value*100/sum ;
								fm.all(arr_fm[i][0]+"r").value = a.toString().substr(0,5);
							}//if
					}//for
			}//if
		}//for
		fm.HospitalSum.value = sum;
		fm.DiagSum.value = sum;
}

//清除上次的fm
function clear_fm()
{
		if(arr_fm != null)
		{
				for (var i = 0; i < arr_fm.length;i++ )
				{//alert(arr_fm[i][0]);
							fm.all(arr_fm[i][0]).value = "";	
							fm.all(arr_fm[i][0]+"r").value = "";	
				}
		}
}

//计算某一客户的理赔次数，与理赔金额
function CalNumAmt()
{
	var selno = CheckGrid.getSelNo()-1;   
	var CustomerNo = CheckGrid.getRowColData(selno, 5);
	var sql = " select count(*) from llcase where customerno = '"+CustomerNo+"'";
	fm.ClaimNum.value = easyExecSql(sql);
	sql = " select sum(b.realpay) from llcase a, llclaim b where  a.customerno = '"+CustomerNo+"' and a.caseno = b.caseno "
	fm.ClaimAmt.value = easyExecSql(sql);
}
                                 
function CalZD()
{                                 
	var selno       = AccountGrid.getSelNo()-1;           
	var AccountType = AccountGrid.getRowColData(selno, 1);
	var mainfeeno   = AccountGrid.getRowColData(selno, 7);
	   
	strSql="select * from llsecurityreceipt where MainFeeNo='"
	+mainfeeno+"'";                           
	var drr=easyExecSql(strSql);                              
	                                                          
	if(drr)                                                   
	{                                                         
		fm.ApplyAmnt.value=drr[0][4];                           
		fm.FeeInSecu.value=drr[0][5];                           
		fm.FeeOutSecu.value=drr[0][6];                          
		fm.TotalSupDoorFee.value=drr[0][7];                     
		fm.TotalApplyAmnt.value=drr[0][4];                      
		fm.PayReceipt.value=drr[0][8];                          
		fm.RefuseReceipt.value=drr[0][9];                       
		fm.SupDoorFee.value=drr[0][10];                         
		fm.YearSupDoorFee.value=drr[0][11];                     
		fm.PlanFee.value=drr[0][12];                            
		fm.YearPlayFee.value=drr[0][13];                        
		fm.SupInHosFee.value=drr[0][14];                        
		fm.YearSupInHosFee.value=drr[0][15];                    
		fm.SecurityFee.value=drr[0][16];                        
		fm.SelfPay1.value=drr[0][19];                           
		fm.SelfPay2.value=drr[0][20];                           
		fm.SelfAmnt.value=drr[0][22];                           
		fm.SecuRefuseFee.value=drr[0][21];                      
	}                                                         
}
//原始页面查询
function ShowInfoPage()
{
	var selno = CheckGrid.getSelNo();//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = CheckGrid.getRowColData(selno-1,2);
	OpenWindowNew("./ICaseCureInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");
}

//保单查询
function ContInfoPage()
{
		var selno = CheckGrid.getSelNo();//alert(CheckGrid.getSelNo());
		if(selno == "" || selno == null)
		{alert("请先选择一条信息");return;}
		var CaseNo = CheckGrid.getRowColData(selno-1,2);
		var ContNo = easyExecSql("select distinct grpcontno from llclaimpolicy where caseno = '"+CaseNo+"'");
		if(ContNo=='00000000000000000000')
		{
			ContNo = easyExecSql("select distinct contno from llclaimpolicy where caseno = '"+CaseNo+"'");
			window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
			}else
				{
					window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
	      }
}