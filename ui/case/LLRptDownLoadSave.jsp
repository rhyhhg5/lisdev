<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp" %>
<%
/*=============================================
Name：LLRptDownLoadSave.jsp
Function： 打印报表下载
Date：2010-8-3
Author：ZhangXiaolei
=============================================*/
%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.lang.*"%>
<%@page import="java.net.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%
    
    //response.reset();
	String filePath = request.getParameter("filePath");//即将下载的文件的相对路径    
	String oldFilePath = request.getParameter("oldFilePath");//即将下载的文件的归档前相对路径    
	String fileName =request.getParameter("fileName");//下载文件时显示的文件保存名称
	
	//判断文件是否存在 不存在就用归档前的路径
	String tRealPath = application.getRealPath("/").replace('\\','/');
	File tFile = new File(tRealPath+filePath+fileName); 
	System.out.println("新路径="+filePath);
	System.out.println("原路径="+oldFilePath);
	System.out.println("文件名称="+fileName);
    if(!tFile.exists())
    {
    	filePath = oldFilePath;
    }
    	
	String urlto=request.getRequestURL().toString();
	String context=request.getContextPath();
	String url=request.getRequestURI();
	//int x=urlto.indexOf(url);
	//System.out.println(x);
	String str=urlto.substring(0,urlto.indexOf(url));
	System.out.println("str="+str);
	//System.out.println("###request.getPathInfo()" + request.getPathInfo());
	
	//###request.getRequestURL()http://localhost:8080/de/case/LLRptDownLoadSave.jsp
//###request.getContextPath()/de
//###request.getRequestURI()/de/case/LLRptDownLoadSave.jsp
//###request.getPathInfo()null
    if( fileName.endsWith(".xls") ) 
    {
    	response.setContentType("application/x-download");//设置为下载application/x-download

    	//通过设置头标题来显示文件传送到前端浏览器的文件信息
   		 response.addHeader("Content-Disposition","attachment;filename=" + URLEncoder.encode(fileName,"UTF-8"));
    	//addHeader(String name, String value);
    	System.out.println("#############" + filePath + fileName); 
    	try
    	{
       	 	RequestDispatcher dis = application.getRequestDispatcher(filePath + fileName);
     		//一个RequestDispatcher对象可以用来转发请求到资源或包含在响应中的资源。给定的路径。
        	if(dis!= null)
        	{
            	dis.forward(request,response);
        	}
        	response.flushBuffer();
    	}
    	catch(Exception e)
    	{
        	e.printStackTrace();
    	}
    	finally
    	{
    	}
    }
    else if ( fileName.endsWith(".vts") ) 
    {
    	try
    	{
			System.out.println("开始打开报表!");
			ExeSQL tExeSQL = new ExeSQL();
			//获取临时文件名
			//String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
			//+getwherepart("Megcom","Megcom")
			//+getwherepart("UserCode","UserCode");
			//String strFilePath = tExeSQL.getOneValue(strSql);
			//String strFilePath = filePath;
			//String strVFFileName = strFilePath + fileName ;
			//获取存放临时文件的路径
			//System.out.println("12");
			//String strRealPath = application.getRealPath("/").replace('\\','/');
			//System.out.println("123");
			//String strVFPathName = strRealPath + "//" + strVFFileName;
			//System.out.println("1234");
			System.out.println("==> Write VTS file to disk ");
			//System.out.println("===strVFFileName : " + strVFFileName);
			//本来打算采用get方式来传递文件路径
			//JRptList t_Rpt = new JRptList();
			
			//t_Rpt.Prt_RptList(pageContext,fileName);
			//String tOutFileName = t_Rpt.mOutWebReportURL;
            //String tOutFileName=str+context+filePath+fileName ;
            //String strRealPath = application.getRealPath("/").replace('\\','/');
            //System.out.println("strRealPath : "+ strRealPath);
            //String strVFFileName = filePath+fileName ;
            //String strVFFileName1=strVFFileName.substring(1);
            //System.out.println("strVFFileName1 : "+ strVFFileName1);
            //String strVFPathName = strRealPath + strVFFileName1;
            //System.out.println("strVFPathName : "+ strVFPathName);
            //System.out.println("=======Finshed in JSP===========");
          
        String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
		String strFilePath = tExeSQL.getOneValue(strSql);
		String strVFFileName = filePath + fileName;
		//获取存放临时文件的路径
		String strRealPath = application.getRealPath("/").replace('\\','/');
		String strVFPathName = strRealPath +"//"+ strVFFileName;	  
		response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?Code=03&RealPath="+strVFPathName);
           // response.sendRedirect("../web/ShowF1Report.jsp?FileName="+tOutFileName+"&RealPath="+strVFPathName);
			
			//response.sendRedirect("../uw/GetF1PrintJ1.jsp?Code=03&RealPath="+strVFPathName);
	  } catch (Exception ex) 
	  {
	  }
	}
    else if ( fileName.endsWith(".zip") ) 
    {
    	String realpath = application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
    	String temp = realpath.substring(realpath.length() - 1, realpath
    			.length());
    	if (!temp.equals("/")) {
    		realpath = realpath + "/";
    	}
    	
    	filePath = realpath+filePath+fileName;
%>
<html>
<a href="../f1print/download.jsp?filename=<%=fileName%>&filenamepath=<%=filePath%>">点击下载</a>
</html>
<%
    	
	}
%>
