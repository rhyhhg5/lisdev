<html>
  <%
  /*******************************************************************************
  * Name     :ICaseCureInput.jsp
  * Function :立案－费用明细信息的初始化页面程序
  * Author   :Xx
  * Date     :2005-7-23
  */
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

  <head >
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../case/LLCheckSocialCont.js"></SCRIPT>
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="./CaseCommon.js"></SCRIPT>
    <SCRIPT src="ICaseCureInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="ICaseCureInit.jsp"%>
  </head>
  <body  onload="initForm();initElementtype();" >
    <form action="./ICaseCureSave.jsp" method=post name=fm target="fraSubmit">

      <!-- 基本帐单信息 -->
       <%@include file="LLRemark.jsp"%>
      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divICaseCure);">
          </td>
          <td class= titleImg>
            账单信息明细
          </td>
        </tr>
      </table>

      <Div  id= "divICaseCure" style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD  class= title8>理赔号</TD>
            <TD  class= input8> <input class="common4"    name=CaseNo onkeydown= "QueryOnKeyDown()"></TD>
            <TD  class= title8>处理人</TD>
            <TD  class= input8> <input class="readonly" readonly   name=cOperator></TD>
            <TD  class= title8>处理日期</TD>
            <TD  class= input8> <input class="readonly" readonly   name=MakeDate></TD>
            <TD  class= input8>管理机构</TD>
            <TD  class= input8><Input class="fcodeno" name=MngCom  ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,180);"><input class=fcodename name=ComName></TD>
          </tr>
          <TR  class= common>
            <TD  class= title8>客户姓名</TD>
            <TD  class= input8> <input class="common4" readonly   name=CustomerName></TD>

            <TD  class= title8>客户号码</TD>
            <TD  class= input8> <input class="common4" readonly   name=CustomerNo></TD>
            <TD  class= title8>性别</TD>
            <TD  class= input8> <input class="code" name="CustomerSex" type=hidden> <input class="common4" readonly   name=CustomerSexName></TD>
            <TD  class= title8>年龄</TD>
            <TD  class= input8 > <input class=common4  name=Age></TD>
          </TR>
          <TR class= common id='titleS1' style="display:'none'">
            <TD  class= title8>身份证号</TD>
            <TD  class= input8 > <input class=common4 readonly name=IDNo></TD>
            <TD  class= title8>参保人员类别</TD>
            <TD  class= input8 ><input class=codeno name=InsuredStat CodeData="0|3^1|在职^2|退休^3|老职工" onclick="return showCodeListEx('insurestat',[this,InsuredStatName],[0,1]);" onkeyup="return showCodeListKeyEx('insurestat',[this,InsuredStatName],[0,1]);"><input class=codename  name=InsuredStatName></TD>
            <TD  class= title8 >客户社保号</TD>
            <TD  class= input8 > <input class=common4 name=SecurityNo></TD>
            <TD  class= title8>单位社保登记号</TD>
            <TD  class= input8 > <input class=common4 name=CompSecuNo></TD>
          </TR>
          <TR  class= common>
            <TD  class= title8>医院名称</TD>
            <TD  class= input8> <input class=common4  name=HospitalName elementtype=nacessary verify="医院名称|notnull"></TD>
            <TD  class= title8>医院代码</TD>
            <TD  class= input8> <input class=code  name=HospitalCode onclick="return showCodeList('llhospiquery',[this,HospitalName,FixFlag,HosGrade,UrbanFlag],[0,1,2,3,4],null,fm.HospitalName.value,'Hospitname',1,240);" onkeyup="return showCodeListKeyEx('llhospiquery',[this,HospitalName,FixFlag,HosGrade,UrbanFlag],[0,1,2,3,4],null,fm.HospitalName.value,'Hospitname',1,240);" elementtype=nacessary verify="医院代码|notnull"></TD>
            <TD  class= title8>医院属性</TD>
            <TD  class= input8> <input class=common4  name=FixFlag readonly></TD>
            <TD  class= title8>医院级别</TD>
            <TD  class= input8 > <input name=HosGrade type=hidden><input  readonly class=common4  name=HosGradeName></TD>
          </TR>
          <TR class= common id='titleS2' style="display:'none'">
          </TR>
          <TR  class= common>
            <TD  class= title8>账单属性</TD>
            <TD  class= input8><input class="fcodeno" name="FeeAtti" onclick="return showCodeList('llfeeatti',[this,FeeAttiName],[0,1]);" onkeyup="return showCodeListKey('llfeeatti',[this,FeeAttiName],[0,1]);"><input class=fcodename  name=FeeAttiName  elementtype=nacessary verify="账单属性|notnull"></TD>
            <TD  class= title8>账单种类</TD>
            <TD  class= input8><input class="fcodeno" name="FeeType"  CodeData="0|3^1|门诊^2|住院^3|门诊特殊病" onclick="return showCodeListEx('FeeType',[this,FeeTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeType',[this,FeeTypeName],[0,1]);"><input class=fcodename name=FeeTypeName  elementtype=nacessary verify="账单种类|notnull"></TD>
            <TD  class= title8>帐单类型</TD>
            <TD  class= input8><input class="fcodeno" name="FeeAffixType" CodeData="0|2^0|原件^1|复印件" onclick="return showCodeListEx('FeeAffixType',[this,FeeAffixTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeAffixType',[this,FeeAffixTypeName],[0,1]);"><input class=fcodename  name=FeeAffixTypeName></TD>
            <TD  class= title8>收据类型</TD>
            <TD  class= input8><input class="fcodeno" name="ReceiptType" onclick="return showCodeList('receipttype',[this,ReceiptTypeName],[0,1]);" onkeyup="return showCodeListKey('receipttype',[this,ReceiptTypeName],[0,1]);"><input class=fcodename  name=ReceiptTypeName></TD>
            <!--<TD  class= title8>城区属性</TD>
            <TD  class= input8 > <input name=UrbanFlag type=hidden><input  readonly class=common4  name=UrbanFlagName></TD>
          --></TR>
          <TR  class= common id='titleC1'>
            <TD  class= title8>账单号码</TD>
            <TD  class= input8> <input class=common4  name=ReceiptNo elementtype=nacessary></TD>
            <TD  class= title8 >住院号</TD>
            <TD  class= input8 > <input class=common4 name=inpatientNo></TD>
            <TD  class= title8 colspan=2>账单合计金额 <input class=readonly readonly  name=SumFee ></TD>
            <TD  class= title8>城区属性</TD>
            <TD  class= input8 > <input name=UrbanFlag type=hidden><input  readonly class=common4  name=UrbanFlagName style="width:120px"></TD><!--
            <TD  class= title8></TD>
            <TD class= input8 > <input class=readonly readonly style="width:120px"></TD>
          --></TR>
          <TR  class= common id='titleC2'>
            <TD  class= title8>结算日期</TD>
            <TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=FeeDate onblur="fillDate()" elementtype=nacessary verify="结算日期|date"></TD>
             <TD class= title8 id='titleInpatient' style="display:''" verify="入院日期|date">入院日期</TD>
            <TD  class= title8 id='titleStart' style="display:'none'">治疗开始</TD>
            <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=HospStartDate onblur="caldate()"  verify="日期|date"></TD>
            <TD class= title8 id='titleOutpatient' style="display:''" verify="出院日期|date">出院日期</TD>
            <TD  class= title8 id='titleEnd' style="display:'none'">治疗结束</TD>
            <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=HospEndDate onblur="caldate()" verify="日期|date"></TD>
            <TD  class= title8 >治疗天数</TD>
            <TD  class= input8> <input class=common4  name=RealHospDate onfocus="caldate()" readonly ></TD>
          </TR>
           <TR  class= common id='titleC3'>
            <TD  class= title8>分割单出具单位</TD>
            <TD  class= input8><input class="fcodeno" name="IssueUnit" onclick="return showCodeList('issueunit',[this,IssueUnitName],[0,1]);" onkeyup="return showCodeListKey('issueunit',[this,IssueUnitName],[0,1]);"><input class=fcodename  name=IssueUnitName></TD>
			<TD  class=titele8>医保类型</TD>
			<TD  class=input8><input class="codeno"  name="MedicareType" onclick="return showCodeList('medicaretype',[this,MedicareTypeName],[0,1]);" onkeyup="return showCodeListKey('medicaretype',[this,MedicareTypeName],[0,1]);"verify="医保类型|notnull" readonly="readonly"><input class=fcodename name=MedicareTypeName elementtype=nacessary></TD>
          </TR>
        </table>
      </div>
      <!-- 北京手工报销帐单信息 -->
      <DIV id=divsecurity style='display:none'>
        <table>  
          <tr>
            <td class= titleImg>
              社保账单申报内容
            </td>
          </tr>
        </table>
        <table class= common cellspacing=0 cellpadding=0>
          <tr class=common height="20">
            <td id=titleMZ class= gridtitle1 rowspan="2">门诊</td>
            <td id=titleZY class= gridtitle1 rowspan="2" style="display:'none'">住院</td>
            <td class= gridtitle0>申报金额</td>
            <td id=titleFeeStart class= gridtitle0>发生费用时间</td>
            <td id=titleApply class= gridtitle0>申报时间</td>
            <td id=titleConDays class= gridtitle0>连续天数</td>
            <td id=titleInHos class= gridtitle0 style="display:'none'">入院时间</td>
            <td id=titleOutHos class= gridtitle0 style="display:'none'">出院时间</td>
            <td id=titleDays class= gridtitle0 style="display:'none'">住院天数</td>
            <td class= gridtitle0>医保支付金额</td>
            <td class= gridtitle0>医保拒付金额</td>
          </tr>
          <tr class=common >
            <td class= grid0 ><input class=common9 name=ApplyAmnt onblur="checkNumber(ApplyAmnt);"></td>
            <td class= grid0 ><input class=common9 name=FeeStart verify="发生费用时间|date"></td>
            <td class= grid0 ><input class=common9 name=FeeEnd verify="申报时间|date"></td>
            <td class= grid0 ><input class=common9 name=FeeDays onblur="checkNumber(FeeDays);"></td>
            <td class= grid0 ><input class=common9 name=FeeInSecu onblur="checkNumber(FeeInSecu);"></td>
            <td class= grid0 ><input class=common9 name=FeeOutSecu onblur="checkNumber(FeeOutSecu);"></td>
          </tr>
          <tr class=common >
            <TD  class= gridtitle1 colspan=3>本次报销前，门诊大额年度累计支付金额</TD>
            <td class= gridtitle0 colspan="2">申报金额合计</td>
            <td class= gridtitle0>报销单据张数</td>
            <td class= gridtitle0>拒付单据张数</td>
          </tr>
          <TR class= common>
            <TD  class= gridtitle colspan=3 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=TotalSupDoorFee onblur="checkNumber(TotalSupDoorFee);"></TD>
            <td class= grid0 colspan=2 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=TotalApplyAmnt onblur="checkNumber(TotalApplyAmnt);"></td>
            <td class= grid0 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=PayReceipt onblur="checkNumber(PayReceipt);"></td>
            <td class= grid0 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=RefuseReceipt onblur="checkNumber(RefuseReceipt);"></td>
          </TR>
        </table>
        <table>
          <tr>
            <td class= titleImg>
             医保支付金额
            </td>
          </tr>
        </table>
        <table class= common cellspacing=0 cellpadding=0>
          <tr class=common >
            <td class= gridtitle1 colspan="2">门诊大额医疗费用</td>
            <td class= gridtitle0 colspan="2">统筹基金医疗费用</td>
            <td class= gridtitle0 colspan="2">住院大额医疗费用</td>
            <td class= gridtitle0 rowspan="2">医保支付合计</td>
          </tr>
          <tr class=common >
            <td class= gridtitle1 >本次支付金额</td>
            <td class= gridtitle0>年度内累计支付金额</td>
            <td class= gridtitle0>本次支付金额</td>
            <td class= gridtitle0>年度内累计支付金额</td>
            <td class= gridtitle0>本次支付金额</td>
            <td class= gridtitle0>年度内累计支付金额</td>
          </tr>
          <tr class=common >
            <td class= gridtitle style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SupDoorFee onblur="checkNumber(SupDoorFee);"></td>
            <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=YearSupDoorFee onblur="checkNumber(YearSupDoorFee);"></td>
            <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=PlanFee onblur="checkNumber(PlanFee);"></td>
            <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=YearPlayFee onblur="checkNumber(YearPlayFee);"></td>
            <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SupInHosFee onblur="checkNumber(SupInHosFee);"></td>
            <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=YearSupInHosFee onblur="checkNumber(YearSupInHosFee);"></td>
            <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SecurityFee onblur="checkNumber(SecurityFee);"></td>
          </tr>
        </table>
        <table>
          <tr>
            <td class= titleImg>
              医保不予支付金额
            </td>
          </tr>
        </table>
        <table class= common cellspacing=0 cellpadding=0>
          <tr class=common >
            <td class= gridtitle1 colspan="2">自付一小计</td>
            <td class= gridtitle0 colspan="2">自付二小计</td>
            <td class= gridtitle0 colspan="2">自费小计</td>
            <td class= gridtitle0 >合计</td>
          </tr>
          <tr class=common >
            <td class= gridtitle colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SelfPay1 onblur="checkNumber(SelfPay1);"></td>
            <td class= grid0 colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SelfPay2 onblur="checkNumber(SelfPay2);"></td>
            <td class= grid0 colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SelfAmnt onblur="checkNumber(SelfAmnt);"></td>
            <td class= grid0  style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=SecuRefuseFee onblur="checkNumber(SecuRefuseFee);"></td>
          </tr>
        </table>
        <Div  id= "divRetireAddFee" style= "display: 'none'">
       <table>
          <tr>
            <td class= titleImg>
              退休人员补充医疗保险支付费用
            </td>
          </tr>
        </table>
         <table class= common cellspacing=0 cellpadding=0>
          <tr class=common >
            <td class= gridtitle1 colspan="2">本次支付金额</td>
            <td class= gridtitle0 colspan="2">年度累计支付金额</td>
       
          </tr>
          <tr class=common >
            <td class= gridtitle colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=RetireAddFee onblur="checkNumber(RetireAddFee);"></td>
            <td class= grid0 colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=YearRetireAddFee onblur="checkNumber(YearRetireAddFee);"></td>

          </tr>
        </table>
         </div>
        <table>
          <tr>
            <td class= titleImg>
              医保不予支付项目和金额明细
            </td>
          </tr>
        </table>
        <table class= common cellspacing=0 cellpadding=0>
          <tr class=common >
            <td class= gridtitle1 colspan="2">费用项目</td>
            <td class= gridtitle0 colspan="2">总金额</td>
            <td class= gridtitle0 colspan="3">医保不支付金额</td>
          </tr>
          <tr class=common >
            <td class= gridtitle1 colspan="2">药费</td>
            <td class= grid0 colspan="2"><input class=common9 name=DrugFee onblur="checkNumber(DrugFee);"></td>
            <td class= grid0 colspan="3"><input class=common9 name=RefuseDrugFee onblur="checkNumber(RefuseDrugFee);"></td>
          </tr>
          <tr class=common >
            <td class= gridtitle1 colspan="2">检查、治疗、化验</td>
            <td class= grid0 colspan="2"><input class=common9 name=ExamFee onblur="checkNumber(ExamFee);"></td>
            <td class= grid0 colspan="3"><input class=common9 name=RefuseExamFee onblur="checkNumber(RefuseExamFee);"></td>
          </tr>
          <tr class=common >
            <td class= gridtitle1 colspan="2">材料</td>
            <td class= grid0 colspan="2"><input class=common9 name=MaterialFee onblur="checkNumber(ExamFee);"></td>
            <td class= grid0 colspan="3"><input class=common9 name=RefuseMaterialFee onblur="checkNumber(RefuseMaterialFee);"></td>
          </tr>
          <tr class=common >
            <td class= gridtitle1 colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'>其他费用</td>
            <td class= grid0 colspan="2" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=OtherFee onblur="checkNumber(OtherFee);"></td>
            <td class= grid0 colspan="3" style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class=common9 name=RefuseOtherFee onblur="checkNumber(RefuseOtherFee);"></td>
          </tr>
        </table>
      </div>
      <br>
      <!-- 特需医疗帐单信息 -->
      <div id=SecuAddiZD style= "display: 'none'">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecuAddiReceipt);">
            </td>
            <td class= titleImg>
              社保补充费用明细
            </td>
          </tr>
        </table>
      <Div  id= "divSecuAddiReceipt" style= "display: 'none'">
        <table  class= common>
          <tr  class= common>
            <td class= title8 text-align: center colSpan=1>
              <span id="spanSecuAddiGrid"  align=center>
              </span>
            </td>
          </tr>
        </table>
      </div>
      </div>
<!-- 云南社保帐单信息
        <table>
          <tr>
            <td class= titleImg>
              社保住院账单
            </td>
          </tr>
        </table>
      <div >
        <table class= common cellspacing=0 cellpadding=0>
          <tr class=common>
            <td class= gridtitle1 colSpan=2 width=25%>费用总额</td>
            <td class= grid0 colSpan=2 width=25%><input class="common9"></td>
            <td class= gridtitle1 width=12.5%>　</td>
            <td class= gridtitle0 width=12.5%>统筹支付</td>
            <td class= gridtitle0 width=12.5%>个人自付(自付一)</td>
            <td class= gridtitle0 width=12.5%>合   计</td>
          </tr>
          <tr class=common>
            <td class= gridtitle1 colSpan=2 >个人自付总额</td>
            <td class= grid0 colSpan=2 ><input class="common9"></td>
            <td class= gridtitle1  colspan=2>实付起付线</td>
            <td class= grid0 colspan=2 ><input class="common9"></td>
          </tr>
          <tr class=common>
            <td class= gridtitle1 colspan=2 >账户支付</td>
            <td class= grid0 colspan=2 ><input class="common9"></td>
            <td class= gridtitle1 >基本医疗统筹共付</td>
            <td class= grid0 ><input class="common9"></td>
            <td class= grid0 ><input class="common9"></td>
            <td class= grid0 ><input class="common9"></td>
          </tr>
          <tr>
            <td class= gridtitle1 colspan=2 >公务员补助支付</td>
            <td class= grid0 colspan=2 ><input class="common9"></td>
            <td class= gridtitle1 >大病医疗统筹共付</td>
            <td class= grid0 ><input class="common9"></td>
            <td class= grid0 ><input class="common9"></td>
            <td class= grid0 ><input class="common9"></td>
          </tr>
          <tr class=common>
            <td class= gridtitle1 width=12.5%>全自费</td>
            <td class= grid0 width=12.5%><input class="common9"></td>
            <td class= gridtitle0 width=12.5%>先自付(自付二)</td>
            <td class= grid0 width=12.5%><input class="common9"></td>
            <td class= gridtitle1  colspan=2>超大病限额自付</td>
            <td class= grid0 colspan=2 ><input class="common9"></td>
          </tr>
          <tr class=common>
            <td class= gridtitle1 colspan=2 style='BORDER-BOTTOM: #799AE1 1pt solid;'>医保范围外金额</td>
            <td class= grid0 colspan=2 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class="common9"></td>
            <td class= gridtitle1 style='BORDER-BOTTOM: #799AE1 1pt solid;'>医保范围内金额</td>
            <td class= grid0 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class="common9"></td>
            <td class= grid0 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class="common9"></td>
            <td class= grid0 style='BORDER-BOTTOM: #799AE1 1pt solid;'><input class="common9"></td>
          </tr>
        </table>
      </div> -->
      <!-- 普通帐单信息 -->
      <div id=commonZD style="display:''">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseReceipt);">
            </td>
            <td class= titleImg>
              账单费用项目明细
            </td>
          </tr>
        </table>
        <Div  id= "divCaseReceipt" style= "display: ''">
          <table  class= common>
            <tr  class= common>
              <td class= title8 text-align: center colSpan=1>
                <span id="spanIFeeCaseCureGrid"  align=center>
                </span>
              </td>
            </tr>
          </table>
        </div>
      </div>
      
      <!-- 社保结算单信息 -->
      <div id= "divSecu" style="display:'none'">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecuItem);">
            </td>
            <td class= titleImg>
              费用项目明细
            </td>
          </tr>
        </table>
        <Div  id= "divSecuItem" style= "display: ''">
          <table class= common>
            <tr class=common>
              <td class= title8 text-align: center colSpan=1>
                <span id="spanFeeItemGrid"  align=center>
                </span>
              </td>
            </tr>
          </table>
        </div>
      </div>
        <Div  id= "divSecuItem2" style= "display: 'none'">
          <table class=common>
            <tr  class= common>
              <td class= title8 text-align: center colSpan=1>
                <span id="spanSecuItemGrid"  align=center>
                </span>
              </td>
            </tr>
          </table>
        </div>
      <!-- 青岛社保帐单信息 -->

      <div id=QDZD style="display:'none'">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQDZD);">
            </td>
            <td class= titleImg>
              账单费用项目明细
            </td>
          </tr>
        </table>
        <Div  id= "divQDZD" style= "display: ''">
          <table  class= common>
            <tr  class= common>
              <td class= title8 text-align: center colSpan=1>
                <span id="spanQDSecuGrid"  align=center>
                </span>
              </td>
            </tr>
          </table>
        </div>
      </div>
      
      <div id=secuspan style="display:'none'">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSecuSpan);">
            </td>
            <td class= titleImg>
              社保账单分段赔付明细
            </td>
          </tr>
        </table>
        <Div  id= "divSecuSpan" style= "display: ''">
          <table  class= common>
            <tr  class= common>
              <td class= title8 text-align: center colSpan=1>
                <span id="spanSecuSpanGrid"  align=center>
                </span>
              </td>
            </tr>
          </table>
        </div>
      </div>
      <hr>
      <div align='left'>
        <INPUT VALUE="查看前一张账单" class=cssButton style='width:100px;' Type=button onclick="zdPrevious();">
        <INPUT VALUE="查看下一张账单" class=cssButton style='width:100px;' Type=button onclick="zdNext();">
      </div>

      <div id="divconfirm" align='right'>
        <INPUT VALUE=" 确  认 " style='width:60px;' class=cssButton TYPE=button onclick="saveFee();" id = "sub3">
        <INPUT VALUE="案件备注信息" style='width:80px;' class=cssButton TYPE=button onclick="openCaseRemark()" id = "sub5">
        <INPUT VALUE="费用明细录入" style='width:80px;' class=cssButton TYPE=button onclick="openChargeDetails();" >
        <INPUT VALUE="扣除明细" style='width:60px;' class=cssButton TYPE=button onclick="openDrug();" >
        <INPUT VALUE="生成手工报销单" class=cssButton style='width:126px;' TYPE=button onclick="getSGBXD();" id = "sub4">
        <INPUT VALUE="处方明细录入" style='width:80px;' class=cssButton TYPE=button onclick="openPrescription();" >
      	<input class=cssButton style='width:80px;' type=button value="下发问题件" name='LowerQuestion' onclick="OnLower()" id = "sub7" >
      </div>

      <hr/>
      <div style="display:'none'">
        <TD  class= title8>重症监护天数</TD>
        <TD  class= input8> <input class=common  name=SeriousWard></TD>
      </div>
      <div align='right' id=Contral>
        <INPUT VALUE="账单重置" class=cssButton style='width:60px;' TYPE=button onclick="zdReset();" id = "sub2">
        <INPUT VALUE="录入完毕" class=cssButton style='width:60px;' TYPE=button onclick="zdComplete();" id = "sub1">
        <INPUT VALUE=" 下一步 " class=cssButton style='width:60px;' id=idNext name=idNext TYPE=button onclick="nextDeal();">
        <INPUT VALUE=" 返  回 " class=cssButton style='width:60px;' TYPE=button onclick="backDeal();">
      </div>
      <div id="titleCaseduty" style="display: ''">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseduty);">
            </td>
            <td class= titleImg>
              待理算责任
            </td>
          </tr>
        </table>
      </div>
      <Div  id= "divCaseduty" style= "display: ''">
        <table  class= common>
          <tr  class= common>
            <td class= title8 text-align: center colSpan=1>
              <span id="spanDutyRelaGrid"  align=center>
              </span>
            </td>
          </tr>
        </table>
      </div>

      <Input type="hidden"  name="RgtNo" >
      <Input type="hidden" name="cOperate" >
      <input type="hidden" name="FirstInHos">
      <Input type="hidden" name="MainFeeNo">
      <Input type="hidden" name="Case_RgtState">
      <input type="hidden" name="LoadC">
      <input type="hidden" name="LoadD">
      <input type="hidden" name= "FeeDutyFlag">
      <input type="hidden" name= "Handler">
      <input type="hidden" name= "Opt">
      <input type=hidden name="ShowCaseRemarkFlag">
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </form>

  </body>
</html>
