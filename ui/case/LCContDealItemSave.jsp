<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LDPersonSave.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%

  //接收信息，并作校验处理。
  //输入参数
  
  LPEdorMainSet mLPEdorMainSet   = new LPEdorMainSet();
  LPEdorItemSet mLPEdorItemSet =new LPEdorItemSet();
  
  LCInsuredSet tLCInsuredSet=new LCInsuredSet();
  LCPolSet tLCPolSet=new LCPolSet();
  LLContDealSchema tLLContDealSchema = new LLContDealSchema();
   
  TransferData tTransferData = new TransferData(); 
  PEdorAppItemUI tPEdorAppItemUI   = new PEdorAppItemUI();
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tOperator = tGI.Operator;
  
  LDCodeDB tLDCodeDB = new LDCodeDB();
        tLDCodeDB.setCodeType("llcontdeal");
        tLDCodeDB.setCode("user");
        if (!tLDCodeDB.getInfo()) {
        }
  tGI.Operator = tLDCodeDB.getCodeName();

  if(tGI==null||tGI.Operator==null||"".equals(tGI.Operator))
  { 
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    CErrors tError = null;
    String tBmCert = "";
    System.out.println("aaaa");
    //后面要执行的动作：添加，修改，删除
    String fmAction=request.getParameter("fmtransact");
    System.out.println("fmAction:"+fmAction); 
     try
    {
    System.out.println("1");
        if(fmAction.equals("INSERT||EDORITEM"))
        {
                String tContNo[] = request.getParameterValues("ContGrid1"); //保单号       
                String tGrpContNo[]= request.getParameterValues("ContGrid10");
                String tRadio[] = request.getParameterValues("InpContGridSel");	//单选框（或复选框）的数组
                for(int index=0;index<tRadio.length;index++)
                {
                    if(tRadio[index].equals("1"))
                    {
                    System.out.println(index);
                        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                        tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
                        tLPEdorItemSchema.setGrpContNo(tGrpContNo[index]); 
                        tLPEdorItemSchema.setDisplayType("1");
                        String tEdorType = request.getParameter("EdorType");
                        if ("HZ".equals(tEdorType)) {
                          tEdorType = "CT";
                        } 
                        tLPEdorItemSchema.setEdorType(tEdorType); 
                        tLPEdorItemSchema.setContNo(tContNo[index]); 
                        tLPEdorItemSchema.setInsuredNo("000000");
                        tLPEdorItemSchema.setPolNo("000000");
                        tLPEdorItemSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
                        tLPEdorItemSchema.setEdorAppDate(request.getParameter("Edorappdate"));
                        mLPEdorItemSet.add(tLPEdorItemSchema); 
                        
                        tLLContDealSchema.setEdorNo(request.getParameter("EdorAcceptNo"));
                        tLLContDealSchema.setEdorType(request.getParameter("EdorTypeValue"));
                    }           
                }   
       }       
   
        // 准备传输数据 VData
         VData tVData = new VData();
         
         tVData.add(mLPEdorItemSet);
         tVData.add(tLLContDealSchema);
         tVData.add(tTransferData);
         tVData.add(tGI);
         
         tGI.Operator = tOperator;
          
         //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        if ( tPEdorAppItemUI.submitData(tVData,fmAction))
        {
            if (fmAction.equals("INSERT||EDORITEM"))
	        {
	    	    System.out.println("11111------return");
	            	
	    	    tVData.clear();
	    	    tVData = tPEdorAppItemUI.getResult();
	    	    //
	    	    LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet(); 
	          tLPEdorItemSet=(LPEdorItemSet)tVData.getObjectByObjectName("LPEdorItemSet",0);
	        }
	        else if (fmAction.equals("DELETE||PEdorItem"))
	        {
	             %>
	            <SCRIPT language="javascript">
	            	if (mLoadFlag == "TASKFRAME")
					{
	                	parent.fraInterface.fraInterface.EdorItemGrid.delCheckTrueLine (); 
	                }
	                else
	                {
	                	parent.fraInterface.EdorItemGrid.delCheckTrueLine ();
	                }
	            </SCRIPT>
	            <%
	        }
	    }
	    else
	    {
  	    Content = "保存失败，原因是:" + tPEdorAppItemUI.mErrors.getFirstError();
        FlagStr = "Fail";
	    }
    }
    
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorAppItemUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  Content = PubFun.changForHTML(Content);
}
%>                                       
<html>
<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

