<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLMainAskSave.jsp
//程序功能：
//创建日期：2005-01-12 16:10:37
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LLMainAskSchema tLLMainAskSchema   = new LLMainAskSchema();
  LLConsultSchema tLLConsultSchema	 = new LLConsultSchema();
  LLAnswerInfoSchema tLLAnswerInfoSchema = new LLAnswerInfoSchema();
  LLSubReportSet tLLSubReportSet = new LLSubReportSet();
  LLCommendHospitalSet tLLCommendHospitalSet = new LLCommendHospitalSet();
  OLLMainAskUI tOLLMainAskUI   = new OLLMainAskUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
  String Path = application.getRealPath("config//Conversion.config");
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    //添加至咨询登记表中
    tLLMainAskSchema.setLogNo(request.getParameter("LogNo"));
    tLLMainAskSchema.setLogState(request.getParameter("LogState"));
    tLLMainAskSchema.setAskType(request.getParameter("AskType"));
    tLLMainAskSchema.setOtherNo(request.getParameter("OtherNo"));
    tLLMainAskSchema.setOtherNoType(request.getParameter("OtherNoType"));
    tLLMainAskSchema.setAskMode(request.getParameter("AskMode"));
    tLLMainAskSchema.setAnswerMode(request.getParameter("AnswerMode"));
    tLLMainAskSchema.setLogerNo(request.getParameter("LogerNo"));
    tLLMainAskSchema.setLogName(request.getParameter("LogName"));
    tLLMainAskSchema.setLogComp(request.getParameter("LogComp"));
    tLLMainAskSchema.setLogDate(request.getParameter("LogDate"));
    tLLMainAskSchema.setPhone(request.getParameter("Phone"));
    tLLMainAskSchema.setMobile(request.getParameter("Mobile"));
    tLLMainAskSchema.setPostCode(request.getParameter("PostCode"));
    tLLMainAskSchema.setAskAddress(request.getParameter("AskAddress"));
    tLLMainAskSchema.setEmail(request.getParameter("Email"));
    //添加到咨询表中	
    
    //后台添加tLLConsultSchema.setConsultNo(request.getParameter("ConsultNo"));
    //后台添加tLLConsultSchema.setLogNo(request.getParameter("LogNo"));
    tLLConsultSchema.setCustomerNo(request.getParameter("LogerNo"));
    tLLConsultSchema.setCustomerName(request.getParameter("LogName"));
    tLLConsultSchema.setCContent(StrTool.Conversion(request.getParameter("CContent"),Path));
    if("0".equals(request.getParameter("AskType"))){
    tLLConsultSchema.setReplyState("2");
    }else{
     tLLConsultSchema.setReplyState("0");
    }
    tLLConsultSchema.setCustStatus(StrTool.Conversion(request.getParameter("CustStatus"),Path));
    tLLConsultSchema.setAccCode(request.getParameter("AccType"));
    
    //回复内容
    tLLAnswerInfoSchema.setAnswer(StrTool.Conversion(request.getParameter("Answer"),Path));

    //推荐医院
    String tHosNum[] = request.getParameterValues("CommHospitalGridNo");     
    String tHospitalNo[] = request.getParameterValues("CommHospitalGrid1");  
    String tHospitalName[] = request.getParameterValues("CommHospitalGrid2");
    String tHospitalAttr[] = request.getParameterValues("CommHospitalGrid4"); 
    String tSessionroom[] = request.getParameterValues("CommHospitalGrid5");
    String tBed[] = request.getParameterValues("CommHospitalGrid6");
      int HosipCount = 0;
	    if (tHospitalNo != null) 
	    {HosipCount = tHospitalNo.length;
	  
	    	 for (int i = 0; i < HosipCount; i++)	
				{ 
  					LLCommendHospitalSchema tLLCommendHospitalSchema = new LLCommendHospitalSchema();
            tLLCommendHospitalSchema.setConsultNo("");
            tLLCommendHospitalSchema.setHospitalCode(tHospitalNo[i]);
            tLLCommendHospitalSchema.setHospitalName(tHospitalName[i]);
            tLLCommendHospitalSchema.setHosAtti(tHospitalAttr[i]);
            tLLCommendHospitalSchema.setSessionRoom(tSessionroom[i]);
            tLLCommendHospitalSchema.setBed(tBed[i]);
            tLLCommendHospitalSet.add(tLLCommendHospitalSchema);
            System.out.println("tLLCommendHospitalSet" + tLLCommendHospitalSet.encode());
            	
	      }
      }

    
    //事件
    String tNum[] = request.getParameterValues("SubReportGridNo");
		String tEventNo[] = request.getParameterValues("SubReportGrid1");
		String tStartDate[] = request.getParameterValues("SubReportGrid2");  
		String tAccProvinceCode[] =request.getParameterValues("SubReportGrid10");  // #3500 发生地点省 
		String tAccCityCode[] =request.getParameterValues("SubReportGrid11");        //发生地点市
		String tAccCountyCode[] =request.getParameterValues("SubReportGrid12");      //发生地点县
		String tAccident[] = request.getParameterValues("SubReportGrid6");     // 发生地点       
		String tInHospital[] = request.getParameterValues("SubReportGrid7");   // 住院日期
		String tOutHospital[] = request.getParameterValues("SubReportGrid8");  // 出院日期
		String tEventInfo[] = request.getParameterValues("SubReportGrid9");    // 事件信息
		String tChk[]=request.getParameterValues("InpSubReportGridChk");
      int ImpartCont3 = 0;
	    if (tStartDate != null) 
	    {
	    	ImpartCont3 = tStartDate.length;
	    	 for (int i = 0; i < ImpartCont3; i++)	
				{ 
            if (tEventNo[i].length()==0 || tChk[i].equals("1")){
            	System.out.println("事件号为空"+tEventNo[i]);
            	LLSubReportSchema tLLSubReportSchema   = new LLSubReportSchema();
            	tLLSubReportSchema.setSubRptNo(tEventNo[i]);
            	tLLSubReportSchema.setCustomerNo(request.getParameter("LogerNo"));
            	tLLSubReportSchema.setCustomerName(request.getParameter("CustomerName"));
            	tLLSubReportSchema.setCustSituation(request.getParameter("CustSituation"));
            	tLLSubReportSchema.setAccDate(tStartDate[i]);
            	System.out.println(tStartDate[i]);
            	tLLSubReportSchema.setAccDesc(tEventInfo[i]);
            	tLLSubReportSchema.setAccPlace(tAccident[i]);
            	tLLSubReportSchema.setInHospitalDate(tInHospital[i]);
            	tLLSubReportSchema.setOutHospitalDate(tOutHospital[i]);
            	tLLSubReportSchema.setAccProvinceCode(tAccProvinceCode[i]); // # 3500 发生地点省
            	tLLSubReportSchema.setAccCityCode(tAccCityCode[i]);    // 发生地点市
            	tLLSubReportSchema.setAccCountyCode(tAccCountyCode[i]); // 发生地点县
            	System.out.println("发生地点省:"+tAccProvinceCode[i]+","+"发生地点市："+tAccCityCode[i]+"发生地点县："+tAccCountyCode[i]+"。");
  			 	tLLSubReportSet.add(tLLSubReportSchema);
            	}
	      }
      }
			

       //工作流标记
  		TransferData mf = new TransferData();
  		mf.setNameAndValue("WFFLAG","0");
  		String logNo = tLLMainAskSchema.getLogNo();
  	//传入咨询类型
  	mf.setNameAndValue("AskType",request.getParameter("AskType"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
		tVData.add(tLLMainAskSchema);
		tVData.add(tLLConsultSchema);
		tVData.add(tLLAnswerInfoSchema);
		tVData.add(tLLCommendHospitalSet);
		tVData.add(tLLSubReportSet);
  	tVData.add(tG);
  	tVData.add(mf);
    tOLLMainAskUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLLMainAskUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
 
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  String ConsultNo="";
  if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact) )
  {
    VData res = tOLLMainAskUI.getResult();
    System.out.println(res);
    LLConsultSchema mLLConsultSchema = new LLConsultSchema();
    TransferData tf = (TransferData) res.getObjectByObjectName("TransferData", 0);
    mLLConsultSchema.setSchema((LLConsultSchema) res.
                                        getObjectByObjectName("LLConsultSchema",
                0));
    
    logNo =(String) tf.getValueByName("logNo");
  	ConsultNo = mLLConsultSchema.getConsultNo();
  }
  //添加各种预处理
  
%>                      

<html>
<script language="javascript">
    <%if(logNo.equals("")){%>
    	parent.fraInterface.fm.all("LogNo").value="";
        <%}else{%>	
	parent.fraInterface.fm.all("LogNo").value = "<%=logNo%>";	
		parent.fraInterface.fm.all("ConsultNo").value = "<%=ConsultNo%>";
	parent.fraInterface.fm.all("CNNo").value = "<%=ConsultNo%>";
	parent.fraInterface.fm.all("NoticeNo").value = "<%=ConsultNo%>";
	<%}%>

	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	//parent.fraInterface.displayMain();
</script>
</html>
