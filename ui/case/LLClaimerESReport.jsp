<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLPolicyInput.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%
GlobalInput tG1 = (GlobalInput)session.getValue("GI");
String CurrentDate= PubFun.getCurrentDate();   
String tCurrentYear=StrTool.getVisaYear(CurrentDate);
String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
String AheadDays="-90";
FDate tD=new FDate();
Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
FDate fdate = new FDate();
String afterdate = fdate.getString( AfterDate );
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLClaimerESReport.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<script>
	function initDate(){
   fm.EndCaseDateS.value="<%=afterdate%>";
   fm.EndCaseDateE.value="<%=CurrentDate%>";
   fm.ManageCom.value="<%=tG1.ManageCom%>";
  }
</script>
</head>
<body onload="initDate();" >    
  <form action="./LLClaimerESReportSave.jsp" method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
		      <TD  class= title>结案起期</TD>
          <TD  class= input> <Input name=EndCaseDateS class='coolDatePicker' dateFormat='short' verify="结案起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>结案止期</TD>
          <TD  class= input> <Input name=EndCaseDateE class='coolDatePicker' dateFormat='short' verify="结案止期|notnull&Date" elementtype=nacessary> </TD> 
       	</TR>
       	<TR  class= common>
		      <TD  class= title>案件类型</TD>
          <TD  class= input> <input class="codeno" CodeData="0|2^1|普通案件^2|批量处理案件"  name=CaseType ondblclick="return showCodeListEx('CaseType',[this,CaseTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CaseType',[this,CaseTypeName],[0,1]);"><input class=codename name=CaseTypeName></TD> 
         </TR>
    </table>
    <hr>

    <input type="hidden" name=op value="">
    <table class=common>
    	<tr class=common>
          <TD  class= title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="downLoad()"></TD>
			</tr>
		</table>
	<Input type="hidden" class= common name="RiskRate" >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 