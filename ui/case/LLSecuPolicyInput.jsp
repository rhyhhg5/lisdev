<html>
  <%
  //Name：LLSecuPolicyInput.jsp
  //Function：社保政策配置
  //Date：2006-09-20
  //Author：Helga
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

  <head >
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/V<u></u>erifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="LLSecuPolicyInput.js"></SCRIPT>
    <%@include file="LLSecuPolicyInputInit.jsp"%>
  </head>
  <body  onload="initForm();initElementtype();" >

    <form action="./LLSecuPolicySave.jsp" method=post name=fm target="fraSubmit">

      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBaseInfo);">
            <TD class= titleImg>
              基本分类信息
            </TD>
          </TR>
        </table>

        <Div  id= "divBaseInfo" style= "display: ''">
          <table  class= common>
            <TR  class= common8>
              <TD  class= title>管理机构</TD>
            <TD  class= input><Input class="codeno" name=ManageCom  verify="管理机构|code:comcode&NOTNULL&INT" onclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName elementtype=nacessary></TD>
              <TD  class= title8>生效日期</TD>
              <TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()" elementtype=nacessary></TD>
              <TD  class= title8>失效日期</TD>
              <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()" elementtype=nacessary></TD>
            </TR>
            <TR  class= common8>
              <TD  class= title>基金类型</TD>
              <TD  class= input><Input class="codeno" name=SecurityType  verify="基金类型|code:comcode&NOTNULL&INT" onclick="return showCodeList('llsecuritytype',[this,SecurityTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('llsecuritytype',[this,SecurityTypeName],[0,1],null,null,null,1);"><input class=codename name=SecurityTypeName elementtype=nacessary></TD>
              <TD  class= title8>费用类型</TD>
              <TD  class= input8><input class="fcodeno" name="FeeType"  CodeData="0|2^1|门诊^2|住院" onclick="return showCodeListEx('FeeType',[this,FeeTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeType',[this,FeeTypeName],[0,1]);"><input class=fcodename name=FeeTypeName  elementtype=nacessary verify="账单种类|notnull" elementtype=nacessary></TD>
              <TD  class= title8>最高支付限额</TD>
              <TD  class= input8><input class=common name=PeakLine elementtype=nacessary></TD>
            </TR>
          </table>
        </Div>

<input name="ModiSave" style="display:''"  class=cssButton type=button value="查 询" onclick="Query()">
<input name="ModiSave" style="display:''"  class=cssButton type=button value="测 算" onclick="Open()">


        <table>
          <TR>
            <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGetLimit);">
            </TD>
            <TD class= titleImg>
              起付线配置
            </TD>
          </TR>
        </table>

        <Div  id= "divGetLimit" style= "display: ''">
          <table class=common>
            <tr class=common>
              <td colspan=6>
                <input type="checkbox" value="1" name="cHosGrade" onclick="initGetLimitStyle();">与医院级别相关
                <input type="checkbox" value="4" name="cUrbanOption" onclick="initGetLimitStyle();">与城区属性相关
                <input type="checkbox" value="2" name="cInsuredStat" onclick="initGetLimitStyle();">与人员类型相关
                <input type="checkbox" value="3" name="cIsFirst" onclick="initGetLimitStyle();">与住院次数相关
                <input type="checkbox" value="4" name="cAge" onclick="initGetLimitStyle();">与人员年龄相关
              </td>
            </tr>
          </table>
          <table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanGetLimitGrid" >
                </span>
              </TD>
            </TR>
          </table>
        </Div>

        <table>
          <TR>
            <TD>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGetRate);">
            </TD>
            <TD class= titleImg>
              分段给付比例配置
            </TD>
          </TR>
        </table>

        <Div  id= "divGetRate" style= "display: ''">
          <table class=common>
            <tr class=common>
              <td colspan=6>
                <input type="checkbox" value="1" name="dHosGrade" onclick="initGetRateGrid()">与医院级别相关
                <input type="checkbox" value="4" name="dUrbanOption" onclick="initGetRateGrid()">与城区属性相关
                <input type="checkbox" value="2" name="dInsuredStat" onclick="initGetRateGrid()">与人员类型相关
                <input type="checkbox" value="3" name="dIsFirst" onclick="initGetRateGrid()">与住院次数相关
                <input type="checkbox" value="4" name="dAge" onclick="initGetRateGrid()">与人员年龄相关
              </td>
            </tr>
          </table>
          <table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanGetRateGrid" >
                </span>
              </TD>
            </TR>
          </table>
        </Div>

        <input name="ModiSave" style="display:''"  class=cssButton type=button value="保 存" onclick="ModifySave()">

        
        <!--隐藏域-->
        <input type=hidden id="fmtransact" name="fmtransact">
        <input type=hidden id="operate" name="operate">

      </form>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </body>
  </html>
