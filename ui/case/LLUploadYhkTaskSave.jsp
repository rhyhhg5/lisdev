<%@page import="com.sinosoft.httpclientybk.inf.N01"%>
<%@page import="com.sinosoft.httpclientybk.inf.U01"%>
<%@page import="com.sinosoft.httpclientybk.inf.YBU04"%>
<%@page import="com.sinosoft.httpclientybk.T01.YbPremInfoUp"%>
<%@page import="com.sinosoft.httpclientybk.T02.YbBankInfoUp"%>
<%@page import="com.sinosoft.httpclientybk.inf.E01"%>
<%@page import="com.sinosoft.httpclientybk.inf.E03"%>
<%@page import="com.sinosoft.httpclientybk.inf.C01"%>
<%@page import="com.sinosoft.httpclientybk.inf.C12"%>
<%@page import="com.sinosoft.httpclientybk.deal.YbkXBTB"%>
<%
//程序名称：ClaimSave.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：wujs
//更新记录：  更新人    更新日期     更新原因/内容

%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page import=" java.text.*"%>
<%@page import= "java.util.*"%>

<%
//接收信息，并作校验处理。
//输入参数
Boolean flag = false;
String oper = request.getParameter("Opt");
String pNumber = request.getParameter("pNumber");
String tEndDate = request.getParameter("EndDate");
String FlagStr = "";
String Content = "";
String tBusinessFlag = "";
VData tVData = new VData();
TransferData tTransferData = new TransferData();
tTransferData.setNameAndValue("PrtNo", pNumber);
tVData.add(tTransferData);

//银行卡缴费信息上传
YbBankInfoUp yhkTask = new YbBankInfoUp();
 if(oper.equals("yhk")) {
	 
 	if(yhkTask.submitData(tVData, "YBKPT"))
		 Content="银行卡缴费结果信息上传成功!";
	else
		 Content="银行卡缴费结果信息上传失败!";
 }
 //核保信息上传
 U01 hbTask = new U01();
 if(oper.equals("hb")) {
	  
 	if(hbTask.submitData(tVData, "YBKPT"))
		 Content="核保信息上传成功!";
	else
		 Content="核保信息上传失败!";
 }
 //承保信息上传
 N01 ykTask = new N01();
 if(oper.equals("cb")) {
 	 if(ykTask.submitData(tVData, "YBKPT"))
 		 Content="承保信息上传成功!";
	 else
		 Content="承保信息上传失败!";
 }
 //保费信息上传
 YbPremInfoUp ybTask = new YbPremInfoUp();
 if(oper.equals("bf")){
	 if(ybTask.submitData(tVData, "YBKPT"))
		 Content="保费信息上传成功";
	 else
		 Content="保费信息上传失败";
}
 //平台自核上传
 YBU04 zhTask = new YBU04();
 if(oper.equals("ptzh")) {

 	 if(zhTask.submitData(tVData, "YBKPT"))
 		 Content="平台自核上传成功!";
	 else
		 Content="平台自核上传失败!";
 }
 //保全信息同步上传
 E01 bqTask = new E01();
 if(oper.equals("bq")) {

 	 if(bqTask.submitData(tVData, "YBKPT"))
 		 Content="保全信息同步上传成功!";
	 else
		 Content="保全信息同步上传失败!";
 }
 //续保退保信息同步上传
 E03 xbtbTask = new E03();
 if(oper.equals("xbtb")) {

 	 if(xbtbTask.submitData(tVData, "YBKPT"))
 		 Content="续保退保信息同步上传成功!";
	 else
		 Content="续保退保信息同步上传失败!";
 }
 
 //理赔信息同步上传
 C01 c01 = new C01();
 if(oper.equals("lp")) {

 	 if(c01.submitData(tVData, "YBKPT"))
 		 Content="理赔信息同步上传成功!";
	 else
		 Content="理赔信息同步上传失败!";
 }

//理赔注销同步上传
C12 c12 = new C12();
if(oper.equals("lpzx")) {

	 if(c12.submitData(tVData, "YBKPT"))
		 Content="理赔注销同步上传成功!";
	 else
		 Content="理赔注销同步上传失败!";
}

YBKE01Task eTask = new YBKE01Task();
 if(oper.equals("test")) {

	 eTask.run();
 	 Content="保全批处理上传完成!";
 }
 //解锁
 YbBankInfoUpUnlockTask unlockTask = new YbBankInfoUpUnlockTask();
 if(oper.equals("unlock")) {
	 unlockTask.run();
 	 Content="上报解锁批处理完成!";
 }
 //保单再发送
 YBKAutoSendMailTask tYBKAutoSendMailTask = new YBKAutoSendMailTask();
 if(oper.equals("sendagain")){
	 tYBKAutoSendMailTask.run();
	 Content="保单再发送批处理完成!";
 }
 
 //医保卡个单抽档测试
 YbkXBTB tYbkXBTB = new YbkXBTB();
 if(oper.equals("ybkcd")){
	 GlobalInput mGlobalInput = new GlobalInput();
     mGlobalInput.Operator="YBK";
     mGlobalInput.ComCode="86310000";
     mGlobalInput.ManageCom="86310000";
	 flag = true;
	 tBusinessFlag = "2";
	 VData mVData = new VData();
	 TransferData mTransferData = new TransferData();
	 mTransferData.setNameAndValue("ContNo", pNumber);
	 mTransferData.setNameAndValue("StartDate","2016-01-01");
	 mTransferData.setNameAndValue("EndDate",tEndDate);
	 mTransferData.setNameAndValue("BusinessFlag",tBusinessFlag);
	 mTransferData.setNameAndValue("ManageCom",mGlobalInput.ManageCom);
	 mTransferData.setNameAndValue("flag", flag);
	 mVData.add(mGlobalInput);
	 mVData.add(mTransferData);
	 if(tYbkXBTB.submitData(mVData, "YBK"))
		 Content="医保卡个单个案抽档完成";
	 else
		 Content="医保卡个单个案抽档失败";
 }
 
 //邮件提醒功能
 YBKClaimMailTask mailTask = new YBKClaimMailTask(pNumber);
 YBKClaimMailTask mailTask1 = new YBKClaimMailTask();
 if(oper.equals("mailremind")){
	 if("".equals(pNumber)||pNumber==null){
		 mailTask1.run();
		 Content="邮件发送成功,请查收!";
	 }else{
		 mailTask.run();
		 Content="邮件发送成功,请查收!";
	 }
 } 
 //人工核保系统强撤功能测试
 YbkUnderWritingCancleTask cancleTask = new YbkUnderWritingCancleTask(pNumber);
 if(oper.equals("ybkCancle")){
	 if("".equals(pNumber)||pNumber==null){
		 Content="请填写业务号!";
	 }else{
		 cancleTask.run();
		 Content="人工核保系统强撤成功,请查收!";
	 }
 }
%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
