var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass(); 
var tSaveType="";



function easyQueryClick()
{
	
  // 书写SQL语句
  
  var strSQL= "SELECT a.affixcode, b.affixname FROM LLMAppReasonAffix a,llmaffix b "
  			  +" where reasoncode='" + fm.all('codetype').value+ "'"
  			  +" and a.affixcode=b.affixcode"
//  			  +" and b.code=a.affixcode"
  			  ;
                                
 arrResult = easyExecSql(strSQL);
	if ( arrResult!=null )
	{
		 displayMultiline(arrResult,AffixGrid);  
  	}
	else
	{
		AffixGrid.clearData();
	}

}


function ModifySave()
{
	
	  if(fm.all('ManageCom').value=="")
	   {
	   	alert("请输入管理机构！");
	   	return;
	   	}     
	  if(fm.all('RgtDateS').value=="")
	   {
	   	alert("请输入生效日期！");
	   	return;
	   	}     
	  if(fm.all('RgtDateE').value=="")
	   {
	   	alert("请输入失效日期！");
	   	return;
	   	}     
	  if(fm.all('FeeType').value=="")
	   {
	   	alert("请输入费用类型！");
	   	return;
	   	}     
	  if(fm.all('SecurityType').value=="")
	   {
	   	alert("请输入基金类型！");
	   	return;
	   	}     
	  if(fm.all('PeakLine').value=="")
	   {
	   	alert("请输入最高支付限额！");
	   	return;
	   	}     	   		   		   		   		  
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.action ="./LLSecuPolicySave.jsp";
	  fm.submit();
   	//alert(GetRateGrid.mulLineCount);
	
}


function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      
   
  }
}

function initGetLimitStyle(obj){
  initGetLimitGrid();
  var arr = new Array();
  var brr = new Array();
  var crr = new Array();
  var drr = new Array();
  var condition = false;
  var LineNo = 1;
  if(fm.cHosGrade.checked){
    arr = new Array("三级","二级","一级");
    aarr = new Array("3","2","1");
    LineNo = LineNo*3;
  }
  if(fm.cUrbanOption.checked){
    brr= new Array("城区","非城区");
    bbrr= new Array("1","0");
    LineNo = LineNo*2;
  }
  if(fm.cInsuredStat.checked){
    crr= new Array("在职","退休");
    ccrr= new Array("1","2");
    LineNo = LineNo*2;
  }
  if(fm.cIsFirst.checked){
    drr=new Array("第一次住院","第二次住院","其它");
    ddrr=new Array("1","2","9");
    LineNo = LineNo*3;
  }
  
  var ai = 0;
  var bi = 0;
  var ci = 0;
  var di = 0;
  for(var num=0;num<LineNo;num++){
    if(num!=0)
      GetLimitGrid.addOne("GetLimitGrid");
    if(arr.length!=0){
      GetLimitGrid.setRowColData(num,1,arr[ai]);
      GetLimitGrid.setRowColData(num,2,aarr[ai]);
      if(drr.length==0&&crr.length==0&&brr.length==0)
        ai++;
    }
    if(brr.length!=0){
      GetLimitGrid.setRowColData(num,3,brr[bi]);
      GetLimitGrid.setRowColData(num,4,bbrr[bi]);
      if(drr.length==0&&crr.length==0)
        bi++;
    }
    if(crr.length!=0){
      GetLimitGrid.setRowColData(num,5,crr[ci]);
      GetLimitGrid.setRowColData(num,6,ccrr[ci]);
      if(drr.length==0)
        ci++;
    }
    if(drr.length!=0){
      GetLimitGrid.setRowColData(num,7,drr[di]);
      GetLimitGrid.setRowColData(num,8,ddrr[di]);
      di++;
    }
    if(di>2){
      di=0;
      if(crr.length!=0)
      {
      	ci++;
      }
      else if(brr.length!=0)
      {
      	bi++;
      }
      else if(arr.length!=0)
      {
      	ai++;
      }
      else 
      {
      }
    }
    if(ci>1){
      ci=0;
      if(brr.length!=0)
      {
      	bi++;
      }
      else if(arr.length!=0)
      {
        ai++;
      }
      else
      {
      }
    }
    if(bi>1){
      bi=0;
      if(arr.length!=0)
      {
      	ai++;
      }
    }
  }
}


function Query()
{
	  if(fm.all('ManageCom').value=="")
	   {
	   	alert("请输入管理机构查询！");
	   	return;
	   	} 
	  if(fm.all('SecurityType').value=="")
	   {
	   	alert("请输入基金类新查询！");
	   	return;
	   	} 	 
	  if(fm.all('FeeType').value=="")
	   {
	   	alert("请输入费用类型查询！");
	   	return;
	   	} 	
	 	    
	var strSQL= "select distinct PeakLine,StartDate,enddate from llsecurityfactor where 1 = 1"
	             +getWherePart('ComCode','ManageCom')
	             +getWherePart('SecurityType','SecurityType')
	             +getWherePart('FeeType','FeeType')
	             +getWherePart('StartDate','RgtDateS')
	             +getWherePart('EndDate','RgtDateE');
		if(fm.all('PeakLine').value!=null&&fm.all('PeakLine').value!="")
	  {
      strSQL = strSQL + " and PeakLine = " +fm.all('PeakLine').value;
	  }	
 arrResult = easyExecSql(strSQL);
	if ( arrResult!=null&&arrResult.length>1 )
	  {
		  alert("查询结果不唯一,请增加条件查询！");  
		  return;
  	}
  else if (arrResult!=null)
  	{
  		fm.all('PeakLine').value=arrResult[0][0];
  		fm.all('RgtDateS').value=arrResult[0][1];
  		fm.all('RgtDateE').value=arrResult[0][2];
  	}
  else 
  	{
  	 alert("没有数据！");
  	 fm.cHosGrade.checked=false;
  	 fm.cUrbanOption.checked=false;
  	 fm.cInsuredStat.checked=false;
  	 fm.cIsFirst.checked=false;
  	 fm.cAge.checked=false;

  	 fm.dHosGrade.checked=false;
  	 fm.dUrbanOption.checked=false;
  	 fm.dInsuredStat.checked=false;
  	 fm.dIsFirst.checked=false;
  	 fm.dAge.checked=false;
  	   	 
  	 initGetLimitGrid();
  	 initGetRateGrid();
  	 return;
  	}


//  	
  strSQL = "select distinct "
          +"(select case levelcode when '1' then '一级' when '2' then '二级' when '3' then '三级' end from dual),LevelCode,"
          +"(select case StandbyFlag1 when '0' then '非城区' when '1' then '城区' end from dual),StandbyFlag1,"
          +"(select case InsuredStat when '1' then '在职' when '2' then '退休' end from dual),InsuredStat,"
          +"(select case FirstInHos when '1' then '第一次住院' when '2' then '第二次住院' when '9' then '其它' end from dual),FirstInHos,"
          +"standbyflag2,"
          +"standbyflag3,"
          +"GetLimit from llsecurityfactor where 1=1"
           	+getWherePart('ComCode','ManageCom')
	          +getWherePart('SecurityType','SecurityType')
	          +getWherePart('FeeType','FeeType')
	          +getWherePart('StartDate','RgtDateS')
	          +getWherePart('EndDate','RgtDateE') ;	
	          
		if(fm.all('PeakLine').value!=null&&fm.all('PeakLine').value!="")
	  {
      strSQL = strSQL + " and PeakLine = " +fm.all('PeakLine').value;
	  }
	  
	  strSQL = strSQL+" order by LevelCode,StandbyFlag1,InsuredStat,FirstInHos,standbyflag2,standbyflag3";

	arrResult = easyExecSql(strSQL);          
  if ( arrResult!=null )
	 {
	 	 for(var num=0;num<arrResult.length;num++)
	 	 {
	 	 	 if(arrResult[num][0]!="")
	 	 	 {
	 	 	 	fm.cHosGrade.checked=true;
	 	 	 }
	 	 }
	 	 
	 	 for(var num=0;num<arrResult.length;num++)
	 	 {
	 	 	 if(arrResult[num][2]!="")
	 	 	 {
	 	 	 	fm.cUrbanOption.checked=true;
	 	 	 }
	 	 }	 	 
	 	 
	 	 for(var num=0;num<arrResult.length;num++)
	 	 {
	 	 	 if(arrResult[num][4]!="")
	 	 	 {
	 	 	 	fm.cInsuredStat.checked=true;
	 	 	 }
	 	 }

	 	 for(var num=0;num<arrResult.length;num++)
	 	 {
	 	 	 if(arrResult[num][6]!="")
	 	 	 {
	 	 	 	fm.cIsFirst.checked=true;
	 	 	 }
	 	 }
	 	 
	 	 for(var num=0;num<arrResult.length;num++)
	 	 {
	 	 	 if(arrResult[num][8]!="")
	 	 	 {
	 	 	 	fm.cAge.checked=true;
	 	 	 	
	 	 	 }
	 	 }
	 	 initGetLimitGrid();
		 displayMultiline(arrResult,GetLimitGrid);  

   }	



//   
  strSQL = "select  "
          +"(select case levelcode when '1' then '一级' when '2' then '二级' when '3' then '三级' end from dual),LevelCode,"
          +"(select case StandbyFlag1 when '0' then '非城区' when '1' then '城区' end from dual),StandbyFlag1,"
          +"(select case InsuredStat when '1' then '在职' when '2' then '退休' end from dual),InsuredStat,"
          +"(select case FirstInHos when '1' then '第一次住院' when '2' then '第二次住院' when '9' then '其它' end from dual),FirstInHos,"
          +"standbyflag2,"
          +"standbyflag3,"
          +"DownLimit,"
          +"UpLimit,"
          +"GetRate from llsecurityfactor where 1=1"
           	+getWherePart('ComCode','ManageCom')
	          +getWherePart('SecurityType','SecurityType')
	          +getWherePart('FeeType','FeeType')
	          +getWherePart('StartDate','RgtDateS')
	          +getWherePart('EndDate','RgtDateE');	
	          
		if(fm.all('PeakLine').value!=null&&fm.all('PeakLine').value!="")
	  {
      strSQL = strSQL + " and PeakLine = " +fm.all('PeakLine').value;
	  }

    strSQL = strSQL+" order by LevelCode,StandbyFlag1,InsuredStat,FirstInHos,standbyflag2,standbyflag3";
	brrResult = easyExecSql(strSQL);          
  if ( brrResult!=null )
	 {
	 	 for(var num=0;num<brrResult.length;num++)
	 	 {
	 	 	 if(brrResult[num][0]!="")
	 	 	 {
	 	 	 	fm.dHosGrade.checked=true;
	 	 	 }
	 	 }
	 	 
	 	 for(var num=0;num<brrResult.length;num++)
	 	 {
	 	 	 if(brrResult[num][2]!="")
	 	 	 {
	 	 	 	fm.dUrbanOption.checked=true;
	 	 	 }
	 	 }	 	 
	 	 
	 	 for(var num=0;num<brrResult.length;num++)
	 	 {
	 	 	 if(brrResult[num][4]!="")
	 	 	 {
	 	 	 	fm.dInsuredStat.checked=true;
	 	 	 }
	 	 }

	 	 for(var num=0;num<brrResult.length;num++)
	 	 {
	 	 	 if(brrResult[num][6]!="")
	 	 	 {
	 	 	 	fm.dIsFirst.checked=true;
	 	 	 }
	 	 }
	 	 
	 	 for(var num=0;num<brrResult.length;num++)
	 	 {
	 	 	 if(brrResult[num][8]!="")
	 	 	 {
	 	 	 	fm.dAge.checked=true;
	 	 	 }
	 	 }
	 	 initGetRateGrid();	 	
		 displayMultiline(brrResult,GetRateGrid);  
   }   
   
}


function Open()
{
  var pathStr="./LLTestCalMain.jsp?Interface=LLTestCal.jsp";
  var newWindow=OpenWindowNew(pathStr,"测算","left");
}