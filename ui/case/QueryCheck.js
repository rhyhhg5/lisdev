//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  showSubmitFrame(mDebug);
  fm.action = './CasePolicySave.jsp';
  fm.submit(); //提交
}

function ShowCheckInfo()
{

  fm.ClmUWNo.value = 0;//为ClmUWNo设定初始值

  if(CasePolicyGrid.getSelNo()==0)
  {
    alert("请选中要返回的记录！");
    return;
  }
  else
  {

    var count = CasePolicyGrid.getSelNo();
    var tClmUWNo   = CasePolicyGrid.getRowColData(CasePolicyGrid.getSelNo()-1,5);
    var tUpClmUWer = CasePolicyGrid.getRowColData(CasePolicyGrid.getSelNo()-1,2);
    fm.ClmUWNo.value   = tClmUWNo;
    fm.UpClmUWer.value = tUpClmUWer;
    fm.action = './ShowCheckDetail.jsp';
    //fm.target = '_blank';
    fm.submit();
  //  alert("0000000");
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LLCasePolicy.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
 }
//查询按钮的函数
function submitFormCasePolicy()
{
  if ((fm.InsuredNo.value=="null")||(fm.InsuredNo.value==""))
  {
    alert("客户号码为空，不能进行查询操作！！！");
    return;
  }
  else
  {
    var i = 0;
    var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    initCasePolicyGrid();
    fm.action = './RegisterQueryCasePolicy.jsp';
    fm.submit(); //提交
  }
}

function displayQueryResult(strResult)
{
  strResult = Conversion(strResult);
  var filterArray          = new Array(3,11,14,5,0);
  turnPage.strQueryResult  = strResult;
  turnPage.useSimulation   = 1;
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  turnPage.pageDisplayGrid = CasePolicyGrid;
  turnPage.pageIndex       = 0;
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
}

function showCasePolicyInfo()
{
	initCasePolicyGrid();
  fm.action = './ShowCheckInfo.jsp';
  fm.submit();
}

function EndPolPower()
{
	fm.action = './EndPolSave.jsp';
	fm.submit(); //提交
}

function StartPolPower()
{
	fm.action = './RestartPolSave.jsp';
	fm.submit(); //提交
}