<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ContUWSave.jsp
//程序功能：合同单人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
 //wdx新增的代码11
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
//输出参数
CErrors tError = null;

String insuredNo = request.getParameter("InsuredNo"); //客户号
//	String tChk[] = request.getParameterValues("ColNamePolGrid"); //Sel Chk
	String tGrid16[] = request.getParameterValues("PolGrid18"); //得到第1列的所有值
	 
	ExeSQL exeSQL = new ExeSQL();
	int leng=tGrid16.length;
	
String FlagStr = "Fail";
String Content = "";
String message="";
 
if (leng > 0) {
	  for (int index = 0; index < leng; index++) {
		    
			String  polno = tGrid16[index];
			System.out.println("客户号："+insuredNo+"polno："+polno);
			String sql=" select contno , polno,riskcode from lcpol where 1=1 and polno='"+polno+"' and InsuredNo='"+insuredNo+"' " 
			 + " union "
			 + " select contno,polno,riskcode from lbpol where 1=1 and polno='"+polno+"' and InsuredNo='"+insuredNo+"' " ;
			SSRS ssrs= new SSRS();
			ssrs=exeSQL.execSQL(sql);
			if (ssrs != null && ssrs.MaxRow ==1) {
				String riskcode=ssrs.GetText(1,3);
				if("170501".equals(riskcode) || "162401".equals(riskcode) ||"162501".equals(riskcode) ){
					//险种正确
					
					
					//新增加，如果责任为理赔疾病身故保险金后，查询到的保额we
					String se1=" select distinct 1  from llclaimdetail a , LcPol b ,llcase c  where 1 =1 "
						+ " and a.contno=b.contno "
						+ " and a.grpcontno=b.grpcontno "
						+ " and a.polno=b.polno "
						+ " and a.riskcode=b.riskcode "
						+ " and a.caseno=c.caseno "
						+ " and c.rgtstate in ('11','12') "
						+ " and a.getdutycode in ('741204','740204','701204') "
						+ " and b.polno='"+polno+"' "
						+ " and b.insuredno='"+insuredNo+"'  " 
						+ " union  "
						+ " select distinct 1  from llclaimdetail a , LbPol b ,llcase c  where 1 =1 "
						+ " and a.contno=b.contno "
						+ " and a.grpcontno=b.grpcontno "
						+ " and a.polno=b.polno "
						+ " and a.riskcode=b.riskcode "
						+ " and a.caseno=c.caseno "
						+ " and c.rgtstate in ('11','12') "
						+ " and a.getdutycode in ('741204','740204','701204') "
						+ " and b.polno='"+polno+"' "
						+ " and b.insuredno='"+insuredNo+"'  " 
						
						
						;
					String se1val=exeSQL.getOneValue(se1) ;
					
					if("1".equals(se1val) || se1val=="1"){
						%>
						
						<script language="javascript" type="">
			    			parent.fraInterface.afterSubmitM("<%=index %>","<%=0 %>");
						</script>
						<%
						continue;
					}
				}else{
				  continue;
				}
				
				LLCaseCommon  llcc = new LLCaseCommon();
				//FlagStr为判断结果的参数，Succ-成功，Fail-执行失败并停止上报
				double money=llcc.getAmntFromAcc(insuredNo,polno,PubFun.getCurrentDate(),"CB");
				System.out.println("金额："+money);
				%>
				
				<script language="javascript" type="">
	    			parent.fraInterface.afterSubmitM("<%=index %>","<%=money %>");
				</script>
				<%
			}else if(ssrs != null && ssrs.MaxRow >1){
				continue;
			}else{
				continue;
			}
			
	  }
}else{
		%>
		<script type="text/javascript">
	alert("查询失败，表格中(table:ColNamePolGrid)未查询到数据");
	</script> 
		<%
	}
 

//----------------------------------------------------------------------------------
	/*
	 *下面内容为原来的，上面为我新增的。下面代码报错。
	 *
	 *
	 *
	 //输出参数
	  CErrors tError = null;
	  String FlagStr = "Fail";
	  String Content = "";
	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  
  //校验处理
  //内容待填充
  
  //接收信息
  System.out.println("Start Save...");
  
	String tSendUW[] = request.getParameterValues("PolGridChk");
	String tContNo[] = request.getParameterValues("PolGrid1");
	String tAppntName[] = request.getParameterValues("PolGrid2");
	String tHealthImpartNo1[] = request.getParameterValues("PolGrid4");
	String tHealthImpartNo2[] = request.getParameterValues("PolGrid5");
	String tNoImpartDesc[] = request.getParameterValues("PolGrid6");
	String tCaseNo = request.getParameter("CaseNo");
	String tInsuredNo = request.getParameter("InsuredNo");
	int tSendUWCount = tSendUW.length;	
	
	System.out.println("tSendUWCount" + tSendUWCount );
	
	
 
  LLCUWBatchSet tLLCUWBatchSet = new LLCUWBatchSet();
	for (int i = 0; i < tSendUWCount; i++)
	{
		if (!tSendUW[i].equals("") )
		{
		
	  	 LLCUWBatchSchema tLLCUWBatchSchema = new LLCUWBatchSchema();
	     tLLCUWBatchSchema.setContNo(tContNo[i]);
	     tLLCUWBatchSchema.setInsuredNo(tInsuredNo);
	     tLLCUWBatchSchema.setCaseNo(tCaseNo);
	     tLLCUWBatchSchema.setAppntName(tAppntName[i]);
	     tLLCUWBatchSchema.setHealthImpartNo1(tHealthImpartNo1[i]);
	     tLLCUWBatchSchema.setHealthImpartNo2(tHealthImpartNo2[i]);
	     tLLCUWBatchSchema.setNoImpartDesc(tNoImpartDesc[i]);
	   	     
		    tLLCUWBatchSet.add( tLLCUWBatchSchema );
		    
		}
	}
 
		  
	  	
 
  // 准备传输数据 VData
  VData tVData = new VData();

	
  tVData.addElement(tLLCUWBatchSet);
  tVData.addElement(tG);
  
 

  // 数据传输
 SecondUWUI tSecondUWUI = new SecondUWUI();
 
	if (!tSecondUWUI.submitData(tVData,""))
	{

      Content = " 保存失败，原因是: " + tSecondUWUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tSecondUWUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理
  System.out.println("Content:"+Content);
  
  */
%>                      
<html>
<script language="javascript">
	

</script>
</html>

