<html>
<%
//程序名称：LLExemptionSave.jsp
//程序功能：豁免保费页面数据处理
//创建日期：2013-12-03 10：10
//创建人  ：Houyd
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  String CaseNo = request.getParameter("CaseNo");
  String InsuredNo = request.getParameter("CustomerNo");
  String InsuredName = request.getParameter("CustomerName");
  String Remark = request.getParameter("Remark");

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tOperate = request.getParameter("fmtransact");
  System.out.println("tOperate:"+tOperate);
  tOperate = tOperate.trim();
  
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  
  //传往UI层的数据封装
  TransferData tTransferData = new TransferData();
  LLUnderWritUI tLLUnderWritUI = new LLUnderWritUI();
  
  if("SAVE".equals(tOperate)) {
      tTransferData.setNameAndValue("CaseNo",CaseNo);
      tTransferData.setNameAndValue("InsuredNo",InsuredNo);
      tTransferData.setNameAndValue("InsuredName",InsuredName);
      tTransferData.setNameAndValue("Remark",Remark);
           
  }
  
  //将封装完毕的页面数据放入容器
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tTransferData);
	tVData.addElement(tG);
	
  //传入BL层进行处理
  try
  {
      tLLUnderWritUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLLUnderWritUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	//Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
%>     
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>