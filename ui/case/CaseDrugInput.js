//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
//提交，保存按钮对应操作

function submitForm()
{
  var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./CaseDrugSave.jsp";
 	//fm.target='_blank';
  fm.submit(); //提交
  sumFee();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();  
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
   	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
 	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
 }

}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在CaseReceipt.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,0,*";
 	}
}



function easyQuery()
{
	var strSQL = "select a.DrugCode,a.DrugName,a.Fee,a.SecuFee,a.SelfPay2,a.SelfFee,coalesce(a.UnReasonableFee,0),a.Remark,"
	+"(select b.receiptno from llfeemain b where b.mainfeeno = a.mainfeeno) "
	+" from LLCasedrug a where 1=1 and mainfeeno is not null and inputtype is null "
         +getWherePart("CaseNo","CaseNo")
         +getWherePart("MainFeeNo","MainFeeNo");
   arrResult = easyExecSql(strSQL);
   if (arrResult){
   displayMultiline(arrResult,CaseDrugGrid);
  }
}

function sumFee()
{
  var sumFee = 0;
  var secuFee = 0;
  var selfPay2 = 0;
  var selfFee = 0;
  var unReasonableFee = 0;
  
  var num = CaseDrugGrid.mulLineCount;
  
  for (var i = 0; i<num; i++){
    sumFee += Number(CaseDrugGrid.getRowColDataByName(i,"SumFee"));
    secuFee += Number(CaseDrugGrid.getRowColDataByName(i,"SecuFee"));
    selfPay2 += Number(CaseDrugGrid.getRowColDataByName(i,"SelfPay2"));
    selfFee += Number(CaseDrugGrid.getRowColDataByName(i,"SelfFee"));
    unReasonableFee += Number(CaseDrugGrid.getRowColDataByName(i,"UnReasonableFee"));
  }
  
  fm.SumFee.value = mathRound(sumFee);
  fm.SecuFee.value = mathRound(secuFee);
  fm.SelfPay2.value = mathRound(selfPay2);
  fm.SelfFee.value = mathRound(selfFee);
  fm.UnReasonableFee.value = mathRound(unReasonableFee);
  
}

function getCaseRemark()
{
  var tCaseNo = fm.all('CaseNo').value;
  if(tCaseNo!=null&&tCaseNo!="")
  {
//	  alert("111");
    var sql = "Select Remark from LLCase where caseno = '"+tCaseNo+"' with ur";
    var tsql="select remark from llcaseext where caseno='"+tCaseNo+"' with ur";	
    var strQueryResult = easyExecSql(sql);
    var tQueryResult = easyExecSql(tsql);
    if(strQueryResult!=null && strQueryResult !="" &&(tQueryResult==null||tQueryResult=="")){
    		fm.Remark.value = strQueryResult[0][0];
     }else if (tQueryResult!=null && tQueryResult !="" &&(strQueryResult==null||strQueryResult=="")){
    	 fm.Remark.value = tQueryResult[0][0];
     }else if(tQueryResult!=null && tQueryResult !="" && strQueryResult!=null && strQueryResult !="" ){
    	 fm.Remark.value = strQueryResult[0][0]+tQueryResult[0][0];
    }else{
    	fm.Remark.value ="";
    	}
    }
   
  }
  


