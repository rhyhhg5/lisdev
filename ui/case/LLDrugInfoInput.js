//批量导入
var mDebug="0";
var mOperate="";
var showInfo;
var ImportPath;
var turnPage = new turnPageClass();

var str="";
function getstr()
{
  str="1 and comcode like #"+fm.MngCom.value+"%#";
}

//校验单价是否为数字并且保留两位小数
function checkNum(obj) {  
    //检查是否是非数字值  
    if (isNaN(obj.value)) {  
        obj.value = "";  
        alert("该项只能输入数字！");  
    }
    else
    {
    	if (obj != null) {  
            //检查小数点后是否为两位
            if (obj.value.toString().split(".").length > 1 && obj.value.toString().split(".")[1].length > 2) {  
                alert("小数点后只能保留两位！");  
                obj.value = "";  
            }  
        }  
        if (obj != null) {  
            //检查小数点后是否为两位
            if (obj.value.toString().length > 16) {  
                alert("最多只能输入16位数字！");  
            }  
        }  
    }
}  

//选择列表信息的单选框上面出现其对应的信息
function onClick(){
	var checkFlag = 0;
	for (i=0; i<EvaluateGrid.mulLineCount; i++)
	{
		if (EvaluateGrid.getSelNo(i))
		{
			checkFlag = EvaluateGrid.getSelNo();
			break;
		}
	}
	if(checkFlag){
		fm.all('DrugCode').value = EvaluateGrid.getRowColData(checkFlag - 1, 1);	
		fm.all('DrugCategory').value = EvaluateGrid.getRowColData(checkFlag - 1, 2);
		fm.all('DrugGenericName').value = EvaluateGrid.getRowColData(checkFlag - 1, 3);
		fm.all('DrugTradeName').value = EvaluateGrid.getRowColData(checkFlag - 1, 4);
		fm.all('DrugEnglishName').value = EvaluateGrid.getRowColData(checkFlag - 1, 5);
		fm.all('DrugClassification').value = EvaluateGrid.getRowColData(checkFlag - 1, 6);
		fm.all('SecondCategory').value = EvaluateGrid.getRowColData(checkFlag - 1, 7);
		fm.all('Formulations').value = EvaluateGrid.getRowColData(checkFlag - 1, 9);
		fm.all('Specifications').value = EvaluateGrid.getRowColData(checkFlag - 1, 10);
		fm.all('Phonetic').value = EvaluateGrid.getRowColData(checkFlag - 1, 11);
		fm.all('Manufacturer').value = EvaluateGrid.getRowColData(checkFlag - 1, 12);
		fm.all('PackageMessage').value = EvaluateGrid.getRowColData(checkFlag - 1, 13);
		fm.all('Administration').value = EvaluateGrid.getRowColData(checkFlag - 1, 14);
		fm.all('HealthNo').value = EvaluateGrid.getRowColData(checkFlag - 1, 15);
		fm.all('CostLevel').value = EvaluateGrid.getRowColData(checkFlag - 1, 16);
		fm.all('PaymentScope').value = EvaluateGrid.getRowColData(checkFlag - 1, 17);
		fm.all('OutpatientLimit').value = EvaluateGrid.getRowColData(checkFlag - 1, 18);
		fm.all('HospitalLimit').value = EvaluateGrid.getRowColData(checkFlag - 1, 19);
		fm.all('InjuriesMark').value = EvaluateGrid.getRowColData(checkFlag - 1, 20);
		fm.all('FertilityMark').value = EvaluateGrid.getRowColData(checkFlag - 1, 21);
		fm.all('PriceCeilingValue').value = EvaluateGrid.getRowColData(checkFlag - 1, 22);
		fm.all('Remark').value = EvaluateGrid.getRowColData(checkFlag - 1, 8);
		fm.all('MngCom_ch').value = EvaluateGrid.getRowColData(checkFlag - 1, 23);
		fm.all('AreaName').value = EvaluateGrid.getRowColData(checkFlag - 1, 24);
		fm.all('DrugCategoryValue').value = EvaluateGrid.getRowColData(checkFlag - 1, 26);
		fm.all('CostLevelValue').value = EvaluateGrid.getRowColData(checkFlag - 1, 27);
		fm.all('OutpatientLimitValue').value = EvaluateGrid.getRowColData(checkFlag - 1, 28);
		fm.all('HospitalLimitValue').value = EvaluateGrid.getRowColData(checkFlag - 1, 29);
		fm.all('InjuriesMarkValue').value = EvaluateGrid.getRowColData(checkFlag - 1, 30);
		fm.all('FertilityMarkValue').value = EvaluateGrid.getRowColData(checkFlag - 1, 31);
		fm.all('MngCom').value = EvaluateGrid.getRowColData(checkFlag - 1, 32);
		fm.all('AreaCode').value = EvaluateGrid.getRowColData(checkFlag - 1, 33);
	}
}

//增删改查后页面自动刷新
function queryAll()
{  
	var tSql = "select DrugCode,(select codename from LDCode where '1423476973000'='1423476973000' and  codetype = 'drugcategory' and code = ll.DrugCategory )DrugCategory, " +
		"DrugGenericName, DrugTradeName,DrugEnglishName,DrugClassification,SecondCategory,Remark,Formulations,Specifications,Phonetic,Manufacturer,PackageMessage," +
		"Administration,HealthNo,(select codename from LDCode where  codetype = 'costlevel' and code = ll.CostLevel ) CostLevel,PaymentScope," +
		"(select codename from LDCode where  codetype = 'yesno' and code = ll.OutpatientLimit ) OutpatientLimit," +
		"(select codename from LDCode where  codetype = 'yesno' and code = ll.HospitalLimit ) HospitalLimit," +
		"(select codename from LDCode where  codetype = 'yesno' and code = ll.InjuriesMark ) InjuriesMark," +
		"(select codename from LDCode where  codetype = 'yesno' and code = ll.FertilityMark ) FertilityMark,PriceCeiling," +
		"(select name from ldcom where comcode = ll.MngCom) MngCom," +
		"(select codename from LDCode where codetype = 'hmareacode' and code = ll.AreaCode) AreaCode," +
		"SerialNo,DrugCategory,CostLevel,OutpatientLimit,HospitalLimit,InjuriesMark,FertilityMark,MngCom,AreaCode from LLCaseDrugInfo ll where 1 = 1 "
		+ getWherePart('DrugCode', 'DrugCode') 
		+ getWherePart('DrugCategory', 'DrugCategoryValue') 
		+ getWherePart('DrugGenericName', 'DrugGenericName')
		" order by SerialNo";
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
	turnPage.queryModal(tSql, EvaluateGrid);
}

//点击”查询“按钮时的操作
function queryClick()
{  
	if("" == (fm.all('DrugCode').value) && "" == (fm.all('DrugCategory').value) && "" == (fm.all('DrugGenericName').value))
	{
		alert("请录入‘药品编码’或‘药品类别’或‘药品通用名称’进行查询");
		return false;
	}
	else
	{
		var tSql = "select DrugCode,(select codename from LDCode where '1423476973000'='1423476973000' and  codetype = 'drugcategory' and code = ll.DrugCategory )DrugCategory, " +
				"DrugGenericName, DrugTradeName,DrugEnglishName,DrugClassification,SecondCategory,Remark,Formulations,Specifications,Phonetic,Manufacturer,PackageMessage," +
				"Administration,HealthNo,(select codename from LDCode where  codetype = 'costlevel' and code = ll.CostLevel ) CostLevel,PaymentScope," +
				"(select codename from LDCode where  codetype = 'yesno' and code = ll.OutpatientLimit ) OutpatientLimit," +
				"(select codename from LDCode where  codetype = 'yesno' and code = ll.HospitalLimit ) HospitalLimit," +
				"(select codename from LDCode where  codetype = 'yesno' and code = ll.InjuriesMark ) InjuriesMark," +
				"(select codename from LDCode where  codetype = 'yesno' and code = ll.FertilityMark ) FertilityMark,PriceCeiling," +
				"(select name from ldcom where comcode = ll.MngCom) MngCom," +
				"(select codename from LDCode where codetype = 'hmareacode' and code = ll.AreaCode) AreaCode," +
				"SerialNo,DrugCategory,CostLevel,OutpatientLimit,HospitalLimit,InjuriesMark,FertilityMark,MngCom,AreaCode from LLCaseDrugInfo ll where 1 = 1 "
				+ getWherePart('DrugCode', 'DrugCode') 
				+ getWherePart('DrugCategory', 'DrugCategoryValue') 
				+ getWherePart('DrugGenericName', 'DrugGenericName')
				" order by SerialNo";
		//执行查询并返回结果
		var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
		if(!strQueryResult)
		{  alert("没有符合条件的信息！"); 
			return false;
		}
	    else turnPage.queryModal(tSql, EvaluateGrid);
	}
}

//保存
function submitForm()
{
	if (verifyInput() == false)
	    return false;
	fm.all('Transact').value ="INSERT";	
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	mAction = "INSERT";
	fm.submit(); //提交 
}

//***************************************************
//* 点击“删除”进行的操作
//***************************************************
function deleteClick()
{
	var checkFlag = 0;
	for (i=0; i<EvaluateGrid.mulLineCount; i++)
	{
		if (EvaluateGrid.getSelNo(i))
		{
			checkFlag = EvaluateGrid.getSelNo();
			break;
		}
	}
	if(checkFlag){
		var tSerialNo = EvaluateGrid.getRowColData(checkFlag - 1, 25);
		if(tSerialNo== null || tSerialNo == ""){
			alert("删除时，获取药品信息失败！");
			return false;
		}
		if (confirm("您确实想删除该记录吗?"))
		{
			var i = 0;
			var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.all('Transact').value ="DELETE";	
			fm.action = "./LLDrugInfoSave.jsp?SerialNo="+tSerialNo;
			fm.submit(); //提交
		}
		else
		{
			alert("您取消了删除操作！");
		}
	}else{
		alert("请选择需要删除的药品信息！");
		return false;
	}
}

//Click事件，当点击“修改”时触发该函数
function updateClick()
{
	if (verifyInput() == false)
	    return false;
	var checkFlag = 0;
	for (i=0; i<EvaluateGrid.mulLineCount; i++)
	{
		if (EvaluateGrid.getSelNo(i))
	    {
	      checkFlag = EvaluateGrid.getSelNo();
	      break;
	    }
	}
	if(checkFlag){
		var tSerialNo = EvaluateGrid.getRowColData(checkFlag - 1, 25);
		if(tSerialNo== null || tSerialNo == ""){
			alert("修改时，获取药品信息失败！");
			return false;
		}
		if (confirm("您确实想修改该记录吗?"))
		{
			var i = 0;
			var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.all('Transact').value ="UPDATE";	
			fm.action = "./LLDrugInfoSave.jsp?SerialNo="+tSerialNo;
			fm.submit(); //提交
		}
		else
		{
			alert("您取消了修改操作！");
		}
	}else{
		alert("请选择需要修改的药品信息！");
	  	return false;
	}
	return true;
}
	
//药品信息导入
function DrugInfoUpload()
{
  getImportPath();
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fmlode.action = "./LLDrugInfoImport.jsp?ImportPath="+ImportPath;
  fmlode.submit(); //提交
  
}

function getImportPath () {
	  var strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	  //判断是否查询成功
	  if (!turnPage.strQueryResult) {
	    alert("未找到上传路径");
	    return;
	  }
	  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	  //查询成功则拆分字符串，返回二维数组
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	  ImportPath = turnPage.arrDataCacheSet[0][0];
}

//模板下载
function downLoad() {
	var formAction = fm.action;
    fm.action = "LLDrugInfoTemplateDownLoad.jsp";
    fm.submit();
    fm.target = "fraSubmit";
    fm.action = formAction;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //初始化
    //initForm();
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    queryAll();
  }
}



