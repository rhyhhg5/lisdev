//该文件中包含客户端需要处理的函数和事件

//程序名称：LLUnderwrit.js
//程序功能：理赔二核送核
//创建日期：2013-11-26
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var flag = true;  //add by Houyd 20140402 只有个单可以提起理赔二核

//查询保单信息
function queryContDetail(){
	
	var tInsuredNo = fm.CustomerNo.value;
	var tContQuery = "select a.contno,(select codename from ldcode where codetype='stateflag' and code=a.stateflag), " +
			"a.cvalidate,a.cinvalidate,b.riskcode,(select riskname from lmriskapp where riskcode=b.riskcode) " +
			"from lccont a,lcpol b,lcinsured c where a.contno=b.contno and b.insuredno=c.insuredno and a.conttype='1' and a.contno=c.contno and c.insuredno='"+tInsuredNo+"'";
			
	var tContAry = easyExecSql(tContQuery);
	if(!tContAry){
		flag = false;
		alert("保单信息查询失败");
	}
		                              
	turnPage.queryModal(tContQuery, ContDetailGrid);
}
//初始化二核信息
function queryUnderWrit(){
	
	var tInsuredNo = fm.CustomerNo.value;
	var tUnderWritQuery = "select distinct c.missionprop1,d.riskcode ,(SELECT riskname FROM lmriskapp where riskcode = d.riskcode) ," +
			"c.missionprop6,c.missionprop13,(select username from llclaimuser where usercode=c.missionprop13)," +
			"case when b.PassFlag='1' then '标准承保' when b.PassFlag='2' then '变更承保' when b.PassFlag='3' then '拒保' end, "+
			"(select givetypedesc from llclaimdetail where caseno=c.missionprop1 and polno=b.polno fetch first 1 rows only),b.polno " +
			"from lpuwmaster b,lbmission c,lcpol d where c.missionprop2 = '"+tInsuredNo+"' " +
			"and c.missionprop10=b.edorno " +
			"and b.polno = d.polno and b.contno = d.contno";
			
	var tArry = easyExecSql(tUnderWritQuery);
	if(!tArry){
		alert("该客户无既往二核记录");
	}
	turnPage2.queryModal(tUnderWritQuery, UnderWritGrid);
	
}
//单选中记录后，查看二核结论
function queryPassFlag(){
	//初始化
	fm.all('UnderWritComment').value = "";
	fm.all('Remark').value = "";
	var tUnderWritLine=UnderWritGrid.getSelNo ();//得到被选中的行号
	var tCaseNo =UnderWritGrid.getRowColData(tUnderWritLine-1,1);//被选中行的案件号
	var tPolNo =UnderWritGrid.getRowColData(tUnderWritLine-1,9);//被选中行的保单险种号
	var tPassFlagQuery = "select lpm.uwidea,lwm.missionprop12 from lpuwmaster lpm,lbmission lwm where lpm.edorno = lwm.missionprop10 and lwm.missionprop1 = '"+tCaseNo+"' and lpm.polno='"+tPolNo+"'";
	var tArry = easyExecSql(tPassFlagQuery);
	if(tArry)
		{ 
			fm.all('UnderWritComment').value = tArry[0][0];
			fm.all('Remark').value = tArry[0][1];
			document.getElementById('Remark').setAttribute('readOnly',true);
					
		}
	
	
	
	
}

//提起二核按钮
function submitUnderWrit()
{	
	var tRgtNo = fm.RgtNo.value;
	var tCaseNo = fm.CaseNo.value;
	if(tCaseNo.substring(0,1) == 'P'){
		alert("团体批次下不可提起二核！");
		return false;
	}
	
	var tCheckUWSql = "select 1 from lbmission where missionprop1='"+tCaseNo+"'"
	var tCheckUWResult = easyExecSql(tCheckUWSql);
	if(tCheckUWResult != null){
		alert("同一案件不可以多次提起二核！")
		return false;
	}
	
	var tRgtStateSql = "select rgtstate from llcase where caseno='"+tCaseNo+"'";
	var tRgtState = easyExecSql(tRgtStateSql);
	if(tRgtState != null){
		if(tRgtState[0][0] == '16'){
		
			alert("案件已处于二核状态，不可重复提起二核");
			return false;
		}
		if(tRgtState[0][0] == '07' ){
		
			alert("案件处于调查状态，不可提起二核");
			return false;
		}
		if(tRgtState[0][0] == '09' ){
		
			alert("案件处于结案状态，不可提起二核");
			return false;
		}
		if(tRgtState[0][0] == '13'){
			
			alert("案件处于延迟状态，不可提起二核");
			return false;
		}
		if(tRgtState[0][0] == '11'){
		
			alert("案件处于通知状态，不可提起二核");
			return false;
		}
		if(tRgtState[0][0] == '14'){
		
			alert("案件处于撤件状态，不可提起二核");
			return false;
		}
		if(tRgtState[0][0] == '12'){
		
			alert("案件处于给付状态，不可提起二核");
			return false;
		}
		
	}
	if(flag){
	    if (!confirm("确认提交理赔二核？"))
        {
          return false;
        }
     }else{
     	alert("该被保险人无个单保单，不可提起二核");
     	return false;
     }
	fm.fmtransact.value = "SAVE";
	var showStr="正在执行操作，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action = "./LLUnderWritSave.jsp";
    fm.submit(); //提交
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

