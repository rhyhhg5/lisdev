<%
//程序名称：LLHospGetDealSave.jsp
//程序功能：
//创建日期：2009-12-23
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LLHospCaseSet tLLHospCaseSet = new LLHospCaseSet();
  
  LLHospGetSchema tLLHospGetSchema = new LLHospGetSchema();
	
  LLHospGetDealBL tLLHospGetDealBL = new LLHospGetDealBL();
  TransferData tTransferData = new TransferData();
  //输出参数
  CErrors tError = null;
  String tOperate = request.getParameter("Operate");
  tOperate = tOperate.trim();
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  String tHCNo = request.getParameter("HCNo");
  
  tLLHospGetSchema.setHCNo(tHCNo);
  tLLHospGetSchema.setPayMode(request.getParameter("paymode"));
  tLLHospGetSchema.setBankCode(request.getParameter("BankCode"));
  tLLHospGetSchema.setBankAccNo(request.getParameter("BankAccNo"));
  tLLHospGetSchema.setAccName(request.getParameter("AccName"));
  tLLHospGetSchema.setDrawer(request.getParameter("HospitalName"));
  
  if ("INSERT".equals(tOperate)) {
    String tChk[] = request.getParameterValues("InpUnDoCaseInfoChk");
    String tCaseNo[] = request.getParameterValues("UnDoCaseInfo4");
    for (int index=0 ; index<tChk.length ; index++) {
       System.out.println(tChk[index]);
       if(tChk[index].equals("1")) {
        System.out.println(tCaseNo[index]);
        LLHospCaseSchema tLLHospCaseSchema = new LLHospCaseSchema();
        tLLHospCaseSchema.setCaseNo(tCaseNo[index]);
        tLLHospCaseSet.add(tLLHospCaseSchema);
       }
    }
  }
  
  if ("ALLINSERT".equals(tOperate)) {	  	  
	  tTransferData.setNameAndValue("CaseNo",request.getParameter("CaseNo"));
	  tTransferData.setNameAndValue("CustomerNo",request.getParameter("CustomerNo"));
	  tTransferData.setNameAndValue("CustomerName",request.getParameter("CustomerName"));
	  tTransferData.setNameAndValue("EndCaseDataS",request.getParameter("EndCaseDataS"));
	  tTransferData.setNameAndValue("EndCaseDataE",request.getParameter("EndCaseDataE"));
	  tTransferData.setNameAndValue("HospitalCode",request.getParameter("HospitalCode"));
	  System.out.print(request.getParameter("HospitalCode")+"=============HospitalCode");
	}
	  
  
  if ("DELETE".equals(tOperate)) {
    String tChk[] = request.getParameterValues("InpDoCaseInfoChk");
    String tCaseNo[] = request.getParameterValues("DoCaseInfo4");
    for (int index=0 ; index<tChk.length ; index++) {
       System.out.println(tChk[index]);
       if(tChk[index].equals("1")) {
        System.out.println(tCaseNo[index]);
        LLHospCaseSchema tLLHospCaseSchema = new LLHospCaseSchema();
        tLLHospCaseSchema.setCaseNo(tCaseNo[index]);
        tLLHospCaseSet.add(tLLHospCaseSchema);
       }
    }
  }
  
  if ("CONFIRM".equals(tOperate)) {
      tTransferData.setNameAndValue("PrePaidFlag",request.getParameter("PrePaidFlag")); //回销预付赔款 1:回销 其他值不回销
      //System.out.println("==========="+request.getParameter("PrePaidFlag"));
  }
  
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLLHospCaseSet);	
	tVData.addElement(tLLHospGetSchema);
	tVData.addElement(tTransferData);
	tVData.addElement(tG);
  try
  {
    tLLHospGetDealBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLLHospGetDealBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	//Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
%>     
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>