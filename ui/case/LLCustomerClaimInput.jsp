<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LLCustomerClaimInput.jsp
//程序功能：F1报表生成
//创建日期：2005-04-16
//创建人  ：St.GN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
 
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LLCustomerClaimInput.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

</head>
<body>    
  <form  method=post name=fm target="f1print">
    
    <table class= common border=0 width=100%>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input> <Input class="codeno" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName></TD>          
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        </TR>
    </table>
    <hr>
		<br>
    <input type="hidden" name=op value="">
    <table class=common>
    	<tr>
					<TD  class= input8> <input type=button class=cssButton value="不同职业理赔统计" style="width:200px" onclick="Ocupation()"></TD>
					<TD  class= input8> <input type=button class=cssButton value="不同行业理赔统计" style="width:200px" onclick="TradeCase()"></TD>
					<!--TD  class= input8> <input type=button class=cssButton value="不同收入理赔统计" style="width:200px" onclick="WageCase()"></TD>
					
			</tr>
			<tr></tr><tr></tr><tr></tr>
			<tr-->
					<TD  class= input8> <input type=button class=cssButton value="不同保障方式理赔统计" style="width:78%" onclick="InsureCase()"></TD>
					<!--TD  class= input8> <input type=button class=cssButton value="不同保障级别理赔统计" style="width:78%" onclick="Mult()"></TD>
					<TD  class= input8> <input type=button class=cssButton value="不同投保年限理赔统计" style="width:78%" onclick="InsureYear()"></TD-->				
			</tr>
		</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>