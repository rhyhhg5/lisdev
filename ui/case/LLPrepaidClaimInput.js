//该文件中包含客户端需要处理的函数和事件

//程序名称：LLPrepaidClaimInput.js
//程序功能：预付赔款录入
//创建日期：2010-11-25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();



//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
 if(showInfo!=null)
 {
   try
   {
     showInfo.focus();  
   }
   catch(ex)
   {
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.GrpContConfirm.disabled=false;
  fm.GrpContCancel.disabled=false;
  fm.Apply.disabled=false;
  fm.Cancel.disabled=false;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var tSQL="select dealer from llprepaidclaim where prepaidno='"+ fm.all("PrepaidNo").value +"'";
    var arrResult = easyExecSql(tSQL);
    if(arrResult && fm.all('fmtransact').value=="Prepaid||Apply")
    {
    	alert(content+"\n 已报上级"+arrResult[0][0]+"审批!");  
    }else{
     var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
     showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    }
    //执行下一步操作
    if (fm.all('fmtransact').value=="GrpCont||Confirm" || fm.all('fmtransact').value=="GrpCont||Cancel") {
        afterGrpContDeal();
    }
    
    if (fm.all('fmtransact').value=="Prepaid||Apply" || fm.all('fmtransact').value=="Prepaid||Cancel") {
        searchPrepaid();
    }
  }
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//根据投保单位名称、团体保单号查询保单
function searchGrpCont()
{
  if (fm.ManageCom.value==null || fm.ManageCom.value=="") {
      alert("请选择管理机构");
      return false;
  }
  
  if ((fm.SGrpName.value==null || fm.SGrpName.value=="")
      && (fm.SGrpContNo.value==null || fm.SGrpContNo.value=="")) {
      alert("请录入投保单位名称或团体保单号");
      return false;
  }
  
  var GrpContSQL = " SELECT a.managecom, a.grpcontno, a.grpname, a.cvalidate, a.cinvalidate, "
                 + " b.state, "
                 + " (CASE when b.state='0' THEN '撤销' WHEN b.state='1' THEN '有预付' ELSE '没有预付' END ), "
                 + " nvl(b.applyamount,0), nvl(b.prepaidbala,0) "
                 + " FROM lcgrpcont a LEFT JOIN llprepaidgrpcont b ON a.grpcontno=b.grpcontno "
                 + " WHERE a.managecom LIKE '" + fm.ManageCom.value + "%'"
                 + " AND a.appflag='1' and a.stateflag in ('1','2','3')"
                 + getWherePart("a.grpname","SGrpName")
                 + getWherePart("a.grpcontno","SGrpContNo")
                 + " order by a.grpcontno ";
  turnPage.queryModal(GrpContSQL,GrpContGrid);
}

//选中后保单后操作
function onSelGrpInfo()
{
  var tSelNo=GrpContGrid.getSelNo();
  var PrepaidState = GrpContGrid.getRowColDataByName(tSelNo-1,"state");
  
  if (PrepaidState=="1") {
      fm.GrpContConfirm.disabled=true;
      showDiv(divLLPrepaidInput,"true");  
      var GrpContNo = GrpContGrid.getRowColDataByName(tSelNo-1,"grpcontno");
      searchGrpInfo(GrpContNo);
  } else {
      fm.GrpContConfirm.disabled=false;
      showDiv(divLLPrepaidInput,"false");
  }
}

function GrpContDeal(DealType)
{
  var tSelNo = GrpContGrid.getSelNo();
  if (tSelNo == 0) {
	  alert("请选择保单");
	  return false;
  }
  
  var PrepaidState = GrpContGrid.getRowColDataByName(tSelNo-1,"state");

  if (DealType=="Confirm") {
      fm.PrepaidNo.value ="";
      fm.all('fmtransact').value="GrpCont||Confirm";
      if (PrepaidState=="1") {
          alert("保单已经预付确认，可直接申请");
          return false;
      }
      fm.GrpContConfirm.disabled=true;
  } else if (DealType=="Cancel") {
      fm.PrepaidNo.value ="";
      fm.all('fmtransact').value="GrpCont||Cancel";
      if (PrepaidState=="0") {
          alert("保单已经预付撤销");
          return false;
      }
      fm.GrpContCancel.disabled=true;
  } else {
      return false;
  }

  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLPrepaidGrpContInputSave.jsp";
  fm.target = "fraSubmit";
  fm.GrpContNo.value = GrpContGrid.getRowColDataByName(tSelNo-1,"grpcontno");
  fm.submit();
}

function afterGrpContDeal() 
{
  searchGrpCont();
  if (fm.all('fmtransact').value=="GrpCont||Confirm") {
      showDiv(divLLPrepaidInput,"true");
      if (fm.GrpContNo.value!="") {
          searchGrpInfo(fm.GrpContNo.value);
      }
  } else {
      showDiv(divLLPrepaidInput,"false");
  }

}

//查询团体保单信息-团体理赔账户、实收保费
function searchGrpInfo(GrpContNo) {
  if (GrpContNo!=null && GrpContNo!="") {
      
      fm.PrepaidNo.value = "";
      fm.PrepaidState.value = "";
      fm.BankCode.value = "";
      fm.BankName.value = "";
      fm.SendFlag.value = "";
      fm.AccNo.value = "";
      fm.AccName.value = "";
      fm.GrpContNo.value = "";
      fm.GrpName.value = "";
      fm.SumActuPay.value = "";
      fm.PayMode.value = "";
      fm.PayModeName.value = "";
      
      var ClaimAccSQL = " SELECT a.claimbankcode, b.bankname, "
                      + " (CASE WHEN b.cansendflag='1' then '是' else '否' END), "
                      + " a.claimbankaccno, a.claimaccname, a.grpcontno, c.grpname, "
                      + " decimal((NVL((SELECT cast(SUM(sumactupaymoney) as decimal(12,2)) FROM ljapaygrp WHERE endorsementno IS NULL "
                      + " AND grpcontno=c.grpcontno and paytype<>'YF'),0) + "
                      + " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=c.grpcontno),0)),12,2) "
                      + " FROM lcgrpcont c, lcgrpappnt a LEFT JOIN ldbank b ON a.claimbankcode = b.bankcode "
                      + " WHERE a.grpcontno=c.grpcontno and c.grpcontno='"+GrpContNo+"'";
      var AccArr = easyExecSql(ClaimAccSQL);
      
      if (AccArr) {
          fm.BankCode.value = AccArr[0][0];
          fm.BankName.value = AccArr[0][1];
          fm.SendFlag.value = AccArr[0][2];
          fm.AccNo.value = AccArr[0][3];
          fm.AccName.value = AccArr[0][4];
          fm.GrpContNo.value = AccArr[0][5];
          fm.GrpName.value = AccArr[0][6];
          fm.SumActuPay.value = AccArr[0][7];
      }
      
      var GrpPolSQL = " SELECT a.riskcode, b.riskname, "
                    + " decimal(NVL((SELECT SUM(sumactupaymoney) FROM ljapaygrp WHERE endorsementno IS NULL "
                    + " AND grpcontno=a.grpcontno AND grppolno=a.grppolno and paytype<>'YF'),0) + "
                    + " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno AND grppolno=a.grppolno),0),12,2), "
                    + " c.applyamount, "
                    + " (SELECT NVL(SUM(n.realpay),0) FROM llcase m,llclaimdetail n WHERE m.caseno=n.caseno "
                    + " AND m.rgtstate in ('09','11','12') AND n.grpcontno=a.grpcontno AND n.grppolno=a.grppolno), "
                    + " '', a.grppolno "
                    + " FROM lcgrppol a, lmrisk b, llprepaidgrppol c"
                    + " WHERE a.grpcontno=c.grpcontno AND a.grppolno=c.grppolno AND a.riskcode=b.riskcode "
                    + " AND a.grpcontno='"+GrpContNo+"'"
                    + " order by a.riskcode";
      turnPage1.queryModal(GrpPolSQL,PrepaidClaimGrid);
  }
}

//预付赔款申请
function submitForm()
{
  if (!verifyInput2()) {
      return false;
  }

  var ClaimCount=PrepaidClaimGrid.mulLineCount;
  var chkFlag = false;
  for (var i=0; i<ClaimCount; i++){
    if(PrepaidClaimGrid.getChkNo(i)){
      var money = PrepaidClaimGrid.getRowColDataByName(i,"money");
      var pay = PrepaidClaimGrid.getRowColDataByName(i,"pay");
      var grppolno = PrepaidClaimGrid.getRowColDataByName(i,"grppolno");
      var riskcode = PrepaidClaimGrid.getRowColDataByName(i,"riskcode");
      if (money == null || money == "") {
          alert("本次预付金额不能为空");
          PrepaidClaimGrid.setFocus(i,6);
          return false;
      }
      
      if (!isNumeric(money)) {
          alert("本次预付金额必须为数字");
          PrepaidClaimGrid.setFocus(i,6);
          return false;
      }
            
      if (money<=0) {
          alert("本次预付金额必须大于0");
          PrepaidClaimGrid.setFocus(i,6);
          return false;
      }
      

	  var checkSQL = " select " + pay + "-" + money
					+ " - a.PrepaidBala -" 
					+ " nvl((select sum(pay) from ljagetclaim where grppolno=a.grppolno and othernotype='5'),0) " 
					+ " from LLPrepaidGrpPol a where a.grppolno='"
					+ grppolno + "'";
	  var balaArr = easyExecSql(checkSQL);
	  
	  if (balaArr) {
		  if (balaArr[0][0]<0) {
			  if(!confirm("险种"+riskcode+"本次预付金额超过实收保费扣除 该险种理赔结案通知赔款与未回销预付赔款 之和，是否继续？")){
			      return false;
			  }		  
		  }		  
	  } else {
		  alert("险种"+riskcode+"预付信息查询失败，请与开发人员联系");
		  return false;
	  }
	  
      chkFlag = true;
    }
  }
  
  if (chkFlag == false){
    alert("请选择预付赔款险种");
    return false;
  }
  
  fm.Apply.disabled=true;
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.all('fmtransact').value="Prepaid||Apply";
  fm.action = "./LLPrepaidClaimInputSave.jsp";
  fm.target = "fraSubmit";
  fm.submit();
}

// 查询申请的预付赔款信息
function searchPrepaid()
{
  if (fm.PrepaidNo.value==null || fm.PrepaidNo.value=="") {
      return false;
  }
  
  var tPrepaidSQL = " SELECT a.prepaidno, CODENAME('llprepaidstate',a.rgtstate), "
                  + " a.grpname, a.grpcontno, "
                  + " decimal((NVL((SELECT cast(SUM(sumactupaymoney) as decimal(12,2)) FROM ljapaygrp WHERE endorsementno IS NULL "
                  + " AND grpcontno=a.grpcontno and paytype<>'YF'),0) + "
                  + " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno),0)),12,2), "
                  + " a.bankcode, "
                  + " (SELECT bankname FROM ldbank where bankcode=a.bankcode), "
                  + " (SELECT CASE WHEN cansendflag='1' THEN '是' ELSE '否' END FROM ldbank where bankcode=a.bankcode), "
                  + " a.bankaccno, a.accname, a.paymode, CODENAME('paymode',a.paymode) "
                  + " FROM llprepaidclaim a "
                  + " WHERE a.rgttype='1' AND a.prepaidno='"+fm.PrepaidNo.value+"'";
  var PrepaidArr = easyExecSql(tPrepaidSQL);
  
  if (PrepaidArr) {
      fm.PrepaidState.value = PrepaidArr[0][1];
      fm.GrpName.value = PrepaidArr[0][2];
      fm.GrpContNo.value = PrepaidArr[0][3];
      fm.SumActuPay.value = PrepaidArr[0][4];
      fm.BankCode.value = PrepaidArr[0][5];
      fm.BankName.value = PrepaidArr[0][6];
      fm.SendFlag.value = PrepaidArr[0][7];
      fm.AccNo.value = PrepaidArr[0][8];
      fm.AccName.value = PrepaidArr[0][9];
      fm.PayMode.value = PrepaidArr[0][10];
      fm.PayModeName.value = PrepaidArr[0][11];
  }
  
  var tPrepaidDetail = " SELECT a.riskcode, b.riskname, "
                    + " decimal((NVL((SELECT cast(SUM(sumactupaymoney) as decimal(12,2)) FROM ljapaygrp WHERE endorsementno IS NULL "
                    + " AND grpcontno=a.grpcontno AND grppolno=a.grppolno and paytype<>'YF'),0) + "
                    + " NVL((SELECT SUM(getmoney) FROM ljagetendorse WHERE grpcontno=a.grpcontno AND grppolno=a.grppolno),0)),12,2), "
                    + " c.applyamount, "
                    + " (SELECT NVL(SUM(n.realpay),0) FROM llcase m,llclaimdetail n WHERE m.caseno=n.caseno "
                    + " AND m.rgtstate in ('09','11','12') AND n.grpcontno=a.grpcontno AND n.grppolno=a.grppolno), "
                    + " a.money, a.grppolno "
                    + " FROM llprepaidclaimdetail a, lmrisk b, llprepaidgrppol c, llprepaidclaim d"
                    + " WHERE a.grpcontno=c.grpcontno AND a.grppolno=c.grppolno AND a.riskcode=b.riskcode "
                    + " AND a.prepaidno=d.prepaidno AND d.rgttype='1' "
                    + " AND a.prepaidno='"+fm.PrepaidNo.value+"'"
                    + " order by a.riskcode";
  turnPage1.queryModal(tPrepaidDetail,PrepaidClaimGrid);
  
  var ClaimCount=PrepaidClaimGrid.mulLineCount;
  for (var i=1; i<=ClaimCount; i++){
       PrepaidClaimGrid.checkBoxSel(i);
  }  
}

// 预付赔款撤销
function cancel() 
{
  if (fm.PrepaidNo.value==null || fm.PrepaidNo.value=="") {
      alert("请输入预付赔款号");  
      return false;
  }
  
  fm.Cancel.disabled=true;
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.all('fmtransact').value="Prepaid||Cancel";
  fm.action = "./LLPrepaidClaimInputSave.jsp";
  fm.target = "fraSubmit";
  fm.submit();
}
