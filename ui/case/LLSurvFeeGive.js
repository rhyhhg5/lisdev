//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage=new turnPageClass();
var turnPage2=new turnPageClass();
//给付确认
function submitForm()
{	
	var comlimit = "";
	var timelimit = "";
	if(fm.comcode.value.length==2)
		comlimit="000000";
	if(fm.comcode.value.length==4)
		comlimit=fm.comcode.value.substring(2)+"0000";
	if(fm.comcode.value.length==8)
		comlimit=fm.comcode.value.substring(2);
	if(fm.Month.value.length==1){
		timelimit = fm.Year.value+"0"+fm.Month.value;
	}
	else{
		timelimit = fm.Year.value+fm.Month.value;
	}
	fm.BatchNo.value = comlimit+timelimit;

	for(i = 0; i <IndirectFeeGrid.mulLineCount; i++)
	{		
		if(IndirectFeeGrid.getChkNo(i))
		{
			var tcomlimit="";
			var ttimelimit="";
			if(fm.comcode.value.length==2)
			tcomlimit=fm.comcode.value+"000000";
			if(fm.comcode.value.length==4)
			tcomlimit=fm.comcode.value+"0000";
			if(fm.comcode.value.length==8)
			tcomlimit=fm.comcode.value;
			if(fm.Month.value.length==1)
			ttimelimit = "0"+fm.Month.value;
			var strSql="select count(1) from ljagetclaim a where a.feefinatype in ('JJ') "
			 +" and exists (select 1 from ljaget where actugetno=a.actugetno and substr(otherno,1,12)='"+tcomlimit+fm.Year.value.substring(2,4)+ttimelimit+"')";
			if(easyExecSql(strSql)>=1)
			{
				alert("该月间接调查费已经结算一次，不能进行第二次结算!");
				return false;
			}
		}		
	}
//	var temsql = "select actugetno from ljaget where otherno in ('C"+fm.BatchNo.value+"','F"+fm.BatchNo.value+"')";
//	var arr = easyExecSql(temsql);
//	if(arr){
//		alert("本月的查勘费用已结，您确认要修改吗？");
//		return false;
//	}
	fm.fmtransact.value = "GIVE||ENSURE";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./LLSurvFeeGiveSave.jsp";
	fm.all("GiveConfirm").disabled = true;
	fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
	showInfo.close();
	if (FlagStr == "Fail" ){
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	fm.all("GiveConfirm").disabled = false;
	queryInqFee();
	queryIndFee();
	queryGiveFee();
}

function afterCodeSelect(ObjType,Obj)
{
	queryInqFee();
	queryIndFee();
	queryGiveFee();
}
//查询直接和间接调查费
function queryAllFee()
{
	queryInqFee();
	queryIndFee();
	queryGiveFee();
}
//查询既往结算的明细信息
function queryGiveInfo()
{
	initInqFeeGrid();
	initIndirectFeeGrid();
	var selno = GiveFeeGrid.getSelNo()-1;
	var OtherNo="";
	if (selno >= 0)
	{
		OtherNo = GiveFeeGrid.getRowColData(selno,1);	//结算号
		var inqfeeSql="select distinct a.otherno,substr(a.surveyno,18,1),a.inqfee,a.inputer,"
				+" (select confer from llsurvey where surveyno=a.surveyno),a.surveyno "
				+" from llinqfeesta a,ljagetclaim b,ljaget c "
				+" where a.otherno =b.otherno and c.actugetno=b.actugetno"
				+" and c.otherno='"+OtherNo+"' and a.inqfee<>0 and a.payed='1'";
		turnPage.queryModal(inqfeeSql,InqFeeGrid);
		
		var indfeeSql = "select distinct a.otherno,a.feesum,a.operator,a.operator "
				+" from llcaseindfee a,ljagetclaim b,ljaget c"
			 	+" where a.otherno =b.otherno and c.actugetno=b.actugetno"
				+" and c.otherno='"+OtherNo+"' and a.feesum<>0 and a.payed='1'";
		turnPage.queryModal(indfeeSql,IndirectFeeGrid);
				
    }	

}
//查询既往结算的调查费
function queryGiveFee()
{
	if(fm.Month.value!=null && fm.Month.value!=""&&fm.Year.value!=null && fm.Year.value!=""){
		month=fm.Month.value/1;
		year= fm.Year.value/1;
		days=getDays(month, year);
		fm.MStartDate.value = year+"-"+month+"-"+"01";
		fm.MEndDate.value = year+"-"+month+"-"+days;
		}else if(fm.Year.value!=null && fm.Year.value!=""){
			year= fm.Year.value/1;
			fm.MStartDate.value = year+"-"+"01"+"-"+"01";
			fm.MEndDate.value = year+"-"+"12"+"-"+"31";
		}
		else{
			fm.MStartDate.value = "";
			fm.MEndDate.value = "";
		}
		var strSql="select a.otherno,a.makedate,sum(b.pay),b.OPConfirmCode,a.confdate "
		+" from ljaget a,ljagetclaim b"
		+" where a.actugetno=b.actugetno and a.othernotype in ('C','F')"
		+getWherePart("a.makedate","MEndDate","<=")
		+getWherePart("a.makedate","MStartDate",">=")
		+getWherePart("a.managecom","comcode","like");
		if(fm.CaseNo.value!=null&&fm.CaseNo.value!="")
		{
			strSql +=" and b.otherno='"+fm.CaseNo.value+"'";
		}
		strSql +=" group by a.otherno,a.makedate,b.OPConfirmCode,a.confdate "
		turnPage.queryModal(strSql,GiveFeeGrid);
}
//直接调查费
function queryInqFee()
{

	if(fm.Month.value!=null && fm.Month.value!=""&&fm.Year.value!=null && fm.Year.value!=""){
	month=fm.Month.value/1;
	year= fm.Year.value/1;
	days=getDays(month, year);
	fm.MStartDate.value = year+"-"+month+"-"+"01";
	fm.MEndDate.value = year+"-"+month+"-"+days;
	}else if(fm.Year.value!=null && fm.Year.value!=""){
		year= fm.Year.value/1;
		fm.MStartDate.value = year+"-"+"01"+"-"+"01";
		fm.MEndDate.value = year+"-"+"12"+"-"+"31";
	}
	else{
		fm.MStartDate.value = "";
		fm.MEndDate.value = "";
	}
	
	strSql0 = "select a.otherno,substr(a.surveyno,18,1),a.inqfee,"
			+ "a.inputer,a.confer,a.surveyno from llinqfeesta a,";
	strSqlpart1 = "llcase b where a.otherno =b.caseno and b.rgtstate in ('11','12','14') "
				+" and exists (select 1 from LLInqFee where SurveyNo=a.SurveyNo and UWState='1')";
	strSqlpart2 = "llconsult b where a.otherno = b.consultno "
				+" and exists (select 1 from LLInqFee where SurveyNo=a.SurveyNo and UWState='1')";
	strSql1 = " and a.payed is null and inqfee>0 "
	+getWherePart("a.inqdept","comcode","like")
	+getWherePart("a.otherno","CaseNo")
	+getWherePart("a.confdate","MEndDate","<=")
	+getWherePart("a.confdate","MStartDate",">=");
	strSql = strSql0 + strSqlpart1+strSql1 + " union " + strSql0 + strSqlpart2+strSql1;
    turnPage.pageLineNum = 100;
	turnPage.queryModal(strSql,InqFeeGrid);
}
//间接调查费
function queryIndFee()
{
	if(fm.Month.value!=null && fm.Month.value!=""&&fm.Year.value!=null && fm.Year.value!=""){
		month=fm.Month.value/1;
		
		year= fm.Year.value/1;
		days=getDays(month, year);
		if(fm.Month.value.length==1){
			month = "0"+month;
		}
		fm.MEndDate.value = year+"-"+month+"-"+days;
		}else if(fm.Year.value!=null && fm.Year.value!=""){
			year= fm.Year.value/1;
			fm.MEndDate.value = year;
		}
		else{
			fm.MEndDate.value = "";
		}
		
	sqlpart = getWherePart("mngcom","comcode","like")+getWherePart("feedate","MEndDate","like")+getWherePart("otherno","CaseNo");
	indsql = "select otherno,feesum,operator,operator from llcaseindfee where payed ='0' "
		   +" and exists (select 1 from LLIndirectFee where FeeDate =llcaseindfee.FeeDate and UWState='1')"+sqlpart;
    turnPage2.pageLineNum = 100;
	turnPage2.queryModal(indsql,IndirectFeeGrid);
	indsql = "select sum(feesum),count(feesum) from llcaseindfee where payed='0' "
		   +" and exists (select 1 from LLIndirectFee where FeeDate =llcaseindfee.FeeDate and UWState='1')"+sqlpart;
	var arr = easyExecSql(indsql);
	if(arr)
	{
		fm.IndFeeSum.value = arr[0][0]=='null'?'0':arr[0][0];
		fm.CaseCount.value = arr[0][1];
	}
	
}
//费用明细打印
function SurvFeePrt()
{
	var selno = GiveFeeGrid.getSelNo()-1;
	if (selno >= 0)
	{
		fm.BatchNo.value = GiveFeeGrid.getRowColData(selno,1);	//结算号	
		var newWindow =OpenWindowNew("SurvFeePrt.jsp?Year="+fm.Year.value+"&Month="+fm.Month.value+"&BatchNo="+fm.BatchNo.value,"SurvFeePrt","left" );
	}
}

function QueryOnKeyDown(){
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13"){
		queryIndFee();
		queryGiveFee();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm(){
	try{
		initForm();
	}
	catch(re){
		alert("在LLSurvFeeGive.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

