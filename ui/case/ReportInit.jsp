<%
//Name:ReportInit.jsp
//function：
//author:刘岩松
//Date:2003-07-01
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
  
  }
  catch(ex)
  {
    alter("在LLReportInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LLReportInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initSubReportGrid();
  }
  catch(re)
  {
    alter("在LLReportInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initSubReportGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";
    iArray[0][1]="30px";
    iArray[0][2]=10;
    iArray[0][3]=0;

    iArray[1]=new Array();
    iArray[1][0]="事故者客户号";
    iArray[1][1]="150px";
    iArray[1][2]=100;
    iArray[1][3]=1;

    iArray[2]=new Array();
    iArray[2][0]="事故者姓名";
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=1;

    iArray[3]=new Array();
    iArray[3][0]="医院代码";
    iArray[3][1]="0px";
    iArray[3][2]=60;
    iArray[3][3]=3;
    iArray[3][4]="HospitalCode";
    iArray[3][5]="3|4";              	                //引用代码对应第几列，'|'为分割符
    iArray[3][6]="0|1";              	        //上面的列中放置引用代码中第几位值
    iArray[3][9]="医院代码|NOTNULL";

    iArray[4]=new Array()
    iArray[4][0]="医院名称";
    iArray[4][1]="0px";
    iArray[4][2]=100;
    iArray[4][3]=3;

    iArray[5]=new Array()
    iArray[5][0]="入院日期";
    iArray[5][1]="0px";
    iArray[5][2]=100;
    iArray[5][3]=3;

    iArray[6]=new Array();
    iArray[6][0]="出院日期";
    iArray[6][1]="0px";
    iArray[6][2]=100;
    iArray[6][3]=3;

    iArray[7]=new Array();
    iArray[7][0]="备注";
    iArray[7][1]="100px";
    iArray[7][2]=100;
    iArray[7][3]=1;

    iArray[8]=new Array();
    iArray[8][0]="分报案号";
    iArray[8][1]="0px";
    iArray[8][2]=100;
    iArray[8][3]=3;

    iArray[9]=new Array();
    iArray[9][0]="事故类型";
    iArray[9][1]="50px";
    iArray[9][2]=100;
    iArray[9][3]=2;
    iArray[9][10]="AccidentType"; // 名字最好有唯一性
    iArray[9][11]= "0|^0|死亡|^1|其他" ;

    SubReportGrid = new MulLineEnter("fm","SubReportGrid");
    SubReportGrid.mulLineCount = 0;
    SubReportGrid.displayTitle = 1;
    SubReportGrid.locked = 0;
    SubReportGrid.canChk =1;
    SubReportGrid.hiddenPlus=0;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    SubReportGrid.hiddenSubtraction=0; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
    SubReportGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex.message);
  }
}
 </script>