<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-07-27 17:39:01
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="ImportRiskStatementInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ImportRiskStatementInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./ImportRiskStatementSave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">
    <br>
    <div id="Notice">
    <table>
    	<tr>
    		<td class= titleImg>
        	 注意：导入产品条款时请用"t"+产品编码作为文件名；导入产品费率表时请用"f"+产品编码作为文件名。导入基本条款时，您需要选中“基本条款”复选框，并填入条款编号及相应的条款名称以示区分，基本条款编号规则为"base"+数字编号。
       	</td> 
      </tr>
      <tr>  		 
    		<td class= titleImg>
        	 示例：守护专家住院定额个人医疗保险的条款文件为"t1201.pdf"，费率表文件为"f1201.pdf"，基本条款文件名可为"base001.pdf"。
       	</td>   		 
    	</tr>
    </table>
      
    </div>
    <div id="divDiskInput" style="display:''">
       <table class = common >
        <TR>
          <TD class= title8><INPUT TYPE="checkBox" NAME="isBasic" width=8% onclick="showRow()">基本条款</input></TD>
          <TD class= title8></TD>
          <TD class= title8 ></TD>
          <TD class= title8></TD>
          <TD class= input8 ><input class=readonly name=YName></TD>
        </tr>
        <tr id='divRiskName' style="display:'none'">
          <TD class= title8>条款编码</TD>
          <TD class= input8><input class=common name=RiskCode></TD>
          <TD class= title8>条款名称</TD>
          <TD class= input8 colspan=3><input class=common name=RiskName></TD>
        </TR>
        <TR>
          <TD class= title8>
            文件名：
          </TD>
          <TD >
            <Input  type="file" class= common name=FileName >
          </TD>
          <TD>
            <INPUT  VALUE="上载条款" class="cssbutton" TYPE=button onclick = "CaseListUpload();" >
            <INPUT  VALUE="删    除" class="cssbutton" TYPE=hidden >
          </TD>
          <TD class= title8 colspan=3></TD>
        </TR>
    	</table>   
    </div>
<hr>
    <table>
    	<tr>
    		<td>
    		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInuredList);">
    		</td>
    		<td class= titleImg>
        	 产品条款信息
       	</td>   		 
    	</tr>
    </table>
   <Div  id= "divDiskErr" style= "display: ''">
	   <Table  class= common>
	       <TR  class= common>
	        <TD text-align: left colSpan=1>
	          <span id="spanRiskStatementGrid" ></span> 
	  	    </TD>
	       </TR>
	    </Table>	
	    <p align="center">
	    	<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"></p>				
	 </Div>	

    <table>
    	<tr>
    		<td>
    		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInuredList);">
    		</td>
    		<td class= titleImg>
        	 尚未导入文件的产品
       	</td>   		 
    	</tr>
    </table>
   <Div  id= "divDiskErr" style= "display: ''">
	   <Table  class= common>
	       <TR  class= common>
	        <TD text-align: left colSpan=1>
	          <span id="spanRiskGrid" ></span> 
	  	    </TD>
	       </TR>
	    </Table>	
	    <p align="center">
	    	<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
	      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
	      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
	      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"></p>				
	 </Div>	

    <input type=hidden name="fmtransact">
    <input type=hidden name="fmAction">
    <input type=hidden name=ImportFile>
    <input type=hidden name=FileType>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
