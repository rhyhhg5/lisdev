<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：SurveyQueryDetail.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LLSurveySchema tLLSurveySchema   = new LLSurveySchema();
  tLLSurveySchema.setClmCaseNo(request.getParameter("ClmCaseNo"));
  tLLSurveySchema.setType(request.getParameter("Type"));
  tLLSurveySchema.setSerialNo(request.getParameter("SerialNo"));
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLLSurveySchema);
  tVData.addElement(tG);
  // 数据传输
  SurveyUI tSurveyUI   = new SurveyUI();
	if (!tSurveyUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tSurveyUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tSurveyUI.getResult();
		System.out.println(tVData.size());
		// 显示
		// 保单信息
		LLSurveySchema mLLSurveySchema = new LLSurveySchema();
		LLSurveySet mLLSurveySet=new LLSurveySet();
		mLLSurveySet=(LLSurveySet)tVData.getObjectByObjectName("LLSurveyBLSet",0);
		mLLSurveySchema=mLLSurveySet.get(1);
		String Path = application.getRealPath("config//Conversion.config");
		mLLSurveySchema.setContent(StrTool.Conversion(mLLSurveySchema.getContent(),Path));
    		mLLSurveySchema.setresult(StrTool.Conversion(mLLSurveySchema.getresult(),Path));
    		System.out.println("RgtNo=="+mLLSurveySchema.getRgtNo());
		System.out.println("ClmCaseNo=="+mLLSurveySchema.getClmCaseNo());
		System.out.println("InsuredNo=="+mLLSurveySchema.getCustomerNo());
		System.out.println("CustomerName=="+mLLSurveySchema.getCustomerName());
		System.out.println("Content=="+mLLSurveySchema.getContent());
		%>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    	<script language="javascript">
				<%
					String strType=mLLSurveySchema.getType();
					if(strType.equals("1"))
					{
				%>
				top.opener.fm.all("CaseNo").value= "<%=mLLSurveySchema.getClmCaseNo()%>";
				<%
					}
					else
					{
				%>
				top.opener.fm.all("SubReportNo").value= "<%=mLLSurveySchema.getClmCaseNo()%>";
				<%
					}
				%>
		var tContent = Conversion("<%=mLLSurveySchema.getContent()%>");
    		var tResult =  Conversion("<%=mLLSurveySchema.getresult()%>");

    	 	top.opener.fm.all("RgtNo").value = "<%=mLLSurveySchema.getRgtNo()%>";
    	 	top.opener.fm.all("CustomerNo").value = "<%=mLLSurveySchema.getCustomerNo()%>";
    	 	top.opener.fm.all("CustomerName").value = "<%=mLLSurveySchema.getCustomerName()%>";
    	 	top.opener.fm.all("SerialNo").value = "<%=mLLSurveySchema.getSerialNo()%>";
    	 	top.opener.fm.all("ClmCaseNo").value = "<%=mLLSurveySchema.getClmCaseNo()%>";
    	 	top.opener.fm.all("Type").value = "<%=mLLSurveySchema.getType()%>";
    	 	top.opener.fm.all("Surveyor").value = "<%=mLLSurveySchema.getSurveyorName()%>";
    	 	top.opener.fm.all("SurveySite").value = "<%=mLLSurveySchema.getSurveySite()%>";
    	 	top.opener.fm.all("SurveyStartDate").value = "<%=mLLSurveySchema.getSurveyStartDate()%>";
    	 	top.opener.fm.all("SurveyEndDate").value = "<%=mLLSurveySchema.getSurveyEndDate()%>";
    	 	top.opener.fm.all("Content").value= tContent;
    	 	top.opener.fm.all("Result").value= tResult;
    	 	top.opener.fm.all("SurveyFlag").value="<%=mLLSurveySchema.getSurveyFlag()%>";
    		top.opener.emptyUndefined();
    	</script>
		<%
	}
 // end of if
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tSurveyUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
  out.println("top.close();");
  out.println("</script>");
%>
