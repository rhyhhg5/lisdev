<%
/******************************************************************************
 * Name     ：CaseAccidentInput.jsp
 * Function :对事故/伤残界面的初始化
 * Author   :LiuYansong
 * Date     :2003-7-16
 */
%>

<script language="JavaScript">

 


var turnPage = new turnPageClass();
function initForm()
{
  try
  {
   fm.CaseNo.value = '<%= request.getParameter("CaseNo") %>';
   
   fm.CaseRelaNo.value = '<%= request.getParameter("CaseRelaNo") %>';
   
   fm.RgtNo.value = '<%= request.getParameter("RgtNo") %>';

 
   // initCaseGrid();
   //initInsuredEventGrid();
    initDiseaseGrid();
    initDeformityGrid();
    initSeriousDiseaseGrid();
    initOtherFactorGrid();
    initDegreeOperationGrid();
    initAccidentGrid();
   // initDisplayFlag();//初始化显示的函数，该函数在CaseAccidentInput.js中
    //showCaseInfo();
    initFM();
  }
  catch(re)
  {
    alert("CaseReceiptInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

function initInsuredEventGrid()
{ 
   var iArray = new Array();
       try
      {
    iArray[0]=new Array("序号","30px","0",0);
    iArray[1]=new Array("事件号","100px","0",0);
   iArray[2]=new Array("发生日期","100px","0",0);
    iArray[3]=new Array("发生地点","100px","0",0);
   iArray[4]=new Array("事件类型","100px","0",0);
   iArray[5]=new Array("主题","300px","0",0);
   

    InsuredEventGrid = new MulLineEnter("fm","InsuredEventGrid");
    InsuredEventGrid.mulLineCount = 1;
    InsuredEventGrid.displayTitle = 1;
    InsuredEventGrid.locked = 0;
    InsuredEventGrid.canChk =0;
    InsuredEventGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    InsuredEventGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
 //   InsuredEventGrid. selBoxEventFuncName = "onSelSelected";
    InsuredEventGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alter(ex);
  }
}
//事故明细
function initCaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         		//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="意外事故代码";         	//列名
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=0;            			//列最大值
    iArray[1][3]=2;
    iArray[1][4]="";
    iArray[1][5]="1|2";              	        //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
    iArray[1][9]="意外事故代码|NOTNULL";

    iArray[2]=new Array();
    iArray[2][0]="意外事故名称";         	//列名
    iArray[2][1]="500px";            		//列宽
    iArray[2][2]=160;            		//列最大值
    iArray[2][3]=1;

    CaseGrid = new MulLineEnter( "fm" , "CaseGrid" );
    CaseGrid.mulLineCount = 1;
    CaseGrid.displayTitle = 1;
    CaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}

function initDiseaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         		//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array("临床诊断","100px","100",1);
    iArray[2]=new Array("疾病","80px","100",1);
    iArray[3]=new Array("疾病种类","80px","100",1);     //,"diseasname","2|3","0|1"
    
    iArray[4]=new Array();
    iArray[4][0]="疾病代码";  //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="80px";      //列宽
    iArray[4][2]=100;         //列最大值
    iArray[4][3]=2;           //是否允许输入,1表示允许，0表示不允许3表示隐藏
    iArray[4][4]="lldiseas"
    iArray[4][5]="3|4";       //引用代码对应第几列，'|'为分割符
    iArray[4][6]="0|1";         
    iArray[4][15]="ICDName"     
    iArray[4][17]="2"           
    
    iArray[5]=new Array("诊治医院","105px","100",2,"hospital","5|6","0|1");
    iArray[6]=new Array("诊治医院名称","105px","100",0);
    iArray[7]=new Array("治疗医生","105px","100",2,"doctor","7|9","0|1");
   // iArray[7]=new Array("确诊时间","60px","100",1);
    //iArray[8]=new Array("主次标记","60px","100",2);
    iArray[8]=new Array("序号","300px","0",3);
    iArray[9]=new Array("治疗医生代码","120px","100",3);
    
    DiseaseGrid = new MulLineEnter( "fm" , "DiseaseGrid" );
    DiseaseGrid.mulLineCount = 1;
    DiseaseGrid.displayTitle = 1;
    DiseaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}
function initSeriousDiseaseGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="重疾名称";         			//列名
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=0;            			//列最大值
    iArray[1][3]=2;
    iArray[1][4]="diseasname";
    iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值
   

    iArray[2]=new Array();
    iArray[2][0]="重疾代码";         			//列名
    iArray[2][1]="120px";            		//列宽
    iArray[2][2]=160;            			//列最大值
    iArray[2][3]=1;
    iArray[2][9]="疾病代码|NOTNULL";
    
     iArray[3]=new Array("诊治医院","120px","100",3,"hospital","3|4","1|0");
     iArray[4]=new Array("诊治医院代码","100px","100",3);
     iArray[5]=new Array("治疗医生","120px","100",3,"doctor","5|8","0|1");
     iArray[5][15]="hospitcode"
     iArray[5][17]="3"
    
   // iArray[6]=new Array("确诊时间","100px","100",1);
   // iArray[7]=new Array("确诊依据","300px","100",1);
   //iArray[8]=new Array("主次标记","60px","100",2);
   iArray[6]=new Array("序号","300px","0",3);
   iArray[7]=new Array("重症监护天数","80px","4",3);
    iArray[8]=new Array("治疗医生代码","80px","100",3); 
     iArray[9]=new Array("重疾责任依据","300px","100",1);
    
    SeriousDiseaseGrid = new MulLineEnter( "fm" , "SeriousDiseaseGrid" );
    SeriousDiseaseGrid.mulLineCount = 1;
    SeriousDiseaseGrid.displayTitle = 1;
    SeriousDiseaseGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}
function initDeformityGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=3;

  

     iArray[1]=new Array("残疾名称","200px","100",2,"desc_wound","1|2|3|4","1|0|3|4");
     iArray[2]=new Array("残疾代码","80px","100",0);
     iArray[3]=new Array("残疾级别","80px","100",0);
     iArray[4]=new Array("给付比例","80px","100",0);
     iArray[5]=new Array("确认依据","120px","100",3);
      iArray[6]=new Array("序号","300px","0",3);
      iArray[7]=new Array("鉴定日期","100px","100",1);
      iArray[8]=new Array("合法认证机构","110px","20",3);
      iArray[9]=new Array("合法认证机构代码","300px","0",3);
     
    DeformityGrid = new MulLineEnter( "fm" , "DeformityGrid" );
    DeformityGrid.mulLineCount = 1;
    DeformityGrid.displayTitle = 1;
    DeformityGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在此出错");
  }
}

//初始化按档次录入初始化信息
function initDegreeOperationGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         //列宽
    iArray[0][2]=10;          	 //列最大值
    iArray[0][3]=0;
    iArray[0][4]="SerialNo";

    iArray[1]=new Array();
    iArray[1][0]="手术名称";    //列名
    iArray[1][1]="200px";       //列宽
    iArray[1][2]=100;           //列最大值
    iArray[1][3]=2;
    iArray[1][4]="lloperation";
    iArray[1][5]="1|2|4";
    iArray[1][6]="0|1|2";


    iArray[2]=new Array();
    iArray[2][0]="手术代码";    //列名
    iArray[2][1]="150px";       //列宽
    iArray[2][2]=100;           //列最大值
    iArray[2][3]=0;



    iArray[3]=new Array();
    iArray[3][0]="手术费用";    //列名
    iArray[3][1]="90px";        //列宽
    iArray[3][2]=100;           //列最大值
    iArray[3][3]=3;

    iArray[4]=new Array();
    iArray[4][0]="手术等级";    //列名
    iArray[4][1]="180px";       //列宽
    iArray[4][2]=100;           //列最大值
    iArray[4][3]=1 ;
      iArray[5]=new Array("序号","300px","0",3);

    DegreeOperationGrid = new MulLineEnter( "fm" , "DegreeOperationGrid" );
    DegreeOperationGrid.mulLineCount = 1;
    DegreeOperationGrid.displayTitle = 1;
    DegreeOperationGrid.canSel = 0;
    DegreeOperationGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在DegreeOperation中出错");
    alert(ex);
  }
}
function initOtherFactorGrid()
{
  var iArray = new Array();
  try
  {
  	iArray[0]=new Array();
    iArray[0][0]="序号";         	//FactorType 列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         	//列宽
    iArray[0][2]=10;          	        //列最大值
    iArray[0][3]=3;
    
    iArray[1]=new Array();
    iArray[1][0]="要素类型";         	//FactorType  列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="40px";         	//列宽
    iArray[1][2]=10;          		//列最大值
    iArray[1][3]=2;
    iArray[1][4]="llotherfactor";
    iArray[1][5]="1|6";
    iArray[1][6]="1|0";

    iArray[2]=new Array();
    iArray[2][0]="要素编号";    	//FactorCode 列名
    iArray[2][1]="10px";            	//列宽
    iArray[2][2]=20;            	//列最大值
    iArray[2][3]=3;

    iArray[3]=new Array();
    iArray[3][0]="要素";    
    iArray[3][1]="40px";            	//列宽
    iArray[3][2]=10;            	//列最大值
    iArray[3][3]=2;
    iArray[3][3]=2;
    iArray[3][4]="llfactor";
    iArray[3][5]="3|2";
    iArray[3][6]="1|0";
    iArray[3][15]="要素类型";
    iArray[3][16]="1";



    iArray[4]=new Array();
    iArray[4][0]="要素值";    	//Value 列名
    iArray[4][1]="30px";        //列宽
    iArray[4][2]=15;            //列最大值
    iArray[4][3]=1;

    iArray[5]=new Array();
    iArray[5][0]="备注";    	//Remark   列名
    iArray[5][1]="70px";        //列宽
    iArray[5][2]=20;            //列最大值
    iArray[5][3]=1 ;

    iArray[6]=new Array();
    iArray[6][0]="要素类型代码";    //Remark   列名
    iArray[6][1]="20px";            //列宽
    iArray[6][2]=15;                //列最大值
    iArray[6][3]=3 ;
  
    OtherFactorGrid = new MulLineEnter( "fm" , "OtherFactorGrid" );
    OtherFactorGrid.mulLineCount = 1;
    OtherFactorGrid.displayTitle = 1;
    OtherFactorGrid.canSel = 0;
    OtherFactorGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在DegreeOperation中出错");
    alert(ex);
  }
}
function initAccidentGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         	//FactorType 列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         	//列宽
    iArray[0][2]=10;          		//列最大值
    iArray[0][3]=3;
    
    iArray[1]=new Array();
    iArray[1][0]="意外序号";         	//FactorType     列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="100px";         	//列宽
    iArray[1][2]=10;          		//列最大值
    iArray[1][3]=3;

    iArray[2]=new Array();
    iArray[2][0]="意外类型";    	//FactorCode  列名
    iArray[2][1]="100px";            	//列宽
    iArray[2][2]=100;            	//列最大值
    iArray[2][3]=3;

    iArray[3]=new Array();
    iArray[3][0]="意外类型名称";    	//FactorName   列名
    iArray[3][1]="150px";            	//列宽
    iArray[3][2]=100;            	//列最大值
    iArray[3][3]=3;
    
    iArray[4]=new Array("意外查询","80px","100",1);

    iArray[5]=new Array();
    iArray[5][0]="意外代码";    	//Value        列名
    iArray[5][1]="90px";            	//列宽
    iArray[5][2]=100;            	//列最大值
    iArray[5][3]=3;
    
    iArray[6]=new Array();
    iArray[6][0]="意外名称";    	//Value        列名
    iArray[6][1]="300px";            	//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=2;
    iArray[6][4]="llacci"
    iArray[6][5]="5|6";       //引用代码对应第几列，'|'为分割符
    iArray[6][6]="0|1";         
    iArray[6][15]="accname"     
    iArray[6][17]="4"   
    
    
    iArray[7]=new Array();
    iArray[7][0]="意外原因代码";    	//Remark   列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=100;            	//列最大值
    iArray[7][3]=3 ;
    
    iArray[8]=new Array();
    iArray[8][0]="意外原因名称";    	//Remark   列名
    iArray[8][1]="180px";            	//列宽
    iArray[8][2]=100;            	//列最大值
    iArray[8][3]=3 ;
    
    iArray[9]=new Array();
    iArray[9][0]="备注";    	//Remark   列名
    iArray[9][1]="180px";       //列宽
    iArray[9][2]=100;           //列最大值
    iArray[9][3]=1 ;
    
    AccidentGrid = new MulLineEnter( "fm" , "AccidentGrid" );
    AccidentGrid.mulLineCount = 1;
    AccidentGrid.displayTitle = 1;
    AccidentGrid.canSel = 0;
    AccidentGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在DegreeOperation中出错");
    alert(ex);
  }
}


</script>