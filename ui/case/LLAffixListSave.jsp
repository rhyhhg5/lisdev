<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLAffixListSave.jsp
//程序功能：
//创建日期：2005-02-16
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.health.*"%>
  <%@page import="java.util.*"%>
  <%
  	System.out.println("<-Star SavePage->");
    LLAffixSet tLLAffixSet= new LLAffixSet();
    LLAffixBL tLLAffixBL= new LLAffixBL();
    VData tVData = new VData();
	String appDate=request.getParameter("appDate"); 
      CErrors tError = null;
	  String tRela  = "";                
	  String FlagStr = "";
	  String Content = "";
	  String transact = "";
	  GlobalInput tG = new GlobalInput(); 
	  tG=(GlobalInput)session.getValue("GI");
	  transact = request.getParameter("fmtransact");	
	String Addflag=request.getParameter("Addrow");  
	String LoadFlag = request.getParameter("LoadFlag");   	  
	String tAffixType[] = request.getParameterValues("AffixGrid10"); //材料类型
	String tAffixCode[] = request.getParameterValues("AffixGrid2"); //材料代码
	String tAffixName[] = request.getParameterValues("AffixGrid3"); //材料名称
	String tSupplyDate[] = request.getParameterValues("AffixGrid4");//提供日期
	String tProperty[] = request.getParameterValues("AffixGrid5");	//材料属性
	String tCount[] = request.getParameterValues("AffixGrid6");		//材料数量
	String tShortCount[] = request.getParameterValues("AffixGrid7");//缺少材料
	String tAffixNo[] = request.getParameterValues("AffixGrid8");
	String tSerialNo[] = request.getParameterValues("AffixGrid9");
	int mShortCount[] = new int[100];//用于判断材料是否齐备
	String ShortCountFlag ="";//1材料不齐备 0材料齐备
	String mSupplyDate[] = new String[100];//用于判断材料齐备日期
	String SupplyDateResult="";//用于存放齐备的日期
	String AffixNo="";
	String tChk[] = request.getParameterValues("InpAffixGridChk"); 
	int affcopies=0;
	int shortcopies=0;
	String shortaff ="";
	if (tChk!=null){
	 for(int index=0;index<tChk.length;index++)
	 {
	   if(tChk[index].equals("1"))
	   {
	   	System.out.println("index : "+index);
	     LLAffixSchema tLLAffixSchema = new LLAffixSchema();
	     System.out.println("AffixType:"+tAffixType[index]);
	     tLLAffixSchema.setAffixType(tAffixType[index]);
	     tLLAffixSchema.setAffixCode(tAffixCode[index]);
	     tLLAffixSchema.setAffixName(tAffixName[index]);
	     if ( tSupplyDate[index]==null || "".equals(tSupplyDate[index]))
	     {
	         tSupplyDate[index]= PubFun.getCurrentDate();
	        
	     }
	     
	     if ( tShortCount[index]==null || "".equals(tShortCount[index]))
	     {
	     		if ("9".equals(LoadFlag)){
					tShortCount[index] = "1";	}
				else{
					 tShortCount[index] = "0";}
		     if ( tCount[index]==null || "".equals(tCount[index]))
		     {
		      tCount[index] = "1";	 
	    	
		     }  	
	     }
	     else
	     {
		     if ( tCount[index]==null || "".equals(tCount[index]))
		     {
		     
		      tCount[index] = tShortCount[index];	 
	    	      tSupplyDate[index]= "";
		     }
		     else
		     {
		     	if ( Integer.parseInt(tShortCount[index]) > Integer.parseInt(tCount[index]) ) 
		     	{
		     		index++;
		     		FlagStr = "Fail";		     		
		     		Content = "缺少份数不能大于份数！" +
		     			"序号：" + index;
		     		tSupplyDate[index]= "";
		     		break;
		     	}
		     }	     
	     }
	     	   
	     if ( Integer.parseInt(tShortCount[index]) == Integer.parseInt(tCount[index]) ) 
	     {
		     	tSupplyDate[index]= "";

	     }	     	     
	     tLLAffixSchema.setSupplyDate(  tSupplyDate[index] );	     	     
	     tLLAffixSchema.setShortCount(tShortCount[index]);
			 System.out.println("缺少的件数"+tShortCount[index]); 
			 shortcopies +=tLLAffixSchema.getShortCount();
			 shortaff +=tAffixName[index]+",";
	     tLLAffixSchema.setCount(tCount[index]);
			 System.out.println("需要的件数"+tCount[index]); 
			 affcopies +=tLLAffixSchema.getCount();
	     tLLAffixSchema.setProperty(tProperty[index]);
	     tLLAffixSchema.setAffixNo(tAffixNo[index]);
	     tLLAffixSchema.setSerialNo(tSerialNo[index]);
	     System.out.println("RgtNo : "+request.getParameter("RgtNo"));
	     tLLAffixSchema.setRgtNo(request.getParameter("RgtNo"));
	     tLLAffixSchema.setCaseNo(request.getParameter("CaseNo"));
	     System.out.println("原来的"+request.getParameter("CaseNo"));
	     tLLAffixSchema.setReasonCode("13");
	     System.out.println("ReasonFlag : "+request.getParameter("ReasonFlag"));
	     tLLAffixSet.add(tLLAffixSchema);
	   }           
	 }
 }
 if(!Addflag.equals("0")){
	String tAddAffixType[] = request.getParameterValues("AddAffixGrid10"); //材料类型
	String tAddAffixCode[] = request.getParameterValues("AddAffixGrid2"); //材料代码
	String tAddAffixName[] = request.getParameterValues("AddAffixGrid3"); //材料名称
	String tAddSupplyDate[] = request.getParameterValues("AddAffixGrid4");//提供日期
	String tAddProperty[] = request.getParameterValues("AddAffixGrid5");	//材料属性
	String tAddCount[] = request.getParameterValues("AddAffixGrid6");		//材料数量
	String tAddShortCount[] = request.getParameterValues("AddAffixGrid7");//缺少材料
	String tAddAffixNo[] = request.getParameterValues("AddAffixGrid8");
	String tAddSerialNo[] = request.getParameterValues("AddAffixGrid9");	 
	System.out.println("******************************************************"+tAddAffixType[0]);
	System.out.println("******************************************************"+tAddAffixCode[0]);
	System.out.println("******************************************************"+tAddAffixName[0]);

	 for(int i=0;i<tAddAffixCode.length;i++)
	 {

	     LLAffixSchema mLLAffixSchema = new LLAffixSchema();
	     mLLAffixSchema.setAffixType(tAddAffixType[i]);
	     mLLAffixSchema.setAffixCode(tAddAffixCode[i]);
	     mLLAffixSchema.setAffixName(tAddAffixName[i]);
	     if ( tAddSupplyDate[i]==null || "".equals(tAddSupplyDate[i]))
	         tAddSupplyDate[i]= PubFun.getCurrentDate();      
	     if ( tAddShortCount[i]==null || "".equals(tAddShortCount[i]))
	     {
	     		if ("9".equals(LoadFlag)){
					tAddShortCount[i] = "1";	}
				else{
					 tAddShortCount[i] = "0";}
		     if ( tAddCount[i]==null || "".equals(tAddCount[i]))
		     {
		      tAddCount[i] = "1";	
		      }  	
	     }
	     else
	     {
		     if ( tAddCount[i]==null || "".equals(tAddCount[i]))
		     {
		     
		      tAddCount[i] = tAddShortCount[i];	 
	    	      tAddSupplyDate[i]= "";
		     }
		     else
		     {
		     	if ( Integer.parseInt(tAddShortCount[i]) > Integer.parseInt(tAddCount[i]) ) 
		     	{
		     		i++;
		     		FlagStr = "Fail";		     		
		     		Content = "缺少份数不能大于份数！" +
		     			"序号：" + i;
		     		tAddSupplyDate[i]= "";
		     		break;
		     	}
		     }	     
	     }
	     	   
	     if ( Integer.parseInt(tAddShortCount[i]) == Integer.parseInt(tAddCount[i]) ) 
	     {
		     	tAddSupplyDate[i]= "";
	     }	     	     
	     mLLAffixSchema.setSupplyDate(  tAddSupplyDate[i] );	     	     
	     mLLAffixSchema.setShortCount(tAddShortCount[i]);
			 System.out.println("缺少的件数"+tAddShortCount[i]); 
			 shortcopies +=mLLAffixSchema.getShortCount();
			 shortaff +=tAddAffixName[i]+",";
	     mLLAffixSchema.setCount(tAddCount[i]);
			 System.out.println("需要的件数"+tAddCount[i]); 
			 affcopies +=mLLAffixSchema.getCount();
	     mLLAffixSchema.setProperty(tAddProperty[i]);
	     mLLAffixSchema.setAffixNo(tAddAffixNo[i]);
	     mLLAffixSchema.setSerialNo(tAddSerialNo[i]);
	     System.out.println("RgtNo : "+request.getParameter("RgtNo"));
	     mLLAffixSchema.setRgtNo(request.getParameter("RgtNo"));
	     mLLAffixSchema.setCaseNo(request.getParameter("CaseNo"));
	     System.out.println("新加的"+request.getParameter("CaseNo"));
	     mLLAffixSchema.setReasonCode("13");
	     System.out.println("ReasonFlag : "+request.getParameter("ReasonFlag"));
	     tLLAffixSet.add(mLLAffixSchema);
	      
	 }
	 }
	 TransferData tTransferData = new TransferData();
		tTransferData.setNameAndValue("LoadFlag",LoadFlag);
		tTransferData.setNameAndValue("appDate",appDate);
if (FlagStr=="") 
{
	try
	{
		// 准备传输数据 VData
		tVData.add(tLLAffixSet);
		tVData.add(tTransferData);
		tVData.add(tG);
		tLLAffixBL.submitData(tVData,transact);

	}
	catch(Exception ex)
	{
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}	 
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	  if (FlagStr=="")
	  {
	    tError = tLLAffixBL.mErrors;
	    if (!tError.needDealError())
	    {                          
	    	Content = " 保存成功! ";
	    	FlagStr = "Success";
	    	tVData.clear();
	    	tVData = tLLAffixBL.getResult();
	    }
	    else                                                                           
	    {
	    	Content = " 保存失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  }

 }
%>
             

<html>
<script language="javascript">
  parent.fraInterface.fm.AffordCopies.value="<%=affcopies%>";
  parent.fraInterface.fm.ShortCopies.value="<%=shortcopies%>";
  parent.fraInterface.fm.ShortAffName.value="<%=shortaff%>";
	parent.fraInterface.fm.ShortCountFlag.value="<%=ShortCountFlag%>" ;
	parent.fraInterface.fm.SupplyDateResult.value="<%=SupplyDateResult%>" ;
	parent.fraInterface.fm.AffixNo.value="<%=AffixNo%>" ;
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>