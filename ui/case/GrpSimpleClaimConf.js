var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var tSaveType="";

//提交，保存按钮对应操作
function submitForm()
{

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
   fm.comfirm1.disabled=false;
   fm.SDTG.disabled=false;
   fm.RSDTG.disabled=false;
   fm.CASERETURN.disabled=false;
  showInfo.close();
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

      queryGrpCaseGrid();
  var selno = GrpRegisterGrid.getSelNo();
  if (selno <= 0)
    return ;
  var jsRgtNo = GrpRegisterGrid.getRowColData(selno - 1, 1);
      aftersql = "select b.codename,r.rgtstate from llregister r,ldcode b "
      +" where b.codetype='llgrprgtstate' and b.code =r.rgtstate and rgtno = '"+jsRgtNo+"'";
      xrr = easyExecSql(aftersql);
      if(xrr){
        GrpRegisterGrid.setRowColData(selno-1,8,xrr[0][0]);
        GrpRegisterGrid.setRowColData(selno-1,9,xrr[0][1]);
      }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//提交前的校验、计算
function beforeSubmit()
{
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1"){
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else{
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

function UWClaim()
{
  var selno = GrpRegisterGrid.getSelNo();
  if (selno <=0)
  {
    alert("请选择一条团体案件！");
    return ;
  }

  //window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  var varSrc="";
  var newWindow = window.open("./FrameMain.jsp?Interface=PayAffirmInput.jsp"+varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SearchGrpRegister()
{
  var startDate = fm.RgtDateStart.value;
  var endDate = fm.RgtDateEnd.value;
  if (startDate==""||startDate==null||endDate==""||endDate==null){
  	alert("请输入受理日期");
  	return false;
  }
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  // 书写SQL语句
  var strSql = " SELECT R.RGTNO, R.CUSTOMERNO, R.GRPNAME, R.RGTOBJNO, R.RGTANTNAME, R.RGTDATE, R.APPPEOPLES, "
  			+" b.codename,R.rgtstate,R.appamnt " 
  			+" FROM LLREGISTER R , ldcode b " 
  			+" WHERE R.RGTOBJ = '0' AND R.RGTCLASS = '1' and R.ApplyerType='5' "
  		//	+" and rgtstate in ('02','03','04','05') AND R.declineflag is null " 
  		    +"  AND R.declineflag is null " 
  			+" and b.codetype='llgrprgtstate' and b.code = r.rgtstate "
  + getWherePart("r.MngCom","ManageCom","like")
  + getWherePart("R.RGTNO","srRgtNo")
  + getWherePart("R.CUSTOMERNO","srCustomerNo")
  + getWherePart("R.GRPNAME","srGrpName")
  + getWherePart("R.RGTANTNAME","srRgtantName")
  + getWherePart("R.rgtstate","GrpRgtState")
  + getWherePart("R.RGTDATE","RgtDateStart",">=")
  + getWherePart("R.RGTDATE","RgtDateEnd","<=")
  + " order by r.rgtno desc"
  turnPage.queryModal(strSql,GrpRegisterGrid);
  showInfo.close();
}

function CaseEnsure()
{
  var CaseCount=GrpCaseGrid.mulLineCount;
  var chkFlag=false;
  for (i=0;i<CaseCount;i++){
    if(GrpCaseGrid.getChkNo(i)==true){
      chkFlag=true;
      var CaseNo = GrpCaseGrid.getRowColData(i,1);
      var paySql = "select e.accdate,b.paytodate from llclaimdetail a,lcpol b,lmriskapp c,llcaserela d,llsubreport e "
	       + " where a.polno=b.polno and a.riskcode=c.riskcode and a.caserelano=d.caserelano "
	       + " and d.subrptno=e.subrptno "
	       + " and c.riskprop='G' and e.accdate >= b.paytodate and a.caseno='"
	       + CaseNo + "' union "
	       + " select e.accdate,b.paytodate from llclaimdetail a,lbpol b,lmriskapp c,llcaserela d,llsubreport e "
	       + " where a.polno=b.polno and a.riskcode=c.riskcode and a.caserelano=d.caserelano "
	       + " and d.subrptno=e.subrptno "
	       + " and c.riskprop='G' and e.accdate >= b.paytodate and a.caseno='"
	       + CaseNo + "' with ur";
      var arr = easyExecSql(paySql);
      if (arr) {
    	  for(var j=0; j<arr.length; j++) {
        	  if(!confirm("案件"+CaseNo+"出险日期"+ arr[j][0] +"在保费交至日"+ arr[j][1] +"之外，是否继续理赔")) {
        		  return false;
        	  }
          }    	  
      }
    }
  }
  
  if (chkFlag==false){
    alert("请选中审批通过的案件");
    return false;
  }
  var StateRadio="";
  for(i = 0; i <fm.StateRadio.length; i++){
    if(fm.StateRadio[i].checked){
      StateRadio=fm.StateRadio[i].value;
      break;
    }
  }
  if(StateRadio=='1'){
  	fm.all('operate').value	= "UW||MAIN";
  }
  if(StateRadio=='4'||StateRadio=='5'){
  	fm.all('operate').value	= "SIGN||MAIN";
  }
  fm.action = "./GrpSimpleClaimConfSave.jsp"; 
  fm.SDTG.disabled=true;
  fm.RSDTG.disabled=true;
  fm.CASERETURN.disabled=true;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交

}

//批量审定
function RGTEnsure()
{ var selno = GrpRegisterGrid.getSelNo();
  if (selno <= 0)
  {
    return ;
  }
  var jsRgtNo = GrpRegisterGrid.getRowColData(selno - 1, 1);
  fm.RgtNo.value = jsRgtNo;
  if(jsRgtNo == null || jsRgtNo == "")
  {
    alert("团体批次号为空！");
    return;
  }

  
  for(i = 0; i <fm.StateRadio.length; i++){
    if(fm.StateRadio[i].checked){
      fm.all('NewStateRadio').value=fm.StateRadio[i].value;
      break;
    }
  }
  if(fm.all('NewStateRadio').value=='0'||fm.all('NewStateRadio').value=='2'||fm.all('NewStateRadio').value=='3'){
  	alert("所选案件无法批量审定!");
  	return;
  }
  if(fm.all('NewStateRadio').value=='1'){
  	fm.all('operate').value	= "UW||MAIN";
  }
  if(fm.all('NewStateRadio').value=='4'||fm.all('NewStateRadio').value=='5'){
  	fm.all('operate').value	= "SIGN||MAIN";
  }
  fm.action = "./GrpAllClaimConfSave.jsp"; 
  fm.SDTG.disabled=true;
  fm.RSDTG.disabled=true;
  fm.CASERETURN.disabled=true;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

function queryGrpCaseGrid()
{
  var selno = GrpRegisterGrid.getSelNo();
  if (selno <= 0)
  {
    return ;
  }
  var jsRgtNo = GrpRegisterGrid.getRowColData(selno - 1, 1);
  fm.RgtNo.value = jsRgtNo;
  fm.AppAmnt.value = GrpRegisterGrid.getRowColData(selno-1,10);
  if(jsRgtNo == null || jsRgtNo == "")
  {
    alert("团体批次号为空！");
    return;
  }
  var StateRadio="";
  for(i = 0; i <fm.StateRadio.length; i++){
    if(fm.StateRadio[i].checked){
      StateRadio=fm.StateRadio[i].value;
      break;
    }
  }

  var strSql = "select c.caseno,c.otheridno,c.customerno,c.customername,c.customersex,c.idno,"
 	 +" case when d.feetype='1' then '门诊' when d.feetype='2' then '住院' when d.feetype='3' then '门诊特殊病' else '' end,"
 	 +" d.feedate,(select DiseaseName from llcasecure where c.caseno=caseno fetch first 1 rows only),'',d.sumfee,"
 	 +" (select sum(realpay) from LLClaimdetail where caseno=c.caseno) "
 	 +" FROM LLCASE C, llfeemain d "
 	 +" WHERE C.RGTNO = '" + jsRgtNo + "' and d.caseno= c.caseno ";
 	 
  var sql0 = " and (c.handler='"+fm.Operator.value+"' or c.operator='"+fm.Operator.value
  	+"' or c.uwer='"+fm.Operator.value+"' or c.signer='"+fm.Operator.value
  	+"' or c.dealer='"+fm.Operator.value+"') "
  switch(StateRadio){
    case "0":
    sqlpart = " order by c.caseno ";
    break;
    case "1":
    sqlpart = " and c.rgtstate in ('03','04','08') order by c.caseno ";
    break;
    case "2":
    sqlpart = " and c.rgtstate='01' order by c.caseno ";
    break;
    case "3":
    sqlpart = " and c.rgtstate='09' order by c.caseno ";
    break;
    case "4":
    sqlpart = sql0+" and c.rgtstate in ('05','06') order by c.caseno ";
    break;
    case "5":
    sqlpart = sql0+" and c.rgtstate='10' order by c.caseno ";
  }
  strSql+=sqlpart;
  turnPage2.queryModal(strSql,GrpCaseGrid);
  divGrpCaseInfo.style.display = '';
  var sumSql = "select sum(a.realpay) from llclaimdetail a,llcase b where b.caseno=a.caseno and a.rgtno = '"+jsRgtNo+"'";
  var sumrr = easyExecSql(sumSql);
  if(sumrr){
    fm.StandPay.value = sumrr[0][0]=='null'?0:sumrr[0][0];
  }
}

function QueryOnKeyDown()
{
  var keycode = event.keyCode;

  if(keycode=="13")
  {
    SearchGrpRegister();
  }
}

function afterCodeSelect( cCodeName, Field )
{
}

function CaseInfo()
{
  varSrc = "";
  var CaseCount=GrpCaseGrid.mulLineCount;
  for (i=0;i<CaseCount;i++){
  	if(GrpCaseGrid.getChkNo(i)==true){
  		varSrc= "&RgtNo="+fm.RgtNo.value+"&CaseNo="+GrpCaseGrid.getRowColData(i,1);
  		//varSrc = "&CaseNo=" + GrpCaseGrid.getRowColData(i,1);
  		break;
  	}
	}
  var pathStr="./FrameUnique.jsp?Interface=LLRegisterInput.jsp"+varSrc;
  var newWindow=OpenWindowNew(pathStr,"简易录入","left");
}

function CaseBack()
{
  initBackListGrid();
  divBackList.style.display='';
  var CaseCount=GrpCaseGrid.mulLineCount;
  var count = 0;
  for (i=0;i<CaseCount;i++){
    if(GrpCaseGrid.getChkNo(i)==true){
      BackListGrid.addOne("BackListGrid");
      BackListGrid.setRowColData(count,1,GrpCaseGrid.getRowColData(i,1));
      BackListGrid.setRowColData(count,2,GrpCaseGrid.getRowColData(i,2));
      BackListGrid.setRowColData(count,3,GrpCaseGrid.getRowColData(i,3));
      BackListGrid.setRowColData(count,4,GrpCaseGrid.getRowColData(i,4));
      BackListGrid.setRowColData(count,6,GrpCaseGrid.getRowColData(i,11));
      BackListGrid.setRowColData(count,7,GrpCaseGrid.getRowColData(i,12));
      count++;
    }
  }
  
}

function CancelBack()
{
  initBackListGrid();
  divBackList.style.display='none';
}

function BackEnsure()
{
	  fm.fmtransact.value = "BACK||MAIN";
	  fm.comfirm1.disabled=true;
	  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.action ="./SimCaseReturnSave.jsp";  
	  fm.submit(); //提交
}

function ScanQuery() {
  window.open("./ClaimEasyScan.jsp?RgtNo="+fm.RgtNo.value+"&SubType=LP1002&BussType=LP&BussNoType=21", "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
}

function submitFormSurvery(){
  var CaseCount=GrpCaseGrid.mulLineCount;
  var CaseNo="";
  var CustomerNo="";
  var CustomerName="";
  var RgtNo="";
  for (var i=0;i<CaseCount;i++){
  	if(GrpCaseGrid.getChkNo(i)==true){
  		CaseNo=GrpCaseGrid.getRowColData(i,1);
  		CustomerNo=GrpCaseGrid.getRowColData(i,3);
  		CustomerName=GrpCaseGrid.getRowColData(i,4);
  		RgtNo=GrpRegisterGrid.getRowColData(0,1);
  		break;
  	}
	}
  strSQL="select otherno from LLSurvey where otherno='"+CaseNo+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult){
    if(!confirm("该案件提起过调查，您确定要继续吗？")){
      return false;
    }
  }
  else{

  }
  
    //TODO:临时校验，社保案件不可提起调查    
//  	var tSocial = CaseNo;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}
	
	//TODO:新增效验，理赔二核时不可提起调查
	var tUWSql = "select rgtstate from llcase where caseno='"+CaseNo+"' with ur";
  	var tUWRgtState=easyExecSql(tUWSql);
	if (tUWRgtState!=null){
	     if(tUWRgtState[0][0]=='16'){
	       alert("理赔二核中不可提起调查，必须等待二核结束！");
	       return false;
	     }
	}
  
  var varSrc = "&CaseNo=" + CaseNo;
  varSrc += "&InsuredNo=" + CustomerNo;
  varSrc += "&CustomerName=" +CustomerName;
  varSrc += "&RgtNo=" + RgtNo;
  varSrc += "&StartPhase=0";
  varSrc += "&Type=1";

  pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,500);
}

function SurveyPrint(){
  var CaseCount=GrpCaseGrid.mulLineCount;
  var caseNo='';
  var ty=0;
  for (var i=0;i<CaseCount;i++){
  	if(GrpCaseGrid.getChkNo(i)==true){
  		ty++;
  		caseNo=GrpCaseGrid.getRowColData(i,1);
  	}
	}
	if (ty == 0){
		alert("请选择要打印调查报告的理赔案件");
		return false;
	}
	if(ty==1){	
	var SurveyNo='';
    var strSQL="select b.OtherNo,substr(b.SurveyNo,18),"
	+ "(select c.codename from ldcode c where codetype='llsurveykind' and code=a.SurveyType),"
	+"a.SurveyStartDate,a.Startman,a.RgtState,a.SurveyType,b.inqno,"
	+ "(select c.codename from ldcode c where codetype='llsurveyflag' and code=a.surveyflag),"
	+ "b.SurveyNo,a.surveysite from LLSurvey a,LLInqApply b where 1=1 and a.surveyno=b.surveyno and not exists (select 1 from llcase where rgtstate='14' and caseno=b.otherno and not exists (select 1 from llcaseoptime where caseno=llcase.caseno and rgtstate='08')) "
	strSQL=strSQL+" and b.otherno='"+caseNo+"'with ur ";
	var arrResult=easyExecSql(strSQL);
	if(arrResult != null){
	    	if(arrResult.length==1){
	    	     try{SurveyNo=arrResult[0][9]}catch(ex){alert(ex.message+"SurveyNo")}
	    	     fm.target = "f1print";
	             fm.action ="./SurveyReportPrt.jsp?SurveyNo=" + SurveyNo;  
                 fm.submit();
	    	     
	    	}else{
	    	 var varSrc = "&CaseNo=" + caseNo;
      			 showInfo = window.open("./FrameMainSimpleLLSurveyReply.jsp?Interface= SimpleLLSurveyReply.jsp"+varSrc,"SimpleLLSurveyReply",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0'); 
	    	     SurveyNo = fm.SurveyNo.value;
	    	     //var newWindow = OpenWindowNew("SurveyReportPrt.jsp?SurveyNo=" + SurveyNo,"","left" );
	    	}
	
	 }
	}

}

function queryCheckGrid()
{
  var CaseCount=GrpCaseGrid.mulLineCount;
  var CaseNo="";
  var ty=0;
  for (var i=0;i<CaseCount;i++){
  	if(GrpCaseGrid.getChkNo(i)==true){
  	    CaseNo=GrpCaseGrid.getRowColData(i,1);
  		ty=ty+1;
  		
  	}
	}
  if(ty==1){
  var isSurvey = "select rgtstate from llcase where caseno='"+CaseNo+"' with ur";
  var rgtState = easyExecSql(isSurvey);
  if(rgtState=="07"){
      initCheckGrid();
      isShow.style.display='';
      var strSql = "select b.OtherNo,substr(b.SurveyNo,18),"
	  + "(select c.codename from ldcode c where codetype='llsurveykind' and code=a.SurveyType),"
	  +"a.SurveyStartDate,a.Startman,a.RgtState,a.SurveyType,b.inqno,"
	  + "(select c.codename from ldcode c where codetype='llsurveyflag' and code=a.surveyflag),"
	  + "b.SurveyNo,a.surveysite from LLSurvey a,LLInqApply b where 1=1 and a.surveyno=b.surveyno and not exists (select 1 from llcase where rgtstate='14' and caseno=b.otherno and not exists (select 1 from llcaseoptime where caseno=llcase.caseno and rgtstate='08')) and b.otherno='"+CaseNo+"' with ur"
	  turnPage.queryModal(strSql,CheckGrid);
  }
  }
  
}
function afterSimpleLLSurveyReply( arrReturn )
{

		if(arrReturn.length==1) 
		{
	  	try{fm.all('SurveyNo').value=arrReturn[0][9]}catch(ex){alert(ex.message+"GrpNo")}
	  	}
	  	var SurveyNo=fm.SurveyNo.value;
	  	if(fm.SurveyNo.value!=null){
	  		     fm.target = "f1print";
	             fm.action ="./SurveyReportPrt.jsp?SurveyNo=" + SurveyNo;  
                 fm.submit();
	    	     
	  	}
       
}
