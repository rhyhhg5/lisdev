<%
//Name：LDDiseaseRiskInit.jsp
//Function：风险疾病库配置
//Date：2010-11-18
//Author  ：张仲豪
%>

<script language="JavaScript">
  function initInpBox( )
  {
    try {
    	var tManageCom = <%=tG1.ManageCom%>;
    	if (tManageCom!='86') {
    		divType.style.display='none';
    	}

    }
    catch(ex) {
      alert("在LLClaimCollectionInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initSelBox(){
    try{
    }
    catch(ex){
      alert("在LLClaimCollectionInit.jsp-->InitSelBox函数中发生异常,初始化界面错误!");
    }
  }

  function initForm(){
    try{
      initDiseaseRiskGrid();
     }
    catch(re){
      alert("LDDiseaseRiskInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+ re.message);
    }
  }

  // 保单信息列表的初始化

  
  function initDiseaseRiskGrid(){
    var iArray = new Array();
    try{
      iArray[0]=new Array("序号","30px","10",0);
      iArray[1]=new Array("疾病分类代码","40px","100",0);
      iArray[2]=new Array("疾病名称","80px","100",0);
      iArray[3]=new Array("风险等级","20px","100",2);
      iArray[3][4] = "risklevel";
      iArray[3][5]="3";
      iArray[3][6]="0";
      iArray[4]=new Array("风险存续期","20px","100",2);
      iArray[4][4] = "riskcontinueflag";
      iArray[4][5]="4";
      iArray[4][6]="0";
      
      DiseaseRiskGrid = new MulLineEnter("fm","DiseaseRiskGrid");
      DiseaseRiskGrid.mulLineCount = 0;
      DiseaseRiskGrid.displayTitle = 1;
      DiseaseRiskGrid.locked = 0;
      DiseaseRiskGrid.canChk = 0;
      DiseaseRiskGrid.canSel = 1;
      DiseaseRiskGrid.hiddenPlus=1;
      DiseaseRiskGrid.hiddenSubtraction=1;
      DiseaseRiskGrid.selBoxEventFuncName="";
      DiseaseRiskGrid.loadMulLine(iArray);
    }
    catch(ex){
      alter(ex);
    }
  }

</script>