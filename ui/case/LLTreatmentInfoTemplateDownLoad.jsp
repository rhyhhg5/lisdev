<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLTreatmentInfoTemplateDownLoad.jsp
//程序功能：药品信息导入模板下载
//创建日期：2015-02-12
//创建人  ：Liyunxia
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    String downLoadFileName = "诊疗项目信息导入模板.xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    //设置表头
    String[][] tTitle = {{"诊疗项目编码","项目名称","项目内涵","说明","类别","拼音简码","打包编码","组合描述","计价单位","单价","备注","管理机构","地区编码"}};
    
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
        createexcellist.setRowColOffset(row,0);//设置偏移
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>
