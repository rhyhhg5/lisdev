<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskLetter.jsp
//程序功能：
//创建日期：2005-
//创建人  ：Yang yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
	String tWorkNo = request.getParameter("workNo");
	
	String BussNoType = "";
	String BussType = "";
	String SubType = "";
	
	SSRS tSSRS = new SSRS();
	ExeSQL tExeSQL = new ExeSQL();
	String sql = 	"select BussNoType, BussType, SubType "
					+ "from ES_DOC_RELATION "
					+ "where bussNo='" + tWorkNo + "' "
					+ "		and subType='BQ02'";
	
	tSSRS = tExeSQL.execSQL(sql);
	if(tSSRS.getMaxRow() > 0)
	{
		BussNoType 	= tSSRS.GetText(1, 1);
		BussType 	= tSSRS.GetText(1, 2);
		SubType 	= tSSRS.GetText(1, 3);
	}
%>

<html>
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="TaskLetter.js"></SCRIPT>
  <%@include file="TaskLetterInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>函件下发 </title>
</head>
<body  onload="initForm();">
	<form name=fm action="./TaskLetterSave.jsp" target=fraSubmit method=post>
	  	<table>
	    	<tr> 
	    	  <td width="17"> 
	    	  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divLetterList);"> 
	    	  </td>
	    	  <td width="185" class= titleImg>函件清单 </td>
	    	</tr>
		</table>
		<Div  id= "divLetterList" style= "display: ''" align=center>  
			<table  class= common>
		 		<tr  class= common>
			  		<td text-align: left colSpan=1>
						<span id="spanLetterGrid" >
						</span> 
			  		</td>
				</tr>
			</table>
			<Div id= "divPage" align=center style= "display: 'none' ">
				<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage(); showCodeName();"> 
				<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage(); showCodeName();"> 					
				<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage(); showCodeName();"> 
				<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage(); showCodeName();"> 
			</Div>
		</Div>
		
		<div id="scanId" style="display: none">
			<table>
	    		<tr> 
	    		  <td width="17"> 
	    		  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divScan);"> 
	    		  </td>
	    		  <td width="185" class= titleImg>扫描件 </td>
	    		</tr>
			</table>
			<Div  id= "divScan" style= "display: ''" align=center>  
				<table  class= common>
			 		<tr  class= common>
				  		<td text-align: left colSpan=1>
							<span id="spanScanGrid" >
							</span> 
				  		</td>
					</tr>
				</table>
				<Div id= "divPage2" align=center style= "display: 'none' ">
					<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage(); showCodeName();"> 
					<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage(); showCodeName();"> 					
					<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage(); showCodeName();"> 
					<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage(); showCodeName();"> 
				</Div>
			</Div>
			
			<!--以页内框架方式引入显示扫描件文件-->
			
		</div>
		<!--函件类型区-->
		<div id="sendCommonId" style="display: none">
			<table class=common>
				<tr  class= common>
			  		<td  class= title id="Objct1">下发对象</td>
			  		<td  class= input id="Objct2">
						<input class="codeNo" name="sendObjct" readOnly ondblclick="return showCodeList('sendObject', [this,sendObjctName], [0,1]);" onkeyup="return showCodeListKey('sendObject', [this,sendObjctName], [0,1]);" ><input class="codeName" name="sendObjctName"   readonly>
			  		</td>
			  		
			  		<td  class= title id="lettertype1">函件类型</td>
			  		<td  class= input id="lettertype2">
						<input class="codeNo" name="letterSubType" ondblclick="return showCodeList('letterType', [this,letterTypeSubName], [0,1]);" onkeyup="return showCodeListKey('letterType', [this,letterTypeSubName],[0,1]);" ><input class="codeName" name="letterTypeSubName"  readonly>
			  		</td>
			  		<td  class= title>地址选择</td>
			  		<td  class= input>
						<input class="codeNo" name="addressNo" readonly ondblclick="return showCodeList('addressNo', [this,addressNoName], [0,1], null, fm.customerNo.value, 'customerNo', 1);" onkeyup="return showCodeListKey('addressNo', [this,addressNoName],[0,1], null, fm.customerNo.value, 'customerNo', 1);" ><input class="codeName" name="addressNoName"  readonly>
			  		</td>
				</tr>
			</table>
		</div>
		
		<!--函件察看编辑区-->
		<div id="createLetterId" style="display: none">
			<!--新建函件时函件信息的录入出，同时在选择函件时存储非核保函件的函件信息，为预览做准备-->
			<textarea class= common name="letterInfo" id="letterInfoId" cols="133" rows="15" verify="函件内容|len<1000"></textarea>
			<!--显示函件图片-->
			<iframe id="pictureId" src="" width="100%" height="0" MARGINWIDTH=0 MARGINHEIGHT=0 scrolling="auto" frameborder="0"></iframe>
			<table class= common>
				<tr  class= common>
			  		<td  class= title>是否回销</td>
			  		<td  class= input>
						<input class="codeNo" name="backFlag"  readonly CodeData="0|^1|是^0|否" ondblclick="return showCodeListEx('backFlag1', [this,backFlagName], [0,1]);" onkeyup="return showCodeListKey('backFlag1', [this,backFlagName],[0,1]);" verify="是否回销|notnull&code:backFlag1"><input class="codeName" name="backFlagName"  elementtype="nacessary" readonly>
			  		</td>
  			  		<td  class= title id="bakeDateID">应回销时间</td>
  			  		<td  class= input id="bakeDateID2">
  						<Input class="coolDatePicker" name="backDate" id="backDate" DateFormat="short" verify="应回销时间|date">
  			  		</td>
		  			<td  class= input colspan=2 align=right>
		  				<INPUT VALUE="取  消" TYPE=button Class="cssButton" name="cancelLetter" onclick="cancel();">
						<INPUT VALUE="预  览" TYPE=button Class="cssButton" name="previewLetter" onclick="preview();">
						<INPUT VALUE="保  存" TYPE=button Class="cssButton" name="saveLetter" onclick="saveDate();">
		  			</td>
				</tr>
			</table>
		</div>
			
			<!--<div id="showLetterId" style="display: none">
				<iframe id="pictureId" src="" width="100%" height="0" MARGINWIDTH=0 MARGINHEIGHT=0 scrolling="auto" frameborder="0"></iframe>
				<textarea name="picture" classs ="common" cols="133" rows="15">sdfasd</textarea>
				<table class= common>
					<tr  class= common>
				  		<td  class= title>是否回销</td>
				  		<td  class= input>
							<select name="backFlag" style="width: 100; height: 23"" >
								<option value = "1">是</option>
								<option value = "0">否</option>
							</select>&nbsp;
				  		</td>
				  		<td  class= title>回销时间</td>
				  		<td  class= input>
							<Input class="coolDatePicker" name="backDate" value="" DateFormat="short">
				  		</td>
				  		<td  class= input colspan=2>
							<INPUT VALUE="预  览" TYPE=button Class="cssButton" name="previewLetter" onclick="preview();">
				  		</td>
					</tr>
				</table>
			</div>-->
		
		<!--客户答复区-->
		
 
    <div id= "divAnwser" style= "display: 'none'">
    	<table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCustomerAnwser);">
        </td>
        <td class= titleImg>客户答复</td>
      </tr>
    </table>
    <div id= "divCustomerAnwser" style= "display: '	'">
      <table  class= common>
        <tr class= common>
          <td text-align: left colSpan=1>
            <span id="spanCustomerAnwserGrid" >
            </span> 
          </td>
        </tr>
      </table>	
      <div id= "divPage3" align=center style= "display: 'none' ">
      <input value="首  页" class=cssButton type="button" onclick="getFirstPage();"> 
      <input value="上一页" class=cssButton type="button" onclick="getPreviousPage();"> 					
      <input value="下一页" class=cssButton type="button" onclick="getNextPage();"> 
      <input value="尾  页" class=cssButton type="button" onclick="getLastPage();">      
     </div>		    
     <br>
     </div> 
      </div> 
     <div id= "saveAnwser" align=center style= "display: 'none' ">
     	<INPUT VALUE="保  存" TYPE=button Class="cssButton" onclick="saveCustomerAnswer();">
    </div>  
     
   



		<div id="feedBack" style="display: none">
			<table>
	    		<tr> 
	    		  <td width="17"> 
	    		  	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divfeedBack);"></td>
	    		  <td width="185" class= titleImg>反馈意见 </td>
	    		</tr>
			</table>
			<Div  id= "divfeedBack" style= "display: ''" align=center>  
				<table class= common>
					<tr  class= common>
						<td class=common colspn=2>
							<textarea class= common name="feedBackInfo" verify="反馈意见|len<100" cols="118%" rows="3"></textarea> 
						</td>
					</tr>
					<tr  class= common id = "idSaveFeedBack">
						<td class=common align="right">
							<INPUT VALUE="取  消" TYPE=reset Class="cssButton" onclick="cancelFeedBack();">
							<INPUT VALUE="保  存" TYPE=button Class="cssButton" onclick="saveFeedBack();">
						</td>
						<td class=common>
						</td>
					</tr>
				</table>
			</Div>
		</div>
		
		<input type=hidden name="customerNo" value="">
		<input type=hidden name="workNo" value="">
		<input type=hidden name="letterType" value="">
		<input type=hidden name="operateType" value="">
		<input type=hidden name="takeBackType" value="">
		<input type=hidden name="takeBackType1" value="">
		<input type=hidden name="showLetterFlag" value="">
		<input type=hidden name="SerialNumber" value="">
		<input type=hidden name="riskcode" value="">
	</Form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
