<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import="java.util.*"%> 
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head >
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLSuveryConfList.js"></SCRIPT>
   <%@include file="LLSuveryConfListInit.jsp"%>
   <script language="javascript">
   function initDate(){
   fm.SurveyStartDate.value="<%=afterdate%>";
   fm.SurveyEndDate.value="<%=CurrentDate%>";
   var usercode="<%=Operator%>";
   fm.comcode.value="<%=Comcode%>";
   fm.Inspector.value=usercode;

   var strSQL="select username from llclaimuser where usercode='"+usercode+"'"
   	+" union select username from llsocialclaimuser where usercode='"+usercode+"'";
   	
   var arrResult = easyExecSql(strSQL);
		if(arrResult != null)
		{
         fm.InspectorName.value= arrResult[0][0];
 		}
   }
   </script>
 </head>

 <body  onload="initDate(); initForm();" >
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/InputButton.jsp"%>
      <table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
         </TD>
         <TD class= titleImg>
         调查工作列表
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput1" style= "display: ''">
       <table  class= common>
<TR  class= common8>
<TD  class= title8>调查审核人</TD><TD  class= input8><input class= codeno name="Inspector" onclick="Dispatch();return showCodeList('optname',[this,InspectorName], [0,1],null,Str1,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,InspectorName], [0,1],null,Str1,'1');"  onkeydown="QueryOnKeyDown()"><input class=codename name= InspectorName></TD>
<TD  class= title8>调查状态</TD><TD class= input8><Input class= "codeno"  name=SurveyFlag CodeData= "0|^0|未回复^1|已回复" onkeydown="QueryOnKeyDown();" onclick="return showCodeListEx('surveyflag',[this,SurveyFlagName], [0,1]);" onkeyup="return showCodeListKeyEx('surveyflag', [this,SurveyFlagName], [0,1]);" ><input class= codename name='SurveyFlagName'></TD>

</TR>
<TR  class= common8>
<TD  class= title8>调查回复人</TD><TD  class= input8><input class= codeno name="Surveyer" onclick="Dispatch();return showCodeList('optname',[this,SurveyerName], [0,1],null,Str0,'1');" onkeyup="Dispatch();return showCodeList('optname',[this,SurveyerName], [0,1],null,Str0,'1');"  onkeydown="QueryOnKeyDown()"><input class=codename name= SurveyerName></TD>
<TD  class= title8>调查机构</TD>
 <TD  class= title8><Input class="codeno" name=MngCom verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,100);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,100);"><input class=codename name=ComName></TD>
          
</TR>
<TR  class= common8>
<TD  class= title8>案件号</TD><TD  class= input8><input class= common name="OtherNo" onkeydown="QueryOnKeyDown()"></TD>
<TD  class= title8>调查类别</TD><TD  class= input8><input class= codeno name="SurveyType" CodeData= "0|^0|即时调查^1|一般调查^2|再次调查" onClick="showCodeListEx('SurveyType',[this,SurveyTypeName],[0,1],'', '', '', 1);" onkeyup="showCodeListKeyEx('SurveyType',[this,SurveyTypeName],[0,1],'', '', '', 1);"><input class=codename name=SurveyTypeName></TD> 
</TR>
<TR  class= common8>
<TD  class= title8>提起日期从</TD><TD  class= input8><input class= 'coolDatePicker' dateFormat="Short" name="SurveyStartDate" onkeydown="SurveyQuery();"></TD>
<TD  class= title8>到</TD><TD  class= input8><input class= 'coolDatePicker' dateFormat="Short" name="SurveyEndDate" onkeydown="SurveyQuery();"></TD>
</TR>
<TR  class= common8>
<TD  class= title8>分配日期从</TD><TD  class= input8><input class= 'coolDatePicker' dateFormate="Short" name="AnswerType" ></TD>
<TD  class= title8>到</TD><TD  class= input8><input class= 'coolDatePicker' dateFormate="Short" name="LogerNo"></TD>
</TR>

</table>
</DIV>
    <Div  id= "divCustomerInfo" align=center style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
		  </table>
	<INPUT VALUE="首页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
         <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
         <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
         <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 	
    </Div>
    <br>
          <input name="AskIn" style="display:''"  class=cssButton type=button value="审  核" onclick="SurveyReply()">
          <input name="InqFee" style="display:''"  class=cssButton type=button value="直接调查费" onclick="SurveyFee()">
          <input name="Additional" style="display:''"  class=cssButton type=button value="间接调查费" onclick="AddFee()">
          <input name="SumUp" style="display:''"  class=cssButton type=button value="调查费用汇总" onclick="SumFee()">
             
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="tSurveyNo" >
     	<Input type="hidden" class= common name="comcode" >
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
