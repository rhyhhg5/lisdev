<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：InsuredUWInfoInput.jsp
//程序功能：被保人核保信息界面
//创建日期：2005-01-06 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=tGI.ComCode%>";//记录登陆机构
	var ContNo = "<%=request.getParameter("ContNo")%>";
	var InsuredNo = "<%=request.getParameter("InsuredNo")%>"
	var SendFlag = "<%=request.getParameter("SendFlag")%>"
	var CaseNo = "<%=request.getParameter("CaseNo")%>"
	var BatNo = "<%=request.getParameter("BatNo")%>"

</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LLSecondUWRisk.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLSecondUWRiskInit.jsp"%>
  <title>险种信息 </title>
</head>
<body  onload="initForm();" >
<form method=post name=fm target="fraSubmit" action= "./LLSecondUWRiskChk.jsp">

<TABLE class= common>
    <TR class= common>
        <TD  class= title>
            <DIV id="divContPlan" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            保险计划
                    </TD>
                    <TD  class= input>
                        <Input class="code" name="ContPlanCode" ondblclick="showCodeListEx('ContPlanCode',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ContPlanCode',[this],[0],'', '', '', true);">
                    </TD>

                    </TR>
	            </TABLE>
            </DIV>
        </TD>
        <TD  class= title>
            <DIV id="divExecuteCom" style="display:'none'" >
	            <TABLE class= common>
		            <TR class= common>
			            <TD  class= title>
                            处理机构
                        </TD>
                        <TD  class= input>
                            <Input class="code" name="ExecuteCom" ondblclick="showCodeListEx('ExecuteCom',[this],[0],'', '', '', true);" onkeyup="showCodeListKeyEx('ExecuteCom',[this],[0],'', '', '', true);">
                        </TD>
		            </TR>
	            </TABLE>
            </DIV>
        </TD>
        <TD  class= title>
        </TD>
    </TR>
</TABLE>

<DIV id=DivOldInfo STYLE="display:'none'">
    <table class= common border=0 width=100%>
    	 <tr>
	        <td class= titleImg align= center>既往信息：</td>
	     </tr>
    </table>
    <table  class= common>
       <tr  class= common>
      	 <td text-align: left colSpan=1>
  					<span id="spanOldInfoGrid" >
  					</span>
  			 </td>
  		 </tr>
    </table>
</DIV>





<DIV id=DivLCPol STYLE="display:''">
    <table class= common border=0 width=100%>
    	 <tr>
	        <td class= titleImg align= center>险种信息：</td>
	     </tr>
    </table>
    <table  class= common>
       <tr  class= common>
      	 <td text-align: left colSpan=1>
  					<span id="spanRiskGrid" >
  					</span>
  			 </td>
  		 </tr>
    </table>
</DIV>
<hr></hr>
<table>
<tr>
  <td>        
          <input value="加费承保录入" class=cssButton type=button name= "AddFee"  onclick="showAdd();">
          <input value="特约承保录入" class=cssButton type=button onclick="showSpec();">
  </td>
</tr>
</table>

<div id = "divUWResult" style = "display: ''">
    	  <!-- 核保结论 -->
    	  <table class= common border=0 width=100%>
    	  	<tr>
			<td class= titleImg align= center>险种核保结论：</td>
	  	</tr>
	  </table>
	   		 	   	
  	  <table  class= common align=center>
    	  			
    	  	<TR >

          		<TD class= title>
          		 	<!--span id= "UWResult"> 保全核保结论 <Input class="code" name=uwstate value= "1" CodeData= "0|^1|本级保单^2|下级保单" ondblClick="showCodeListEx('uwstate',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('uwstate',[this,''],[0,1]);">  </span-->
          		 	核保结论
          		 </td>
          		 <td class=input>
          		 <Input class="code" name=uwstate ondblclick="return showCodeList('uwstate',[this]);" onkeyup="return showCodeListKey('uwstate',[this]);">
	   		 	   </TD>
	   		 	  
	   		 	  
	   		 	   </div>
	   		 	   <td class=input>
	   		 	   </td>
	   		 	   <td class=input>
	   		 	   </td>
          	</TR>
          </table>
         <table class=common>
		
		<tr>
		      	<TD height="24"  class= title>
            		核保意见
          	</TD>
          	<td></td>
          	</tr>
		<tr>
      		<TD  class= input> <textarea name="UWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
      		<td></td>
      		</tr>
	  </table>
	  <div id =divUWButton1 style="display:''">
	  <p>
    		<INPUT VALUE="确  定" class=cssButton TYPE=button onclick="submitForm(1);">
    		<INPUT VALUE="取  消" class=cssButton TYPE=button onclick="cancelchk();">
    		 <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="InitClick();">
  	</p>
  </div>
</div>

<DIV id=DivButton STYLE="display:''">
<!--table>
<tr>
	<td>
          <input value="体检资料查询" class=cssButton type=button onclick="showHealthQ();" >
          <INPUT VALUE="生存调查查询" class=cssButton TYPE=button onclick="RReportQuery();">
          <input value="体检资料录入" class=cssButton type=button onclick="showHealth();" width="200">
          <input value="生调请求说明" class=cssButton type=button onclick="showRReport();">

  </td>
</tr>
</table-->
  <Input name = CaseNo type=hidden>
  <Input name = PolNo type=hidden>
  <Input name = flag type = hidden>
  <Input name = BatNo type = hidden>

</DIV>
	
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
