<html>
  <%
  /*******************************************************************************
  * Name     :ICaseCureInput.jsp
  * Function :立案－费用明细信息的初始化页面程序
  * Author   :Xx
  * Date     :2005-7-23
  */
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>

  <head >
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <SCRIPT src="./CaseCommon.js"></SCRIPT>
    <SCRIPT src="ICaseCureCheckInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file="ICaseCureCheckInputInit.jsp"%>
  </head>
  <body  onload="initForm();initElementtype();" >
    <form action="./ICaseCureSave.jsp" method=post name=fm target="fraSubmit">

      <!-- 基本帐单信息 -->
       <%@include file="LLRemark.jsp"%>
      <table>
        <tr>
          <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divICaseCure);">
          </td>
          <td class= titleImg>
            账单信息明细
          </td>
        </tr>
      </table>

      <Div  id= "divICaseCure" style= "display: ''">
        <table  class= common>
          <TR  class= common>
            <TD  class= title8>理赔号</TD>
            <TD  class= input8> <input class="common4"    name=CaseNo onkeydown= "QueryOnKeyDown()"></TD>
            <TD  class= title8>处理人</TD>
            <TD  class= input8> <input class="readonly" readonly   name=cOperator></TD>
            <TD  class= title8>处理日期</TD>
            <TD  class= input8> <input class="readonly" readonly   name=MakeDate></TD>
            <TD  class= input8>管理机构</TD>
            <TD  class= input8><Input class="fcodeno" name=MngCom verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1,180);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1,180);"><input class=fcodename name=ComName></TD>
          </tr>
          <TR  class= common>
            <TD  class= title8>客户姓名</TD>
            <TD  class= input8> <input class="common4" readonly   name=CustomerName></TD>

            <TD  class= title8>客户号码</TD>
            <TD  class= input8> <input class="common4" readonly   name=CustomerNo></TD>
            <TD  class= title8>性别</TD>
            <TD  class= input8> <input class="code" name="CustomerSex" type=hidden> <input class="common4" readonly   name=CustomerSexName></TD>
            <TD  class= title8>年龄</TD>
            <TD  class= input8 > <input class=common4  name=Age></TD>
          </TR>
          <TR class= common id='titleS1' style="display:'none'">
            <TD  class= title8>身份证号</TD>
            <TD  class= input8 > <input class=common4 readonly name=IDNo></TD>
            <TD  class= title8>参保人员类别</TD>
            <TD  class= input8 ><input class=codeno name=InsuredStat CodeData="0|2^1|在职^2|退休" onclick="return showCodeListEx('insurestat',[this,InsuredStatName],[0,1]);" onkeyup="return showCodeListKeyEx('insurestat',[this,InsuredStatName],[0,1]);"><input class=codename  name=InsuredStatName></TD>
            <TD  class= title8 >客户社保号</TD>
            <TD  class= input8 > <input class=common4 name=SecurityNo></TD>
            <TD  class= title8>单位社保登记号</TD>
            <TD  class= input8 > <input class=common4 name=CompSecuNo></TD>
          </TR>
          <TR  class= common>
            <TD  class= title8>医院名称</TD>
            <TD  class= input8> <input class=common4  name=HospitalName elementtype=nacessary verify="医院名称|notnull"></TD>
            <TD  class= title8>医院代码</TD>
            <TD  class= input8> <input class=code  name=HospitalCode onclick="return showCodeList('llhospiquery',[this,HospitalName,FixFlag,HosGrade,HosGradeName],[0,1,2,3,4],null,fm.HospitalName.value,'Hospitname',1,240);" onkeyup="return showCodeListKeyEx('llhospiquery',[this,HospitalName,FixFlag,HosGrade,HosGradeName],[0,1,2,3,4],null,fm.HospitalName.value,'Hospitname',1,240);" elementtype=nacessary verify="医院代码|notnull"></TD>
            <TD  class= title8>医院属性</TD>
            <TD  class= input8> <input class=common4  name=FixFlag readonly></TD>
            <TD  class= title8>医院级别</TD>
            <TD  class= input8 > <input name=HosGrade type=hidden><input  readonly class=common4  name=HosGradeName></TD>
          </TR>
          <TR class= common id='titleS2' style="display:'none'">
          </TR>
          <TR  class= common>
            <TD  class= title8>账单属性</TD>
            <TD  class= input8><input class="fcodeno" name="FeeAtti" CodeData="0|3^0|医院^1|社保结算^2|手工报销^3|社保补充" onclick="return showCodeListEx('feeatti',[this,FeeAttiName],[0,1]);" onkeyup="return showCodeListKeyEx('feeatti',[this,FeeAttiName],[0,1]);"><input class=fcodename  name=FeeAttiName  elementtype=nacessary verify="账单属性|notnull"></TD>
            <TD  class= title8>账单种类</TD>
            <TD  class= input8><input class="fcodeno" name="FeeType"  CodeData="0|2^1|门诊^2|住院" onclick="return showCodeListEx('FeeType',[this,FeeTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeType',[this,FeeTypeName],[0,1]);"><input class=fcodename name=FeeTypeName  elementtype=nacessary verify="账单种类|notnull"></TD>
            <TD  class= title8>帐单类型</TD>
            <TD  class= input8><input class="fcodeno" name="FeeAffixType" CodeData="0|2^0|原件^1|复印件" onclick="return showCodeListEx('FeeAffixType',[this,FeeAffixTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('FeeAffixType',[this,FeeAffixTypeName],[0,1]);"><input class=fcodename  name=FeeAffixTypeName></TD>
            <TD  class= title8>城区属性</TD>
            <TD  class= input8 > <input name=UrbanFlag type=hidden><input  readonly class=common4  name=UrbanFlagName></TD>
          </TR>
          <TR  class= common id='titleC1'>
            <TD  class= title8>账单号码</TD>
            <TD  class= input8> <input class=common4  name=ReceiptNo elementtype=nacessary></TD>
            <TD  class= title8 >住院号</TD>
            <TD  class= input8 > <input class=common4 name=inpatientNo></TD>
            <TD  class= title8 colspan=2>账单合计金额 <input class=readonly readonly  name=SumFee ></TD>
            <TD  class= title8> </TD>
            <TD class= input8 > <input class=readonly readonly style="width:120px"></TD>
          </TR>
          <TR  class= common id='titleC2'>
            <TD  class= title8>结算日期</TD>
            <TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=FeeDate onblur="fillDate()" elementtype=nacessary verify="结算日期|date&notnull"></TD>
            <TD  class= title8 id='titleInpatient' style="display:''">入院日期</TD>
            <TD  class= title8 id='titleStart' style="display:'none'">治疗开始</TD>
            <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=HospStartDate onblur="caldate()"  verify="日期|date"></TD>
            <TD  class= title8 id='titleOutpatient' style="display:''">出院日期</TD>
            <TD  class= title8 id='titleEnd' style="display:'none'">治疗结束</TD>
            <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=HospEndDate onblur="caldate()" verify="日期|date"></TD>
            <TD  class= title8 >治疗天数</TD>
            <TD  class= input8> <input class=common4  name=RealHospDate onfocus="caldate()"></TD>
          </TR>
        </table>
      </div>
      <div id="divReceipt" style= "display: ''">
        <%@include file="HospReceipt.jsp"%>
      </div>
      <hr>

      <div align='left'>
        <INPUT VALUE="查看前一张账单" class=cssButton Type=button onclick="zdPrevious();">
        <INPUT VALUE="查看下一张账单" class=cssButton Type=button onclick="zdNext();">
      </div>

      <div id="divconfirm" align='right'>
        <INPUT VALUE="确  认" class=cssButton TYPE=button onclick="saveFee();">
        <INPUT VALUE="扣除明细" class=cssButton TYPE=button onclick="openDrug();">
        <INPUT VALUE="生成手工报销单" class=cssButton TYPE=button onclick="getSGBXD();">
      </div>
<hr>
      <div align='right' id=Control>
        <INPUT VALUE="账单重置" class=cssButton TYPE=button onclick="zdReset();">
        <INPUT VALUE="录入完毕" class=cssButton TYPE=button onclick="zdComplete();">
        <INPUT VALUE="下一步" class=cssButton id=idNext name=idNext TYPE=button onclick="nextDeal();">
        <INPUT VALUE="返回" class=cssButton TYPE=button onclick="backDeal();">
      </div>
      <div id="divCaseduty" style="display: ''">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,titleCaseduty);">
            </td>
            <td class= titleImg>
              待理算责任
            </td>
          </tr>
        </table>
        <Div  id= "titleCaseduty" style= "display: ''">
          <table  class= common>
            <tr  class= common>
              <td class= title8 text-align: center colSpan=1>
                <span id="spanDutyRelaGrid"  align=center>
                </span>
              </td>
            </tr>
          </table>
        </div>
      </div>

      <Input type="hidden"  name="RgtNo" >
      <Input type="hidden"  name="SeriousWard" >
      <Input type="hidden" name="cOperate" >
      <input type="hidden" name="FirstInHos">
      <Input type="hidden" name="MainFeeNo">
      <Input type="hidden" name="Case_RgtState">
      <input type="hidden" name="LoadC">
      <input type="hidden" name= "FeeDutyFlag">
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </form>

  </body>
</html>