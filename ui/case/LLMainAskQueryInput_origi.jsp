<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLMainAskQueryInput.js"></SCRIPT>
   <%@include file="LLMainAskQueryInit.jsp"%>
   <script language="javascript">
   function initDate(){
   		fm.RgtDateS.value="<%=afterdate%>";
   		fm.RgtDateE.value="<%=CurrentDate%>";
   		var usercode="<%=Operator%>";
   		var comcode="<%=Comcode%>";
//   		fm.Operator.value=usercode;
//   		fm.OrganCode.value=comcode;
//   		
//   		
//   		var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
//   		var arrResult = easyExecSql(strSQL);
//			if(arrResult != null)
//			{
//			         fm.optname.value= arrResult[0][0];
// 			}
   }
   </script>
 </head>
<body  onload="initDate();initForm();">
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">

<table>
        <TR>
         <TD class= titleImg>
         请录入查询条件
         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
       	<TR class = common>
       		<TD  class= title>客户姓名</TD>                                                             
       		<TD  class= input><Input class=common8 name=CustomerName onkeydown="QueryOnKeyDown()"></TD> 
       		<TD  class= title>客户号</TD>                                                               
       		<TD  class= input><Input class=common name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>    
       		<TD  class= title></TD>
       		<TD  class= input></TD>
       	</TR>
				<TR  class= common>	
					<TD  class= title>咨询通知号</TD>
					<TD  class= input> <Input class=common8 name=ConsultNo  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>保单号</TD>
					<TD  class= input><Input class=common8 name=ContNo onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>身份证号</TD>
					<TD  class= input><Input class=common name=IDNo onkeydown="QueryOnKeyDown()"></TD>
				</TR>
				<TR  class= common8 >
					<TD  class= title>管理机构</TD>
					<TD  class= input><Input class= "code8"  name=OrganCode onkeydown="QueryOnKeyDown();" onclick="getstr();return showCodeList('stati',[this], [0],null,str,'1');" onkeyup="getstr();return showCodeListKey('stati', [this], [0],null,str,'1');" >
					<TD  class= title>操作员姓名</TD>
					<TD  class= input><Input class= "code8"  name=optname onkeydown="QueryOnKeyDown();" onclick="return showCodeList('optname',[this, Operator], [0, 1],null,fm.optname.value,'username','1');" onkeyup="return showCodeListKey('optname', [this, Operator], [0, 1],null,Str,'1');" >
					<TD  class= title>操作员代码</TD>
					<TD  class= input><Input class="readonly" readonly name=Operator></TD>
				</TR>
				<TR>
					<TD  class= title>类型</TD>
	        <TD  class= input8><Input  CodeData="0|2^0|咨询^1|通知" onClick="return showCodeListEx('asktype',[this,AskTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('asktype',[this,AskTypeName],[0,1]);" class=codeno name="AskType" onkeydown="QueryOnKeyDown()"><Input class= codename name=AskTypeName onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>受理日期从</TD>
					<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>到</TD>
					<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
				</TR>
       </table>
     </Div>
     <hr>
 <!--  <input style="display:''"  class=cssButton type=button value="查 询" onclick="easyQuery()"> -->
    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1 align=center>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>
       
      
    </Div>
  </br>
   <Div  align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>    
<!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
