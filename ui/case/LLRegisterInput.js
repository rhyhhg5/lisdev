
/*******************************************************************************
* Name: LLRegisterInput.js
* Function:立案主界面的函数处理程序
* Author:LiuYansong
*/
var showInfo;
var mode_flag;//判断保险金的领取方式，若是4或者7则对银行进行判断。
var tAppealFlag="0";//申诉处理Flag
var turnPage = new turnPageClass();        //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();   

//#1769 案件备注信息的录入和查看功能 add by Houyd
function showCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}
	//#1769 案件备注信息的录入和查看功能 自动弹出页面只弹一次
	//获取session中的ShowCaseRemarkFlag
	var tShowCaseRemarkFlag = fm.ShowCaseRemarkFlag.value;
	//alert(tShowCaseRemarkFlag);	
	if(tShowCaseRemarkFlag == "" || tShowCaseRemarkFlag == null || tShowCaseRemarkFlag == "null"){
		//tShowCaseRemarkFlag = "1";//准备存入session的标记:0-未弹出；1-已弹出
		//alert(tShowCaseRemarkFlag);		
	}else{
		return ;
	}	
	var tSql = "select 1 from llcaseremark where caseno='"+aCaseNo+"' with ur";
	var tResult = easyExecSql(tSql);
	if(tResult != null){
		pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  		showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
  	}	
	if(fm.RgtType.value=="13"){
		  fm.LowerQuestion.disabled=false;
	  }else {
		  fm.LowerQuestion.disabled=true;
	  }
}
function selMess(name,mng,ope){
	if(mng==null){
	alert("当前登录机构为空");
	return false;
	}
	if(ope==null){
	alert("当前操作员为空");
	return false;
	}
	var localObj = window.location;
//	alert(localObj);
	var contextPath = localObj.pathname.split("/")[1];
//	alert(contextPath);
	var basePath = localObj.protocol+"//"+localObj.host;
	var server_context=basePath;
//	alert("server_context:"+server_context);
	var context=server_context+'/easyscan5/EasyScanAdapter.jsp';
//	alert(context);
	jsinit(context,mng,ope);
	if(name=='selbank'){
//		alert("银行卡："+jscall('02'));
		var bankno=jscall('02');
		fm.BankAccNo.value=bankno;
	}else if(name=='selcard'){
//		alert("身份证："+jscall('01'));
		var card=jscall('01');
		fm.tIDNo.value=card;
		
	}
}
function openCaseRemark(aCaseNo){
	var varSrc = "";
	if(aCaseNo != '' && aCaseNo != null){
		varSrc = "&CaseNo=" + aCaseNo;
	}else{
		varSrc = "&CaseNo=" + fm.CaseNo.value;
		aCaseNo = fm.CaseNo.value;
	}

	pathStr="./LLCaseRemarkMain.jsp?Interface=LLCaseRemarkInput.jsp"+varSrc;
  	showInfo = OpenWindowNew(pathStr,"LLCaseRemarkInput","middle",850,500);
}
    
/*******************************************************************************
* Name        :main_init
* Author      :LiuYansong
* CreateDate  :2003-12-16
* ModifyDate  :
* Function    :初始化主程序
*/
function main_init(A_jsp,Target_Flag,T_jsp,Info){
  var i = 0;
  var showStr = "正在"+Info+"数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action ="./"+A_jsp;
  //alert("target标志是"+Target_Flag);
  if(Target_Flag=="1"){
    fm.target = "./"+T_jsp;
  }
  else{
    fm.target = "fraSubmit";
  }
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
fm.IdAppConf.disabled=false;
  showInfo.close();
  
  if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var strsql = "select a.RgtState,b.codename,a.rigister,a.MakeDate from LLcase a, ldcode b where a.caseno='"+fm.CaseNo.value +"'"
    +" and b.codetype='llrgtstate' and b.code=a.rgtstate";
    arrResult = easyExecSql(strsql);

    if ( arrResult ){
      fm.RgtState.value= arrResult[0][1];
      fm.Handler.value= arrResult[0][2];
      //fm.ModifyDate.value= arrResult[0][4];
      fm.ModifyDate.value = getCurrentDate();

      initTitle();
	  fm.Sex.value = fm.Sex.value=='女'?'1':'0';
      ClientafterQuery( fm.RgtNo.value,fm.CaseNo.value );
    }

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    if(fm.appReasonCode[05].checked == true){
      var pathStr="./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerName.value+"&RgtNo="+fm.RgtNo.value+"&paymode="+fm.paymode.value;
      showInfo=OpenWindowNew(pathStr,"EventInput","middle");
    }
  //alert(fm.OpType.value);
  if(fm.OpType.value=="OK")
   {
    //执行下一步操作(发送email、短信)
  	var ExeSql = "select returnmode,rgtantmobile,email,rgtantname from llregister where rgtno='"+fm.RgtNo.value+"'";
  	arr=easyExecSql(ExeSql);
  	if(arr)
  	{ //alert(arr[0][1]);
  		//alert(arr[0][2]);
  	  //fm.AnswerMode.value=arr[0][0];
  	  fm.Mobile.value=arr[0][1];
  	  fm.Email.value=arr[0][2];
  	  
  	  fm.SMSContent.value="尊敬的"+arr[0][3]+"先生/女士,您的理赔申请已受理。"
  	                      +"如有疑问，敬请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
  	                      +"发信人：中国人保健康"
  	  fm.EmailContent.value="尊敬的"+arr[0][3]+"先生/女士,您的理赔申请已受理。"
  	                      +"如有疑问，敬请拨打95518（北京）或4006695518（全国其他地区）进行咨询。"
  	                      +"发信人：中国人保健康"
  	  fm.action="./GrpSendMail.jsp";
  	  fm.submit();
  	}
   } 	  
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try{
    initForm();
  }
  catch(re){
    alert("在Register.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
  if(cDebug=="1"){
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else{
    parent.fraMain.rows = "0,0,0,0,*";
  }
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";
  }
}

//打印理赔申请书的函数
function PLPsqs(){
  main_init("PLPsqs.jsp","1","f1print","打印");
  showInfo.close();
}

function dealCaseGetMode(){
  mode_flag="1";
  if(fm.CaseGetMode.value=="4"||fm.CaseGetMode.value=="7")
  {
    if(fm.BankCode.value=="")
    {
      alert("请您录入开户银行的信息！！！");
      mode_flag="0";
      return;
    }
    if(fm.AccName.value=="")
    {
      alert("请您录入户名信息！！！！");
      mode_flag="0";
      return;
    }
    if(fm.BankAccNo.value=="")
    {
      alert("请您录入账户信息！！！");
      mode_flag="0";
      return;
    }
  }
}

function ClientQuery(){
  var  wherePart = getWherePart("a.InsuredNo", 'CustomerNo' )
  + getWherePart("a.name", 'CustomerName' )
  + getWherePart( 'a.ContNo','ContNo' )
  + getWherePart('a.OthIDNo','OtherIDNo')
  + getWherePart('a.IDNo','tIDNo');
  if ( wherePart == ""){
    alert("请输入查询条件");
    return false;
  }
  wherePart = wherePart + getWherePart("a.idtype", 'tIDType');
  var strSQL0 ="select distinct a.insuredno,a.name,a.sex,a.birthday,"
  +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=a.IDType),"
  +"a.idno,'' ,'',a.IDType,a.OthIDNo,a.OthIDType,a.addressno,"
  //#2329 （需求编号：2014-229）客户基本信息要素录入需求
  +"codename('sex',a.sex),codename('nativeplace',a.nativeplace),codename('occupationtype',a.occupationtype),a.IDStartDate,a.IDEndDate ";
  var sqlpart1="from lcinsured a,lccont b where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' ";
  var sqlpart2="from lbinsured a,lbcont b where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' ";
  var sqlpart3="from lcinsured a,lccont b,llregister c where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' and b.GrpContNo=c.RgtObjNo and c.RgtNo='"+fm.RgtNo.value+"'";
  var sqlpart4="from lbinsured a,lbcont b,llregister c where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' and b.GrpContNo=c.RgtObjNo and c.RgtNo='"+fm.RgtNo.value+"'";
  var strSQL;
  if (fm.RgtNo.value==null||fm.RgtNo.value==""){
    strSQL=strSQL0+sqlpart1+ wherePart+" union "+strSQL0+sqlpart2+wherePart;
  }else{
    strSQL=strSQL0+sqlpart3+ wherePart+" union "+strSQL0+sqlpart4+wherePart;    
  }
  //+ " and b.AddressNo= a.AddressNo ";
  var arr= easyExecSql( strSQL,1,0,1 );


  if ( arr==null ){
    alert("没有符合条件的数据");
    return false;
  }
//  try{
    //如果查询出的数据是一条，显示在页面，如果是多条数据填
    //出一个页面，显示在mulline中
    if(arr.length==1){
      afterLLRegister(arr);
    }
    else{
      var varSrc = "&CustomerNo=" + fm.CustomerNo.value;
      varSrc += "&CustomerName=" + fm.CustomerName.value;
      varSrc += "&ContNo=" + fm.ContNo.value;
      varSrc += "&IDType=" + fm.tIDType.value;
      varSrc += "&IDNo=" + fm.tIDNo.value;
      varSrc += "&OtherIDNo=" + fm.OtherIDNo.value;
      if (fm.RgtNo.value!=null){
          varSrc += "&RgtNo=" + fm.RgtNo.value;
      }
      showInfo = window.open("./FrameMainPersonQuery.jsp?Interface= LLPersonQuery.jsp"+varSrc,"RgtSurveyInput",'width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
    }
 
      var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
                  + fm.CustomerNo.value+"'";
      var	crrResult1 = easyExecSql(strSQLx);
      if(crrResult1){
      	alert("请务必查询相关客户信息");
      }
//}catch(ex){
}

function openAffix(){
	if (fm.CaseNo.value==null||fm.CaseNo.value=="") {
		alert("未获得理赔号！");
		return false;
	}
  var varSrc ="";
  pathStr="./CaseAffixMain.jsp?CaseNo="+fm.CaseNo.value+"&RgtNo="+fm.RgtNo.value+"&LoadFlag=1&LoadC="+fm.LoadC.value;
  showInfo = OpenWindowNew(pathStr,"","middle");
}

function submitFormSurvery(){
  strSQL="select otherno from LLSurvey where otherno='"+fm.CaseNo.value+"'";
  arrResult = easyExecSql(strSQL);
  if(arrResult){
    if(!confirm("该案件提起过调查，您确定要继续吗？")){
      return false;
    }
  }
  else{  }
  if((fm.CustomerNo.value==""||fm.CustomerNo.value==null)||
  (fm.CustomerName.value==""||fm.CustomerName.value==null))
  {
  	alert("请录入客户号码!");
  	return false;
  }
  
  //TODO:临时校验，社保案件不可提起调查,需要通过案件号判断受理的案件
//  	var tSocial = fm.CaseNo.value;
//	if(!checkSocial('',tSocial,'')){
//		return false;
//	}

  		 
  //TODO:新增效验，理赔二核时不可提起调查
	var tUWSql = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"' with ur";
  	var tUWRgtState=easyExecSql(tUWSql);
	if (tUWRgtState!=null){
	     if(tUWRgtState[0][0]=='16'){
	       alert("理赔二核中不可提起调查，必须等待二核结束！");
	       return false;
	     }
	}
	
  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&InsuredNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  varSrc += "&StartPhase=0";
  varSrc += "&Type=1";

  pathStr="./FrameMainRgtSurvey.jsp?Interface=RgtSurveyInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"RgtSurveyInput","middle",800,500);
}

//TODO：理赔二核页面跳转
function submitLLUnderWrit(){

  if((fm.CustomerNo.value==""||fm.CustomerNo.value==null)||
  (fm.CustomerName.value==""||fm.CustomerName.value==null))
  {
  	alert("请录入客户号码!");
  	return false;
  }
  
  if(fm.RgtNo.value.substring(0,1) == 'P'){
		alert("团体批次下不可提起二核！");
		return false;
	}

  //TODO:临时校验，社保案件不可提起二核,需要判断业务人员是通过客户号还是保单号受理的该案件	
  if(fm.CustomerNo.value!=""&&fm.CustomerNo.value!=null&&(fm.all('ContNo').value=="" || fm.all('ContNo').value==null)){
  	var tSocial = fm.CustomerNo.value;
	if(!checkSocial(tSocial,'','insuredno')){
		return false;
	}
  }else{
  	var tSocial = fm.all('ContNo').value;
	if(!checkSocial(tSocial,'','')){
		return false;
	}
  }
  // by lyc #2157
//	var tRgtStateSql = "select rgtstate from llcase where caseno='"+fm.CaseNo.value+"'";
//	var tRgtState = easyExecSql(tRgtStateSql);
//	if(tRgtState != null){
//		if(tRgtState[0][0] == '07' || tRgtState[0][0] == '13'){
//		
//			alert("案件处于调查状态或延迟状态，不可提起二核");
//			return false;
//		}
//		if(tRgtState[0][0] == '09' || tRgtState[0][0] == '11'){
//		
//			alert("案件处于结案状态或通知状态，不可提起二核");
//			return false;
//		}
//		if(tRgtState[0][0] == '14'){
//		
//			alert("案件处于撤件状态，不可提起二核");
//			return false;
//		}
//	}
//  
  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&InsuredNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  pathStr="./FrameMainLLUnderWrit.jsp?Interface=LLUnderWritInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"LLUnderWritInput","middle",800,500);
}
/**
校验万能险死亡日期后产生续期续保应收状态
a)	若续期催收成功、正转出，则提示先到保全做万能续期回退
b)	若续保续期已抽档但未收费，系统提示“该保单有续期续保应收记录，是否继续？”，若继续操作则作废原续期续保应收记录的收费操作
*/
function checkDueFee()
{
	for(var rowNum=0;rowNum<EventGrid. mulLineCount;rowNum++)
	{
	  if(EventGrid.getChkNo(rowNum))
	  {
	  	 //受理时判断万能险的出险日期之后是否存在续期记录
	  	  var happenDate=EventGrid.getRowColData(rowNum,2);
		  var sql = "  select count(1) "
		            +" from LJSPayB a,lcpol b,ljapayperson c"
		            +" where a.otherno=b.contno and b.insuredNo = '" + fm.CustomerNo.value + "' "
		            + " and a.getnoticeno = c.getnoticeno and b.polno=c.polno"
		            + " and b.grpcontno=c.grpcontno and b.grppolno=c.grppolno and b.contno=c.contno "
		            +" and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='4') "  //万能保单
		            +" and a.dealState in ('1','5') "
		            +" and c.lastpaytodate>='"+ happenDate +"'";　
		  var rs = easyExecSql(sql);
		  if(rs && rs[0][0] >=1)
		  {
		     return confirm("万能保单出险后有续期收费记录，是否继续？");
		  }
	  }	
	}  
  return true;
}
function submitForm()
{
	var date1=fm.IDEndDate.value;
	var date=new Date();
	var date2=date.getYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
	if(date1!="" && date1!=null){
		var dateValue=compareDate(date1,date2);
		if(dateValue==2){
			fm.all('operate').value = "INSERT||MAIN";
			fm.action ="./LLIndividualSave.jsp";
			fm.submit();
			alert("被保险人"+fm.CustomerName.value+"，"+"客户号"+fm.CustomerNo.value+"，"+"证件已过期，请联系客户变更证件有效期。");
			return false;
		}
		if(dateValue==1 || dateValue == 0) {
			var dateno = dateDiff(date2,date1,"D");
			if(dateno<90) {
				fm.all('operate').value = "INSERT||MAIN";
				fm.action ="./LLIndividualSave.jsp";
				fm.submit();
				alert("被保险人"+fm.CustomerName.value+"，"+"客户号"+fm.CustomerNo.value+"，"+"证件即将过期，请提醒客户尽快变更证件有效期。");
			}
		}
	}

	// # 3067 案件赔款领取人姓名校验**start**
	var tSocialSql="Select CHECKGRPCONT(rgtobjno) from llregister where rgtno ='"+fm.RgtNo.value+"' with ur";
	var aSocial= easyExecSql(tSocialSql);
	//alert(aSocial)
	if(aSocial==null ||aSocial[0][0] !='Y' ){
		//alert("--------");
	var samplingSql="select togetherFlag from llregister where 1=1 and rgtno='"+ fm.RgtNo.value +"' with ur";
	var registerRule= easyExecSql(samplingSql);
	if(registerRule==null||registerRule[0][0]=='1'||registerRule==''){
		if (fm.DeathDate.value==null || fm.DeathDate.value=="") {
			if(fm.paymode.value!='1'&&fm.paymode.value!='2'){
				if(fm.CustomerName.value != fm.AccName.value){
					if(!confirm("该案件领款人与被保险人姓名不一致，领款人是否已提交《理赔授权委托书》、《保险赔款权益转让书》或其他证明材料"))
						return false;
				}
			 }
		   }
		}
	  }

	//#3067 案件赔款领取人姓名校验**end**
	
	//#2329 （需求编号：2014-229）客户基本信息要素录入需求
	if((fm.RgtantPhone.value==""||fm.RgtantPhone.value==null) && (fm.Mobile.value==""||fm.Mobile.value==null)){
		alert("申请人电话和申请人手机必须录入其中一项！");
		return false;
	}
	
	if(fm.CBirthday.value==""||fm.CBirthday.value==null)
	{
		alert("出生日期为空！请录入客户信息后按回车键，系统会自动带出出生日期！");
		return false;
	}
	if(fm.ContRemark.value.length>1600)
	{
	  	alert("保单客户特约信息录入超过1600个汉字,无法保存!");
	  	return;
   }
	if(fm.RgtType.value=="13"&&(fm.CaseNo.value==null||fm.CaseNo.value=="")){
		alert("受理方式不能选择：微信（PICC健康生活）线上受理！");
		return false;
	}

   //---如果被保险人的保单存在未回销的预付赔款给出提示信息--------Begin---
   var PrepaidBalaSql="select PrepaidBala from LLPrepaidGrpCont a where exists (select 1 from lccont where grpcontno=a.grpcontno and insuredno='"+ fm.CustomerNo.value +"'"
   			+" union select 1 from lbcont where grpcontno=a.grpcontno and insuredno='"+ fm.CustomerNo.value +"')";      			
   var tPrepaidBala=easyExecSql(PrepaidBalaSql);

   if(tPrepaidBala!="" && tPrepaidBala!=null && tPrepaidBala > 0 && fm.PrePaidFlag.checked==false )
   {
	  if(!confirm("确定案件不回销预付赔款?"))
	  {
  		 return false;
      }	
   }
   if((tPrepaidBala == null || tPrepaidBala == "" || tPrepaidBala <=0)&& fm.PrePaidFlag.checked==true)
   {
   		alert("被保险人投保的保单不存在预付未回销赔款!");
   		return false;
   }
   //----------------End---
   //身故必录身故日期和事件类型
   if (fm.appReasonCode[5].checked || fm.CustStatus.value=="07") {
        if (fm.DeathDate.value==null || fm.DeathDate.value=="") {
            alert("申请原因为身故或事故者现状为死亡时，请录入死亡日期");
            return;
        }
        //3633 死亡日期
        if(!llcheckInputDate(fm.DeathDate.value)){
        	alert("请核实死亡日期录入是否正确");
        	return false;
        }
        if (dateDiff(fm.DeathDate.value,getCurrentDate(),"D")<0) {
            alert("死亡日期不能晚于当前日期");
            return;
        }
   }
    
   //#2818
   var appdate=fm.AppDate.value;
   if(fm.AppDate.value ==null || fm.AppDate.value == ""){
   	alert("输入有误，可能是如下错误中的一个："+"\n"+"申请日期不能为空！");
   	return false;
   }
   if(!isDate(appdate)){
	   alert("输入有误，可能是如下错误中的一个："+"\n"+"申请日期不是有效的日期格式(YYYY-MM-DD)");
	   return false;
   }
   //3633 申请日期
   if(!llcheckInputDate(appdate)){
   	alert("请核实申请日期录入是否正确");
   	return false;
   }


   //校验各种保全项目
   if(!checkBQ()){
   		return false;
   }
   
   var customerno=fm.CustomerNo.value; 

 //续期待核销的不能继续操作，有续期应收的提示是否继续
  if(!checkDueFee())
  {
    return false;  
  }

  if(fm.paymode.value!='1'&&fm.paymode.value!='2'){
  	
    if(fm.BankCode.value==''){
      alert("请录入银行编码！");
      return false;
    }
    if(fm.BankAccNo.value==''){
      alert("请录入银行账号！");
      return false;
    }
    if(fm.AccName.value==''){
      alert("请录入银行账户名！");
      return false;
    }
    
    if(fm.paymode.value=='4'){
    	var tBankSQL="SELECT * FROM ldbank WHERE bankcode='"+fm.BankCode.value+"' AND cansendflag='1'";
    	if (!easyExecSql(tBankSQL)>0){
    		if(confirm("该银行不支持银行转帐，是否修改为银行汇款")){
    			fm.paymode.value='11';
    			fm.paymodename.value = "银行汇款";
    		} else {
    		  return false;
    	  } 
    	}
    }
  }else{
  	fm.BankCode.value = "";
  	fm.BankAccNo.value = "";
  	fm.AccName.value ="";
  }
  if(fm.RgtNo.value.substring(0,1)!='P'||fm.AppealFlag.value=='1'){
    if( verifyInput2() == false ) return false;
    
  }
  else{
    if(fm.paymode.value==''){
      alert("请录入受益金领取方式！");
      return false;
    }
  }
  
  
  if (fm.RgtNo.value.substring(0,1)=='P'&&fm.AppealFlag.value!='1'){
      var strSQLRGT = " select rgtstate from LLRegister where rgtno = '" + fm.RgtNo.value + "' and rgtclass = '1'";
			var	arrResultRGT = easyExecSql(strSQLRGT);
				if(arrResultRGT != null){
				  if (arrResultRGT[0][0]!='01'){
				    alert("该批次已受理申请完毕");
				    return;
				  }
				}
			}  
  if(fm.ReturnMode.value=='3'&&fm.Email.value==''){
    alert("您选择的回复方式为电子邮件，请录入电子邮箱地址！")
    return false;
  }
  if(fm.ReturnMode.value=='4'&&fm.Mobile.value==''){
    alert("您选择的回复方式为短信，请录入手机号码！")
    return false;
  }
  var EventCount=EventGrid.mulLineCount;
  var chkFlag=false;
  for (i=0;i<EventCount;i++){
    if(EventGrid.getChkNo(i)==true){

      // add new 
      if((EventGrid.getRowColData(i,2) == null)||(EventGrid.getRowColData(i,2) == ""))
      {
        alert("第"+(i+1)+"行发生日期有误，请核实事件发生日期");   //3558   
      	EventGrid.setFocus(i,1,EventGrid);
        return false;
      }
    //3558
      var chDate=EventGrid.getRowColData(i,2);
    	if(!isDate(chDate)){
    		alert("第"+(i+1)+"行发生日期有误，请核实事件发生日期");
    		return false;
    	}
    	//3633 发生日期
    	if(!llcheckInputDate(chDate)){
    		alert("请核实第"+(i+1)+"行发生日期录入是否正确")
    		return false;
    	}
    	var iDate=EventGrid.getRowColData(i,10);//3558
    	var oDate=EventGrid.getRowColData(i,11);

    	if (oDate.length!=0&&iDate.length!=0){
        	iDate=modifydate(iDate);
        	oDate=modifydate(oDate);
     	  if(isDate(oDate)&&isDate(iDate)){
 	    	  if(dateDiff(iDate,oDate,"D")<0){
 	        	alert("第"+(i+1)+"行入出院日期有误，请核实被保险人入出院日期");
 	        	return false;
 	    	  }
     	  }else{
     		  alert("第"+(i+1)+"行入出院日期有误，请核实被保险人入出院日期");
     		  return false;
     	  }
       }//3558
    	//3633 入院日期、出院日期
    	if(iDate.length!=0){
    		if(!llcheckInputDate(iDate)){
         		alert("请核实第"+(i+1)+"行入院日期录入是否正确")
         		return false;
         	}
    	}
    	if(oDate.length!=0){
    		if(!llcheckInputDate(oDate)){
         		alert("请核实第"+(i+1)+"行出院日期录入是否正确")
         		return false;
         	}
    	}
    	
      // add end
      // #3500 发生地点校验**start**
      if((EventGrid.getRowColData(i,3)== null) || (EventGrid.getRowColData(i,3)=="")){
    	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(省)信息");
    	  EventGrid.setFocus(i,1,EventGrid);
          return false;
      }
//      alert(EventGrid.getRowColData(i,15));
      if((EventGrid.getRowColData(i,4)== null) || (EventGrid.getRowColData(i,4)=="")){
    	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(市)信息");
    	  EventGrid.setFocus(i,1,EventGrid);
          return false;
      }
      if((EventGrid.getRowColData(i,5)== null) || (EventGrid.getRowColData(i,5)=="")){
    	  alert("第"+(i+1)+"行客户事件信息录入信息不全,请录入发生地点(县)信息");
    	  EventGrid.setFocus(i,1,EventGrid);
          return false;
      }
       // 校验发生地点省市县编码
       var checkAccCode=checkAccPlace(EventGrid.getRowColData(i,15),EventGrid.getRowColData(i,16),EventGrid.getRowColData(i,17))
       if(checkAccCode !=""){
    	   alert(checkAccCode);
    	   return false;
       }
      // #3500 发生地点校验**end**
      chkFlag=true;
    }
  }
  if (chkFlag==false){
    alert("请选中关联事件");
    return false;
  }

  	// 3501被保人客户查询增加仅对身份证和户口本校验**start**
	if(fm.tIDType.value=='0'|| fm.tIDType.value=='5'){
		var tIdType=fm.tIDType.value;
		var tIdNo=fm.tIDNo.value;  
		var tCBirthday=fm.CBirthday.value;
		var tShowSexName=fm.ShowSexName.value;
		if(tShowSexName !="" && tShowSexName !=null){
			var tSexSQL="select code from ldcode where codetype='sex' and codename='"+ fm.ShowSexName.value +"' with ur";
			var aSex =easyExecSql(tSexSQL);
//			alert(aSex[0][0]);
		}
		var strChkIdNo=checkIdNo(tIdType,tIdNo,tCBirthday,aSex[0][0]);
		if(strChkIdNo!=""){
			if(!confirm("被保人身份证号不符合规则，请核实")){
				return false;
			}
		}
	}
	
	// 客户查询增加仅对身份证和户口本校验**end**
	
	// #3501  申请人查询增加仅对身份证和户口本校验**start**
	 
	  if(fm.Relation.value=="00"){
			var IdType=fm.IDType.value;
			var IdNo=fm.IDNo.value;  
			var CBirthday=fm.CBirthday.value;
			var ShowSexName=fm.ShowSexName.value;
			if(tShowSexName !="" && tShowSexName !=null){
				var tSexSQL="select code from ldcode where codetype='sex' and codename='"+ fm.ShowSexName.value +"' with ur";
				var aSex =easyExecSql(tSexSQL);
//				alert(aSex[0][0]);
			}
			if(IdType=="0" || IdType=="5"){
				var strChkIdNo=checkIdNo(IdType,IdNo,CBirthday,aSex[0][0]);
		  		if(strChkIdNo!=""){
		  			if(!confirm("申请人身份证号不符合规则，请核实")){
						return false;
					}
		  		}
			}
		}else{
			if(fm.IDType.value=="0" || fm.IDType.value=="5"){
				var strChkIdNo=checkIdNo(fm.IDType.value,fm.IDNo.value,"","");
		  		if(strChkIdNo!=""){
		  			alert("申请人身份证号不符合规则，请核实");
		  			return false;
		  		}
			} 
		}
	
	// 申请人查询增加仅对身份证和户口本校验**end**
	    if(!checkID1(fm.tIDNo.value)){
	        return false;
	      }
  //long long ago 的版本，与本次需求不符，by gzh 注释于20120917
  /*
  for(var rowNum=0;rowNum<EventGrid.mulLineCount;rowNum++){
   if(EventGrid.getChkNo(rowNum)){
      var happenDate=EventGrid.getRowColData(rowNum,2);
      var sql3="select count(1) From lpedoritem a,lpedorapp b where  contno in(select contno from lccont where insuredno='"+customerno+"') and a.edoracceptno = b.edoracceptno and a.edorappdate>'"+happenDate+"' with ur";
      
      if(easyExecSql(sql3)>0){
      if(!confirm("第"+(rowNum+1)+"行的事件在出险后申请过保全项目，是否继续下面的操作？")){return false;}
      }

   
      }
    }
    */
	    //用批次号为空 来确认是受理申请，表示正在做个案
	    if( (fm.RgtNo.value==null || fm.RgtNo.value.length==0 )){
	    	 //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
	    	if( fm.paymode.value!="1" && fm.paymode.value!="2"){
	    		if(fm.RelaDrawerInsured.value=="" ){
    				alert("领款人和被保险人关系不能为空!");
    				return false;
    			}
	    		
	    		if(fm.RelaDrawerInsured.value!="28"){
//	    			必录
	    			if(fm.DrawerIDType.value=="" ){
	    				alert("领款人证件类型不能为空!");
	    				return false;
	    			}
	    			if(fm.DrawerID.value=="" ){
	    				alert("领款人证件号码不能为空!");
	    				return false;
	    			}
	    		}
	    		
	    	}
	    	
	    	
	 	   if( fm.paymode.value!="1" && fm.paymode.value!="2" &&
	 			   fm.RelaDrawerInsured.value=="00" &&
	 			   (fm.DrawerIDType.value=="0" || fm.DrawerIDType.value=="5")
	 	   ){
	 		   
	 		    var DrawerCBirthday=fm.CBirthday.value;
	 			var DrawerShowSexName=fm.ShowSexName.value;
	 			var DrawerSex="";
	 			if(DrawerShowSexName !="" && DrawerShowSexName !=null){
	 				var tSexSQL="select code from ldcode where codetype='sex' and codename='"+ fm.ShowSexName.value +"' with ur";
	 				aSex =easyExecSql(tSexSQL);
	 				DrawerSex=aSex[0][0];
	 			}
	 			//alert("领取人性别："+DrawerSex);
	 			var strChkIdNo=checkIdNo(fm.DrawerIDType.value,fm.DrawerID.value,DrawerCBirthday,DrawerSex);
	 			if(strChkIdNo!=""){
	 				if(!confirm("领款人证件号码不符合规则，请核实")){
	 					  return false;
	 				  }
	 	  		}
	 	   }
	 	   if( fm.paymode.value!="1" && fm.paymode.value!="2" &&
	 			   fm.RelaDrawerInsured.value!="00" &&
	 			   fm.RelaDrawerInsured.value!="28" &&
	 			   (fm.DrawerIDType.value=="0" || fm.DrawerIDType.value=="5")
	 	   ){
	 		   var strChkIdNo=checkIdNo(fm.DrawerIDType.value,fm.DrawerID.value,"","");
	 			if(strChkIdNo!=""){
	 				alert("领款人证件号码不符合规则，请核实");
	 				return false;
	 	  		}
	 		 }
  	    //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
	    	
	    }else{ //批次的一种情况，给付方式为1 2 
	    	  if(fm.RgtNo.value!=null && fm.RgtNo.value.length>0){
	    		  	var sqlrtgtos ="select rgtclass,TogetherFlag from LLRegister where 1=1 and  rgtno ='"+fm.RgtNo.value+"'  ";
		      		var sqlrtgtoresults=easyExecSql(sqlrtgtos);
		      		if(sqlrtgtoresults){
		      		
		      			if( sqlrtgtoresults[0][0]=="1" && (sqlrtgtoresults[0][1]=="1" || sqlrtgtoresults[0][1]=="2" )){
		      				 //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
		      				
		      				if( fm.paymode.value!="1" && fm.paymode.value!="2"){
		      		    		if(fm.RelaDrawerInsured.value=="" ){
		      	    				alert("领款人和被保险人关系不能为空!");
		      	    				return false;
		      	    			}
		      		    		
		      		    		if(fm.RelaDrawerInsured.value!="28"){
//		      		    			必录
		      		    			if(fm.DrawerIDType.value=="" ){
		      		    				alert("领款人证件类型不能空!");
		      		    				return false;
		      		    			}
		      		    			if(fm.DrawerID.value=="" ){
		      		    				alert("领款人证件号码不能为空!");
		      		    				return false;
		      		    			}
		      		    		}
		      		    		
		      		    	}
		      				
		      				
		      			   if( fm.paymode.value!="1" && fm.paymode.value!="2" &&
		      					   fm.RelaDrawerInsured.value=="00" &&
		      					   (fm.DrawerIDType.value=="0" || fm.DrawerIDType.value=="5")
		      			   ){
		      				   
		      				    var DrawerCBirthday=fm.CBirthday.value;
		      					var DrawerShowSexName=fm.ShowSexName.value;
		      					var DrawerSex="";
		      					if(DrawerShowSexName !="" && DrawerShowSexName !=null){
		      						var tSexSQL="select code from ldcode where codetype='sex' and codename='"+ fm.ShowSexName.value +"' with ur";
		      						aSex =easyExecSql(tSexSQL);
		      						DrawerSex=aSex[0][0];
		      					}
		      					//alert("领取人性别："+DrawerSex);
		      					var strChkIdNo=checkIdNo(fm.DrawerIDType.value,fm.DrawerID.value,DrawerCBirthday,DrawerSex);
		      					if(strChkIdNo!=""){
		      						if(!confirm("领款人证件号码不符合规则，请核实")){
		      							  return false;
		      						  }
		      			  		}
		      			   }
//		      			    
		      			   if( fm.paymode.value!="1" && fm.paymode.value!="2" &&
		      					   fm.RelaDrawerInsured.value!="00" &&
		      					   fm.RelaDrawerInsured.value!="28" &&
		      					   (fm.DrawerIDType.value=="0" || fm.DrawerIDType.value=="5")
		      			   ){
		      				   var strChkIdNo=checkIdNo(fm.DrawerIDType.value,fm.DrawerID.value,"","");
		      					if(strChkIdNo!=""){
		      						alert("领款人证件号码不符合规则，请核实");
		      						return false;
		      			  		}
		      				 }
 		      			    //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
		      			}
		      		}
	    	  } 
	    }
	    
  if(fm.AppealFlag.value=="0"){
    //处理申请确认
    var nowDate=getCurrentDate();
    if(EventGrid.checkValue2(EventGrid.name,EventGrid)== false) return false;

    for(var rowNum=0;rowNum<EventGrid.mulLineCount;rowNum++){
      var happenDate=EventGrid.getRowColData(rowNum,2);
      var happenDate1=modifydate(happenDate);
      var day=dateDiff(happenDate1,nowDate,"D");
      if(day<0){
        alert("您在第"+(rowNum+1)+"行输入的发生日期不应晚于当前时间"); 
        return false;
      }
      var inDate=EventGrid.getRowColData(rowNum,10);
      inDate=modifydate(inDate);
      var outDate=EventGrid.getRowColData(rowNum,11);
      if (outDate.length!=0){
        outDate=modifydate(outDate);
        day1=dateDiff(inDate,outDate,"D");
        if(dateDiff(inDate,outDate,"D")<0){
          alert("您在第"+(rowNum+1)+"行输入的出院日期不应早于住院时间");
          return false;
        }
      }
      
      //发生日期#3235
    
//    for (i=0;i<EventCount;i++){
      if(EventGrid.getChkNo(rowNum)==true){
    	  var caseno=fm.CaseNo.value;
    	    var rgtdate ="";
    	    if(caseno!="" && caseno!=null){
    	  	  var rgtdateSQL="select rgtdate from llcase where 1 = 1 and caseno='"+fm.CaseNo.value+"' fetch first rows only ";
    	  	  var rgtdateResult = easyExecSql(rgtdateSQL);
    	  	  rgtdate = rgtdateResult[0][0] ;
    	  	  if(rgtdate=="" || rgtdate==null){
    	  		  var date = new Date(); 
    	  		  var mon = date.getMonth() + 1;
    	  		  var day = date.getDate();
    	  		  rgtdate = date.getFullYear() + "-" + (mon<10?"0"+mon:mon) + "-" +(day<10?"0"+day:day);
    	  	  }
    	    }else{
    	  	  var date = new Date(); 
    	  	  var mon = date.getMonth() + 1;
    	  	  var day = date.getDate();
    	  	  rgtdate = date.getFullYear() + "-" + (mon<10?"0"+mon:mon) + "-" +(day<10?"0"+day:day);
    	    }
//      	alert("页面显示："+EventGrid.getRowColData(i,2));
      	//因为在比较日期时，比如2017-01-01和2017-1-1在比较时前者小于后者。而实际为相等的。所以做以下调整
      	var newDate=happenDate1;
      	//有横杠yyyy-mm-dd
      	if(newDate.indexOf("-")!=-1){
      		var varDate=new Date(newDate.replace(/-/,"/"));
          	var mon = varDate.getMonth()  + 1;         //getMonth()返回的是0-11，则需要加1
          	if(mon <=9){                                     //如果小于9的话，则需要加上0
          	mon = "0" + mon;
          	}
          	var day = varDate.getDate();                  
          	if(day <=9){                                     //如果小于9的话，则需要加上0
          	day = "0" + day;
          	}
          	newDate = varDate.getFullYear() + "-" + mon + "-" +  day;
      	}
//      	else{//没横杠yyyymmdd
//      		var pattern = /(\d{4})(\d{2})(\d{2})/;
//      		var formatedDate = EventGrid.getRowColData(i,2).replace(pattern, '$1-$2-$3');
//      		newDate=formatedDate;
//      	}
      	
//      	alert("加0："+newDate);
      	if(isDate(newDate)&&isDate(appdate)&&isDate(rgtdate)){
        if(newDate>appdate || appdate>rgtdate)
        {
        	alert("事件发生日期、申请日期、受理日期先后顺序不符合逻辑，请检查录入是否正确");   
          return false;
        }
      	}
      }
//    }
    //申请日期 var appdate=fm.AppDate.value;
    //受理日期:立案日期，，llcase 的 rgtdate,,3235
      
      
    }
        //简易案件，机构处理，上报总公司校验
      if((fm.SimpleCase.checked == true && fm.EasyCase.checked == true && fm.HeadCase.checked == true)||(fm.SimpleCase.checked == true && fm.EasyCase.checked == true)||(fm.EasyCase.checked == true && fm.HeadCase.checked == true)||(fm.SimpleCase.checked == true && fm.HeadCase.checked == true)){  
        alert("【简易案件】，【上报总公司】和【机构处理】只能勾选其中一个，请重试");
        return false;
      }
       //上报分公司校验
      if((fm.SimpleCase.checked == true && fm.EasyCase.checked == true && fm.HeadCase.checked == true && fm.FenCase.checked == true)||(fm.SimpleCase.checked == true && fm.EasyCase.checked == true && fm.FenCase.checked == true)||(fm.EasyCase.checked == true && fm.HeadCase.checked == true && fm.FenCase.checked == true)||(fm.SimpleCase.checked == true && fm.HeadCase.checked == true && fm.FenCase.checked == true)||(fm.HeadCase.checked == true && fm.FenCase.checked == true)||(fm.EasyCase.checked == true && fm.FenCase.checked == true)||(fm.SimpleCase.checked == true && fm.FenCase.checked == true)){  
        alert("【简易案件】，【上报总公司】和【机构处理】只能勾选其中一个，请重试");
        return false;
      }
      if(fm.SimpleCase.checked != true && fm.EasyCase.checked != true && fm.HeadCase.checked != true && fm.FenCase.checked != true){
       if(fm.all('CaseRule').value==""||fm.all('CaseRule').value==null){
        strCaseSQL = "select othersign from ldcode1 where codetype='LLRegStyle' and code1='5'   and code=substr('"+fm.all('CaseRuleMng').value+"',1,4) with ur";
        fm.all('CaseRule').value = easyExecSql(strCaseSQL);
       }
       if(fm.all('CaseRule').value=="1"){
          fm.EasyCase.checked = true;
       }
       if(fm.all('CaseRule').value=="2"){
          fm.SimpleCase.checked = true;
       }
       if(fm.all('CaseRule').value=="3"){
          fm.FenCase.checked = true;
       }
       if(fm.all('CaseRule').value=="4"){
          fm.HeadCase.checked = true;
       }
      }
         //个险案件标记校验
    if(fm.CaseFlag.checked == true){
       strCaseSQL = "select 1 from lccont c where  c.insuredno='"+fm.all('CustomerNo').value+"' and c.ContType='1' and not exists (select 1 from lpedoritem p where p.contno =c.contno and p.edortype in ('WT','CT','XT'))"
        +" union (select 1 from lcgrpcont a,lcpol b where  a.grpcontno=b.grpcontno and a.cardflag in ('2', '3') and b.insuredno='"+fm.all('CustomerNo').value+"')";
    crrCaseFlag = easyExecSql(strCaseSQL);
    if(!crrCaseFlag){
      alert("该客户无个险保单，请核实!");
      return false;
     }
    }
    //556反洗钱黑名单客户的理赔监测功能
//     var strblack=" select 1 from lcblacklist where trim(name)='"+fm.all('CustomerName').value+"' with ur";
//      var crrblack=easyExecSql(strblack);
//      if(crrblack){
//       alert("该客户为黑名单客户，须请示上级处理。");
//       return false;
//      }
//      if(crrblack){
//       if(!confirm("该客户为黑名单客户，是否受理此案件？")){
//          return false;
//       	}
    	//#2684关于理赔环节黑名单监测规则修改的需求
       var checkName1 = fm.CustomerName.value;
       var checkId1 = fm.tIDNo.value;
   	   if(!checkBlacklist(checkName1,checkId1)){
       	return false;
       }
        
      
     var strblackgrp="select 1 from LCGrpcont where grpcontno in( "
		+"select grpcontno from lcinsured where insuredno='"+fm.all('CustomerNo').value+"' and grpcontno<>'00000000000000000000'  "
		+"union select grpcontno from lbinsured where insuredno='"+fm.all('CustomerNo').value+"' and grpcontno<>'00000000000000000000') "
		+"and exists (select 1 from lcblacklist where trim(name)=trim(LCGrpcont.grpname)) "
		+"union "
		+"select 1 from LbGrpcont where grpcontno in( "
		+"select grpcontno from lcinsured where insuredno='"+fm.all('CustomerNo').value+"' and grpcontno<>'00000000000000000000'  "
		+"union select grpcontno from lbinsured where insuredno='"+fm.all('CustomerNo').value+"' and grpcontno<>'00000000000000000000') "
		+"and exists (select 1 from lcblacklist where trim(name)=trim(LbGrpcont.grpname)) with ur"
	  var crrblackgrp=easyExecSql(strblackgrp);
//      if(crrblackgrp){
//       alert("该客户所在团单为黑名单客户，须请示上级处理。");
//       return false;
//      }
	  if(crrblackgrp){
	   if(!confirm("该客户所在团单为黑名单客户，是否受理此案件？")){
          return false;
       }
	  }
	  
		
	    //客户尚有未结案校
	    strSQL = "select caseno from llcase where rgtstate not in ('14','09','12','11') and customerno='"
	    +fm.all('CustomerNo').value+"' and caseno <> '"+fm.CaseNo.value+"'" ;
	    crrResultTemp = easyExecSql(strSQL);

	    if(crrResultTemp){
	      if(!confirm("该客户有案件"+crrResultTemp+"未结案，您确定要继续吗？"))
	      return false;
	    }
	   
//TODO:受理申请（团体）的出险人员确定，与个案使用同一页面，通过页面的批次号判断是否为社保（在个案页面查询的话不校验）。
  if(fm.RgtNo.value.substring(0,1)!="P" || (fm.RgtNo.value == "" || fm.RgtNo.value == null)){//批次案件
  	//TODO:临时校验，个案申请确认时,校验不可以进行社保保单下的个单的受理,需要判断业务人员是通过客户号还是保单号受理的该案件
  	if(fm.CustomerNo.value!=""&&fm.CustomerNo.value!=null&&(fm.all('ContNo').value=="" || fm.all('ContNo').value==null)){
  		var tSocial = fm.CustomerNo.value;
		if(!checkSocial(tSocial,'','insuredno')){
			return false;
		}
  	}else if(fm.all('ContNo').value!="" && fm.all('ContNo').value!=null){
  		var tSocial = fm.all('ContNo').value;
		if(!checkSocial(tSocial,'','')){
			return false;
		}
  	}

    //客户尚有未结案的个险案件校验add lyc 2014-07-16 #2034 #2256
    strSQL1 = "select caseno from llcase where rgtstate not in ('14','12','11') and customerno='"
    +fm.all('CustomerNo').value+"' and caseno <> '"+fm.CaseNo.value+"' and " +
    		"exists(select 1 from llclaimuser where usercode=llcase.handler)" 
    +"and exists(select 1 from lcpol where lcpol.insuredno='"+fm.all('CustomerNo').value+"' and conttype='1')";
    crrResultTemp1 = easyExecSql(strSQL1);
    
    if(crrResultTemp1){
    	alert("该客户存在案件号为"+crrResultTemp1+"的案件还处理完毕，请处理完毕后再申请，谢谢！");
      return false;
    }
    //校验客户所有保全项
    strSQL2 = "select a.edoracceptno ,(select edorname from lmedoritem where edorcode=b.edortype fetch first 1 rows only) "
    +"from lpedorapp a,lpedoritem b where a.edoracceptno=b.edoracceptno " 
    +"and a.edorstate!='0' and b.contno in ( "
    +"select contno from lcinsured where grpcontno='00000000000000000000' " 
    +"and insuredno='"+fm.all('CustomerNo').value+"') ";
        crrResultArr = easyExecSql(strSQL2);
        if(crrResultArr){
        	for(var i=0;i<crrResultArr.length;i++){
        	alert("该客户存在工单号为"+crrResultArr[i][0]+"的"+crrResultArr[i][1]+"保全项 还未结案，请处理完毕后再申请，谢谢！");
        	}
        	return false;
        }
//	   var strSQL3 = "select edoracceptno,otherno ,edorappdate  from lpedorapp   "
//	    	+"where edorstate!='0' and othernotype='1' and otherno  in ( "
//	    	+"select appntno from lcappnt where contno in  "
//	    	+"(select contno from lcinsured where insuredno ='"+fm.all('CustomerNo').value+"')) ";
//    			var crrResultTemp2 = easyExecSql(strSQL3);
//    			if(crrResultTemp2){
//    				for(var i=0;i<crrResultTemp2.length;i++){
//    					alert("该客户已申请保全"+crrResultTemp2[i][0]+"申请人代码"+crrResultTemp2[i][1]+"申请日期"+crrResultTemp2[i][2]+"");
//    					return false;
//    				}
//    			}
    //add lyc
    			
  }
         
    if ( fm.CaseNo.value !='' ){
      //			alert( "案件已保存，您不能执行此操作");
      //			return false;
    }
    if(fm.CustomerNo.value==null||fm.CustomerNo.value==""){
      alert("请输入客户数据");
    }
    else{
      if (fm.all('rgtflag').value == "1"){
        fm.all('operate').value = "UPDATE||MAIN";

        strSQL = " select AppPeoples,customerno,grpname from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
        arrResult = easyExecSql(strSQL);
        if(arrResult != null){
          var AppPeoples;
          try{AppPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "AppPeoples")}
          var appntno= arrResult[0][1];
          strSQL = "select InsuredNo from lcpol where appntno='"+appntno+"' and InsuredNo='"
          +fm.all('CustomerNo').value
          +"' union select InsuredNo from lbpol where appntno='"+appntno+"' and InsuredNo='"
          +fm.all('CustomerNo').value +"'";
          crrResult = easyExecSql(strSQL);
          if(crrResult){
          }
          else{
            if(!confirm("团体客户"+arrResult[0][2]+"未给个人客户"+fm.CustomerName.value+"投保，您确定要继续吗？"))
            return false;
          }
        }
        else{
          alert("团体案件信息查询失败！");
          return;
        }

        strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
        arrResult = easyExecSql(strSQL);
        if(arrResult != null){
          var RealPeoples;
          try{RealPeoples = arrResult[0][0]} catch(ex) {alert(ex.message + "RealPeoples")}
        }
        if ( (eval(AppPeoples) - eval(RealPeoples)) < 5 ){
          alert("团体申请人数：" + AppPeoples + "实际已录入人数：" + RealPeoples);
        }
        if ( eval(RealPeoples) >= eval(AppPeoples) ){
          //					if (!confirm("团体申请人数：" + AppPeoples + ", 实际已录入人数：" + RealPeoples + "你确认要补加个人客户吗?"));
          //					{
          //						return;
          //					}
        }
      }
      else{
        fm.all('operate').value = "INSERT||MAIN";
      }
        fm.IdAppConf.disabled=true;
      var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
       
      showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
      fm.action ="./ClientReasonSave.jsp";
      fm.submit();
     
    }
  }
  else{
    if(fm.AppeanRCode.value==null||fm.AppeanRCode.value==""){
      alert("请选择申诉原因！");
      return false;
    }
  
    fm.all('operate').value = "APPEALINSERT||MAIN";
    fm.IdAppConf.disabled=true;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action ="./AppealSave.jsp";   
    fm.submit();
  }
 
}


function afterAffix(ShortCountFlag,SupplyDateResult){
  var strsql = "select a.RgtState,b.codename,AffixGetDate from LLcase a, ldcode b where a.caseno='"+fm.CaseNo.value +"'"
  +" and b.codetype='llrgtstate' and b.code=a.rgtstate"
  arrResult = easyExecSql(strsql);
  if ( arrResult ){
    fm.RgtState.value= arrResult[0][1];
    fm.AffixGetDate.value= arrResult[0][2];
  }

  fm.ModifyDate.value = getCurrentDate();
}

function SaveAffixDate(){
  if(fm.AffixGetDate.value!=""&&fm.AffixGetDate.value!=null){
    var strSQL="update llcase set affixgetdate='"+fm.AffixGetDate.value+"' where caseno='"+fm.CaseNo.value+"'";
    easyExecSql(strSQL);
  }
}

//ClientCaseQueryInput.js查询返回接口2005-2-21 13:09
function ClientafterQuery(RgtNo,CaseNo){
  //团体批次
  if ( RgtNo!=null && RgtNo.length>0){
    var arrResult = new Array();
    var strSQL="select "
    +"(select codename from ldcode where ldcode.codetype='llaskmode' and ldcode.code=llregister.RgtType),"
    +"AppAmnt,"
    +"(select codename from ldcode where ldcode.codetype='llreturnmode' and ldcode.code=llregister.ReturnMode),"
    +"RgtantName,"
    +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llregister.IDType),"
    +"IDNo,"
    +"(select codename from ldcode where ldcode.codetype='llrelation' and ldcode.code=llregister.relation),"
    +"RgtantPhone,RgtantAddress,PostCode,"
    +"(select codename from ldcode where ldcode.codetype='llgetmode' and ldcode.code=llregister.getmode), "
    +"IDType,bankcode,bankaccno,accname,RgtType,ReturnMode,relation,getmode, "
    +"(select bankname from ldbank where ldbank.bankcode=llregister.bankcode), "
    +"RgtantMobile,email "
    +"from LLRegister where rgtno='"+RgtNo+"'";
    arrResult = easyExecSql(strSQL);
    if( arrResult!=null){
      fm.all('RgtTypeName').value= arrResult[0][0];
      fm.all('AppAmnt').value= arrResult[0][1];
      fm.all('ReturnModeName').value= arrResult[0][2];
      fm.all('RgtantName').value= arrResult[0][3];
      fm.all('IDTypeName').value= arrResult[0][4];
      fm.all('IDNo').value= arrResult[0][5];
      fm.all('RelationName').value= arrResult[0][6];
      fm.all('RgtantPhone').value= arrResult[0][7];
      fm.all('RgtantAddress').value= arrResult[0][8];
      fm.all('PostCode').value= arrResult[0][9];
      fm.all('paymodeName').value= arrResult[0][10];
      fm.all('IDType').value=arrResult[0][11];
      fm.all('BankCode').value= arrResult[0][12];
      fm.all('BankAccNo').value= arrResult[0][13];
      fm.all('AccName').value=arrResult[0][14];
      fm.all('RgtType').value=arrResult[0][15];
      fm.all('ReturnMode').value=arrResult[0][16];
      fm.all('Relation').value=arrResult[0][17];
      fm.all('paymode').value = arrResult[0][18];
      fm.all('BankName').value = arrResult[0][19];
      fm.all('Mobile').value = arrResult[0][20];
      fm.all('Email').value = arrResult[0][21];
      var test1 = "select togetherflag from llregister where rgtno='"+fm.RgtNo.value+"'";
      var	test1RR = easyExecSql(test1);
      if(test1RR =='3'){
          if(fm.paymode.value=='1' || fm.paymode.value=='2'){
          divBankAcc.style.display='none';
          fm.BankName.value='';
          fm.AccName.value='';
          fm.BankCode.value='';
          fm.BankAccNo.value='';
          }
          fm.paymode.readOnly=true;
          fm.BankName.readOnly=true;
          fm.AccName.readOnly=true;
          fm.BankCode.readOnly=true;
          fm.paymodename.readOnly = true;
          fm.BankAccNo.readOnly = true;
          fm.paymode.onclick='';
          fm.BankCode.onclick='';
          fm.paymode.onkeyup='';
          fm.BankCode.onkeyup='';
          
      }
      else{
          fm.paymode.value="";
          fm.BankCode.value="";
          fm.AccName.value="";
          fm.BankAccNo.value="";
          fm.paymodename.value = "";
          fm.BankAccNo.value = "";
          fm.BankName.value = "";
     }
    }
    //个人案件
    if (CaseNo!=null && CaseNo.length>0){
      var strSQL="select customerno,customername,customersex,custbirthday,"
      +"(select codename from ldcode where ldcode.codetype='idtype' and ldcode.code=llcase.IDType),"
      +"idno,idtype,otheridno,otheridtype,mobilephone from llcase where caseno='"+CaseNo+"'";
      arrResult = easyExecSql(strSQL);
      if( arrResult != null ){
         	
        fm.CustomerNo.value   =arrResult[0][0];
        fm.CustomerName.value =arrResult[0][1];
        fm.Sex.value          =arrResult[0][2];
        fm.CBirthday.value    =arrResult[0][3];
        fm.tIDTypeName.value  =arrResult[0][4];
        fm.tIDNo.value        =arrResult[0][5];
        fm.tIDType.value = arrResult[0][6];
        fm.OtherIDNo.value = arrResult[0][7];
        fm.OtherIDType.value = arrResult[0][8];
        fm.MobilePhone.value = arrResult[0][9];
//        fm.CaseOrder.value = arrResult[0][9];
//        fm.CaseOrderName.value = arrResult[0][10];

      }
      //#2329 （需求编号：2014-229）客户基本信息要素录入需求
      var SQLPart = " and a.InsuredNo='"+fm.CustomerNo.value+"'";
  	  var SQL0 ="select distinct a.addressno," 
  		+"codename('sex',a.sex),codename('nativeplace',a.nativeplace),codename('occupationtype',a.occupationtype),a.IDStartDate,a.IDEndDate ";
  	  var sqlpart1="from lcinsured a,lccont b where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' ";
  	  var sqlpart2="from lbinsured a,lbcont b where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' ";
  	  var sqlpart3="from lcinsured a,lccont b,llregister c where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' and b.GrpContNo=c.RgtObjNo and c.RgtNo='"+RgtNo+"'";
  	  var sqlpart4="from lbinsured a,lbcont b,llregister c where 1=1 and a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' and b.GrpContNo=c.RgtObjNo and c.RgtNo='"+RgtNo+"'";
      var SQL="";
  	  if (RgtNo.substring(0,1) != "P" || RgtNo == "" || RgtNo == null){
    	SQL=SQL0+sqlpart1+ SQLPart+" union "+SQL0+sqlpart2+SQLPart;
  	  }else{
    	SQL=SQL0+sqlpart3+ SQLPart+" union "+SQL0+sqlpart4+SQLPart;    
  	  }
  	  var mArray= easyExecSql( SQL,1,0,1 );
  	  var addresscode = "";
  	  if(mArray){
  	    addresscode = mArray[0][0];
  	  	fm.ShowSexName.value     	=mArray[0][1];
	  	fm.ShowNativePlace.value 	=mArray[0][2];
	  	fm.ShowOccupationType.value =mArray[0][3];
	  	fm.IDStartDate.value        =mArray[0][4];
	  	fm.IDEndDate.value          =mArray[0][5];
  	  }
  	  if (addresscode != null && addresscode != '') {
    	SQL = "select b.PostalAddress,b.homephone from lcaddress b where customerno='"+fm.CustomerNo.value+"' and b.AddressNo='" + addresscode +"'";
    	mArray = easyExecSql(SQL);
    	if (mArray){
    		fm.ShowPostalAddress.value  =mArray[0][0];
			fm.ShowPhone.value          =mArray[0][1];
    	}else{
    	    fm.ShowPostalAddress.value  ='';
			fm.ShowPhone.value          ='';
    	}
      }
  	      
        var firFi = CaseNo.substring(0,1);
        var aflag = fm.AppealFlag.value;
        if(aflag==1) {
          if(firFi=='R' || firFi=='S'){
            var sql_app = "select AppeanRCode,AppealReason from LLAppeal where appealno='"+CaseNo+"' with ur";
            var result1 = easyExecSql(sql_app);
            if(result1!=null){
                fm.AppeanRCode.value= result1[0][0];
                fm.AppealReason.value= result1[0][1];
            }
            var sql_BZ = "select AppealRDesc from llappeal where appealno='"+CaseNo+"' with ur"; 
            var result2 = easyExecSql(sql_BZ);
            if(result2!=null){
            fm.AppealRDesc.value= result2[0][0];
            }
          }
        }
      strSQL="select AccStartDate,AccidentDate,DeathDate,AccidentSite,"
      +"AccdentDesc,AffixGetDate,"
      +"(select codename from ldcode where ldcode.codetype='llcuststatus' and ldcode.code=llcase.custstate),custstate"
      +" from llcase where caseno='"+CaseNo+"'";
      //	alert(strSQL);
      arrResult = easyExecSql(strSQL);
      if( arrResult != null ){
        fm.all('DeathDate').value= arrResult[0][2];
        fm.all('CustStatusName').value= arrResult[0][6];
        fm.all('CustStatus').value= arrResult[0][7];
        if ( arrResult[0][5]!=null || arrResult[0][5]!='null' ){
          fm.all('AffixGetDate').value =arrResult[0][5] ;
        }
      }
      strSQL = "select casegetmode,BankCode,BankAccNo,AccName, "+
      "(select b.bankname from ldbank b where b.bankcode=a.bankcode), "+
      "(select c.codename from ldcode c where c.codetype='llgetmode' and c.code=a.casegetmode)"+
      " from llcase a where a.caseno='"+fm.CaseNo.value+"'"
      brrResult = easyExecSql(strSQL);
      if(brrResult){
        fm.all('paymode').value=brrResult[0][0];
        fm.all('BankCode').value= brrResult[0][1];
        fm.all('BankAccNo').value= brrResult[0][2];
        fm.all('AccName').value=brrResult[0][3];
        fm.all('BankName').value=brrResult[0][4];
        fm.all('paymodeName').value = brrResult[0][5];
      }
      showBank();

	  fm.appReasonCode[00].checked = false;
      fm.appReasonCode[01].checked = false;
      fm.appReasonCode[02].checked = false;
      fm.appReasonCode[03].checked = false;
      fm.appReasonCode[04].checked = false;
      fm.appReasonCode[05].checked = false;
      fm.appReasonCode[06].checked = false;
      fm.appReasonCode[07].checked = false; 
      fm.appReasonCode[08].checked = false;
      fm.appReasonCode[09].checked = false;
      fm.appReasonCode[10].checked = false;
      strSQL="select ReasonCode,Reason from LLAppClaimReason where 1=1 "
      +"and rgtno='"+RgtNo+"' "
      +" and caseno='"+CaseNo+"'";
      arrResult = easyExecSql(strSQL);
      if ( arrResult!=null ){

        for (i=0;i< arrResult.length ;i++){

          //fm.all("appReasonCode"+ arrResult[i][0])
//          alert(arrResult[i][0]<04)
          if(arrResult[i][0]<04){
        	  var ind= eval(arrResult[i][0])-1;
        	  }
          else if(arrResult[i][0]==11){
        	  var ind= 03;
          }else {
        	  var ind= eval(arrResult[i][0]);
	          }
          fm.appReasonCode[ind].checked = true;

        }
      }
      //#2818
    	strSQLz="select AppDate from llregister where RgtNo= '"+fm.CaseNo.value+"'";
    	arrResultz=easyExecSql(strSQLz);
    	if(arrResultz !=null){
    		try{fm.all('AppDate').value=arrResultz[0][0]}catch(ex){alert(ex.message+"AppDate")}
    	}

      //#2807
    	strSQLz1="select RelaDrawerInsured,(select codeName from LDcode where codetype='relation'  and code=llcaseExt.reladrawerinsured),draweridtype, (select codeName from LDcode where codetype='idtype'   and code=llcaseExt.draweridtype) , drawerid  from llcaseExt where caseno= '"+fm.CaseNo.value+"'";
    	arrResultz=easyExecSql(strSQLz1);
    	if(arrResultz !=null){
    		try{fm.all('RelaDrawerInsured').value=arrResultz[0][0]}catch(ex){alert(ex.message+"RelaDrawerInsured")}
    		try{fm.all('RelaDrawerInsuredName').value=arrResultz[0][1]}catch(ex){alert(ex.message+"RelaDrawerInsuredName")}
    		var sqlrtgto ="select rgtclass,TogetherFlag from LLRegister where 1=1 and (rgtno ='"+RgtNo+"'  or rgtno='"+fm.CaseNo.value+"' ) ";
    		var sqlrtgtoresult=easyExecSql(sqlrtgto);
    		if(sqlrtgtoresult){
    			if( (sqlrtgtoresult[0][0]=="1" && (sqlrtgtoresult[0][1]=="1" || sqlrtgtoresult[0][1]=="2"  )) || sqlrtgtoresult[0][0]=="0" ){
    			//团，给付方式为1,2  || 个
    			//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
        		try{fm.all('DrawerIDType').value=arrResultz[0][2]}catch(ex){alert(ex.message+"DrawerIDType")}
        		try{fm.all('DrawerIDTypeName').value=arrResultz[0][3]}catch(ex){alert(ex.message+"DrawerIDTypeName")}
        		try{fm.all('DrawerID').value=arrResultz[0][4]}catch(ex){alert(ex.message+"DrawerID")}
        		 //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
    		
    			}
    		}
    		
    		//alert(strSQLz1);
    	}
    	
    	
       //事件查询
      QueryEvent();
      fm.all("CaseNo").value = CaseNo;
      fm.all("RgtNo").value =  RgtNo;
      strSQL="select RgtState,rigister,ModifyDate,caseprop,codename,PrePaidflag from llcase,ldcode where caseno='"+CaseNo+"' and code=RgtState and codetype='llrgtstate'";
      arrResult = easyExecSql(strSQL);
      if(arrResult!=null){
        fm.all("Handler").value = arrResult[0][1];
        fm.all("ModifyDate").value = arrResult[0][2];
        fm.RgtState.value=arrResult[0][4];
        if(arrResult[0][5]!="" && arrResult[0][5]!=null && arrResult[0][5]==1)
        {
        	fm.PrePaidFlag.checked = "true";
        }else{
        	fm.PrePaidFlag.checked = "";
        }
        if(arrResult[0][3]=="06"){
          fm.SimpleCase.checked = "true";
        }
        if(arrResult[0][3]=="09"){
          fm.EasyCase.checked = "true";
        } 
//         if(arrResult[0][6]!="" && arrResult[0][6]!=null && arrResult[0][6]=='I')
//        {
//        	fm.CaseFlag.checked = "true";
//        }else{
//        	fm.CaseFlag.checked = "";
//        }
      }
      //strSQL = "select appealtype,case appealtype  when '1' then '纠错类' when '0' then '申诉类' end ,appeanrcode,appealreason,appealrdesc from LLAppeal where appealno ='"+fm.CaseNo.value+"'";
     // brrResult = easyExecSql(strSQL);
     // if(brrResult){
     //   fm.all('AppealType').value=brrResult[0][0];
      //  fm.all('AppealTypeName').value=brrResult[0][1];
       // fm.all('AppeanRCode').value= brrResult[0][2];
     //   fm.all('AppealReason').value= brrResult[0][3];
      //  fm.all('AppealRDesc').value= brrResult[0][4];

      //}
      
      

      showPRemark();
      showCaseRemark();	//#1769 案件备注信息的录入和查看功能 add by Houyd
    }
  }
}

function afterLLRegister(arr){
  if(arr){
    fm.CustomerNo.value  =arr[0][0];
    fm.CustomerName.value=arr[0][1];
    fm.Sex.value         =arr[0][2];
    fm.CBirthday.value   =arr[0][3];
    fm.tIDTypeName.value =arr[0][4];
    fm.tIDType.value     =arr[0][8];
    fm.OtherIDNo.value   =arr[0][9];
    fm.OtherIDType.value =arr[0][10];
    fm.tIDNo.value    =arr[0][5];

	var addresscode = arr[0][11];
	
	//#2329 （需求编号：2014-229）客户基本信息要素录入需求
	fm.ShowSexName.value     	=arr[0][12];
	fm.ShowNativePlace.value 	=arr[0][13];
	fm.ShowOccupationType.value =arr[0][14];
	fm.IDStartDate.value        =arr[0][15];
	fm.IDEndDate.value          =arr[0][16];
	
	if (addresscode != null && addresscode != '') {
    	strSQL = "select b.PostalAddress,b.homephone from lcaddress b where customerno='"+fm.CustomerNo.value+"' and b.AddressNo='" + addresscode +"'";
    	arr = easyExecSql(strSQL);
    	if (arr){
    		fm.ShowPostalAddress.value  =arr[0][0];
			fm.ShowPhone.value          =arr[0][1];
    	}else{
    	    fm.ShowPostalAddress.value  ='';
			fm.ShowPhone.value          ='';
    	}
    }	
    	
    var rgtSQL = "";
    if (fm.RgtNo.value != null && fm.RgtNo.value != "") {
    	rgtSQL = "and exists (select 1 from llregister where rgtobjno=a.grpcontno and rgtno='"+fm.RgtNo.value+"') ";
    }
    
    var strPhoneSQl = "select distinct a.grpinsuredphone From lcinsured a,lcpol b "
    	            + " where a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' and b.conttype='2' "
    	            + " and a.insuredno=b.insuredno and a.grpinsuredphone is not null and length(a.grpinsuredphone)=11 "
    	            + rgtSQL
    	            + " and a.insuredno='" + fm.CustomerNo.value + "' union "
    	            + " select distinct a.grpinsuredphone From lbinsured a,lbpol b "
    	            + " where a.contno=b.contno and b.appflag='1' and b.stateflag<>'4' and b.conttype='2' "
    	            + " and a.insuredno=b.insuredno and a.grpinsuredphone is not null and length(a.grpinsuredphone)=11 "
    	            + rgtSQL
    	            + " and a.insuredno='" + fm.CustomerNo.value + "' fetch first 1 rows only with ur";
    
    var PhoneARR =  easyExecSql( strPhoneSQl, 1,0,1);
    if (PhoneARR) {
    	fm.MobilePhone.value=PhoneARR[0][0];
    }
    
    if (addresscode != null && addresscode != '' && (fm.MobilePhone.value ==null || fm.MobilePhone.value == "")) {
    	strSQL = "select b.mobile from lcaddress b where customerno='"+fm.CustomerNo.value+"' and b.AddressNo='" + addresscode +"'";
    	arr = easyExecSql( strSQL, 1,0,1);
    	if (arr){
    		fm.MobilePhone.value=arr[0][0];
    	}
    }
    
    searchBankAcc();
    QueryEvent();
    showPRemark();
  }
}

// 根据客户号，查询客户最新的理赔金账户信息
// 如果是团体统一给付下的案件，不允许修改个人的给付方式和理赔金账户，都为空
function searchBankAcc() {
	if (fm.CustomerNo.value != null && fm.CustomerNo.value != "") {
		var tTogetherFlag = "";
		if (fm.RgtNo.value != null && fm.RgtNo.value != "") {
			var TogetherFlagSQL = "select togetherflag from llregister where rgtno='"+fm.RgtNo.value+"'";
			tTogetherFlag = easyExecSql(TogetherFlagSQL);
		}
		
		if (tTogetherFlag != "3") {
			var strSQL ="select distinct a.Bankcode,a.Bankaccno,a.Accname,a.modifydate,a.modifytime "
				+ " from lcinsured a where a.insuredno='"+fm.CustomerNo.value+"' and a.bankcode is not null and a.bankaccno is not null and a.accname is not null "
				+ " union select distinct a.Bankcode,a.Bankaccno,a.Accname,a.modifydate,a.modifytime "
				+ " from lbinsured a where a.insuredno='"+fm.CustomerNo.value+"' and a.bankcode is not null and a.bankaccno is not null and a.accname is not null "
				+ " ORDER BY ModifyDate DESC,ModifyTime DESC";
			var drr= easyExecSql( strSQL,1,0,1 );
			if (drr&&drr[0][1]!='') {
				fm.BankCode.value=drr[0][0];
			    fm.BankAccNo.value=drr[0][1];
			    fm.AccName.value=drr[0][2];
			}
		} else {
			if(fm.paymode.value == "1" || fm.paymode.value == "2") {
				divBankAcc.style.display='none';
		        fm.BankName.value='';
		        fm.AccName.value='';
		        fm.BankCode.value='';
		        fm.BankAccNo.value='';
		    }
		    
			fm.paymode.readOnly=true;
			fm.BankName.readOnly=true;
			fm.AccName.readOnly=true;
			fm.BankCode.readOnly=true;
			fm.paymodename.readOnly = true;
			fm.BankAccNo.readOnly = true
			fm.paymode.onclick='';
			fm.BankCode.onclick='';
			fm.paymode.onkeyup='';
			fm.BankCode.onkeyup='';			
		}
	}
}

//判断查询输入窗口的案件类型是否是回车，
//如果是回车调用查询客户函数
function QueryOnKeyDown(){
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13"){
    ClientQuery();
  }
}

//弹出窗口显示复选框选择受益人
function OpenBnf(){
  var pathStr="./LLBnfMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&LoadFlag="+1+"&CustomerName="+fm.CustomerName.value+"&RgtNo="+fm.RgtNo.value+"&paymode="+fm.paymode.value;
  showInfo=OpenWindowNew(pathStr,"EventInput","middle");
}
//弹出窗口下发问题件
function OnLower(){
  var pathStr="./LLOnlinClaimQuestionMain.jsp?InsuredNo=" + fm.CustomerNo.value+"&CaseNo="+fm.CaseNo.value+"&CustomerName="+fm.CustomerName.value+"&Handler="+fm.Handler.value;
  showInfo=OpenWindowNew(pathStr,"EventInput","middle");
}

//打印接口
function PrintPage(){
  var caseno=fm.CaseNo.value;
  if(fm.CaseNo.value==""){
    alert("请先保存数据");
    return false;
  }
 // var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'lp005' and otherno='"+caseno+"'");
    fm.payreceipt.disabled=true;
//	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
//	{

//		fm.action="./PayAppPrt.jsp?CaseNo="+caseno;
//		fm.submit();
//	}
//	else
//	{   
		//window.open("../uw/PrintPDFSave.jsp?Code=0lp008&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ActuGetNo+"&PrtSeq="+PrtSeq);
	//	fm.action = "../uw/PrintPDFSave.jsp?Code=0lp005&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ caseno +"&PrtSeq="+PrtSeq;
		var showStr="正在准备打印数据，请稍后...";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.target = "fraSubmit";
		fm.action = "../uw/PDFPrintSave.jsp?Code=lp005&OtherNo="+caseno;
		fm.submit();
//	}
  //showInfo = window.open("./PayAppPrt.jsp?CaseNo="+fm.CaseNo.value,"PrintPage",'width=600,height=400,top=100,left=60,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}
//function printPDF()
//{ 	
//    var caseno=fm.CaseNo.value;  //记录案件号
//	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = 'lp005' and otherno='"+caseno+"'");
//	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
//	{
//		alert("打印管理表中数据不存在");
//		return false;
//	}
//	else
//	{
//		//window.open("../uw/PrintPDFSave.jsp?Code=0lp005&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+caseno+"&PrtSeq="+PrtSeq);
//		fm.action = "../uw/PrintPDFSave.jsp?Code=0lp005&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+ caseno +"&PrtSeq="+PrtSeq;
//		fm.submit();
//	}
//}
////PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	fm.payreceipt.disabled=false;
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

function afterCodeSelect( cCodeName, Field ) {
  if( cCodeName == "llRelation" && Field.value=="00"){
    fm.RgtantName.value = fm.CustomerName.value;
    fm.IDTypeName.value=fm.tIDTypeName.value;
    fm.IDType.value = fm.tIDType.value;
    fm.IDNo  .value=fm.tIDNo.value;

    addSQL = "select b.PostalAddress,b.zipcode,b.phone,b.mobile,b.Email from lcinsured a,lcaddress b where b.AddressNo=a.AddressNo and b.CustomerNo=a.InsuredNo and a.InsuredNo='"+fm.CustomerNo.value+"'";
    crr = easyExecSql( addSQL , 1,0,1);
    if (crr){
      fm.RgtantAddress.value=crr[0][0];
      fm.PostCode.value =crr[0][1];
      fm.RgtantPhone.value = crr[0][2];
      fm.Mobile.value=crr[0][3];
      fm.Email.value = crr[0][4];
    }
  }
  if( cCodeName == "paymode"){
    if(Field.value=="1"||Field.value=="2"){
      divBankAcc.style.display='none';
      fm.BankAccM.disabled=true;
    }
    else{
      divBankAcc.style.display='';
      fm.BankAccM.disabled=false;
      searchBankAcc();
    }
  }
//#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
  if((fm.RgtNo.value!=null && fm.RgtNo.value.length>0) || (fm.CaseNo.value!=null && fm.CaseNo.value.length>0)){
  var ssqlrtgto ="select rgtclass,TogetherFlag from LLRegister where 1=1 and (rgtno ='"+fm.RgtNo.value+"'  or rgtno='"+fm.CaseNo.value+"' ) ";
	var ssqlrtgtoresult=easyExecSql(ssqlrtgto);
	if(ssqlrtgtoresult){
		if( (ssqlrtgtoresult[0][0]=="1" && (ssqlrtgtoresult[0][1]=="1" || ssqlrtgtoresult[0][1]=="2"  )) || ssqlrtgtoresult[0][0]=="0" ){
			if(fm.paymode.value!="1" && fm.paymode.value!="2"){
				  if( cCodeName == "relation" ){ //领款人和被保险人关系
					  if(Field.value=="00"){//本人
						  fm.DrawerIDType.value = fm.tIDType.value;
						  fm.DrawerIDTypeName.value=fm.tIDTypeName.value;
						  fm.DrawerID.value = fm.tIDNo.value;
					  }
					}
				}
		}
	}
  }
  
  
  if((fm.RgtNo.value==null || fm.RgtNo.value.length==0)){
				if(fm.paymode.value!="1" && fm.paymode.value!="2"){
					  if( cCodeName == "relation" ){ //领款人和被保险人关系
						  if(Field.value=="00"){//本人
							  fm.DrawerIDType.value = fm.tIDType.value;
							  fm.DrawerIDTypeName.value=fm.tIDTypeName.value;
							  fm.DrawerID.value = fm.tIDNo.value;
						  }
						}
					}
	  }
  
  //#3729 领款人和被保险人关系字段必录功能及增加领款人证件类型与证件号码需求
}

function RgtFinish(){
	fm.all('OpType').value="OK";
  strSQL = " select AppPeoples,rgtstate from LLRegister where rgtno = '" + fm.all('RgtNo').value + "' and rgtclass = '1'";
  arrResult = easyExecSql(strSQL);
  if(arrResult){
    if ( arrResult[0][1]!='01'){
      alert("团体案件状态不允许该操作！");
      return;
    }
    var AppPeoples;
    AppPeoples = arrResult[0][0];
  }
  else{
    alert("团体案件信息查询失败！");
    return;
  }

  strSQL = " select count(*) from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
  arrResult = easyExecSql(strSQL);
  if(arrResult){
    var RealPeoples;
    RealPeoples = arrResult[0][0];
  }
  strSQL = " select customername from llcase where rgtno = '" + fm.all('RgtNo').value + "'";
  brr = easyExecSql(strSQL)
  if(brr){
    count = brr.length;
  }
  if ( RealPeoples < AppPeoples ){
    if(confirm("团体申请下个人申请尚未全部申请完毕！" + "团体申请人数：" + AppPeoples + "实际申请人数：" + RealPeoples+ "\n您确认要结束本团体批次吗?")){
      fm.realpeoples.value=RealPeoples;
      alert("本批次申请人数为："+RealPeoples+"人")
    }
    else{
      return;
    }
  }
  fm.realpeoples.value=RealPeoples;
  fm.all('operate').value = "UPDATE||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.action = "./LLGrpRegisterSave.jsp";
  fm.submit();
}

function CaseChange(){
  initTitle();
  fm.Sex.value = fm.Sex.value=='女'?'1':'0';
  ClientafterQuery(fm.RgtNo.value,fm.CaseNo.value);
}

function shouCheck()
{	
	  if(fm.CaseNo.value!=null && fm.CaseNo.value!="")
	{
	var rowNum=EventGrid.mulLineCount;
	var sqlsCheckSubrptNo="select subrptno  from llcaserela where CaseNo='"+fm.CaseNo.value+"' ";
	var CheckSubrptNo= easyExecSql(sqlsCheckSubrptNo);

    for(var i=0;i<rowNum;i++)
    {
    var value2=EventGrid.getRowColDataByName (i,"Subno");
  	 for(var j=0;j<CheckSubrptNo.length;j++){
  	   if (CheckSubrptNo[j][0]==value2){
  	      EventGrid.checkBoxSel(i+1);
  	    }
      }
     }
	}
}


// 根据客户号查询事件信息
// 如果通过案件查询，标记关联事件
function QueryEvent() {
    initEventGrid();
	if (fm.CustomerNo.value == null || fm.CustomerNo.value == "") {
		return true;
	}
	// #3500 发生地点查询
	var strSQLCV="select SubRptNo,AccDate,(select codename from ldcode1 where codetype='province1' and code=accprovincecode), " +
			"(select codename from ldcode1 where codetype='city1' and code=acccitycode fetch first 1 row only),(select codename from ldcode1 where codetype='county1' and code=acccountycode fetch first 1 rows only), " +
			"AccPlace,AccidentType,AccSubject,HospitalName,InHospitalDate,OutHospitalDate,AccDesc,"
		+ "case when AccidentType='1' then '疾病' when AccidentType='2' then '意外' else '' end," +"AccidentType,accprovincecode,acccitycode,acccountycode from LLSubReport where CustomerNo='"
		+ fm.CustomerNo.value +"' order by AccDate desc";
		
    turnPage1.pageLineNum = 20;
	turnPage1.queryModal(strSQLCV,EventGrid);
	shouCheck();
}

/**
*申诉信息
**/
function OnAppeal(){
  IdAppeal.style.display='';
  fm.IdAppConf.value ="申诉保存";
  tAppealFlag="1";//申诉Flag状态改变
}

function showBank()
{
  if(fm.paymode.value=="1"||fm.paymode.value=="2"){
    divBankAcc.style.display='none';
    fm.BankAccM.disabled=true;
  }
  else{
    divBankAcc.style.display='';
    fm.BankAccM.disabled=false;
  }
}

function DealCancel(){
  var varSrc="&CaseNo=" + fm.CaseNo.value +"&CustomerNo="+fm.CustomerNo.value+"&CustomerName="+fm.CustomerName.value;
  showInfo = window.open("./FrameMainCaseCancel.jsp?Interface=LLCaseCancelInput.jsp"+varSrc,"撤件",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}

function showPRemark(){
	if (fm.CustomerNo.value == null || fm.CustomerNo.value == "") {
		return true;
	}

  var grpResult = new Array();
  var perResult = new Array();
  var arrResult = new Array();
  var polResult = new Array();
  var temp="";
  var str1SQL="select grpcontno from lccont where "+
  " insuredno='"+fm.CustomerNo.value+"' "+
  " and grpcontno in (select grpcontno from lcgrpcont where appflag='1')"+
  " and appflag='1'";
  grpResult=easyExecSql(str1SQL);
  if(grpResult != null&&grpResult != ''){
    for ( i = 0; i <= grpResult.length ;i++){
      var strSQL="select '保单'||grpcontno||'约定:'||remark from lcgrpcont "+
      "where grpcontno='"+grpResult[i]+"'";
      arrResult=easyExecSql(strSQL);

      var polSQL="Select '保障计划'||CONTPLANCODE||'下险种'||RISKCODE||coalesce((select codename from ldcode where codetype='CalTitle' and code=c.calfactor),'')||'约定:'||remark FROM LCCONTPLANDUTYPARAM c "+
      " where grpcontno='"+grpResult[i]+"' AND CONTPLANCODE!='11' and remark !='' "+
      "GROUP BY CONTPLANCODE,RISKCODE,remark,calfactor";
      polResult=easyExecSql(polSQL);
      if(arrResult != null&&arrResult != ''){
        try{
          temp += arrResult+"\n";
        }
        catch(ex){
          alert(ex.message)
        }
      }
      if(polResult !=null&&polResult != ''){
        try{
          temp += polResult+"\n";
        }
        catch(ex){
          alert(ex.message)
        }
      }
    }
  }
  var str2SQL="select contno from lccont where "+
  " insuredno='"+fm.CustomerNo.value+"' "+
  " and appflag='1'" ;
  perResult=easyExecSql(str2SQL);
  if(perResult != null&&perResult != ''){
    for ( i = 0; i <= perResult.length ;i++){
      var str3SQL="select '保单'||contno||'下险种'||(select riskcode from lcpol p where p.polno=c.polno)||'约定:'||speccontent from lcpolspecrela c "+
      " where contno ='"+perResult[i]+"'";
      arrResult=easyExecSql(str3SQL);
       var lptbInfoSql="select Impart from lptbinfo  where contno='"+perResult[i]+"'";
       var lptbInfoResult=easyExecSql(lptbInfoSql);
      if(arrResult != null&&arrResult != ''){
        try{
          temp +=	arrResult+"\n";
        }
        catch(ex){
          alert(ex.message)
        }
      }
      else{
      }
        if(lptbInfoResult != null&&lptbInfoResult != ''){
        try{
         temp +=	lptbInfoResult+"\n";
        }
        catch(ex){
          alert(ex.message)
        }
      }
      else{
      }
      
      
      
    }
  }
  fm.all('ContRemark').value=temp.replace(/\,/g,"\n");
  if(temp==""){
    fm.all('ContRemark').value="无特别约定，请依照条款处理";
  }
}

function ContQuery(){
  var varSrc = "&CaseNo=" + fm.CaseNo.value;
  varSrc += "&InsuredNo=" + fm.CustomerNo.value;
  varSrc += "&CustomerName=" + fm.CustomerName.value;
  varSrc += "&RgtNo=" + fm.RgtNo.value;
  pathStr="./FrameMainContQuery.jsp?Interface=LLContQueryInput.jsp"+varSrc;
  showInfo = OpenWindowNew(pathStr,"LLContQueryInput","middle",800,330);
}

function SendMail(){
	showInfo.close();
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "邮件发送成功！" ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
}
function SendMsg()
{
	showInfo.close();
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "短信发送成功！" ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
}
	
function openIssue()
{

	var varSrc = "&CaseNo=" + fm.CaseNo.value;
	varSrc += "&CustomerNo=" + fm.CustomerNo.value;
	varSrc += "&CustomerName=" + fm.CustomerName.value;
	varSrc += "&RgtNo=" + fm.RgtNo.value;
	varSrc += "&Operator=" + fm.Handler.value;
	//alert(varSrc);
	pathStr="./FrameIssue.jsp?Interface=LLIssueInput.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLIssueInput","middle",850,500);

}

function CertifySearch() {
	if ((fm.ContNo.value==""||fm.ContNo.value==null)
	    &&(fm.tIDNo.value==""||fm.tIDNo.value==null)) {
	    	alert("请输入证件号码或保单号码");
	    	return false;
	}
	
	var varSrc = "&ContNo=" + fm.ContNo.value;
	varSrc += "&IDNo=" + fm.tIDNo.value;
	pathStr = "./FrameCertify.jsp?Interface=LLWSCertify.jsp"+varSrc;
	showInfo = OpenWindowNew(pathStr,"LLWSCertify","middle",850,500);
}

function afterCertify (CustomerNo) {
	fm.CustomerNo.value = CustomerNo;
	fm.CustomerName.value = "";
	fm.tIDNo.value = "";
	fm.ContNo.value = "";
	ClientQuery();
}

function checkID1 (vIDNO) {
    if(vIDNO==""||vIDNO==null||fm.tIDType.value==""||fm.tIDType.value==null){
    alert("证件号码和证件类型不能为空，请确认是否录入,申请失败！");
    return false;
    }
    if(fm.CustomerNo.value!=""&&fm.CustomerNo.value!=null)
	{  
		var strSql = "select idtype,idno from lcinsured where insuredno='"+fm.CustomerNo.value+"' and idno = '"+vIDNO + "' and idtype = '"+fm.tIDType.value +"' union all select idtype,idno from lbinsured where insuredno='"+fm.CustomerNo.value +"' and idno = '" +vIDNO +  "' and idtype = '"+fm.tIDType.value +"' with ur";
        var arrResult = easyExecSql(strSql);
    if (arrResult){
    
    } else {
    	 alert("录入的证件号码【"+vIDNO+"】或者证件类型【"+fm.tIDType.value+"】有误,申请失败！");
         return false;
    }
  }
  return true;
}

//by gzh 20120912
//整理对保全的校验
function checkBQ(){
	var tError = "";//非阻断错误
	var tZDError = "";//阻断错误
	var tTorG = "";//若无对应的保全项目，该变量仅赋值，不使用。
	var customerno=fm.CustomerNo.value;
	//正在进行中，提示不阻断
	var tIngSql = " select a.grpcontno,a.contno,b.edortype,b.edorno, "
			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
			+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
			+ " where a.grpcontno=b.grpcontno "
			+ " and a.insuredno='"+customerno+"' "
			+ " and a.grpcontno!='00000000000000000000' "
			+ " and c.edoracceptno=b.edorno"
			+ " and c.edorstate != '0' "
			+ " and b.edortype in ('RR','RS','AC','WJ','YS')"
			+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
			+ " union all "
			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
			+ " from lcinsured a,lpedoritem b,lpedorapp c "
			+ " where a.contno=b.contno "
			+ " and a.insuredno='"+customerno+"' "
			+ " and a.grpcontno='00000000000000000000' "
			+ " and c.edoracceptno=b.edorno"
			+ " and c.edorstate != '0' "
			+ " and b.edortype in ('ZF','PC','AD','YS','AE')"
			+ " group by a.grpcontno,a.contno,b.edortype,b.edorno ";
	var tIngArr = easyExecSql(tIngSql);
 
	if(tIngArr){//存在正在进行中的保全项目
		for(var i=0;i<tIngArr.length;i++){
			if(tIngArr[i][0] != null && tIngArr[i][0] != "" && tIngArr[i][0] != "00000000000000000000"){
				tTorG = "团单(保单号码："+tIngArr[i][0]+")";
				
			}else{
				tTorG = "个单(保单号码："+tIngArr[i][1]+")";
			}
			//提示不阻断
			tError +="该客户所在"+tTorG+"正在进行"+tIngArr[i][4]+"保全操作,工单号为："+tIngArr[i][3]+"。 \n";
		}
	}
	//正在进行中，提示且阻断
	var tIngSql1 = "select * from ( "
				+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    			+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
    			+ " where a.grpcontno=b.grpcontno "
    			+ " and a.insuredno='"+customerno+"' "
    			+ " and a.grpcontno!='00000000000000000000' "
    			+ " and c.edoracceptno=b.edorno"
    			+ " and c.edorstate != '0' "
    			+ " and b.edortype in ('WT','XT','GA','TQ','TA','SG','CT','TF','LQ','ZB') "//团单保全
    			+ " union all "
    			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    			+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    			+ " from lcinsured a,lpedoritem b,lpedorapp c "
    			+ " where a.contno=b.contno "
    			+ " and a.insuredno='"+customerno+"' "
    			+ " and a.grpcontno='00000000000000000000' "
    			+ " and c.edoracceptno=b.edorno"
    			+ " and c.edorstate != '0' "
    			+ " and b.edortype in ('PR','BP','BA','FC','FX','WT','GF','RF','LN','BF','XT','BC','CM','TB','NS','TF','CT','WX','ZB','LQ','RB')"//个单保全
    			+ " ) as temp "
    			+ " group by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname "
    			+ " order by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname ";
	var tIngArr1 = easyExecSql(tIngSql1);
	if(tIngArr1){
		for(var i=0;i<tIngArr1.length;i++){
			if(tIngArr1[i][0] != null && tIngArr1[i][0] != "" && tIngArr1[i][0] != "00000000000000000000"){
				tTorG = "团单(保单号码："+tIngArr1[i][0]+")";
				
			}else{
				tTorG = "个单(保单号码："+tIngArr1[i][1]+")";
			}
			tZDError +="该客户所在"+tTorG+"正在进行"+tIngArr1[i][4]+"保全操作,工单号为："+tIngArr1[i][3]+"。 \n";
		}
	}
	//该被保人所在分单的保全项目
	var tIngSql2 = " select a.grpcontno,a.contno,b.edortype,b.edorno, "
				+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
				+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
				+ " where a.grpcontno=b.grpcontno "
				+ " and a.insuredno='"+customerno+"' "
				+ " and a.grpcontno!='00000000000000000000' "
				+ " and c.edoracceptno=b.edorno"
				+ " and c.edorstate != '0' "
				+ " and b.edortype in ('LP','ZT','BC','CM','WD','JM')"
				+ " group by a.grpcontno,a.contno,b.edortype,b.edorno "
				+ " order by a.grpcontno,a.contno,b.edortype,b.edorno ";
    var tIngArr2 = easyExecSql(tIngSql2);
    if(tIngArr2){
    	for(var i=0;i<tIngArr2.length;i++){
    		tTorG = "团单(保单号码："+tIngArr2[i][0]+")";
    		var tempSql = " select 1 from lpinsured a "
					 + " where a.grpcontno='"+tIngArr2[i][0]+"' "
					 + " and a.contno = '"+tIngArr2[i][1]+"' "
					 + " and a.edortype = '"+tIngArr2[i][2]+"' "
					 + " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate<>'0') "
					 + " union all "
					 + " select 1 from lpbnf a "
					 + " where 1=1 "
					 + " and a.contno = '"+tIngArr2[i][1]+"' "
					 + " and a.edortype = '"+tIngArr2[i][2]+"' "
					 + " and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate<>'0') "
					 + " with ur ";
			var tempArr = easyExecSql(tempSql);
			if(tempArr){
				if(tIngArr2[i][2] == "LP"){
					tError +="该客户正在"+tTorG+"中进行"+tIngArr2[i][4]+"保全操作,工单号为："+tIngArr2[i][3]+"。 \n";
				}else{
					tZDError +="该客户正在"+tTorG+"中进行"+tIngArr2[i][4]+"保全操作,工单号为："+tIngArr2[i][3]+"。 \n";
				}
			}
    	}
    }
	//保全处理完成
	var tDoneSql = "select * from ( "
			+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lcinsured a,lpgrpedoritem b,lpedorapp c"
    		+ " where a.grpcontno=b.grpcontno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno!='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('RS','WT','XT','CT') "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lcinsured a,lpedoritem b,lpedorapp c "
    		+ " where a.contno=b.contno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('FC','WT','XT','CT') "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lbinsured a,lpgrpedoritem b,lpedorapp c"
    		+ " where a.grpcontno=b.grpcontno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno!='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('RS','WT','XT','CT') "
    		+ " union all "
    		+ " select a.grpcontno,a.contno,b.edortype,b.edorno, "
    		+ " (select edorname from lmedoritem where edorcode = b.edortype fetch first 1 rows only )"
    		+ " from lbinsured a,lpedoritem b,lpedorapp c "
    		+ " where a.contno=b.contno "
    		+ " and a.insuredno='"+customerno+"' "
    		+ " and a.grpcontno='00000000000000000000' "
    		+ " and c.edoracceptno=b.edorno"
    		+ " and c.edorstate = '0' "
    		+ " and b.edortype in ('FC','WT','XT','CT') "
    		+ " ) as temp "
    		+ " group by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname "
    		+ " order by temp.grpcontno,temp.contno,temp.edortype,temp.edorno,temp.edorname ";
	var tDoneArr = easyExecSql(tDoneSql);
	if(tDoneArr){//存在已完成的保全项目
		for(var i=0;i<tDoneArr.length;i++){
			if(tDoneArr[i][0] != null && tDoneArr[i][0] != "" && tDoneArr[i][0] != "00000000000000000000"){
				tTorG = "团单(保单号码："+tDoneArr[i][0]+")";
				
			}else{
				tTorG = "个单(保单号码："+tDoneArr[i][1]+")";
			}
			//由20121115修改，需求调整，做完保全的，仅提示不阻断
			tError +="该客户已在"+tTorG+"中做过"+tDoneArr[i][4]+"保全操作，工单号为："+tDoneArr[i][3]+"。 \n";
		}
	}
	//没有保全号的保全项目(各种满期给付各种特殊处理)。。。。//0 给付完成；1 正在给付。
	//常无忧B满期给付
	var tCWMJSql = " select temp.ContNo,temp.edorstate "
			   + " from "
			   + " ( "
			   + " select a.contno," 
			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate "
			   + " from ljsgetdraw a where insuredno='"+customerno+"' and feefinatype='TF' and riskcode='330501' "
			   + " ) as temp"
			   + " group by temp.ContNo,temp.edorstate "
			   + " order by temp.ContNo,temp.edorstate ";
	var tCWMJArr = 	easyExecSql(tCWMJSql);
	if(tCWMJArr){
		for(var i=0;i<tCWMJArr.length;i++){
			tTorG = "个单(保单号码："+tCWMJArr[i][0]+")";
			if(tCWMJArr[i][1] == "1"){
				tZDError +="该客户所在"+tTorG+"正在进行常无忧B满期给付保全操作。 \n";
			}
		}
	}
	//个单满期给付
	var tGDMJSql = " select temp.ContNo,temp.edorstate "
			   + " from "
			   + " ( "
			   + " select a.contno," 
			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate "
			   + " from ljsgetdraw a where insuredno='"+customerno+"' and feefinatype='TF' and riskcode not in ('170206','330501') "
			   + " ) as temp"
			   + " group by temp.ContNo,temp.edorstate "
			   + " order by temp.ContNo,temp.edorstate ";
	var tGDMJArr = 	easyExecSql(tGDMJSql);
	if(tGDMJArr){
		for(var i=0;i<tGDMJArr.length;i++){
			tTorG = "个单(保单号码："+tGDMJArr[i][0]+")";
			if(tGDMJArr[i][1] == "0"){//需求调整，做完保全的仅提示不阻断
				tError +="该客户已在"+tTorG+"中做过个单满期给付保全操作。 \n";
			}else{
				tZDError +="该客户所在"+tTorG+"正在进行个单满期给付保全操作。 \n";
			}
		}
	}   
	var tMJSql = " select temp.GrpContNo,temp.edorName,temp.edorstate "
	           + " from "
	           + " ( "
	           + " select a.grpcontno GrpContNo,'团单满期给付' edorName," //团单满期给付
			   + " (case when exists (select 1 from ljaget where actugetno=a.getnoticeno) then '0' else '1' end) edorstate"
			   + " from ljsgetdraw a,lcinsured b "
			   + " where a.grpcontno = b.grpcontno and b.grpcontno != '00000000000000000000' "
			   + " and a.feefinatype='TF' and a.riskcode='170206' "
			   + " and b.insuredno='"+customerno+"' "
			   + " union all "
			   + " select a.contno GrpContNo,'特需险满期给付' edorName,'1' edorstate" //特需险满期给付
			   + " from lgwork a,lcinsured b " 
			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
			   + " and a.typeno='070015' "
			   + " and b.insuredno='"+customerno+"' "
			   + " and a.statusno != '5' "
			   + " union all "
			   + " select a.contno GrpContNo,'特需险满期给付' edorName,'0' edorstate" //特需险满期给付
			   + " from lgwork a,lcinsured b " 
			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
			   + " and a.typeno='070015' "
			   + " and b.insuredno='"+customerno+"' "
			   + " and a.statusno = '5' "
			   + " ) as temp "
			   + " group by temp.GrpContNo,temp.edorName,temp.edorstate "
			   + " order by temp.GrpContNo,temp.edorName,temp.edorstate "
			   + " with ur "; 
	var tMJArr = easyExecSql(tMJSql);
	if(tMJArr){
		for(var i=0;i<tMJArr.length;i++){
			tTorG = "团单(保单号码："+tMJArr[i][0]+")";
			if(tMJArr[i][2] == "0"){//需求调整，做完保全的，仅提示不阻断
				tError +="该客户所在"+tTorG+"已做过"+tMJArr[i][1]+"\n";
			}else{
				tZDError +="该客户所在"+tTorG+"正在进行"+tMJArr[i][1]+"\n";
			}
		}
	}
	//所在团单正在定期结算
	var tDJSql = " select distinct a.contno "
			   + " from lgwork a,lcinsured b "
			   + " where a.contno = b.grpcontno and b.grpcontno != '00000000000000000000' "
			   + " and b.grpcontno not in (select code from ldcode where codetype='lp_dqjs_pass') "
			   + " and a.typeno like '06%' and a.statusno not in ('5','8') "
			   + " and b.insuredno='"+customerno+"' ";
	var tDJArr = 	easyExecSql(tDJSql);
	if(tDJArr){
		for(var i=0;i<tDJArr.length;i++){
			tTorG = "团单(保单号码："+tDJArr[i][0]+")";
			tZDError +="该客户所在"+tTorG+"正在进行定期结算保全操作。 \n";
		}
	}
	//若存在多个保单，则仅提示不阻断
	var tCSQL = "select count(1) from lcinsured where insuredno = '"+customerno+"' "
			  + " and exists (select 1 from lccont where prtno = lcinsured.prtno and appflag = '1') "
			  + "having count(1) >1 ";
	var tCArr = 	easyExecSql(tCSQL);
	if(tCArr){
		tError = tError + tZDError;
		tZDError = "";
	}
	if(tError != ""){//提示不阻断处理
		tError +="是否继续？";
		if(!confirm(tError)){
			return false;
		}
	}
	if(tZDError != ""){//提示不阻断处理
		alert(tZDError);
		return false;
	}
	return true;
}

//#2684关于理赔环节黑名单监测规则修改的需求  added by zqs
function checkBlacklist(checkName,checkId){
	//需要验证的证件号与人名都为空
	if((checkId==""||checkId==null)&&(checkName==""||checkName==null)){
		return true;
	}
	//需要验证的证件号为空
	if(checkId==""||checkId==null){
		var strblack="select 1 from lcblacklist where trim(name)in('"+checkName+"') and (idno='' or idno is null) with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   	   	  		//alert("若“证件号码”在黑名单库中为空");
   	   	  		if(!confirm("该客户为黑名单客户,是否继续处理此案件?")){
   	   	      		return false;
   	   	    	}
		}
		return true;
	}
	//需要验证的姓名为空
	if(checkName==""||checkName==null){
		var strblack=" select 1 from lcblacklist where trim(idno)in('"+checkId+"')with ur";
		var arrblack=easyExecSql(strblack);
   		if(arrblack){
   			//alert("姓名”不匹配为空，“证件号码”匹配");
   			if(!confirm("该客户为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		return true;
	}
	
	//1.若理赔案件和黑名单库中的 “姓名”匹配情况下，分三种情况
	var strblack=" select name,idno from lcblacklist where trim(name)in('"+checkName+"') or trim(idno)in('"+checkId+"') with ur";
   	var arrblack=easyExecSql(strblack);
   	if(arrblack){
   		var strblack1=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and trim(idno)in('"+checkId+"') with ur";
   		var arrblack1=easyExecSql(strblack1);
   		if(arrblack1){
   			alert("该客户为黑名单客户");
   	   	  	return false;
   		}
   		var strblack2=" select 1 from lcblacklist where trim(idno)in('"+checkId+"') with ur";
   		var arrblack2=easyExecSql(strblack2);
   		if(arrblack2){
   			if(!confirm("该客户为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
   		var strblack3=" select 1 from lcblacklist where trim(name)in('"+checkName+"') and (trim(idno)in('') or idno is null) with ur";
   		var arrblack3=easyExecSql(strblack3);
   		if(arrblack3){
   			if(!confirm("该客户为黑名单客户,是否继续处理此案件?")){
   	   	      return false;
   	   	    }
   		}
     }
     return true;
}
