<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLSurveyReplySave.jsp
//程序功能：
//创建日期：2005-02-23 11:53:36
//创建人  ：YangMing
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  System.out.println("LLCaseTransferRgtSave.jsp!!!");
  LLCaseSet tLLCaseSet = new LLCaseSet();
 
  CaseTransferBL tCaseTransferBL   = new CaseTransferBL();

  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "part";
  String CaseNo ="";

  
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录

  String tRgtNo = request.getParameter("RgtNo");
  String Operate = request.getParameter("PCLPYcode");
  String Remark = request.getParameter("PCRemark");
  
  ExeSQL tExeSQL = new ExeSQL();
  //理赔二核'16'
  String strSql = "select CaseNo from llcase where RgtNo='"+tRgtNo+"' and rgtstate not in ('07','09','12','14','11','13','16')";
  
  System.out.println(strSql + "zjl 查询案件号");
  
  SSRS CaseNoSSRS = tExeSQL.execSQL(strSql);
  
  System.out.println(CaseNoSSRS.getMaxRow() + "zjl 多少案件");
  
  for(int i = 1;CaseNoSSRS.getMaxRow() >= i ; i++)
  {
  		LLCaseSchema tLLCaseSchema = new LLCaseSchema();        	
	    tLLCaseSchema.setCaseNo(CaseNoSSRS.GetText(i, 1));	
	  
	    tLLCaseSet.add(tLLCaseSchema);      
  }
   
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLLCaseSet);
  	tVData.add(tG);
	tVData.add(Remark);
  	System.out.println("transact"+transact);
  	System.out.println("tVData"+tVData);
    tCaseTransferBL.submitData(tVData,Operate);

  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tCaseTransferBL.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
