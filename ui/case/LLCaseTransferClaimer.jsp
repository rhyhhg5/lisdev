<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：wujs
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
		String Handler= request.getParameter("Handler");
		String RgtState= request.getParameter("RgtState");
		String ManageCom= request.getParameter("ManageCom");
		String StartDate= request.getParameter("StartDate");
		String EndDate= request.getParameter("EndDate");
		String RgtNo= request.getParameter("RgtNo");
	    String CustomerName= request.getParameter("CustomerName");
	    String CustomerNo= request.getParameter("CustomerNo");
	    String CaseNo= request.getParameter("CaseNo");
	    String ContType= request.getParameter("ContType");
	   	System.out.print("Handler!!!!!!!!!!!!!!!!!!!"+Handler);
		System.out.print("ManageCom!!!!!!!!!!!!!!!!!!!"+ManageCom);
 %>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LLCaseTransferClaimer.js"></SCRIPT>
   <%@include file="LLCaseTransferInit.jsp"%>
   <script language="javascript">
   	var Handler="<%=Handler%>"
   	var ManageCom="<%=ManageCom%>"
   	var RgtState="<%=RgtState%>"
   	var Operator="<%=Operator%>"
   	var StartDate="<%=StartDate%>"
   	var EndDate="<%=EndDate%>"
   	var ContType="<%=ContType%>"
   	var CaseNo="<%=CaseNo%>"
   	var CustomerNo="<%=CustomerNo%>"
   	var CustomerName="<%=CustomerName%>"
   	var RgtNo="<%=RgtNo%>"
   </script>
 </head>
<body  onload="initForm();">
   <form action="./LLCaseTransferSave.jsp" method=post name=fm target="fraSubmit">

<table>
        <TR>
         <TD>
           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput2);">
         </TD>
         <TD class= titleImg>
         理赔人信箱
         </TD>
       </TR>
      </table>
      <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>
       
   <Div  align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div> 
	<table  class= common>
	<TR  class= common8>
<TD  class= title8   >理赔员</TD>
<TD  class= title8 >
	<Input type=hidden  class=code name="llusercode" >
	 <Input class="code" name=llusername 
	 ondblclick="return showCodeList('optname',[this,llusercode],[1,0],null,'<%=Operator%>','upusercode',1);"
	 onkeyup="return showCodeListKey('optname',[this,llusercode],[1,0],null,'<%=Handler%>','upusercode',1);" >
<TD></TD><TD></TD></TR>
<TR  class= common8>
<TD  class= title8   >备注</TD>
<TD >
<textarea name="Remark" cols="60%"  class="common"  ></textarea>
</TD>
<TD >
<input type="button" class=cssButton name="Send" value="人工变更" onclick="submitForm()"> </TD>

</TR>

		<TR class= common8 >
 <td><input type="button" class=cssButton name="Send" value="自动变更" disabled = true onclick="submitFormAuto()"> </TD>
	</TR>

	</table>
	<hr>
	<table>
  	<TR>
    	<TD>
      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClaimerInfo);">
      </TD>
      <TD class= titleImg>
         理赔员统计
      </TD>
  	</TR>
	</table>   
	 <Div  id= "divClaimerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1>
            <span id="spanClaimerGrid" >
            </span>
          </TD>
        </TR>
      </table>
	</Div>
	<Div  align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</Div> 
	
  <hr>
	<table  class= common>
	<TD class= titleImg>
         批次理赔人变更
      </TD>
		<TR  class= common8>
			<TD  class= title8 >理赔员</TD>
			<TD  class= title8 >
				<Input type=hidden  class=code name="PCLPYcode" >
				 <Input class="code" name=PCLPYname 
				 ondblclick="return showCodeList('optname',[this,PCLPYcode],[1,0],null,'<%=Operator%>','upusercode',1);"
				 onkeyup="return showCodeListKey('optname',[this,PCLPYcode],[1,0],null,'<%=Handler%>','upusercode',1);" >
			</TD>
          	<TD  class= title8>批次号</TD>
          	<TD class= input><Input class="common" name="RgtNo" ></TD>
			
			<TR  class= common8> 
			<TD  class= title8   >备注</TD>
			<TD >
			<textarea name="PCRemark" cols="60%"  class="common"  ></textarea>
			</TD>
			<TD >
			<input type="button" class=cssButton name="Send" value="批次理赔人变更" onclick="PCsubmitForm()"> </TD>
			</TD>
			</TR>
	</table>
	<Input type=hidden  class=code name="Hand" >
  </form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

	</body>
</html>
