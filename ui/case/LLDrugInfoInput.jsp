<html> 
<%
  //程序名称：LLDrugInfoInput.jsp
  //程序功能：药品信息
  //创建日期：2014-02-09 00:00:00
  //创建人  ：Liyunxia
  //更新记录：  更新人    更新日期     更新原因/内容
  %>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>

<%
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LLDrugInfoInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LLDrugInfoInit.jsp"%>
  
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<title>药品信息</title>   
</head>
<body onload="initForm(); initElementtype();" >
<form action="./LLDrugInfoSave.jsp" method=post name=fm target="fraSubmit">
			<%@include file="../common/jsp/OperateButton.jsp"%>
			<%@include file="../common/jsp/InputButton.jsp"%>
			<hr>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDHospital1);">
    		</td>
    		 <td class= titleImg>
        		 药品信息
       	 </td>   		 
    	</tr>
    </table>
<Div  id= "divLDHospital1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      药品编码
    </TD>
    <TD  class= input>
      <Input class=common name=DrugCode elementtype=nacessary  verify="药品编码|NOTNULL&len<=30">
    </TD>
    <TD  class= title>
      药品类别
    </TD>
    <TD  class= input>
      <Input class= "codeno"  name=DrugCategoryValue  ondblclick="return showCodeList('drugcategory',[this,DrugCategory],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('drugcategory',[this,DrugCategory],[0,1],null,null,null,1);" readonly verify="药品类别|NOTNULL" ><Input class=codename  name=DrugCategory elementtype=nacessary>
    </TD>    
  	<TD  class= title>
      药品通用名称
    </TD>
    <TD  class= input>
      <Input class=common name=DrugGenericName elementtype=nacessary  verify="药品通用名称|NOTNULL&len<=200">
    </TD>
  	
  </TR>
  <TR  class= common>
    <TD  class= title>
      商品名
    </TD>
    <TD  class= input>
      <Input class=common name=DrugTradeName >
    </TD> 
    <TD  class= title>
      英文名
    </TD>
    <TD  class= input>
      <Input class=common name=DrugEnglishName >
    </TD>
	<TD  class= title>
      药品分类
    </TD>
    <TD  class= input>
	  <Input class= 'code' name=DrugClassification >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      次类别
    </TD>
    <TD  class= input>
      <Input class=common name=SecondCategory >
    </TD> 
    <TD  class= title>
      医保剂型
    </TD>
    <TD  class= input>
      <Input class=common name=Formulations  verify="医保剂型|len<=50">
    </TD>
	<TD  class= title>
      药品规格
    </TD>
    <TD  class= input>
	  <Input class= 'common' name=Specifications verify="药品规格|len<=50">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      拼音简码
    </TD>
    <TD  class= input>
      <Input class=common name=Phonetic >
    </TD> 
    <TD  class= title>
      生产企业名称
    </TD>
    <TD  class= input>
      <Input class=common name=Manufacturer >
    </TD>
	<TD  class= title>
      包装信息
    </TD>
    <TD  class= input>
	  <Input class= 'common' name=PackageMessage  verify="包装信息|len<=50">
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      给药途径
    </TD>
    <TD  class= input>
      <Input class=common name=Administration >
    </TD> 
    <TD  class= title>
      医保序号
    </TD>
    <TD  class= input>
      <Input class=common name=HealthNo >
    </TD>
	<TD  class= title>
      费用等级
    </TD>
    <TD  class= input>
	  <Input class= "codeno"  name=CostLevelValue  ondblclick="return showCodeList('costlevel',[this,CostLevel],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('costlevel',[this,CostLevel],[0,1],null,null,null,1);"  ><Input class=codename  name=CostLevel >
    </TD>
  </TR>  
  <TR  class= common>
    <TD  class= title>
      限定支付范围
    </TD>
    <TD  class= input>
      <Input class=common name=PaymentScope>
    </TD> 
    <TD  class= title>
      限门诊使用
    </TD>
    <TD  class= input>
      <Input class= "codeno"  name=OutpatientLimitValue  ondblclick="return showCodeList('yesno',[this,OutpatientLimit],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('yesno',[this,OutpatientLimit],[0,1],null,null,null,1);"><Input class=codename  name=OutpatientLimit >
    </TD>
	<TD  class= title>
      限住院使用
    </TD>
    <TD  class= input>
	  <Input class= "codeno"  name=HospitalLimitValue  ondblclick="return showCodeList('yesno',[this,HospitalLimit],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('yesno',[this,HospitalLimit],[0,1],null,null,null,1);"  ><Input class=codename  name=HospitalLimit >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      工伤标识
    </TD>
    <TD  class= input>
      <Input class= "codeno"  name=InjuriesMarkValue  ondblclick="return showCodeList('yesno',[this,InjuriesMark],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('yesno',[this,InjuriesMark],[0,1],null,null,null,1);"  ><Input class=codename  name=InjuriesMark >
    </TD> 
    <TD  class= title>
      生育标识
    </TD>
    <TD  class= input>
      <Input class= "codeno"  name=FertilityMarkValue  ondblclick="return showCodeList('yesno',[this,FertilityMark],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('yesno',[this,FertilityMark],[0,1],null,null,null,1);"  ><Input class=codename  name=FertilityMark >
    </TD>
	<TD  class= title>
      最高限价
    </TD>
    <TD  class= input>
    	<input class= common name=PriceCeilingValue onkeyup="checkNum(this)"  verify="最高限价|len<=16"/>    
    </TD>
  </TR>  
  <TR  class= common>
    <TD  class= title>
      备注
    </TD>
		<TD  class= input colspan="6">
		    <textarea name="Remark" cols="100%" rows="3">
		    </textarea>
		</TD> 
  </TR>     
  <TR  class= common>
    <TD  class= title>管理机构</TD><TD  class= input>
		<Input class= "codeno"  name=MngCom onkeydown="QueryOnKeyDown();" ondblclick="getstr();return showCodeList('comcode',[this,MngCom_ch], [0,1],null,str,'1');" onkeyup="getstr();return showCodeListKey('comcode', [this,MngCom_ch], [0,1],null,str,'1');"  verify="管理机构|notnull"><Input class=codename  name=MngCom_ch elementtype=nacessary>

    <TD  class= title>地区名称</TD><TD  class= input>
		<Input class= "codeno"  name=AreaCode    ondblclick="return showCodeList('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);" onkeyup="return showCodeListKey('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);"   verify="地区名称|notnull&len<=20" ><Input class=codename  name=AreaName elementtype=nacessary>
  </TR>
</table>
</Div>
<Div id=CurrentlyInfo style="display: ''">  
	 <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		 <td class= titleImg>
        		 药品信息列表（请至少录入‘药品编码’或‘药品类别’或‘药品通用名称’进行查询）
       	 </td>   		 
    	</tr>
    </table> 
	<Div id="comHospital1" style="display:'' ">
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanEvaluateGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
	</div>
</div>

   
    <input type=hidden id="fmtransact" name="Transact">
    <input type=hidden id="fmAction" name="fmAction">

    <input type=hidden name="SpecialClass">
    <input type=hidden name="Operator">
    <input type=hidden name="MakeDate">
    <input type=hidden name="MakeTime">
		<input type=hidden name="ModifyDate">
		<input type=hidden name="ModifyTime">
		<input type=hidden name="LoadFlag">
		<input type = hidden name = HiddenBtn>
		<Input type=hidden name=FlowNo ondblclick="generateFlowNo();">
</form>
<hr>
  <form action="./LLDrugInfoImport.jsp" method=post name=fmlode target="fraSubmit" ENCTYPE="multipart/form-data" >
  	<table class = common >    	
    <TR  >
      <TD  width='8%' style="font:9pt">
        文件名：
      </TD>
      
      <TD  width='80%'>
        <Input  type="file"　width="100%" name=FileName class= common>
        <INPUT  VALUE="药品信息导入" class="cssbutton" TYPE=button onclick = "DrugInfoUpload();" >
        <INPUT  VALUE="导入模板下载" class="cssbutton" TYPE=button onclick = "downLoad();" >
        <Input  type="hidden"　width="100%" name="insuredimport" value ="1">
      </TD>
    </TR>
    
	    <input type=hidden name=ImportFile>
	</table>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
