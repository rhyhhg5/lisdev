<html>
<%
/*******************************************************************************
 * Name     :CasePolicyInput.jsp
 * Function :立案－立案险种明细的初始化页面程序
 * Date     :2003-7-21
 * Author   :LiuYansong
 */
%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>


  <SCRIPT src="CasePolicyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="CasePolicyInit.jsp"%>

</head>
<body  onload="initForm();" >
  <form action="./CasePolicySave.jsp" method=post name=fm target="fraSubmit">
     
    <!--立案保单信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCasePolicy);">
    		</td>
    		<td class= titleImg>
    			 保单明细
    		</td>
    	</tr>
    </table>
	<Div  id= "divCasePolicy" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanCasePolicyGrid" >
					</span>
				</td>
			</tr>
		</table>
  </div>
 <!--
    <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  	</Div>
  -->

         <INPUT VALUE="案件保单保存" TYPE=button class=cssButton onclick="submitForm();">
         <INPUT VALUE="返 回" TYPE=button class=cssButton onclick="top.close()">
     <Div  id= "divCasePolicyDuty1" style= "display: 'none'">    
         <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCasePolicyDuty);">
    		</td>
    		<td class= titleImg>
    			 责任明细
    		</td>
    	</tr>
    </table>
	<Div  id= "divCasePolicyDuty" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanPolicyDutyGrid" >
					</span>
				</td>
			</tr>
		</table>
		<INPUT VALUE="给付责任选择" TYPE=button class=cssButton onclick="">
		<!--
		  <INPUT VALUE="自动责任选择" TYPE=button class=cssButton onclick="submitForm();">
		  <INPUT VALUE="手工责任选择" TYPE=button class=cssButton onclick="submitForm();">
		   <INPUT VALUE="预理算" TYPE=button class=cssButton onclick="submitForm();">
		   -->
  </div>
  </div>
  <input type=hidden name="CaseNo">
  <input type=hidden name="RgtNo">
  <input type=hidden name="CaseRelaNo">
  <input type=hidden name="CustomerNo">
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
  
</body>

</html>