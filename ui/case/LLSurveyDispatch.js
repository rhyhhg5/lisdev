var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var tSaveType="";

function QueryOnKeyDown(){
  var keycode = event.keyCode;
  //回车的ascii码是13
  if(keycode=="13"){
    easyQuery();
  }
}

function easyQuery(){
   var startDate = fm.RgtDateS.value;
  var endDate = fm.RgtDateE.value;
  if((fm.OtherNo.value).charAt(0)=="Z"||(fm.OtherNo.value).charAt(0)=="T") {
  } else {
      if (startDate==""||startDate==null||endDate==""||endDate==null){
    	alert("请输入受理日期");
  	  return false;
  }
  if(dateDiff(startDate,endDate,"M")>3){
      alert("统计期最多为三个月！");
      return false;
  }
  }
  
  var typeRadio="";
  for(i = 0; i <fm.typeRadio.length; i++){
    if(fm.typeRadio[i].checked){
      typeRadio=fm.typeRadio[i].value;
      break;
    }
  }
  	//对于rgtstate=2的情况，属于历史数据，目前‘咨询通知’案件的调查的rgtstate=1
	//对于SurveyClass，系统中不存在‘自动提调’
  var strSql="select distinct a.otherno,substr(a.surveyno,18),b.MngCom,b.startman,b.makedate,"
  +"case b.RgtState when '2' then '咨询通知' when '0' then '受理状态' when '1' then '检录状态' else CodeName('llrgtstate',b.RgtState) end,"
  +"a.InqNo,a.inqper,case b.surveyclass when '0' then '即时提调' when '1' then '一般提调' else '自动提调' end,"
  +"a.othernotype,'','',b.surveysite,(select n.codename from llcase m,ldcode n where m.caseno=b.otherno and m.rgtstate=n.code and n.codetype='llrgtstate') from llinqapply a,llsurvey b where b.surveyno=a.surveyno and not exists (select 1 from llcase where rgtstate='14' and caseno=b.otherno and not exists (select 1 from llcaseoptime where caseno=llcase.caseno and rgtstate='08')) and "
  var sqlpart = getWherePart("a.inqper","Replyer")
  +getWherePart("b.otherno","OtherNo")
  +getWherePart("b.surveyclass","SurveyType")+getWherePart("b.surveyflag","SurveyFlag")
  +getWherePart("b.makedate","SurveyStartDate",">=")+getWherePart("b.makedate","SurveyEndDate","<=")
  +getWherePart("a.InqStartDate","DispatchStartDate",">=")+getWherePart("a.InqStartDate","DispatchEndDate","<=");
  switch(typeRadio){
    case "0":
    divConfCondition.style.display='';
    divconf.style.display='';
    divDispatch.style.display='none';
    divComfirm.style.display='none';
    strSql+= " (a.Dipatcher='"+fm.Inspector.value+"' or a.suber='"+fm.Inspector.value+"' or b.confer='"+fm.Inspector.value
           +"' or exists(select 1 from llsurveydispatch where a.otherno=otherno and a.surveyno=surveyno and a.inqno=inqno and dispatcher='"+fm.Inspector.value+"'))"+sqlpart;
    break
    case "1":
    divConfCondition.style.display='none';
    divconf.style.display='none';
    divDispatch.style.display='';
    divComfirm.style.display='';
    strSql+= " a.InqPer is null and (a.Dipatcher='"+fm.Inspector.value+"' or a.suber='"+fm.Inspector.value+"')"+sqlpart;
    break;
    case "2":
    divConfCondition.style.display='none';
    divconf.style.display='none';
    divDispatch.style.display='';
    divComfirm.style.display='none';
    strSql+= " a.InqPer is not null and (a.Dipatcher='"+fm.Inspector.value+"' or a.suber='"+fm.Inspector.value+"')"+sqlpart;
    break;
    case "3":
    divConfCondition.style.display='';
    divconf.style.display='';
    divDispatch.style.display='none';
    divComfirm.style.display='none';
    strSql+= " b.SurveyFlag ='2' and (b.confer='"+fm.Inspector.value+"' or b.operator='"+fm.Inspector.value+"' or a.Dipatcher='"+fm.Inspector.value+"') and inqper is not null "+sqlpart;
    break;
    case "4":
    divConfCondition.style.display='';
    divconf.style.display='';
    divDispatch.style.display='none';
    divComfirm.style.display='none';
    strSql+="b.SurveyFlag='3' and (b.confer='"+fm.Inspector.value+"' or b.operator='"+fm.Inspector.value+"') and inqper is not null "+sqlpart;
  }
  var dpart = "";
  if(fm.RgtDateS.value != "" && fm.RgtDateS.value != null && fm.RgtDateE.value !="" && fm.RgtDateE.value != null)
  {
  	dpart += " and exists (select 1 from llcase where caseno=b.otherno and rgtdate>='"+fm.RgtDateS.value+"' and rgtdate<='"+fm.RgtDateE.value+"' union"
  		  +" select 1 from LLConsult where consultno=b.otherno and makedate>='"+fm.RgtDateS.value+"' and makedate<='"+fm.RgtDateE.value+"')"
  } 
  strSql = strSql+ dpart + " order by a.otherno";
  
  turnPage.queryModal(strSql, CheckGrid);
  fm.result.value = "";
  fm.newResult.value = "";
}

//对CodeSelect选择后的判
var LoadC;
function afterCodeSelect( cCodeName, Field ){
  if( cCodeName == "optname" )
  {
    strSQL = "select count(*) from llsurvey a,llinqapply b where a.surveyno=b.surveyno and a.SurveyFlag='0' and b.inqper='"+fm.Surveyer.value +"'";

    var arr = easyExecSql(strSQL);
    if(arr){
      fm.Quantity.value=arr[0][0];
    }
  }
}

function getstr()
{
  //alert("aaa");
  str="1 and code like #"+fm.OrganCode.value+"%#";
}


function getRights(){
  var strSQL="select claimpopedom from llclaimuser where usercode='"+fm.Operator.value+"'";
  var arrResult = easyExecSql(strSQL);
  if(arrResult != null)
  {
    var Rights = arrResult[0][0];
    //alert(Rights);

  }
  Str="1 and claimpopedom < #"+Rights+"#";
}

function Dispatch(){
  var tComCode = fm.ComCode.value;
  if(tComCode == "86"){
  	
  }else{
  	tComCode = fm.ComCode.value.substring(0,4);
  	//alert(tComCode);
  }
  if (fm.DealWith.value=="1"){//上报，支持社保
    Str="1 and surveyflag=#1# and stateflag=#1# and comcode=#86#"
    +" union select usercode, username, claimpopedom from llsocialclaimuser where handleflag=#1# and surveyflag=#1# and stateflag=#1# and comcode=#86#";
    
  }
  else{//下发，支持社保,同机构内，可以由一个主管下发给另一个主管
  	
    Str="1 and surveyflag<>#0# and stateflag=#1# and comcode like #"+tComCode+"#||#%# "
    +"union select usercode, username, claimpopedom from llsocialclaimuser where (surveyflag=#1# or surveyflag=#2#) and stateflag=#1# and comcode like #"+tComCode+"#||#%# ";
  }
  //调查主管查询，支持社保
  Str1="1 and surveyflag=#1# and stateflag=#1# and comcode like #"+fm.ComCode.value+"%# "
  	+"union select usercode, username, claimpopedom from llsocialclaimuser where surveyflag=#1# and stateflag=#1# and comcode like #"+fm.ComCode.value+"%#";
}

function submitForm()
{
  //	alert(fm.DealWith.value);
   if(fm.OrganCode.value !="86" && (fm.DealWith.value =="" || fm.DealWith.value == null || fm.Surveyer.value =="" || fm.Surveyer.value == null))
  {	
  	alert("处理和处理人为必录项!");
  	return false;
  }else if(fm.OrganCode.value =="86" && (fm.ComCode.value == null || fm.ComCode.value == "" || fm.Dealer.value == null || fm.Dealer.value == "" ))
  {	
  	alert("下发调查机构和处理人为必录项!");
  	return false;
  }
  fm.selno.value = CheckGrid.getSelNo()-1;
  if (CheckGrid.getSelNo()<=0){
    alert("请选中一条待分配的调查案件");
    return false;
  }
  else{
  	//目前核心已经没有调查案件转发的功能 此处校验形同虚设
    inqperson = CheckGrid.getRowColData(fm.selno.value,8);
    if(fm.DealWith.value=='2'){      
      if(inqperson==''||inqperson==null){
        alert("该案件还未被分配，不能被转发！");
        return false;
      }
    }
    if(fm.DealWith.value=='1'){
      if(inqperson!=''&&inqperson!=null){
        alert("该案件已分配，不能被上报！");
        return false;
      }
    }
  }
  fm.fmtransact.value = "DISPATCH";
  fm.comfirm1.disabled=true;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./LLSurveyDispatchSave.jsp";
  fm.submit(); //提交
}

function afterSubmit( FlagStr, content ){
  fm.comfirm1.disabled=false;
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //showDiv(operateButton,"true");
    // showDiv(inputButton,"false");
    
    var typeRadio="";
    for(i = 0; i <fm.typeRadio.length; i++){
      if(fm.typeRadio[i].checked){
      typeRadio=fm.typeRadio[i].value;
      break;
      }
    }
    
    if (typeRadio==1||typeRadio==2) {
    	var selno = CheckGrid.getSelNo()-1;
    
    	var strSQL="select remark from llinqapply where inqno='"+CheckGrid.getRowColData(selno,7)+"' with ur";
    	var arrResult = easyExecSql(strSQL);
    	if(arrResult!=null){
    		fm.result.value=arrResult[0][0];
    	} 
    }
  }
}

function DealSurvey(){
  var typeRadio="";
  for(i = 0; i <fm.typeRadio.length; i++){
    if(fm.typeRadio[i].checked){
      typeRadio=fm.typeRadio[i].value;
      break;
    }
  }
  
  var selno = CheckGrid.getSelNo()-1;
    
    var strSQL="select remark from llinqapply where inqno='"+CheckGrid.getRowColData(selno,7)+"' with ur";
    var arrResult = easyExecSql(strSQL);
    if(arrResult!=null){
      fm.result.value=arrResult[0][0];
    }
  
  if (typeRadio==1||typeRadio==2){
         
    var caseNo;
    if (selno <0){
    }
    else{
      var varSrc = "&tSurveyNo="+CheckGrid.getRowColData(selno,7);
      varSrc += "&CaseNo=" + CheckGrid.getRowColData(selno,1);
      varSrc += "&InqNo=" + CheckGrid.getRowColData(selno,7);
      varSrc += "&LoadC=2";
    }
    var newWindow = window.open("./SurveyReplyMain.jsp?MenuFlag=0"+varSrc,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  }
}

function SurveyFee(){
  var row=CheckGrid.getSelNo()-1;
  var CaseNo="";
  var SurveyNo="";
  if(row<0){
    CaseNo = '';
    SurveyNo = '';
  }
  else{
    CaseNo = CheckGrid.getRowColData(row,1);
    SurveyNo = CheckGrid.getRowColData(row,2);
  }
  var newWindow = window.open("./SurveyFeeMain.jsp?Interface=LLSurveyFeeInput.jsp&CaseNo="+CaseNo+"&SurveyNo="+SurveyNo,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function AddFee(){
  var newWindow = window.open("./SurveyFeeMain.jsp?Interface=LLAddSurvFeeInput.jsp","",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SumFee(){
  var row=CheckGrid.getSelNo()-1;
  var CaseNo="";
  if(row<0){
    CaseNo = '';
  }
  else{
    CaseNo = CheckGrid.getRowColData(row,1);
  }
  var newWindow = window.open("./SurveyFeeMain.jsp?Interface=LLSurvFeeSum.jsp&CaseNo="+CaseNo,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function GiveEnsure(){
  var newWindow = window.open("./FrameMainSurveyFee.jsp?Interface=LLSurvFeeGive.jsp","",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

function SurveyReply(){
  var row=CheckGrid.getSelNo()-1;
  if(row<0){
    fm.tSurveyNo.value = '';
  }
  else{
    fm.tSurveyNo.value = CheckGrid.getRowColData(row,7);
  }
  var newWindow = window.open("./SurveyConfMain.jsp?MenuFlag=0&tSurveyNo="+fm.tSurveyNo.value,"",'top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}
