<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuInit.jsp
//程序功能：页面初始化
//创建日期：2005-08-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  boolean hasUW = true;
	String missionId = request.getParameter("MissionId");
	String subMissionId = request.getParameter("SubMissionId");
	String activityId = request.getParameter("ActivityId");
	LWMissionDB tLWMissionDB = new LWMissionDB();
	tLWMissionDB.setMissionID(missionId);
	tLWMissionDB.setSubMissionID(subMissionId);
	tLWMissionDB.setActivityID(activityId);
	if (tLWMissionDB.query().size() > 0)
	{
		hasUW = false;
	}
	System.out.println("hasUW" + hasUW);
%>
<SCRIPT>
//初始化表单
function initForm()
{
  try
  {
  	initInput();
  	initUWErrGrid();
  	initPolGrid();
  	initCaseGrid();
  	initAddFeeGrid();
  	initSpecGrid();
  	initManuUWGrid();
  	queryUWErr();
  	queryUWPol();
  	queryUWCase();
  	queryManuUW();
  	//showEdorInfo();
  	//showButtonDiv();
// <%
// 		if (hasUW == false)
// 		{
// 		  out.print("makeWorkFlow();");
// 		}
//%>
  	initElementtype();
  }
  catch (ex)
  {
    alert("页面初始化错误，请重新登陆！");
  }
}

//初始化表单中的Input
function initInput()
{
	fm.MissionId.value = "<%=request.getParameter("MissionId")%>";
	fm.SubMissionId.value = "<%=request.getParameter("SubMissionId")%>";
	fm.EdorNo.value = "<%=request.getParameter("EdorNo")%>";
	fm.CaseNo.value = "<%=request.getParameter("CaseNo")%>";
	fm.AppntNo.value = "<%=request.getParameter("AppntNo")%>";
	fm.ActivityId.value = "<%=request.getParameter("ActivityId")%>";
	mEdorNo = fm.EdorNo.value;
}


//自核信息列表初始化
function initUWErrGrid()
{
  var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="0px";         			//列宽
	  iArray[0][2]=10;          			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="项目类型";    	      //列名
	  iArray[1][1]="50px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[2]=new Array();
	  iArray[2][0]="保单号";    	      //列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  iArray[2][21]="contNo";
	  
	  iArray[3]=new Array();
	  iArray[3][0]="印刷号";    	      //列名
	  iArray[3][1]="80px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="被保人";    	      //列名
	  iArray[4][1]="80px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="险种代码";    	      //列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[6]=new Array();
	  iArray[6][0]="送核原因";         		//列名
	  iArray[6][1]="400px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[7]=new Array();                                                 
	  iArray[7][0]="规则代码";         		//列名                             
	  iArray[7][1]="60px";            		//列宽                             
	  iArray[7][2]=100;            			//列最大值                           
	  iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();                                                 
	  iArray[8][0]="保全类型编码";         		//列名                             
	  iArray[8][1]="60px";            		//列宽                             
	  iArray[8][2]=100;            			//列最大值                           
	  iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  iArray[8][21]="EdorType";
	  
  	UWErrGrid = new MulLineEnter("fm", "UWErrGrid"); 
		UWErrGrid.mulLineCount = 0;
		UWErrGrid.displayTitle = 1;
		UWErrGrid.locked = 1;
		UWErrGrid.canSel = 1;
		UWErrGrid.canChk = 0;
		UWErrGrid.hiddenSubtraction = 1;
		UWErrGrid.hiddenPlus = 1;
		UWErrGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}

//保单信息列表的初始化
function initPolGrid() 
{
	var iArray = new Array();
	try 
	{
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=30;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="险种号";         	//列名
	  iArray[1][1]="60px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[2]=new Array();
	  iArray[2][0]="项目类型";         	//列名
	  iArray[2][1]="60px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[3]=new Array();
	  iArray[3][0]="保单号";         	//列名
	  iArray[3][1]="80px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="印刷号";         	//列名
	  iArray[4][1]="80px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[5]=new Array();
	  iArray[5][0]="险种序号";         		//列名
	  iArray[5][1]="60px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[6]=new Array();
	  iArray[6][0]="险种代码";         		//列名
	  iArray[6][1]="80px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[7]=new Array();
	  iArray[7][0]="被保人客户号";         		//列名
	  iArray[7][1]="80px";            		//列宽
	  iArray[7][2]=100;            			//列最大值
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[8]=new Array();
	  iArray[8][0]="被保人";         		//列名
	  iArray[8][1]="80px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[9]=new Array();
	  iArray[9][0]="保费";         		    //列名
	  iArray[9][1]="60px";            		//列宽
	  iArray[9][2]=100;            			//列最大值
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[10]=new Array();
	  iArray[10][0]="保额";         		    //列名
	  iArray[10][1]="60px";            		//列宽
	  iArray[10][2]=100;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[11]=new Array();
	  iArray[11][0]="档次";         		//列名
	  iArray[11][1]="80px";            		//列宽
	  iArray[11][2]=100;            			//列最大值
	  iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[12]=new Array();
	  iArray[12][0]="保险起期";         		//列名
	  iArray[12][1]="80px";            		//列宽
	  iArray[12][2]=100;            			//列最大值
	  iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[13]=new Array();
	  iArray[13][0]="保险止期";         	//列名
	  iArray[13][1]="60px";            		//列宽
	  iArray[13][2]=100;            			//列最大值
	  iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[14]=new Array();
	  iArray[14][0]="交费间隔";         	//列名
	  iArray[14][1]="60px";            	//列宽
	  iArray[14][2]=100;            		//列最大值
	  iArray[14][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[15]=new Array();
	  iArray[15][0]="交费年期";         	//列名
	  iArray[15][1]="60px";            	//列宽
	  iArray[15][2]=100;            		//列最大值
	  iArray[15][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[16]=new Array();
	  iArray[16][0]="核保状态";         	//列名
	  iArray[16][1]="60px";            	//列宽
	  iArray[16][2]=100;            		//列最大值
	  iArray[16][3]=0;
	  
	  iArray[17]=new Array();
	  iArray[17][0]="项目类型";         	//列名
	  iArray[17][1]="60px";            	//列宽
	  iArray[17][2]=100;            		//列最大值
	  iArray[17][3]=3;
	  iArray[17][21]="EdorType";

	
	  PolGrid = new MulLineEnter("fm", "PolGrid");
	  //这些属性必须在loadMulLine前
	  PolGrid.mulLineCount = 0;
	  PolGrid.displayTitle = 1;
	  PolGrid.locked = 1;
	  PolGrid.canSel = 1;
	  PolGrid.hiddenPlus = 1;
	  PolGrid.hiddenSubtraction = 1;
	  PolGrid.selBoxEventFuncName = "queryUWInfo";
	  PolGrid.loadMulLine(iArray);
	} 
	catch(ex) 
	{
	  alert(ex);
	}
}

//保单信息列表的初始化
function initCaseGrid() 
{
	var iArray = new Array();
	try 
	{
		  iArray[0]=new Array();
		  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		  iArray[0][1]="30px";            		//列宽
		  iArray[0][2]=30;            			//列最大值
		  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[1]=new Array();
		  iArray[1][0]="理赔号";         	//列名
		  iArray[1][1]="80px";            		//列宽
		  iArray[1][2]=100;            			//列最大值
		  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[2]=new Array();
		  iArray[2][0]="客户号";         	//列名
		  iArray[2][1]="80px";            		//列宽
		  iArray[2][2]=100;            			//列最大值
		  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		  iArray[3]=new Array();
		  iArray[3][0]="客户姓名";         	//列名
		  iArray[3][1]="40px";            		//列宽
		  iArray[3][2]=100;            			//列最大值
		  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[4]=new Array();
		  iArray[4][0]="被保人持有的保单";         	//列名
		  iArray[4][1]="80px";            		//列宽
		  iArray[4][2]=100;            			//列最大值
		  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		  iArray[5]=new Array();
		  iArray[5][0]="险种代码";         		//列名
		  iArray[5][1]="60px";            		//列宽
		  iArray[5][2]=100;            			//列最大值
		  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		  iArray[6]=new Array();
		  iArray[6][0]="险种名称";         		//列名
		  iArray[6][1]="80px";            		//列宽
		  iArray[6][2]=100;            			//列最大值
		  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		  iArray[7]=new Array();
		  iArray[7][0]="承保时的核保结论";       //列名
		  iArray[7][1]="80px";            		//列宽
		  iArray[7][2]=100;            			//列最大值
		  iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许


		  CaseGrid = new MulLineEnter("fm", "CaseGrid");
	  //这些属性必须在loadMulLine前
	  CaseGrid.mulLineCount = 0;
	  CaseGrid.displayTitle = 1;
	  CaseGrid.locked = 1;
	  CaseGrid.hiddenPlus = 1;
	  CaseGrid.hiddenSubtraction = 1;
	  CaseGrid.loadMulLine(iArray);
	} 
	catch(ex) 
	{
	  alert(ex);
	}
}

//加费信息
function initAddFeeGrid() {
  var iArray = new Array();
  try 
  {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=30;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="加费金额";         	//列名
		iArray[1][1]="20px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="加费比例";         	//列名
		iArray[2][1]="10px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="加费起始日期";         		//列名
		iArray[3][1]="10px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="加费终止日期";         		//列名
		iArray[4][1]="10px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
		
		
		AddFeeGrid = new MulLineEnter("fm", "AddFeeGrid");
		AddFeeGrid.mulLineCount = 1;
		AddFeeGrid.displayTitle = 1;
		AddFeeGrid.locked = 1;
		AddFeeGrid.canSel = 0;
		AddFeeGrid.hiddenPlus = 1;
		AddFeeGrid.hiddenSubtraction = 1;
		AddFeeGrid.addEventFuncName="checkAddfee";
		AddFeeGrid.loadMulLine(iArray);
	} 
	catch(ex) 
	{
	  alert(ex);
	}
}

function initSpecGrid() 
{
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="免责信息代码";         	//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="speccode";              	//是否引用代码:null||""为不引用
    iArray[1][5]="1|2";              	    //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";              	 
    iArray[1][19]=1;

    iArray[2]=new Array();
    iArray[2][0]="免责信息";         	//列名
    iArray[2][1]="150px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="免责起始日期";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="免责终止日期";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="流水号";         	//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    SpecGrid = new MulLineEnter("fm", "SpecGrid");
    SpecGrid.mulLineCount = 1;
    SpecGrid.displayTitle = 1;
    SpecGrid.locked = 0;
    SpecGrid.canSel = 0;
    SpecGrid.hiddenPlus = 0;
    SpecGrid.hiddenSubtraction = 0;
    SpecGrid.loadMulLine(iArray);
  } 
  catch(ex) 
  {
    alert(ex);
  }
}


//人工核保信息初始化
function initManuUWGrid()
{                               
  var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="0px";         			//列宽
	  iArray[0][2]=10;          			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[1]=new Array();
	  iArray[1][0]="项目类型";    	      //列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
	
		iArray[2]=new Array();
	  iArray[2][0]="项目类型";    	      //列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  iArray[2][4]="EdorCodeForView";
		iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[2][9]="批改类型|code:EdorCodeForView&NOTNULL";
		iArray[2][18]=250;
		iArray[2][19]= 0 ;
	  
	  iArray[3]=new Array();
	  iArray[3][0]="保单号";    	      //列名
	  iArray[3][1]="80px";            	//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="险种号";    	      //列名
	  iArray[4][1]="60px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="险种序号";    	      //列名
	  iArray[5][1]="50px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[6]=new Array();
	  iArray[6][0]="险种代码";    	      //列名
	  iArray[6][1]="80px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[7]=new Array();
	  iArray[7][0]="被保人客户号";         		//列名
	  iArray[7][1]="80px";            		//列宽
	  iArray[7][2]=100;            			//列最大值
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="被保人";         		//列名
	  iArray[8][1]="60px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[9]=new Array();                                                 
	  iArray[9][0]="核保结论";         		//列名                           
	  iArray[9][1]="200px";            		//列宽                             
	  iArray[9][2]=100;            			//列最大值                           
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[10]=new Array();                                                 
	  iArray[10][0]="加费标志";         		//列名                           
	  iArray[10][1]="80px";            		//列宽                             
	  iArray[10][2]=100;            			//列最大值                           
	  iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[11]=new Array();                                                 
	  iArray[11][0]="免责标志";         		//列名                           
	  iArray[11][1]="80px";            		//列宽                             
	  iArray[11][2]=100;            			//列最大值                           
	  iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[12]=new Array();                                                 
	  iArray[12][0]="降档标志";         		//列名                           
	  iArray[12][1]="80px";            		//列宽                             
	  iArray[12][2]=100;            			//列最大值                           
	  iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[13]=new Array();                                                 
	  iArray[13][0]="核保意见";         		//列名                           
	  iArray[13][1]="120px";            		//列宽                             
	  iArray[13][2]=100;            			//列最大值                           
	  iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[14]=new Array();                                                 
	  iArray[14][0]="核保日期";         		//列名                           
	  iArray[14][1]="80px";            		//列宽                             
	  iArray[14][2]=100;            			//列最大值                           
	  iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[15]=new Array();                                                 
	  iArray[15][0]="客户不同意处理";     //列名                           
	  iArray[15][1]="80px";            		//列宽                             
	  iArray[15][2]=100;            			//列最大值                           
	  iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[16]=new Array();                                                  
	  iArray[16][0]="客户反馈结果";       //列名                             
	  iArray[16][1]="80px";            		//列宽                               
	  iArray[16][2]=100;            			//列最大值                           
	  iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许

 		ManuUWGrid = new MulLineEnter("fm", "ManuUWGrid"); 
		ManuUWGrid.locked = 1;
		ManuUWGrid.canSel = 0;
		ManuUWGrid.canChk = 0;
		ManuUWGrid.hiddenSubtraction = 1;
		ManuUWGrid.hiddenPlus = 1;
		//ManuUWGrid.selBoxEventFuncName="queryAddition";
		ManuUWGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
	 alert(ex);
	}
}
</SCRIPT>