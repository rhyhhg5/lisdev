<%
//LLRgtChangePayModeSave.jsp
//Function：批次给付方式修改
//Date：2008-4-11
//Author  ：MN
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>
<%
  GrpRegisterBL tGrpRegisterBL   = new GrpRegisterBL();
  LLRegisterSchema tLLRegisterSchema = new LLRegisterSchema();
  VData tVData = new VData();
  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String strOperate = request.getParameter("fmtransact");
  System.out.println("strOperate::"+strOperate);
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	String LoadFlag = request.getParameter("LoadFlag");
  String RgtNo = request.getParameter("RgtNo");
  String TogetherFlag = request.getParameter("TogetherFlag");
  String CaseGetMode = request.getParameter("CaseGetMode");
  String BankCode = request.getParameter("BankCode");
  String BankAccNo = request.getParameter("BankAccNo");
  String AccName = request.getParameter("AccName");
  tLLRegisterSchema.setRgtNo(RgtNo);
  tLLRegisterSchema.setTogetherFlag(TogetherFlag);
  tLLRegisterSchema.setCaseGetMode(CaseGetMode);
  tLLRegisterSchema.setBankCode(BankCode);
  tLLRegisterSchema.setBankAccNo(BankAccNo);
  tLLRegisterSchema.setAccName(AccName);
  
  try
  {
    tVData.addElement(tLLRegisterSchema);
    tVData.addElement(tG);
    if(tGrpRegisterBL.submitData(tVData,strOperate))
    {
      Content = "给付方式修改成功";
      FlagStr = "Succ";
    }
    else
    {
      FlagStr = "Fail";
    }
  }
  catch(Exception ex)
  {
    Content = "给付方式修改失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("返回的标志是"+FlagStr);
  if (FlagStr=="Fail")
  {
    tError = tGrpRegisterBL.mErrors;
    if (tError.needDealError())
    {
      Content = " 修改失败,原因是"+tError.getFirstError();
      FlagStr = "Fail";
      tVData.clear();
    }
  }
  else
  {
    Content = "修改成功！";
    FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>