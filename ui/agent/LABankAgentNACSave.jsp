<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgenttempSchema tLAAgenttempSchema   = new LAAgenttempSchema();
  LATreeTempSchema tLATreeTempSchema = new LATreeTempSchema();
  LAAgentSchema tLAAgentSchema=new LAAgentSchema();
  LATreeSchema tLATreeSchema=new LATreeSchema();
  LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
  LARearRelationSet tLARearRelationSet = new LARearRelationSet();
  LABankAgentNACUI tLABankAgentNACUI = new LABankAgentNACUI();
  String AgentCode = "";

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  System.out.println("操作符：[ " + tOperate + " ]");
  String tIsManager = request.getParameter("hideIsManager");
  System.out.println(request.getParameter("ManageCom"));
  System.out.println(tIsManager+"*********");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    LisIDEA tLisIdea = new LisIDEA();
    tLAAgenttempSchema.setPassword(tLisIdea.encryptString("1111"));//设置销售人员默认密码，方便以后查询分析系统使用
    tLAAgenttempSchema.setAgentCode(request.getParameter("AgentCode"));
    tLAAgenttempSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLAAgenttempSchema.setManageCom(request.getParameter("ManageCom"));
    tLAAgenttempSchema.setBranchType(request.getParameter("BranchType"));
    tLAAgenttempSchema.setBranchType2(request.getParameter("BranchType2"));
    tLAAgenttempSchema.setName(request.getParameter("Name"));
    tLAAgenttempSchema.setSex(request.getParameter("Sex"));
    tLAAgenttempSchema.setBirthday(request.getParameter("Birthday"));
    tLAAgenttempSchema.setIDNo(request.getParameter("IDNo"));
    tLAAgenttempSchema.setIDNoType("0");
    tLAAgenttempSchema.setNativePlace(request.getParameter("NativePlace"));
    tLAAgenttempSchema.setNationality(request.getParameter("Nationality"));
    tLAAgenttempSchema.setPolityVisage(request.getParameter("PolityVisage"));
    tLAAgenttempSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLAAgenttempSchema.setDegree(request.getParameter("Degree"));
    tLAAgenttempSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
    tLAAgenttempSchema.setSpeciality(request.getParameter("Speciality"));
    tLAAgenttempSchema.setPostTitle(request.getParameter("PostTitle"));
    tLAAgenttempSchema.setHomeAddress(request.getParameter("HomeAddress"));
    tLAAgenttempSchema.setZipCode(request.getParameter("ZipCode"));
    tLAAgenttempSchema.setPhone(request.getParameter("Phone"));
    tLAAgenttempSchema.setMobile(request.getParameter("Mobile"));
    tLAAgenttempSchema.setEMail(request.getParameter("EMail"));
    tLAAgenttempSchema.setOldCom(request.getParameter("OldCom"));
    tLAAgenttempSchema.setOldOccupation(request.getParameter("OldOccupation"));
    tLAAgenttempSchema.setHeadShip(request.getParameter("HeadShip"));
    tLAAgenttempSchema.setDevGrade(request.getParameter("DevGrade"));
    tLAAgenttempSchema.setTrainDate(request.getParameter("TrainDate"));
    tLAAgenttempSchema.setTrainPeriods(request.getParameter("TrainPeriods"));
    tLAAgenttempSchema.setEmployDate(request.getParameter("EmployDate"));
    tLAAgenttempSchema.setAgentState(request.getParameter("AgentState"));
    tLAAgenttempSchema.setRemark(request.getParameter("Remark"));
    tLAAgenttempSchema.setChannelName(request.getParameter("ChannelName"));
    tLAAgenttempSchema.setQuafNo(request.getParameter("QuafNo"));
    tLAAgenttempSchema.setQuafStartDate(request.getParameter("Quafstartdate"));
    tLAAgenttempSchema.setQuafEndDate(request.getParameter("QuafEndDate"));
    if(!tOperate.equals("UPDATE||MAIN")){
    	tLAAgenttempSchema.setSaleQuaf(request.getParameter("hiddenSaleQuaf"));
    	tLAAgentSchema.setCrs_Check_Status("00");//新增标志
    	tLAAgentSchema.setSaleQuaf(request.getParameter("salequaf"));
    }else{
    	String sql = "select crs_check_status from laagent where agentcode = '"+request.getParameter("AgentCode")+"'";
	    String crs_check_status = new ExeSQL().getOneValue(sql);
	    if("01".equals(crs_check_status)){
	    	tLAAgentSchema.setCrs_Check_Status("99"); //更新标志
	    }else{
	    	tLAAgentSchema.setCrs_Check_Status("00"); //更新标志
	    }
    	tLAAgenttempSchema.setSaleQuaf(request.getParameter("salequaf"));
    	tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));//设置销售人员默认密码，方便以后查询分析系统使用
    	tLAAgentSchema.setAgentCode(request.getParameter("AgentCode"));
    	tLAAgentSchema.setAgentGroup(request.getParameter("AgentGroup"));
    	tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
    	tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
    	tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
    	tLAAgentSchema.setName(request.getParameter("Name"));
    	tLAAgentSchema.setSex(request.getParameter("Sex"));
    	tLAAgentSchema.setBirthday(request.getParameter("Birthday"));
    	tLAAgentSchema.setIDNo(request.getParameter("IDNo"));
    	tLAAgentSchema.setIDNoType("0");
    	tLAAgentSchema.setNativePlace(request.getParameter("NativePlace"));
    	tLAAgentSchema.setNationality(request.getParameter("Nationality"));
    	tLAAgentSchema.setPolityVisage(request.getParameter("PolityVisage"));
    	tLAAgentSchema.setRgtAddress(request.getParameter("RgtAddress"));
    	tLAAgentSchema.setDegree(request.getParameter("Degree"));
        tLAAgentSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
        tLAAgentSchema.setSpeciality(request.getParameter("Speciality"));
        tLAAgentSchema.setPostTitle(request.getParameter("PostTitle"));
        tLAAgentSchema.setHomeAddress(request.getParameter("HomeAddress"));
        tLAAgentSchema.setZipCode(request.getParameter("ZipCode"));
        tLAAgentSchema.setPhone(request.getParameter("Phone"));
        tLAAgentSchema.setMobile(request.getParameter("Mobile"));
        tLAAgentSchema.setEMail(request.getParameter("EMail"));
        tLAAgentSchema.setOldCom(request.getParameter("OldCom"));
        tLAAgentSchema.setOldOccupation(request.getParameter("OldOccupation"));
        tLAAgentSchema.setHeadShip(request.getParameter("HeadShip"));
        tLAAgentSchema.setDevGrade(request.getParameter("DevGrade"));
        tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));
        tLAAgentSchema.setTrainPeriods(request.getParameter("TrainPeriods"));
        tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
        tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
        tLAAgentSchema.setRemark(request.getParameter("Remark"));
        tLAAgentSchema.setSaleQuaf(request.getParameter("salequaf"));
        tLAAgentSchema.setChannelName(request.getParameter("ChannelName"));
        tLAAgentSchema.setInsideFlag("1");
        tLAAgentSchema.setQuafNo(request.getParameter("QuafNo"));
        tLAAgentSchema.setQuafStartDate(request.getParameter("Quafstartdate"));
        tLAAgentSchema.setQuafEndDate(request.getParameter("QuafEndDate"));
        tLAAgentSchema.setAgentType(request.getParameter("AgentKind"));
        tLAAgentSchema.setGroupAgentCode(request.getParameter("GroupAgentCode"));
        //修改时记录修改前入司时间
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("OldEmployDate",request.getParameter("hideEmployDate"));
        
        tLATreeSchema.setAgentCode(request.getParameter("AgentCode"));
        tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
        tLATreeSchema.setAgentGroup(request.getParameter("AgentGroup"));
        //职级的存放
    	tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
    	tLATreeSchema.setUpAgent(request.getParameter("BranchManager"));
    	tLATreeSchema.setAgentSeries(request.getParameter("AgentSeries"));
    	tLATreeSchema.setBranchType(request.getParameter("BranchType"));
    	tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
    	tLATreeSchema.setAgentLastGrade(request.getParameter("ExpYear"));//同业年限
    	tLATreeSchema.setAgentLastSeries(request.getParameter("hasExp"));//是否有同业经验
    }
    tLAAgenttempSchema.setInsideFlag("1");
    tLAAgenttempSchema.setSource(request.getParameter("RejectRemark"));
    //modify by zhuxt 增加业务员类型和委托代理合同号
    tLAAgenttempSchema.setRetainContNo(request.getParameter("RetainContno"));
    tLAAgenttempSchema.setAgentKind(request.getParameter("AgentKind"));
    
    //取得行政信息--在bl中设置职级及系列
    tLATreeTempSchema.setAgentCode(request.getParameter("AgentCode"));
    tLATreeTempSchema.setManageCom(request.getParameter("ManageCom"));
    tLATreeTempSchema.setAgentGroup(request.getParameter("AgentGroup"));
    //职级的存放
	tLATreeTempSchema.setAgentGrade(request.getParameter("AgentGrade"));
	tLATreeTempSchema.setUpAgent(request.getParameter("BranchManager"));
	tLATreeTempSchema.setAgentSeries(request.getParameter("AgentSeries"));
    tLATreeTempSchema.setBranchType(request.getParameter("BranchType"));
    tLATreeTempSchema.setBranchType2(request.getParameter("BranchType2"));
    tLATreeTempSchema.setAgentLastGrade(request.getParameter("ExpYear"));//同业年限
    tLATreeTempSchema.setAgentLastSeries(request.getParameter("hasExp"));//是否有同业经验

    //修改时记录修改前入司时间
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("OldEmployDate",request.getParameter("hideEmployDate"));

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.add(tIsManager);
  tVData.addElement(tLAAgenttempSchema);
  tVData.addElement(tLATreeTempSchema);
  tVData.addElement(tLAWarrantorSet);
  tVData.addElement(tLARearRelationSet);
  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
  tVData.add(tTransferData);
  try
  {
    System.out.println("this will save the data!!!");
    tLABankAgentNACUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
    ex.printStackTrace();
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankAgentNACUI.mErrors;
    if (!tError.needDealError())
    {
      AgentCode = tLABankAgentNACUI.getAgentCode();
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println(Content);
System.out.println(FlagStr);
  //添加各种预处理

%>
<html>
<script language="javascript">
    parent.fraInterface.fm.all('AgentCode').value = '<%=AgentCode%>';
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
