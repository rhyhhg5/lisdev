<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);

%>
<head >
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LAZJDimissionInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAZJDimissionInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAZJDimissionSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="./AgentOp1.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLADimission1);">
    
    <td class=titleImg>
     离职信息
    </td> 
    </td>
    </tr>
    </table>
  <Div  id= "divLADimission1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 
		  专员代码 
		</td>
        <td  class= input> 
		  <input class= common name=AgentCode  onchange="return checkValid();"  type = hidden> 
		  <input class= common name=groupAgentCode verify="业务员代码 |notnull" onchange="return checkValid();" elementtype=nacessary> 
		</td>
        <td  class= title> 
		   专员姓名 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=AgentName > 
		</td>
      </tr>
      <tr  class= common> 
         <td  class= title> 
		  管理机构 
		</td>
        <td  class= input> 
		  <input class="readonly" readonly name=ManageCom > 
		</td>
        <td  class= title> 
		  职级 
		</td>
        <td  class= input> 
		  <input name=AgentGrade class='readonly'   readonly >  
		</td>
      
      </tr>
    <tr  class= common> 
      <td  class= title> 
		   销售机构代码 
		  </td>
      <td  class= input> 
		   <Input class='readonly' name=BranchAttr  readonly >  
		  </td>
      <td  class= title> 
		   销售机构名称
		  </td>
      <td  class= input> 
		   <Input class='readonly' name=BranchName  readonly >  
	   	</td>
      
    </tr>
    <tr  class= common> 
      <td  class= title> 
		   离职次数 
		  </td>
      <td  class= input> 
		   <Input class='readonly' name=DepartTimes  readonly >  
		  </td>
      <td  class= title> 
		   离职原因
		  </td>
      <td  class= input> 
		   <Input class='readonly' name=DepartRsn  readonly >  
	   	</td>
      
    </tr>
    <tr  class= common> 
      <td  class= title> 
		   离职申请日期 
		  </td>
      <td  class= input> 
		   <Input class='readonly' name=ApplyDate  readonly >  
		  </td>
      <td  class= title> 
		   离职日期
		  </td>
        <td  class= input> 
		  <Input class='coolDatePicker' name=DepartDate dateFormat='short' verify="离职日期|notnull&DATE" elementtype=nacessary> 
		  </td>
      
    </tr>          
      
     
    </table>
  </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=ReceiptFlag>
    <input type=hidden name=BranchType value =<%=BranchType%>>
    <input type=hidden name=BranchType2 value=<%=BranchType2%>>
    <input type=hidden name=DepartState value=''>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDimissionGrid);">
    		</td>
    		<td class= titleImg>
    			负责网点信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divDimissionGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentWageGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton"> 				
  	</div>
  	
  	
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divContGrid);">
    		</td>
    		<td class= titleImg>
    			未签单或未回执回销保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();" class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();" class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();" class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();" class="cssButton"> 				
  	</div>
    
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
