<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
	//程序名称：AdjustBankInput.jsp
	//程序功能：
	//创建日期：2008-03-08 16:25:40
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String BranchType=request.getParameter("BranchType");
	String BranchType2=request.getParameter("BranchType2");
	System.out.println("BranchType:"+BranchType);
	System.out.println("BranchType2:"+BranchType2);
	String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
	String tTitleAgent = "1".equals(BranchType)?"销售人员":"业务员";
	%>
	<script>
		var manageCom = <%=tG.ManageCom%>;
		var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and agentstate<#06#";
		var tsql_Branch=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and  (EndFlag <> #Y# or EndFlag is null)  and branchlevel=#43# "  ;
	</script>
	<%@page contentType="text/html;charset=GBK" %>
	<head >
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="AdjustBankInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="AdjustBankInit.jsp"%>
		<%@include file="../agent/SetBranchType.jsp"%>
		<%@include file="../common/jsp/ManageComLimit.jsp"%>
		<title>人员调动</title>
	</head>

	<body onload="initForm();initElementtype();" >
		<form action="./AdjustBankSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentInfo);">
					</td>
					<td class=titleImg>调动人员信息</td>
				</tr>
			</table>
			<Div  id= "divAgentInfo" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD  class= title>
							<%=tTitleAgent%>代码
						</TD>
						<TD  class= input>
							<Input class=code  readonly name=AgentCode ondblclick="return showCodeList('agentcode',[this,AgentName,OldAgentGroup], [0,1,2],null,msql,1,true);" onkeyup="return showCodeListKey('agentcode', [this,AgentName,OldAgentGroup], [0,1,2],null,msql,1,true);" verify="业务员代码|NotNull" elementtype=nacessary>
						</TD>
							<TD  class= title>
							<%=tTitleAgent%>姓名
						</TD>
						<TD  class= input8>
							<Input class=common name=AgentName >
						</TD>
						<TD class= title>
							<%=tTitleAgent%>职级
						</TD>
						<TD class= input>
							<Input type=hidden name=AgentGrade><Input name=AgentGradeName class=readOnly readOnly >
						</TD>
					</TR>
					<!--TR  class= common>
						<TD  class= title>
							推荐人代码
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=RecomAgentCode >
						</TD>
						<TD  class= title>
							推荐人名称
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=RecomAgentName >
						</TD>
						<TD  class= title>
							推荐人级别
						</TD>
						<TD  class= input>
							<Input type=hidden name=RecomAgentGrade><Input class=readOnly readOnly name=RecomAgentGradeName >
						</TD>
					</TR-->
					<TR  class= common>
						<TD  class= title>
							销售单位代码
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=OldBranchCode >
							<Input type=hidden name=OldAgentGroup >
						</TD>
						<TD  class= title>
							销售单位名称
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=OldBranchName >
						</TD>
						<TD  class= title>
							管理机构
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=OldManageCom >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							机构主管
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=OldBranchManager >
						</TD>
						<TD  class= title>
							主管姓名
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=OldManagerName >
						</TD>
						<!--TD  class= title >
							调动类型
						</TD>
						<TD  class= Input >
							<input class=codeno CodeData="0|2^0|断裂原有关系^1|带走原有关系"  name=IsSingle ondblclick="return showCodeListEx('IsSingle',[this,IsSingleName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('IsSingle',[this,IsSingleName],[0,1],null,null,null,1);" verify="调动类型|NotNull"><input class=codename name=IsSingleName elementtype=nacessary>
						</TD-->
					</TR>
				</table>
			</div>
			<hr>
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divObject);">
					</td>
					<td class=titleImg>
						调动目标机构信息
					</td>
				</tr>
			</table>
			<Div  id= "divObject" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD  class= title>
							机构代码
						</TD>
						<TD  class= input>
							<Input class=code  readonly name=BranchCode ondblclick="return showCodeList('branchattr',[this,BranchName],[0,1],null,tsql_Branch,1);" onkeyup="return showCodeListKey('branchattr',[this,BranchName],[0,1],null,tsql_Branch,1);" verify="销售机构|notnull" elementtype=nacessary >
							<Input type=hidden name=AgentGroup >
						</TD>
						<TD  class= title>
							机构名称
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=BranchName >
						</TD>
						<TD  class= title>
							管理机构
						</TD>
						<TD  class= input>
							<Input class=readOnly readOnly name=ManageCom >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							机构主管
						</TD>
						<TD  class= input>
							<Input class=readonly readonly name=BranchManager >
						</TD>
						<TD class= title>主管姓名</TD>
						<TD class= input>
							<Input class='readonly'readonly name=ManagerName >
						</TD>
						<TD  class= title>
							调动日期
						</TD>
						<TD  class= input>
							<Input class='coolDatePicker' name=AdjustDate dateFormat='short' verify="调动日期|notnull&DATE" elementtype=nacessary>
						</TD>
					</TR>
				</table>

			</div>
			<br>
			<table  class= common style="display:'none'">
				<tr  class= common>
					<td text-align: center colSpan=1>
						<span id="spanAgentGrid">
						</span>
					</td>
				</tr>
				<tr>
					<td alain=center>
						<INPUT VALUE="首  页" TYPE=button class=cssButton onclick="turnPage.firstPage();">
						<INPUT VALUE="上一页" TYPE=button class=cssButton onclick="turnPage.previousPage();">
						<INPUT VALUE="下一页" TYPE=button class=cssButton onclick="turnPage.nextPage();">
						<INPUT VALUE="尾  页" TYPE=button class=cssButton onclick="turnPage.lastPage();">
					</td>
				</tr>
			</table>
			<table class=common>
				<TR  class= common>
					<TD class= common>
						<input type=button class=cssButton name=saveb value='保  存' onclick="submitForm()">
						<input type=button class=cssButton value='重  置' onclick="initForm();">
					</TD>
				</TR>
			</table>
		</div>
		<Input type=hidden name=BranchType value='' >
		<input type=hidden name=BranchType2 value=''>
		<input type=hidden name=BranchLevel value=''>
		<input type=hidden name=UpBranch value=''>
		<input type=hidden name=AgentSeries value=''>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
</body>
</html>
