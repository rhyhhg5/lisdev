//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
var queryFlag = false;

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLAassessmainInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//提交之前基本校验
function beforeSubmit()
{
	var managecom=new Date(fm.all('ManageCom ').value);
	var indexcalno=new Date(fm.all('Indexcalno').value);
	var agentgrade=new Date(fm.all('AgentGrade').value);
    if (managecom==null||managecom=='')
   {
   	alert("管理机构为必填项，不能为空！");
   	return false;
   }
   if(indexcalno==null||indexcalno==''){
   	alert("考核月为必填项，不能为空!");
   }
   if(agentgrade==null||agentgrade==''){
   alert("营销员职级为必填项，不能为空！");
   }
   return true;
}
//查询之前先检查数据
function check()
{
 if(fm.all("ManageCom").value==null||fm.all("ManageCom").value==''){
	   alert("管理机构为必填项，不能为空！");
	   return false;
	}
	if(fm.all("Indexcalno").value==null||fm.all("Indexcalno").value==''){
	  alert("考核月为必填项，不能为空!");
	  return false;
	}
	if(fm.all('AgentGrade').value==null||fm.all('AgentGrade').value==''){
	 alert("营销员职级为必填项，不能为空！");
	 return false;
	}
	return true;
}
//页面查询
function easyQueryClick()
{	
	if(!check())
    return false ;
	// 初始化表格	
	// 书写SQL语句
	var strSQL = "";
     strSQL ="select indexcalno,managecom,agentgrade,state from laassessmain where managecom='"+fm.all("ManageCom").value+"'"
            +" and agentgrade='"+fm.all("AgentGrade").value+"'"
            +" and indexcalno='"+fm.all("Indexcalno").value+"'"
    		+ " order by indexcalno,managecom,agentgrade";	
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
   }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
   queryFlag = true;
}
//Click事件，当点击“修改”图片时触发该函数
function updateclick()
{
	fm.fmAction.value = "UPDATE";
	if(queryFlag ==false) 
	{
		alert("请先查询");
		return false;	
	}
	if (confirm("您确实想修改该记录吗?"))
	{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./LAassessmainSave.jsp";
	fm.submit(); //提交
	}
	else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}           

function submitForm()
{
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.action = "./LAassessmainSave.jsp";
  fm.submit(); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


