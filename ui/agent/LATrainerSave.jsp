<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchGroupSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.db.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LATrainerSchema tLATrainerSchema = new LATrainerSchema();
  LATrainerUI tLLATrainerUI = new LATrainerUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();

  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  tLATrainerSchema.setTrainerNo(request.getParameter("TrainerNo"));
  tLATrainerSchema.setPManageCom(request.getParameter("PManageCom"));
  tLATrainerSchema.setCManageCom(request.getParameter("CManageCom"));
  tLATrainerSchema.setAgentGroup(request.getParameter("BranchAttr"));
  tLATrainerSchema.setTrainerName(request.getParameter("TrainerName"));
  tLATrainerSchema.setSex(request.getParameter("Sex"));
  tLATrainerSchema.setTrainerGrade(request.getParameter("TrainerGrade"));
  tLATrainerSchema.setBirthday(request.getParameter("Birthday"));
  tLATrainerSchema.setPolityVisage(request.getParameter("PolityVisage"));
  tLATrainerSchema.setWorkDate(request.getParameter("WorkDate"));
  tLATrainerSchema.setInsureDate(request.getParameter("InsureDate"));
  tLATrainerSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
  tLATrainerSchema.setSpeciality(request.getParameter("Speciality"));
  tLATrainerSchema.setEducation(request.getParameter("Education"));
  tLATrainerSchema.setDegree(request.getParameter("Degree"));
  tLATrainerSchema.setMobile(request.getParameter("Mobile"));
  tLATrainerSchema.setAssess(request.getParameter("Assess"));
  tLATrainerSchema.setMulitId(request.getParameter("MulitId"));
  tLATrainerSchema.setTrainerState("0");
  tLATrainerSchema.setOperator(request.getParameter("Operator"));
  tLATrainerSchema.setBranchType(request.getParameter("BranchType"));
  tLATrainerSchema.setBranchType2(request.getParameter("BranchType2"));
  tLATrainerSchema.setWorkYear(request.getParameter("WorkYear"));
  tLATrainerSchema.setTakeOfficeDate(request.getParameter("TakeOfficeDate"));
  tLATrainerSchema.setBusinessManagerName(request.getParameter("BusinessManagerName"));
  tLATrainerSchema.setIDNoType(request.getParameter("IDNoType"));
  tLATrainerSchema.setIDNO(request.getParameter("IDNo"));
  tLATrainerSchema.setTrainerCode(request.getParameter("TrainerCode"));
  tLATrainerSchema.setTrainerFlag("0");
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLATrainerSchema);
	tVData.add(tG);
  

   if(request.getParameter("hideOperate").equals("DELETE||MAIN")){
	//删除原影印文件
   String oldLaborContract = request.getParameter("LaborContract");
   if(oldLaborContract!=null&&!oldLaborContract.equals("")){
	System.out.println("文件名："+oldLaborContract);
   String path = application.getRealPath("").replace('\\', '/') + '/';
   String filePath = "temp/train";
	File tFile = new File(path + filePath + "/"+ oldLaborContract);
	if (tFile.exists()) {
		if (tFile.isFile()) {
			if (!tFile.delete()) {
				FlagStr = "Fail";
				Content = "原影像文件删除错误";
				System.out.println(Content);
			}
		}
	}
  }
}
  if(Content.equals("原影像文件删除错误")){
	  %>
	  <script language="javascript" >
		alert("原影像文件删除错误!");
	</script>
	<%  
	return;
  }
  
  try
  {
	  
	  tLLATrainerUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLLATrainerUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {

    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    	System.out.println(Content);
    }
  }
  System.out.println(Content);
  //添加各种预处理
%>
<html>

<script language="javascript" type="">
	var FlagStr='<%=FlagStr%>';
	var Content1='<%=Content%>';
parent.fraInterface.afterSubmit(FlagStr,Content1);
</script>
</html>

