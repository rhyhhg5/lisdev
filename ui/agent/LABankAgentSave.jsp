<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
  LARearRelationSet tLARearRelationSet = new LARearRelationSet();
  //ALABankAgentUI tLABankAgent   = new ALABankAgentUI();
  LAAgentUI tLABankAgent = new LAAgentUI();
  String AgentCode = "";

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  String tIsManager = request.getParameter("hideIsManager");
  System.out.println(request.getParameter("ManageCom"));
  System.out.println(tIsManager+"*********");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    LisIDEA tLisIdea = new LisIDEA();
    tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));//设置销售人员默认密码，方便以后查询分析系统使用
    tLAAgentSchema.setAgentCode(request.getParameter("AgentCode"));
    tLAAgentSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
    tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
    tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
    tLAAgentSchema.setName(request.getParameter("Name"));
    tLAAgentSchema.setSex(request.getParameter("Sex"));
    tLAAgentSchema.setBirthday(request.getParameter("Birthday"));
    tLAAgentSchema.setIDNo(request.getParameter("IDNo"));
    tLAAgentSchema.setIDNoType("0");
    tLAAgentSchema.setNativePlace(request.getParameter("NativePlace"));
    tLAAgentSchema.setNationality(request.getParameter("Nationality"));
    tLAAgentSchema.setPolityVisage(request.getParameter("PolityVisage"));
    tLAAgentSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLAAgentSchema.setDegree(request.getParameter("Degree"));
    tLAAgentSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
    tLAAgentSchema.setSpeciality(request.getParameter("Speciality"));
    tLAAgentSchema.setPostTitle(request.getParameter("PostTitle"));
    tLAAgentSchema.setHomeAddress(request.getParameter("HomeAddress"));
    tLAAgentSchema.setZipCode(request.getParameter("ZipCode"));
    tLAAgentSchema.setPhone(request.getParameter("Phone"));
    tLAAgentSchema.setBP(request.getParameter("BP"));
    tLAAgentSchema.setMobile(request.getParameter("Mobile"));
    tLAAgentSchema.setEMail(request.getParameter("EMail"));
    tLAAgentSchema.setOldCom(request.getParameter("OldCom"));
    tLAAgentSchema.setOldOccupation(request.getParameter("OldOccupation"));
    tLAAgentSchema.setHeadShip(request.getParameter("HeadShip"));
    //tLAAgentSchema.setQuafNo(request.getParameter("QuafNo"));
   // tLAAgentSchema.setQuafEndDate(request.getParameter("QuafEndDate"));
    tLAAgentSchema.setDevGrade(request.getParameter("DevGrade"));
    tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));
    tLAAgentSchema.setTrainPeriods(request.getParameter("TrainPeriods"));
    tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
    tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
   // tLAAgentSchema.setBankCode(request.getParameter("BankCode"));
    //tLAAgentSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLAAgentSchema.setRemark(request.getParameter("Remark"));
    //tLAAgentSchema.setAgentKind(request.getParameter("AgentKind"));
    tLAAgentSchema.setOperator(request.getParameter("Operator"));
    tLAAgentSchema.setChannelName(request.getParameter("ChannelName"));
    tLAAgentSchema.setInsideFlag(request.getParameter("InsideFlag"));
    tLAAgentSchema.setFullTimeFlag(request.getParameter("GrpFlag"));
    //取得行政信息--在bl中设置职级及系列
    tLATreeSchema.setAgentCode(request.getParameter("AgentCode"));
    tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
    tLATreeSchema.setAgentGroup(request.getParameter("hideAgentGroup"));
    //tLATreeSchema.setUpAgent(request.getParameter("UpAgent"));
    tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
    tLATreeSchema.setAgentLine("A");   
    tLATreeSchema.setInDueFormFlag(request.getParameter("InDueFormFlag"));
    tLATreeSchema.setInDueFormDate(request.getParameter("InDueFormDate"));    
    //tLATreeSchema.setAgentSeries(request.getParameter("AgentKind"));
    //tLATreeSchema.setAgentKind(request.getParameter("AgentKind"));
    tLATreeSchema.setBranchType(request.getParameter("BranchType"));
    tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
    tLATreeSchema.setTutorShip(request.getParameter("TutorShip"));
//  //取得担保人信息
//  int lineCount = 0;
//  String arrCount[] = request.getParameterValues("WarrantorGridNo");
//  String tCautionerName[] = request.getParameterValues("WarrantorGrid1");
//  String tCautionerSex[] = request.getParameterValues("WarrantorGrid2");
//  String tCautionerID[] = request.getParameterValues("WarrantorGrid3");
//  String tCautionerCom[] = request.getParameterValues("WarrantorGrid4");
//  String tHomeAddress[] = request.getParameterValues("WarrantorGrid5");
//  String tCautionerMobile[] = request.getParameterValues("WarrantorGrid6");
//  String tZipCode[] = request.getParameterValues("WarrantorGrid7");
//  String tPhone[] = request.getParameterValues("WarrantorGrid8");
//  String tRelation[] = request.getParameterValues("WarrantorGrid9");
//  lineCount = arrCount.length; //行数
//  LAWarrantorSchema tLAWarrantorSchema;
//  for(int i=0;i<lineCount;i++)
//  {
//    tLAWarrantorSchema = new LAWarrantorSchema();
//    tLAWarrantorSchema.setAgentCode(request.getParameter("AgentCode"));
//    //tLAWarrantorSchema.setSerialNo(i+1);
//    tLAWarrantorSchema.setCautionerName(tCautionerName[i]);
//    tLAWarrantorSchema.setCautionerSex(tCautionerSex[i]);
//    tLAWarrantorSchema.setCautionerID(tCautionerID[i]);
//    tLAWarrantorSchema.setCautionerCom(tCautionerCom[i]);
//    tLAWarrantorSchema.setHomeAddress(tHomeAddress[i]);
//    tLAWarrantorSchema.setMobile(tCautionerMobile[i]);
//    tLAWarrantorSchema.setZipCode(tZipCode[i]);
//    tLAWarrantorSchema.setPhone(tPhone[i]);
//    tLAWarrantorSchema.setRelation(tRelation[i]);
//    tLAWarrantorSet.add(tLAWarrantorSchema);
//    System.out.println("for:"+tCautionerName[i]);
//  }
//  System.out.println("end 担保人信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tIsManager);
  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
  tVData.addElement(tLAWarrantorSet);
  tVData.addElement(tLARearRelationSet);
  try
  {
    System.out.println("this will save the data!!!");
    tLABankAgent.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankAgent.mErrors;
    if (!tError.needDealError())
    {
    AgentCode = tLABankAgent.getAgentCode();
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
        parent.fraInterface.fm.all('AgentCode').value = '<%=AgentCode%>';
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
