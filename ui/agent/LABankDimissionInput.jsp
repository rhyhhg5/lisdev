<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-08-11
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LADimissionInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LADimissionInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LADimissionSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLADimission1);">

    <td class=titleImg>
      客户经理离职信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divLADimission1" style= "display: ''">
    <table  class= common>
      <tr  class= common>
        <td  class= title>
		  客户经理代码
		</td>
        <td  class= input>
		  <input class= common name=AgentCode onchange="return checkValid();" elementtype=nacessary>
		</td>
        <td  class= title>
		  客户经理姓名
		</td>
        <td  class= input>
		  <input class="readonly" readonly name=AgentName >
		</td>
      </tr>
      <tr  class= common>
         <td  class= title>
		  离职次数
		</td>
        <td  class= input>
		  <input class="readonly" readonly name=DepartTimes >
		</td>
        <td  class= title>
		  离职原因
		</td>
        <td  class= input>
		  <input name=DepartRsn class='code' ondblclick="return showCodeList('DepartRsn',[this]);" onkeyup="return showCodeListKey('DepartRsn',[this]);" elementtype=nacessary>
		</td>

      </tr>
      <tr  class= common>
        <td  class= title>
		  离职日期
		</td>
        <td  class= input>
		  <input class=readonly readonly name=DepartDate>
		</td>
        <td  class= title>
		  劳动合同是否终止
		</td>
        <td  class= input>
		  <input name=ContractFlag class='code' verify="劳动合同是否终止|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
		</td>

      </tr>
      <tr  class= common>
        <td  class= title>
		  工作证回收标志
		</td>
        <td  class= input>
		  <input name=WorkFlag class='code' verify="工作证回收标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
		</td>
	<td  class= title>
		  结清标志
		</td>
        <td  class= input>
		  <input name=CheckFlag class='code' verify="结清标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);">
		</td>
      </tr>
      <tr  class= common>
        <!--td  class= title>
		  保险费暂收据回收标志
		</td>
        <td  class= input>
		  <input name=ReceiptFlag class='code' verify="保险费暂收据回收标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
		</td-->
        <!--td  class= title> 
		  丢失标志
		</td>
        <td  class= input>
		  <input name=LostFlag class='code' MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);" >
		</td-->
		 <!--td  class= title>
		   业务人员转任内勤人员标志
		</td>
        <td  class= input>
		</td-->
		  <td  class= title>
		  停止薪资发放标志
		</td>
        <td  class= input>
		  <input name=WageFlag class='code' verify="停止佣金发放标志|code:yesno" ondblclick="return showCodeList('yesno',[this]);" onkeyup="return showCodeListKey('yesno',[this]);">
		</td>
		 <td  class= title>
		  备注
		</td>
        <td  class= input>
		  <input name=Noti class= common >
		</td>
      </tr>
      <tr  class= common>
	       <td  class= title>
		   操作员代码
		</td>
        <td  class= input>
		  <input name=Operator class="readonly" readonly>
		  <input name=TurnFlag type=hidden>
		</td>
      </tr>
    </table>
  </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=ReceiptFlag>
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=PbcFlag value=''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
