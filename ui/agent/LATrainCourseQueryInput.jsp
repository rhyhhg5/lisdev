<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LABranchGroupInput.jsp
	//程序功能：
	//创建日期：
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String LoginManagecom = tG.ManageCom;
	String CurDate = PubFun.getCurrentDate();
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
	
%>
<script >
	var cSql = " 1 and length(trim(comcode)) in (#4#,#2#) ";
</script>

<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LATrainCourseQueryInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LATrainCourseQueryInit.jsp"%>
</head>
<body onload="initForm();initElementtype();" >
<form action="./LATrainCourseQuerySave.jsp" method=post name=fm target="fraSubmit">
<table>
	<tr class=common>
		<td class=common><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divLATrain1);">
		<td class=titleImg>基础信息录入</td>
	</tr>
</table>
<Div id="divLATrain1" style="display: ''">
<table class=common>
	<tr>
	 <TD  class= title>
            省分公司
          </TD>
          <TD  class= input>
          <Input class="codeno" name=PManageCom verify="省分公司|code:comcodeallsign&notnull&len<=4"
          ondblclick="return showCodeList('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1);" 
          onkeyup="return showCodeListKey('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1);"
         ><Input class=codename name=PManageComName readOnly 
		  verify="省分公司|notnull" elementtype=nacessary>
          </TD>
    <TD  class= title>
            中心支公司
          </TD>
          <TD  class= input>
          <Input class="codeno" name=CManageCom verify="中心支公司|code:comcode&len>7"
          ondblclick="return getManagecom(CManageCom,CManageComName);" 
          onkeyup="return getManagecom(CManageCom,CManageComName);"
         ><Input class=codename name=CManageComName readOnly >
     </TD>
	</tr>
	<TR class=common>
		<TD class=title>培训开始时间</TD>
		<TD class=input><Input class='coolDatePicker' name=StartDate
			verify="培训开始时间|DATE&len=10" format='short'  onchange="return getEndDate();">
		</TD>
<!--	</TR>-->
<!--	<TR class=common>-->
		<TD class=title>培训结束时间</TD>
		<TD class=input><Input class='coolDatePicker' name=EndDate
			verify="培训结束时间|DATE&len=10" format='short'  onchange="return getEndDate();">
		</TD>
	</TR>
	<TR class=common>
	<TD class=title>培训地点</TD>
		<TD class=input><Input class=common  name=TrainPlace>
		</TD>
		<TD class=title>培训班名称</TD>
        <TD  class= input>
          <Input name=Course id=Course class="codeno" verify="性别|code:Course" 
           ondblclick="return showCodeList('course',[this,CourseName],[0,1]);" 
           onkeyup="return showCodeListKey('course',[this,CourseName],[0,1]);"
           ><Input class=codename name=CourseName readOnly >
        </TD>
	</TR>
	<tr>
			<TD class=title>营业部</TD>
			<TD class=input><Input name=BranchAttr id=BranchAttr
			class="codeno"
			ondblclick="return getBranchAttr(BranchAttr,BranchAttrName);"
			onkeyup="return getBranchAttr(BranchAttr,BranchAttrName);"><Input
			maxlength=12 class="codename" name=BranchAttrName></TD>
	</tr>
</table>
      <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">  
      <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
      <INPUT VALUE="下  载" TYPE=button onclick="downLoad();" class="cssButton">
</Div>
   <input type=hidden name=querySQL value=""> 
   <table>
    <tr>
     <td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
     </td>
     <td class= titleImg>
    	查询结果
     </td>
     </tr>
    </table>
  <div id="divSetGrid" style="display:''">     
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
     <span id="spanSetGrid" >
     </span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
 </div>
<input type=hidden name=BranchType value=<%=BranchType%>> 
<input type=hidden name=BranchType2 value=<%=BranchType2%>> 
<input type=hidden name=LoginManagecom value=<%=LoginManagecom%>> 
<span  id="spanCode" style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
