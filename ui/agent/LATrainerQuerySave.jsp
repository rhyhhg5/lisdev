<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABankWagePaySave.jsp
//程序功能： 
//创建日期：2016-3-24
//创建人  ：XX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
 boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    //薪资月，如果 为12月，则显示 年终奖，否则不显示 
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "组训报表 _"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    String querySql = request.getParameter("querySQL");
    System.out.println(querySql);
    querySql = querySql.replaceAll("%25","%");
    		 //设置表头
		    String[][] tTitle = {{"省分公司代码","省分公司名称","中心支公司代码","中心支公司名称","营业部代码","营业部名称","组训姓名","性别","组训职级","出生日期","政治面貌","参加工作时间","入司时间","毕业院校","专业","最高学历","最高学位","手机号码","年度考核","兼职身份","在职状态","入司前保险工作年限","营业部经理姓名","任本职级日期","证件类型 ","证件类型名称","证件号码","组训人员工号","组训状态"}};
		    //表头的显示属性
		    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29};
		    
		    //数据的显示属性
		    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29};
		    //生成文件
		    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
		    createexcellist.createExcelFile();
		    String[] sheetName ={"组训报表"};
		    createexcellist.addSheet(sheetName);
		    int row = createexcellist.setData(tTitle,displayTitle);
		    if(row ==-1) errorFlag = true;
		        createexcellist.setRowColOffset(row,0);//设置偏移
		        System.out.println(querySql);
		    if(createexcellist.setData(querySql,displayData)==-1)
		        errorFlag = true;
		    if(!errorFlag)
		        //写文件到磁盘
		        try{
		            createexcellist.write(tOutXmlPath);
		        }catch(Exception e)
		        {
		            errorFlag = true;
		            System.out.println(e);
		        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>