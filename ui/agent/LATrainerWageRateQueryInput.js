//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug = "0";
var cName = "";
var strSql = "";
try {
	var turnPage = new turnPageClass(); // 使用翻页功能，必须建立为全局变量
} catch (ex) {
}
// 提数操作
function getManagecom(Managecom, ManagecomName) {
	var pManagecom = fm.all('PManageCom').value;
	if (pManagecom == null || pManagecom == '') {
		alert("请先录入省公司代码");
		return false;
	}
	var strsql = "1 and length(trim(comcode))=8 and comcode like #"
			+ pManagecom + "%#";
	showCodeList('comcode', [ Managecom, ManagecomName ], [ 0, 1 ], null,
			strsql, 1, 1);
}
function getBranchAttr(BranchAttr, BranchAttrName) {
	var cManagecom = fm.all('CManageCom').value;

	if (cManagecom == null || cManagecom == '') {
		alert("请先录入中心支公司代码");
		return false;
	}
	var strsql1 = "1 and EndFlag=#N# and length(trim(branchattr))=10 and branchtype = #"
			+ fm.all('BranchType').value
			+ "# and branchtype2 = #"
			+ fm.all('BranchType2').value
			+ "# and managecom like #"
			+ cManagecom + "%#";
	showCodeList('BranchAttr', [ BranchAttr, BranchAttrName ], [ 0, 1 ], null,
			strsql1, 1, 1);

}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {

	showInfo.close();

	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		fm.Calculate.disabled = false;
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		// parent.fraInterface.initForm();
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		fm.Calculate.disabled = false;
	}
}

// 重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm() {
	try {
		initForm();
	} catch (re) {
		alert("在LLReport.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

// 执行查询状态
function AgentWageStateQuery() {

	if (!verifyInput()) {
		return false;
	}

	// 校验是否该月佣金已算过
	var strSQ = "select state from latrainerWagehistory  " + "where 1=1 "
			+ getWherePart('WageNo', 'WageNo')
			+ getWherePart('ManageCom', 'PManageCom', 'like')
			+ getWherePart('ManageCom', 'CManageCom')
			+ getWherePart('BranchType', 'BranchType')
			+ getWherePart('BranchType2', 'BranchType2')
			+ " order by state desc ";
	// 判断是否查询成功
	var strQueryResult = easyQueryVer3(strSQ, 1, 1, 1);
	if (!strQueryResult) {
		alert("该月佣金提数计算还未进行!");
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	var tState = arr[0][0];

	if (tState == "00") {
		alert("佣金还没有计算！");
		return false;
	} else if (tState == "11") {
		alert("佣金计算正在进行中");
		return false;
	} 
    return true;
}

// 执行查询
function LATrainerWageRateQueryQuery() {
	if (!verifyInput())
		return false;
	if (!AgentWageStateQuery())
		return false;
	divAgentQuery.style.display = '';
	initAgentQueryGrid();
	showRecord();
}

// 显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord() {
	// 拼SQL语句，从页面采集信息
	var tbranchattr = " and 1=1";
	if (fm.all('BranchAttr').value != null && fm.all('BranchAttr').value != "") {
		tbranchattr = "and a.agentgroup in(select agentgroup from labranchgroup  where branchattr ='"
				+ fm.all('BranchAttr').value
				+ "' and branchtype='1' and branchtype2='01')";
	}
	var strSql = "select a.trainerCode,trainername,(select distinct(codename) from ldcode where code =a.trainerGrade and codetype='trainergrade'),a.cmanagecom,(select  Name from ldcom where char(length(trim(comcode))) = '8' and Sign='1' and comcode = a.cmanagecom  ), "
			+ " b.branchattr,(select distinct(name) from labranchgroup where branchtype='1' and branchtype2='01' and agentgroup=b.agentgroup), "
			+ " (select TransMoney from LATrainerIndex where WageCode='GX0001' and pmanagecom=a.pmanagecom ), case when (select CommisionRate " 
			+ "  from LATrainerIndex where WageCode='GX0002' and pmanagecom=a.cmanagecom and trainerCode=a.trainerCode and agentgroup=a.agentgroup and wageno=b.wageno) is null then 0 else (select CommisionRate " 
			+ "  from LATrainerIndex where WageCode='GX0002' and pmanagecom=a.cmanagecom and trainerCode=a.trainerCode and agentgroup=a.agentgroup and wageno=b.wageno) end," 
			+ " b.f01,b.f04,b.f05,b.f06,b.f07,b.f10 "
			+ " from latrainer a,LATrainerWageIndex b  where a.agentgroup=b.agentgroup and a.trainercode is not null and a.trainercode <>'' "
			+ tbranchattr
			+ getWherePart('b.WageNo', 'WageNo')
			+ getWherePart('a.pManageCom', 'PManageCom','like')
			+ getWherePart('a.cManageCom', 'CManageCom')
			+ getWherePart('a.BranchType', 'BranchType')
			+ getWherePart('a.BranchType2', 'BranchType2');

	// 查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSql, 1, 1, 1);

	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		// 清空MULTILINE，使用方法见MULTILINE使用说明
		AgentQueryGrid.clearData('AgentQueryGrid');
		alert("查询失败！");
		return false;
	}

	// 清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	// 设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = AgentQueryGrid;

	// 保存SQL语句
	turnPage.strQuerySql = strSql;

	// 设置查询起始位置
	turnPage.pageIndex = 0;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
			turnPage.pageIndex, turnPage.pageLineNum);

	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

	// 控制是否显示翻页按钮
	if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
		try {
			window.divPage.style.display = "";
		} catch (ex) {
		}
	} else {
		try {
			window.divPage.style.display = "none";
		} catch (ex) {
		}
	}
}

function AgentWageDownLoad() {
	if (!verifyInput())
		return false;

	if (AgentQueryGrid.mulLineCount == 0) {
		alert("列表中没有数据可下载");
		return false;
	} else {
		fm.querySql.value = turnPage.strQuerySql;
		fm.submit();
	}
}

function initLAManageQualityScoreGridValue() {
	LAManageQualityScoreGrid.setRowColData(0, 1, "10万以下");
	LAManageQualityScoreGrid.setRowColData(0, 2, "0.50%");
	LAManageQualityScoreGrid.setRowColData(0, 3, "70%以下");
	LAManageQualityScoreGrid.setRowColData(0, 4, "0.5");
	LAManageQualityScoreGrid.setRowColData(0, 5, "20%以下");
	LAManageQualityScoreGrid.setRowColData(0, 6, "0.5");

	LAManageQualityScoreGrid.setRowColData(1, 1, "10万（含）-20万");
	LAManageQualityScoreGrid.setRowColData(1, 2, "0.7%");
	LAManageQualityScoreGrid.setRowColData(1, 3, "70%（含）—88%");
	LAManageQualityScoreGrid.setRowColData(1, 4, "0.7");
	LAManageQualityScoreGrid.setRowColData(1, 5, "20%（含）—30%");
	LAManageQualityScoreGrid.setRowColData(1, 6, "0.7");

	LAManageQualityScoreGrid.setRowColData(2, 1, "20万（含）-30万");
	LAManageQualityScoreGrid.setRowColData(2, 2, "1%");
	LAManageQualityScoreGrid.setRowColData(2, 3, "88%（含）—90%");
	LAManageQualityScoreGrid.setRowColData(2, 4, "1");
	LAManageQualityScoreGrid.setRowColData(2, 5, "30%（含）—40%");
	LAManageQualityScoreGrid.setRowColData(2, 6, "1");

	LAManageQualityScoreGrid.setRowColData(3, 1, "30万（含）以上");
	LAManageQualityScoreGrid.setRowColData(3, 2, "1.3%");
	LAManageQualityScoreGrid.setRowColData(3, 3, "90%（含）以上");
	LAManageQualityScoreGrid.setRowColData(3, 4, "1.3");
	LAManageQualityScoreGrid.setRowColData(3, 5, "40%（含）以上");
	LAManageQualityScoreGrid.setRowColData(3, 6, "1.3");

}