<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//	朱向峰	2004-11-15 9:47	
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
 // System.out.println("BranchType2:"+BranchType2);
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var tsql=" 1 and  branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#01#";
   var msql=" 1 and GradeProperty2 <> #0# and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#01#";
   var msql2=" 1 and GradeProperty2 = #0# and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#01#";
   var tsql_Branch=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and  (EndFlag <> #Y# or EndFlag is null) "  ;
   //var tsql="1 and code <> #N#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LABankAgentInput1.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LABankAgentInit1.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
	<form action="./LABankAgentSave1.jsp" method=post name=fm target="fraSubmit">
    <span id="operateButton">
	<table class="common" align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="允许入司"  TYPE=button onclick="return addClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="信息修改"  TYPE=button onclick="return updateClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="拒绝入司"  TYPE=button onclick="return rejectClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return queryClick();">
			</td>
		</tr>
	</table>
</span>
    <%@include file="../common/jsp/InputButton.jsp"%>
	<table>
		<tr class=common>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
			<td class=titleImg>
				银保人员信息
			</td>
			</td>
		</tr>
	</table>
	<Div id= "divLAAgent1" style= "display: ''">
    <table  class= common>
      <TR  class= common>
        <TD class= title>
          业务员编码
        </TD>
        <TD  class= input>
          <Input class= 'readonly' readonly name=AgentCode type=hidden>
          <Input class= 'readonly' readonly name=GroupAgentCode >
        </TD>
        <TD  class= title>
          业务员姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common verify="业务员姓名|NotNull&len<=20" elementtype=nacessary>
        </TD>
        <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" verify="性别|notnull&code:Sex" 
           ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" 
           onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"
           ><Input class=codename name=SexName readOnly elementtype=nacessary>
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          出生日期
        </TD>
        <TD  class= input>
          <Input name=Birthday class='coolDatePicker' dateFormat='short'  verify="出生日期|NotNull&Date" elementtype=nacessary>
        </TD>
        <TD  class= title>
          身份证号码
        </TD>
        <TD  class= input>
          <Input name=IDNo class= common verify="身份证号码|notnull&len<=20" onchange="return changeIDNo();"  elementtype=nacessary>
        </TD>
         <TD  class= title>
          民族
        </TD>
        <TD  class= input>
          <Input name=Nationality class="codeno" id="Nationality" 
           ondblclick="return showCodeList('Nationality',[this,NationalityName],[0,1]);" 
           onkeyup="return showCodeListKey('Nationality',[this,NationalityName],[0,1]);" 
           ><Input class=codename name=NationalityName readOnly >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          户口所在地
        </TD>
        <TD  class= input>
          <Input name=RgtAddress class="codeno" 
           ondblclick="return showCodeList('NativePlaceBak',[this,RgtAddressName],[0,1]);" 
           onkeyup="return showCodeListKey('NativePlaceBak',[this,RgtAddressName],[0,1]);"
           ><Input class=codename name=RgtAddressName readOnly >
        </TD>
        <TD  class= title>
          籍贯
        </TD>
        <TD  class= input>
          <Input name=NativePlace class="codeno" verify="籍贯|code:NativePlaceBak" id="NativePlaceBak" 
           ondblclick="return showCodeList('NativePlaceBak',[this,NativePlaceName],[0,1]);" 
           onkeyup="return showCodeListKey('NativePlaceBak',[this,NativePlaceName],[0,1]);"
           ><Input class=codename name=NativePlaceName readOnly >
        </TD>
        <TD  class= title>
          政治面貌
        </TD>
       <TD  class= input>
          <Input name=PolityVisage class="codeno" verify="政治面貌|code:polityvisage" id="polityvisage" 
           ondblclick="return showCodeList('polityvisage',[this,PolityVisageName],[0,1]);" 
           onkeyup="return showCodeListKey('polityvisage',[this,PolityVisageName],[0,1]);" 
           ><Input class=codename name=PolityVisageName readOnly >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          学历
        </TD>
        <TD  class= input>
          <Input name=Degree class="codeno" verify="学历|notnull&code:Degree" id="Degree" 
           ondblclick="return showCodeList('Degree',[this,DegreeName],[0,1]);" 
           onkeyup="return showCodeListKey('Degree',[this,DegreeName],[0,1]);"
           ><Input class=codename name=DegreeName verify="学历|notnull" readOnly elementtype=nacessary>
        </TD>
       <TD  class= title>
          毕业院校
        </TD>
        <TD  class= input>
          <Input name=GraduateSchool verify="毕业院校|len<20&notnull"class= common  elementtype=nacessary>
        </TD>
        <TD  class= title>
          专业
        </TD>
        <TD  class= input>
          <Input name=Speciality class= common verify="专业|notnull" elementtype=nacessary>
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          职称
        </TD>
        <TD  class= input>
          <Input name=PostTitle class='codeno' verify="职称|code:posttitle" 
           ondblclick="return showCodeList('posttitle',[this,PostTitleName],[0,1]);" 
           onkeyup="return showCodeListKey('posttitle',[this,PostTitleName],[0,1]);" 
           ><Input class=codename name=PostTitleName readOnly >
        </TD>
        <TD  class= title>
          家庭地址
        </TD>
        <TD  class= input>
          <Input name=HomeAddress class= common verify="家庭地址|notnull" elementtype=nacessary>
        </TD>
        <TD  class= title>
          邮政编码
        </TD>
        <TD  class= input>
          <Input name=ZipCode class= common >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          联系电话
        </TD>
        <TD  class= input>
          <Input name=Phone class= common verify="联系电话|len<=18&notnull" elementtype=nacessary>
        </TD>
       <TD  class= title>
          E_mail
        </TD>
        <TD  class= input>
          <Input name=EMail class= common >
        </TD>
        <TD  class= title>
          手机
        </TD>
        <TD  class= input>
          <Input name=Mobile class= common verify="手机|notnull&num" elementtype=nacessary>
        </TD>
      </TR>
      <TR  class= common>
				<TD  class= title>
          原工作职务
        </TD>
        <TD  class= input>
          <Input name=HeadShip class= common  >
        </TD>
        <TD  class= title>
          原工作单位
        </TD>
        <TD  class= input>
          <Input name=OldCom class= common verify="原工作单位|notnull" elementtype=nacessary>
        </TD>
        <TD  class= title>
          原职业
        </TD>
        <TD  class= input>
          <Input name=OldOccupation class='codeno' verify="原职业|code:occupationcode" 
           ondblclick="return showCodeList('occupationcode',[this,OldOccupationName],[0,1]);" 
           onkeyup="return showCodeListKey('occupationcode',[this,OldOccupationName],[0,1]);"
           ><Input class=codename name=OldOccupationName readOnly >
        </TD>
      </TR>
      <TR  class= common>
         <TD  class= title>
          入司时间
        </TD>
        <TD  class= input>
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' verify="入司时间|notnull&Date" elementtype=nacessary>
        </TD>
        <TD  class= title>
          备注
        </TD>
        <TD  class=input>
          <Input name=Remark class= common >
        </TD>
        <TD  class= title>
          状态
        </TD>
        <TD  class=input>
          <Input name=AgentStateName class='readonly' readonly >
        </TD>
      </TR>
      <TR  class= common>
      	<TD  class= title>
          是否有同业经验
        </TD>
      	<TD  class= input>
          <Input name=hasExp class='codeno' verify="是否有同业经验|notnull&code:yesno" 
           ondblclick="return showCodeList('yesno',[this,hasExpName],[0,1]);" 
           onkeyup="return showCodeListKey('yesno',[this,hasExpName],[0,1]);" 
           ><Input class=codename name=hasExpName readOnly elementtype=nacessary>
        </TD>
        <TD  class= title>
          同业年限
        </TD>
      	<TD  class= input>
          <Input name=ExpYear class= common verify="同业年限|num" >
        </TD> <td ><font color="red" >(注：同业年限</font></td><td><font color="red" >必须为代表年数的“数字”)</font></td> 
        
        </tr>
        <tr>
        <td   class= title>代理人资格证</td>
        <TD  class= input>
          <Input name=salequaf class='codeno' verify="代理人资格证|notnull&code:yesno" 
           ondblclick="return showCodeList('yesno',[this,hasQuafName],[0,1]);" 
           onkeyup="return showCodeListKey('yesno',[this,hasQuafName],[0,1]);" 
           readonly ><Input class=codename name=hasQuafName readOnly elementtype=nacessary>
           
        </TD>
        <TD  class= title>
          资格证书号
        </TD>
      	<TD  class= input>
          <Input name=QuafNo class= common >
        </TD>
         <TD  class= title>
          取证日期
        </TD>        
      	<TD  class= input>
          <Input name=Quafstartdate class='coolDatePicker' verify="取证日期|Date" dateFormat='short' >
        </TD> 
         
      </TR> 
      <tr>
       <TD  class= title>
          到期日期
        </TD>        
      	<TD  class= input>
          <Input name=QuafEndDate class='coolDatePicker' verify="到期日期|Date" dateFormat='short' >
        </TD> 
        <TD  class= title>
          离职时间
        </TD>        
      	<TD  class= input>
          <Input name=OutWorkDate class=common  >
        </TD> 
        <TD  class= title>
          审核失败批注
        </TD>        
      	<TD  class= input>
          <Input name=RejectReason class=common  >
        </TD> 
      </tr>
      <TR>
      <TD class=title>销售人员类型</TD>
		<TD class=input><Input name=AgentType class='codeno'
			verify="销售人员类型|notnull"
			ondblclick="return showCodeList('agenttypecode',[this,AgentTypeName],[0,1]);"
			onkeyup="return showCodeListKey('agenttypecode',[this,AgentTypeName],[0,1]);"><Input
			class=codename name=AgentTypeName elementtype=nacessary readOnly></TD>
		<TD class=title>委托代理合同编号</TD>
		<TD class=input><Input name=RetainContno class=common></TD>
      </TR>
      <TR  class= common>
		<TD  class=input colSpan= 3>
			<font color='red'>提示：1.输入项“联系电话”会打印到客户的相关单证上<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.按钮“信息修改”只能对已入司的代理人信息进行修改</font>
		</TD>
	  </TR>
      	           
    </table>
        <!--离司日期-->
        
    </Div>
    <!--行政信息-->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                行政信息
            </td>
            </td>
    	</tr>
     </table>
		<Div id= "divLAAgent3" style= "display: ''">
			<table class=common>
				<tr class=common>
					<TD class= title>职级</TD>
					<TD class= input>
						<Input name=AgentGrade class="codeno" 
						ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,tsql,1);"  
						onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,tsql,1);" 
						><Input class=codename name=AgentGradeName  elementtype=nacessary></TD>	
					<!--TD  class= title>业务职级</TD>
					<TD  class= input>
				    <Input name=AgentGrade1 class="codeno" 
				    ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName1],[0,1],null,msql2,1);"  
				    onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName1],[0,1],null,msql2,1);" ><Input class=codename name=AgentGradeName1 ></TD-->	
				</tr>
				<tr class=common>
					<TD class= title>所属团队</TD>
					<TD class= input>
					<Input name=BranchAttr class="codeno" verify="所属团队|notnull" 
				    ondblclick="return showCodeList('branchattr',[this,BranchAttrName],[0,1],null,tsql_Branch,1);"   
				    onkeyup="return showCodeListKey('branchattr',[this,BranchAttrName],[0,1],null,tsql_Branch,1);"   
				   ><Input maxlength=12 class="codename" name=BranchAttrName verify="所属团队|notnull" elementtype=nacessary></TD>
					<TD class= title>团队主管</TD>
					<TD class= input>
					<Input name=UpAgent class= readonly  readonly></TD>
				</tr>
			</table>
		</Div>
    <Input type=hidden name=AgentGrade1 >
    <Input type=hidden name=AgentSeries >
    <Input type=hidden name=AgentSeries1 >
    <Input type=hidden name=AgentInsideFlag >
		<Input type=hidden name=InsideFlag >
		<Input type=hidden name=Operator >
		<Input type=hidden name=hideIdNo value='' >
		<Input type=hidden name=ManageCom >
		<Input type=hidden name=hideBranchattr value=''>
		<input type=hidden name=hideOperate value=''>
		<input type=hidden name=AgentGroup value=''>
		<input type=hidden name=BranchLevel value=''>
		<input type=hidden name=hiddenSaleQuaf value=''>
		<input type=hidden name=hiddenQuafNo value=''>
		<input type=hidden name=hiddenQuafDate value=''>
		<!--input type=hidden name=hideManageCom value=''-->
		<input type=hidden name=BranchType value=''>		
		<input type=hidden name=BranchType2 value=''>
		<input type=hidden name=AgentState value=''>
		<input type=hidden name=BranchManager value=''>
		<input type=hidden name=hideIsManager value='false'>
		<input type=hidden name=hideAgentGrade value=''>
		<input type=hidden name=hideAgentGrade1 value=''>
		<input type=hidden name=hideSex value=''>
		<input type=hidden name=hideBirthday value=''>
		<input type=hidden name=hideEmployDate value=''>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</form>
</body>
</html>

