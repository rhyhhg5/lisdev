
// 保存操作
function submitForm() {

	fm.all('hideOperate').value = 'Save';
	// 前台录入信息校验
	if (verifyInput() == false)
		return false;

	if (!beforeSubmitForm()) {
		return false;
	}
	fm.submit();
	var i = 0;
	var showStr = "保存成功";
	alert(showStr);
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all('hideOperate').value = '';// 重置操作符为空
	initForm();// 初始化表单为空，避免重复提交
}

// 删除
function deleteClick() {
	if (fm.all('hideOperate').value != 'Query') {
		alert("请先点击查询,然后删除！");
		return false;
	}
	fm.all('hideOperate').value = 'Delete';
	if (verifyInput() == false)
		return false;
	var i = 0;
	var showStr = "删除成功";
	fm.submit();// 提交
	alert(showStr);
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all('hideOperate').value = '';// 重置操作符为空
	initForm();// 初始化表单为空
}

// 修改操作
function updateClick() {
	if (fm.all('hideOperate').value != 'Query') {
		alert("请先点击查询,然后修改！");
		return false;
	}
	if (verifyInput() == false)
		return false;
	// 设置操作符
	fm.all('hideOperate').value = 'Modify';
	var i = 0;
	fm.submit();// 提交
	var showStr = "修改成功";
	alert(showStr);
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.all('hideOperate').value = '';// 重置操作符为空
	initForm();// 初始化表单为空

}
// Click事件，当点击“查询”图片时触发该函数
function queryClick() {
	// 点击查询后不改变操作符 方便修改、删除使用判断
	fm.all('hideOperate').value = 'Query';
	initForm();
	showInfo = window.open("TestQuery.jsp?BranchType=null&BranchType2=null");
}

var turnPage = new turnPageClass(); // 使用翻页功能，必须建立为全局变量

// 查询页面，查询返回操作
function afterQuery(arrQueryResult) {
	var arr = new Array();
	if (arrQueryResult != null) {
		arr = arrQueryResult;
		fm.all('ManageCom').value = arr[0][0];// 管理机构
		fm.all('ManageComName').value = arr[0][1];
		fm.all('WageNo').value = arr[0][2];// 薪资月
		fm.all('AgentGrade').value = arr[0][3];// 职级
		fm.all('State').value = arr[0][4];// 考核状态
		fm.all('BranchType').value = arr[0][5];// 展业类型
		fm.all('BranchTypeName').value = arr[0][6];
		fm.all('BranchType2').value = arr[0][7];// 销售渠道
		fm.all('BranchType2Name').value = arr[0][8];

	}

}
// 提交数据前的校验
function beforeSubmitForm() {
	var strSQL = "";
	strSQL = "select ManageCom,(select Name from LDCom where ComCode=laassessmain.ManageCom),indexcalno,AgentGrade,state,BranchType,(select CodeName from ldcode where codetype = 'branchtype' and Code=laassessmain.BranchType),BranchType2,(select CodeName from ldcode where codetype = 'branchtype2' and Code=laassessmain.BranchType2) from laassessmain where 1=1 "
			+ getWherePart('ManageCom', 'ManageCom')// 管理机构
			+ getWherePart('BranchType', 'BranchType')// 展业类型
			+ getWherePart('BranchType2', 'BranchType2')// 销售渠道
			+ getWherePart('indexcalNo', 'WageNo')// 薪资月
			+ getWherePart('AgentGrade', 'AgentGrade');// 职级
	var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	if (strQueryResult) {
		alert("该条数据已经存在，请点击修改按钮！");
		return false;
	}
	return true;
}

/**
 * 初始化表单
 */
function initForm() {
	fm.all('ManageCom').value = '';
	fm.all('ManageComName').value = '';
	fm.all('WageNo').value = '';
	fm.all('AgentGrade').value = '';
	fm.all('State').value = '';
	fm.all('BranchType').value = '';
	fm.all('BranchTypeName').value = '';
	fm.all('BranchType2').value = '';
	fm.all('BranchType2Name').value = '';
}
