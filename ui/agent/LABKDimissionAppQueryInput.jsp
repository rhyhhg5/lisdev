<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  
  String tTitleAgent = "";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
    if("02".equals(BranchType2))
    {
      tTitleAgent = "中介专员";
    }
  }else if("3".equals(BranchType))
  {
  	tTitleAgent = "银代人员";
  }
  else if("4".equals(BranchType))
  {
  	tTitleAgent = "电销人员";
  }
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./LABankAgentQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LABankAgentQueryInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
<title><%=tTitleAgent%>查询 </title>
</head>
<body onload="initForm();initElementtype();">
  <form action="./LAAgentQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                <%=tTitleAgent%>查询条件
            </td>
            </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common>
        <TD class= title>
         业务员编码
        </TD>
        <TD  class= input>
          <Input class=common  name=AgentCode >
        </TD>
        <TD class= title>
          销售团队
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup >
        </TD>
        <TD class= title>
          管理机构
        </TD>
        <TD class= input>
          <Input name=ManageCom class='codeno' id="ManageCom" 
           ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"  
           onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
          ><Input name=ManageComName class="codename">
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
           业务员姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD>
        <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" MAXLENGTH=1 
           ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" 
           onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" 
          ><Input name=SexName class="codename">
        </TD>
        <TD  class= title>
          出生日期
        </TD>
        <TD  class= input>
          <Input name=Birthday class="coolDatePicker" dateFormat="short" >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          身份证号码
        </TD>
        <TD  class= input>
          <Input name=IDNo class= common >
        </TD>
         
        
        <TD  class= title>
          入司时间
        </TD>
        <TD  class= input>
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' >
        </TD>
         <TD class=title>在职状态</TD>
       		<td class=input>
       		<Input class="codeno" name = AgentState  CodeData = "0|^01|在职|^02|离职登记|^03|离职确认"
       		 ondblclick = "return showCodeListEx('AgentState',[this,AgentStateName],[0,1]);"
       		 onkeyup = "return showCodeListKeyEx('AgentState',[this,AgentStateName],[0,1]);"><Input class="codename" name= AgentStateName readonly=true>
       		 </td> 
      </TR>
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
    </Div>

    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGridB);">
    		</td>
    		<td class= titleImg>
    			  业务员结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGridB" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGridB" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
