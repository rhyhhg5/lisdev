
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();


//***************************************************
//* 验证必填项
//***************************************************
function checkValid()
{
	if (trim(document.fm.ManageCom.value)=="")
	{
		alert("请填写管理机构!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		return false;
	}
	
	if (document.fm.ManageCom.value.length != 8)
	{
		alert("管理机构输入错误!");
		document.fm.ManageCom.value="";
		document.fm.ManageComName.value="";
		fm.all('ManageCom').focus();
		return false;
	}

	return true;
}

/*
//***************************************************
//* 单击“fanhui”进行的操作
//***************************************************
/*function getPara()
{ alert("1234566");
	var tGrideRow = EvaluateGrid.getSelNo();
	var tMakeContNo = EvaluateGrid.getRowColData(tGrideRow-1,1);
	var tProposalContNo = EvaluateGrid.getRowColData(tGrideRow-1,2);
	var tAgentCode = EvaluateGrid.getRowColData(tGrideRow-1,3);
	var tAgentName = EvaluateGrid.getRowColData(tGrideRow-1,4);
	var tManageCom = EvaluateGrid.getRowColData(tGrideRow-1,5);
	var tBranchAttr = EvaluateGrid.getRowColData(tGrideRow-1,6);
	var tRiskCode = EvaluateGrid.getRowColData(tGrideRow-1,8);
	var tPrem = EvaluateGrid.getRowColData(tGrideRow-1,9);
	var tAppntName = EvaluateGrid.getRowColData(tGrideRow-1,10);
    alert(tAppntName);
    document.fm.ProposalContNo.value =tProposalContNo ;
    document.fm.AgentCode.value =tAgentCode ;
    document.fm.ManageCom.value =tManageCom ;
    document.fm.RiskCode.value =tRiskCode ;
    document.fm.Prem.value =tPrem ;
    document.fm.AppntName.value =tAppntName ;
    document.fm.AgentName.value =tAgentName ;
    document.fm.BranchAttr.value = tBranchAttr;
    document.fm.MakeContNo.value = tMakeContNo;
    
 	
	}
	*/
//***************************************************
//* 单击“查询”进行的操作
//***************************************************

function easyQueryClick()

{  
	//alert("kaishi");
	//验证必填项
	if(!checkValid())
	{
		return false;	
	}
	//alert("zhengque");
	var tSQL = "";

	tSQL = "select a.ProposalContNo,a.AppntName,getUniteCode(a.AgentCode),"
	+ "(select name from LAAgent where agentcode =a.agentcode),a.ManageCom,"
	+ " a.BranchAttr,c.Name,a.Prem,a.MakeContNo"
    + " FROM "
    + " LAMakeCont a ,LAAgent b,LABranchGroup c" 
    + " where  1=1 "
    + " and  a.agentcode = b.agentcode"
    + " and a.Agentgroup =c.Agentgroup ";
   //业务系列

  tSQL += getWherePart('a.ManageCom','ManageCom','like')
  + getWherePart('a.ProposalContNo','ProposalContNo')
  + getWherePart('b.GroupAgentCode','AgentCode')
  + getWherePart('a.AppntName','AppntName') 
  + getWherePart('a.Prem','Prem')
  + getWherePart('a.InputDate','InputDate')
  + "ORDER BY a.MakeContNo" ;
	//alert(tSQL);
	//执行查询并返回结果
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
	if(!strQueryResult)
	{  alert("没有符合条件的信息！"); 
		turnPage.queryModal(tSQL, EvaluateGrid); 
		return false;
		}
    else turnPage.queryModal(tSQL, EvaluateGrid);
	
	
}

//fanhui
function returnParent()
{
  var arrReturn = new Array();
	var tSel = EvaluateGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
			    top.opener.afterQuery( arrReturn );
			}
			
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}	
			top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = EvaluateGrid.getSelNo();
	//alert("000" + tRow);
	if( tRow == 0 || tRow == null  )
	//|| arrDataSet == null
	{  alert("1");   return arrSelected;   }
		
	arrSelected = new Array();
	//alert(EvaluateGrid.getRowColData(tRow-1,9));
	var strSQL = "";
	strSQL = "select "
	         +"a.ManageCom,"
	         + "getUniteCode(a.AgentCode),"
	         +" a.BranchAttr,"
	         + "b.name,"
	         +"a.ProposalContNo,"
	         +"a.AppntName,"
	         +"a.InputDate,"
	         +"a.Prem,"
	         +"a.MakeContNo, "
	         +"c.Name"
	         +" from LAMakeCont a ,LAAgent b,Ldcom c"
	         +" where 1=1 " 
	         +" and a.Makecontno = '"+ EvaluateGrid.getRowColData(tRow-1,9)
	         +"' and a.Proposalcontno= '"+EvaluateGrid.getRowColData(tRow-1,1)
	         +"' and c.ComCode= '"+EvaluateGrid.getRowColData(tRow-1,5)
	         +"' and a.AgentCode=b.AgentCode"
	         +" order by a.Makecontno";
	//alert("422");         
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
    if (!turnPage.strQueryResult) {
      alert("查询失败！");
      return false;
     }

    arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

    return arrSelected;


}


//***************************************************
//* 提交操作完成后的处理
//***************************************************
function afterSubmit(FlagStr, content)
{
	showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    //EvaluateGrid.clearData("EvaluateGrid");
  }
}