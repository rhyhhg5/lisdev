<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LATrainerCommisionInput.jsp
//程序功能：组训基本工资标准录入
//创建时间：2018-06-7
//创建人  ：WangQingMin

%>
<%@page contentType="text/html;charset=GBK" %>
 <script language="JavaScript">
 var msql=" 1 and   char(length(trim(comcode)))<=#4# ";

   var StrSql="1 and code not in (select riskcode from lmriskapp where risktype4=#4#)  ";
   var tSql = " 1 and BranchType=#1# and BranchType2=#01#  and managecom like #" ;
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LATrainerCommisionInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="LATrainerCommisionInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>　
<body  onload="initForm();initElementtype();" >
 <form action="./LATrainerCommisionSave.jsp" method=post name=fm target="fraSubmit">  
  <table>
   <tr>
    <td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
   <table class=common >
   <tr class=title>
   	<TD  class= title>管理机构</TD>
    <TD  class= input>
      <Input class="codeno" name=ManageCom  verify="管理机构|notnull&code:comcode"
        ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,1,1);" 
        onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,1,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
      </TD>
       </TR>       
  </table>
    </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="修  改" class=cssButton onclick="return DoSave();">  
    		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 
    	</td>
    </tr>      
  </table>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>基本工资信息结果
				</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>  
      
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div> 
   <br><hr> 
       <input type=hidden id="fmAction" name="fmAction">
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




