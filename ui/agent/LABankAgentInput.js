//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


function initEdorType(cObj)
{
	mEdorType = " #1# and codealias=#3# ";
	showCodeList('agentkind',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{	
	mEdorType = " #1# and codealias=#3#";
	showCodeListKey('agentkind',[cObj], null, null, mEdorType, "1");
}

//提交，保存按钮对应操作
function submitForm()
{
  if (mOperate!='DELETE||MAIN')
  {
    if (!beforeSubmit())
      return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
	fm.all('AgentGrade').disabled = true;   	        
    fm.all('AgentKind').disabled = true;
   	fm.all('AgentGroup').disabled = true;
   	fm.all('ChannelName').disabled = true;
  showInfo.close();
  fm.all('hideIsManager').value = false;
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LABankAgentInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	    
  if( verifyInput() == false ) return false;
  
  var strChkIdNo = chkIdNo(trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value))
  if (strChkIdNo != "")
  {  	
        alert(strChkIdNo);
	return false;
  }
  if (!judgeManager(trim(fm.all('BranchManager').value),trim(fm.all('AgentGrade').value),fm.all('Branchlevel').value))
    return false;
  return true;
}           

function judgeManager(cManager,cAgentGrade,cLevel)
{
    var clue="",query="";
    if(cManager == null || cManager == '' || cManager == fm.all('AgentCode').value)
    {
      if (cAgentGrade.substr(1)<'08') //高级二档以下
      {
          alert('请先确定所在机构的组长或经理！');
          return false;
      }else
      {
      	 //alert(cLevel);
      	 switch(trim(cLevel))
      	 {
      	   case '1':  //银代部级
//      	      if (cAgentGrade.substr(1) == '12')  //暂定为内外勤资深都可作银代部经理
      	        query = "是否指定该人为该银代部的经理？";
//      	      else
//      	        clue = "必须先为该银代部增员经理！";
      	      break;
      	   case '2':  //银代协理级
//      	      if (cAgentGrade.substr(1) == '11')
      	        query = "是否指定该人为该银代部的经理助理？";
//      	      else
//      	        clue = "必须先为该银代部增员经理助理！";
      	      break;
      	   case '3':  //银代组级
//      	      if (cAgentGrade.substr(1) >= '08' && cAgentGrade.substr(1) <= '10')
      	        query = "是否指定该人为该组的经理？";
//      	      else
//      	        clue = "必须先为该组增员渠道经理！";      	      
      	      break;
      	 }
      	 if (query != '')
      	 {
      	   if (confirm(query))
      	   {
      	     fm.all('hideIsManager').value = 'true';
      	     return true;
      	   }
      	   else
      	   {
      	     fm.all('hideIsManager').value = 'false';
      	     return false;
      	   }
      	 }else
      	  {
      	     alert(clue);
      	     return false;	 
      	  }
      }
    }else   //已存在管理人员
    {
    	switch(cLevel)
      	{
      	   case '1':  //银代部级
      	      alert('银代部的经理已存在，不能再在该级机构增员了！');
      	      return false;
      	   case '2':  //银代协理级
      	      alert('银代部的经理协理已存在，不能再在该级机构增员了！');
      	      return false;
      	}
    }
    return true;
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
 else 
 {
  	parent.fraMain.rows = "0,0,0,82,*";
 }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.all('AgentCode').value = '';
  fm.all('IDNo').value = '';
  fm.all('AgentGrade').value = '';
  fm.all('AgentGroup').value = '';
  fm.all('ManageCom').value = '';
  fm.all('UpAgent').value = '';
  initForm();
  fm.all('AgentGrade').disabled = false;
  fm.all('AgentKind').disabled = false;
  fm.all('AgentGroup').disabled = false;
  fm.all('ChannelName').disabled = false;
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	fm.all('AgentGrade').disabled = false;
  fm.all('AgentKind').disabled = false;
  fm.all('AgentGroup').disabled = false;
  fm.all('ChannelName').disabled = false; 
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');    
  }else
  {
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {  	
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./LABankAgentQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }else
  {	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function changeGroup()
{
   if (getWherePart('AgentGroup')=='')
     return false;
     
   var tAgentGrade = trim(fm.all('AgentGrade').value);
   if (tAgentGrade==null ||tAgentGrade==''){
     alert('请先录入职级！');
     fm.all('AgentGroup').value = '';
     return false;
   }
   
   var tAgentKind = trim(fm.all('AgentKind').value);
   var strSQL = "";
   strSQL = "select a.BranchAttr,a.ManageCom,a.BranchManager,a.AgentGroup,a.BranchLevel,a.BranchJobType," 
           +" (select b.BranchManager from labranchGroup b where b.AgentGroup = a.UpBranch and (b.state<>'1' or b.state is null)) upAgent,"
           +"(Select AgentState from LAAgent where Agentcode = a.BranchManager) agentstate,name from LABranchGroup a "
           +"where 1=1 and a.BranchType = '3' and a.EndFlag <> 'Y' and (a.state<>'1' or a.state is null)"
           + getWherePart('a.BranchAttr','AgentGroup');			
     	 //alert(strSQL);
   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该销售机构！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('ManageCom').value = '';
   	fm.all('BranchManager').value = '';
   	//fm.all('hideManageCom').value = '';
   	fm.all('hideAgentGroup').value = '';
   	fm.all('BranchLevel').value = '';
   	return false;
   }  
   var arr = decodeEasyQueryResult(strQueryResult);
   //判断职级的内外勤与所属机构的内外亲属性是否一致(渠道组以上机构不判断)
   if (trim(arr[0][4])=='3')  
   {
     if ((trim(arr[0][5])=='0' && tAgentGrade.substr(0,1)=='B')
       ||(trim(arr[0][5])=='1' && tAgentGrade.substr(0,1)=='C')) //0,C:内勤 1,B:专管员
     {
        alert("职级与机构属性不一致！");	
        fm.all('AgentGroup').value = '';
        fm.all('UpAgent').value = '';
   	fm.all('BranchManager').value = '';
        fm.all('ManageCom').value = '';
        //fm.all('hideManageCom').value = '';
        fm.all('hideAgentGroup').value = '';
        fm.all('BranchLevel').value = '';
        return false;
     }
   }
   fm.all('AgentGroup').value = trim(arr[0][0]);
   fm.all('ChannelName').value = trim(arr[0][8]);
   
//根据岗位名称确定上级代理人
//如果组长，
   if (tAgentKind=='02'||tAgentKind=='03')
     fm.all('UpAgent').value = trim(arr[0][6]);
     //如果经理
   else if (tAgentKind=='01')
     fm.all('UpAgent').value = "";
   else//组员
     fm.all('UpAgent').value = trim(arr[0][2]);
     
   fm.all('ManageCom').value = trim(arr[0][1]);
   //查询管理人员是否离职
   if (arr[0][7] == '01' || arr[0][7] == '02')
     fm.all('BranchManager').value = trim(arr[0][2]);
   else
     fm.all('BranchManager').value = '';
   //fm.all('hideManageCom').value = arr[0][1];
   fm.all('hideAgentGroup').value = trim(arr[0][3]);
   fm.all('BranchLevel').value = trim(arr[0][4]);
   return true;
}

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;		                
		fm.all('AgentCode').value = arrResult[0][0];
                fm.all('Name').value = arrResult[0][5];
                fm.all('Sex').value = arrResult[0][6];
                fm.all('Birthday').value = arrResult[0][7];
                fm.all('NativePlace').value = arrResult[0][8];
                fm.all('Nationality').value = arrResult[0][9];
                fm.all('HomeAddress').value = arrResult[0][13];
                fm.all('ZipCode').value = arrResult[0][15];
                fm.all('Phone').value = arrResult[0][16];
                fm.all('BP').value = arrResult[0][17];
                fm.all('Mobile').value = arrResult[0][18];
                fm.all('EMail').value = arrResult[0][19];
                fm.all('IDNo').value = arrResult[0][21];
                fm.all('PolityVisage').value = arrResult[0][24];
                fm.all('Degree').value = arrResult[0][25];
                fm.all('GraduateSchool').value = arrResult[0][26];
                fm.all('Speciality').value = arrResult[0][27];
                fm.all('PostTitle').value = arrResult[0][28];
                fm.all('OldCom').value = arrResult[0][31];
                fm.all('OldOccupation').value = arrResult[0][32];
                fm.all('HeadShip').value = arrResult[0][33];
               // fm.all('QuafNo').value = arrResult[0][37];
                //fm.all('QuafEndDate').value = arrResult[0][39];
               // fm.all('TrainPeriods').value = arrResult[0][73];
                fm.all('EmployDate').value = arrResult[0][49];
                fm.all('AgentState').value = arrResult[0][61];
                fm.all('RgtAddress').value = arrResult[0][64];
                //fm.all('BankCode').value = arrResult[0][65];
                //fm.all('BankAccNo').value = arrResult[0][66];
                fm.all('Remark').value = arrResult[0][60];
                fm.all('AgentKind').value = arrResult[0][43];
                fm.all('Operator').value = arrResult[0][67];   
                //77-BranchManager,78-AgentGrade,79-BranchAttr,80-AgentGroup,81-UpAgentName,82-BranchLevel
                //行政信息
                fm.all('ManageCom').value = arrResult[0][2];
                fm.all('UpAgent').value = arrResult[0][81];
                fm.all('AgentGrade').value = arrResult[0][78];
                
                
   	        fm.all('BranchManager').value = arrResult[0][77];
                //显式机构代码
                fm.all('AgentGroup').value = arrResult[0][79];
                fm.all('hideAgentGroup').value = arrResult[0][80];
   	        fm.all('BranchLevel').value = arrResult[0][82];
   	        fm.all('ChannelName').value=arrResult[0][83];
   	        fm.all('InsideFlag').value=arrResult[0][45];
   	        fm.all('AgentGrade').disabled = true;   	        
            fm.all('AgentKind').disabled = true;
   	        fm.all('AgentGroup').disabled = true;
   	        fm.all('ChannelName').disabled = true;
                
        }
}

function changeIDNo()
{
   if (getWherePart('IDNo')=='')
     return false;
   var strSQL = "";
   strSQL = "select * from LAAgent where 1=1 and agentstate<='02'"
           + getWherePart('IDNo');		
     	 //alert(strSQL);
   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
   if (strQueryResult)
   {
   	alert('该身份证号已存在!');
   	fm.all('IDNo').value = '';
   	return false;
   }
   return true; 
}


/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	//alert(cCodeName);
	try	{
		if( cCodeName == "AgentGrade" )	
		{
			checkvalid();//loadFlag在页面出始化的时候声明
		}
	}
	catch( ex ) {
	}
}


function checkvalid()
{ 
  	fm.InsideFlag.value="";  
}         