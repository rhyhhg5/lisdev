<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LASIAgentQueryInput.jsp
//程序功能：
//创建日期：2017-11-29
//创建人  ：王清民
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String qManageCom=tG.ManageCom;
  System.out.println("BranchType:"+BranchType);
  System.out.println("managecom:"+qManageCom);
%>
<script>
   var msqlBranch=" 1 and CODEALIAS =#"+'2'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="./LASIAgentQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LASIAgentQueryInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
<title>查询 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAgentQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            </td>
            <td class= titleImg>
                查询条件
            </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common>
        <TD class= title>
          管理机构
        </TD>
        <TD  class= input>
            <Input class="codeno" name=ManageCom   verify="管理机构|notnull&code:comcode" 
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
             ><Input name=ManageComName class="codename" readOnly elementtype=nacessary> 
        </TD> 
        <TD class= title>
          销售团队
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup >
        </TD>
         <TD class= title>
          编码
        </TD>
        <TD  class= input>
          <Input class=common  name=AgentCode  type = hidden>
          <Input class=common  name=groupAgentCode >
        </TD>
       
      </TR>
      <TR  class= common>
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD>
        <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" MAXLENGTH=1 
           ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" 
           onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" 
          ><Input name=SexName class="codename">
        </TD>
        <TD  class= title>
          出生日期
        </TD>
        <TD  class= input>
          <Input name=Birthday class="coolDatePicker" dateFormat="short" >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          身份证号码
        </TD>
        <TD  class= input>
          <Input name=IDNo class= common >
        </TD>
        <TD  class= title>
          入司时间
        </TD>
        <TD  class= input>
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' >
        </TD>
           <TD class = title style="display:none">
             渠道类型
          </TD>
          <TD  class= input  style="display:none">
            <Input class='codeno' name=BranchType2     
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,msqlBranch,1);" 
             ><Input class=codename name=BranchType2Name readOnly >
          </TD>
      </TR>
    
  
    </table>
          <input type=hidden name=BranchType value=''> 
   
                   
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
    </Div>
   
    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 结果
    		</td>
    	</tr>
    </table>

    	<Div  id= "divAgentGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
   
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
