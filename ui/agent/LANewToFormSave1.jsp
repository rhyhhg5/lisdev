<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LANewToFormSave1.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema mLAAgentSchema = new LAAgentSchema();
  LATreeSchema  mLATreeSchema = new LATreeSchema();
  LANewToFormUI mLANewToFormUI  = new LANewToFormUI();

  //输出参数
  CErrors tError = null;
  String tOperate="UPDATE||INSERT";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //取得被调整信息
  System.out.println("begin LANewToFormUI schema...");

  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("AgentCode",request.getParameter("AgentCode"));// 人员编码
  tTransferData.setNameAndValue("NewEmploydate",request.getParameter("AdjustDate"));// 人员入司时间
  tTransferData.setNameAndValue("IntroNewAgency",request.getParameter("IntroNewAgency"));// 推荐人
  tTransferData.setNameAndValue("TrainDate",request.getParameter("NewTrainDate"));// 筹备日期
  
  tTransferData.setNameAndValue("AgentGroup",request.getParameter("AgentGroup"));//人员所属内部团队编码
  tTransferData.setNameAndValue("AgentGrade",request.getParameter("AgentGrade"));//人员所属职级
  
  

  //传递数据
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tG);
  tVData.add(tTransferData);
  System.out.println("add over");
   try
  {
    System.out.println("this will save the data!!!");
    mLANewToFormUI.submitData(tVData,tOperate);
     System.out.println(FlagStr);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLANewToFormUI.mErrors;
    if (!tError.needDealError())
    {
    //Agentcode = tLAAgent.getAgentCode();
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

