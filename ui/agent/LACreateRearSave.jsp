<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LANewToFormSave1.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LACreateRearBL mLACreateRearBL  = new LACreateRearBL();

  //输出参数
  CErrors tError = null;
  String tOperate="INSERT";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //取得被调整信息
  System.out.println("begin LACreateRearBL schema...");

  
  //传递数据
  VData tVData = new VData();
  FlagStr="";
  tVData.add(request.getParameter("ManageCom"));
  tVData.add(request.getParameter("AgentCode"));
  tVData.add(request.getParameter("RearAgentCode"));
  tVData.add(tG);
  
   try
  {
    System.out.println("this will save the data!!!");
    mLACreateRearBL.submitData(tVData,tOperate);
     System.out.println(FlagStr);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLACreateRearBL.mErrors;
    if (!tError.needDealError())
    {
    //Agentcode = tLAAgent.getAgentCode();
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

