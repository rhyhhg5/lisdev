<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABankNewTreeSave.jsp
//程序功能：
//创建日期：2004-04-1 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数  
  System.out.println("begin labanknewtreesave.jsp...");
  LABankNewTreeUI tLABankNewTreeUI   = new LABankNewTreeUI();

  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    String tAgentCode=new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+request.getParameter("AgentCode")+"'");    //需要变更的业务员代码
    String tAgentGroup =request.getParameter("AgentGroup");  //新录入的销售机构代码
    String tNewAgentGrade=request.getParameter("NewAgentGrade");    //新行政职级
    String tDate=request.getParameter("AdjustDate");   //调整日期
    String tBranchType=request.getParameter("BranchType");
    String tBranchType2=request.getParameter("BranchType2");
  
  // 准备传输数据 VData
   VData tVData = new VData();
   FlagStr=""; 
   tVData.add(tG); 
   tVData.add(tAgentCode);
   tVData.add(tAgentGroup);
   tVData.add(tNewAgentGrade);
   tVData.add(tDate);
   tVData.add(tBranchType);
   tVData.add(tBranchType2);
  
  try
  {
    System.out.println("this will save the data!!!");
    tLABankNewTreeUI.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankNewTreeUI.mErrors;
    if (!tError.needDealError())
    {        
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">        
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

