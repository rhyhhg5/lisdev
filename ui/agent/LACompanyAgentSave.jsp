<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2006-10-30 15:12:33
//创建人   
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LAAgentComUI tLABankAgent = new LAAgentComUI();
  String AgentCode = request.getParameter("AgentCode");
  String tBranchType = request.getParameter("BranchType");
  String tBranchType2 = request.getParameter("BranchType2");
  System.out.println("如为修改，应显示原工号:"+AgentCode);
  String tGroupAgentCode = request.getParameter("GroupAgentCode");
  System.out.println("如为修改，应显示集团工号:"+tGroupAgentCode);
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("Operate");
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    LisIDEA tLisIdea = new LisIDEA();
    tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));//设置销售人员默认密码，方便以后查询分析系统使用
    tLAAgentSchema.setAgentCode(AgentCode);
    tLAAgentSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLAAgentSchema.setBranchCode(request.getParameter("AgentGroup"));
    tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
    tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
    tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
    tLAAgentSchema.setName(request.getParameter("Name"));
    tLAAgentSchema.setSex("3");  //其他,公司业务没有性别
    tLAAgentSchema.setBirthday(request.getParameter("EmployDate"));//与录入日期一样
    //tLAAgentSchema.setIDNo("aaaaaa");
    tLAAgentSchema.setIDNo(request.getParameter("IDNo"));
    tLAAgentSchema.setIDNoType("4");                       //4表示其他类型
  	tLAAgentSchema.setFullTimeFlag(request.getParameter("GrpFlag"));
 
    tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
    tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
    //modify by zhuxt 20140926 默认合同制
	//2014-11-17   杨阳
	//功能 #2279 根据需求，把代理制改为营销人员；合同制改为直销人员，但对应代码不变
    tLAAgentSchema.setAgentType("2");
    
    
  
   // tLAAgentSchema.setBankCode(request.getParameter("BankCode"));
    //tLAAgentSchema.setBankAccNo(request.getParameter("BankAccNo"));
   // tLAAgentSchema.setRemark(request.getParameter("Remark"));
    //tLAAgentSchema.setAgentKind(request.getParameter("AgentKind"));
    
    tLAAgentSchema.setOperator(request.getParameter("Operator"));
   // tLAAgentSchema.setChannelName(request.getParameter("ChannelName"));
    tLAAgentSchema.setInsideFlag("0");
    tLAAgentSchema.setGroupAgentCode(tGroupAgentCode);
    //取得行政信息--在bl中设置职级及系列
    tLATreeSchema.setAgentCode(AgentCode);
    tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
    tLATreeSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLATreeSchema.setBranchCode(request.getParameter("AgentGroup"));
    tLATreeSchema.setAgentLine("A");
    
    tLATreeSchema.setBranchType(request.getParameter("BranchType"));
    tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
    if("6".equals(tBranchType)){
    	tLATreeSchema.setInitGrade("Y01"); 
    	tLATreeSchema.setAgentGrade("Y01");
    }
    if("2".equals(tBranchType)){
    	tLATreeSchema.setInitGrade("D01"); 
    	tLATreeSchema.setAgentGrade("D01");
    }
    tLATreeSchema.setSpeciFlag("1");//特殊人员
    tLATreeSchema.setInsideFlag("0");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
 // tVData.addElement(tLAWarrantorSet);
 // tVData.addElement(tLARearRelationSet);
  try
  {
    System.out.println("this will save the data!!!");
    tLABankAgent.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankAgent.mErrors;
    if (!tError.needDealError())
    {
      if(tOperate.equals("INSERT||MAIN"))
    	AgentCode = tLABankAgent.getAgentCode();
    System.out.println("AgentCode"+AgentCode);
    	String tGroupAgentCodeSQL = "select GroupAgentCode from laagent where agentcode = '"+AgentCode+"' ";
    	tGroupAgentCode = new ExeSQL().getOneValue(tGroupAgentCodeSQL);
    	System.out.println("处理完成时原工号："+AgentCode);
    	System.out.println("处理完成时集团工号："+tGroupAgentCode);
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
        parent.fraInterface.fm.all('AgentCode').value = '<%=AgentCode%>';
        parent.fraInterface.fm.all('GroupAgentCode').value = '<%=tGroupAgentCode%>';
       // alert(parent.fraInterface.fm.all('AgentCode').value);
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
