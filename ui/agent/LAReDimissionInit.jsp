<%
//程序名称：LADimissionInit.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     String gToday = PubFun.getCurrentDate(); //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{ 
  try
  {
    fm.all('AgentCode').value = '';
    fm.all('GroupAgentCode').value = '';
    fm.all('AgentName').value='';
    fm.all('DepartTimes').value = '';
    fm.all('DepartDate').value = '<%=gToday%>';
    fm.all('DepartRsn').value = '';
    fm.all('ApplyDate').value = '';
//    fm.all('QualityDestFlag').value = '';
//    fm.all('PbcFlag').value = '';
//    fm.all('ReturnFlag').value = '';
//    fm.all('AnnuityFlag').value = '';
//    fm.all('VisitFlag').value = '';
//    fm.all('BlackFlag').value = '';
//    fm.all('OweMoney').value = '';
//    fm.all('BlackListRsn').value = '';    
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>'; 
            fm.all('CurrDate').value = '<%=gToday%>';    
    

  }
  catch(ex)
  {
    alert("在LADimissionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LADimissionInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
//    initAgentWageGrid();
//    initContGrid();
//    initSaleContGrid();
  }
  catch(re)
  {
    alert("在LADimissionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentWageGrid
 ************************************************************
 */
function initAgentWageGrid()
{
	var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]= "<%=tTitleAgent%>" + "编码";         //列名
    iArray[1][1]="100px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]= "<%=tTitleAgent%>" + "姓名";         //列名
    iArray[2][1]="80px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="管理机构";         //列名
    iArray[3][1]="100px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="销售机构";         //列名
    iArray[4][1]="100px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="销售机构名称";         //列名
    iArray[5][1]="150px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();
    iArray[6][0]="<%=tTitleWage%>" + "年月";         //列名
    iArray[6][1]="60px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[7]=new Array();
    iArray[7][0]="实发" + "<%=tTitleWage%>";         //列名
    iArray[7][1]="60px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

    AgentWageGrid = new MulLineEnter( "fm" , "AgentWageGrid" ); 

    //这些属性必须在loadMulLine前
    AgentWageGrid.mulLineCount = 0;   
    AgentWageGrid.displayTitle = 1;
  	AgentWageGrid.hiddenPlus = 1;
  	AgentWageGrid.hiddenSubtraction = 1;
    AgentWageGrid.locked=1;
    AgentWageGrid.canSel=0;
    AgentWageGrid.canChk=0;
    AgentWageGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert("初始化AgentWageGrid时出错："+ ex);
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentWageGrid
 ************************************************************
 */
function initContGrid()
{
	var iArray = new Array();
  
  try
  {
  
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]= "保单号";         //列名
    iArray[1][1]="70px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

	iArray[2]=new Array();
    iArray[2][0]= "印刷号";         //列名
    iArray[2][1]="70px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;
	
    iArray[3]=new Array();
    iArray[3][0]= "投保人名称";         //列名
    iArray[3][1]="110px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="保费";         //列名
    iArray[4][1]="50px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
	
	iArray[5]=new Array();
    iArray[5][0]= "签单日期";         //列名
    iArray[5][1]="60px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="回执回销日期";         //列名
    iArray[6][1]="60px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
	
    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 

    //这些属性必须在loadMulLine前
    ContGrid.mulLineCount = 0;   
    ContGrid.displayTitle = 1;
  	ContGrid.hiddenPlus = 1;
  	ContGrid.hiddenSubtraction = 1;
    ContGrid.locked=1;
    ContGrid.canSel=0;
    ContGrid.canChk=0;
    ContGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert("初始化AgentWageGrid时出错："+ ex);
  }
}


function initSaleContGrid()
{
	var iArray = new Array();
  
  try
  {
  
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]= "保单号";         //列名
    iArray[1][1]="70px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

		iArray[2]=new Array();
    iArray[2][0]= "印刷号";         //列名
    iArray[2][1]="70px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;
	
    iArray[3]=new Array();
    iArray[3][0]= "投保人名称";         //列名
    iArray[3][1]="110px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="保费";         //列名
    iArray[4][1]="50px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
	
		iArray[5]=new Array();
    iArray[5][0]= "签单日期";         //列名
    iArray[5][1]="60px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="回执回销日期";         //列名
    iArray[6][1]="60px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="保单类型";         //列名
    iArray[7][1]="60px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=3;         //是否允许录入，0--不能，1--允许
	
    SaleContGrid = new MulLineEnter( "fm" , "SaleContGrid" ); 

    //这些属性必须在loadMulLine前
    SaleContGrid.mulLineCount = 0;   
    SaleContGrid.displayTitle = 1;
  	SaleContGrid.hiddenPlus = 1;
  	SaleContGrid.hiddenSubtraction = 1;
    SaleContGrid.locked=1;
    SaleContGrid.canSel=0;
    SaleContGrid.canChk=0;
    SaleContGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert("初始化SaleContGrid时出错："+ ex);
  }
}
</script>