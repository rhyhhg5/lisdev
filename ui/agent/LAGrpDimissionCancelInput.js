//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var mDepartDate="";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	  
  if(!verifyInput())
  {
      return false;	
  }
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function registerCancel()
{
	var tState=fm.all('hideState').value;
  if(tState==null || tState==''){
  	alert('操作失败，离职状态数据丢失！');
  	return false;
  }else if(tState>='06'){
  	alert('代理人已离职确认，请选择离职确认回退操作！');
  	return false;
  }
  if (confirm("您确实想进行离职回退吗?"))
  {
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“修改”图片时触发该函数
function confirmCancel()
{
  var tState=fm.all('hideState').value;
  if(tState==null || tState==''){
  	alert('操作失败，离职状态数据丢失！');
  	return false;
  }else if(tState<'06'){
  	alert('代理人已离职登记，请选择离职登记回退操作！');
  	return false;
  }
  
  if (confirm("您确实想进行离职回退吗?"))
  {
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }

}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";
var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
  showInfo=window.open("./LAGrpDimissionCancelHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证代理人编码的合理性
function checkValid()
{ 

  var strSQL = "";
  var strQueryResult = null;
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var tManageCom =getManageComLimit(); 
  var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select getunitecode(AgentCode),name,managecom from LAAgent where agentstate>='03' "+strAgent//getWherePart('AgentCode')
             +" and branchtype='"+tBranchType+"'"
             +" and branchtype2='"+tBranchType2+"'";
     strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
     //alert(strSQL);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员或此业务员没有离职登记！");
    fm.all('AgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  else
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult); 
  	fm.all('AgentName').value = arrDataSet[0][1]; 
  	mManageCom=arrDataSet[0][2]; 	  
  	if(mManageCom.indexOf(tManageCom)<0)
  	{
  	alert("您没有该业务员的操作权限!");
  	fm.all('AgentCode').value  = "";
        fm.all('DepartTimes').value = "";
        initInpBox();
        return false;
  	}  	  
  }
  //查询成功则拆分字符串，返回二维数组
  var tBranchType=fm.all('BranchType').value;
   var tBranchType2=fm.all('BranchType2').value;
  strQueryResult = null;
  var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  //strSQL = "select AgentCode,departtimes,applydate from LADimission where 1=1 "+ getWherePart('AgentCode')
	//   +" and branchtype='"+tBranchType+"'"
	//   +" and branchtype2='"+tBranchType2+"'"
	//   + " order by AgentCode,departtimes";
	strSQL = "select getunitecode(a.AgentCode),b.name,a.DepartTimes,a.DepartRsn,a.DepartDate,a.ApplyDate,"
	       + "a.QualityDestFlag,a.PbcFlag,a.ReturnFlag,a.AnnuityFlag,a.VisitFlag,a.BlackFlag,"
	       + "a.BlackListRsn,case when (a.DepartState='03' or a.DepartState='04' or a.DepartState='05') then '离职未确认'"
	       + " when (a.DepartState='08' or a.DepartState='07' or a.DepartState='06') then '离职已确认' else '' end,"
	       + "a.DestoryFlag,a.operator,a.makedate,a.departstate,b.agentstate,b.outworkdate from ladimission a,laagent b"
	       + " where a.agentcode=b.agentcode " +strAgent//getWherePart('a.AgentCode','AgentCode')
	       +" order by a.modifydate desc,a.modifytime desc fetch first 1 rows only with ur";
  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult){
    alert('查询失败，此代理人为非正常离职，不能通过此功能回退。');
    fm.all('AgentCode').value  = "";
    initInpBox();
    return false;
  }else
  {
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    if(arrDataSet[0][17]!=arrDataSet[0][18]){
    	alert('查询失败，此代理人为非正常离职，不能通过此功能回退。');
    	fm.all('AgentCode').value  = "";
    	initInpBox();
    	return false;
    }else if(arrDataSet[0][4]!=arrDataSet[0][19]){
    	alert('查询失败，此代理人为非正常离职，不能通过此功能回退。');
    	fm.all('AgentCode').value  = "";
    	initInpBox();
    	return false;
    }
    afterQuery(arrDataSet);
    //fm.all('DepartTimes').value = eval(arrDataSet[0][1]);
    //fm.all('ApplyDate').value=arrDataSet[0][2];
  }
  // 设置离职欠费标记
	document.fm.OweMoney.value = getDebtMark(document.fm.AgentCode.value);
}
//用来显示返回的选项
function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
 // alert(arrQueryResult);
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.all('AgentCode').value = arrResult[0][0];
    fm.all('AgentName').value = arrResult[0][1];
    fm.all('DepartTimes').value = arrResult[0][2];    
    fm.all('DepartRsn').value = arrResult[0][3];
    showOneCodeNametoAfter('DepartRsn','DepartRsn');
    fm.all('DepartDate').value = arrResult[0][4];
    mDepartDate=arrResult[0][4];
    fm.all('ApplyDate').value=arrResult[0][5];
    fm.all('QualityDestFlag').value = arrResult[0][6];
    showOneCodeNametoAfter('YesNo','QualityDestFlag');
    fm.all('PbcFlag').value = arrResult[0][7];
    showOneCodeNametoAfter('YesNo','PbcFlag');
    fm.all('ReturnFlag').value = arrResult[0][8];
    showOneCodeNametoAfter('YesNo','ReturnFlag');
    fm.all('AnnuityFlag').value = arrResult[0][9];
    showOneCodeNametoAfter('YesNo','AnnuityFlag');
    fm.all('VisitFlag').value = arrResult[0][10];
    showOneCodeNametoAfter('YesNo','VisitFlag');
    fm.all('BlackFlag').value = arrResult[0][11];
    showOneCodeNametoAfter('YesNo','BlackFlag');
    fm.all('BlackListRsn').value = arrResult[0][12];
    fm.all('DepartState').value = arrResult[0][13];
    fm.all('DestoryFlag').value = arrResult[0][14];
    fm.all('Operator').value=arrResult[0][15];
    fm.all('ModifyDate').value=arrResult[0][16];
    fm.all('hideState').value=arrResult[0][17];
    showOneCodeNametoAfter('YesNo','DestoryFlag');
    // 设置离职欠费标记
		document.fm.OweMoney.value = getDebtMark(arrResult[0][0]);
		// 表示佣金列表
  }   
}

function getAgentName(tAgentCode)
{
	var tAgentName='';
	var strSQL = "select name from LAAgent where 1=1 and AgentCode='"+tAgentCode
             +"'";
             //alert(strSQL);
  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  if (strQueryResult)
  {
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  tAgentName = arrDataSet[0][0]; 
	}
  return tAgentName;
}

/**************************************************************
 * 功能描述：查询是否有欠费
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function getDebtMark(pmAgentCode)
{
 	var tSQL = "";
 	var tDebtMark = "";
 	
	tSQL  = "select";
	tSQL += " case when value(summoney,0) < 0 then 'Y'";
	tSQL += "      else 'N'";
	tSQL += "  end as aaa";
	tSQL += " from lawage";
	tSQL += " where branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"'";
	tSQL += "  and agentcode=getagentcode('"+pmAgentCode+"')";
	tSQL += " order by indexcalno desc";
	
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
  if (strQueryResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult);
  	tDebtMark = arrDataSet[0][0]; 
	}
	
	if("" == tDebtMark)
	{
		tDebtMark = "N";
	}
	
  return tDebtMark;
}

