<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentQueryInput.jsp
//程序功能：
//创建日期：2006-10-26 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./LASocialAgentQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LASocialAgentQueryInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
<title> 查询 </title>
</head>
<body onload="initForm();initElementtype();">
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                查询条件
            </td>
            </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
     <TR  class= common>
        <TD class= title>
         公司业务代码
        </TD>
        <TD  class= input>
          <Input class= 'common'   name=AgentCode >
        </TD>
        <TD  class= title>
           公司业务姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common verify="公司业务姓名|  len<=20" >
        </TD>
     </TR>
     <TR  class= common>

         <TD  class= title>
             管理机构
         </TD>
         <TD  class= input>
            <Input class="codeno" name=ManageCom  verify="管理机构|code:comcode&NOTNULL&len>=8"
			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
         </TD>    
         
         <TD  class= title>
          建立日期
         </TD>
         <TD  class= input>
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' verify="建立日期|Date"  >
         </TD>                           
     </tr>
     <tr  class= common>
         <td  class= title> 销售团队 </td>
         <td  class= input> <input class='codeno' name=BranchAttr 
        	   ondblclick="return getChangeComName(this,BranchAttrName);"
             onkeyup="return getChangeComName1(this,BranchAttrName);"
         ><input class='codeName' readonly name=BranchAttrName  > </td>   
     </tr>    
  </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
    </Div>

    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 公司业务结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
