//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug = "0";
var mAction = "";
var turnPage = new turnPageClass();
// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	// alert(FlagStr);
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		// 初始化
		// initForm();
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		// parent.fraInterface.initForm();
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		showDiv(operateButton, "true");
		showDiv(inputButton, "false");
		// 执行下一步操作
		if (mAction == "INSERT")
			mAction = "INSERT||OK";

	}
}

// Click事件，当点击“修改”图片时触发该函数
function updateClick() {
	if (fm.all('ManageCom').value == '') {
		alert("请先查询，再作修改操作！");
		return false;
	}
	if (verifyInput2() == false)
		return false;
	fm.all('Transact').value = "UPDATE";
	var i = 0;
	var showStr = "正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); // 提交

}

// Click事件，当点击“查询”图片时触发该函数
function queryClick() {
	window.open("./LAUpBranchAttrQuery.html");
}

/*******************************************************************************
 * 查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 参数 ： 查询返回的二维数组 返回值： 无
 * ********************************************************************
 */
function afterQuery(arrQueryResult) {
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
    arrResult = arrQueryResult;
    fm.all('ManageCom').value = arrResult[0][0];
    fm.all('BranchAttr').value = arrResult[0][1];
    fm.all('AgentGroup').value = arrResult[0][3];
    fm.all('Name').value = arrResult[0][2];
    fm.all('UpBranchAttr').value = arrResult[0][4];
    showAllCodeName();
	}
}
