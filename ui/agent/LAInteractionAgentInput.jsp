<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
	//程序名称：LAAgentInput.jsp
	//程序功能：个人代理增员管理
	//创建日期：2002-08-16 15:39:06
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String BranchType=request.getParameter("BranchType");
	String BranchType2=request.getParameter("BranchType2");
	String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
	%>
	<script>
		var manageCom = <%=tG.ManageCom%>;
		var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
	</script>
	<%@page contentType="text/html;charset=GBK" %>
	<head >
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="LAInteractionAgentInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LAInteractionAgentInit.jsp"%>
		<%@include file="../agent/SetBranchType.jsp"%>
	</head>
	<body  onload="initForm();initElementtype();" >
		<form action="./LAInteractionAgentSave.jsp" method=post name=fm target="fraSubmit">
			<%@include file="./AgentOp4.jsp"%>
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
						<td class=titleImg>
						  互动专员信息
						</td>
				</tr>
			</table>
			<Div  id= "divLAAgent1" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD class= title>
							互动专员代码
						</TD>
						<TD  class= input>
							<Input class= 'readonly' readonly name=AgentCode type=hidden>
							<Input class= 'readonly' readonly name=GroupAgentCode >
						</TD>
						<TD  class= title>
							姓名
						</TD>
						<TD  class= input>
							<Input name=Name class= common verify="姓名|NotNull&len<=15" elementtype=nacessary>
						</TD>
						<TD  class= title>
							性别
						</TD>
						<TD  class= input>
							<Input name=Sex class="codeno" verify="性别|NotNull" 
							ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,null,'100');" 
							onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" debug='10'
							><Input name=SexName class="codename"  readonly elementtype=nacessary >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							出生日期
						</TD>
						<TD  class= input>
							<Input name=Birthday class=common  verify="出生日期|NotNull&Date" elementtype=nacessary>
						</TD>
						
						<TD  class= title>
							民族
						</TD>
						<TD  class= input>
							<Input name=Nationality class="codeno" id="Nationality" 
							ondblclick="return showCodeList('Nationality',[this,NationalityName],[0,1],null,null,null,null,'100');" 
							onkeyup="return showCodeListKey('Nationality',[this,NationalityName],[0,1]);"
							><Input name=NationalityName class="codename"  >
						</TD>
						<TD  class= title>
							学历
						</TD>
						<TD  class= input>
							<Input name=Degree class="codeno" verify="学历|code:Degree" id="Degree" 
							ondblclick="return showCodeList('Degree',[this,DegreeName],[0,1]);" 
							onkeyup="return showCodeListKey('Degree',[this,DegreeName],[0,1]);"
							><Input name=DegreeName class="codename"  >
						</TD>
						
					</TR>
					<TR  class= common>
					<TD  class= title>
							证件类型
						</TD>
						<TD  class= input>
							<Input name=IDNoType class= "codeno" verify="证件类型|notnull&code:idtype"
							ondblclick="return showCodeList('idtype',[this,IDNoTypeName],[0,1],null,null,null,null,'100');"
							onkeyup="return showCodeListKey('idtype',[this,IDNoTypeName],[0,1]);"
							><Input name=IDNoTypeName class="codename" readonly elementtype=nacessary>
						</TD>
						<TD  class= title>
							证件号码
						</TD>
						<TD  class= input>
							<Input name=IDNo class= common verify="证件号码|notnull&len<=20" elementtype=nacessary onchange="return changeIDNo();">
						</TD>
						<TD  class= title>
							入司日期
						</TD>
						<TD  class= input>
							<Input name=EmployDate class='coolDatePicker' dateFormat='short' verify="入司时间|NotNull&Date" elementtype=nacessary  onchange="return checkdate();">
						</TD>					
					</TR>
					<TR  class= common>
						<TD  class= title>
							专业
						</TD>
						<TD  class= input>
							<Input name=Speciality class= common  verify="毕业院校|len<=13"  >
						</TD>
						<TD  class= title>
							家庭地址
						</TD>
						<TD  class= input>
							<Input name=HomeAddress class= common  verify="家庭地址|len<=26">
						</TD>
						<TD  class= title>
							邮政编码
						</TD>
						<TD  class= input>
							<Input name=ZipCode class= common verify="邮政编码|len=6">
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>
							联系电话
						</TD>
						<TD  class= input>
							<Input name=Phone class= common verify="联系电话|len<=18" >
						</TD>
						<TD  class= title>
							手机
						</TD>
						<TD  class= input>
							<Input name=Mobile class= common verify="手机|len<=15"  >
						</TD>
		                <TD class=title>
		                     标识试用期
		                </TD>
		                <TD class=input><Input name=InDueFormFlag class='codeno'
						verify="标识试用期|notnull"  CodeData="0|^0|有|^1|无"
						ondblclick="return showCodeListEx('InDueFormFlag',[this,InDueFormFlagName],[0,1]);"
						onkeyup="return showCodeListKeyEx('InDueFormFlag',[this,InDueFormFlagName],[0,1]);"><Input
						class=codename name=InDueFormFlagName elementtype=nacessary readOnly></TD>
					</TR>	
					<TR>
					<TD class=title>销售人员类型</TD>
					<TD class=input><Input name=AgentType class='codeno'
						verify="销售人员类型|notnull"
						ondblclick="return showCodeList('activeagenttypecode',[this,AgentTypeName],[0,1]);"
						onkeyup="return showCodeListKey('activeagenttypecode',[this,AgentTypeName],[0,1]);"><Input
						class=codename name=AgentTypeName elementtype=nacessary readOnly></TD>
					<TD class=title>委托代理合同编号</TD>
					<TD class=input><Input name=RetainContno class=common></TD>
					
					<TD class=title>考核指标</TD>
					<TD class=input><Input name=AssessType class='codeno'
						verify="考核指标|notnull"  CodeData="0|^1|月均互动开拓提奖|^2|季度规模保费"
						ondblclick="return showCodeListEx('AssessType',[this,AssessTypeName],[0,1]);"
						onkeyup="return showCodeListKeyEx('AssessType',[this,AssessTypeName],[0,1]);"><Input
						class=codename name=AssessTypeName elementtype=nacessary readOnly></TD>
						
					</TR>					
				</table>
			</Div>
				<table>
				<tr>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent7);">
						<td class= titleImg>
							资格证信息
						</td>
				</tr>
			</table>
			<Div id= "divLAAgent7" style= "display: ''">
			<table  class= common  >
        <TR  class= common>
          <TD  class= title>
            资格证书号
          </TD>
          <TD  class= input>
            <Input class= 'common' name=QualifNo  onblur='checkInput(this.value);'>
          </TD>
          <TD  class= title>
            批准单位
          </TD>
          <TD  class= input>
             <Input class= 'common' name=GrantUnit >
          </TD>
          <TD  class= title>
            发放日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=GrantDate verify="发放日期|Date"> 
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            有效起期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=ValidStart verify="有效起期|Date"> 
          </TD>
           <TD  class= title>
            有效止期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=ValidEnd verify="有效止期|Date"> 
          </TD>
          <TD  class= title>
            资格证书状态
          </TD>
          <TD class=input><input name=QualifState class="codeno" name="QualifState" 
             CodeData="0|^0|有效|^1|失效"
             ondblClick="showCodeListEx('QualifStateList',[this,QualifStateName],[0,1]);"
             onkeyup="showCodeListKeyEx('QualifStateList',[this,QualifStateName],[0,1]);"
             ><Input class=codename name=QualifStateName readOnly>
          </TD>
        </TR>  
  </table>
 </Div>			
 <!--行政信息-->
 <table>
 	<tr>
 		<td class=common>
 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
 			<td class= titleImg>
 				行政信息
 			</td>
 	</tr>
 </table>
 <Div id= "divLAAgent3" style= "display: ''">
 	<table class=common>
 		<tr class=common>
 			<TD class= title>
 				互动专员职级
 			</TD>
 			<TD class= input>
 				<Input name=AgentGrade class="codeno"  verify="营销员职级|notnull&code:AgentGrade"
 				ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
 				onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
 				><Input name=AgentGradeName class="codename" elementtype=nacessary >
 			</TD>
 			<TD class= title>
 				销售团队代码
 			</TD>
 			<TD class= input>
 				<Input class=common name=BranchCode verify="销售团队代码|notnull" onchange="return changeGroup();" elementtype=nacessary>
 			</TD>
 			<TD class= title>
 				销售团队名称
 			</TD>
 			<TD class= input>
 				<Input class='readonly' name=BranchName  readonly>
 			</TD>
 		</tr>
 		<tr class=common>
 		  <TD class= title>
 				管理机构
 			</TD>
 		  <TD class= input>
 				<Input name=ManageCom class='readonly'readonly >
 		  </TD>
 		</tr>
 		<tr>
 			<TD class= title>
 				推荐人
 			</TD>
 			<TD class= input>
 				<Input class=common name=IntroAgency onchange="return changeIntroAgency();" type = hidden>
 				<Input class=common name=newIntroAgency onchange="return changeIntroAgency();"  >
 			</TD>
 			<TD class= title>
 				推荐人姓名
 			</TD>
 			<TD class= input>
 				<Input class=readonly name=IntroAgencyName readonly>
 			</TD>
 		</tr>

 	</table>

 </Div>
 <table>
 	<tr>
 		<td class=common>
 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
 			<td class= titleImg>
 				担保人信息
 			</td>
 	</tr>
 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 			<td text-align: left colSpan=1>
 				<span id="spanWarrantorGrid" >
 				</span>
 			</td>
 		</tr>
 	</table>
 </div>
 <Input type=hidden name=BranchType  value =<%=BranchType %>>   
 <Input type=hidden name=BranchType2 value =<%=BranchType2 %>>    
 <Input type=hidden name=AgentGroup  >  
 <Input type=hidden name=AgentState  >  
 <Input type=hidden name=hiddenAgentGrade  >  
  <Input type=hidden name=hiddenEmployDate  >  
 <input type=hidden name=hideOperate value=''>
 
 <table>
 	<TR>
 		<TD class=common >
 			<Input class=common type=button value="银行信息维护" onclick="BankClick();">
 		</TD>
    </TR>
 </Table>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>


