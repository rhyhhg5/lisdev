<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
GlobalInput globalInput = (GlobalInput)session.getValue("GI");

if(globalInput == null) {
	out.println("session has expired");
	return;
}

String strOperator = globalInput.Operator;
String strManageCom = globalInput.ManageCom;
%>
<script language="JavaScript">

var tAgentTitleGrid = "业务员";

function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';
    fm.all('BranchAttr').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
    //fm.all('RiskCode').value = '';
    fm.all('AppntName').value = '';
    fm.all('Prem').value = '';
    fm.all('ProposalContNo').value = '';
    fm.all('InputDate').value = '';
    fm.all('MakeContNo').value = '';

  }
  catch(ex)
  {
    alert("在LAAssessInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    //initEvaluateGrid();
  }
  catch(re)
  {
    alert("LAAssessInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
