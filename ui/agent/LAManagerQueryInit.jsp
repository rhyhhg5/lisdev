<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
	  var LoginManagecom = fm.all('LoginManagecom').value;
	  if(LoginManagecom.length==8){
			fm.all('PManageCom').value=LoginManagecom.substring(0,4);
		 	showAllCodeName();
		 	fm.all('CManageCom').value=LoginManagecom;
		    var strSQL="select name from ldcom where comcode='"+LoginManagecom+"'";
		    var arrResult = easyExecSql(strSQL);
		 		if(arrResult != null)
		 		{
		 			fm.all('CManageComName').value= arrResult[0][0];
		  		}
		   }
	  
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

var AgentQueryGrid ;
function initSetGrid()
{
  var iArray = new Array();		
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         //列名
      iArray[0][1]="30px";         //列名
      iArray[0][2]=100;         //列名
      iArray[0][3]=0;         //列名

      iArray[1]=new Array();
      iArray[1][0]="营业部经理工号";         //列名
      iArray[1][1]="100px";         //宽度
      iArray[1][2]=100;         //最大长度
      iArray[1][3]=0;      
      
      iArray[2]=new Array();
      iArray[2][0]="省分公司代码";         //列名
      iArray[2][1]="80px";         //宽度
      iArray[2][2]=100;         //最大长度
      iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[3]=new Array();
      iArray[3][0]="省分公司名称";         //列名
      iArray[3][1]="80px";         //宽度
      iArray[3][2]=100;         //最大长度
      iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[4]=new Array();
      iArray[4][0]="中心支公司代码";         //列名
      iArray[4][1]="90px";         //宽度
      iArray[4][2]=100;         //最大长度
      iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[5]=new Array();
      iArray[5][0]="中心支公司名称";         //列名
      iArray[5][1]="90px";         //宽度
      iArray[5][2]=100;         //最大长度
      iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[6]=new Array();
      iArray[6][0]="营业部代码";         //列名
      iArray[6][1]="80px";         //宽度
      iArray[6][2]=100;         //最大长度
      iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[7]=new Array();
      iArray[7][0]="营业部名称";         //列名
      iArray[7][1]="180px";         //宽度
      iArray[7][2]=100;         //最大长度
      iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[8]=new Array();
      iArray[8][0]="姓名";         //列名
      iArray[8][1]="80px";         //宽度
      iArray[8][2]=100;         //最大长度
      iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
       
      iArray[9]=new Array();
      iArray[9][0]="性别";         //列名
      iArray[9][1]="60px";         //宽度
      iArray[9][2]=100;         //最大长度
      iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
 
      iArray[10]=new Array();
      iArray[10][0]="岗位职级";         //列名
      iArray[10][1]="80px";         //宽度
      iArray[10][2]=100;         //最大长度
      iArray[10][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[11]=new Array();
      iArray[11][0]="任本职级日期";         //列名
      iArray[11][1]="80px";         //宽度
      iArray[11][2]=100;         //最大长度
      iArray[11][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[12]=new Array();
      iArray[12][0]="政治面貌";         //列名
      iArray[12][1]="60px";         //宽度
      iArray[12][2]=100;         //最大长度
      iArray[12][3]=0; 
      
      iArray[13]=new Array();
      iArray[13][0]="入司时间";         //列名
      iArray[13][1]="80px";         //宽度
      iArray[13][2]=100;         //最大长度
      iArray[13][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[14]=new Array();
      iArray[14][0]="毕业院校";         //列名
      iArray[14][1]="100px";         //宽度
      iArray[14][2]=100;         //最大长度
      iArray[14][3]=0; 
      
      iArray[15]=new Array();
      iArray[15][0]="所学专业";         //列名
      iArray[15][1]="80px";         //宽度
      iArray[15][2]=100;         //最大长度
      iArray[15][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[16]=new Array();
      iArray[16][0]="毕业时间";         //列名
      iArray[16][1]="80px";         //宽度
      iArray[16][2]=100;         //最大长度
      iArray[16][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[17]=new Array();
      iArray[17][0]="最高学历";            //列名
      iArray[17][1]="60px";         //宽度
      iArray[17][2]=100;         //最大长度
      iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
                               
      iArray[18]=new Array();  
      iArray[18][0]="最高学位";             //列名
      iArray[18][1]="60px";         //宽度
      iArray[18][2]=100;         //最大长度
      iArray[18][3]=0;         
                               
      iArray[19]=new Array();  
      iArray[19][0]="年度考核";                 //列名
      iArray[19][1]="60px";        //宽度
      iArray[19][2]=100;         //最大长度
      iArray[19][3]=0;         
                               
      iArray[20]=new Array();  
      iArray[20][0]="兼职身份";                 //列名
      iArray[20][1]="80px";          //宽度
      iArray[20][2]=100;         //最大长度
      iArray[20][3]=0;         //是否允许录入，0--不能，1--允许
                               
      iArray[21]=new Array();  
      iArray[21][0]="在职状态";                 //列名
      iArray[21][1]="60px";        //宽度
      iArray[21][2]=100;         //最大长度
      iArray[21][3]=0;         //是否允许录入，0--不能，1--允许
                               
      iArray[22]=new Array();  
      iArray[22][0]="从事保险工作年限";                 //列名
      iArray[22][1]="80px";          //宽度
      iArray[22][2]=100;         //最大长度
      iArray[22][3]=0;         //是否允许录入，0--不能，1--允许
                               
      iArray[23]=new Array();  
      iArray[23][0]="参加工作时间";                 //列名
      iArray[23][1]="80px";        //宽度
      iArray[23][2]=100;         //最大长度
      iArray[23][3]=0;         
   
      iArray[24]=new Array();  
      iArray[24][0]="出生日期";                 //列名
      iArray[24][1]="120px";        //宽度
      iArray[24][2]=100;         //最大长度
      iArray[24][3]=0; 
      
      iArray[25]=new Array();  
      iArray[25][0]="证件类型";                 //列名
      iArray[25][1]="0px";        //宽度
      iArray[25][2]=100;         //最大长度
      iArray[25][3]=0; 

      iArray[26]=new Array();  
      iArray[26][0]="证件类型名称";                 //列名
      iArray[26][1]="100px";        //宽度
      iArray[26][2]=100;         //最大长度
      iArray[26][3]=0;       
      
      iArray[27]=new Array();  
      iArray[27][0]="证件号码";                 //列名
      iArray[27][1]="100px";        //宽度
      iArray[27][2]=100;         //最大长度
      iArray[27][3]=0; 
 
      iArray[28]=new Array();  
      iArray[28][0]="手机号码";                 //列名
      iArray[28][1]="100px";        //宽度
      iArray[28][2]=100;         //最大长度
      iArray[28][3]=0; 
           
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.mulLineCount = 0;   
    SetGrid.displayTitle = 1;
    SetGrid.hiddenPlus = 1;
    SetGrid.hiddenSubtraction = 1;
    SetGrid.locked=1;
    SetGrid.canSel=1;
    SetGrid.loadMulLine(iArray); 
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>