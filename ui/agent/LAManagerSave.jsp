<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchGroupSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.db.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAManagerSchema tLAManagerSchema = new LAManagerSchema();
  LAManagerUI tLLAManagerUI = new LAManagerUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();

  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  tLAManagerSchema.setManagerCode(request.getParameter("ManagerCode"));
  tLAManagerSchema.setPManageCom(request.getParameter("PManageCom"));
  tLAManagerSchema.setCManageCom(request.getParameter("CManageCom"));
  tLAManagerSchema.setAgentGroup(request.getParameter("BranchAttr"));
  tLAManagerSchema.setManagerName(request.getParameter("ManagerName"));
  tLAManagerSchema.setSex(request.getParameter("Sex"));
  tLAManagerSchema.setManagerGrade(request.getParameter("ManagerGrade"));
  tLAManagerSchema.setTakeOfficeDate(request.getParameter("TakeOfficeDate"));
  tLAManagerSchema.setPolityVisage(request.getParameter("PolityVisage"));
  tLAManagerSchema.setInsureDate(request.getParameter("InsureDate"));
  tLAManagerSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
  tLAManagerSchema.setSpeciality(request.getParameter("Speciality"));
  tLAManagerSchema.setGraduateDate(request.getParameter("GraduateDate"));
  tLAManagerSchema.setEducation(request.getParameter("Education"));
  tLAManagerSchema.setDegree(request.getParameter("Degree"));
  tLAManagerSchema.setAssess(request.getParameter("Assess"));
  tLAManagerSchema.setMulitId(request.getParameter("MulitId"));
  tLAManagerSchema.setManagerState(request.getParameter("ManagerState"));
  tLAManagerSchema.setWorkYear(request.getParameter("WorkYear"));
  tLAManagerSchema.setWorkDate(request.getParameter("WorkDate"));
  tLAManagerSchema.setBirthday(request.getParameter("Birthday"));
  tLAManagerSchema.setIDNoType(request.getParameter("IDNoType"));
  tLAManagerSchema.setIDNO(request.getParameter("IDNo"));
  tLAManagerSchema.setMobile(request.getParameter("Mobile"));
  tLAManagerSchema.setOperator(request.getParameter("Operator"));
  tLAManagerSchema.setBranchType(request.getParameter("BranchType"));
  tLAManagerSchema.setBranchType2(request.getParameter("BranchType2"));
  tLAManagerSchema.setBranchAttr(request.getParameter("BranchAttr"));
  tLAManagerSchema.setLaborContract(request.getParameter("LaborContract"));
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLAManagerSchema);
  tVData.add(tG);

  try
  {
	  
	  tLLAManagerUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLLAManagerUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    	System.out.println(Content);
    }
  }
  System.out.println(Content);
  //添加各种预处理
%>
<html>

<script language="javascript" type="">
	var FlagStr='<%=FlagStr%>';
	var Content1='<%=Content%>';
parent.fraInterface.afterSubmit(FlagStr,Content1);
</script>
</html>

