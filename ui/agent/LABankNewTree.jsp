<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<html>
<%
//程序名称：LABankTree.jsp
//程序功能：
//创建日期：2004-03-31 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tOperator = tG.Operator;
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String CurrentDate= PubFun.getCurrentDate();  
%>
<script>
	var msql=" 1 and GradeProperty2 = #0# and branchtype=#3# and branchtype2=#01#";
 	var msql2=" 1 and GradeCode <> #F00# and branchtype=#3# and branchtype2=#01#";
 	var tsql_Branch=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and  (EndFlag <> #Y# or EndFlag is null) "  ;
</script>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LABankNewTree.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LABankNewTreeInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LABankNewTreeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateUpdButton.jsp"%>    
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
          <td class=titleImg>
            员工职级信息
          </td>
        </td>
      </tr> 
    </table>
    <Div  id= "divLAAgent1" style= "display: ''">
      
    <table  class= common>
      <TR  class= common>
        <TD class= title> 
          业务员代码
        </TD>
        <TD  class= input> 
          <Input class= 'readonly' readonly name=AgentCode >
        </TD>       
	      <TD  class= title>
          业务员姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= 'readonly' readonly>
        </TD> 
      </tr>
      <tr class=common>       
        <TD class= title>
          所属团队代码 
        </TD>
        <TD class= input>
          <Input name=BranchAttr class='readonly'readonly > 
        </TD>
        <TD class= title>
          所属团队名称 
        </TD>
        <TD class= input>
          <Input class='readonly'readonly  name=ChannelName> 
        </TD>
      </tr> 
      
      <tr class=common>
      	<TD  class= title>
					团队主管
				</TD>
				<TD  class= input>
					<Input class=readOnly readOnly name=OldUpAgent >
				</TD>
        <TD class= title>
          现职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade readOnly class="codeno"><input class=codename name=AgentGradeComName readOnly>
        </TD>
       </tr>  
       
       <tr class=common>
        <TD class= title>
          拟改职级
        </TD>
        <TD class= input>
          <Input name=NewAgentGrade class="codeno" verify="拟改职级|notnull&code:AgentGrade" 
		       ondblclick="return showCodeList('AgentGrade',[this,NewAgentGradeComName],[0,1],null,msql2,'1');" 
		       onkeyup="return showCodeListKey('AgentGrade',[this,NewAgentGradeComName],[0,1],null,msql2,'1');" ><input class=codename name=NewAgentGradeComName>
        </TD>
        <TD  class= title>
          调整日期 
        </TD>
        <TD  class= input> 
          <Input name=AdjustDate class='coolDatePicker' dateFormat='short' verify="调整日期|notnull&Date " elementtype=nacessary > 
        </TD> 
       </tr>  
       
       <tr class=common>
        <TD  class= title>
          操作员代码 
      </TD>
      
      <TD  class= input> 
        <Input name=Operator class='readonly' readonly > 
      </TD> 
	  	 <TD class= title>
      		录入时间
      </TD>
      <TD class= input>
      	<Input name=MakeDate class='readonly' readonly > 
      </TD>
				<!--TD  class= title>
					团队主管
				</TD>
				<TD  class= input>
					<Input class=readOnly readOnly name=OldUpAgent >
				</TD>
			</TR>		
      <TR class=common> 
      <TD  class= title>
          操作员代码 
      </TD>
      <TD  class= input> 
        <Input name=Operator class='readonly' readonly > 
      </TD>
      <TD class= title>
      		录入时间
      </TD>
      <TD class= input>
      	<Input name=MakeDate class='readonly' readonly > 
      </TD-->
      </TR>
     </table>
    </Div>
    
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgentGroup1);">
          <td class=titleImg>
            团队信息
          </td>
        </td>
      </tr> 
    </table>
    <Div  id= "divLAAgentGroup1" style= "display: ''">
    	<table  class= common>
    		<TR  class= common>
					<TD  class= title>
						销售团队代码
					</TD>
					<TD  class= input>
						<Input class=code   name=BranchCode ondblclick="return showCodeList('branchattr',[this,BranchName],[0,1],null,tsql_Branch,1);" onkeyup="return showCodeListKey('branchattr',[this,BranchName],[0,1],null,tsql_Branch,1);" >
					</TD>
					<TD  class= title>
						销售团队名称
					</TD>
					<TD  class= input>
						<Input class=readOnly readOnly name=BranchName >
					</TD>
				</TR>
				<TR  class= common>	
					<TD  class= title>
						管理机构
					</TD>
					<TD  class= input>
						<Input class=readOnly readOnly name=ManageCom >
					</TD>
					<TD  class= title>
						级别
					</TD>
					<TD  class= input>
						<Input class=readOnly readOnly name=BranchLevel >
					</TD>
				</TR>
				<TR  class= common>	
					<TD  class= title>
						团队主管
					</TD>
					<TD  class= input>
						<Input class=readOnly readOnly name=UpAgent >
					</TD>
				</TR>		
    	</table>
  	</Div>
  	 <p><font color="red">注：请先“查询”后进行“修改” 调整日期必须是从某个月的1号开始!</font></p>
  	 <p><font color="red">注："调整日期"即为调整后职级的"生效日期".请正确填写该日期</font></p>
  	 <p><font color="red">注：只有跨层级调整人员职级时，才需要选择团队信息</font></p>
    <input type=hidden name=BranchType>
    <input type=hidden name=BranchType2>
    <input type=hidden name=AgentGroup>
    <input type=hidden name=BranchSeries>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

        