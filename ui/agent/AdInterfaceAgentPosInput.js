
var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//修改人：杨阳  时间：2014-12-16
function checkAgentCode()
{
//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
   var sql=" select agentcode  from laagent   where  groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
    changeAgent();
    
}
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
  {
    return false;
  }

  if(beforeSubmit() == false)
  {
  	return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

//  showSubmitFrame(mDebug);
  fm.submit(); //提交
  fm.save.disabled=true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.reset();
  initForm();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    //AgentGrid.clearData("AgentGrid");
    //fm.all('AdInterfaceBranchCode').value = '';
    //BranchChange();
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//取消按钮对应操作
function cancelForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
  var tYearMonth = document.fm.IndexCalNo.value;
  var tBranchattr = fm.all("OldAgentGroup").value;
 // alert(tYearMonth);
  // 验证录入的考核年月必须是数字
  if(!isInteger(tYearMonth))
  {
  	alert("考核年月必须录数字！");
  	return false;
  }
  //alert(tYearMonth.substr(0,4)+"-"+tYearMonth.substr(4,6)+"-01")
  // 验证考核年月是否合法
  if(!isDate(tYearMonth.substr(0,4)+"-"+tYearMonth.substr(4,6)+"-01"))
  {
  	alert("请录入一个有效年月!");
  	return false;
  }
  //调整前后职级不能一致
  //alert(fm.all("NewAgentGrade").value);
 // alert(fm.all("OldAgentGrade").value);
  if(fm.all("NewAgentGrade").value==fm.all("OldAgentGrade").value)
  {
  	alert("当前职级不能与目标职级相同!");
  	return false;
   }
   //限制跨两个系列升级
   var SQL = "select gradeproperty2 from laagentgrade where gradecode='"+fm.all("NewAgentGrade").value+"' and branchtype='5'";//新职级系列
   var SQL1 = "select gradeproperty2 from laagentgrade where gradecode='"+fm.all("OldAgentGrade").value+"' and branchtype='5'";//旧职级系列
   var strQueryResult  = easyQueryVer3(SQL, 1, 1, 1);
    var strQueryResult1  = easyQueryVer3(SQL1, 1, 1, 1);
    if((!strQueryResult)||(!strQueryResult1))
    {
       alert("职级描述出错！");
       return false;
    }
   var arrResult = decodeEasyQueryResult(strQueryResult);
   var arrResult1 = decodeEasyQueryResult(strQueryResult1);  
   var oldseries = arrResult1[0][0];
   var newseries = arrResult[0][0];
   var newseries = parseInt(newseries);
   var oldseries = parseInt(oldseries);
   if(fm.all("NewAgentGrade").value == 'B01')  
   {
    newseries = 0; 
   }
    if(fm.all("OldAgentGrade").value=='B01')
   {
    oldseries = 0;
   }
  // alert(newseries);
  // alert(oldseries);
    if(newseries==1&&oldseries==0){
    var tBranchSQL ="select BranchManager from labranchgroup where branchtype ='5' and branchtype2='01' and branchattr ='"+tBranchattr+"'";//查询业务员所在团队主管
    var strBranchResult = easyQueryVer3(tBranchSQL,1,1,1);
    var arr = decodeEasyQueryResult(strBranchResult);
    if(arr[0][0]!=null && arr[0][0]!=""){
    	alert("业务员'"+fm.all("GroupAgentCode").value+"'所在的团队有主管,请先进行团队调整到无主管团队，再进行人员调岗！");
    	return false;
      }
    } 
//   if((newseries==0&&oldseries==2)||(newseries==2&&oldseries==0))
//   {
//     alert("不能跨两个系列升降级，可以通过两次一级操作！");
//     fm.all('NewAgentGrade').value = '';
//     fm.all('NewAgentGradeName').value = '';
//     return false;
//   }
   var tNewSeries = "";
   var tOldSeries = "";   
   var tNewGrade = fm.all("NewAgentGrade").value ;
   var tOldGrade = fm.all("OldAgentGrade").value ;   
	return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{

}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  //alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}



function CreateBranch()
{
	 //mOperate="QUERY||MAIN";
    var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
    showInfo=window.open("./LABranchGroupInputHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

function changeGroup()
{
   var tBranchAttr=fm.all('NewAgentGroup').value;
	 var strSQL="";	
      strSQL = "select Name,BranchManager,BranchManagerName from LABranchGroup where 1=1 and BranchType = '"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"'"
   						+" and EndFlag <> 'Y' and branchAttr = '"+tBranchAttr+"' and (state<>'1' or state is null)";
   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert("系统中没有机构代码为"+tBranchAttr+"的机构!");
   	return false;
   }
   var arrResult = decodeEasyQueryResult(strQueryResult);
   fm.all('NewAgentGroupName').value=arrResult[0][0];
   fm.all('BranchManager').value=arrResult[0][1];
   fm.all('BranchManagerName').value=arrResult[0][2];
}

function changeAgent()
{
	var strSQL = "select a.name,(select branchattr from labranchgroup where agentgroup=a.agentgroup),"
	 + " (select name from labranchgroup where agentgroup=a.agentgroup),"
	 + " (select agentgrade from latree where agentcode=a.agentcode),a.agentgroup,a.managecom "
	 + " from laagent a "
	 + " where a.agentcode='"+fm.all('AgentCode').value+"' and a.agentstate<='02' "
	 + " and a.branchtype='5' and a.branchtype2='01' "
	 //暂时去掉筹备人员，筹备人员不参与职级调整
//	 + " and (a.noworkflag='N' or a.noworkflag is null )";
  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   	if (!strQueryResult)
   	{
   		alert("代码为"+fm.all('GroupAgentCode').value+"营销员不存在或不符合人员调岗条件!");
   		fm.all('AgentCode').value='';
   		fm.all('Name').value='';
   		fm.all('OldAgentGroup').value='';
   		fm.all('OldAgentGroupName').value='';
   		fm.all('OldAgentGrade').value='';
   		fm.all('ManageCom').value='';
   		fm.all('AgentGroup').value='';  		
   		return false;
   	}
   	var arrResult = decodeEasyQueryResult(strQueryResult);
   	if(arrResult[0][0])
   	fm.all('Name').value=arrResult[0][0];
   	fm.all('OldAgentGroup').value=arrResult[0][1];
   	fm.all('OldAgentGroupName').value=arrResult[0][2];
   	fm.all('OldAgentGrade').value=arrResult[0][3];
   	fm.all('ManageCom').value=arrResult[0][5];
   	fm.all('AgentGroup').value=arrResult[0][4];
//   	alert(fm.all('ManageCom').value);
//   	alert(fm.all('AgentGroup').value);
   	 if(fm.OldAgentGrade.value!=''&&fm.NewAgentGrade.value!=''){
  	var sql0 = "select gradeproperty2 from laagentgrade where branchtype='5' and gradecode='";
  	strSQL = sql0+fm.OldAgentGrade.value+"'";
  	var oldGP2 = easyExecSql(strSQL);
  	if(fm.OldAgentGrade.value=='B01')
  		oldGP2='0';
  	strSQL = sql0+fm.NewAgentGrade.value+"'";
  	var newGP2 = easyExecSql(strSQL);
  	if(fm.NewAgentGrade.value=='B01')
  		newGP2='0';
  		fm.all('OldSeries').value=oldGP2;
  		fm.all('NewSeries').value=newGP2;
  	if(oldGP2<=newGP2)
  	{
  		fm.TransferType.value = '1';  //升级
  	}
    else
    {
    fm.TransferType.value='0';//jiangji
    }
  }
}
  
function afterCodeSelect( cCodeName, Field )
{
	 
	  
	if(cCodeName =="laagentname"){
   	var tAgentCode=fm.all('AgentCode').value;
	 	var strSQL="";		 
    strSQL = "select a.Name,c.BranchAttr,c.Name,b.AgentGrade,a.ManageCom,a.AgentGroup from LAAgent a,LATree b,LABranchGroup c where 1=1 and a.BranchType = '"+fm.all('BranchType').value+"' and a.branchtype2='"+fm.all('BranchType2').value+"'"
   						+" and a.agentcode=b.agentcode and b.agentgroup=c.agentgroup and a.agentcode='"+tAgentCode+"'";
   	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   	if (!strQueryResult)
   	{
   		alert("系统中没有代码为"+tAgentCode+"营销员!");
   		fm.all('AgentCode').value='';
   		fm.all('Name').value='';
   		fm.all('OldAgentGroup').value='';
   		fm.all('OldAgentGroupName').value='';
   		fm.all('OldAgentGrade').value='';
   		fm.all('ManageCom').value='';
   		fm.all('AgentGroup').value='';
   		
   		return false;
   	}
   	var arrResult = decodeEasyQueryResult(strQueryResult);
   	fm.all('Name').value=arrResult[0][0];
   	fm.all('OldAgentGroup').value=arrResult[0][1];
   	fm.all('OldAgentGroupName').value=arrResult[0][2];
   	fm.all('OldAgentGrade').value=arrResult[0][3];
   	fm.all('ManageCom').value=arrResult[0][4];
   	fm.all('AgentGroup').value=arrResult[0][5];
  }
  if(fm.OldAgentGrade.value!=''&&fm.NewAgentGrade.value!=''){
  	var sql0 = "select gradeproperty2 from laagentgrade where branchtype='5' and gradecode='";
  	strSQL = sql0+fm.OldAgentGrade.value+"'";
  	var oldGP2 = easyExecSql(strSQL);
  	if(fm.OldAgentGrade.value=='B01')
  		oldGP2='0';
  	strSQL = sql0+fm.NewAgentGrade.value+"'";
  	var newGP2 = easyExecSql(strSQL);
  	if(fm.NewAgentGrade.value=='B01')
  		newGP2='0';
  		fm.all('OldSeries').value=oldGP2;
  		fm.all('NewSeries').value=newGP2;
  	if(oldGP2<=newGP2){
  		fm.TransferType.value = '1';  //升级
  //		titleGroup1.style.display='';
  //		titleGroupName1.style.display='';
  //		titleManager1.style.display='';
  //		titleManagerName1.style.display='';
 // 		titleGroup2.style.display='none';
 // 		titleGroupName2.style.display='none';
 // 		titleManager2.style.display='none';
//  		titleManagerName2.style.display='none';
  	}
  	else{
 // 		alert("业务员降序列，请填入归属销售单位与当前销售单位相同！")
  		fm.TransferType.value = '0';  //降级
 // 		titleGroup1.style.display='none';
 // 		titleGroupName1.style.display='none';
 // 		titleManager1.style.display='none';
 // 		titleManagerName1.style.display='none';
 // 		titleGroup2.style.display='';
 // 		titleGroupName2.style.display='';
 // 		titleManager2.style.display='';
 // 		titleManagerName2.style.display='';
  	}
  }
}