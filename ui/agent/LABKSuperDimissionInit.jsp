<%
//程序名称：LADimissionInit.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     String gToday = PubFun.getCurrentDate(); //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{ 
  try
  {
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value='';
    
    
    fm.all('ManageCom').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('BranchAttr').value = '';
    fm.all('BranchName').value = '';
    fm.all('DepartTimes').value = '';
    fm.all('DepartRsn').value = '';
    fm.all('ApplyDate').value = ''; 
    fm.all('DepartDate').value =  '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';     
    fm.all('DepartState').value = '';  
   // alert(fm.all('BranchType2').value);
  }
  catch(ex)
  {
    alert("在LADimissionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  
  }
  catch(ex)
  {
    alert("在LADimissionInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initAgentWageGrid();
    initContGrid();
    initRenewalContGrid();
  }
  catch(re)
  {
    alert("在LADimissionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentWageGrid
 ************************************************************
 */
function initAgentWageGrid()
{
	var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]= "代理机构编码";         //列名
    iArray[1][1]="100px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]= "代理机构名称";         //列名
    iArray[2][1]="120px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

     
    
    
     
    AgentWageGrid = new MulLineEnter( "fm" , "AgentWageGrid" ); 

    //这些属性必须在loadMulLine前
    AgentWageGrid.mulLineCount = 0;   
    AgentWageGrid.displayTitle = 1;
  	AgentWageGrid.hiddenPlus = 1;
  	AgentWageGrid.hiddenSubtraction = 1;
    AgentWageGrid.locked=1;
    AgentWageGrid.canSel=0;
    AgentWageGrid.canChk=0;
   
    AgentWageGrid.loadMulLine(iArray); 
    
  }
  catch(ex)
  {
    
    alert("初始化AgentWage1Grid时出错："+ ex);
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentWageGrid
 ************************************************************
 */
function initContGrid()
{
	var iArray = new Array();
  
  try
  {
  
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]= "保单号";         //列名
    iArray[1][1]="70px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]= "印刷号";         //列名
    iArray[2][1]="70px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;

    iArray[3]=new Array();
    iArray[3][0]= "签单日期";         //列名
    iArray[3][1]="70px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="回执回销日期";         //列名
    iArray[4][1]="70px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 

    //这些属性必须在loadMulLine前
    ContGrid.mulLineCount = 0;   
    ContGrid.displayTitle = 1;
  	ContGrid.hiddenPlus = 1;
  	ContGrid.hiddenSubtraction = 1;
    ContGrid.locked=1;
    ContGrid.canSel=0;
    ContGrid.canChk=0;
    ContGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert("初始化AgentWageGrid时出错："+ ex);
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentWageGrid
 ************************************************************
 */
 function initRenewalContGrid()
{
	var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]= "保单号";         //列名
    iArray[1][1]="100px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]= "印刷号";         //列名
    iArray[2][1]="120px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]= "营销员编码";         //列名
    iArray[3][1]="120px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    RenewalContGrid = new MulLineEnter( "fm" , "RenewalContGrid" ); 

    //这些属性必须在loadMulLine前
    RenewalContGrid.mulLineCount = 0;   
    RenewalContGrid.displayTitle = 1;
  	RenewalContGrid.hiddenPlus = 1;
  	RenewalContGrid.hiddenSubtraction = 1;
    RenewalContGrid.locked=1;
    RenewalContGrid.canSel=0;
    RenewalContGrid.canChk=0;
   
    RenewalContGrid.loadMulLine(iArray); 
    
  }
  catch(ex)
  {
    
    alert("初始化RenewalContGrid时出错："+ ex);
  }
}

</script>