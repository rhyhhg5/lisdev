<%
//程序名称：LAAgentInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('AgentCode').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('SexName').value = '';
    fm.all('Birthday').value = '';
    fm.all('NativePlace').value = '';
    fm.all('NativePlaceName').value = '';
    fm.all('Nationality').value = '';
    fm.all('NationalityName').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('IDNo').value = '';
    fm.all('PolityVisage').value = '';
    fm.all('PolityVisageName').value = '';
    fm.all('Degree').value = '';
    fm.all('DegreeName').value = '';
    fm.all('GraduateSchool').value = '';
    fm.all('Speciality').value = '';
    fm.all('PostTitle').value = '';
    
    fm.all('PostTitleName').value = '';
    fm.all('OldCom').value = '';
    fm.all('OldOccupation').value = '';
    fm.all('OldOccupationName').value = '';
    fm.all('HeadShip').value = '';
    //fm.all('QuafNo').value = '';
    //fm.all('QuafEndDate').value = '';
    //fm.all('TrainPeriods').value = '';
    fm.all('EmployDate').value = '';
    fm.all('RgtAddress').value = '';
    
    fm.all('RgtAddressName').value = '';
   // fm.all('BankCode').value = '';
    //fm.all('BankAccNo').value = '';
    fm.all('Remark').value = '';
    fm.all('hasExp').value = ''; 
    fm.all('ExpYear').value = '';
    fm.all('salequaf').value='N';
    fm.all('hasQuafName').value='否';
    //fm.all('salequaf').disabled=true;
    //fm.all('hiddenSaleQuaf').value = 'Y';
    fm.all('hiddenQuafNo').value = '';
    fm.all('hiddenQuafDate').value = '';
    fm.all('QuafNo').value = '';
    fm.all('Quafstartdate').value = '';  
    fm.all('Operator').value = '';
    fm.all('RejectReason').value='';
    fm.all('OutWorkDate').value='';
    //行政信息
    fm.all('BranchAttr').value = '';
    fm.all('UpAgent').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('AgentGradeName').value = '';
    fm.all('ManageCom').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('AgentState').value ='01';
    fm.all('AgentStateName').value ='在职';
    fm.all('BranchType').value = '<%=BranchType%>';
   
    fm.all('BranchType2').value = '<%=BranchType2%>';
  //  showOneCodeNametoAfter('agentcomtype','BranchType2');   

    //fm.all('InsideGroupFlagName').value = '';

  }
  catch(ex)
  {
    alert("在LAAgentInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
//    setOption("t_sex","0=男&1=女&2=不详");
//    setOption("sex","0=男&1=女&2=不详");
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");
  }
  catch(ex)
  {
    alert("在LAAgentInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
   // initWarrantorGrid();
  }
  catch(re)
  {
    alert("LAAgentInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
