<%
//程序名称：LADimissionQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String tTitleAgent="";
  String tTitleWage = "";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
    tTitleWage  = "佣金";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
    tTitleWage  = "薪资";
  }
 
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./LABKDimissionBackQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LABKDimissionBackQueryInit.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>

<title>离职管理 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LADimissionQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLADimission1);">
    </IMG>
      <td class=titleImg> 查询条件 </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLADimission1" style= "display: ''">
     <table  class= common>
       <tr  class= common> 
       	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7" 
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
          ><Input name=ManageComName class="codename"> 
          </TD> 
        <td  class= title> 
		  业务员编码 
		</td>
        <td  class= input> 
		  <input class= common name=AgentCode type=hidden> 
		  <input class= common name=groupAgentCode > 
		</td>
		 </tr>
      <tr  class= common>    
        <td  class= title> 
		  业务员姓名 
		</td>
        <td  class= input> 
		  <input class=common  name=AgentName > 
		</td>
     
        <td  class= title> 
		  离职原因 
		</td>
        <td  class= input> 
		  <input name=DepartRsn class='codeno' 
		   ondblclick="return showCodeList('DepartRsn',[this,DepartRsnName],[0,1]);" 
		   onkeyup="return showCodeListKey('DepartRsn',[this,DepartRsnName],[0,1]);" 
		  ><Input name=DepartRsnName class="codename"> 
		</td>
       </tr>
      <tr  class= common> 
         <td  class= title> 
		  离职登记日期
		</td>
        <td  class= input> 
		  <Input class='coolDatePicker' name=ApplyDate dateFormat='short'>
		</td>
        <td  class= title> 
		  离职确认日期 
		</td>
        <td  class= input> 
		  <Input class='coolDatePicker' name=DepartDate dateFormat='short' >
		</td>
     </tr>
      <tr  class= common> 
      <td  class= title> 
		  销售机构代码
		</td>
        <td  class= input> 
		  <Input class=common name=BranchAttr >
		</td>
    <td  class= title> 
    离职状态
		</td>
    <td  class= input> 
    	<input name=AgentState class="codeno" name="AgentState" verify="离职状态" 
         CodeData="0|^03|离职登记|^06|离职已确认"
         ondblClick="showCodeListEx('AgentStateList',[this,AgentStateName],[0,1]);"
         onkeyup="showCodeListKeyEx('AgentStateList',[this,AgentStateName],[0,1]);"
         ><Input class=codename name=AgentStateName readOnly >
    </td>
    
   
    
    
     </tr>
    </table> 
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          
          <input type=hidden name=tManageCom value= '<%=tGI.ManageCom%>'>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton"> 
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton"> 						
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divDimissionGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanDimissionGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton"> 				
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
