//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLAagentidnoInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//提交之前基本校验
function beforeSubmit()
{
	var managecom=new Date(fm.all('ManageCom ').value);
    if (managecom==null||managecom=='')
   {
   	alert("管理机构为必填项，不能为空！");
   	return false;
   }
}

//查询之前先检查数据
function check()
{
 if(fm.all("ManageCom").value==null||fm.all("ManageCom").value==''){
	   alert("管理机构为必填项，不能为空！");
	   return false;
	}
	return true;
}
//页面查询
function easyQueryClick() {
	
	if(fm.all('Managecom').value==''){
		alert("请输入管理机构！");
		return false;
	}
	var strSql = "";
	var BranchTypeCode = fm.all('BranchTypeCode').value
	fm.all('branchtype').value = BranchTypeCode.substr(0, 1);
	fm.all('branchtype2').value = BranchTypeCode.substr(1, 3);
    
	if (fm.all('AgentState').value == '1') {
     
		strSql = " select a.managecom,b.codename||c.codename,a.Name,case when a.agentstate<'06' then '在职'  when a.agentstate>='06' then '离职' end,"
				+ " a.AgentCode,a.idno "
				+ "from laagent a ,ldcode b, ldcode c where 1=1 and a.AgentState<'06' and b.codetype = 'branchtype' and c.codetype = 'branchtype2' "
				+ "and (c.code = '01' or (b.code = '1' and c.code in ('02', '03')) or(b.code = '2' and c.code in( '02','04'))) and  replace(b.code||c.code,' ','')in(replace(a.branchtype||a.branchtype2,' ',''))"
				+ getWherePart("ManageCom", "ManageCom","like")
				+ getWherePart("BranchType", "BranchType")
				+ getWherePart("BranchType2", "BranchType2")
				+ getWherePart("Name", "Name")
				+ getWherePart("Idno", "Idno")
				+ getWherePart("AgentCode", "AgentCode")
				+ " order by AgentCode fetch first 1000 rows only";
	} else if (fm.all('AgentState').value == '2') {
		strSql = " select a.managecom,b.codename||c.codename,a.Name,case when a.agentstate<'06' then '在职'  when a.agentstate>='06' then '离职' end ,"
				+ "a.AgentCode,a.idno "
				+ "from laagent a ,ldcode b, ldcode c where 1=1 and a.AgentState>='06' and b.codetype = 'branchtype' and c.codetype = 'branchtype2' "
				+ "and (c.code = '01' or (b.code = '1' and c.code in ('02', '03')) or(b.code = '2' and c.code in( '02','04'))) and  replace(b.code||c.code,' ','')in(replace(a.branchtype||a.branchtype2,' ',''))"
				+ getWherePart("ManageCom", "ManageCom",'like')
				+ getWherePart("BranchType", "BranchType")
				+ getWherePart("BranchType2", "BranchType2")
				+ getWherePart("Name", "Name")
				+ getWherePart("Idno", "Idno")
				+ getWherePart("AgentCode", "AgentCode")
				+ " order by AgentCode fetch first 1000 rows only";
	} else {
		strSql = " select a.managecom,b.codename||c.codename,a.Name,case when a.agentstate<'06' then '在职'  when a.agentstate>='06' then '离职' end ,"
				+ "a.AgentCode,a.idno "
				+ "from laagent a ,ldcode b, ldcode c where 1=1 and b.codetype = 'branchtype' and c.codetype = 'branchtype2' "
				+ "and (c.code = '01' or (b.code = '1' and c.code in ('02', '03')) or(b.code = '2' and c.code in( '02','04'))) and  replace(b.code||c.code,' ','')in(replace(a.branchtype||a.branchtype2,' ',''))"
				+ getWherePart("ManageCom", "ManageCom",'like')
				+ getWherePart("BranchType", "BranchType")
				+ getWherePart("BranchType2", "BranchType2")
				+ getWherePart("Name", "Name")
				+ getWherePart("Idno", "Idno")
				+ getWherePart("AgentCode", "AgentCode")
				+ " order by AgentCode fetch first 1000 rows only";
	}
	turnPage.queryModal(strSql, PolGrid);
	showCodeName();
}
//Click事件，当点击“修改”图片时触发该函数
function updateclick()
{  // alert("**********");
	fm.fmAction.value = "UPDATE";
	//alert("aaaa");
	if (confirm("您确实想修改该记录吗?"))
	{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//alert("222222222");
	fm.action = "./LAagentidnoSave.jsp";
	//alert("@@@@@@@@");
	fm.submit(); //提交
	}
	else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}           

function submitForm()
{
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.action = "./LAagentidnoSave.jsp";
  fm.submit(); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


