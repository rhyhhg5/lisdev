//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var strAgentGrade;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (mOperate!='DELETE||MAIN')
  {
    if (!beforeSubmit())
      return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //fm.hideOperate.value=mOperate;
  //if (fm.hideOperate.value=="")
  //{
    //alert("操作控制数据丢失！");
 // }
//  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    fm.all('AgentCode').value = "";
    fm.all('Name').value = "";
    
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LATreeEditInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	  
  var tAgentCode = fm.all('AgentCode').value;
  //var tName = fm.all('Name').value;
  if ((tAgentCode == null)||(trim(tAgentCode) == "")) 
  {
     alert("请确定代理人编码！");
     return false;	
  }
   
  if( verifyInput() == false ) return false;  
  
//  var tAgentGrade = fm.AgentGrade.value;
//  var tOldAgentGrade = fm.OldAgentGrade.value;
//  if (tAgentGrade > tOldAgentGrade)
//  {
//     alert("职级调整只能降级！");
//     return false;
//  }
  
  if (!checkRearAgent())
    return false;
    
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,500,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function changeIntroAgency()
{	
   if (getWherePart('IntroAgency')=='')
     return false;
   if (fm.all('IntroAgency').value==fm.all('AgentCode').value)
   {
   	alert('与原代理人编码相同！');
   	fm.all('IntroAgency').value = '';
   	return false;
   }  
   var strSQL = "";
   strSQL = "select AgentCode,ManageCom, AgentGroup from LAAgent where 1=1 "
           + "and (AgentState is null or AgentState < '06') "
           + getWherePart('AgentCode','IntroAgency');		
   //alert(strSQL);
   var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该代理人！');
   	fm.all('IntroAgency').value = '';
   	return false;
   }  
   return true;
}




//校验育成代理人
function checkRearAgent()
{
   var strSQL = "",str = "";
   var strQueryResult = null;
   
   strSQL = "select AgentCode from LAAgent where (AgentState < '06' or AgentState is not null) "
   //育成代理人
   if (trim(fm.all('RearAgent').value)=='')
     return true;
   if (fm.all('RearAgent').value==fm.all('AgentCode').value)
   {
      alert('与原代理人编码相同！');
      fm.all('RearAgent').value = '';
      return false;	
   }
   str = getWherePart('AgentCode','RearAgent');
            
   //alert(strSQL+str);
   strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该育成代理人！');
   	fm.all('RearAgent').value = '';
   	return false;
   }  
   //增部代理人
   if (trim(fm.all('RearDepartAgent').value)=='')
     return true;
   if (trim(fm.all('RearDepartAgent').value)==trim(fm.all('AgentCode').value))
   {
   	alert('与原代理人编码相同！');
   	fm.all('RearDepartAgent').value = '';
   	return false;
   }  
   str = getWherePart('AgentCode','RearDepartAgent');
            
   //alert(strSQL+str);
   strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该增部代理人！');
   	fm.all('RearDepartAgent').value = '';
   	return false;
   } 
   //增督导代理人
   if (trim(fm.all('RearSuperintAgent').value)=='')
     return true;
   if(fm.all('RearSuperintAgent').value==fm.all('AgentCode').value)
   {
     alert('与原代理人编码相同！');
   	fm.all('RearSuperintAgent').value = '';
   	return false;	
   }
   str = getWherePart('AgentCode','RearSuperintAgent');
            
   //alert(strSQL+str);
   strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('不存在该育成督导代理人！');
   	fm.all('RearSuperintAgent').value = '';
   	return false;
   }
   return true;
}

function agentConfirm()
{
   var tAgentCode = fm.all('AgentCode').value;
   var tName = fm.all('Name').value;
   if (((tAgentCode == null)||(trim(tAgentCode) == ""))
       &&((tName==null)||(trim(tName) == "")))  
   {
     alert("请输入代理人代码或名称！");
     return false;	
   }
   // 书写SQL语句
   var tReturn = parseManageComLimitlike();
   var strSQL = "";
   strSQL = "select agentCode,Name from LAAgent where agentState <'06' "
      + tReturn
	  + getWherePart('AgentCode')
	  + getWherePart('Name')
	  + getWherePart('BranchType');
//   alert(strSQL);   
   var strQueryResult = easyQueryVer3(strSQL,1,1,1);
   if(!strQueryResult)
   {
     alert('不存在该代理人！')
     fm.all('AgentCode').value = '';
     fm.all('Name').value = '';
     return false;	
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   tAgentCode = trim(arr[0][0]);
   tName = trim(arr[0][1]);
   fm.all('AgentCode').value = tAgentCode;
   fm.all('Name').value = tName;
   
   //查询设置行政信息
   strSQL = "select a.agentCode,"
                  +"a.ManageCom,"
                  +"c.BranchManager,"
                  +"b.IntroAgency,"
                  +"b.AgentSeries,"
                  +"b.AgentGrade,"
                  +"(select branchattr from labranchgroup where a.BranchCode = AgentGroup and (state<>'1' or state is null)) bt,"
	          +"b.AscriptSeries,"
	          +"c.BranchLevel,"
	          +"c.upBranch,"
	          +"c.BranchManagerName,"
	          +"c.upBranchAttr,"
	          +"a.BranchCode,b.BranchCode "
	    +"from LAAgent a,LATree b,LABranchGroup c "
	    +"where a.AgentCode = b.AgentCode "
	     +"and a.AgentGroup = c.AgentGroup "
	     +"and a.AgentCode='"+tAgentCode+"'"; 
   
   strQueryResult = easyQueryVer3(strSQL,1,1,1);
   if(!strQueryResult)
   {
     alert('查询该代理人的行政信息失败！')
     return false;	
   }
   var arrResult = decodeEasyQueryResult(strQueryResult);
   
   fm.all('BranchAttr').value = arrResult[0][6];
   fm.all('ManageCom').value = arrResult[0][1];
   fm.all('IntroAgency1').value = arrResult[0][3];
   fm.all('IntroAgency').value = arrResult[0][3];
   fm.all('AgentGrade1').value = arrResult[0][5];   
   fm.all('AgentGrade').value = arrResult[0][5];
   fm.all('OldAgentGrade').value = arrResult[0][5];
   fm.all('BranchCode').value = arrResult[0][13];
   if (arrResult[0][7]!=null && trim(arrResult[0][7])!='')
   {                	
     if (arrResult[0][7].indexOf(":")!=-1)
     {
        var arrRear = arrResult[0][7].split(":");
        //alert(arrRear.length);
        fm.all('RearAgent1').value = arrRear.length>0?arrRear[0]:'';
        fm.all('RearDepartAgent1').value = arrRear.length>1?arrRear[1]:'';
        fm.all('RearSuperintAgent1').value = arrRear.length>2?arrRear[2]:'';
        fm.all('RearAreaSuperintAgent1').value = arrRear.length>3?arrRear[3]:'';
        fm.all('RearAgent').value = arrRear.length>0?arrRear[0]:'';
        fm.all('RearDepartAgent').value = arrRear.length>1?arrRear[1]:'';
        fm.all('RearSuperintAgent').value = arrRear.length>2?arrRear[2]:'';
        fm.all('RearAreaSuperintAgent').value = arrRear.length>3?arrRear[3]:'';
     }
     else
     {
       fm.all('RearAgent1').value = arrResult[0][7];
       fm.all('RearAgent').value = arrResult[0][7];
       fm.all('RearDepartAgent1').value = '';
       fm.all('RearSuperintAgent1').value = '';
       fm.all('RearAreaSuperintAgent1').value = '';        
       fm.all('RearDepartAgent').value = '';
       fm.all('RearSuperintAgent').value = '';
       fm.all('RearAreaSuperintAgent').value = '';       
     }
   }
//显式机构代码
//2-BranchManager 3-IntroAgency 4-AgentSeries 5-AgentGrade 6-BranchAttr(所属组的显式代码) 
//7-AscriptSeries 8-BranchLevel 9-upBranch 10-BranchManagerName 11-upBranchAttr 
//12-BranchCode(所属组的隐式代码)
   if (arrResult[0][2]!=null)//管理人员=代理人代码
   {
     //if (arrResult[0][5] <= 'A03') 
       //fm.all('UpAgent').value = arrResult[0][2]; //组经理 
     fm.all('GroupManagerName').value = arrResult[0][10];                 
   }
   //确定部经理 arrResult[0][9]:上级机构
   if ((arrResult[0][9]!=null)&&(trim(arrResult[0][9])!=''))
   {
      strSQL = "select BranchManager,BranchManagerName from LABranchGroup where 1=1 "
             + " and EndFlag <> 'Y' and AgentGroup = '"+arrResult[0][9]+"' and (state<>'1' or state is null)";		
      //alert('11--'+strSQL+'  '+arrResult[0][82]);
      strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
     if (strQueryResult)
     {
        var arr = decodeEasyQueryResult(strQueryResult);
        //alert(arrResult[0][2]+":"+arrResult[0][0]);
        //if (trim(arrResult[0][2])==tAgentCode)
          //fm.all('UpAgent').value = trim(arr[0][0]);
        fm.all('DepManagerName').value = trim(arr[0][1]); 
     }
   }
   if (arrResult[0][5] >= 'A06')
     fm.all('DepManagerName').value = tName; //部经理是他本身

   fm.all('AdjustDate').value = "";
//   fm.all('strAgentGrade').value = "1 and codealias = #"+fm.BranchType.value+"# "
//                                  +"and code <= #"+fm.AgentGrade1.value+"#"; 
   return true;
}

function clearData()
{
    fm.all('AgentCode').value = '';  
    fm.all('Name').value = '';      
    //行政信息
    fm.all('BranchAttr').value = '';
    fm.all('GroupManagerName').value = '';
    fm.all('DepManagerName').value = '';
    fm.all('IntroAgency').value = '';
    fm.all('ManageCom').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('AgentGrade1').value = '';
    fm.all('RearAgent').value = '';
    fm.all('RearDepartAgent').value = '';
    fm.all('RearSuperintAgent').value = '';
    fm.all('RearAreaSuperintAgent').value = '';
    fm.all('AdjustDate').value = '';
    fm.all('BranchCode').value = '';
}

function bGroupClick()
{
   window.open("../agentbranch/LABranchGroupInput.html");
}
function bChangeGroupClick()
{
   window.open("../agentbranch/ChangeBranchInput.html");
}
function bAdjustAgentClick()
{
   window.open("../agentbranch/AdjustAgentInput.html");
}
function bSetManagerClick()
{
   window.open("../agentbranch/LABranchManager.html");
}