//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var mDepartDate="";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  if (mOperate==""){
  	mOperate="INSERT||MAIN";
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  mOperate = "";
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
//下面增加相应的代码
 
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
    
  showInfo=window.open("./LABKSuperDimissionQueryhtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}  


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	  
  if(!verifyInput())
  {
      return false;	
  }
 
  if (mDepartDate!=null&&mDepartDate!='')
  {
  		if (fm.all('DepartDate').value !=mDepartDate)
  		{
			  	alert("离职日期不可修改！");
			  	fm.all('DepartDate').focus();
			  	return false;
  		}
   }	
 

 if ((fm.all('ApplyDate').value != '')||(fm.all('ApplyDate').value!=null))
 {
 	//alert(fm.all('ApplyDate').value+fm.all('DepartDate').value);
  	if(fm.all("DepartDate").value<fm.all('ApplyDate').value)
  	{
  		alert("离职申请日期不能大于离职日期");
  		return false;
  	}
  }
  var rowNum1=AgentWageGrid.mulLineCount ;
  if(rowNum1>=1)
  {
  	alert("业务员还有管辖的网点, 不能做离职确认!");
  	return false ;
  }//ContGrid
  var rowNum2=ContGrid.mulLineCount ;
  if(rowNum2>=1)
  {
  	alert("业务员还有未签单的保单, 不能做离职确认!");
  	return false ;
  }
  var rowNum3=RenewalContGrid.mulLineCount ;
  if(rowNum3>=1)
  {
  	
    alert("业务员还有未分配的续期保单，不能进行离职确认");
    return false;
    
  }
  var StrAgent3 = "";
  if(fm.AgentCode.value != null && fm.AgentCode.value != ""){
  	StrAgent3 = "and a.AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
  }
  strSQL = "select 1 from LAAgent a where 1=1    "
             //+getWherePart('a.AgentCode','AgentCode')
             + StrAgent3
             + getWherePart('a.BranchType','BranchType')
	           + getWherePart('a.BranchType2','BranchType2')
             +" and a.agentstate>='03' and a.agentstate<='05' " ;
    strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
    //判断是否查询成功
    if (!strQueryResult) {
    alert("业务员已经离职确认或此业务员没有离职登记！");
    fm.all('AgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  	   
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
 initForm();
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert("请确定代理人编码！");
  else if ((fm.all('DepartTimes').value==null)||(fm.all('DepartTimes').value==''))
    alert("请查询出要修改的记录！");
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           

 
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert("请确定代理人编码！");
  else if ((fm.all('DepartTimes').value==null)||(fm.all('DepartTimes').value==''))
    alert("请查询出要删除的记录！");
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证代理人编码的合理性
function checkValid()
{ 
  var strSQL = "";
  var strQueryResult = null;
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var tManageCom =getManageComLimit(); 
  var tAgentCode=fm.all('AgentCode').value;
  var strAgent = "";
  if (getWherePart('AgentCode')!='')
  {
  	 strAgent = "and a.AgentCode = getAgentCode('"+tAgentCode+"')";
     strSQL = "select getUniteCode(a.AgentCode),a.name,a.managecom,b.agentgrade,(select branchattr  from labranchgroup c where a.agentgroup=c.agentgroup),(select  name from labranchgroup c where a.agentgroup=c.agentgroup) from LAAgent a,latree b where 1=1 and a.agentcode=b.agentcode  "
             //+getWherePart('a.AgentCode','AgentCode')
             + strAgent
             +" and a.agentstate>='03' and a.agentstate<='05' "
             +" and a.branchtype='"+tBranchType+"'"
             +" and a.branchtype2='"+tBranchType2+"'";
     strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
     
     //alert(strSQL);
     
     
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员或此业务员没有离职登记！");
    fm.all('AgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  else
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult); 
  	fm.all('AgentName').value = arrDataSet[0][1];
  	fm.all('ManageCom').value = arrDataSet[0][2];	 
  	fm.all('AgentGrade').value = arrDataSet[0][3];	
  	fm.all('BranchAttr').value = arrDataSet[0][4];
  	fm.all('BranchName').value = arrDataSet[0][5];
  	mManageCom=arrDataSet[0][2]; 	  
  	
  	if(mManageCom.indexOf(tManageCom)<0)
  	{
  	alert("您没有该业务员的操作权限!");
  	fm.all('AgentCode').value  = "";
        fm.all('DepartTimes').value = "";
        initInpBox();
        return false;
  	}  	  
  }
  var strAgent1 = ""; 
  if(fm.AgentCode.value != null && fm.AgentCode.value != ""){
  	strAgent1 = "and a.AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
  }
  //查询成功则拆分字符串，返回二维数组
  var tBranchType=fm.all('BranchType').value;
   var tBranchType2=fm.all('BranchType2').value;
  strQueryResult = null;
  
  //strSQL = "select AgentCode,departtimes,applydate from LADimission where 1=1 "+ getWherePart('AgentCode')
	//   +" and branchtype='"+tBranchType+"'"
	//   +" and branchtype2='"+tBranchType2+"'"
	//   + " order by AgentCode,departtimes";
	strSQL = "select getUniteCode(a.AgentCode),b.name,a.DepartTimes,a.DepartRsn,a.DepartDate,a.ApplyDate,"
	       + "a.QualityDestFlag,a.PbcFlag,a.ReturnFlag,a.AnnuityFlag,a.VisitFlag,a.BlackFlag,"
	       + "a.BlackListRsn,case when (a.DepartState='03' or a.DepartState='04' or a.DepartState='05') then '离职未确认'"
	       + " when (a.DepartState='08' or a.DepartState='07' or a.DepartState='06') then '离职已确认' else '' end,"
	       + "a.DestoryFlag,a.operator,a.makedate from ladimission a,laagent b"
	       + " where a.agentcode=b.agentcode " 
	       //+getWherePart('a.AgentCode','AgentCode');
	       + strAgent1;
  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
    
  if (!strQueryResult)
  {
    fm.all('DepartTimes').value = '1';
     
    }
  else
  {
   
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    //afterQuery(arrDataSet);
    
    fm.all('DepartTimes').value = eval(arrDataSet[0][2]);
    fm.all('DepartRsn').value = eval(arrDataSet[0][3]);
    fm.all('ApplyDate').value=arrDataSet[0][5];
     
  }
  // 设置离职欠费标记
//	document.fm.OweMoney.value = getDebtMark(document.fm.AgentCode.value);
	// 表示佣金列表
	 
  queryAgentBank(tAgentCode);
  queryAgentCont(tAgentCode);
  queryRenewalCont(tAgentCode);
  
}
//用来显示返回的选项
function afterQuery(arrQueryResult)
{	
  var arrResult = new Array();
 // alert(arrQueryResult);
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.all('AgentCode').value = arrResult[0][0];
    fm.all('AgentName').value = arrResult[0][1];
    fm.all('DepartTimes').value = arrResult[0][6];  
    fm.all('ManageCom').value = arrResult[0][2];  
    fm.all('AgentGrade').value = arrResult[0][3];  
    fm.all('BranchAttr').value = arrResult[0][4]; 
    fm.all('BranchName').value = arrResult[0][5]; 
    fm.all('DepartRsn').value = arrResult[0][7];
    showOneCodeNametoAfter('DepartRsn','DepartRsn');
    fm.all('DepartDate').value = arrResult[0][8];
    //mDepartDate=arrResult[0][4];
    fm.all('ApplyDate').value=arrResult[0][9];
    
    fm.all('DepartState').value = arrResult[0][10];
    
    
    // 设置离职欠费标记
	 //	document.fm.OweMoney.value = getDebtMark(arrResult[0][0]);
		// 表示佣金列表
		//queryAgentWage(arrResult[0][0]);
		 queryAgentBank(fm.all('AgentCode').value);
     queryAgentCont(fm.all('AgentCode').value);
     queryRenewalCont(fm.all('AgentCode').value);
  }   
}

function getAgentName(tAgentCode)
{
	var tAgentName='';
	var strSQL = "select name from LAAgent where 1=1 and AgentCode= getAgentCode('"+tAgentCode +"')";
             //alert(strSQL);
  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  if (strQueryResult)
  {
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  tAgentName = arrDataSet[0][0]; 
	}
  return tAgentName;
}

/*
function handler(key_event)
{
	if (document.all)
	{
		if (key_event.keyCode==13)
		{
		    parent.fraInterface.fm.action = "DimAgentQuery.jsp";	
		    if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value == null))
                    {
  	              alert("请输入代理人编码！");
  	              fm.all('AgentCode').focus();
  	              return false;
                    }
		    fm.submit();
		    parent.fraInterface.fm.action = "LADimissionSave.jsp";	
		    return true;
		}
	}  	
}*/

/**************************************************************
 * 功能描述：查询是否有欠费
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function getDebtMark(pmAgentCode)
{
 	var tSQL = "";
 	var tDebtMark = "";
 	
	tSQL  = "select";
	tSQL += " case when value(summoney,0) < 0 then 'Y'";
	tSQL += "      else 'N'";
	tSQL += "  end as aaa";
	tSQL += " from lawage";
	tSQL += " where branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"'";
	tSQL += "  and agentcode= getAgentCode('"+pmAgentCode+"')";
	tSQL += " order by indexcalno desc";
	
	var strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
  if (strQueryResult)
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult);
  	tDebtMark = arrDataSet[0][0]; 
	}
	
	if("" == tDebtMark)
	{
		tDebtMark = "N";
	}
	
  return tDebtMark;
}

/**************************************************************
 * 功能描述：查询制定业务员佣金信息
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function queryAgentWage(pmAgentCode)
{
	var tSQL = "";
	
	tSQL  = "select getUniteCode(a.AgentCode),";
	tSQL += "       b.Name,";
	tSQL += "       a.ManageCom,";
	tSQL += "       a.branchattr,";
	tSQL += "       c.Name,";
	tSQL += "       a.indexcalno,";
	tSQL += "       a.summoney";
	tSQL += "  from LAWage a,LAAgent b,LABranchGroup c";
	tSQL += " where a.AgentCode = b.AgentCode";
	tSQL += "   and b.AgentGroup = c.AgentGroup";
	tSQL += "   and a.branchtype='"+fm.all('BranchType').value+"' and a.branchtype2='"+fm.all('BranchType2').value+"'";
	tSQL += "   and a.agentcode = getAgentCode('"+pmAgentCode+"')";
	tSQL += " order by a.indexcalno DESC";
	
	//执行查询并返回结果
	turnPage.queryModal(tSQL, AgentWageGrid);
}
/**************************************************************
 * 功能描述：查询制定业务员网点信息
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function queryAgentBank(pmAgentCode)
{
	var strAgent4 = "";
	if(fm.AgentCode.value != null && fm.AgentCode.value != "" ){
		strAgent4 = "and a.AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
	}
	var tSQL = "";
	tSQL  = "select a.agentcom,";
	tSQL += "       b.Name";
	 
	tSQL += "  from lacomtoagent a,lacom b ";
	tSQL += " where a.relatype='1' ";
	tSQL += "   and a.agentcom = b.agentcom ";
	tSQL += "   and (b.endflag<>'Y' or b.endflag is null)";
	tSQL += "   and b.branchtype='"+fm.all('BranchType').value+"' and b.branchtype2='"+fm.all('BranchType2').value+"'" 
	//+getWherePart('a.AgentCode','AgentCode') ;
	tSQL += strAgent4;
	tSQL += " order by a.agentcom  ";
	
	//执行查询并返回结果
	turnPage.queryModal(tSQL, AgentWageGrid);
}
/**************************************************************
 * 功能描述：查询制定业务员网点信息
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function queryAgentCont(pmAgentCode){
	var tSQL = "";
	
	tSQL ="select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode= getAgentCode('"+pmAgentCode+"') and  signdate is null and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)";
	 
	//tSQL+=" union select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode='"+pmAgentCode+"'   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)"; 
	     +" and not exists (select 1 from lcpol where contno=lccont.contno and agentcode= getAgentCode('"+pmAgentCode+"') and riskcode in('330501','530301'))";
	tSQL += " and makedate>=current date - 90 days ";     		
	tSQL += " order by  contno  ";
	
	//执行查询并返回结果
	turnPage2.queryModal(tSQL, ContGrid);
}

function queryRenewalCont(pmAgentCode)
{
  var tSQL = "";
  
  tSQL ="select distinct a.contno,a.prtno, getUniteCode(a.agentcode) from lcpol a where a.stateflag='1' and exists (select 1 from lmriskapp where riskcode=a.riskcode and riskperiod ='L') and payintv<>0 and a.agentcode= getAgentCode('"+pmAgentCode+"')";
  
  tSQL +="order by contno";
  
  turnPage3.queryModal(tSQL, RenewalContGrid);
}

function ListExecl()
{
  
//定义查询的数据
var mAgentCode=fm.all('AgentCode').value
if ( mAgentCode == null || mAgentCode == "" )
	{
		alert("请输入业务员代码！");
		return false;
	}
var strSQL = "";
strSQL = "select distinct a.contno,a.prtno,getUniteCode(a.agentcode) from lcpol a where a.stateflag='1' and exists (select 1 from lmriskapp where riskcode=a.riskcode and riskperiod ='L') and payintv<>0 and a.agentcode=getAgentCode('"+mAgentCode+"')";
     
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '保单号','印刷号','营销员编码' from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '应分配续期保单明细 查询下载' from dual where 1=1 ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}