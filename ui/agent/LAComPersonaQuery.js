//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAComPersonaQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


function returnParent()
{
        var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{	
				//alert("1");
				arrReturn = getQueryResult();
				//alert("2");
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	tRow = AgentGrid.getSelNo();
	//alert("111" + tRow);
	if( tRow == 0 || tRow == null)
	  return arrSelected;
	var arrSelected = new Array();
	var strSQL = "";
	strSQL = "select a.personalcode,a.name,a.agentcom,b.name,a.idno,a.password,a.loginname,a.sex,a.headship,a.birthday,a.homeaddress,a.phone,a.employdate,a.outworkdate,a.operator,a.noti,a.mobile,a.managecom,'',a.usertype,a.rulestate,(select q.groupagentcode from laagent q where q.agentcode = a.agentcode fetch first row only ),Mac_id,Mac_flag  "
	        +" from  lacompersonal a,lacom b "
	        +" where a.agentcom=b.agentcom  and a.LoginName='"+AgentGrid.getRowColData(tRow-1,6)+"' "
	        +" union select getUniteCode(a.personalcode),a.name,a.agentcom,'',a.idno,a.password,a.loginname,a.sex,a.headship,a.birthday,a.homeaddress,a.phone,a.employdate,a.outworkdate,a.operator,a.noti,a.mobile,a.managecom,b.name,a.usertype,a.rulestate,b.groupagentcode,Mac_id,Mac_flag  "
	        +" from lacompersonal a,laagent b"
	        +" where a.agentcode=b.agentcode and a.LoginName='"+AgentGrid.getRowColData(tRow-1,6)+"' ";
	        //personalcode=0
	        //name=1
	        //agentcom=2
	        //b.name=3//''=3
	        //idno=4
	        //password=5
	        //loginname=6
	        //sex=7
	        //headship=8
	        //birthday=9
	        //homeaddress=10
	        //phone=11
	        //employdate=12
	        //outworkdate=13
	        //operator=14
	        //noti=15
	        //mobile=16
	        //managecom=17
	        //usertype=19
	        //rulestate=20
	       	         
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
        //判断是否查询成功
       if (!turnPage.strQueryResult) {
         alert("查询失败！");
         return false;
       }
       //查询成功则拆分字符串，返回二维数组
       arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
//	alert(arrSelected);
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initAgentGrid();
	// 书写SQL语句
	var strSQL = "";
	//判断不同用户类型,使用不同SQL:
	//若为01(代理机构人员),用lacom表做关联
	//若为02(直接机构人员),用laagent表做关联	
	strSQL = "select a.personalcode,a.name,a.agentcom,b.name,a.idno,a.LoginName,a.password,c.Codename,(select q.groupagentcode from laagent q where q.agentcode = a.agentcode fetch first row only ),'',d.CodeName"
	        +" from  lacompersonal a,lacom b,"
	        +" (select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where 1 = 1 and codetype = 'usertype' order by Code fetch first 2000 rows only ) c,"
	        +" (select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where 1 = 1 and codetype = 'rulestate' order by Code fetch first 2000 rows only) d "
	        +" where a.agentcom=b.agentcom and a.Usertype=c.code and a.rulestate=d.code "
	        + getWherePart('a.PersonalCode','AgentCode')
	        + getWherePart('a.Name','Name')
	        + getWherePart('a.IDNo','IDNo')
	        + getWherePart('a.Loginname','Loginname')	        
	        +" union select getUniteCode(a.personalcode),a.name,a.agentcom,'',a.idno,a.LoginName,a.password,c.Codename,b.groupagentcode,b.name,d.CodeName"
	        +" from  lacompersonal a,laagent b,"
	        +" (select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where 1 = 1 and codetype = 'usertype' order by Code fetch first 2000 rows only ) c,"
	        +" (select Code, CodeName, CodeAlias, ComCode, OtherSign from ldcode where 1 = 1 and codetype = 'rulestate' order by Code fetch first 2000 rows only) d "
	        +" where a.agentcode=b.agentcode and a.Usertype=c.code and a.rulestate=d.code"
	  //       + tReturn
	         + getWherePart('getUniteCode(a.PersonalCode)','AgentCode')
	         + getWherePart('b.Name','Name')
	         + getWherePart('a.IDNo','IDNo')
	         + getWherePart('a.Loginname','Loginname');
     	//alert(strSQL); 
//     	turnPage.queryModal(strSQL, AgentGrid); 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有找到相关数据，请输入正确条件重新查询！");
    return false;
}

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}


