var mDebug = "0";
var mOperate = "";
var showInfo;
var turnPage = new turnPageClass();
var mCodeName = "";
function clearData() {
	initForm();
}

function easyQueryClick() {
	if(fm.all('Managecom').value==''){
		alert("请输入管理机构！");
		return false;
	}
	var strSql = "";
	var BranchTypeCode = fm.all('BranchTypeCode').value
	fm.all('branchtype').value = BranchTypeCode.substr(0, 1);
	fm.all('branchtype2').value = BranchTypeCode.substr(1, 3);

	if (fm.all('AgentState').value == '1') {

		strSql = " select a.managecom,b.codename||c.codename,a.Name,case when a.agentstate<'06' then '在职'  when a.agentstate>='06' then '离职' end,"
				+ " a.AgentCode,a.GroupAgentCode "
				+ "from laagent a ,ldcode b, ldcode c where 1=1 and a.AgentState<'06' and b.codetype = 'branchtype' and c.codetype = 'branchtype2' "
				+ "and (c.code = '01' or (b.code = '1' and c.code in ('02', '03')) or(b.code = '2' and c.code in( '02','04'))) and  replace(b.code||c.code,' ','')in(replace(a.branchtype||a.branchtype2,' ',''))"
				+ getWherePart("ManageCom", "ManageCom","like")
				+ getWherePart("BranchType", "BranchType")
				+ getWherePart("BranchType2", "BranchType2")
				+ getWherePart("Name", "Name")
				+ getWherePart("GroupAgentCode", "GroupAgentCode")
				+ getWherePart("AgentCode", "AgentCode")
				+ " order by AgentCode fetch first 1000 rows only";
	} else if (fm.all('AgentState').value == '2') {
		strSql = " select a.managecom,b.codename||c.codename,a.Name,case when a.agentstate<'06' then '在职'  when a.agentstate>='06' then '离职' end ,"
				+ "a.AgentCode,a.GroupAgentCode "
				+ "from laagent a ,ldcode b, ldcode c where 1=1 and a.AgentState>='06' and b.codetype = 'branchtype' and c.codetype = 'branchtype2' "
				+ "and (c.code = '01' or (b.code = '1' and c.code in ('02', '03')) or(b.code = '2' and c.code in( '02','04'))) and  replace(b.code||c.code,' ','')in(replace(a.branchtype||a.branchtype2,' ',''))"
				+ getWherePart("ManageCom", "ManageCom",'like')
				+ getWherePart("BranchType", "BranchType")
				+ getWherePart("BranchType2", "BranchType2")
				+ getWherePart("Name", "Name")
				+ getWherePart("GroupAgentCode", "GroupAgentCode")
				+ getWherePart("AgentCode", "AgentCode")
				+ " order by AgentCode fetch first 1000 rows only";
	} else {
		strSql = " select a.managecom,b.codename||c.codename,a.Name,case when a.agentstate<'06' then '在职'  when a.agentstate>='06' then '离职' end ,"
				+ "a.AgentCode,a.GroupAgentCode "
				+ "from laagent a ,ldcode b, ldcode c where 1=1 and b.codetype = 'branchtype' and c.codetype = 'branchtype2' "
				+ "and (c.code = '01' or (b.code = '1' and c.code in ('02', '03')) or(b.code = '2' and c.code in( '02','04'))) and  replace(b.code||c.code,' ','')in(replace(a.branchtype||a.branchtype2,' ',''))"
				+ getWherePart("ManageCom", "ManageCom",'like')
				+ getWherePart("BranchType", "BranchType")
				+ getWherePart("BranchType2", "BranchType2")
				+ getWherePart("Name", "Name")
				+ getWherePart("GroupAgentCode", "GroupAgentCode")
				+ getWherePart("AgentCode", "AgentCode")
				+ " order by AgentCode fetch first 1000 rows only";
	}
	// if (fm.all('agentcode').value==null||fm.all('agentcode').value==''){
	// alert("请输入系统内部工号！");
	// return false;
	// }
	turnPage.queryModal(strSql, LAGroupCodeQueryBoxGrid);
	showCodeName();
}
function submitForm() {
	if(fm.all('Managecom').value==''){
		alert("请输入管理机构！");
		return false;
	}
	var mManageCom = fm.all('Managecom').value;
	var condition = "";
	if (getWherePart('ManageCom') != '') {
		condition += " and a.ManageCom like '" + mManageCom + "%'";
	}
	var mState = fm.all('AgentState').value;
	if (getWherePart('AgentState') != '') {
		if (mState == "1") {
			condition += " and a.AgentState in ('01','02','03','04','05') ";
		} else if (mState == "2") {
			condition += " and a.AgentState in ('06','07','08')";
		}
	}
	var BranchTypeCode = fm.all('BranchTypeCode').value;
	if (getWherePart('BranchTypeCode') != '') {
		fm.all('branchtype').value = BranchTypeCode.substr(0, 1);
		fm.all('branchtype2').value = BranchTypeCode.substr(1, 3);
		branchtype = fm.all('branchtype').value;
		branchtype2 = fm.all('branchtype2').value;
		condition += " and a.BranchType='" + branchtype
				+ "' and a.BranchType2='" + branchtype2 + "'";
	}
	var Name = fm.all('Name').value;;
	if (getWherePart('Name') != '') {
		condition += " and a.Name = '" + Name + "'";
	}
	var AgentCode = fm.all('AgentCode').value;;
	if (getWherePart('AgentCode') != '') {
		condition += " and a.AgentCode = '" + AgentCode + "'";
	}
	var GroupAgentCode = fm.all('GroupAgentCode').value;;
	if (getWherePart('GroupAgentCode') != '') {
		condition += " and a.GroupAgentCode = '" + GroupAgentCode + "'";
	}
	var strSQL = "";
	strSQL = " select a.managecom,b.codename||c.codename,a.Name,case when a.agentstate<'06' then '在职'  when a.agentstate>='06' then '离职' end,"
			+ " a.AgentCode,a.GroupAgentCode "
			+ "from laagent a ,ldcode b, ldcode c where 1=1 and  b.codetype = 'branchtype' and c.codetype = 'branchtype2' "
			+ "and (c.code = '01' or (b.code = '1' and c.code in ('02', '03')) or(b.code = '2' and c.code in( '02','04'))) and  replace(b.code||c.code,' ','')in(replace(a.branchtype||a.branchtype2,' ',''))"
			+ condition + " order by AgentCode  fetch first 9999 rows only with ur";
	fm.querySql.value = strSQL;
	// 定义列名
	var strSQLTitle = "select '管理机构','业务渠道','业务员姓名','人员状态','业务员编码','集团工号'  from dual where 1=1 ";
	fm.querySqlTitle.value = strSQLTitle;
	// 定义表名
	fm.all("Title").value = "select '业务员集团工号信息明细表 ：' from dual where 1=1 ";

	fm.action = " ../agentquery/LAPrintTemplateSave.jsp";

	fm.submit(); //提交 	
}
function getGroupAgentCode (){
	if(fm.all('Managecom').value==''){
		alert("请输入管理机构！");
		return false;
	}
	var showStr = "正在获取集团工号，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//	fm.all("but").disabled=true;
	fm.action = "./LAGroupCodeQuerySave.jsp";
	fm.submit(); //提交 	
}
function afterSubmit(FlagStr, content, transact) {
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		// 执行下一步操作
	}
	easyQueryClick();
//	fm.all("but").disabled=false;
}