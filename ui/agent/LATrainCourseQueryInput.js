//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//修改人：解青青  时间：2014-11-23
function checkAgentCode()
{
//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
   var sql=" select agentcode  from laagent   where  1=1" 
             + getWherePart("groupagentcode","GroupAgentCode")
//   		"groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
    
}

function getManagecom(Managecom,ManagecomName)
{
	    var pManagecom = fm.all('PManageCom').value;
	    if(pManagecom==null||pManagecom=='')
	    {
	       alert("请先录入省公司代码");
	       return false;
	    }
  var strsql ="1 and  length(trim(comcode))=8 and comcode like #"+pManagecom+"%#";
  showCodeList('comcode',[Managecom,ManagecomName],[0,1],null,strsql,1,1);
}  

// 页面触发获取营业部基础信息
function getBranchAttr(BranchAttr,BranchAttrName)
{
	var cManagecom = fm.all('CManageCom').value;
	
    if(cManagecom==null||cManagecom=='')
    {
       alert("请先录入中心支公司代码");
       return false;
    }
    var strsql1 ="1 and length(trim(branchattr))=10 and branchtype = #"+fm.all('BranchType').value+"# and branchtype2 = #"+fm.all('BranchType2').value+"# and managecom like #"+cManagecom+"%#";
    showCodeList('branchattr',[BranchAttr,BranchAttrName],[0,1],null,strsql1,1,1);
    
}



/*****************************
  * 注: 提交前的校验、计算  
  ****************************/
function beforeSubmit()
{
  //添加操作	
}           
/*****************************
  * 注:显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示 
  ****************************/
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

/******************************************
 * 注: 查询得到返回值
 ******************************************/
function getQueryResult()
{
  var arrSelected = null;
  tRow = SetGrid.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet == null )
    return arrSelected;
  arrSelected = new Array();
  var tCourseNo=SetGrid.getRowColData(tRow-1,12);
  
//获取开始结束时间
  var StartDate=fm.all('StartDate').value;
  var EndDate=fm.all('EndDate').value;
  
  var strSQL = "select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
      +" a.CManagecom,(select name from ldcom where comcode = a.cmanagecom)," 
      +"(select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup =a.agentgroup)," 
      +	"(select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup =a.agentgroup),"
      +" a.startdate,a.enddate,a.coursename,"
      +" (select codename from ldcode where codetype = 'course' and code = a.coursename),"
      +" a.trainplace,a.courseno "
      +"  from latraincourse a  where  a.branchtype = '1' and a.branchtype2 = '01'"
      +" and a.courseno = '"+tCourseNo+"'"
      +getWherePart('a.coursename','Course')
      +getWherePart('a.cmanagecom','CManageCom')
      +getWherePart('a.pmanagecom','PManageCom','like')
      +getWherePart('a.trainplace','TrainPlace','like');
  if(StartDate!=null&&StartDate!=''){
 	 strSQL=strSQL+" and a.enddate>='"+StartDate+"'";
  }
  if(EndDate!=null&&EndDate!=''){
 	 strSQL=strSQL+" and a.enddate<='"+EndDate+"'";
  }
  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  arrSelected = decodeEasyQueryResult(strQueryResult);
  return arrSelected;
}

/******************************************
 * 注: 返回主界面
 ******************************************/
function returnParent()
{
  var arrReturn = new Array();
  var tSel = SetGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
    try
    {	
      arrReturn = getQueryResult();
      top.opener.afterQuery( arrReturn );
    }
    catch(ex)
    {
      alert( "没有发现父窗口的afterQuery接口。" + ex );
    }
    top.close();
		
  }
}

/******************************************
 * 注: 查询操作
 ******************************************/
function easyQueryClick()
{
     // 提前校验
	 if (!verifyInput()) return false ;
	 
     // 初始化表格
     initSetGrid();
     //获取开始结束时间
     var StartDate=fm.all('StartDate').value;
     var EndDate=fm.all('EndDate').value;
     var BranchAttr=fm.all('BranchAttr').value;
  // 书写SQL语句
     var strSQL ="select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
    	 +" a.CManagecom,(select name from ldcom where comcode = a.cmanagecom)," 
         +"(select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = a.agentgroup)," 
         +"(select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup =a.agentgroup),"
         +" a.startdate,a.enddate,a.coursename,"
         +" (select codename from ldcode where codetype = 'course' and code = a.coursename),"
         +" a.trainplace,a.courseno "
         +"  from latraincourse a  where   a.branchtype = '1' and a.branchtype2 = '01'"
         +getWherePart('a.coursename','Course')
         +getWherePart('a.cmanagecom','CManageCom')
         +getWherePart('a.pmanagecom','PManageCom','like')
         +getWherePart('a.trainplace','TrainPlace','like')
         if(StartDate!=null&&StartDate!=''){
        	 strSQL=strSQL+" and a.enddate>='"+StartDate+"'";
         }
         if(EndDate!=null&&EndDate!=''){
        	 strSQL=strSQL+" and a.enddate<='"+EndDate+"'";
         }
         if(BranchAttr!=null&&BranchAttr!=''){
        	 strSQL=strSQL+" and a.agentgroup=(select agentgroup from labranchgroup where branchattr='"+BranchAttr+"' and branchtype='1' and branchtype2='01')";
         }
         strSQL+=" order by a.pmanagecom";
      turnPage.queryModal(strSQL, SetGrid);  
       
	
}
function downLoad()
{
    // 提前校验
	 if (!verifyInput()) return false ;
	//
	   //获取开始结束时间
    var StartDate=fm.all('StartDate').value;
    var EndDate=fm.all('EndDate').value;
    var BranchAttr=fm.all('BranchAttr').value;
    // 书写SQL语句
    var strSql ="select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
   	 +" a.CManagecom,(select name from ldcom where comcode = a.cmanagecom)," 
        +"(select branchattr from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup = a.agentgroup)," 
        +"(select name from labranchgroup where branchtype = '1' and branchtype2 = '01' and agentgroup =a.agentgroup),"
        +" a.startdate,a.enddate,a.coursename,"
        +" (select codename from ldcode where codetype = 'course' and code = a.coursename),"
        +" a.trainplace,a.courseno "
        +"  from latraincourse a  where   a.branchtype = '1' and a.branchtype2 = '01'"
        +getWherePart('a.coursename','Course')
        +getWherePart('a.cmanagecom','CManageCom')
        +getWherePart('a.pmanagecom','PManageCom','like')
        +getWherePart('a.trainplace','TrainPlace','like')
        if(StartDate!=null&&StartDate!=''){
        	strSql=strSql+" and a.enddate>='"+StartDate+"'";
        }
        if(EndDate!=null&&EndDate!=''){
        	strSql=strSql+" and a.enddate<='"+EndDate+"'";
        }
        if(BranchAttr!=null&&BranchAttr!=''){
        	strSql=strSql+" and a.agentgroup=(select agentgroup from labranchgroup where branchattr='"+BranchAttr+"' and branchtype='1' and branchtype2='01')";
        }
        strSql+=" order by a.pmanagecom";
	
	fm.all('querySQL').value = strSql;
	fm.submit();
}
