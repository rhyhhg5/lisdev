//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var mDepartDate="";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
 
  if (mOperate==""){
  	mOperate="INSERT||MAIN";
  }

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.hideOperate.value=mOperate;

  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
		initForm();
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  //  showDiv(operateButton,"true"); 
  //  showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

function queryClick()
{
  //下面增加相应的代码
 
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  showInfo=window.open("./LAZJDimissionAppQueryhtml.jsp?BranchType="+tBranchType+"&BranchType2="+tBranchType2+"");
}           


//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	  
 if( verifyInput() == false ) return false;
 
  return true;
}           
//查寻是否存在没签单的保单
function checkcontno()
{
  var tAgentCode=fm.all('AgentCode').value;	
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var sql="";
  if (tBranchType=='1' && tBranchType2=='01')
  {
    sql="select contno  from  lccont where  agentcode=getunitecode('"+tAgentCode+"') and   (appflag<>'1' or appflag is null ) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)";	
  }
  else if (tBranchType=='2' && tBranchType2=='01')
  { 
    sql="select grpcontno  from  lcgrpcont where  agentcode=getunitecode('"+tAgentCode+"') and  (appflag<>'1' or appflag is null ) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) ";	  	
  }
  else if (tBranchType=='2' && tBranchType2=='02')
  { 
    sql="select grpcontno  from  lcgrpcont where  agentcode=getunitecode('"+tAgentCode+"') and  (appflag<>'1' or appflag is null ) and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) ";	  	
  }
  strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if(strQueryResult)
  {//如果有没签单的
  	alert("此业务员还有没签单的保单，不能离职！");
  	return false ;
  }
  else
   {//如果都签单了
   var sql="";
    if (tBranchType=='1' && tBranchType2=='01')
    {
     sql="select contno  from  lccont where  agentcode=getunitecode('"+tAgentCode+"')   and  customgetpoldate is null  and  uwflag<>'a' and uwflag<>'8' and uwflag<>'1'";			
     strQueryResult = easyQueryVer3(sql, 1, 1, 1); 

       if(strQueryResult)
       {
           var tArr = new Array();
           tArr = decodeEasyQueryResult(strQueryResult);	    
     
   	  if (confirm("此业务员存在没有回执回销的保单,保单号为:"+tArr+"  您确实对该业务员进行离职登记吗?"))
          {             
           return true; 
          }
          else
          {
            mOperate="";
             alert("您取消了该操作！");
            return false; 
          }  	 
      }              
    }
    else if (tBranchType=='2' && tBranchType2=='01')
    {
       sql="select grpcontno  from  lcgrpcont where  agentcode=getunitecode('"+tAgentCode+"')   and  customgetpoldate is null  and  uwflag<>'a' and uwflag<>'8' and uwflag<>'1'";			
       strQueryResult = easyQueryVer3(sql, 1, 1, 1); 
       if(strQueryResult)
       {
           var tArr = new Array();
           tArr = decodeEasyQueryResult(strQueryResult);	    
            	   

   	  if (confirm("此业务员存在没有回执回销的保单,保单号为:"+tArr+"  您确实对该业务员进行离职登记吗?"))
          {             
           return true; 
          }
          else
          {
            mOperate="";
             alert("您取消了该操作！");
            return false; 
          }   	   	 
      }                
    }
     else if (tBranchType=='2' && tBranchType2=='02')
    {
       sql="select grpcontno  from  lcgrpcont where  agentcode=getunitecode('"+tAgentCode+"')   and  customgetpoldate is null  and  uwflag<>'a' and uwflag<>'8' and uwflag<>'1'";			
       strQueryResult = easyQueryVer3(sql, 1, 1, 1); 
       if(strQueryResult)
       {
           var tArr = new Array();
           tArr = decodeEasyQueryResult(strQueryResult);	    
            	   

   	  if (confirm("此专员存在没有回执回销的保单,保单号为:"+tArr+"  您确实对该业务员进行离职登记吗?"))
          {             
           return true; 
          }
          else
          {
            mOperate="";
             alert("您取消了该操作！");
            return false; 
          }   	   	 
      }                
    }
  		return true;
   }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
 initForm();
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert("请确定业务员编码！");
  else if ((fm.all('DepartTimes').value==null)||(fm.all('DepartTimes').value==''))
    alert("请查询出要修改的记录！");
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           
       

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert("请确定业务员编码！");
  else if ((fm.all('DepartTimes').value==null)||(fm.all('DepartTimes').value==''))
    alert("请查询出要删除的记录！");
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证代理人编码的合理性
function checkValid()
{ 
 
  var strSQL = "";
  var strQueryResult = null;
//  var tBranchType=fm.all('BranchType').value;
//  var tBranchType2=fm.all('BranchType2').value;
  var tManageCom =getManageComLimit(); 
  var tAgentCode=fm.all('AgentCode').value;
  var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  if (getWherePart('AgentCode')!='')
  {
     strSQL = "select getunitecode(a.AgentCode),a.name,a.managecom,b.agentgrade,(select branchattr  from labranchgroup c where a.agentgroup=c.agentgroup),(select  name from labranchgroup c where a.agentgroup=c.agentgroup),a.branchtype,a.branchtype2 from LAAgent a,latree b where 1=1 and a.agentcode=b.agentcode  "
//             +getWherePart('a.AgentCode','AgentCode')
     		+ strAgent
             +" and a.agentstate<'03'   ";
//             +" and a.branchtype='"+tBranchType+"'"
//             +" and a.branchtype2='"+tBranchType2+"'";
     strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('AgentCode').value = '';
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员或此业务员已经离职登记！");
    fm.all('AgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  else
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult); 
  	fm.all('AgentName').value = arrDataSet[0][1];
  	fm.all('ManageCom').value = arrDataSet[0][2];	 
  	fm.all('AgentGrade').value = arrDataSet[0][3];	
  	fm.all('BranchAttr').value = arrDataSet[0][4];
  	fm.all('BranchName').value = arrDataSet[0][5];
  	fm.all('BranchType').value = arrDataSet[0][6];
    fm.all('BranchType2').value = arrDataSet[0][7];
  	mManageCom=arrDataSet[0][2]; 	  
  	
  	if(mManageCom.indexOf(tManageCom)<0)
  	{
  	alert("您没有该业务员的操作权限!");
  	fm.all('AgentCode').value  = "";
        fm.all('DepartTimes').value = "";
        initInpBox();
        return false;
  	}  	  
  }
  //查询成功则拆分字符串，返回二维数组
  var tBranchType=fm.all('BranchType').value;
   var tBranchType2=fm.all('BranchType2').value;
  strQueryResult = null;
  //strSQL = "select AgentCode,departtimes,applydate from LADimission where 1=1 "+ getWherePart('AgentCode')
	//   +" and branchtype='"+tBranchType+"'"
	//   +" and branchtype2='"+tBranchType2+"'"
	//   + " order by AgentCode,departtimes";
  var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	strSQL = "select getunitecode(a.AgentCode),b.name,a.DepartTimes,a.DepartRsn,a.DepartDate,a.ApplyDate,"
	       + "a.QualityDestFlag,a.PbcFlag,a.ReturnFlag,a.AnnuityFlag,a.VisitFlag,a.BlackFlag,"
	       + "a.BlackListRsn,case when (a.DepartState='03' or a.DepartState='04' or a.DepartState='05') then '离职未确认'"
	       + " when (a.DepartState='08' or a.DepartState='07' or a.DepartState='06') then '离职已确认' else '' end,"
	       + "a.DestoryFlag,a.operator,a.makedate from ladimission a,laagent b"
	       + " where a.agentcode=b.agentcode " +strAgent;//getWherePart('a.AgentCode','AgentCode');
  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult)
    fm.all('DepartTimes').value = '1';
  else
  {
  	
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    //afterQuery(arrDataSet);
    
    fm.all('DepartTimes').value = eval(arrDataSet[0][2]);
    fm.all('DepartRsn').value = eval(arrDataSet[0][3]);
    fm.all('ApplyDate').value=arrDataSet[0][5];
  }
  // 设置离职欠费标记
//	document.fm.OweMoney.value = getDebtMark(document.fm.AgentCode.value);
	// 表示佣金列表
	
	queryAgentBank(tAgentCode);
  queryAgentCont(tAgentCode);
}
  
//用来显示返回的选项
function afterQuery(arrQueryResult)
{	
	 
  var arrResult = new Array();
  if( arrQueryResult != null )
  {
    arrResult = arrQueryResult;
    fm.all('AgentCode').value = arrResult[0][0]; 
    checkValid();
                                                                                                                                                                                                                            	
  }   
}
function getAgentName(tAgentCode)
{
	var tAgentName='';
	var strSQL = "select name from LAAgent where 1=1 and AgentCode='"+tAgentCode
             +"'";
             //alert(strSQL);
  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  if (strQueryResult)
  {
  var arrDataSet = decodeEasyQueryResult(strQueryResult);
  tAgentName = arrDataSet[0][0]; 
	}
  return tAgentName;
}
 
 /**************************************************************
 * 功能描述：查询制定业务员网点信息
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function queryAgentBank(pmAgentCode)
{
	var tSQL = "";
	
	tSQL  = "select a.agentcom,";
	tSQL += "       b.Name";
	 
	tSQL += "  from lacomtoagent a,lacom b ";
	tSQL += " where a.relatype='1' ";
	tSQL += "   and a.agentcom = b.agentcom ";
	tSQL += "   and (b.endflag<>'Y' or b.endflag is null) ";
	tSQL += "   and b.branchtype='"+fm.all('BranchType').value+"' and b.branchtype2='"+fm.all('BranchType2').value+"'" 
	+getWherePart('getunitecode(a.AgentCode)','AgentCode') ;
	tSQL += " order by a.agentcom  ";
	
	//执行查询并返回结果
	turnPage.queryModal(tSQL, AgentWageGrid);
}
/**************************************************************
 * 功能描述：查询制定业务员网点信息
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function queryAgentCont(pmAgentCode)
{
	var tSQL = "";
	
	tSQL ="select contno,prtno,signdate,customgetpoldate  from  lccont where  grpcontno='00000000000000000000' and getunitecode(agentcode)='"+pmAgentCode+"' and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	tSQL+=" union select contno,prtno,signdate,customgetpoldate  from  lccont where  grpcontno='00000000000000000000' and getunitecode(agentcode)='"+pmAgentCode+"'   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	tSQL +="union  select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  getunitecode(agentcode)='"+pmAgentCode+"' and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;	 
	tSQL+=" union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  getunitecode(agentcode)='"+pmAgentCode+"'   and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;		
	//执行查询并返回结果
	turnPage2.queryModal(tSQL, ContGrid);
}