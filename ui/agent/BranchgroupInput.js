//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
var queryFlag = false;

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoBranchgroupInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//提交之前基本校验
function beforeSubmit()
{
	var managecom=new Date(fm.all('ManageCom ').value);
	var branchattr=new Date(fm.all('Branchattr').value);
	var agentgroup=new Date(fm.all('Agentgroup').value);
	var branchtypecode=new Date(fm.all('BranchTypeCode').value);
    if (managecom==null||managecom=='')
   {
   	alert("管理机构为必填项，不能为空！");
   	return false;
   }
   if(branchattr==null||branchattr==''){
   	alert("团队外部编码为必填项，不能为空!");
   	return false;
   }
   if(agentgroup==null||agentgroup==''){
   alert("团队核心编码为必填项，不能为空！");
   return false;
   }
   if(branchtypecode==null||branchtypecode==''){
   alert("业务渠道为必填项，不能为空！");
   return false;
   }
   return true;
}
//查询之前先检查数据
function check()
{
 if(fm.all("ManageCom").value==null||fm.all("ManageCom").value==''){
	   alert("管理机构为必填项，不能为空！");
	   return false;
	}
	if(fm.all("Branchattr").value==null||fm.all("Branchattr").value==''){
	  alert("团队外部编码为必填项，不能为空!");
	  return false;
	}
	if(fm.all('Agentgroup').value==null||fm.all('Agentgroup').value==''){
	 alert("团队核心编码为必填项，不能为空！");
	 return false;
	}
	if(fm.all('BranchTypeCode').value==null||fm.all('BranchTypeCode').value==''){
	 alert("业务渠道为必填项，不能为空！");
	 return false;
	}
	return true;
}
//页面查询
function easyQueryClick()
{	
	if(!check())
    return false ;
    var BranchTypeCode = fm.all('BranchTypeCode').value
	fm.all('BranchType').value = BranchTypeCode.substr(0, 1);
	fm.all('BranchType2').value = BranchTypeCode.substr(1, 3);
	// 初始化表格	
	// 书写SQL语句
	var strSQL = "";
     strSQL ="select managecom,branchattr,agentgroup,name,getunitecode(branchmanager),branchmanagername,endflag,enddate from labranchgroup where managecom='"+fm.all("ManageCom").value+"'"
            +" and agentgroup='"+fm.all("Agentgroup").value+"'"
            +" and branchattr='"+fm.all("Branchattr").value+"'"
            +" and branchtype='"+fm.all('BranchType').value+"'"
            +" and branchtype2='"+fm.all('BranchType2').value+"'"
    		+ " order by managecom,branchattr,agentgroup";	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
   }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
   queryFlag = true;
}
//Click事件，当点击“修改”图片时触发该函数
function updateclick()
{
	fm.fmAction.value = "UPDATE";
	if(queryFlag ==false) 
	{
		alert("请先查询");
		return false;	
	}
	if (confirm("您确实想修改该记录吗?"))
	{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./BranchgroupSave.jsp";
	fm.submit(); //提交
	}
	else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}           

function submitForm()
{
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.action = "./BranchgroupSave.jsp";
  fm.submit(); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


