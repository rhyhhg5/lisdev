//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var tOrphanCode="";
var tOriTrainDate="";
var tOriNoWorkFlag="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//alert("****");
	fm.action = "./LASave.jsp";
	//alert("#####");
	fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		//    showDiv(operateButton,"true");
		//    showDiv(inputButton,"false");
		fm.all('AgentGrade').disabled = false;
		fm.all('ManageCom').disabled = false;
		fm.all('IntroAgency').disabled = false;
		fm.all('BranchCode').disabled = false;
		fm.all('GroupManagerName').disabled = false;
		fm.all('DepManagerName').disabled = false;
		initForm();
	}
	catch(re)
	{
		alert("在LAAgent.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//取消按钮对应操作
function cancelForm()
{
	//  window.location="../common/html/Blank.html";
	showDiv(operateButton,"true");
	showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
	 
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{    //alert("******");
	//	initForm();
	//下面增加相应的代码
	//mOperate="QUERY||MAIN";
	showInfo=window.open("./LAQuery.html");
	//alert("######");
	 //提交
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	      if (!beforeSubmit())
		{
			return false;
		}
			//下面增加相应的删除代码
			if (confirm("您确实想删除该记录吗?"))
			{
				mOperate="DELETE||MAIN";
				submitForm();
			}
			else{
				mOperate="";
				alert("您取消了删除操作！");
			}
}
//提交前的校验、计算
function beforeSubmit()
{
   var sql="select 1 from lccont where agentcode='"+fm.all('hideagentcode').value+"'"
          +"union select 1 from lcgrpcont where agentcode='"+fm.all('hideagentcode').value+"'"
          +"union select 1 from lcpol where agentcode='"+fm.all('hideagentcode').value+"'";
   var strQueryResult  = easyQueryVer3(sql, 1, 1, 1);
   if(strQueryResult!=null&&strQueryResult!=''){
      alert("该业务员名下有保单不能直接删除这个业务员信息！");
      return false;
   }
   return true;
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}
function afterQuery(arrQueryResult)
{
		var arrResult = new Array();
		resetForm();

		if( arrQueryResult != null )
		{
			arrResult = arrQueryResult;
			fm.all('AgentCode').value = arrResult[0][0];
			fm.all('Name').value = arrResult[0][1];
			fm.all('IDNo').value = arrResult[0][2];
			fm.all('EmployDate').value = arrResult[0][3];
			fm.all('AgentState').value = arrResult[0][5];
			fm.all('AgentGrade').value = arrResult[0][6];
			fm.all('BranchCode').value = arrResult[0][7];
			fm.all('ManageCom').value =arrResult[0][8];
			fm.all('GroupManagerName').value = arrResult[0][9];
			fm.all('DepManagerName').value = arrResult[0][10];
			fm.all('AgentGroup2').value = arrResult[0][11];
			fm.all('Group2ManagerName').value = arrResult[0][14];
			fm.all('IntroAgency').value = arrResult[0][12];
			fm.all('IntroAgencyName').value = arrResult[0][13];
			fm.all('hideagentcode').value = arrResult[0][15];
			fm.all('hideagentgroup').value = arrResult[0][16];
			fm.all('hidebranchcode').value = arrResult[0][17];
   }
}


				








