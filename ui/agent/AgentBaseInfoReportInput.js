//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{		
  //if(!verifyInput()) return false;
  //定义查询的数据
 var mManageCom=fm.all('Managecom').value;
 var condition="";
 if(getWherePart('ManageCom')!='')
 {
 	  condition+=" and a.ManageCom like '"+mManageCom+"%'";
 }
 var mState=fm.all('state').value; 
if(getWherePart('state')!=''){
  if(mState=="0"){
      condition+=" and a.AgentState in ('01','02') ";
    }
    else if(mState=="1"){
      condition+=" and a.AgentState in ('03','04','05')";
   }
    else if(mState=="2"){
      condition+=" and a.AgentState not in ('01','02','03','04','05') ";
   }
}
 var mAgentSeries=fm.all('agentseries').value;
        if(getWherePart('agentseries')!=''){
            condition+=" and b.AgentSeries='"+mAgentSeries+"' ";
        }
 var mCountDate=fm.all('CountDate').value;
        if(getWherePart('CountDate')!=''){
            condition+=" and a.EmployDate<='"+mCountDate+"' ";
        }
  var strSQL="";
        strSQL="select (select name from ldcom where comcode=substr(a.managecom,1,4)),(select name from ldcom where comcode=a.managecom),(select name from labranchgroup where agentgroup=a.AgentGroup),a.name,getunitecode(a.agentcode), "
        +"case sex when '0' then '男' when '1' then '女' when '2' then '其他' end sex, "
				+" (days(current date)-days(Birthday))/365 age,Birthday,EmployDate, "
				+" case Degree when '0' then '博士以上' when '1' then '硕士' when '2' then '大本' when '3' then '大专' when '4' then '中专' when '5' then '高中' when '6' then '高中以下' when '7' then '其他' end degreee, "
				+" case salequaf when 'Y' then '是' when 'N' then '否' else '' end salequaf,a.QuafNo,a.Quafstartdate,a.Quafenddate,year(current date-EmployDate) workage, "
   			+"  case Isconnman when 'Y' then '是' when 'N' then '否' else '' end Isconnman,b.VIPProperty , "
   			+" a.Mobile,a.IDNo, "
				+" (select gradename from laagentgrade where gradecode=b.agentgrade) grade, "
				+" case AgentSeries when '0' then '员工'  when '1' then '经理' end AgentSeries, "
				+" case AgentState when '01' then '在职' when '02' then '在职' when '03' then '离职登记' when '04' then '离职等级' when '05' then '离职等级' else '离职' end state, "
				+" case when a.agentstate>'02' and a.agentstate<'06' then (select max(applydate) from ladimission where agentcode=a.agentcode) else a.outworkdate end " 
				+" from laagent a,latree b "
				+" where a.agentcode=b.agentcode and a.BranchType='3' and a.BranchType2='01'"
				+condition
				+" order by a.managecom,a.agentcode"
                +" with ur ";
  fm.querySql.value = strSQL;
  
  //定义列名
  var strSQLTitle = "select '所属分公司','支公司','营业部','姓名','业务编码','性别','年龄','出生日期','入司时间','学历','代理人资格证（是/否）','资格证书号','取证日期','到期日期','参加工作年限','是否有同业经验（是/否）','同业工作年限','手机号码','身份证号','职 级','员工属性（管理/销售）','在职状态','离司时间'  from dual where 1=1 ";
  fm.querySqlTitle.value = strSQLTitle;
  //定义表名
  fm.all("Title").value="select '银保销售序列人员信息明细表 ：' from dual where 1=1 ";
   
  fm.action = " ../agentquery/LAPrintTemplateSave.jsp";

  fm.submit(); //提交 	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AgentBaseInfoReportInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else 
 	{
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
function easyQueryClick()
{	 
  
}
function afterCodeSelect(codeName,Field)
{
	
}
function beforeSubmit()
{
   return true;
}