//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var tOrphanCode="";
window.onfocus=myonfocus;
     //var currdate = PubFun.getCurrentDate();
 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}

//提交，保存按钮对应操作
function submitForm()
{
/*	//zff  身份证尾号不能为小x的校验
	if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}*/
	if( verifyInput() == false ) return false;
	AscripText();
	if (mOperate!='DELETE||MAIN')
	{
		if (!beforeSubmit())
		{
			return false;
		}
	}
	if(mOperate==null || mOperate==""||mOperate=="INSERT||MAIN")
	{
		if(!checkEmployDate()){
			return false;
		}
		
		//增加对筹备人员的校验20091117
		if(fm.NoWorkFlag.value == 'Y'){
			var tAgentGrade=fm.AgentGrade.value;
	 	  if(tAgentGrade == 'A01')
		   {  
		   	alert("筹备人员职级不能为A01！");
		   	fm.AgentGrade.value='';
		   	fm.AgentGradeName.value='';
		   	return false;  
		   } 
		  
//		   if(tAgentGrade=='B03')
//		   {
//		   alert("资深营业处主任不能参加财补筹备！");
//		   fm.AgentGrade.value='';
//		   fm.AgentGradeName.value='';
//		   return false;
//		   }
//		   if(tAgentGrade=='B12')
//		   {
//		   alert("资深营业区经理不能参加财补筹备！");
//		   fm.AgentGrade.value='';
//		   fm.AgentGradeName.value='';
//		   return false;
//		   }
          var sS = "select '1'  "
		+" from LABranchGroup where 1=1 "
		+" and BranchType = '1' and BranchType2 = '01'  and EndFlag <> 'Y' "
		+" and (state<>'1' or state is null)"
		+" and ApplyGBFlag='Y' "
		+" and branchattr='"+fm.BranchCode.value+"'";
		
		var strQueryResult  = easyQueryVer3(sS, 1, 1, 1);
		if (!strQueryResult)
		{
		   alert("所属团队不为申请“申报标记”团队，不能添加筹备标记！");
		   fm.NoWorkFlag.value='';
		   fm.NoWorkFlagName.value='';
		   fm.TrainDate.value='';
		   return false;
		}                   
		if(!checkTrainDate())
		{  
		   	return false;  
		}
	  }
	  else 	
	  {
	    if(fm.all('TrainDate').value!=null && fm.all('TrainDate').value!='')
	    {	
	  	alert("非筹备人员不能录入筹备开始日期！");
	  	fm.all('TrainDate').value = '';
	  	fm.all('PrepareEndDate').value = '';
	  	fm.all('PreparaGrade').value = '';
	  	fm.all('WageVersion').value = '2015';
		  return false;		
	    }
	    else
	    {
	    	fm.all('TrainDate').value = '';
	  	    fm.all('PrepareEndDate').value = '';
	  	    fm.all('PreparaGrade').value = '';
	    	fm.all('WageVersion').value = '2015';
	    }
	  }
	 } 
	if(!checkAgentGroup2())
	{
		return false;
	}
	if (!confirm('确认您的操作'))
	{
		return false;
	}
	
	if(mOperate==null || mOperate=="")
	{
		if(addClick()== false)
		return false;;
	}
	
	// ↓ *** LiuHao *** 2005-11-02 *** add 加入对出生日的判断 必须在当天之前 ******
	var tDate = new Date();
	if(document.fm.Birthday.value > tDate.getYear()+"-"+tDate.getMonth()+"-"+tDate.getDay())
	{
		alert("[出生日期]小于当前日期，无法录入！");
		return false;
	}
	// ↑ *** LiuHao *** 2005-11-02 *** add 加入对出生日的判断 必须在当天之前 ******
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.OrphanCode.value=tOrphanCode;
	fm.hideOperate.value=mOperate;
	if (fm.hideOperate.value=="")
	{
		alert("操作控制数据丢失！");
	}
	//  showSubmitFrame(mDebug);
	if (mOperate == "INSERT||MAIN")
	fm.all('AgentState').value = fm.all('initAgentState').value;
	//alert("1111");
	if(fm.AgentGrade.value > 'B01')
	{
		strSQL = "select BranchAttr,ManageCom,BranchManager,AgentGroup,BranchManagerName,UpBranch,UpBranchAttr "
		+" from LABranchGroup where 1=1 "
		+" and BranchType = '"+fm.all('BranchType').value+"' and BranchType2 = '"+fm.all('BranchType2').value+"'  and EndFlag <> 'Y' and "
		+"(state<>'1' or state is null)"
		+ getWherePart('BranchAttr','BranchCode');

		var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
		if (!strQueryResult)
		{
			alert('不存在该销售机构！');
			fm.all('BranchCode').value = '';
			fm.all('UpAgent').value = '';
			fm.all('GroupManagerName').value = '';
			fm.all('DepManagerName').value = '';
			fm.all('ManageCom').value = '';
			//fm.all('hideManageCom').value = '';
			fm.all('hideBranchCode').value = '';
			fm.all('ManagerCode').value = '';
			fm.all('upBranchAttr').value = '';
			return false;
		}
		var arr = decodeEasyQueryResult(strQueryResult);
		// alert(arr[0][2]);
		if(arr[0][2]==null || arr[0][2]=="")
		{
			fm.hideIsManager.value = 'true';
		}
		 
	}else{
	  fm.hideIsManager.value = 'false';
	}
	if(fm.AgentGrade.value == 'B21')
	{
		fm.SpeciFlag.value = '01';
	}
	fm.submit(); //提交
	
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	var tAgentCode=fm.all('AgentCode').value;
	//fm.reset();
	//initInpBox();
	fm.all('AgentCode').value=tAgentCode;
	mOperate="";
	showInfo.close();
//	var wageSQL="select lawage.agentcode from lawage,laagent where lawage.agentcode ='"+fm.all('AgentCode').value+"' and lawage.agentcode=laagent.agentcode and laagent.agentstate<'03'";
//	var wageQueryResult=easyQueryVer3(wageSQL,1,1,1)
//	if (wageQueryResult)
//	{
//		fm.all('AgentGrade').disabled = true;
//		fm.all('ManageCom').disabled = true;
//		if (fm.all('IntroAgency').value!=null && fm.all('IntroAgency').value!='')
//		{
//			fm.all('IntroAgency').disabled = true;
//		}
//		fm.all('BranchCode').disabled = true;
//		fm.all('GroupManagerName').disabled = true;
//		fm.all('DepManagerName').disabled = true;
//		//    fm.all('RearAgent').disabled = true;
//		//    fm.all('RearDepartAgent').disabled = true;
//		//    fm.all('RearSuperintAgent').disabled = true;
//		//    fm.all('RearAreaSuperintAgent').disabled = true;
//	}
//	fm.all('hideIsManager').value = false;
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		//parent.fraInterface.initForm();
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		//  showDiv(operateButton,"true");
		//  showDiv(inputButton,"false");
		//执行下一步操作
	}
	
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		//    showDiv(operateButton,"true");
		//    showDiv(inputButton,"false");
		fm.all('AgentGrade').disabled = false;
		fm.all('ManageCom').disabled = false;
		fm.all('IntroAgency').disabled = false;
		fm.all('BranchCode').disabled = false;
		fm.all('GroupManagerName').disabled = false;
		fm.all('DepManagerName').disabled = false;
		initForm();
	}
	catch(re)
	{
		alert("在LAAgent.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}

//取消按钮对应操作
function cancelForm()
{
	//  window.location="../common/html/Blank.html";
	showDiv(operateButton,"true");
	showDiv(inputButton,"false");
}

function verifyRecomrelation()
{
//
    //var strSQL1 = " select CostCenter from labranchgroup where branchattr='"+fm.all('BranchCode').value
            // +"' and BranchType='"+fm.all('BranchType').value+"' and BranchType2='"
             //+fm.all('BranchType2').value+"' with ur ";
   // var strQueryResult1 = easyQueryVer3(strSQL1, 1, 1, 1);

    //var strSQL2 = " select CostCenter from labranchgroup where agentgroup in (select agentgroup from laagent where agentcode='"
             // +fm.all('IntroAgency').value+"' ) with ur " ;
   // var strQueryResult2 = easyQueryVer3(strSQL2, 1, 1, 1);

		//if (strQueryResult1 && strQueryResult2)
		//{
		 //   var arr1= decodeEasyQueryResult(strQueryResult1);
		  //  var arr2= decodeEasyQueryResult(strQueryResult2);
		  //   var strSQL3 = "select 1 from dual where  '"+arr1[0][0]+"'= '"+arr2[0][0]+"' with ur";
		     
		  // var strQueryResult  = easyQueryVer3(strSQL3, 1, 0, 1);
	  if (fm.all('IntroAgency').value!=null && fm.all('IntroAgency').value!=''){
	   var strSQL = " select 1 from labranchgroup where branchattr='"+fm.all('BranchCode').value
             +"' and BranchType='"+fm.all('BranchType').value+"' and BranchType2='"
             +fm.all('BranchType2').value+"' and trim(costcenter) = (select trim(CostCenter) from labranchgroup where agentgroup in (select agentgroup from laagent where agentcode=getagentcode('"
              +fm.all('IntroAgency').value+"' ))) with ur";
              var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
		   if (!strQueryResult)
		    {     

		        alert("不能跨成本中心推荐代理人:销售团队("+fm.all('BranchCode').value
		              +")与推荐人("+fm.all('IntroAgencyName').value+","
		              +fm.all('IntroAgency').value+") 所在团队的成本中心不一致!");
			    fm.all('BranchCode').value = '';
			    fm.all('UpAgent').value = '';
			    fm.all('GroupManagerName').value = '';
			    fm.all('DepManagerName').value = '';
			    fm.all('ManageCom').value = '';
			    //fm.all('hideManageCom').value = '';
			    fm.all('hideBranchCode').value = '';
			    fm.all('ManagerCode').value = '';
			    fm.all('upBranchAttr').value = '';
			    fm.all('IntroAgencyName').value = '';
                fm.all('IntroAgency').value = '';
			    return false;
			}
		}
	return true;
}


//提交前的校验、计算
function beforeSubmit()
{
	 
	if( verifyInput() == false ) return false;
	
	  var strReturn = checkIdNo(trim(fm.all('IDNoType').value),trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
		if (strReturn!='')
		{
			alert('营销员'+strReturn);
			return false;
		}
	if ((trim(fm.all('UpAgent').value)=='')&&(trim(fm.all('AgentGrade').value)<='A08'))
	{
//	alert('请先确定该销售机构的负责人！');
//	return false;
	}
	//检查资格证
	var qualifNo = fm.all("QualifNo").value;
	var grantUnit = fm.all("GrantUnit").value;
	var grantDate = fm.all("GrantDate").value;
	var validStart = fm.all("ValidStart").value;
	var validEnd = fm.all("ValidEnd").value;
	var qualifState = fm.all("QualifState").value;
	if(qualifNo!=""||grantUnit!=""||grantDate!=""||validStart!=""||validEnd!=""||qualifState!=""){
		if(qualifNo==""||grantUnit==""||grantDate==""||validStart==""||validEnd==""||qualifState==""){
			alert("资格证信息必录项不能为空");
			return false;
		}
	}
	//检查担保人信息是否录入
	
	var lineCount = 0;
	var tempObj = fm.all('WarrantorGridNo'); //假设在表单fm中
	if (tempObj == null)
	{
		alert("请填写担保人信息！");
		return false;
	}
	WarrantorGrid.delBlankLine("WarrantorGrid");
	lineCount = WarrantorGrid.mulLineCount;
	if (lineCount == 0)
	{
		alert("请填写担保人信息！");
		return false;
	}
	else{
		var sValue;
		var strChkIdNo;
		if(!WarrantorGrid.checkValue2(WarrantorGrid.name,WarrantorGrid)){
			return false;
		}
		var i;
		var rowNum = WarrantorGrid.mulLineCount;
		var strReturn;

		for(i=0;i<rowNum;i++)
		{
			strReturn=checkIdNo('0',WarrantorGrid.getRowColData(i,4),'',WarrantorGrid.getRowColData(i,2));
			if(strReturn!=''){
				alert('担保人'+strReturn);
				return false;
			}
		}
	}
	fm.all('AgentGrade').disabled = false;
	fm.all('ManageCom').disabled = false;
	fm.all('IntroAgency').disabled = false;
	fm.all('BranchCode').disabled = false;
	fm.all('GroupManagerName').disabled = false;
	fm.all('DepManagerName').disabled = false;
	//    fm.all('RearAgent').disabled = false;
	//    fm.all('RearDepartAgent').disabled = false;
	//    fm.all('RearSuperintAgent').disabled = false;
	//    fm.all('RearAreaSuperintAgent').disabled = false;

	//alert(1);
	if(fm.AgentGrade.value < 'B05' && fm.BranchCode.value.length != 14&& fm.BranchCode.value.length != 15)
	{
		alert('职级与机构级别不匹配！');
		if (mOperate=="UPDATE||MAIN")
		{
			fm.all('AgentGrade').disabled = true;
			fm.all('ManageCom').disabled = true;
			if (fm.all('IntroAgency').value!=null && fm.all('IntroAgency').value!='')
			{
				if(manageCom!='86'){
					fm.all('IntroAgency').disabled = true;
				}
			}
			fm.all('BranchCode').disabled = true;
			fm.all('GroupManagerName').disabled = true;
			fm.all('DepManagerName').disabled = true;
		}
		return false;
	}
	if(fm.AgentGrade.value == 'B11' && fm.BranchCode.value.length != 12)
	{
		alert('职级与机构级别不匹配！');
		if (mOperate=="UPDATE||MAIN")
		{
			fm.all('AgentGrade').disabled = true;
			fm.all('ManageCom').disabled = true;
			if (fm.all('IntroAgency').value!=null && fm.all('IntroAgency').value!='')
			{
				if(manageCom!='86'){
					fm.all('IntroAgency').disabled = true;
				}
			}
			fm.all('BranchCode').disabled = true;
			fm.all('GroupManagerName').disabled = true;
			fm.all('DepManagerName').disabled = true;
		}
		return false;
	}
	if (fm.all('initAgentState').value != '02')
	{
		if(fm.AgentGrade.value == 'B21'&& fm.all('SpeciFlag').value=='00')
		{
			alert('营业服务部经理请设置为特殊人员！');
			return false;
		}
	}
	if(fm.AgentGrade.value == 'B21' && fm.BranchCode.value.length != 10)
	{
		alert('职级与机构级别不匹配！');
		if (mOperate=="UPDATE||MAIN")
		{
			fm.all('AgentGrade').disabled = true;
			fm.all('ManageCom').disabled = true;
			if (fm.all('IntroAgency').value!=null && fm.all('IntroAgency').value!='')
			{
				fm.all('IntroAgency').disabled = true;
			}
			fm.all('BranchCode').disabled = true;
			fm.all('GroupManagerName').disabled = true;
			fm.all('DepManagerName').disabled = true;
		}
		return false;
	}
    if (fm.all('initOperate').value == 'INSERT')
	{
		mOperate="INSERT||MAIN";
		}
	if(fm.all('BranchType').value=='1' &&　fm.all('BranchType2').value=='01' && mOperate=="INSERT||MAIN" )
	{
	   var tAgentGrade=fm.all('AgentGrade').value;
	   var tGroupManagerName=fm.all('GroupManagerName').value;
	   var tDepManagerName=fm.all('DepManagerName').value;
	   var tMinister=fm.all('Minister').value;
	   if( (tAgentGrade >'B01' && tAgentGrade<'B11') && (tGroupManagerName !='' && tGroupManagerName!=null ))
	   {
	   	alert("该团队已有处经理，不能在录入处经理系列职级!");
	   	return false ;		
	   }
	   if( (tAgentGrade >='B11' && tAgentGrade<'B21') && (tDepManagerName !='' && tDepManagerName!=null ))
	   {
	   	alert("该团队已有区经理，不能在录入区经理系列职级!");
	   	return false ;		
	   }
	   if( tAgentGrade=='B21' && (tMinister !='' && tMinister!=null ))
	   {
	   	alert("该团队已有部经理，不能在录入部经理系列职级!");
	   	return false ;		
	   }
	   if(fm.all('ManageCom').value==null || fm.all('ManageCom').value==''){
	   		alert("没有得到管理机构编码，操作失败。");
	   		return false ;
	   }
	   var strSQL2 = "select agentstate  from LAAgent where 1=1 "
	   +" and managecom='"+fm.all('ManageCom').value+"' "
	+ getWherePart('BranchType')
	+ getWherePart('BranchType2')
	+ getWherePart('IDNoType')
	+ getWherePart('IDNo');
	//alert(strSQL);	
	var strQueryResult2  = easyQueryVer3(strSQL2, 1, 1, 1);
	if (strQueryResult2)
	{
	var arr= decodeEasyQueryResult(strQueryResult2);
	if(arr[0][0]>='06'){
	  	alert("此人在该机构做过离职，请在二次增员界面进行增员操作！");
	  	fm.all('IDNo').value = '';
	 	return false;
	  }
	
	}	  
	}
	if(!verifyRecomrelation()){
		
	    return false;
	}
	  return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
	//下面增加相应的代码
	if (fm.all('initOperate').value == 'INSERT')
	{
		mOperate="INSERT||MAIN";
		//showDiv(operateButton,"false");
		//showDiv(inputButton,"true");
		//fm.all('AgentCode').value = '';
		if (fm.all('AgentCode').value !='')
		//resetForm();
		return true;
	}
	else{
		alert('在此不能新增！');
		return false;
	}
	
	return true;
}

function checkEmployDate(){
	var tCurDate=new Date();
	var tEmpDate=fm.all('EmployDate').value;
	var arr1=tEmpDate.split("-");

	var tEmpDate=new Date(arr1[0],arr1[1],arr1[2]);
	//alert(parseInt("22"));
	
	//if(tEmpDate.getYear()<tCurDate.getYear()){
		//alert(parseInt(tEmpDate.getYear()));
	//}
	
	if(tEmpDate.getYear()<tCurDate.getYear()
		||(tEmpDate.getYear()==tCurDate.getYear() && tEmpDate.getMonth()<(tCurDate.getMonth()+1))
		||(tEmpDate.getYear()==tCurDate.getYear() && tEmpDate.getMonth()==(tCurDate.getMonth()+1) &&　tEmpDate.getDate()<tCurDate.getDate())){
		alert("入司时间不应早于当前日期！操作失败。");
		return false;
	}
	return true;
}


// 校验筹备日期
function checkTrainDate()
{
	if ((fm.all("TrainDate").value == null)||(fm.all("TrainDate").value == ''))
	{
		alert('筹备人员必须录入筹备开始日期');
		return false;
	}else {
 	var tEmpDate=fm.all('EmployDate').value;
    var tTrainDate=fm.all('TrainDate').value;
    if (tEmpDate >tTrainDate||tEmpDate <tTrainDate)
	 {
       alert('筹备开始日期必须等于入司日期！');
       fm.all('TrainDate').value='';
       fm.all('NoWorkFlag').value='';
       fm.all('NoWorkFlagName').value='';
	   return false	;
     }
    if ('2009-10-01' > tTrainDate)
	 {
       alert('筹备开始日期必须大于等于2009-10-01！');
       fm.all('TrainDate').value='';
       fm.all('NoWorkFlag').value='';
       fm.all('NoWorkFlagName').value='';
	   return false	;
     }
     // add new 
     if(tTrainDate >'2013-12-15')
     {
       alert('依据个险总部最新需求办法，此版筹备基本法截止日期为2013-12-15。');
       fm.all('TrainDate').value='';
       fm.all('NoWorkFlag').value='';
       fm.all('NoWorkFlagName').value='';
	   return false	;
     }
  }
  //设置筹备相关属性
    fm.all('PrepareEndDate').value = '';
	fm.all('PreparaGrade').value = fm.all('AgentGrade').value ;
	fm.all('WageVersion').value = '2015A';
	
	return true;
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (fm.all('initOperate').value == 'INSERT')
	{
		alert('在此不能修改！');
	}
	else{
		if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
		{
			alert('请先查询出要修改的代理人记录！');
			fm.all('AgentCode').focus();
		}
		else{
			if ((fm.all("AgentState").value >='03'))
			{
				alert('代理人已离职,不能修改他(她)的信息!');
				return false;
			}
			if (confirm("您确实想修改该记录吗?"))
			{
				mOperate="UPDATE||MAIN";
				submitForm();
			}
			else{
				mOperate="";
				alert("您取消了修改操作！");
			}
		}
	}
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	//下面增加相应的代码
	//mOperate="QUERY||MAIN";
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2 = fm.all('BranchType2').value;
	var tAgentState = fm.all('initAgentState').value;
	if (tAgentState=='02')
	{//二次增员
		showInfo=window.open("./LAAgentSecondQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
	}
	else{
		showInfo=window.open("./LAAgentQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
	}
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if (fm.all('initOperate').value == 'INSERT')
	{
		alert('在此不能删除！');
	}
	else{
		if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
		{
			alert('请先查询出要删除的代理人记录！');
			fm.all('AgentCode').focus();
		}
		else{
			//下面增加相应的删除代码
			if (confirm("您确实想删除该记录吗?"))
			{
				mOperate="DELETE||MAIN";
				submitForm();
			}
			else{
				mOperate="";
				alert("您取消了删除操作！");
			}
		}
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}

function changeGroup()
{
	fm.all('AgentGroup2').value="";
	fm.all('Group2ManagerName').value="";
	if (getWherePart('BranchCode')=='')
	return false;
	var tAgentGrade = trim(fm.all('AgentGrade').value);
	if (tAgentGrade==null ||tAgentGrade=='')
	{
		alert('请先录入代理人职级！');
		fm.all('BranchCode').value = '';
		return false;
	}
	var strSQL = "";
	strSQL = "select BranchAttr,ManageCom,BranchManager,AgentGroup,BranchManagerName,UpBranch,UpBranchAttr "
	+" from LABranchGroup where 1=1 "
	+" and BranchType = '"+fm.all('BranchType').value+"' and BranchType2 = '"+fm.all('BranchType2').value+"'  and EndFlag <> 'Y' and "
	+"(state<>'1' or state is null)"
	+ getWherePart('BranchAttr','BranchCode');
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	if (!strQueryResult)
	{
		alert('不存在该销售机构！');
		fm.all('BranchCode').value = '';
		fm.all('UpAgent').value = '';
		fm.all('GroupManagerName').value = '';
		fm.all('DepManagerName').value = '';
		fm.all('ManageCom').value = '';
		//fm.all('hideManageCom').value = '';
		fm.all('hideBranchCode').value = '';
		fm.all('ManagerCode').value = '';
		fm.all('upBranchAttr').value = '';
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	//判断管理人员
	fm.all('ManageCom').value = trim(arr[0][1]);
	fm.all('hideBranchCode').value = trim(arr[0][0]);
	fm.all('ManagerCode').value = trim(arr[0][2]);
	fm.all('upBranchAttr').value = trim(arr[0][6]);
	var b = trim(arr[0][2]);
	var tUpBranch = trim(arr[0][0]);//上级机构
	var up = trim(arr[0][5]);
	var newUpBranchattr=trim(arr[0][6]);
	if(up != '')
	{
		var strSQL = "";
		strSQL = "select BranchAttr from labranchgroup where agentgroup = '"+up+"'";
		strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
		if (!strQueryResult)
		{
			alert('查找上级销售机构出错！');
			fm.all('BranchCode').value = '';
			fm.all('UpAgent').value = '';
			fm.all('GroupManagerName').value = '';
			fm.all('DepManagerName').value = '';
			fm.all('ManageCom').value = '';
			fm.all('hideBranchCode').value = '';
			fm.all('ManagerCode').value = '';
			fm.all('upBranchAttr').value = '';
			return false;
		}
		arr1 = decodeEasyQueryResult(strQueryResult);
		up = arr1[0][0];
	}

	var a = fm.BranchCode.value;
	if (tAgentGrade <= 'B01')
	{
		if(a.length != 14&&a.length != 15)
		{
			alert("代理人职级"+tAgentGrade+"与销售机构级别不符合要求");//员工级别应该任营业处
			fm.all('BranchCode').value = '';
			fm.all('UpAgent').value = '';
			fm.all('GroupManagerName').value = '';
			fm.all('DepManagerName').value = '';
			fm.all('ManageCom').value = '';
			//fm.all('hideManageCom').value = '';
			fm.all('hideBranchCode').value = '';
			fm.all('ManagerCode').value = '';
			fm.all('upBranchAttr').value = '';
			fm.all('Minister').value = '';
			return false;
		}
//	if(b == ''&& tAgentGrade < 'B01')//B01不当经理，2007-3-15
//	{
//		alert('销售机构没有经理，请先录入经理！');
//		fm.all('BranchCode').value = '';
//		fm.all('UpAgent').value = '';
//		fm.all('GroupManagerName').value = '';
//		fm.all('DepManagerName').value = '';
//		fm.all('ManageCom').value = '';
//		//fm.all('hideManageCom').value = '';
//		fm.all('hideBranchCode').value = '';
//		fm.all('ManagerCode').value = '';
//		fm.all('upBranchAttr').value = '';
//		fm.all('Minister').value = '';
//		return false;
//	}
		//alert(arr[0][4]);
		fm.all('GroupManagerName').value = trim(arr[0][4]); //处经理
		fm.all('UpAgent').value = trim(arr[0][2]);
		if(up.length > 10)
		{
			tUpBranch = tUpBranch.substr(0,12);
			//查询上级机构
			strSQL = "select AgentGroup,BranchManager,BranchManagerName from LABranchGroup where 1=1 and BranchType = '"+fm.all('BranchType').value+"' and BranchType2='"+fm.all('BranchType2').value+"' "
			+" and EndFlag <> 'Y' and branchAttr = '"+tUpBranch+"' and (state<>'1' or state is null)";
			//fm.IntroAgency.value = strSQL;
			strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
			if (!strQueryResult)
			{
				alert('所录销售机构的上级机构(区级)不存在,无法显示经理！');
				fm.all('BranchCode').value = '';
				fm.all('UpAgent').value = '';
				fm.all('GroupManagerName').value = '';
				fm.all('DepManagerName').value = '';
				fm.all('ManageCom').value = '';
				//fm.all('hideManageCom').value = '';
				fm.all('hideBranchCode').value = '';
				fm.all('ManagerCode').value = '';
				fm.all('upBranchAttr').value = '';
				fm.all('Minister').value = '';
				return false;
			}
			else{
				arr = decodeEasyQueryResult(strQueryResult);
//			if(trim(arr[0][1]) == '')
//			{
//				alert('上级销售机构(区级)没有经理，请先录入经理！');
//				fm.all('BranchCode').value = '';
//				fm.all('UpAgent').value = '';
//				fm.all('GroupManagerName').value = '';
//				fm.all('DepManagerName').value = '';
//				fm.all('ManageCom').value = '';
//				//fm.all('hideManageCom').value = '';
//				fm.all('hideBranchCode').value = '';
//				fm.all('ManagerCode').value = '';
//				fm.all('upBranchAttr').value = '';
//				fm.all('Minister').value = '';
//				return false;
//			}
			fm.all('DepManagerName').value = trim(arr[0][2]);
			}
			tUpBranch = tUpBranch.substr(0,10);
			//查询上级机构
			strSQL = "select AgentGroup,BranchManager,BranchManagerName from LABranchGroup where 1=1 and BranchType = '"+fm.all('BranchType').value+"' and BranchType2='"+fm.all('BranchType2').value+"' "
			+" and EndFlag <> 'Y' and branchAttr = '"+tUpBranch+"' and (state<>'1' or state is null)";
			//fm.IntroAgency.value = strSQL;
			strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
			if (!strQueryResult)
			{
				alert('所录销售机构的上级机构(部级)不存在,无法显示经理！');
				fm.all('BranchCode').value = '';
				fm.all('UpAgent').value = '';
				fm.all('GroupManagerName').value = '';
				fm.all('DepManagerName').value = '';
				fm.all('ManageCom').value = '';
				//fm.all('hideManageCom').value = '';
				fm.all('hideBranchCode').value = '';
				fm.all('ManagerCode').value = '';
				fm.all('upBranchAttr').value = '';
				fm.all('Minister').value = '';
				return false;
			}
			else{
				arr = decodeEasyQueryResult(strQueryResult);
				//			 	if(trim(arr[0][1]) == '')
				//			 	{
				//			  alert('上级销售机构(部级)没有经理，请先录入经理！');
				//			  fm.all('BranchCode').value = '';
				//			  fm.all('UpAgent').value = '';
				//			  fm.all('GroupManagerName').value = '';
				//			  fm.all('DepManagerName').value = '';
				//			  fm.all('ManageCom').value = '';
				//			  //fm.all('hideManageCom').value = '';
				//			  fm.all('hideBranchCode').value = '';
				//			  fm.all('ManagerCode').value = '';
				//			  fm.all('upBranchAttr').value = '';
				//			  fm.all('Minister').value = '';
				//			 	return false;
				//			}
				fm.all('Minister').value = trim(arr[0][2]);
			}
		}
		else{
			tUpBranch = tUpBranch.substr(0,10);
			//查询上级机构
			strSQL = "select AgentGroup,BranchManager,BranchManagerName from LABranchGroup where 1=1 and BranchType = '"+fm.all('BranchType').value+"' and BranchType2='"+fm.all('BranchType2').value+"'"
			+" and EndFlag <> 'Y' and branchAttr = '"+tUpBranch+"' and (state<>'1' or state is null)";
			//fm.IntroAgency.value = strSQL;
			strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
			if (!strQueryResult)
			{
				alert('所录销售机构的上级机构(部级)不存在,无法显示经理！');
				fm.all('BranchCode').value = '';
				fm.all('UpAgent').value = '';
				fm.all('GroupManagerName').value = '';
				fm.all('DepManagerName').value = '';
				fm.all('ManageCom').value = '';
				//fm.all('hideManageCom').value = '';
				fm.all('hideBranchCode').value = '';
				fm.all('ManagerCode').value = '';
				fm.all('upBranchAttr').value = '';
				fm.all('Minister').value = '';
				return false;
			}
			else{
				arr = decodeEasyQueryResult(strQueryResult);
				//      if(trim(arr[0][1]) == '')
				//      {
				//		    alert('上级销售机构(部级)没有经理，请先录入经理！');
				//				fm.all('BranchCode').value = '';
				//				fm.all('UpAgent').value = '';
				//				fm.all('GroupManagerName').value = '';
				//				fm.all('DepManagerName').value = '';
				//				fm.all('ManageCom').value = '';
				//				//fm.all('hideManageCom').value = '';
				//				fm.all('hideBranchCode').value = '';
				//				fm.all('ManagerCode').value = '';
				//				fm.all('upBranchAttr').value = '';
				//				fm.all('Minister').value = '';
				//				return false;
				//      }
				fm.all('Minister').value = trim(arr[0][2]);
			}
		}
	}
	if (tAgentGrade > 'B01')
	{
		if(b != '')
		{
			alert('销售机构已有经理！');
			fm.all('BranchCode').value = '';
			fm.all('UpAgent').value = '';
			fm.all('GroupManagerName').value = '';
			fm.all('DepManagerName').value = '';
			fm.all('ManageCom').value = '';
			//fm.all('hideManageCom').value = '';
			fm.all('hideBranchCode').value = '';
			fm.all('ManagerCode').value = '';
			fm.all('upBranchAttr').value = '';
			fm.all('Minister').value = '';
			return false;
		}
		if(tAgentGrade > 'B01' && tAgentGrade <= 'B04')
		{
			if(a.length != 14&&a.length != 15)
			{
				alert("代理人职级"+tAgentGrade+"与销售机构级别不符合要求");//处经理级别应该任营业处的经理
				fm.all('BranchCode').value = '';
				fm.all('UpAgent').value = '';
				fm.all('GroupManagerName').value = '';
				fm.all('DepManagerName').value = '';
				fm.all('ManageCom').value = '';
				//fm.all('hideManageCom').value = '';
				fm.all('hideBranchCode').value = '';
				fm.all('ManagerCode').value = '';
				fm.all('upBranchAttr').value = '';
				fm.all('Minister').value = '';
				return false;
			}
			//var tUpBranch = trim(arr[0][5]);
			//alert(tUpBranch);
			if(up.length > 10)
			{
				tUpBranch = tUpBranch.substr(0,12);
				//查询上级机构
				strSQL = "select AgentGroup,BranchManager,BranchManagerName from LABranchGroup where 1=1 and BranchType = '"+fm.all('BranchType').value+"' and BranchType2='"+fm.all('BranchType2').value+"' "
				+" and EndFlag <> 'Y' and branchAttr = '"+tUpBranch+"' and (state<>'1' or state is null)";
				//fm.IntroAgency.value = strSQL;
				strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
				if (!strQueryResult)
				{
					alert('所录销售机构的上级机构(区级)不存在,无法显示经理！');
					fm.all('BranchCode').value = '';
					fm.all('UpAgent').value = '';
					fm.all('GroupManagerName').value = '';
					fm.all('DepManagerName').value = '';
					fm.all('ManageCom').value = '';
					//fm.all('hideManageCom').value = '';
					fm.all('hideBranchCode').value = '';
					fm.all('ManagerCode').value = '';
					fm.all('upBranchAttr').value = '';
					fm.all('Minister').value = '';
					return false;
				}
				else{
					arr = decodeEasyQueryResult(strQueryResult);
					if(trim(arr[0][1]) == '' && newUpBranchattr=='1')
					{
						alert('上级销售机构(区级)没有经理，请先录入经理！');
						fm.all('BranchCode').value = '';
						fm.all('UpAgent').value = '';
						fm.all('GroupManagerName').value = '';
						fm.all('DepManagerName').value = '';
						fm.all('ManageCom').value = '';
						//fm.all('hideManageCom').value = '';
						fm.all('hideBranchCode').value = '';
						fm.all('ManagerCode').value = '';
						fm.all('upBranchAttr').value = '';
						fm.all('Minister').value = '';
						return false;
					}
					fm.all('UpAgent').value = trim(arr[0][1]);
					fm.all('DepManagerName').value = trim(arr[0][2]);
				}
				tUpBranch = tUpBranch.substr(0,10);
				//查询上级机构
				strSQL = "select AgentGroup,BranchManager,BranchManagerName from LABranchGroup where 1=1 and BranchType = '"+fm.all('BranchType').value+"' and BranchType2='"+fm.all('BranchType2').value+"'"
				+" and EndFlag <> 'Y' and branchAttr = '"+tUpBranch+"' and (state<>'1' or state is null)";
				//fm.IntroAgency.value = strSQL;
				strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
				if (!strQueryResult)
				{
					alert('所录销售机构的上级机构(部级)不存在,无法显示经理！');
					fm.all('BranchCode').value = '';
					fm.all('UpAgent').value = '';
					fm.all('GroupManagerName').value = '';
					fm.all('DepManagerName').value = '';
					fm.all('ManageCom').value = '';
					//fm.all('hideManageCom').value = '';
					fm.all('hideBranchCode').value = '';
					fm.all('ManagerCode').value = '';
					fm.all('upBranchAttr').value = '';
					fm.all('Minister').value = '';
					return false;
				}
				else{
					arr = decodeEasyQueryResult(strQueryResult);
					//		      if(trim(arr[0][1]) == '')
					//		      {
					//			      alert('上级销售机构(部级)没有经理，请先录入经理！');
					//					  fm.all('BranchCode').value = '';
					//					  fm.all('UpAgent').value = '';
					//					  fm.all('GroupManagerName').value = '';
					//					  fm.all('DepManagerName').value = '';
					//					  fm.all('ManageCom').value = '';
					//					  //fm.all('hideManageCom').value = '';
					//					  fm.all('hideBranchCode').value = '';
					//					  fm.all('ManagerCode').value = '';
					//					  fm.all('upBranchAttr').value = '';
					//					  fm.all('Minister').value = '';
					//					  return false;
					//	      	}
					fm.all('Minister').value = trim(arr[0][2]);
				}
			}
			else{
				tUpBranch = tUpBranch.substr(0,10);
				//查询上级机构
				strSQL = "select AgentGroup,BranchManager,BranchManagerName from LABranchGroup where 1=1 and BranchType = '"+fm.all('BranchType').value+"' and BranchType2='"+fm.all('BranchType2').value+"'"
				+" and EndFlag <> 'Y' and branchAttr = '"+tUpBranch+"' and (state<>'1' or state is null)";
				//fm.IntroAgency.value = strSQL;
				strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
				if (!strQueryResult)
				{
					alert('所录销售机构的上级机构(部级)不存在,无法显示经理！');
					fm.all('BranchCode').value = '';
					fm.all('UpAgent').value = '';
					fm.all('GroupManagerName').value = '';
					fm.all('DepManagerName').value = '';
					fm.all('ManageCom').value = '';
					//fm.all('hideManageCom').value = '';
					fm.all('hideBranchCode').value = '';
					fm.all('ManagerCode').value = '';
					fm.all('upBranchAttr').value = '';
					fm.all('Minister').value = '';
					return false;
				}
				else{
					arr = decodeEasyQueryResult(strQueryResult);
					//	      if(trim(arr[0][1]) == '')
					//	      {
					//		      	alert('上级销售机构(部级)没有经理，请先录入经理！');
					//					  fm.all('BranchCode').value = '';
					//					  fm.all('UpAgent').value = '';
					//					  fm.all('GroupManagerName').value = '';
					//					  fm.all('DepManagerName').value = '';
					//					  fm.all('ManageCom').value = '';
					//					  //fm.all('hideManageCom').value = '';
					//					  fm.all('hideBranchCode').value = '';
					//					  fm.all('ManagerCode').value = '';
					//					  fm.all('upBranchAttr').value = '';
					//					  fm.all('Minister').value = '';
					//					  return false;
					//      		}
					fm.all('UpAgent').value = trim(arr[0][1]);
					fm.all('Minister').value = trim(arr[0][2]);
				}
			}
		}
		if(tAgentGrade == 'B11')
		{
			if(a.length != 12)
			{
				alert("代理人职级B11与销售机构级别不符合要求");//区经理级别应该任营业区的经理
				fm.all('BranchCode').value = '';
				fm.all('UpAgent').value = '';
				fm.all('GroupManagerName').value = '';
				fm.all('DepManagerName').value = '';
				fm.all('ManageCom').value = '';
				//fm.all('hideManageCom').value = '';
				fm.all('hideBranchCode').value = '';
				fm.all('ManagerCode').value = '';
				fm.all('upBranchAttr').value = '';
				fm.all('Minister').value = '';
				return false;
			}
			//var tUpBranch = trim(arr[0][0]);
			tUpBranch = tUpBranch.substr(0,10);
			//查询上级机构
			strSQL = "select AgentGroup,BranchManager,BranchManagerName from LABranchGroup where 1=1 and BranchType = '"+fm.all('BranchType').value+"' and BranchType2='"+fm.all('BranchType2').value+"'"
			+" and EndFlag <> 'Y' and branchAttr = '"+tUpBranch+"' and (state<>'1' or state is null)";
			//fm.IntroAgency.value = strSQL;
			strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
			if (!strQueryResult)
			{
				alert('所录销售机构的上级机构(部级)不存在,无法显示经理！');
				fm.all('BranchCode').value = '';
				fm.all('UpAgent').value = '';
				fm.all('GroupManagerName').value = '';
				fm.all('DepManagerName').value = '';
				fm.all('ManageCom').value = '';
				//fm.all('hideManageCom').value = '';
				fm.all('hideBranchCode').value = '';
				fm.all('ManagerCode').value = '';
				fm.all('upBranchAttr').value = '';
				fm.all('Minister').value = '';
				return false;
			}
			else{
				arr = decodeEasyQueryResult(strQueryResult);
				//      		if(trim(arr[0][1]) == '')
				//      		{
				//			      alert('上级销售机构(部级)没有经理，请先录入经理！');
				//					  fm.all('BranchCode').value = '';
				//					  fm.all('UpAgent').value = '';
				//					  fm.all('GroupManagerName').value = '';
				//					  fm.all('DepManagerName').value = '';
				//					  fm.all('ManageCom').value = '';
				//					  //fm.all('hideManageCom').value = '';
				//					  fm.all('hideBranchCode').value = '';
				//					  fm.all('ManagerCode').value = '';
				//					  fm.all('upBranchAttr').value = '';
				//					  fm.all('Minister').value = '';
				//  					return false;
				//      		}
				fm.all('UpAgent').value = trim(arr[0][1]);
				fm.all('Minister').value = trim(arr[0][2]);
			}
		}
		if(tAgentGrade == 'B21')
		{
			if(a.length != 10)
			{
				alert("代理人职级B21与销售机构级别不符合要求");//区经理级别应该任营业区的经理
				fm.all('BranchCode').value = '';
				fm.all('UpAgent').value = '';
				fm.all('GroupManagerName').value = '';
				fm.all('DepManagerName').value = '';
				fm.all('ManageCom').value = '';
				//fm.all('hideManageCom').value = '';
				fm.all('hideBranchCode').value = '';
				fm.all('ManagerCode').value = '';
				fm.all('upBranchAttr').value = '';
				fm.all('Minister').value = '';
				return false;
			}
			fm.all('GroupManagerName').value ='';
			fm.all('DepManagerName').value ='';
			fm.all('Minister').value = '';
			fm.all('UpAgent').value = '';
			fm.SpeciFlag.value = '01';
		}
		fm.all('hideIsManager').value = 'true';
		//alert(fm.all('hideIsManager').value);
	}
	return true;
}

function judgeManager(cManager,cAgentGrade,cZSValue)
{
	//alert(cManager+":"+cAgentGrade+":"+cZSValue);
	//alert(fm.all('UpAgent').value);
	if ((cManager==null)||(trim(cManager)==''))
	{
		//增员非经理级的的代理人不能操作
		if (cAgentGrade <= 'A08')
		{
			alert('必须增员该销售机构的管理人员！');
			fm.all('AgentGrade').value = '';
			return false;
		}
		//检验已经设定其上级代理人
		if(cAgentGrade <= 'B04' && cAgentGrade >= 'B01')
		{
			if(fm.all('UpAgent').value == '')
			{
				alert('必须增员该销售机构的上级机构的管理人员！');
				fm.all('AgentGrade').value = '';
				return false;;
			}

			var str = "是否指定该代理人为该销售单位的管理人员？";
			if (confirm(str))
			fm.all('hideIsManager').value = 'true';
			else{
				fm.all('hideIsManager').value  = 'false';
				fm.all('BranchCode').value     = '';
				//fm.all('hideBranchCode').value = '';
				return false;
			}
		}
	}
	else{
		//alert('  '+cManager);
		var tACode = fm.all('AgentCode').value;
		if (tACode == null)
		tACode = '';
		if ((cAgentGrade > 'A08')&&(tACode != cManager))
		{
			alert('该销售单位已存在管理人员！');
			fm.all('BranchCode').value = '';
			fm.all('UpAgent').value = '';
			fm.all('GroupManagerName').value = '';
			fm.all('DepManagerName').value = '';
			//fm.all('hideManageCom').value = '';
			fm.all('ManageCom').value = '';
			fm.all('hideBranchCode').value = '';
			fm.all('ManagerCode').value = '';
			fm.all('upBranchAttr').value = '';
			return false;
		}
		if (cAgentGrade > 'A08')
		fm.all('hideIsManager').value = 'true';
		else
			fm.all('hideIsManager').value  = 'false';
	}
	return true;
}

	function changeIntroAgency()
	{
		if (getWherePart('IntroAgency')=='')
		return false;
		var tagentcode=fm.all('AgentCode').value;
		var tintroagency=fm.all('IntroAgency').value;
		if (tintroagency==tagentcode)
		{
			alert('不能与原代理人编码相同!')
			fm.all('IntroAgency').value = '';
			return false;
		}		
		var strSQL1 = "";
		strSQL1 = "select 1 from LAwage  where 1=1 and agentcode=getAgentCode('"+tagentcode+"')";
		var strQueryResult1 = easyQueryVer3(strSQL1, 1, 1, 1);
		//alert(strQueryResult1);	
		if (strQueryResult1)
		{
			alert('该业务员已经进行过佣金计算,不能修改推荐人!');
			fm.all('IntroAgency').value = '';
			return false;
		}
		var strSQL = "";
		strSQL = "select getUniteCode(AgentCode),ManageCom, AgentGroup,name from LAAgent  where 1=1 "
		+ "and (AgentState is null or AgentState < '03') "
		+ getWherePart('groupAgentCode','IntroAgency');
		var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
		if (!strQueryResult)
		{
			alert('不存在该业务员！');
			fm.all('IntroAgency').value = '';
			return false;
		}
		var arr = decodeEasyQueryResult(strQueryResult);
		fm.all('IntroAgencyName').value = arr[0][3];
		return true;
	}
	
	

	
	function afterQuery(arrQueryResult)
	{
		var arrResult = new Array();

		resetForm();

		if( arrQueryResult != null )
		{
			arrResult = arrQueryResult;
			fm.all('AgentCode').value = arrResult[0][0];
			//alert(arrResult[0][0]);
			//fm.all('Password').value = arrResult[0][3];
			//fm.all('EntryNo').value = arrResult[0][4];
			fm.all('Name').value = arrResult[0][5];
			fm.all('Sex').value = arrResult[0][6];
			fm.all('Birthday').value = arrResult[0][7];
			fm.all('NativePlace').value = arrResult[0][8];
			fm.all('Nationality').value = arrResult[0][9];
			//fm.all('Marriage').value = arrResult[0][10];
			//fm.all('CreditGrade').value = arrResult[0][11];
			//fm.all('HomeAddressCode').value = arrResult[0][12];
			fm.all('HomeAddress').value = arrResult[0][13];
			//fm.all('PostalAddress').value = arrResult[0][14];
			fm.all('ZipCode').value = arrResult[0][15];
			fm.all('Phone').value = arrResult[0][16];
			fm.all('BP').value = arrResult[0][17];
			fm.all('Mobile').value = arrResult[0][18];
			fm.all('EMail').value = arrResult[0][19];
			//fm.all('MarriageDate').value = arrResult[0][20];
			fm.all('IDNo').value = arrResult[0][21];
			//fm.all('Source').value = arrResult[0][22];
			//fm.all('BloodType').value = arrResult[0][23];
			fm.all('PolityVisage').value = arrResult[0][24];
			fm.all('Degree').value = arrResult[0][25];
			fm.all('GraduateSchool').value = arrResult[0][26];
			fm.all('Speciality').value = arrResult[0][27];
			fm.all('PostTitle').value = arrResult[0][28];
			//fm.all('ForeignLevel').value = arrResult[0][29];
			//fm.all('WorkAge').value = arrResult[0][30];
			fm.all('OldCom').value = arrResult[0][31];
			fm.all('OldOccupation').value = arrResult[0][32];
			fm.all('HeadShip').value = arrResult[0][33];
			//fm.all('RecommendAgent').value = arrResult[0][34];
			//fm.all('Business').value = arrResult[0][35];
			//fm.all('SaleQuaf').value = arrResult[0][36];
			//fm.all('QuafNo').value = arrResult[0][37];
			//fm.all('QuafStartDate').value = arrResult[0][38];
			//fm.all('QuafEndDate').value = arrResult[0][39];
			//fm.all('DevNo1').value = arrResult[0][40];
			//fm.all('DevNo2').value = arrResult[0][41];
			//fm.all('RetainContNo').value = arrResult[0][42];
			//fm.all('AgentKind').value = arrResult[0][43];
			//fm.all('DevGrade').value = arrResult[0][44];
			//fm.all('InsideFlag').value = arrResult[0][45];
			//fm.all('FullTimeFlag').value = arrResult[0][46];
			//fm.all('NoWorkFlag').value = arrResult[0][47];
			fm.all('TrainPeriods').value = arrResult[0][73];
			fm.all('EmployDate').value = arrResult[0][49];
			//fm.all('InDueFormDate').value = arrResult[0][50];
			//fm.all('OutWorkDate').value = arrResult[0][51];
			//fm.all('Approver').value = arrResult[0][57];
			//fm.all('ApproveDate').value = arrResult[0][58];
			fm.all('AssuMoney').value = arrResult[0][59];
			fm.all('AgentState').value = arrResult[0][61];
			//fm.all('QualiPassFlag').value = arrResult[0][62];
			//fm.all('SmokeFlag').value = arrResult[0][63];
			fm.all('RgtAddress').value = arrResult[0][64];
			fm.all('BankCode').value = arrResult[0][65];
			//fm.all('BankAccNo').value = arrResult[0][66];
			fm.all('Remark').value = arrResult[0][60];
			fm.all('Operator').value = arrResult[0][67];
			fm.all('IDNoType').value = arrResult[0][78];
		
			
			if (fm.all('initAgentState').value != '02')
			{
					if(arrResult[0][90] == '' || arrResult[0][90] == null)
					{
						fm.all('SpeciFlag').value ='00';	
					}
					else
						fm.all('SpeciFlag').value = arrResult[0][90];
			
						if(fm.all('SpeciFlag').value=='01')
					{
						fm.all('SpeciFlagName').value='是';
					}
					else 
						if(fm.all('SpeciFlag').value=='00')
					{
						fm.all('SpeciFlagName').value='否';
					}
					else
						fm.all('SpeciFlagName').value='否';
			}
			
				
			showOneCodeNametoAfter('idtype','IDNoType','IDNoTypeName');
			fm.all('ManageCom').value = arrResult[0][2];
			fm.all('IntroAgency').value = arrResult[0][80];	
			fm.all('hideBranchCode').value = arrResult[0][83];
			
			//二次增员时，不显示代理人以前的行政信息
			if (fm.all('initAgentState').value != '02')
			{
				//显式机构代码
				//79-BranchManager 80-IntroAgency 81-AgentSeries 82-AgentGrade 83-BranchAttr(所属组的显式代码)
				//84-AscriptSeries 85-BranchLevel 86-upBranch 87-BranchManagerName 88-upBranchAttr 89-BranchAttr(所属组的区级显式代码) 90-SpeciFlag
				//行政信息
				//    alert('agentgroup:'+arrResult[0][81]);
				
				//fm.all('hideManageCom').value = arrResult[0][2];
				


				fm.all('AgentSeries').value = arrResult[0][81];
				fm.all('AgentGrade').value = arrResult[0][82];
				fm.all('ManagerCode').value = arrResult[0][79];
				fm.all('upBranchAttr').value = arrResult[0][88];
				fm.all('BranchCode').value = arrResult[0][89];
				//    if (arrResult[0][82]!=null && trim(arrResult[0][82])!='')
				//    {
				//      if (arrResult[0][82].indexOf(":")!=-1)
				//      {
				//    var arrRear = arrResult[0][82].split(":");
				//    fm.all('RearAgent').value = arrRear.length>0?arrRear[0]:'';
				//    fm.all('RearDepartAgent').value = arrRear.length>1?arrRear[1]:'';
				//    fm.all('RearSuperintAgent').value = arrRear.length>2?arrRear[2]:'';
				//    fm.all('RearAreaSuperintAgent').value = arrRear.length>3?arrRear[3]:'';
				//      }
				//      else
				//    fm.all('RearAgent').value = arrResult[0][82];
				//    }

				//alert(arrResult[0][77]+','+arrResult[0][0]);
				//trim(arrResult[0][77])!=trim(arrResult[0][0]) ;)&&(arrResult[0][80]<='A05')
				//员工
				if(arrResult[0][82] <= 'A08')
				{
					if(arrResult[0][79]!=null)//管理人员=代理人代码
					{
						fm.all('UpAgent').value = arrResult[0][79]; //处经理
						fm.all('GroupManagerName').value = arrResult[0][87]; //处经理
					}

					if(trim(arrResult[0][86])!='')
					{
						//区经理
						var strSQL = "select BranchManager,BranchManagerName,upBranch,branchattr from LABranchGroup where 1=1 "
						+ " and EndFlag <> 'Y' and AgentGroup = '"+arrResult[0][86]+"' and (state<>'1' or state is null)";
						var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
						if (strQueryResult)
						{
								var arr = decodeEasyQueryResult(strQueryResult);
								var upattr = trim(arr[0][3])
								if(upattr.length > 10 )
								{
									fm.all('DepManagerName').value = trim(arr[0][1]);
									var up =  trim(arr[0][2]);
									//部经理
									if(up!='')
									{
										var strSQL = "select BranchManager,BranchManagerName,upBranch from LABranchGroup where 1=1 "
										+ " and EndFlag <> 'Y' and AgentGroup = '"+trim(up)+"' and (state<>'1' or state is null)";
										var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
										if (strQueryResult)
										{
											var arr = decodeEasyQueryResult(strQueryResult);
											fm.all('Minister').value = trim(arr[0][1]);
										}
									}
								}
								else
								{
										fm.all('Minister').value = trim(arr[0][1]);
								}
						}
					}
				}
				if(arrResult[0][82] == 'B11')
				{
					fm.all('DepManagerName').value = arrResult[0][87]; //区经理
					//部经理
					if(trim(arrResult[0][86])!='')
					{
						var strSQL = "select BranchManager,BranchManagerName from LABranchGroup where 1=1 "
						+ " and EndFlag <> 'Y' and AgentGroup = '"+arrResult[0][86]+"' and (state<>'1' or state is null)";
						var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
						if (strQueryResult)
						{
								var arr = decodeEasyQueryResult(strQueryResult);
								fm.all('UpAgent').value = trim(arr[0][0]);
								fm.all('Minister').value = trim(arr[0][1]);
						}
					}
						//fm.hideIsManager.value = "true";
				}
				if(arrResult[0][82] == 'B21')
				{
						fm.all('Minister').value = arrResult[0][87]; ///部经理
				}
				if(arrResult[0][82] <= 'B04' && arrResult[0][82] >= 'B01')//处经理
				{
						fm.all('GroupManagerName').value = arrResult[0][87];
						var strSQL = "select BranchManager,BranchManagerName,upbranch,branchattr from LABranchGroup where 1=1 "
						+ " and EndFlag <> 'Y' and AgentGroup = '"+arrResult[0][86]+"' and (state<>'1' or state is null)";
						var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
						if (strQueryResult)
						{
							var arr = decodeEasyQueryResult(strQueryResult);
							fm.all('UpAgent').value = trim(arr[0][0]);
							var upattr = trim(arr[0][3])
							if(upattr.length > 10 )
							{
								fm.all('DepManagerName').value = trim(arr[0][1]);
								var up =  trim(arr[0][2]);
								//部经理
								if(up!='')
								{
									var strSQL = "select BranchManager,BranchManagerName,upBranch from LABranchGroup where 1=1 "
									+ " and EndFlag <> 'Y' and AgentGroup = '"+trim(up)+"' and (state<>'1' or state is null)";
									var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
									if (strQueryResult)
									{
										var arr = decodeEasyQueryResult(strQueryResult);
										fm.all('Minister').value = trim(arr[0][1]);
									}
								}
							}
							else
								{
									fm.all('Minister').value = trim(arr[0][1]);
								}
						}
							//fm.hideIsManager.value = "true";
				}
						//  	if(arrResult[0][80] > 'A08')
						//    //确定部经理 arrResult[0][82]:上级机构
						//    if ((arrResult[0][84]!=null)&&(trim(arrResult[0][84])!=''))
						//    {
						//       var strSQL = "select BranchManager,BranchManagerName from LABranchGroup where 1=1 "
						//      + " and EndFlag <> 'Y' and AgentGroup = '"+arrResult[0][84]+"' and (state<>'1' or state is null)";
						//     	       //alert('11--'+strSQL+'  '+arrResult[0][82]);
						//       var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
						//       if (strQueryResult)
						//       {
						//       var arr = decodeEasyQueryResult(strQueryResult);
						//       if (trim(arrResult[0][77])==trim(arrResult[0][0]))
						// fm.all('UpAgent').value = trim(arr[0][0]);
						//       fm.all('DepManagerName').value = trim(arr[0][1]);
						//       }
						//    }
						//    if (arrResult[0][80] >= 'B11')
						//      fm.all('DepManagerName').value = trim(arrResult[0][5]); //部经理是他本身
		var tIntroAgency=fm.all('IntroAgency').value;
			var tIntroAgentcySql="select name from laagent where agentcode=getAgentCode('"+tIntroAgency+"')";

			var strQueryResult = easyQueryVer3(tIntroAgentcySql,1,1,1);
			fm.all('IntroAgencyName').value=" ";

			if (strQueryResult)
			{
				var arrQueryResult=decodeEasyQueryResult(strQueryResult);


				fm.all('IntroAgencyName').value=arrQueryResult[0][0];
				//alert(1);
				//WarrantorGrid.clearData("WarrantorGrid");
				// alert(1);
			}
			//alert('*****');
			var Sql_SexName="select codename from ldcode where codeType='sex' and code='"+fm.all('Sex').value+"' ";
			var strQueryResult_SexName  = easyQueryVer3(Sql_SexName, 1, 1, 1);
			if (strQueryResult_SexName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_SexName);
				fm.all('SexName').value= trim(arr[0][0]) ;
			}

			if(fm.all('Nationality').value!="" && fm.all('Nationality').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='nationality' and code='"+fm.all('Nationality').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('NationalityName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('NativePlace').value!="" && fm.all('NativePlace').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='nativeplacebak' and code='"+fm.all('NativePlace').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('NativePlaceName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('PolityVisage').value!="" && fm.all('PolityVisage').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='polityvisage' and code='"+fm.all('NativePlace').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('PolityVisageName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('RgtAddress').value!="" && fm.all('RgtAddress').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='nativeplacebak' and code='"+fm.all('RgtAddress').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('RgtAddressName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('Degree').value!="" && fm.all('Degree').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='degree' and code='"+fm.all('Degree').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('DegreeName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('PostTitle').value!="" && fm.all('PostTitle').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='posttitle' and code='"+fm.all('PostTitle').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('PostTitleName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('BankCode').value!="" && fm.all('BankCode').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='bank' and code='"+fm.all('BankCode').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('BankCodeName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('AgentGrade').value!="" && fm.all('AgentGrade').value!=null)
			{
				showOneCodeNametoAfter('gradename','AgentGrade','AgentGradeName');
				var Sql_NationalityName="select gradename from laagentgrade where  gradecode='"+fm.all('AgentGrade').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);

				if (strQueryResult_NationalityName)
				{

					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('AgentGradeName').value= trim(arr[0][0]) ;
				}

			}
						//fm.all('AgentGrade').readOnly = true;
						fm.all('ManageCom').readOnly = true;
						if (fm.all('IntroAgency').value!=null && fm.all('IntroAgency').value!='')
						{	   if(manageCom!='86')
								{
									fm.all('IntroAgency').readOnly = true;
								}
						}
						fm.all('BranchCode').readOnly = true;
						fm.all('GroupManagerName').readOnly = true;
						fm.all('DepManagerName').readOnly = true;
						fm.all('EmployDate').readOnly=true;

					easyQuery();
			}//一次增员
		
		}//结果不为空


			//    var wageSQL="select lawage.agentcode from lawage,laagent where lawage.agentcode ='"+fm.all('AgentCode').value+"' and laagent.agentcode=lawage.agentcode and laagent.agentstate<'03' ";
			//    var wageQueryResult=easyQueryVer3(wageSQL,1,1,1)
			//    if (wageQueryResult)
			//    {
			//     fm.all('AgentGrade').disabled = true;
			//     fm.all('ManageCom').disabled = true;
			//     if (fm.all('IntroAgency').value!=null && fm.all('IntroAgency').value!='')
			//     {
			// fm.all('IntroAgency').disabled = true;
			//     }
			//     fm.all('BranchCode').disabled = true;
			//     fm.all('GroupManagerName').disabled = true;
			//     fm.all('DepManagerName').disabled = true;
			//     fm.all('EmployDate').readOnly=true;
			//    fm.all('RearAgent').disabled = true;
			//    fm.all('RearDepartAgent').disabled = true;
			//    fm.all('RearSuperintAgent').disabled = true;
			//    fm.all('RearAreaSuperintAgent').disabled = true;
			//   }

			var tIntroAgency=fm.all('IntroAgency').value;
			var tIntroAgentcySql="select name from laagent where agentcode=getAgentCode('"+tIntroAgency+"')";

			var strQueryResult = easyQueryVer3(tIntroAgentcySql,1,1,1);
			fm.all('IntroAgencyName').value=" ";

			if (strQueryResult)
			{
				var arrQueryResult=decodeEasyQueryResult(strQueryResult);


				fm.all('IntroAgencyName').value=arrQueryResult[0][0];
				//alert(1);
				//WarrantorGrid.clearData("WarrantorGrid");
				// alert(1);
			}
			//alert('*****');
			var Sql_SexName="select codename from ldcode where codeType='sex' and code='"+fm.all('Sex').value+"' ";
			var strQueryResult_SexName  = easyQueryVer3(Sql_SexName, 1, 1, 1);
			if (strQueryResult_SexName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_SexName);
				fm.all('SexName').value= trim(arr[0][0]) ;
			}

			if(fm.all('Nationality').value!="" && fm.all('Nationality').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='nationality' and code='"+fm.all('Nationality').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('NationalityName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('NativePlace').value!="" && fm.all('NativePlace').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='nativeplacebak' and code='"+fm.all('NativePlace').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('NativePlaceName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('PolityVisage').value!="" && fm.all('PolityVisage').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='polityvisage' and code='"+fm.all('NativePlace').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('PolityVisageName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('RgtAddress').value!="" && fm.all('RgtAddress').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='nativeplacebak' and code='"+fm.all('RgtAddress').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('RgtAddressName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('Degree').value!="" && fm.all('Degree').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='degree' and code='"+fm.all('Degree').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('DegreeName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('PostTitle').value!="" && fm.all('PostTitle').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='posttitle' and code='"+fm.all('PostTitle').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('PostTitleName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('BankCode').value!="" && fm.all('BankCode').value!=null)
			{
				var Sql_NationalityName="select codename from ldcode where codeType='bank' and code='"+fm.all('BankCode').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
				if (strQueryResult_NationalityName)
				{
					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('BankCodeName').value= trim(arr[0][0]) ;
				}
			}

			if(fm.all('AgentGrade').value!="" && fm.all('AgentGrade').value!=null)
			{
				var Sql_NationalityName="select gradename from laagentgrade where  gradecode='"+fm.all('AgentGrade').value+"' ";

				var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);

				if (strQueryResult_NationalityName)
				{

					var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
					fm.all('AgentGradeName').value= trim(arr[0][0]) ;
				}

			}
		}

		function easyQuery()
		{
			// 书写SQL语句
			var strSQL = "";
			strSQL = "select CautionerName,CautionerSex,(select codename from ldcode where codetype='sex' and code=cautionersex),CautionerID,CautionerCom,HomeAddress,Mobile,ZipCode,Phone,Relation from LAWarrantor where 1=1 "
			;
//	      modify lyc 2014-11-27 统一工号
		       if(fm.all("AgentCode").value!=""){
		    	   strSQL +=" and AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
		       }
		       strSQL += "with ur";
			//alert(strSQL);
			turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

			//判断是否查询成功
			if (!turnPage.strQueryResult) {
				alert("担保人信息查询失败！");
				return false;
			}
			//查询成功则拆分字符串，返回二维数组
			turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
			//tArr = decodeEasyQueryResult(turnPage.strQueryResult);
			//turnPage.arrDataCacheSet = chooseArray(tArr,[0,1,2,3,4,5]);
			//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
			turnPage.pageDisplayGrid = WarrantorGrid;

			//保存SQL语句
			turnPage.strQuerySql     = strSQL;

			//设置查询起始位置
			turnPage.pageIndex       = 0;

			//在查询结果数组中取出符合页面显示大小设置的数组
			arrDataSet       = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
			var tArr = new Array();
			tArr = arrDataSet;
			//调用MULTILINE对象显示查询结果

			//displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
			displayMultiline(tArr, turnPage.pageDisplayGrid);

		}
function changeIDNo()
{
   var IDNoType=fm.all("IDNoType").value;
	if (IDNoType==null || IDNoType=='')
	{
		alert("请先录入证件类型！");
		return false ;
	}
//	if(IDNoType!='0')
//	{
//		return true;//如果不是身份证号，则不用校验
//	}
	if (getWherePart('IDNo')=='')
	return false;
	var strSQL1 = "";
	//检验是否为二次入司
	strSQL1 = "select agentstate  from LAAgent where 1=1 "
	
	+ getWherePart('IDNoType')
	+ getWherePart('ManageCom')
	+ getWherePart('IDNo');
	//alert(strSQL);	
	var strQueryResult1  = easyQueryVer3(strSQL1, 1, 1, 1);
	if (strQueryResult1)
	{
	var arr= decodeEasyQueryResult(strQueryResult1);
	if(arr[0][0]=='01'||arr[0][0]=='02'||arr[0][0]=='03'||arr[0][0]=='04'||arr[0][0]=='05')
	  {
		alert('该代理人已在职或为准离职状态!不能再次做增员处理');
		fm.all('IDNo').value = '';
		return false;
	  }
	}
	
	var strSQL = "";
    strSQL = "select * from LAAgent where 1=1 and agentstate<'06'"
    	   + getWherePart('IDNoType')
           + getWherePart('IDNo');
     	 //alert(strSQL);
    var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
    if (strQueryResult)
    {
    	alert('该证件号码已存在并未离职!');
    	fm.all('IDNo').value = '';
    	return false;
    }
	
	return true;
}
function changeIDNo1()
{
	var IDNoType=fm.all("IDNoType").value;
	if (IDNoType==null || IDNoType=='')
	{
		alert("请先录入证件类型！");
		return false ;
	}
	if(IDNoType!='0')
	{
		return true;//如果不是身份证号，则不用校验
	}
	if (getWherePart('IDNo')=='')
	return false;
	var strSQL = "";
	//检验是否为二次入司
	strSQL = "select agentstate from LAAgent where 1=1"
	+ getWherePart('IDNoType')
	+ getWherePart('IDNo');
	//alert(strSQL);	
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	if (strQueryResult)
	{
	  var arr= decodeEasyQueryResult(strQueryResult);
	  if(arr[0][0]=='01'||arr[0][0]=='02'||arr[0][0]=='03'||arr[0][0]=='04')
	  {
		alert('该代理人已在职或为准离职状态!不能再次做增员处理');
		fm.all('IDNo').value = '';
		return false;
	  }
	  if(arr[0][0]=='07')
	  {
	   	if(!confirm('此人已两次以上入司，是否允许再次入司？'))
   	   		{
   	   	 		fm.IDNo.value = '';
   	   	 		return false;
   	   		}
	  }
	  if(arr[0][0]=='06')
	  {//标准的二次增员，不做身份证限制
	    return true;
	  }
	}
	return true;
}
		
//校验育成代理人
function checkRearAgent()
{
	var strSQL = "",str = "";
	var strQueryResult = null;

	strSQL = "select getUniteCode(AgentCode) from LAAgent where (AgentState < '03' or AgentState is not null) "
	//育成代理人
	if (trim(fm.all('RearAgent').value)=='')
	return true;
	if (trim(fm.all('RearAgent').value)==trim(fm.all('AgentCode').value))
	{
		alert('与原代理人编码相同!');
		fm.all('RearAgent').value = '';
		return false;
	}
	str = getWherePart('groupAgentCode','RearAgent');

	//alert(strSQL+str);
	strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
	if (!strQueryResult)
	{
		alert('不存在该育成代理人！');
		fm.all('RearAgent').value = '';
		return false;
	}
	//增部代理人
	if (trim(fm.all('RearDepartAgent').value)=='')
	return true;
	if(trim(fm.all('RearDepartAgent').value)==trim(fm.all('AgentCode').value))
	{
		alert('与原代理人编码相同!');
		fm.all('RearDepartAgent').value = '';
		return false;
	}
	str = getWherePart('groupagentcode','RearDepartAgent');

	//alert(strSQL+str);
	strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
	if (!strQueryResult)
	{
		alert('不存在该增部代理人！');
		fm.all('RearDepartAgent').value = '';
		return false;
	}
	//育成督导长代理人
	if (trim(fm.all('RearSuperintAgent').value)=='')
	return true;

	if (trim(fm.all('RearSuperintAgent').value)==trim(fm.all('AgentCode').value))
	{
		alert('与原代理人编码相同!');
		fm.all('RearSuperintAgent').value = '';
		return false;
	}
	str = getWherePart('groupagentcode','RearSuperintAgent');

	//alert(strSQL+str);
	strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
	if (!strQueryResult)
	{
		alert('不存在该育成督导代理人！');
		fm.all('RearSuperintAgent').value = '';
		return false;
	}
	//育成区域督导长代理人
	if (trim(fm.all('RearAreaSuperintAgent').value)=='')
	return true;
	if (trim(fm.all('RearAreaSuperintAgent').value)==trim(fm.all('AgentCode').value))
	{
		alert('与原代理人编码相同!');
		fm.all('RearAreaSuperintAgent').value = '';
		return false;
	}
	str = getWherePart('groupagentcode','RearAreaSuperintAgent');

	//alert(strSQL+str);
	strQueryResult = easyQueryVer3(strSQL+str, 1, 1, 1);
	if (!strQueryResult)
	{
		alert('不存在该育成区域督导代理人！');
		fm.all('RearAreaSuperintAgent').value = '';
		return false;
	}
	return true;
}

function agentConfirm()
{
	if (getWherePart('AgentCode')=='')
	{
		return false;
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.* from LAAgent a where 1=1 "
	//+ "and a.AgentGroup = b.AgentGroup "
	+ getWherePart('a.groupagentcode','AgentCode');
	//   alert(strSQL);
	var strQueryResult = easyQueryVer3(strSQL,1,1,1);
	if(!strQueryResult)
	{
		alert('不存在该代理人！')
		fm.all('AgentCode').value = '';
		return false;
	}
	var arr = decodeEasyQueryResult(strQueryResult);
	var AgentState = arr[0][61];
	if (AgentState==null||AgentState=='01'||AgentState=='02'||AgentState=='03'||AgentState=='04')
	{
		alert('该代理人在职或为准离职状态，不能作二次增员！');
		fm.all('AgentCode').value = '';
		return false;
	}
	if (trim(AgentState) == '07')
	{
		alert('该代理人已离职两次，不能再次增员！');
		fm.all('AgentCode').value = '';
		return false;
	}

/*	if (trim(AgentState)=='06')
	{

		if (!compare(arr[0][51]))
		{
			alert("该代理人在离职半年后才可重新入司!");
			return false;
		}
	}
*/
	var tAgentCode=fm.all("AgentCode").value;
	var tBranchAttr=fm.all("BranchCode").value;
	var tBranchType=fm.all("BranchType").value;
	var tBranchType2=fm.all("BranchType2").value;
	var tsql = "select managecom from labranchgroup where branchattr='"+tBranchAttr+"' and branchtype='"+tBranchType+"'  and branchtype2='"+tBranchType2+"'";
	var strQueryResult  = easyQueryVer3(tsql, 1, 0, 1);
	if (!strQueryResult)
	{
		alert('此团队不存在！');
		fm.all('BranchCode').value = '';
		return false;
	}
	var arr=decodeEasyQueryResult(strQueryResult);
	var tManageCom1=arr[0][0];
	tsql = "select managecom from laagent  where agentcode=getAgentCode('"+tAgentCode+"')";
	strQueryResult  = easyQueryVer3(tsql, 1, 0, 1);
	if (!strQueryResult)
	{
		alert('此人员不存在！');
		fm.all('AgentCode').value = '';
		return false;
	}
	arr=decodeEasyQueryResult(strQueryResult);
	var tManageCom2=arr[0][0];
	// alert(tManageCom2);
	// alert(tManageCom1);
	if(tManageCom2!=tManageCom1)
	{
		alert('团队代码录入有误，人员代码与机构类型不匹配！');
		fm.all('BranchCode').value = '';
		return false;
	}

	fm.all('AgentState').value = fm.all('initAgentState').value;

	mOperate = "UPDATE||MAIN";

	submitForm();
	// if (!afterQuery(arr))
	//   return false;
	return true;
}

//二次增员必须是在离职后半年
function compare(DepartDate)
{

	var d = new Date();
	var month,year,day;
	day = d.getDate();
	month = d.getMonth() + 1 - 6;
	if (month == 0)
	{
		year=d.getYear()-1;
	}
	else
		if (month < 0)
		{
			month=12+month;
			year=d.getYear()-1;
		}
		if (month.toString().length == 1)
		month='0'+month;
		if (day.toString().length == 1)
		day='0'+day;
		var dd = year+'-'+month+'-'+day;
		//alert(dd+'   '+DepartDate);
		if (trim(dd)<trim(DepartDate))
		return false;
	return true;
}
				
function saveForm()
{
	mOperate = "INSERT||MAIN";
	submitForm();
}      
				
function BankClick()
{
	var AgentCode=fm.all('AgentCode').value;
	if(!AgentCode)
	{
		alert('营销员代码不能为空！');
		return false;
	}
	//增加html页面，直接打开jsp下拉选无法使用
	showInfo=window.open("../agentdaily/LAAccounts.html");
}

function CertClick()
{
	var AgentCode=fm.all('AgentCode').value;
	if(!AgentCode)
	{
		alert('营销员代码不能为空！');
		return false;
	}
	showInfo=window.open("../agentdaily/LACertificationInput.jsp?AgentCode="+fm.all('AgentCode').value+"");
}

function AgentClick()
{
	var AgentCode=fm.all('AgentCode').value;
	if(!AgentCode)
	{
		alert('营销员代码不能为空！');
		return false;
	}
	showInfo=window.open("../agentdaily/LAQualificationInput.jsp?AgentCode="+fm.all('AgentCode').value+"");
}

function ArchClick()
{
	var AgentCode=fm.all('AgentCode').value;
	if(!AgentCode)
	{
		alert('营销员代码不能为空！');
		return false;
	}
	showInfo=window.open("../agentdaily/LAArchieveInputHtml.jsp?AgentCode="+fm.all('AgentCode').value+"&BranchType="+document.fm.BranchType.value+"&BranchType2="+document.fm.BranchType2.value+"");
}

function AscripText()
{  var AgentCode=fm.all('AgentCode').value;
	 var tSql="select '1' from laascription  where agentold=getAgentCode('"+AgentCode+"') ";
	 tSql+="and validflag='N' and ascripstate<'3' ";
	 var strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
	 if (!strQueryResult)
	{
		var tSql1="select * from laorphanpolicy where  flag='1' and agentcode=getAgentCode('"+AgentCode+"') ";
		strQueryResult  = easyQueryVer3(tSql1, 1, 0, 1);		
	}
	 if (strQueryResult)
	 {
	 	if (confirm("是否将孤儿单重新回归给此代理人？"))
          {
           tOrphanCode="Y";             
           return true; 
          }
     else
          {
            tOrphanCode= "N";
            return true;
          }  	 
	 	
	 	}
	  return true;
	}
	

function checkdate() 
{ 
var   date   =   new   Date(); 
var   curYear=date.getYear(); 
var   curMonth=date.getMonth()+1; 
var   curDate=date.getDate(); 
var   strDate=curYear+ "-"+curMonth+ "-"+curDate;
var r=/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/g;
var v=fm.all('EmployDate').value;
if(r.test(v)) 
{ 
return true; 
} 
else 
{ 
alert("入司录入日期为空或不是有效的日期格式(YYYY-MM-DD),请重新进行录入!"); 
fm.all('EmployDate').value= strDate;
} 
} 

function checkPhone()
{
  var Phone = fm.all('Phone').value.trim();
  var result=CheckFixPhoneNew(Phone);
  var result2=CheckPhoneNew(Phone);
  if(result!=""&&result2!=""){
	  alert("联系电话不符合规则，请录入正确的固话或手机号！");
	  fm.all('Phone').value='';
	  return false;
	}
  var sql = "select getUniteCode(AgentCode) from LAAgent where BranchType = '1' and Phone = '"+Phone+"' and AgentState <'06' ";
  if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!="")
   {
       sql +=" and groupagentcode <>'"+fm.all('AgentCode').value+"'";
   } 
  sql+="fetch first 1 rows only";
  var strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
  if (!strQueryResult)
  {   
     return true;
  }else{
   arr=decodeEasyQueryResult(strQueryResult);
   alert("系统已存有联系电话号为"+Phone+"的在职营销员（工号为："+arr[0][0]+"），烦请重新录入员工联系电话!");
   fm.all('Phone').value='';
   return false;
  }
  
  return true;
}

function checkMobile()
{
  var Mobile = fm.all('Mobile').value.trim();
  if(Mobile!=null&&Mobile!="")
  {
	  var result=CheckPhoneNew(Mobile);
	  if(result!=""){
		  alert(result);
		  fm.all('Mobile').value='';
		  return false
		}
    var sql = "select groupagentcode from LAAgent where BranchType = '1' and Mobile = '"+Mobile+"' and AgentState <'06' ";
    if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!="")
     {
       sql +=" and groupagentcode <>'"+fm.all('AgentCode').value+"'";
     } 
    sql+="fetch first 1 rows only";
    var strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
   if (!strQueryResult)
    {
       return true;
    }else{
     arr=decodeEasyQueryResult(strQueryResult);
     alert("系统已存有手机号为"+Mobile+"的在职营销员（工号为："+arr[0][0]+"），烦请重新录入员工手机号!");
     fm.all('Mobile').value='';
     return false;
    }
  }  
  return true;
}

function checkInput(str)
{	
	fm.all('QualifNo').value = str.replace(/(^[\s\u3000]*)|([\s\u3000]*$)/g, "");
}
function afterCodeSelect( cCodeName, Field )
{
	if(cCodeName =="AgentGrade"){
		var agentgrade = fm.all('AgentGrade').value;
		if("A03"==agentgrade||"A05"==agentgrade||"A07"==agentgrade||"B03"==agentgrade||"B12"==agentgrade)
		{
			alert("根据015个险基本法，不能选择此职级！");
			fm.all('AgentGrade').value='';
			fm.all('AgentGradeName').value='';
			return false;
		}
		if("B01"<=agentgrade)
		{
			 fm.all('AgentGroup2').value="";
			 fm.all('Group2ManagerName').value="";
		}
   	}
}
function initAgentGroup2(cObj,cObj2)
{
	var tAgentGrade = fm.all('AgentGrade').value;
	var tGroup2 = fm.all('BranchCode').value;
	if(tAgentGrade==""||tAgentGrade==null)
	{
		alert("请选择职级");
		return false;
	}
	if(tGroup2==""||tGroup2==null)
	{
		alert("请选择销售机构");
		return false;
	}
	if("B01"<=tAgentGrade)
	{
		 fm.all('AgentGroup2').value="";
		 fm.all('Group2ManagerName').value="";
	}
	else 
	{
		showCodeList('AAgentGroup2',[cObj,cObj2], [0,1], null, tGroup2, "b.BranchAttr",'1');
	}
	return true;
}

function checkAgentGroup2()
{
	var agentgrade = fm.all('AgentGrade').value;
	var agentgroup2 = fm.all('AgentGroup2').value;
	if(agentgrade<'B01')
	{
		if(""==agentgroup2||null==agentgroup2)
		{
			if(!confirm("该营销员不归属于营业组团队，是否继续操作？"))
			{
				return false;
			}
		}
		else
		{
			if(!confirm("该营销员归属于营业组团队，是否继续操作？"))
			{
				return false;
			}
		}
	}
	return true;
}
