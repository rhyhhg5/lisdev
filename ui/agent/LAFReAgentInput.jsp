<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容 
//	朱向峰	2004-11-15 9:47	中英人寿
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
    var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<head >
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LAFReAgentInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LAFReAgentInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
 <form action="./LAFReAgentSave.jsp" method=post name=fm target="fraSubmit">
     <%//@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%//@include file="../common/jsp/InputButton.jsp"%>
   <table class="common" align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return updateClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return queryClick();">
			</td>
		</tr>
	
    </Table>
    <table>
		<tr class=common>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
			<td class=titleImg>
				业务经理信息
			</td>
		</tr>
    </table>
	<Div id= "divLAAgent1" style= "display: ''">
    <table  class= common>
      <TR  class= common>
        <TD class= title>
          业务经理编码
        </TD>
        <TD  class= input>
          <Input class= 'readonly' readonly name=AgentCode type=hidden>
          <Input class= 'readonly' readonly name=GroupAgentCode >
        </TD>
        <TD  class= title>
          业务经理姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common verify="客户经理姓名|NotNull&len<=20" elementtype=nacessary>
        </TD>
        <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" verify="性别|code:Sex" 
          ondblclick="return showCodeList('Sex',[this,SexName],[0,1]);" 
          onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);"
          ><Input class=codename name=SexName readOnly  elementtype=nacessary> 
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          出生日期
        </TD>
        <TD  class= input>
          <Input name=Birthday class=common  verify="出生日期|NotNull&Date" elementtype=nacessary>
        </TD>
        <TD  class= title>
          身份证号码
        </TD>
        <TD  class= input>
          <Input name=IDNo class= common verify="身份证号码|notnull&len<=20" onchange="return changeIDNo();" elementtype=nacessary>
        </TD>
         <TD  class= title>
          民族
        </TD>
        <TD  class= input>
          <Input name=Nationality class="codeno" id="Nationality" 
           ondblclick="return showCodeList('Nationality',[this,NationalityName],[0,1]);" 
           onkeyup="return showCodeListKey('Nationality',[this,NationalityName],[0,1]);" 
           ><Input class=codename name=NationalityName readOnly>
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          户口所在地
        </TD>
        <TD  class= input>
          <Input name=RgtAddress class="codeno" 
           ondblclick="return showCodeList('NativePlaceBak',[this,RgtAddressName],[0,1]);" 
           onkeyup="return showCodeListKey('NativePlaceBak',[this,RgtAddressName],[0,1]);"
           ><Input class=codename name=RgtAddressName readOnly>
        </TD>
        <TD  class= title>
          籍贯
        </TD>
        <TD  class= input>
          <Input name=NativePlace class="codeno" verify="籍贯|code:NativePlaceBak" id="NativePlaceBak" 
           ondblclick="return showCodeList('NativePlaceBak',[this,NativePlaceName],[0,1]);" 
           onkeyup="return showCodeListKey('NativePlaceBak',[this,NativePlaceName],[0,1]);"
           ><Input class=codename name=NativePlaceName readOnly>
        </TD>
        <TD  class= title>
          政治面貌
        </TD>
       <TD  class= input>
          <Input name=PolityVisage class="codeno" verify="政治面貌|code:polityvisage" id="polityvisage" 
           ondblclick="return showCodeList('polityvisage',[this,PolityVisageName],[0,1]);" 
           onkeyup="return showCodeListKey('polityvisage',[this,PolityVisageName],[0,1]);" 
           ><Input class=codename name=PolityVisageName readOnly>
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          学历
        </TD>
        <TD  class= input>
          <Input name=Degree class="codeno" verify="学历|code:Degree" id="Degree" 
           ondblclick="return showCodeList('Degree',[this,DegreeName],[0,1]);" 
           onkeyup="return showCodeListKey('Degree',[this,DegreeName],[0,1]);"
           ><Input class=codename name=DegreeName readOnly>
        </TD>
       <TD  class= title>
          毕业院校
        </TD>
        <TD  class= input>
          <Input name=GraduateSchool class= common >
        </TD>
        <TD  class= title>
          专业
        </TD>
        <TD  class= input>
          <Input name=Speciality class= common >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          职称
        </TD>
        <TD  class= input>
          <Input name=PostTitle class='codeno' verify="职称|code:posttitle" 
           ondblclick="return showCodeList('posttitle',[this,PostTitleName],[0,1]);" 
           onkeyup="return showCodeListKey('posttitle',[this,PostTitleName],[0,1]);"
           ><Input class=codename name=PostTitleName readOnly>
        </TD>
        <TD  class= title>
          家庭地址
        </TD>
        <TD  class= input>
          <Input name=HomeAddress class= common >
        </TD>
        <TD  class= title>
          邮政编码
        </TD>
        <TD  class= input>
          <Input name=ZipCode class= common >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          联系电话
        </TD>
        <TD  class= input>
          <Input name=Phone class= common verify="联系电话|len<=18&notnull" elementtype=nacessary>
        </TD>
        <TD  class= title>
          传呼
        </TD>
        <TD  class= input>
          <Input name=BP class= common >
        </TD>
        <TD  class= title>
          手机
        </TD>
        <TD  class= input>
          <Input name=Mobile class= common >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          E_mail
        </TD>
        <TD  class= input>
          <Input name=EMail class= common >
        </TD>
         <TD  class= title>
          原工作单位
        </TD>
        <TD  class= input>
          <Input name=OldCom class= common >
        </TD>
        <TD  class= title>
          原职业
        </TD>
        <TD  class= input>
          <!--Input name=OldOccupation class='codeno' verify="原职业|code:occupationcode" 
           ondblclick="return showCodeList('occupationcode',[this,OldOccupationName],[0,1]);" 
           onkeyup="return showCodeListKey('occupationcode',[this,OldOccupationName],[0,1]);"
           --><Input class='common' name=OldOccupation >
        </TD>

      </TR>
      <TR  class= common>

        <TD  class= title>
          原工作职务
        </TD>
        <TD  class= input>
          <Input name=HeadShip class= common  >
        </TD>
         <TD  class= title>
          入司时间
        </TD>
        <TD  class= input>
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' verify="入司时间|notnull&Date" elementtype=nacessary>
        </TD>
        <TD  class= title>
          备注
        </TD>
        <TD  class=input>
          <Input name=Remark class= common >
        </TD>
      </TR>  
     <tr class=common>
					<TD class= title>是否转正</TD>
					<TD class= input>
						<Input class=codeno readOnly name=InDueFormFlag verify="是否转正|notnull" CodeData="0^N|未转正^Y|已转正" 
						 ondblClick="showCodeListEx('CertifyStateList',[this,InDueFormName],[0,1],null,null,null,1);  "
             onkeyup="showCodeListKeyEx('CertifyStateList',[this,InDueFormName],[0,1],null,null,null,1);"
             ><Input class=codename name=InDueFormName  readOnly >
					</TD>
						 <TD  class= title>
          人员类别
        </TD>
        <TD  class= input>
          <Input name=InsideFlag class='codeno' verify="人员类别" 
           ondblclick="return showCodeList('insideflag',[this,InsideFlagName],[0,1]);" 
           onkeyup="return showCodeListKey('insideflag',[this,InsideFlagName],[0,1]);"
           ><Input class=codename name=InsideFlagName readOnly>
        </TD>				
			
				</tr>
				<tr>
		<TD class=title>销售人员类型</TD>
		<TD class=input><Input name=AgentType class='codeno'
			verify="销售人员类型|notnull"
			ondblclick="return showCodeList('agenttypecode',[this,AgentTypeName],[0,1]);"
			onkeyup="return showCodeListKey('agenttypecode',[this,AgentTypeName],[0,1]);"><Input
			class=codename name=AgentTypeName elementtype=nacessary readOnly></TD>
		<TD class=title>委托代理合同编号</TD>
		<TD class=input><Input name=RetainContno class=common></TD>
	</tr>
      		<TR  class= common>
				<TD  class=input colSpan= 3>
					<font color='red'>提示：输入项“联系电话”会打印到客户的相关单证上</font>
				</TD>
			</TR>

    </table>
        <!--离司日期-->
        <Input name=OutWorkDate class=common type=hidden >
    </Div>
    <table>
	 <tr>
		<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent7);">
		<td class= titleImg>
		资格证信息
		</td>
	</tr>
</table>
    <Div id= "divLAAgent7" style= "display: ''">
	<table  class= common  >
        <TR  class= common>
        <TD  class= title>
      资格证书号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=QualifNo>
    </TD>
    <TD  class= title>
      批准单位
    </TD>
    <TD  class= input>
       <Input class= 'common' name=GrantUnit>
    </TD>

          <TD  class= title>
            发放日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=GrantDate verify="发放日期|Date" > 
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            有效起期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=ValidStart verify="有效起期|Date"> 
          </TD>
           <TD  class= title>
            有效止期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=ValidEnd verify="有效止期|&Date"> 
          </TD>
          <TD  class= title>
            资格证书状态
          </TD>
          <TD class=input><input name=QualifState class="codeno" name="QualifState" 
             CodeData="0|^0|有效|^1|失效"
             ondblClick="showCodeListEx('QualifStateList',[this,QualifStateName],[0,1]);"
             onkeyup="showCodeListKeyEx('QualifStateList',[this,QualifStateName],[0,1]);"
             ><Input class=codename name=QualifStateName>
          </TD>
        </TR>  
        <TR  class= common>
		<TD  class=input colSpan= 5>
		  <font color='red'>提示：如需录入资格证,需录入完整信息:包括资格证书号、批准单位、发放日期、有效起期、有效止期、资格证书状态</font>
			</TD>
		</TR>
  </table>
 </Div >
    <!--行政信息-->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" 
                 OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                行政信息
            </td>
    	</tr>
     </table>
		<Div id= "divLAAgent3" style= "display: ''">
			<table class=common>
				<tr class=common>
					<TD class= title>业务经理职级</TD>
					<TD class= input>
						<Input name=AgentGrade class="codeno" verify="客户经理职级|notnull&code:AgentGrade" 
						 ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
          onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);"
          ><Input class=codename name=AgentGradeName readOnly elementtype=nacessary>
					</TD>	
					
					<!--TD  class= title>业务经理类别</TD>
					<TD  class= input-->
						<!--Input name=AgentKind class='code' verify="人员类别|notnull" ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);" elementtype=nacessary-->
		  			<!--input class='code' name=AgentKind verify="客户经理类别|notnull" 
		         								ondblclick="return showCodeList('AgentKind',[this],null,null,'2','Codealias');" 
														onkeyup="return showCodeListKey('AgentKind',[this],null,null,'2','Codealias');" elementtype=nacessary>
					</TD-->
						<!--TD  class= title></TD>
					<TD  class= input>
						<Input type=hidden name=AgentKind >
					</TD-->
				</tr>
				<tr class=common>
	                                <TD class=title>辅导人代码</TD>
					<TD class= input>
					<Input name=TutorShip class='code'  
					ondblclick="EdorType(this,TutorShipName);" onkeyup="KeyUp(this,TutorShipName);"
					>	
					<TD class=title>辅导人姓名</TD>
					<TD class= input>
					<Input name=TutorShipName class='code'  
					ondblclick="EdorType1(this,TutorShip);" onkeyup="KeyUp1(this,TutorShip);"
					>	
				</tr>	
				<tr class=common>
					<TD class= title>所属团队</TD>
					<TD class= input>
						<Input class=common name=AgentGroup verify="所属机构|notnull" onchange="return changeGroup();" elementtype=nacessary>
					</TD>
					<TD class= title>团队主管</TD>
					<TD class= input>
						<Input name=UpAgent class= readonly  readonly type=hidden>
						<Input name=UpGroupAgent class= readonly  readonly>
					</TD>
				</tr>
				
					
				
			</table>
		</Div>
		<Input type=hidden  name=InDueFormDate >
		<!--Input type=hidden name=InsideFlag -->
		<input type=hidden name=AgentState value=''>
		<Input type=hidden name=Operator >
		<Input type=hidden name=ManageCom >
		<input type=hidden name=hideIdNo value=''>
		<input type=hidden name=hideOperate value=''>
		<input type=hidden name=hideAgentGroup value=''>
		<input type=hidden name=BranchLevel value=''>
		<input type=hidden name=GrpFlag value='2'>
		<input type=hidden name=BranchType value=''>
		<input type=hidden name=BranchType2 value=''>
		<input type=hidden name=BranchManager value=''>
		<input type=hidden name=hideIsManager value='false'>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</form>
</body>
</html>

