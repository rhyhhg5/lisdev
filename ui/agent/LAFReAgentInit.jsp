<%
//程序名称：LAAgentInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('AgentCode').value = '';
    fm.all('GroupAgentCode').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    fm.all('NativePlace').value = '';
    fm.all('Nationality').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('IDNo').value = '';
    fm.all('PolityVisage').value = '';
    fm.all('Degree').value = '';
    fm.all('GraduateSchool').value = '';
    fm.all('Speciality').value = '';
    fm.all('PostTitle').value = '';
    fm.all('OldCom').value = '';
    fm.all('OldOccupation').value = '';
    fm.all('HeadShip').value = '';
    //fm.all('QuafNo').value = '';
    //fm.all('QuafEndDate').value = '';
    //fm.all('TrainPeriods').value = '';
    fm.all('EmployDate').value = '';
    fm.all('RgtAddress').value = '';
   // fm.all('BankCode').value = '';
    //fm.all('BankAccNo').value = '';
     fm.all('InsideFlag').value = '1';
     fm.all('InsideFlagName').value = '正常';
    fm.all('Remark').value = '';
   // fm.all('AgentKind').value = '';
    fm.all('Operator').value = '';
    if(getBranchType()!='2')
    	fm.all('ChannelName').value ='';
    //行政信息
    fm.all('UpAgent').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('AgentState').value ='02';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value ='<%=BranchType2%>';
    
    // 资格证信息
    fm.all('QualifNo').value= '';
	fm.all('GrantUnit').value= '';
	fm.all('GrantDate').value= '';
	fm.all('ValidStart').value= '';
	fm.all('ValidEnd').value= '';
	fm.all('QualifState').value= '';
	fm.all('QualifStateName').value= '';
  }
  catch(ex)
  {
    alert("在LAAgentInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
//    setOption("t_sex","0=男&1=女&2=不详");
//    setOption("sex","0=男&1=女&2=不详");
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");
  }
  catch(ex)
  {
    alert("在LAAgentInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
   // initWarrantorGrid();
  }
  catch(re)
  {
    alert("LAAgentInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
