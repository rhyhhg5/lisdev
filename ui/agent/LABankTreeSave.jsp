<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABankTreeSave.jsp
//程序功能：
//创建日期：2004-04-1 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数  
  System.out.println("begin labanktreeUI...");
  LABankTreeUI tLABankTreeUI   = new LABankTreeUI();

  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    String tAgentCode=request.getParameter("AgentCode");    //需要变更的业务员代码
    String tAgentGroup =request.getParameter("AgentGroup");  //新录入的销售机构代码
    String tNewAgentGrade1 =request.getParameter("NewAgentGrade1"); //新业务职级
    String tNewAgentGrade=request.getParameter("NewAgentGrade");    //新行政职级
    String tDate=request.getParameter("AdjustDate");   //调整日期
    String tBranchType=request.getParameter("BranchType");
    String tBranchType2=request.getParameter("BranchType2");
  
  // 准备传输数据 VData
   VData tVData = new VData();
   FlagStr="";  
   //System.out.println("tAgentCode:"+tAgentCode);
   //System.out.println("savejsp: tAgentCode"+tAgentCode);
   tVData.add(new ExeSQL().getOneValue("select agentcode from laagent where groupagentcode='"+tAgentCode+"'"));
  // System.out.println("savejsp: tAgentGroup"+tAgentGroup);
   tVData.add(tAgentGroup);
  // System.out.println("savejsp: tNewAgentGrade1"+tNewAgentGrade1);
   tVData.add(tNewAgentGrade1);
  // System.out.println("savejsp: tNewAgentGrade"+tNewAgentGrade);
   tVData.add(tNewAgentGrade);
  // System.out.println("savejsp: tDate"+tDate);
   tVData.add(tDate);
  // System.out.println("savejsp: tAgentCode"+tAgentCode);
   tVData.add(tG);
 //  System.out.println("savejsp: tBranchType"+tBranchType);
   tVData.add(tBranchType);
  // System.out.println("savejsp: tBranchType2"+tBranchType2);
   tVData.add(tBranchType2);
  
  try
  {
    System.out.println("this will save the data!!!");
    tLABankTreeUI.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABankTreeUI.mErrors;
    if (!tError.needDealError())
    {        
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">        
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

