<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LADimissionAppSave.jsp
//程序功能：
//创建日期：2005-03-16 15:12:33
//创建人  ：zhanghui
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.util.Date"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LADimissionSchema tLADimissionSchema   = new LADimissionSchema();

  LASocialDimissionBL tLADimission   = new LASocialDimissionBL();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();

  tG=(GlobalInput)session.getValue("GI");

  //保存员工代码
  tLADimissionSchema.setAgentCode(request.getParameter("AgentCode"));
  //保存申请日期         
  tLADimissionSchema.setApplyDate(request.getParameter("AppDate"));
  //保存离职原因
  tLADimissionSchema.setDepartRsn(request.getParameter("DepartRsn"));
  //保存备注
  tLADimissionSchema.setNoti(request.getParameter("Noti"));
  //保存操作人代码
  tLADimissionSchema.setOperator(request.getParameter("Operator"));    
  tLADimissionSchema.setBranchType(request.getParameter("BranchType"));
  tLADimissionSchema.setBranchType2(request.getParameter("BranchType2"));
  tLADimissionSchema.setDepartTimes(request.getParameter("DepartTimes"));
    

  System.out.println("Schema:over");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLADimissionSchema);
  tVData.add(tG);
  try
  {
    System.out.println("Before LADimission.submiteData");
    tLADimission.submitData(tVData,tOperate);
    System.out.println("After LADimission.submiteData");
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLADimission.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
        System.out.println("no reord");
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
    parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

