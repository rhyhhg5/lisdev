<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LAAgentTrainDateBL tLAAgentTrainDateBL = new LAAgentTrainDateBL();
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  //add by lyc 统一工号 2014-11-27
  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("AgentCode")+"' fetch first row only";
  String cAgentCode = new ExeSQL().getOneValue(cSql);
  tLAAgentSchema.setAgentCode(cAgentCode);
  tLAAgentSchema.setNoWorkFlag(request.getParameter("NoWorkFlag"));
  tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));

  System.out.println("begin agent schema...");
 

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.addElement(tLAAgentSchema);
  try
  {
    System.out.println("this will save the data!!!");
    tLAAgentTrainDateBL.submitData(tVData,"");
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAgentTrainDateBL.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    	
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
