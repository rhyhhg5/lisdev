<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：UpdateInput.jsp
//程序功能：
//创建日期：
//创建人  zyy程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//	xx	2017-09-01 10:24	
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="UpdateInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="UpdateInit.jsp"%>
</head> 
<body  onload="initForm();initElementtype();" >
  <form action="./UpdateSave.jsp" method=post name=fm target="fraSubmit">
  <input type=hidden name=querySQL value=""> 
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		人员基础信息表信息查询条件
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:comcode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>        
            <td class=title>
           业务核心编码
        </td>
        <td class=input>
        <input  name=Agentcode verify="业务核心编码|NOTNULL" elementtype=nacessary>
        </td>
        </TR>
        <TR class=common>
          <TD class=title>
           业务员姓名
          </TD>
          <TD class=input>
             <Input  name=Name>
          </TD>
          <TD class=title>
            业务员工号
          </TD>
           <TD class=input>
             <Input  name=Groupagentcode>
           </TD>
        </TR>
        <TR  class= common>	
         <TD class=title>展业类型</TD>
           <TD class=input><Input class='codeno' name=BranchType
		    verify="展业类型|notnull"
		    ondblclick="return showCodeList('branchtype',[this,BranchTypeName],[0,1]);"
			onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],[0,1]);"
			readonly><Input class=codename name=BranchTypeName elementtype=nacessary>
	   </TD>
         <TD class=title>销售渠道</TD>
		<TD class=input><Input class='codeno' name=BranchType2
		    verify="销售渠道|notnull"
		    ondblclick="return showCodeList('branchtype2',[this,BranchType2Name],[0,1]);"
			onkeyup="return showCodeListKey('branchtype2',[this,BranchType2Name],[0,1]);"
			readonly><Input class=codename name=BranchType2Name elementtype=nacessary>
	   </TD>
	   </TR>
	   <TR class=common>
          <TD class=title>
           离职日期
          </TD>
          <TD class=input>
             <Input  name=Date>
          </TD>
          </TR>
      </table>       
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
      				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="return easyQueryClick()">
      				<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateclick()">
    			</Td>
  			</Tr>
		</Table>
    </Div>
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
	  <Div  id= "LAAgent1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid"></span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT CLASS=cssButton VALUE="首页"   TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾页"   TYPE=button onclick="turnPage.lastPage();">				
  	</div>   
  	<input type=hidden id="fmAction" name="fmAction">     
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>