<%
//程序名称：LAFRAgentInit.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('AgentCode').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    fm.all('NativePlace').value = '';
    fm.all('Nationality').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('IDNo').value = '';
    fm.all('PolityVisage').value = '';
    fm.all('Degree').value = '';
    fm.all('GraduateSchool').value = '';
    fm.all('Speciality').value = '';
    fm.all('PostTitle').value = '';
    fm.all('OldCom').value = '';
    fm.all('OldOccupation').value = '';
    fm.all('HeadShip').value = '';
    fm.all('EmployDate').value = '';
    fm.all('RgtAddress').value = '';
    fm.all('Remark').value = '';
    fm.all('GrpFlag').value = '';
    fm.all('Operator').value = '';
    
    fm.all('IndueFormDate1').value = '';
    fm.all('FormDateModi').value = '';
    fm.all('FormDateOper').value = '';
    fm.all('AgentType').value = '';
    fm.all('AgentTypeName').value = '';
    if(getBranchType()!='2')
    fm.all('ChannelName').value ='';
    //行政信息
    fm.all('UpAgent').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('ManageCom').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('AgentState').value ='01';
    fm.all('InitGrade').value = '';
    fm.all('OldStartDate').value = '';
    fm.all('OldEndDate').value = '';
    fm.all('AgentLastGrade').value ='';
    fm.all('AgentLastSeries').value ='';
    fm.all('InsideFlag').value ='1';
    fm.all('InsideFlagName').value ='正常';
    
    //资格证信息
    fm.all('QualifNo').value= '';
	fm.all('GrantUnit').value= '';
	fm.all('GrantDate').value= '';
	fm.all('ValidStart').value= '';
	fm.all('ValidEnd').value= '';
	fm.all('QualifState').value= '';
	fm.all('QualifStateName').value= '';
  }
  catch(ex)
  {
    alert("在LAFRAgentInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在LAFRAgentInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
   // initWarrantorGrid();
  }
  catch(re)
  {
    alert("在LAFRAgentInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
