<%
//程序名称：AgentAscriptionInit.jsp
//程序功能：
//创建日期：2002-08-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    //fm.all('MarriageDate').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    //fm.all('PostalAddress').value = '';
    //fm.all('ZipCode').value = '';
    //fm.all('Phone').value = '';
    fm.all('IDNo').value = '';
    //fm.all('Source').value = '';
    //fm.all('WorkAge').value = '';
    fm.all('InsureCom').value = '';
    //fm.all('HeadShip').value = '';
    //fm.all('Reason').value = '';
    //fm.all('Business').value = '';
  }
  catch(ex)
  {
    alert("在AgentAscriptionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initAgentGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="黑名单编号"; //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[2]=new Array();
    iArray[2][0]="客户经理姓名"; //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="性别"; //列名
    iArray[3][1]="30px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="电话"; //列名
    iArray[4][1]="120px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
   iArray[5]=new Array();
    iArray[5][0]="身份证号码";         //列名
    iArray[5][1]="150px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="记入名单原因"; //列名
    iArray[6][1]="100px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[7]=new Array();
    iArray[7][0]="所属保险公司"; //列名
    iArray[7][1]="120px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许

    AgentGrid = new MulLineEnter( "fm" , "AgentGrid" );
    
        AgentGrid.mulLineCount = 0;   
        AgentGrid.displayTitle = 1;
      AgentGrid.hiddenPlus = 1;
      AgentGrid.hiddenSubtraction = 1;
        AgentGrid.locked=1;
        AgentGrid.canSel=1;
        AgentGrid.canChk=0;
        AgentGrid.loadMulLine(iArray); 
  }
  catch(ex)
  {
    alert("在AfterAscriptQryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initAgentGrid();   
  }
  catch(re)
  {
    alert("AfterAscriptQryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>