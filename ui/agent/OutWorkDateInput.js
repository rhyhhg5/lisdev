   //               该文件中包含客户端需要处理的函数和事件
//程序名称：OutWorkDateInput.js
//程序功能：Input.js
//创建日期：2017-10-24
//创建人  ：yangjian
var showInfo;
var mDebug="0";

try{
  var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
 }
 catch(ex)
 {}
 
//修改操作
function OutWorkDateUpdate()
{
	var i;
	var selFlag = true;
	
	var iCount = 0;
	
	var rowNum = OutWorkDateGrid.mulLineCount;
	if(!selFlag) return selFlag;
	
	
	for(i=0;i<rowNum;i++)
	{
		if(OutWorkDateGrid.getChkNo(i))
		{
			iCount++;
			var newState = OutWorkDateGrid.getRowColData(i,6);
			if((newState == null)||(newState == ""))
			{
				alert("第"+iCount+"行的离职日期（改）不能为空");
				OutWorkDateGrid.setFocus(i,6,OutWorkDateGrid);
				selFlag = false;
				break;
			}
		}
	}
	if(!selFlag) return selFlag;
	if(iCount == 0)
	{
		alert("请选择要修改的离职时间记录!");
		return false
	}
	
	
  var i = 0;
  var showStr="正在修改数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.action="./OutWorkDateSave.jsp";
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close(); 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
} 
//执行查询
function OutWorkDateQuery()
{
  fm.target="fraSubmit";
  //首先检验录入框
  if(!verifyForm("fm")) return false;
  // 拼SQL语句，从页面采集信息
  var strSQL = "select ManageCom,GroupAgentCode,Name,AgentCode,OutWorkDate,'',BranchType2,BranchType from laagent where 1=1 "    
		+ getWherePart('ManageCom','ManageCom','like')
		+ getWherePart('GroupAgentCode','GroupAgentCode')
		+ getWherePart('Name','Name')
		+ getWherePart('AgentCode','AgentCode')
		+ getWherePart('AgentState','AgentState')
		+ getWherePart('OutWorkDate','OutWorkDate')
		+ getWherePart('BranchType2','BranchType2')
		+ getWherePart('BranchType','BranchType')
		
		
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    OutWorkDateGrid.clearData('OutWorkDateGrid');  
    alert("查询失败！");
    return false;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = OutWorkDateGrid;    
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }	
}

