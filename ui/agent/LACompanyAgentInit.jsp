<%
//程序名称：LACompanyAgentInit.jsp
//程序功能：
//创建日期：2006-10-30 15:39:06
//创建人   
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('AgentCode').value = '';
    fm.all('Name').value = '';
    fm.all('EmployDate').value = '';
    fm.all('Operator').value = '';
    fm.all('ManageCom').value = '';
    fm.all('BranchAttr').value = '';
    fm.all('AgentState').value ='01';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value ='<%=BranchType2%>';
    if(('<%=BranchType%>')==6){
    	fm.all('State').disabled = true; 
    	fm.all('StateName').disabled = true; 
    }
  }
  catch(ex)
  {
    alert("LACompanyAgentInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
//    setOption("t_sex","0=男&1=女&2=不详");
//    setOption("sex","0=男&1=女&2=不详");
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");
  }
  catch(ex)
  {
    alert("LACompanyAgentInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
   // initWarrantorGrid();
  }
  catch(re)
  {
    alert("LACompanyAgentInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
