<%
//程序名称：LAAgentInput.jsp
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tGI = (GlobalInput)session.getValue("GI");
    String strManageCom = tGI.ComCode;
    String currdate = PubFun.getCurrentDate();
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                             
    fm.all('AgentCode').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('SexName').value = '';
    fm.all('Birthday').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('Phone').value = '';
    fm.all('Mobile').value = '';
    fm.all('IDNo').value = '';
    fm.all('HeadShip').value = '';
    fm.all('EmployDate').value = '';
    fm.all('OutWorkDate').value = '';
    fm.all('Noti').value = '';
    fm.all('UserType').value='';
    fm.all('UserTypeName').value='';
    fm.all('AgentCom').value='';
    fm.all('AgentComName').value='';//AgentId
    fm.all('AgentId').value='';
    fm.all('Password').value = '';
    fm.all('PasswordConfirm').value = '';
    fm.all('Operator').value = '';
    fm.all('LoginName').value = '';
    var manageCom = "<%=tGI.ManageCom%>";
    fm.all('ManageCom').value = manageCom;
    fm.all('ManageComName').value = '';
    fm.all('RuleState').value = '';
    fm.all('RuleStateName').value = '';
    fm.all('UserType').value = '';
    fm.all('AgentCode').value = '';
    
    fm.all('Mac_flag').value = '0';
    fm.all('backFlagName').value = '否';
  }
  catch(ex)
  {
    alert("在LAAgentInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAAgentInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    //initWarrantorGrid(); 
    showAllCodeName();   
  }
  catch(re)
  {
    alert("LAAgentInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
  
// 担保人信息的初始化
/*
function initWarrantorGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="姓名";          		        //列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="性别";         		        //列名
      iArray[2][1]="30px";            			//列宽
      iArray[2][2]=10;            			//列最大值
      iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][4]="Sex";
      iArray[2][5]="2|3";     //引用代码对应第几列，'|'为分割符
      iArray[2][6]="0|1";

      iArray[3]=new Array();
      iArray[3][0]="性别名称";      	   		//列名
      iArray[3][1]="30px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0; 
      
        
      iArray[4]=new Array();
      iArray[4][0]="身份证号码";      	   		//列名
      iArray[4][1]="130px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="单位";      	   		//列名
      iArray[5][1]="110px";            			//列宽
      iArray[5][2]=30;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
      iArray[6]=new Array();
      iArray[6][0]="家庭地址";      	   		//列名
      iArray[6][1]="120px";            			//列宽
      iArray[6][2]=40;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[7]=new Array();
      iArray[7][0]="手机";      	   		//列名
      iArray[7][1]="80px";            			//列宽
      iArray[7][2]=20;            			//列最大值
      iArray[7][3]=3;  
      
      iArray[8]=new Array();
      iArray[8][0]="邮政编码";      	   		//列名
      iArray[8][1]="50px";            			//列宽
      iArray[8][2]=20;            			//列最大值
      iArray[8][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="联系电话";      	   		//列名
      iArray[9][1]="80px";            			//列宽
      iArray[9][2]=20;            			//列最大值
      iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="关系";      	   		//列名
      iArray[10][1]="40px";            			//列宽
      iArray[10][2]=20;            			//列最大值
      iArray[10][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][4]='relaseries';
       iArray[10][5]="10|11";     //引用代码对应第几列，'|'为分割符
      iArray[10][6]="0|1";
      
      iArray[11]=new Array();
      iArray[11][0]="关系名称";      	   		//列名
      iArray[11][1]="50px";            			//列宽
      iArray[11][2]=20;            			//列最大值
      iArray[11][3]=2;              			//是否允许输入,1表示允许，0表示不允许
     
      WarrantorGrid = new MulLineEnter( "fm" , "WarrantorGrid" ); 
      //这些属性必须在loadMulLine前
      WarrantorGrid.mulLineCount = 1;   
      WarrantorGrid.displayTitle = 1;
      //WarrantorGrid.locked=1;      
      WarrantorGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
*/
}

</script>
