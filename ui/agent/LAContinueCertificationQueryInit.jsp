<%
//程序名称：LACertificationQueryInit.jsp
//程序功能：功能描述
//创建日期：2005-03-20 18:07:04
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('AgentCode').value = "";
    fm.all('CertifNo').value = "";
    //fm.all('Idx').value = "";
    fm.all('AuthorUnit').value = "";
    fm.all('GrantDate').value = "";
    fm.all('InvalidDate').value = "";
    
    //fm.all('InvalidRsn').value = "";
    //fm.all('reissueDate').value = "";
    //fm.all('reissueRsn').value = "";
    fm.all('StartValidDate').value = "";
    fm.all('StartValidDate').value = "";
    fm.all('CertiState').value = "";
    //fm.all('MakeDate').value = "";
    //fm.all('MakeTime').value = "";
    //fm.all('ModifyDate').value = "";
    //fm.all('ModifyTime').value = "";
  }
  catch(ex) {
    alert("在LACertificationQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLACertificationGrid();  
  }
  catch(re) {
    alert("LACertificationQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LACertificationGrid;
function initLACertificationGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="营销员代码";         		//列名
    iArray[1][1]="100px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="展业证书号";         		//列名
    iArray[2][1]="120px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="批准单位";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="发放日期";         		//列名
    iArray[4][1]="80px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]="0";         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array(); 
    iArray[5][0]="起始有效日期";   //列名
    iArray[5][1]="80px"        //宽度
    iArray[5][3]=100;        //最大长度
    iArray[5][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();  
    iArray[6][0]="截至有效日期";         		//列名 
    iArray[6][1]="80px"        //宽度 
    iArray[6][3]=100;        //最大长度 
    iArray[6][4]="0";  //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="展业证书状态";         		//列名
    iArray[7][1]="80px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="失效日期";         		//列名
    iArray[8][1]="80px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="失效原因";         		//列名
    iArray[9][1]="80px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="操作人";         		//列名
    iArray[10][1]="0px";         		//宽度
    iArray[10][3]=100;         		//最大长度
    iArray[10][4]=3;         		//是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="最近操作日";         		//列名
    iArray[11][1]="0px";         		//宽度
    iArray[11][3]=100;         		//最大长度
    iArray[11][4]=3;         		//是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="展业证状态";         		//列名
    iArray[12][1]="0px";         		//宽度
    iArray[12][3]=100;         		//最大长度
    iArray[12][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="距离展业证失效天数";         		//列名
    iArray[13][1]="100px";         		//宽度
    iArray[13][3]=100;         		//最大长度
    iArray[13][4]=0;         		//是否允许录入，0--不能，1--允许
    
                                                                                                                  
    LACertificationGrid = new MulLineEnter( "fm" , "LACertificationGrid" ); 
    //这些属性必须在loadMulLine前
 
    LACertificationGrid.mulLineCount = 0;   
    LACertificationGrid.displayTitle = 1;
    LACertificationGrid.hiddenPlus = 1;
    LACertificationGrid.hiddenSubtraction = 1;
    LACertificationGrid.canSel = 1;
    LACertificationGrid.canChk = 0;
    LACertificationGrid.selBoxEventFuncName = "showOne";
 
    LACertificationGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LACertificationGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
