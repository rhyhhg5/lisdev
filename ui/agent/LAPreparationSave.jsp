<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAscriptionSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人Abigale    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>


<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema;
  LAAgentSet tLAAgentSet = new LAAgentSet();
  LAPreparationUI tLAPreparationUI  = new LAPreparationUI();
 
  //输出参数
  CErrors tError = null;
  String tOperate="UPDATE||MAIN";
  String tRela  = "";
  String FlagStr = "Succ";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tFlag= request.getParameter("Flag");
  String tagent= request.getParameter("AgentCode"); 
  String tmanagecom= request.getParameter("ManageCom"); 
  String tbranchtype= request.getParameter("BranchType");
  String tbranchtype2= request.getParameter("BranchType2");
    //取得授权信息
  if(tFlag.equals("SELECT"))
  {
     System.out.println("begin LAAgent schema : "+tFlag);
     String tChk[] = request.getParameterValues("InpAscriptionGridChk");
     int lineCount = 0;
     int number = 0; 
     for(int index=0;index<tChk.length;index++)
     {
        if(tChk[index].equals("1"))           
        number++;
     }
     System.out.println("number"+number);    
     if(number==0)
	   {
       	Content = " 失败，原因:没有选择要调整的人员！";
      		FlagStr = "Fail";		
	   } 
     else
	   {
        String tAgentCode[]=request.getParameterValues("AscriptionGrid4");
        String tPreparaType[]=request.getParameterValues("AscriptionGrid13");
        System.out.println(tAgentCode[0]);      	 
	      lineCount = tChk.length; //行数
	      System.out.println("length= "+String.valueOf(lineCount));
	      
	      for(int i=0;i<lineCount;i++)
	      {
	        if(tChk[i].trim().equals("1"))
	        {
	           tLAAgentSchema = new LAAgentSchema();
	           System.out.println("选择的数据是:"+tChk[i]);
	           tLAAgentSchema.setAgentCode(tAgentCode[i]); 
	           tLAAgentSchema.setPreparaType(tPreparaType[i]); 
             tLAAgentSet.add(tLAAgentSchema);
	        }
	       } 
	       System.out.println("end 归属信息...");
	   }
  }	  
//'  else if(tFlag.equals("ALL"))
//'  {
//'       tLAAgentSchema = new LAAgentSchema();
//'       tLAAgentSchema.setAgentCode(tagent); 
//'       tLAAgentSet.add(tLAAgentSchema);
//'   }
  if(!FlagStr.equals("Fail"))
  {
	  // 准备传输数据 VData
    VData tVData = new VData();
    FlagStr="";
    tVData.add(tG);
    tVData.addElement(tLAAgentSet);
    
    System.out.println("add over");
    try
    {
      tLAPreparationUI.submitData(tVData,tOperate);
    }
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
   }   
   if (!FlagStr.equals("Fail"))
   {
     tError = tLAPreparationUI.mErrors;
     if (!tError.needDealError())
     {
     	Content = " 保存成功! ";
     	FlagStr = "Succ";
     }
     else
     {
     	Content = " 保存失败，原因是:" + tError.getFirstError();
     	FlagStr = "Fail";
     }
   }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

