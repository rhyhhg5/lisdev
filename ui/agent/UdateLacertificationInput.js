//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoUdateLacertificationInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//提交之前基本校验
function beforeSubmit()
{
}

//查询之前先检查数据
function check()
{
}
//页面查询
function easyQueryClick() {
	
	var strSql ="select a.groupagentcode,a.name,b.agentcode,b.CertifNo from laagent a,lacertification b where a.agentcode=b.agentcode "
				+ getWherePart("a.Name", "Name")
				+ getWherePart("b.CertifNo", "CertifNo")
				+ getWherePart("a.AgentCode", "AgentCode")
				+ getWherePart("a.groupagentcode", "GroupAgentcode")
				;
	//alert(strSql);
	turnPage.queryModal(strSql, PolGrid);
	showCodeName();
}
//Click事件，当点击“修改”图片时触发该函数
function updateclick()
{  // alert("**********");
	fm.fmAction.value = "UPDATE";
	//alert("aaaa");
	if (confirm("您确实想修改该记录吗?"))
	{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//alert("222222222");
	fm.action = "./UdateLacertificationSave.jsp";
	//alert("@@@@@@@@");
	fm.submit(); //提交
	}
	else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}           
function deleteclick()
{
  fm.fmAction.value = "DELETE";
  if (confirm("您确实想删除该记录吗?"))
	{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//alert("222222222");
	fm.action = "./UdateLacertificationSave.jsp";
	//alert("@@@@@@@@");
	fm.submit(); //提交
	}
	else
	{
		fm.OperateType.value = "";
		alert("您取消了删除操作！");
	}
}
function submitForm()
{
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.action = "./UdateLacertificationSave.jsp";
  fm.submit(); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


