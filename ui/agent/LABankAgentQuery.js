 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //initPolGrid();
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

function initEdorType(cObj)
{
	mEdorType = " 1 and codealias=#2# ";
	showCodeList('agentkind',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{
	mEdorType = " 1 and codealias=#2#";
	showCodeListKey('agentkind',[cObj], null, null, mEdorType, "1");
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
  }
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
  var arrReturn = new Array();
   if(fm.all('BranchType').value=='2')
	var tSel = AgentGrid.getSelNo();
   if(fm.all('BranchType').value=='3')
   var tSel = AgentGridB.getSelNo();
    if(fm.all('BranchType').value=='4')
   var tSel = AgentGridC.getSelNo();
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{
				arrReturn = getQueryResult();	
			    if(arrReturn==null) return false;
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
    arrSelected = new Array();

	var strSQL = "";
	if(fm.all('BranchType').value=='2')
	{
	  tRow = AgentGrid.getSelNo();
	  var AgentCode = AgentGrid.getRowColData(tRow-1,1);	   	
	if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected; 
	strSQL = "select getunitecode(a.AgentCode),a.Name,a.Sex,a.Birthday,a.NativePlace,a.Nationality,a.RgtAddress,a.HomeAddress,"
	         +"a.ZipCode,a.Phone,a.BP,a.Mobile,a.EMail,a.IDNo,a.PolityVisage,a.Degree,a.GraduateSchool,a.Speciality,"
	         +"a.PostTitle,a.OldCom,a.OldOccupation,a.HeadShip,a.EmployDate,a.AgentState,a.Remark,b.AgentKind,a.Operator,"
	         +"a.ManageCom,getUniteCode(c.branchmanager),b.AgentGrade,getUniteCode(c.branchmanager),c.AgentGroup,c.BranchLevel,a.InsideFlag,b.agentseries,"
	         +"c.branchattr,getUniteCode(b.TutorShip),b.InDueFormFlag,a.FullTimeFlag , c.InsideFlag,c.name,(select name from laagent where agentcode=b.TutorShip),"
	         +"a.branchtype2,b.initgrade,b.indueformdate,b.oldstartdate,b.oldenddate ,b.agentlastgrade,b.agentlastseries,a.insideflag,a.agenttype,a.retaincontno"
	        +" from LAAgent a,LATree b,LABranchGroup c where 1=1 "
	        +"and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup  and (a.AgentState is null or a.AgentState < '03') "
	        +getWherePart('b.BranchType','BranchType')
	        +getWherePart('b.BranchType2','BranchType2')
	        +" and a.AgentCode=getagentcode('"+AgentCode+"') and (c.state<>'1' or c.state is null)";
	}
	else if(fm.all('BranchType').value=='3')
	{
	  tRow = AgentGridB.getSelNo();
	  var AgentCode = AgentGridB.getRowColData(tRow-1,1);
	  if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected; 
	 strSQL = "select getunitecode(a.AgentCode),a.Name,a.Sex,a.Birthday,a.NativePlace,a.Nationality,a.RgtAddress,a.HomeAddress,a.ZipCode,a.Phone,a.BP,a.Mobile,a.EMail,a.IDNo,a.PolityVisage,a.Degree,a.GraduateSchool,a.Speciality,a.PostTitle,a.OldCom,a.OldOccupation,a.HeadShip,a.EmployDate,a.AgentState,a.Remark,b.AgentKind,a.Operator,a.ManageCom,getUniteCode(c.branchmanager),b.AgentGrade,getUniteCode(c.branchmanager),c.AgentGroup,c.BranchLevel,a.InsideFlag,b.agentseries,c.branchattr,getUniteCode(b.TutorShip),b.InDueFormFlag,a.FullTimeFlag,b.isConnMan,b.SpeciFlag,b.agentgrade1,b.agentseries1,a.quafno,a.quafstartdate,a.salequaf "
	        +" from LAAgent a,LATree b,LABranchGroup c where 1=1 "
	        +"and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup "
	        +getWherePart('b.BranchType','BranchType')
	        +getWherePart('b.BranchType2','BranchType2')
	        +" and a.AgentCode=getagentcode('"+AgentCode+"') and (c.state<>'1' or c.state is null)";
	 }
	else if(fm.all('BranchType').value=='4')
	{
	  tRow = AgentGridC.getSelNo();
	  var AgentCode = AgentGridC.getRowColData(tRow-1,1);	   	
	if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected; 
	strSQL = "select getunitecode(a.AgentCode),a.Name,a.Sex,a.Birthday,a.NativePlace,a.Nationality,a.RgtAddress,a.HomeAddress,a.ZipCode,a.Phone,a.BP,a.Mobile,a.EMail,a.IDNo,a.PolityVisage,a.Degree,a.GraduateSchool,a.Speciality,a.PostTitle,a.OldCom,a.OldOccupation,a.HeadShip,a.EmployDate,a.AgentState,a.Remark,b.AgentKind,a.Operator,a.ManageCom,getUniteCode(c.branchmanager),b.AgentGrade,getUniteCode(c.branchmanager),c.AgentGroup,c.BranchLevel,a.InsideFlag,b.agentseries,c.branchattr,getUniteCode(b.TutorShip),b.InDueFormFlag,a.FullTimeFlag , c.InsideFlag,c.name,(select name from laagent where agentcode=b.TutorShip) "
	        +" from LAAgent a,LATree b,LABranchGroup c where 1=1 "
	        +"and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup "
	        +getWherePart('b.BranchType','BranchType')
	        +getWherePart('b.BranchType2','BranchType2')
	        +" and a.AgentCode=getagentcode('"+AgentCode+"') and (c.state<>'1' or c.state is null)";
	}
	//alert("111" + tRow);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  alert("离职登记或者离职确认人员不能进行修改操作");
    
    // 添加 2012-5-23
     return null;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);

	return arrSelected;
}

/**
 * 点击取得管理机构
 */
function clickBranchType2(cObj,cObjName)
{
	
	var mCondition =" 1 and CODEALIAS =#"+'2'+"#";
	showCodeList('BranchType2',[cObj,cObjName],[0,1],null,mCondition,1);
	return true;
}

/**
 * 按键取得管理机构
 */
function keyGetBranchType2(cObj,cObjName)
{
	var mCondition =" 1 and CODEALIAS =#"+'2'+"#";
	showCodeListKey('BranchType2',[cObj,cObjName],[0,1],null,mCondition,1);
	return true;
}


// 查询按钮
function easyQueryClick()
{
	var tReturn = getManageComLimitlike("a.managecom");
	// 初始化表格
	if(fm.all('BranchType').value=='2')
	{
	initAgentGrid();	
	if (verifyInput() == false)
	return false;	
	// alert(fm.all('BranchType2').value);
	        //不选择
	        /*
			if(fm.all('BranchType2').value=='01')
		    {
		    }
		    //选择  02
		    else
		    {
		    //alert(fm.all('BranchType2').value);
		      if(fm.all('ManageCom').value=='86'||fm.all('ManageCom').value.substring(0,4)=='8611')
	          {
	          }
	          else
	          {
	            if(fm.all('BranchType2').value!='02'){
	            alert("只能选择渠道类型为“02-中介”");
	            fm.all('BranchType2').value='02';
                fm.all('BranchType2Name').value='中介';
                return false;
                }
              }
		    }
	
	*/

	// 书写SQL语句
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSQL = "";
		strSQL = "select getunitecode(a.agentcode),c.branchattr,d.agentgrade,a.name,a.idno,(case a.agentstate when '01' then '一次入司' when '02' then '二次入司' when '03' then '一次离职登记' when '04' then '二次离职登记' when '06' then '离职确认' end)"
	+",case when (a.fulltimeflag is null or a.fulltimeflag='1') then '常规业务员' else '职团开拓'  end,case a.agenttype when '1' then '营销人员' else '直销人员' end"
	     +"  from LAAgent a,LABranchGroup c,latree d where 1=1 "
	         + "and a.agentgroup = c.AgentGroup  and (c.state<>'1' or c.state is null) and a.agentcode=d.agentcode"
	         + tReturn
	         //+ getWherePart('a.AgentCode','AgentCode','like')
	         +strAgent
	         + getWherePart('c.BranchAttr','AgentGroup','like')
	         + getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Name','Name','like')
	         + getWherePart('a.Sex','Sex')
	         + getWherePart('a.Birthday','Birthday')
	         + getWherePart('a.IDNo','IDNo')
	         //+ getWherePart('a.AgentKind','AgentKind')
	         + getWherePart('a.EmployDate','EmployDate')
	         //add by zhuxt 20140903
	         + getWherePart('a.AgentType','AgentType')
	         //end add
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2');
	         
	         //+ getWherePart('a.agentstate','AgentState1');
	         if(fm.all('BranchType2').value=='01')
	        {
	            if(fm.all('AgentState1').value!=''||fm.all('AgentState1').value==null)
	            {
		        strSQL += " and a.agentstate = '"+fm.all('AgentState1').value+"'  ";
		        }
	        }
	        if(fm.all('BranchType2').value=='02')
		       {
		            if(fm.all('AgentState2').value!=''||fm.all('AgentState2').value==null)
		            {
			        strSQL += " and a.agentstate = '"+fm.all('AgentState2').value+"'  ";
			        }
		       }
	        if(fm.GrpFlag.value=='1')
	        {
		        strSQL += " and (a.fulltimeflag='1' or a.fulltimeflag is null)  ";
	        }
	        else if(fm.GrpFlag.value=='2')
		    {
			        strSQL += " and a.fulltimeflag = '2' ";
		    }
		    if(fm.all('BranchType2').value==''||fm.all('BranchType2').value==null)
		    {
		      if(fm.all('ManageCom').value=='86'||fm.all('ManageCom').value.substring(0,4)=='8611')
	          {
	           strSQL += " and 1=1  ";
	          }
	          else
	          {
	             strSQL += " and a.BranchType2 ='02' ";
              }
		    }
        }
       if(fm.all('BranchType').value=='3')
	   {
		initAgentGridB();
	
		// 书写SQL语句
		var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
		var strSQL = "";
			strSQL = "select getunitecode(a.agentcode),c.branchattr,d.agentgrade,a.name,a.idno,case a.agentstate when '01' then '在职' when '03' then '离职登记' else '离职确认' end"
		//+",case when (a.fulltimeflag is null or a.fulltimeflag='1') then '常规业务员' else '职团开拓'  end "
		     +"  from LAAgent a,LABranchGroup c,latree d where 1=1 "
		         + "and a.agentgroup = c.AgentGroup  and (c.state<>'1' or c.state is null) and a.agentcode=d.agentcode"
		         //and (a.AgentState is null or a.AgentState < '06')
		         + tReturn
		        // + getWherePart('a.AgentCode','AgentCode','like')
		         + strAgent
		         + getWherePart('c.BranchAttr','AgentGroup','like')
		         + getWherePart('a.ManageCom','ManageCom','like')
		         + getWherePart('a.Name','Name','like')
		         + getWherePart('a.Sex','Sex')
		         + getWherePart('a.Birthday','Birthday')
		         + getWherePart('a.IDNo','IDNo')
		      //   + getWherePart('a.AgentKind','AgentKind')
		         + getWherePart('a.EmployDate','EmployDate')
		         + getWherePart('a.BranchType','BranchType')
		         + getWherePart('a.BranchType2','BranchType2');
		         //与前面查询条件保持一致
		       if(fm.AgentState.value=='01')
	        {
		        strSQL += " and a.agentstate<='02'  ";
	        }
	        if(fm.AgentState.value=='02')
		      {
			        strSQL += " and a.agentstate = '03' ";
		      }
		      if(fm.AgentState.value=='03')
		      {
			        strSQL += " and a.agentstate > '03' ";
		      }
	        } 
	        
 if(fm.all('BranchType').value=='4')
	{
	initAgentGridC();

	// 书写SQL语句
		var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSQL = "";
		strSQL = "select getunitecode(a.agentcode),c.branchattr,d.agentgrade,a.name,a.idno,a.agentstate"
	+",case when (a.fulltimeflag is null or a.fulltimeflag='1') then '常规业务员' else '职团开拓'  end "
	     +"  from LAAgent a,LABranchGroup c,latree d where 1=1 "
	         + "and a.agentgroup = c.AgentGroup and (a.AgentState is null or a.AgentState < '03') and (c.state<>'1' or c.state is null) and a.agentcode=d.agentcode"
	         + tReturn
	        // + getWherePart('a.AgentCode','AgentCode','like')
	         + strAgent
	         + getWherePart('c.BranchAttr','AgentGroup','like')
	         + getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Name','Name','like')
	         + getWherePart('a.Sex','Sex')
	         + getWherePart('a.Birthday','Birthday')
	         + getWherePart('a.IDNo','IDNo')
	      //   + getWherePart('a.AgentKind','AgentKind')
	         + getWherePart('a.EmployDate','EmployDate')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2');
	        if(fm.GrpFlag.value=='1')
	        {
		        strSQL += " and (a.fulltimeflag='1' or a.fulltimeflag is null)  ";
	        }
	        else if(fm.GrpFlag.value=='2')
		      {
			        strSQL += " and a.fulltimeflag = '2' ";
		      }
        }
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
 if(fm.all('BranchType').value=='2')
  turnPage.pageDisplayGrid = AgentGrid;
 else if(fm.all('BranchType').value=='3')
   turnPage.pageDisplayGrid = AgentGridB;
   else if(fm.all('BranchType').value=='4')
   turnPage.pageDisplayGrid = AgentGridC;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果

  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}


function doDownLoad(){
	var tReturn = getManageComLimitlike("a.managecom");
	  if( verifyInput() == false ) return false;
		if(fm.all('BranchType').value=='2')
		{
		if (verifyInput() == false)
		return false;	
		// 书写SQL语句
		var strAgent = "";
		if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
			strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
		}
		var strSql = "";
			strSql = "select getunitecode(a.agentcode),c.branchattr,d.agentgrade,a.name,a.idno,(case a.agentstate when '01' then '一次入司' when '02' then '二次入司' when '03' then '一次离职登记' when '04' then '二次离职登记' when '06' then '离职确认' end)"
		+",case when (a.fulltimeflag is null or a.fulltimeflag='1') then '常规业务员' else '职团开拓'  end,case a.agenttype when '1' then '营销人员' else '直销人员' end"
		     +"  from LAAgent a,LABranchGroup c,latree d where 1=1 "
		         + "and a.agentgroup = c.AgentGroup  and (c.state<>'1' or c.state is null) and a.agentcode=d.agentcode"
		         + tReturn
		         //+ getWherePart('a.AgentCode','AgentCode','like')
		         +strAgent
		         + getWherePart('c.BranchAttr','AgentGroup','like')
		         + getWherePart('a.ManageCom','ManageCom','like')
		         + getWherePart('a.Name','Name','like')
		         + getWherePart('a.Sex','Sex')
		         + getWherePart('a.Birthday','Birthday')
		         + getWherePart('a.IDNo','IDNo')
		         //+ getWherePart('a.AgentKind','AgentKind')
		         + getWherePart('a.EmployDate','EmployDate')
		         //add by zhuxt 20140903
		         + getWherePart('a.AgentType','AgentType')
		         //end add
		         + getWherePart('a.BranchType','BranchType')
		         + getWherePart('a.BranchType2','BranchType2');
		         
		         //+ getWherePart('a.agentstate','AgentState1');
		         if(fm.all('BranchType2').value=='01')
		        {
		            if(fm.all('AgentState1').value!=''||fm.all('AgentState1').value==null)
		            {
		            	strSql += " and a.agentstate = '"+fm.all('AgentState1').value+"'  ";
			        }
		        }
		        if(fm.all('BranchType2').value=='02')
			       {
			            if(fm.all('AgentState2').value!=''||fm.all('AgentState2').value==null)
			            {
			            	strSql += " and a.agentstate = '"+fm.all('AgentState2').value+"'  ";
				        }
			       }
		        if(fm.GrpFlag.value=='1')
		        {
		        	strSql += " and (a.fulltimeflag='1' or a.fulltimeflag is null)  ";
		        }
		        else if(fm.GrpFlag.value=='2')
			    {
		        	strSql += " and a.fulltimeflag = '2' ";
			    }
			    if(fm.all('BranchType2').value==''||fm.all('BranchType2').value==null)
			    {
			      if(fm.all('ManageCom').value=='86'||fm.all('ManageCom').value.substring(0,4)=='8611')
		          {
			    	  strSql += " and 1=1  ";
		          }
		          else
		          {
		        	  strSql += " and a.BranchType2 ='02' ";
	              }
			    }
	        }
		var mArr = easyExecSql(strSql);
		fm.querySql.value = strSql;
		if(fm.querySql.value!=null && fm.querySql.value!=""&& mArr!=null){
			var formAction = fm.action;
			fm.action = "LABankAgentQueryDownload.jsp";
			fm.submit();
			fm.target = "fraSubmit";
			fm.action = formAction;
		}else{
			alert("没有数据");
			return;
		}
}


