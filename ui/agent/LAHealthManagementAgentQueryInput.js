//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
    var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
      alert( "请先选择一条记录，再点击返回按钮" );
	else
	{
			try
			{	
				arrReturn = getQueryResult();
				top.opener.afterQuery(arrReturn);
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	tRow = AgentGrid.getSelNo();
	if( tRow == 0 || tRow == null)
	  return arrSelected;
	var arrSelected = new Array();
	var strSQL = "";
	strSQL = "Select "
        +"getunitecode(a.Agentcode), "
        +"a.name, "
        +"a.Sex, "
        +"(Select codename From ldcode where codetype = 'sex' and code=a.sex),"
        +"a.birthday,"
        +"a.Nationality,"
        +"(Select codename From ldcode where codetype = 'nationality' and code=a.Nationality),"
        +"a.Degree,"
        +"(Select codename From ldcode where codetype = 'degree' and code=a.Degree),"
        +"a.IDNoType,"
        +"(Select codename From ldcode where codetype = 'idtype' and code=a.IDNoType), "
        +"a.IDNo,"
        +"a.EmployDate,"
        +"a.Speciality,"
        +"a.HomeAddress,"
        +"a.ZipCode,"
        +"a.Phone,"
        +"a.Mobile,"
        +"a.InsideFlag,"
        +"(case a.InsideFlag when '0' then '合同制' when '1' then '代理制' else '' end),"
        +"a.BranchType,"
        +"a.BranchType2,"
        +"(Select QualifNo from LAQualification where agentcode = a.agentcode),"
        +"(Select GrantUnit from LAQualification where agentcode = a.agentcode),"
        +"(Select GrantDate from LAQualification where agentcode = a.agentcode),"
        +"(Select ValidStart from LAQualification where agentcode = a.agentcode),"
        +"(Select ValidEnd from LAQualification where agentcode = a.agentcode),"
        +"(Select State from LAQualification where agentcode = a.agentcode),"
        +"(case (Select State from LAQualification where agentcode = a.agentcode) when '0' then '有效' when '1' then '失效' else '' end ),"
        +"b.agentgrade,"
        +"(Select gradename from laagentgrade where gradecode = b.agentgrade),"
        +"c.branchattr,"
        +"c.name,"
        +"a.ManageCom,"
        +"getunitecode(b.introagency),"
        +"(Select name from laagent where agentcode = b.introagency), "
        +"a.AgentGroup,"
        +"a.AgentState,"
        +"a.AgentType,"
        +"a.RetainContno,"
        +"b.AssessType,"
        +"b.InDueFormFlag,a.agentkind "
        +" from LAAgent a,LATree b,LABranchGroup c  "
        +" where 1=1 "
        +" and a.AgentCode = b.AgentCode and a.AgentGroup = c.AgentGroup "
        +" and (c.state<>'1' or c.state is null) "
        +" and a.GroupAgentCode='"+AgentGrid.getRowColData(tRow-1,1)+"'";

	    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
        //判断是否查询成功
       if (!turnPage.strQueryResult) {
         alert("查询失败！");
         return false;
       }
       //查询成功则拆分字符串，返回二维数组
       arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
    if(!verifyInput()) return false;

	initAgentGrid();
	var tReturn = getManageComLimitlike("a.managecom");
	// 书写SQL语句
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSQL = "";
	strSQL = "select getunitecode(a.agentcode),b.BranchAttr,a.managecom,a.name,c.agentgrade,a.idno,a.agentstate,"
	         + "case when a.agentstate='01' then '在职'  when a.agentstate='02' then '二次增员' "
	         + " when a.agentstate='03' then '离职登记' when a.agentstate='04' then '二次离职登记' "
	         + " when a.agentstate='06' then '离职确认' when a.agentstate='07' then '二次离职确认' end,"
	         + "case when a.agenttype = '1' then '营销人员' when a.agenttype = '2' then '直销人员' when a.agenttype='3' then '劳务派遣制' end   "	         
	         + " from LAAgent a,LABranchGroup b,LATree c where 1=1 "
	         + " and a.Agentgroup = b.agentGroup and c.agentcode=a.agentcode and (b.state<>'1' or b.state is null)";
	         if(fm.all("AgentGroup").value!=null && fm.all("AgentGroup").value!='')
     	     {
	             strSQL+= " and b.branchattr like '"+fm.all('AgentGroup').value+"%'";
	         }
	         strSQL+=" and 1= 1 "
	         + tReturn
	        // + getWherePart('a.AgentCode','AgentCode')
	         + strAgent
	         + getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Name','Name')
	         + getWherePart('a.IDNo','IDNo')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	         + getWherePart('a.AgentType','AgentType')
	         + getWherePart('a.IDNoType','IDNoType') ; 
     	turnPage.queryModal(strSQL, AgentGrid); 
}

function downLoad()
{
	// 初始化表格
    if(!verifyInput()) return false;
	var rowNum = AgentGrid.mulLineCount;
	if(rowNum<=0) {
	    alert("请先查询出所要下载的人员信息");
		return false;
	}	
	var tReturn = getManageComLimitlike("a.managecom");
	// 书写SQL语句
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSQL = "";
	strSQL = "select getunitecode(a.agentcode),b.BranchAttr,a.managecom,a.name,c.agentgrade,a.idno,"
	         + "case when a.agentstate='01' then '在职'  when a.agentstate='02' then '二次增员' "
	         + " when a.agentstate='03' then '离职登记' when a.agentstate='04' then '二次离职登记' "
	         + " when a.agentstate='06' then '离职确认' when a.agentstate='07' then '二次离职确认' end," 
	         + " case when a.agenttype = '1' then '营销人员' when a.agenttype = '2' then '直销人员' end ,a.employdate,a.outworkdate "
	         + " from LAAgent a,LABranchGroup b,LATree c where 1=1 "
	         + " and a.Agentgroup = b.agentGroup and c.agentcode=a.agentcode and (b.state<>'1' or b.state is null)";
	         if(fm.all("AgentGroup").value!=null && fm.all("AgentGroup").value!='')
     	     {
	             strSQL+= " and b.branchattr like '"+fm.all('AgentGroup').value+"%'";
	         }
	         strSQL+=" and 1= 1 "
	         + tReturn
	        // + getWherePart('a.AgentCode','AgentCode')
	         + strAgent
	         + getWherePart('a.ManageCom','ManageCom','like')
	         + getWherePart('a.Name','Name')
	         + getWherePart('a.IDNo','IDNo')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	         + getWherePart('a.AgentType','AgentType')
	         + getWherePart('a.IDNoType','IDNoType') ; 
//     	turnPage.queryModal(strSQL, AgentGrid); 
	 		var mArr = easyExecSql(strSQL);
			fm.querySql.value = strSQL;
			
			if(fm.querySql.value!=null && fm.querySql.value!=""&& mArr!=null){
				var formAction = fm.action;
				fm.action = "LAInteractionAgentDownload.jsp";
				fm.submit();
				fm.target = "fraSubmit";
				fm.action = formAction;
			}else{
				alert("没有数据");
				return;
			}     
}

