<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  //System.out.println("BranchType:"+BranchType);
  //System.out.println("BranchType2:"+BranchType2);

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAladimissionQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAgentladimissionQInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>营销员查询 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAgentQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                营销员查询条件
            </td>
            </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          营销员编码 
        </TD>
        <TD  class= input> 
          <Input class=common  name=AgentCode >
        </TD>
        <TD class= title>
          销售机构 
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup > 
        </TD>
        <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:comcode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>        
      </TR>
      <TR  class= common> 
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD>        
       <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" verify="性别|code:Sex" CodeData="0|男|1|女" ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,null,'100');" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" debug='10'
          ><Input name=SexName class="codename"  >
        </TD> 
        <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class=common > 
        </TD>
      </TR-->
      <TR  class= common> 
         <TD  class= title>
           证件号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common > 
        </TD>       
        <TD  class= title>
          入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' > 
        </TD>
        <TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='codeno'  maxlength=2 CodeData="01|在职|02|二次增员|03|离职登记|04|二次离职|05|考核清退|06|离职确认|07|二次离职确认|08|考核清退确认" ondblclick="return  showCodeList('AgentState',[this,AgentStateName],[0,1]);" 
          onkeyup="return showCodeListKey('AgentState',[thisAgentStateName],[0,1]);" 
          ><Input name=AgentStateName class="codename" >
        </TD>
      </TR>
      <TR  class= common>
      	<TD  class= title>
          特殊人员 
        </TD>
        <TD  class= input>
          <Input name=Speciflag class="codeno"  
            CodeData= "0|2^00|否^01|是" 
          ondblclick="showCodeListEx('Speciflag',[this,SpeciflagName],[0,1]);" 
          onkeyup="showCodeListKeyEx('Speciflag',[this,SpeciflagName],[0,1]);" 
          ><Input name=SpeciflagName class="codename"  readonly> 
        </TD> 
        <TD class= title>筹备标记</TD>
					  <TD class= input>
						<Input class=codeno readOnly name=NoWorkFlag verify="筹备标记" CodeData="0|^Y|是|^N|否"
						 ondblClick="showCodeListEx('noworkflag',[this,NoWorkFlagName],[0,1],null,null,null,1);  "
             onkeyup="showCodeListKeyEx('noworkflag',[this,NoWorkFlagName],[0,1],null,null,null,1);"
             ><Input class=codename name=NoWorkFlagName  readOnly >
					</TD> 
    	</TR>
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <input type=hidden class=Common name=Title >
          
          <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();">
          <INPUT VALUE="返  回" class="cssbutton" TYPE=button onclick="returnParent();"> 	
    </Div>      
          				
    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 营销员结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 						
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
