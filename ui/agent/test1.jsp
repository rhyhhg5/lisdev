<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：LABranchGroupInput.jsp
	//程序功能：
	//创建日期：
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String Operator = tG.Operator;
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
%>

<%@page contentType="text/html;charset=GBK"%>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="test1.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
</head>
<body onload="initElementtype();">
<form action="TestSave.jsp" method=post name=fm target="fraSubmit">
<span id="operateButton">
	<table class="common" align=center>
		<tr align=right>
			<td class=button >
				&nbsp;&nbsp;
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="查询"  name=Query TYPE=button onclick="return queryClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="修改" name=Modify TYPE=button onclick="return updateClick();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="保存" name=Save TYPE=button onclick="return submitForm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="删除"  name=Delete TYPE=button onclick="return deleteClick();">
			</td>
		</tr>
	</table>
</span>
<table>
	<tr class=common>
		<td class=titleImg>查询条件</td>
	</tr>
</table>
<Div id="divLABranchGroup1" style="display: ''">
<table class=common>
	<TR class=common>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class='codeno' name=ManageCom
			verify="管理机构|notnull"
			ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" 
			readonly ><Input class=codename name=ManageComName elementtype=nacessary>
			</TD>			
		<TD class=title>薪资月</TD>
		<TD class=input><Input class=common name=WageNo verify="薪资月|notnull" elementtype=nacessary onchange="return getBranch();">
		<font color = red >yymm</font>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>职级</TD>
		<TD class=input><Input class=common name=AgentGrade verify="职级|notnull" elementtype=nacessary onchange="return getBranch();">
		</TD>
		<TD class=title>考核状态</TD>
		<TD class=input><Input class=
		common name=State verify="考核状态|notnull&len=1&value>=0&value<=2"  elementtype=nacessary onchange="return getBranch();">
		</TD>
	</TR>
	<TR class=common>
	<TD class=title>展业类型</TD>
		<TD class=input><Input class='codeno' name=BranchType
		    verify="展业类型|notnull"
		    ondblclick="return showCodeList('branchtype',[this,BranchTypeName],[0,1]);"
			onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],[0,1]);"
			readonly><Input class=codename name=BranchTypeName  elementtype=nacessary>
	   </TD>
		<TD class=title>销售渠道</TD>
		<TD class=input><Input class='codeno' name=BranchType2
		    verify="销售渠道|notnull"
		    ondblclick="return showCodeList('branchtype2',[this,BranchType2Name],[0,1]);"
			onkeyup="return showCodeListKey('branchtype2',[this,BranchType2Name],[0,1]);"
			readonly><Input class=codename name=BranchType2Name elementtype=nacessary>
	   </TD>
	</TR>
</table>
</Div>

<table class='common'>
	<p><font color="red">注：考核状态：有三种状态，分别是考核未确认（0）、考核确认未归属（1）、组织归属完毕（2），此功能组织归属完毕状态不修改。</font></p>
</table>
<!-- <input type=hidden name=MakeDate >
<input type=hidden name=MakeTime>
<input type=hidden name=AClass> -->


<input type=hidden name=hideOperate value=''>  
<input type=hidden name=AgentGroup value=''> <!--后台操作的隐式机构编码，不随机构的调整而改变 --> 
<input type=hidden name=FoundDate value=''>
<input type=hidden name=ReturnFlag value=''>
<input type=hidden name=BranchManager value=''>
<span  id="spanCode" style="display: none; position:absolute; slategray"></span>
</form>
</body>
</html>
