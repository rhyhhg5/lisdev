<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-03-20 18:07:04
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
    String agentcode=request.getParameter("AgentCode");
    if (agentcode==null || agentcode.equals(""))
    {
       agentcode="";
    }
  String BranchType=request.getParameter("BranchType");
  //String BranchType2=request.getParameter("BranchType2");
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAContinueCertificationInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAContinueCertificationInit.jsp"%>
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="./LAContinueCertificationSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCode1);">
    		</td>
    		 <td class= titleImg >
        		 展业证基本信息
       		 </td>   		 
    	</tr>
    </table>
<table  class= common align='center'>
  <TR  class= common>
    <TD  class= title>
      <%=tTitleAgent%>代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=GroupAgentCode verify="营销员代码|NotNull " elementtype=nacessary onchange="return checkvalid();">
    </TD>
  	<TD  class= title>
      <%=tTitleAgent%>姓名
    </TD>
    <TD  class= input>
      <Input class= 'readonly' name=AgentName readonly>
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      展业证书号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=CertifNo verify="展业证书号|NotNull " elementtype=nacessary onblur="checkInput(this.value);">
    </TD>
    <TD  class= title>
      批准单位
    </TD>
    <TD  class= input>
      <Input class= 'common' name=AuthorUnit verify="批准单位|NotNull " elementtype=nacessary >
    </TD>
  </TR>
  <TR  class= common> 
    <TD  class= title>
      发放日期
    </TD>
    <TD  class= input>
     <Input class= "coolDatePicker" dateFormat="short" name=GrantDate verify="发放日期|NOTNULL&DATE"  elementtype=nacessary> 
    </TD>
    <TD  class= title>
      起始有效期
    </TD>
    <TD  class= input>
     <Input class= "coolDatePicker" dateFormat="short" name=StartValidDate verify="起始有效期|NOTNULL&DATE " elementtype=nacessary> 
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      截至有效日期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=EndValidDate verify="截至有效日期|NOTNULL&DATE " elementtype=nacessary > 
    </TD>
    <TD  class= title>
      展业证书状态
    </TD>
    <TD class=input><input name=CertiState class="codeno" name="CertiState" verify="展业证书状态|NOTNULL&DATE " 
         CodeData="0|^0|有效|^1|失效"
         ondblClick="showCodeListEx('CertiStateList',[this,CertiStateName],[0,1]);"
         onkeyup="showCodeListKeyEx('CertiStateList',[this,CertiStateName],[0,1]);"
         ><Input class=codename name=CertiStateName readOnly elementtype=nacessary>
    </TD>
  </TR>
  <TR class= common>
    <TD  class= title>
      失效日期
    </TD>
    <TD  class= input>
      <Input class= "coolDatePicker" dateFormat="short" name=InvalidDate > 
    </TD>
  	<TD  class= title>
      失效原因
    </TD>
    <TD  class= input>
      <Input class= 'common' name=InvalidRsn >
    </TD>
  </TR>
  <TR class= common>
    <TD  class= title>
      操作员
    </TD>
    <TD  class= input>
      <Input class= "readonly" readonly name=Operator> 
    </TD>
  	<TD  class= title>
      最近操作日
    </TD>
    <TD  class= input>
      <Input class= "readonly" readonly name=ModifyDate> 
    </TD>
  </TR> 
  
  <TR class=common>
	<TD class=title>
		
	</TD>
	<TD class=input>
		<Input class="readonly" type=hidden name=diskimporttype>
</TR>

</table>

<br><hr>

    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden name=BranchType value=''> 
    <input type=hidden name=BranchType2 value=''> 
    <input type=hidden name=CertiState1 value=''>
    <input type=hidden name=CertifNoQuery value=''>
    <input type=hidden name=AgentCode value=''>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
