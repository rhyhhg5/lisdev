<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LABankAgentInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LABankAgentInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LABankAgentSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
          <td class=titleImg>
            员工信息
          </td>
        </td>
      </tr> 
    </table>
    <Div  id= "divLAAgent1" style= "display: ''">
      
    <table  class= common>
      <TR  class= common>
       <TD class= title> 
          员工代码
        </TD>
        <TD  class= input> 
          <Input class= 'readonly' readonly name=AgentCode >
        </TD>       
	<TD  class= title>
          员工姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common verify="姓名|NotNull&len<=20">
        </TD> 
        
        <TD  class= title>
          性别 
        </TD>
        <TD  class= input>
          <Input name=Sex class="code" verify="性别|code:Sex" ondblclick="return showCodeList('Sex',[this]);" 
		onkeyup="return showCodeListKey('Sex',[this]);" >
        </TD> 
      </TR> 
      <TR  class= common> 
        <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class=common  verify="出生日期|NotNull&Date"> 
        </TD>
         <TD  class= title>
          民族
        </TD>
        <TD  class= input>
          <Input name=Nationality class="code" id="Nationality" ondblclick="return showCodeList('Nationality',[this]);" 			onkeyup="return showCodeListKey('Nationality',[this]);" > 
        </TD>
        <TD  class= title> 
          籍贯
        </TD>
        <TD  class= input>
          <Input name=NativePlace class="code" verify="籍贯|code:NativePlaceBak" id="NativePlaceBak" 
			ondblclick="return showCodeList('NativePlaceBak',[this]);" 
			onkeyup="return showCodeListKey('NativePlaceBak',[this]);">
        </TD>  
      </TR>
      <TR  class= common> 
         <TD  class= title>
          户口所在地
        </TD>
        <TD  class= input> 
          <Input name=RgtAddress class="code" ondblclick="return showCodeList('NativePlaceBak',[this]);" 
		onkeyup="return showCodeListKey('NativePlaceBak',[this]);"> 
        </TD>
        <TD  class= title>
          政治面貌 
        </TD>
       <TD  class= input> 
          <Input name=PolityVisage class="code" verify="政治面貌|code:polityvisage" id="polityvisage" 
		ondblclick="return showCodeList('polityvisage',[this]);" 
		onkeyup="return showCodeListKey('polityvisage',[this]);" > 
        </TD> 
        <TD  class= title>
          身份证号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common verify="身份证号码|notnull&len<=20" onchange="return changeIDNo();"> 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          学历 
        </TD>
        <TD  class= input> 
          <Input name=Degree class="code" verify="学历|code:Degree" id="Degree" 
		ondblclick="return showCodeList('Degree',[this]);" 
		onkeyup="return showCodeListKey('Degree',[this]);"> 
        </TD>
       <TD  class= title> 
          毕业院校
        </TD>
        <TD  class= input> 
          <Input name=GraduateSchool class= common > 
        </TD>
        <TD  class= title>
          专业 
        </TD>
        <TD  class= input> 
          <Input name=Speciality class= common > 
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          职称 
        </TD>
        <TD  class= input> 
          <Input name=PostTitle class='code' verify="职称|code:posttitle" 
		ondblclick="return showCodeList('posttitle',[this]);" 
		onkeyup="return showCodeListKey('posttitle',[this]);" > 
        </TD>
        <TD  class= title>
          家庭地址 
        </TD>
        <TD  class= input> 
          <Input name=HomeAddress class= common > 
        </TD>
        <TD  class= title>
          邮政编码 
        </TD>
        <TD  class= input> 
          <Input name=ZipCode class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          住宅电话 
        </TD>
        <TD  class= input> 
          <Input name=Phone class= common > 
        </TD>
        <TD  class= title> 
          传呼
        </TD>
        <TD  class= input>
          <Input name=BP class= common > 
        </TD>
        <TD  class= title>
          手机 
        </TD>
        <TD  class= input> 
          <Input name=Mobile class= common > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          E-mail 
        </TD>
        <TD  class= input> 
          <Input name=EMail class= common > 
        </TD>
         <TD  class= title>
          原工作单位 
        </TD>
        <TD  class= input> 
          <Input name=OldCom class= common > 
        </TD>
        <TD  class= title> 
          原职业 
        </TD>
        <TD  class= input> 
          <Input name=OldOccupation class='code' verify="原职业|code:occupation" ondblclick="return showCodeList('occupation',[this]);" onkeyup="return showCodeListKey('occupation',[this]);"> 
        </TD>               
      <TR  class= common>
        <TD  class= title>
          原工作职务 
        </TD>
        <TD  class= input> 
          <Input name=HeadShip class= common  > 
        </TD>
         <TD  class= title>
          入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' verify="入司时间|notnull&Date" > 
        </TD>
        <TD  class= title>
          备注
        </TD>
        <TD  class=input> 
          <Input name=Remark class= common > 
        </TD>
      </TR>
    </Div>	
    <!--行政信息-->    
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                行政信息
            </td>
            </td>
    	</tr>
     </table>
     <Div id= "divLAAgent3" style= "display: ''">
       <table class=common>
      <tr class=common>
        <TD  class= title>
          岗位名称 
        </TD>
        <TD  class= input> 
          <Input name=AgentKind class='code' verify="岗位名称|notnull" 
          ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);"> 
        </TD>
        <TD class= title>
          职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade class="code" verify="职级|notnull&code:AgentGrade" 
		       ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
		       onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
        </TD>        
	      <TD class= title>
          上级人员
        </TD>
        <TD class= input>
          <Input name=UpAgent class='readonly'readonly > 
        </TD>
      </tr>
      <tr class=common>
        <TD class= title>
          所属机构代码 
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup onchange="return changeGroup();"> 
        </TD>
        <TD class= title>
          所属渠道名称 
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=ChannelName> 
        </TD>
        <TD  class= title>
          操作员代码 
        </TD>
        <TD  class= input> 
          <Input name=Operator class= 'readonly' readonly > 
        </TD>
      </TR>
     </table>
    </Div>
    <input type=hidden name=AgentState value=''>
<!--    <Input type=hidden name=Operator >
--> 
    <Input type=hidden name=InsideFlag > 
    <Input type=hidden name=ManageCom > 
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=hideAgentGroup value=''>
    <input type=hidden name=BranchLevel value=''>
    <!--input type=hidden name=hideManageCom value=''-->
    <input type=hidden name=BranchType value=''>
    <input type=hidden name=BranchManager value=''>
    <input type=hidden name=hideIsManager value='false'>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

        