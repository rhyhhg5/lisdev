<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAFRAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LAQualificationSchema tLAQualificationSchema   = new LAQualificationSchema();

  LAFRAgentUI tLAFRAgentUI = new LAFRAgentUI();
  String AgentCode = "";
  String GroupAgentcode = "";

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  String tIndueformflag = request.getParameter("InDueFormFlag");
  String tIsManager = request.getParameter("hideIsManager");
  System.out.println("this :"+request.getParameter("TutorShip"));
  System.out.println("tOperate :"+tOperate);
  System.out.println("转正标记InDueFormFlag"+tIndueformflag);
  System.out.println(request.getParameter("ManageCom"));
  System.out.println(tIsManager+"*********");
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String cAgentCode = "";
  String cAgentCode1 = "";
  String cAgentCode2 = "";
  
  //add by lyc 统一工号 2014-11-27
  if(!"".equals(request.getParameter("AgentCode")) ){
  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("AgentCode")+"' fetch first row only";
   cAgentCode = new ExeSQL().getOneValue(cSql);
  }  
  

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
    LisIDEA tLisIdea = new LisIDEA();
    tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));//设置销售人员默认密码，方便以后查询分析系统使用
    tLAAgentSchema.setAgentCode(cAgentCode);
    tLAAgentSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
    tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
    tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
    tLAAgentSchema.setName(request.getParameter("Name"));
    tLAAgentSchema.setSex(request.getParameter("Sex"));
    tLAAgentSchema.setBirthday(request.getParameter("Birthday"));
    tLAAgentSchema.setIDNo(request.getParameter("IDNo"));
    tLAAgentSchema.setIDNoType("0");
    
    tLAAgentSchema.setNativePlace(request.getParameter("NativePlace"));
    tLAAgentSchema.setNationality(request.getParameter("Nationality"));
    tLAAgentSchema.setPolityVisage(request.getParameter("PolityVisage"));
    tLAAgentSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLAAgentSchema.setDegree(request.getParameter("Degree"));
    tLAAgentSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
    tLAAgentSchema.setSpeciality(request.getParameter("Speciality"));
    tLAAgentSchema.setPostTitle(request.getParameter("PostTitle"));
    tLAAgentSchema.setHomeAddress(request.getParameter("HomeAddress"));
    tLAAgentSchema.setZipCode(request.getParameter("ZipCode"));
    tLAAgentSchema.setPhone(request.getParameter("Phone"));
    tLAAgentSchema.setBP(request.getParameter("BP"));
    tLAAgentSchema.setMobile(request.getParameter("Mobile"));
    tLAAgentSchema.setEMail(request.getParameter("EMail"));
    tLAAgentSchema.setOldCom(request.getParameter("OldCom"));
    tLAAgentSchema.setOldOccupation(request.getParameter("OldOccupation"));
    tLAAgentSchema.setHeadShip(request.getParameter("HeadShip"));
    tLAAgentSchema.setDevGrade(request.getParameter("DevGrade"));
    tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));
    tLAAgentSchema.setTrainPeriods(request.getParameter("TrainPeriods"));
    //add by zhuxt 20140903
    tLAAgentSchema.setAgentType(request.getParameter("AgentType"));
    tLAAgentSchema.setRetainContNo(request.getParameter("RetainContno"));
    tLAAgentSchema.setGroupAgentCode(request.getParameter("AgentCode"));
    
    //===后加
    tLAAgentSchema.setInDueFormDate(request.getParameter("InDueFormDate"));
    System.out.println("打印出来的indueformdate 是"+request.getParameter("InDueFormDate"));
    

    if(tOperate.equals("UPDATE||MAIN"))
    {
	    String sql = "select crs_check_status from laagent where agentcode = '"+cAgentCode+"'";
	    String crs_check_status = new ExeSQL().getOneValue(sql);
	    if("01".equals(crs_check_status)){
	    	tLAAgentSchema.setCrs_Check_Status("99"); //更新标志
	    }else{
	    	tLAAgentSchema.setCrs_Check_Status("00"); //更新标志
	    }    
	    tLAAgentSchema.setEmployDate(request.getParameter("hideEmployDate"));
    }
    else{// by gzh 20110407  
    	tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
    	tLAAgentSchema.setCrs_Check_Status("00");//新增标志
    }
    tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
    tLAAgentSchema.setRemark(request.getParameter("Remark"));
    tLAAgentSchema.setOperator(request.getParameter("Operator"));
    tLAAgentSchema.setChannelName(request.getParameter("ChannelName"));
    tLAAgentSchema.setInsideFlag(request.getParameter("InsideFlag"));
    tLAAgentSchema.setFullTimeFlag(request.getParameter("GrpFlag"));
    if(!"".equals(request.getParameter("UpAgent")) ){
    	  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("UpAgent")+"' fetch first row only";
    	   cAgentCode1 = new ExeSQL().getOneValue(cSql);
    	  }  
    if(!"".equals( request.getParameter("TutorShip") ) ){
    	  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("TutorShip")+"' fetch first row only";
    	   cAgentCode2 = new ExeSQL().getOneValue(cSql);
    	  }  
    	  	     
    
  
    //取得行政信息--在bl中设置职级及系列
    tLATreeSchema.setAgentCode(cAgentCode);
    tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
    tLATreeSchema.setAgentGroup(request.getParameter("hideAgentGroup"));
    tLATreeSchema.setUpAgent(cAgentCode1);
    tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
    tLATreeSchema.setAgentLine("A");
    tLATreeSchema.setInDueFormFlag(request.getParameter("InDueFormFlag"));
    tLATreeSchema.setInDueFormDate(request.getParameter("InDueFormDate"));
    tLATreeSchema.setInitGrade(request.getParameter("InitGrade"));
    tLATreeSchema.setOldStartDate(request.getParameter("OldStartDate"));
    tLATreeSchema.setOldEndDate(request.getParameter("OldEndDate"));
    tLATreeSchema.setAgentLastSeries(request.getParameter("AgentLastSeries"));
    tLATreeSchema.setAgentLastGrade(request.getParameter("AgentLastGrade"));
    //tLATreeSchema.setAgentSeries(request.getParameter("AgentKind"));
    //tLATreeSchema.setAgentKind(request.getParameter("AgentKind"));
    tLATreeSchema.setBranchType(request.getParameter("BranchType"));
    tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
    tLATreeSchema.setTutorShip(cAgentCode2);
    
    tLAQualificationSchema.setQualifNo(request.getParameter("QualifNo"));
    tLAQualificationSchema.setGrantUnit(request.getParameter("GrantUnit"));
    tLAQualificationSchema.setGrantDate(request.getParameter("GrantDate"));
    tLAQualificationSchema.setValidStart(request.getParameter("ValidStart"));
    tLAQualificationSchema.setValidEnd(request.getParameter("ValidEnd"));
    tLAQualificationSchema.setState(request.getParameter("QualifState"));
    
    //后加 主管人员入司即转正,转正日期为入司日期
    if(tOperate.equals("INSERT||MAIN"))
    {
      if(tIndueformflag.equals("Y"))
       {
    	tLAAgentSchema.setInDueFormDate(request.getParameter("EmployDate"));
    	tLATreeSchema.setInDueFormDate(request.getParameter("EmployDate"));
       }
    }

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tIsManager);
  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
  tVData.addElement(tLAQualificationSchema);

  try
  {
    System.out.println("this will save the data!!!");
    tLAFRAgentUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAFRAgentUI.mErrors;
    if (!tError.needDealError())
    {
    AgentCode = tLAFRAgentUI.getAgentCode();
    String cSql = "select groupagentcode from laagent where agentcode = '"+AgentCode+"' fetch first row only";
    GroupAgentcode = new ExeSQL().getOneValue(cSql);
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
        parent.fraInterface.fm.all('AgentCode').value = '<%=GroupAgentcode%>';
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
