/** 
 * 程序名称：LACertificationInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-20 18:07:04
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
/*
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}

*/

function checkvalid()
{
	var tSql = "";
  
  tSql  = "SELECT";
  tSql += "    A.AgentCode";
  tSql += "  FROM";
  tSql += "    LAAgent A";
  tSql += " WHERE 1=1";
  tSql += getWherePart('A.GroupAgentCode','GroupAgentCode');
  
  var strQueryResult = easyQueryVer3(tSql, 1, 0, 1);
  
  if (!strQueryResult) {
    alert("无此业务员！");
    fm.all('GroupAgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  //查询成功则拆分字符串，返回二维数组
  else{
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentCode').value=tArr[0][0];
  }
}

// 查询按钮
function easyQueryClick() {
		// 初始化表格
  if(!verifyInput())
  {
  	return false;
  }
  var tsql="";
  var tState=fm.all("AgentState").value;
  if(tState=='01')
  {
  	 tsql = " and AgentState<='02'   ";
  	}
  else if (tState=='02')
  {
  	 tsql = " and AgentState<='05'  and AgentState>='03'  ";
  }
  else if (tState=='03')
  {
  	tsql = " and AgentState>='06'   ";
  }
  
  initLACertificationGrid()
   var tReturn = getManageComLimitlike("b.ManageCom");	    
	//此处书写SQL语句			     
 	var strSql = "select getUniteCode(a.AgentCode),a.CertifNo,a.AuthorUnit,a.GrantDate,a.ValidStart,a.ValidEnd,Case a.State When '0' THEN '有效' else '失效' END,a.InvalidDate,"
 	    + "a.InvalidRsn,a.Operator,a.ModifyDate,state,(case a.state when '1' then '已失效' else (select char(days(a.validend) - days(current date)) from dual) end) from LACertification a,laagent b where a.agentcode=b.agentcode and b.branchtype ='1' and b.branchtype2='05' "
    + tReturn
   + getWherePart("a.AgentCode", "AgentCode")
    + getWherePart("a.CertifNo", "CertifNo")
    + getWherePart("a.AuthorUnit", "AuthorUnit")
    + getWherePart("a.GrantDate", "GrantDate")
    + getWherePart("a.InvalidDate", "InvalidDate")
    + getWherePart("ValidStart", "StartValidDate")
    + getWherePart("ValidEnd", "EndValidDate")
    + getWherePart("State", "CertiState")
    + getWherePart("b.ManageCom", "ManageCom")
    + tsql 
    if(fm.all("DistanceDay").value!=null&&fm.all("DistanceDay").value!="")
    {
    	strSql+=" and current date >= a.validend- "+fm.all("DistanceDay").value+" day  and a.validend>=current date and a.state = '0'";
    }
    +"order by b.agentcode"; 
	turnPage.queryModal(strSql, LACertificationGrid);
	if (!turnPage.strQueryResult) {
    alert("没有满足条件的信息！");
    return false;
    }
	
}
function showOne(parm1, parm2) 
	{	
}
function returnParent()
{
        var arrReturn = new Array();
	var tSel = LACertificationGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		top.close();
		//alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LACertificationGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LACertificationGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}







