<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：OutWorkDateSave.jsp
//程序功能：Save.jsp
//创建日期：2017-10-24
//创建人  ：yangjian
%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentquery.*"%>

<%
// 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";

  
  GlobalInput tGI = new GlobalInput(); 
  tGI=(GlobalInput)session.getValue("GI");  
  
  LAAgentSet tLAAgentSet = new LAAgentSet();
  LATreeSet tLATreeSet = new LATreeSet();
  LARecomRelationSet tLARecomRelationSet = new LARecomRelationSet();
  LARearRelationSet tLARearRelationSet = new LARearRelationSet();
  
  
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆"; 
  }
  else //页面有效
  {
	  int lineCount = 0;
	  // 在JSP中获取CheckBox每一行
	  String tChk[] = request.getParameterValues("InpOutWorkDateGridChk");
	  // 在JSP中得到MulLine中的值 
	  String tAgentCode[]= request.getParameterValues("OutWorkDateGrid4");
	  String tOutWorkDate[]= request.getParameterValues("OutWorkDateGrid6");
	  
	  for(int index=0;index<tChk.length;index++)
      {
          if(tChk[index].equals("1")) 
          {
        	  LAAgentSchema tLAAgentSchema = new LAAgentSchema();
        	  tLAAgentSchema.setAgentCode(tAgentCode[index]);
        	  tLAAgentSchema.setOutWorkDate(tOutWorkDate[index]);
        	  tLAAgentSet.add(tLAAgentSchema); 
        	
        	  
          }
	  }
	}

//准备传输数据 VData
  VData tLAAgentVData = new VData();
  tLAAgentVData.addElement(tGI);
  tLAAgentVData.addElement(tLAAgentSet);
  OutWorkDateBL tOutWorkDateBL = new OutWorkDateBL();
  
  tOutWorkDateBL.submitData(tLAAgentVData, "");
 
    	
   //如果在Catch中发现异常，则不从错误类中提取错误信息
   try
   {
     tError = tOutWorkDateBL.mErrors;
     if (!tError.needDealError())
     {                          
       Content = " 保存成功! ";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content = " 失败，原因:" + tError.getFirstError();
       FlagStr = "Fail";
     }
   } 
   catch(Exception ex)
   {
     Content = " 失败，原因:" + ex.toString();
     FlagStr = "Fail";    
   }    
   System.out.println(Content);  
   
  

%>                      
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

