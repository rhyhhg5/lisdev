<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LABComRateCommSetInput.jsp
//程序功能：银代手续费比例录入界面
//创建时间：2008-01-24
//创建人  ：Huxl

%>
<%@page contentType="text/html;charset=GBK" %>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="LAInteractiveReportInput.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="LAInteractiveReportInit.jsp"%> 
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   
</head>

<body  onload="initForm();initElementtype();" >
 <form action="LAInteractiveSave.jsp" method=post name=fm target="fraSubmit">
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
  <table class=common >
  <tr>
   	<TD  class= title>管理机构</TD>
    <TD  class= input>
           <Input class="codeno" name=ManageCom  verify="管理机构|NOTNULL"
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
            ><Input class="codename" name=ManageComName readOnly  elementtype=nacessary> 
      </TD>
     <td  class= title>业务渠道</td>
    <TD  class= input>
           <Input class="codeno" name=Channel  verify="业务渠道|NOTNULL"  CodeData="0|^1|个险直销|^2|个险中介|^3|团险直销|^4|团险中介|^5|银保|^6|电销|^7|所有渠道" 
            ondblclick="return showCodeListEx('Channel',[this,ChannelName],[0,1]);" 
            onkeyup="return showCodeListKeyEx('Channel',[this,ChannelName],[0,1]);"><input class=codename name=ChannelName readonly=true elementtype=nacessary>
          </TD> 
      </tr>
      
    <tr class=common>
         <TD  class= title>统计起期</TD>
     <TD  class= input> <Input class='coolDatePicker' name=StartDate verify="统计起期|NOTNULL" elementtype=nacessary></TD> 
     <TD  class= title>统计止期</TD>
     <TD  class= input> <Input class='coolDatePicker' name=EndDate verify="统计止期|NOTNULL" elementtype=nacessary></TD> 
    </tr>
    
   <tr class=common>	
       <td class = title>业务员编码</td>	   	
    <TD class= input> <Input class= "common" name=AgentCode></TD> 		
          <td  class= title> 业务员姓名 </td>
          <TD  class= input> <Input class= "common" name=Name></TD>  	
  </tr>
    <tr>
    <td  class= common>代理人状态</td>
           <td class = input>
           <Input class="codeno" name=AgentState  verify="代理人状态"  CodeData="0|^1|在职|^2|离职" 
            ondblclick="return showCodeListEx('AgentState',[this,AgentStateName],[0,1]);" 
            onkeyup="return showCodeListKeyEx('AgentState',[this,AgentStateName],[0,1]);"><input class=codename name=AgentStateName readonly=true >
          </TD>	
    </tr>
 
  </table>
  </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="下  载" class=cssButton onclick="DoDownload();">
     		<input type=button value="重  置" class=cssButton onclick="reset();">   
    	</td>
   </tr>      
  </table>
  <table>
  	<tr>
    	<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
			</td>
    	<td class= titleImg>查询结果
    	</td>
		</tr>
  </table>
  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanAgentGrid" ></span>
     </td>
    </tr>    
   </table>      
   <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
   <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
   <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
   <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div> 
    <table>
  	<tr>
   </tr>
    <tr><td><p> <font color="#ff0000">注：查询下载结果中,团队编码和团队名称取得是业务员当前所属团队的团队编码及团队名称</font></p></td></tr>
   <tr><td><p> <font color="#ff0000">注：只统计有互动部标志的人员相关信息</font></p></td></tr>
  </table>
          <input type=hidden class=Common name=querySql > 
          <input type=hidden class=Common name=querySqlTitle > 
          <input type=hidden class=Common name=Title >
          <input type=hidden class=Common name=fmAction >

 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




