<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-07-22 
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LATreeEditInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LATreeEditInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LATreeEditSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
          <td class=titleImg>
            查询条件
          </td>
        </td>
      </tr> 
    </table>
    <Div  id= "divLATree" style= "display: ''">     
      <table class=common>
        <TR class= common>
          <TD class=title>
            代理人编码
          </TD>
          <TD class=input>
            <input class=common name=AgentCode >
          </TD>
          <TD class=title>
            代理人名称
          </TD>
          <TD class=input>
            <input class=common name=Name >
          </TD>
        </TR>
        <TR class=common >
          <TD> &nbsp;&nbsp; </TD>
          <TD> &nbsp;&nbsp; </TD>
          <TD> &nbsp;&nbsp; </TD>
          <TD class=common>
            <input class=common type=button value="确定" onclick="return agentConfirm();">
          </TD>
        </TR>
      </table> 
    </Div>
    <!--原行政信息-->    
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                原行政信息
            </td>
            </td>
    	</tr>
     </table>
     <Div id= "divLATree1" style= "display: ''">
       <table class=common>
        <tr class=common>     
        <TD class= title>
          代理人职级
        </TD>
        <TD class= input>
          <Input name=AgentGrade1 class="readonly" readonly >
        </TD>
        <TD class= title>
          管理机构
        </TD>
        <TD class= input>
          <Input name=ManageCom class='readonly'readonly >
        </TD>        
        </tr>
        <tr class=common>
        <TD class= title>
          销售机构 
        </TD>
        <TD class= input>
          <Input class='readonly'readonly name=BranchAttr> 
        </TD>
        <TD class= title>
          推荐人 
        </TD>
        <TD class= input>
          <Input class='readonly'readonly name=IntroAgency1 > 
        </TD>
        </tr>
        <TR class=common>
        <TD class= title>
          组经理
        </TD>
        <TD class= input>
          <Input name=GroupManagerName class='readonly'readonly > 
        </TD>
        <TD class= title>
          部经理
        </TD>
        <TD class= input>
          <Input name=DepManagerName class='readonly'readonly > 
        </TD>
        </tr>
        <tr class=common>
        <TD class= title>
          组育成人
        </TD>
        <TD class= input>
          <Input name=RearAgent1 class='readonly'readonly> 
        </TD>
        <TD class= title>
          部育成人
        </TD>
        <TD class= input>
          <Input name=RearDepartAgent1 class='readonly'readonly> 
        </TD>
        </tr>
        <TR class=common>        
        <TD class= title>
          督导长育成人 
        </TD>
        <TD class= input>
          <Input class='readonly'readonly name=RearSuperintAgent1 > 
        </TD> 
        <TD class= title>
          区域督导长育成人 
        </TD>
        <TD class= input>
          <Input class='readonly'readonly name=RearAreaSuperintAgent1 > 
        </TD> 
        </TR>
       </table>   
       <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent3);">
            <td class= titleImg>
                修改行政信息
            </td>
            </td>
    	</tr>
     </table>
     <Div id= "divLATree1" style= "display: ''">
       <table class=common>
        <tr class=common>     
        <TD class= title>
          代理人职级
        </TD>
        <TD class= input>
          <input type=hidden name=strAgentGrade value="">
          <!--Input name=AgentGrade class="code"  verify="代理人职级|notnull&code:AgentGrade" 
               ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,fm.strAgentGrade.value,'1');" 
               onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,fm.strAgentGrade.value,'1');" --> 
          <Input name=AgentGrade class="code" verify="代理人职级|notnull&code:AgentGrade"
                                 ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
                                 onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" >
        </TD>      
        <TD class= title>
          推荐人 
        </TD>
        <TD class= input>
          <Input class=common name=IntroAgency onchange="return changeIntroAgency();"> 
        </TD>
        </tr>
        <TR>
        <TD class= title>
          组育成人
        </TD>
        <TD class= input>
          <Input name=RearAgent class=common verify="组育成人|len<=10"> 
        </TD>
        <TD class= title>
          部育成人
        </TD>
        <TD class= input>
          <Input name=RearDepartAgent class=common verify="部育成人|len<=10"> 
        </TD>
        </tr>
        <TR class=common>        
        <TD class= title>
          督导长育成人 
        </TD>
        <TD class= input>
          <Input class=common name=RearSuperintAgent verify="督导长育成人|len<=10"> 
        </TD> 
        <TD class= title>
          区域督导长育成人 
        </TD>
        <TD class= input>
          <Input class=common name=RearAreaSuperintAgent verify="区域督导长育成人|len<=10"> 
        </TD> 
        </TR>
        <TR class=common>
        <TD class= title>
          调整日期 
        </TD>
        <TD class= input>
          <Input class='coolDatePicker' name=AdjustDate DateFormat='short' verify="调整日期|notnull&Date"> 
        </TD>
        <TD
         class=titles>员工待遇级别
         </td>
        <td class=input> <Input class=Code type=text name=BranchCode ondblclick="return showCodeList('EmployeeGrade', [this]);" onkeyup="return showCodeList('EmployeeGrade', [this]);"></td>               
        </tr>
        <TR class=common >
          <TD>  &nbsp;&nbsp;   </TD>
          <TD>  &nbsp;&nbsp;   </TD>
          <TD class=common>
            <br> <input class=common type=button value="保存" onclick="return submitForm();">
          </TD>
          <TD class=common>
            <br> <input class=common type=button value="重置" onclick="return clearData();">
          </TD>
        </TR>        
        <TR class=common >
          <TD class=common> 
            <BR> <input class=common type=button value="销售单位维护" onclick="bGroupClick();" > 
          </TD>
          <TD class=common> 
            <BR> <input class=common type=button value="机构调整" onclick="bChangeGroupClick();" >
          </TD>
          <TD class=common>
             <BR> <input class=common type=button value="人员调动" onclick="bAdjustAgentClick();" >
          </TD>
          <TD class=common>
            <br> <input class=common type=button value="机构主管任命" onclick="bSetManagerClick();">
          </TD>
        </TR>
        <input type=hidden name=BranchType value="">
        <input type=hidden name=OldAgentGrade value="">
       </table>   
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

        