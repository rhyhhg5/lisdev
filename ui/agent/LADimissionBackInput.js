//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var mDepartDate="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;

  if (mOperate==""){
  	mOperate="UPDATE||MAIN";
  }

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.hideOperate.value=mOperate;

  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
}


//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";
var tBranchType = fm.all('BranchType').value;
var tBranchType2 = fm.all('BranchType2').value;
 // showInfo=window.open("./LADimissionAppQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}  


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
		initForm();
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	  
  if ((fm.all('AgentCode').value == '')||(fm.all('AgentCode').value==null))
  {
  	alert("请输入员工编码！");
  	fm.all('AgentCode').focus();
  	return false;
  }	
  return true;
}   
        


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
 initForm();
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert("请确定业务员编码！");
  else if ((fm.all('DepartTimes').value==null)||(fm.all('DepartTimes').value==''))
    alert("请查询出要修改的记录！");
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           


//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("AgentCode").value==null)||(fm.all("AgentCode").value==''))
    alert("请确定业务员编码！");
  else if ((fm.all('DepartTimes').value==null)||(fm.all('DepartTimes').value==''))
    alert("请查询出要删除的记录！");
  else
  {
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证业务员编码的合理性
function checkValid()
{ 

  var strSQL = "";
  var strQueryResult = null;
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var tManageCom =getManageComLimit(); 
  if (getWherePart('AgentCode')!='')
  {
//     modify lyc 统一工号 2014-11-26	
	  var cAgentCode = fm.all('AgentCode').value;
	  var cSql = "select agentcode from laagent where  groupagentcode = '"+cAgentCode+"' fetch first row only";
	  var cAgentCode = easyExecSql(cSql);
     strSQL = "select a.AgentCode,(select name from laagent where agentcode=a.agentcode)"
     +",a.departrsn,(select CodeName from ldcode where  codetype='departrsn' and code=a.departrsn) "
     +", a.applydate,a.noti,a.departdate,a.DepartTimes "
     +" from ladimission a  where 1=1 "
     +" and a.agentcode = '"+cAgentCode+"'"
     +" and a.branchtype='"+tBranchType+"'"
     +" and a.branchtype2='"+tBranchType2+"'"
     +" and exists (select '1' from laagent where agentcode=a.agentcode and agentstate in ('03','04'))"
     +" order by a.departtimes desc  fetch first 1 rows only";
     strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
    // alert(strSQL);
  }
  else
  {
    fm.all('AgentCode').value = '';
    initInpBox();
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员离职信息！");
    fm.all('AgentCode').value  = "";
    initInpBox();
    return false;
  }
  else
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult); 
  	fm.all('AgentName').value = arrDataSet[0][1]; 
  	fm.all('DepartRsn').value = arrDataSet[0][2];	
  	fm.all('DepartRsnName').value = arrDataSet[0][3];
  	fm.all('AppDate').value = arrDataSet[0][4];   
  	fm.all('Noti').value = arrDataSet[0][5];
  	fm.all('OutWorkDate').value = arrDataSet[0][6];   
  	fm.all('DepartTimes').value = arrDataSet[0][7];  
  	
//  	mManageCom= arrDataSet[0][4]; 
//  //	alert(mManageCom.indexOf(tManageCom)); 
//  	if(mManageCom.indexOf(tManageCom)<0)
//  	{
//  	alert("您没有该业务员的操作权限!");
//  	fm.all('AgentCode').value  = "";
//        fm.all('DepartTimes').value = "";
//        initInpBox();
//        return false;
//  	}
  }
  //查询成功则拆分字符串，返回二维数组
//  var tBranchType=fm.all('BranchType').value;
//  var tBranchType2=fm.all('BranchType2').value;
//  strQueryResult = null;
//  strSQL = "select AgentCode,departtimes from LADimission where 1=1 "+ getWherePart('AgentCode')
//	   +getWherePart('BranchType')
//	   +getWherePart('BranchType2')
//	   + " order by AgentCode,departtimes";
//  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
//  if (!strQueryResult)
//    fm.all('DepartTimes').value = '1';
//  else
//  {
//    var arrDataSet = decodeEasyQueryResult(strQueryResult);  
//    fm.all('DepartTimes').value = eval(arrDataSet[arrDataSet.length-1][1])+1;
//  }

}
