//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var mDepartDate="";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit()) return false; 

  if (mOperate==""){
  	mOperate="INSERT||MAIN";
  }

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;    
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
	initForm();
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	  
  if ((fm.all('GroupAgentCode').value == '')||(fm.all('GroupAgentCode').value==null))
  {
  	alert("请输入员工编码！");
  	fm.all('GroupAgentCode').focus();
  	return false;
  }	  
  if ((fm.all('DepartRsn').value == '')||(fm.all('DepartRsn').value==null))
  {
  	alert("请输入离职原因！");
  	fm.all('DepartRsn').focus();
  	return false;
  }  
  if ((fm.all('AppDate').value == '')||(fm.all('AppDate').value==null))
  {
  	alert("请输入申请日期！");
  	
  	return false;
  }
  if(!isDate(document.fm.AppDate.value))
  {
  	alert("请输入一个合法日期！");
  	return false;
  }
 

  var tCurrDate= fm.all('CurrDate').value;
  var tAppDate=fm.all('AppDate').value;
  //申请日期不能大于操作日
  if(tAppDate>tCurrDate)
  {
    alert("申请日期不能大于操作日！");
  	return false;
  }
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证业务员编码的合理性
function checkValid()
{ 
  var strSQL = "";
  var strQueryResult = null;
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var tManageCom =getManageComLimit(); 
  if (getWherePart('GroupAgentCode')!='')
  {
     	
     strSQL = "select AgentCode,name,branchtype,branchtype2,managecom from LAAgent where 1=1 "+getWherePart('GroupAgentCode','GroupAgentCode')             
            +" and (Agentstate in ('01','02') or (AgentState is null))"
            +" and branchtype='"+tBranchType+"'"
            +" and branchtype2 = '"+tBranchType2+"'" ;
     strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  }
  else
  {
    fm.all('GroupAgentCode').value = '';
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  //判断是否查询成功
  if (!strQueryResult) 
  {
    alert("无此业务或次业务已做过离职登记！");
    fm.all('GroupAgentCode').value  = "";
    fm.all('DepartTimes').value = "";
    initInpBox();
    return false;
  }
  else
  {
  	var arrDataSet = decodeEasyQueryResult(strQueryResult); 
  	fm.all('AgentCode').value  =arrDataSet[0][0];
  	fm.all('AgentName').value = arrDataSet[0][1]; 	
  	fm.all('BranchType').value = arrDataSet[0][2];
  	fm.all('BranchType2').value = arrDataSet[0][3];   
  	mManageCom= arrDataSet[0][4];  
  	if(mManageCom.indexOf(tManageCom)<0)
  	{
  	 alert("您没有该业务的操作权限!");
  	 fm.all('GroupAgentCode').value  = "";
     fm.all('DepartTimes').value = "";
     initInpBox();
     return false;
  	}
  }
  //查询成功则拆分字符串，返回二维数组
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  strQueryResult = null;
  strSQL = "select AgentCode,departtimes from LADimission where 1=1 "+ getWherePart('AgentCode')
	   +getWherePart('BranchType')
	   +getWherePart('BranchType2')
	   + " order by AgentCode,departtimes";
  strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult)
  {
  	 fm.all('DepartTimes').value = '1';
  }else
  {
    var arrDataSet = decodeEasyQueryResult(strQueryResult);  
    fm.all('DepartTimes').value = eval(arrDataSet[arrDataSet.length-1][1])+1;
  }
  queryAgentCont(fm.all('AgentCode').value);

}


/**************************************************************
 * 功能描述：查询业务员名下保单信息
 * 参    数：业务员代码  pmAgentCode
 * 返 回 值：boolean
 **************************************************************/
function queryAgentCont(pmAgentCode)
{
	var tSQL = "";
	
	tSQL ="select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode='"+pmAgentCode+"' and grpcontno='00000000000000000000' and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	 
	tSQL+=" union select contno,prtno,signdate,customgetpoldate  from  lccont where  agentcode='"+pmAgentCode+"'  and grpcontno='00000000000000000000'  and  (customgetpoldate is null or getpoldate is null)  and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	
	tSQL +="union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  agentcode='"+pmAgentCode+"' and   (appflag<>'1' or appflag is null ) and stateflag<>'3'  and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;
	 
	tSQL+=" union select grpcontno,prtno,signdate,customgetpoldate  from  lcgrpcont where  agentcode='"+pmAgentCode+"'   and  (customgetpoldate is null or getpoldate is null)  and stateflag<>'3' and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1') or uwflag is null) and not exists (select 'X' from lcrnewstatelog where newcontno = lcgrpcont.grpcontno)" ;		
	
	//执行查询并返回结果
	turnPage2.queryModal(tSQL, ContGrid);
}
