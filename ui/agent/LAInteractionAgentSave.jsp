<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.util.Date"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LAQualificationSchema tLAQualificationSchema   = new LAQualificationSchema();
  LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
  LAActiveActionAgentBL tLAActiveActionAgentBL = new LAActiveActionAgentBL();
  String Agentcode = request.getParameter("AgentCode");
  String GroupAgentCode = request.getParameter("GroupAgentCode");
  LARearRelationSet tLARearRelationSet = new LARearRelationSet();
  
	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();

  String tRearStr = "";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
	String sql1 = "select agentcode from laagent where agentcode = '"+request.getParameter("AgentCode")+"'";//
	String  tAgentCode= new ExeSQL().getOneValue(sql1);
  System.out.println("我是科比"+tAgentCode);


  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
  LisIDEA tLisIdea = new LisIDEA();
  tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));
  tLAAgentSchema.setAgentCode(request.getParameter("AgentCode"));
  tLAAgentSchema.setGroupAgentCode(request.getParameter("GroupAgentCode"));
  tLAAgentSchema.setName(request.getParameter("Name"));
  tLAAgentSchema.setSex(request.getParameter("Sex"));
  tLAAgentSchema.setBirthday(request.getParameter("Birthday"));
  tLAAgentSchema.setNationality(request.getParameter("Nationality"));
  tLAAgentSchema.setDegree(request.getParameter("Degree"));
  tLAAgentSchema.setIDNoType(request.getParameter("IDNoType"));
  tLAAgentSchema.setIDNo(request.getParameter("IDNo"));
  tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
  tLAAgentSchema.setSpeciality(request.getParameter("Speciality"));
  tLAAgentSchema.setHomeAddress(request.getParameter("HomeAddress"));
  tLAAgentSchema.setZipCode(request.getParameter("ZipCode"));
  tLAAgentSchema.setPhone(request.getParameter("Phone"));
  tLAAgentSchema.setMobile(request.getParameter("Mobile"));
  //tLAAgentSchema.setInsideFlag(request.getParameter("InsideFlag"));
  tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
  tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
//by rdw 20140731  修改时，当状态为“00” 未提交集团，则仍为‘00’，若为‘01’，则修改为99
  if("INSERT||MAIN".equals(tOperate)){
	  tLAAgentSchema.setCrs_Check_Status("00");
  }
  if("UPDATE||MAIN".equals(tOperate)){
	  String sql = "select crs_check_status from laagent where agentcode = '"+request.getParameter("AgentCode")+"'";
	  String crs_check_status = new ExeSQL().getOneValue(sql);
	  if("01".equals(crs_check_status)){
	  	tLAAgentSchema.setCrs_Check_Status("99"); //更新标志
	  }else{
	  	tLAAgentSchema.setCrs_Check_Status("00"); //更新标志
	  } 
  }
  
  tLAAgentSchema.setAgentGroup(request.getParameter("AgentGroup"));
  tLAAgentSchema.setBranchCode(request.getParameter("AgentGroup"));// Branchcode 设置为AgentGroup
  tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
  tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
//add by zhuxt 20140905
tLAAgentSchema.setAgentType(request.getParameter("AgentType"));
tLAAgentSchema.setRetainContNo(request.getParameter("RetainContno"));
    

  //资格证信息获取
  //校验是否录入资格证 add by wrx 2015-7-9
  String QualifNo = request.getParameter("QualifNo").trim();
  if(QualifNo!=null&&!QualifNo.equals("")){
	  tLAQualificationSchema.setQualifNo(request.getParameter("QualifNo").trim());
	  tLAQualificationSchema.setGrantUnit(request.getParameter("GrantUnit"));
	  tLAQualificationSchema.setGrantDate(request.getParameter("GrantDate"));
	  tLAQualificationSchema.setValidStart(request.getParameter("ValidStart"));
	  tLAQualificationSchema.setValidEnd(request.getParameter("ValidEnd"));
	  tLAQualificationSchema.setState(request.getParameter("QualifState"));
  }

  //取得行政信息
  	String tsql2 = "select agentcode from laagent where groupagentcode = '"+request.getParameter("newIntroAgency")+"'";//推荐人还用原系统工号
	  String  tnewIntroAgency= new ExeSQL().getOneValue(tsql2);
  	System.out.println("我是乔丹"+tnewIntroAgency);
  tLATreeSchema.setAgentCode(tAgentCode);
  tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
  tLATreeSchema.setAgentGroup(request.getParameter("AgentGroup"));
  tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
  tLATreeSchema.setIntroAgency(tnewIntroAgency); 
  tLATreeSchema.setAgentLine("A");
  tLATreeSchema.setBranchType(request.getParameter("BranchType"));
  tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
  //2014-12-1
  //对于互动渠道增加考核类型
  tLATreeSchema.setAssessType(request.getParameter("AssessType"));
  //增加薪资版本为11，因为后面薪资计算的时候都把此值设置为11
  //2014-12-10  yangyang
  tLATreeSchema.setWageVersion("11");
  //转正标记为N,即没有转正
  String tInDueFormFlag = request.getParameter("InDueFormFlag");
  if("1".equals(tInDueFormFlag))
  {
	  tLAAgentSchema.setInDueFormDate(request.getParameter("EmployDate"));
	  tLATreeSchema.setInDueFormDate(request.getParameter("EmployDate"));
  }
  tLATreeSchema.setInDueFormFlag(tInDueFormFlag);
  //取得担保人信息
  int lineCount = 0;
  String arrCount[] = request.getParameterValues("WarrantorGridNo");
  String tCautionerName[] = request.getParameterValues("WarrantorGrid1");
  String tCautionerSex[] = request.getParameterValues("WarrantorGrid2");
  String tCautionerID[] = request.getParameterValues("WarrantorGrid4");
  String tCautionerCom[] = request.getParameterValues("WarrantorGrid5");
  String tHomeAddress[] = request.getParameterValues("WarrantorGrid6");
  String tZipCode[] = request.getParameterValues("WarrantorGrid8");
  String tPhone[] = request.getParameterValues("WarrantorGrid9");
  String tRelation[] = request.getParameterValues("WarrantorGrid10");
  lineCount = arrCount.length; //行数
  LAWarrantorSchema tLAWarrantorSchema;
  for(int i=0;i<lineCount;i++)
  {
    tLAWarrantorSchema = new LAWarrantorSchema();
    tLAWarrantorSchema.setAgentCode(tAgentCode);
    tLAWarrantorSchema.setCautionerName(tCautionerName[i]);
    tLAWarrantorSchema.setCautionerSex(tCautionerSex[i]);
    tLAWarrantorSchema.setCautionerID(tCautionerID[i]);
    tLAWarrantorSchema.setCautionerCom(tCautionerCom[i]);
    tLAWarrantorSchema.setHomeAddress(tHomeAddress[i]);
    tLAWarrantorSchema.setZipCode(tZipCode[i]);
    tLAWarrantorSchema.setPhone(tPhone[i]);
    tLAWarrantorSchema.setRelation(tRelation[i]);
    tLAWarrantorSet.add(tLAWarrantorSchema);
    System.out.println("for:"+tCautionerName[i]);
  }

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);

  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
  tVData.addElement(tLAWarrantorSet);
  tVData.addElement(tLAQualificationSchema);

System.out.println("tOperate:"+tOperate);
  try
  {
    System.out.println("this will save the data!!!");	
    tLAActiveActionAgentBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAActiveActionAgentBL.mErrors;
    if (!tError.needDealError())
    {
    	if(Agentcode == null || "".equals(Agentcode)){
    		Agentcode = tLAActiveActionAgentBL.getAgentCode();
    	}
    	String tGroupAgentCodeSQL = "select GroupAgentCode from laagent where agentcode = '"+Agentcode+"' ";
    	GroupAgentCode = new ExeSQL().getOneValue(tGroupAgentCodeSQL);
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
        parent.fraInterface.fm.all('AgentCode').value = '<%=Agentcode%>';
		parent.fraInterface.fm.all('GroupAgentCode').value = '<%=GroupAgentCode%>';
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
