<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
function initInpTest(){
	try{
		fm.all('ManageCom').value = "";
		fm.all('ManageComName').value = "";
		fm.all('BranchTypeCode').value = "";
		fm.all('BranchTypeCodeName').value = "";
		fm.all('Name').value = "";
		fm.all('AgentState').value = "";
		fm.all('AgentStateName').value = "";
		fm.all('GroupAgentCode').value = "";
		fm.all('AgentCode').value = "";
	}
	catch(ex) {
    alert("在LAGroupCodeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  } 
}
function initForm() {
  try {
    initInpTest();
    initLAGroupCodeQueryBoxGrid();  
  }
  catch(re) {
    alert("LAGroupCodeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LAGroupCodeQueryBoxGrid;
function initLAGroupCodeQueryBoxGrid(){
	var iArray = new Array();
	try{
	  	iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名
	    iArray[0][1]="30px";         		//列名
	    iArray[0][3]=0;         		//列名
	    
	    iArray[1]=new Array();
		iArray[1][0]="管理机构";         	  //列名
		iArray[1][1]="50px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=0;  
		
	    iArray[2]=new Array();
		iArray[2][0]="业务渠道";         	  //列名
		iArray[2][1]="50px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;   
		  
	    iArray[3]=new Array();
		iArray[3][0]="业务员姓名";         	  //列名
		iArray[3][1]="50px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=0;   
		 
	    iArray[4]=new Array();
		iArray[4][0]="人员状态";         	  //列名
		iArray[4][1]="50px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=0;  
		  
	    iArray[5]=new Array();
		iArray[5][0]="业务员编码";         	  //列名
		iArray[5][1]="50px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;   
		 
	    iArray[6]=new Array();
		iArray[6][0]="集团工号";         	  //列名
		iArray[6][1]="50px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0; 
		   
		
		
		LAGroupCodeQueryBoxGrid = new MulLineEnter("fm", "LAGroupCodeQueryBoxGrid"); 
		//设置Grid属性
		LAGroupCodeQueryBoxGrid.mulLineCount = 1;
		LAGroupCodeQueryBoxGrid.displayTitle = 1;
		// LAGroupCodeQueryBoxGrid.locked = 1;
		// LAGroupCodeQueryBoxGrid.canSel = 1;
		// LAGroupCodeQueryBoxGrid.canChk = 0;
		LAGroupCodeQueryBoxGrid.hiddenSubtraction = 1;
		LAGroupCodeQueryBoxGrid.hiddenPlus = 1;
		// LGWorkTestBoxGrid.selBoxEventFuncName = "onQuerySelected";//点击grid单选框时间
		LAGroupCodeQueryBoxGrid.loadMulLine(iArray);  
	}
	catch(ex){
		alert(ex);
	}
}
</script>
