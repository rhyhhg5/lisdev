
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  LANonGrpCodeUI tLANonGrpCodeUI = new LANonGrpCodeUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  String tManageComJudge=request.getParameter("ManageCom");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LAAgentSet tSetU = new LAAgentSet();	//用于更新
	  // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      tVData.add(tGI);
      //创建数据集
      if(tAction.equals("SELECTPAY")){
      	String tChk[] = request.getParameterValues("InpAgentGridChk"); 
      	String tManageCom[] = request.getParameterValues("AgentGrid3");
      	String tName[] = request.getParameterValues("AgentGrid4");
      	String tSeriNo[] = request.getParameterValues("AgentGrid7");
      	String tBirthDay[] = request.getParameterValues("AgentGrid9");
      	String tAgentState[] = request.getParameterValues("AgentGrid10");
      	String tAgentCode[] = request.getParameterValues("AgentGrid13");
      	for(int i=0;i<tChk.length;i++){
      		if(tChk[i].equals("1")){
          		//创建一个新的Schema
      	  		LAAgentSchema tSch = new LAAgentSchema();
         		tSch.setBranchType("1");
          		tSch.setBranchType2("01");
          		tSch.setAgentCode(tAgentCode[i].trim());
          		tSch.setAgentState(tAgentState[i].trim());
          		tSch.setManageCom(tManageCom[i].trim());
          		tSch.setName(tName[i].trim());
          		tSch.setIDNo(tSeriNo[i].trim());
          		tSch.setBirthday(tBirthDay[i].trim());
      	  		tSetU.add(tSch);
       		}
      	}
      	tVData.add(tSetU);
        System.out.println("Start tLANonGrpCodeUI Submit...SELECTPAY");
        tLANonGrpCodeUI.submitData(tVData,tAction);
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLANonGrpCodeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	FlagStr = "Fail";
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
