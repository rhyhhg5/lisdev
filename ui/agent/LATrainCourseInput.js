var showInfo;

function getManagecom(Managecom,ManagecomName)
{
	    var pManagecom = fm.all('PManageCom').value;
	    if(pManagecom==null||pManagecom=='')
	    {
	       alert("请先录入省公司代码");
	       return false;
	    }
  var strsql ="1 and length(trim(comcode))=8 and comcode like #"+pManagecom+"%#";
  showCodeList('comcode',[Managecom,ManagecomName],[0,1],null,strsql,1,1);
}  

// 页面触发获取营业部基础信息
function getBranchAttr(BranchAttr,BranchAttrName)
{
	var cManagecom = fm.all('CManageCom').value;
	
    if(cManagecom==null||cManagecom=='')
    {
       alert("请先录入中心支公司代码");
       return false;
    }
    var strsql1 ="1 and EndFlag=#N# and length(trim(branchattr))=10 and branchtype = #"+fm.all('BranchType').value+"# and branchtype2 = #"+fm.all('BranchType2').value+"# and managecom like #"+cManagecom+"%#";
    showCodeList('branchattr',[BranchAttr,BranchAttrName],[0,1],null,strsql1,1,1);
    
}

// 保存操作
function submitForm()
{
	
  if(fm.all('ReturnFlag').value=='Y')
  {
     alert("查询返回的信息如想进行调整，烦请通过修改按钮进行操作！");
     return false;
  }
  //前台录入信息校验 
  if( verifyInput() == false ) return false;
  if(!beforeSubmit()) return false;
  fm.all("hideOperate").value="INSERT||MAIN";
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();// 提交
}
// 修改操作
function updateClick()
{
  
  if( verifyInput() == false ) return false;
  if(!beforeSubmit()) return false;
  if(fm.all("CourseNo").value==null||fm.all("CourseNo").value==""){
	  alert('请查询返回后再进行修改操作');
  }
  var i = 0;
  fm.all("hideOperate").value="UPDATE||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();// 提交
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
    var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
  showInfo=window.open("./LATrainCourseQueryHtml.jsp?BranchType="+tBranchType+"&BranchType2="+tBranchType2);
}    
//删除操作
function deleteClick()
{
	  if(fm.all("CourseNo").value==null||fm.all("CourseNo").value==""){
		  alert('请查询返回后再进行删除操作');
	  }
	  var i = 0;
	  fm.all("hideOperate").value="DELETE||MAIN";
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit();// 提交	
	  initForm();
}    

// 查询页面，查询返回操作
function afterQuery(arrQueryResult)
{
	var arr = new Array();
	if( arrQueryResult != null )
	{
		arr = arrQueryResult;
		fm.all('PManageCom').value= arr[0][0];
		fm.all('PManageComName').value= arr[0][1];	                                   
		fm.all('CManageCom').value= arr[0][2];
        fm.all('CManageComName').value= arr[0][3];
        fm.all('BranchAttr').value= arr[0][4]
        fm.all('BranchAttrName').value= arr[0][5];
        fm.all('StartDate').value= arr[0][6];
        fm.all('EndDate').value= arr[0][7];
        fm.all('TrainPlace').value= arr[0][10];  
        fm.all('Course').value= arr[0][8]; 
        fm.all('CourseName').value= arr[0][9];
        fm.all('CourseNo').value= arr[0][11];
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content="+content  ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}
// 简单逻辑校验
function beforeSubmit()
{
	  var startDate=fm.all('StartDate').value;
	  var endDate=fm.all('EndDate').value;
      if(endDate<startDate){
    	  alert('培训结束时间不能小于培训开始时间');
    	  return false;
      }
	return true;
}
//上传
function upLoad(dataname)
{
	
	fm2.all("dataName").value=dataname
//	alert(dataname);
//	alert(fm.all(dataname).value);
	if(fm2.all(dataname).value=="")
	{
	alert("请选择要上载的文件.");
	return false;
    }
    var TCourseNo = fm.all('CourseNo').value;
	if(TCourseNo==null||TCourseNo=="")
	{
	alert("请先查询返回后再进行上传操作");
	return false;
    }
    fm2.all("filePath").value="test";
    fm2.all('tBranchAttrName').value=fm.all('BranchAttrName').value
    fm2.all('TCourseNo').value = fm.all('CourseNo').value;
    if (verifyInput() == false)
    return false;

	var showStr="正在上载数据……";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
//	fm.all(dataname).disabled=true
	fm2.action="./LATrainCourseUpLoad.jsp";
	fm2.encoding ="multipart/form-data";
	fm2.submit();
	fm2.all('CourseList').value=="";
	fm2.all('TraineeList').value=="";
	fm2.all('PictureFile').value=="";
	fm2.all('ConclusionFile').value=="";
	
}

function download(downName)
{
	
	fm2.all('downName').value=downName;
//	alert(dataname);
//	alert(fm.all(downName).value);
    var TCourseNo = fm.all('CourseNo').value;
	if(TCourseNo==""||fm2.all('downName').value==""||TCourseNo==null||fm2.all('downName').value==null)
	{
	alert("请先查询返回后再进行下载操作");
	return false;
    }
//	var showStr="下载完成后可关闭该页面……";
//	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm2.action="./LATrainCourseDownLoad.jsp?TCourseNo="+TCourseNo+"&downName="+downName;
    fm2.submit();
	
}

function afterdownload(content){
	  alert('进来啦:'+content);
     if(content=='1'){
    	 alert('下载成功');
     }
     if(content=='2'){
    	 alert('下载失败，请确认文件是否存在');
     }
     if(content=='3'){
    	 alert('下载失败,未找到相关文件');
     }
     
}

function afterCodeSelect(codeName,Field){
	if(Field.value.length==4&codeName=='comcode'){
		fm.all('CManageCom').value=''
		fm.all('CManageComName').value=''
		fm.all('BranchAttr').value=''
		fm.all('BranchAttrName').value=''
	}
	if(Field.value.length==8&codeName=='comcode'){
		fm.all('BranchAttr').value=''
		fm.all('BranchAttrName').value=''
	}
		
}
