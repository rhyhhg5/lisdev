<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LATrainerWageRateQueryInput.jsp
//程序功能：
//创建日期：2018-6-7
//创建人  ：wangQingMin
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String BranchType = request.getParameter("BranchType");
	String BranchType2 = request.getParameter("BranchType2");
	System.out.println("BranchType:" + BranchType);
	System.out.println("BranchType2:" + BranchType2);
	String msql = " 1 and branchtype='" + BranchType
			+ "' and branchtype2='" + BranchType2 + "'";
%>
<script language="JavaScript">
	var cSql = " 1 and length(trim(comcode)) <= #4# ";
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="LATrainerWageRateQueryInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LATrainerWageRateQueryInit.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="./LATrainerWageRateQuerySave.jsp" method=post name=fm
		target="fraSubmit">
		<table style="display: none">
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divAgent1);"></td>
				<td class=titleImg>组训员工月终薪资确认</td>
			</tr>
		</table>
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divAgent1);"></td>
				<td class=titleImg>组训员工月终薪资查询</td>
			</tr>
		</table>
		<Div id="divAgent1">
			<table class=common>
				<tr class=common>
					<TD class=title>省分公司</TD>
					<TD class=input><Input class="codeno" name=PManageCom
						verify="省分公司|code:comcodeallsign&NOTNULL&len<=4"
						ondblclick="return showCodeList('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1);"
						onkeyup="return showCodeListKey('comcodeallsign',[this,PManageComName],[0,1],null,cSql,1,1);"><Input
						class=codename name=PManageComName readOnly elementtype=nacessary></TD>
					<TD class=title>中心支公司</TD>
					<TD class=input><Input class="codeno" name=CManageCom
						verify="中心支公司|code:comcode&len>7"
						ondblclick="return getManagecom(CManageCom,CManageComName);"
						onkeyup="return getManagecom(CManageCom,CManageComName);"><Input
						class=codename name=CManageComName readOnly elementtype=""></TD>
				</tr>
				<TR class=common>
					<TD class=title>营业部</TD>
					<TD class=input><Input name=BranchAttr id=BranchAttr
						class="codeno" 
						ondblclick="return getBranchAttr(BranchAttr,BranchAttrName);"
						onkeyup="return getBranchAttr(BranchAttr,BranchAttrName);"><Input
						maxlength=12 class="codename" name=BranchAttrName  elementtype=""></TD>
					<TD class=title>薪资年月</TD>
					<TD class=input><Input class=common name=WageNo
						verify="薪资年月|NUM&NOTNULL&len=6" elementtype=nacessary></TD>
				</TR>
			</table>
			<input type=button class=cssbutton value="查   询"
				onclick="LATrainerWageRateQueryQuery();"> <input type=button
				class=cssbutton value="下   载" onclick="AgentWageDownLoad();">
		</Div>
		<Div id="divAgentQuery" style="display:">
			<Table class=common>
				<TR class=common>
					<TD text-align: left colSpan=1><span id="spanAgentQueryGrid"></span>
					</TD>
				</TR>
			</Table>
			<INPUT CLASS=cssbutton VALUE="首页" TYPE=button
				onclick="turnPage.firstPage();"> <INPUT CLASS=cssbutton
				VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button
				onclick="turnPage.nextPage();"> <INPUT CLASS=cssbutton
				VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		</Div>
		<input type=hidden name=BranchType value=''> <input
			type=hidden name=BranchType2 value=''> <input type=hidden
			id="fmAction" name="fmAction"> <input type=hidden
			class=Common name=querySql>
		<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,divLAAgent3);">
				<td class=titleImg>薪资计算标准</td>
			</tr>
		</table>
		<Div id="divLAAgent3" style="display: ''">
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span
						id="spanLAManageQualityScoreGrid"> </span> <span
						style="color: red"> <br>注：各薪资计算方法如下<br>
							绩优达成率=绩优人力*2/(月初人力+月末人力)*100%;<br>
							绩优达成率系数=绩优达成率/10%，绩优达成率系数最高不超过1.3,若超过1.3按1.3计算，最低不低于0.5,若低于0.5按0.5计算;<br>
							举绩率=（考核期内新单期缴大于0的人力数/考核期内平均人力数）*100%;<br> 考核期内平均人力数
							=（月初人力+月末人力）/2 ;
					</span></td>
				</tr>
			</table>
		</Div>
        <span id="spanCode"
			style="display: none; position: absolute;"></span>
	</form>
</body>
</html>