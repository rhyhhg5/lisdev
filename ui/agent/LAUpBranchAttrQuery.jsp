<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<title>团队信息查询</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <script src="./LAUpBranchAttrQuery.js"></script> 

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAUpBranchAttrQueryInit.jsp"%>

</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm target=fraSubmit method=post>
	    <table  class= common align=center>
	        <tr>
          <td class= titleImg>
        请输入要查询的条件
          </td>
        </tr>
      	<TR  class= common>
          <TD  class= title> 管理机构 </TD>
                  <TD  class= input>
            <Input class="codeno" name=ManageCom  
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input 
            class ='codename' name ='ManageComName' >
          </TD> <TD  class= title> 销售团队编码 </TD>
          <TD  class= input> <Input class= common name=BranchAttr > </TD>
       </TR>             
      	<TR  class= common>
      	<TD  class= title> 团队核心编码</TD>
    	  <TD  class= input>  <Input class= common  name=AgentGroup></TD>
          <TD  class= title> 展业类型 </TD>
          <TD  class= input>  <Input class= "codeno"  name="BranchType" ondblclick="return showCodeList('BranchType',[this,BranchTypeName],[0,1]);" onkeyup="return showCodeListKey('BranchType',[this,BranchTypeName],[0,1]);"><Input class="codename"  name="BranchTypeName"></TD>
	 </TR> 
	 <tr>
	 		<TD  class= title> 销售渠道</TD><TD  class= input>
    	<Input class= codeno  name=BranchType2  ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1]);" onkeyup="return showCodeListKey('BranchType2',[this,BranchType2Name],[0,1],null,null,null,1);" ><Input class=codename  name=BranchType2Name></TD>
	 </tr>
   </Table>  
      <INPUT VALUE="查  询" class=cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="返  回" class=cssButton TYPE=button onclick="returnParent();">   
    <Table>
    	<TR>	
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroup1);">
    		</TD>
    		<TD class= titleImg>
    			 查询结果
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divGroup1" style= "display: ''" align = center>
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanGroupGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="getLastPage();"> 
 </Div>					

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>	
</html>
