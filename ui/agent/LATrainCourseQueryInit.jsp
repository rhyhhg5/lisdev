<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2006-08-22
//创建人  ：luomin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
		 var LoginManagecom = fm.all('LoginManagecom').value;
		 if(LoginManagecom.length==8){
			fm.all('PManageCom').value=LoginManagecom.substring(0,4);
		 	showAllCodeName();
		 	fm.all('CManageCom').value=LoginManagecom;
		    var strSQL="select name from ldcom where comcode='"+LoginManagecom+"'";
		    var arrResult = easyExecSql(strSQL);
		 		if(arrResult != null)
		 		{
		 			fm.all('CManageComName').value= arrResult[0][0];
		  		}
		 }
	  
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

var AgentQueryGrid ;
function initSetGrid()
{
  var iArray = new Array();		
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         //列名
      iArray[0][1]="30px";         //列名
      iArray[0][2]=100;         //列名
      iArray[0][3]=0;         //列名

      iArray[1]=new Array();
      iArray[1][0]="省分公司代码";         //列名
      iArray[1][1]="60px";         //宽度
      iArray[1][2]=100;         //最大长度
      iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[2]=new Array();
      iArray[2][0]="省分公司名称";         //列名
      iArray[2][1]="80px";         //宽度
      iArray[2][2]=100;         //最大长度
      iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[3]=new Array();
      iArray[3][0]="中心支公司代码";         //列名
      iArray[3][1]="60px";         //宽度
      iArray[3][2]=100;         //最大长度
      iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[4]=new Array();
      iArray[4][0]="中心支公司名称";         //列名
      iArray[4][1]="60px";         //宽度
      iArray[4][2]=100;         //最大长度
      iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[5]=new Array();
      iArray[5][0]="营业部代码";         //列名
      iArray[5][1]="80px";         //宽度
      iArray[5][2]=100;         //最大长度
      iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[6]=new Array();
      iArray[6][0]="营业部名称";         //列名
      iArray[6][1]="60px";         //宽度
      iArray[6][2]=100;         //最大长度
      iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
      
      iArray[7]=new Array();
      iArray[7][0]="培训开始时间";         //列名
      iArray[7][1]="60px";         //宽度
      iArray[7][2]=100;         //最大长度
      iArray[7][3]=0;

      iArray[8]=new Array();
      iArray[8][0]="培训结束时间";         //列名
      iArray[8][1]="60px";         //宽度
      iArray[8][2]=100;         //最大长度
      iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[9]=new Array();
      iArray[9][0]="培训班";         //列名
      iArray[9][1]="0px";         //宽度
      iArray[9][2]=100;         //最大长度
      iArray[9][3]=0;         //是否允许录入，0--不能，1--允许

      iArray[10]=new Array();
      iArray[10][0]="培训班名称";         //列名
      iArray[10][1]="60px";         //宽度
      iArray[10][2]=100;         //最大长度
      iArray[10][3]=0;         //是否允许录入，0--不能，1--允许

	  iArray[11]=new Array();
      iArray[11][0]="培训地点";         //列名
      iArray[11][1]="60px";         //宽度
      iArray[11][2]=100;         //最大长度
      iArray[11][3]=0;         //是否允许录入，0--不能，1--允许

	  iArray[12]=new Array();
      iArray[12][0]="培训地点";         //列名
      iArray[12][1]="0px";         //宽度
      iArray[12][2]=100;         //最大长度
      iArray[12][3]=0;         //是否允许录入，0--不能，1--允许


    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.mulLineCount = 0;   
    SetGrid.displayTitle = 1;
    SetGrid.hiddenPlus = 1;
    SetGrid.hiddenSubtraction = 1;
    SetGrid.locked=1;
    SetGrid.canSel=1;
    SetGrid.loadMulLine(iArray); 
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    
  }
  catch(re)
  {
    alert("在LaratecommisionSetInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>