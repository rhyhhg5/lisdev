//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LADimissionQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
  var arrReturn = new Array();
	var tSel = DimissionGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
			try
			{					
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = DimissionGrid.getSelNo();	
	if( tRow == 0 || tRow == null || arrDataSet == null )	
	  return arrSelected;	
	arrSelected = new Array();
	var strSQL = "";
	strSQL = strSQL = "select getunitecode(a.AgentCode),b.name,a.DepartTimes,a.DepartRsn,a.DepartDate,a.ApplyDate,a.QualityDestFlag,a.PbcFlag,a.ReturnFlag,a.AnnuityFlag,a.VisitFlag,a.BlackFlag,a.BlackListRsn,case when (a.DepartState='03' or a.DepartState='04' or a.DepartState='05') then '离职未确认' when (a.DepartState='08' or a.DepartState='07' or a.DepartState='06') then '离职已确认' else '' end,a.DestoryFlag,a.Operator,a.ModifyDate from ladimission a,laagent b where a.agentcode=b.agentcode and a.agentcode=getagentcode('"+DimissionGrid.getRowColData(tRow-1,1)+"') and a.departtimes="+DimissionGrid.getRowColData(tRow-1,3); 
	     
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有满足条件的记录！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
}


// 查询按钮
function easyQueryClick()
{
	if(!verifyInput())
  {
  	return false;
  }
	// 初始化表格
	initDimissionGrid();
	var tReturn = getManageComLimitlike("c.managecom");
	// 书写SQL语句
	var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSQL = "";
	strSQL = "select getunitecode(a.agentcode),(select b.name from laagent b where b.agentcode=a.agentcode),a.DepartTimes,a.DepartRsn,a.DepartDate,case when (a.DepartState='03' or a.DepartState='04' or a.DepartState='05') then '离职未确认' when (a.DepartState='08' or a.DepartState='07' or a.DepartState='06') then '离职已确认' else '' end   from LADimission a,laagent c where 1=1 and  a.agentcode=c.agentcode"
	         //+ getWherePart('a.AgentCode','AgentCode')
			+strAgent
	         + tReturn
	         + getWherePart('a.DepartDate','DepartDate')
	         + getWherePart('a.DepartRsn','DepartRsn')
	         + getWherePart('a.ApplyDate','ApplyDate')
	         + getWherePart('a.QualityDestFlag','QualityDestFlag')
	         + getWherePart('a.PbcFlag','PbcFlag')
	         + getWherePart('a.ReturnFlag','ReturnFlag')
	         + getWherePart('a.AnnuityFlag','AnnuityFlag')
	         + getWherePart('a.VisitFlag','AnnuityFlag')
	         + getWherePart('a.BlackFlag','AnnuityFlag')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	         + getWherePart('a.branchattr','ManageCom','like');
	if((fm.all('AgentName').value!='')&&(fm.all('AgentName').value!=null))
	 strSQL=strSQL+" and agentcode in (select agentcode from laagent where name='"+fm.all('AgentName').value+"') ";        	 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有满足条件的记录！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  turnPage.pageDisplayGrid = DimissionGrid;    
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function ListExecl()
{
if(!verifyInput())
  {
  	return false;
  }  
//定义查询的数据
var strSQL = "";
var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
var tManageCom=fm.all('ManageCom').value
strSQL = "select getunitecode(a.agentcode),(select b.name from laagent b where b.agentcode=a.agentcode),a.DepartTimes,a.DepartRsn,a.DepartDate,case when (a.DepartState='03' or a.DepartState='04' or a.DepartState='05') then '离职未确认' when (a.DepartState='08' or a.DepartState='07' or a.DepartState='06') then '离职已确认' else '' end   from LADimission a,laagent c where 1=1 and  a.agentcode=c.agentcode"
	        // + getWherePart('a.AgentCode','AgentCode')
             + strAgent
	         + getWherePart('a.DepartDate','DepartDate')
	         + getWherePart('a.DepartRsn','DepartRsn')
	         + getWherePart('a.ApplyDate','ApplyDate')
	         + getWherePart('a.QualityDestFlag','QualityDestFlag')
	         + getWherePart('a.PbcFlag','PbcFlag')
	         + getWherePart('a.ReturnFlag','ReturnFlag')
	         + getWherePart('a.AnnuityFlag','AnnuityFlag')
	         + getWherePart('a.VisitFlag','AnnuityFlag')
	         + getWherePart('a.BlackFlag','AnnuityFlag')
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	         //+ getWherePart('a.branchattr','ManageCom','like');
	if((fm.all('AgentName').value!='')&&(fm.all('AgentName').value!=null))
	 strSQL=strSQL+" and agentcode in (select agentcode from laagent where name='"+fm.all('AgentName').value+"') ";
     strSQL=strSQL+" and branchattr like '"+tManageCom+"%'";
fm.querySql.value = strSQL;
  
//定义列名
var strSQLTitle = "select '营销员编码','营销员姓名','离职次数','离职原因','离职日期','离职状态'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
  
//定义表名
fm.all("Title").value="select '离职确认 查询下载' from dual where 1=1  ";  
  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}
