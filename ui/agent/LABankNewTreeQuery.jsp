<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABankTreeQuery.jsp
//程序功能：
//创建日期：2004-04-1 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>

	var msql=" 1 and gradecode like #F%# and branchtype=#2# and branchtype2=#01# ";
 	var msql2=" 1 and GradeCode <> #F00# and branchtype=#3# and branchtype2=#01#";
	var msql3="1 and agentstate in (#01#,#02#) and branchtype=#3# and branchtype2=#01# ";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LABankNewTreeQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LABankNewTreeQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>专管员查询 </title>
</head>
<body  onload="initForm();" >
  <form  method=post name=fm target="fraSubmit">
    <table>
    	<tr>
        <td class=common>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
        <td class= titleImg>
            员工查询条件
        </td>
        </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
    <TR  class= common> 
      <TD class= title> 
        业务员代码
      </TD>
      <TD  class= input> 
        <Input name=AgentCode class="codeno" verify="业务员代码|notnull&code:AgentCode" 
		     ondblclick="return showCodeList('AgentCode',[this,AgentCodeName1],[0,1],null,msql3,'1');" 
		     onkeyup="return showCodeListKey('AgentCode',[this,AgentCodeName1],[0,1],null,msql3,'1');" ><input class=codename name=AgentCodeName1 > 
      </TD>               
       <TD class= title>
        职级
      </TD>
      <TD class= input>
        <Input name=AgentGrade class="codeno" verify="行政职级|notnull&code:agentgrade" 
		     ondblclick="return showCodeList('agentgrade',[this,AgentGradeName],[0,1],null,msql2,'1');" 
		     onkeyup="return showCodeListKey('agentgrade',[this,AgentGradeName],[0,1],null,msql2,'1');" ><input class=codename name=AgentGradeName >  
      </TD>
    </TR>
    <!--TR  class= common> 
      <TD class= title>
        行政职级
      </TD>
      <TD class= input>
        <Input name=AgentGrade class="codeno" verify="行政职级|notnull&code:agentgrade" 
		     ondblclick="return showCodeList('agentgrade',[this,AgentGradeName],[0,1],null,msql2,'1');" 
		     onkeyup="return showCodeListKey('agentgrade',[this,AgentGradeName],[0,1],null,msql2,'1');" ><input class=codename name=AgentGradeName >  
      </TD>
    </tr-->
  </table>
		<input type=hidden name=BranchType value=''>
		<input type=hidden name=BranchType2 value=''>
    <INPUT VALUE="查询" TYPE=button class= cssButton onclick="easyQueryClick();"> 
    <INPUT VALUE="返回" TYPE=button class= cssButton onclick="returnParent();"> 	
  </Div>      
         				
  <table>
  	<tr class=common>
			<td class=common>
  			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
  		</td>
  		<td class= titleImg>
  			 员工结果
  		</td>
  	</tr>
  </table>
  <Div  id= "divAgentGrid" style= "display: ''">
    <table  class= common>
    	<tr  class= common>
    		<td text-align: left colSpan=1>
  				<span id="spanAgentGrid" >
  				</span> 
  		 	</td>
  		</tr>
  	</table>
    <INPUT VALUE="首页" TYPE=button class= cssButton onclick="turnPage.firstPage();"> 
    <INPUT VALUE="上一页" TYPE=button class= cssButton onclick="turnPage.previousPage();"> 					
    <INPUT VALUE="下一页" TYPE=button class= cssButton onclick="turnPage.nextPage();"> 
    <INPUT VALUE="尾页" TYPE=button class= cssButton onclick="turnPage.lastPage();"> 						
  </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
