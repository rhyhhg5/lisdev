//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function initEdorType(cObj){
	//程序会自动将#转换为'，所以需要特别注意
	mEdorType = " 1 and codealias=#2# ";
	showCodeList('agentkind',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{
	mEdorType = " 1 and codealias=#2#";
	showCodeListKey('agentkind',[cObj], null, null, mEdorType, "1");
}



//提交，保存按钮对应操作
function submitForm(){
	if (mOperate!='DELETE||MAIN'){
		if (!beforeSubmit())
			return false;
		if (mOperate!="UPDATE||MAIN")
			mOperate="INSERT||MAIN";
	}
  if(!checkBeiFen()) return false;
  if(!checkJiangSu()) return false;
  if(!checkQual()) return false;
  if(!checkPhone()) return false;
  if(!checkMobile()) return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  showInfo.close();
  fm.all('hideIsManager').value = false;
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		initInpBox();
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
	  initForm();
  }
  catch(re)
  {
  	alert("在LAFRAgentInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
		//xjh Add For PICC

	//zff  身份证尾号不能为小x的校验
	if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}

	// 验证邮件地址输入是否正确
	var tEMail = document.fm.EMail.value;
	if(tEMail != "" && tEMail != null)
	{
		var tArrValue = tEMail.split("@");
		if(tArrValue.length != 2)
		{
			alert("邮件地址输入有误！");
			return false;
		}
		var tEMailLen = tEMail.length;
		if(tEMail.indexOf("@") <= 0 || tEMail.indexOf("@") == tEMailLen-1)
		{
			alert("邮件地址输入有误！");
			return false;
		}
	}
	if (mOperate!="UPDATE||MAIN")
	{
		if(document.fm.AgentCode.value != "" && document.fm.AgentCode.value != null)
		{
			alert("查询出来的数据只允许进行修改操作！");
			return false;
		}
	}
	
	//添加操作
	if( verifyInput() == false ) return false;

	var strReturn = checkIdNo( '0',trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value))
	if (strReturn != ''){
		alert(strReturn);
		return false;
	}
	return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


//Click事件，当点击增加图片时触发该函数
function addClick(){
	//下面增加相应的代码
	mOperate="INSERT||MAIN";
	showDiv(operateButton,"false");
	showDiv(inputButton,"true");
	fm.all('AgentCode').value = '';
	fm.all('IDNo').value = '';
	fm.all('AgentGrade').value = '';
	fm.all('AgentGroup').value = '';
	fm.all('ManageCom').value = '';
	fm.all('UpAgent').value = '';
	//fm.all('AgentKind').disabled = false;
	//fm.all('AgentGrade').disabled = false;
	//fm.all('AgentGroup').disabled = false;
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');
  }else
  {
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";

   var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
   // showInfo=window.open("./LAAMAgentQuery.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
    showInfo=window.open("./LAAMAgentQueryHtml.jsp?BranchType=" + tBranchType );
}

//Click事件，当点击“查询”图片时触发该函数
function getAgentGrade(cObj,cCodeName)
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";

   var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   if (tBranchType2 ==null || tBranchType2 =="")
   {
   	 alert("请选择销售渠道");
   	 return false ;
   }
   var msql = "  1 and branchtype=#"+tBranchType+"# and branchtype2=#"+tBranchType2+"#";
    return showCodeList('AgentGrade',[cObj,cCodeName],[0,1],null,msql,1);
}
function getAgentGrade1(cObj,cCodeName)
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";

   var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   if (tBranchType2 ==null || tBranchType2 =="")
   {
   	 alert("请选择销售渠道");
   	 return false ;
   }
   var msql = "  1 and branchtype=#"+tBranchType+"# and branchtype2=#"+tBranchType2+"#";
   return showCodeListKey('AgentGrade',[cObj,cCodeName],[0,1],null,msql,1);
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }else
  {
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function changeGroupItem()
{

	if (trim(document.fm.AgentGroup.value) == "")		
	  return false;


var tSQL = "select c.InsideFlag,c.name from labranchgroup c ";
  tSQL += " where 1=1 ";
  tSQL += " and c.branchattr='"+document.fm.AgentGroup.value+"' ";
  tSQL += " and c.BranchType2='"+fm.all('BranchType2').value+"'";
  tSQL += " and c.branchtype='"+fm.all('BranchType').value+"'";
  var arr1 = easyExecSql(tSQL);

  if( arr1 != null ){
	document.fm.AgentGroupName.value=arr1[0][1];
  }

	return changeGroup();
}

function changeGroup()
{

   var tAgentGrade = trim(fm.all('AgentGrade').value);
   if (tAgentGrade==null ||tAgentGrade==''){
     alert('请先录入职级！');
     fm.all('AgentGroup').value = '';
     return false;
   }
  // var tAgentKind = trim(fm.all('AgentKind').value);
  // if (tAgentKind==null ||tAgentKind==''){
  //   alert('请录入类别！');
  //   fm.all('AgentKind').value = '';
  //   fm.all('AgentGroup').value = '';
  //   return false;
  // }

   var strSQL = "";
   strSQL = "select a.BranchAttr,a.ManageCom,a.BranchManager,a.AgentGroup,a.BranchLevel,a.BranchJobType,"
           +" (select b.BranchManager from labranchGroup b where b.AgentGroup = a.UpBranch and (b.state<>'1' or b.state is null)) upAgent"
           +" from LABranchGroup a "
           +"where 1=1 and a.BranchType = '1' and a.BranchType2='"+fm.all('BranchType2').value+"' "
           +" and a.EndFlag <> 'Y' and (a.state<>'1' or a.state is null)"
           + getWherePart('a.BranchAttr','AgentGroup');
     	
     	 //fm.Remark.value = strSQL;
   var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
   if (!strQueryResult)
   {
   	alert('不存在该销售机构！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('ManageCom').value = '';
   	fm.all('BranchManager').value = '';
   	fm.all('hideAgentGroup').value = '';
   	fm.all('BranchLevel').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   

   fm.all('AgentGroup').value = trim(arr[0][0]);
   fm.all('ManageCom').value = trim(arr[0][1]);
   fm.all('BranchManager').value = trim(arr[0][2]);
   fm.all('hideAgentGroup').value = trim(arr[0][3]);
   fm.all('BranchLevel').value = trim(arr[0][4]);
   fm.all('UpAgent').value = trim(arr[0][6]);

   return true;
}

function afterQuery(arrQueryResult)
{
  	  
  var arrResult = new Array();
  
	if( arrQueryResult != null )
	{
        initInpBox();	
		arrResult = arrQueryResult;
		fm.all('AgentCode').value = arrResult[0][0];
		fm.all('Name').value = arrResult[0][1];
		fm.all('Sex').value = arrResult[0][2];
		fm.all('Birthday').value = arrResult[0][3];
		fm.all('NativePlace').value = arrResult[0][4];
		fm.all('Nationality').value = arrResult[0][5];
		fm.all('RgtAddress').value = arrResult[0][6];
		fm.all('HomeAddress').value = arrResult[0][7];
		fm.all('ZipCode').value = arrResult[0][8];
		fm.all('Phone').value = arrResult[0][9];
		fm.all('BP').value = arrResult[0][10];
		fm.all('Mobile').value = arrResult[0][11];
		fm.all('EMail').value = arrResult[0][12];
		fm.all('IDNo').value = arrResult[0][13];
		fm.all('PolityVisage').value = arrResult[0][14];
		fm.all('Degree').value = arrResult[0][15];
		fm.all('GraduateSchool').value = arrResult[0][16];
		fm.all('Speciality').value = arrResult[0][17];
		fm.all('PostTitle').value = arrResult[0][18];
		fm.all('OldCom').value = arrResult[0][19];
		fm.all('OldOccupation').value = arrResult[0][20];
		fm.all('HeadShip').value = arrResult[0][21];
		fm.all('EmployDate').value = arrResult[0][22];
		fm.all('AgentState').value = arrResult[0][23];

		fm.all('Remark').value = arrResult[0][24];	
		fm.all('AgentKind').value =arrResult[0][25];	
		fm.all('Operator').value = arrResult[0][26];
		fm.all('ManageCom').value = arrResult[0][27];
		fm.all('AgentGrade').value = arrResult[0][29];
				//存入的是branchattr	
		fm.all('AgentGroup').value = arrResult[0][35];
		fm.all('AgentGroupName').value = arrResult[0][40];
		
		fm.all('groupAgentCode').value = arrResult[0][50]; //集团工号
		
       fm.all('BranchType2').value = arrResult[0][42];
       if ( fm.all('BranchType2').value =='04')
		{	    
       	fm.all('BranchType2Name').value = '交叉销售';
       }
       else
       {	fm.all('BranchType2Name').value = '中介';}
		document.fm.AgentKind.readOnly=true ;
		document.fm.AgentGrade.readOnly=true ;
		document.fm.AgentGroup.readOnly=true ;
		// 查询并显示编码所代表的中文意思
		showCodeName();
		
	}
}

function showCodeName()
{ 
	var tSQL = "";
	var tCode = "";
	// 性别
	tCode = trim(document.fm.Sex.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'sex' and code = '"+tCode+"'";
		document.fm.SexName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 民族
	tCode = trim(document.fm.Nationality.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'nationality' and code = '"+tCode+"'";
		document.fm.NationalityName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 户口所在地
	tCode = trim(document.fm.RgtAddress.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'nativeplacebak' and code = '"+tCode+"'";
		document.fm.RgtAddressName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 籍贯
	tCode = trim(document.fm.NativePlace.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'nativeplacebak' and code = '"+tCode+"'";
		document.fm.NativePlaceName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 政治面貌
	tCode = trim(document.fm.PolityVisage.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'polityvisage' and code = '"+tCode+"'";
		document.fm.PolityVisageName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 学历
	tCode = trim(document.fm.Degree.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'degree' and code = '"+tCode+"'";
		document.fm.DegreeName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 职称
	tCode = trim(document.fm.PostTitle.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select CodeName from ldcode where codetype = 'posttitle' and code = '"+tCode+"'";
		document.fm.PostTitleName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	// 原职业
	tCode = trim(document.fm.OldOccupation.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select trim(OccupationName)||'-'||workname from LDOccupation where OccupationCode = '"+tCode+"'";
		document.fm.OldOccupationName.value = getArrValueBySQL(tSQL);
	}
	tSQL = "";
	tCode = "";
	
	 // 资格证信息
	tCode = trim(document.fm.groupAgentCode.value);
	if(tCode != "" || tCode != null)
	{
		tSQL = "select QualifNo,GrantUnit,GrantDate,ValidStart,ValidEnd,"
		+"State,Case State WHEN '0' THEN '有效' else '无效' END "
		+" from laqualification where agentcode =  getagentcode('"+tCode+"')";
		var strQueryResult  = easyQueryVer3(tSQL, 1, 1, 1);
		if (strQueryResult)
		{
					var arr = decodeEasyQueryResult(strQueryResult);
					fm.all('QualifNo').value= trim(arr[0][0]) ;
					fm.all('GrantUnit').value= trim(arr[0][1]) ;
					fm.all('GrantDate').value= trim(arr[0][2]) ;
					fm.all('ValidStart').value= trim(arr[0][3]) ;
					fm.all('ValidEnd').value= trim(arr[0][4]) ;
					fm.all('QualifState').value= trim(arr[0][5]) ;
					fm.all('QualifStateName').value= trim(arr[0][6]) ;
				}
	}
	
	
	// 中介专员职级
	//tCode = trim(document.fm.AgentGrade.value);
	//if(tCode != "" || tCode != null)
	//{
	//	tSQL = "select GradeName from LAAgentGrade where branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"'  and GradeCode = '"+tCode+"'";
	//	document.fm.AgentGradeName.value = getArrValueBySQL(tSQL);
	//}
	//tSQL = "";
	//tCode = "";
	// 中介专员类别
	//tCode = trim(document.fm.AgentKind.value);
	//if(tCode != "" || tCode != null)
	//{
	//	tSQL = "select CodeName from ldcode where codetype = 'agentkind' and code = '"+tCode+"'";
	//	document.fm.AgentKindName.value = getArrValueBySQL(tSQL);
	//}
	//tSQL = "";
	//tCode = "";
	// 机构主管
	//tCode = trim(document.fm.AgentGroup.value);
	//if(tCode != "" || tCode != null)
	//{
	//	tSQL = "select a.agentcode from latree a,labranchgroup c ";
    //tSQL += " where a.agentgroup=c.agentgroup";
    //tSQL += " and c.branchattr='" + tCode + "' and a.agentkind='E'";
    //tSQL += " and a.branchtype='"+fm.all('BranchType').value+"' and a.branchtype2='"+fm.all('BranchType2').value+"'";
    //tSQL += " and a.agentcode not in (select agentcode from LADimission)";
	//	document.fm.UpAgent.value = getArrValueBySQL(tSQL);
	//}
	//tSQL = "";
	//tCode = "";
}

function changeIDNo()
{
   if (getWherePart('IDNo')=='')
     return false;
   var strSQL = "";
   strSQL = "select * from LAAgent where 1=1 and agentstate<'06'"
           + getWherePart('IDNo');
     	 //alert(strSQL);
   var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
   if (strQueryResult)
   {
   	alert('该身份证号已存在并未离职!');
   	fm.all('IDNo').value = '';
   	return false;
   }
   return true;
}


/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	//alert(cCodeName);
	try	{
		if( cCodeName == "AgentGrade" )
		{
			checkvalid();//loadFlag在页面出始化的时候声明
		}
	}
	catch( ex ) {
	}
}


function checkvalid()
{
  //	fm.InsideFlag.value="";
}

/***************************************
 * 根据参数查询结果
 *     参数：pmSQL  查询用的SQL文
 *     返回：查询结果（要求SQL文的结果是唯一值）
 ***************************************/
function getArrValueBySQL(pmSQL)
{
	//alert(pmSQL);
	var strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
	
	if (!strQueryResult)
		return "";
		
	var arr = decodeEasyQueryResult(strQueryResult);
	
	return arr[0][0];
}
/******************************************
 * 根据参数查询结果
 *     参数：针对北分增加相关校验
 *     返回：查询结果（要求SQL文的结果是唯一值）
 ***************************************/
 function checkBeiFen()
 {
   var tmanagecom = fm.all('ManageCom').value.substring(0,4);
   if(tmanagecom=='8611')
   {
     if(!verifyNotNull('资格证书号', fm.all('QualifNo').value)) 
      {
       alert("北分公司中介人员录入时需录入相关资格证号");
       return false;
      }
     if(!verifyNotNull('批准单位', fm.all('GrantUnit').value)) 
      {
       alert("北分公司中介人员录入时需录入批准单位");
       return false;
      }
     if(!verifyNotNull('发放日期', fm.all('GrantDate').value))
     {
       alert("资格证发放日期不能为空");
       return false;
     }
     if(!verifyDate('发放日期', fm.all('GrantDate').value))
     {
       alert("发放日期格式不是有效的日期格式,烦请重新录入");
       return false;
     }
     if(!verifyNotNull('有效起期', fm.all('ValidStart').value))
     {
       alert("资格证有效起期不能为空");
       return false;
     }
     if(!verifyDate('有效起期', fm.all('ValidStart').value))
     {
       alert("有效起期格式不是有效的日期格式,烦请重新录入");
       return false;
     }
     if(!verifyNotNull('有效止期', fm.all('ValidEnd').value))
     {
       alert("资格证有效止期不能为空");
       return false;
     }
     if(!verifyDate('有效止期', fm.all('ValidEnd').value))
     {
       alert("有效止期格式不是有效的日期格式,烦请重新录入");
       return false;
     }
     if(!verifyNotNull('资格证状态', fm.all('QualifState').value))
     {
       alert("资格证状态不能为空");
       return false;
     }
   }
   return true;
 }
 /******************************************
  * 根据参数查询结果
  *     参数：针对江苏增加相关校验
  *     返回：查询结果（要求SQL文的结果是唯一值）
  ***************************************/
  function checkJiangSu()
  {
    var tmanagecom = fm.all('ManageCom').value.substring(0,4);
    if(tmanagecom=='8632')
    {
      if(!verifyNotNull('资格证书号', fm.all('QualifNo').value)) 
       {
        alert("江苏公司中介人员录入时需录入相关资格证号");
        return false;
       }
      if(!verifyNotNull('批准单位', fm.all('GrantUnit').value)) 
       {
        alert("江苏公司中介人员录入时需录入批准单位");
        return false;
       }
      if(!verifyNotNull('发放日期', fm.all('GrantDate').value))
      {
        alert("资格证发放日期不能为空");
        return false;
      }
      if(!verifyDate('发放日期', fm.all('GrantDate').value))
      {
        alert("发放日期格式不是有效的日期格式,烦请重新录入");
        return false;
      }
      if(!verifyNotNull('有效起期', fm.all('ValidStart').value))
      {
        alert("资格证有效起期不能为空");
        return false;
      }
      if(!verifyDate('有效起期', fm.all('ValidStart').value))
      {
        alert("有效起期格式不是有效的日期格式,烦请重新录入");
        return false;
      }
      if(!verifyNotNull('有效止期', fm.all('ValidEnd').value))
      {
        alert("资格证有效止期不能为空");
        return false;
      }
      if(!verifyDate('有效止期', fm.all('ValidEnd').value))
      {
        alert("有效止期格式不是有效的日期格式,烦请重新录入");
        return false;
      }
      if(!verifyNotNull('资格证状态', fm.all('QualifState').value))
      {
        alert("资格证状态不能为空");
        return false;
      }
    }
    return true;
  }
 /******************************************
 * 根据参数查询结果
 *     参数：针对资格证表结构增加相关校验,防止插入报错(应对资格证为非必录项这一情况)
 *     
 ***************************************/
 
 function checkQual()
 {
    if(fm.all('QualifNo').value!=null&&fm.all('QualifNo').value!="")
    {
     if(!verifyNotNull('批准单位', fm.all('GrantUnit').value)) 
      {
       alert("如想录入人员资格证,需录入相关完整信息.涉及内容包括:资格证书号、批准单位、发放日期、有效起期、有效止期、资格证状态");
       return false;
      }
     if(!verifyNotNull('发放日期', fm.all('GrantDate').value))
     {
       alert("如想录入人员资格证,需录入相关完整信息.涉及内容包括:资格证书号、批准单位、发放日期、有效起期、有效止期、资格证状态");
       return false;
     }
     if(!verifyDate('发放日期', fm.all('GrantDate').value))
     {
       alert("发放日期格式不是有效的日期格式,烦请重新录入");
       return false;
     }
     if(!verifyNotNull('有效起期', fm.all('ValidStart').value))
     {
       alert("如想录入人员资格证,需录入相关完整信息.涉及内容包括:资格证书号、批准单位、发放日期、有效起期、有效止期、资格证状态");
       return false;
     }
     if(!verifyDate('有效起期', fm.all('ValidStart').value))
     {
       alert("有效起期格式不是有效的日期格式,烦请重新录入");
       return false;
     }
     if(!verifyNotNull('有效止期', fm.all('ValidEnd').value))
     {
       alert("如想录入人员资格证,需录入相关完整信息.涉及内容包括:资格证书号、批准单位、发放日期、有效起期、有效止期、资格证状态");
       return false;
     }
     if(!verifyDate('有效止期', fm.all('ValidEnd').value))
     {
       alert("有效止期格式不是有效的日期格式,烦请重新录入");
       return false;
     }
     if(!verifyNotNull('资格证状态', fm.all('QualifState').value))
     {
       alert("如想录入人员资格证,需录入相关完整信息.涉及内容包括:资格证书号、批准单位、发放日期、有效起期、有效止期、资格证状态");
       return false;
     }
    }
    return true;
 }
 
 // 校验家庭电话
 function checkPhone()
{
  
  var Phone = fm.all('Phone').value.trim();
  if(Phone!=null&&Phone!=""){
	  var result=CheckFixPhoneNew(Phone);
	  var result2=CheckPhoneNew(Phone);
	  if(result!=""&&result2!=""){
		  alert("联系电话不符合规则，请录入正确的固话或手机号！");
		  fm.all('Phone').value='';
		  return false;
		}
  }
  var sql = "select AgentCode from LAAgent where BranchType = '1' and Phone = '"+Phone+"' and AgentState <'06' ";
  if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!="")
  {
    sql +=" and AgentCode <>getagentcode('"+fm.all('AgentCode').value+"')";
  }
  sql+="fetch first 1 rows only";
  var strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
  if (!strQueryResult)
  {
     return true;
  }else{
   arr=decodeEasyQueryResult(strQueryResult);
   alert("系统已存有家庭电话号为"+Phone+"的在职营销员（工号为："+arr[0][0]+"），烦请重新录入员工家庭电话!");
   fm.all('Phone').value='';
   return false;
  }
  return true;
}
 // 校验手机
function checkMobile()
{
  var Mobile = fm.all('Mobile').value.trim();
  if(Mobile!=null&&Mobile!="")
  {
	  var result=CheckPhoneNew(Mobile);
	  if(result!=""){
		  alert(result);
		  fm.all('Mobile').value='';
		  return false
		}
    var sql = "select AgentCode from LAAgent where BranchType = '1' and Mobile = '"+Mobile+"' and AgentState <'06' " ;
    if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!="")
     {
       sql +=" and AgentCode <>getagentcode('"+fm.all('AgentCode').value+"')";
     } 
    sql+="fetch first 1 rows only";
    var strQueryResult  = easyQueryVer3(sql, 1, 0, 1);
   if (!strQueryResult)
    {
       return true;
    }else{
     arr=decodeEasyQueryResult(strQueryResult);
     alert("系统已存有手机号为"+Mobile+"的在职营销员（工号为："+arr[0][0]+"），烦请重新录入员工手机号!");
     fm.all('Mobile').value='';
     return false;
    }
  }  
  return true;
}