<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agent.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.encrypt.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAAgentSchema tLAAgentSchema   = new LAAgentSchema();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  LAQualificationSchema tLAQualificationSchema   = new LAQualificationSchema();
  LAWarrantorSet tLAWarrantorSet = new LAWarrantorSet();
  LAQualificationSet tLAQualificationSet = new LAQualificationSet();
  LARearRelationSet tLARearRelationSet = new LARearRelationSet();
  LAAgentUI tLAAgent = new LAAgentUI();
  String Agentcode = "";
  String GroupAgentcode = "";
  String cIntroAgency = "";

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tIsManager = request.getParameter("hideIsManager");
  String tOrphanCode = request.getParameter("OrphanCode");
  String tRearStr = "";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";

  System.out.println("begin agent schema...");
  //取得代理人信息(增加加密信息1111)
  LisIDEA tLisIdea = new LisIDEA();
  tLAAgentSchema.setPassword(tLisIdea.encryptString("1111"));

  tLAAgentSchema.setAgentCode(request.getParameter("AgentCode"));
  tLAAgentSchema.setAgentGroup(request.getParameter("hideBranchCode")); //暂存显式代码
  tLAAgentSchema.setBranchCode(request.getParameter("hideBranchCode"));
  tLAAgentSchema.setManageCom(request.getParameter("ManageCom"));
  tLAAgentSchema.setBranchType(request.getParameter("BranchType"));
  tLAAgentSchema.setCrs_Check_Status("00");
  tLAAgentSchema.setBranchType2(request.getParameter("BranchType2"));
  tLAAgentSchema.setName(request.getParameter("Name"));
  System.out.println(request.getParameter("BranchCode"));
  System.out.println(request.getParameter("hideBranchCode"));
  tLAAgentSchema.setSex(request.getParameter("Sex"));
  tLAAgentSchema.setBirthday(request.getParameter("Birthday"));
  tLAAgentSchema.setIDNo(request.getParameter("IDNo"));

  tLAAgentSchema.setIDNoType(request.getParameter("IDNoType"));
  tLAAgentSchema.setNativePlace(request.getParameter("NativePlace"));
  tLAAgentSchema.setNationality(request.getParameter("Nationality"));
  tLAAgentSchema.setPolityVisage(request.getParameter("PolityVisage"));
  tLAAgentSchema.setRgtAddress(request.getParameter("RgtAddress"));
  tLAAgentSchema.setDegree(request.getParameter("Degree"));
  tLAAgentSchema.setGraduateSchool(request.getParameter("GraduateSchool"));
  tLAAgentSchema.setSpeciality(request.getParameter("Speciality"));
  tLAAgentSchema.setPostTitle(request.getParameter("PostTitle"));
  tLAAgentSchema.setHomeAddress(request.getParameter("HomeAddress"));
  tLAAgentSchema.setZipCode(request.getParameter("ZipCode"));
  tLAAgentSchema.setPhone(request.getParameter("Phone"));
  tLAAgentSchema.setBP(request.getParameter("BP"));
  tLAAgentSchema.setMobile(request.getParameter("Mobile"));
  tLAAgentSchema.setEMail(request.getParameter("EMail"));
  tLAAgentSchema.setOldCom(request.getParameter("OldCom"));
  tLAAgentSchema.setOldOccupation(request.getParameter("OldOccupation"));
  tLAAgentSchema.setHeadShip(request.getParameter("HeadShip"));
  tLAAgentSchema.setQuafNo(request.getParameter("QuafNo"));
  tLAAgentSchema.setQuafEndDate(request.getParameter("QuafEndDate"));
  tLAAgentSchema.setDevNo1(request.getParameter("DevNo1"));
  tLAAgentSchema.setDevGrade(request.getParameter("DevGrade"));
  tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));
  tLAAgentSchema.setTrainPeriods(request.getParameter("TrainPeriods"));
  tLAAgentSchema.setEmployDate(request.getParameter("EmployDate"));
  tLAAgentSchema.setAssuMoney(request.getParameter("AssuMoney"));
  tLAAgentSchema.setAgentState(request.getParameter("AgentState"));
  tLAAgentSchema.setQualiPassFlag(request.getParameter("QualiPassFlag"));
  tLAAgentSchema.setBankCode(request.getParameter("BankCode"));
  tLAAgentSchema.setBankAccNo(request.getParameter("BankAccNo"));
  tLAAgentSchema.setRemark(request.getParameter("Remark"));
  tLAAgentSchema.setOperator(request.getParameter("Operator"));
  tLAAgentSchema.setInsideFlag(request.getParameter("InsideFlag"));
  //筹备人员标记及开始日期
  tLAAgentSchema.setNoWorkFlag(request.getParameter("NoWorkFlag"));
  tLAAgentSchema.setTrainDate(request.getParameter("TrainDate"));
  tLAAgentSchema.setPrepareEndDate(request.getParameter("PrepareEndDate"));
  tLAAgentSchema.setPreparaGrade(request.getParameter("PreparaGrade"));
  tLAAgentSchema.setWageVersion(request.getParameter("WageVersion"));
  //modify by zhuxt 20140902
  //个险直销业务员类型默认为代理制
  tLAAgentSchema.setAgentType(request.getParameter("AgentType"));
    //
  String tQualifNoQuery=request.getParameter("QualifNoQuery");
    tLAQualificationSchema.setQualifNo(request.getParameter("QualifNo").trim());
    tLAQualificationSchema.setGrantUnit(request.getParameter("GrantUnit"));
    tLAQualificationSchema.setGrantDate(request.getParameter("GrantDate"));
    tLAQualificationSchema.setInvalidDate(request.getParameter("InvalidDate"));
    tLAQualificationSchema.setInvalidRsn(request.getParameter("InvalidRsn"));
    tLAQualificationSchema.setValidStart(request.getParameter("ValidStart"));
    tLAQualificationSchema.setValidEnd(request.getParameter("ValidEnd"));
    tLAQualificationSchema.setState(request.getParameter("QualifState"));
    tLAQualificationSchema.setPasExamDate(request.getParameter("PasExamDate"));
    tLAQualificationSchema.setExamYear(request.getParameter("ExamYear"));
    tLAQualificationSchema.setExamTimes(request.getParameter("ExamTimes"));
	  //add by lyc 统一工号 2014-11-27
	  if(!"".equals(request.getParameter("IntroAgency"))){
	  String cSql = "select agentcode from laagent where groupagentcode = '"+request.getParameter("IntroAgency")+"' fetch first row only";
	  cIntroAgency = new ExeSQL().getOneValue(cSql);
	  }



  //取得行政信息--在bl中设置职级及系列
  tLATreeSchema.setAgentCode(request.getParameter("AgentCode"));
  tLATreeSchema.setManageCom(request.getParameter("ManageCom"));
  tLATreeSchema.setIntroAgency(cIntroAgency);
  tLATreeSchema.setUpAgent(request.getParameter("UpAgent"));
  tLATreeSchema.setAgentSeries(request.getParameter("AgentSeries"));
  tLATreeSchema.setAgentGrade(request.getParameter("AgentGrade"));
  tLATreeSchema.setAgentLine("A");
  tLATreeSchema.setBranchType(request.getParameter("BranchType"));
  tLATreeSchema.setBranchType2(request.getParameter("BranchType2"));
  tLATreeSchema.setSpeciFlag(request.getParameter("SpeciFlag"));
  //薪资版本
  tLATreeSchema.setWageVersion(request.getParameter("WageVersion"));
  
   //添加 营业组团队编码
   String serialno="";
  String tAgentGroup2 =  "";
  String tAgentGroup2Hidden =  "";
  String tAgentGroup ="";
  String tAgentGroupHidden = request.getParameter("hideBranchCode");
  String tAgentGrade = request.getParameter("AgentGrade");
  String tName ="";
  String tAgentGroupSQL = "select agentgroup from labranchgroup where branchattr ='"+tAgentGroupHidden+"'";
  tAgentGroup = new ExeSQL().getOneValue(tAgentGroupSQL);
  
  //添加营业组团队编码
  if("B01".equals(tAgentGrade))
  {
  	serialno = PubFun1.CreateMaxNo(tAgentGroup,4);
  	tAgentGroup2Hidden = tAgentGroup+serialno;
  	tAgentGroup2  = tAgentGroupHidden+serialno;
  }
  else
  {
  	tAgentGroup2 = request.getParameter("AgentGroup2");
  	if(!"".equals(tAgentGroup2))
  	{
	  	tAgentGroup2Hidden =tAgentGroup2.replaceAll(tAgentGroupHidden,tAgentGroup);
  	}
  }
  tLATreeSchema.setAgentGroup2(tAgentGroup2Hidden);
  if(tLAAgentSchema.getNoWorkFlag()!=null&&tLAAgentSchema.getTrainDate()!=null)
  {
   String tTraindate = AgentPubFun.formatDate(tLAAgentSchema.getTrainDate(),"yyyy-MM-dd");
    if((("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim()))&&tTraindate.compareTo("2012-04-01")>=0&&tTraindate.compareTo("2012-06-30")<=0)||
    		(("B".equals(tLATreeSchema.getAgentGrade().substring(0,1).trim())&&tTraindate.compareTo("2013-01-01")>=0&&tTraindate.compareTo("2013-07-01")<0)))
     {
	  tLAAgentSchema.setWageVersion("2012A");
	  tLATreeSchema.setWageVersion("2012A");
     }
    if(tTraindate.compareTo("2013-04-01")>=0) 
    {
      tLAAgentSchema.setWageVersion("2013A");
   	  tLATreeSchema.setWageVersion("2013A");
    }
  }
  //团队建设
  ExeSQL tExe = new ExeSQL();
  String tSql = "select gbuildflag,gbuildstartdate,gbuildenddate from labranchgroup where branchtype='1' and branchtype2='01' and branchattr='"+tLAAgentSchema.getAgentGroup()+"' ";
  SSRS tSSRS = tExe.execSQL(tSql);
  // update new 2013-12-16
  if(tSSRS!=null&&(tSSRS.GetText(1,1).equals("A")||tSSRS.GetText(1,1).equals("B"))&&!tLAAgentSchema.getWageVersion().equals("2013A")&&tLAAgentSchema.getEmployDate().compareTo("2014-04-01")<0)
  {
	  tLAAgentSchema.setWageVersion("2013B");
   	  tLATreeSchema.setWageVersion("2013B"); 
   	  tLAAgentSchema.setGBuildFlag(tSSRS.GetText(1,1));
   	  tLAAgentSchema.setGBuildStartDate(tLAAgentSchema.getEmployDate());
   	  ExeSQL ttExe = new ExeSQL();
      String ttSql = "select enddate from LASTATSEGMENT where char(yearmonth) = db2inst1.DATE_FORMAT(((date('"+tLAAgentSchema.getEmployDate()+"') + 1 year) - 1 month),'yyyymm') and  stattype = '1'";
      SSRS ttSSRS = ttExe.execSQL(ttSql);
   	  tLAAgentSchema.setGBuildEndDate(ttSSRS.GetText(1,1));     
  }
  //取得担保人信息
  int lineCount = 0;
  String arrCount[] = request.getParameterValues("WarrantorGridNo");
  String tCautionerName[] = request.getParameterValues("WarrantorGrid1");
  String tCautionerSex[] = request.getParameterValues("WarrantorGrid2");
  String tCautionerID[] = request.getParameterValues("WarrantorGrid4");
  String tCautionerCom[] = request.getParameterValues("WarrantorGrid5");
  String tHomeAddress[] = request.getParameterValues("WarrantorGrid6");
  String tZipCode[] = request.getParameterValues("WarrantorGrid8");
  String tPhone[] = request.getParameterValues("WarrantorGrid9");
  String tRelation[] = request.getParameterValues("WarrantorGrid10");
  lineCount = arrCount.length; //行数
  LAWarrantorSchema tLAWarrantorSchema;
  for(int i=0;i<lineCount;i++)
  {
    tLAWarrantorSchema = new LAWarrantorSchema();
    tLAWarrantorSchema.setAgentCode(request.getParameter("AgentCode"));
    tLAWarrantorSchema.setCautionerName(tCautionerName[i]);
    tLAWarrantorSchema.setCautionerSex(tCautionerSex[i]);
    tLAWarrantorSchema.setCautionerID(tCautionerID[i]);
    tLAWarrantorSchema.setCautionerCom(tCautionerCom[i]);
    tLAWarrantorSchema.setHomeAddress(tHomeAddress[i]);
    tLAWarrantorSchema.setZipCode(tZipCode[i]);
    tLAWarrantorSchema.setPhone(tPhone[i]);
    tLAWarrantorSchema.setRelation(tRelation[i]);
    tLAWarrantorSet.add(tLAWarrantorSchema);
    System.out.println("for:"+tCautionerName[i]);
  }
  System.out.println("end 担保人信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";

  tVData.add(tG);
  tVData.add(tIsManager);

  tVData.addElement(tLAAgentSchema);
  tVData.addElement(tLATreeSchema);
  tVData.addElement(tLAWarrantorSet);
  tVData.addElement(tLARearRelationSet);
  tVData.addElement(tLAQualificationSchema);
  tVData.addElement(tOrphanCode);

System.out.println("tOperate:"+tOperate);
  try
  {
    System.out.println("this will save the data!!!");
    tLAAgent.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAAgent.mErrors;
    if (!tError.needDealError())
    {
    Agentcode = tLAAgent.getAgentCode();
    ExeSQL cExe = new ExeSQL();
    String cSql = "select groupagentcode  from laagent where  agentcode = '"+Agentcode+"'";
    SSRS cSSRS = cExe.execSQL(cSql);
    GroupAgentcode = cSSRS.GetText(1,1);
    if(!"".equals(tAgentGroup2Hidden))
    {
	    cSql = "select name from laagent where agentcode in (select agentcode from latree where agentgrade ='B01' and  agentgroup2= '"+tAgentGroup2Hidden+"')";
	    cSSRS = cExe.execSQL(cSql);
	    tName = cSSRS.GetText(1,1);
    }
    else
    {
    	tName ="";
    }
    /**if(("".equals(tAgentGroup2)||null==tAgentGroup2)&&)
    {
	    String cSql1 = "select agentgroup2 from latree where  agentcode = '"+Agentcode+"'";
	    SSRS cSSRS1 = cExe.execSQL(cSql1);
	    tAgentGroup2 = cSSRS1.GetText(1,1);
    }
    */
    System.out.println("lyc-fighting: " + GroupAgentcode  );
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript" type="">
        parent.fraInterface.fm.all('AgentCode').value = '<%=GroupAgentcode%>';
		parent.fraInterface.fm.all('AgentGroup2').value = '<%=tAgentGroup2%>';
		parent.fraInterface.fm.all('Group2ManagerName').value = '<%=tName%>';
	    parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
