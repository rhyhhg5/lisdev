//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦

function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


function EdorType(cObj,dobj)
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;

  mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# ";
  showCodeList('Agentcode',[cObj,dobj], [0,1], null, mEdorType, "1");
      
}

function KeyUp(cObj,dobj)
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;

	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# ";     
	showCodeListKey('Agentcode',[cObj,dobj], [0,1], null, mEdorType, "1");
      }
      
      
function EdorType1(cObj,dobj)
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;

	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# ";
  showCodeList('AgentName',[cObj,dobj], [0,1], null, mEdorType, "1");

}

function KeyUp1(cObj,dobj)
{
	var tManageCom=fm.all('ManageCom').value;	
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
  
	mEdorType = " 1 and branchtype=#"+tBranchType+"#  and branchtype2=#"+tBranchType2+"# and agentstate<#03#  and ManageCom like #"+ tManageCom +"%# ";
  showCodeListKey('AgentName',[cObj,dobj], [0,1], null, mEdorType, "1");
       }


function initEdorType(cObj){
	//程序会自动将#转换为'，所以需要特别注意
	mEdorType = " 1 and codealias=#2# ";
	showCodeList('agentkind',[cObj], null, null, mEdorType, "1");
}

function actionKeyUp(cObj)
{
	mEdorType = " 1 and codealias=#2#";
	showCodeListKey('agentkind',[cObj], null, null, mEdorType, "1");
}

/*************************************************************
/**提交，保存按钮对应操作
**************************************************************/
function submitForm(){
	if (mOperate!='DELETE||MAIN'){
   	
		if (mOperate!="UPDATE||MAIN")
			mOperate="INSERT||MAIN";
			
		if (!beforeSubmit())
			return false;	
		
	       }
	if (mOperate=='INSERT||MAIN'){
		if (fm.all('AgentCode').value != ""){
		    if (confirm("您确实想增加该记录吗?"))
    		{
			      
			         }
			  else{
				      alert("您取消了操作");
	            if(fm.all('AgentCode').value!=null && fm.all('AgentGrade').value!="")
	            {
		            fm.all('AgentGrade').disabled = true;
		            fm.all('AgentGroup').disabled = true;
		            fm.all('TutorShip').disabled = true;
		            fm.all('TutorShipName').disabled = true;

		
	            }
				return false;
			      }
		}
	}
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  fm.submit(); //提交
  
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
		
		fm.reset();
		initInpBox();
  showInfo.close();
  fm.all('hideIsManager').value = false;
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

  }
  mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAFRAgentInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{

}

//提交前的校验、计算
function beforeSubmit(){
	//添加操作
	//zff  身份证尾号不能为小x的校验
	if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}
//	if(!verifyInput2()) return false;
	if( verifyInput() == false ) return false;
	//新增校验联系电话
	var Phone = fm.all('Phone').value.trim();
	var result=CheckFixPhoneNew(Phone);
	var result2=CheckPhoneNew(Phone);
    if(result!=""&&result2!=""){
  	  alert("联系电话不符合规则，请录入正确的固话或手机号！");
	  fm.all('Phone').value='';
	  return false;
	}
    //新增校验手机号
    var Mobile = fm.all('Mobile').value.trim();
    if(Mobile!=null&&Mobile!="")
     { 
	  var result=CheckPhoneNew(Mobile);
      if(result!=""){
	  alert(result);
	  fm.all('Mobile').value='';
	  return false
	  }
    }
	if ( mOperate=='INSERT||MAIN')
	{
  //校验身份证号是否存在		
	if(!changeIDNo() )  return false;
	  }
        
   
	var tAgentGrade =  fm.all('AgentGrade').value;
	if(tAgentGrade.substr(0,1)== 'D')
	{				
			fm.all('InDueFormFlag').value = 'N';
		
				// fm.all('InDueFormName').value = '未转正';
	}else
	{
			fm.all('InDueFormFlag').value = 'Y'; 
				
			fm.all('InDueFormDate').value = fm.all('EmployDate').value;
				// fm.all('InDueFormName').value = '已转正';
				
	}

	
	var strReturn = checkIdNo('0',trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
	if (strReturn != ''){
		alert(strReturn);
		return false;
	}
	if(fm.all("TutorShipName").value!=null && fm.all("TutorShipName").value!='')
	{
		
	 if(fm.all("TutorShip").value==null || fm.all("TutorShip").value=='')
	 {
	  alert("辅导人姓名存在，必须录入辅导人代码,请查询并双击输入框选择!");
	  return false;
	  }
	}
	var tOutWorkDate=fm.all('OutWorkDate').value ;
	var tEmployDate=fm.all('EmployDate').value ;
		
	if(tOutWorkDate>=tEmployDate)
	{
	  alert("此人员的离职日期"+tOutWorkDate+",入司时间必须在离职日期之后!");
	 
	  return false;
	}
	//新添加 如果离职和入司相差一个月 就判断上个月是否计算完薪资了
	var tSQL="select 1 from dual where DATE_FORMAT(date('"+fm.all('OutWorkDate').value+"'),'yyyymm')=DATE_FORMAT(date('"+fm.all('EmployDate').value+"')- 1 month,'yyyymm')  with ur";
	var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
    if (strQueryResult)
    {
       var tsql="select '1' from lawage where agentcode='"+fm.all('AgentCode').value+"' and indexcalno=DATE_FORMAT(date('"+fm.all('OutWorkDate').value+"'),'yyyymm') ";
       var sResult=easyQueryVer3(tsql, 1, 1, 1);
       if(!sResult){
       alert("上次离职日期所在月还没有进行薪资结算，不能进行当月的二次入司，否则会引起薪资计算有误！");
       fm.all('EmployDate').value='';
       return false;
       }
    }
	//////////////
	if(fm.all("AgentGrade").value=="" )
	{
	  alert("请选择业务经理的职级");
	 
	  return false;
	}
	if(fm.all("AgentGrade").value=="" )
	{
	  alert("请选择业务经理的职级");
	 
	  return false;
	}
	if(fm.all("AgentGroup").value=="" )
	{
	  alert("请输入所属团队代码");
	 
	  return false;
	}
	     var tAgentCode=fm.all("AgentCode").value;
        var tBranchAttr=fm.all("AgentGroup").value;
        var tBranchType=fm.all("BranchType").value;
        var tBranchType2=fm.all("BranchType2").value;
        var tsql = "select managecom from labranchgroup where branchattr='"+tBranchAttr+"' and branchtype='"+tBranchType+"'  and branchtype2='"+tBranchType2+"'";
   	    
        var strQueryResult  = easyQueryVer3(tsql, 1, 0, 1);  
        if (!strQueryResult)
        {
           alert('此团队不存在！');
           fm.all('AgentGroup').value = '';	
           return false;
        }	
        var arr=decodeEasyQueryResult(strQueryResult);
        var tManageCom1=arr[0][0];
        tsql = "select managecom from laagent  where agentcode='"+tAgentCode+"'";
        strQueryResult  = easyQueryVer3(tsql, 1, 0, 1);  
        	
        if (!strQueryResult)
        {
           alert('此人员不存在！');
           fm.all('AgentCode').value = '';	
           return false;
        }	
        arr=decodeEasyQueryResult(strQueryResult);
        var tManageCom2=arr[0][0];
       // alert(tManageCom2);
       // alert(tManageCom1);
        if(tManageCom2!=tManageCom1)
        {
           alert('团队代码录入有误，人员代码与机构类型不匹配！');
           fm.all('AgentGroup').value = '';	
           return false;	
        }
        fm.GrpFlag.value="2";
		//xjh Add For PICC
//	fm.all('AgentKind').disabled = false;
	fm.all('AgentGrade').disabled = false;
	fm.all('AgentGroup').disabled = false;	
	fm.all('TutorShip').disabled = false;
  fm.all('TutorShipName').disabled = false;
	return true;
		
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug){
	if(cDebug=="1"){
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else{
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


//Click事件，当点击增加图片时触发该函数
function addClick(){
	//下面增加相应的代码
	mOperate="INSERT||MAIN";

	fm.all('AgentCode').value = '';
	fm.all('IDNo').value = '';
	fm.all('AgentGrade').value = '';
	fm.all('AgentGroup').value = '';
	fm.all('ManageCom').value = '';
	fm.all('UpAgent').value = '';
	//fm.all('AgentKind').disabled = false;
	//fm.all('AgentGrade').disabled = false;
	//fm.all('AgentGroup').disabled = false;
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');
  }else
  {
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
     
      submitForm();
      
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";
   var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
    showInfo=window.open("./LAFReAgentQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }else
  {
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
function checkTutorShip()
{
   var strSQL = "";
   strSQL = "select agentcode from laagent where  (AgentState is null or AgentState < '03') " 
          + getWherePart('AgentCode','TutorShip')
          + getWherePart('BranchType','BranchType')
           + getWherePart('BranchType2','BranchType2');
           
   var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  // alert(strSQL);
   if (!strQueryResult)
   {
    alert('没有此代理人');
    fm.all('TutorShip').value = '';	
    return false;
   }	
   return true;	
}

function changeGroup()
{
   if (getWherePart('AgentGroup')=='')
     return false;

   var tAgentGrade = trim(fm.all('AgentGrade').value);
   if (tAgentGrade==null ||tAgentGrade==''){
     alert('请先录入职级！');
     fm.all('AgentGroup').value = '';
     return false;
   }


   var strSQL = "";
   strSQL = "select a.BranchAttr,a.ManageCom,a.BranchManager,a.AgentGroup,a.BranchLevel,a.BranchJobType,"
           +" (select b.BranchManager from labranchGroup b where b.AgentGroup = a.UpBranch and (b.state<>'1' or b.state is null)) upAgent from LABranchGroup a "
           +"where 1=1 and a.BranchType = '"+fm.all('BranchType').value+"' and a.BranchType2='"+fm.all('BranchType2').value+"' and a.EndFlag <> 'Y' and (a.state<>'1' or a.state is null)"
           + getWherePart('a.BranchAttr','AgentGroup');
     	 //alert(strSQL);
     	 //fm.Remark.value = strSQL;
   var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
   if (!strQueryResult)
   {
   	alert('不存在该销售机构！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('UpGroupAgent').value = '';
   	fm.all('ManageCom').value = '';
   	fm.all('BranchManager').value = '';
   	//fm.all('hideManageCom').value = '';
   	fm.all('hideAgentGroup').value = '';
   	fm.all('BranchLevel').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   
//外勤AgentGrade为D,E系列
if(trim(arr[0][4]) == '21')
{
	
	if(tAgentGrade.substr(0,1) != 'D' && tAgentGrade.substr(0,1) != 'E')
	{
   	alert('销售机构级别与职级不对应！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('UpGroupAgent').value = '';
   	fm.all('ManageCom').value = '';
   	fm.all('BranchManager').value = '';
   	//fm.all('hideManageCom').value = '';
   	fm.all('hideAgentGroup').value = '';
   	fm.all('BranchLevel').value = '';
   	return false;
	}
	if(tAgentGrade >= 'E01')
	{

		if(trim(arr[0][2]) != '' && trim(arr[0][2]) != null)
		{
	   	alert('销售机构已经有经理！');
	   	fm.all('AgentGroup').value = '';
	   	fm.all('UpAgent').value = '';
	   	fm.all('UpGroupAgent').value = '';
	   	fm.all('ManageCom').value = '';
	   	fm.all('BranchManager').value = '';
	   	//fm.all('hideManageCom').value = '';
	   	fm.all('hideAgentGroup').value = '';
	   	fm.all('BranchLevel').value = '';
	   	return false;
		}
		else{
			fm.hideIsManager.value = 'true';
		}
	}
	if(tAgentGrade <= 'D10')
	{


	}
}
//内勤AgentGrade为F系列
if(trim(arr[0][4]) == '22')
{
	if(tAgentGrade.substr(0,1) != 'F')
	{
   	alert('销售机构级别与职级不对应！');
   	fm.all('AgentGroup').value = '';
   	fm.all('UpAgent').value = '';
   	fm.all('UpGroupAgent').value = '';
   	fm.all('ManageCom').value = '';
   	fm.all('BranchManager').value = '';
   	//fm.all('hideManageCom').value = '';
   	fm.all('hideAgentGroup').value = '';
   	fm.all('BranchLevel').value = '';
   	return false;
	}
/*		if(tAgentKind == 'E')
		{
				if(trim(arr[0][2]) != '')
				{
			   	alert('销售机构已有经理！');
			   	fm.all('AgentGroup').value = '';
			   	fm.all('UpAgent').value = '';
			   	fm.all('ManageCom').value = '';
			   	fm.all('BranchManager').value = '';
			   	//fm.all('hideManageCom').value = '';
			   	fm.all('hideAgentGroup').value = '';
			   	fm.all('BranchLevel').value = '';
			   	return false;
				}
	   	if(confirm("是否将其作为此机构主管"))
	   	{
	   		fm.hideIsManager.value = 'true';
	   	}
	  	else{
		   	fm.all('AgentGrade').value = '';
		   	fm.all('AgentKind').value = '';
		   	fm.all('AgentGroup').value = '';
		   	fm.all('UpAgent').value = '';
		   	fm.all('ManageCom').value = '';
		   	fm.all('BranchManager').value = '';
		   	//fm.all('hideManageCom').value = '';
		   	fm.all('hideAgentGroup').value = '';
		   	fm.all('BranchLevel').value = '';
		   	return false;
			}
		}
*/		
//		else
//			{
//				if(trim(arr[0][2]) == '')
//				{
//			   	alert('销售机构没有经理，请先录入经理！');
//			   	fm.all('AgentGroup').value = '';
//			   	fm.all('UpAgent').value = '';
//			   	fm.all('ManageCom').value = '';
//			   	fm.all('BranchManager').value = '';
//			   	//fm.all('hideManageCom').value = '';
//			   	fm.all('hideAgentGroup').value = '';
//			   	fm.all('BranchLevel').value = '';
//			   	return false;
//				}
//			}	
}

//   var tAgentKind = trim(fm.all('AgentKind').value);
   fm.all('AgentGroup').value = trim(arr[0][0]);
   
   
     fm.all('UpAgent').value = trim(arr[0][6]);
     var tGAgentCodeSQL = "select groupagentcode from laagent where agentcode = '"+trim(arr[0][6])+"' ";
     var strQueryResult = easyExecSql(tGAgentCodeSQL);
	 if(strQueryResult){
		fm.all('UpGroupAgent').value = strQueryResult[0][0];
	 }
     
     //如果经理
   if (fm.hideIsManager.value == 'true'){
     fm.all('UpAgent').value = "";
     fm.all('UpGroupAgent').value = "";
     }
   else//组员
   {
     fm.all('UpAgent').value = trim(arr[0][2]);
     var tGAgentCodeSQL1 = "select groupagentcode from laagent where agentcode = '"+trim(arr[0][2])+"' ";
     var strQueryResult1 = easyExecSql(tGAgentCodeSQL1);
	 if(strQueryResult1){
		fm.all('UpGroupAgent').value = strQueryResult1[0][0];
	 }
     
    }

   fm.all('ManageCom').value = trim(arr[0][1]);
   fm.all('BranchManager').value = trim(arr[0][2]);
   //fm.all('hideManageCom').value = arr[0][1];
   fm.all('hideAgentGroup').value = trim(arr[0][3]);
   fm.all('BranchLevel').value = trim(arr[0][4]);
   return true;
}

function afterQuery(arrQueryResult){
	var arrResult = new Array(); 

	if( arrQueryResult != null ){
		arrResult = arrQueryResult;
		//alert(arrResult[0][39]); 
		fm.all('GroupAgentCode').value = arrResult[0][0];
		var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+arrResult[0][0]+"' ";
		var strQueryResult = easyExecSql(tYAgentCodeSQL);
		if(!strQueryResult){
			alert("获取业务员编码失败！");
			return false;
		}
		fm.all('AgentCode').value = strQueryResult[0][0];
		fm.all('Name').value = arrResult[0][1];
		fm.all('Sex').value = arrResult[0][2];
	  var Sql_SexName="select codename from ldcode where codeType='sex' and code='"+fm.all('Sex').value+"' ";
    var strQueryResult_SexName  = easyQueryVer3(Sql_SexName, 1, 1, 1);
		if (strQueryResult_SexName)
	  {
		  var arr = decodeEasyQueryResult(strQueryResult_SexName);
		  fm.all('SexName').value= trim(arr[0][0]) ;
		 }
		fm.all('Birthday').value = arrResult[0][3];
		fm.all('NativePlace').value = arrResult[0][4];
			if(fm.all('NativePlace').value!="" && fm.all('NativePlace').value!=null)
		  {
		  var Sql_NationalityName="select codename from ldcode where codeType='nativeplacebak' and code='"+fm.all('NativePlace').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('NativePlaceName').value= trim(arr[0][0]) ;
		  }
		 }
		 
		fm.all('Nationality').value = arrResult[0][5];
		if(fm.all('Nationality').value!="" && fm.all('Nationality').value!=null)
	        {
		  var Sql_NationalityName="select codename from ldcode where codeType='nationality' and code='"+fm.all('Nationality').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
		  {
		    var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
		    fm.all('NationalityName').value= trim(arr[0][0]) ;
		  }
		 }
		 
		fm.all('RgtAddress').value = arrResult[0][6];
		if(fm.all('RgtAddress').value!="" && fm.all('RgtAddress').value!=null)
		{
		  var Sql_NationalityName="select codename from ldcode where codeType='nativeplacebak' and code='"+fm.all('RgtAddress').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
		  {
		     var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
		     fm.all('RgtAddressName').value= trim(arr[0][0]) ;
		  }
		 }
		 
		fm.all('HomeAddress').value = arrResult[0][7];
		fm.all('ZipCode').value = arrResult[0][8];
		fm.all('Phone').value = arrResult[0][9];
		fm.all('BP').value = arrResult[0][10];
		fm.all('Mobile').value = arrResult[0][11];
		fm.all('EMail').value = arrResult[0][12];
		fm.all('IDNo').value = arrResult[0][13];
		fm.all('hideIdNo').value = arrResult[0][13];
		fm.all('PolityVisage').value = arrResult[0][14];
		if(fm.all('PolityVisage').value!="" && fm.all('PolityVisage').value!=null)
		  {
		  var Sql_NationalityName="select codename from ldcode where codeType='polityvisage' and code='"+fm.all('NativePlace').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('PolityVisageName').value= trim(arr[0][0]) ;
		  }
		 }
		fm.all('Degree').value = arrResult[0][15];
			if(fm.all('Degree').value!="" && fm.all('Degree').value!=null)
		  {
		  var Sql_NationalityName="select codename from ldcode where codeType='degree' and code='"+fm.all('Degree').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('DegreeName').value= trim(arr[0][0]) ;
		  }
		 }
		fm.all('GraduateSchool').value = arrResult[0][16];
		fm.all('Speciality').value = arrResult[0][17];
		fm.all('PostTitle').value = arrResult[0][18];
		 if(fm.all('PostTitle').value!="" && fm.all('PostTitle').value!=null)
		  {
		  var Sql_NationalityName="select codename from ldcode where codeType='posttitle' and code='"+fm.all('PostTitle').value+"' ";
     
                  var strQueryResult_NationalityName  = easyQueryVer3(Sql_NationalityName, 1, 1, 1);
		  if (strQueryResult_NationalityName)
			{
				var arr = decodeEasyQueryResult(strQueryResult_NationalityName);
			  fm.all('PostTitleName').value= trim(arr[0][0]) ;
		  }
		 }
		fm.all('OldCom').value = arrResult[0][19];
		fm.all('OldOccupation').value = arrResult[0][20];
		fm.all('HeadShip').value = arrResult[0][21];
		
		 
		
		fm.all('Remark').value = arrResult[0][24];		
		fm.all('Operator').value = arrResult[0][67];
		fm.all('ManageCom').value = arrResult[0][27];
		
		fm.all('InsideFlag').value =arrResult[0][33];
		if(arrResult[0][33]=='2'){
			fm.all('InsideFlag').value ='2';
			fm.all('InsideFlagName').value='虚拟';		
		}	else
	  {
			 fm.all('InsideFlag').value ='1';
			 fm.all('InsideFlagName').value='正常';
		}	
               
		
		
		var grade = trim(arrResult[0][34]);
		if(grade == '0')
		{
			//fm.all('AgentKind').value = 'D';
		}
		else
		{
			//fm.all('AgentKind').value = 'E';
		}		
		//显式机构代码	
		fm.all('OutWorkDate').value = arrResult[0][37];
		 fm.all('GrpFlag').value = arrResult[0][38];
		if(fm.all('GrpFlag').value=='2'){
			//fm.all('GrpFlagName').value='职团开拓';		
		}	else 
		{
			 fm.all('GrpFlag').value='1';
			//fm.all('GrpFlagName').value='常规业务员';
		}	
	//alert(arrResult[0][39]); 
	//fm.all('InDueFormFlag').value = arrResult[0][39];
		if(arrResult[0][39]=='N'){
			fm.all('InDueFormFlag').value ='N';
			fm.all('InDueFormName').value='未转正';		
		}	else
		{
			 fm.all('InDueFormFlag').value ='Y';
			 //alert(arrResult[0][39]); 
			 fm.all('InDueFormName').value='已转正';
		}	
    fm.all('QualifNo').value= arrResult[0][40];
	fm.all('GrantUnit').value= arrResult[0][41];
	fm.all('GrantDate').value= arrResult[0][42];
	fm.all('ValidStart').value= arrResult[0][43];
	fm.all('ValidEnd').value= arrResult[0][44];

	fm.all('QualifState').value= arrResult[0][45];
		//alert(arrResult[0][45]);
	//fm.all('QualifStateName').value= arrResult[0][38];
	if(fm.all('QualifState').value=='0')
	{
		fm.all('QualifStateName').value='失效';
	}
	if(fm.all('QualifState').value=='1')
	{
		 fm.all('QualifStateName').value='有效';
	}	 	   
	}
}

function changeIDNo()
{
	if(fm.IDNo.value==''||fm.IDNo.value==null){
		alert('新增人员身份证号不能为空！');
		return false;
	}else{
	var tIDNo=fm.all('IDNo').value;
	var thideIdNo=fm.all('hideIdNo').value;
	if(tIDNo==thideIdNo){
		return true;
	}
	strSQL = "select agentstate from LAAgent where 1=1 and agentstate<='05' and idno='"+fm.IDNo.value+"' and idnotype='0'";
	//alert(strSQL);	
	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	if (strQueryResult)
	{
		alert('该代理人已在职或为准离职状态!不能再次做增员处理');
		fm.all('IDNo').value = '';
		return false;
	}
	var strSQL2 = "select agentstate from LAAgent where 1=1 and agentstate='07'  and idnotype='0'"
	+ getWherePart('BranchType')
	+ getWherePart('BranchType2')
	+ getWherePart('IDNo');
	//alert(strSQL);	
	var strQueryResult2  = easyQueryVer3(strSQL2, 1, 1, 1);
	if (strQueryResult2)
	{
	  		if(!confirm('此人已两次以上入司，是否允许再次入司？'))
   	   		{
   	   	 		fm.IDNo.value = '';
   	   	 		return false;
   	   		}
	}
		return true;
	}
}

/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	//alert(cCodeName);
	try	{
		if( cCodeName == "AgentGrade" )
		{
			checkvalid();//loadFlag在页面出始化的时候声明
		}
		
	}
 
	catch( ex ) {
	}	
}


 

function checkvalid()
{

}
