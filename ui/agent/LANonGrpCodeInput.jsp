<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  //System.out.println("BranchType:"+BranchType);
  //System.out.println("BranchType2:"+BranchType2);

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LANonGrpCodeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LANonGrpCodeInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>营销员查询 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LANonGrpCodeSave.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                营销员查询条件
            </td>
            </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
      <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:comcode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>  
        <TD  class= title>
          证件类型 
        </TD>
        <TD  class= input>
							<Input name=IDNoType class= "codeno" verify="证件类型|code:idtype"
							ondblclick="return showCodeList('idtype',[this,IDNoTypeName],[0,1],null,null,null,null,'100');"
							onkeyup="return showCodeListKey('idtype',[this,IDNoTypeName],[0,1]);"
							><Input name=IDNoTypeName class="codename" >
		</TD>
        <TD class= title>
          销售机构 
        </TD>
        <TD class= input>
          <Input class=common name=AgentGroup > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= common >
        </TD>        
       <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input name=Sex class="codeno" verify="性别|code:Sex" CodeData="0|男|1|女" ondblclick="return showCodeList('Sex',[this,SexName],[0,1],null,null,null,null,'100');" onkeyup="return showCodeListKey('Sex',[this,SexName],[0,1]);" debug='10'
          ><Input name=SexName class="codename"  >
        </TD> 
        <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class=common > 
        </TD>
      </TR-->
      <TR  class= common> 
         <TD  class= title>
           证件号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= common > 
        </TD>       
        <TD  class= title>
          入司时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class='coolDatePicker' dateFormat='short' > 
        </TD>
      </TR>
      <TR  class= common>
    	</TR>
    </table>
          <input type=hidden name=BranchType value='<%=BranchType%>'>
          <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
          <input type=hidden name=querysql value=''>
          <input type=hidden name=fmAction value=''>
          <input type=hidden class=Common name=Title >
          
          <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();">
          <Input value="下 载" class="cssbutton" TYPE=button onclick="DownClick();">
          <INPUT VALUE="删 除" class="cssbutton" TYPE=button onclick="deleteClick();">
          <INPUT VALUE="重 置" class="cssbutton" TYPE=button onclick="return DoReset();">
           	
    </Div>      
          				
    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 营销员结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" class="cssbutton" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" TYPE=button onclick="turnPage.lastPage();"> 						
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
