//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
var tOrphanCode="";
window.onfocus=myonfocus;

 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,500,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}


// 身份证信息校验
function changeIDNo()
{
    var IDNoType=fm.all("IDNoType").value;
	if (IDNoType==null || IDNoType=='')
	{
		alert("请先录入证件类型！");
		return false ;
	}
//	if(IDNoType!='0')
//	{
//		return true;//如果不是身份证号，则不用校验
//	}
	if (getWherePart('IDNo')=='')
	return false;
	var strSQL1 = "";
	//检验是否为二次入司
	strSQL1 = "select agentstate  from LAAgent where 1=1 "	
	+ getWherePart('IDNoType')
	+ getWherePart('IDNo');
	var strQueryResult1  = easyQueryVer3(strSQL1, 1, 1, 1);
	if (strQueryResult1)
	{
	   var arr= decodeEasyQueryResult(strQueryResult1);
	   if(arr[0][0]=='01'||arr[0][0]=='02'||arr[0][0]=='03'||arr[0][0]=='04'||arr[0][0]=='05')
	    {
		  alert('该代理人已在职或为准离职状态!不能再次做增员处理');
		  fm.all('IDNo').value = '';
		  fm.all('EmployDate').disabled = false;
		    fm.all('BranchCode').disabled = false;
		    fm.all('IntroAgency').disabled = false;
		    fm.all('QualifNo').disabled = false;
		  return false;
	    }
	}		
	return true;
}

//校验入司时间格式问题
function checkdate() 
{ 
   var   date   =   new   Date(); 
   var   curYear=date.getYear(); 
   var   curMonth=date.getMonth()+1; 
   var   curDate=date.getDate(); 
   var   strDate=curYear+ "-"+curMonth+ "-"+curDate;
   var r=/((^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(10|12|0?[13578])([-\/\._])(3[01]|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(11|0?[469])([-\/\._])(30|[12][0-9]|0?[1-9])$)|(^((1[8-9]\d{2})|([2-9]\d{3}))([-\/\._])(0?2)([-\/\._])(2[0-8]|1[0-9]|0?[1-9])$)|(^([2468][048]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([3579][26]00)([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][0][48])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][2468][048])([-\/\._])(0?2)([-\/\._])(29)$)|(^([1][89][13579][26])([-\/\._])(0?2)([-\/\._])(29)$)|(^([2-9][0-9][13579][26])([-\/\._])(0?2)([-\/\._])(29)$))/g;
   var v=fm.all('EmployDate').value;
   if(r.test(v)) 
   { 
       return true; 
   }else 
   { 
       alert("入司录入日期为空或不是有效的日期格式(YYYY-MM-DD),请重新进行录入!"); 
       fm.all('EmployDate').value= strDate;
   } 
}

//校验团队信息
function changeGroup()
{
  var BranchAttr = fm.all('BranchCode').value;
  if(BranchAttr!=null&&BranchAttr!='')
  {
  	//校验录入团队是否有效
  	var strSQL = "";
  	strSQL ="Select BranchAttr,name,ManageCom,AgentGroup From LABranchGroup where 1=1"
  	+" and BranchType = '"+fm.all('BranchType').value+"' and BranchType2 = '"+fm.all('BranchType2').value+"'"
  	+" and EndFlag <>'Y' and (state<>'1' or state is null)"
  	+" and BranchAttr = '"+BranchAttr+"'";
  	var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);                                                                                                                                               
	if (!strQueryResult)                                                                                                                                                                                
	{  
	  alert("录入销售团队代码无效！");
	  fm.all('BranchCode').value ='';
	  fm.all('BranchName').value ='';
	  fm.all('ManageCom').value ='';
	  fm.all('AgentGroup').value ='';	
	  return false;
	}
    var arr = decodeEasyQueryResult(strQueryResult); 
    fm.all('BranchCode').value =arr[0][0];
	fm.all('BranchName').value =arr[0][1];
	fm.all('ManageCom').value  =arr[0][2];
	fm.all('AgentGroup').value =arr[0][3]; 
	
  }
  
  return true;
}

//校验录入资格证号格式问题
function checkInput(str)
{	
	fm.all('QualifNo').value = str.replace(/(^[\s\u3000]*)|([\s\u3000]*$)/g, "");
}

function checkAgent(){
	var tAgentGrade = fm.all('AgentGrade').value;
	var tBranchCode = fm.all('BranchCode').value;
	var tSubAgentCode = trim(tAgentGrade.substr(0,1));
	
	var tManagerSQL = "select branchmanager from labranchgroup where branchtype ='"+fm.all('BranchType').value+"'" +
			" and branchtype2='"+fm.all('BranchType2').value+"' and branchattr = '"+tBranchCode+"' ";
	 var strQueryResult1 = easyQueryVer3(tManagerSQL, 1, 1, 1);	
	 var arr = decodeEasyQueryResult(strQueryResult1); 
		  if(arr[0][0]!=null && arr[0][0]!="" && "V"== tSubAgentCode){
		      alert("团队'"+tBranchCode+"'下存在主管,请选择业务职级！");
		      fm.all('AgentGrade').value="";
		      fm.all('AgentGradeName').value="";
		      return false;
		  }
	  
	  return true;
}


//推荐人信息校验
function changeIntroAgency()
{
  if (getWherePart('newIntroAgency')=='') return false;
  var tagentcode=fm.all('AgentCode').value;
  var tintroagency=fm.all('newIntroAgency').value;
  if (tintroagency==tagentcode)
  {
	 alert('不能与原代理人编码相同!')
	 fm.all('newIntroAgency').value = '';
	 return false;
  }		
  var strSQL1 = "";
  strSQL1 = "select 1 from LAwage  where 1=1 and agentcode='"+tagentcode+"'";
  var strQueryResult1 = easyQueryVer3(strSQL1, 1, 1, 1);	
  if (strQueryResult1)
  {
	 alert('该业务员已经进行过佣金计算,不能修改推荐人!');
	 fm.all('IntroAgency').value = '';
	 return false;
  }
  var strSQL = "";
  strSQL = "select AgentCode,ManageCom, AgentGroup,name,GroupAgentCode from LAAgent  where 1=1 "
		+ " and (AgentState is null or AgentState < '03') "
		+ " and BranchType = '"+fm.all('BranchType').value+"' " 
		+ " and BranchType2 = '"+fm.all('BranchType2').value+"'"
		+ getWherePart('GroupAgentCode','newIntroAgency');
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult)
  {
	 alert('不存在该业务员！');
	 fm.all('IntroAgency').value = '';
	 return false;
  }
		
  var arr = decodeEasyQueryResult(strQueryResult);
	  fm.all('IntroAgencyName').value = arr[0][3];
  fm.all('newIntroAgency').value = arr[0][4];
  return true;
}
	
//提交前的校验、计算
function beforeSubmit()
{
	 //zff  身份证尾号不能为小x的校验
	/*if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}*/
	 
	if( verifyInput() == false ) return false;
	//新增校验联系电话
	var Phone = fm.all('Phone').value.trim();
	if(Phone!=null&&Phone!="")
    { 
	var result=CheckFixPhoneNew(Phone);
    if(result!=""){
	  alert(result);
	  fm.all('Phone').value='';
	  return false
	  }
    }
    //新增校验手机号
	  var Mobile = fm.all('Mobile').value.trim();
	  if(Mobile!=null&&Mobile!="")
	    { 
		  var result=CheckPhoneNew(Mobile);
	      if(result!=""){
		  alert(result);
		  fm.all('Mobile').value='';
		  return false
		  }
	   } 	  

	var strReturn = checkIdNo(trim(fm.all('IDNoType').value),trim(fm.all('IDNo').value),trim(fm.all('Birthday').value),trim(fm.all('Sex').value));
	if (strReturn != '')
		{
			alert('互动专员'+strReturn);
			return false;
		}
	var QualifNo = fm.all('QualifNo').value;
	var GrantUnit = fm.all('GrantUnit').value;
	var GrantDate = fm.all('GrantDate').value;
	var ValidStart = fm.all('ValidStart').value;
	var ValidEnd = fm.all('ValidEnd').value;
	var QualifState = fm.all('QualifState').value;
	if(QualifNo!=""||GrantUnit!=""||GrantDate!=""||ValidStart!=""||ValidStart!=""||QualifState!=""){
		if(QualifNo==null||QualifNo==""){
			alert("请录入资格证书号");
			return false;
		}
		if(GrantUnit==null||GrantUnit==""){
			alert("请录入批准单位");
			return false;
		}
		if(GrantDate==null||GrantDate==""){
			alert("请录入发放日期");
			return false;
		}
		if(ValidStart==null||ValidStart==""){
			alert("请录入有效起期");
			return false;
		}
		if(ValidEnd==null||ValidEnd==""){
			alert("请录入有效止期");
			return false;
		}
		if(QualifState==null||QualifState==""){
			alert("请录入资格证书状态");
			return false;
		}
	}
	//检查担保人信息是否录入	
	var lineCount = 0;
	var tempObj = fm.all('WarrantorGridNo'); //假设在表单fm中
	if (tempObj == null)
	{
		alert("请填写担保人信息！");
		return false;
	}
	WarrantorGrid.delBlankLine("WarrantorGrid");
	lineCount = WarrantorGrid.mulLineCount;
	if (lineCount == 0)
	{
		alert("请填写担保人信息！");
		return false;
	}
	else{
		var sValue;
		var strChkIdNo;
		if(!WarrantorGrid.checkValue2(WarrantorGrid.name,WarrantorGrid)){
			return false;
		}
		var i;       
		var rowNum = WarrantorGrid.mulLineCount;
		var strReturn;

		for(i=0;i<rowNum;i++)
		{
			strReturn=checkIdNo('0',WarrantorGrid.getRowColData(i,4),'',WarrantorGrid.getRowColData(i,2));
			if(strReturn!=''){
				alert('担保人'+strReturn);
				return false;
			}
		}	
	}
	//add by zhuxt 20140903
//	if(fm.all('AgentType').value == '1') 
//	{
//		if(fm.all('RetainContno').value == null || fm.all('RetainContno').value == '') 
//		{
//			alert("销售人员类型为代理制，必须录入委托代理合同号！");
//			fm.RetainContno.focus();
//			return false;
//		}
//	}
	  return true;
}

//入司时间校验：校验入司日期必须大于等于当前日期
function checkEmployDate(){
//	var tCurDate=new Date();
//	var tEmpDate=fm.all('EmployDate').value;
//	if(tEmpDate<(tCurDate.getYear()+"-"+(tCurDate.getMonth()+1)+"-"+tCurDate.getDate()))
//	{
//		alert("入司时间不应早于当前日期！操作失败。");
//		return false;
//	}
//	var arr1=tEmpDate.split("-");
//	var tEmpDate=new Date(arr1[0],arr1[1],arr1[2]);	
//	if(tEmpDate.getYear()<tCurDate.getYear()
//		||(tEmpDate.getYear()==tCurDate.getYear() && tEmpDate.getMonth()<tCurDate.getMonth())
//		||(tEmpDate.getYear()==tCurDate.getYear() && tEmpDate.getMonth()==tCurDate.getMonth() &&　tEmpDate.getDate()<tCurDate.getDate())){
//		alert("入司时间不应早于当前日期！操作失败。");
//		return false;
//	}
	return true;
}

//如果是录入操作，校验不能有工号
function checkInsert()
{
	if(mOperate=='INSERT||MAIN')
	{
		if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!='')
		{
		    alert("请通过修改功能进行人员信息调整操作！");
			return false;
		}
	
	}	
    return true;

}


//提交，保存按钮对应操作
function submitForm()
{
	if( verifyInput() == false ) return false;
	if (mOperate!='DELETE||MAIN')
	{
      	if (!beforeSubmit()) return false;
      	if(!checkInDueFormFlag())return false;

	}
		
	if(mOperate==null || mOperate=="")
	{
		
		fm.all('hideOperate').value = 'INSERT||MAIN';
		mOperate ='INSERT||MAIN';
	}
	
	if(mOperate==null || mOperate==""||mOperate=="INSERT||MAIN")
	{
		if(!checkEmployDate()){ return false;}
		if(!changeIDNo()){ return false;}
		if(!checkAgent()){return false;}
		fm.all('GroupAgentCode').value="";
		fm.all('AgentCode').value="";
	 } 
	if (!confirm('确认您的操作'))
	{
		return false;
	}

	var tDate = new Date();
	if(document.fm.Birthday.value > tDate.getYear()+"-"+tDate.getMonth()+"-"+tDate.getDay())
	{
		alert("[出生日期]小于当前日期，无法录入！");
		return false;
	}
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.hideOperate.value=mOperate;
	fm.submit(); //提交
	
  
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	var tAgentCode=fm.all('AgentCode').value;
	fm.all('AgentCode').value=tAgentCode;
	mOperate="";
	showInfo.close();
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else{

		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	}
	
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	//下面增加相应的代码
	var tBranchType = fm.all('BranchType').value;
	var tBranchType2 = fm.all('BranchType2').value;
    //跳转到查询页面
	showInfo=window.open("./LAInteractionAgentQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);

}

//数据信息返回
function afterQuery(arrQueryResult)
{   
	var arr = new Array();
    if( arrQueryResult != null )
	{  
		arr = arrQueryResult;
		fm.all('GroupAgentCode').value = arr[0][0];
	  	//var tAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+arr[0][0]+"'";
	  	//var strQueryResult = easyExecSql(tAgentCodeSQL);
	  	//if(strQueryResult == null){
	  	//	alert("获取工号失败！");
	  	//	return false;
	  	//}
	  	fm.all('AgentCode').value = arr[0][42];
		fm.all('Name').value = arr[0][1];
		fm.all('Sex').value = arr[0][2];
		fm.all('SexName').value = arr[0][3];
		fm.all('Birthday').value = arr[0][4];
		fm.all('Nationality').value = arr[0][5];
		fm.all('NationalityName').value = arr[0][6];
		fm.all('Degree').value = arr[0][7];
		fm.all('DegreeName').value = arr[0][8];
		fm.all('IDNoType').value = arr[0][9];
		fm.all('IDNoTypeName').value = arr[0][10];
		fm.all('IDNo').value = arr[0][11];
		fm.all('EmployDate').value = arr[0][12];
		fm.all('hiddenEmployDate').value = arr[0][12];
		fm.all('Speciality').value = arr[0][13];
		fm.all('HomeAddress').value = arr[0][14];
		fm.all('ZipCode').value = arr[0][15];
		fm.all('Phone').value = arr[0][16];
		fm.all('Mobile').value = arr[0][17];
		//fm.all('InsideFlag').value = arr[0][18];
		//fm.all('InsideFlagName').value = arr[0][19];
		fm.all('BranchType').value = arr[0][20];
		fm.all('BranchType2').value = arr[0][21];
		
		// 资格证信息
		fm.all('QualifNo').value = arr[0][22];
		fm.all('GrantUnit').value = arr[0][23];
		fm.all('GrantDate').value = arr[0][24];
		fm.all('ValidStart').value = arr[0][25];
		fm.all('ValidEnd').value = arr[0][26];
		fm.all('QualifState').value = arr[0][27];
		fm.all('QualifStateName').value = arr[0][28];
		
		//行政信息
		fm.all('AgentGrade').value = arr[0][29];
		fm.all('hiddenAgentGrade').value = arr[0][29];
		fm.all('AgentGradeName').value = arr[0][30];
		fm.all('BranchCode').value = arr[0][31];
		fm.all('BranchName').value = arr[0][32];
		fm.all('ManageCom').value = arr[0][33];
		fm.all('newIntroAgency').value = arr[0][34];
		fm.all('IntroAgencyName').value = arr[0][35];
		fm.all('AgentGroup').value = arr[0][36];
		fm.all('AgentState').value = arr[0][37];
		//add by zhuxt 20140904
		fm.all('AgentType').value = arr[0][38];
		fm.all('RetainContno').value = arr[0][39];
		if(fm.all('AgentType').value != null && fm.all('AgentType').value != '') 
		{
			var typeSQL = "select codename from ldcode where codetype = 'agenttypecode' and code = '" + fm.all('AgentType').value + "'";
			var typeQueryResult= easyQueryVer3(typeSQL, 1, 1, 1);
			if(typeQueryResult){
				var typeArr = decodeEasyQueryResult(typeQueryResult);
				fm.all('AgentTypeName').value = trim(typeArr[0][0]);
			}
		}
		fm.all('AssessType').value = arr[0][40];
		if(fm.all('AssessType').value==1)
		{
			fm.all('AssessTypeName').value	= "月均互动开拓提奖";
		}
		else if(fm.all('AssessType').value==2)
		{
			fm.all('AssessTypeName').value = "季度规模保费";
		}
		//录入框
		fm.all('EmployDate').disabled = true;
		fm.all('BranchCode').disabled = true;
	//	fm.all('QualifNo').disabled = true;
		if(fm.all('IntroAgency').value !=null&&fm.all('IntroAgency').value !="")
		{
		   fm.all('IntroAgency').disabled = true;
		}
		fm.all('InDueFormFlag').value =arr[0][41];
		fm.all('InDueFormFlagName').value =arr[0][43];
		fm.all('QualifStateName').readOnly = false;
		//担保人信息处理
		easyQuery();

	}

}
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
	{
	   alert('请先查询出要修改的代理人记录！');
	   fm.all('AgentCode').focus();
	}else{
		if ((fm.all("AgentState").value >='03'))
		{
			alert('代理人已离职,不能修改他(她)的信息!');
			return false;
		}
		//zff  身份证尾号不能为小x的校验
	/*if(fm.IDNo.value.substr(fm.IDNo.value.length-1,1)=='x')
  	{
  		alert("身份证最后一位不能用小写x，请重新录入！");
  		return false;
  	}*/
		if (confirm("您确实想修改该记录吗?"))
		{
			mOperate="UPDATE||MAIN";
		    fm.all('EmployDate').disabled = false;
		    fm.all('BranchCode').disabled = false;
		    fm.all('IntroAgency').disabled = false;
		    fm.all('QualifNo').disabled = false;
		    if(!checkAgentGrade()) return false;
		    if(!checkEmployDateUpdate()) return false;
			submitForm();
		}else{
				mOperate="";
				alert("您取消了修改操作！");
			}
		}
}




//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else{
		cDiv.style.display="none";
	}
}



function easyQuery()
{
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select CautionerName,CautionerSex,(select codename from ldcode where codetype='sex' and code=cautionersex),CautionerID,CautionerCom,HomeAddress,Mobile,ZipCode,Phone,Relation,(Select codename From ldcode where codetype = 'relaseries' and code =LAWarrantor.Relation ) from LAWarrantor where 1=1 "
	+ getWherePart('AgentCode');
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

	//判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("担保人信息查询失败！");
		return false;
	}
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = WarrantorGrid;

	//保存SQL语句
	turnPage.strQuerySql     = strSQL;

	//设置查询起始位置
	turnPage.pageIndex       = 0;

	//在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet       = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	var tArr = new Array();
	tArr = arrDataSet;
	//调用MULTILINE对象显示查询结果

	displayMultiline(tArr, turnPage.pageDisplayGrid);

}

//校验职级信息
function checkAgentGrade()
{
	var tAgentGrade = fm.all('AgentGrade').value;
	var tLastAgentGrade = fm.all('hiddenAgentGrade').value;
    if(tAgentGrade!=tLastAgentGrade)
    {
    	
    	alert("人员职级信息不能调整,请知悉！");
    	return false;
    }
	return true;
}

function checkEmployDateUpdate()
{

  var strSQL = "";
  var tEmployDate = fm.all('EmployDate').value;
  var tHiddenEmployDate = fm.all('hiddenEmployDate').value; 
  strSQL = "Select 1 from dual where date('"+tEmployDate+"')=date('"+tHiddenEmployDate+"')";
  var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
  if (!strQueryResult)
  {
	 alert('不能修改入司时间！');
	 fm.all('EmployDate').value=tHiddenEmployDate;
	 return false;
  }

  return true;
}
	
//校验标识试用期录入
function checkInDueFormFlag(){
	var InDueFormFlag=fm.all('InDueFormFlag').value;
	if(InDueFormFlag==null||InDueFormFlag==''){
		alert("标识试用期不能为空！");
	    return false;
	}
	if(InDueFormFlag=='1'&&fm.all('AgentGrade').value=='U01'){
		alert("见习经理必须有标识试用期");
		return false;
}
	return true;
}

function BankClick()
{
	var AgentCode=fm.all('GroupAgentCode').value;
	var tBranchType =fm.all('BranchType').value;
	var tBranchType2= fm.all('BranchType2').value;
	if(!AgentCode)
	{
		alert('互动专员代码不能为空！');
		return false;
	}
	showInfo=window.open("../agentdaily/LActiveAccounts.html?AgentCode="+fm.all('GroupAgentCode').value+"&BranchType=" + tBranchType+"&BranchType2=" + tBranchType2);
}
				







