//               该文件中包含客户端需要处理的函数和事件

var arrDataSet = new Array(); 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function getManagecom(Managecom,ManagecomName)
{
	    var pManagecom = fm.all('PManageCom').value;
	    if(pManagecom==null||pManagecom=='')
	    {
	       alert("请先录入省公司代码");
	       return false;
	    }
  var strsql ="1 and length(trim(comcode))=8 and comcode like #"+pManagecom+"%#";
  showCodeList('comcode',[Managecom,ManagecomName],[0,1],null,strsql,1,1);
}  

// 页面触发获取营业部基础信息
function getBranchAttr(BranchAttr,BranchAttrName)
{
	var cManagecom = fm.all('CManageCom').value;
	
    if(cManagecom==null||cManagecom=='')
    {
       alert("请先录入中心支公司代码");
       return false;
    }
    var strsql1 ="1 and length(trim(branchattr))=10 and branchtype = #"+fm.all('BranchType').value+"# and branchtype2 = #"+fm.all('BranchType2').value+"# and managecom like #"+cManagecom+"%#";
    showCodeList('branchattr',[BranchAttr,BranchAttrName],[0,1],null,strsql1,1,1);
    
}

//修改人：解青青  时间：2014-11-23
function checkAgentCode()
{
//   var sql = "select * from laagent where agentstate<='02' and branchtype='1' and branchtype2='01'";
   var sql=" select agentcode  from laagent   where  1=1" 
             + getWherePart("groupagentcode","GroupAgentCode")
//   		"groupagentcode='"+fm.GroupAgentCode.value+"' "
            + getWherePart("ManageCom","ManageCom",'like')	;
    var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
    if(!strQueryResult)
    {
      alert("系统中不存在该代理人！");   
      return false;
    }
    
    var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
    
}





/*****************************
  * 注: 提交前的校验、计算  
  ****************************/
function beforeSubmit()
{
  //添加操作	
}           
/*****************************
  * 注:显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示 
  ****************************/
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

/******************************************
 * 注: 查询得到返回值
 ******************************************/
function getQueryResult()
{
  var arrSelected = null;
  tRow = SetGrid.getSelNo();
  if( tRow == 0 || tRow == null || arrDataSet == null )
    return arrSelected;
  arrSelected = new Array();
  var tTrainNo=SetGrid.getRowColData(tRow-1,23);
  var strSql = "select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
      +" a.CManagecom,(select name from ldcom where comcode = a.cmanagecom)," 
      +"b.branchattr,b.name,"
      +"a.trainername,a.sex,(select codename from ldcode where code = a.sex and codetype = 'sex')," 
      +"a.trainergrade,(select codename from ldcode where code = a.trainergrade and codetype = 'trainergrade'),"
      +"a.birthday,a.polityvisage,(select codename from ldcode where code = a.polityvisage and codetype = 'polityvisage'),"
      +"a.WorkDate,a.InsureDate,a.GraduateSchool,a.Speciality,a.Education," 
      +	"(select codename from ldcode where code = a.Education and codetype = 'degree'),"
      +	"a.Degree,(select codename from ldcode where code = a.Degree and codetype = 'edu'),a.Mobile,a.Assess,"
      +"(select codename from ldcode where code = a.Assess and codetype = 'assess'),a.MulitId,"
      +"(select codename from ldcode where code = a.MulitId and codetype = 'yesno'),"
      +"a.trainerstate,"
      +"(case when a.trainerstate = '0' then '在职' when a.trainerstate = '1' then '离职' else '' end),a.trainerno,a.WorkYear,a.BusinessManagerName,a.TakeOfficeDate,a.LaborContract,"
      +"a.idnotype,(select codename from ldcode where codetype='idtype' and code=a.idnotype),a.idno,a.trainerCode"
      +"  from latrainer a,labranchgroup b  where a.agentgroup = b.agentgroup and b.branchtype = '1' and b.branchtype2 = '01'"
      +" and trainerno = '"+tTrainNo+"'"
  var strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  arrSelected = decodeEasyQueryResult(strQueryResult);
  return arrSelected;
}

/******************************************
 * 注: 返回主界面
 ******************************************/
function returnParent()
{
  var arrReturn = new Array();
  var tSel = SetGrid.getSelNo();	
  if( tSel == 0 || tSel == null )
    alert( "请先选择一条记录，再点击返回按钮。" );
  else
  {
    try
    {	
      arrReturn = getQueryResult();
      top.opener.afterQuery( arrReturn );
    }
    catch(ex)
    {
      alert( "没有发现父窗口的afterQuery接口。" + ex );
    }
    top.close();
		
  }
}

/******************************************
 * 注: 查询操作
 ******************************************/
function easyQueryClick()
{
     // 提前校验
	 if (!verifyInput()) return false ;
	 
     // 初始化表格
     initSetGrid();

  // 书写SQL语句
     var strSQL ="select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
         +" a.CManagecom,(select name from ldcom where comcode = a.cmanagecom)," 
         +"b.branchattr,b.name,b.BranchManager,"
         +" a.trainername,(select codename from ldcode where code = a.sex and codetype = 'sex'),(select codename from ldcode where code = a.trainergrade and codetype = 'trainergrade'),"
         +"a.Birthday,(select codename from ldcode where code=a.polityvisage and codetype='polityvisage'),a.WorkDate,a.InsureDate,a.GraduateSchool,a.Speciality,(select codename from ldcode where code = a.Education and codetype = 'degree'),(select codename from ldcode where code = a.Degree and codetype = 'edu'),a.Mobile,(select codename from ldcode where code = a.Assess and codetype = 'assess'),(select codename from ldcode where code = a.MulitId and codetype = 'yesno'),case when a.trainerstate = '0' then '在职' when a.trainerstate = '1' then '离职' else '' end,a.trainerno, "
         +"a.WorkYear,a.BusinessManagerName,a.TakeOfficeDate,a.idnotype,a.idno,a.trainerCode,a.trainerFlag from latrainer a,labranchgroup b  where a.agentgroup = b.agentgroup and b.branchtype = '1' and b.branchtype2 = '01'"
         +getWherePart('a.cmanagecom','CManageCom')
         +getWherePart('b.branchattr','BranchAttr')
         +getWherePart('a.pmanagecom','PManageCom','like')
         +getWherePart('a.trainerstate','TrainerState')
         +getWherePart('a.trainerCode','TrainerCode')
         +'order by a.pmanagecom'
      turnPage.queryModal(strSQL, SetGrid);  
       
	
}
function downLoad()
{
	if (!verifyInput()) return false ;
	 var strSQL ="select a.pmanagecom,(select name from ldcom where comcode = a.pmanagecom)," 
         +" a.CManagecom,(select name from ldcom where comcode = a.cmanagecom)," 
         +"b.branchattr,b.name,"
         +" a.trainername,(select codename from ldcode where code = a.sex and codetype = 'sex'),(select codename from ldcode where code = a.trainergrade and codetype = 'trainergrade'),"
         +"a.Birthday,(select codename from ldcode where code=a.polityvisage and codetype='polityvisage'),a.WorkDate,a.InsureDate,a.GraduateSchool,a.Speciality,(select codename from ldcode where code = a.Education and codetype = 'degree'),(select codename from ldcode where code = a.Degree and codetype = 'edu'),a.Mobile,(select codename from ldcode where code = a.Assess and codetype = 'assess'),(select codename from ldcode where code = a.MulitId and codetype = 'yesno'),case when a.trainerstate = '0' then '在职' when a.trainerstate = '1' then '离职' else '' end,a.WorkYear,a.BusinessManagerName,a.TakeOfficeDate,"
         +"a.idnotype,(select codename from ldcode where codetype='idtype' and code=a.idnotype),a.idno,a.trainerCode,a.trainerFlag  from latrainer a,labranchgroup b  where a.agentgroup = b.agentgroup and b.branchtype = '1' and b.branchtype2 = '01'"
         +getWherePart('a.cmanagecom','CManageCom')
         +getWherePart('b.branchattr','BranchAttr')
         +getWherePart('a.pmanagecom','PManageCom','like')
         +getWherePart('a.trainerstate','TrainerState')
         +getWherePart('a.trainerCode','TrainerCode')
         +'order by a.pmanagecom'

	fm.all('querySQL').value = strSQL;
	fm.submit();
}
