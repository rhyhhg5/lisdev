<%
//程序名称：LADimissionQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);

  String tTitleAgent="";
  String tTitleWage = "";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
    tTitleWage  = "佣金";
  }else if("2".equals(BranchType))
  {
    tTitleAgent = "业务员";
    tTitleWage  = "薪资";
  }
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"> </SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"> </SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" > </SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"> </SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"> </SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./LADimissionAppQueryInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LADimissionAppQueryInit.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>

  <title>离职管理 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LADimissionQuerySubmit.jsp" method=post name=fm target="fraSubmit">

<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLADimission1);">
    </IMG>
      <td class=titleImg> 查询条件 </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLADimission1" style= "display: ''">
     <table  class= common>
       <tr  class= common>

     	<TD class= title><%=tTitleAgent%>编码</TD>
						<TD  class= input>
							<Input class=common name=AgentCode
							 onchange="return getAgentMs();"
							  verify="编码|notnull"   elementtype=nacessary>
						</TD>
						<TD class= title> <%=tTitleAgent%>姓名 </TD>
						<TD  class= input>
							<Input class=readonly readonly name=AgentName>
						</TD>


    </tr>
    </table>


          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <input type=hidden name=Operation value="">
          <input type=hidden name=tManageCom value= '<%=tGI.ManageCom%>'>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
          <INPUT VALUE="打  印" TYPE=button onclick="submitForm();" class="cssButton">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 查询结果
    		</td>
    		<td class=common>
    		<font color="red">(注意：对于处于失效中止可复效的保单也在此列表显示，但不阻断业务员进行离职登记。)</font>
    		</td>
    	</tr>
    </table>
  	<Div  id= "divDimissionGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanDimissionGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>


      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
