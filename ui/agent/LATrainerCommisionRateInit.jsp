<%
//程序名称：LaTrainercommisionRateInit.jsp
//程序功能：
//创建时间：2018-06-7
//创建人  ：WangQingMin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
 var  mSQL=" 1  and  char(length(trim(comcode)))  = #8# and sign=#1# and comcode<>#86000000# " ;
 </SCRIPT>       


<script language="JavaScript">
function initInpBox()
{
  try
  {
	  	fm.all('PManageCom').value='';
	  	fm.all('PManageComName').value='';
	  	fm.all('CManageCom').value='';
	  	fm.all('CManageComName').value='';
	  	fm.all('BranchAttr').value='';
	  	fm.all('BranchAttrName').value='';
	  	fm.all('WageNo').value='';
	  	fm.all('TrainerCode').value='';
  }
  catch(ex)
  {
    alert("在LATraineCommisionInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
        
    iArray[1]=new Array();
    iArray[1][0]="管理机构"; //列名
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[1][4]="ComCode";
    iArray[1][5]="1|2";
    iArray[1][6]="0|1";
    iArray[1][9]="管理机构|code:comcode&NotNull&NUM&len=8";  
    iArray[1][15]="1";
    iArray[1][16]=mSQL; 
    
    iArray[2]=new Array();
    iArray[2][0]="管理机构名称"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许        
    
    iArray[3]=new Array();
    iArray[3][0]="营业部"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;          //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][7]="getBranchAttr1";    

    iArray[4]=new Array();
    iArray[4][0]="营业部名称"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;          //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许    
  
    iArray[5]=new Array();
    iArray[5][0]="薪资年月"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;          //列最大值
    iArray[5][3]=1;              //是否允许输入,1表示允许,0表示不允许    
    iArray[5][9]="薪资年月|NotNull&NUM&len=6";
 
    iArray[6]=new Array();
    iArray[6][0]="组训人员工号"; //列名
    iArray[6][1]="50px";      	      		//列宽
    iArray[6][2]=20;            			//列最大值
    iArray[6][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[6][7]="getTrainerCode";    

    
    iArray[7]=new Array();
    iArray[7][0]="组训人员姓名"; //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=100;          //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    iArray[7][9]="组训人员姓名|NotNull";
    
    iArray[8]=new Array();
    iArray[8][0]="月绩效比例";            //列名 idx
    iArray[8][1]="80px";          //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=1; 
    iArray[8][9]="月绩效比例|NotNull&num&value>=0&value<=1";

   
    iArray[9]=new Array();
    iArray[9][0]="idx";            //列名 idx
    iArray[9][1]="0px";          //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0; 
   
    iArray[10]=new Array();
    iArray[10][0]="AgentGroup";            //列名 idx
    iArray[10][1]="0px";          //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=0; 
    
  
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
    SetGrid.displayTitle = 1;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LATraineCommisionInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
  try
  {
    initInpBox();    
    initSetGrid();
  }
  catch(re)
  {
    alert("在LATraineCommisionInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>