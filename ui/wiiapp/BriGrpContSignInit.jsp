<%
//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String mManageCom = tGI.ComCode;
String mOpertator = tGI.Operator;
%>

<script language="JavaScript">
var Operator = "<%=mOpertator%>";

function initForm()
{
    try
    {
        initBriGrpContListGrid();
        
        fm.all('ManageCom').value = <%=mManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initBriGrpContListGrid()
{
	var iArray = new Array();

    try
    {
          iArray[0]=new Array();
          iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
          iArray[0][1]="30px";            		//列宽
          iArray[0][2]=10;            			//列最大值
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
          iArray[1]=new Array();
          iArray[1][0]="集体投保单号码";         		//列名
          iArray[1][1]="140px";            		//列宽
          iArray[1][2]=100;            			//列最大值
          iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
          iArray[2]=new Array();
          iArray[2][0]="印刷号";         		//列名
          iArray[2][1]="100px";            		//列宽
          iArray[2][2]=100;            			//列最大值
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
          iArray[3]=new Array();
          iArray[3][0]="团体客户号";         		//列名
          iArray[3][1]="100px";            		//列宽
          iArray[3][2]=200;            			//列最大值
          iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
          iArray[4]=new Array();
          iArray[4][0]="投保单位名称";         		//列名
          iArray[4][1]="200px";            		//列宽
          iArray[4][2]=200;            			//列最大值
          iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
          iArray[5]=new Array();
          iArray[5][0]="管理机构";         		//列名
          iArray[5][1]="80px";            		//列宽
          iArray[5][2]=100;            			//列最大值
          iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
          iArray[6]=new Array();
          iArray[6][0]="业务员代码";         		//列名
          iArray[6][1]="100px";            		//列宽
          iArray[6][2]=100;            			//列最大值
          iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
          iArray[7]=new Array();
          iArray[7][0]="工作流任务号";         		//列名
          iArray[7][1]="100px";            		//列宽
          iArray[7][2]=200;            			//列最大值
          iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
          
          iArray[8]=new Array();
          iArray[8][0]="工作流子任务号";         		//列名
          iArray[8][1]="0px";            		//列宽
          iArray[8][2]=200;            			//列最大值
          iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
          
          iArray[9]=new Array();
          iArray[9][0]="是否缴费";         		//列名
          iArray[9][1]="50px";            		//列宽
          iArray[9][2]=200;            			//列最大值
          iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
          
          iArray[10]=new Array();
          iArray[10][0]="已缴费金额";         		//列名
          iArray[10][1]="80px";            		//列宽
          iArray[10][2]=200;            			//列最大值
          iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
          
          iArray[11]=new Array();
          iArray[11][0]="催缴通知书状态";         		//列名
          iArray[11][1]="90px";            		//列宽
          iArray[11][2]=200;            			//列最大值
          iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
          iArray[11][4]="printstate"; 


        BriGrpContListGrid = new MulLineEnter("fm", "BriGrpContListGrid"); 

        BriGrpContListGrid.mulLineCount = 0;   
        BriGrpContListGrid.displayTitle = 1;
        BriGrpContListGrid.canSel = 0;
        BriGrpContListGrid.hiddenSubtraction = 1;
        BriGrpContListGrid.hiddenPlus = 1;
        BriGrpContListGrid.canChk = 1;
        BriGrpContListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ResultGrid时出错：" + ex);
    }
}
</script>

