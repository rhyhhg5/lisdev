//程序名称：
//程序功能：
//创建日期：2011-04-25
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


function qryGrpContInfo()
{
    var tGrpContNo = fm.GrpContNo.value;
    
    var tStrSql = ""
        + " select "
        + " PrtNo, GrpContNo "
        + " from LCGrpCont lgc "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + tGrpContNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        return ;
    }
    
    var tPrtNo = tArrResult[0][0];
    //var tGrpContNo = tArrResult[0][1];
    
    fm.PrtNo.value = tPrtNo;
}

/**
 * 查询已申请结算单。
 */
function qryInsuImpList()
{
    var tGrpContNo = fm.GrpContNo.value;

    if(!verifyInput2())
    {
        return false;
    }

    var tStrSql = ""
        + " select "
        + " tmp.InsuredNo, "
        + " tmp.Name, tmp.Sex, tmp.Birthday, tmp.IdType, tmp.IdNo, "
        + " sum(tmp.Prem) SumInsuPrem, "
        + " tmp.OccupationType, "
        + " tmp.InsuredStat "
        + " from "
        + " ( "
        + " select "
        + " lci.InsuredNo, "
        + " lci.Name, CodeName('sex', lci.Sex) Sex, lci.Birthday, CodeName('idtype', lci.IdType) IdType, lci.IdNo, "
        + " CodeName('occupationtype', lci.OccupationType) OccupationType, "
        + " CodeName('insustat', lci.InsuredStat) InsuredStat, "
        + " lcp.Prem, "
        + " ''  "
        + " from LCGrpCont lgc "
        + " inner join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo "
        + " inner join LCInsured lci on lci.InsuredNo = lcp.InsuredNo and lci.GrpContNo = lgc.GrpContNo "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + tGrpContNo + "' "
        + " ) as tmp "
        + " group by tmp.InsuredNo, tmp.Name, tmp.Sex, tmp.Birthday, tmp.IdType, tmp.IdNo, tmp.OccupationType, tmp.InsuredStat "
        + " order by InsuredNo "
        ;
    
    turnPage1.pageDivName = "divInsuImpListGridPage";
    turnPage1.queryModal(tStrSql, InsuImpListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        return false;
    }
    
    return true;
}
