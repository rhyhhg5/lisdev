<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;
String strBatchNo = request.getParameter("BatchNo");
%>

<script language="JavaScript">
var mBatchNo = "<%=strBatchNo%>";

function initInpBox()
{
    try
    {
        //fm.all("ManageCom").value = "";
        //fmImport.BatchNo.value = mBatchNo;
    }
    catch(ex)
    {
        alert("初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInpBox();
        initEsDocMainGrid();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

/**
 * 导入批次错误日志别表。
 */
function initEsDocMainGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="180px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="业务号";
        iArray[2][1]="120px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="保单号码";
        iArray[3][1]="120px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="是否签单";
        iArray[4][1]="120px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="导入时间";
        iArray[5][1]="120px";
        iArray[5][2]=100;
        iArray[5][3]=0;


        EsDocMainGrid = new MulLineEnter("fm", "EsDocMainGrid"); 

        EsDocMainGrid.mulLineCount = 0;   
        EsDocMainGrid.displayTitle = 1;
        EsDocMainGrid.canSel = 0;
        EsDocMainGrid.hiddenSubtraction = 1;
        EsDocMainGrid.hiddenPlus = 1;
        EsDocMainGrid.canChk = 0;
        EsDocMainGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化EsDocMainGrid时出错：" + ex);
    }
}
</script>