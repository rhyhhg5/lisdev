<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;

%>

<script language="JavaScript">

function initInpBox()
{
    try
    {
        fm.all('ManageCom').value = <%=tGI.ManageCom%>;
    }
    catch(ex)
    {
        alert("初始化界面错误!");
    }
}

function initForm()
{
    try
    {
        initInpBox();
        initConfirmBatchGrid();
        initConfirmLogGrid();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

/**
 * 导入确认列表
 */
function initConfirmBatchGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="报送单位";
        iArray[2][1]="30px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="报送人员";
        iArray[3][1]="30px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="任务数量";
        iArray[4][1]="30px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="导入状态";
        iArray[5][1]="30px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="导入开始日期";
        iArray[6][1]="60px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="导入完成日期";
        iArray[7][1]="50px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="批次确认状态";
        iArray[8][1]="40px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="批次确认时间";
        iArray[9][1]="40px";
        iArray[9][2]=100;
        iArray[9][3]=0;
        
        iArray[10]=new Array();
        iArray[10][0]="入机时间";
        iArray[10][1]="80px";
        iArray[10][2]=100;
        iArray[10][3]=0;
        
        ConfirmBatchGrid = new MulLineEnter("fm", "ConfirmBatchGrid"); 

        ConfirmBatchGrid.mulLineCount = 0;   
        ConfirmBatchGrid.displayTitle = 1;
        ConfirmBatchGrid.canSel = 1;
        ConfirmBatchGrid.hiddenSubtraction = 1;
        ConfirmBatchGrid.hiddenPlus = 1;
        ConfirmBatchGrid.canChk = 0;
        ConfirmBatchGrid.loadMulLine(iArray);
        ConfirmBatchGrid.selBoxEventFuncName = "BatchNoShow"; 
    }
    catch(ex)
    {
        alert("初始化ConfirmBatchGrid时出错：" + ex);
    }
}
/**
 * 导入批次错误日志别表。
 */
function initConfirmLogGrid()
{
    var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="业务号码";
        iArray[2][1]="60px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="导入状态";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="导入日期";
        iArray[4][1]="0px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="任务确认状态";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="任务确认时间";
        iArray[6][1]="0px";
        iArray[6][2]=100;
        iArray[6][3]=3;
        
        iArray[7]=new Array();
        iArray[7][0]="处理标志";
        iArray[7][1]="60px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="处理说明";
        iArray[8][1]="60px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="合同号";
        iArray[9][1]="0px";
        iArray[9][2]=100;
        iArray[9][3]=3;
        
        iArray[10]=new Array();
        iArray[10][0]="单位名称";
        iArray[10][1]="60px";
        iArray[10][2]=100;
        iArray[10][3]=0;
        
        iArray[11]=new Array();
        iArray[11][0]="组织机构代码";
        iArray[11][1]="60px";
        iArray[11][2]=100;
        iArray[11][3]=0;
        
        iArray[12]=new Array();
        iArray[12][0]="生效日期";
        iArray[12][1]="60px";
        iArray[12][2]=100;
        iArray[12][3]=0;
        
        iArray[13]=new Array();
        iArray[13][0]="满期日期";
        iArray[13][1]="60px";
        iArray[13][2]=100;
        iArray[13][3]=0;
        
        iArray[14]=new Array();
        iArray[14][0]="签发日期";
        iArray[14][1]="60px";
        iArray[14][2]=100;
        iArray[14][3]=0;
        
        iArray[15]=new Array();
        iArray[15][0]="财务到帐日期";
        iArray[15][1]="60px";
        iArray[15][2]=100;
        iArray[15][3]=0;
        
        iArray[16]=new Array();
        iArray[16][0]="保费";
        iArray[16][1]="60px";
        iArray[16][2]=100;
        iArray[16][3]=0;

        ConfirmLogGrid = new MulLineEnter("fm", "ConfirmLogGrid"); 

        ConfirmLogGrid.mulLineCount = 0;   
        ConfirmLogGrid.displayTitle = 1;
        ConfirmLogGrid.canSel = 0;
        ConfirmLogGrid.hiddenSubtraction = 1;
        ConfirmLogGrid.hiddenPlus = 1;
        ConfirmLogGrid.canChk = 0;
        ConfirmLogGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化ConfirmLogGrid时出错：" + ex);
    }
}
</script>