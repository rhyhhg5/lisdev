<%
//程序名称：
//程序功能：
//创建日期：2011-08-09
//创建人  ：zhangyang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String tManageCom = tGI.ComCode;
String tOperator = tGI.Operator;
%>

<script language="JavaScript">
var tManageCom = "<%=tManageCom%>";
var tOperator = "<%=tOperator%>";

function initForm()
{
    try
    {
        initWIIPolicyListGrid();
        
        fm.all('ManageCom').value = <%=tManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initWIIPolicyListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="印刷号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="管理机构";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="生效日期";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="录入日期";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="录入人";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="MissionId";
        iArray[6][1]="150px";
        iArray[6][2]=100;
        iArray[6][3]=3;
        
        iArray[7]=new Array();
        iArray[7][0]="SubMissionId";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=3;
        
        iArray[8]=new Array();
        iArray[8][0]="ProcessId";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=3;
        
        iArray[9]=new Array();
        iArray[9][0]="ActivityId";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=3;
        
        iArray[10]=new Array();
        iArray[10][0]="ActivityStatus";
        iArray[10][1]="80px";
        iArray[10][2]=100;
        iArray[10][3]=3;
        
        iArray[11]=new Array();
        iArray[11][0]="投保单状态";
        iArray[11][1]="100px";
        iArray[11][2]=100;
        iArray[11][3]=0;
        
        iArray[12]=new Array();
        iArray[12][0]="业务归档号";
        iArray[12][1]="150px";
        iArray[12][2]=200;
        iArray[12][3]=0;
        
        iArray[13]=new Array();
        iArray[13][0]="投保单位名称";
        iArray[13][1]="100px";
        iArray[13][2]=200;
        iArray[13][3]=0;
        
        iArray[14]=new Array();
        iArray[14][0]="业务员代码";
        iArray[14][1]="120px";
        iArray[14][2]=100;
        iArray[14][3]=0;
        
        iArray[15]=new Array();
        iArray[15][0]="期交保费";
        iArray[15][1]="80px";
        iArray[15][2]=80;
        iArray[15][3]=0;
        
        iArray[16]=new Array();
        iArray[16][0]="导入批次号";
        iArray[16][1]="120px";
        iArray[16][2]=80;
        iArray[16][3]=0;
        
        iArray[17]=new Array();
        iArray[17][0]="被保险人数";
        iArray[17][1]="80px";
        iArray[17][2]=80;
        iArray[17][3]=0;
        
        iArray[18]=new Array();
        iArray[18][0]="扫描件";
        iArray[18][1]="80px";
        iArray[18][2]=80;
        iArray[18][3]=0;
        
        iArray[19]=new Array();
        iArray[19][0]="保险合同号";
        iArray[19][1]="80px";
        iArray[19][2]=80;
        iArray[19][3]=0;
        
        iArray[20]=new Array();
        iArray[20][0]="财务收费日期";
        iArray[20][1]="80px";
        iArray[20][2]=80;
        iArray[20][3]=0;
        
        iArray[21]=new Array();
        iArray[21][0]="保单签发日期";
        iArray[21][1]="80px";
        iArray[21][2]=80;
        iArray[21][3]=0;
        
        iArray[22]=new Array();
        iArray[22][0]="银行代码";
        iArray[22][1]="80px";
        iArray[22][2]=80;
        iArray[22][3]=0;
        
        iArray[23]=new Array();
        iArray[23][0]="账户名称";
        iArray[23][1]="80px";
        iArray[23][2]=80;
        iArray[23][3]=0;
        
        iArray[24]=new Array();
        iArray[24][0]="账户号码";
        iArray[24][1]="80px";
        iArray[24][2]=80;
        iArray[24][3]=0;
        
        iArray[25]=new Array();
        iArray[25][0]="客户银行代码";
        iArray[25][1]="80px";
        iArray[25][2]=80;
        iArray[25][3]=0;
        
        iArray[26]=new Array();
        iArray[26][0]="委托付款授权协议编号";
        iArray[26][1]="80px";
        iArray[26][2]=80;
        iArray[26][3]=0;
        
        iArray[27]=new Array();
        iArray[27][0]="区县";
        iArray[27][1]="80px";
        iArray[27][2]=80;
        iArray[27][3]=0;  

        WIIPolicyListGrid = new MulLineEnter("fm", "WIIPolicyListGrid"); 

        WIIPolicyListGrid.mulLineCount = 0;   
        WIIPolicyListGrid.displayTitle = 1;
        WIIPolicyListGrid.canSel = 1;
        WIIPolicyListGrid.hiddenSubtraction = 1;
        WIIPolicyListGrid.hiddenPlus = 1;
        WIIPolicyListGrid.canChk = 0;
        WIIPolicyListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化WIIPolicyListGrid时出错：" + ex);
    }
}
</script>

