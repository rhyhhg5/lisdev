// 
var mDebug="0";
var mOperate="";
var showInfo;

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();

var succFlag = false;


// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
    if(cDebug=="1")
    {
        parent.fraMain.rows = "0,0,50,82,*";
    }
    else
    {
        parent.fraMain.rows = "0,0,0,82,*";
    }
}


/**
 * 创建保单数据
 */
function saveWrapPlanInfo()
{
    var checkedRowNum = 0;
    var chkedRowNo = null;
    var rowNum = WrapListGrid.mulLineCount;

    for (var i = 0; i < rowNum; i++)
    {
        if(checkedRowNum > 1)
        {
            break;
        }

        if(WrapListGrid.getChkNo(i))
        {
            checkedRowNum += 1;
            chkedRowNo = i;
        }
    }
    
    if(checkedRowNum < 1)
    {
        alert("请至少选择一个套餐。");
        return true;
    }

    if(checkedRowNum >= 2)
    {
        alert("最多选择一个套餐!");
        WrapDutyParamListGrid.clearData();
        return false;
    }

    if(!verifyInput2())
    {
        return false;
    }
    
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    var tUrl = "./BriGrpPolInputSave.jsp";
    fm.action = tUrl;
    fm.submit();
    fm.action = "";
    
    fm.btnCreateWrapPlan.disabled = true;
    
    return true;
}


function afterSubmit(FlagStr, content)
{
    showInfo.close();
    window.focus();

    if (FlagStr == "Fail")
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //showDiv(operateButton,"true");
        //showDiv(inputButton,"false");
        //执行下一步操作
        qryGrpPolicy();
    }
    
    fm.btnCreateWrapPlan.disabled = false;
}


function goToBack()
{
    var tPrtNo = fm.PrtNo.value;
    var tGrpContNo = fm.GrpContNo.value;
    var tMissionId = fm.MissionID.value;
    var tSubMissionId = fm.SubMissionID.value;
    var tProcessId = fm.ProcessID.value;
    var tActivityId = fm.ActivityID.value;
    var tActivityStatus = fm.ActivityStatus.value;
    var tManageCom = fm.ManageCom.value;
    var tQueryFlag = fm.QueryFlag.value;
    
    var tStrUrl = "./BriGrpContInput.jsp"
        + "?PrtNo=" + tPrtNo
        + "&GrpContNo=" + tGrpContNo
        + "&MissionID=" + tMissionId
        + "&SubMissionID=" + tSubMissionId
        + "&ProcessID=" + tProcessId
        + "&ActivityID=" + tActivityId
        + "&ActivityStatus=" + tActivityStatus
        + "&ManageCom=" + tManageCom
        + "&QueryFlag=" + tQueryFlag
        ;

    window.location = tStrUrl;
}


function inpConfirm()
{
    var tPrtNo = fm.PrtNo.value;

    var tStrSql = ""
        + " select "
        + " 1  "
        + " from LCGrpCont lgc "
        + " inner join LCCont lcc on lcc.GrpContNo = lgc.GrpContNo "
        + " inner join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo "
        + " where 1 = 1 "
        + " and lgc.PrtNo = '" + tPrtNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        alert("保单尚未导入被保人险人。请尝试导入被保人后并成功计算保费。");
        return false;
    }
    
    if(!checkMarkettypeSalechnl()){
    	return false;
    }
    if(!checkRiskSalechnl()){
    	return false;
    }

    //被保险人数校验
    if(!checkPeople()){
    	return false;
    }
    
    //校验补充工伤险业务 集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    //by yuchunjian  date 20180730   redmine 3884
    if(!checkFXlevel()){
    	return false;
    }
    
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    var tUrl = "./BriGrpContInpConfirmSave.jsp";
    fm.action = tUrl;
    fm.submit();
    fm.action = "";
    
    fm.btnInpConfirm.disabled = true;
}


function afterInpConfirmSubmit(FlagStr, content)
{
    showInfo.close();
    window.focus();

    if (FlagStr == "Fail")
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //showDiv(operateButton,"true");
        //showDiv(inputButton,"false");
        //执行下一步操作
        alert("已录入完毕，请复核。");
        qryGrpPolicy();
        displayConfirmDiv();
        return ;
    }
    fm.btnCreateWrapPlan.disabled = false;
}


function chkConfirm()
{
    var tPrtNo = fm.PrtNo.value;

    var tStrSql = ""
        + " select "
        + " 1  "
        + " from LCGrpCont lgc "
        + " inner join LCCont lcc on lcc.GrpContNo = lgc.GrpContNo "
        + " inner join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo "
        + " where 1 = 1 "
        + " and lgc.PrtNo = '" + tPrtNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        alert("保单尚未导入被保人险人。请尝试导入被保人后并成功计算保费。");
        return false;
    }
    
    if(!checkMarkettypeSalechnl()){
    	return false;
    }
    if(!checkRiskSalechnl()){
    	return false;
    }

    //被保险人数校验
    if(!checkPeople()){
    	return false;
    }
    
    //校验补充工伤险业务 集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    var tUrl = "./BriGrpContChkConfirmSave.jsp";
    fm.action = tUrl;
    fm.submit();
    fm.action = "";
    
    fm.btnInpConfirm.disabled = true;
}


function afterChkConfirmSubmit(FlagStr, content)
{
    showInfo.close();
    window.focus();

    if (FlagStr == "Fail")
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //showDiv(operateButton,"true");
        //showDiv(inputButton,"false");
        //执行下一步操作
        top.window.close();
        return ;
    }
    fm.btnCreateWrapPlan.disabled = false;
}


function displayConfirmDiv()
{
    var tPrtNo = fm.PrtNo.value;

    var tStrSql = ""
        + " select "
        + " (case when lwm.MissionProp20 = '1' then '1' else '0' end) tConfirmFlag "
        + " from LWMission lwm "
        + " where 1 = 1 "
        + " and lwm.ProcessId = '0000000012' "
        + " and lwm.ActivityId In ('0000012001') "
        + " and lwm.MissionProp1 = '" + tPrtNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        return false;
    }
    
    var tConfirmFlag = tArrResult[0][0];
    if(tConfirmFlag == '1')
    {
        // 显示复核
        fm.btnInpConfirm.disabled = true;
        fm.btnChkConfirm.disabled = false;

        fm.btnCreateWrapPlan.value = "   修  改   ";
    }
    else
    {
        // 显示录入
        fm.btnInpConfirm.disabled = false;
        fm.btnChkConfirm.disabled = true;

        fm.btnCreateWrapPlan.value = "   保  存   ";
    }
}


function goToInsuList()
{
    var tGrpContNo = fm.GrpContNo.value;

    var tStrSql = ""
        + " select "
        + " 1 "
        + " from LCGrpCont lgc "
        + " inner join LCContPlan lccp on lccp.GrpContNo = lgc.GrpContNo "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + tGrpContNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        alert("请先录入保单套餐信息。");
        return false;
    }

    var tGrpContNo = fm.GrpContNo.value;
    if (tGrpContNo == null || tGrpContNo == "")
    {
        alert("保单合同信息未找到，请尝试重新进入后，再次操作。");
        return false;
    }
    
    var strUrl = "./DiskApplyInputMain.jsp?GrpContNo=" + tGrpContNo;
    showInfo=window.open(strUrl, "被保人清单导入", "width=1000px, height=600px, top=0, left=0");
}


/**
 * 初始化查询
 */
function qryGrpPolicy()
{
    qryGrpContInfo();
    qryWrapInfo();
    
    qryGrpContWrapInfo();
}


/**
 * 保单信息查询
 */
function qryGrpContInfo()
{
    var tGrpContNo = fm.GrpContNo.value;
    
    var tStrSql = ""
        + " select "
        + " tmp.GrpContNo, tmp.PrtNo, tmp.Managecom, "
        + " (tmp.SaleChnl || '-' || CodeName('salechnl', tmp.SaleChnl)) SaleChnl, "
        + " tmp.CValidate, tmp.CInValidate, "
        + " tmp.SumInsuCount, tmp.SumInsuPrem "
        + " from "
        + " ( "
        + " select "
        + " tmpPolicy.GrpContNo, tmpPolicy.PrtNo, tmpPolicy.Managecom, tmpPolicy.SaleChnl, "
        + " tmpPolicy.CValidate, tmpPolicy.CInValidate, "
        + " nvl(sum(tmpPolicy.SumInsuCount), 0) SumInsuCount, "
        + " nvl(sum(tmpPolicy.SumInsuPrem), 0) SumInsuPrem "
        + " from "
        + " ( "
        + " select "
        + " lgc.GrpContNo, lgc.PrtNo, lgc.Managecom, lgc.SaleChnl, "
        + " lgc.CValidate, lgc.CInValidate, "
        + " count(distinct lcp.InsuredNo) SumInsuCount, "
        + " nvl(sum(lcp.Prem), 0) SumInsuPrem, "
        + " '' "
        + " from LCGrpCont lgc "
        + " left join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo and lcp.PolTypeFlag in ('0') "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + tGrpContNo + "' "
        + " group by lgc.GrpContNo, lgc.PrtNo, lgc.Managecom, lgc.SaleChnl, lgc.CValidate, lgc.CInValidate "
        + " union all "
        + " select "
        + " lgc.GrpContNo, lgc.PrtNo, lgc.Managecom, lgc.SaleChnl, "
        + " lgc.CValidate, lgc.CInValidate, "
        + " lgc.Peoples2 SumInsuCount, "
        + " nvl(sum(lcp.Prem), 0) SumInsuPrem, "
        + " '' "
        + " from LCGrpCont lgc "
        + " left join LCPol lcp on lcp.GrpContNo = lgc.GrpContNo and lcp.PolTypeFlag in ('1') "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + tGrpContNo + "' "
        + " group by lgc.GrpContNo, lgc.PrtNo, lgc.Managecom, lgc.SaleChnl, lgc.CValidate, lgc.CInValidate, lgc.Peoples2 "
        + " ) as tmpPolicy "
        + " group by tmpPolicy.GrpContNo, tmpPolicy.PrtNo, tmpPolicy.Managecom, tmpPolicy.SaleChnl, tmpPolicy.CValidate, tmpPolicy.CInValidate "
        + " ) as tmp "
        ;
        
    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        return false;
    }
    
    // fm.GrpContNo.value = tArrResult[0][0];
    // fm.PrtNo.value = tArrResult[0][1];
    
    // fm.Managecom.value = tArrResult[0][2];
    fm.SaleChnl.value = tArrResult[0][3];
    
    fm.CValidate.value = tArrResult[0][4];
    fm.CInValidate.value = tArrResult[0][5];
    
    fm.SumInsuCount.value = tArrResult[0][6];
    fm.SumInsuPrem.value = tArrResult[0][7];
    
    return true;
}


function qryWrapInfo()
{
    var tManageCom = fm.ManageCom.value;
    
    var tStrSql = ""
        + " select "
        + " ldw.RiskWrapCode, ldw.WrapName, ldw.WrapManageCom, min(ldrw.EndDate) MinEndDate "
        + " from LDWrap ldw "
        + " inner join LDRiskWrap ldrw on ldrw.RiskWrapCode = ldw.RiskWrapCode "
        + " where 1 = 1 "
        + " and ldw.WrapType = 'G' "
        + " and (ldw.WrapManageCom = '" + tManageCom + "' or ldw.WrapManageCom = substr('" + tManageCom + "', 1, length(ldw.WrapManageCom))) "
        + " group by ldw.RiskWrapCode, ldw.WrapName, ldw.WrapManageCom "
        ;

    turnPage1.pageLineNum = 20;
    turnPage1.pageDivName = "divWrapListGridPage";
    turnPage1.queryModal(tStrSql, WrapListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("[" + tManageCom + "]机构下未找到可销售产品");
        return false;
    }
    
    qryWrapDutyParamInfo();
    
    return true;
}


function qryGrpContWrapInfo()
{
    var tGrpContNo = fm.GrpContNo.value;
    
    var tStrSql = ""
        + " select "
        + " distinct RiskWrapCode "
        + " from LCContPlanRisk "
        + " where 1 = 1 "
        //+ " and RiskWrapCode = '900101' "
        + " and RiskWrapFlag = 'Y' "
        + " and GrpContNo = '" + tGrpContNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);    
    if(!tArrResult)
    {
        return ;
    }
    
    var tWrapCode = tArrResult[0][0];
    chkedWrapInfo(tWrapCode);
}


function qryWrapDutyParamInfo()
{    
    var checkedRowNum = 0;
    var chkedRowNo = null;
    var rowNum = WrapListGrid.mulLineCount;

    for (var i = 0; i < rowNum; i++)
    {
        if(checkedRowNum > 1)
        {
            break;
        }

        if(WrapListGrid.getChkNo(i))
        {
            checkedRowNum += 1;
            chkedRowNo = i;
        }
    }
    
    if(checkedRowNum < 1)
    {
        WrapDutyParamListGrid.clearData();
        return true;
    }

    if(checkedRowNum >= 2)
    {
        alert("最多选择一个套餐!");
        WrapDutyParamListGrid.clearData();
        return false;
    }
    
    var tRowDatas = WrapListGrid.getRowData(chkedRowNo);
    var tWrapCode = tRowDatas[0];
    var tGrpContNo = fm.GrpContNo.value;
    
    var tStrSql = ""
        + " select "
        + " ldrdw.RiskWrapCode, "
        + " ldrdw.RiskCode, lmra.RiskName, ldrdw.MainRiskCode, "
        + " ldrdw.DutyCode, lmd.DutyName, "
        + " ldrdw.CalFactor, ldrdw.CalFactorName, "
        + " ldrdw.CalFactorType, "
        + " (case ldrdw.CalFactorType when '2' then lccpdp.CalFactorValue when '1' then ldrdw.CalFactorValue end) CalFactorValue, "
        + " (case ldrdw.CalFactorType when '2' then '' else '套餐已约定' end) CalFactorTypeName "
        + " from LDWrap ldw "
        + " inner join LDRiskWrap ldrw on ldrw.RiskWrapCode = ldw.RiskWrapCode "
        + " inner join LDRiskDutyWrap ldrdw on ldrdw.RiskWrapCode = ldrw.RiskWrapCode and ldrdw.RiskCode = ldrw.RiskCode "
        + " inner join LMRiskApp lmra on lmra.RiskCode = ldrw.RiskCode "
        + " inner join LMDuty lmd on lmd.DutyCode = ldrdw.DutyCode "
        + " left join LCContPlanDutyParam lccpdp on lccpdp.RiskCode = ldrdw.RiskCode and lccpdp.DutyCode = ldrdw.DutyCode and lccpdp.CalFactor = ldrdw.CalFactor and lccpdp.GrpContNo = '" + tGrpContNo + "' "
        + " where 1 = 1 "
        + " and ldw.RiskWrapCode = '" + tWrapCode + "' "
        ;

    turnPage3.pageLineNum = 100;
    turnPage3.pageDivName = "divWrapDutyParamListGridPage";
    turnPage3.queryModal(tStrSql, WrapDutyParamListGrid);

    if (!turnPage3.strQueryResult)
    {
        alert("[" + tWrapCode + "]套餐未找到相关要素信息。");
        return false;
    }

    return true;
}


function chkedWrapInfo(cWrapCode)
{
    qryWrapInfo();

    var rowNum = WrapListGrid.mulLineCount;

    for (var i = 0; i < rowNum; i++)
    {        
        var tRowDatas = WrapListGrid.getRowData(i);
        var tWrapCode = tRowDatas[0];
        
        if(cWrapCode == tWrapCode)
        {
            WrapListGrid.checkBoxSel(i + 1);
            qryWrapDutyParamInfo();
            break;
        }
    }
}

//查看已导入被保险人清单
function qryInsuImportList()
{
    var tGrpContNo = fm.GrpContNo.value;
    
    var tStrUrl = "./BriInsuImpListMain.jsp"
        + "?GrpContNo=" + tGrpContNo
        ;

    window.open(tStrUrl, "InsuImportList");
}

function checkMarkettypeSalechnl(){
	var tSQL = "select salechnl,markettype from lcgrpcont where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单数据失败！");
		return false;
	}
	var tSaleChnl = arr[0][0];
	var tMarketType = arr[0][1];
	var tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+tMarketType+"' and code1 = '"+tSaleChnl+"' ";
	var arrCheck = easyExecSql(tcheckSQL);
	if(!arrCheck){
		alert("市场类型和销售渠道不匹配，请核查！");
		return false;
	}
	return true;
}
function checkRiskSalechnl(){
	var tSQL = "select salechnl,riskcode from lcgrppol where prtno = '"+fm.PrtNo.value+"'";
	var arr = easyExecSql(tSQL);
	if(!arr){
		alert("获取保单险种数据失败！");
		return false;
	}
	for(var i=0;i<arr.length;i++){
		var tSaleChnl = arr[i][0];
		var tRiskCode = arr[i][1];
		var tcheckSQL = "select codename from ldcode where codetype = 'unitesalechnlrisk' and code = '"+tRiskCode+"' ";
		var arrCheck = easyExecSql(tcheckSQL);
		if(!arrCheck){
			if(tSaleChnl == "16"){
				alert("险种"+tRiskCode+"为非社保险种，与销售渠道"+tSaleChnl+"不匹配！");
				return false;
			}
		}else{
			if(arrCheck[0][0] != "all" && arrCheck[0][0] != tSaleChnl){
				alert("险种"+tRiskCode+"与销售渠道"+tSaleChnl+"不匹配！");
				return false;
			}
		}
	}
	return true;
}
function checkPeople(){
	var tPrtNo = fm.PrtNo.value;
	var tSqlCount="select peoples2 from LCGrpCont where PrtNo='" + tPrtNo + "'";
	var arrCount = easyExecSql(tSqlCount);
	if(arrCount<3){
		alert("被保险人数应大于等于3！");
		return false;
	}
	return true;
}

//校验补充工伤险集团校验销售相互代理业务的销售渠道与中介机构是否符合描述。   add by 赵庆涛 2016-06-28

function checkCrsBussSaleChnl()
{
	 var Crs_SaleChnl = null;  
	 var arr = easyExecSql("select Crs_SaleChnl from lcgrpcont where prtno = '" + fm.PrtNo.value + "' ");
	 if(arr){
		 Crs_SaleChnl = arr[0][0];
	    }
	 if(Crs_SaleChnl != null && Crs_SaleChnl != "")
	 {
		var strSQL = "select 1 from lcgrpcont lgc where 1 = 1"  
			+ " and lgc.Crs_SaleChnl is not null " 
			+ " and lgc.Crs_BussType = '01' " 
			+ " and not (lgc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lgc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
			+ " and lgc.PrtNo = '" + fm.PrtNo.value + "' ";
		var arrResult = easyExecSql(strSQL);
        if (arrResult != null)
        {
        	alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
        	return false;
        }
	}
	return true;
}

//by yuchunjian  date 20180730   redmine 3884
function checkFXlevel(){
	var sql1 = "select name from lcgrpappnt where GrpContNo='"+fm.GrpContNo.value+"' ";
	var arrResult = easyExecSql(sql1);
	if(arrResult){
		var sqlblack = "select max(HighestEvaLevel) from fx_FXQVIEW where AOGName= '"+arrResult[0][0]+"' ";
		var fxarr = easyExecSql(sqlblack);
		   if(sqlblack){
			   if(fxarr[0][0]=="3"){
				   if(!confirm(arrResult[0][0] +"，风险等级评估为高风险客户，确认要完成录入？")){
					   return false;
				   }
			   }else if(fxarr[0][0]=="2"){
				   if(!confirm(arrResult[0][0] +"，风险等级评估为中风险客户，确认要完成录入？")){
					   return false;
				   }
			   }
		   }
	}
	return true;
}

