<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2011-03-23
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.lis.wiitb.*"%>
<%@page import="com.sinosoft.lis.db.*"%>


<%
	//输出参数
	System.out.println("开始执行大连工伤险签单");
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	String Priview = "PREVIEW";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	TransferData tTransferData = new TransferData();
  
  	//接收信息
  	// 投保单列表
	LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
	String tGrpContNo[] = request.getParameterValues("BriGrpContListGrid1");
	String tRadio[] = request.getParameterValues("InpBriGrpContListGridSel");
	
	boolean flag = false;
	String mGrpContNo = "";
	int grouppolCount = tGrpContNo.length;
	for (int i = 0; i < grouppolCount; i++)
	{
		if( tGrpContNo[i] != null && tRadio[i].equals( "1" ))
		{
		    mGrpContNo = tGrpContNo[i] ;
		    tTransferData.setNameAndValue("GrpContNo",mGrpContNo);
		    flag = true;
			break;
		}
	}
  	if (flag == true)
  	{
        
		tLCGrpContSchema.setGrpContNo(mGrpContNo);
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCGrpContSchema );
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
        BriGrpSignBL tBriGrpSignBL = new BriGrpSignBL();
		boolean bl = tBriGrpSignBL.submitData(tVData,"");
		int n = tBriGrpSignBL.mErrors.getErrorCount();
		if( n == 0 ) 
	    { 
		    if ( bl )
		    {
	    		Content = " 签单成功";
		   		FlagStr = "Succ";
	    	}
	    	else
	    	{
	    	   Content = " 签单失败";
	    	   FlagStr = "Fail";
	    	}
	    }
	    else
	    {
	         
	    	String strErr = "";
			for (int i = 0; i < n; i++)
			{
				strErr += (i+1) + ": " + tBriGrpSignBL.mErrors.getError(i).errorMessage + "; ";
				System.out.println(tBriGrpSignBL.mErrors.getError(i).errorMessage );
			}
			 if ( bl )
			 {
	    		Content = " 部分签单成功,但是有如下信息: " +strErr;
		   		FlagStr = "Succ";
	    	}
	    	else
	    	{
		 		  Content = " 签单失败，原因是: " + strErr;
				  FlagStr = "Fail";
			}
		} // end of if
	} // end of if
	
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
