//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

function queryEsDocMain()
{
    //var tBatchNo = fm.ImportBatchNo.value;
    //if(tBatchNo =="" || tBatchNo == null){
    	//alert("批次号不能为空！");
    	//return false;
    //}
    var tStrSql = "select lbid.batchno,lgc.prtno,lgc.grpcontno,'已签单', varchar(lbid.MakeDate) || ' '|| lbid.MakeTime "
    			+ "from lcgrpcont lgc "
                + "inner join lcbrigrpimportdetail lbid on lgc.prtno = lbid.bussno "
                + "where 1=1 and lgc.cardflag = '4' and lbid.importstate = '02' "
                + "and lbid.enableflag = '01' and lbid.dealflag = '01' "
                + "and lgc.appflag = '1' and lgc.signdate is not null "
                + "and lgc.prtno not in (select DocCode from es_doc_main) "
                + "and substr(lgc.prtno,3,3) = '001' "
                + getWherePart( 'lgc.managecom','manageCom' )
				+ getWherePart( 'lbid.batchno','EsDocMainBatchNo' )
				+ getWherePart( 'lgc.prtno','EsDocMainPrtNo' )
				+ getWherePart( 'lgc.grpcontno','EsDocMainContNo' )
				+ getWherePart('importDate', 'importStartDate',">=")
        		+ getWherePart('importDate', 'importEndDate',"<=")
                + "union "
                + "select lbid.batchno,lgc.prtno,lgc.grpcontno,'未签单', varchar(lbid.MakeDate) || ' '|| lbid.MakeTime "
                + "from lcgrpcont lgc "
                + "inner join lcbrigrpimportdetail lbid on lgc.prtno = lbid.bussno "
                + "where 1=1 and lgc.cardflag = '4' and lbid.importstate = '02' "
                + "and lbid.enableflag = '01' and lbid.dealflag != '01' "
                + "and lgc.appflag !='1' "
                + "and lbid.batchno = (select batchno from lcbrigrpimportdetail where bussno = lgc.prtno order by makedate desc fetch first 1 rows only) "
                + "and lgc.prtno not in (select DocCode from es_doc_main) "
                + "and substr(lgc.prtno,3,3) = '001' "
                + getWherePart( 'lgc.managecom','manageCom' )
				+ getWherePart( 'lbid.batchno','EsDocMainBatchNo' )
				+ getWherePart( 'lgc.prtno','EsDocMainPrtNo' )
				+ getWherePart( 'lgc.grpcontno','EsDocMainContNo' )
				+ getWherePart('importDate', 'importStartDate',">=")
        		+ getWherePart('importDate', 'importEndDate',"<=")
//prompt('',tStrSql);
    turnPage2.pageDivName = "divEsDocMainGridPage";
    turnPage2.queryModal(tStrSql, EsDocMainGrid);

    if (!turnPage2.strQueryResult)
    {
        alert("没有匹配的查询结果！");
        return false;
    }
    fm.querySql.value = tStrSql;
    return true;
}

//下载事件
function downloadList()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && EsDocMainGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "XmlListEsDocMainDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再下载！");
        return ;
    }
}

