//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryBriGrpContList()
{
    if(!verifyInput2())
    {
        return false;
    }

    var strSQL = "select lgc.proposalgrpcontno, lgc.prtno,lgc.appntno,lgc.grpname,lgc.managecom,getUniteCode(lgc.agentcode),'','', "
    			+"(case when (select max('已交费') from ljtempfee where otherno = lgc.prtno and enteraccdate is not null and confdate is null) is null then '未交费' else '已交费' end), "
    			+"(case when (select sum(paymoney) from ljtempfee where otherno=lgc.prtno  and enteraccdate is not null and confdate is null) is null then 0 " 
    			+"else (select sum(paymoney) from ljtempfee where otherno=lgc.prtno and enteraccdate is not null ) end ), "
    			+"(select stateflag from loprtmanager where otherno = lgc.prtno and code = '76') "
    			+"from lcgrpcont lgc "
    			+"where 1=1 and lgc.appflag !='1' "
    			+"and lgc.prtno in "
    			+"(select bussno from lcbrigrpimportdetail where enableflag = '01' and importstate = '02' and dealflag = '00') "
    			+"and not exists( select 1 from lwmission where missionprop1 = lgc.prtno or missionprop2 = lgc.prtno or missionprop3 = lgc.prtno or missionprop5 = lgc.prtno)"
    + getWherePart( 'lgc.prtno','PrtNo' )
    + getWherePart( 'lgc.ManageCom','ManageCom','like' )
    //prompt('',strSQL);			
    turnPage1.pageDivName = "divBriGrpContListGridPage";
    turnPage1.queryModal(strSQL, BriGrpContListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有待签发的保单信息！");
        return false;
    }
    
    return true;
}


//提交，保存按钮对应操作
function signGrpPol()
{
    var tSel = BriGrpContListGrid.getSelNo();
    var cPolNo = "";
    var tGrpContNo = "";
    if( tSel != null && tSel != 0 )
        cPolNo = BriGrpContListGrid.getRowColData( tSel - 1, 2 );
    if( cPolNo == null || cPolNo == "" )
        alert("请选择一张集体投保单后，再进行签单操作");
    else
    {
        var strSql = "select * from ldsystrace where PolNo='" + cPolNo + "' and PolState=1006 ";
      	var arrResult = easyExecSql(strSql);
      	if (arrResult!=null && arrResult[0][1]!=Operator) {
        	alert("该印刷号的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
        	return;
      	}
      //锁定该印刷号
      //	tGrpContNo = BriGrpContListGrid.getRowColData( tSel - 1, 1 );
      //	var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + cPolNo + "&CreatePos=承保签单&PolState=1006&Action=INSERT";
      //	var a = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
        var i = 0;
        var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.action = "BriGrpContImportSignSave.jsp";
        fm.submit(); //提交
    }
}

function afterSubmit( FlagStr, content )
{
    showInfo.close();
    window.focus();
    fm.all("workType").value = "";
    if( FlagStr == "Fail" )
    {   
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

        // 刷新查询结果
        queryBriGrpContList();
    }
}

