<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.wiitb.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>

<%
System.out.println("XmlListConfirmSave.jsp Begin ...");

GlobalInput tG = (GlobalInput)session.getValue("GI");

String FlagStr = "Fail";
String Content = "";
String tBatchNo = request.getParameter("BatchNo");

System.out.println("确认时获取批次号码为："+tBatchNo);

try
{
    if(tBatchNo != null && !"".equals(tBatchNo))
    {
        VData tVData = new VData();
        
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", tBatchNo);
        
        tVData.add(tTransferData);
        tVData.add(tG);
    
        BriGrpConfirmUI tBriGrpConfirmUI = new BriGrpConfirmUI();
        
        if (!tBriGrpConfirmUI.submitData(tVData, "Confrim"))
        {
            //Content = " 导入清单失败! 原因是: " + tBriGrpConfirmUI.mErrors.getLastError();
            Content = " 批次确认失败! 原因详见“批次确认日志”！";
            FlagStr = "Fail";
        }
        else
        {
            Content = " 批次确认成功！";
            FlagStr = "Succ";
        }
    }
    else
    {
        Content = " 批次号获取失败！";
        FlagStr = "Fail";
    }
}
catch (Exception e)
{
    Content = " 获取批次信息失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("XmlListConfirmSave.jsp End ...");
%>
                  
<html>
<script language="javascript">
parent.fraInterface.afterConfirmBatch("<%=FlagStr%>", "<%=Content%>", "<%=tBatchNo%>");
</script>
</html>

