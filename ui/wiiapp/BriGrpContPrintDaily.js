//该文件中包含客户端需要处理的函数和事件
var arrDataSet
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//查询事件
function easyQueryClick()
{
    if(!checkDate()) return false;

    var strSQL = "";
    if(fm.contType.value == "2")
    {
        strSQL = "select a.ManageCom, BranchAttr, c.Name, "
            + "(select CodeName from LDCode where CodeType = 'salechnl' and Code = a.SaleChnl), "
            + " getUniteCode(a.AgentCode), (select d.Name from LAAgent d where a.AgentCode = d.AgentCode), "
            + "a.PrtNo, a.GrpContNo, a.GrpName, a.Prem, "
            + "a.CValidate, a.SignDate, b.MakeDate, b.MakeTime "
            + "from LCGrpCont a, LOPRTManager b, LABranchGroup c "
            + "where a.GrpContNo = b.OtherNo and c.AgentGroup = a.AgentGroup "
            + "and a.ManageCom like '" + fm.manageCom.value + "%' "
            + " and a.appflag = '1' and a.printcount = 1 "
            + " and a.cardflag = '4' and b.code = 'GS002' "
            //+ "and exists (select 1 from LDCode1 where CodeType = 'printchannel' and Code = substr(a.ManageCom, 1, 4) and Code1 = '2' and CodeName = '1') "
            + "and b.MakeTime between '" + fm.startTime.value + ":00:00' and '" + fm.endTime.value + ":00:00' "
            + getWherePart('a.PrtNo', 'prtNo')
            + getWherePart('b.MakeDate', 'dailyDate')
            + "order by c.BranchAttr, a.AgentCode";
    }
    else
    {
        alert("请选择保单类型");
        return false;
    }

    turnPage.queryModal(strSQL, ContPrintDailyGrid);

    fm.querySql.value = strSQL;
}

//校验日期
function checkDate()
{
	if(fm.dailyDate.value == null || fm.dailyDate.value == "")
	{
		alert("查询日期必选!");
		return false;
	}
    if(fm.startTime.value == null || fm.startTime.value == "")
    {
        alert("查询起始时间必选！");
        return false;
    }
    if(fm.endTime.value == null || fm.endTime.value == "")
    {
        alert("查询终止时间必选！");
        return false;
    }
    return true;
}

//下载事件
function download()
{
    if(fm.querySql.value != null && fm.querySql.value != "" && ContPrintDailyGrid.mulLineCount > 0)
    {
        fm.action = "BriGrpContPrintDailyDownload.jsp";
        fm.target = "_blank";
        fm.submit();
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
