<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：BriGrpContDelete.jsp
//程序功能：团单整单删除
//创建日期：2004-12-06 11:10:36
//创建人  ：Zhangrong
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	//String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var Brief = "<%=request.getParameter("Brief")%>";
</script>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="BriGrpContDelete.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BriGrpContDeleteInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
        <TR  class= common>
         <!-- <TD  class= title>
            团体投保单号
          </TD>
          <TD  class= input>-->
            <Input class=common name=QGrpProposalNo type="hidden">
          <!--<TD  class= title>
            团体保单号
          </TD>
          <TD  class= input>-->
            <Input class=common name=QGrpContNo type="hidden">

          <TD  class= title>
            印刷号
          </TD>
          <TD  class= input>
            <Input class=common name=QPrtNo >
          </TD>

          
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<Input class="codeNo"  name=QManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,QManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,QManageComName],[0,1],null,null,null,1);"><input class=codename name=QManageComName readonly=true >
            
          </TD>  
          <TD  class= title>
            业务员代码
          </TD>
          <TD  class= input>
            <Input class=codeNo name=QAgentCode  ondblclick="return showCodeList('AgentCodet2',[this,QAgentCodeName],[0,1],null,fm.all('QManageCom').value,'ManageCom',1);" onkeyup="return showCodeListKey('AgentCodet2',[this,QAgentCodeName],[0,1]);showAllCodeName();"><input class=codename name=QAgentCodeName readonly=true >
          </TD>   
          <TD  class= title>
            <!-- 保单状态 -->
          </TD>
          <TD  class= input>
            <Input class="code" type = "hidden" readonly name=QState value= "0" CodeData= "0|^0|未签单^1|已签单" ondblClick="showCodeListEx('State',[this,''],[1,0],'', '', '', 1);" onkeyup="showCodeListKeyEx('State',[this,''],[1,0],'', '', '', 1);">
          </TD>     
        </TR>
        
    </table>
       <INPUT VALUE="查  询" Class="cssButton" TYPE=button onclick="querygrp();">
       <INPUT type= "hidden" name= "Operator" value= "">
       <INPUT type= "hidden" name= "GrpContNo" value= "">
       <INPUT type= "hidden" name= "PrtNo" value= "">
       <INPUT type= "hidden" name= "xPrtNo" value= "">
    <!-- 查询未过团体单（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 团体投保单查询结果
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divLCPol1" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrpGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      	<table  class= common>
      		<INPUT VALUE="首  页" Class="cssButton" TYPE=button onclick="getFirstPage();"> 
      		<INPUT VALUE="上一页" Class="cssButton" TYPE=button onclick="getPreviousPage();"> 					
      		<INPUT VALUE="下一页" Class="cssButton" TYPE=button onclick="getNextPage();"> 
      		<INPUT VALUE="尾  页" Class="cssButton" TYPE=button onclick="getLastPage();">    	
    	</table>
    	<P>
      	<table  class= common>
            <TR  class= common>
            <INPUT VALUE="团单整单删除" Class="cssButton" TYPE=button onclick="deleteGrpCont();">
            <!--  
    	      <INPUT VALUE="删除被保险人" Class="cssButton" TYPE=button onclick="deleteCont();"> 
    	    -->
    	      </TR>
    	
    	</table>
    	</P>
	</Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
