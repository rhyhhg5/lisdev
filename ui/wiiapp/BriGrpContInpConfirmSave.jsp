<%@page contentType="text/html;charset=GBK"%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.wiitb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.brieftb.ContInputAgentcomChkBL"%>

<%
System.out.println("BriGrpContInpConfirmSave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{
    String tPrtNo = request.getParameter("PrtNo");
    String tProcessID = request.getParameter("ProcessID");
    String tActivityID = request.getParameter("ActivityID");

    // 更新录入完毕标志
    String tStrSql = ""
        + " update LWMission lwm "
        + " set lwm.MissionProp20 = '1' "
        + " where 1 = 1 "
        + " and lwm.ProcessId = '0000000012' "
        + " and lwm.ActivityId In ('0000012001') "
        + " and lwm.MissionProp1 = '" + tPrtNo + "' "
        ;
    
    MMap tMMap = new MMap();
    tMMap.put(tStrSql, "UPDATE");
    
    VData data = new VData();
    data.add(tMMap);

    PubSubmit p = new PubSubmit();
    //江苏团险校验
    boolean tJSFlag = false;
    {
    	String tSalechnlSQL = "select salechnl from lcgrpcont where prtno='"+tPrtNo+"'";
    	  String tSaleChnl = new ExeSQL().getOneValue(tSalechnlSQL);
    	  String tSql = "select 1 from ldcode where codetype='JSZJsalechnl' "
    			  	  + "and code = '"+tSaleChnl+"'";
    	  SSRS tSSRS = new ExeSQL().execSQL(tSql);
    	  String tManagecomSQL = "select managecom from lcgrpcont where prtno='"+tPrtNo+"'";
    	  String tManageCom = new ExeSQL().getOneValue(tManagecomSQL);
    	  String tSubManageCom = tManageCom.substring(0,4);
    	  String tGrpContNo = new ExeSQL().getOneValue("select grpcontno from lcgrpcont where prtno='"+tPrtNo+"'");
  	  if(!(tSSRS == null || tSSRS.MaxRow != 1) && "8632".equals(tSubManageCom)){
  		  ContInputAgentcomChkBL tDealQueryAgentInfoBL = new ContInputAgentcomChkBL();
      	  TransferData tJSTransferData = new TransferData();
      	  tJSTransferData.setNameAndValue("ContNo",tGrpContNo);
      	  VData tJSVData = new VData();
      	  tJSVData.add(tJSTransferData);
      	  if(!tDealQueryAgentInfoBL.submitData(tJSVData, "check")){
      		  Content = tDealQueryAgentInfoBL.mErrors.getError(0).errorMessage;
      		  FlagStr = "Fail";
      		  tJSFlag = true;
      	  }
  	  }
    }
    if(!tJSFlag){
    	if(!p.submitData(data, "")){
        	Content = " 保存失败! 原因是: " + p.mErrors.getFirstError();
        	FlagStr = "Fail";
    	}else{
        	Content = " 保存成功！";
        	FlagStr = "Succ";
    	}
    }
    
}
catch (Exception e)
{
    Content = " 操作失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}


System.out.println("BriGrpContInpConfirmSave.jsp End ...");
%>

<html>
    <script language="javascript">
    parent.fraInterface.afterInpConfirmSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
