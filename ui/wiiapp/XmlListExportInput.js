//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

function queryBatchList()
{
    if(!checkData()){
		return false;
	}
    var tBatchNo = fm.BatchNo.value;
    var tStrSql = ""
        + " select lbibi.BatchNo, lbibi.BranchCode, lbibi.SendOperator,lbibi.TaskCount,"
        +"(select codename from ldcode where codetype = 'wiiimportstate' and code = lbibi.ImportState),"
        +"ImportStartDate,ImportEndDate, varchar(MakeDate) || ' '|| MakeTime "
        + " from lcbrigrpimportbatchinfo lbibi "
        + " where 1 = 1 "
        + " and BatchConfState = '02'" 
        //+ getWherePart('ManageCom', 'manageCom', 'like')
        + getWherePart('importStartDate', 'importStartDate',">=")
        + getWherePart('importEndDate', 'importEndDate',"<=")
        + getWherePart('BatchNo', 'BatchNo')
        + " order by varchar(MakeDate) || ' '|| MakeTime desc"
        ;
//prompt('',tStrSql);
    turnPage1.pageDivName = "divExportBatchGridPage";
    turnPage1.queryModal(tStrSql, ExportBatchGrid);

    if (!turnPage1.strQueryResult)
    {
        alert("没有可回送的批次信息！");
        return false;
    }
    return true;
}

//下载事件
function downLoadBatch()
{
    if(!checkBatchInfo()){
    	return false;
    }
    //var i = 0;
    //var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    //var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    //showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.action = "./XmlListExportDownload.jsp?BatchNo="+ExportBatchGrid.getRowColData(ExportBatchGrid.getSelNo() - 1, 1);
    fm.submit(); //提交
}

function checkBatchInfo(){

	var tSel = ExportBatchGrid.getSelNo();
	if( tSel == null || tSel == 0 ){
		alert("请先选择回送数据的批次！");
		return false;
	}
	var tBatchNo = ExportBatchGrid.getRowColData(ExportBatchGrid.getSelNo() - 1, 1);
    if(tBatchNo == null || tBatchNo == ""){
    	alert("请先选择回送数据的批次！");
    	return false;
    }
    
    var tSql = "select * from lcbrigrpimportbatchinfo where batchno = '"+tBatchNo+"' and BatchConfState = '02'";
    var result = easyExecSql(tSql);
	if (!result)
	{
		alert("该批次未确认，请先执行业务确认操作！");
		return false;
	}
	
	return true;
}
/**
 * 导入清单提交后动作。
 */
function afterExportBatch(FlagStr, Content, cBatchNo)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "false" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    
    fm.downloadbtn.disabled = false;
}
//查询前的校验
function checkData()
{
	var tBatchNo = fm.BatchNo.value; 					//导入批次号
	var tImportStartDate = fm.all('importStartDate').value;	//导入日期起期
	var tImportEndDate = fm.all('importEndDate').value;		//导入日期止期
	
	if(tBatchNo == null || tBatchNo == ""){
		if(tImportStartDate == null || tImportStartDate == "")
		{
			alert("请填写批次或选择导入日期起期！");
			return false;
		}
	
	if(tImportEndDate == null || tImportEndDate == "")
		{
			alert("请填写批次或选择导入日期止期！");
			return false;
		}
		
		
	if(tImportStartDate != null && tImportStartDate != ""
		&& tImportEndDate != null && tImportEndDate != "")
		{
			if(dateDiff(tImportStartDate,tImportEndDate,"M")>3)
			{
				alert("导入日期统计期最多为三个月！");
				return false;
			}
		}
	}	
	return true;
}
function BatchNoShow()
{
    //fm.BatchNo.value = ExportBatchGrid.getRowColData(ExportBatchGrid.getSelNo() - 1, 1);
}
