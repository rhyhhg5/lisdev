<%
//程序名称：
//程序功能：
//创建日期：2011-02-10
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String tManageCom = tGI.ComCode;
String tOperator = tGI.Operator;
%>

<script language="JavaScript">
var tManageCom = "<%=tManageCom%>";
var tOperator = "<%=tOperator%>";

function initForm()
{
    try
    {
        initWIIPolicyListGrid();
        
        fm.all('ManageCom').value = <%=tManageCom%>;

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}


function initWIIPolicyListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="印刷号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="管理机构";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="扫描日期";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="录入日期";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="录入人";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="MissionId";
        iArray[6][1]="150px";
        iArray[6][2]=100;
        iArray[6][3]=3;
        
        iArray[7]=new Array();
        iArray[7][0]="SubMissionId";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=3;
        
        iArray[8]=new Array();
        iArray[8][0]="ProcessId";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=3;
        
        iArray[9]=new Array();
        iArray[9][0]="ActivityId";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=3;
        
        iArray[10]=new Array();
        iArray[10][0]="ActivityStatus";
        iArray[10][1]="80px";
        iArray[10][2]=100;
        iArray[10][3]=3;


        WIIPolicyListGrid = new MulLineEnter("fm", "WIIPolicyListGrid"); 

        WIIPolicyListGrid.mulLineCount = 0;   
        WIIPolicyListGrid.displayTitle = 1;
        WIIPolicyListGrid.canSel = 1;
        WIIPolicyListGrid.hiddenSubtraction = 1;
        WIIPolicyListGrid.hiddenPlus = 1;
        WIIPolicyListGrid.canChk = 0;
        WIIPolicyListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化WIIPolicyListGrid时出错：" + ex);
    }
}
</script>

