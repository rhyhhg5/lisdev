// 
var mDebug="0";
var mOperate="";
var showInfo;

var turnPage = new turnPageClass();
var succFlag = false;


// 显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{	
	
    if(cDebug=="1")
    {
        parent.fraMain.rows = "0,0,50,82,*";
    }
    else
    {
        parent.fraMain.rows = "0,0,0,82,*";
    }
}


function afterCodeSelect(cCodeName, Field)
{
	//关闭社保渠道勾选交叉销售功能（社保渠道不得选择交叉销售业务）
	if(cCodeName=="unitesalechnl"){
		fm.all("MixComFlag").style.display="";
		fm.MixComFlag.checked = false;
		if(fm.SaleChnl.value=="18"||fm.SaleChnl.value=="20"){
			fm.all("MixComFlag").style.display="none";
			fm.MixComFlag.checked = false;
		}
		isMixCom();
	}
	
	
    if(cCodeName == "SaleChnl" || cCodeName == "unitesalechnl")
    {
        var tSaleChnl = Field.value;
        displaySaleChnl(tSaleChnl);
        
        fm.AgentCode.value = "";
        fm.AgentCom.value = "";
        fm.AgentCom2.value = "";
    }
}


function displaySaleChnl(cSaleChnl)
{
    if(cSaleChnl == "03")
    {
        fm.AgentCom.style.display = "";        
        fm.AgentCom.className = "code";
        fm.AgentCom2.style.display = "none"; 
        
        var tBranchType = "2";
        var tBranchType2 = "02";
        fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
    }else if(cSaleChnl == "15")
    {
        fm.AgentCom.style.display = "";        
        fm.AgentCom.className = "code";
        fm.AgentCom2.style.display = "none"; 
        
        var tBranchType = "5";
        var tBranchType2 = "01";
        fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
    }else if(cSaleChnl == "20")
    {
        fm.AgentCom.style.display = "";        
        fm.AgentCom.className = "code";
        fm.AgentCom2.style.display = "none"; 
        
        var tBranchType = "6";
        var tBranchType2 = "02";
        fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
    }else if(cSaleChnl == "10")
    {
        fm.AgentCom.style.display = "";        
        fm.AgentCom.className = "code";
        fm.AgentCom2.style.display = "none"; 
        
        var tBranchType = "#1";
        var tBranchType2="02#,#04";
        fm.AgentVar.value = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
    }else if(cSaleChnl == "04")
    {
        fm.AgentCom.style.display = "none";        
        fm.AgentCom.className = "code";
        fm.AgentCom2.style.display = "";
        fm.AgentCom2.value = fm.AgentCom.value;
    }
    else
    {
        fm.AgentCom.style.display = "none";
        fm.AgentCom2.style.display = "none";         
    }
}


function displayConfirmDiv()
{
    var tPrtNo = fm.PrtNo.value;

    var tStrSql = ""
        + " select "
        + " (case when lwm.MissionProp20 = '1' then '1' else '0' end) tConfirmFlag "
        + " from LWMission lwm "
        + " where 1 = 1 "
        + " and lwm.ProcessId = '0000000012' "
        + " and lwm.ActivityId In ('0000012001') "
        + " and lwm.MissionProp1 = '" + tPrtNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        return ;
    }
    
    var tConfirmFlag = tArrResult[0][0];
    if(tConfirmFlag == '1')
    {
        // 显示复核
        fm.btnCreateGrpCont.value = " 修  改 ";
    }
    else
    {
        // 显示录入
        fm.btnCreateGrpCont.value = " 保  存 ";
    }
}


function goNext()
{
    var tGrpContNo = fm.GrpContNo.value;

    var tStrSql = ""
        + " select "
        + " 1 "
        + " from LCGrpCont lgc "
        + " where 1 = 1 "
        + " and lgc.GrpContNo = '" + tGrpContNo + "' "
        ;

    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        alert("请先录入保单信息。");
        return false;
    }
    
    var tPrtNo = fm.PrtNo.value;
    var tGrpContNo = fm.GrpContNo.value;
    var tMissionId = fm.MissionID.value;
    var tSubMissionId = fm.SubMissionID.value;
    var tProcessId = fm.ProcessID.value;
    var tActivityId = fm.ActivityID.value;
    var tActivityStatus = fm.ActivityStatus.value;
    var tManageCom = fm.ManageCom.value;
    var tQueryFlag = fm.QueryFlag.value;
    
    var tStrUrl = "./BriGrpPolInput.jsp"
        + "?PrtNo=" + tPrtNo
        + "&GrpContNo=" + tGrpContNo
        + "&MissionID=" + tMissionId
        + "&SubMissionID=" + tSubMissionId
        + "&ProcessID=" + tProcessId
        + "&ActivityID=" + tActivityId
        + "&ActivityStatus=" + tActivityStatus
        + "&ManageCom=" + tManageCom
        + "&QueryFlag=" + tQueryFlag
        ;

    window.location = tStrUrl;
}

//选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
    	if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value == null)
    	{
    		alert("选择交叉销售时，对方机构名称不能为空，请核查！");
    		fm.all('GrpAgentComName').focus();
    		return false;
    	}
    	*/
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}    
    // --------------------
/**
 * 创建保单数据
 */
function saveSGrpContInfo()
{
	
	//校验投保人地址
	if (!CheckAddress()) {
		return false;
	}
	
	//选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
  if(!checkSaleChnlInfo()){
   	return false;
   }
  
  if(!ExtendCheck())
	{
		return false;
	}
    if(!verifyInput2())
    {
        return false;
    }
    
  //2016-05-09 赵庆涛  #2810 
    //统一社会信用代码”为非必录项，但必须18位，数字和英文字母组合。
//    if(!checkUnifiedSocialCreditNo()){
//      return false;
//    }
//    
    
    //2017-02-08 赵庆涛
//  社保渠道录单时（包含社保综拓），如勾选交叉销售，渠道类型中关闭相互代理及联合展业选项
    if(!checkCrs_SaleChnl()){
    	return false;
    }
    
    //2017-07-04 赵庆涛
//  纳税人信息录入校验
    if(!checkTaxpayer()){
    	return false;
    }
    
    if(!checkPhone(fm.Phone1.value)){
    	return false;
    }
    //by baixiaofei 2018.07.30
    //检测检测组织机构代码和统一社会信用代码是否至少有一个填写
    if(!checkCreditNo()){
    	return false;
    }
//  by baixiaofei 20180803 
//  检测录入手机号码是否合规
    if(!checkMobile()){
    	return false;
    }

    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    var tUrl = "./BriGrpContInputSave.jsp";
    fm.action = tUrl;
    fm.submit();
    fm.action = "";
    
    fm.btnCreateGrpCont.disabled = true;
    
    return true;
}


function afterSubmit(FlagStr, content)
{
    showInfo.close();
    window.focus();

    if (FlagStr == "Fail")
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
        showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    }
    else
    {
        var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //showDiv(operateButton,"true");
        //showDiv(inputButton,"false");
        //执行下一步操作
        qryGrpPolicy();
    }
    
    fm.btnCreateGrpCont.disabled = false;
}


function qryGrpPolicy()
{
    qryGrpContInfo();
    qryGrpAppntInfo();
    
    var tSaleChnl = fm.SaleChnl.value;
    displaySaleChnl(tSaleChnl);
}


/**
 * 保单信息查询
 */
function qryGrpContInfo()
{
    var tPrtNo = fm.PrtNo.value;
    
    var tStrSql = ""
        + " select "
        + " lgc.GrpContNo, lgc.PrtNo, lgc.Managecom, lgc.SaleChnl, lgc.MarketType, "
        + " lgc.AgentCom, lgc.AgentCode, laa.Name, "
        + " lgc.PolApplyDate, lgc.HandlerName, lgc.FirstTrialOperator, "
        + " lgc.Peoples3, lgc.OnWorkPeoples, lgc.OffWorkPeoples, lgc.OtherPeoples, "
        + " lgc.PayMode, lgc.PayIntv, lgc.PremScope, "
        + " lgc.CValidate, lgc.CInValidate, lgc.Remark,getUniteCode(lgc.AgentCode) "
        + " from LCGrpCont lgc "
        + " inner join LAAgent laa on laa.AgentCode = lgc.AgentCode "
        + " where 1 = 1 "
        + " and lgc.PrtNo = '" + tPrtNo + "' "
        ;
    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        return ;
    }
    
    fm.GrpContNo.value = tArrResult[0][0];
    //fm.PrtNo.value = tArrResult[0][1];
    fm.ManageCom.value = tArrResult[0][2];
    fm.SaleChnl.value = tArrResult[0][3];
    fm.MarketType.value = tArrResult[0][4];
    fm.AgentCom.value = tArrResult[0][5];
    fm.AgentCode.value = tArrResult[0][6];
    fm.AgentName.value = tArrResult[0][7];
    fm.PolApplyDate.value = tArrResult[0][8];
    fm.HandlerName.value = tArrResult[0][9];
    fm.FirstTrialOperator.value = tArrResult[0][10];
    
    fm.Peoples3.value = tArrResult[0][11];
    fm.OnWorkPeoples.value = tArrResult[0][12];
    fm.OffWorkPeoples.value = tArrResult[0][13];
    fm.OtherPeoples.value = tArrResult[0][14];
    
    fm.PayMode.value = tArrResult[0][15];
    fm.PayIntv.value = tArrResult[0][16];
    fm.PremScope.value = tArrResult[0][17];
    
    fm.CValiDate.value = tArrResult[0][18];
    fm.CInValiDate.value = tArrResult[0][19];    
    fm.GroupAgentCode.value = tArrResult[0][21]; 
    document.getElementById("Remark").innerText = tArrResult[0][20];

    var yj = easyExecSql("select codename('crs_busstype',Crs_BussType) from lcgrpcont where 1=1 and PrtNo = '" + fm.PrtNo.value + "' ");
    try{fm.all('Crs_BussTypeName').value = yj[0][0]; } catch(ex) { };
}


function qryGrpAppntInfo()
{
    var tPrtNo = fm.PrtNo.value;
    
    var tStrSql = ""
        + " select "
        + " lga.CustomerNo, lga.Name, lga.OrgancomCode, lga.OtherCertificates, "
        + " lgad.GrpAddress, lgad.GrpZipCode, "
        + " lgad.LinkMan1, lgad.Phone1,lgad.E_Mail1,lga.TaxpayerType,lga.TaxNo,lga.CustomerBankCode,lga.CustomerBankAccNo,lga.UnifiedSocialCreditNo,lgad.postalprovince,lgad.postalcity,lgad.DetailAddress,lgad.postalcounty , lgad.Mobile1  "
        + " from LCGrpAppnt lga "
        + " inner join LCGrpAddress lgad on lgad.CustomerNo = lga.CustomerNo and lgad.AddressNo = lga.AddressNo "
        + " where 1 = 1 "
        + " and lga.PrtNo = '" + tPrtNo + "' "
        ;
    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        return ;
    }
    
    fm.CustomerNo.value = tArrResult[0][0];
    fm.GrpName.value = tArrResult[0][1];
    fm.OrgancomCode.value = tArrResult[0][2];
    fm.OtherCertificates.value = tArrResult[0][3];
    fm.GrpAddress.value = tArrResult[0][4];
    fm.GrpZipCode.value = tArrResult[0][5];
    fm.LinkMan1.value = tArrResult[0][6];
    fm.Phone1.value = tArrResult[0][7];    
    fm.E_Mail1.value = tArrResult[0][8]; 
    fm.TaxpayerType.value = tArrResult[0][9]; 
    fm.TaxNo.value = tArrResult[0][10]; 
    fm.CustomerBankCode.value = tArrResult[0][11]; 
    fm.CustomerBankAccNo.value = tArrResult[0][12]; 
    fm.UnifiedSocialCreditNo.value = tArrResult[0][13];
    fm.ProvinceID.value = tArrResult[0][14];
    fm.CityID.value = tArrResult[0][15];
    fm.DetailAddress.value = tArrResult[0][16];
    fm.CountyID.value = tArrResult[0][17];
	fm.Mobile1.value = tArrResult[0][18];
	//市的反显
	var provinceID = fm.ProvinceID.value;
	if ( provinceID != "") {
		var sql2 = "select codename from ldcode1 where codetype='city1' and code = '" + fm.CityID.value  + "'";
		var tarrResult2 = easyExecSql(sql2);
		if (tarrResult2 != null){
			fm.City.value = tarrResult2[0][0];
		}
	}
    //县的反显
	var cityID = fm.CityID.value;
	if ( cityID != "") {
		var sql2 = "select codename from ldcode1 where codetype='county1' and code = '" + fm.CountyID.value  + "'";
		var tarrResult2 = easyExecSql(sql2);
		if (tarrResult2 != null){
			fm.County.value = tarrResult2[0][0];
		}
	}
}


function qryGrpInfo(cCustomerNo)
{
    var tStrSql = ""
        + " select "
        + " ldg.CustomerNo, ldg.GrpName, ldg.OrgancomCode, ldg.OtherCertificates, "
        + " TmpAddrInfo.GrpAddress, TmpAddrInfo.GrpZipCode, "
        + " TmpAddrInfo.LinkMan1, TmpAddrInfo.Phone1,TmpAddrInfo.E_Mail1,ldg.TaxpayerType,ldg.TaxNo,ldg.CustomerBankCode,ldg.CustomerBankAccNo,ldg.UnifiedSocialCreditNo, "
        + " ''  "
        + " from LDGrp ldg "
        + " left join "
        + " ( "
        + " select "
        + " lgad.* "
        + " from LCGrpAddress lgad "
        + " inner join "
        + " ( "
        + " select "
        + " slgad.CustomerNo, max(integer(slgad.AddressNo)) AddressNo "
        + " from LCGrpAddress slgad "
        + " where 1 = 1 "
        + " and slgad.CustomerNo = '" + cCustomerNo + "' "
        + " group by slgad.CustomerNo "
        + " ) as AddrInfo"
        + " on AddrInfo.CustomerNo = lgad.CustomerNo and AddrInfo.AddressNo = integer(lgad.AddressNo) "
        + " ) as TmpAddrInfo "
        + " on ldg.CustomerNo = TmpAddrInfo.CustomerNo "
        + " where 1 = 1 "
        + " and ldg.CustomerNo = '" + cCustomerNo + "' "
        ;
    var tArrResult = easyExecSql(tStrSql);
    if(!tArrResult)
    {
        cleanGrpAppntInfo();
        return ;
    }
    
    fm.CustomerNo.value = tArrResult[0][0];
    fm.GrpName.value = tArrResult[0][1];
    fm.OrgancomCode.value = tArrResult[0][2];
    fm.OtherCertificates.value = tArrResult[0][3];
    fm.GrpAddress.value = tArrResult[0][4];
    fm.GrpZipCode.value = tArrResult[0][5];
    fm.LinkMan1.value = tArrResult[0][6];
    fm.Phone1.value = tArrResult[0][7];
    fm.E_Mail1.value = tArrResult[0][8]; 
    fm.TaxpayerType.value = tArrResult[0][9]; 
    fm.TaxNo.value = tArrResult[0][10]; 
    fm.CustomerBankCode.value = tArrResult[0][11]; 
    fm.CustomerBankAccNo.value = tArrResult[0][12]; 
    fm.UnifiedSocialCreditNo.value = tArrResult[0][13];
}


/**
 * 查询业务员信息
 */
function qryAgentList()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        return ;
    }
    if(fm.all('SaleChnl').value == "")
    {
        alert("请先录入售出渠道信息！");
        return ;
    }
    var saleChnl = (fm.SaleChnl != null && fm.SaleChnl != "undefined") ? fm.SaleChnl.value : "";
    var agentCom = (fm.AgentCom != null && fm.AgentCom != "undefined") ? fm.AgentCom.value : "";

    // 交叉渠道，个险直销人员可以销售团险产品。
    var branchType = 2;
    if(saleChnl == '06')
        branchType = 1;
    
    var strURL = "../sys/AgentCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
        + "&SaleChnl=" + saleChnl + "&AgentCom=" + agentCom + "&branchtype=" + branchType;
    var newWindow = window.open(strURL, "AgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
}


/**
 * 查询业务员信息后反馈函数
 */
function afterQuery2(arrResult)
{
    if(arrResult != null)
    {
        fm.AgentCode.value = arrResult[0][0];
        fm.AgentName.value = arrResult[0][5];
        fm.GroupAgentCode.value = arrResult[0][95];
    }
}


/**
 * 投保公司的查询
 */
function qryGrpInfoList()
{
    var tGrpName = fm.GrpName.value;
    if(tGrpName == null || tGrpName == "")
    {
        return ;
    }
    
    var tStrSql = "select 1 from LDGrp where GrpName like '%" + tGrpName + "%'";
    
    var tArrResult = easyExecSql(tStrSql);
    if(tArrResult != null)
    {
        var tUrl = ""
            + "../app/FrameMainGrpAppntQuery.jsp"
            + "?Interface=GrpAppntQuery.jsp"
            + "&Grpname=" + tGrpName
            ;
        showinfo = window.open(tUrl, "RgtSurveyInput", "width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0");
    }
}


function qryGrpInfoView()
{
    var tCustomerNo = fm.CustomerNo.value;
    var tGrpName = ""; // 客户号查询时，对单位名称不做过滤
    
    var tUrl = ""
        + "../app/FrameMainGrpAppntQuery.jsp"
        + "?Interface=GrpAppntQuery.jsp"
        + "&Grpname=" + tGrpName
        + "&CustomerNo=" + tCustomerNo
        ;
    showinfo = window.open(tUrl, "RgtSurveyInput", "width=800,height=550,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0");
}


function afterAppntQuery(arrReturn)
{
    var tGrpCustomerno = arrReturn[0][0];
    qryGrpInfo(tGrpCustomerno);
}


function cleanGrpAppntInfo()
{
    fm.GrpName.value = "";
    fm.OrgancomCode.value = "";
    fm.OtherCertificates.value = "";
    fm.GrpAddress.value = "";
    fm.GrpZipCode.value = "";
    fm.LinkMan1.value = "";
    fm.Phone1.value = "";
    fm.E_Mail1.value = "";
    fm.TaxpayerType.value = "";
    fm.TaxpayerNo.value = ""; 
    fm.CustomerBankCode.value = "";
    fm.CustomerBankAccNo.value = "";

}
//显示或者综合开拓
function isExtend()
{
    if(fm.ExtendFlag.checked == true)
    {
        fm.all('ExtendID').style.display = "";
        fm.all('AssistSaleChnl').value ="";
        fm.all('AssistSaleChnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value ="";
    }
    if(fm.ExtendFlag.checked == false)
    {
        fm.all('AssistSaleChnl').value ="";
        fm.all('AssistSaleChnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value ="";
        fm.all('ExtendID').style.display = "none";
    }
}
//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryAssistAgent()
{
    if(fm.all('AssistSaleChnl').value == "" || fm.all('AssistSaleChnl').value == null)
    {
    	alert("请先选择协助销售渠道！");
    	return false;
    }
    if(fm.all('AssistAgentCode').value == "" )
    {  
        var tAssistSaleChnl = (fm.AssistSaleChnl != null && fm.AssistSaleChnl != "undefined") ? fm.AssistSaleChnl.value : "";
        var strURL = "../sys/AssistAgentCommonQueryMain.jsp?AssistSaleChnl=" + tAssistSaleChnl;        
        //alert(strURL);
        var newWindow = window.open(strURL, "AssistAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('AssistAgentCode').value != "")
    {	
        var strGrpSql = "select agentcode,name from LAAgent where agentcode ='" + fm.all('AssistAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery6(arrResult);
            //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {            
            alert("代码为:[" +  fm.AssistAgentCode.value + "]的业务员不存在，请确认!");
            //fm.GrpAgentCode.value="";
        }
    }
}
function afterQuery6(arrResult){
  if(arrResult!=null) {
    fm.AssistAgentCode.value = arrResult[0][0];
    fm.AssistAgentName.value = arrResult[0][1];
  }
}
function ExtendCheck()
{    
    if(fm.ExtendFlag.checked == true)
    {
    	if(fm.AssistSaleChnl.value == "" || fm.AssistSaleChnl.value == null)
    	{
    		alert("选择综合开拓时，协助销售渠道不能为空，请核查！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistAgentCode.value == "" || fm.AssistAgentCode.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员代不能为空，请核查！");
    		fm.all('AssistAgentCode').focus();
    		return false;
    	}
    	if(fm.AssistAgentName.value == "" || fm.AssistAgentName.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员姓名不能为空，请核查！");
    		fm.all('AssistAgentName').focus();
    		return false;
    	}
    	var tSQL = "select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"+fm.AssistSaleChnl.value+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("销售渠道与业务员编码不匹配！");
			return false;
		}
		var tSQL1 = "select 1 from laagent where agentcode = '"+fm.AssistAgentCode.value+"' and BranchType = '"+arrResult[0][0]+"' and BranchType2 = '"+arrResult[0][1]+"'";
		var arrResult1 = easyExecSql(tSQL1);
		if(!arrResult1){
			alert("销售渠道与业务员编码不匹配，请核查！");
			return false;
		}
      	return true;
    }else{
    	return true;
    }
}

//by baixiaofei 20180730 
//检测组织机构代码和统一社会信用代码是否至少有一个填写
function checkCreditNo(){
	var tOrgancomCode = fm.OrgancomCode.value;
	var tUnifiedSocialCreditNo = fm.UnifiedSocialCreditNo.value;
	if(tOrgancomCode=="" && tUnifiedSocialCreditNo==""){
		alert("组织机构代码和统一社会信用代码至少填写一项！");
		return false;
	}
	return true;
}
//by baixiaofei 20180803 
//检测录入手机号码是否合规
function checkMobile(){
	var tMobile1=fm.Mobile1.value;
	if(tMobile1!=""){
	if(tMobile1.length!=11){
		alert("手机号必须为11位数字，请核实！");
		return false;
	}
	if(tMobile1.substring(0,2)!=13&&tMobile1.substring(0,2)!=14&&tMobile1.substring(0,2)!=15&&tMobile1.substring(0,2)!=16&&tMobile1.substring(0,2)!=18&&tMobile1.substring(0,2)!=17&&tMobile1.substring(0,2)!=19){
        alert("手机号不符合规则，请核实！");
        return false;
     }
	return true;
}
	return true;
}
//function checkUnifiedSocialCreditNo(){
//	var tUnifiedSocialCreditNo = fm.UnifiedSocialCreditNo.value;
//	var b = /^[0-9a-zA-Z]*$/g;
//	if(b.test(tUnifiedSocialCreditNo)){
//		return true;
//	}else {
//		alert("统一社会信用代码只能是数字和英文字母组合！");
//		return false;
//	}
//}

function checkCrs_SaleChnl(){
	var tCrs_BussType=fm.Crs_BussType.value;
	var tSaleChnl=fm.SaleChnl.value;
	if((tCrs_BussType=="01"||tCrs_BussType=="02")&&(tSaleChnl=="16"||tSaleChnl=="18")){
		alert("社保直销及社保综拓渠道出单若选择交叉销售，交叉销售业务类型不能为相互代理或联合展业！");
		return false;
	}
	return true;
}

function checkTaxpayer(){
	var tTaxpayerType = fm.TaxpayerType.value;
	var tTaxNo = fm.TaxNo.value;
	var tUnifiedSocialCreditNo = fm.UnifiedSocialCreditNo.value;
	var tOrgancomCode = fm.OrgancomCode.value;
	var tCustomerBankCode = fm.CustomerBankCode.value;
	var tCustomerBankAccNo = fm.CustomerBankAccNo.value;
	var tE_Mail1 = fm.E_Mail1.value;
	if(tTaxpayerType  == "" ){
		if(tCustomerBankCode !=　""
			||tCustomerBankAccNo !=　""){
			alert("纳税人类型为空时，客户开户银行、 客户银行账户不可录入！");
			return false;
		}
	}else if(tTaxpayerType == "1"){
		if((tTaxNo =="" && tUnifiedSocialCreditNo =="" && tOrgancomCode =="")
			||tCustomerBankCode ==　""
			||tCustomerBankAccNo ==　""
			||tE_Mail1 ==　""){
			alert("当纳税人类型为“一般纳税人”时，税务登记证号码、统一社会信用代码 、组织机构代码三项必录其一，客户开户银行、 客户银行账户、电子邮箱为必录项！");
			return false;
		}
	}else if(tTaxpayerType == "2"){
		if(tTaxNo =="" && tUnifiedSocialCreditNo =="" && tOrgancomCode == ""){
			alert("当纳税人类型为“小规模纳税人”时，税务登记证号码、统一社会信用代码 、组织机构代码三项必录其一！");
			return false;
		}
	}
	if(tTaxNo != ""){
		var a = /^[0-9a-zA-Z]*$/g;
		var tlen = tTaxNo.length;
		if(!a.test(tTaxNo)){
			alert("税务登记证号码只允许数字和字母！");
			return false;
		}
		if(tlen != 15 && tlen != 18 && tlen != 20
				){
			alert("税务登记证号码允许的位数为15、18和20位");
			return false;
		}
	}
	if(tCustomerBankCode != ""){
		var tSQL = "select 1 from ldbank where bankcode = '"+tCustomerBankCode+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("请选择或输入正确的客户开户银行！");
			return false;
		}
	}
	return true;
}

function initExtend(){
	var tSql = "select AssistSalechnl,AssistAgentCode from LCExtend where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.ExtendFlag.checked = true;
			fm.all('ExtendID').style.display = "";
			fm.AssistSaleChnl.value = arrResult[0][0];
			fm.AssistAgentCode.value = arrResult[0][1];
			var  tSql1 = "select name from LAAgent where agentcode ='" + arrResult[0][1] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.AssistAgentName.value = arrResult1[0][0];
		}
	}
}

function checkSaleChnlInfo(){
	var tManageCom = fm.ManageCom.value;
	var tAgentCom = fm.AgentCom.value;
	var tAgentCode = fm.AgentCode.value;
	var tSaleChnl = fm.SaleChnl.value;
	var tMarketType = fm.MarketType.value;
	
	var tSQLCode = "select 1 from laagent where agentcode = '"+tAgentCode+"' and managecom = '"+tManageCom+"' ";
	var arrCode = easyExecSql(tSQLCode);
	if(!arrCode){
		alert("业务员与管理机构不匹配！");
		return false;
	}
	var agentCodeSql = " select 1 from LAAgent a, LDCode1 b where a.AgentCode = '"+ tAgentCode+ "'" 
                 	+ " and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
                    + " and b.CodeType = 'salechnl' and b.Code = '"+ tSaleChnl + "' "
                    + " union all "
					+ " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					+ " and '03' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  "
					+ " union all "
					+ " select 1 from LAAgent a where a.AgentCode = '" + tAgentCode + "' "
					+ " and '10' = '" + tSaleChnl + "' and a.BranchType2 in ('04','03')  ";
    var arrAgentCode = easyExecSql(agentCodeSql);
    if(!arrAgentCode){
    	alert("业务员和销售渠道不匹配！");
		return false;
    }
    var tcheckSQL = "select 1 from ldcode1 where codetype = 'markettypesalechnl' and code = '"+tMarketType+"' and code1 = '"+tSaleChnl+"' ";
	var arrCheck = easyExecSql(tcheckSQL);
	if(!arrCheck){
		alert("市场类型和销售渠道不匹配，请核查！");
		return false;
	}
	if(tSaleChnl == "03" || tSaleChnl == "04" || tSaleChnl == "10" || tSaleChnl == "15" || tSaleChnl == "20"){
		var tSQLCom = "select 1 from lacom where agentcom = '"+tAgentCom+"' and managecom = '"+tManageCom+"' ";
		var arrCom = easyExecSql(tSQLCom);
		if(!arrCom){
			alert("中介机构与管理机构不匹配！");
			return false;
		}
		var tSQLComCode = "select 1 from lacomtoagent where agentcode = '"+tAgentCode+"' and agentcom = '"+tAgentCom+"' ";
		var arrCom = easyExecSql(tSQLComCode);
		if(!arrCom){
			alert("业务员与中介机构不匹配！");
			return false;
		}
	}
	return true;
}


//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value = "";
        fm.all('Crs_SaleChnlName').value = "";
        fm.all('Crs_BussType').value = "";
        fm.all('Crs_BussTypeName').value = "";
        fm.all('GrpAgentCom').value = "";
        fm.all('GrpAgentComName').value = "";
        fm.all('GrpAgentCode').value = "";
        fm.all('GrpAgentName').value = "";
        fm.all('GrpAgentIDNo').value = "";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}
//双击对方业务员框要显示的页面
function otherSalesInfo(){
	//alert("双击对方业务员代码");
	window.open("../sys/MixedSalesAgentMain.jsp?ManageCom="+fm.ManageCom.value);
}
//对方业务员页面查询完“返回”按钮，接收数据
function afterQueryMIX(arrResult){
	if(arrResult!=null){
		var arr = arrResult.split("#");
		fm.GrpAgentCom.value = arr[0]; //对方机构代码
		//fm.GrpAgentComName.value = arr[1]; //对方机构名称
		fm.GrpAgentCode.value = arr[2]; //对方业务员代码
		fm.GrpAgentName.value = arr[3]; //对方业务员姓名
		fm.GrpAgentIDNo.value = arr[4]; //对方业务员身份证
		
	}else{
		alert("返回过程出现异常，请重新操作");
	}	
}


//省，市，县详细地址填完输入框失去焦点的时候自动在上面的“投保人地址（联系地址）”填充数据
function FillAddress(){
	var province = fm.Province.value;
	var city = fm.City.value;
	var county = fm.County.value;
	var detailAddress = fm.DetailAddress.value;
	if (fm.CityID.value == "000000") {
		fm.GrpAddress.value = province + detailAddress;
	}else if(fm.CountyID.value == "000000") {
		fm.GrpAddress.value = province + city + detailAddress;
	}else{
		fm.GrpAddress.value = province + city + county + detailAddress;
	}
}

//保存时校验联系人地址
function CheckAddress(){
	//校验判断联系人地址是否为空
 		if (fm.Province.value=="" || fm.City.value=="" || fm.County.value=="" || fm.DetailAddress.value=="") {
 			alert("团体基本资料中的省，市，县和详细地址不能为空，请核实！");
 			return false;
 		}
 		
 		//校验省和市是否与匹配
 		var provinceID = fm.ProvinceID.value;
 		var cityID = fm.CityID.value;
 		var countyID = fm.CountyID.value;
 		var strSql = "select code from ldcode1 where codetype='province1' and code = '" + provinceID + "'";
 		var strSql2 = "select code1 from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
 		var strSql3 = "select code from ldcode1 where codetype='city1' and code = '" + cityID  + "'";
 		var strSql4 = "select code1 from ldcode1 where codetype='county1' and code = '" + countyID  + "'";
 		var arrResult = easyExecSql(strSql);
 		var arrResult2 = easyExecSql(strSql2);
 		var arrResult3 = easyExecSql(strSql3);
 		var arrResult4 = easyExecSql(strSql4);
 		
 		if (provinceID=="710000" || provinceID=="810000" || provinceID=="820000"||provinceID=="990000") {
 			if(cityID != "000000"){
 	 			alert("省和市不匹配，请核实！");
 	 			return false;
 			}
 			if(countyID != "000000"){
 				alert("市和县不匹配，请核实！");
 	 			return false;
 			}

 		}else if ((provinceID=="620000"&&cityID=="620200") || (provinceID=="440000"&&(cityID=="442000"||cityID=="441900")) ) {
			if(countyID != "000000"){
				alert("市和县不匹配，请核实！");
	 			return false;
			}
		}else{
 	 		if (arrResult[0][0] != arrResult2[0][0]) {
 	 			alert("省和市不匹配，请核实！");
 	 			return false;
 	 		}
 	 		if (arrResult3[0][0] != arrResult4[0][0]) {
 	 			alert("市和县不匹配，请核实！");
 	 			return false;
 	 		}
 		}
 		return true;

}

//对页面录入的联系电话进行校验
function checkPhone(pho){
	var str = "";
	if(pho!=null && pho!=""){
		if(pho.charAt(0)=="1"){
			str=CheckPhone(pho);
		}else{
			str=CheckFixPhone(pho);
		}
		if(str!=""){
			str="联系电话(移动电话)若为手机号，则必须为11位数字；若为固定电话，则仅允许包含数字、括号和“-”！";
			alert(str);
			return false;
		}else{
			return true;
		}
	}
	return true;
}