<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
%>
<html>
<script>
    var PrtNo = "<%=request.getParameter("PrtNo")%>";
    var GrpContNo = "<%=request.getParameter("GrpContNo")%>";
    var scantype = "<%=request.getParameter("scantype")%>";
    
    var MissionID = "<%=request.getParameter("MissionID")%>";
    var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
    var ProcessID = "<%=request.getParameter("ProcessID")%>";
    var ActivityID = "<%=request.getParameter("ActivityID")%>";
    var ActivityStatus = "<%=request.getParameter("ActivityStatus")%>";
    var QueryFlag = "<%=request.getParameter("QueryFlag")%>";
    
    var ManageCom = "<%=request.getParameter("ManageCom")%>";
    var PolApplyDate = "<%=request.getParameter("PolApplyDate")%>";
    var LoadFlag = "<%=request.getParameter("LoadFlag")%>";
    var CurrentDate = "<%=PubFun.getCurrentDate()%>";
</script>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    
    <script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    <script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    <script src="../common/javascript/Common.js"></script>
    <script src="../common/cvar/CCodeOperate.js"></script>
    <script src="../common/javascript/MulLine.js"></script>
    <script src="../common/Calendar/Calendar.js"></script>
    <script src="../common/javascript/VerifyInput.js"></script>

    <link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    <link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    <%@include file="BriGrpPolInputInit.jsp"%>
    <script src="BriGrpPolInput.js"></script>
</head>

<body  onload="initForm();initElementtype();" >
    <form action="./SGrpContInputSave.jsp" method="post" name="fm" target="fraSubmit">
        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">保障情况</td>
            </tr>
        </table>
        
        <div id="divPolicyCode" style="display:''">
            <table class="common">
                <tr class="common">
                    <td class="title">印刷号</td>
                    <td class="input">
                        <input class="readonly" name="PrtNo" readonly="readonly" />
                    </td>
                    <td class="title">管理机构</td>
                    <td class="input">
                        <input class="readonly" name="ManageCom" readonly="readonly" />
                    </td>
                    <td class="title">销售渠道</td>
                    <td class="input">
                        <input class="readonly" name="SaleChnl" readonly="readonly" />
                    </td>
                </tr>
                <tr class="common">
                    <td class="title">责任生效日期</td>
                    <td class="input">
                        <input class="readonly" name="CValidate" readonly="readonly" />
                    </td>
                    <td class="title">责任终止日期</td>
                    <td class="input">
                        <input class="readonly" name="CInValidate" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
                <tr class="common">
                    <td class="title">实保人数</td>
                    <td class="input">
                        <input class="readonly" name="SumInsuCount" readonly="readonly" />
                    </td>
                    <td class="title">总保费</td>
                    <td class="input">
                        <input class="readonly" name="SumInsuPrem" readonly="readonly" />
                    </td>
                    <td class="title">&nbsp;</td>
                    <td class="input">&nbsp;</td>
                </tr>
            </table>
        </div>
        
        <hr />
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">套餐信息</td>
            </tr>
        </table>
        <div id="divWrapListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanWrapListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divWrapListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" />
                <input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />
                <input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" />
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />
            </div>
        </div>
        
        <br />

        <table>
            <tr>
                <td class="common">
                    <img src="../common/images/butExpand.gif" style="cursor:hand;" />
                </td>
                <td class="titleImg">责任要素信息</td>
            </tr>
        </table>
        <div id="divWrapDutyParamListGrid" style="display: ''">
            <table class="common">
                <tr class="common">
                    <td>
                        <span id="spanWrapDutyParamListGrid"></span> 
                    </td>
                </tr>
            </table>
            
            <div id="divWrapDutyParamListGridPage" style="display: ''" align="center">
                <input type="button" class="cssButton" value="首  页" onclick="turnPage3.firstPage();" />
                <input type="button" class="cssButton" value="上一页" onclick="turnPage3.previousPage();" />
                <input type="button" class="cssButton" value="下一页" onclick="turnPage3.nextPage();" />
                <input type="button" class="cssButton" value="尾  页" onclick="turnPage3.lastPage();" />
            </div>
        </div>
        
        <hr />
        
        <br />
        
        <table align="right">
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnInpConfirm" name="btnInpConfirm" value="  录入完毕  " onclick="inpConfirm();" disabled="false" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnChkConfirm" name="btnChkConfirm" value="  复核完毕  " onclick="chkConfirm();" disabled="false" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnGoBack" name="btnGoBack" value="   上一步   " onclick="goToBack();" />
                </td>
            </tr>
        </table>
        
        <table align="left">
            <tr>
                <td class="common">
                    <input class="cssButton" type="button" id="btnInsuList" name="btnInsuList" value=" 被保人清单 " onclick="goToInsuList();" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnCreateWrapPlan" name="btnCreateWrapPlan" value="   保  存   " onclick="saveWrapPlanInfo();" />   
                </td>
                <td class="common">
                    <input class="cssButton" type="button" id="btnEditGrpCont" name="btnEditGrpCont" value=" 修  改 " onclick="alert();" disabled="disabled" style="display: none;" />
                </td>
                <td calss="common">
                	<input class="cssButton" type="button" id="btnQueryInsuList" name="btnQueryInsuList" value=" 查看已导入被保人清单 " onclick="qryInsuImportList();" style="display: none" />
                </td>
            </tr>
        </table>
        
        <input type="hidden" id="GrpContNo" name="GrpContNo" />
        <input type="hidden" id="AgentVar" name="AgentVar" />

        <input type="hidden" id="ProcessID" name="ProcessID" />
        <input type="hidden" id="MissionID" name="MissionID" />
        <input type="hidden" id="SubMissionID" name="SubMissionID" />
        <input type="hidden" id="ActivityID" name="ActivityID" />
        <input type="hidden" id="ActivityStatus" name="ActivityStatus" />
        <input type="hidden" id="QueryFlag" name="QueryFlag" />

    </form>
    <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>