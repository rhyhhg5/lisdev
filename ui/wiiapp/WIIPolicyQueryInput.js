//程序名称：
//程序功能：
//创建日期：2011-08-09
//创建人  ：zhangyang
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


/**
 * 查询已申请结算单。
 */
function queryWIIPolicyList()
{
    if(!verifyInput2())
    {
        return false;
    }
    
    if(!checkDate())
    {
    	return false;
    }
    
    var tStrSql = ""
        + " select "
        + " lwm.MissionProp1, lwm.MissionProp2, '', '', '', "
        + " lwm.MissionId, lwm.SubMissionId, lwm.ProcessId, lwm.ActivityId, lwm.ActivityStatus, "
        + " '新单' AppFlag, edm.ArchiveNo, '', '', '', "
        + " '', '', (case when edm.DocId is null then '无扫描件' else '有扫描件' end) "
        + " ,'',"
		+ " (select distinct confmakedate from ljtempfee where otherno = lwm.MissionProp1),"
		+ " (select signdate from lcgrpcont where prtno = lwm.MissionProp1),"
		+ " '','','','','','' "
        + " from LWMission lwm "
        + " left join Es_Doc_Main edm on edm.DocCode = lwm.MissionProp1 and edm.SubType in ('TB08', 'TB09') "
        + " where 1 = 1 "
        + " and lwm.ProcessId = '0000000012' "
        + " and lwm.ActivityId In ('0000012001') "
        + " and not exists(select 1 from lcgrpcont where PrtNo = lwm.MissionProp1) "
        + getWherePart("lwm.MissionProp1", "PrtNo")
        + getWherePart("lwm.MissionProp2", "ManageCom",'like')
        + getWherePart("''", "AgentCode")
        + getWherePart("''", "AgentGroup")
        + getWherePart("''", "StartPolApplyDate")
        + getWherePart("''", "EndPolApplyDate")
        + " union all "
        + " select "
        + " lgc.PrtNo, lgc.ManageCom, varchar(lgc.CValiDate), varchar(lgc.InputDate), lgc.InputOperator, "
        + " '', '', '', '', '',"
        + " CodeName('appflag', lgc.AppFlag) AppFlag, edm.ArchiveNo, lgc.GrpName, getUniteCode(lgc.AgentCode), varchar(lgc.Prem), "
        + " (select Max(BatchNo) from LCBriGrpImportDetail where BussNo = lgc.PrtNo and ImportState = '02'), "
        + " char((select peoples2 from lcgrpcont where prtno = lgc.PrtNo)), "
        + " (case when edm.DocId is null then '无扫描件' else '有扫描件' end) " 
        + ",lgc.grpcontno,"
        + " (select confmakedate from ljtempfee where otherno = lgc.prtno or otherno = lgc.grpcontno order by tempfeeno fetch first 1 rows only),"
        + " lgc.signdate,lgc.bankcode,lgc.accname,lgc.bankaccno,"
        + " (select CustomerBankCode from LCGrpContSub where prtno = lgc.prtno), "
        + " (select AuthorizeCode from LCGrpContSub where prtno = lgc.prtno), "
        + " (select County from LCGrpContSub where prtno = lgc.prtno) "
        + " from LCGrpCont lgc "
        + " left join Es_Doc_Main edm on edm.DocCode = lgc.PrtNo and edm.SubType in ('TB08', 'TB09') "
        + " where 1 = 1 " 
        + " and lgc.CardFlag = '4' "
        + getWherePart("lgc.PrtNo", "PrtNo")
        + getWherePart("lgc.ManageCom", "ManageCom",'like')
        + getWherePart(" getUniteCode(lgc.AgentCode)", "AgentCode")
        + getWherePart("lgc.AgentGroup", "AgentGroup")
        + getWherePart("lgc.PolApplyDate", "StartPolApplyDate", ">=")
        + getWherePart("lgc.PolApplyDate", "EndPolApplyDate", "<=")
        ;
    
    turnPage1.pageDivName = "divWIIPolicyListGridPage";
    turnPage1.queryModal(tStrSql, WIIPolicyListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("未找到符合条件的保单信息！");
        return false;
    }
     fm.querySql.value = tStrSql;
    
    return true;
}

/**
 * 进入卡折录入。
 */
 
function downLoadWIIPolicyList()
{
	 if(fm.querySql.value != null && fm.querySql.value != "" && WIIPolicyListGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "WIIPolicyQueryDownLoad.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再下载！");
        return ;
    }
}
/**
 * 进入卡折录入。
 */
function queryData()
{
    var tRow = WIIPolicyListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = WIIPolicyListGrid.getRowData(tRow);
    
    var tPrtNo = tRowDatas[0];
    var tManageCom = tRowDatas[1];
    var tMissionId = tRowDatas[5];
    var tSubMissionId = tRowDatas[6];
    var tProcessId = tRowDatas[7];
    var tActivityId = tRowDatas[8];
    var tActivityStatus = tRowDatas[9];
    
    var tStrUrl = "./BriGrpContInputScanMain.jsp"
        + "?PrtNo=" + tPrtNo
        + "&MissionID=" + tMissionId
        + "&SubMissionID=" + tSubMissionId
        + "&ProcessID=" + tProcessId
        + "&ActivityID=" + tActivityId
        + "&ActivityStatus=" + tActivityStatus
        + "&ManageCom=" + tManageCom 
        + "&QueryFlag=1 "
        ;

    window.open(tStrUrl, "");
    
    WIIPolicyListGrid.clearData();
}

//校验投保申请日期
function checkDate()
{
	var tStartPolApplyDate = fm.StartPolApplyDate.value;
	var tEndPolApplyDate = fm.EndPolApplyDate.value;
	
	if((tStartPolApplyDate != null && tStartPolApplyDate != "") && (tEndPolApplyDate == null || tEndPolApplyDate == ""))
	{
		alert("请录入投保终止日期！");
		return false;
	}
	
	if((tStartPolApplyDate == null || tStartPolApplyDate == "") && (tEndPolApplyDate != null && tEndPolApplyDate != ""))
	{
		alert("请录入投保起始日期！");
		return false;
	}
	
	if((tStartPolApplyDate != null && tStartPolApplyDate != "") && (tEndPolApplyDate != null && tEndPolApplyDate != ""))
	{
		if(dateDiff(tStartPolApplyDate, tEndPolApplyDate, "M") > 3)
		{
			alert("投保日期区间不能大于3个月！")
			return false;
		}
	}
	
	return true;
}

