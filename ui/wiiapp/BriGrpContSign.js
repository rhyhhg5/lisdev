//程序名称：
//程序功能：
//创建日期：2008-11-4
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryBriGrpContList()
{
    if(!verifyInput2())
    {
        return false;
    }

    //var tStrSql = ""
    //    + " select distinct lwm.MissionProp1, lwm.MissionProp2, lwm.MissionProp3, lwm.MissionProp4, "
    //    + " lwm.MissionId, lwm.SubMissionId, lwm.ProcessId, lwm.ActivityId, lwm.ActivityStatus "
    //    + " from LWMission lwm "
    //    + " inner join LJTempFee ljtf on ljtf.OtherNo = lwm.MissionProp2 and ljtf.ConfFlag = '0' "
    //    + " where 1 = 1 "
    //    + " and lwm.ProcessId = '0000000011' "
    //    + " and lwm.ActivityId = '0000011002' "
    //    + getWherePart("lwm.MissionProp2", "PrtNo")
    //    + getWherePart("lwm.MissionProp4", "ManageCom")
    //    + " with ur "
    //    ;
    var whISJF = "";
    if(fm.ISJF.value == "1"){
    	whISJF = " where x.I is not null"
    }else if(fm.ISJF.value == "2"){
    	whISJF = " where x.I is null"
    }
    var strSQL = "select A,B,C,D,E,F,G,H,(case when I is null then '未交费' else '已交费' end) ,J, M from( "
    +"select missionprop1 A,missionprop2 B,missionprop9 C,missionprop7 D,missionprop4 E"
    +",getUniteCode(missionprop5) F,missionid G,submissionid H,(select max('已交费') I from ljtempfee where otherno=missionprop2) "
    +", ( case when (select sum(paymoney) from ljtempfee where otherno=missionprop2  and enteraccdate is not null) is null then 0  else (select sum(paymoney) from ljtempfee where otherno=missionprop2 and enteraccdate is not null ) end ) J  "
    +",(select stateflag from loprtmanager where otherno = missionprop1 and code = '76') M "
    +"from lwmission  where processid='0000000012' and activityid in ('0000012002') "
    
    //+ getWherePart( 'missionprop1','GrpContNo' )
    + getWherePart( 'missionprop2','PrtNo' )
    //+ getWherePart( 'missionprop5','AgentCode' )
    //+ getWherePart( 'missionprop6','AgentGroup' )
    //+ getWherePart( 'missionprop9','GrpNo' )
    //+ getWherePart( 'missionprop7','GrpName' )
    + getWherePart( 'lwmission.missionprop4','ManageCom','like' )
    +" ) as x" 
    + whISJF;
    
    turnPage1.pageDivName = "divBriGrpContListGridPage";
    turnPage1.queryModal(strSQL, BriGrpContListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有待处理的结算待信息！");
        return false;
    }
    
    return true;
}


//提交，保存按钮对应操作
function signGrpPol(wFlag)
{
	btnForbid();
    //判定是否有选择打印数据
    var count = 0;
	for(var i = 0; i < BriGrpContListGrid.mulLineCount; i++ )
	{
		if( BriGrpContListGrid.getChkNo(i) == true )
		{
			count ++;
		}
	}
	if(count == 0){
		alert("请选择一张集体投保单后，再进行签单操作！");
		btnPermit();
		return;
	}
	if(wFlag == 1){
		if(count >1){
		alert("只能选择一张保单进行签单！");
		btnPermit();
		return;
		}
	}
    //var tSel = BriGrpContListGrid.getSelNo();
    var cPolNo = "";
    var tMissionID ="";
    var tSubMissionID = "";
    var tGrpContNo = "";
    fm.all("workType").value = "";
    
    for(var i = 0; i < BriGrpContListGrid.mulLineCount; i++ )
	{
		if( BriGrpContListGrid.getChkNo(i) == true )
		{
			cPolNo = BriGrpContListGrid.getRowColData( i, 2 );
			if( cPolNo == null || cPolNo == "" )
	        alert("请选择一张集体投保单后，再进行签单操作");
	    	else
	    	{
		        var strSql = "select * from ldsystrace where PolNo='" + cPolNo + "' and PolState=1006 ";
		      	var arrResult = easyExecSql(strSql);
		      	if (arrResult!=null && arrResult[0][1]!=Operator) {
		        	alert("印刷号为("+cPolNo+")的投保单已经被操作员（" + arrResult[0][1] + "）在（" + arrResult[0][5] + "）位置锁定！您不能操作，请选其它的印刷号！");
		        	btnPermit();
		        	return;
		      	}
	      //锁定该印刷号
		      	tMissionID = BriGrpContListGrid.getRowColData( i, 7 );
		      	tSubMissionID = BriGrpContListGrid.getRowColData( i, 8 );
		      	tGrpContNo = BriGrpContListGrid.getRowColData( i, 1 );
		      	var strSql ="select ActivityStatus from lwmission where activityid='0000012002' and missionid='"+tMissionID+"' and submissionid='"+tSubMissionID+"' with ur";
		      	var arr = easyExecSql(strSql);
		        if(arr){
		            if(arr[0][0]=="0"){
		                var strSql = "select count(1) from lccont where appflag in ('1','9') and PrtNo='"+cPolNo+"' with ur";
		                alert("印刷号为（"+cPolNo+"）保单正在签单过程中！已完成"+easyExecSql(strSql)+"个被保险人，请稍等");
		                return;
		            }
		        }
			}
		}
    }
   	var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + cPolNo + "&CreatePos=承保签单&PolState=1006&Action=INSERT";
   	var a = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 
    var i = 0;
    var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.action = "BriGrpContSignSave.jsp";
    fm.submit(); //提交
}

function afterSubmit( FlagStr, content )
{
	btnPermit();
    showInfo.close();
    window.focus();
    fm.all("workType").value = "";
    if( FlagStr == "Fail" )
    {   
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

        // 刷新查询结果
        queryBriGrpContList();
    }
}

//将签单按钮置为不可点击状态（置灰）
function btnForbid(){
	fm.btnSingConfirm.disabled = true;
	fm.btnSingBatch.disabled = true;
}

//将签单按钮恢复为可用状态(解除置灰)
function btnPermit(){
	fm.btnSingConfirm.disabled = false;
	fm.btnSingBatch.disabled = false;
}
