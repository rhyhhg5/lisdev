<%@page contentType="text/html;charset=GBK"%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.wiitb.*"%>
<%@page import="com.sinosoft.workflow.tb.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>

<%
System.out.println("BriGrpPolInputSave.jsp Begin ...");

String FlagStr = "Succ";
String Content = "";

GlobalInput tGI = (GlobalInput)session.getValue("GI");

try
{
    // 保单信息
    String tPrtNo = request.getParameter("PrtNo");
    String tGrpContNo = request.getParameter("GrpContNo");
    
    TransferData tTransData = new TransferData();
    tTransData.setNameAndValue("PrtNo", tPrtNo);
    tTransData.setNameAndValue("GrpContNo", tGrpContNo);
    // --------------------
    
    // 套餐要素信息
    String[] tRowNums = request.getParameterValues("WrapDutyParamListGridNo");
    String[] tRiskWrapCodes = request.getParameterValues("WrapDutyParamListGrid1");
    String[] tRiskCodes = request.getParameterValues("WrapDutyParamListGrid2");
    String[] tRiskNames = request.getParameterValues("WrapDutyParamListGrid3");
    String[] tMainRiskCodes = request.getParameterValues("WrapDutyParamListGrid4");
    String[] tDutyCodes = request.getParameterValues("WrapDutyParamListGrid5");
    String[] tDutyNames = request.getParameterValues("WrapDutyParamListGrid6");
    String[] tCalFactors = request.getParameterValues("WrapDutyParamListGrid7");
    String[] tCalFactorNames = request.getParameterValues("WrapDutyParamListGrid8");
    String[] tCalFactorTypes = request.getParameterValues("WrapDutyParamListGrid9");
    String[] tCalFactorValues = request.getParameterValues("WrapDutyParamListGrid10");
    String[] tCalFactorDemos = request.getParameterValues("WrapDutyParamListGrid11");
    
    LCRiskDutyWrapSet tLCRiskDutyWrapSet = new LCRiskDutyWrapSet();
    for(int idx = 0; idx < tRowNums.length; idx++)
    {
        LCRiskDutyWrapSchema tWrapDutyParam = new LCRiskDutyWrapSchema();
        
        tWrapDutyParam.setRiskWrapCode(tRiskWrapCodes[idx]);
        tWrapDutyParam.setRiskCode(tRiskCodes[idx]);
        tWrapDutyParam.setDutyCode(tDutyCodes[idx]);
        tWrapDutyParam.setCalFactor(tCalFactors[idx]);
        tWrapDutyParam.setCalFactorType(tCalFactorTypes[idx]);
        tWrapDutyParam.setCalFactorValue(tCalFactorValues[idx]);

        tLCRiskDutyWrapSet.add(tWrapDutyParam);
    }
    // --------------------
    
    VData tVData = new VData();
    tVData.add(tGI);
    tVData.add(tTransData);
    
    tVData.add(tLCRiskDutyWrapSet);
    
    BriGrpWrapPlanUI tBriGrpWrapPlanUI = new BriGrpWrapPlanUI();
    if(!tBriGrpWrapPlanUI.submitData(tVData, "Create"))
    {
        Content = " 保存失败! 原因是: " + tBriGrpWrapPlanUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
    else
    {
        Content = " 保存成功！";
        FlagStr = "Succ";
    }
    
}
catch (Exception e)
{
    Content = " 操作失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}


System.out.println("BriGrpPolInputSave.jsp End ...");
%>

<html>
    <script language="javascript">
    parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>
