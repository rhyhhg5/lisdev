<%
//程序名称：
//程序功能：
//创建日期：2011-04-25
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
GlobalInput tGI = (GlobalInput)session.getValue("GI");
String tManageCom = tGI.ComCode;
String tOperator = tGI.Operator;
%>

<script language="JavaScript">

function initForm()
{
    try
    {
        initInsuImpListGrid();
        initBox();
        
        qryGrpContInfo();
        qryInsuImpList();

        initElementtype();
        showAllCodeName();
    }
    catch(e)
    {
        alert("InitForm 函数中发生异常：初始化界面错误!");
    }
}


function initBox()
{
    try
    {
        fm.GrpContNo.value = GrpContNo;
    }
    catch(e)
    {
        alert("InitBox 函数中发生异常：初始化界面错误!");
    }
}


function initInsuImpListGrid()
{
	var iArray = new Array();

    try
    {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="客户号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="姓名";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="性别";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="出生日期";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="证件类型";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="证件号码";
        iArray[6][1]="150px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="保费（元）";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="职业类别";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="职业状态";
        iArray[9][1]="80px";
        iArray[9][2]=100;
        iArray[9][3]=0;


        InsuImpListGrid = new MulLineEnter("fm", "InsuImpListGrid"); 

        InsuImpListGrid.mulLineCount = 0;   
        InsuImpListGrid.displayTitle = 1;
        InsuImpListGrid.canSel = 0;
        InsuImpListGrid.hiddenSubtraction = 1;
        InsuImpListGrid.hiddenPlus = 1;
        InsuImpListGrid.canChk = 0;
        InsuImpListGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
        alert("初始化 InsuImpListGrid 时出错：" + ex);
    }
}
</script>

