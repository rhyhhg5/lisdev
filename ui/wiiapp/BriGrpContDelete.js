//程序名称：BriGrpContDelete
//程序功能：团单整单删除
//创建日期：2004-12-06 11:10:36
//创建人  ：Zhangrong
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    alert(content);
  }
  else
  { 
    alert("操作成功");
  }
  querygrp();
  fm.all("GrpContNo").value = "";
  fm.all("PrtNo").value = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//查询集体单
function querygrp()
{
	// 初始化表格                                                                                                                                                           
	initGrpGrid();	
	if(!verifyInput2())
	return false;
	
	// 书写SQL语句
	var str = "";	
	var strsql = "";
	strsql = "select a.ProposalGrpContNo,a.GrpContNo,a.PrtNo,a.GrpName,a.ManageCom from LCGrpCont a where 1=1 "
				 + getWherePart( 'a.ProposalGrpContNo','QGrpProposalNo' )
				 + getWherePart( 'a.GrpContNo','QGrpContNo' )
				 + getWherePart( 'a.ManageCom','QManageCom','like' )
				 + getWherePart( 'getUniteCode(a.AgentCode)','QAgentCode' )
				 + getWherePart( 'a.PrtNo','QPrtNo');
	//if (fm.all("QState").value == "已签单")
		//strsql = strsql + " and a.AppFlag = '1' "
	//else if (fm.all("QState").value != null && fm.all("QState").value != "")
		//strsql = strsql + " and (a.AppFlag not in ('1') or a.AppFlag is null)"
	if (Brief=="1")
		strsql = strsql + " and cardflag='0' ";
		
	if(Brief == "2")
	{
		strsql = strsql + " and cardflag='4' ";
		strsql += " union select '','',missionprop1,'','' from lwmission where missionprop1 not in ( select prtno from lcgrpcont ) and activityid = '0000012001' and missionprop2 like '"+manageCom+"%%' ";
  		strsql += getWherePart( 'missionprop1','QPrtNo');
  		strsql += getWherePart( 'missionprop2','QManageCom','like');
	}
	else
	{
		strsql += "union select '','',missionprop1,'','' from lwmission where missionprop1 not in ( select prtno from lcgrpcont ) and activityid in ('0000002098','0000002099') and missionprop3 like '"+manageCom+"%%'";
  		strsql += getWherePart( 'missionprop1','QPrtNo');
  		strsql += getWherePart( 'missionprop3','QManageCom','like');
  		strsql += getWherePart( 'getUniteCode(missionprop1)','QAgentCode');
	}
//	strsql +=" order by  a.GrpContNo ";
//	alert();
	
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件集体单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  fm.all("GrpContNo").value = "";
  fm.all("PrtNo").value = "";
  return true;
}


//
function GrpContSelect(parm1,parm2)
{
	var cPrtNo = "";
	var cGrpPolNo = "";
	
	if(fm.all(parm1).all('InpGrpGridSel').value == '1' )
	{
		//当前行第1列的值设为：选中
   		cPrtNo = fm.all(parm1).all('GrpGrid3').value;	//流水号
   		cGrpContNo = fm.all(parm1).all('GrpGrid2').value;	//集体合同号
   		cGrpProposalNo = fm.all(parm1).all('GrpGrid1').value;	//集体投保单号
   		//alert(cGrpContNo);
   	}
   	if(cPrtNo == null || cPrtNo == "" )
   	{
   		alert("请选择一条有效的团体单!");
   	}
   	else
   	{
		fm.all("GrpContNo").value = cGrpContNo;
		fm.all("PrtNo").value = cPrtNo;
   	}
}

function deleteGrpCont()
{
	//alert(fm.all("PrtNo").value); return ;
    var tSel = GrpGrid.getSelNo();
    if( tSel == 0 || tSel == null || fm.PrtNo.value == "" || fm.PrtNo.value == null)
    {
        alert( "请先选择一条记录。" );
        return false;
    }
    if(!checkSign()){
    	alert( "该保单已签单，不能删除。" );
        return false;
    }
	fm.xPrtNo.value=fm.all("PrtNo").value;
	var strsql="select 1 from ljtempfee where otherno='"+fm.all('PrtNo').value+"' "
			+ "and tempfeeno not in (select tempfeeno from LJAGetTempFee) and enteraccdate is not null";
	var arr = easyExecSql(strsql);
	if(arr){
		alert("财务已交费！不能进行新单删除！");
		return;
	}
	
	if(!checkConfirm()){
		return false;
	}
	if (!confirm("确认要删除该保单吗？"))
	{
		return;
	}

	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./BriGrpContDeleteChk.jsp";
	fm.submit();
}


function deleteCont()
{
	var strsql="select 1 from ljtempfee where otherno='"+fm.all("PrtNo").value+"' "
			+ "and tempfeeno not in (select tempfeeno from LJAGetTempFee) and confdate is not null";
	var arr = easyExecSql(strsql);
	if(arr){
		alert("财务已交费！不能进行新单删除！");
		return;
	}
	var tSql = " select 1 from lwmission where missionprop2 = '"+fm.all("PrtNo").value+"'"
	         + " and activityid = '0000002005'";
	var arr1 = easyExecSql(tSql);
	if(arr1){
		alert("该保单已经通过人工核保！不能进行新单删除！");
		return;
	}         
	if (fm.all("GrpContNo").value == null || fm.all("GrpContNo").value == "")
	{
		alert("请先选择一条保单！");
		return;
	}
  else
  {
	       window.open("./InGrpContDeleteMain.jsp?GrpContNo="+ fm.all("GrpContNo").value);
	   
	}

}
function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="comcode"){
    fm.QAgentCode.value = "";
  	fm.QAgentCodeName.value = "";   
 }
}
//是否签单
function checkSign(){
	var tSignSql = "select appflag,signdate from lcgrpcont where prtno = '"+fm.PrtNo.value+"'";
	var arr1 = easyExecSql(tSignSql);
	if(arr1){
		if(arr1[0][0] == '1' || (arr1[0][1] != '' && arr1[0][1] != null)){//已签单
			return false;
		}
	}
	return true;
}
//是否确认
function checkConfirm(){
	var tConfirmSql = "select enableflag,confstate from lcbrigrpimportdetail where bussno = '"+fm.PrtNo.value+"' order by makedate desc ";
	var arr1 = easyExecSql(tConfirmSql);
	if(arr1){
		if(arr1[0][0]=='01' && arr1[0][1] != '02'){
			alert("该保单未生成回送数据，不可以删除！");	
			return false;
		}
		if(arr1[0][0]=='00'){
			alert("该保单未处理完成，不可以删除！");	
			return false;
		}
	}
	return true;
}