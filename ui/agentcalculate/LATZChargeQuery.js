//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var End = fm.EndDate.value;
	var cur = fm.CurrentDate.value;	
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
	fm.all('divSpanLACommisionGrid').style.display='';
	fm.all('divSpanLACommisionGrid1').style.display='none';
    //首先检验录入框
  if(!verifyInput()) return false;   
 
 var  sql= "select distinct aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,sum(nn),oo,pp,qq,rr from ("
  			
  			+"select a.managecom aa,(select name from ldcom where comcode=a.ManageCom) bb,"
  			+"(select riskname from lmriskapp where riskcode=a.riskcode) cc,"
  			+"a.grpcontno dd,b.grpname ee,b.grpname ff,e.signdate gg,b.cvalidate hh,b.paytodate ii,"
  			+"c.name jj,d.licenseno kk,b.prem ll,a.chargerate mm,a.charge nn,"
  			+"(select (select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno"
  			+" and chargedate=a.accountdate fetch first 1 rows only)"
  			+" from LIDataTransResult a, liaboriginaldata b  where b.serialno = a.standbystring3 "
  			+" and a.summoney<>0 and a.classid='00000151' and a.contno=b.grpcontno and b.payno=e.receiptno fetch first 1 rows only) oo," 
            +"d.name pp, "
            +"(select confdate from ljaget where otherno=a.receiptno fetch first 1 rows only) qq "
            +",case when (substr(e.managecom,1,4)='8637' or substr(e.managecom,1,4)='8621') then (select actugetno from ljaget where otherno=a.receiptno fetch first 1 rows only) else '' end  rr "
            +"from lacommision e,LACharge a,lcgrppol b,laagent c,lacom d where "
  			+"e.commisionsn=a.commisionsn and e.agentcode=c.agentcode "
  			+"and a.agentcom=d.agentcom and a.grpcontno=b.grpcontno and a.riskcode=b.riskcode "
             + "and a.chargetype='51' and a.chargestate<>'0' " 
             + getWherePart('a.ManageCom', 'ManageCom','like')
             + getWherePart('a.tmakedate', 'StartDate','>=')
             + getWherePart('a.tmakedate', 'EndDate','<=')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('e.AgentCom', 'AgentCom')
          	 + getWherePart('a.grpcontno', 'ContNo')
          	 + getWherePart('a.riskcode', 'RiskCode')
          	 +" union "
          	 +"select a.managecom aa,(select name from ldcom where comcode=a.ManageCom) bb,"
  			+"(select riskname from lmriskapp where riskcode=a.riskcode) cc,"
  			+"a.grpcontno dd,b.grpname ee,b.grpname ff,e.signdate gg,b.cvalidate hh,b.paytodate ii,"
  			+"c.name jj,d.licenseno kk,b.prem ll,a.chargerate mm,a.charge nn,"
  			+"(select (select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno"
  			+" and chargedate=a.accountdate fetch first 1 rows only)"
  			+" from LIDataTransResult a, liaboriginaldata b  where b.serialno = a.standbystring3 "
  			+" and a.summoney<>0 and a.classid='00000151' and a.contno=b.grpcontno and b.payno=e.receiptno fetch first 1 rows only) oo," 
            +"d.name pp, "
            +"(select confdate from ljaget where otherno=a.receiptno fetch first 1 rows only) qq "
            +",case when (substr(e.managecom,1,4)='8637' or substr(e.managecom,1,4)='8621') then (select actugetno from ljaget where otherno=a.receiptno fetch first 1 rows only) else '' end  rr "
            +"from lacommision e,LACharge a,lbgrppol b,laagent c,lacom d where "
  			+"e.commisionsn=a.commisionsn and e.agentcode=c.agentcode "
  			+"and a.agentcom=d.agentcom and a.grpcontno=b.grpcontno and a.riskcode=b.riskcode "
             + "and a.chargetype='51' and a.chargestate<>'0' " 
             + getWherePart('a.ManageCom', 'ManageCom','like')
                  + getWherePart('a.tmakedate', 'StartDate','>=')
             + getWherePart('a.tmakedate', 'EndDate','<=')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('e.AgentCom', 'AgentCom')
          	 + getWherePart('a.grpcontno', 'ContNo')
          	 + getWherePart('a.riskcode', 'RiskCode')
          	 +" union "
          	 +"select a.managecom aa,(select name from ldcom where comcode=a.ManageCom) bb,"
  			+"(select riskname from lmriskapp where riskcode=a.riskcode) cc,"
  			+"a.contno dd,b.appntname ee,b.insuredname ff,e.signdate gg,b.cvalidate hh,b.enddate ii,"
  			+"c.name jj,d.licenseno kk,b.prem ll,a.chargerate mm,a.charge nn,"
  			+"(select (select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno"
  			+" and chargedate=a.accountdate fetch first 1 rows only)"
  			+" from LIDataTransResult a, liaboriginaldata b  where b.serialno = a.standbystring3 "
  			+" and a.summoney<>0 and a.classid='00000151' and a.contno=b.grpcontno and b.payno=e.receiptno fetch first 1 rows only) oo," 
            +"d.name pp, "
            +"(select confdate from ljaget where otherno=a.receiptno fetch first 1 rows only) qq  "
            +",case when (substr(e.managecom,1,4)='8637' or substr(e.managecom,1,4)='8621') then (select actugetno from ljaget where otherno=a.receiptno fetch first 1 rows only) else '' end  rr "
            +"from lacommision e,LACharge a,lcpol b,laagent c,lacom d where "
  			+"e.commisionsn=a.commisionsn and e.agentcode=c.agentcode "
  			+"and a.agentcom=d.agentcom and a.contno=b.contno and a.riskcode=b.riskcode "
             + "and a.chargetype='51' and a.chargestate<>'0' and b.grpcontno='00000000000000000000' " 
             + getWherePart('a.ManageCom', 'ManageCom','like')
                  + getWherePart('a.tmakedate', 'StartDate','>=')
             + getWherePart('a.tmakedate', 'EndDate','<=')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('e.AgentCom', 'AgentCom')
          	 + getWherePart('a.contno', 'ContNo')
          	 + getWherePart('a.riskcode', 'RiskCode')
          	 +" union "
          	 +"select a.managecom aa,(select name from ldcom where comcode=a.ManageCom) bb,"
  			+"(select riskname from lmriskapp where riskcode=a.riskcode) cc,"
  			+"a.contno dd,b.appntname ee,b.insuredname ff,e.signdate gg,b.cvalidate hh,b.enddate ii,"
  			+"c.name jj,d.licenseno kk,b.prem ll,a.chargerate mm,a.charge nn,"
  			+"(select (select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno"
  			+" and chargedate=a.accountdate fetch first 1 rows only)"
  			+" from LIDataTransResult a, liaboriginaldata b  where b.serialno = a.standbystring3 "
  			+" and a.summoney<>0 and a.classid='00000151' and a.contno=b.grpcontno and b.payno=e.receiptno fetch first 1 rows only) oo," 
            +"d.name pp, "
            +"(select confdate from ljaget where otherno=a.receiptno fetch first 1 rows only) qq "
            +",case when (substr(e.managecom,1,4)='8637' or substr(e.managecom,1,4)='8621') then (select actugetno from ljaget where otherno=a.receiptno fetch first 1 rows only) else '' end  rr "
            +"from lacommision e,LACharge a,lbpol b,laagent c,lacom d where "
  			+"e.commisionsn=a.commisionsn and e.agentcode=c.agentcode "
  			+"and a.agentcom=d.agentcom and a.contno=b.contno and a.riskcode=b.riskcode "
             + "and a.chargetype='51' and a.chargestate<>'0'  and b.grpcontno='00000000000000000000' " 
             + getWherePart('a.ManageCom', 'ManageCom','like')
                  + getWherePart('a.tmakedate', 'StartDate','>=')
             + getWherePart('a.tmakedate', 'EndDate','<=')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('e.AgentCom', 'AgentCom')
          	 + getWherePart('a.contno', 'ContNo')
          	 + getWherePart('a.riskcode', 'RiskCode')
          	 
          	 +") as aaa group by aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,oo,pp,qq,rr order by aa,dd with ur "        	 
          	 ;   
	turnPage.queryModal(sql, LACommisionGrid);   	
	
}
function easyQueryClick1()
{
	fm.all('divSpanLACommisionGrid').style.display='none';
	fm.all('divSpanLACommisionGrid1').style.display='';
    //首先检验录入框
  if(!verifyInput()) return false;   
 
  var  sql= 
           "select (select name from lacom where agentcom=LACharge.AgentCom) comName,(select name from ldcom where comcode=LACharge.ManageCom) ManageName,"
 					+"(select name from labranchgroup where agentgroup=LACOMMISION.agentgroup)  groupname, "
 					+"LACOMMISION.agentcode, "
 					+"(select  name from laagent where agentcode=LACOMMISION.agentcode) agentName  , "
 					+"LACharge.ContNo, "
 					+"(select  appntname  from lccont where contno=LACharge.contno union select  appntname  from lbcont where contno=LACharge.contno) appntname, "
 					+"LACharge.RiskCode, sum(LACharge.TransMoney), LACOMMISION.tenteraccdate,LACOMMISION.signdate,(select CInValiDate from lccont where contno =LACharge.contno union select CInValiDate from lbcont where contno =LACharge.contno ) CInValiDate  " 			
 					+"from 	LACharge ,LACOMMISION where 1=1 "
 					+ " AND LACharge.CommisionSN = LACOMMISION.CommisionSN"
          + " and LACharge.GrpContNo = '00000000000000000000' and LACharge.WageNo>='"+fm.all('WageNo').value+"'and LACharge.WageNo<='"+fm.all('WageNo1').value+"' " 
          + getWherePart('LACharge.ManageCom', 'ManageCom')
          + getWherePart('LACharge.BranchType', 'BranchType')
          + getWherePart('LACharge.AgentCom', 'AgentCom') 
          + " group by LACharge.AgentCom,LACharge.ManageCom,LACOMMISION.agentgroup,LACOMMISION.agentcode,"  
          + "  LACharge.RiskCode,  LACOMMISION.tenteraccdate,LACOMMISION.signdate,lacharge.contno"     	 
          	 ;  
	turnPage.queryModal(sql, LACommisionGrid1); 
}
function afterCodeSelect(codeName,Field)
{
	if(codeName == "BranchType2")
	{
          var  sql=" select enddate+1 day  from  lawagelog  where 1=1 and  wageno=(select max(wageno) from lawagelog where branchtype='"+fm.BranchType.value+"' "
                     +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom')
                     + getWherePart('BranchType', 'BranchType')
          	     + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	  {
           fm.all('StartDate').value= tArr[0][0];                                   
             
          }
	}
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("计算起期必须小于等于计算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}

function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#2# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
function doDownLoad(){
	if(!verifyInput()) return false;   
 
 var  sql= "select distinct aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,sum(nn),oo,pp,qq,rr from ("
  			
  			+"select a.managecom aa,(select name from ldcom where comcode=a.ManageCom) bb,"
  			+"(select riskname from lmriskapp where riskcode=a.riskcode) cc,"
  			+"a.grpcontno dd,b.grpname ee,b.grpname ff,e.signdate gg,b.cvalidate hh,b.paytodate ii,"
  			+"c.name jj,d.licenseno kk,b.prem ll,a.chargerate mm,a.charge nn,"
  			+"(select (select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno"
  			+" and chargedate=a.accountdate fetch first 1 rows only)"
  			+" from LIDataTransResult a, liaboriginaldata b  where b.serialno = a.standbystring3 "
  			+" and a.summoney<>0 and a.classid='00000151' and a.contno=b.grpcontno and b.payno=e.receiptno fetch first 1 rows only) oo," 
            +"d.name pp, "
            +"(select confdate from ljaget where otherno=a.receiptno fetch first 1 rows only) qq "
            +",case when (substr(e.managecom,1,4)='8637' or substr(e.managecom,1,4)='8621') then (select actugetno from ljaget where otherno=a.receiptno fetch first 1 rows only) else '' end  rr "
            +"from lacommision e,LACharge a,lcgrppol b,laagent c,lacom d where "
  			+"e.commisionsn=a.commisionsn and e.agentcode=c.agentcode "
  			+"and a.agentcom=d.agentcom and a.grpcontno=b.grpcontno and a.riskcode=b.riskcode "
             + "and a.chargetype='51' and a.chargestate<>'0' " 
             +" and a.managecom like '"+fm.all('ManageCom').value+"%' "
                  + getWherePart('a.tmakedate', 'StartDate','>=')
             + getWherePart('a.tmakedate', 'EndDate','<=')
             + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('e.AgentCom', 'AgentCom')
          	 + getWherePart('a.grpcontno', 'ContNo')
          	 + getWherePart('a.riskcode', 'RiskCode')
          	 +" union "
          	 +"select a.managecom aa,(select name from ldcom where comcode=a.ManageCom) bb,"
  			+"(select riskname from lmriskapp where riskcode=a.riskcode) cc,"
  			+"a.grpcontno dd,b.grpname ee,b.grpname ff,e.signdate gg,b.cvalidate hh,b.paytodate ii,"
  			+"c.name jj,d.licenseno kk,b.prem ll,a.chargerate mm,a.charge nn,"
  			+"(select (select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno"
  			+" and chargedate=a.accountdate fetch first 1 rows only)"
  			+" from LIDataTransResult a, liaboriginaldata b  where b.serialno = a.standbystring3 "
  			+" and a.summoney<>0 and a.classid='00000151' and a.contno=b.grpcontno and b.payno=e.receiptno fetch first 1 rows only) oo," 
            +"d.name pp, "
            +"(select confdate from ljaget where otherno=a.receiptno fetch first 1 rows only) qq "
            +",case when (substr(e.managecom,1,4)='8637' or substr(e.managecom,1,4)='8621') then (select actugetno from ljaget where otherno=a.receiptno fetch first 1 rows only) else '' end  rr "
            +"from lacommision e,LACharge a,lbgrppol b,laagent c,lacom d where "
  			+"e.commisionsn=a.commisionsn and e.agentcode=c.agentcode "
  			+"and a.agentcom=d.agentcom and a.grpcontno=b.grpcontno and a.riskcode=b.riskcode "
             + "and a.chargetype='51' and a.chargestate<>'0' " 
             +" and a.managecom like '"+fm.all('ManageCom').value+"%' "
                  + getWherePart('a.tmakedate', 'StartDate','>=')
             + getWherePart('a.tmakedate', 'EndDate','<=')
             + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('e.AgentCom', 'AgentCom')
          	 + getWherePart('a.grpcontno', 'ContNo')
          	 + getWherePart('a.riskcode', 'RiskCode')
          	 +" union "
          	 +"select a.managecom aa,(select name from ldcom where comcode=a.ManageCom) bb,"
  			+"(select riskname from lmriskapp where riskcode=a.riskcode) cc,"
  			+"a.contno dd,b.appntname ee,b.insuredname ff,e.signdate gg,b.cvalidate hh,b.enddate ii,"
  			+"c.name jj,d.licenseno kk,b.prem ll,a.chargerate mm,a.charge nn,"
  			+"(select (select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno"
  			+" and chargedate=a.accountdate fetch first 1 rows only)"
  			+" from LIDataTransResult a, liaboriginaldata b  where b.serialno = a.standbystring3 "
  			+" and a.summoney<>0 and a.classid='00000151' and a.contno=b.contno and b.payno=e.receiptno fetch first 1 rows only) oo," 
            +"d.name pp, "
            +"(select confdate from ljaget where otherno=a.receiptno fetch first 1 rows only) qq "
            +",case when (substr(e.managecom,1,4)='8637' or substr(e.managecom,1,4)='8621') then (select actugetno from ljaget where otherno=a.receiptno fetch first 1 rows only) else '' end  rr "
            +"from lacommision e,LACharge a,lcpol b,laagent c,lacom d where "
  			+"e.commisionsn=a.commisionsn and e.agentcode=c.agentcode "
  			+"and a.agentcom=d.agentcom and a.contno=b.contno and a.riskcode=b.riskcode "
             + "and a.chargetype='51' and a.chargestate<>'0' and b.grpcontno='00000000000000000000' " 
             +" and a.managecom like '"+fm.all('ManageCom').value+"%' "
                  + getWherePart('a.tmakedate', 'StartDate','>=')
             + getWherePart('a.tmakedate', 'EndDate','<=')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('e.AgentCom', 'AgentCom')
          	 + getWherePart('a.contno', 'ContNo')
          	 + getWherePart('a.riskcode', 'RiskCode')
          	 +" union "
          	 +"select a.managecom aa,(select name from ldcom where comcode=a.ManageCom) bb,"
  			+"(select riskname from lmriskapp where riskcode=a.riskcode) cc,"
  			+"a.contno dd,b.appntname ee,b.insuredname ff,e.signdate gg,b.cvalidate hh,b.enddate ii,"
  			+"c.name jj,d.licenseno kk,b.prem ll,a.chargerate mm,a.charge nn,"
  			+"(select (select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno"
  			+" and chargedate=a.accountdate fetch first 1 rows only)"
  			+" from LIDataTransResult a, liaboriginaldata b  where b.serialno = a.standbystring3 "
  			+" and a.summoney<>0 and a.classid='00000151' and a.contno=b.contno and b.payno=e.receiptno fetch first 1 rows only) oo," 
            +"d.name pp, "
            +"(select confdate from ljaget where otherno=a.receiptno fetch first 1 rows only) qq "
            +",case when (substr(e.managecom,1,4)='8637' or substr(e.managecom,1,4)='8621') then (select actugetno from ljaget where otherno=a.receiptno fetch first 1 rows only) else '' end  rr "
            +"from lacommision e,LACharge a,lbpol b,laagent c,lacom d where "
  			+"e.commisionsn=a.commisionsn and e.agentcode=c.agentcode "
  			+"and a.agentcom=d.agentcom and a.contno=b.contno and a.riskcode=b.riskcode "
             + "and a.chargetype='51' and a.chargestate<>'0'  and b.grpcontno='00000000000000000000' " 
            +" and a.managecom like '"+fm.all('ManageCom').value+"%' "
                 + getWherePart('a.tmakedate', 'StartDate','>=')
             + getWherePart('a.tmakedate', 'EndDate','<=')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('e.AgentCom', 'AgentCom')
          	 + getWherePart('a.contno', 'ContNo')
          	 + getWherePart('a.riskcode', 'RiskCode')
          	 
          	 +") as aaa group by aa,bb,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,oo,pp,qq,rr order by aa,dd with ur "        	 
          	 ;
  fm.all('querySQL').value=sql;
  fm.action = "./LATZChargeQueryReport.jsp";
  fm.target="f1print";
  fm.submit();
}