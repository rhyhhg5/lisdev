<%@page contentType="text/html;charset=gb2312"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAWageGatherSave.jsp
//程序功能：
//创建日期：2005-6-3 10:04
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="java.text.SimpleDateFormat"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>
<%
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
  String Content = "";
  String tOperate = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String currentDate = PubFun.getCurrentDate();
	currentDate = AgentPubFun.formatDate(currentDate,"yyyy-MM-dd"); 	
	if(tG==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  { 	 
      //接受参数
      String tManageCom = request.getParameter("ManageCom");
      String tBranchType = request.getParameter("BranchType");
      String tBranchType2 = request.getParameter("BranchType2");
      String tEndDate = request.getParameter("EndDate");
       
      //准备参数     
      String Year  = AgentPubFun.formatDate(tEndDate,"yyyy");
      String Month = AgentPubFun.formatDate(tEndDate,"MM"); 	
      String YearMonth = Year+Month;
      System.out.println("YearMonth:"+YearMonth);
       
      tEndDate = AgentPubFun.formatDate(tEndDate,"yyyy-MM-dd");
      if(currentDate.compareTo(tEndDate)<=0)
      {      
      	FlagStr="Fail";
      	Content="计算止期应小于今天";
      	
      }
      else 
      {
        LAChargeLogSchema tLAChargeLogSchema = new LAChargeLogSchema();
        tLAChargeLogSchema.setManageCom(tManageCom);       
        tLAChargeLogSchema.setEndDate(tEndDate);	 
        tLAChargeLogSchema.setBranchType(tBranchType);       
        tLAChargeLogSchema.setBranchType2(tBranchType2); 	
        tLAChargeLogSchema.setChargeMonth(Month);
        tLAChargeLogSchema.setChargeCalNo(YearMonth);
        tLAChargeLogSchema.setChargeYear(Year);
        VData tVData=new VData();
        tVData.addElement(tG);	   
        tVData.addElement(tLAChargeLogSchema);	 
        BankChargeGatherNewUI tBankChargeGatherNewUI = new BankChargeGatherNewUI();  
        if (!tBankChargeGatherNewUI.submitData(tVData,""))
	  	  {   	  	     
	  	    FlagStr="Fail";
	  	    Content=tBankChargeGatherNewUI.mErrors.getFirstError();	  	     
	  	  }
	  	  else
	  	 	{
	  	 	   FlagStr="Succ";
	  	 	   Content="保存成功";
	  	 	}	                
     }  
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
