<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：DataIntoLACommision.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<% 
    String CurDate = PubFun.getCurrentDate();    
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String strSql= " 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'"; 
%>
<script>
	 var  mBranchType =  <%=BranchType%>;
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="BankChargQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="BankChargQueryInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="BankChargeQuerySave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		手续费查询
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
           <TD  class= input>
      <Input class="codeno" name=ManageCom verify="管理机构|notnull"
        ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,1,'sign',null,null);" 
        onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,1,'sign',null,null);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
      </TD>        
           <TD class = title>
             银邮机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom ondblclick="return getAgentComName(this,AgentComName);" onkeyup="return getAgentComName(this,AgentComName);" verify="银邮机构|code" ><Input class=codename name=AgentComName readOnly >
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
           收费起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="财务结算起期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            收费止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="财务结算止期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
        </TR>
        <tr class=common>	
  		  <td  class= title>结算状态</td>
          <TD  class= input> <Input class= "codeno" name=ChargeState CodeData="0|^1|已结算|^0|未结算|" ondblclick="return showCodeListEx('F01list',[this,StateName],[0,1]);" onkeyup="return showCodeListKeyEx('F01list',[this,StateName],[0,1]);" onchange="" ><input class=codename name=StateName  readonly=true ></TD> 	
  		  <td  class= title>财务实付状态</td>
          <TD  class= input> <Input class= "codeno" name=ConfState CodeData="0|^1|已付|^0|未付|" ondblclick="return showCodeListEx('ConfState',[this,ConfStateName],[0,1]);" onkeyup="return showCodeListKeyEx('ConfState',[this,ConfStateName],[0,1]);" onchange="" ><input class=codename name=ConfStateName  readonly=true ></TD> 	     
        </tr>
      </table>       
          <input type =button class=common value="手续费查询" onclick="easyQueryClick();">
          <input type =button class=common value="手续费下载" onclick="DoDownload();">
          <input type =button class=common value="银保保费明细查询" onclick="easyQueryClick1();">
          <input type =button class=common value="银保保费明细下载" onclick="DoDownload1();">
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">     
          <input type="hidden" class=input name=Type >  
          <input type="hidden" class=input name=querySql >         
    </Div>
    
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <Div id= "divSpanLACommisionGrid" align=center style= "display: '' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid">
  				</span> 
		    </td>
			</tr>
	  </table>
	</div>
	<Div id= "divSpanLACommisionGrid1" align=center style= "display: 'none' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid1">
  				</span> 
		    </td>
			</tr>
	  </table>
	</div>
      <Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div>
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>