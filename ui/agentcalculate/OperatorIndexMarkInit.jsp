<%
//程序名称：OperatorIndexMarkInit.jsp
//程序功能：
//创建日期：2008-03-03
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<script language="JavaScript">

function initForm()
{
  try
  {
    initOperatorIndexMarkGrid();
  }
  catch(re)
  {
    alert("OperatorIndexMarkInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var OperatorIndexMarkGrid;
function initOperatorIndexMarkGrid() {                            
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=0;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="主键";         		//列名
    iArray[1][1]="0px";         		//宽度
    
    
    iArray[2]=new Array();
    iArray[2][0]="达成率最小值";         		//列名
    iArray[2][1]="100px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=1;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="达成率最大值";         		//列名
    iArray[3][1]="100px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=1;         		//是否允许录入，0--不能，1--允许  
    
    
    iArray[4]=new Array();
    iArray[4][0]="对应分数";         		//列名
    iArray[4][1]="100px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=1;         		//是否允许录入，0--不能，1--允许    
    
    
    
   OperatorIndexMarkGrid = new MulLineEnter( "fm" , "OperatorIndexMarkGrid" ); 
    //这些属性必须在loadMulLine前
 
    //OperatorIndexMarkGrid.mulLineCount = 10;   
    OperatorIndexMarkGrid.displayTitle = 1;
    //OperatorIndexMarkGrid.hiddenPlus = 0;
    //OperatorIndexMarkGrid.hiddenSubtraction = 0;
    //OperatorIndexMarkGrid.canSel = 0;
    OperatorIndexMarkGrid.canChk = 1;
    OperatorIndexMarkGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
