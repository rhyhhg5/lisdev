//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //initArchieveGrid();
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("Input.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  var tSel = ArchieveGrid.mulLineCount;		
  if( tSel == 0 || tSel == null )
  {
    alert("请先查询出保单的提奖信息!");
    return false;
  }
  for(var j=0;j<ArchieveGrid.mulLineCount;j++)
  {
  	var trate=ArchieveGrid.getRowColData(j,12); 
  	if(trate>=0&&trate<=1)
  	{}
  	else
  	{
  	 alert("录入的提奖比例格式或范围不对！");
            return false; 
  	}
  	//if (trate<=0||trate>=1)
  	//{
  	//  if(confirm("保单提奖比例存在小于或等于0或大于等于1的数，您确实想保存该纪录吗？"))
  	//   {
  	//   	break;
    //    }
  	//
  	//  else
    //      {
    //        alert("您取消了修改操作！");
    //        return false;           
   //     }
    //   }
   }
   return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  if(!beforeSubmit())
  {
    return false;
  }
	
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
   var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  if(!beforeSubmit())
  {
    return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}                    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if(!beforeSubmit())
  {
    return false;
  }  
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}



function easyQueryClick() {
  if (verifyInput() == false)
  return false;	
  initArchieveGrid();
  var tReturn = getManageComLimitlike("b.ManageCom");
	//此处书写SQL语句			     			    
  var strSql = "select a.commisionsn,a.managecom,a.wageno,"
  +"getunitecode(a.agentcode), (select name from laagent where agentcode=a.agentcode),"
  +"a.riskcode,(select riskname from lmriskapp where riskcode=a.riskcode),"
  +"a.agentcom,a.transmoney,a.fyc,a.signdate,a.fycrate "
  +" from Lacommision  a  "
  +" where 1=1 and a.branchtype='2' and a.branchtype2='02' "
  + getWherePart('a.GrpContNo', 'GrpContNo')
  +" order by wageno " ;
 
  var arrResult = new Array();
  arrResult = easyExecSql(strSql);
  
  if (!arrResult) 
  {
    alert("保单不存在或者查询错误！");
    return false;
  }
  displayMultiline(arrResult,ArchieveGrid);
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ArchieveGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	//设置需要返回的数组
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = ArchieveGrid.getRowData(tRow-1);
	return arrSelected;
}   

//校验mulline
function chkMulLine()
{
var i;
var selFlag = true;
var iCount = 0;
var rowNum = ArchieveGrid.mulLineCount;
for(i=0;i<rowNum;i++)
{
 if(ArchieveGrid.getChkNo(i))
 {
	iCount++;
	if((ArchieveGrid.getRowColData(i,12) == null)||(ArchieveGrid.getRowColData(i,12) == ""))
	{
	  alert("提奖比例不能为空");
	  ArchieveGrid.setFocus(i,1,ArchieveGrid);
	  selFlag = false;
	  break;
    }
	else if(ArchieveGrid.getRowColData(i,12)<'0')
	{
	  alert("提奖比例不能为负数!");
	  ArchieveGrid.setFocus(i,1,ArchieveGrid);
 	 selFlag = false;
	  break;
   }
	else if(ArchieveGrid.getRowColData(i,12)<'0'||ArchieveGrid.getRowColData(i,12)>'1')
	{
	  alert("提奖比例不在0到1之间!");
	  ArchieveGrid.setFocus(i,1,ArchieveGrid);
	  selFlag = false;
	  break;
	}
 }
 }
return true;
}          
