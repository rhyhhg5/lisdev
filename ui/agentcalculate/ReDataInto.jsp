<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>  
<%
//程序名称：ReDataInto.jsp
//程序功能：
//创建日期：2003-08-01 
//创建人  ：lh创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="ReDataInto.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="ReDataIntoInit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form action="./ReDataIntoSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		扎帐信息调整
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
				<TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>           
          <TD class = title>
             展业类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType ondblclick="return showCodeList('BranchType',[this,BranchTypeName],[0,1]);" verify="展业类型|code:BranchType" ><input class=codename name=BranchTypeName readonly=true >
          </TD>
        </TR>
        <TR  class = common>          
           <TD  class = title>
            计算起期
          </TD>          
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="计算起期|DATE&NOTNULL" >
          </TD>
           <TD  class= title>
            计算止期
          </TD>          
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="计算止期|DATE&NOTNULL" >
          </TD>        
        </TR>         
        <TR  class = common>          
           <TD  class = title>
            代理人编码
          </TD>          
          <TD  class= input>
            <Input class= common name=AgentCode >
          </TD>
           <TD  class= title>
            保单号
          </TD>          
          <TD  class= input>
            <Input class= common name=PolNo >
          </TD>        
        </TR>    
        <TR  class = common>          
           <TD  class = title>
            机构编码
          </TD>          
          <TD  class= input>
            <Input class= common name=BranchAttr >
          </TD>
          </TD>        
        </TR>   
      </table>
    </Div> 
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent2);">
    		</td>
    		 <td class= titleImg>
        		调整项目
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent2" style= "display: ''"> 
    <table> 
    <TR class=input>
      <TD class=common>
        <input type="checkbox" value="00,09,10" name="DirectWage" onclick="">直接佣金
      
        <input type="checkbox" value="2345" name="Charge" onclick="">手续费
     
        <input type="checkbox" value="03" name="StandPrem" onclick="">标保
 
        <input type="checkbox" value="04" name="CalCount" onclick="">件数
     
        <input type="checkbox" value="01" name="GrpFyc" onclick="">组提佣
 
        <input type="checkbox" value="02" name="DepFyc" onclick="">部提佣
      </TD>
      <input type=hidden value="" name=CalTypeStr >
    </TR>
    <TR>      
      <TD>&nbsp;&nbsp</TD>
      <TD>&nbsp;&nbsp</TD>
      <TD>&nbsp;&nbsp</TD>
      <TD>&nbsp;&nbsp</TD>
    </TR>
    <TR class=input>     
      <TD class=common>
          <input type =button class=common value="调整计算" onclick="AgentWageCalSave();">    
      </TD> 
    </TR>   
    </table>
    </Div>
  </form>   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>