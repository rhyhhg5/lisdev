<%
//程序名称：
//程序功能：
//创建日期：2008-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('GrpContNo').value = '';
    fm.all('IndexNo').value = '';
//    fm.all('ManageCom').value = '';
//    fm.all('BranchAttr').value = '';
//    fm.all('AppntNo').value = '';
//    fm.all('AppntName').value = '';
//    fm.all('TransMoney').value = '';
//    fm.all('RiskNum').value = '';
//    fm.all('CurPayToDate').value = '';
//    fm.all('SignDate').value = '';
  }
  catch(ex)
  {
    alert("DataGrpContSplitInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();   
    initSetGrid();
    
  }
  catch(re)
  {
    alert("DataGrpContSplitInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
    
    
    iArray[1]=new Array();
	  iArray[1][0]="团险保单号";          		//列名
	  iArray[1][1]="100px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	

    
    iArray[2]=new Array();
    iArray[2][0]="管理机构"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="销售团队代码"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
   
    iArray[4]=new Array();
    iArray[4][0]="投保人号"; //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="投保人名称"; //列名
    iArray[5][1]="100px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许

 
    iArray[6]=new Array();
    iArray[6][0]="保费"; //列名
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许    

        
    iArray[7]=new Array();
    iArray[7][0]="险种"; //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许    

    
    iArray[8]=new Array();
    iArray[8][0]="签单日期"; //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许   

		
		iArray[9]=new Array();
    iArray[9][0]="业务员"; //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="业务员名称"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
  
  

    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 0;
		//SetGrid.mulLineCount = 1;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =1;
    SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LaratecommisionSetInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}


</script>
