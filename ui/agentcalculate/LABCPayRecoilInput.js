//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
	fm.all('divSpanLACommisionGrid').style.display='';
	fm.all('divSpanLACommisionGrid1').style.display='none';
    //首先检验录入框
  if(!verifyInput()) return false;   
 
  var  sql= "select managecom,aa,agentcom,bb,contno,riskcode,sum(transmoney),chargerate,"
  			+"sum(charge),mm,tmakedate,dd from ("
  			+"select a.managecom,(select name from ldcom where comcode=a.ManageCom) aa,a.AgentCom,"
  			+"(select name from lacom where agentcom=a.agentcom) bb,"
  			+"a.ContNo,a.RiskCode,a.TransMoney,a.ChargeRate,a.Charge," 
  			+"(case a.chargestate when '0' then '未结算' when '1' then '已结算' end) mm,"
  			+"a.tmakedate,"
  			+"(select codename from ldcode where codetype='transtype' and code=a.transtype)dd "
  			+"from LACharge a where a.chargetype='99' and a.chargestate='1' and a.charge >0 "
  			+" AND NOT EXISTS (SELECT '1' FROM LAWAGEACTIVITYLOG where WageLogType='CR'  AND COMMISIONSN=a.COMMISIONSN ) "
             + " and a.tmakedate='"+fm.all('TMakeDate').value+"'  " 
             + getWherePart('a.ManageCom', 'ManageCom','like')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('a.ContNo','ContNo')
          	 +") as hh group by managecom,aa,agentcom,bb,contno,riskcode,"
          	 +"chargerate,mm,tmakedate,dd"
          	 + " order  by ManageCom,AgentCom"        	 
          	 ;   
	turnPage.queryModal(sql, LACommisionGrid);   	
	
}
function afterCodeSelect(codeName,Field)
{
	if(codeName == "BranchType2")
	{
          var  sql=" select enddate+1 day  from  lawagelog  where 1=1 and  wageno=(select max(wageno) from lawagelog where branchtype='"+fm.BranchType.value+"' "
                     +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom')
                     + getWherePart('BranchType', 'BranchType')
          	     + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	  {
           fm.all('StartDate').value= tArr[0][0];                                   
             
          }
	}
}

function beforeSubmit()
{
    var tSQL = "select 'Y' from ljaget where confdate is not null and otherno in (select receiptno from lacharge a where a.receiptno = ljaget.otherno"
           	 + getWherePart('a.ManageCom', 'ManageCom')
           	 + getWherePart('a.TMakeDate', 'TMakeDate')
	         + getWherePart('a.ContNo', 'ContNo');
  			 tSQL+=" )" ;
  	var strQueryResult = easyQueryVer3(tSQL, 1, 1, 1);
  	if(strQueryResult)
  	{
  	    alert("该笔保单已做了付费确认操作,不能进行手续费反冲操作！");
  	    return false;
  	}
         return true;
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
function selectPay(){
	if(!chkMulLine()){
		return false;
	}
	fm.all('fmAction').value='SELECTPAY';
	submitForm();
}
function allRecoil(){
	fm.all('fmAction').value='ALLRECOIL';
	submitForm();
}



