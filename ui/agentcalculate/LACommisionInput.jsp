<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LACommisionInput.jsp
//程序功能：
//创建日期：2003-02-16 15:12:44
//创建人  ：程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LACommision.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>  
  <%@include file="./LACommisionInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  
  <title>查询 </title>
</head>
<body  onload="initForm();" >
  <form action="./LACommisionSave.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACommisionGroup1);">
    
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLACommisionGroup1" style= "display: ''">
  <table  class= common>
    <tr  class= common> 
      <td  class= title> 佣金计算年月代码</td>
      <td colspan="3"  class= input> <input class=common name=WageNo > </td>
    </tr>
    <tr  class= common> 
      <td  class= title> 代理人编码</td>
      <td  class= input> 
        <input class= common name=AgentCode > 
      </td>
      <td  class= title>代理人组别</td>
      <td  class= input> <input class= common name=AgentGroup > </td>
    </tr>
    <input type=hidden name=Operator value=''>
  </table>
  <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();">
  <INPUT VALUE="重置" TYPE=button onclick="resetForm();">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACommisionGrid);">
    		</td>
    		
      <td class= titleImg> 查询结果 </td>
    	</tr>
    </table>
  	<Div  id= "divLACommisionGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLACommisionGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
<table  class= common>
       		<tr  class= common>
       		      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 	
      	</tr>
    	</table>				
  	</div>
  	<table class = common>
  	<tr class = common>
  	<INPUT class = common VALUE="保存本页面" TYPE=button onclick="updateClick();">
  	</tr>
  	</table>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
