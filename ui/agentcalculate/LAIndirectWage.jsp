<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAIndirectWage.jsp
//程序功能：
//创建日期：2017-08-25
//创建人  ：zhangyingying程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();
    System.out.println(CurDate);
%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LAIndirectWage.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LAIndirectWageInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LAIndirectWageSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		银代主管间接绩效比例区间提数
       		 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR  class = common>
           <TD  class = title>
            计算起期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="计算起期|DATE&NOTNULL" >
          </TD>
           <TD  class= title>
            计算止期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="计算止期|DATE&NOTNULL" >
          </TD>
        </TR>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=manageCom 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);" verify="管理机构|code:comcode" 
             ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>
        </TR>
      </table>
       <input type =button class=common  name=calculate value="提数计算" onclick="AgentWageCalSave();">
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
          <input type="hidden" class=input name=mOperate value="">
    </Div>
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		银代主管间接绩效比例维护
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR  class= common> 
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input 
            class ='codename' name ='ManageComName' elementtype=nacessary>
          </TD>   
           <TD  class= title>
            主键commisionsn
          </TD>
          <TD  class= input>
            <Input class=common  name=CommisionSN  >
          </TD>       
          </tr>
           <TR  class = common>
           <TD  class = title>
            财务实收起期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=tmakedate1 verify="财务实收起期|DATE&NOTNULL" >
          </TD>
           <TD  class= title>
            财务实收止期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=tmakedate2 verify="财务实收止期|DATE&NOTNULL" >
          </TD>
        </TR>
         <TR  class= common>			
		<TD class=title>展业类型</TD>
		<TD class=input><Input class='codeno' name=BranchType
		    verify="展业类型|notnull"
		    ondblclick="return showCodeList('branchtype',[this,BranchTypeName],[0,1]);"
			onkeyup="return showCodeListKey('branchtype',[this,BranchTypeName],[0,1]);"
			readonly><Input class=codename name=BranchTypeName elementtype=nacessary>
	   </TD>
	    <TD class=title>销售渠道</TD>
		<TD class=input><Input class='codeno' name=BranchType2
		    verify="销售渠道|notnull"
		    ondblclick="return showCodeList('branchtype2',[this,BranchType2Name],[0,1]);"
			onkeyup="return showCodeListKey('branchtype2',[this,BranchType2Name],[0,1]);"
			readonly><Input class=codename name=BranchType2Name elementtype=nacessary>
	   </TD>
	   </TR>
      </table></div><br>
          <input type =button class=cssbutton value="查 询" onclick="easyQueryClick();">    
          <input type =button class=cssbutton value="绩效比例修改" onclick="GrpRateUpdate();">
          <input type =button class=cssbutton value="折标保费修改" onclick="F1Update();">
          <input type="hidden" class=input name=tOperate value="">
    <Div  id= "divLACross1" style= "display: ''"> 
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 查询结果信息
  		</td>
  	</tr>
  </table>
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLAIndirectWageGrid">
  				</span> 
		    </td>
			</tr>
		</table>
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>