//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage = new turnPageClass();


//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}
       

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


// 查询按钮
function easyQueryClick()
{
	//查询修改
//	alert(111);
	var tReturn = getManageComLimitlike("a.ManageCom");
//	alert(tReturn);
	var old_AgentGroup=fm.all('AgentGroup').value;
	var new_AgentGroup="";
	if (old_AgentGroup!='')
	{
      strSQL_AgentGroup = "select AgentGroup from labranchgroup where 1=1 "
                         +"and BranchAttr='"+old_AgentGroup+"' and (state<>'1' or state is null)"
      var strQueryResult_AgentGroup = easyQueryVer3(strSQL_AgentGroup, 1, 1, 1);
      if (!strQueryResult_AgentGroup)
      {
      	alert("查询失败");
      	return false;
      }
      if (strQueryResult_AgentGroup)
      {
        var arrDataSet_AgentGroup = decodeEasyQueryResult(strQueryResult_AgentGroup);
       new_AgentGroup=arrDataSet_AgentGroup[0][0];
      }
    }
    var str_new_AgentGroup="";
    if (new_AgentGroup!='')
      str_new_AgentGroup=" and a.AgentGroup='"+new_AgentGroup+"' ";
 // 初始化表格
       initLACommisionGrid();
	
  // 书写SQL语句
 
  var strSQL = "";
	strSQL = "select getUniteCode(a.AgentCode),b.BranchAttr,a.WageNo,a.CalcDate,a.CommDate,a.DirectWage,a.CommisionSN,"
	      +"a.PolNo,a.MainPolNo,a.CValiDate,a.GetPolDate,a.SignDate,a.ReceiptNo,a.TransMoney,a.LastPayToDate,"
			  +"a.CurPayToDate,a.PayYear,a.CommDire,a.RiskCode,a.PayIntv,a.AgentGroup,a.AgentCom,a.AgentType"
			  +" from LACommision a,LABranchGroup b where a.AgentGroup=b.AgentGroup and (b.state<>'1' or b.state is null)"
	      + getWherePart('WageNo')
//	      + getWherePart('AgentCode')
//	      modify lyc 2014-11-27 统一工号
	       if(fm.all("AgentCode").value!=""){
	    	   strSQL +=" and AgentCode = getAgentCode('"+fm.AgentCode.value+"')";
	       }
	      + str_new_AgentGroup
	      + tReturn
	      +" order by agentcode";
	         
	         
	         

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //alert(strSQL);
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
  }
  
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  
  turnPage.arrDataCacheSet = arrDataSet1;

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = LACommisionGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  
  
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{	
    
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {  	
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
}           

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {         
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 




