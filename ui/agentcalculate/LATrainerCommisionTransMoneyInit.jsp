  <%
//程序名称：LATrainerCommisionTransMoneyInit.jsp
//程序功能：
//创建日期：2018-06-7
//创建人  ： WangQingMin
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  { 
  	fm.all('ManageCom').value = ''; 
  	fm.all('StartDate').value = ''; 
  	fm.all('EndDate').value = ''; 
    fm.all('BranchType').value = '<%=BranchType%>';     
    fm.all('BranchType2').value = '<%=BranchType2%>';
    fm.all('Calculate').disabled = false;
  }
  catch(ex)
  {
    alert("在LATrainerCommisionTransMoneyInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
function initForm()
{
  try
  {
    initInpBox();
  }
  catch(re)
  {
    alert("LATrainerCommisionTransMoneyInit.jsp-->InitForm函数中发生异常:初始化界面错误2!");
  }
}

</script>
