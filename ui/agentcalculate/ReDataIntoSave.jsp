<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ReDataIntoSave.jsp
//程序功能：
//创建日期：2003-08-01
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="java.text.SimpleDateFormat"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>
<%
  // 输出参数
  CErrors tError = null;
  String FlagStr = "";
  String Content = "";
  VData tVData = new VData();

  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp

  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");
    FlagStr = "Fail";
    Content = "页面失效,请重新登陆";
  }
  else //页面有效
  {
    String ManageCom = request.getParameter("ManageCom");
    String StartDate = request.getParameter("StartDate");
    String EndDate   = request.getParameter("EndDate");
    String tBranchType = request.getParameter("BranchType");
    String tCalTypeStr = request.getParameter("CalTypeStr");
    System.out.println("BranchType:"+tBranchType);
    System.out.println("EndDate1:"+EndDate);
    String Year  = StartDate.substring(0,StartDate.indexOf("-"));
    String Month = StartDate.substring(StartDate.indexOf("-")+1,StartDate.lastIndexOf("-"));
    if (Integer.parseInt(Month) < 10 && Month.trim().length() == 1)
      Month = "0"+ Month.trim();
    String YearMonth = Year+Month;
    String strInfo = "";
    System.out.println("YearMonth:"+YearMonth);
    FDate fDate = new FDate();
    
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    StartDate = sdf.format(fDate.getDate(StartDate));
    EndDate = sdf.format(fDate.getDate(EndDate));
    System.out.println("StartDate2:"+StartDate);
    System.out.println("EndDate2:"+EndDate);

   //佣金计算日志表
   LAWageLogSchema tLAWageLogSchema = new LAWageLogSchema();
   tLAWageLogSchema.setWageYear(Year);
   tLAWageLogSchema.setWageMonth(Month);
   tLAWageLogSchema.setStartDate(StartDate);
   tLAWageLogSchema.setEndDate(EndDate);
   LACommisionSchema tLACommisionSchema = new LACommisionSchema();
   tLACommisionSchema.setAgentCode(request.getParameter("AgentCode"));
   tLACommisionSchema.setPolNo(request.getParameter("PolNo"));
   tLACommisionSchema.setBranchAttr(request.getParameter("BranchAttr"));


//	 LAWageLogQueryUI tLAWageLogQueryUI = null;//查询
   AgentWageCalReUI tAgentWageCalReUI = null;//计算           
          
   	      tVData.clear();
              tVData.addElement(tGI);
              tVData.addElement(ManageCom);
              tVData.addElement(tLAWageLogSchema);
              tVData.addElement(tBranchType);        //temp code          
              tVData.addElement(tLACommisionSchema);
              tVData.addElement(tCalTypeStr);    
              tAgentWageCalReUI = new AgentWageCalReUI();
              tAgentWageCalReUI.submitData(tVData,"INSERT||AGENTWAGE");
              if (tAgentWageCalReUI.mErrors.needDealError()) //提数
              {
                 Content = StartDate+"提数失败，原因:" + tAgentWageCalReUI.mErrors.getFirstError();
                 FlagStr = "Fail";

              }


         

         try
         {
            if (!FlagStr.equals("Fail") && !tAgentWageCalReUI.mErrors.needDealError())
            {
                   Content = " 保存成功! ";
                   FlagStr = "Succ";
            }
         }
         catch(Exception ex)
         {
            Content = " 失败，原因:" + ex.toString();
            FlagStr = "Fail";
         }
         System.out.println(Content);

 }//页面有效区

%>
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>