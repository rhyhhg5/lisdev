var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();


// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initAssessMarkGrid();
	//alert(fm.all('AgentCode').value);
	// 书写SQL语句
	var strSQL = "";
        var tReturn = getManageComLimitlike("b.ManageCom");
	strSQL = "select a.AgentCode,a.Idx,a.AssessMarkType,a.DoneDate,a.AssessYM,a.ToolMark,a.ChargeMark,a.MeetMark,a.RuleMark,a.assessmark,a.StandbyMark1,a.StandbyMark2 from LAAssessMark a,LAAgent b where 1=1 "
	         +" And a.AgentCode = b.AgentCode "
	         
	         + getWherePart('a.BranchType','BranchType')
	         + getWherePart('a.BranchType2','BranchType2')
	         + getWherePart('AssessMarkType','AssessType')
	         + getWherePart('B.name','AgentCodeName','like')
	         + getWherePart('ToolMark','ToolMark','','1')
	         + getWherePart('ChargeMark','ChargeMark','','1')
	         + getWherePart('MeetMark','MeetMark','','1')
	         + getWherePart('RuleMark','RuleMark','','1')
	         + getWherePart('AssessMark','AssessMark','','1')
	         + getWherePart('AssessYM','AssessDate')
	         + getWherePart('b.AgentCode','AgentCode')
	         + tReturn;
//	 if(fm.all('AgentCodeName').value!=null && fm.all('AgentCodeName').value!='')
//	 {     
//	 	strSQL+=" and a.agent  
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AssessMarkGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}


function returnParent()
{
  var arrReturn = new Array();
	var tSel = AssessMarkGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{
				arrReturn = AssessMarkGrid.getRowData(tSel-1);
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();		
	}
}