<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LACommisionSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LACommisionSchema ttLACommisionSchema   = new LACommisionSchema();
  LACommisionSet tLACommisionSet = new LACommisionSet();
  LACommisionUI tLACommisionUI   = new LACommisionUI();

  //输出参数
  CErrors tError = null;
  String tOperate="UPDATE||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String cAgentCode = "";
  
  GlobalInput tG = new GlobalInput();
   //tG.Operator = "Admin";
   //tG.ComCode  = "001";
   //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");
  
  //取得信息
  int lineCount = 0;
  String arrCount[] = request.getParameterValues("LACommisionGridNo");
  String tAgentCode[] = request.getParameterValues("LACommisionGrid1");
  //   String tAgentGroup[] 后台完成
  String tWageNo[] = request.getParameterValues("LACommisionGrid3");
  String tCalcDate[] = request.getParameterValues("LACommisionGrid4");
  String tCommDate[] = request.getParameterValues("LACommisionGrid5");
  String tDirectWage[] = request.getParameterValues("LACommisionGrid6");
  String tCommisionSN[] = request.getParameterValues("LACommisionGrid7");
  String tPolNo[] = request.getParameterValues("LACommisionGrid8");
  String tMainPolNo[] = request.getParameterValues("LACommisionGrid9");
  //
  String tCValiDate[] = request.getParameterValues("LACommisionGrid10");
  String tGetPolDate[] = request.getParameterValues("LACommisionGrid11");
  String tSignDate[] = request.getParameterValues("LACommisionGrid12");
  
  String tReceiptNo[] = request.getParameterValues("LACommisionGrid13");
  String tTransMoney[] = request.getParameterValues("LACommisionGrid14");
  //
  String tLastPayToDate[] = request.getParameterValues("LACommisionGrid15");
  String tCurPayToDate[] = request.getParameterValues("LACommisionGrid16");
  String tPayYear[] = request.getParameterValues("LACommisionGrid17");
  
  String tCommDire[] = request.getParameterValues("LACommisionGrid18");
  String tRiskCode[] = request.getParameterValues("LACommisionGrid19");
  String tPayIntv[] = request.getParameterValues("LACommisionGrid20");
  //
  String tAgentCom[] = request.getParameterValues("LACommisionGrid22");
  String tAgentType[] = request.getParameterValues("LACommisionGrid23");
 
   
  lineCount = arrCount.length; //行数

  for(int i=0;i<lineCount;i++)
  {
    LACommisionSchema tLACommisionSchema = new LACommisionSchema();
    //tLACommisionSchema.setAgentCode(request.getParameter("AgentCode"));    
    //tLACommisionSchema.setSerialNo(i+1);    
	  //add by lyc 统一工号 2014-12-05
	  if(""!= tAgentCode[i]){
	  String cSql = "select agentcode from laagent where groupagentcode = '"+tAgentCode[i]+"' fetch first row only";
	   cAgentCode = new ExeSQL().getOneValue(cSql);
	  }
    tLACommisionSchema.setAgentCode(cAgentCode);
  
    tLACommisionSchema.setWageNo(tWageNo[i]);
    tLACommisionSchema.setCalcDate(tCalcDate[i]);
    tLACommisionSchema.setCommDate(tCommDate[i]);
    tLACommisionSchema.setDirectWage(tDirectWage[i]);
    //System.out.println("i:"+tDirectWage[i]);
    tLACommisionSchema.setCommisionSN(tCommisionSN[i]);
    tLACommisionSchema.setPolNo(tPolNo[i]);
    tLACommisionSchema.setMainPolNo(tMainPolNo[i]);
    
    tLACommisionSchema.setCValiDate(tCValiDate[i]);
    tLACommisionSchema.setGetPolDate(tGetPolDate[i]);
    tLACommisionSchema.setSignDate(tSignDate[i]);
    
    tLACommisionSchema.setReceiptNo(tReceiptNo[i]);
    tLACommisionSchema.setTransMoney(tTransMoney[i]);
    
    tLACommisionSchema.setLastPayToDate(tLastPayToDate[i]);
    tLACommisionSchema.setCurPayToDate(tCurPayToDate[i]);
    tLACommisionSchema.setPayYear(tPayYear[i]);
    
    tLACommisionSchema.setCommDire(tCommDire[i]);
    tLACommisionSchema.setRiskCode(tRiskCode[i]);
    tLACommisionSchema.setPayIntv(tPayIntv[i]);
    
    tLACommisionSchema.setAgentCom(tAgentCom[i]);
    tLACommisionSchema.setAgentType(tAgentType[i]);
    
    tLACommisionSet.add(tLACommisionSchema);
    //System.out.println("for:"+tCautionerName[i]);
  }
  System.out.println("end 担保人信息...");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  
  tVData.add(tG);
  //tVData.addElement(tLAAgentSchema);
  //tVData.addElement(tLATreeSchema);
  tVData.addElement(tLACommisionSet);
  try
  {
    tLACommisionUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLACommisionUI.mErrors;
    if (!tError.needDealError())
    {
        Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
        parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

