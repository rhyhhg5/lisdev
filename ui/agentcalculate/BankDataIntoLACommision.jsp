<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：DataIntoLACommision.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();    
    String BranchType=request.getParameter("BranchType");
    System.out.println(BranchType);
    String BranchType2=request.getParameter("BranchType2");
%>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="BankDataIntoLACommision.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="BankDataIntoLACommisionInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./BankDataIntoLACommisionSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		区间提数
       		 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom 
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,'8','to_char(length(trim(comcode)))',1);" verify="管理机构|notnull&code:comcode" 
             ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>          
           <TD class = title>
             上次计算止期
          </TD>
          <TD  class= input>
            <Input class=readOnly name=lastenddate readOnly>            
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
            计算起期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="计算起期|DATE&NOTNULL" >
          </TD>
           <TD  class= title>
            计算止期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="计算止期|DATE&NOTNULL" >
          </TD>
        </TR>
        <TR class=input>    
       </TR>
      </table>
       <input type =button class=common value="提数计算" onclick="AgentWageCalSave();">       
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">          
          <input type="hidden" class=input name=BranchType2 value="<%=BranchType2%>">          
    </Div>  

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>