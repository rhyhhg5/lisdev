//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //initArchieveGrid();
   
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("LAContFYCRateInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
  var tSel = ArchieveGrid.mulLineCount;		
  if( tSel == 0 || tSel == null )
  {
    alert("请先查询出保单的网点手续费信息!");
    return false;
  }
  for(var j=0;j<ArchieveGrid.mulLineCount;j++)
  {
    var trate=ArchieveGrid.getRowColData(j,11); 
    if (trate == null || trate =='')
       {
       	 alert("手续费比例不能为空！");
       	  return false ; 
       	}
       if (!isNumeric(trate) )
       {
       	  alert("手续费比例应该为数字！");
       	  return false ;  
       }

	  	if (trate<0||trate>1)
	  	{
    	 alert("保单中介手续费比例小于0或者大于1，不能进行保存！");
         return false;           
		}
	  if(trate.length>8)
       {
          alert("手续费比例长度不能大于8！");
       	  return false ;       	
       } 
   }
   return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{

	var strsql="select 1 from laratecharge where otherno='"
            +fm.all('ContNo').value+"'";
   var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
   if (strQueryResult){
    alert("修改操作，请选择修改按钮！");
    return false;
    }
  if(!beforeSubmit())
  {
    return false;
  }
	
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
   var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.fmtransact.value = "INSERT||MAIN" ;
  fm.submit();
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	var strsql="select 1 from laratecharge where otherno='"
            +fm.all('ContNo').value+"'";
   var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
   if (!strQueryResult){
    alert("新增操作，请选择保存按钮！");
    return false;
    }
	  if(!beforeSubmit())
  {
    return false;
  }
  //下面增加相应的代码
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}                    
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	var strsql="select 1 from laratecharge where otherno='"
            +fm.all('ContNo').value+"'";
   var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
   if (!strQueryResult){
    alert("没有保存，数据库中无记录，无法删除！");
    return false;
    }
  //下面增加相应的删除代码
  if(!beforeSubmit())
  {
    return false;
  }  
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function getname()
{
 var strsql="select name from laagent where agentstate<'03' "
            + getWherePart('AgentCode','AgentCode')
            + getWherePart('BranchType','BranchType')	
	    + getWherePart('BranchType2','BranchType2');
 var strQueryResult = easyQueryVer3(strsql, 1, 1, 1);
 //alert(strsql);
     if (!strQueryResult)
  {
    alert("没有此代理人！");

    fm.all('AgentCode').value  = '';
    fm.all('AgentCodeName').value  = '';
    return;
  }
   //查询成功则拆分字符串，返回二维数组
   
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);	    
  fm.all('AgentCodeName').value=tArr[0][0];
}
function easyQueryClick()
{
  if (verifyInput() == false)
  return false;	
  //校验保单是否属于银行代理的保单
  var SQL = "select grpcontno,salechnl from lccont where contno='"+fm.ContNo.value
  			+"' or grpcontno ='"+fm.ContNo.value+"'"
  			+" union "
  			+"select grpcontno,salechnl from lbcont where contno='"+fm.ContNo.value
  			+"' or grpcontno ='"+fm.ContNo.value+"'";
  var arrResult1 = new Array();
  arrResult1 = easyExecSql(SQL);
  if(arrResult1 == null||arrResult1[0][1]!='03')
  {
   alert("该保单不存在或者不是中介保单!");
   return false;
  }
  initArchieveGrid();
   var tReturn = getManageComLimitlike("b.ManageCom");
	//此处书写SQL语句	
	var strSql;
	if(arrResult1[0][0]=='00000000000000000000'){
		strSql = "select b.contno,b.riskcode,(select riskname  from  lmriskapp d  where d.riskcode=b.riskcode ), "
        +" b.managecom,(select name from ldcom where comcode=b.managecom), "
	    +" b.agentcom,(select name from lacom where agentcom=b.agentcom),db2inst1.getunitecode(b.agentcode),(select name from laagent where agentcode=b.agentcode),'',"
	    +" case when (select a.rate  from LARatecharge a  where  a.otherno=b.contno  and a.riskcode=b.riskcode) is null then 0 else  (select a.rate  from LARatecharge a  where  a.otherno=b.contno and a.riskcode=b.riskcode) end  from  lcpol b "
	    +" where 1=1  "
	    +tReturn
    	+ getWherePart('b.ContNo', 'ContNo')
    	+" union select b.contno,b.riskcode,(select riskname  from  lmriskapp d  where d.riskcode=b.riskcode ),"
	    +" b.managecom,(select name from ldcom where comcode=b.managecom), "
	    +" b.agentcom,(select name from lacom where agentcom=b.agentcom),db2inst1.getunitecode(b.agentcode),(select name from laagent where agentcode=b.agentcode),'',"
	    +" case when (select a.rate  from LARatecharge a  where  a.otherno=b.contno and a.riskcode=b.riskcode) is null then 0 else  (select a.rate  from LARatecharge a  where  a.otherno=b.contno and a.riskcode=b.riskcode) end from  lbpol b "
	    +" where 1=1       "
	    +tReturn
   		+ getWherePart('b.ContNo', 'ContNo')	    
    	+" order by riskcode " ;
	}else {
		strSql = "select b.grpcontno,b.riskcode,(select riskname  from  lmriskapp d  where d.riskcode=b.riskcode ), "
        +" b.managecom,(select name from ldcom where comcode=b.managecom), "
	    +" b.agentcom,(select name from lacom where agentcom=b.agentcom),db2inst1.getunitecode(b.agentcode),(select name from laagent where agentcode=b.agentcode),'',"
	    +" case when (select a.rate  from LARatecharge a  where  a.otherno=b.grpcontno  and a.riskcode=b.riskcode) is null then 0 else  (select a.rate  from LARatecharge a  where  a.otherno=b.grpcontno and a.riskcode=b.riskcode) end  from  lcgrppol b "
	    +" where 1=1  "
	    +tReturn
    	+ getWherePart('b.grpContNo', 'ContNo')
    	+" union select b.grpcontno,b.riskcode,(select riskname  from  lmriskapp d  where d.riskcode=b.riskcode ), "
	    +" b.managecom,(select name from ldcom where comcode=b.managecom), "
	    +" b.agentcom,(select name from lacom where agentcom=b.agentcom),db2inst1.getunitecode(b.agentcode),(select name from laagent where agentcode=b.agentcode),'',"
	    +" case when (select a.rate  from LARatecharge a  where  a.otherno=b.grpcontno and a.riskcode=b.riskcode) is null then 0 else  (select a.rate  from LARatecharge a  where  a.otherno=b.grpcontno and a.riskcode=b.riskcode) end from  lbgrppol b "
	    +" where 1=1       "
	    +tReturn
   		+ getWherePart('b.grpContNo', 'ContNo')	    
    	+" order by riskcode " ;
	}		     			    
  
   //alert(strSql);
 
  var arrResult = new Array();
  arrResult = easyExecSql(strSql);
  
  if (!arrResult) 
  {
    alert("查询失败！");
    return false;
  }
  displayMultiline(arrResult,ArchieveGrid);
   //查询成功则拆分字符串，返回二维数组 
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = ArchieveGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = ArchieveGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}             
