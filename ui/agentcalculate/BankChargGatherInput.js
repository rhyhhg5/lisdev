//程序名称：LAWageGatherInput.js
//程序功能：查询没有主管的单位的函数
//创建日期：2005-6-3 10:03
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

/*********************************************************************
 *  点击查询按钮响应事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	easyQuery();
}

/*********************************************************************
 *  描述  ：  执行回退操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{
	//判断画面输入内容是否合法
  if(verifyInput2() == false) return false;
  
  //jiaoyan
  var  sql=" select count(1) from  LACharge  "
          +"  where 1=1 and  ChargeType ='99'  AND ManageCom LIKE '"+fm.ManageCom.value+"%' "
          +"  AND BranchType = '3' AND  BranchType2 = '01' AND TMakeDate >='"+fm.EndDate.value+"'  "
          +" and ( chargestate='1' or chargestate='3' ) ";
  //alert(sql);
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  var tcount= tArr[0][0];                                   
  if(tcount>0)
  {
  alert("手续费已经结算，不可进行手续费计算回退重新计算手续费！");
  return false;
  } 

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
  fm.submit();
}

/*********************************************************************
 *  回退操作结束后的处理
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	 	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		// 刷新查询结果
		//easyQueryClick();		
	}
}