<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LaratecommisionSetSave.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     	更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcalculate.*"%>

<%
  //输出参数
  CErrors tError = null;
  String Content = "";
  String FlagStr = "Fail";
  LAACChargeReturnUI tLAACChargeReturnUI = new LAACChargeReturnUI();
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
  if ( tGI==null )  {
%>
<html>
  <script language="javascript">
    parent.fraInterface.afterSubmit("Fail","网页超时或者没有操作员信息");
  </script>
</html>
<%
  }
  else
  {
    try
    {
      String tAction = request.getParameter("fmAction");
      LAChargeSet tSetU = new LAChargeSet();	//用于更新
	  // 准备传输数据 VData
      VData tVData = new VData();
      FlagStr="";
      Content="";
      tVData.add(tGI);
      //创建数据集
      if(tAction.equals("SelectReturn")){
    	// LAChargeGridNo
      	String tChk[] = request.getParameterValues("InpLAChargeGridChk"); 
      	String tCommisionSn[]= request.getParameterValues("LAChargeGrid12");
      	for(int i=0;i<tChk.length;i++){
      		if(tChk[i].equals("1")){
          		//创建一个新的Schema
      	  		LAChargeSchema tSch = new LAChargeSchema();
         		tSch.setBranchType("2");
          		tSch.setBranchType2("02");
          		tSch.setChargeType("51");
          		tSch.setCommisionSN(tCommisionSn[i].trim());
      	  		tSetU.add(tSch);
       		}
      	}
      	tVData.add(tSetU);
        tLAACChargeReturnUI.submitData(tVData,tAction);
      }else{
      	String tManageCom=request.getParameter("ManageCom");
        String tTMakeDate=request.getParameter("TMakeDate");
      	//String tAgentCom=request.getParameter("AgentCom");
      	String tGrpContNo=request.getParameter("GrpContNo");
      	//String tRiskcode = request.getParameter("RiskCode");
      	//String tWrapCode=request.getParameter("WrapCode");
      	tVData.add(1,tManageCom);
      	tVData.add(2,tTMakeDate);
      	tVData.add(3,tGrpContNo);
      	//tVData.add(4,tRiskcode);
      	//tVData.add(5,tWrapCode);
        tLAACChargeReturnUI.submitData(tVData,tAction);
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
      FlagStr = "Fail";
      Content = FlagStr + "操作失败，原因是：" + ex.toString();
    }
   }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLAACChargeReturnUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }  
  
%>
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
