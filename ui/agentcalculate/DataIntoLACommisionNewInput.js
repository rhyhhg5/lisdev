//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false;
  
  var End = fm.EndDate.value;
	var cur = fm.CurrentDate.value;	
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交
  fm.calculate.disabled=true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行提数         
function AgentWageCalSave()
{
  submitForm();	
}   

function easyQueryClick()
{
    //首先检验录入框
  if(""==fm.all("WageNo").value) 
  {
	  alert("薪资月不能为空");
	  return false;
  }
  var  strSQL="select a.wageno,a.sourcetablename,a.datatype,b.describe,a.enddate,a.state,current date -1 days,b.extractdatatype   " 
	  + " from lawagenewlog a,lacommisionconfig b "
	  + " where a.sourcetablename = b.sourcetablename " 
  	  + " and a.datatype = b.datatype " 
  	  +	" and wageno ='"+fm.all("WageNo").value+"' with ur "
  turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	if(!turnPage.strQueryResult)
	{  
		alert("没有符合条件的信息！"); 
		return false;
	}
	//查询成功则拆分字符串，返回二维数组
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
	  //turnPage.arrDataCacheSet = chooseArray(tArr,[0,2,4,6,10])
	  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	  turnPage.pageDisplayGrid = LAWageNewLogGrid;

	  //保存SQL语句
	  turnPage.strQuerySql     = strSQL;

	  //设置查询起始位置
	  turnPage.pageIndex       = 0;

	  //在查询结果数组中取出符合页面显示大小设置的数组
	 arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	 //tArr=chooseArray(arrDataSet,[0,2,4,6,10])
	  //调用MULTILINE对象显示查询结果

	  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	  //displayMultiline(tArr, turnPage.pageDisplayGrid);
}
function updateState()
{
	if(""==fm.all("WageNo").value) 
	{
		alert("薪资月不能为空");
		return false;
	}
	var iCount = 0;
	var tExtractDataTypes ="";
	var rowNum = LAWageNewLogGrid.mulLineCount;
	for(i=0;i<rowNum;i++)
	{
		if(LAWageNewLogGrid.getChkNo(i))
		{
			tExtractDataTypes = tExtractDataTypes+"'"+LAWageNewLogGrid.getRowColData(i,8)+"',"
			iCount++;
		}
	}
	if(0==iCount)
	{
		alert("请选择一条记录进行操作");
		return false;
	}
	tExtractDataTypes = tExtractDataTypes.substring(0,tExtractDataTypes.length-1);
	var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all("operator").value="update";
	fm.all('configwhereSql').value =tExtractDataTypes;
    fm.action="DataIntoLACommisionNewSave.jsp";
	//fm.action="../f1print/RateCommisionDownloadXLS.jsp";
	//fm.all('op').value = '';
	fm.submit();

}
function AgentWageCalAllData()
{
	if(""==fm.all("TMakeDate").value) 
	{
		alert("全部补提时，补提日期不能为空");
		return false;
	}
	
	var tExtractDataTypes = "select extractdatatype from lacommisionconfig where state ='1' ";
	var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all("operator").value="all";
	fm.all('configwhereSql').value =tExtractDataTypes;
    fm.action="DataIntoLACommisionNewSave.jsp";
	//fm.action="../f1print/RateCommisionDownloadXLS.jsp";
	//fm.all('op').value = '';
	fm.submit();
}
function AgentWageCalOneData()
{
	if(""==fm.all("FetchBussTypeCode").value) 
	{
		alert("单独补提时，提取业务类型不能为空");
		return false;
	}
	var tMakedate = fm.all("TMakeDate").value;
	if(""==tMakedate) 
	{
		alert("单独补提时，补提日期不能为空");
		return false;
	}
	var tCurrentDate = getCurrentDate("-");
	if(2!=compareDate(tMakedate,tCurrentDate))
	{
		alert("补提日期不能大于昨天 ！");
		return false;
	}
	var tExtractDataTypes =  fm.all('FetchBussTypeCode').value;
	var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all("operator").value="one";
	fm.all('configwhereSql').value ="'"+tExtractDataTypes+"'";
    fm.action="DataIntoLACommisionNewSave.jsp";
	//fm.action="../f1print/RateCommisionDownloadXLS.jsp";
	//fm.all('op').value = '';
	fm.submit();
}

 
