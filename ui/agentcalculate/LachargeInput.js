//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在DataIntoLACommision.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 //alert("*************");
	fm.all('divSpanLACommisionGrid').style.display='';
	fm.all('divSpanLACommisionGrid1').style.display='none';
    //首先检验录入框
  if(!verifyInput()) return false;   
 var managecom = fm.all('ManageCom').value;
 var tagentcom ='';
  if('8632'==managecom.substr(0,4))
  {
	  if(""==fm.all('AgentCom').value||null==fm.all('AgentCom').value)
	  {
		  alert("江苏中介机构手续费结算时需要录入中介机构");
		  return false;
	  }
	  if(""==fm.all('ContType').value||null==fm.all('ContType').value)
	  {
		  alert("江苏中介机构手续费结算时需要录入团个标记");
		  return false;
	  }
  }
  //alert("@@@@@@");
  var tContType = fm.all('ContType').value;
  //alert("%%%:"+tContType);
  var tBackSign = fm.all('BackSign').value;
  // alert("^^^^:"+tBackSign);
   var  sql='';
   if(fm.all('BranchType').value=='1'&&fm.all('BranchType2').value=='02'){
         sql= "select  a.managecom,(select name from ldcom where comcode=a.ManageCom) aa,a.AgentCom,"
  			+"(select name from lacom where agentcom=a.agentcom) bb,"
  			+"(case when a.grpcontno='00000000000000000000' then a.ContNo else a.grpcontno end) as contno,a.RiskCode,a.TransMoney," 
  			//首期手续费比率，首期手续费;续期手续费比率，续期手续费
  			+"coalesce((select FirConChargeRate  from lachargefee where commisionsn = a.commisionsn and exists( select 'x' from lacommision where commisionsn = a.commisionsn and payyear = 0 and Renewcount >= 0 union select 'x' from lacommision where commisionsn = a.commisionsn and payyear > 0 and Renewcount = 0 )),0),coalesce((select FirConCharge from lachargefee where commisionsn = a.commisionsn and exists (select 'x' from lacommision where commisionsn = a.commisionsn and payyear = 0 and Renewcount >= 0 union select 'x' from lacommision where commisionsn = a.commisionsn and payyear > 0 and Renewcount = 0 )),0),"
  		    +"case when a.grpcontno='00000000000000000000' then (select signdate from lccont where contno=a.contno) else (select signdate from lcgrpcont where grpcontno=a.grpcontno) end cc,"
  			+"a.tmakedate,a.receiptno,a.transtype,"
  			+"(select codename from ldcode where codetype='transtype' and code=a.transtype )dd,a.commisionsn ee"
  			+",(select completetime from db2inst1.returnvisittable where policyno=a.contno and returnvisitflag in ('1','4')  fetch first 1 rows only ) ff "
  			+" from LACharge a where a.chargetype='15' and a.chargestate='0' "
             + " and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' " 
             + getWherePart('a.ManageCom', 'ManageCom','like')
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('a.AgentCom', 'AgentCom')
          	 +getWherePart('a.ContNo', 'ContNo')
          	 ;
  			if(tContType=='0')
  			{
  				  sql+= " and exists(select 1 from lccont where grpcontno=a.contno and ContType='2' union select 1 from lbcont where grpcontno=a.contno and ContType='2' union select 1 from lpcont where grpcontno=a.contno and ContType='2')" 
  			}
  			else if(tContType=='1')
  			{
  				sql+= " and exists(select 1 from lccont where contno=a.contno and ContType='1' union select 1 from lbcont where contno=a.contno and ContType='1' union select 1 from lpcont where contno=a.contno and ContType='1')" 
  			}
             sql+= " order  by ManageCom,AgentCom,tmakedate  "        	 
           ;   
     }
     else{
         sql="select managecom,aa,agentcom,bb,contno,riskcode,sum(transmoney),chargerate,"
  			+"sum(charge),cc,tmakedate,receiptno,transtype,dd,commisionsn ,ff "
  			+"from ("
  			+"select a.managecom,(select name from ldcom where comcode=a.ManageCom) aa,"
  			+"a.AgentCom,(select name from lacom where agentcom=a.agentcom) bb,"
  			+"a.ContNo,a.RiskCode,a.TransMoney,a.ChargeRate,a.Charge,"
  		    +"case when a.grpcontno='00000000000000000000' then (select signdate from lccont where contno=a.contno) else (select signdate from lcgrpcont where grpcontno=a.contno) end cc,"
  			+"a.tmakedate,a.receiptno,a.transtype,"
  			+"(select codename from ldcode where codetype='transtype' and code=a.transtype)dd,a.commisionsn  "
  			+",(select completetime from db2inst1.returnvisittable where policyno=a.contno and returnvisitflag in ('1','4')  fetch first 1 rows only ) ff "
  			+" from LACharge a where a.chargestate='0' "
            +" and a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' "
            +" and a.branchtype='"+fm.all('BranchType').value+"' and a.branchtype2='"+fm.all('BranchType2').value+"'"
            +getWherePart('a.ManageCom', 'ManageCom')
			sql+=getWherePart('a.AgentCom', 'AgentCom');
			sql+=getWherePart('a.ContNo', 'ContNo')
          	;
          	if(fm.all('BranchType').value=='2'&&fm.all('BranchType2').value=='02'){
          	sql+=" and a.chargetype='51'"
          	}else if(fm.all('BranchType').value=='5'&&fm.all('BranchType2').value=='01'){
          	 sql+=" and a.chargetype='55'"
          	}else if(fm.all('BranchType').value=='3'&&fm.all('BranchType2').value=='01'){
          	 sql+=" and a.chargetype='99'"
          	}
			  if(tContType=='0')
	  			{
	  				 sql+= " and exists(select 1 from lccont where grpcontno =a.grpcontno and conttype ='2' union select 1 from lbcont where grpcontno =a.grpcontno and conttype ='2' union select 1 from lpcont where grpcontno =a.grpcontno and conttype ='2' )";
	  			}
	  			else if(tContType=='1')
	  			{
	  				sql+= " and exists(select 1 from lccont where contno =a.contno and conttype ='1' union select 1 from lbcont where contno =a.contno and conttype ='1' union select 1 from lpcont where contno =a.contno and conttype ='1' )";
	  			}
             sql+=")as hh " ;
             
             if(tBackSign =='1')
             {
            	 sql += " where ff is not null ";
             }else if(tBackSign == '2')
             {
            	 sql += " where ff is null ";
             }else if(tBackSign == '0')
             {
            	 sql += " ";
             }
             	sql +=	" group by managecom,aa,agentcom,bb,contno,riskcode,chargerate,cc,tmakedate,receiptno,transtype,dd,commisionsn,ff  "
  			+ " order  by ManageCom,AgentCom,tmakedate"        	 
          	;   
         } 
	turnPage.queryModal(sql, LACommisionGrid);   	
	//alert("$$$:"+sql);
}
function afterCodeSelect(codeName,Field)
{
	if(codeName == "BranchType2")
	{
          var  sql=" select enddate+1 day  from  lawagelog  where 1=1 and  wageno=(select max(wageno) from lawagelog where branchtype='"+fm.BranchType.value+"' "
                     +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom')
                     + getWherePart('BranchType', 'BranchType')
          	     + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	  {
           fm.all('StartDate').value= tArr[0][0];                                   
             
          }
	}
}

function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("财务结算起期必须小于等于财务结算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}
function chkMulLine(){
	//alert("enter chkmulline");
	var i;
	var iCount = 0;
	var rowNum = LACommisionGrid.mulLineCount;
	for(i=0;i<rowNum;i++){
		if(LACommisionGrid.getChkNo(i)){
			iCount++;
		} 
	}
	if(iCount == 0){
		alert("请选择要结算的记录!");
		return false
	}
	return true;
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#"+fm.all('BranchType').value +"# and BranchType2=#"+fm.all('BranchType2').value+"# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}
function selectPay(){
	if(!chkMulLine()){
		return false;
	}
	fm.all('fmAction').value='SELECTPAY';
	submitForm();
}
function allPay(){
	fm.all('fmAction').value='ALLPAY';
	var tContType = fm.all('ContType').value;
    var tBackSign = fm.all('BackSign').value;
	submitForm();
}




