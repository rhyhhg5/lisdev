<%
//程序名称：LAWrapChargeInit.jsp
//程序功能：
//创建日期：2012-11-07 09:54:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {
   fm.all('ManageCom').value = '';
   fm.all('StartDate').value = '';
   fm.all('EndDate').value = '';
   fm.all('BranchType').value = '<%=BranchType%>';
   fm.all('BranchType2').value = '<%=BranchType2%>';
   
  }
  catch(ex)
  {
    alert("在LAWrapChargeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LAWrapChargeInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();   
    initWrapChargeGrid(); 
  }
  catch(re)
  {
    alert("LAWrapChargeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化ComGrid
 ************************************************************
 */
function initWrapChargeGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="管理机构编码";         //列名
    iArray[1][1]="100px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         //列名
    iArray[2][1]="100px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="套餐编码";         //列名
    iArray[3][1]="100px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="套餐名称";         //列名
    iArray[4][1]="100px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="保费";         //列名
    iArray[5][1]="100px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[6]=new Array();
    iArray[6][0]="手续费";         //列名
    iArray[6][1]="100px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="手续费状态";         //列名
    iArray[7][1]="100px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
 
    WrapChargeGrid = new MulLineEnter( "fm" , "WrapChargeGrid" ); 

    //这些属性必须在loadMulLine前
    WrapChargeGrid.mulLineCount = 0;   
    WrapChargeGrid.displayTitle = 1;
    WrapChargeGrid.canSel=0;
    WrapChargeGrid.hiddenPlus = 1;
    WrapChargeGrid.hiddenSubtraction = 1;
    WrapChargeGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
        alert("初始化WrapChargeGrid时出错："+ ex);
  }
}

</script>