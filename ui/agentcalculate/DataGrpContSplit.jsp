<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：DataIntoLACommision.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String tBranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    if (tBranchType==null || tBranchType.equals(""))
    {
       tBranchType="";
    }
    if (BranchType2==null || BranchType2.equals(""))
    {
       BranchType2="";
    }
    String CurDate = PubFun.getCurrentDate();
    System.out.println(CurDate);
%> 
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="DataGrpContSplit.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="DataGrpContSplitInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./DataGrpContSplitSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		区间保单拆分
       		 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
       
       <TR class = common >
         <TD class= title>
          团体合同号
        </TD>
        <TD class= input>
          <Input class=common name=GrpContNo verify="团体合同号|NOTNULL"  elementtype=nacessary >
        </TD>
          <TD  class= title>
            保单拆分年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexNo verify="拆分年月|NOTNULL"  elementtype=nacessary  >
          </TD>
       </TR>  
      <!--TR class= common>
			  <TD class= title>
          所属管理机构
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=ManageCom>
        </TD>
        <TD class= title>
          销售机构
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=BranchAttr >
        </TD>
      </TR>
      <TR class= common>
			  <TD class= title>
          投保客户编号
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=AppntNo>
        </TD>
        <TD class= title>
          投保客户名称
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=AppntName>
        </TD>
      </TR>
      <TR class= common>
			  <TD class= title>
          保费
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=TransMoney>
        </TD>
        <TD class= title>
          险种个数
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=RiskNum>
        </TD>
      </TR>
      <TR class= common>
        <TD class= title>
          缴费日期
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=CurPayToDate>
        </TD>
			  <TD class= title>
          签单日期
        </TD>
        <TD class= input>
          <Input class=readonly readonly name=SignDate>
        </TD>
      </TR-->
    
      </table>
      
    </Div> 
     <input type=button class=cssButton  value="查  询"  onclick="easyQueryClick();">
       <input type =button class=cssButton value="保单拆分" onclick="AgentWageCalSave();">          
       <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
      <p> <font color="#ff0000">注：保单拆分将按照保单分配中设定的比例对此合同在保单拆分月发生的保费情况进行拆分。 </font></p>
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
    		</td>
    		<td class= titleImg>保单基本信息（销售数据）</td>
    	</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>     
         <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>  
     <input type=hidden name=BranchType value='<%=tBranchType%>'> 
    <input type=hidden name=BranchType2 value='<%=request.getParameter("BranchType2")%>'>

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>