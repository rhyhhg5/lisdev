<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：DataIntoLACommision.jsp
//程序功能：
//创建日期：2003-06-24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    String CurDate = PubFun.getCurrentDate();    
    String BranchType=request.getParameter("BranchType");
    String BranchType2=request.getParameter("BranchType2");
    String strSql= " 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'"; 
%>
<script>
	 var  mBranchType =  <%=BranchType%>;
	 //var  mBranchType2 =  <%=BranchType2%>;
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>   
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>     
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="LAACChargeQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="LAACChargeQueryInit.jsp"%>
</head> 
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">
  <input type=hidden name=querySQL value=""> 
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		手续费查询
       	 </td>
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
        <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1]);" 
            onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1]);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>        
           <TD class = title>
             中介机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom ondblclick="return getAgentComName(this,AgentComName);" onkeyup="return getAgentComName(this,AgentComName);"  ><Input class=codename name=AgentComName readOnly >
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
           财务结算起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="起始年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            财务结算止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="终止年月|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          
        </TR> 
        <tr class=common>	
  		  <td  class= title>结算状态</td>
          <TD  class= input> <Input class= "codeno" name=ChargeState CodeData="0|^1|已结算|^0|未结算|^X|不结算" ondblclick="return showCodeListEx('F01list',[this,StateName],[0,1]);" onkeyup="return showCodeListKeyEx('F01list',[this,StateName],[0,1]);" onchange="" readonly=true><input class=codename name=StateName  readonly=true ></TD> 	
  		<TD  class = title>
           保险合同号
          </TD>
          <TD  class= input>
     		<Input class= 'common' name=GrpContNo >
    	  </TD>
    	  
    	 
  		</tr>  
         <TD  class = title>
           卡单批单号
          </TD>
          <TD  class= input>
     		<Input class= 'common' name=CardNo >
    	  </TD>
    	  <td  class= title>财务实付状态</td>
          <TD  class= input> <Input class= "codeno" name=ConfState CodeData="0|^1|已付|^0|未付|" ondblclick="return showCodeListEx('ConfState',[this,ConfStateName],[0,1]);" onkeyup="return showCodeListKeyEx('ConfState',[this,ConfStateName],[0,1]);" onchange="" ><input class=codename name=ConfStateName  readonly=true ></TD>
    	 <tr>
         <TD  class = title>
           保单类型
          </TD>
          <TD  class= input> <Input class= "codeno" name=ContFlag CodeData="0|^0|新单|^1|保全|" ondblclick="return showCodeListEx('ContFlag',[this,ContFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('ConfState',[this,ContFlagName],[0,1]);" onchange="" ><input class=codename name=ContFlagName  readonly=true ></TD>
    	  <td class=title>投保单位</td>
    	        <TD  class= input> 
                    <Input class='common' name=AppntName >  
          </TD>
    	 </tr>
    	 <tr>
    	    <td class=title>中介专员</td>
    	  <td><Input class= 'common' name=GroupAgentcode ></td>
    	 </tr>           
      </table>       
          
         <Table class="common" align=center>
  			<Tr>
    			<Td class=button>
      				<INPUT class=cssButton VALUE="查  询"  TYPE=button onclick="easyQueryClick()">
      				<INPUT class=cssButton VALUE="打  印"  TYPE=button onclick="doDownLoad()">
    			</Td>
  			</Tr>
		</Table>
         <!-- 
         <input type =button class=common value="银保保费明细查询" onclick="easyQueryClick1();">
         <input type =button class=cssButton value="查询" onclick="easyQueryClick();">
          --> 
          <input type="hidden" class=input name=CurrentDate value="<%=CurDate%>">
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">
          <input type="hidden" class=input name=BranchType2 value="<%=BranchType2%>">           
    	  
    </Div>
    <p> <font color="#ff0000">注：为防止系统压力过大，用户点击打印后请耐心等待系统后台处理！不要重复点击“打印”按钮以防用户被锁定</font></p> 
     <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLACross1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <Div id= "divSpanLACommisionGrid" align=center style= "display: '' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
	<Div id= "divSpanLACommisionGrid1" align=center style= "display: 'none' ">
      <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLACommisionGrid1">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
    	<Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 
      </div>
      
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>