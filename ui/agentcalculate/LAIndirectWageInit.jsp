<%
//程序名称：LAIndirectWageInit.jsp
//程序功能：
//创建日期：2017-08-25
//创建人  ：zhangyingying程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
String CurrentDate = PubFun.getCurrentDate();
String CurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = '';
    fm.all('StartDate').value = '';
    fm.all('EndDate').value = '';
    fm.all('BranchType').value = '';
    fm.all('BranchType2').value = '';
    fm.all('manageCom').value = '';
    fm.all('tmakedate1').value = '';
    fm.all('tmakedate2').value = '';
    fm.all('CommisionSN').value = '';
  }
  catch(ex)
  {
    alert("LAIndirectWageInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initLAIndirectWageGrid(); 
  }
  catch(re)
  {
    alert("LAIndirectWageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var LAIndirectWageGrid;
function initLAIndirectWageGrid() {  
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="40px";         		//宽度
    iArray[0][3]=100;         		//最大长度
    iArray[0][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[1]=new Array();
    iArray[1][0]="commisionsn";         		//列名
    iArray[1][1]="60px";         		//宽度
    iArray[1][3]=100;         		//最大长度
    iArray[1][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[2]=new Array();
    iArray[2][0]="银代团队主管首期";         		//列名
    iArray[2][1]="60px";         		//宽度
    iArray[2][3]=100;         		//最大长度
    iArray[2][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[3]=new Array();
    iArray[3][0]="团队首期绩效比例";         		//列名
    iArray[3][1]="60px";         		//宽度
    iArray[3][3]=100;         		//最大长度
    iArray[3][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[4]=new Array();
    iArray[4][0]="折标保费";         		//列名
    iArray[4][1]="60px";         		//宽度
    iArray[4][3]=100;         		//最大长度
    iArray[4][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[5]=new Array();
    iArray[5][0]="J奖励方案";         		//列名
    iArray[5][1]="60px";         		//宽度
    iArray[5][3]=100;         		//最大长度
    iArray[5][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();
    iArray[6][0]="J奖励方案比例";         		//列名
    iArray[6][1]="60px";         		//宽度
    iArray[6][3]=100;         		//最大长度
    iArray[6][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="提数日期";         		//列名
    iArray[7][1]="60px";         		//宽度
    iArray[7][3]=100;         		//最大长度
    iArray[7][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="展业类型";         		//列名
    iArray[8][1]="60px";         		//宽度
    iArray[8][3]=100;         		//最大长度
    iArray[8][4]=0;         		//是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="销售渠道";         		//列名
    iArray[9][1]="60px";         		//宽度
    iArray[9][3]=100;         		//最大长度
    iArray[9][4]=0;         		//是否允许录入，0--不能，1--允许
    
   LAIndirectWageGrid = new MulLineEnter( "fm" , "LAIndirectWageGrid" ); 
    //这些属性必须在loadMulLine前
 
    LAIndirectWageGrid.mulLineCount = 10;   
    LAIndirectWageGrid.displayTitle = 1;
    LAIndirectWageGrid.hiddenPlus = 1;
    LAIndirectWageGrid.hiddenSubtraction = 1;
    LAIndirectWageGrid.canSel = 0;
    LAIndirectWageGrid.canChk = 1;
  
    LAIndirectWageGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
  }
  catch(ex) {
    alert(ex);
  }
}

</script>
