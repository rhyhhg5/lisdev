//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
var hiddenStartDate="";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeCalculate()) return false;

  var startDate=fm.all('StartDate').value;
  var endDate=fm.all('EndDate').value;

  if(endDate<startDate)
  {
    alert("计算起期必须小于等于计算止期！");
    fm.all('EndDate').value = '';
    return false;
  }
  if(hiddenStartDate!=""&&hiddenStartDate!=null)
  {
     if(fm.all('StartDate').value!=hiddenStartDate)
     {
     alert("'计算起期'必须为'上次计算止期'的下一天！");
     fm.all('StartDate').value = hiddenStartDate;
     return false;
     }
  }
   
  var End = fm.EndDate.value;
	var cur = fm.CurrentDate.value;	
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("LAECInterCalChargeInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 function afterCodeSelect(codeName,Field)
{
	if(codeName == "ComCode")
	{
          var  sql=" select enddate,enddate+1 day  from  lachargelog  where 1=1 and  chargecalno=(select max(chargecalno) from lachargelog where branchtype='"+fm.BranchType.value+"' "
                     +"  and  branchtype2='"+fm.BranchType2.value+"' and managecom='"+fm.ManageCom.value+"') "
                     + getWherePart('ManageCom', 'ManageCom')
                     + getWherePart('BranchType', 'BranchType')
          	         + getWherePart('BranchType2', 'BranchType2');
          var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
       
   //查询成功则拆分字符串，返回二维数组
   
          var tArr = new Array();
          tArr = decodeEasyQueryResult(strQueryResult);
	
          if( tArr != null )
	    {
           fm.all('StartDate').value= tArr[0][1];                                   
           fm.all('lastEndDate').value= tArr[0][0];  
           hiddenStartDate=tArr[0][1];       
          }
          else 
          {
           fm.all('StartDate').value= "2018-01-01";                                   
           hiddenStartDate= fm.all('StartDate').value; 
          }
	}
}   

    
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行提数         
function AgentWageCalSave()
{
  submitForm();	
}  


function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("计算起期必须小于等于计算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}
function beforeCalculate()
{
  var startDate=fm.all('StartDate').value;
  var endDate=fm.all('EndDate').value;
  

   var temp1=startDate.split("-");
   var tEmpDate1=new Date(temp1[0],temp1[1],temp1[2]);
   
   var temp2=endDate.split("-");
   var tEmpDate2=new Date(temp2[0],temp2[1],temp2[2]);
  
   var tYear1 = parseInt(temp1[0]);
   var tYear2 = parseInt(temp2[0]);
   var tMonth1 = parseInt(temp1[1]);
   var tMonth2 = parseInt(temp2[1]);

   if(tYear1!=tYear2||(tYear1=tYear2&&tMonth1!=tMonth2)){
      alert("手续费不能跨年或跨月进行计算！");
      return false;
    }
    return true;
    
}
