<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PolTxtCreateChk.jsp
//程序功能：保险稽核系统文件生成
//创建日期：2004-07-08 11:10:36
//创建人  ：wanglong
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.audit.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
  
  	//接收信息
    //接收信息
  	TransferData tTransferData = new TransferData();
    String tStatDate = request.getParameter("Bdate");
	String tEndDate = request.getParameter("Edate");
	String tOptear = request.getParameter("Opreat");
	
	System.out.println("tStatDate:"+tStatDate);
	System.out.println("tEndDate:"+tEndDate);
	System.out.println("tOptear:"+tOptear);

	if(tStatDate != null && tEndDate != null  )
	{
	    tTransferData.setNameAndValue("StatDate",tStatDate);
	    tTransferData.setNameAndValue("EndDate",tEndDate);
		  tTransferData.setNameAndValue("Operate",tOptear);
	}
	else
    {
		Content = "传输数据失败!";
		flag = false;
	}

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData );
		tVData.add( tG );
		
		// 数据传输
  		PolChkTxtCreateUI tPolChkTxtCreateUI   = new PolChkTxtCreateUI();
  			System.out.println("before PolChkTxtCreateUI!!!!");			
		 if (!tPolChkTxtCreateUI.submitData(tVData))//保监会报表工作流XML文件生成0000000202
		{
			int n = tPolChkTxtCreateUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			System.out.println("Error: "+tPolChkTxtCreateUI.mErrors.getError(i).errorMessage);
			Content = "  保险稽核TXT文件生成失败，原因是: " + tPolChkTxtCreateUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPolChkTxtCreateUI.mErrors;
		    if (!tError.needDealError())
		    {  
		      if(tOptear!="1") {
		        Content = " 保险稽核文件生成成功! ";
		      }
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
