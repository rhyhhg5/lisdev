<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UWAutoInit.jsp
//程序功能：个人自动核保
//创建日期：2002-06-19 11:10:36
//创建人  :ck
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('StatYear').value = '';
    fm.all('StatMonth').value = '';
}
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在ProposalQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
//    initInpBox();
//    initSelBox();    
	initPolGrid();
	initContent("A");
//	showCodeName();
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			  //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		  //列宽
		iArray[0][2]=10;            			  //列最大值
		iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		

		iArray[1]=new Array();  
		iArray[1][0]="模块类型";         		  //列名
		iArray[1][1]="0px";            		  //列宽
		iArray[1][2]=100;            			  //列最大值
		iArray[1][3]=2;              			  //是否允许输入,1表示允许，0表示不允许
	//  iArray[1][10]="ItemType"; // 名字最好有唯一性
	//	iArray[1][11]="0|^A01|新保单信息表^A02|缴费信息表^A03|批单信息表^A04|给付信息表^A05|报案信息表^A06|赔案信息表^A07|险种代码表^A08|营销员信息表^A09|中介机构信息表^A10|分支机构信息表^A11|员工信息表^A12|银保专管员信息表^A13|财务总账科目代码表^A14|财务明细科目代码表^A15|财务凭证信息表^A16|单证代码表^A17|单证信息表";
		
	    
		iArray[2]=new Array();
		iArray[2][0]="模块名称";          	      //列名
		iArray[2][1]="80px";            		  //列宽
		iArray[2][2]=100;            			  //列最大值
		iArray[2][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

		
		iArray[3]=new Array();
		iArray[3][0]="操作时间";          	      //列名
		iArray[3][1]="80px";            		  //列宽
		iArray[3][2]=100;            			  //列最大值
		iArray[3][3]=0;              			  //是否允许输入,1表示允许，0表示不允许
		
		
        iArray[4]=new Array();
 		iArray[4][0]="表名";          	      //列名
 		iArray[4][1]="0px";            		  //列宽
 		iArray[4][2]=100;            			  //列最大值
 		iArray[4][3]=2;              			  //是否允许输入,1表示允许，0表示不允许
		



      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount =0;   
      PolGrid.displayTitle = 1;
      PolGrid.locked=1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction=1;
      PolGrid.canChk =1; // 1为显示CheckBox列，0为不显示 (缺省值)
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>