<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：deleteDataSubmit.jsp
//程序功能：数据重提递交页面
//创建日期：2005-08-16 11:10:36
//创建人  ：xinxu
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String FlagStr = "Fail";
  String Content = "处理完成";
  
  /** 往前面传输数据的容器 */
	VData mResult = new VData();
  
	/**传输到后台处理的map*/
	CErrors tError = null; 
	System.out.println("Come in the deleteDataSubmit");
	ExeSQL tExeSQL = new ExeSQL();
	ExeSQL tExeSQL1 = new ExeSQL();
	String[] arr = new String[18];
	String tTableName = "";
	tTableName = request.getParameter("fmTableName");
	try{
		String[] tSubTableName = tTableName.split(",");
		
		for (int i = 0; i < tSubTableName.length; i++)
		{
			//System.out.println("tSubTableName["+ i +"]" + tSubTableName[i]);
			if (!(tSubTableName[i]=="" || tSubTableName[i]==null || tSubTableName[i].equals("")))
			{
				String tSql = "ALTER TABLE " + tSubTableName[i] + " ACTIVATE NOT LOGGED INITIALLY WITH EMPTY TABLE";
				System.out.println("tSql"+tSql);
				if (!tExeSQL.execUpdateSQL(tSql))
				{
					int n = tExeSQL.mErrors.getErrorCount();
					for (int j = 0; j < n; j++){
						System.out.println("Error: " + tExeSQL.mErrors.getError(j).errorMessage);
					}
					Content = "保监会稽核删除数据失败，原因是: " + tExeSQL.mErrors.getError(0).errorMessage;
					FlagStr = "Fail";
					break;
				}
			}	
		}
    }catch(Exception ex){//异常处理
         Content = Content.trim()+"提示：异常终止!";
    }
			

	//mMap.put("delete from POL_MAIN", "DELETE");
	//mMap.put("delete from prem_info", "DELETE");
	//mMap.put("delete from Endo_Fee", "DELETE");
	//mMap.put("delete from pay_due", "DELETE");
	//mMap.put("delete from claim_report", "DELETE");
	//mMap.put("delete from claim_main", "DELETE");
	//mMap.put("delete from Plan_Info", "DELETE");
	//mMap.put("delete from Salesman_Info", "DELETE");
	//mMap.put("delete from agt_code", "DELETE");
	//mMap.put("delete from Branch_Info", "DELETE");
	//mMap.put("delete from Banc_Speci_Info", "DELETE");
	//mMap.put("delete from Voucher_Info", "DELETE");
	//mMap.put("delete from Bill_Code_Info", "DELETE");
	//mMap.put("delete from Bill_Info", "DELETE");
	//mMap.put("delete from Gener_Account_Code", "DELETE");
	//mMap.put("delete from Sub_Account_Code", "DELETE");
	//mMap.put("delete from Staff_Info", "DELETE");
	//mResult.add(mMap);

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>