<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="PolTxtCreate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <%@page import="com.sinosoft.lis.db.LDSysVarDB"%>
  

  <title>TXT 文件生成</title>
</head>
<body>
  <form method=post name=fm action= "./PolTxtCreateChk.jsp" target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	
      	<TR  class= common>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Bdate>
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Edate>
          </TD>
		   <Input class= common name=BatchNo type=hidden>
        </TR>
     </table>
	 <br>
	 <table  class= common align=center>
	    <TR  class= common>
          <TD>
              <INPUT  class=common VALUE="XML 文件生成" TYPE=button onclick="SubmitForm1();">
		  
			  <input type=hidden id="Opreat" name="Opreat">
          </TD>         
        </TR>
    </table>      
   </form>
 <%  
	//当前生成的XML 的路径
    String mFilePath;
	mFilePath = "../circresult/reportXml/";
%>

  <TR  class= common>
  <td >
        稽核上报数据下载（用鼠标右键选择要下载文件选择'目标另存为'下载）
    </td>
   </TR>
  <table  class= common align=center>
	    <TR  class= common>
          <TD>
          <a href="<%=mFilePath%>pol_main.txt" target='_blank'>下载pol_main.txt</font></a>
          </TD>         
          <TD>
          <a href="<%=mFilePath%>prem_info.txt" target='_blank'>下载prem_info.txt</font></a>
          </TD>         
          <TD>
          <a href="<%=mFilePath%>endo_fee.txt" target='_blank'>下载endo_fee.txt</font></a>
          </TD>  
        </TR>
       <TR  class= common>
          <TD>
          <a href="<%=mFilePath%>pay_due.txt" target='_blank'>下载pay_due.txt</font></a>
          </TD>         
          <TD>
          <a href="<%=mFilePath%>claim_main.txt" target='_blank'>下载claim_main.txt</font></a>
          </TD>         
          <TD>
          <a href="<%=mFilePath%>claim_settled.txt" target='_blank'>下载claim_settled.txt</font></a>
          </TD>  
      </TR>
          <TR  class= common>
          <TD>
          <a href="<%=mFilePath%>plan_info.txt" target='_blank'>下载plan_info.txt</font></a>
          </TD>         
          <TD>
          <a href="<%=mFilePath%>agent_info.txt" target='_blank'>下载agent_info.txt</font></a>
          </TD>         
          <TD>
          <a href="<%=mFilePath%>agt_code.txt" target='_blank'>下载agt_code.txt</font></a>
          </TD>  
      </TR>
    </table>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
