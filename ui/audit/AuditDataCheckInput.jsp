<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：AuditDataCheckInput.jsp
//程序功能：保监会稽核数据校验界面
//创建日期：2009-06-18
//创建人  : qisl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="AuditDataCheck.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AuditDataCheckInit.jsp"%>
  <title>数据质量检查</title>
</head>
<body  onload="initForm();initElementtype();" >
  <form method=post name=fm target="fraSubmit" action= "./AuditDataCheckSubmit.jsp" >
  
    <table>
	<tr>
    	<td>
    	  <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCheckCondition);">
    	</td>
    	<td class= titleImg>
        	请输入校验条件
       	</td>   		 
    </tr>
  </table>
  <Div  id= "divCheckCondition" style= "display: ''">
    <table  class= common align=center>
     	<TR  class= common>
          	<TD  class= title>
            	校验时间段起期
          	</TD>
          	<TD  class= input>
            	<input class=coolDatePicker  name=StartDate elementtype=nacessary verify="起始日期|NOTNULL" >
           	</TD><TD>日期格式：YYYY-MM-DD</TD> 
	    </TR>           	
		<TR>       	
			<TD  class= title>
				校验时间段止期
			</TD>
			<TD  class= input>
				<input class=coolDatePicker  name=EndDate elementtype=nacessary verify="结束日期|NOTNULL" >
				<input type=hidden  name="fmItemType"> 
			</TD>
			<TD  class= title>
					管理机构
			</TD>
			<TD  class= input>
					<Input class="code" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">            
			</TD>  
		</TR>
    </table>
    </Div> 
     
    <div id= "divCmdButton", style= "display:''">
    <INPUT class="cssButton" value="数据校验" type=button onclick = "submitForm();"> 
    <INPUT class="cssButton" VALUE="校验结果查询" type=button onclick="queryClick();">
    </div>
     
     <Div  id= "divCheckResultGrid" style= "display: ''">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanCheckResultGrid" >
					</span> 
			  	</td>
			</tr>
		</table>
	    <table>	
	      <Div  id= "divPage" align=center style= "display: 'none' ">	
	      <INPUT VALUE="首  页" class = CssButton TYPE=button onclick="turnPage.firstPage();"> 
          <INPUT VALUE="上一页" class = CssButton TYPE=button onclick="turnPage.previousPage();"> 					
          <INPUT VALUE="下一页" class = CssButton TYPE=button onclick="turnPage.nextPage();"> 
          <INPUT VALUE="尾  页" class = CssButton TYPE=button onclick="turnPage.lastPage();">
          </Div>
        </table>
    </Div>
    
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
