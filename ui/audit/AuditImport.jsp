<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="java.io.*"%>
<%
//程序名称：AuditImport.jsp
//程序功能：保监会报表财务数据导入
//创建日期：2004-07-08 11:10:36
//创建人  :ck
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="AuditImport.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AuditImportInit.jsp"%>
  <title>数据导入处理</title>
</head>
<body  onload="initForm();initElementtype();" >
	
<%
    String realpath=application.getRealPath("/").substring(0,application.getRealPath("/").length());//UI地址
    String temp=realpath.substring(realpath.length()-1,realpath.length());
	  if(!temp.equals("/"))
		{
		    realpath=realpath+"/"; 
		}
		String templatename="ExcelUploadTemplate.xls";//模板名字
		String templatepathname=realpath+"f1print/picctemplate/"+templatename;//模板名字和地址
		//System.out.println("*********************templatepathname= " + templatepathname);
		//System.out.println("************************realpath="+realpath);
		String outname="稽核Excel上载模版.xls";
	  try {
					outname = java.net.URLEncoder.encode(outname, "UTF-8");
					outname = java.net.URLEncoder.encode(outname, "UTF-8");
					templatepathname = java.net.URLEncoder.encode(templatepathname, "UTF-8");
					templatepathname = java.net.URLEncoder.encode(templatepathname, "UTF-8");
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
%>
 <table class = common width=100%>
      <TR  class= common>
      <TD class = title width=100%>
        <font fontsize=46 size="3" color="#ff0000">两点注意事项：</font>
      </TD>
      <TD class = common width=45%>
        <font fontsize=16 color="#ff0000">一.请仔细阅读模板列标题的批注并参考样例数据（模板中的红色数据）填写模板所需信息<br>
        	    二.分支机构不用填写险种代码表sheet信息，险种代码信息由总公司填写</font>
      </TD>
      <TD>
      <INPUT  class=common  VALUE="Excel模版文件下载" TYPE=button onclick="javascript:window.location='../f1print/download.jsp?filename=<%=outname%>&amp;filenamepath=<%=templatepathname%>'"/>
    	<!--<font size="5"><a href="../f1print/download.jsp?filename=<%=outname%>&amp;filenamepath=<%=templatepathname%>"> 
    	Excel模版文件下载</a></font>  -->
    	
      </TD>         
    </TR>
</table>
 
 <form action="UploadDataSave.jsp" method=post name=fmup target="fraSubmit" ENCTYPE="multipart/form-data">
    <table class = common width=100%>
      <TR  class= common>
      <TD class = title width=100%>
        文件地址：
      </TD>
      <TD class = common width=45%>
        <Input  class = common type="file" name=FileName>
      </TD>
      <TD>
    <INPUT  class=common  VALUE="上载数据" TYPE=button onclick="uploadData();">
      </TD>         
    </TR>
</table>
  </form>
  <form method=post name=fm target="fraSubmit" action= "./AuditImportChk.jsp" >
<!--增加选择框-->  
    <table>
	<tr>
    	<td>
    		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
    	</td>
    	<td class= titleImg>
        	请选择提数的模块
       	</td>   		 
    </tr>
  </table>
  
    <Div  id= "divPolGrid" style= "display: ''">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanPolGrid" >
					</span> 
			  	</td>
			</tr>
        </table>
    </Div>
    
    <!-- 选择查询条件-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请选择查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
     	<TR  class= common>
          	<TD  class= title>
            	统计起始日期
          	</TD>
          	<TD  class= input>
            	<input class=coolDatePicker  name=StartDate verify="起始日期|NOTNULL" >
           	</TD><TD>日期格式：YYYY-MM-DD</TD> 
	    </TR>           	
		<TR>       	
			<TD  class= title>
				统计结束日期
			</TD>
			<TD  class= input>
				<input class=coolDatePicker  name=EndDate verify="结束日期|NOTNULL" >
				<input type=hidden  name="fmItemType"> 
				<input type=hidden  name="fmTableName">
			</TD>
			<TD  class= title>
					管理机构
			</TD>
			<TD  class= input>
					<Input class="code" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">            
			</TD>  
		</TR>
    </table>
     <TR  class= common>
    <INPUT VALUE="系统数据导入" class= common TYPE=button onclick = "autochk();"> 
    <INPUT  class=common VALUE="数据删除" TYPE=button onclick="deleteData();">
     </TR>
     <BR><BR>
     <TR  class= common>
    <font fontsize=16 color="#ff0000">**在进行系统数据导入操作前必须进行数据删除操作**</font></TR>
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
