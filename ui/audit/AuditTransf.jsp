<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：AuditImport.jsp
//程序功能：保监会报表财务数据导入
//创建日期：2004-07-08 11:10:36
//创建人  :ck
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="AuditTransf.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AuditTransfInit.jsp"%>
  <title>数据转换库处理</title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action= "./AuditTransfChk.jsp">
<!--增加选择框-->  
    <table>
	<tr>
    	<td>
    		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
    	</td>
    	<td class= titleImg>
        	稽核数据导入的模块
       	</td>   		 
    </tr>
  </table>
  
    <Div  id= "divPolGrid" style= "display: ''">
    	<table  class= common>
     		<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanPolGrid" >
					</span> 
			  	</td>
			  	<!-- <td text-align: left colSpan=1>
		    <span id="spanHidePolGrid" style= "display: none"></span>
		    </td> -->
			</tr>
        </table>
    </Div>
    <BR>
     <TR  class= common>
     <TD>
     <INPUT VALUE="稽核数据导入到SQLSERVER" class= common TYPE=button onclick = "autochk();"> 
     </TD>         
     </TR>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
