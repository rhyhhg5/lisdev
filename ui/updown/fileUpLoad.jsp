<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	//程序名称：uploadFile.jsp
	//程序功能：用户手册,模板上传界面
	//创建日期：2007-11-23
	//创建人  ：shaoax
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="./fileUpLoad.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	</head>
	<body onload = "initForm();initElementtype();">
		<form method="POST" action="fileUpLoadSave.jsp" name="fm"
			target="fraSubmit" ENCTYPE="multipart/form-data">
			<!--form method="POST" action="fileUploadSave.jsp" name="fm"-->
			<table class=common align=center border="0" width="30%">
				<tr class=common>
					<td class=titleImg colspan = "4">
						请选择要上传的文件:
					</td>
				</tr>
				<tr>
					<td class=input colspan = "4">
						<input class=common type="file" name="fileName" size="100">
					</td>
				</tr>
				<tr>
					<td class=title>
						文件类型:
					</td>
					<td class=input>
						<Input class="codeno" name="fileType"
							ondblclick="return showCodeList('filetype',[this,fileTypeName,filePath],[0,1,2],null,null,null,1);"
							onkeyup="return showCodeList('filetype',[this,fileTypeName,filePath],[0,1,2],null,null,null,1);"
							verify="文件类型|notnull"  readonly="readonly" /><Input class="codename" readonly="readonly" name="fileTypeName" elementtype=nacessary/>
					</td>
					<td class=title>
						文件细类:
					</td>
					<td class=input>
						<Input class="codeno" name="fileDetailType"
							ondblclick="return showCodeList('filetypedetail',[this,fileDetailTypeName,filePath],[0,1,2],null,fileType1,'Code',1);"
							onkeyup="return showCodeList('filetypedetail',[this,fileDetailTypeName,filePath],[0,1,2],null,fileType1,'Code',1);"
							verify="文件细类|notnull" readonly="readonly" /><Input class="codename" readonly="readonly" name="fileDetailTypeName" elementtype=nacessary/>
					</td>
				</tr>
				<tr>
					<td class=title colspan = "4">
						文件描述:
					</td>
				</tr>
				<tr>
					<td class=input colspan = "4">
						<textarea class=common name="discription" verify="文件描述|notnull" rows="5" cols="50"></textarea>
					</td>
				</tr>
				<tr>
					<td>
						<input type="hidden" name=filePath>
						<input value="提  交" class=cssButton type=button name = submitbutton onclick="submitData();">
					</td>
				</tr>

			</table>
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>







