//程序名称：fileUpload.js
//程序功能：文件上传
//创建日期：2007-11-23
//创建人  ：shaoax
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();
var flag1 = 0;
var fileType1 = "";
window.onfocus=myonfocus;

//下拉框选择后执行
function afterCodeSelect(control, Filed)
{	
  if(control == 'filetype')
  {
    fileType1 = fm.fileType.value;
		fm.all("fileDetailType").value = "";
		fm.all("fileDetailTypeName").value = "";
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
    window.focus();
    showInfo.close();   
	//fm.all('submitbutton').disabled=false;
	
 	/**if (FlagStr == "Fail" )
	{
		//如果失败，则返回错误信息
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		//如果提交成功，则执行查询操作
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}*/
	alert(content);
	initForm();  
}

function submitData()
{
	if(fm.all('filename').value=="")
		{
		alert("请选择要上载的文件.");
		return false;
	}
	
	if (verifyInput() == false)
    return false;
	
	var showStr="正在上载数据……";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//fm.all('submitbutton').disabled=true;
	fm.submit();
}
function initForm()
{
	try
	{
		fm.all("fileName").value = "";
		fm.all("fileType").value = "";
		fm.all("fileTypeName").value = "";
		fm.all("fileDetailType").value = "";
		fm.all("fileDetailTypeName").value = "";
		fm.all("discription").value = "";
	}
    catch(ex)
    {
        alert("fileUpload.js-->InitForm函数中发生异常:初始化界面错误!");
    }
}

 

 