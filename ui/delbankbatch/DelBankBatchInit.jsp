<%
//程序名称：GetSendtoMedical.jsp
//程序功能：
//创建日期：2014-04-21 09:12:36
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox() { 
}                                     

function initForm() {
  try {
  	initInpBox();
  	  	
  	initBankGrid();
  }
  catch(re) {
    alert("InitForm函数中发生异常:初始化界面错误!");
  }
}
var BankGrid;
function initBankGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";                   	//列宽
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="应付/应收号";         		//列名
    iArray[1][1]="60px";            	    //列宽
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    

    iArray[2]=new Array();
    iArray[2][0]="收付费类型";         		//列名
    iArray[2][1]="40px";            	//列宽
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
    iArray[3]=new Array();
    iArray[3][0]="金额";         		
    iArray[3][1]="40px";            	
    iArray[3][3]=0;
    
    iArray[4]=new Array();
    iArray[4][0]="发盘时间";         		
    iArray[4][1]="40px";            	
    iArray[4][3]=0;
    
    BankGrid = new MulLineEnter( "fm" , "BankGrid" ); 
    
    BankGrid.mulLineCount = 0;   
    BankGrid.displayTitle = 1;
    BankGrid.hiddenPlus = 1;
    BankGrid.hiddenSubtraction = 1;
    BankGrid.canSel = 1;
    BankGrid.locked = 1;
    //BankGrid.selBoxEventFuncName = "showStatistics";
    BankGrid.loadMulLine(iArray);
    
    //这些操作必须在loadMulLine后面
    //BankGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex.message);
  }
}
</script>

	
