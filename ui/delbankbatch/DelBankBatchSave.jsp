<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：DelBankBatchSave.jsp
//程序功能：
//创建日期：2014-07-15 15:30:36
//创建人  ：cy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="com.sinosoft.lis.delbankbatch.*"%>
  

  
<%
  System.out.println("\n\n--DelBankBatchSave Start---");
  
  
  DelBankBatchUI tDelBankBatchSaveUI1 = new  DelBankBatchUI();

  String tfmAction = request.getParameter("fmAction");
  System.out.println(tfmAction);
  TransferData transferData1 = new TransferData();
  transferData1.setNameAndValue("Serialno", request.getParameter("ErrorSerialno"));
  transferData1.setNameAndValue("paycode", request.getParameter("paycode"));
  transferData1.setNameAndValue("paytype", request.getParameter("paytype"));
  transferData1.setNameAndValue("fmAction", tfmAction);
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
    
  VData tVData = new VData();
  tVData.add(transferData1);
  tVData.add(tGlobalInput);
  
  String Content = "";
  String FlagStr = "";

  if (!tDelBankBatchSaveUI1.submitData(tVData,tfmAction)) {
   // VData rVData = tDelBankBatchSaveUI1.getResult();
    Content = " 处理失败！";
  	FlagStr = "Fail";
  }
  else {
    Content = " 处理成功! ";
  	FlagStr = "Succ";
  }

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.initQuery();
</script>
</html>
