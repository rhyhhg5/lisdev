//程序名称：FamilyContQuery.js
//程序功能： 
//创建日期：2014-8-6
//创建人  ：yangyang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
	{
		try 
		{
			showInfo.focus();
		}
		catch (ex) 
		{
			showInfo = null;
		}
	}
}

function easyQueryClick() 
{ 	
  if(!verifyInput2())
  {
      return false;
  }
  if(!checkDate())
  {
    return false;
  }
  
  var condition = "";
  var condition1 = "";
  var strsql ="";
  //为避免 SQLSTATE 58004 错误，修改状态查询的 exists 为 in、
  var strsql1="";
  
  if(fm.State.value == "1"||fm.State.value == "0")
  {
	condition = "and (b.AppFlag <> '1' or 1=1) and  exists (select * from LWMission where processid ='0000000013' and activityid='0000013001' and MissionProp1=a.DocCode) ";
    var sql2= "select a.DocCode, nvl(b.ContNo,'待录入'), (select Name from LDCom where ComCode = a.ManageCom), "
        + "    nvl(CodeName('lcsalechnl', b.SaleChnl),'待录入'), "
        + "    nvl(b.AppntName,'待录入'), b.CValiDate,'待录入', "
        + "    varchar(b.Prem), "
        + "     ArchiveNo  "
        + "from ES_Doc_Main a left join LCCont b "
        + "on a.DocCode = b.PrtNo "
        + "where a.SubType ='TB27' "
        + "    and (b.ContType is null or b.ContType = '1') "
        + "    and a.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart('a.DocCode', 'PrtNo','=')
        + getWherePart('b.ContNo', 'ContNo','=')
//        + getWherePart('b.AgentCom', 'AgentCom')
//        + getWherePart('b.AgentCode', 'AgentCode')
        + getWherePart('b.SaleChnl', 'SaleChnl')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('b.CValiDate', 'CValiDate')
        + getWherePart('a.MakeDate', 'CValiDateStart','>=')
        + getWherePart('a.MakeDate', 'CValiDateEnd','<=')
        + condition ;
        
    condition1 = " and (b.AppFlag <> '1' or 1=1) and  exists (select * from LWMission where processid ='0000000013' and activityid='0000013001' and MissionProp1=b.prtno) "
       +" and not exists (select 1 from ES_Doc_Main a where doccode=b.prtno and a.subtype='TB27' )";
    var sql3="select prtno,contno,(select Name from LDCom where ComCode = b.ManageCom),"
        	+" nvl(CodeName('lcsalechnl', b.SaleChnl),'待录入'),"
        	+ "    nvl(b.AppntName,'待录入'), b.CValiDate,'待录入', "
            + "    varchar(b.Prem),(select ArchiveNo from ES_Doc_Main a where a.DocCode = b.PrtNo and  a.SubType in ('TB10') ) "
            +"  from lccont b where 1=1 "      
            +" and prtno like 'XFYJ%'"
            + getWherePart('b.prtno', 'PrtNo','=')
            + getWherePart('b.ContNo', 'ContNo','=')
            + getWherePart('b.SaleChnl', 'SaleChnl')
            + getWherePart('b.AppntName', 'AppntName')
            + getWherePart('b.CValiDate', 'CValiDate')
            + getWherePart('b.MakeDate', 'CValiDateStart','>=')
            + getWherePart('b.MakeDate', 'CValiDateEnd','<=')
            + condition1;
        //strsql1+=sql3;
       //	strsql += sql2;
        strsql =strsql+ sql2+" union all "+sql3;
  }
 
  if(fm.State.value == "2"||fm.State.value == "0")
  {
	condition =" and b.AppFlag <> '1' and exists (select 1 from LWMission where processid ='0000000013' and activityid='0000013002' and MissionProp2=a.DocCode) "
      + " and not exists (select 1 from ljtempfee ljt where ljt.otherno = a.DocCode and othernotype = '4' and ljt.enteraccdate is not null and confdate is null) ";
    var sql2= "select a.DocCode, b.ContNo, (select Name from LDCom where ComCode = a.ManageCom), "
        + "    CodeName('lcsalechnl', b.SaleChnl), "
        + "    b.AppntName, b.CValiDate,'待收费', "
        + "    varchar(b.Prem), "
        + "     ArchiveNo  "
        + "from ES_Doc_Main a left join LCCont b "
        + "on a.DocCode = b.PrtNo "
        + "where a.SubType ='TB27' "
        + "    and (b.ContType is null or b.ContType = '1') "
        + "    and a.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart('a.DocCode', 'PrtNo','=')
        + getWherePart('b.ContNo', 'ContNo','=')
//        + getWherePart('b.AgentCom', 'AgentCom')
//        + getWherePart('b.AgentCode', 'AgentCode')
        + getWherePart('b.SaleChnl', 'SaleChnl')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('b.CValiDate', 'CValiDate')
        + getWherePart('a.MakeDate', 'CValiDateStart','>=')
        + getWherePart('a.MakeDate', 'CValiDateEnd','<=')
        + condition;
      condition1 = " and b.AppFlag <> '1'  and  exists (select * from LWMission where processid ='0000000013' and activityid='0000013002' and MissionProp2=b.prtno) "
    + " and not exists (select 1 from ljtempfee ljt where ljt.otherno = b.prtno and othernotype = '4' and ljt.enteraccdate is not null and confdate is null) "
      +" and not exists (select 1 from ES_Doc_Main a where doccode=b.prtno and a.subtype='TB27' )";
      var sql3="select prtno,contno,(select Name from LDCom where ComCode = b.ManageCom),"
    	+" nvl(CodeName('lcsalechnl', b.SaleChnl),'待录入'),"
    	+ "    nvl(b.AppntName,'待录入'), b.CValiDate,'待收费', "
    	+ "    varchar(b.Prem),(select ArchiveNo from ES_Doc_Main a where a.DocCode = b.PrtNo and  a.SubType in ('TB10') ) "
        +"  from lccont b where 1=1 "    
        +" and prtno like 'XFYJ%'"
        + getWherePart('b.prtno', 'PrtNo','=')
        + getWherePart('b.ContNo', 'ContNo','=')
        + getWherePart('b.SaleChnl', 'SaleChnl')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('b.CValiDate', 'CValiDate')
        + getWherePart('b.MakeDate', 'CValiDateStart','>=')
        + getWherePart('b.MakeDate', 'CValiDateEnd','<=')
        + condition1;
       
	    if(fm.State.value=="0"){
	        strsql = sql2 +" union all "+strsql+" union all "+sql3; 
	        strsql1=sql3 +" union all "+strsql1; ;
	      }else {
	      	strsql = sql2+" union all "+sql3;
	      	 strsql1=sql3;
	      }   	   	
  }
  if(fm.State.value == "3"||fm.State.value == "0")
  {
	condition =" and b.AppFlag <> '1' and exists (select 1 from LWMission where processid ='0000000013' and activityid='0000013002' and MissionProp2=a.DocCode) "
    + "and exists (select 1 from ljtempfee ljt where ljt.otherno = a.DocCode and othernotype = '4' and ljt.enteraccdate is not null and confdate is null) ";
    var sql2= "select a.DocCode, b.ContNo, (select Name from LDCom where ComCode = a.ManageCom), "
        + "    CodeName('lcsalechnl', b.SaleChnl), "
        + "    b.AppntName, b.CValiDate,'待签单', "
        + "    varchar(b.Prem), "
        + "     ArchiveNo  "
        + "from ES_Doc_Main a left join LCCont b "
        + "on a.DocCode = b.PrtNo "
        + "where a.SubType ='TB27' "
        + "    and (b.ContType is null or b.ContType = '1') "
        + "    and a.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart('a.DocCode', 'PrtNo','=')
        + getWherePart('b.ContNo', 'ContNo','=')
//        + getWherePart('b.AgentCom', 'AgentCom')
//        + getWherePart('b.AgentCode', 'AgentCode')
        + getWherePart('b.SaleChnl', 'SaleChnl')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('b.CValiDate', 'CValiDate')
        + getWherePart('a.MakeDate', 'CValiDateStart','>=')
        + getWherePart('a.MakeDate', 'CValiDateEnd','<=')
        + condition;
        
     condition1 = " and b.AppFlag <> '1'  and  exists (select * from LWMission where processid ='0000000013' and activityid='0000013002' and MissionProp2=b.prtno) "
    	 +" and not exists (select 1 from ES_Doc_Main a where doccode=b.prtno and a.subtype='TB27' )"
    	 + " and exists (select 1 from ljtempfee ljt where ljt.otherno = b.prtno and othernotype = '4' and ljt.enteraccdate is not null and confdate is null) ";
    var sql3="select prtno,contno,(select Name from LDCom where ComCode = b.ManageCom),"
    	+" nvl(CodeName('lcsalechnl', b.SaleChnl),'待录入'),"
    	+ "    nvl(b.AppntName,'待录入'), b.CValiDate,'待签单', "
    	+ "    varchar(b.Prem),(select ArchiveNo from ES_Doc_Main a where a.DocCode = b.PrtNo and  a.SubType in ('TB10'))  "
        +"  from lccont b where 1=1 "    
        +" and prtno like 'XFYJ%'"
        + getWherePart('b.prtno', 'PrtNo','=')
        + getWherePart('b.ContNo', 'ContNo','=')
        + getWherePart('b.SaleChnl', 'SaleChnl')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('b.CValiDate', 'CValiDate')
        + getWherePart('b.MakeDate', 'CValiDateStart','>=')
        + getWherePart('b.MakeDate', 'CValiDateEnd','<=')
        +condition1;
	    if(fm.State.value=="0"){
	    	strsql = sql2 +" union all "+strsql+" union all "+sql3; 
	        strsql1=sql3 +" union all "+strsql1; ;
	      }else {
	    	  strsql = sql2+" union all "+sql3;
	      	strsql1=sql3;
	      }   	   
  }
  if(fm.State.value == "4"||fm.State.value == "0")
  {
	condition = " and b.AppFlag = '1' and exists(select 1 from LBMission where processid ='0000000013' and activityid='0000013002' and MissionProp2=a.DocCode)";
    var sql2= "select a.DocCode, b.ContNo, (select Name from LDCom where ComCode = a.ManageCom), "
        + "    CodeName('lcsalechnl', b.SaleChnl), "
        + "    b.AppntName, b.CValiDate,'已签单', "
        + "    varchar(b.Prem), "
        + "     ArchiveNo  "
        + "from ES_Doc_Main a left join LCCont b "
        + "on a.DocCode = b.PrtNo "
        + "where a.SubType ='TB27' "
        + "    and (b.ContType is null or b.ContType = '1') "
        + "    and a.ManageCom like '" + fm.ManageCom.value + "%' "
        + getWherePart('a.DocCode', 'PrtNo','=')
        + getWherePart('b.ContNo', 'ContNo','=')
//        + getWherePart('b.AgentCom', 'AgentCom')
//        + getWherePart('b.AgentCode', 'AgentCode')
        + getWherePart('b.SaleChnl', 'SaleChnl')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('b.CValiDate', 'CValiDate')
        + getWherePart('a.MakeDate', 'CValiDateStart','>=')
        + getWherePart('a.MakeDate', 'CValiDateEnd','<=')
        + condition;
        
    condition1 = " and (b.AppFlag = '1' ) and  not exists (select * from LWMission where processid ='0000000013' and activityid='0000013002' and MissionProp2=b.prtno) "
    +" and not exists (select 1 from ES_Doc_Main a where doccode=b.prtno and a.subtype='TB27' )";
    var sql3="select prtno,contno,(select Name from LDCom where ComCode = b.ManageCom),"
    	+" nvl(CodeName('lcsalechnl', b.SaleChnl),'待录入'),"
    	+ "    nvl(b.AppntName,'待录入'), b.CValiDate,'已签单', "
    	+ "    varchar(b.Prem),(select ArchiveNo from ES_Doc_Main a where a.DocCode = b.PrtNo and  a.SubType in ('TB10'))  "
        +"  from lccont b where 1=1 " 
        +" and prtno like 'XFYJ%'"
        + getWherePart('b.prtno', 'PrtNo','=')
        + getWherePart('b.ContNo', 'ContNo','=')
        + getWherePart('b.SaleChnl', 'SaleChnl')
        + getWherePart('b.AppntName', 'AppntName')
        + getWherePart('b.CValiDate', 'CValiDate')
        + getWherePart('b.MakeDate', 'CValiDateStart','>=')
        + getWherePart('b.MakeDate', 'CValiDateEnd','<=')
        +condition1;
            
	    if(fm.State.value=="0"){
	    	strsql = sql2 +" union all "+strsql+" union all "+sql3; 
	        strsql1=sql3 +" union all "+strsql1; ;
	      }else {
	    	  strsql = sql2+" union all "+sql3;
	      	strsql1=sql3;
	      } 
  }
 // var result = easyExecSql(strsql);
//  if(result){
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strsql, LCContGrid);
//  }else{
//	  turnPage2.pageDivName = "divPage2";
//	turnPage2.queryModal(strsql1, LCContGrid);
//  }
//  	strsql	+= "order by a.DocCode ";
	
	return true;
}

function queryClick()
{
  if(!easyQueryClick())
      return false;
  if(LCContGrid.mulLineCount == 0)
  {
      alert("没有查询到数据");
      return false;
  }
}

function checkDate()
{
  var sql = "select 1 from Dual where date('" + fm.CValiDateEnd.value + "') > date('" + fm.CValiDateStart.value + "') + 3 month ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    if(!confirm("生效日期起止时间大于3个月！是否继续？"))
    {
      return false;
    }
  }
  
  return true;
}

//保单下载
function downloadCont()
{
    if(LCContGrid.mulLineCount == 0)
    {
        alert("没有需要下载的数据");
        return false;
    }
    fm.Sql.value = turnPage2.strQuerySql;
 	fm.action = "FamilyContQueryDownLoad.jsp";
    fm.submit();
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
	cDiv.style.display="";
	}
	else
	{
	cDiv.style.display="none";  
	}
}
//2014-8-12 新增回退功能
function goBackCont()
{
	var tSel = LCContGrid.getSelNo();
	var prtno ="";
	var state ="";
	if( tSel != null && tSel != 0 )
	{
		prtno = LCContGrid.getRowColData( tSel - 1, 1 );
	}
	if( prtno == null || prtno == "" )
	{
		alert("请选择一条记录后再进行回退");
		return;
	}
	state = LCContGrid.getRowColData( tSel - 1, 7);
	if("待收费"!=state)
	{
		alert("只有待收费的数据才可以回退");
		return;
	}
	var sql = "select 1 from LJSPay where OtherNo ='"+prtno+"' and BankOnTheWayFlag = '1'";
	var result = easyExecSql(sql);
	if(!result)
	{
		fm.action="FamilyContBack.jsp?prtno="+prtno;
		fm.submit();
	}
}
function afterSubmit( FlagStr, content )
{
  window.focus();
  try
  {
	  showInfo.close();
	}
	catch(ex)
	{}
	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		easyQueryClick();
	}
	 
}
