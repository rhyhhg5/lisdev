<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：FamilySignQueryInputSave.jsp
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.workflow.brieftb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%

CErrors tError = null;
String FlagStr = "";
String Content = "";
String Priview = "PREVIEW";

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
TransferData tTransferData = new TransferData();


LCContSchema tLCContSchema = new LCContSchema();

String szTemplatePath = application.getRealPath("f1print/template/") + "/";	
String sOutXmlPath = application.getRealPath("");	

String tContNo[] = request.getParameterValues("GrpGrid1");
System.out.println(request.getParameterValues("GrpGrid1"));
String tRadio[] = request.getParameterValues("InpGrpGridSel");
System.out.println(request.getParameterValues("InpGrpGridSel"));
String tMissionID[] = request.getParameterValues("GrpGrid7");
System.out.println(request.getParameterValues("GrpGrid7"));
String tSubMissionID[] = request.getParameterValues("GrpGrid8");
System.out.println(request.getParameterValues("GrpGrid8"));
String workType = request.getParameter("workType");
System.out.println(request.getParameterValues("workType"));
boolean flag = false;
int grouppolCount = tContNo.length;
for (int i = 0; i < grouppolCount; i++)
{
  if( tContNo[i] != null && tRadio[i].equals( "1" ))
  {
    System.out.println("GrpContNo:"+i+":"+tContNo[i]);
    tLCContSchema.setContNo( tContNo[i] );
    tTransferData.setNameAndValue("TemplatePath",szTemplatePath );
    tTransferData.setNameAndValue("OutXmlPath",sOutXmlPath );
    tTransferData.setNameAndValue("MissionID",tMissionID[i] );
    tTransferData.setNameAndValue("SubMissionID",tSubMissionID[i] );
    flag = true;
    break;
  }
}

if (flag == true)
{
  VData tVData = new VData();
  tVData.add( tLCContSchema );
  tVData.add( tTransferData );
  tVData.add( tG );

  BriefTbWorkFlowUI tBriefTbWorkFlowUI = new BriefTbWorkFlowUI();
  boolean bl= tBriefTbWorkFlowUI.submitData( tVData, "0000013002");
  int n = tBriefTbWorkFlowUI.mErrors.getErrorCount();
  if( n == 0 )
  {
    if ( bl )
    {
      Content = " 签单成功! ";
      FlagStr = "Succ";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单成功! ";
      }
    }
    else
    {
      Content = "签单失败";
      FlagStr ="Fail" ;
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单失败! ";
      }
    }
  }
  else
  {

    String strErr = "";
    for (int i = 0; i < n; i++)
    {
      strErr += (i+1) + ": " + tBriefTbWorkFlowUI.mErrors.getError(i).errorMessage + "; ";
      System.out.println(tBriefTbWorkFlowUI.mErrors.getError(i).errorMessage );
    }
    if ( bl )
    {
      Content = " 部分签单成功,但是有如下信息: " +strErr;
      FlagStr = "Succ";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单成功! ";
      }
    }
    else
    {
      Content = "集体投保单签单失败，原因是: " + strErr;
      FlagStr = "Fail";
      if("PREVIEW".equals(workType))
      {
        Content = " 预打保单失败! ";
      }
    }
  } // end of if
} // end of if

%>
<html>
<script language="javascript">
                 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
