<%@ page contentType="text/html;charset=GBK" %>
<%@ include file="../common/jsp/UsrCheck.jsp" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<html>
<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String strCurDay = PubFun.getCurrentDate();
%>
<script>
	var strCurDay = "<%=strCurDay%>";
	var ComCode = "<%=tGI.ManageCom%>";
	var Operator = "<%=tGI.Operator%>";
</script>
	<head>
    	<script src="../common/easyQueryVer3/EasyQueryCache.js"></script>
    	<script src="../common/easyQueryVer3/EasyQueryVer3.js"></script>
    	<script src="../common/javascript/Common.js"></script>
    	<script src="../common/cvar/CCodeOperate.js"></script>
    	<script src="../common/javascript/MulLine.js"></script>
   	 	<script src="../common/Calendar/Calendar.js"></script>

    	<link href="../common/css/Project.css" rel="stylesheet" type="text/css">
    	<link href="../common/css/mulLine.css" rel="stylesheet" type="text/css">

    	<script src="FamilyTaxImport.js"></script>
    	<%@include file="FamilyTaxImportInit.jsp" %>
	</head>
	<body onload="initForm();">
		<form action="" method=post name=fm target="fraSubmit">
       	 	<table>
          	  	<tr>
                	<td class="titleImg">请输入查询条件：</td>
           	 	</tr>
       		</table>
       		<div id="BatchInfo" style="display:''" align="center">
	            <table class="common">
	                <tr class="common">
	                    <td class="title8">批次号</td>
	                    <td class="input8">
	                        <input class="common" name="BatchNo" />
	                    </td>
	                    <td class="title8">管理机构</td>
	                    <td class="input8">
	                        <input class="codeNo" name="ManageCom" verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly="readonly" /><input class="codename" name="ManageComName" readonly="readonly" elementtype="nacessary" />
	                    </td>
	                    <td class="title8">申请日期</td>
	                    <td class="input8">
	                        <input class="coolDatePicker" dateFormat="short" name="ApplyDate" verify="申请日期|date" />
	                    </td>
	                </tr>
	           	</table>
        	</div>
       	 	<input type="button" class=cssButton value=" 查  询 " name=QueryBatchButton onclick="QueryBatchClick();">
       	 	<input type="button" class=cssButton value=" 申  请 " name=ApplyButton onclick="Apply();">
       	 	<table>
       	 		<tr>
                	<td class="common">
                    	<img src="../common/images/butExpand.gif" style="cursor:hand;" onclick="showPage(this, BatchDetail);"/>
                	</td>
                	<td class="titleImg">批次信息：</td>
            	</tr>
       	 	</table>
       	 	<div id="BatchDetail" style="display: ''" align="center">
       	 		<table class="common">
                	<tr class="common">
                    	<td>
                        	<span id="spanBatchDetailGrid"></span> 
                    	</td>
               	 	</tr>
            	</table>
            	<div id="divBatchDetailGridPage" style="display: ''" align="center">
                	<input type="button" class="cssButton" value="首  页" onclick="turnPage1.firstPage();" />
                	<input type="button" class="cssButton" value="上一页" onclick="turnPage1.previousPage();" />
                	<input type="button" class="cssButton" value="下一页" onclick="turnPage1.nextPage();" />
                	<input type="button" class="cssButton" value="尾  页" onclick="turnPage1.lastPage();" />
            	</div>
       	 	</div>
       	 	<input type="button" class=cssButton value=" 批次导入" name=ImportButton onclick="batchImport();">
		</form>
		<span id="spanCode" style="display: none; position:absolute; slategray"></span>
	</body>
</html>