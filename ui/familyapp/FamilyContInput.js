//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
var turnPage6 = new turnPageClass();

var succFlag = false;
window.onfocus=myonfocus;
parent.fraMain.rows = "0,0,0,0,*";
var ConfirmFlag = false;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//提交，保存按钮对应操作
function submitForm()
{
    //增加对身份证号的校验 20170828
	var appntIDType=fm.appnt_IDType.value;
    if(appntIDType=="0"||appntIDType=="5")
    {
    	var appntIDNo=fm.appnt_IDNo.value;
    	if(appntIDNo != null && appntIDNo != ""){
    		var strChkIDNo=checkIdNo(appntIDType,appntIDNo,fm.appnt_AppntBirthday.value,fm.appnt_AppntSex.value);
    		if(strChkIDNo !=""){
    			alert(strChkIDNo);
    			return false;
    		}
    	}
    }
    
    if (verifyInput2()== false) 
    {        	
        return false;
    }
  //选择交叉销售后，对交叉销售各不为空项校验。
	if(!MixComCheck())
	{
	  return false;
	}
    
    var havesql ="select * from lwmission where missionprop2='"+fm.PrtNo.value+"'";
    var missionc=easyExecSql(havesql);
    if(missionc!=null){
    alert("该保单已录入完毕，不能再进行保存！");
    return false;
    }
    
	
	if(!checkNative()){
	   return false;
	}
	
	if(!checkAllapp()){
	   return false;
	}
    //zxs 20190522
	if(!checkAppDate()){
		return false;
	}
	
    var sql = "select * from lccont where prtno='"+fm.PrtNo.value+"'";
    var have = easyExecSql(sql);
    if(have!=null){
    alert("该保单已存在，请点击修改");
    return false;
    }
    var appZipcode=fm.appnt_ZipCode.value;
    var len=parseInt("6");
    var saleChnl = fm.SaleChnl.value;
    
    
    if(appZipcode!=""){
        if(appZipcode.length !=len){
            alert("投保人邮编长度必须等于6!");
            return false;
        }
    }

    if(fm.PolApplyDate.value== "")
    {
        alert("投保单申请日期必须填写!");
        return false;
    }
   
    var appntIDType = fm.appnt_IDType.value;
//    if(!checkIDType(appntIDType))
//    {
//    	alert("投保人证件类型填写有误，不符合系统规定0到4范围内，请进行修改！");
//    	return false;
//    }
    
    if(!checkCrsBussParams())
    {
        return false;
    }
    
    if(!ExtendCheck()){
    	return false;
    }
    // --------------------
    
    if(!checkSaleChnl()){
        return false;
    }

 if(fm.appnt_AppntNo.value!=null&&fm.appnt_AppntNo.value!=""){

  var tsql="select Name, IDType, IDNo, Birthday,Sex from LDPerson where customerno='"+fm.appnt_AppntNo.value+"'";
  var  cc1=easyExecSql(tsql);
  if(fm.all('appnt_AppntName').value!=cc1[0][0]&&fm.all('appnt_IDType').value!=cc1[0][1]&&fm.all('appnt_IDNo').value!=cc1[0][2]&&fm.all('appnt_AppntBirthday').value!=cc1[0][3]&&fm.all('appnt_AppntSex').value!=cc1[0][4]){
    alert("输入投保人五要素与投保人信息不符！");
    
    return false;
  
    }
  }    
    
  if(!valiAddress()){
    	return false;
    }

    
    
    //加入缴费频次不能为空的校验。
    
    if(fm.PayIntv.value == "")
    {
        alert("缴费频次不能为空!");
        return false;
    }
    
    //续期缴费提醒不能为空的校验
    if(fm.DueFeeMsgFlag.value == "")
    {
        alert("是否需要续期缴费提醒不能为空！");
        return false;
    }
    // ----------------------------------

	

    if(fm.appnt_IDType.value == "0"){
        if(!checkIdCard(fm.appnt_IDNo.value)){
            return false;
        }
    }
    
    if(fm.appnt_AppntSex.value == "")
    {
        fm.appnt_AppntSex.value = "2";
    }
    if(fm.appnt_AppntBirthday.value == "")
    {
        fm.appnt_AppntBirthday.value = "1900-01-01";
    }

    if(fm.PayMode.value ==""){
        alert("缴费方式不能为空");
        return false;
    }
    else if(fm.PayMode.value =="4"||fm.PayMode.value =="6"){
        if(fm.BankCode.value ==""){
            alert("开户银行不能为空");
            return false;
        }
        if(fm.BankAccNo.value ==""){
            alert("账号不能为空");
            return false;
        }
        if(fm.AccName.value ==""){
            alert("户名不能为空");
            return false;
        }
        var sql = "select 1 from LDBank "
        + "where (BankUniteFlag is null or BankUniteFlag<>'1') "
        + "    and BankCode = '" + fm.BankCode.value + "' ";
        var rs = easyExecSql(sql);
        if(rs == null || rs[0][0] == "" || rs[0][0] == "null")
        {
            alert("没有银行编码对应的银行");
            return false;
        }
    }
    var strsql="select agentgroup from laagent where agentcode='"+fm.AgentCode.value+"'";
    var agentcode =easyExecSql(strsql);
    fm.AgentGroup.value=agentcode;
    fm.fmAction.value='INSERT||MAIN';
    var i = 0;
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.action="./FamilyContSave.jsp";
    fm.saveButton.disabled = true;
    fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,ContNo,PrintFlag)
{
	fm.saveButton.disabled = false;
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    ConfirmFlag = false;
  }
  else
  {
  	initForm();
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  if(ConfirmFlag&&(!(fm.fmAction.value=="DELETE||MAIN"))){
	  	top.opener.initForm();
	    if(PrintFlag == "Y"){
	   	top.close();}
	  }
	  if(ConfirmFlag){
	  	top.opener.close();
	  	top.window.close();	  
	  }
  }
  

}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LDClassInfo.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}
//取消按钮对应操作
function cancelForm()
{
  //  window.location="../common/html/Blank.html";
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}
//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    var i = 0;
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    //showSubmitFrame(mDebug);
    fm.fmAction.value = "DELETE||MAIN";
    fm.action = "./BriefFamilyContSave.jsp";
    fm.submit(); //提交
    initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}
//代理人查询
function queryAgent()
{
    
    if(fm.all('ManageCom').value=="")
    {
        alert("请先录入管理机构信息！");
        return;
    }
    if(fm.all('GroupAgentCode').value == "")
    {
      
        var branchType = 1;
        if(fm.all('SaleChnl').value == "11" || fm.all('SaleChnl').value == "12")   branchType = 2;
        //alert(fm.all('SaleChnl').value);
        var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&AgentCom="+fm.all('AgentCom').value+"&SaleChnl="+fm.all('SaleChnl').value+"&branchtype= null","AgentCommonQueryMain",
            'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
     
    }
    if(fm.all('GroupAgentCode').value != "")
    {

        var cAgentCode = fm.GroupAgentCode.value;  //保单号码
        var strSql = "select GroupAgentCode,Name,AgentGroup,agentcode from LAAgent where GroupAgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentName.value = arrResult[0][1];
            fm.AgentGroup.value = arrResult[0][2];
            fm.AgentCode.value = arrResult[0][3];
            alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value="";
            fm.AgentCode.value = "";
            alert("编码为:["+fm.all('GroupAgentCode').value+"]的代理人不存在，请确认!");
        }
    }
}

//代理人查询返回
//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{
  if(arrResult!=null)
  {
    fm.AgentCode.value = arrResult[0][0];
    fm.AgentName.value = arrResult[0][5];
    fm.AgentGroup.value = arrResult[0][1];
    fm.GroupAgentCode.value = arrResult[0][95];
  }
}

/*********************************************************************
 *  Click事件，当双击“投保单位客户号”录入框时触发该函数
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showAppnt1()
{
  if( mOperate == 0 )
  {
    mOperate = 2;
    showInfo = window.open( "../sys/GroupMain.html" );
  }
}
function showAppnt()
{
  if (fm.all("GrpNo").value == "" )
  {
    showAppnt1();
  }
  else
  {
    arrResult = easyExecSql("select * from LDGrp b where  b.CustomerNo='" + fm.all("GrpNo").value + "'", 1, 0);
    if (arrResult == null)
    {
      alert("未查到投保单位信息");
    }
    else
    {
      afterQuery(arrResult);
    }
  }
}
//用于个单显示刷出险种
function showRiskInfo(){
	var strSql = "select a.riskcode,a.riskname,"
	+"to_zero((select case when b.mult=0 then b.amnt else b.mult end from lcpol b where b.ContNo='"
	+fm.ContNo.value+"' and b.RiskCode=a.RiskCode )) ,"
	+"to_zero((select b.copys from lcpol b where b.ContNo='"
	+fm.ContNo.value+"' and b.RiskCode=a.RiskCode )), "
	+"to_zero((select b.Prem from lcpol b where b.ContNo='"
	+fm.ContNo.value+"' and b.RiskCode=a.RiskCode )) "
	+"from lmriskapp a where  a.RiskProp='I'  ";
	//境外救援
	if(MissionProp5 == "1")
	{
		strSql = strSql + "and a.RiskType8='1' order by RiskCode";
	}
	//交通意外
	if(MissionProp5 == "2")
	{
		strSql = strSql + "and a.RiskType8='2' order by RiskCode";
	}
	//高原疾病
	if(MissionProp5 == "3")
	{
		strSql = strSql + "and a.RiskType8='5' order by RiskCode";
	}
	//增加卡单险种
	if(MissionProp5 == "4")
	{
		strSql = "select a.riskcode,'',"
		+"case when mult>20 then premlot else mult end "
		+"from lmcardrisk a where certifycode in "
			+"(select a.certifycode from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='"+fm.PrtNo.value+"')";
		//由于卡单类险种的代理人,由卡记录
		var agentSql = "select RECEIVECOM from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='"+fm.PrtNo.value+"'";
		var arrAgent = easyExecSql(agentSql);
		if(arrAgent){
			var receive = arrAgent[0][0];
			//
			receive = receive.substring(0,1);
			if(receive == "D"){
				var AgentCode = arrAgent[0][0];
				//alert(AgentCode);
				//alert(easyExecSql("select * from LAAgent where AgentCode='"+AgentCode.replace("D","")+"'"));
				var AgentInfo = easyExecSql("select * from LAAgent where AgentCode='"+AgentCode.replace("D","")+"'");
				afterQuery2(AgentInfo);
				fm.ManageCom.value=AgentInfo[0][2];
			}else if (receive == "E"){
				//alert("此单证还未下发给代理人,目前系统不支持非代理人卡单！");
				var AgentCom = arrAgent[0][0];
				var AgentComInfo = easyExecSql("select * from LACom where AgentCom='"+AgentCom.replace("E","")+"'");
				if( AgentComInfo ){
					fm.AgentCom.value = AgentComInfo[0][0];
					fm.SaleChnl.value = "03";
				}
				if(fm.AgentCom.value == "" || fm.AgentCom.value == "null" || fm.AgentCom.value == null){
					alert("下发给中介机构错误，未查询到此中介公司");
					return;
				}
				var arrAgentCom = easyExecSql("select * from LAComToAgent where AgentCom='"+fm.AgentCom.value+"'");
				if(arrAgentCom){
					var AgentInfo = easyExecSql("select * from LAAgent where AgentCode='"+arrAgentCom[0][2]+"'");
					afterQuery2(AgentInfo);
				}else{
					//没有对应中介专员
					fm.AgentCode.value = "0000000000";
					fm.AgentGroup.value = "0000000000";
				}
			}
		}
		//展现卡单信息
		showCardInfo();
	}
  
	var arr = easyExecSql(strSql);
	if(arr){
		displayMultiline(arr,RiskGrid,turnPage);
	}
	if(RiskGrid.mulLineCount==1){
		RiskGrid.checkBoxSel(1);
	}
}
//简易保单的合同信息查询
function queryContInfo(){
//  alert(ContType);
	var PrtNo = fm.PrtNo.value ;
   var tSql = "select contno from lccont where prtno = '"+fm.PrtNo.value+"'";
	var arr=easyExecSql(tSql);
	var tsql1="select familyid  from lccont where prtno='"+fm.PrtNo.value+"'";
	var addressno=easyExecSql(tsql1);
	fm.ContNo.value=arr;
	fm.NeedMulBnf.value="0"
	//fm.appnt_AddressNo.value="2000";
	if(PrtNo!=null && PrtNo!="" && PrtNo!="null"){
	  //  var arr6 ="select * from  where "
	  		
		var arr5 = easyExecSql("select agentcode,polapplydate,appntno,appntname,appntidtype,appntidno,appntsex,appntbirthday,PayIntv,PayMode,bankcode,bankaccno,DueFeeMsgFlag,accname,salechnl,agentcom,codename('unitesalechnl',salechnl),AgentSaleCode,managecom from lccont where prtno='"+fm.PrtNo.value+"'");
		
		if(arr5){
			try{fm.all('AgentCode').value=arr5[0][0];}catch(ex){alert(ex.message)};
			try{fm.all('GroupAgentCode').value=easyExecSql("select groupagentcode from laagent where agentcode ='"+arr5[0][0]+"'");}catch(ex){};
			try{fm.all('PolApplyDate').value=arr5[0][1];}catch(ex){alert(ex.message)};
			try{fm.all('appnt_AppntNo').value=arr5[0][2];}catch(ex){alert(ex.message)};
			try{fm.all('appnt_AppntName').value=arr5[0][3];}catch(ex){alert(ex.message)};
			try{fm.all('appnt_IDType').value=arr5[0][4];}catch(ex){alert(ex.message)};
			try{fm.all('appnt_IDNo').value=arr5[0][5];}catch(ex){alert(ex.message)};
			try{fm.all('appnt_AppntSex').value=arr5[0][6];}catch(ex){alert(ex.message)};
			try{fm.all('appnt_AppntBirthday').value=arr5[0][7];}catch(ex){alert(ex.message)};
			try{fm.all('PayIntv').value=arr5[0][8];}catch(ex){alert(ex.message)};
			try{fm.all('PayMode').value=arr5[0][9];}catch(ex){alert(ex.message)};
			try{fm.all('BankCode').value=arr5[0][10];}catch(ex){alert(ex.message)};
			try{fm.all('BankAccNo').value=arr5[0][11];}catch(ex){alert(ex.message)};
			try{fm.all('DueFeeMsgFlag').value=arr5[0][12];}catch(ex){alert(ex.message)};
			try{fm.all('AccName').value=arr5[0][13];}catch(ex){alert(ex.message)};
			try{fm.all('AgentName').value=easyExecSql("select name from laagent where agentcode ='"+arr5[0][0]+"'");}catch(ex){};
			try{fm.all('SaleChnl').value=arr5[0][14];}catch(ex){};
			if (fm.all('SaleChnl').value == "18" || fm.all('SaleChnl').value == "20"||fm.all('SaleChnl').value == "03"||fm.all('SaleChnl').value == "10"||fm.all('SaleChnl').value == "22") {
				fm.all("MixComFlag").style.display = "none";
				fm.MixComFlag.checked = false;
		  	}
			try{fm.all('SaleChnlName').value=arr5[0][16];}catch(ex){};
			try{fm.all('AgentCom').value=arr5[0][15];}catch(ex){};
			try{fm.all('AgentComName').value=easyExecSql("select name from lacom where agentcom ='"+arr5[0][15]+"'");}catch(ex){};
			try{fm.all('AgentSaleCode').value=arr5[0][17];}catch(ex){};
			try{fm.all('ManageCom').value=arr5[0][18];}catch(ex){};
			try{fm.all('ManageComName').value=easyExecSql("select name from ldcom where comcode ='"+arr5[0][18]+"'");}catch(ex){};
			var arrResult = easyExecSql("select * from lccont where PrtNo='"+PrtNo+"'");
			try{fm.all('GrpAgentCom').value = arrResult[0][102]; }catch(ex){};
            try{fm.all('GrpAgentCode').value = arrResult[0][103]; }catch(ex){};
            try{fm.all('GrpAgentName').value = arrResult[0][104]; }catch(ex){};
			try{fm.all('Crs_SaleChnl').value = arrResult[0][106]; }catch(ex){};
			try{fm.all('Crs_BussType').value = arrResult[0][107]; }catch(ex){};
            try{fm.all('GrpAgentIDNo').value = arrResult[0][108]; } catch(ex) { };

            var yj = easyExecSql("select codename('crs_busstype',Crs_BussType) from lccont where 1=1 and PrtNo = '" + fm.PrtNo.value + "' ");
            try{fm.all('Crs_BussTypeName').value = yj[0][0]; } catch(ex) { };

			if(arrResult[0][102] != "" && arrResult[0][103] != "" &&arrResult[0][104]!=""&&arrResult[0][106]!=""&&arrResult[0][107]!=""&&arrResult[0][108]!="")
            {   
               fm.MixComFlag.checked = true;
               if(fm.MixComFlag.checked == true)
              {
                 fm.all('GrpAgentComID').style.display = "";
	             fm.all('GrpAgentTitleID').style.display = "";
	             fm.all('GrpAgentTitleIDNo').style.display = "";
              }
              var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
		      var arrResult1 = easyExecSql(strSql);
		      if (arrResult1 != null) {
		         fm.GrpAgentComName.value = arrResult1[0][0];
		      }
		      else{  
		         fm.GrpAgentComName.value = "";
	          }
            }
			
			}
			if(fm.ContNo.value!=null&&fm.ContNo.value!=""&&fm.ContNo.value!="null"){
				var appnt_info = easyExecSql("select idstartdate,idenddate,OccupationCode,OccupationType,Salary,Position,nativeplace,nativecity from lcappnt where contno='"+fm.ContNo.value+"'");
				if( appnt_info ) {
					try{fm.all('appnt_IDStartDate').value=appnt_info[0][0];}catch(ex){alert(ex.message)};
			        try{fm.all('appnt_IDEndDate').value=appnt_info[0][1];}catch(ex){alert(ex.message)};
			        try{fm.all('appnt_OccupationCode').value=appnt_info[0][2];}catch(ex){alert(ex.message)};
			        try{fm.all('appnt_OccupationType').value=appnt_info[0][3];}catch(ex){alert(ex.message)};
			        try{fm.all('appnt_Salary').value=appnt_info[0][4];}catch(ex){alert(ex.message)};
				    try{fm.all('appnt_Position').value=appnt_info[0][5];}catch(ex){alert(ex.message)};
				    try{fm.all('appnt_NativePlace').value=appnt_info[0][6];}catch(ex){alert(ex.message)};
				    try{fm.all('appnt_NativeCity').value=appnt_info[0][7];}catch(ex){alert(ex.message)};
				    try{fm.all('appnt_NativePlaceName').value="";}catch(ex){alert(ex.message)};
				}
					
					var arr2 = easyExecSql("select postaladdress,mobile,email,zipcode,GrpName,HomeCode,HomeNumber,PostalCity,PostalCommunity,PostalCounty,PostalProvince,PostalStreet,phone from lcaddress where customerno ='"+fm.appnt_AppntNo.value+"' and addressno='"+addressno+"'");
					if(arr2){
					try{fm.all('appnt_PostalAddress').value=arr2[0][0];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_Mobile').value=arr2[0][1];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_EMail').value=arr2[0][2];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_ZipCode').value=arr2[0][3];}catch(ex){alert(ex.message)};
					try{fm.all('WorkName').value=arr2[0][4];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_HomeCode').value=arr2[0][5];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_HomeNumber').value=arr2[0][6];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_PostalCommunity').value=arr2[0][8];}catch(ex){alert(ex.message)};
					var rs1 =easyExecSql("select codename from ldcode1 where codetype='province1' and code= '"+arr2[0][10]+"' and code1 ='0' ");
					var rs2 =easyExecSql("select codename from ldcode1 where codetype='city1' and code= '"+arr2[0][7]+"' and code1 ='"+arr2[0][10]+"' ");
					var rs3 =easyExecSql("select codename from ldcode1 where codetype='county1' and code= '"+arr2[0][9]+"' and code1 ='"+arr2[0][7]+"' ");
					if(rs1 != null){
						try{fm.all('Province').value=arr2[0][10];}catch(ex){alert(ex.message)};
						try{fm.all('City').value=arr2[0][7];}catch(ex){alert(ex.message)};
						try{fm.all('County').value=arr2[0][9];}catch(ex){alert(ex.message)};
						try{fm.all('appnt_PostalProvince').value=rs1;}catch(ex){alert(ex.message)};
						try{fm.all('appnt_PostalCity').value=rs2;}catch(ex){alert(ex.message)};
						try{fm.all('appnt_PostalCounty').value=rs3;}catch(ex){alert(ex.message)};
					}else{
						var rs4 =easyExecSql("select code from ldcode1 where codetype='province1' and codename = '"+arr2[0][10]+"'");
						var rs5 =easyExecSql("select code from ldcode1 where codetype='city1' and codename = '"+arr2[0][7]+"'");
						var rs6 =easyExecSql("select code from ldcode1 where codetype='county1' and codename = '"+arr2[0][9]+"'");
						try{fm.all('Province').value=rs4;}catch(ex){alert(ex.message)};
						try{fm.all('City').value=rs5;}catch(ex){alert(ex.message)};
						try{fm.all('County').value=rs6;}catch(ex){alert(ex.message)};
						try{fm.all('appnt_PostalProvince').value=arr2[0][10];}catch(ex){alert(ex.message)};
						try{fm.all('appnt_PostalCity').value=arr2[0][7];}catch(ex){alert(ex.message)};
						try{fm.all('appnt_PostalCounty').value=arr2[0][9];}catch(ex){alert(ex.message)};
					}
					try{fm.all('appnt_PostalStreet').value=arr2[0][11];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_Phone').value=arr2[0][12];}catch(ex){alert(ex.message)};
					
					}
			var str1 ="select countytype,familysalary from LCContSub where prtno ='"+PrtNo+"'";
			var arr1 = easyExecSql(str1);	
			if(arr1){
			try{fm.all('appnt_CountyType').value=arr1[0][0];}catch(ex){alert(ex.message)};
			try{fm.all('FamilySalary').value=arr1[0][1];}catch(ex){alert(ex.message)};
			}
		}
		
	}
	showAllCodeName();
	controlNativeCity("");
}

//查询代理机构
function queryAgentCom(){
    if(fm.all('ManageCom').value==""){
        alert("请先录入管理机构信息！");
        return;
    }
    var newWindow = window.open("./LAComMain.jsp?ManageCom="+fm.all('ManageCom').value,"LAComQueryInput",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
}

//根据客户号码和地址代码查询客户地址信息
function queryAddress(CustomerNo,AddressNo){
	// var tPrtNo=fm.PrtNo.value;
	if(AddressNo!="" && AddressNo!= null && AddressNo!="null"){
//			alert(tPrtNo);
		//var arr = easyExecSql("select PostalAddress,ZipCode,HomePhone,Mobile,CompanyPhone,EMail from lcaddress where CustomerNo in (select appntno from lccont where prtno='"+ tPrtNo +"')  and AddressNo='"+AddressNo+"'");
        var sSQL = "select grpname from lcaddress where CustomerNo in(select insuredno from lcinsured where prtno = '"+fm.PrtNo.value+"')  fetch first 1 rows only";
        var arrResult = easyExecSql(sSQL);
        try{fm.all('insured_WorkName').value=arrResult[0][0];}catch(ex){alert(ex.message)};  
		
		// 简易件被保人地址信息，读取时，读成投保人的地址信息了。
		var tTmpStrSQL = "select PostalAddress,ZipCode,HomePhone,Mobile,CompanyPhone,EMail,Phone "
		    + " from lcaddress "
		    + " where CustomerNo = '" + CustomerNo + "'  and AddressNo='" + AddressNo + "'";
		var arr = easyExecSql(tTmpStrSQL);
		// --------------------------------------------
		
//		alert(arr);CustomerNo!="" && CustomerNo!= null && CustomerNo!="null" && 
		if(arr){
			return arr;
		}else{
			return null;
		}
	}else{
		return null;
	}
}

//双击下拉后操作
function afterCodeSelect(tCodeName,tObj,InputObj){
	
	//关闭社保渠道勾选交叉销售功能（社保渠道不得选择交叉销售业务）
	if(tCodeName=="unitesalechnl"){
		fm.all("MixComFlag").style.display="";
		fm.MixComFlag.checked = false;
		if (fm.SaleChnl.value == "18" || fm.SaleChnl.value == "20"||fm.SaleChnl.value == "03"||fm.SaleChnl.value == "10"||fm.SaleChnl.value == "22") {
			fm.all("MixComFlag").style.display="none";
			fm.MixComFlag.checked = false;
		}
		isMixCom();
	}
	
	
    if(!InputObj){
        InputObj=tObj;
    }
    var SaleChnl = fm.all('SaleChnl').value;

    if(SaleChnl=="03")
    {
        AgentCodeG.style.display='none';
        TXCode.style.display='';
        AgentNameG.style.display='none';
        TXName.style.display='';
        zhongjiecode.style.display='';
        zhongjiename.style.display='';
        agentComCode = "agentcominput";
        BlankTD1.style.display='none';
        BlankTD2.style.display='none';
        fm.all("AgentSaleCodeID").style.display='';
    }
    else if(SaleChnl=="03")
    {
        AgentCodeG.style.display='none';
        TXCode.style.display='';
        AgentNameG.style.display='none';
        TXName.style.display='';
        zhongjiecode.style.display='';
        zhongjiename.style.display='';
        agentComCode = "agentcominput";
        BlankTD1.style.display='none';
        BlankTD2.style.display='none';
        fm.all("AgentSaleCodeID").style.display='';
    }else if(SaleChnl=="20")
    {
        AgentCodeG.style.display='none';
        TXCode.style.display='';
        AgentNameG.style.display='none';
        TXName.style.display='';
        zhongjiecode.style.display='';
        zhongjiename.style.display='';
        agentComCode = "agentcominput";
        BlankTD1.style.display='none';
        BlankTD2.style.display='none';
        fm.all("AgentSaleCodeID").style.display='';
    }
    else if(SaleChnl=="10" || SaleChnl=="15")
    {
    	AgentCodeG.style.display='none';
        TXCode.style.display='';
        AgentNameG.style.display='none';
        TXName.style.display='';
        zhongjiecode.style.display='';
        zhongjiename.style.display='';
        agentComCode = "agentcominput";
        BlankTD1.style.display='none';
        BlankTD2.style.display='none';
        fm.all("AgentSaleCodeID").style.display='';
    }
    else if(SaleChnl == "04" )
    {
        AgentCodeG.style.display='none';
        TXCode.style.display='';
        AgentNameG.style.display='none';
        TXName.style.display='';
        zhongjiecode.style.display='';
        zhongjiename.style.display='';
        agentComCode = "agentcombank";
        BlankTD1.style.display='none';
        BlankTD2.style.display='none';
        fm.all("AgentSaleCodeID").style.display='';
    }
    else if(SaleChnl=="08")
    {
        AgentCodeG.style.display='none';
        TXCode.style.display='';
        AgentNameG.style.display='none';
        TXName.style.display='';
        zhongjiecode.style.display='';
        zhongjiename.style.display='';
        agentComCode = "agentcomphone";
    }
    else if(SaleChnl == "11" || SaleChnl == "12")
    {
        AgentCodeG.style.display='none';
        TXCode.style.display='';
        AgentNameG.style.display='none';
        TXName.style.display='';
        BlankTD1.style.display='none';
        BlankTD2.style.display='none';
        zhongjiecode.style.display='';
        zhongjiename.style.display='';
        agentComCode = "agentcominput";
        var tBranchType = "2";
        var tBranchType2 = "";
        if (SaleChnl == "11")    tBranchType2 = "04";
        if (SaleChnl == "12")    tBranchType2 = "04";
        manageComCode = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value;
    }
    else if(SaleChnl == "01" || SaleChnl == "02" || SaleChnl == "13" || SaleChnl == "14"){
    	AgentCodeG.style.display='';
        TXCode.style.display='none';
        AgentNameG.style.display='';
        TXName.style.display='none';
        zhongjiecode.style.display='none';
        fm.AgentCom.value="";
        fm.AgentComName.value='';
        zhongjiename.style.display='none';
        BlankTD1.style.display='';
        BlankTD2.style.display='';
    }
    else
    {
        //SaleManagecom.style.display='none';
        AgentCodeG.style.display='';
        TXCode.style.display='none';
        AgentNameG.style.display='';
        TXName.style.display='none';
        zhongjiecode.style.display='none';
        zhongjiename.style.display='none';
        BlankTD1.style.display='';
        BlankTD2.style.display='';
    }
    if(SaleChnl=="10" || SaleChnl=="15"|| SaleChnl=="04"|| SaleChnl=="03"|| SaleChnl=="20"){
    	fm.all("AgentSaleCodeID").style.display='';
    	if(fm.AgentSaleCode.value != ""){
    		var strSql2 = "select Name from laagenttemp where '1337927697000'='1337927697000' and  AgentCode='" + fm.AgentSaleCode.value+"' with ur";
    		var arrResult2 = easyExecSql(strSql2);
    		if (arrResult2 != null) {
    		     fm.AgentSaleName.value = arrResult2[0][0];
    		}
    	}
    } else {
    	fm.all("AgentSaleCodeID").style.display='none';
    	fm.AgentSaleCode.value="";
    	fm.AgentSaleName.value="";
    }
    
    if(SaleChnl != "11" && SaleChnl != "12")
    {
    	if(SaleChnl == "03")
    	{
    		var tBranchType="#2";
			var tBranchType2="02#,#04";
			manageComCode = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
    	}
    	else if(SaleChnl == "10")
    	{
    		var tBranchType="#1";
			var tBranchType2="02#,#04";
			manageComCode = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
    	}
    	else if(SaleChnl == "15")
    	{
    		var tBranchType="#5";
			var tBranchType2="01";
			manageComCode = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
    	}else if(SaleChnl == "20"){
            var tBranchType = "#6";
            var tBranchType2 = "02";
    		manageComCode = tBranchType + "^" + tBranchType2 + "^" + fm.ManageCom.value + "#";
    	}
    	else
    	{
        	manageComCode = fm.all('ManageCom').value;
        }
    }
    
    if(tCodeName=="Relation" && InputObj.name=="insured_RelationToAppnt"){
        dealRelationToAppnt(InputObj.value);
        // 2007-04-28 update LY
        getAge();
    }
    //自动填写受益人信息
    if (tCodeName == "customertype") {
        if (tObj.value == "A") {
            var index = BnfGrid.mulLineCount;
            BnfGrid.setRowColData(index-1, 2, fm.all("appnt_AppntName").value);
            BnfGrid.setRowColData(index-1, 3, fm.all("appnt_AppntSex").value);
            BnfGrid.setRowColData(index-1, 4, fm.all("appnt_IDType").value);
            BnfGrid.setRowColData(index-1, 5, fm.all("appnt_IDNo").value);
            //BnfGrid.setRowColData(index-1, 8, fm.all("AppntHomeAddress").value);
            BnfGrid.setRowColData(index-1, 9, "A");
        }
        else if (tObj.value == "I") 
        {
            var index = BnfGrid.mulLineCount;
            if(BnfGrid.getRowColData(index-1,1)=="1"){
                alert("身故受益人不能选择被保险人本人");
                BnfGrid.setRowColData(index-1, 2, "");
                BnfGrid.setRowColData(index-1, 3, "");
                BnfGrid.setRowColData(index-1, 4, "");
                BnfGrid.setRowColData(index-1, 5, "");
                BnfGrid.setRowColData(index-1, 6, "");
                BnfGrid.setRowColData(index-1, 7, "");
                BnfGrid.setRowColData(index-1, 8, "");
            } else {
                BnfGrid.setRowColData(index-1, 2, fm.all("insured_Name").value);
                BnfGrid.setRowColData(index-1, 3, fm.all("insured_Sex").value);
                BnfGrid.setRowColData(index-1, 4, fm.all("insured_IDType").value);
                BnfGrid.setRowColData(index-1, 5, fm.all("insured_IDNo").value);
                BnfGrid.setRowColData(index-1, 6, "00");
                //BnfGrid.setRowColData(index-1, 8, fm.all("AppntHomeAddress").value);
                BnfGrid.setRowColData(index-1, 9, "I");
            }
        }
    }

    //银行网点选中后自动带出银代专员信息   2008-3-13
    if (tCodeName == "agentcombank")
    {
        fm.AgentCode.value = "";
        fm.GroupAgentCode.value = "";
        fm.AgentName.value = "";
        fm.AgentGroup.value = "";
        if(SaleChnl == "04")
        {
            var agentCodeSql = "select a.AgentCode, b.Name, b.AgentGroup,b.groupagentcode from LAComToAgent a left join LAAgent b "
            + "on a.AgentCode = b.AgentCode where AgentCom='"
            + fm.AgentCom.value
            + "' and RelaType='1' and b.BranchType = '3' and b.BranchType2 = '01' and b.groupagentcode is not null and b.groupagentcode<>'' ";
            var arr = easyExecSql(agentCodeSql);
            if(arr){
                fm.AgentCode.value = arr[0][0];
                fm.AgentName.value = arr[0][1];
                fm.AgentGroup.value = arr[0][2];
                fm.GroupAgentCode.value = arr[0][3];
            }
        }
    }

    //中介   2008-3-13
    if (tCodeName == "agentcombrief")
    {
        
    }

    //银行网点选中后自动带出银代专员信息   2008-4-10
    if (tCodeName == "agentcomphone")
    {
        fm.AgentCode.value = "";
        fm.AgentName.value = "";
        fm.AgentGroup.value = "";
        if(SaleChnl == "08")
        {
            var agentCodeSql = "select a.AgentCode, b.Name, b.AgentGroup from LAComToAgent a left join LAAgent b "
            + "on a.AgentCode = b.AgentCode where AgentCom='"
            + fm.AgentCom.value
            + "' and RelaType='1' and b.BranchType = '4' and b.BranchType2 = '01'";
            var arr = easyExecSql(agentCodeSql);
            if(arr){
                fm.AgentCode.value = arr[0][0];
                fm.AgentName.value = arr[0][1];
                fm.AgentGroup.value = arr[0][2];
            }
        }
    }
   if(tCodeName=="NativePlace" && InputObj.name=="appnt_NativePlace"){
        if (tObj.value == "OS"){
           fm.all("NativePlace").style.display = "";
	       fm.all("NativeCity").style.display = "";
	       fm.all("NativeCityTitle").innerHTML="国家";
        }else if (tObj.value == "HK"){
           fm.all("NativePlace").style.display = "";
	       fm.all("NativeCity").style.display = "";
	       fm.all("NativeCityTitle").innerHTML="地区";
        }else{
           fm.all("NativePlace").style.display = "none";
	       fm.all("NativeCity").style.display = "none";
        }
        fm.appnt_NativeCity.value="";
        fm.appnt_NativeCityName.value="";
    }
}
//处理被保人与投保人的关系
function dealRelationToAppnt(tReation){
	if(tReation == "00"){
		//是投保人本人．
		InsuredInfoDiv.style.display="none";
		insured_InsuredNoText.style.display = "none";
		insured_InsuredNoClass.style.display = "none";
		fm.esayQuery.style.display = "none";
	}else{
		InsuredInfoDiv.style.display="";
		insured_InsuredNoText.style.display = "";
		insured_InsuredNoClass.style.display = "";
		fm.esayQuery.style.display = "";
	}
}

//处理修改操作
function updateClick(){

	var appntIDType=fm.appnt_IDType.value;
    if(appntIDType=="0"||appntIDType=="5")
    {
    	var appntIDNo=fm.appnt_IDNo.value;
    	if(appntIDNo != null && appntIDNo != ""){
    		var strChkIDNo=checkIdNo(appntIDType,appntIDNo,fm.appnt_AppntBirthday.value,fm.appnt_AppntSex.value);
    		if(strChkIDNo !=""){
    			alert(strChkIDNo);
    			return false;
    		}
    	}
    }
	
	if (verifyInput2()== false) 
    {        	
        return false;
    }
	//选择交叉销售后，对交叉销售各不为空项校验。 zxs 
	if(!MixComCheck())
	{
	  return false;
	}
	
	//var len=parseInt("6");
    // 集团交叉业务要素校验
	var havesql ="select * from lwmission where missionprop2='"+fm.PrtNo.value+"'";
    var missionc=easyExecSql(havesql);
    if(missionc!=null){
    alert("该保单已录入完毕，不能再进行修改！");
    return false;
    }
    
    if(!checkNative()){
	   return false;
	}
	
	if(!checkSaleChnl()){
        return false;
    }
    
    if(!checkCrsBussParams())
    {
        return false;
    }
    
    if(!ExtendCheck()){
    	return false;
    }
    
    if(!checkAllapp()){
 	   return false;
 	}
    
    //zxs 20190522
	if(!checkAppDate()){
		return false;
	}
	
	 if(fm.appnt_AppntNo.value!=null&&fm.appnt_AppntNo.value!=""){
  var tsql="select Name, IDType, IDNo, Birthday,Sex from LDPerson where customerno='"+fm.appnt_AppntNo.value+"'";
  var  cc1=easyExecSql(tsql);
if(cc1){

  if(fm.all('appnt_AppntName').value!=cc1[0][0]&&fm.all('appnt_IDType').value!=cc1[0][1]&&fm.all('appnt_IDNo').value!=cc1[0][2]&&fm.all('appnt_AppntBirthday').value!=cc1[0][3]&&fm.all('appnt_AppntSex').value!=cc1[0][4]){
    alert("输入投保人五要素与投保人信息不符！");
    
    return false;
  
   }
  }else{
  alert("投保人客户号有误，请重新录入");
  return false;
  }  
  } 
  
  var appZipcode=fm.appnt_ZipCode.value;
    var len=parseInt("6");
    var saleChnl = fm.SaleChnl.value;
    
    
    if(appZipcode!=""){
        if(appZipcode.length !=len){
            alert("投保人邮编长度必须等于6!");
            return false;
        }
    }

    if(fm.PolApplyDate.value== "")
    {
        alert("投保单申请日期必须填写!");
        return false;
    }
   
    var appntIDType = fm.appnt_IDType.value;
    
//    if(!checkIDType(appntIDType))
//    {
//    	alert("投保人证件类型填写有误，不符合系统规定0到4范围内，请进行修改！");
//    	return false;
//    }
  
	//alert(fm.BankCode.value);
	//alert(fm.BankAccNo.value);
	//alert(fm.AccName.value);
 // if(!fm.BankCode.value!=null&&!fm.BankAccNo.value!=null&&!fm.AccName.value!=null){
    
 //   alert("开户银行、银行账号，户名三者必须同时填写或都不填写");
//    return false;
 //   }
       if(fm.PayMode.value ==""){
        alert("缴费方式不能为空");
        return false;
    }
    else if(fm.PayMode.value =="4"||fm.PayMode.value =="6"){
        if(fm.BankCode.value ==""){
            alert("开户银行不能为空");
            return false;
        }
        if(fm.BankAccNo.value ==""){
            alert("账号不能为空");
            return false;
        }
        if(fm.AccName.value ==""){
            alert("户名不能为空");
            return false;
        }
       } 
	var strsql="select agentgroup from laagent where agentcode='"+fm.AgentCode.value+"'";
    var agentcode =easyExecSql(strsql);
    fm.AgentGroup.value=agentcode;
	fm.fmAction.value = "UPDATE||MAIN" ;
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./FamilyContSave.jsp";
	fm.submit(); //提交
}
function BriefInputConfirm(ActivityID,tContType){
	var MissionID = fm.MissionID.value;
	var SubMissionID = fm.SubMissionID.value;
	var PayMethod = fm.PayMethod.value;
	 //校验代理机构及代理机构业务员
    if(!checkAgentComAndSaleCdoe()){
    	return false;
    }
    
    if(!checkAgentCode()){
    	return false;
    }
    
    if(!checkAppnt()){
    	return false;
    }
    
    if(!checkInsured()){
    	return false;
    }
    
	if(tContType !=4 ){
		if(MissionID == null || MissionID == "null" || MissionID == ""){
			alert("没有查询到MissionID");
			return;
		}
		if(SubMissionID == null || SubMissionID == "null" || SubMissionID == ""){
			alert("没有查询到SubMissionID");
			return;
		}
		if(ActivityID == null || ActivityID == "null" || ActivityID == ""){
			alert("没有查询到ActivityID");
			return;
		}
	}
	
	if(tContType == "6" && PayMethod== "01"){	
	    var tPrtNo = fm.PrtNo.value;
        if(!chkAppAcc(tPrtNo))
        {
            return false;
        }
	
	}


	var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  if(tContType == "4"){
  	fm.action="BriefCardInputConfirm.jsp";
  }else{
// alert(ContType) 	;
	  fm.action="BriefSingleContInputConfirm.jsp?ActivityID="+ActivityID;
	}
  ConfirmFlag = true;
  fm.submit(); //提交
}
function queryAppnt(){
    var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
	showInfo = window.open( "../sys/LDPersonQueryNew2.jsp?Flag=appnt" ,"",sFeatures);
}
function afterQueryAppnt(arrResult){

	if(arrResult){
		var CustomerNo = arrResult[0][0];
		var strSql = "select a.name,a.sex,a.birthday,a.idtype,a.IDNo,a.CustomerNo,a.Grpname ,a.nativeplace,a.nativecity from ldperson a where a.CustomerNo='"+CustomerNo+"' ";
		var arr = easyExecSql(strSql); 
		if(arr){
			try{fm.all('appnt_AppntName').value=arr[0][0];}catch(ex){alert(ex.message)};       			
			try{fm.all('appnt_AppntSex').value=arr[0][1];}catch(ex){alert(ex.message)};                                                                               
			try{fm.all('appnt_AppntBirthday').value=arr[0][2];}catch(ex){alert(ex.message)};   
			try{fm.all('appnt_IDType').value=arr[0][3];}catch(ex){alert(ex.message)};          
			try{fm.all('appnt_IDNo').value=arr[0][4];}catch(ex){alert(ex.message)};            
			try{fm.all('appnt_AppntNo').value=arr[0][5];}catch(ex){alert(ex.message)}; 
			try{fm.all('WorkName').value=arr[0][6];}catch(ex){alert(ex.message)}; 
			try{fm.all('appnt_NativePlace').value=arr[0][7];}catch(ex){alert(ex.message)};
			try{fm.all('appnt_NativeCity').value=arr[0][8];}catch(ex){alert(ex.message)};
			try{fm.all('appnt_NativePlaceName').value="";}catch(ex){alert(ex.message)};			 
			var strSql = "select a.PostalAddress,a.HomePhone,a.Mobile,a.CompanyPhone,a.EMail,a.AddressNo,a.zipcode,a.Phone,a.PostalProvince,a.PostalCity,a.PostalCounty,a.PostalStreet,a.PostalCommunity,a.homecode,a.homenumber from LCaddress a where a.CustomerNo='"+CustomerNo+"' order by AddressNo desc";		
			var arr = easyExecSql(strSql);
			if(arr){
				try{fm.all('appnt_PostalAddress').value=arr[0][0];}catch(ex){alert(ex.message)};   
				try{fm.all('appnt_HomePhone').value=arr[0][1];}catch(ex){alert(ex.message)};       
				try{fm.all('appnt_Mobile').value=arr[0][2];}catch(ex){alert(ex.message)};          
				try{fm.all('appnt_CompanyPhone').value=arr[0][3];}catch(ex){alert(ex.message)};    
				try{fm.all('appnt_EMail').value=arr[0][4];}catch(ex){alert(ex.message)};          
				
				try{fm.all('appnt_ZipCode').value=arr[0][6];}catch(ex){alert(ex.message)};     
				try{fm.all('appnt_Phone').value=arr[0][7];}catch(ex){alert(ex.message)};
				var rs1 =easyExecSql("select codename from ldcode1 where codetype='province1' and code= '"+arr[0][8]+"' and code1 ='0' ");
				var rs2 =easyExecSql("select codename from ldcode1 where codetype='city1' and code= '"+arr[0][9]+"' and code1 ='"+arr[0][8]+"' ");
				var rs3 =easyExecSql("select codename from ldcode1 where codetype='county1' and code= '"+arr[0][10]+"' and code1 ='"+arr[0][9]+"' ");
				if(rs1 != null){
					try{fm.all('Province').value=arr[0][8];}catch(ex){alert(ex.message)};
					try{fm.all('City').value=arr[0][9];}catch(ex){alert(ex.message)};
					try{fm.all('County').value=arr[0][10];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_PostalProvince').value=rs1;}catch(ex){alert(ex.message)};
					try{fm.all('appnt_PostalCity').value=rs2;}catch(ex){alert(ex.message)};
					try{fm.all('appnt_PostalCounty').value=rs3;}catch(ex){alert(ex.message)};
				}else{
					var rs4 =easyExecSql("select code from ldcode1 where codetype='province1' and codename = '"+arr[0][8]+"'");
					var rs5 =easyExecSql("select code from ldcode1 where codetype='city1' and codename = '"+arr[0][9]+"'");
					var rs6 =easyExecSql("select code from ldcode1 where codetype='county1' and codename = '"+arr[0][10]+"'");
					try{fm.all('Province').value=rs4;}catch(ex){alert(ex.message)};
					try{fm.all('City').value=rs5;}catch(ex){alert(ex.message)};
					try{fm.all('County').value=rs6;}catch(ex){alert(ex.message)};
					try{fm.all('appnt_PostalProvince').value=arr[0][8];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_PostalCity').value=arr[0][9];}catch(ex){alert(ex.message)};
					try{fm.all('appnt_PostalCounty').value=arr[0][10];}catch(ex){alert(ex.message)};
				}
				try{fm.all('appnt_PostalStreet').value=arr[0][11];}catch(ex){alert(ex.message)};
				try{fm.all('appnt_PostalCommunity').value=arr[0][12];}catch(ex){alert(ex.message)};
				try{fm.all('appnt_HomeCode').value=arr[0][13];}catch(ex){alert(ex.message)};
				try{fm.all('appnt_HomeNumber').value=arr[0][14];}catch(ex){alert(ex.message)};	
				try{fm.all('appnt_PostalAddress').value=arr[0][0];}catch(ex){alert(ex.message)};   
				try{fm.all('appnt_Mobile').value=arr[0][2];}catch(ex){alert(ex.message)};             

			}else{
				try{fm.all('appnt_PostalAddress').value="";}catch(ex){alert(ex.message)};   
				try{fm.all('appnt_HomePhone').value="";}catch(ex){alert(ex.message)};       
				try{fm.all('appnt_Mobile').value="";}catch(ex){alert(ex.message)};          
				try{fm.all('appnt_CompanyPhone').value="";}catch(ex){alert(ex.message)};    
				try{fm.all('appnt_EMail').value="";}catch(ex){alert(ex.message)};          
				try{fm.all('appnt_ZipCode').value="";}catch(ex){alert(ex.message)};    
				try{fm.all('appnt_Phone').value="";}catch(ex){alert(ex.message)};
				try{fm.all('appnt_PostalProvince').value="";}catch(ex){alert(ex.message)};
				try{fm.all('appnt_PostalCity').value="";}catch(ex){alert(ex.message)};
				try{fm.all('appnt_PostalCounty').value="";}catch(ex){alert(ex.message)};
				try{fm.all('appnt_PostalStreet').value="";}catch(ex){alert(ex.message)};
				try{fm.all('appnt_PostalCommunity').value="";}catch(ex){alert(ex.message)};
				try{fm.all('appnt_HomeCode').value="";}catch(ex){alert(ex.message)};
				try{fm.all('appnt_HomeNumber').value="";}catch(ex){alert(ex.message)};
			}
		}
	}
	showAllCodeName();
	controlNativeCity("");
}
function queryInsured(){
    var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
	showInfo = window.open( "../sys/LDPersonQueryNew2.jsp?Flag=insured","",sFeatures);	
}
function afterQueryInsured(arrResult){
	if(arrResult){
		var CustomerNo = arrResult[0][0];
		//alert(CustomerNo);
		var strSql = "select a.name,a.sex,a.birthday,a.englishname,a.idtype,a.IDNo,a.CustomerNo from ldperson a where a.CustomerNo='"+CustomerNo+"' ";
		var arr = easyExecSql(strSql);
//		alert(arr);
		if(arr){

			try{fm.all('insured_Name').value=arr[0][0];}catch(ex){alert(ex.message)};       
			try{fm.all('insured_Sex').value=arr[0][1];}catch(ex){alert(ex.message)};        
			try{fm.all('insured_Birthday').value=arr[0][2];}catch(ex){alert(ex.message)};   
			try{fm.all('insured_EnglishName').value=arr[0][3];}catch(ex){alert(ex.message)};     
			try{fm.all('insured_IDType').value=arr[0][4];}catch(ex){alert(ex.message)};          
			try{fm.all('insured_IDNo').value=arr[0][5];}catch(ex){alert(ex.message)};     
			try{fm.all('insured_InsuredNo').value=arr[0][6];}catch(ex){alert(ex.message)};    
			var strSql = "select a.PostalAddress,a.HomePhone,a.Mobile,a.CompanyPhone,a.EMail,a.AddressNo,a.zipcode,a.Phone from LCaddress a where a.CustomerNo='"+CustomerNo+"' order by AddressNo desc";
			var arr = easyExecSql(strSql);
			if(arr){

				try{fm.all('insured_PostalAddress').value=arr[0][0];}catch(ex){alert(ex.message)};   
				try{fm.all('insured_HomePhone').value=arr[0][1];}catch(ex){alert(ex.message)};       
				try{fm.all('insured_Mobile').value=arr[0][2];}catch(ex){alert(ex.message)};          
				try{fm.all('insured_CompanyPhone').value=arr[0][3];}catch(ex){alert(ex.message)};    
				try{fm.all('insured_EMail').value=arr[0][4];}catch(ex){alert(ex.message)};          
				try{fm.all('insured_AddressNo').value=arr[0][5];}catch(ex){alert(ex.message)};
				try{fm.all('insured_ZipCode').value=arr[0][6];}catch(ex){alert(ex.message)};      
				try{fm.all('insured_Phone').value=arr[0][7];}catch(ex){alert(ex.message)};
			}else{
				try{fm.all('insured_PostalAddress').value="";}catch(ex){alert(ex.message)};   
				try{fm.all('insured_HomePhone').value="";}catch(ex){alert(ex.message)};       
				try{fm.all('insured_Mobile').value="";}catch(ex){alert(ex.message)};          
				try{fm.all('insured_CompanyPhone').value="";}catch(ex){alert(ex.message)};    
				try{fm.all('insured_EMail').value="";}catch(ex){alert(ex.message)};          
				try{fm.all('insured_ZipCode').value="";}catch(ex){alert(ex.message)}; 
				try{fm.all('insured_Phone').value="";}catch(ex){alert(ex.message)};
			}
		}
	}
}
function showDays(){
	if(!isDate(fm.CInValiDate.value)||!isDate(fm.CValiDate.value))
		return;
	if(LoadFlag=="2"){
		dealDays();
	}
	var strSql = "select integer(to_date('"+fm.CInValiDate.value+"')-to_date('"+fm.CValiDate.value+"'))+1 from dual";	
	var arr=easyExecSql(strSql);
	if(arr)
	{
		fm.Days.value = arr[0][0];
	}
}

function initTrackAccident(){	
    //alert(MissionProp5);	
		//交通意外　高原疾病  卡单 
	if(MissionProp5 == "2" || MissionProp5 == "3" || MissionProp5 == "4" ){
		fm.MissionProp5.value = MissionProp5;
		//隐藏投保单填写日期，收单日期	  	
		InputDateText.style.display = "none";
		InputDateClass.style.display = "none";	 
	  ReceiveDateText.style.display = "none";
		ReceiveDateClass.style.display = "none";
	
		fm.InputDate.value = CurrentDate;
		
		fm.ReceiveDate.value = CurrentDate;
		//投保人客户号，投保人拼音
		//AppntInfoDiv.style .display = "none";
		appnt_EnglishNameClass.style.display = "none";
		appnt_EnglishNameText.style.display = "none";
		
		
		//被保险人客户号,被保险人拼音
		insured_InsuredNoText.style.display = "none";
		insured_InsuredNoClass.style.display = "none";
		fm.esayQuery.style.display = "none";
		insured_EnglishNameText.style.display = "none";
		insured_EnglishClass.style.display= "none";
		
		//隐藏抵达国家，护照号码，全年多次保障标识,责任生效日期
		NationGridDiv.style.display = "none";
		RiskInfoDiv.style.display = "none";	  	  
		fm.CValiDate.value = CurrentDate;
		PayInfoDiv.style.display="none";
		PayInfoTitleDiv.style.display="none";
		//fm.PayIntv.value="12";
		fm.PayIntv.value="";
		
		//将性别，出生日期，证件号码，证件类型该为非必录项
		fm.appnt_PostalAddress.elementtype = "";
		fm.appnt_PostalAddress.verify = "";
		
		fm.appnt_ZipCode.elementtype = "";
		fm.appnt_ZipCode.verify = "";
		
		fm.appnt_HomePhone.elementtype = "";
		fm.appnt_HomePhone.verify = "";
		
		fm.appnt_Mobile.elementtype = "";
		fm.appnt_Mobile.verify = "";
		
		fm.insured_PostalAddress.elementtype = "";
		fm.insured_PostalAddress.verify = "";
		 
		fm.insured_ZipCode.elementtype = "";
		fm.insured_ZipCode.verify = "";
		 
		fm.insured_HomePhone.elementtype = "";
		fm.insured_HomePhone.verify = "";
		 
		fm.insured_Mobile.elementtype = "";
		fm.insured_Mobile.verify = "";
		if(MissionProp5 != "4"){
		 	fm.appnt_SexName.elementtype = "";
		 	fm.appnt_AppntSex.verify = "";
		 	
		 	fm.appnt_AppntBirthday.elementtype = "";
		 	fm.appnt_AppntBirthday.verify = "";
		 	
		 	fm.appnt_IDTypeName.elementtype = "";		 
		 	fm.appnt_IDType.verify = "";
		 	
		 	fm.appnt_IDNo.elementtype = "";
		 	fm.appnt_IDNo.verify = "";
		 	
		 	fm.insured_SexName.elementtype = "";
		 	fm.insured_Sex.verify = "";
		 	
		 	fm.insured_Birthday.elementtype = "";
		 	fm.insured_Birthday.verify = "";
		 	
		 	fm.insured_IDTypeName.elementtype = "";
		 	fm.insured_IDType.verify = "";
		 	
		 	fm.insured_IDNo.elementtype = "";
		 	fm.insured_IDNo.verify = "";		 	
		}
	 	
	 	//身份证号码初始化为0去掉
	 	fm.insured_IDType.value = "";
	 	fm.appnt_IDType.value = "";
	 	
	 	//初始化是被保险人默认为本人
	 	fm.insured_RelationToAppnt.value = "00";      			 
	}
	//将交通工具修改为意外险平台
	if(MissionProp5 == "2"){
		//增加显示生效日期
		RiskInfoDiv.style.display='';   
	 	BnfInfoDiv.style.display='none';	
	 	PassPortNo.style.display='none';	
	 	PassPort.style.display='none';	
	 	TimesFlag.style.display='none';	
	 	TimesFlagNo.style.display='none';	
	 	Reach.style.display='none';	   	
	  ReachType.style.display='none';	 
	  Days.style.display='none';
	  CInValiDate_title_td.style.display='none';	 
	  CInValiDate_common_td.style.display='none';
	  InputDateText.style.display = "";
		InputDateClass.style.display = "";	 
	  ReceiveDateText.style.display = "";
		ReceiveDateClass.style.display = "";
		fm.NeedMulBnf.value = "1";
		fm.RiskWrapFlag.value = "1";
		PayInfoTitleDiv.style.display="";
		PayInfoDiv.style.display="";
		fm.PayMode.value="1";
		fm.CValiDate.value=easyExecSql("select date('"+CurrentDate+"') + 1 day from dual") ; 
	}
	//高原疾病　特有要素
	if(MissionProp5 == "3"){
        //增加显示生效日期
		RiskInfoDiv.style.display='';   
	 	BnfInfoDiv.style.display='none';	
	 	PassPortNo.style.display='none';	
	 	PassPort.style.display='none';	
	 	TimesFlag.style.display='none';	
	 	TimesFlagNo.style.display='none';	
	 	Reach.style.display='none';	   	
        ReachType.style.display='none';
        CInValiDate_title_td.style.display='none';	 
	    CInValiDate_common_td.style.display='none';
        Days.style.display='none';

		table3.style.display="none";
		bnf_table3.style.display="none";
		table6.style.display="none";
		RemarkDiv.style.display="none";
		fm.CValiDate.value = "";
		fm.CValiDate.onfocus=dealHighDay;
		CInValiDate_span.style.display="none";
		RiskInfoGridDiv.style.display="none";
	}
	//高原疾病　特有要素
	if(MissionProp5 == "4"){
		//卡单使用多受益人控制
		fm.NeedMulBnf.value = "1";
		table3.style.display="";
		bnf_table3.style.display="";
		table6.style.display="none";
		RemarkDiv.style.display="none";
		fm.CValiDate.value = "";
		fm.CValiDate.onfocus=dealHighDay;
		CInValiDate_span.style.display="none";
		RiskInfoGridDiv.style.display="none";
		CardDiv.style.display="";
		//ManageInfoDiv1.style.display="none";
		table1.style.display="none";
		BankCom.style.display="none";
		fm.CValiDate.onfocus=dealCardRisk;
		fm.CValiDate.onkeyup=dealCardRisk;
		RiskInfoDiv.style.display='';
		fm.RiskWrapFlag.value='0';
		CInValiDate_title_td.style.display='none';	 
	  CInValiDate_common_td.style.display='none';
	  fm.CValiDate.value = CurrentDate;
	  PassPortNo.style.display='none';	
	 	PassPort.style.display='none';
	 	TimesFlag.style.display='none';	
	 	TimesFlagNo.style.display='none';	
	 	Reach.style.display='none';	   	
	  ReachType.style.display='none';	
	  Days.style.display='none';
	  fm.all('PayIntv').value = '0';
	  fm.all('PayMode').value = '1';
	}
		
 	// 境外救援，加入多受益人录入。
 	if(MissionProp5 == "1")
 	{
 	    // 显示受益人录入框
        fm.NeedMulBnf.value = "1";
 	}
 	
    //加入少儿保险判断，电销平台显示同少儿险。
    if (MissionProp5 == "6" || "5" == MissionProp5)
    {
        //投保人客户号，投保人拼音
        //AppntInfoDiv.style.display = "none";
        appnt_EnglishNameClass.style.display = "none";
        appnt_EnglishNameText.style.display = "none";
        
        //被保险人客户号,被保险人拼音
        insured_InsuredNoText.style.display = "none";
        insured_InsuredNoClass.style.display = "none";
        fm.esayQuery.style.display = "none";
        insured_EnglishNameText.style.display = "none";
        insured_EnglishClass.style.display= "none";
        
        // 显示受益人录入框
        fm.NeedMulBnf.value = "1";
        
        fm.MissionProp5.value = MissionProp5; 	
        fm.SaleChnl.value = "04";
        fm.insured_RelationToAppnt.value = "00";
        
        fm.RiskWrapFlag.value = "1";
        OccupationCodeClass.style.display='';
        OccupationCodeText.style.display=''; 
        RiskWrapInfoGridDiv.style.display='';
        WorkNameText.style.display=''; 
        WorkNameClass.style.display=''; 
        RiskInfoGridDiv.style.display='none';
        NationGridDiv.style.display='none';
        RiskInfoDiv.style.display='';   
        BnfInfoDiv.style.display='none';	
        PassPortNo.style.display='none';	
        PassPort.style.display='none';	
        TimesFlag.style.display='none';	
        TimesFlagNo.style.display='none';	
        Reach.style.display='none';	   	
        ReachType.style.display='none';	 
        Days.style.display='none';
        CInValiDate_title_td.style.display='none';	 
        CInValiDate_common_td.style.display='none';
        fm.all('PayMode').value = '4';    
        //fm.all('PayIntv').value = '12';
        fm.all('PayIntv').value = "";
        Tempfeetitle.style.display='';
        input1.style.display='';
        ZhiweiApptitle.style.display='';
        ZhiweiApp.style.display='';
        ZhiweiInstitle.style.display='';
        ZhiweiIns.style.display='';
        fm.RiskWrapFlag.value="1"
    }
    
    //电销平台显示联系电话
    if ("5" == MissionProp5)
    {
        //appntHomePhoneTitle.style.display = "none";
        //appntHomePhoneInput.style.display = "none";
        appntPhoneTitle.style.display = "";
        appntPhoneInput.style.display = "";
        //appntCompanyPhoneTitle.style.display = "none";
        //appntCompanyPhoneInput.style.display = ""none;
        //insuredHomePhoneTitle.style.display = "none";
        //insuredHomePhoneInput.style.display = "none";
        insuredPhoneTitle.style.display = "";
        insuredPhoneInput.style.display = "";
        //insuredCompanyPhoneTitle.style.display = "none";
        //insuredCompanyPhoneInput.style.display = "none";
        fm.PayIntv.CodeData = "0|^0|趸缴^1|月缴^12|年缴";
    }
    else
    {
        appntHomePhoneTitle.style.display = "";
        appntHomePhoneInput.style.display = "";
        //appntPhoneTitle.style.display = "none";
        //appntPhoneInput.style.display = "none";
        appntCompanyPhoneTitle.style.display = "";
        appntCompanyPhoneInput.style.display = "";
        insuredHomePhoneTitle.style.display = "";
        insuredHomePhoneInput.style.display = "";
        //insuredPhoneTitle.style.display = "none";
        //insuredPhoneInput.style.display = "none";
        insuredCompanyPhoneTitle.style.display = "";
        insuredCompanyPhoneInput.style.display = "";
    }

	if(fm.NeedMulBnf.value == "1")
	{
		MulBnfGrid.style.display="";
		bnf_table3.style.display="none";
	}
	
	if(fm.RiskWrapFlag.value == "1"){
		RiskInfoGridDiv.style.display="none";
		RiskWrapInfoGridDiv.style.display='';
	}else{
		RiskInfoGridDiv.style.display="";
		RiskWrapInfoGridDiv.style.display="none";
	}

	if(MissionProp5=="4"){ 
		
		RiskInfoGridDiv.style.display="none";
	}
    
    if("5" == MissionProp5)
    {
        fm.SaleChnl.value = "08";
    }
    // by gzh 20110304 当保单类型为 6 银行保险时，显示投保人查询功能。
    //if("6" == MissionProp5)
    //{
    //    AppntInfoDiv.style.display = "";
    //}

}
//针对卡单保险期间
function dealCardRisk(){
	if(!isDate(fm.CValiDate.value))
		return;
	var strSql = "select polperiod,polperiodflag from LMCertifyDes where certifycode in (select certifycode from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='"+fm.PrtNo.value+"')";
	var arr = easyExecSql(strSql);
	var tYears = "0";
	var tYearsFlag = "Day";
	if(arr){
		tYears = arr[0][0];
		if(arr[0][0] == "Y"){
			tYearsFlag = "Year";
		}else if(arr[0][0] == "D"){
			tYearsFlag = "Day";
		}else if(arr[0][0] == "M"){
			tYearsFlag = "Month";
		}
	}
	var strSql = "select date('"+fm.CValiDate.value+"') + "+tYears+" "+tYearsFlag+" - 1 day from dual";
	var arr=easyExecSql(strSql);
	
	if(arr){
		fm.CInValiDate.value = arr[0][0];
	}
	
	var strSql = "select integer(to_date('"+fm.CInValiDate.value+"')-to_date('"+fm.CValiDate.value+"'))+1 from dual";	
	var arr=easyExecSql(strSql);
	
	if(arr){
		fm.Days.value = arr[0][0];
	}
}
//针对高原疾病的保险期间
function dealHighDay(){
	var strSql = "select date('"+fm.CValiDate.value+"') + 30 day - 1 day from dual";
	var arr=easyExecSql(strSql);
	
	if(arr){
		fm.CInValiDate.value = arr[0][0];
	}
	
	var strSql = "select integer(to_date('"+fm.CInValiDate.value+"')-to_date('"+fm.CValiDate.value+"'))+1 from dual";	
	var arr=easyExecSql(strSql);
	
	if(arr){
		fm.Days.value = arr[0][0];
	}
}
//处理交通意外预览页面
function showAccident(){
	var tContNo = fm.ContNo.value;
	
	if(tContNo == "" || tContNo == "null" || tContNo == null){
		alert("");
		return;
	}
//alert(fm.MissionProp5.value);
	var urlStr = "./BriefRiskPreviewMain.jsp?ContNo="+fm.ContNo.value+"&ContType="+fm.MissionProp5.value;
	var sFeatures = "status:no;help:0;close:0;dialogWidth:1024px;dialogHeight:768px;resizable=1";
  window.open(urlStr,"window0",sFeatures);
}

/*********************************************************************
 *  根据身份证号取得出生日期和性别
 *  参数  ：  身份证号
 *  返回值：  无
 *********************************************************************
 */

function getAppntBirthdaySexByIDNo(iIdNo) {
	
  if(fm.all('appnt_IDType').value=="0") {  	
    fm.all('appnt_AppntBirthday').value=getBirthdatByIdNo(iIdNo);
    fm.all('appnt_AppntSex').value=getSexByIDNo(iIdNo);
    fm.appnt_SexName.value = getCodeName('sex', fm.appnt_AppntSex.value);
  }
}

function getInsuredBirthdaySexByIDNo(iIdNo) {
	
  if(fm.all('insured_IDType').value=="0") {  	
    fm.all('insured_Birthday').value=getBirthdatByIdNo(iIdNo);
    fm.all('insured_Sex').value=getSexByIDNo(iIdNo);
    fm.insured_SexName.value = getCodeName('sex', fm.all('insured_Sex').value);
  }
}

function getCodeName(cCodeType, cCode)
{
	var sql = "select CodeName from LDCode where CodeType = '" +  cCodeType + "' and Code = '" + cCode + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
  	return rs[0][0];
 	}
 	return null;
}

function SignCont(){
	var MissionID = fm.MissionID.value;
	var SubMissionID = fm.SubMissionID.value;
	if(MissionID == null || MissionID == "null" || MissionID == ""){
		alert("没有查询到MissionID");
		return;
	}
	if(SubMissionID == null || SubMissionID == "null" || SubMissionID == ""){
		alert("没有查询到SubMissionID");
		return;
	}
}

function signPol()
{
	var tSql = "select contno from lccont where prtno = '"+fm.PrtNo.value+"'";
	var arr=easyExecSql(tSql);
		 
	if(arr)
	{		 
			fm.ContNo.value = arr[0][0];
	}
		
	if(fm.ContNo.value == null || fm.ContNo.value == "")
	{
		 alert("请检查该是否保存过该合同!")
		 return ;
	} 	
	else
	{		
		var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.action="./BriefSingleContSignSave.jsp";
		fm.submit(); //提交
	}
}
//查询显示旅行目的
function queryImpart()
{
	var strSql = "select ImpartParamModle from lccustomerimpart where ContNo='"+fm.ContNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		fm.IntentionName.value = arr[0][0];
		fm.Intention.value = arr[0][1];
	}
}
function CheckBnfDate(){
	var passVerify = true;
	if(fm.bnf_Name.value!=""){
		//alert(fm.bnf_Name.value);
			if(fm.bnf_Sex.value==""){
			 alert("受益人性别为空,请录入受益人性别!");
			 return false;
			}
			if(fm.bnf_IDType.value=="0"){
				if(fm.bnf_IDNo.value==""){
			 	alert("证件类型为身份证,请录入证件号码!");
			 	return false;
			}
		}
	}
	if(fm.NeedMulBnf.value == "1"){
		BnfGrid.delBlankLine();
		if(BnfGrid.mulLineCount>0){
			var i;
	    var sumLiveBnf = new Array();
	    var sumDeadBnf = new Array();
	    for (i=0; i<BnfGrid.mulLineCount; i++) {
	      if (BnfGrid.getRowColData(i, 8)==null||BnfGrid.getRowColData(i, 8)=='') {
	        BnfGrid.setRowColData(i, 8,"1");
	      }
	      if (BnfGrid.getRowColData(i, 7)==null||BnfGrid.getRowColData(i, 7)=='') {
	        BnfGrid.setRowColData(i, 7,"1");
	      }
	      if (BnfGrid.getRowColData(i, 1) == "0") {
		        if (typeof(sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 8))]) == "undefined")
		          sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 8))] = 0;
	        	sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 8))] = sumLiveBnf[parseInt(BnfGrid.getRowColData(i, 8))] + parseFloat(BnfGrid.getRowColData(i, 7));
	        } else if (BnfGrid.getRowColData(i, 1) == "1") {
		        if (typeof(sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 8))]) == "undefined")
		          sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 8))] = 0;
	        	sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 8))] = sumDeadBnf[parseInt(BnfGrid.getRowColData(i, 8))] + parseFloat(BnfGrid.getRowColData(i, 7));
	        }
	      }
	
	    for (i=0; i<sumLiveBnf.length; i++) {
		    if (typeof(sumLiveBnf[i])!="undefined" && sumLiveBnf[i]>1) {
			    alert("生存受益人受益顺序 " + i + " 的受益比例和为：" + sumLiveBnf[i] + " 。大于100%，不能提交！");
			    passVerify = false;
		    } else if (typeof(sumLiveBnf[i])!="undefined" && sumLiveBnf[i]<1) {
			    alert("注意：生存受益人受益顺序 " + i + " 的受益比例和为：" + sumLiveBnf[i] + " 。小于100%");
			    passVerify = false;
		    }
	    }
	
	    for (i=0; i<sumDeadBnf.length; i++) {
			  if (typeof(sumDeadBnf[i])!="undefined" && sumDeadBnf[i]>1) {
			    alert("死亡受益人受益顺序 " + i + " 的受益比例和为：" + sumDeadBnf[i] + " 。大于100%，不能提交！");
			    passVerify = false;
			  } else if (typeof(sumDeadBnf[i])!="undefined" && sumDeadBnf[i]<1) {
			    alert("注意：死亡受益人受益顺序 " + i + " 的受益比例和为：" + sumDeadBnf[i] + " 。小于100%");
			    passVerify = false;
			  }
	    }
	    for (var i=0; i<BnfGrid.mulLineCount; i++){
		    if(BnfGrid.getRowColData(i,1)=="1" && BnfGrid.getRowColData(i,6)=="00"){
		    	alert("身故受益人不能选择被保险人本人,请检查!");
		      passVerify = false;
		    }
		  }
		  
		  if(BnfGrid.checkValue2(BnfGrid.name,BnfGrid)== false)
		  	passVerify = false;
		}
	}
	return passVerify;
}
function getBnfBirthdaySexByIDNo(iIdNo)
{
	if(fm.all('bnf_IDType').value=="0")
	{
		//fm.all('AppntBirthday').value=getBirthdatByIdNo(iIdNo);
		fm.all('bnf_Sex').value=getSexByIDNo(iIdNo);
	}	
}
//从卡单 读出险种信息
function showCardInfo(){
	var tCardNo = fm.PrtNo.value;
	var strSql = "select * from lzcard a,lzcardnumber b where a.subcode=b.cardtype and a.startno=b.cardserno and cardno='"+fm.PrtNo.value+"'";
	var arr = easyExecSql(strSql);
	if(arr){
		var SubCode = arr[0][1];
		var CertifyCode = arr[0][0];
		fm.CardType.value = easyExecSql("select CertifyName from LMCertifyDes where  CertifyCode='"+CertifyCode+"'");
		fm.ReleaseDate.value = arr[0][12];
		var cardRiskInfo = easyExecSql("select a.riskcode,a.prem,a.PremLot,a.Mult,b.riskname from lmcardrisk a,lmriskapp b where a.riskcode=b.riskcode and CertifyCode='"+CertifyCode+"'");
		if(cardRiskInfo){
			fm.CardRiskCode.value = cardRiskInfo[0][0]+"-"+cardRiskInfo[0][4];
			if(cardRiskInfo[0][3] > 0 ){
				fm.CardAmnt.value = cardRiskInfo[0][3] +"档";
			}else{
				fm.CardAmnt.value = cardRiskInfo[0][2] +"元";
			}
			fm.CardPrem.value = cardRiskInfo[0][1];
		}
	}
	var strSql = "select ActiveDate from LZCardNumber a,LZCardPrint b where a.CardType=b.SubCode "
						+ " and a.CardSerNo>=b.StartNo and a.CardSerNo<=EndNo and a.CardNo='"+tCardNo+"'" ;
	var arr = easyExecSql(strSql);
	if(arr){
		fm.CardValiDate.value = arr;
	}
}

function addInit(){
	var index = BnfGrid.mulLineCount;
	BnfGrid.setRowColData(index-1, 1, "1");
}

function getaccname(){
	//if(MissionProp5=="6")
	fm.all('AccName').value=fm.all('appnt_AppntName').value;
	}
	


function initRiskWrapGrid2()
{
try{
	//and a.RiskCode='000000' and a.DutyCode='000000'
  var strsql = "select distinct 'Factor',ChooseFlag,CalFactor,CalFactorName,Function "
              +"from LDRiskDutyWrap a,LDWrap b "
              +"where 1=1 and a.RiskWrapCode=b.RiskWrapCode and b.WrapType='"
              +MissionProp5
              +"' and b.WrapProp='Y'"

	var arr = easyExecSql(strsql);	

	if(!arr){
		arr = new Array();
		arr[0] = new Array();
	}
	
	var iArray = new Array();
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=10;
	iArray[0][3]=0;
	
	iArray[1]=new Array();
	iArray[1][0]="套餐编码";
	iArray[1][1]="80px";
	iArray[1][2]=10;
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="套餐名称";
	iArray[2][1]="100px";
	iArray[2][2]=10;
	iArray[2][3]=0;
	
	 strSqlMain = "select a.RiskWrapCode,a.WrapName"
	var i = 2;
	for(var m=0 ; m<arr.length ; m++){
	//	alert(arr[m]);
		for(var n=0 ; n<arr[m].length ; n++){
			if(arr[m][n]=="Factor"){
				var display = 0;
				if(arr[m][n+1] == "6"){
					display = 1;
				}
				if(arr[m][n+1] == "7"){
					display = 3;
				}
			}else{
				break
			}
			i++;
			iArray[i]=new Array();
			iArray[i][0]="标志位";
			iArray[i][1]="30px";
			iArray[i][2]=10;
			iArray[i][3]=3;
			
			i++;
			iArray[i]=new Array();
			iArray[i][0]="要素";
			iArray[i][1]="30px";
			iArray[i][2]=10;
			iArray[i][3]=3;
			
			i++;
			iArray[i]=new Array();
			iArray[i][0]=arr[m][n+3];
			iArray[i][1]="150px";
			iArray[i][2]=10;
			iArray[i][3]=display;//控制是否显示此要素
			
		//	alert(arr[m][n+4]);
		  strSqlMain += ",'Factor','"+arr[m][n+2]+"',(select ";
			strSqlMain += arr[m][n+4];
			strSqlMain += "("+arr[m][n+2]+")";
	    strSqlMain +=  " from LCPol where  prtno='"+"' and Riskcode in (select Riskcode from LDRiskWrap where RiskWrapCode=a.RiskWrapCode))";
		}
	}
		strSqlMain += " from LDWrap a where a.WrapType='"+MissionProp5+"' and a.WrapProp='Y' ";
        
        // 增加对套餐机构的控制。
        strSqlMain += " and (a.WrapManageCom = '" + ManageCom + "' or a.WrapManageCom = substr('" + ManageCom + "', 1, 2) or a.WrapManageCom = substr('" + ManageCom + "', 1, 4)) ";
        strSqlMain += " order by a.RiskWrapCode ";
        // --------------------
        
		fm.RiskWrapCol.value=i;		  
		RiskWrapGrid = new MulLineEnter( "fm" , "RiskWrapGrid" );
		//这些属性必须在loadMulLine前
		RiskWrapGrid.mulLineCount = 0;
		RiskWrapGrid.displayTitle = 1;
		RiskWrapGrid.locked = 0;
		RiskWrapGrid.canSel = 0;
		RiskWrapGrid.canChk = 1;
		RiskWrapGrid.hiddenPlus = 1;
		RiskWrapGrid.hiddenSubtraction = 1;
		RiskWrapGrid.loadMulLine(iArray);
	}catch (ex){
		alert(ex.message);
	}
}	
	
function chosebox()
{
	if(MissionProp5 == "6")
	{
	  	/*initRiskWrapGrid2();
	  	turnPage.queryModal(strSqlMain,RiskWrapGrid);
	  	RiskWrapGrid.setRowColData(0,5,"20000");
	  	RiskWrapGrid.setRowColData(0,8,"一年");
	  	RiskWrapGrid.setRowColData(0,11,"");
	  	RiskWrapGrid.setRowColData(0,14,"220000");
	  	//RiskWrapInfoGridDiv.style.display='none';
	  	RiskWrapGrid.checkBoxSel(1);*/
        for(var i = 0 ; i < RiskWrapGrid.mulLineCount; i++)
        {
            if(RiskWrapGrid.getRowColData(i,1) == "WR0004")
            {
                //RiskWrapGrid.setRowColData(i,m,"");
                var tmpArr = RiskWrapGrid.getRowData(i);
                for(var m = 0 ; m < tmpArr.length; m++)
                {
                    if(tmpArr[m] == "InsuYear")
                	{
                	    m += 1;
                	    RiskWrapGrid.setRowColData(i, m + 1, "一年");
                	    continue;
                	}
                	if(tmpArr[m] == "Amnt")
                	{
                	    m += 1;
                	    RiskWrapGrid.setRowColData(i, m + 1, "220000");
                	    continue;
                	}
                	if(tmpArr[m] == "Prem")
                	{
                	    m += 1;
                	    RiskWrapGrid.setRowColData(i, m + 1, "20000");
                	    continue;
                	}
		        }
            }
        }
	}
}

/**
 * 计算被保人年龄
 * 2007-04-28 update LY
 */
function getAge()
{
    var sxDate = fm.all("CValiDate") == null ? "" : fm.all("CValiDate").value;
    var insured_RelationToAppnt = fm.all("insured_RelationToAppnt") == null ? "" : fm.all("insured_RelationToAppnt").value;
    var birthday;

    if(insured_RelationToAppnt == "")
    {
        document.getElementById("age").value = age;
        return ;
    }

    if(insured_RelationToAppnt == "00")
    {
        birthday = fm.all("appnt_AppntBirthday") == null ? "" : fm.all("appnt_AppntBirthday").value;
    }
    else
    {
        birthday = fm.all("insured_Birthday") == null ? "" : fm.all("insured_Birthday").value;
    }

    var age = "";
    birthday = toDate(birthday);
    sxDate = toDate(sxDate);
    if(birthday != null && sxDate != null)
    {
        var year = birthday.getFullYear();
        var month = birthday.getMonth();
        var day = birthday.getDate();
        var eYear = sxDate.getFullYear();
        var eMonth = sxDate.getMonth();
        var eDay = sxDate.getDate();
        age = eYear - year;
        if(month > eMonth)
        {
            age -= 1;
        }
        else if(month == eMonth)
        {
            if(day > eDay)
            {
                age -= 1;
            }
        }
    }

    document.getElementById("age").value = age;
}

function toDate(str)
{
    var pattern = /^(\d{4})(-)(0?[1-9]|1[0-2])(-)(0?[1-9]|[12][0-9]|3[01])$/g; 
    var arr = pattern.exec(str);
    if (arr == null)
        return null;
    var date = new Date(arr[1], arr[3]-1, arr[5]);
    return date;
}

function afterDateSelect()
{
    getAge();
}
// end update

function getComcodeList()
{
    var strSql = "select ComCode, Name, ShortName, Address from ldcom where 1 = 1 and Sign='1' and comcode like '86%' and length(trim(comcode)) = 8 order by comcode with ur";
    var arr = easyExecSql(strSql);
    var strResult = "";
    if(arr != null)
    {
        strResult += "0|";
        for(index in arr)
        {
            if(arr[index] != "")
            {
                strResult += "^" + arr[index][0];
                strResult += "|" + arr[index][1];
            }
        }
    }
    //alert(strResult);
    fm.ManageCom.CodeData = strResult;
}

/**
 * 校验缴费频次与险种缴费期间对应关系。
 * 根据险种进行判断。（目前没有相关描述表，暂时写死。）
 */
function checkPayEndYearWithPayIntv()
{
    var tWrapCode = null;
    for(var i = 0, n = RiskWrapGrid.mulLineCount; i < n; i++)
    {
        if(RiskWrapGrid.getChkNo(i))
        {
            tWrapCode = RiskWrapGrid.getRowColData(i,1);
            var tValues = RiskWrapGrid.getRowData(i)
            if(false == ruleOfPayEndYearWithPayIntv(tWrapCode, tValues))
            {
                return false;
            }
        }
    }
    return true;
}

/**
 * 缴费频次与险种缴费期间对应规则。
 * @param cWrapCode 套餐代码
 */
function ruleOfPayEndYearWithPayIntv(cWrapCode, cValues)
{
    var tResult = false;
    switch(cWrapCode)
    {
        case "WR0010":
            if(cValues)
            {
                var tPayIntv = fm.PayIntv.value;
                var tPayEndYear = "";
                var tPayEndYearFlag = "";
                
                for(var i = 0, n = cValues.length; i < n; i++)
                {
                    if("Factor" == cValues[i])
                    {
                        if("PayEndYear" == cValues[i+1])
                        {
                            tPayEndYear = cValues[i+2];
                        }
                        if("PayEndYearFlag" == cValues[i+1])
                        {
                            tPayEndYearFlag = cValues[i+2];
                        }
                    }
                }
                tResult = checkWR0010PayEndYearWithPayIntv(tPayIntv, tPayEndYear, tPayEndYearFlag);
            }
            else
            {
                tResult = false;
                alert("获取数据失败。");
            }
            break;
        case "WR0013":
            if(cValues)
            {
                var tPayIntv = fm.PayIntv.value;
                var tPayEndYear = "";
                var tPayEndYearFlag = "";
                var tInsuYear = "";
                var tInsuYearFlag = "";
                
                for(var i = 0, n = cValues.length; i < n; i++)
                {
                    if("Factor" == cValues[i])
                    {
                        if("PayEndYear" == cValues[i+1])
                        {
                            tPayEndYear = cValues[i+2];
                        }
                        if("PayEndYearFlag" == cValues[i+1])
                        {
                            tPayEndYearFlag = cValues[i+2];
                        }
                        if("InsuYear" == cValues[i+1])
                        {
                            tInsuYear = cValues[i+2];
                        }
                        if("InsuYearFlag" == cValues[i+1])
                        {
                            tInsuYearFlag = cValues[i+2];
                        }
                    }
                }
                tResult = checkWR0013PayEndYearWithPayIntv(tPayIntv, tPayEndYear, tPayEndYearFlag, tInsuYear, tInsuYearFlag);
            }
            else
            {
                tResult = false;
                alert("获取数据失败。");
            }
            break;
        default :
            tResult = true;
    }
    return tResult;
}

/**
 * 校验套餐WR0010的缴费年期与缴费频次。
 * 没问题返回true，有错误返回false
 */
function checkWR0010PayEndYearWithPayIntv(cPayIntv, cPayEndYear, cPayEndYearFlag)
{
    if("0" == cPayIntv)
    {
        if("100" != cPayEndYear
            || "A" != cPayEndYearFlag)
        {
            alert("选择趸缴方式缴费时，WR0010缴费期间必须填写为：100，缴费期间单位必须填写为：A");
            return false;
        }
    }
    else if("12" == cPayIntv)
    {
        if("Y" != cPayEndYearFlag
            || ("5" != cPayEndYear
            && "10" != cPayEndYear
            && "20" != cPayEndYear))
        {
            alert("选择年缴方式缴费时，WR0010缴费期间必须填写为：5/10/20，缴费期间单位必须填写为：Y");
            return false;
        }
    }
    return true;
}

/**
 * 校验套餐WR0013的缴费年期、保险期间与缴费频次。
 * 没问题返回true，有错误返回false
 */
function checkWR0013PayEndYearWithPayIntv(cPayIntv, cPayEndYear, cPayEndYearFlag, cInsuYear, cInsuYearFlag)
{
    if("12" != cPayIntv && "1" != cPayIntv)
    {
        alert("缴费频次只能为月缴和年缴！");
        return false;
    }
    if("Y" != cPayEndYearFlag || ("10" != cPayEndYear && "15" != cPayEndYear && "20" != cPayEndYear))
    {
        alert("WR0013缴费期间必须填写为：10/15/20，缴费期间单位必须填写为：Y");
        return false;
    }
    if(cPayEndYear != cInsuYear || cPayEndYearFlag != cInsuYearFlag)
    {
        alert("保险期间和缴费期间不一致！");
        return false;
    }
    return true;
}

//查询代理机构后执行
function afterAgentComQuery(arrResult)
{
    if(arrResult != null)
    {
        fm.AgentCom.value = arrResult[0][0];
        fm.AgentComName.value = arrResult[0][1];
    }
}

/**
 * 集团交叉要素校验
 */
/**
 * 集团交叉要素校验
 */
function checkCrsBussParams()
{
    var tCrs_SaleChnl = trim(fm.Crs_SaleChnl.value);
    var tCrs_BussType = trim(fm.Crs_BussType.value);
    
    var tGrpAgentCom = trim(fm.GrpAgentCom.value);
    var tGrpAgentCode = trim(fm.GrpAgentCode.value);
    var tGrpAgentName = trim(fm.GrpAgentName.value);
    var tGrpAgentIDNo = trim(fm.GrpAgentIDNo.value);
    
    if(tCrs_SaleChnl != null && tCrs_SaleChnl != "")
    {
        if(tCrs_BussType == null || tCrs_BussType == "")
        {
            alert("选择集团交叉渠道时，集团交叉业务类型不能为空。");
            return false;
        }
        if(tGrpAgentCom == null || tGrpAgentCom == "")
        {
            alert("选择集团交叉渠道时，对方业务员机构不能为空。");
            return false;
        }
        if(tGrpAgentCode == null || tGrpAgentCode == "")
        {
            alert("选择集团交叉渠道时，对方业务员代码不能为空。");
            return false;
        }
        if(tGrpAgentName == null || tGrpAgentName == "")
        {
            alert("选择集团交叉渠道时，对方业务员姓名不能为空。");
            return false;
        }
        if(tGrpAgentIDNo == null || tGrpAgentIDNo == "")
        {
            alert("选择集团交叉渠道时，对方业务员身份证不能为空。");
            return false;
        }
    }
    else
    {
        if(tCrs_BussType != null && tCrs_BussType != "")
        {
            alert("未选择集团交叉渠道时，集团交叉业务类型不能填写。");
            return false;
        }
        if(tGrpAgentCom != null && tGrpAgentCom != "")
        {
            alert("未选择集团交叉渠道时，对方业务员机构不能为填写。");
            return false;
        }
        if(tGrpAgentCode != null && tGrpAgentCode != "")
        {
            alert("未选择集团交叉渠道时，对方业务员代码不能为填写。");
            return false;
        }
        if(tGrpAgentName != null && tGrpAgentName != "")
        {
            alert("未选择集团交叉渠道时，对方业务员姓名不能为填写。");
            return false;
        }
        if(tGrpAgentIDNo != null && tGrpAgentIDNo != "")
        {
            alert("未选择集团交叉渠道时，对方业务员身份证不能为填写。");
            return false;
        }
    }
    
    return true;
}

//显示或者隐藏交叉销售
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('GrpAgentComID').style.display = "";
        fm.all('GrpAgentTitleID').style.display = "";
        fm.all('GrpAgentTitleIDNo').style.display = "";
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('Crs_SaleChnl').value ="";
        fm.all('Crs_SaleChnlName').value ="";
        fm.all('Crs_BussType').value="";
        fm.all('Crs_BussTypeName').value="";
        fm.all('GrpAgentCom').value="";
        fm.all('GrpAgentComName').value="";
        fm.all('GrpAgentCode').value="";
        fm.all('GrpAgentName').value="";
        fm.all('GrpAgentIDNo').value="";
        fm.all('GrpAgentComID').style.display = "none";
        fm.all('GrpAgentTitleID').style.display = "none";
        fm.all('GrpAgentTitleIDNo').style.display = "none";
    }
}

//根据对方机构代码带出对方机构名称
function GetGrpAgentName()
{
	var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     fm.GrpAgentComName.value = "";
	}
}  
//根据机构代码显示机构名称
function getAgentName()
{
    var strSql = "select Under_Orgname from lomixcom where GrpAgentCom='" + fm.GrpAgentCom.value+"'";
    var arrResult1 = easyExecSql(strSql);
	if (arrResult1 != null) {
	     fm.GrpAgentComName.value = arrResult1[0][0];
	}
	else{  
	     alert("对方机构代码有错误,请修改");
	     fm.GrpAgentCom.value = "";
	     return false;
	}
}


/**
 * 校验生效日期填写规范。
 * 规则：生效日期 在 录入日期（判断录入当天） 之前时，返回false；否则，返回true；
 */
function chkValidate()
{
    var tValidate = fm.CValiDate.value;
    var tCurDate = getCurrentDate();
    
    if(dateDiff(tCurDate, tValidate, "D") < 0)
    {
        return false;
    }
    return true;
}

/*********************************************************************
 * 校验电话的填写规范。
 * 规则：电话号码必须是，返回false；否则，返回true；
 *********************************************************************
 */
 function chkPhoneNumber(num)
 {
 	if(num == "")
 		return true;
 	var pattern = /^([0-9])+(-[0-9]+)*/g;
 	if(pattern.test(num))
 		return true;
 	else
 		return false;
 }
 
 /*********************************************************************
 *  验证身份证号、生日＆性别
 *  参数  ：  
 *  返回值：  无
 *********************************************************************
 */
function BirthdaySexAppntIDNo()
{
	if(fm.all('appnt_IDType').value=="0")
	{
		if(fm.all('appnt_AppntBirthday').value!=getBirthdatByIdNo(fm.all('appnt_IDNo').value) || fm.all('appnt_AppntSex').value!=getSexByIDNo(fm.all('appnt_IDNo').value))
		{
			alert("投保人的生日＆性别与身份证号输入有问题");
			return false;
		}
	}
	if(fm.all('insured_IDType').value=="0")
	{
		if(fm.all('insured_Birthday').value!=getBirthdatByIdNo(fm.all('insured_IDNo').value) || fm.all('insured_Sex').value!=getSexByIDNo(fm.all('insured_IDNo').value))
		{
			alert("被保人的生日＆性别与身份证号输入有问题");
			return false;
		}
	}	
}

//校验职业代码
function CheckOccupationCode(Type,Code)
{
	var strSql = "select 1 from ldoccupation where OccupationCode = '" + Code +"' and OccupationType = '" + Type + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别和职业代码与系统描述不一致，请查看！");
		return false
	} 
	return true;
}

//校验职业类别
function CheckOccupationType(Type)
{
	var strSql = "select 1 from dual where '" + Type + "' in (select distinct occupationtype from ldoccupation)";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
		return false
	} 
	return true;
}

//校验被保人职业类别与职业代码
function checkOccTypeOccCode(OccupationType, OccupationCode)
{
	if(OccupationType != null && OccupationType != "")
    {
    	if(!CheckOccupationType(OccupationType))
		{
			return false;
		}
    }
    	
    if(OccupationCode != null && OccupationCode != "")
    {
    	if(!CheckOccupationCode(OccupationType,OccupationCode))
		{
			return false;
		}
    }
    return true;
}

//校验被保人性别
function CheckSex(sex)
{
	if(sex != "0" && sex != "1" && sex != "2")
	{
		alert("被保人的性别填写有误，请查看。\n性别必须是半角数字的0、1、2 ，规则是0-男，1-女，2-其他");
		return false;
	}
	return true;
}



//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryGrpAgent()
{
    //if(fm.all('GrpAgentCode').value == "" && fm.all('GrpAgentIDNo').value == "" )
    //{  
    //    var tGrpAgentCom = (fm.GrpAgentCom != null && fm.GrpAgentCom != "undefined") ? fm.GrpAgentCom.value : "";
    //    var tGrpAgentName = (fm.GrpAgentName != null && fm.GrpAgentName != "undefined") ? fm.GrpAgentName.value : "";
    //    var tGrpAgentIDNo = (fm.GrpAgentIDNo != null && fm.GrpAgentIDNo != "undefined") ? fm.GrpAgentIDNo.value : "";
    //    var strURL = "../sys/GrpAgentCommonQueryMain.jsp?GrpAgentCom=" + tGrpAgentCom +
    //                 "&GrpAgentName=" + tGrpAgentName + "&GrpAgentIDNo=" + tGrpAgentIDNo;        
    //    //alert(strURL);
    //    var newWindow = window.open(strURL, "GrpAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    //}else if(fm.all('GrpAgentCode').value != "")
    //{	
    //    var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Sales_Cod='" + fm.all('GrpAgentCode').value+"'";
    //    var arrResult = easyExecSql(strGrpSql);
    //    if (arrResult != null)
    //    {
    //        afterQuery3(arrResult);
    //        //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    //    }
    //    else
    //    {            
    //        alert("代码为:[" +  fm.GrpAgentCode.value + "]的业务员不存在，请确认!");
    //        //fm.GrpAgentCode.value="";
    //    }
    //}else if(fm.all('GrpAgentIDNo').value != "")
    //{	
    //    var strGrpSql = "select Sales_Cod,Sales_Nam,Id_No from LOMixSalesman where Id_No='" + fm.all('GrpAgentIDNo').value+"'";
    //    var arrResult = easyExecSql(strGrpSql);
    //    if (arrResult != null)
    //    {
    //        afterQuery3(arrResult);
    //        //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    //    }
    //    else
    //    {            
    //        alert("身份证号码为:[" +  fm.GrpAgentIDNo.value + "]的业务员不存在，请确认!");
    //        //fm.GrpAgentCode.value="";
    //    }
    //}
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery3(arrResult)
{  
  //if(arrResult!=null)
  //  {  	
  //	fm.GrpAgentCode.value = arrResult[0][0];
  //	fm.GrpAgentName.value = arrResult[0][1];
  //  fm.GrpAgentIDNo.value = arrResult[0][2];
  //}
}

//执行查询交叉销售对方机构代码 date 20101117 by gzh
function queryGrpAgentCom()
{
	//if(fm.all('Crs_SaleChnl').value == "" || fm.all('Crs_SaleChnl').value == null)
	//{
	//	alert("请先选择交叉销售渠道！！");
	//	return false;
	//}
    //if(fm.all('GrpAgentCom').value == "")
    //{  
    //    var tCrs_SaleChnl =  fm.Crs_SaleChnl.value;
    //    var tCrs_SaleChnlName = fm.Crs_SaleChnlName.value;
    //    var strURL = "../sys/GrpAgentComQueryMain.jsp?Crs_SaleChnl="+tCrs_SaleChnl+"&Crs_SaleChnlName="+tCrs_SaleChnlName;        
    //    //alert(strURL);
    //    var newWindow = window.open(strURL, "GrpAgentComQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    //}else if(fm.all('GrpAgentCom').value != "")
    //{	
    //    var strGrpSql = "select grpagentcom,Under_Orgname  from lomixcom  where  grpagentcom ='"+fm.GrpAgentCom.value+"'";
    //    var arrResult = easyExecSql(strGrpSql);
    //    if (arrResult != null)
    //    {
    //        afterQuery4(arrResult);
    //        //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    //    }
    //    else
    //    {            
    //        alert("机构代码为:[" +  fm.GrpAgentCom.value + "]的机构不存在，请确认!");
    //        //fm.GrpAgentCode.value="";
    //    }
    //}
}


//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始 date 20101029 by gzh
function afterQuery4(arrResult)
{  
  //if(arrResult!=null)
  //  { 	
  //	fm.GrpAgentCom.value = arrResult[0][0];
  //	fm.GrpAgentComName.value = arrResult[0][1];
  //}
}


//选择交叉销售后对交叉销售各不为空项的校验
    // by gzh 20101118
function MixComCheck()
{    
    if(fm.MixComFlag.checked == true)
    {
    	if(fm.Crs_SaleChnl.value == "" || fm.Crs_SaleChnl.value == null)
    	{
    		alert("选择交叉销售时，交叉销售渠道不能为空，请核查！");
    		fm.all('Crs_SaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.Crs_BussType.value == "" || fm.Crs_BussType.value == null)
    	{
    		alert("选择交叉销售时，交叉销售业务类型不能为空，请核查！");
    		fm.all('Crs_BussType').focus();
    		return false;
    	}
    	if(fm.GrpAgentCom.value == "" || fm.GrpAgentCom.value == null)
    	{
    		alert("选择交叉销售时，对方机构代码不能为空，请核查！");
    		fm.all('GrpAgentCom').focus();
    		return false;
    	}
    	/**
    	if(fm.GrpAgentComName.value == "" || fm.GrpAgentComName.value == null)
    	{
    		alert("选择交叉销售时，对方机构名称不能为空，请核查！");
    		fm.all('GrpAgentComName').focus();
    		return false;
    	}
    	*/
    	if(fm.GrpAgentCode.value == "" || fm.GrpAgentCode.value == null)
    	{
    		alert("选择交叉销售时，对方业务员代码不能为空，请核查！");
    		fm.all('GrpAgentCode').focus();
    		return false;
    	}
    	if(fm.GrpAgentName.value == "" || fm.GrpAgentName.value == null)
    	{
    		alert("选择交叉销售时，对方业务员姓名不能为空，请核查！");
    		fm.all('GrpAgentName').focus();
    		return false;
    	}
    	if(fm.GrpAgentIDNo.value == "" || fm.GrpAgentIDNo.value == null)
    	{
    		alert("选择交叉销售时，对方业务员身份证号码不能为空，请核查！");
    		fm.all('GrpAgentIDNo').focus();
    		return false;
    	}
    	//zxs 20190815 #4502
    	if(fm.GrpAgentCode.value.length!=10){
    		alert("对方业务员代码字段必须为10位，请核实！");
    		return false;
    	}
    	if(fm.Crs_SaleChnl.value=='02'&&fm.GrpAgentCode.value.indexOf("3")!=0){
    		alert("交叉销售渠道是寿代健，对方业务员代码需以数字3开头！");
    		return false;
    	}
    	if(fm.Crs_SaleChnl.value=='01'&&fm.GrpAgentCode.value.indexOf("1")!=0){
    		alert("交叉销售渠道是财代健，对方业务员代码需以数字1开头！");
    		return false;
    	}
      	return true;
    }else{
    	return true;
    }
}    
    // --------------------
    
//校验证件类别    by   zhangyang   20101222    
function checkIDType(IDType)
{
	if(IDType != "0" && IDType != "1" && IDType != "2" && IDType != "3" && IDType != "4")
	{
		return false;
	}
	return true;
}    

function qryAppAccInfo()
{
    var tStrSql = ""
        + " select "
        + " distinct lcaa.CustomerNo, lcc.AppntName, lcaa.AccGetMoney "
        + " from LCCont lcc "
        + " inner join LCAppAcc lcaa on lcaa.CustomerNo = lcc.AppntNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.PrtNo = '" + prtNo + "' "
        + " and lcaa.State = '1' "
        ;
      
    var arrResult = easyExecSql(tStrSql);
    if(arrResult != null)
    {
        var tAppAccInfo = arrResult[0];
      
        var tAppAccNo = tAppAccInfo[0];
        fm.CustNoAppAcc.value = tAppAccNo;
      
        var tAppAccName = tAppAccInfo[1];
        fm.CustNameAppAcc.value = tAppAccName;
      
        var tAppAccBalance = tAppAccInfo[2];
        fm.BalanceAppAcc.value = tAppAccBalance;
    }
}


function getImpartAll()
{
    getImpartInfo();
    getImpartDetailInfo();
}


/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartInfo()
{
    initImpartGrid();

    clearImpart();    //清空
    getImpartbox();   //保障状况告知※健康告知
}


/*********************************************************************
 *  查询告知信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getImpartDetailInfo()
{
    initImpartDetailGrid();
    
    var InsuredNo=fm.all("InsuredNo").value;
    var ContNo=fm.all("ContNo").value;

    var sql = "select ImpartVer,ImpartCode,ImpartDetailContent,DiseaseContent,StartDate,EndDate,Prover,CurrCondition,IsProved from LCCustomerImpartDetail where CustomerNo='"+InsuredNo+"' and ContNo='"+ContNo+"' and CustomerNoType='I'";
    
    turnPage6.pageLineNum = 50;
    turnPage6.pageDivName = "divPage6";
    turnPage6.queryModal(sql, ImpartDetailGrid);
}

//校验黑名单
function checkBlackName(){
	
	var tAppntName = fm.appnt_AppntName.value;
	var strSQL1 = "select 1 from LCBlackList where type = '0' and name = '"+tAppntName+"'";
	var arrResult1 = easyExecSql(strSQL1);
	if(arrResult1 != null)
	{
		if (!confirm("该保单投保人姓名："+tAppntName+",存在于黑名单中，确认要保存吗？"))
		{
			return false;
		}
	}
	var tName = "";
	if(fm.insured_RelationToAppnt.value == '00'){
		tName = fm.appnt_AppntName.value;
	}else{
		tName = fm.insured_Name.value;
	}
	var strSQL3 = "select 1 from LCBlackList where type = '0' and name = '"+tName+"'";
	var arrResult3 = easyExecSql(strSQL3);
	
	if(arrResult3 != null)
	{
		if (!confirm("该保单被保人姓名："+tName+",存在于黑名单中，确认要保存吗？"))
		{
			return false;
		}
	}
	return true;
}
//by zcx 20120524 增加对代理销售业务员的查询
function queryAgentSaleCode()
{
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        fm.all('ManageCom').focus();
        return ;
    }
    
    if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom').focus();
        return ;
    }
    var tAgentCom = fm.all('AgentCom').value;
    if(fm.all('AgentSaleCode').value == "")
    {
        var strURL = "../sys/AgentSaleCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&AgentCom=" + tAgentCom;
        var newWindow = window.open(strURL, "AgentSaleCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentSaleCode').value != "")
    {
        var cAgentCode = fm.AgentSaleCode.value;  //保单号码
        var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom1').value + "'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentSaleName.value = arrResult[0][1];
            //alert("查询结果:  代理销售业务员代码:["+arrResult[0][0]+"] ，代理销售业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value = "";
            alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
        }
    }
}

function queryAgentSaleCode2() {

  if(fm.all('AgentSaleCode').value != ""){
  	if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom1').focus();
        return ;
    }
    var cAgentCode = fm.AgentSaleCode.value;  //保单号码
    var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom1').value + "'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentSaleName.value = arrResult[0][1];
      //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
    }
  }
}

function afterQuery5(arrResult){
  if(arrResult!=null) {
    fm.AgentSaleCode.value = arrResult[0][0];
    fm.AgentSaleName.value = arrResult[0][1];
  }
}

//北分中介时，中介机构及代理销售业务员必须填写
function checkAgentComAndSaleCdoe(){
	var strSql = "select AgentCom,AgentSaleCode from lccont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%' and salechnl = '10'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCom = arrResult[0][0];
	    var tAgentSaleCode = arrResult[0][1];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCom == null || tAgentCom ==''){
	   		alert("中介公司代码必须录入，请核查！");
	   		return false;    	
	   	}
	   	if(tAgentSaleCode == null || tAgentSaleCode ==''){
	   		alert("代理销售业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	
	   	var tSqlCode = "select ValidStart,ValidEnd from LAQualification where agentcode = '"+tAgentSaleCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tValidStart = arrCodeResult[0][0];
	   		var tValidEnd = arrCodeResult[0][1];
	   		if(tValidStart == null || tValidStart == ''){
	   			alert("代理销售业务员资格证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tValidEnd == null || tValidEnd == ''){
	   			alert("代理销售业务员资格证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
	   			alert("代理销售业务员资格证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("代理销售业务员资格证信息不完整，请核查！");
	   		return false;
	   	}
	   	
	   	var tSqlCom = "select LicenseStartDate,LicenseEndDate,EndFlag from lacom where agentcom = '"+tAgentCom+"'";
	   	var arrComResult = easyExecSql(tSqlCom);
	   	if(arrComResult){
	   		var tLicenseStartDate = arrComResult[0][0];
	   		var tLicenseEndDate = arrComResult[0][1];
	   		var tEndFlag = arrComResult[0][2];
	   		if(tLicenseStartDate == null || tLicenseStartDate == ''){
	   			alert("中介公司许可证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tLicenseEndDate == null || tLicenseEndDate == ''){
	   			alert("中介公司许可证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == null || tEndFlag == ''){
	   			alert("中介机构合作终止状态为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == 'Y'){
	   			alert("中介机构合作终止状态为失效，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tLicenseEndDate)=='1'){
	   			alert("中介公司许可证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("中介公司许可证信息不完整，请核查！");
	   		return false;
	   	}
    }
    return true;
}
function initAgentSaleCode(){
	var tSqlCode = "select AgentSaleCode from LCCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(arrCodeResult){
		if(arrCodeResult[0][0] != null && arrCodeResult[0][0] != ""){
			var tSQL = "select agentcode,name from laagenttemp where agentcode = '"+arrCodeResult[0][0]+"'";
			var arrResult = easyExecSql(tSQL);
			if(arrResult){
				fm.AgentSaleCode.value = arrResult[0][0];
    			fm.AgentSaleName.value = arrResult[0][1];
			}
		}
	}
}
//北分业务员必须填写
function checkAgentCode(){
	var strSql = "select AgentCode from lccont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCode = arrResult[0][0];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCode == null || tAgentCode ==''){
	   		alert("业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	var tSqlCode = "select branchtype,branchtype2 from laagent where agentcode = '"+tAgentCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tSqlValid = "";
	   		if(arrCodeResult[0][0] == "1" || arrCodeResult[0][0] == "2" || arrCodeResult[0][0] == "4" || arrCodeResult[0][0] == "5" || arrCodeResult[0][0] == "7"){
	   			tSqlValid = "select ValidStart,ValidEnd from LAQUALIFICATION where agentcode = '"+tAgentCode+"' and state='0'";
	   		} else if (arrCodeResult[0][0] == "3"){
	   			tSqlValid = "select Quafstartdate,QuafEndDate from laagent where agentcode = '"+tAgentCode+"'";
	   		} else {
	   			alert("业务员资销售渠道异常，请核查！");
	   			return false;
	   		}
	   		var arrValid = easyExecSql(tSqlValid);
	   		if(arrValid){
		   		var tValidStart = arrValid[0][0];
		   		var tValidEnd = arrValid[0][1];
		   		if(tValidStart == null || tValidStart == ''){
		   			alert("业务员资格证有效起期为空，请核查！");
		   			return false;
		   		}
		   		if(tValidEnd == null || tValidEnd == ''){
		   			alert("业务员资格证有效止期为空，请核查！");
		   			return false;
		   		}
		   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
		   			alert("业务员资格证已失效，请核查！");
		   			return false;
		   		}
		   	}else{
		   		alert("业务员不存在有效资格证信息，请核查！");
		   		return false;
		   	}
	   	}else{
	   		alert("业务员不存在，请核查！");
	   		return false;
	   	}
    }
    return true;
}

function checkAppnt(){
	var tSql = "select cc.prtno,cad.Customerno,cc.Managecom,cad.Mobile,cad.Phone,cad.homephone,cad.companyphone,ca.idno,ca.idtype,ca.appntbirthday,ca.appntsex from lccont cc inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno " +
				"inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno where cc.prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		// 印刷号
		var prtno = appntResult[0][0];
		// 投保人客户号
		var customerno = appntResult[0][1];
		// 管理机构
		var managecom = appntResult[0][2];
		// 移动电话
		var mobile = appntResult[0][3];
		// 联系电话
		var phone = appntResult[0][4];
		// 家庭电话
		var homephone = appntResult[0][5];
		// 单位电话
		var companyphone = appntResult[0][6];
		// 证件号
		var id = appntResult[0][7];
		// 证件类型
		var idtype = appntResult[0][8];
		// 出生日期
		var birthday = appntResult[0][9];
		// 性别
		var sex = appntResult[0][10];
		
		
		// 共有校验
		if(!isNull(mobile)) {
			if(!isInteger(mobile) || mobile.length != 11){
				alert("投保人移动电话需为11位数字，请核查！");
		   		return false;
			}
		}
		
		if(idtype == "0"){
		
			if(birthday != getBirthdatByIdNo(id)){
				alert("投保人身份证号与出生日期不符，请核查！");
				return false;
			}
			
			if(sex != getSexByIDNo(id)){
				alert("投保人身份证号与性别不符，请核查！");
				return false;
			}
		}
		
		// 机构特殊校验  --需要在数据库中配置 codetype = 字段名 + check

		// 暂时统一对投保人进行校验，若需要单独校验，可以调整为按字段进行校验
		// 若配置为空则会对全部机构进行校验
		if(manageCheck(managecom.substring(0, 4),"appnt")){
			
			if(isNull(id)){
				alert("投保人证件号码不能为空，请核查！");
				return false;
			}
			
			if(isNull(mobile) && isNull(companyphone) && isNull(homephone)){
				alert("投保人家庭电话、移动电话、办公电话不能同时为空，请核查！");
		   		return false;
			}
			
			if(!isNull(mobile)){
				var mobilSql = "select count(distinct customerno) from lcaddress where mobile='" + mobile + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno) ";
				var result = easyExecSql(mobilSql);
				if(result){
					var count = result[0][0];
					if(count >= 2){
						alert("该投保人移动电话已在三个以上不同投保人的保单中出现！");
						return false;
					}
				}
			}
		}
	}
	return true;
}

function checkInsured(){
	var tSql = "select name,idno,idtype,birthday,sex from lcinsured where prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		for(var index = 0; index < appntResult.length; index++){
			// 被保人姓名
			var name = appntResult[index][0];
			// 证件号
			var id = appntResult[index][1];
			// 证件类型
			var idtype = appntResult[index][2];
			// 出生日期
			var birthday = appntResult[index][3];
			// 性别
			var sex = appntResult[index][4];
			
			if(idtype == "0"){
		
				if(birthday != getBirthdatByIdNo(id)){
					alert("被保人" + name + "身份证号与出生日期不符，请核查！");
					return false;
				}
			
				if(sex != getSexByIDNo(id)){
					alert("被保人" + name + "身份证号与性别不符，请核查！");
					return false;
				}
			}
		}
	}

	return true;
}

// 判断机构是否需要进行校验
// managecom 机构
// checkflag 校验项
function manageCheck(managecom, checkflag){
	var checkSql = "select (case (select count(1) from ldcode where codetype = '" + checkflag + "check') " +  
			"when 0 then 1 else (select distinct 1 from ldcode where codetype = '" + checkflag + "check' and code = '" + managecom + "') end ) " + 
			"from dual";
	var result = easyExecSql(checkSql);
	if(result){
		if(result[0][0] == "1"){
			// 该机构需要进行校验
			return true;
		}
	}
	// 该机构不需要进行校验
	return false;
}

// 判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}
//校验停售产品
function checkStopWrap(){
	
	var tSaleChnl = fm.SaleChnl.value;
	var tWrapCode = null;
	var tError = "";
	var strSQL = "";
	var arrResult = "";
    for(var i = 0, n = RiskWrapGrid.mulLineCount; i < n; i++)
    {
        if(RiskWrapGrid.getChkNo(i))
        {
            tWrapCode = RiskWrapGrid.getRowColData(i,1);
            strSQL = "select 1 from ldcode1 where codetype = 'stopwrap' and code = '"+tWrapCode+"' and code1 = '"+tSaleChnl+"' ";
            arrResult = easyExecSql(strSQL);
            if(arrResult){
            	tError = tError+"套餐编码为"+tWrapCode+",该套餐已停售，无法保存！\n";
            }
        }
    }
    if(tError != ""){
    	alert(tError);
    	return false;
    }
	return true;
}

// 改为从规则引擎校验，不再在程序里进行校验
function checkWrapInfo (){
	var tSql = "select riskwrapcode from lcriskdutywrap where prtno='" + fm.PrtNo.value + "'";
	var contResult = easyExecSql(tSql);
	
	// 必录项相关校验
	if(contResult && contResult[0][0] == 'WR0271'){
		var insuredno = "";
		tSql = "select distinct payintv,insuredappage,OccupationType,insuredno from lcpol where prtno='" + fm.PrtNo.value + "'";
		contResult = easyExecSql(tSql);
		if(contResult) {
			for(var row = 0; row < contResult.length; row++){
				var payintv = contResult[row][0];
				var insuredappage = contResult[row][1];
				var occupationType = contResult[row][2];
				insuredno = contResult[row][3];
				
				if(payintv != "12"){
					alert("套餐WR0271，缴费方式必须为年缴，请核查！");
					return false;
				}
				if(parseInt(insuredappage) < 18){
					alert("套餐WR0271，被保人年龄不能小于18岁，请核查！");
					return false;
				}
				if(parseInt(insuredappage) > 65){
					alert("套餐WR0271，被保人年龄不能大于65岁，请核查！");
					return false;
				}
				if(occupationType != "1" && occupationType != "2" && occupationType != "3"){
					alert("套餐WR0271，被保人职业类别不能大于3，请核查！");
					return false;
				}
			}
							
			var tSql = "select prtno from lcpol where insuredno='" + insuredno 
				+ "' and prtno<>'" + fm.PrtNo.value 
				+ "' and exists (select 1 from lcriskdutywrap where prtno=lcpol.prtno and riskwrapcode='WR0271')";
			var contResult = easyExecSql(tSql);
				
			if(contResult){
				alert("该被保人已购买过WR0271套餐的保单，印刷号：" + contResult[0][0] + "，无法再次购买，请核查！");
				return false;
			}
		} else {
			alert("未录入险种相关信息，请确认！");
			return false;
		}
	}
	return true;
}


//综合开拓信息显示
function isAssist()
{
    if(fm.ExtendFlag.checked == true)
    {
        fm.all('ExtendID').style.display = "";
    }
    if(fm.ExtendFlag.checked == false)
    {
        fm.all('AssistSalechnlName').value ="";
        fm.all('AssistAgentCode').value ="";
        fm.all('AssistAgentName').value="";
        fm.all('AssistSaleChnl').value="";
        fm.all('ExtendID').style.display = "none";
    }
}

//执行查询交叉销售业务员代码 date 20101029 by gzh
function queryAssistAgent()
{
    if(fm.all('AssistSaleChnl').value == "" || fm.all('AssistSaleChnl').value == null)
    {
    	alert("请先选择协助销售渠道！");
    	return false;
    }
    if(fm.all('AssistAgentCode').value == "" )
    {  
        var tAssistSaleChnl = (fm.AssistSaleChnl != null && fm.AssistSaleChnl != "undefined") ? fm.AssistSaleChnl.value : "";
        var strURL = "../sys/AssistAgentCommonQueryMain.jsp?AssistSaleChnl=" + tAssistSaleChnl;        
        var newWindow = window.open(strURL, "AssistAgentCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }else if(fm.all('AssistAgentCode').value != "")
    {	
        var strGrpSql = "select agentcode,name from LAAgent where agentcode ='" + fm.all('AssistAgentCode').value+"'";
        var arrResult = easyExecSql(strGrpSql);
        if (arrResult != null)
        {
            afterQuery6(arrResult);
        }
        else
        {            
            alert("代码为:[" +  fm.AssistAgentCode.value + "]的业务员不存在，请确认!");
        }
    }
}
function afterQuery6(arrResult){
	if(arrResult!=null) {
		fm.AssistAgentCode.value = arrResult[0][0];
		fm.AssistAgentName.value = arrResult[0][1];
	}
}
function ExtendCheck()
{    
    if(fm.ExtendFlag.checked == true)
    {
    	if(fm.AssistSaleChnl.value == "" || fm.AssistSaleChnl.value == null)
    	{
    		alert("选择综合开拓时，协助销售渠道不能为空，请核查！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.AssistSaleChnl.value != "02" && fm.AssistSaleChnl.value != "13" && fm.AssistSaleChnl.value != "01")
    	{
    		alert("协助销售渠道只能选择个险直销、团险直销或银代直销！");
    		fm.all('AssistSaleChnl').focus();
    		return false;
    		
    	}
    	if(fm.SaleChnl.value == "01" || fm.SaleChnl.value == "10"){
    		if(fm.AssistSaleChnl.value != "02" && fm.AssistSaleChnl.value != "13"){
    			alert("销售渠道为个险渠道，协助销售渠道只能选择银代直销或团险直销！");
    			fm.all('AssistSaleChnl').focus();
    			return false;
    		}
    	}
    	
    	if(fm.SaleChnl.value == "04" || fm.SaleChnl.value == "13"){
    		if(fm.AssistSaleChnl.value != "02" && fm.AssistSaleChnl.value != "01"){
    			alert("销售渠道为银保渠道，协助销售渠道只能选择个险直销或团险直销！");
    			fm.all('AssistSaleChnl').focus();
    			return false;
    		}
    	}
    	
    	if(fm.AssistAgentCode.value == "" || fm.AssistAgentCode.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员代码不能为空，请核查！");
    		fm.all('AssistAgentCode').focus();
    		return false;
    	}
    	if(fm.AssistAgentName.value == "" || fm.AssistAgentName.value == null)
    	{
    		alert("选择综合开拓时，协助销售人员姓名不能为空，请核查！");
    		fm.all('AssistAgentName').focus();
    		return false;
    	}
    	var tSQL = "select code1,codename from ldcode1 where codetype = 'salechnl' and code = '"+fm.AssistSaleChnl.value+"' ";
		var arrResult = easyExecSql(tSQL);
		if(!arrResult){
			alert("销售渠道与业务员代码不匹配！");
			return false;
		}
		var tSQL1 = "select 1 from laagent where agentcode = '"+fm.AssistAgentCode.value+"' and BranchType = '"+arrResult[0][0]+"' and BranchType2 = '"+arrResult[0][1]+"'";
		var arrResult1 = easyExecSql(tSQL1);
		if(!arrResult1){
			alert("销售渠道与业务员代码不匹配，请核查！");
			return false;
		}
      	return true;
    }else{
    	fm.AssistSaleChnl.value == "";
    	fm.AssistAgentCode.value == "";
    	fm.AssistAgentName.value == "";
    	return true;
    }
}
function initExtend(){
	
	var tSql = "select AssistSalechnl,AssistAgentCode from LCExtend where prtno='" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.ExtendFlag.checked = true;
			fm.all('ExtendID').style.display = "";
			fm.AssistSaleChnl.value = arrResult[0][0];
			fm.AssistAgentCode.value = arrResult[0][1];
			var  tSql1 = "select name from LAAgent where agentcode ='" + arrResult[0][1] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.AssistAgentName.value = arrResult1[0][0];
		}
	}
}
function valiAddress(){
	
	var addr = fm.appnt_PostalAddress.value;
	var managecom  = fm.ManageCom.value;
	if(manageCheck(managecom.substring(0, 4),"addr")){
		if(addr.trim().length<6){
			alert("地址信息低于字符限制要求，须提详细地址信息!");
			return false;
		}
	}
	return true;
}

function chkAppAcc(cPrtNo)
{ 
    var tStrSql = ""
        + " select "
        + " tmp.ContNo, tmp.AppntNo, tmp.AppntName, tmp.SumPrem, nvl(sum(lcaa.AccGetMoney), 0) SumAccGetMoney "
        + " from "
        + " ( "
        + " select "
        + " lcc.ContNo, lcc.AppntNo, lcc.AppntName, nvl(sum(lcp.Prem), 0) SumPrem  "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcc.ContNo = lcp.ContNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.PayMethod = '01' "
        + " and lcc.PrtNo = '" + cPrtNo + "' "
        + " group by lcc.ContNo, lcc.AppntNo, lcc.AppntName "
        + " ) as tmp "
        + " left join LCAppAcc lcaa on lcaa.CustomerNo = tmp.AppntNo and lcaa.State = '1' "
        + " group by tmp.ContNo, tmp.AppntNo, tmp.AppntName, tmp.SumPrem "
        ;
    var arrAppAccInfo = easyExecSql(tStrSql);
    if(arrAppAccInfo != null)
    {
        if(arrAppAccInfo.length >= 2)
        {
            alert("投保人帐户信息出现异常。");
            return false;
        }
        var tAppAccInfo = arrAppAccInfo[0];
        var tSumPrem = tAppAccInfo[3];
        var tSumAccGetMoney = tAppAccInfo[4];
        if(Subtr(tSumPrem, tSumAccGetMoney) > 0)
        {
            alert("该单投保人帐户可用余额不足。该单首期保费[" + tSumPrem + "]，投保人帐户余额[" + tSumAccGetMoney + "]");
            return false;
        }
    }
    
    return true;
}

function Subtr(arg1, arg2)
{
    var r1, r2, m, n;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    n=(r1>=r2)?r1:r2;
    return ((arg1*m-arg2*m)/m).toFixed(n);
}

function initCountyType(){
   var tSql = "select CountyType,FamilySalary from lccontsub where prtno='" + fm.PrtNo.value + "' ";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		if(arrResult[0][0] != null && arrResult[0][0] != "" && arrResult[0][0] != "null"){
			fm.appnt_CountyType.value = arrResult[0][0];
			var  tSql1 = "select codename from ldcode where codetype= 'countytype' and code ='" + arrResult[0][0] + "'";
			var arrResult1 = easyExecSql(tSql1);
			fm.appnt_CountyName.value = arrResult1[0][0];
		}
		fm.FamilySalary.value=arrResult[0][1];
	}
}
function nextClick(){
//showInfo = window.open( "../familyapp/FamilySingleContInput.jsp?prtNo="+prtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"");
 var havesql ="select * from lwmission where missionprop2='"+fm.PrtNo.value+"'";
    var missionc=easyExecSql(havesql);
    if(missionc!=null){
    alert("该保单已录入完毕，不能再进行下一步！");
    return false;
    }
 
 var Str="select Contno from lccont where prtno='"+prtNo+"'";
 var ContNo = easyExecSql(Str);
 
 if(!ContNo){
  alert("请先保存保单信息!");
  return;
 }
urlStr = "./FamilySingleContMain.jsp?prtNo="+fm.PrtNo.value+"&ContNo="+ContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ManageCom="+ManageCom+"";
window.open(urlStr);

}

function checkAddress(){
   var PostalProvince = trim(fm.appnt_PostalProvince.value);
   var PostalCity = trim(fm.appnt_PostalCity.value);
   var PostalCounty = trim(fm.appnt_PostalCounty.value);
   var PostalStreet = trim(fm.appnt_PostalStreet.value);
   var PostalCommunity = trim(fm.appnt_PostalCommunity.value);

   //校验联系地址级联关系
   var checkCityError = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
		   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"' )");
   if(checkCityError == null || checkCityError == ""){
	   alert("投保人省、市地址级联关系不正确，请检查！");
	   return false;
   }
   var checkCityError2 = easyExecSql("select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"')");
//   var checkCityError2 = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
//	   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"'))");
   if(checkCityError2 == null || checkCityError2 == ""){
	   alert("投保人市、县地址级联关系不正确，请检查！");
	   return false;
   }
   //当省级单位下没有市县级单位或市级单位下没有县级单位的时候改变拼接完整地址的方式
   if(fm.appnt_PostalCity.value == '空' && fm.appnt_PostalCounty.value == '空'){
	   fm.appnt_PostalAddress.value=PostalProvince+PostalStreet+PostalCommunity;
   }else if(fm.appnt_PostalCity.value != '空' && fm.appnt_PostalCounty.value == '空') {
	   fm.appnt_PostalAddress.value = PostalProvince+PostalCity + PostalStreet+PostalCommunity;
   }else{
	   fm.appnt_PostalAddress.value=PostalProvince+PostalCity+PostalCounty+PostalStreet+PostalCommunity;
   }
   
   return true;
}

function checkSaleChnl(){
    
    var saleChnl = fm.SaleChnl.value;
    //销售渠道和业务员的校验 2008-4-10
    if(saleChnl != null && saleChnl != "")
    {
        //业务员离职校验
        var agentStateSql = "select 1 from LAAgent where AgentCode = '" + fm.AgentCode.value + "' and AgentState < '06'";
        var arr = easyExecSql(agentStateSql);
        if(!arr){
            alert("该业务员已离职！");
            return;
        }
        var tSQLCode = "select 1 from laagent where agentcode = '"+fm.AgentCode.value+"' and managecom = '"+fm.ManageCom.value+"' ";
	    var arrCode = easyExecSql(tSQLCode);
	    if(!arrCode){
		    alert("业务员与管理机构不匹配！");
	     	return false;
	    }
              
        //业务员和销售渠道的校验
        var agentCodeSql = "select 1 from LAAgent a, LDCode1 b where a.AgentCode = '" + fm.AgentCode.value
        + "' and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName and b.Code = '" + saleChnl + "'"
        + "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '10' = '" + saleChnl + "' and a.BranchType2 = '04'  "
		+ "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '03' = '" + saleChnl + "' and a.BranchType2 = '04'  "
		+ "union all "
		+ "select 1 from LAAgent a where a.AgentCode = '" + fm.AgentCode.value + "' "
		+ "and '02' = '" + saleChnl + "' and a.BranchType = '2' and a.BranchType2 = '02'  "
		;

        var arr = easyExecSql(agentCodeSql);
        if(!arr){
            alert("业务员和销售渠道不相符！");
            return;
        }
    }
    else
    {
        alert("销售渠道为空！");
        return;
    }
   
    //
    if(saleChnl == "04" || saleChnl == "10" || saleChnl == "15"|| saleChnl == "03" || saleChnl == "20" )
    {
        var agentCodeSql = "select 1 from LAComToAgent where AgentCom='" + fm.AgentCom.value
        + "' and RelaType='1' and AgentCode = '" + fm.AgentCode.value + "'";
        var arr = easyExecSql(agentCodeSql);
        if(!arr){
            alert("中介公司和中介专员不相符！");
            return;
        }
      
    }
    return true;
}

function checkAllapp(){
   var mobile = fm.appnt_Mobile.value;
    if(!chenkIdNo()){
	  return false;
	}
	
	if(!chenkHomePhone()){
	  return false;
	}
	
	if(mobile != null && mobile !="") {
	    if(!isInteger(mobile) || mobile.length != 11){
		    alert("投保人移动电话需为11位数字，请核查！");
   	      	return false;
	    } 
	}
	
	if(!checkMobile(mobile)){
	  return false;
	}
	   var homephone = fm.appnt_HomePhone.value;
	if(!isNull(homephone)){
		   var checkhomephone =CheckFixPhone(homephone);
		   if(checkhomephone !=""){
			   alert("投保人"+checkhomephone);
			   return false;
		   }
	   }
	
	if(!checkAddress()){
		  return false;
		}
	
   return true;
}

function chenkIdNo(){
        var idtype = fm.appnt_IDType.value;
        var idno = fm.appnt_IDNo.value;
        if(idtype == "" || idtype == null){
             alert("证件类型不能为空！");
             return false;
        }else{
             var arr = easyExecSql("select code1,codealias,othersign from ldcode1 where codetype='checkidtype' and code='"+idtype+"'");
             if(arr!=null){
                var othersign = arr[0][2];
                var first = arr[0][0];
                var two = arr[0][1];
                if("double"==othersign){
                   if(idno.length!=first&&idno.length!=two){
                       alert("录入的证件号码有误，请检查！");
                       return false;
                   }
                }
                if("min"==othersign){
                   if(idno.length<first){
                      alert("录入的证件号码有误，请检查！");
                       return false;
                   }
                }
                if("between"==othersign){
                   if(idno.length<first||idno.length>two){
                      alert("录入的证件号码有误，请检查！");
                      return false;
                   }
                }
                
             }else{
                alert("选择的证件类型有误，请检查！");
                return false;
             }
        }
    	//zxs 20190404
    	var nameResult = checkValidateName(idtype,fm.appnt_AppntName.value);
    	if(nameResult!=""){
    		alert("投保人"+nameResult);
    		return false;
    	} 
        return true;
}

function checkMobile(mobile){
   if(mobile != "" && mobile != null){
      if(mobile.length!=11){
          alert("移动电话不符合规则，请核实！");
          return false;
      }else{
         if(mobile.substring(0,2)!=13&&mobile.substring(0,2)!=14&&mobile.substring(0,2)!=16&&mobile.substring(0,2)!=15&&mobile.substring(0,2)!=18&&mobile.substring(0,2)!=17&&mobile.substring(0,2)!=19){
            alert("移动电话不符合规则，请核实！");
            return false;
         }
      }
   }else{
      if(!confirm("投保人移动电话为空，是否继续？")){
         return false;
	  }
   }
   return true;
}

function chenkHomePhone(){
    
    var homecode = "";
    var homenumber = "";
    var str = "";
    homecode = trim(fm.appnt_HomeCode.value);
    homenumber = trim(fm.appnt_HomeNumber.value);
    str ="投保人";
    
    if(homecode == "" || homecode == null){
         if(homenumber != "" && homenumber != null){
            alert(str+"固定电话区号未进行录入！");
            return false;
         }
    }else{
        if(homecode.length!=3 && homecode.length!=4||!isInteger(homecode)){
            alert(str+"固定电话区号录入有误，请检查！");
            return false;
        }
        if(homenumber == "" || homenumber == null){
            alert(str+"固定电话电话号码未进行录入！");
            return false;
        }
        if(homenumber.length!=7&&homenumber.length!=8&&homenumber.length!=10||!isInteger(homenumber)){
            alert(str+"固定电话电话号码录入有误，请检查！");
            return false;
        }else{
           if(homenumber.length==10){
               if(homenumber.substring(0, 3)!=400&&homenumber.substring(0, 3)!=800){
                  alert(str+"固定电话电话号码录入有误，请检查！");
                  return false;
               }
           }
        
        }
    
    }
    
    fm.appnt_HomePhone.value=homecode+homenumber;

    
    return true;
}

//双击对方业务员框要显示的页面
function otherSalesInfo(){
	//alert("双击对方业务员代码");
	window.open("../sys/MixedSalesAgentMain.jsp?ManageCom="+fm.ManageCom.value);
}
//对方业务员页面查询完“返回”按钮，接收数据
function afterQueryMIX(arrResult){
	if(arrResult!=null){
		var arr = arrResult.split("#");
		fm.GrpAgentCom.value = arr[0]; //对方机构代码
		//fm.GrpAgentComName.value = arr[1]; //对方机构名称
		fm.GrpAgentCode.value = arr[2]; //对方业务员代码
		fm.GrpAgentName.value = arr[3]; //对方业务员姓名
		fm.GrpAgentIDNo.value = arr[4]; //对方业务员身份证
		
	}else{
		alert("返回过程出现异常，请重新操作");
	}	
}

function controlNativeCity(displayFlag)
{
  if(fm.appnt_NativePlace.value=="OS"){
    fm.all("NativePlace").style.display = displayFlag;
	fm.all("NativeCity").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="国家";
  }else if(fm.appnt_NativePlace.value=="HK"){
    fm.all("NativePlace").style.display = displayFlag;
	fm.all("NativeCity").style.display = displayFlag;
	fm.all("NativeCityTitle").innerHTML="地区";
  }else{
    fm.all("NativePlace").style.display = "none";
	fm.all("NativeCity").style.display = "none";
  }
} 

function checkNative(){
   if(fm.appnt_NativePlace.value=="OS"||fm.appnt_NativePlace.value=="HK"){
      if(fm.appnt_NativeCity.value==""||fm.appnt_NativeCity.value==null){
         alert("投保人所属国家/地区未录入！");
         return false;
      }
   }
   return true;
}
//zxs 20190522
function checkAppDate(){
	var polApplyDate = fm.all('PolApplyDate').value;
	var receiveDate =  fm.all('ReceiveDate').value;
	var daysql = "select code from ldcode where  codetype = 'checkpolapplydate' ";
	var dayresult = easyExecSql(daysql);
	var sql = "select case when '"+polApplyDate+"' between current date -"+dayresult+" day and  current date  then 1 else 0 end  from dual ";
	var result = easyExecSql(sql);
	if(result=='0'){
		alert("投保申请日期录入有误，不得晚于当前日期，且不得早于当前日期-"+dayresult+"天。");
		return false;
	}
	if(receiveDate!=null&&receiveDate!=""){
		var sql1 = "select case when '"+receiveDate+"' between current date -"+dayresult+" day and  current date  then 1 else 0 end  from dual ";
		var result1 = easyExecSql(sql1);
		if(result1=='0'){
			alert("投保收单日期录入有误，不得晚于当前日期，且不得早于当前日期-"+dayresult+"天。");
			return false;
		}
	}
	return true;
}