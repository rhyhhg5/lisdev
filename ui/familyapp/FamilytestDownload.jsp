
<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<%@page contentType="text/html;charset=gbk"%>
<%@ include file="../common/jsp/UsrCheck.jsp" %>



<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%
boolean errorFlag = false;

//获得session中的人员信息
GlobalInput tG = (GlobalInput)session.getValue("GI");

//生成文件名
Calendar cal = new GregorianCalendar();
String min=String.valueOf(cal.get(Calendar.MINUTE));
String sec=String.valueOf(cal.get(Calendar.SECOND));
String downLoadFileName = "投保人_"+tG.Operator+"_"+ min + sec + ".xls";
String filePath = application.getRealPath("f1print");
String tOutXmlPath = filePath +File.separator+ downLoadFileName;
System.out.println("OutXmlPath:" + tOutXmlPath);
System.out.println(tG.Operator);
String querySql = request.getParameter("querySql");
System.out.println(querySql);
if(querySql==null){
	//针对360浏览器下载会自动提交两次问题
	String tBatchno=(String)session.getAttribute("BatchNo");
	//如果SQL由JS生成,应该在session中直接存sql就可以
	querySql=tBatchno;
	System.out.println("360");
	session.removeAttribute("Batchno");
}else{
	String tBatchno = request.getParameter("BatchNo");
	session.setAttribute("BatchNo", querySql);
}
	querySql = querySql.replaceAll("%25","%");
	//设置表头
	String[][] tTitle = {{"印刷号","管理机构","销售渠道","中介公司代码","业务员代码","业务员名称","投保单申请日期","收单日期","投保人姓名","证件类型","证件号码","客户类型","证件生效日期","证件失效日期","性别","家庭收入(万元)","出生日期","职业代码","年收入(万元)","工作单位","岗位职务","国籍","邮政编码","电子邮箱","联系电话","固定电话","移动电话","联系地址","缴费频次","缴费方式","开户银行","账 号","户名","是否需要续期缴费提醒","特别约定","套餐份数","交叉销售渠道","交叉销售业务类型","对方业务员代码","对方机构代码","对方业务员姓名","对方业务员证件号码","代理销售业务员编码"}};
	//表头的显示属性
	int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43};
  
//数据的显示属性
int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43};
//生成文件
CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
createexcellist.createExcelFile();
String[] sheetName ={"list"};
createexcellist.addSheet(sheetName);
int row = createexcellist.setData(tTitle,displayTitle);
if(row ==-1) errorFlag = true;
createexcellist.setRowColOffset(row,0);//设置偏移
if(createexcellist.setData(querySql,displayData)==-1)
	errorFlag = true;
if(!errorFlag)
//写文件到磁盘
try{
   createexcellist.write(tOutXmlPath);
}catch(Exception e)
{
	errorFlag = true;
	System.out.println(e);
}
//返回客户端
if(!errorFlag)
	downLoadFile(response,filePath,downLoadFileName);
out.clear();
	out = pageContext.pushBody();


%>

