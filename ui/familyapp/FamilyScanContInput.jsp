<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	//程序名称：FamilyScanContInput.jsp
	//程序功能：家庭简易投保
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
	<%
		GlobalInput tGI = new GlobalInput();
		tGI = (GlobalInput) session.getValue("GI");
	%>
	<script>	
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="FamilyScanContInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="FamilyScanContInputInit.jsp"%>
		<title>扫描录入</title>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="" method=post name=fm target="fraSubmit">
			<!-- 保单信息部分 -->
			<table class=common border=0 width=100%>
				<tr>
					<td class=titleImg align=center>
						请输入查询条件：
					</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						印刷号
					</TD>
					<TD class=input>
						<Input class=common name=PrtNo elementtype=nacessary
							verify="印刷号码|notnull">
					</TD>
	   <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<Input class="codeNo"  name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
          </TD>
					<TD class=title>
						系统申请日期
					</TD>
					<TD class=input>
						<Input class="coolDatePicker" dateFormat="short" name=InputDate
							verify="投保申请日期|date">
					</TD>
				</TR>
			</table>
			<INPUT VALUE="查  询" class=cssButton TYPE=button
				onclick="easyQueryClick();">
			<span id="divConttype" style="display: ''"> </span>
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLCGrp1);">
					</td>
					<td class=titleImg>
						投保单信息
					</td>
				</tr>
				<tr>
					<INPUT type="hidden" class=Common name=MissionID value="">
					<INPUT type="hidden" class=Common name=SubMissionID value="">
					<INPUT type="hidden" class=Common name=ActivityID value="">
					<INPUT type="hidden" class=Common name=AgentCom value="">
				</tr>
			</table>
			<Div id="divLCGrp1" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanGrpGrid"> </span>
						</td>
					</tr>
				</table>
				<INPUT VALUE="首  页" class=cssButton TYPE=hidden
					onclick="getFirstPage();">
				<INPUT VALUE="上一页" class=cssButton TYPE=hidden
					onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class=cssButton TYPE=hidden
					onclick="getNextPage();">
				<INPUT VALUE="尾  页" class=cssButton TYPE=hidden
					onclick="getLastPage();">
			</div>
			<Div id="divLCGrp2" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanPersonGrid"> </span>
						</td>
					</tr>
				</table>


			</div>
			<p>
				<INPUT VALUE="开始录入" class=cssButton TYPE=button
					onclick="GoToInput();">
			</p>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>

