
<%
            //程序名称：FamilySingleContInputInit.jsp
            //程序功能：
            //创建日期：201408528
            //创建人  ：zhangjd
            //更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
//添加页面控件的初始化。
%>
<script language="JavaScript">


function initForm()
{
  try
  {  
    fm.PrtNo.value = prtNo;
    fm.insured_PrtNo.value=prtNo;  
    initContno();
    initBox();
    initInsuredGrid();//被保险人列表初始化
    initBnfGrid();
    initRiskWrapGrid();
    queryInsuredInit();
    queryRiskWrap();
    queryBnfInit();
    //管理初始化套餐勾选信息
    initWrapCheck();

  }
  catch(ex)
  {
    alert("在初始化过程中出错："+ex.message)
  }
}
function initBox()
{
  
}

var turnPage = new turnPageClass();

//初始化被保人信息
function queryInsuredInit(){
    var tSql=" select InsuredNo,Name,(select codename from ldcode where codetype='sex' and code=Sex)Sex,(select codename from ldcode where codetype='idtype' and code=IDType)IDType,IDNo,(select codename from ldcode where codetype='relation' and code=RelationToAppnt)RelationToAppnt from LCInsured where PrtNo='"+prtNo+"'";
    turnPage.queryModal(tSql,InsuredGrid);
    
}

//初始化受益人信息
function queryBnfInit(){
    var tSql=" select distinct b.BnfType,b.Name,b.Sex,b.IDType,b.IDNo,(select SequenceNo from lcinsured i where i.InsuredNo =b.InsuredNo  and i.contno='"+fm.ContNo.value+"') seq,b.RelationToInsured,b.BnfLot,b.BnfGrade from LCBnf b where b.ContNo='"+fm.ContNo.value+"'";
    turnPage1.queryModal(tSql,BnfGrid);
    
}



//初始化套餐信息
function queryRiskWrap(){
    var tSql=" select a.riskwrapcode,a.wrapname ";
        
        var tSqll=" select contno from lcpol where contno='"+ContNo+"'";
        
        var arr=easyExecSql(tSqll);
        if(arr !=null && arr[0][0] !=""){
        tSql+=
              " , 'Amnt',(Select sum(c.Amnt)         "+                                
             " From Lcpol c              "+                                
             " where c.Prtno = '"+prtNo+"' ),  "+ 
             "'Prem',"+                            
             " (Select Sum(c.Prem)        "+                                
             " From Lcpol c              "+                                
             " where c.Prtno = '"+prtNo+"' ), "+ 
             "'InsuYear',"+                            
             "(Select Max(d.Insuyear)     "+                                
             " From Lcpol c ,             "+                                
             "      Lcriskdutywrap b,     "+                                
             "      Lcduty d              "+                                
             " Where c.Contno = d.Contno   "+                               
             " And b.Dutycode = d.Dutycode   "+                             
             " And b.Calfactor = 'InsuYear'    "+                           
             " And c.Prtno = '"+prtNo+"'    "+                           
             " And b.Riskwrapcode = a.Riskwrapcode    "+                    
             " And c.Contno = b.Contno    "+                                
             " And c.Riskcode = b.Riskcode    "+                            
             " And c.Riskcode In (Select Riskcode   "+                      
             "                   From Ldriskwrap    "+                      
             "                   Where Riskwrapcode = a.Riskwrapcode)), "+ 
             "'InsuYearFlag',"+  
             "(Select Max(d.Insuyearflag)                               "+  
             " From Lcpol c,              "+                                
             "      Lcriskdutywrap b,     "+                                
             "      Lcduty d              "+                                
             " Where c.Contno = d.Contno   "+                               
             " And b.Dutycode = d.Dutycode    "+                            
             " And b.Calfactor = 'InsuYearFlag'  "+                         
             " And c.Prtno = '"+prtNo+"'   "+                            
             " And b.Riskwrapcode = a.Riskwrapcode   "+                     
             " And c.Contno = b.Contno    "+                                
             " And c.Riskcode = b.Riskcode   "+                             
             " And c.Riskcode In (Select Riskcode  "+                       
             "                   From Ldriskwrap  "+                        
             "                   Where Riskwrapcode = a.Riskwrapcode)),'Copys',"+
             " (select calfactorvalue from lcriskdutywrap  where prtno='"+prtNo+"' "+
             " and riskwrapcode=a.riskwrapcode "+
             " and calfactor='Copys' fetch first 1 rows only )"; 
            
        }
        
         
        tSql+= "  from ldwrap a where wraptype='10' and (WrapManageCom ='"+ManageCom+"' or WrapManageCom=substr("+ManageCom+",1,2) or WrapManageCom=substr("+ManageCom+",1,4))";
    turnPage2.queryModal(tSql,RiskWrapGrid);
}


function  initWrapCheck(){
	if(RiskWrapGrid.mulLineCount==0)
	{
		//alert("该机构下没有定义套餐");
	}
	else
	{
	  RiskWrapGrid.checkBoxSel(1);
 	}	
	 

}

//初始化保单号
function initContno(){
   var tSql="select contno from lccont where prtno='"+prtNo+"'";
   var arr=easyExecSql(tSql);
   if(arr){
   fm.ContNo.value=arr[0][0];
   }else{
    alert("初始化保单号错误!");
   }
   
}

// 受益人信息列表的初始化
function initBnfGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号"; 			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";		//列宽
    iArray[0][2]=10;			//列最大值
    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="受益人类别"; 		//列名
    iArray[1][1]="60px";		//列宽
    iArray[1][2]=40;			//列最大值
    iArray[1][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="BnfType";
    iArray[1][9]="受益人类别|notnull&code:BnfType";

    iArray[2]=new Array();
    iArray[2][0]="姓名"; 	//列名
    iArray[2][1]="30px";		//列宽
    iArray[2][2]=30;			//列最大值
    iArray[2][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[2][9]="姓名|notnull&len<=20";//校验
    
    iArray[3]=new Array();
    iArray[3][0]="性别"; 	//列名
    iArray[3][1]="30px";		//列宽
    iArray[3][2]=30;			//列最大值
    iArray[3][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="sex";
    iArray[3][9]="性别|notnull&len<=20";//校验

    iArray[4]=new Array();
    iArray[4][0]="证件类型"; 		//列名
    iArray[4][1]="40px";		//列宽
    iArray[4][2]=40;			//列最大值
    iArray[4][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[4][4]="IDType";
    iArray[4][9]="证件类型|notnull&code:IDType";

    iArray[5]=new Array();
    iArray[5][0]="证件号码"; 		//列名
    iArray[5][1]="120px";		//列宽
    iArray[5][2]=80;			//列最大值
    iArray[5][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[5][9]="证件号码|notnull&len<=20";

    iArray[6]=new Array();
    iArray[6][0]="被保人序号";  //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=60;            //列最大值
    iArray[6][3]=2;            //是否允许输入,1表示允许，0表示不允许
    iArray[6][4]="insurednum";
    iArray[6][9]="与被保人序号|notnull&len<=3";  
    iArray[6][15]="prtNo";
    iArray[6][16]="#"+fm.PrtNo.value+"#";      
    
    
    iArray[7]=new Array();
    iArray[7][0]="与被保人关系"; 	//列名
    iArray[7][1]="60px";		//列宽
    iArray[7][2]=60;			//列最大值
    iArray[7][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[7][4]="Relation";
    iArray[7][9]="与被保人关系|code:Relation";

    iArray[8]=new Array();
    iArray[8][0]="受益比例"; 		//列名
    iArray[8][1]="40px";		//列宽
    iArray[8][2]=40;			//列最大值
    iArray[8][3]=1;			//是否允许输入,1表示允许，0表示不允许
    iArray[8][9]="受益比例|Decimal&len<=10";

    iArray[9]=new Array();
    iArray[9][0]="受益顺序"; 		//列名
    iArray[9][1]="40px";		//列宽
    iArray[9][2]=40;			//列最大值
    iArray[9][3]=2;			//是否允许输入,1表示允许，0表示不允许
    iArray[9][4]="OccupationType";
    iArray[9][9]="受益顺序|code:OccupationType";

    iArray[10]=new Array();
    iArray[10][0]="住址（填序号）"; 		//列名
    iArray[10][1]="160px";		//列宽
    iArray[10][2]=100;			//列最大值
    iArray[10][3]=3;			//是否允许输入,1表示允许，0表示不允许
    iArray[10][9]="住址|len<=80";

    iArray[11]=new Array();
    iArray[11][0]="速填"; 		//列名
    iArray[11][1]="30px";		//列宽
    iArray[11][2]=30;			//列最大值
    iArray[11][3]=3;			//是否允许输入,1表示允许，0表示不允许
    iArray[11][4]="customertype";

    BnfGrid = new MulLineEnter( "fm" , "BnfGrid" );
    //这些属性必须在loadMulLine前
    BnfGrid.mulLineCount = 0;
    BnfGrid.displayTitle = 1;
    BnfGrid.addEventFuncName="addInit";
    BnfGrid.loadMulLine(iArray);
		
  }
  catch(ex)
  {
    alert("在FamilySingleContInputInit.jsp-->initBnfGrid函数中发生异常:初始化界面错误!"+ex.message);
  }
}

// 被保人列表的初始化
function initInsuredGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";          //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=10;            //列最大值
    iArray[0][3]=0;         //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="被保人客户号";       //列名
    iArray[1][1]="60px";        //列宽
    iArray[1][2]=40;            //列最大值
    iArray[1][3]=0;         //是否允许输入,1表示允许，0表示不允许
   

    iArray[2]=new Array();
    iArray[2][0]="姓名";  //列名
    iArray[2][1]="30px";        //列宽
    iArray[2][2]=30;            //列最大值
    iArray[2][3]=0;         //是否允许输入,1表示允许，0表示不允许
    iArray[2][9]="姓名|notnull&len<=20";//校验
    
    iArray[3]=new Array();
    iArray[3][0]="性别";  //列名
    iArray[3][1]="30px";        //列宽
    iArray[3][2]=30;            //列最大值
    iArray[3][3]=0;         //是否允许输入,1表示允许，0表示不允许
    iArray[3][4]="sex";
    iArray[3][9]="性别|notnull&len<=20";//校验

    iArray[4]=new Array();
    iArray[4][0]="证件类型";        //列名
    iArray[4][1]="40px";        //列宽
    iArray[4][2]=40;            //列最大值
    iArray[4][3]=0;         //是否允许输入,1表示允许，0表示不允许
    iArray[4][4]="IDType";
    iArray[4][9]="证件类型|notnull&code:IDType";

    iArray[5]=new Array();
    iArray[5][0]="证件号码";        //列名
    iArray[5][1]="120px";       //列宽
    iArray[5][2]=80;            //列最大值
    iArray[5][3]=0;         //是否允许输入,1表示允许，0表示不允许
    iArray[5][9]="证件号码|notnull&len<=20";

    iArray[6]=new Array();
    iArray[6][0]="与被保人关系";  //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=60;            //列最大值
    iArray[6][3]=0;         //是否允许输入,1表示允许，0表示不允许
    iArray[6][4]="Relation";
    iArray[6][9]="与投保人关系|code:Relation";

   

    InsuredGrid = new MulLineEnter( "fm" , "InsuredGrid" );
    //这些属性必须在loadMulLine前
    InsuredGrid.mulLineCount = 0;
    InsuredGrid.displayTitle = 1;
    InsuredGrid.hiddenPlus=1;
    InsuredGrid.canSel = 1;
    InsuredGrid.hiddenSubtraction=1;
    //InsuredGrid.addEventFuncName="addInit";
    InsuredGrid.selBoxEventFuncName = "clickSelBox";
    
    InsuredGrid.loadMulLine(iArray);
        
    
  }
  catch(ex)
  {
    alert("在FamilySingleContInputInit.jsp-->initInsuredGrid函数中发生异常:初始化界面错误!");
  }
}



// 保障计划列表的初始化
function initRiskWrapGrid()
{
try{
	
	var iArray = new Array();
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=10;
	iArray[0][3]=0;
	
	iArray[1]=new Array();
	iArray[1][0]="套餐编码";
	iArray[1][1]="60px";
	iArray[1][2]=10;
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="套餐名称";
	iArray[2][1]="160px";
	iArray[2][2]=10;
	iArray[2][3]=0;
	
    iArray[3]=new Array();
    iArray[3][0]="Amnt";        //列名
    iArray[3][1]="40px";        //列宽
    iArray[3][2]=40; 
    iArray[3][3]=3;            //列最大值
    
    iArray[4]=new Array();
    iArray[4][0]="保额";        //列名
    iArray[4][1]="40px";        //列宽
    iArray[4][2]=40;            //列最大值
   
    iArray[5]=new Array();
    iArray[5][0]="Prem";        //列名
    iArray[5][1]="120px";       //列宽
    iArray[5][2]=80;            //列最大值
    iArray[5][3]=3;
    
    iArray[6]=new Array();
    iArray[6][0]="保费";        //列名
    iArray[6][1]="120px";       //列宽
    iArray[6][2]=80;            //列最大值

    iArray[7]=new Array();
    iArray[7][0]="InsuYear";        //列名
    iArray[7][1]="120px";       //列宽
    iArray[7][2]=80;            //列最大值
    iArray[7][3]=3;
    
    iArray[8]=new Array();
    iArray[8][0]="保险期间";  //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=60;            //列最大值
   
    iArray[9]=new Array();
    iArray[9][0]="InsuYearFlag";        //列名
    iArray[9][1]="120px";       //列宽
    iArray[9][2]=80;            //列最大值
    iArray[9][3]=3;
    
    iArray[10]=new Array();
    iArray[10][0]="保险期间标记";  //列名
    iArray[10][1]="60px";        //列宽
    iArray[10][2]=60;            //列最大值
    
    iArray[11]=new Array();
    iArray[11][0]="Copys";        //列名
    iArray[11][1]="120px";       //列宽
    iArray[11][2]=80;            //列最大值
    iArray[11][3]=3;
    
    iArray[12]=new Array();
    iArray[12][0]="套餐份数";  //列名
    iArray[12][1]="60px";        //列宽
    iArray[12][2]=60;            //列最大值
    
	 
    RiskWrapGrid = new MulLineEnter( "fm" , "RiskWrapGrid" );
		//这些属性必须在loadMulLine前
    RiskWrapGrid.mulLineCount = 0;
	RiskWrapGrid.displayTitle = 1;
	RiskWrapGrid.locked = 0;
	RiskWrapGrid.canSel = 0;
	RiskWrapGrid.canChk = 1;
	RiskWrapGrid.hiddenPlus = 1;
	RiskWrapGrid.hiddenSubtraction = 1;
	RiskWrapGrid.loadMulLine(iArray);
	}catch (ex){
		alert(ex.message);
	}
}

</script>
