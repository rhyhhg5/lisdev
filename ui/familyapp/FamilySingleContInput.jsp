<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
    <script>
    var prtNo = "<%=request.getParameter("prtNo")%>";
    var ManageCom = "<%=request.getParameter("ManageCom")%>";
    var ContNo = "<%=request.getParameter("ContNo")%>";
    var MissionID = "<%=request.getParameter("MissionID")%>";
    var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
</script>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=GBK">
        <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
        <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
        <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
        <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
        <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
        <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
        <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
        <LINK href="../common/css/Project.css" rel=stylesheet
            type=text/css>
        <LINK href="../common/css/mulLine.css" rel=stylesheet
            type=text/css>
        <%@include file="FamilySingleContInputInit.jsp"%>
        <SCRIPT src="FamilySingleContInput.js"></SCRIPT>


    </head>
    <body onload="initForm();initElementtype();">
        <form action="./FamilySingleContSave.jsp" method=post name=fm
            target="fraSubmit">

            <table id="table2">
                <tr>
                    <td>
                        <img src="../common/images/butExpand.gif"
                            style="cursor:hand;"
                            OnClick="showPage(this,InsuredInfoDiv);">
                    </td>
                    <td class="titleImg">
                        被保人清单基本信息
                    </td>
                </tr>
                </div>
            </table>

            <div id="InsuredInfoDiv" style="display: ''">
                <table class=common>
                    <tr class=common>
                        <td class=title COLSPAN="1"
                            id=insured_InsuredNoText>
                            被保险人客户号
                        </td>
                        <td class=input8 COLSPAN="1"
                            id=insured_InsuredNoClass>
                            <Input class=common name="insured_InsuredNo">
                            <Input type=button class=cssbutton
                                name="esayQuery" VALUE="查询"
                                OnClick="queryInsured()">
                        </td>
                        <td class=title COLSPAN="1">
                            与投保人关系
                        </td>
                        <td class=title COLSPAN="1">
                            <Input class=codeno
                                name=insured_RelationToAppnt
                                ondblclick="return showCodeList('Relation', [this,insured_RelationToAppntName],[0,1]);"
                                onkeyup="return showCodeListKey('Relation', [this,insured_RelationToAppntName],[0,1]);"
                                onchange="getAge();"><input class=codename
                                name=insured_RelationToAppntName
                                readonly=true elementtype=nacessary>
                        </td>
                        <td class=title COLSPAN="1">
                            与主被保险人关系
                        </td>
                        <td class=title COLSPAN="1">
                            <Input class=codeno
                                name=insured_RelationToMainInsured
                                ondblclick="return showCodeList('Relation', [this,insured_RelationToMainInsuredName],[0,1]);"
                                onkeyup="return showCodeListKey('Relation', [this,insured_RelationToMainInsuredName],[0,1]);"
                                onchange="getAge();"><input class=codename
                                name=insured_RelationToMainInsuredName
                                readonly=true elementtype=nacessary>
                        </td>
                        <td ></td>
                        <td ></td>
                        

                    </tr>
                </table>
                
             <div id="InsuredInfo" style="display: ''">
            <table class=common>
                <tr class=common>
                    <td class=title COLSPAN="1">
                        姓名
                    </td>
                    <td class=input COLSPAN="1">
                        <Input class=common name=insured_Name
                            elementtype=nacessary
                            verify="姓名|notnull&len<=60"
                            >
                    </td>

                    <TD class=title8>
                        证件类型
                    </TD>
                    <TD class=input8>
                        <Input class=codeno name=insured_IDType value=""
                            verify="证件类型|code:IDType"
                            ondblclick="return showCodeList('IDType',[this,insured_IDTypeName],[0,1],null,null,null,1);"
                            onkeyup="return showCodeListKey('IDType',[this,insured_IDTypeName],[0,1],null,null,null,1);"><input class=codename name=insured_IDTypeName
                            readonly=true elementtype=nacessary
                            verify="被保险人证件类型|code:IDType">
                    </TD>
                    <TD class=title COLSPAN="1">
                        证件号码
                    </TD>
                    <TD class=input COLSPAN="1">
                        <Input class=common name=insured_IDNo
                            elementtype=nacessary
                            verify="证件号码|notnull&len<=30"
                             onblur="checkidtype();getBirthdaySexByIDNo(this.value);">
                    </TD>
                   <TD class=title COLSPAN="1">
                        性别
                    </TD>
                    <TD class=input COLSPAN="1">
                        <Input class=codeno name=insured_Sex
                            verify="投保人性别|notnull"
                            " ondblclick="return showCodeList('Sex',[this,insured_SexName],[0,1],null,null,null,1);"
                            onkeyup="return showCodeListKey('Sex',[this,insured_SexName],[0,1],null,null,null,1);"><input class=codename name=insured_SexName>
                    </TD>
                </tr>
                <tr class=common>
                 <TD class=title COLSPAN="1">
                        出生日期
                    </TD>
                    <TD class=input COLSPAN="1">
                        <Input class=cooldatepicker
                            name=insured_Birthday
                            verify="出生日期|notnull&len<=20"
                           >
                    </TD>
                    <TD class=title COLSPAN="1">
                        证件生效日期
                    </TD>
                    <TD class=input COLSPAN="1">
                        <Input class=coolDatePicker name=IDStartDate
                            verify="证件生效日期|date">
                    </TD>
                    <TD class=title COLSPAN="1">
                        证件失效日期
                    </TD>
                    <TD class=input COLSPAN="1">
                        <Input class=coolDatePicker name=IDEndDate
                            verify="证件失效日期|date">
                    </TD>
                    <TD class=title>
                        年收入(万元)
                    </TD>
                    <TD class=input>
                        <Input class=common name=insured_Salary>
                    </TD>
                </tr>
                <tr class=common>
                    <TD class=title id=OccupationCodeText>
                        职业代码
                    </TD>
                    <TD class=input8 id=OccupationCodeClass>
                        <Input class=code name=insured_OccupationCode
                            style="width:60px" verify="职业代码"
                            " ondblclick="return showCodeList('OccupationCode',[this,insured_OccupationCodeName,insured_OccupationType],[0,1,2],null,null,null,1,300);"
                            onkeyup="return showCodeListKey('OccupationCode',[this,insured_OccupationCodeName,insured_OccupationType],[0,1,2],null,null,null,1,300);"><input class=codename
                            name=insured_OccupationCodeName
                            readonly=true style="width:130px">
                    </TD>
                    <TD class=title>
                        职业类别
                    </TD>
                    <TD class=input>
                        <Input class=common name=insured_OccupationType>
                    </TD>
                    <TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <Input class=codeno name=insured_NativePlace  verify="被保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,insured_NativePlaceName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('NativePlace',[this,insured_NativePlaceName],[0,1],null,null,null,1);" ><input class=codename name=insured_NativePlaceName readonly=true >
          
          </TD>
          <TD  class= title id="NativePlace1" style="display: none">
          <div id="NativeCityTitle1">国家</div>
          </TD>
          <TD  class= input id="NativeCity1" style="display: none">
          <input class=codeNo name="insured_NativeCity" verify="被保人国家|code:NativeCity" ondblclick="return showCodeList('NativeCity',[this,insured_NativeCityName],[0,1],null,fm.insured_NativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,insured_NativeCityName],[0,1],null,fm.insured_NativePlace.value,'ComCode',1);"><input class=codename name=insured_NativeCityName readonly=true >    
          </TD>
                </tr>
                <tr class=common>
                    <td class=title COLSPAN="1" id=insured_WorkNameText>
                        工作单位
                    </td>
                    <td class=input8 COLSPAN="1"
                        id=insured_WorkNameClass>
                        <input class=common3 name=insured_WorkName
                            style="width:150px" verify="工作单位">
                    </td>
                    <td class=title id=ZhiweiInstitle
                        style="display: ''">
                        岗位职务
                    </td>
                    <TD class=input id=ZhiweiIns style="display: ''">
                        <Input name=insured_Position VALUE=""
                            CLASS=common>
                    </TD>
                    </tr>
                <tr class=common>
                    <td class=title8 COLSPAN="1">
                        联系地址
                    </td>
                 <TD  class= input colspan=9>
					<Input class="codeno" name=Province verify="省（自治区直辖市）|notnull" ondblclick="return showCodeList('Province1',[this,insured_PostalProvince],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,insured_PostalProvince],[0,1],null,'0','Code1',1);" readonly=true ><input class=codename name=insured_PostalProvince readonly=true elementtype=nacessary>省（自治区直辖市）
					<Input class="codeno" name=City verify="市|notnull" ondblclick="return showCodeList('City1',[this,insured_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,insured_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" readonly=true ><input class=codename name=insured_PostalCity readonly=true elementtype=nacessary>市
					<Input class="codeno" name=County verify="县（区）|notnull" ondblclick="return showCodeList('County1',[this,insured_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,insured_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" readonly=true ><input class=codename name=insured_PostalCounty readonly=true elementtype=nacessary>县（区）
					<input  class= common10 name=insured_PostalStreet verify="乡镇（街道）|notnull"> 乡镇（街道）
			    	<input  class= common10 name=insured_PostalCommunity verify="村（社区）|notnull"> 村（社区）
                    <input class=common3 type = hidden name=insured_PostalAddress verify="联系地址">
	   			 </TD>
                    
                </tr>
                <tr class=common8>
                    <TD class=title8 COLSPAN="1"
                        id=insuredHomePhoneTitle style="display: ''">
                        家庭电话
                    </TD>
                    <TD class=input8 COLSPAN="1"
                        id=insuredHomePhoneInput style="display: ''">
                        <Input class=common8 name=insured_HomePhone
                            verify="家庭电话|len<=30">
                    </TD>
                    <TD class=title8 COLSPAN="1" id=insuredPhoneTitle
                        style="display: 'none'">
                        联系电话
                    </TD>
                    <TD class=input8 COLSPAN="1" id=insuredPhoneInput
                        style="display: 'none'">
                        <Input class=common8 name=insured_Phone
                            verify="联系电话|len<=30">
                    </TD>
                    <TD class=title8>
                        移动电话
                    </TD>
                    <TD class=input8>
                        <Input class=common8 name=insured_Mobile
                            verify="移动电话|len<=30">
                    </TD>
                    <TD class=title8 COLSPAN="1"
                        id=insuredCompanyPhoneTitle style="display: ''">
                        办公电话
                    </TD>
                    <TD class=input8 COLSPAN="1"
                        id=insuredCompanyPhoneInput style="display: ''">
                        <Input class=common8 name=insured_CompanyPhone
                            verify="办公电话|len<=30">
                    </TD>
                    <TD class=title8 COLSPAN="1">
                        电子邮箱
                    </TD>
                    <TD class=input8 COLSPAN="1">
                        <Input class=common8 name=insured_EMail
                            verify="电子邮箱|len<=60&Email">
                    </TD>
                </tr>
                <tr class=common>
                    <TD class=title8>
                        邮政编码
                    </TD>
                    <TD class=input8>
                        <Input class=common8 name=insured_ZipCode
                            verify="邮政编码|zipcode">
                    </TD>
                </tr>
            </table>

           </Div>
            <hr>

            <INPUT class=cssButton name="deleteButton" VALUE="新  增"
                TYPE=button onclick="addInsured();">
            <INPUT class=cssButton name="modifyButton" VALUE="修  改"
                TYPE=button onclick="updateInsured();">
            <INPUT class=cssButton name="saveButton" VALUE="删  除"
                TYPE=button onclick="deleteInsured();">
            <br>


                <table class=common id="MulBnfGrid" style="display: ''">
                    <tr class=common>
                        <td text-align: left colSpan=1>
                            <span id="spanInsuredGrid"> </span>
                        </td>
                    </tr>
                </table>

                <Div id="divPage6" align=center style="display: '' ">
                    <INPUT VALUE="首  页" class=cssButton TYPE=button
                        onclick="getFirstPage();">
                    <INPUT VALUE="上一页" class=cssButton TYPE=button
                        onclick="getPreviousPage();">
                    <INPUT VALUE="下一页" class=cssButton TYPE=button
                        onclick="getNextPage();">
                    <INPUT VALUE="尾  页" class=cssButton TYPE=button
                        onclick="getLastPage();">
                </Div>


           </div>

            <hr>

            <table id="table9" style="display: ''">
                <tr>
                    <td>
                        <img src="../common/images/butExpand.gif"
                            style="cursor:hand;"
                            OnClick="showPage(this,BnfInfoDiv);">
                    </td>
                    <td class="titleImg">
                        受益人信息
                    </td>
                </tr>
            </table>
            <div id="BnfInfoDiv" style="display: ''">
                <table class=common id="MulBnfGrid" style="display: ''">
                    <tr class=common>
                        <td text-align: left colSpan=1>
                            <span id="spanBnfGrid"> </span>
                        </td>
                    </tr>
                </table>

            </div>


            <table id="table9" style="display: ''">
                <tr>
                    <td>
                        <img src="../common/images/butExpand.gif"
                            style="cursor:hand;"
                            OnClick="showPage(this,RiskWrapInfoGridDiv);">
                    </td>
                    <td class="titleImg">
                        险种信息
                    </td>
                </tr>
            </table>
            <div id="RiskWrapInfoGridDiv" style="display: ''">
                <table class=common>
                    <tr class=common>
                        <td text-align: left colSpan=1>
                            <span id="spanRiskWrapGrid"> </span>
                        </td>
                    </tr>
                </table>
                <div id="" align=right style="display: '' ">
                    <INPUT class=cssButton name="modifyButton"
                        VALUE="保存险种信息" TYPE=button
                        onclick="return savePolInfo();">
                </div>

            </div>

            <hr>

            <Div id="" align=right style="display: '' ">
                <INPUT class=cssButton name="modifyButton" VALUE="录入完毕"
                    TYPE=button onclick="return inputConfirm(1);">
                <INPUT class=cssButton name="saveButton" VALUE="上一步"
                    TYPE=button onclick="return goback();">
            </Div>
            <!--隐藏域-->
            <input type="hidden" name="PrtNo" />
            <input type="hidden" name="ContNo" />
            <input type="hidden" name="insured_PrtNo" />
            <input type="hidden" name="WorkFlowFlag" />
            <input type="hidden" name="fmAction" />
            <input type="hidden" name="MissionID" />
            <input type="hidden" name="SubMissionID" />
            <input type="hidden" name="insuredNo" />


        </form>
        <span id="spanCode"
            style="display: none; position:absolute; slategray"></span>
    </body>
</html>
