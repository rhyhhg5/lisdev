<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
	<script>
    var prtNo = "<%=request.getParameter("prtNo")%>";
    var ManageCom = "<%=request.getParameter("ManageCom")%>";
    var CurrentDate = "<%=PubFun.getCurrentDate()%>";
    var MissionID = "<%=request.getParameter("MissionID")%>";
    var SubMissionID = "<%=request.getParameter("SubMissionID")%>";
    var tsql = "1 and code in (select code from ldcode where codetype=#paymodebrief#)";
    var msql = "";
    //managecomcode agentcomcode
</script>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="FamilyContInputInit.jsp"%>
		<SCRIPT src="FamilyContInput.js"></SCRIPT>
		<SCRIPT src="../intlapp/ContInsuredLCImpart.js"></SCRIPT>

	</head>
	<body onload="initForm();initElementtype();">
		<form action="./FamilyContSave.jsp" method=post name=fm
			target="fraSubmit">
			<table id="table1">
				<tr>
					<td>
						<img src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,ManageInfoDiv);">
					</td>
					<td class="titleImg">
						管理信息
					</td>
				</tr>
			</table>
			<div id="ManageInfoDiv1" style="display: ''">
				<table class=common>
					<tr class=common>
						<td class=title>
							印刷号
						</td>
						<td class=input>
							<Input class=common8 name=PrtNo elementtype=nacessary
								TABINDEX="-1" readonly="true" />
						</td>
	   <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          	<Input class="codeNo"  name=ManageCom  verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
          </TD>
						<TD class=title8>
							销售渠道
						</TD>
						<TD class=input8>
							<Input class=codeNo name=SaleChnl verify="销售渠道|notnull"
								ondblclick="return showCodeList('unitesalechnl',[this,SaleChnlName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('unitesalechnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true
								elementtype=nacessary>
						</TD>
					</tr>
					<tr>
						<td colspan="6">
							<font color="black">如果是交叉销售,请选择</font>
							<INPUT TYPE="checkbox" NAME="MixComFlag"  onclick="isMixCom();">
						</td>
					</tr>
					<%@include file="../sys/MixedSalesAgent.jsp"%>
					<!--  
					<tr class="common8" id="GrpAgentComID" style="display: none">
						<td class="title8">
							交叉销售渠道
						</td>
						<td class="input8">
							<input class="codeNo" name="Crs_SaleChnl" id="Crs_SaleChnl"
								verify="交叉销售渠道|code:crs_salechnl"
								ondblclick="return showCodeList('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('crs_salechnl',[this,Crs_SaleChnlName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_SaleChnlName"
								readonly="readonly" elementtype=nacessary />
						</td>
						<td class="title8">
							交叉销售业务类型
						</td>
						<td class="input8">
							<input class="codeNo" name="Crs_BussType" id="Crs_BussType"
								verify="交叉销售业务类型|code:crs_busstype"
								ondblclick="return showCodeList('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('crs_busstype',[this,Crs_BussTypeName],[0,1],null,null,null,1);" /><input class="codename" name="Crs_BussTypeName"
								readonly="readonly" elementtype=nacessary />
						</td>
						<td class="title8">
							&nbsp;
						</td>
						<td class="input8">
							&nbsp;
						</td>
					</tr>
					<tr class=common id="GrpAgentTitleID" style="display: 'none'">
						<td CLASS="title">
							对方机构代码
						</td>
						<td CLASS="input" COLSPAN="1">
							<Input class="code" name="GrpAgentCom" elementtype=nacessary
								ondblclick="return showCodeList('grpagentcom',[this],null,null, '1 and #1# = #1# and comp_cod =(case #' + fm.all('Crs_SaleChnl').value + '# when #01# then #000002# when #02# then #000100# end )', '1');"
								onkeyup="return showCodeListKey('grpagentcom',[this],null,null, '1 and #1# = #1# and comp_cod =(case #' + fm.all('Crs_SaleChnl').value + '# when #01# then #000002# when #02# then #000100# end)', '1');"
								onfocus="GetGrpAgentName();" onchange="getAgentName();">
						</td>
						<td class=title>
							对方机构名称
						</td>
						<td class=input>
							<Input class="common" name="GrpAgentComName"
								elementtype=nacessary TABINDEX="-1" readonly>
						</td>
						<td CLASS="title">
							对方业务员代码
						</td>
						<td CLASS="input" COLSPAN="1">
							<input NAME="GrpAgentCode" CLASS="common" elementtype=nacessary>
						</td>
					</tr>
					<tr class=common id="GrpAgentTitleIDNo" style="display: 'none'">
						<td class="title">
							对方业务员姓名
						</td>
						<td class="input" COLSPAN="1">
							<Input name=GrpAgentName CLASS="common" elementtype=nacessary>
						</td>
						<td CLASS="title">
							对方业务员身份证
						</td>

						<td CLASS="input" COLSPAN="1">
							<input NAME="GrpAgentIDNo" CLASS="common" elementtype=nacessary>
						</td>
					</tr>
					-->
					<div id="ManageInfoDiv2" style="display: ''">
						<table class=common>
							<TR class=common>
                          <TD  class= title8 id=zhongjiecode   style="display: ''">
	                         中介公司代码
	                      </TD>
	                      <TD  class= input8 id=zhongjiename style="display: ''">
	                         <Input class="codeNo" name=AgentCom verify="中介公司代码|code:AgentCom"
	                            ondblclick="return showCodeList(agentComCode,[this,AgentComName],[0,1],null, manageComCode, 'ManageCom');"
	                            onkeyup="return showCodeListKey(agentComCode,[this,AgentComName],[0,1],null, manageComCode, 'ManageCom');"><Input class="codeName" name=AgentComName readonly >
	                         <input name="btnQueryBranch" class="common" type="button" value="?" onclick="queryAgentCom()" style="width:20">
	                      </TD>
								<TD class=title8 id=AgentCodeG style="display: 'none'">
									业务员代码
								</TD>
								<TD class=title8 id=TXCode style="display: ''">
									中介专员代码
								</TD>
								<TD class=input8>
									<Input NAME=GroupAgentCode VALUE="" MAXLENGTH=0 CLASS=code8
										elementtype=nacessary ondblclick="return queryAgent();"
										onkeyup="return queryAgent2();" verify="代理人编码|notnull">
								    <Input NAME=AgentCode VALUE="" MAXLENGTH=0 CLASS=code8 type='hidden'>
										
								</TD>
								<TD class=title8 id=AgentNameG style="display: 'none'">
									业务员名称
								</TD>
								<TD class=title8 id=TXName style="display: ''">
									中介专员名称
								</TD>
								<TD class=input8>
									<Input NAME=AgentName VALUE="" CLASS=common>
								</TD>
								<TD class=title8 id="BlankTD1" style="display: 'none'">
								</TD>
								<TD class=input8 id="BlankTD2" style="display: 'none'">
								</TD>
								<Input class="readonly" type=hidden readonly name=AgentGroup
									verify="业务员组别notnull&len<=12">


							</TR>
							<TR class=common id="AgentSaleCodeID" style="display: 'none'">
								<TD class=title8>
									代理销售业务员编码
								</TD>
								<TD class=input8>
									<Input NAME=AgentSaleCode VALUE="" MAXLENGTH=10 CLASS=code8
										ondblclick="return queryAgentSaleCode();"
										onblur="return queryAgentSaleCode2();">
								</TD>
								<TD class=title8>
									代理销售业务员姓名
								</TD>
								<TD class=input8>
									<Input NAME=AgentSaleName VALUE="" readonly CLASS=common>
								</TD>
								<TD class=title8>
								</TD>
								<TD class=input8>
								</TD>
							</TR>
							<tr class=common>
								<TD class=title8 id=InputDateText>
									投保单申请日期
								</TD>
								<TD class=input8 id=InputDateClass>
									<Input class="cooldatepicker" name="PolApplyDate"
										verify="投保单申请日期|notnull&&date" elementtype=nacessary>
								</TD>
								<TD class=title8 id=ReceiveDateText>
									收单日期
								</TD>
								<TD class=input8 id=ReceiveDateClass>
									<Input class="cooldatepicker" name=ReceiveDate
										verify="收单日期|notnull&&date">
								</TD>

								<td class=title id=Tempfeetitle style="display: ''">
									缴费凭证号
								</td>
								<TD class=input id=input1 style="display: ''">
									<Input name=TempFeeNo VALUE="" CLASS=common>
									<font style="font-size:7pt;color:red;">银行转账先收费时录入</font>
								</TD>

							</tr>
						</table>
					</div>
					<table class=common>
						<tr>
							<td colspan="6">
								<font color="black">综合开拓标志</font>
								<INPUT TYPE="checkbox" NAME="ExtendFlag" onclick="isAssist();">
							</td>
						</tr>
						<tr class=common id="ExtendID" style="display: 'none'">
							<td CLASS="title">
								协助销售渠道
							</td>
							<td CLASS="input" COLSPAN="1">
								<Input class=codeNo name="AssistSaleChnl"
									ondblclick="return showCodeList('assistsalechnl_b',[this,AssistSalechnlName],[0,1]);"
									onkeyup="return showCodeListKey('assistsalechnl_b',[this,AssistSalechnlName],[0,1]);"><input class=codename name=AssistSalechnlName readonly=true
									elementtype=nacessary>
							</td>
							<td class=title>
								协助销售人员代码
							</td>
							<td class=input>
								<Input class="code8" name="AssistAgentCode"
									elementtype=nacessary ondblclick="return queryAssistAgent();">
							</td>
							<td CLASS="title">
								协助销售人员姓名
							</td>
							<td CLASS="input" COLSPAN="1">
								<input NAME="AssistAgentName" CLASS="common" TABINDEX="-1"
									readonly>
							</td>
						</tr>
					</table>

					<br />

					<div id="divAppntAccInfo" style="display:'none'">
						<table>
							<tr>
								<td>
									<img src="../common/images/butExpand.gif" style="cursor:hand;" />
								</td>
								<td class="titleImg">
									投保人帐户信息：
								</td>
							</tr>
						</table>
						<table class="common">
							<tr>
								<td class="title">
									缴费模式
								</td>
								<td class="input">
									<input class="codeNo" name="PayMethod"
										ondblclick="return showCodeList('paymethod',[this,PayMethodName],[0,1],null,null,null,1);"
										onkeyup="return showCodeListKey('paymethod',[this,PayMethodName],[0,1],null,null,null,1);"><input class="codename" name="PayMethodName"
										readonly="readonly" />
								</td>
								<td class="title">
									&nbsp;
								</td>
								<td class="input">
									&nbsp;
								</td>
								<td class="title">
									&nbsp;
								</td>
								<td class="input">
									&nbsp;
								</td>
							</tr>
							<tr>
								<td class="title">
									投保人帐户编号
								</td>
								<td class="input">
									<input class="readonly" name="CustNoAppAcc" readonly="readonly" />
								</td>
								<td class="title">
									投保人帐户户名
								</td>
								<td class="input">
									<input class="readonly" name="CustNameAppAcc"
										readonly="readonly" />
								</td>
								<td class="title">
									投保人帐户余额
								</td>
								<td class="input">
									<input class="readonly" name="BalanceAppAcc"
										readonly="readonly" />
								</td>
							</tr>
						</table>

						<br />

					</div>

					<table id="table2">
						<tr>
							<td>
								<img src="../common/images/butExpand.gif" style="cursor:hand;"
									OnClick="showPage(this,AppntInfoDiv);">
							</td>
							<td class="titleImg">
								投保人信息
							</td>
						</tr>
					</table>
					<div id="AppntInfoDiv" style="display: ''">
						<table class=common>
							<tr class=common>
								<td class=title COLSPAN="1">
									投保人客户号
								</td>
								<td class=input COLSPAN="1">
									<Input class=common name=appnt_AppntNo verify="投保人客户号|len<=60">
								</td>
								<td>
									<Input type=button class=cssbutton VALUE="查询"
										OnClick="queryAppnt()">
								</td>
								<td></td>
							</tr>
						</table>
					</div>
					<table class=common>
						<div id="AppntInfoDiv1" style="display: ''">
						<tr class=common>
							<td class=title COLSPAN="1">
								姓名
							</td>
							<td class=input COLSPAN="1">
								<Input class=common name=appnt_AppntName elementtype=nacessary
									verify="姓名|notnull&len<=60" onblur="getaccname()">
							</td>
							<TD class=title8>
								证件类型
							</TD>
							<TD class=input8>
								<Input class=codeno name=appnt_IDType value="0"
									verify="证件类型|code:IDType"
									ondblclick="return showCodeList('IDType',[this,appnt_IDTypeName],[0,1],null,null,null,1);"
									onkeyup="return showCodeListKey('IDType',[this,appnt_IDTypeName],[0,1],null,null,null,1);"><input class=codename name=appnt_IDTypeName readonly=true
									elementtype=nacessary verify="投保人证件类型|code:IDType">
							</TD>
							<TD class=title COLSPAN="1">
								证件号码
							</TD>
							<TD class=input COLSPAN="1">
								<Input class=common name=appnt_IDNo elementtype=nacessary
									verify="证件号码|notnull&len<=30"
									onblur="getAppntBirthdaySexByIDNo(this.value)">
							</TD>
							<TD class=title COLSPAN="1">
								客户类型
							</TD>
							<TD class=input COLSPAN="1">
								<Input class=codeno name=appnt_CountyType
									CodeData="0|^1|城镇^2|农村"
									ondblclick="return showCodeListEx('appnt_CountyType',[this,appnt_CountyName],[0,1],null,null,null,1);"
									onkeyup="showCodeListKeyEx('appnt_CountyType',[this,appnt_CountyName],[0,1],null,null,null,1);"><input class=codename name=appnt_CountyName>
							</TD>
						</tr>
						<tr class=common>
							<TD class=title COLSPAN="1">
								证件生效日期
							</TD>
							<TD class=input COLSPAN="1">
								<Input class=coolDatePicker name=appnt_IDStartDate
									verify="证件生效日期|date">
							</TD>
							<TD class=title COLSPAN="1">
								证件失效日期
							</TD>
							<TD class=input COLSPAN="1">
								<Input class=coolDatePicker name=appnt_IDEndDate
									verify="证件失效日期|date">
							</TD>
							<TD class=title COLSPAN="1">
								性别
							</TD>
							<TD class=input COLSPAN="1">
								<Input class=codeno name=appnt_AppntSex verify="投保人性别|notnull"
									" ondblclick="return showCodeList('Sex',[this,appnt_SexName],[0,1],null,null,null,1);"
									onkeyup="return showCodeListKey('Sex',[this,appnt_SexName],[0,1],null,null,null,1);"><input class=codename name=appnt_SexName>
							</TD>
							<TD class=title>
								家庭收入(万元)
							</TD>
							<TD class=input>
								<Input class=common name=FamilySalary>
							</TD>
						</tr>
						<tr class=common>
							<TD class=title COLSPAN="1">
								出生日期
							</TD>
							<TD class=input COLSPAN="1">
								<Input class=cooldatepicker name=appnt_AppntBirthday
									verify="出生日期|notnull&len<=20" onchange="getAge();">
							</TD>
							<TD class=title id=OccupationCodeText>
								职业代码
							</TD>
							<TD class=input8 id=OccupationCodeClass>
								<Input class=code name=appnt_OccupationCode style="width:60px"
									verify="职业代码"
									" ondblclick="return showCodeList('OccupationCode',[this,appnt_OccupationName,appnt_OccupationType],[0,1,2],null,null,null,1,300);"
									onkeyup="return showCodeListKey('OccupationCode',[this,appnt_OccupationName,appnt_OccupationType],[0,1,2],null,null,null,1,300);"><input class=codename name=appnt_OccupationName readonly=true
									style="width:130px">
							</TD>
							<TD class=title>
								职业类别
							</TD>
							<TD class=input>
								<Input class=common name=appnt_OccupationType>
							</TD>
							<TD class=title>
								年收入(万元)
							</TD>
							<TD class=input>
								<Input class=common name=appnt_Salary>
							</TD>
						</tr>
						<tr class=common>
							<td class=title COLSPAN="1" id=WorkNameText>
								工作单位
							</td>
							<td class=input8 COLSPAN="1" id=WorkNameClass>
								<input class=common3 name=WorkName style="width:150px"
									verify="工作单位">
							</td>
							<td class=title id=ZhiweiApptitle style="display: ''">
								岗位职务
							</td>
							<TD class=input id=ZhiweiApp style="display: ''">
								<Input name=appnt_Position VALUE="" CLASS=common>
							</TD><TD  class= title>
            国籍
          </TD>
          <TD  class= input>
          <Input class=codeno name=appnt_NativePlace  verify="投保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,appnt_NativePlaceName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('NativePlace',[this,appnt_NativePlaceName],[0,1],null,null,null,1);" ><input class=codename name=appnt_NativePlaceName readonly=true >
          
          </TD>
          <TD  class= title id="NativePlace" style="display: none">
          <div id="NativeCityTitle">国家</div>
          </TD>
          <TD  class= input id="NativeCity" style="display: none">
          <input class=codeNo name=appnt_NativeCity verify="投保人国家|code:NativeCity" ondblclick="return showCodeList('NativeCity',[this,appnt_NativeCityName],[0,1],null,fm.appnt_NativePlace.value,'ComCode',1);" onkeyup="return showCodeListKey('NativeCity',[this,appnt_NativeCityName],[0,1],null,fm.appnt_NativePlace.value,'ComCode',1);"><input class=codename name=appnt_NativeCityName readonly=true >    
          </TD>
						</tr>
  <tr>
	      <td class=title8 COLSPAN="1">
	      		联系地址
	      	</td>
	    <TD  class= input colspan=9>
			<Input class="codeno" name=Province verify="省（自治区直辖市）|notnull" ondblclick="return showCodeList('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1);" readonly=true ><input class=codename name=appnt_PostalProvince readonly=true elementtype=nacessary>省（自治区直辖市）
			<Input class="codeno" name=City verify="市|notnull" ondblclick="return showCodeList('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" readonly=true ><input class=codename name=appnt_PostalCity readonly=true elementtype=nacessary>市
			<Input class="codeno" name=County verify="县（区）|notnull" ondblclick="return showCodeList('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" readonly=true ><input class=codename name=appnt_PostalCounty readonly=true elementtype=nacessary>县（区）
			<input  class= common10 name=appnt_PostalStreet verify="乡镇（街道）|notnull" > 乡镇（街道）
	    	<input  class= common10 name=appnt_PostalCommunity verify="村（社区）|notnull" > 村（社区）
		      	<input class= common3 name=appnt_PostalAddress type="hidden" verify="联系地址">
	    </TD>
	      	<!-- <td class=input8 COLSPAN=5>
		      	<input class= common3 name=appnt_PostalAddress type="hidden" verify="联系地址">
		      	<input class= common10 name=appnt_PostalProvince verify="投保人联系地址|notnull"> 省（自治区直辖市）
		      	<input class= common10 name=appnt_PostalCity verify="投保人联系地址|notnull"> 市
		      	<input class= common10 name=appnt_PostalCounty verify="投保人联系地址|notnull"> 县（区）
		      	<input class= common10 name=appnt_PostalStreet verify="投保人联系地址|notnull"> 乡镇（街道）
		      	<input class= common10 name=appnt_PostalCommunity verify="投保人联系地址|notnull"> 村（社区）
		      </td> -->
	      </tr>
	      <tr class=common8 >
	      	<TD  class= title8 COLSPAN="1" id=appntHomePhoneTitle style="display: ''">
            固定电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=appntHomePhoneInput style="display: ''">
            <Input class= common8 name=appnt_HomePhone  type="hidden" >
            <input class= common11 name=appnt_HomeCode> - <input class= common10 name=appnt_HomeNumber>
          </TD>
	      	<TD  class= title8 COLSPAN="1" id=appntPhoneTitle style="display: ''">
            联系电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=appntPhoneInput style="display: ''">
            <Input class= common8 name=appnt_Phone  verify="联系电话|len<=30">
          </TD>
          <TD  class= title8>
            移动电话
          </TD>
          <TD  class= input8>
            <Input class= common8 name=appnt_Mobile   verify="移动电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1" id=appntCompanyPhoneTitle style="display: 'none'">
            办公电话
          </TD>
          <TD  class= input8 COLSPAN="1" id=appntCompanyPhoneInput style="display: 'none'">
            <Input class= common8 name=appnt_CompanyPhone verify="办公电话|len<=30">
          </TD>
          <TD  class= title8 COLSPAN="1">
            电子邮箱
          </TD>
          <TD  class= input8 COLSPAN="1">
            <Input class= common8 name=appnt_EMail verify="电子邮箱|len<=60&Email">
          </TD>
	      </tr>
	      <tr class=common >
		      <TD  class= title8>
            邮政编码
          </TD>
          <TD  class= input8>
            <Input class= common8 name=appnt_ZipCode    verify="邮政编码|zipcode">
          </TD>
	      </tr>
					</table>

					<div id="PayInfoTitleDiv" style="display: ''">
						<table id="table5">
							<tr>
								<td>
									<img src="../common/images/butExpand.gif" style="cursor:hand;"
										OnClick="showPage(this,PayInfoDiv);">
								</td>
								<td class="titleImg">
									缴费信息
								</td>
							</tr>
						</table>
					</div>
					<div id="PayInfoDiv" style="display: ''">
						<table class="common">
							<tr class=common8>
								<TD class=title8 COLSPAN="1">
									缴费频次
								</TD>
								<TD class=input8 COLSPAN="1">
									<Input class=codeNo name=PayIntv CodeData="0|^0|趸缴^12|年缴"
										verify="缴费频次|notnull"
										ondblclick="return showCodeListEx('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);"
										onkeyup="showCodeListKeyEx('grppayintv',[this,GrpContPayIntvName],[0,1],null,null,null,1);"><input class=codename name=GrpContPayIntvName
										elementtype=nacessary readonly=true>
								</TD>
								<TD class=title8 COLSPAN="1">
									缴费方式
								</TD>
								<TD class=input8 COLSPAN="1">
									<Input class=codeNo name=PayMode
										verify="缴费方式|notnull&code:PayModeBrief"
										ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"
										onkeyup="showCodeListKey('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"><input class=codename name=PayModeName readonly=true>
								</TD>
							</tr>
						</table>
					</div>
					<div id="BankCom" style="display: ''">
						<table class="common">
							<tr>
								<TD class=title8 COLSPAN="1">
									开户银行
								</TD>
								<TD class=input8 COLSPAN="1">
									<Input class=codeNo name=BankCode
										verify="开户银行|code:bank&len<=24"
										ondblclick="return showCodeList('SimipleBankBranchCom',[this,BankCodeName],[0,1],null,fm.PayMode.value,'ComCode',1);"
										onkeyup="return showCodeListKey('SimipleBankBranchCom',[this,BankCodeName],[0,1],null,fm.PayMode.value,'ComCode',1);"><input class=codename name=BankCodeName>
								</TD>
							</tr>
							<tr class=common8>
								<TD class=title8>
									账&nbsp;&nbsp;&nbsp;号
								</TD>
								<TD class=input8>
									<Input class=common8 name=BankAccNo verify="帐号|len<=40">
								</TD>
								<TD class=title8>
									户&nbsp;&nbsp;&nbsp;名
								</TD>
								<TD class=input8>
									<Input class=common8 name=AccName verify="户名|len<=40">
								</TD>
							</tr>
							<tr class="common8" id="ShowMsgFlag">
								<td class="title8">
									是否需要续期缴费提醒
								</td>
								<td class="input8" colspan="3">
									<input name="DueFeeMsgFlag" class="codeNo"
										ondblclick="return showCodeList('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);"
										onkeyup="return showCodeListKey('duefeemsgflag',[this,DueFeeMsgFlagName],[0,1],null,null,null,1);"
										verify="是否需要续期缴费提醒|notnull&code:duefeemsgflag"><input class="codename" name="DueFeeMsgFlagName" readonly=true
										elementtype=nacessary />
								</td>
							</tr>
						</table>
					</div>
					<font size=2 color="#ff0000"> <b>新、旧缴费方式对照说明</b>
						&nbsp;&nbsp;1现金--->1现金、3转账支票--->3支票、4银行转账--->4银行转账
						<table id="table6">
							<tr>
								<td>
									<img src="../common/images/butExpand.gif" style="cursor:hand;"
										OnClick="showPage(this,RemarkDiv);">
								</td>
								<td class="titleImg">
									备注栏
								</td>
							</tr>
						</table>
						<div id="RemarkDiv" style="display ''">
							<table class=common>
								<TR class=common>
									<TD class=title8>
										特别约定
									</TD>
								</TR>
								<TR class=common>
									<TD class=title8>
										<textarea name="Remark" cols="110" rows="3" class="common3"
											width=100% value="" type="_moz">
		        </textarea>
									</TD>
								</TR>
							</table>
						</div>
						<table class=common>
							<tr class=common>
								<td>
									<span id="operateButton">
										<table class=common align=center>
											<tr align=right>
												<td class=button width="10%" align=right>
													<INPUT class=cssButton name="querybutton" VALUE="查  询"
														TYPE=hidden onclick="return queryClick();">
												</td>
												<td class=button width="10%" align=right>
													<INPUT class=cssButton name="deleteButton" VALUE="删  除"
														TYPE=hidden onclick="return deleteClick();">
												</td>
												<td class=button>
												</td>
												<td class=button width="10%" align=right>
													<INPUT class=cssButton name="saveButton" VALUE="保  存"
														TYPE=button onclick="return submitForm();">
												</td>
												<td class=button width="10%" align=right>
													<INPUT class=cssButton name="modifyButton" VALUE="修  改"
														TYPE=button onclick="return updateClick();">
												</td>
												<td class=button width="10%" align=right>
													<INPUT class=cssButton name="modifyButton" VALUE="下 一 步"
														TYPE=button onclick="return nextClick();">
												</td>

											</tr>
										</table> </span>
								</td>
							</tr>
						</table> <!--隐藏域--> <input type=hidden id="MissionProp5"
							name="MissionProp5"> <INPUT TYPE=hidden name=mContNo>
						<Input type="hidden" name="Flag" value="1"> <input
							type="hidden" name=PrtFlag> <input type="hidden"
							name=ContNo> <input type="hidden" name=fmAction>
						<input type="hidden" name=appnt_AddressNo> <input
							type="hidden" name=mCardFlag> <input type="hidden"
							name=ContType> <input type="hidden" name=NeedMulBnf>
						<input type="hidden" name=RiskWrapCol> <input
							type="hidden" name=RiskWrapFlag> <input type="hidden"
							name="InputDate" /> <input type="hidden" name="InputTime" />
						</form> <span id="spanCode"
						style="display: none; position:absolute; slategray"></span>
	</body>
</html>
