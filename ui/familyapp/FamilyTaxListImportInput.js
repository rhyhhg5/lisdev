var turnPage = new turnPageClass();

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var checksubmitflg = false;
var flag = false;

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;
/**
 * 初始化批次号。
 */
function initBatchNoInput(cBatchNo) {
	if (cBatchNo != null && cBatchNo != "") {
		fmImport.BatchNo.value = cBatchNo;
	} else {
		fmImport.BatchNo.value = "";
	}
}

/**
 * 导入清单。
 */
function importTaxList() {

	if (!beforeImportTaxList()) {
		return false;
	}
	var tBatchNo = fmImport.BatchNo.value;
	fmImport.btnImport.disabled = true;

	var showStr = "正在导入清单数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	var filename = fmImport.all('FileName').value;

	fmImport.action = "./FamilyTaxListImportSave.jsp?BatchNo=" + tBatchNo
			+ "&FileName=" + filename;

	fmImport.submit();

}

/**
 * 导入清单前的检查，校验，准备
 */

function beforeImportTaxList() {
	var tBatchNo = fmImport.BatchNo.value;
	var tFileName = fmImport.FileName.value;
	// 文件名不能为空
	if (tFileName == null || tFileName == "") {
		alert("请选择文件！");
		return false;
	}
	// 文件类型应为表格类型.xls
	if (fmImport.FileName.value.indexOf(".xls") == -1
			|| fmImport.FileName.value.indexOf(".xlsx") >= 0) {
		alert("文件类型不正确！");
		return false;
	}
	// 文件名与批次号要一致
	if (tFileName.indexOf("\\") > 0) {
		tFileName = tFileName.substring(tFileName.lastIndexOf("\\") + 1);
	}
	if (tFileName.indexOf("/") > 0) {
		tFileName = tFileName.substring(tFileName.lastIndexOf("/") + 1);
	}
	if (tFileName.indexOf("_") > 0) {
		tFileName = tFileName.substring(0, tFileName.lastIndexOf("_"));
	}
	if (tFileName.indexOf(".") > 0) {
		tFileName = tFileName.substring(0, tFileName.lastIndexOf("."));
	}
	if (tFileName != tBatchNo) {
		alert("文件名与批次号不一致，请检查上传文件的文件名！");
		return false;
	}
	// 0：待导入;1:待内检;已进行导入
	// 2:已确认;不可进行导入3：內检存在问题;不可进行导入
	var tSQL = "select state from xfyjappnt where batchno='" + tBatchNo
			+ "' and prtno='000000'";
	var tArr = easyExecSql(tSQL);
	var tBatchState = "";
	if (tArr) {
		tBatchState = tArr[0][0];
	}

	if (tBatchState != '0') {
		alert("该批次已进行过导入，不可再次进行导入！");
		return false;
	}
	return true;
}

/**
 * 导入清单提交后动作。
 */
function afterSubmit(FlagStr, Content, cBatchNo) {

	showInfo.close();
	window.focus();

	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		fm.ImportErrBatchNo.value = cBatchNo;
		queryImportErrLog();
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		fm.ImportErrBatchNo.value = cBatchNo;
		queryImportErrLog();
	}

	fmImport.btnImport.disabled = false;
}

/**
 * 批次导入确认。
 */
function confirmImport() {
	// 只有批次状态变为 1:待确认的批次才可以点击【批次导入确认】按钮
	var tBatchNo = fmImport.BatchNo.value;
	var tSQL = "select 1 from xfyjappnt where batchno='" + tBatchNo
			+ "' and state in ('0','2')";
	var arr = easyExecSql(tSQL);
	if (arr) {
		alert("该批次未导入或已确认，不能进行【批次导入确认】操作");
		return false;
	}
	var tSQL1 = "select 1 from xfyjappnt where batchno='" + tBatchNo
	+ "' and flag='0'";
	var arr = easyExecSql(tSQL1);
	if (!arr) {
	alert("该批次不存在可以确认的保单，不能进行【批次导入确认】操作");
	return false;
	}
	// 若批次中含有内检失败记录，不允许确认
	var tConfirmSQL = "select 1 from xfyjappnt where batchno='" + tBatchNo
	+ "' and flag='1'";
	var tArr = easyExecSql(tConfirmSQL);

	if (tArr) {
		alert("该批次中包含内检失败数据，请先删除导入错误保单！");
		return false;
	}
	// 若批次中含有内检失败记录，不允许确认
	var tConfirmSQL1 = "select 1 from xfyjlog where batchno='" + tBatchNo
	+ "' and other='导入失败！'";
	var tArr1 = easyExecSql(tConfirmSQL1);

	if (tArr1) {
		alert("该批次中包含内检失败数据，请先删除导入错误保单！");
		return false;
	}
	fm.btnImportConfirm.disabled = true;
	fm.btnImportDelete.disabled = true;

	var showStr = "正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.action = "./FamilymoreConfirm.jsp";
	fm.submit();

	fm.action = "";
}

/**
 * 批次导入确认后动作。
 */
function afterSubmit1(FlagStr, Content, cBatchno) {
	showInfo.close();
	window.focus();

	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	}
	queryImportErrLog();
}

/**
 * 批次被保人删除
 */
window.confirm = function(str)  //Author: meizz
{
    str=str.replace(/\'/g, "'&chr(39)&'").replace(/\r\n|\n|\r/g, "'&VBCrLf&'");
    execScript("n = msgbox('"+ str +"',4, '删除提示')", "vbscript");
    return(n == 6);
}
function delImport() {
	// 若批次状态为3-已确认，则无需进行被保人删除操作
	
	var tBatchNo = fmImport.BatchNo.value;
	var tSQL3 = "select 1 from xfyjappnt where batchno='" + tBatchNo
			+ "' and state = '0' and prtno='000000'"; //--
	var arr3 = easyExecSql(tSQL3);
	if (arr3) {
		alert("该批次未上传，不能进行【批次被保人删除】操作！");
		return;
	}
	var tSQL4 = "select 1 from xfyjappnt where batchno='" + tBatchNo
	+ "' and prtno<>'000000' and flag in ('1','2')"; //--
	var arr4 = easyExecSql(tSQL4);
	if (!arr4) {
		alert("该批次没有可删除保单，不能进行【批次被保人删除】操作！");
		return;
	}
	 var flag=2;
	 var msg = "是否同时删除确认失败的保单？";  
	
//	 if (confirm(msg)==true){   
//		 flag=2;  
//	 }else{   
//		 flag=1; 	 
//	 } 
	fm.btnImportConfirm.disabled = true;
	fm.btnImportDelete.disabled = true;

	var showStr = "正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.action = "./FamilyTaxDeleteSave.jsp?flag="+flag;
	fm.submit();

	fm.action = "";
	
	
}
function delImport1() {
	
	var tBatchNo = fmImport.BatchNo.value;
	var tSQL3 = "select 1 from xfyjappnt where batchno='" + tBatchNo
			+ "' and state in ('1','3') and prtno='000000'"; //--
	var arr3 = easyExecSql(tSQL3);
	if (!arr3) {
		alert("该批次未上传或已确定完毕，不能进行【批次被保人删除】操作！");
		return;
	}
	// 若批次状态为3-已确认，则无需进行被保人删除操作
	 var flag='3';
	 var msg = "请确定整个批次删除，一旦删除将会所有导入保单全部删除？";  
	
	 if (confirm(msg)==true){   
		  
	 }else{   
		return false;	 
	 } 
	fm.btnImportConfirm.disabled = true;
	fm.btnImportDelete.disabled = true;

	var showStr = "正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.action = "./FamilyTaxDeleteSave.jsp?flag="+flag;
	fm.submit();

	fm.action = "";
}
// 查询未成功导入的记录
function queryImportErrLog() {

	var batchno = fmImport.BatchNo.value;
	var StrSQL11 = "select count(*) from xfyjappnt where batchno='" + batchno
	+ "' and prtno<>'000000' with ur ";
	
	var all = easyExecSql(StrSQL11);
	if(all==null){
		all=0
	}
	var StrSQL12 = "select count(*) from xfyjappnt where batchno='" + batchno
	+ "' and  prtno<>'000000' and flag='0'";
	
	var succ = easyExecSql(StrSQL12);
	if(succ==null){
		succ=0
	}
	fm.SumImportBatchCount.value = all;
	fm.BatchNo.value = batchno;
	fm.SuccessCount.value = succ;
	var tStrSql = "select batchno,prtno,loginfo,other from xfyjlog where batchno='"
			+ batchno + "'";

	turnPage2.pageDivName = "divImportErrLogGridPage";
	turnPage2.queryModal(tStrSql, ImportErrLogGrid);
	fm.all('Sql').value = tStrSql;
	if (!turnPage2.strQueryResult) {
		alert("没有该批次导入信息！");
		return false;
	} else {

	}
	

	fm.btnImportConfirm.disabled = false;
	fm.btnImportDelete.disabled = false;

	return true;
}

function queryDown() {

	if (ImportErrLogGrid.mulLineCount == 0) {
		alert("没有需要下载的数据");
		return false;
	}
	var batchno = fmImport.BatchNo.value;
	fm.BatchNo.value = batchno;
	fm.action = "./FamilylistDownload.jsp";
	fm.submit();
	fm.action = "";
}

// 内检后动作
function afterSubmit2(FlagStr, Content, cflag) {
	showInfo.close();
	window.focus();

	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

	}
	
	
	queryImportErrLog();
	
}

// 返回
function goBack() {
	window.location.replace("./FamilyTaxImport.jsp");
}

  