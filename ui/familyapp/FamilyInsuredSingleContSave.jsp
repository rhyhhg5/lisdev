
<%--
  保存简易保单信息 2005-11-11 16:31 Yangming
--%>
<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.familyapp.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
            String FlagStr = ""; //操作结果
            String Content = ""; //控制台信息
            String tAction = ""; //操作类型：delete update insert
            String tOperate = ""; //操作代码
            String mLoadFlag = "";
            String upInsuredNo="";
            CErrors tError;
            GlobalInput tG = new GlobalInput();
            tG = (GlobalInput) session.getValue("GI");
            tAction = request.getParameter("fmAction");
            upInsuredNo = request.getParameter("insuredNo");
            //处理被保人
            LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
            try
            {

                tLCInsuredSchema.setInsuredNo(request.getParameter("insured_InsuredNo"));
                tLCInsuredSchema.setPrtNo(request.getParameter("insured_PrtNo"));
                tLCInsuredSchema.setRelationToMainInsured(request.getParameter("insured_RelationToMainInsured"));//朱被保险人(第一被保险人)
                tLCInsuredSchema.setPosition(request.getParameter("insured_Position"));
                tLCInsuredSchema.setSalary(request.getParameter("insured_Salary"));//年收入
                tLCInsuredSchema.setOccupationType(request.getParameter("insured_OccupationType"));
                tLCInsuredSchema.setOccupationCode(request.getParameter("insured_OccupationCode"));
                tLCInsuredSchema.setOperator(request.getParameter(tG.Operator));
                tLCInsuredSchema.setMakeDate(request.getParameter(PubFun.getCurrentDate()));
                tLCInsuredSchema.setMakeTime(request.getParameter(PubFun.getCurrentTime()));
                tLCInsuredSchema.setModifyDate(request.getParameter(PubFun.getCurrentDate()));
                tLCInsuredSchema.setModifyTime(request.getParameter(PubFun.getCurrentTime()));
                tLCInsuredSchema.setName(request.getParameter("insured_Name"));
                tLCInsuredSchema.setSex(request.getParameter("insured_Sex"));
                tLCInsuredSchema.setIDStartDate(request.getParameter("IDStartDate"));
                tLCInsuredSchema.setIDEndDate(request.getParameter("IDEndDate"));
                tLCInsuredSchema.setBirthday(request.getParameter("insured_Birthday"));
                tLCInsuredSchema.setIDType(request.getParameter("insured_IDType"));
                tLCInsuredSchema.setIDNo(request.getParameter("insured_IDNo"));
                tLCInsuredSchema.setRelationToAppnt(request.getParameter("insured_RelationToAppnt"));
                tLCInsuredSchema.setNativePlace(request.getParameter("insured_NativePlace"));
                tLCInsuredSchema.setNativeCity(request.getParameter("insured_NativeCity"));

                //查询被保险人个数赋值保险人顺序
                System.out.println("处理完成被保人信息！");
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                System.out.println("出错！！！");
            }

            LCAddressSchema mInsuredAddressSchema = new LCAddressSchema();
            /**
             * 1、联系地址 PostalAddress
             * 2、邮政编码 ZipCode
             * 3、家庭电话 HomePhone
             * 4、移动电话 Mobile
             * 5、办公电话 CompanyPhone
             * 6、电子邮箱 EMail
             */

            System.out.println("开始处理被保人地址信息");
            try
            {
                mInsuredAddressSchema.setGrpName(request.getParameter("insured_WorkName"));
                mInsuredAddressSchema.setPostalAddress(request.getParameter("insured_PostalAddress"));
                mInsuredAddressSchema.setPostalProvince(request.getParameter("Province"));
                mInsuredAddressSchema.setPostalCity(request.getParameter("City"));
                mInsuredAddressSchema.setPostalCounty(request.getParameter("County"));
                mInsuredAddressSchema.setPostalStreet(request.getParameter("insured_PostalStreet"));
                mInsuredAddressSchema.setPostalCommunity(request.getParameter("insured_PostalCommunity")); 
                mInsuredAddressSchema.setZipCode(request.getParameter("insured_ZipCode"));
                mInsuredAddressSchema.setPhone(request.getParameter("insured_Phone"));
                mInsuredAddressSchema.setHomePhone(request.getParameter("insured_HomePhone"));
                mInsuredAddressSchema.setMobile(request.getParameter("insured_Mobile"));
                mInsuredAddressSchema.setCompanyPhone(request.getParameter("insured_CompanyPhone"));
                mInsuredAddressSchema.setEMail(request.getParameter("insured_EMail"));
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                System.out.println("出错了！");
            }
            System.out.println("处理完成");

            //完成全部的封装，开始递交．
            FamilySingleContInputUI tFamilySingleContInputUI = new FamilySingleContInputUI();
            try
            {
                VData tVData = new VData();
                TransferData tTransferData = new TransferData();
                tTransferData.setNameAndValue("InsuredAddress", mInsuredAddressSchema);
                tTransferData.setNameAndValue("upInsuredno",upInsuredNo);
                tVData.add(tLCInsuredSchema);

                tVData.add(tG);
                tVData.add(tTransferData);
                System.out.println("开始递交数据！" + tAction);
                tFamilySingleContInputUI.submitData(tVData, tAction);
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                Content = "保存失败，原因是:" + ex.toString();
                FlagStr = "Fail";
            }
            if (FlagStr == "")
            {
                tError = tFamilySingleContInputUI.mErrors;
                if (!tError.needDealError())
                {
                    Content = " 保存成功! ";
                    FlagStr = "Success";
                }
                else
                {
                    Content = "保存失败，原因是:" + tError.getFirstError();
                    FlagStr = "Fail";
                }
            }
%>
<html>
    <script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
