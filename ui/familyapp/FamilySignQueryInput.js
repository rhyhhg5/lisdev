//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function signPol()
{
	var tSel = GrpGrid.getSelNo();
	var cPolNo = "";
	fm.all("workType").value = "";
	if( tSel != null && tSel != 0 )
		cPolNo = GrpGrid.getRowColData( tSel - 1, 1 );

	if( cPolNo == null || cPolNo == "" )
		alert("请选择一张集体投保单后，再进行签单操作");
	else
	{
		//校验转账支票是否到账确认
		var prtno = GrpGrid.getRowColData(tSel-1,2);
		if(!isConfirm(prtno))
			return false;
		//校验百万安行产品 意外险平台保单 是否上传扫描件
		prtno = GrpGrid.getRowColData( tSel - 1, 2 );
		if(!valiScanCopy(prtno)){
			//alert("该产品是简易平台银行保险出单的百万安行套餐产品，并未上传扫描件，不能签单！");
			return false;
		}
		
		var i = 0;
		var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.submit(); //提交
	}
}

//提交，预打保单按钮对应操作
function priviewGrpPol()
{
	var tSel = GrpGrid.getSelNo();
	var cPolNo = "";
	fm.all("workType").value = "PREVIEW";
	if( tSel != null && tSel != 0 )
		cPolNo = GrpGrid.getRowColData( tSel - 1, 1 );

	if( cPolNo == null || cPolNo == "" )
		alert("请选择一张集体投保单后，再进行签单操作");
	else
	{
		var i = 0;
		var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.submit(); //提交
	}
}
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	fm.all("workType").value = "";
	if( FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	 	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

		// 刷新查询结果
		easyQueryClick();		
	}
}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

var turnPage = new turnPageClass();  
function easyQueryClick()
{
	// 初始化表格
	initGrpGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select distinct a.missionprop1,a.missionprop2,b.AppntNo,b.AppntName,b.ManageCom,getUniteCode(b.agentcode),a.missionid,a.submissionid from lwmission a,lccont b,ljtempfee c  where 1=1"			 
	       + " and a.processid = '0000000013'"
	       + " and a.activityid = '0000013002' "
	       + " and a.missionprop2=b.prtno "
	       + " and a.MissionProp2=c.otherno"
	       + " and c.othernotype='4' "
	       + " and c.confflag='0' "
	       + " and (c.EnterAccDate is not null) "
	       //2014-8-6添加管理机构条件
	       + " and b.managecom like '"+fm.ManageCom.value+"%'"
				 + getWherePart( 'a.missionprop1','ContNo' )
				 + getWherePart( 'a.missionprop2','PrtNo' )
				 + getWherePart( 'b.AgentCode','AgentCode' )
				 + getWherePart( 'b.AgentGroup','AgentGroup' )
				 + getWherePart( 'b.AppntNo','AppntNo' )
				 + getWherePart( 'b.AppntName','AppntName' )
				 //;+ " and a.MissionProp7 like '" + comcode + "%%'";  //集中权限管理体现	
				 + " order by a.missionprop2";
	turnPage.queryModal(strSQL, GrpGrid);
	if (!turnPage.strQueryResult)
	{
		alert("未查询到满足条件的数据！");
		return false;
	}
}

/*********************************************************************
 *  显示EasyQuery的查询结果
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGrpGrid();
		//HZM 到此修改
		GrpGrid.recordNo = (currBlockIndex - 1) * MAXMEMORYPAGES * MAXSCREENLINES + (currPageIndex - 1) * MAXSCREENLINES;
		GrpGrid.loadMulLine(GrpGrid.arraySave);		
		//HZM 到此修改	
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GrpGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		GrpGrid.delBlankLine();
	} // end of if
}

//发首期交费通知书
function SendFirstNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  var tSel = GrpGrid.getSelNo();
	var cProposalNo = "";
	if( tSel != null && tSel != 0 )
		cProposalNo = GrpGrid.getRowColData( tSel - 1, 1 );
 
  cOtherNoType="01"; //其他号码类型
  cCode="57";        //单据类型
  
  
  if (cProposalNo != "")
  {
  	showModalDialog("../uw/GrpUWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("请先选择保单!");
  }
}

function queryAgent()
{
	if(fm.all('ManageCom').value==""){
		 alert("请先录入管理机构信息！"); 
		 return;
	}
    if(fm.all('GroupAgentCode').value == "")	{  
	  //var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+",AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	  var newWindow = window.open("../sys/AgentCommonQueryMain.jsp?ManageCom="+fm.all('ManageCom').value+"&SaleChnl=all","AgentCommonQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');	  
	  }
	if(fm.all('GroupAgentCode').value != "")	 {

	var cAgentCode = fm.GroupAgentCode.value;  //保单号码	
	var strSql = "select GroupAgentCode,Name,AgentGroup,AgentCode from LAAgent where GroupAgentCode='" + cAgentCode +"' and ManageCom = '"+fm.all('ManageCom').value+"'";
    var arrResult = easyExecSql(strSql);
       //alert(arrResult);
    if (arrResult != null) {
      fm.AgentName.value = arrResult[0][1];
      fm.AgentGroup.value = arrResult[0][2];
      fm.AgentCode.value= arrResult[0][3];
      alert("查询结果:  代理人编码:["+arrResult[0][0]+"] 代理人名称为:["+arrResult[0][1]+"]");
    }
    else{
     fm.AgentGroup.value="";
     fm.AgentCode.value="";
     alert("编码为:["+fm.all('GroupAgentCode').value+"]的代理人不存在，请确认!");
     }
	}	
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery2(arrResult)
{  
  if(arrResult!=null)
  {
  	fm.AgentCode.value = arrResult[0][0];
  	fm.AgentGroup.value = arrResult[0][1];fm.all('AgentGroupName').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrResult[0][1]+"'");
  	fm.GroupAgentCode.value = arrResult[0][95];
  }
}
//百万安行产品 意外险平台保单 无扫描件不能签单
function valiScanCopy(prtno){

    var valiRiskSQL = "select 1 from lcriskdutywrap where prtno = '"+prtno+"' and riskwrapcode in ( select code from ldcode where codetype = 'valiscancopy' and othersign = '1' ) with ur";
	var RiskResult = easyExecSql(valiRiskSQL);
	if(RiskResult!=null){
		valiSQL = "select 1 from es_doc_main where doccode = '"+ prtno +"' with ur";
		 var arrResult = easyExecSql(valiSQL);
	     if(arrResult == null){ 
	        alert("该产品并未上传扫描件，不能签单！");
	    	return false;
	     }
	  }
    
	
     return true;
}


//校验转账支票是否已经到账确认
function isConfirm(prtno) {

	var valSQL = "select 1 as result from ljtempfeeclass a left join ljtempfee b on a.tempfeeno = b.tempfeeno  where  b.otherno = '"
			+ prtno
			+ "' and a.paymode in ('2','3') and a.enteraccdate is null with ur ";

	var result = easyExecSql(valSQL);

	if (result != null) {
		alert("该保单的收费方式是'转账支票'，尚未进行到账确认，不能进行签单");
		return false;
	}
	 return true;
}

function initAgentC()
{  
  if(fm.all('GroupAgentCode').value == "")
  {
  	fm.AgentCode.value = "";
  	fm.AgentGroup.value = "";
  }
}