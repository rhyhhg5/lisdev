<%
//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script src="../common/javascript/Common.js"></script>

<%
	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	String strManageCom = tGI.ComCode;
	String strBatchNo = request.getParameter("BatchNo");
%>

<script language="JavaScript">
	var mBatchNo = "<%=strBatchNo%>";

function initInpBox(){
    try{
        fmImport.BatchNo.value = mBatchNo;
        fm.BatchNo.value = mBatchNo;
    }catch(ex){
        alert("初始化界面错误!");
    }
}

function initForm(){
    try{
        initInpBox();
        initImportErrLogGrid();        
        fm.ImportErrBatchNo.value = fmImport.BatchNo.value;
 
        
        
    }catch(e){
        alert("InitForm函数中发生异常:初始化界面错误!");
    }
}

/**
 * 导入批次错误日志别表。
 */
function initImportErrLogGrid(){
	
    var iArray = new Array();

    try{
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=3;
        
        iArray[1]=new Array();
        iArray[1][0]="批次号";
        iArray[1][1]="120px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="印刷号";
        iArray[2][1]="60px";
        iArray[2][2]=60;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="日志信息";
        iArray[3][1]="150px";
        iArray[3][2]=150;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="状态";
        iArray[4][1]="120px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        ImportErrLogGrid = new MulLineEnter("fm", "ImportErrLogGrid"); 

        ImportErrLogGrid.mulLineCount = 0;   
        ImportErrLogGrid.displayTitle = 1;
        ImportErrLogGrid.canSel = 0;
        ImportErrLogGrid.hiddenSubtraction = 1;
        ImportErrLogGrid.hiddenPlus = 1;
        ImportErrLogGrid.canChk = 0;
        ImportErrLogGrid.loadMulLine(iArray);
    }catch(ex){
        alert("初始化ImportErrLogGrid时出错：" + ex);
    }
}
</script>