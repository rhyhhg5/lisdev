<%
//程序名称：PhoneInterviewUpdateInit.jsp
//程序功能:
//创建日期：2005-04-02
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="javascript">
var turnPage = new turnPageClass();

var DetailWorkNo = "<%=request.getParameter("DetailWorkNo")%>";
<%
	session.setAttribute("DetailWorkNo", request.getParameter("DetailWorkNo"));
%>
function initForm()
{
	try
	{
		initInfo();
		initLCPolInfo();
		setContInfoForInterview(fm.contNo.value);
	}
	catch(re)
	{
		alert("InitForm函数中发生异常:初始化界面错误!");
	}
}

//初始化工单信息
function initInfo()
{
	fm.phoneWorkNo.value = DetailWorkNo;
	fm.LoadFlag.value = "<%=request.getParameter("LoadFlag")%>";
		
	var strSql = "select * " +
			 		"from LGPhoneAnwser " +
			 		"where phoneWorkNo= '" + DetailWorkNo + "' " +
			 		"order by phoneItemNo";
	var arrResult = easyExecSql(strSql);
	
	if (!arrResult)
	{
		//alert("没有找到符合条件的记录!");
		//如果之前没有回访任务则新增
		fm.all("fmtransact").value = "INSERT||MAIN";
		
		//得到业务类型
		strSql = "select TypeNo " +
			 		"from LGWork " +
			 		"where WorkNo= '" + DetailWorkNo + "' ";
		arrResult = easyExecSql(strSql);
		if (arrResult)
		{
			fm.TypeNo.value = arrResult[0][0];
		}
		return false;
	}
	
	fm.contNo.value = arrResult[0][10];
	
	var answer;
	var remark;
	var item;
	var checkedFlag;
	
	for(var k = 0; k < arrResult.length; k++)
	{
		//设置第k个问题的答案
		item = arrResult[k][1];	//答案号
		answer = "answer";
		answer = answer + item;
		checkedFlag = arrResult[k][3];
		if(checkedFlag == "0" || checkedFlag == "1" || checkedFlag == "2")
		{
			fm.all(answer)[checkedFlag].checked = true; 
		}
		
		//设置第k个问题的备注
		remark = "remark";
		remark = remark + item;
		fm.all(remark).value = arrResult[k][4];
		
		//显示该问题
		var div = "div" + item;
		document.all(div).style.display = "";
	}

	return true;
}

function initLCPolInfo()
{
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[0][21]="index";
      
      iArray[1]=new Array();
      iArray[1][0]="被保人";         	  //列名
      iArray[1][1]="30px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="insuredName";
      
      iArray[2]=new Array();
      iArray[2][0]="险种代码";         	  //列名
      iArray[2][1]="30px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="riskCode";
      
      iArray[3]=new Array();
      iArray[3][0]="险种名称";         	  //列名
      iArray[3][1]="120px";            	//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="riskName";
      
      iArray[4]=new Array();
      iArray[4][0]="期交保费";          //列名
      iArray[4][1]="30px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[4][21]="prem";
      
      iArray[5]=new Array();
      iArray[5][0]="缴费年期";         	//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[5][21]="payYears";
    
      LCPolGrid = new MulLineEnter("fm", "LCPolGrid"); 
      //设置Grid属性
      LCPolGrid.mulLineCount = 0;
      LCPolGrid.displayTitle = 1;
      LCPolGrid.locked = 1;
      LCPolGrid.canSel = 0;
      LCPolGrid.canChk = 0;
      LCPolGrid.hiddenSubtraction = 1;
      LCPolGrid.hiddenPlus = 1;
      LCPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}
</script>