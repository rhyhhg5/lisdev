<%
//程序名称：TaskCommonContInit.jsp
//程序功能：工单管理客服信息初始化页面
//创建日期：2005-03-20 15:20:36
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<SCRIPT>
var turnPage5 = new turnPageClass();
var turnPage6 = new turnPageClass();

// 个单信息列表的初始化
function initContGrid()
{                         
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保单号";         	  //列名
      iArray[1][1]="120px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="印刷号";         	  //列名
      iArray[2][1]="120px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="投保人";         	  //列名
      iArray[3][1]="60px";            	//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="主被保人";          //列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="投保日期";         	//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[6]=new Array();
      iArray[6][0]="截止日期";         	//列名
      iArray[6][1]="80px";            	//列宽
      iArray[6][2]=200;            		  //列最大值
      iArray[6][3]=0;              		  //是否允许输入,1表示允许，0表示不允许 
      
      iArray[7]=new Array();
      iArray[7][0]="交至日期";         	//列名
      iArray[7][1]="80px";            	//列宽
      iArray[7][2]=200;            		  //列最大值
      iArray[7][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="保费";         	//列名
      iArray[8][1]="80px";            	//列宽
      iArray[8][2]=200;            		  //列最大值
      iArray[8][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="保额";         	//列名
      iArray[9][1]="80px";            	//列宽
      iArray[9][2]=200;            		  //列最大值
      iArray[9][3]=3;              		  //是否允许输入,1表示允许，0表示不允许        
      
      iArray[10]=new Array();
      iArray[10][0]="保单状态";         		  //列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="保单联系地址";         		  //列名
      iArray[11][1]="190px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
			iArray[12]=new Array();       
			iArray[12][0]="已回访"; 
			iArray[12][1]="50px";        
			iArray[12][2]=200;            
			iArray[12][3]=0;              

      ContGrid = new MulLineEnter("fm", "ContGrid"); 
      //设置Grid属性
      ContGrid.mulLineCount = 0;
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      ContGrid.canSel = 1;
      ContGrid.canChk = 0;
      ContGrid.hiddenSubtraction = 1;
      ContGrid.hiddenPlus = 1;
	  //ScanGrid.selBoxEventFuncName = "showPic";
      ContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
  
  queryContInfo();
}

//团单信息初始化
function initGrpContGrid()
{                         
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保单号";         	  //列名
      iArray[1][1]="120px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="印刷号";         	  //列名
      iArray[2][1]="120px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="投保单位";         	  //列名
      iArray[3][1]="60px";            	//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="参保人数";          //列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="申请日期";         	//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[6]=new Array();
      iArray[6][0]="生效日期";         	//列名
      iArray[6][1]="80px";            	//列宽
      iArray[6][2]=200;            		  //列最大值
      iArray[6][3]=0;              		  //是否允许输入,1表示允许，0表示不允许 
      
      iArray[7]=new Array();
      iArray[7][0]="交费方式";         	//列名
      iArray[7][1]="80px";            	//列宽
      iArray[7][2]=200;            		  //列最大值
      iArray[7][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="总保费";         	//列名
      iArray[8][1]="80px";            	//列宽
      iArray[8][2]=200;            		  //列最大值
      iArray[8][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="总保额";         	//列名
      iArray[9][1]="80px";            	//列宽
      iArray[9][2]=200;            		  //列最大值
      iArray[9][3]=3;              		  //是否允许输入,1表示允许，0表示不允许        
      
      iArray[10]=new Array();
      iArray[10][0]="保单状态";         		  //列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="保单联系地址";         		  //列名
      iArray[11][1]="190px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      GrpContGrid = new MulLineEnter("fm", "GrpContGrid"); 
      //设置Grid属性
      GrpContGrid.mulLineCount = 0;
      GrpContGrid.displayTitle = 1;
      GrpContGrid.locked = 1;
      GrpContGrid.canSel = 1;
      GrpContGrid.canChk = 0;
      GrpContGrid.hiddenSubtraction = 1;
      GrpContGrid.hiddenPlus = 1;
      GrpContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
  qureyGrpContInfo();
}
</SCRIPT>