<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：Appeal.jsp
//程序功能：投诉录入页面
//创建日期：2005-03-28
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <%@include file="AppealInit.jsp"%>
	<SCRIPT src="Appeal.js"></SCRIPT>
	
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>投诉录入 </title>
</head>
<body onload="initForm();">
  <form action="AppealSave.jsp" method=post name=fm target="fraSubmit"> 		
		<table>
			<tr id="forButton" style="display: none">
    		<td class=common >
    			<input type="button" value="保  存" class=cssButton onclick="submitForm(WorkNo, '');">
    		</td>
    	</tr>
    	<tr>
     	 	<td  class=common>
     	 		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAppealSource)">
      	</td>
    		<td class= titleImg>投诉来源&nbsp;</td>
    	</tr>
  	</table>
  	<Div id="divAppealSource" style="display: ''"> 
    	<table class=common>
   			<tr>
   				<td class=input colspan=6>   					
   					<input type=radio name="AppealWayNo" value="0">电话
   					<input type=radio name="AppealWayNo" value="1">亲访
   					<input type=radio name="AppealWayNo" value="2">代理人反馈
   					<input type=radio name="AppealWayNo" value="3">信函
   					<input type=radio name="AppealWayNo" value="4">传真
   					<input type=radio name="AppealWayNo" value="5">网络媒体或消协转办
   					<input type=radio name="AppealWayNo" value="6">员工反馈
   				</td>
   	    </tr>
	  	</table>
		</div>
		<table>
    	<tr>
     	 	<td  class=common>
     	 		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAcceptor)">
      	</td>
    		<td class= titleImg>投诉受理人&nbsp;</td>
    	</tr>
  	</table>
  	<div id="divAcceptor" style="display: ''">
			<table class=common>
  	    <tr class=common>
  	    	<td class=input>姓名</td>
  	    	<TD class=input><input class="common" name="AcceptorName" elementtype="nacessary"></TD>
  	    	<td class=input>编号</td>
  	    	<TD class= input><input class="common" name="AcceptorNo"></TD>
          <td class=input>岗位</td>
          <TD class= input><input class="common" name="AcceptorPost"></TD>
  	    </tr>
			</table>
  	</div>
  	<table>
    	<tr>
     	 	<td  class=common>
     	 		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAppealedInfo)">
      	</td>
    		<td class= titleImg>投诉对象资料&nbsp;</td>
    	</tr>
  	</table>
  	<div id="divAppealedInfo" style="display: ''">
	<table class=common>
  	    <tr class=common>
  	    	<td class=input>姓名</td>
  	    	<td class=input> 
  	    		<input class="common" name="AppealObjName" elementtype="nacessary">
  	    	</td>
  	    	<td class=input>编号</td>
  	    	<td class=input><input class=common name="AppealObjNo"></td>
  	    	<td class=input>联系电话</td>
  	    	<td class=input><input class=common name="AppealObjPhone"></td>
  	    </tr>
  	    <tr class=common>
  	    	<td class=input>所属部门</td>
  	    	<td class=input>
  	    		<input class=code name="AppealObjDep" type=hidden>
  	    		<input class=code name="AppealObjDepName" ondblclick="return showCodeList('AcceptCom', [this, AppealObjDep], [1, 0]);" onkeyup="return ('AcceptCom', [this, AppealObjDep], [1,0]);" verify="所属部门|&code:AcceptCom">
  	    	</td>
  	    	<td class=input>上级经理</td>
  	    	<td class="code"><input class=common name="DirectBoss" ></td>
  	    </tr>
	</table>
  	</div>
  	<table>
    	<tr>
     	 	<td  class=common>
     	 		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAppealContent)">
      	</td>
    		<td class= titleImg>投诉内容&nbsp;</td>
    	</tr>
  	</table>
  	<div id="divAppealContent" style="display: ''">
			<table class=common>
  	    <tr class=common>
  	    	<td class=input>
  	    		<textarea class="common" name="AppealContent" cols="100%" rows="3"></textarea>
  	    	</td>
  	    </tr>
			</table>
  	</div>
		<table>
    	<tr>
     	 	<td  class=common>
     	 		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAppealDeal)">
      	</td>
    		<td class= titleImg>投诉处理&nbsp;</td>
    	</tr>
  	</table>
		<div id="divAppealDeal" style="display: ''">
			<table class=common>
  	    <tr class=common>
  	    	<td class="input" colspan="6">
  	    		<textarea class="common" name="AppealFeeback" cols="100%" rows="3"></textarea>
  	    	</td>
  	    </tr>
  	  </table>
  	</div>
  	<table id="dealOPTable" style="display: none">
    	<tr>
     	 	<td  class=common>
     	 		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divAppealIdea)">
      	</td>
    		<td class= titleImg>处理意见&nbsp;</td>
    	</tr>
  	</table>
  	<div id="divAppealIdea" style="display: none">
  		<table>  		
  	    <tr class=common>
  	    	<td class="input" colspan="6">
  	    		<textarea class="common" name="AppealDealOP" cols="100%" rows="3"></textarea>
  	    	</td>
  	    </tr>
			</table>
		</div>
		<table>
    	<tr>
     	 	<td  class=common>
     	 		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCustomerAttitude)">
      	</td>
    		<td class= titleImg>客户对处理所持态度&nbsp;</td>
    	</tr>
  	</table>
  	<div id="divCustomerAttitude" style="display: ''">
  		<table>  		
  	    <tr class=common>
  	    	<td class="input" colspan="6">
  	    		<textarea class="common" name="CustomerOpinion" cols="100%" rows="3"></textarea>
  	    	</td>
  	    </tr>
  	  	<tr class=common>
  	    	<td class=common colspan="6">再处理？  	    		
  	    		<input type="radio" name="DealTwiseFlag" value="0">是
  	    		<input type="radio" name="DealTwiseFlag" value="1">否
  	    	</td>
  	    </tr>
			</table>
		</div>
		<table>
    	<tr>
     	 	<td  class=common>
     	 		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divChargeAttitude)">
      	</td>
    		<td class= titleImg>客户服务主管审阅意见：&nbsp;</td>
    	</tr>
  	</table>
  	<div id="divChargeAttitude" style="display: ''">
  		<table>  		
  	    <tr class=common>
  	    	<td class="input" colspan="6">
  	    		<textarea class="common" name="ServiceMangerOpinion" cols="100%" rows="3"></textarea>
  	    	</td>
  	    </tr>
			</table>
		</div>
		<input type=hidden name="fmtransact" value="">
		<input type=hidden name="WorkNo" value="">
  	<input type=hidden name="TypeNo" value="">
  	<input type=hidden name="WorkBoxNo" value="">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
