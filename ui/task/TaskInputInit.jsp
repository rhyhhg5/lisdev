<%
//程序名称：TaskInputInit.jsp
//程序功能：工单管理咨询信息录入页面初始化
//创建日期：2005-02-21 16:14:56
//创建人  ：qiuyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput) session.getValue("GI");	
  String workNo = request.getParameter("AcceptNo");
  String scanBeforeInput = request.getParameter("ScanBeforeInput");
%>       
<script language="JavaScript">
var operator = "<%=tGI.Operator%>";  //操作员编号
var subTypeNoCondition = "";  //查询子业务类型的条件

//表单初始化
function initForm()
{
	try
	{
		initCustomerGrid();
		initContGrid();
		initGrpContGrid();
		initInpBox();
		initElementtype();
		
		showCodeName();
		afterCodeSelect("TaskTopTypeNo", "TopTypeNo");
	}
	catch(re)
	{
		//alert("InitForm函数中发生异常:初始化界面错误!");
		alert("操作错误，请关闭上次登陆打开的页面后重试。");
	}
}

//初始化输入框
function initInpBox()
{
	
	//查询登陆用户所属部门的业务类型
	var sql = 	"select worktypeno " +
				"from LGGroup " +
				"where groupno in " +
					"(select groupNo " +
					"from LGGroupMember " +
					"where memberno='" + operator + "')";
	var topTypeNo = easyExecSql(sql);
	
	//工单信息
	fm.all("CustomerNo").value = "";
	//fm.all("ContNo").value = "";
	fm.all("IDNo").value = "";
	fm.all("PriorityNo").value = "";
	fm.all("TopTypeNo").value = topTypeNo;
	fm.all("TopTypeNoname").value = topTypeNo;	
	fm.all("TypeNo").value = "";
	fm.all("DateLimit").value = "";
	fm.all("ApplyTypeNo").value = "";
	fm.all("ApplyName").value = "";
	fm.all("AcceptWayNo").value = "";
	fm.all("AcceptDate").value = getCurrentDate();
	fm.all("Remark").value = "";
	
	fm.ScanBeforeInput.value = "<%=scanBeforeInput%>";
	fm.WorkNoEdit.value = "<%=workNo%>";
	
	sql = "  select AcceptCom, AcceptorNo "
	      + "from LGWork "
	      + "where workNo = '<%=workNo%>'";
	var result = easyExecSql(sql);
	if(result)
	{
	  fm.AcceptCom.value = result[0][0];
	  fm.AcceptorNo.value = result[0][1];
	}
	
	subTypeNoCondition = topTypeNo;
}

// 保单信息列表的初始化
function initCustomerGrid()
{                         
  var iArray = new Array();
  try
  {
			iArray[0]=new Array();
			iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1]="0px";            		//列宽
			iArray[0][2]=200;            			//列最大值
			iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[1]=new Array();
			iArray[1][0]="作业号";         	  //列名
			iArray[1][1]="80px";            	//列宽
			iArray[1][2]=200;            			//列最大值
			iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[2]=new Array();
			iArray[2][0]="受理号";         	  //列名
			iArray[2][1]="120px";            	//列宽
			iArray[2][2]=200;            			//列最大值
			iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[3]=new Array();
			iArray[3][0]="优先级";          //列名
			iArray[3][1]="60px";            		//列宽
			iArray[3][2]=200;            			//列最大值
			iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
			iArray[3][4]="PriorityNo";
			iArray[3][5]="3";              	                //引用代码对应第几列，'|'为分割符
			iArray[3][9]="优先级|code:PriorityNo&NOTNULL";
			iArray[3][18]=250;
			iArray[3][19]= 0 ;
			
			iArray[4]=new Array();
			iArray[4][0]="业务类型";         	//列名
			iArray[4][1]="100px";            		//列宽
			iArray[4][2]=200;            			//列最大值
			iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
			iArray[4][4]="TaskTypeNo";
			iArray[4][5]="3";              	                //引用代码对应第几列，'|'为分割符
			iArray[4][9]="业务类型|code:TaskTypeNo&NOTNULL";
			iArray[4][18]=250;
			iArray[4][19]= 0 ;
			
			iArray[5]=new Array();
			iArray[5][0]="客户号";         	//列名
			iArray[5][1]="80px";            	//列宽
			iArray[5][2]=200;            		  //列最大值
			iArray[5][3]=3;              		  //是否允许输入,1表示允许，0表示不允许 
			
			iArray[6]=new Array();
			iArray[6][0]="姓名";         	//列名
			iArray[6][1]="80px";            	//列宽
			iArray[6][2]=200;            		  //列最大值
			iArray[6][3]=3;              		  //是否允许输入,1表示允许，0表示不允许
			
			iArray[7]=new Array();
			iArray[7][0]="记录";         	//列名
			iArray[7][1]="80px";            	//列宽
			iArray[7][2]=200;            		  //列最大值
			iArray[7][3]=3;              		  //是否允许输入,1表示允许，0表示不允许
			
			iArray[8]=new Array();
			iArray[8][0]="经办人";         	//列名
			iArray[8][1]="60px";            	//列宽
			iArray[8][2]=200;            		  //列最大值
			iArray[8][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
			iArray[8][4]="TaskMemberNo";
			iArray[8][5]="3";              	                //引用代码对应第几列，'|'为分割符
			iArray[8][9]="经办人|code:TaskMemberNo&NOTNULL";
			iArray[8][18]=250;
			iArray[8][19]= 0 ;           
			
			iArray[9]=new Array();
			iArray[9][0]="经办日期";              //列名
			iArray[9][1]="120px";            	//列宽
			iArray[9][2]=200;            			//列最大值
			iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[10]=new Array();
			iArray[10][0]="受理机构";         		  //列名
			iArray[10][1]="150px";            		//列宽
			iArray[10][2]=200;            			//列最大值
			iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			iArray[10][4]="AcceptCom";
			iArray[10][5]="3";              	                //引用代码对应第几列，'|'为分割符
			iArray[10][9]="受理机构|code:AcceptCom";
			iArray[10][18]=250;
			iArray[10][19]= 0 ;
			
			iArray[11]=new Array();
			iArray[11][0]="时间限制";         		  //列名
			iArray[11][1]="60px";            	  //列宽
			iArray[11][2]=200;            		  //列最大值
			iArray[11][3]=0;              		  //是否允许输入,1表示允许，0表示不允许  
			
			iArray[12]=new Array();
			iArray[12][0]="处理状态";         		  //列名
			iArray[12][1]="60px";            		//列宽
			iArray[12][2]=200;            			//列最大值
			iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			iArray[12][4]="TaskStatusNo";
			iArray[12][5]="3";              	                //引用代码对应第几列，'|'为分割符
			iArray[12][9]="处理状态|code:TaskStatusNo&NOTNULL";
			iArray[12][18]=250;
			iArray[12][19]= 0 ;
			
			iArray[13]=new Array();
			iArray[13][0]="批注";         	  //列名
			iArray[13][1]="40px";            	//列宽
			iArray[13][2]=200;            			//列最大值
			iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			iArray[14]=new Array();
			iArray[14][0]="扫描";         	  //列名
			iArray[14][1]="40px";            	//列宽
			iArray[14][2]=200;            			//列最大值
			iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
			CustomerGrid = new MulLineEnter("fm", "CustomerGrid"); 
			//设置Grid属性
			CustomerGrid.mulLineCount = 0;
			CustomerGrid.displayTitle = 1;
			CustomerGrid.locked = 1;
			CustomerGrid.canSel = 1;
			CustomerGrid.canChk = 0;
			CustomerGrid.hiddenSubtraction = 1;
			CustomerGrid.hiddenPlus = 1;
			CustomerGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}

// 个单信息列表的初始化
function initContGrid()
{                         
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保单号";         	  //列名
      iArray[1][1]="120px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="contNo";
      
      iArray[2]=new Array();
      iArray[2][0]="印刷号";         	  //列名
      iArray[2][1]="120px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="投保人";         	  //列名
      iArray[3][1]="60px";            	//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="主被保人";          //列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="投保日期";         	//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[6]=new Array();
      iArray[6][0]="截止日期";         	//列名
      iArray[6][1]="80px";            	//列宽
      iArray[6][2]=200;            		  //列最大值
      iArray[6][3]=0;              		  //是否允许输入,1表示允许，0表示不允许 
      
      iArray[7]=new Array();
      iArray[7][0]="交至日期";         	//列名
      iArray[7][1]="80px";            	//列宽
      iArray[7][2]=200;            		  //列最大值
      iArray[7][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="保费";         	//列名
      iArray[8][1]="80px";            	//列宽
      iArray[8][2]=200;            		  //列最大值
      iArray[8][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="保额";         	//列名
      iArray[9][1]="80px";            	//列宽
      iArray[9][2]=200;            		  //列最大值
      iArray[9][3]=3;              		  //是否允许输入,1表示允许，0表示不允许        
      
      iArray[10]=new Array();
      iArray[10][0]="保单状态";         		  //列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][21] = "PolState";
      
      iArray[11]=new Array();
      iArray[11][0]="保单联系地址";         		  //列名
      iArray[11][1]="190px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
			iArray[12]=new Array();       
			iArray[12][0]="已回访"; 
			iArray[12][1]="50px";        
			iArray[12][2]=200;            
			iArray[12][3]=0;  
			

      ContGrid = new MulLineEnter("fm", "ContGrid"); 
      //设置Grid属性
      ContGrid.mulLineCount = 0;
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      ContGrid.canSel = 1;
      ContGrid.canChk = 0;
      ContGrid.hiddenSubtraction = 1;
      ContGrid.hiddenPlus = 1;
	    ContGrid.selBoxEventFuncName = "setContInfoForInterview";
      ContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}

//团单信息初始化
function initGrpContGrid()
{
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="保单号";         	  //列名
      iArray[1][1]="120px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="GrpContNo";
      
      iArray[2]=new Array();
      iArray[2][0]="印刷号";         	  //列名
      iArray[2][1]="120px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="投保单位";         	  //列名
      iArray[3][1]="60px";            	//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="参保人数";          //列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="申请日期";         	//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[6]=new Array();
      iArray[6][0]="生效日期";         	//列名
      iArray[6][1]="80px";            	//列宽
      iArray[6][2]=200;            		  //列最大值
      iArray[6][3]=0;              		  //是否允许输入,1表示允许，0表示不允许 
      
      iArray[7]=new Array();
      iArray[7][0]="交费方式";         	//列名
      iArray[7][1]="80px";            	//列宽
      iArray[7][2]=200;            		  //列最大值
      iArray[7][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="总保费";         	//列名
      iArray[8][1]="80px";            	//列宽
      iArray[8][2]=200;            		  //列最大值
      iArray[8][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="总保额";         	//列名
      iArray[9][1]="80px";            	//列宽
      iArray[9][2]=200;            		  //列最大值
      iArray[9][3]=3;              		  //是否允许输入,1表示允许，0表示不允许        
      
      iArray[10]=new Array();
      iArray[10][0]="保单状态";         		  //列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[10][21] = "PolStateGrp";
      
      iArray[11]=new Array();
      iArray[11][0]="保单联系地址";         		  //列名
      iArray[11][1]="190px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="保单性质";         		  //列名
      iArray[12][1]="55px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=0; 
    
      GrpContGrid = new MulLineEnter("fm", "GrpContGrid"); 
      //设置Grid属性
      GrpContGrid.mulLineCount = 0;
      GrpContGrid.displayTitle = 1;
      GrpContGrid.locked = 1;
      GrpContGrid.canSel = 1;
      GrpContGrid.canChk = 0;
      GrpContGrid.hiddenSubtraction = 1;
      GrpContGrid.hiddenPlus = 1;
      GrpContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}

</script>