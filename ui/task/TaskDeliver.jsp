<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskDeleiver.jsp
//程序功能：工单管理工单转交页面
//创建日期：2005-01-17
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="TaskDeliver.js"></SCRIPT>
  <%@include file="TaskDeliverInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单转交</title>
</head>
<body onload="initForm();" >
<% 
   if (WorkNo.length == 1) 
   {
%>
  <form action="" name="fm" target=fraSubmit method=post>
  <%@include file="TaskCommonView.jsp"%>
  </form>
<% 
  }
%>
  <form action="./TaskDeliverSave.jsp" method=post name=fm2 target="fraSubmit">
  	<input type="hidden" class= common name="DeliverType">
    <table>
      <tr>
        <td class=common>
	      	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskSend);">
	      </td>
        <td class= titleImg>工单转交&nbsp;</td>
        <td class=common>
        <%if (WorkNo.length != 1) 
          {
        	out.print("(工单数:"+WorkNo.length+")");
          }
        %>
        </td>
	   </tr>
    </table>
  <Div id="divTaskSend" style="display: '' ">
    <table class= common>
      <tr>
        <td class= title> 转交信箱 </td>
        <td class= input> 
        	<Input class= common name="WorkBoxNo" elementtype="nacessary" verify="转交信箱|notnull" >
        	<input type="button" Class="cssButton" name="search" value="查  询" onclick="queryBoxNo();">
        </td>
        <td class= title> 是否复制 </td>
        <td class= input>
        	<input type="radio" name="CopyFlag" value="Y" onclick="selCopy();">是</input>
 			<input type="radio" name="CopyFlag" value="N" onclick="selNotCopy();" checked>否</input>
        </td>
      </tr>
<%
   if (WorkNo.length == 1) 
   {
%>
      <tr class= common id="hideInput" style="display: 'none'">
        <TD class= title> 新业务类型 </TD>
        <TD class= input>
        	<input class="code" name="TopTypeNo" type="hidden">
        	<input class="code" name="TopTypeNoname" readonly elementtype="nacessary" ondblclick="return showCodeList('TaskTopTypeNo', [this,TopTypeNo], [1,0]);" onkeyup="return showCodeListKey('TaskTopTypeNo', [this,TopTypeNo],[1,0]);" verify="业务类型|notnull&code:TaskTopTypeNo">
        </TD> 
        <TD class= title> 新子业务类型 </TD>
        <TD class= input>
        	<input class="code" name="TypeNo" type="hidden">
        	<!--input class="code" name="TypeNoname" readonly ondblclick="return showCodeList('TaskTypeNo', [this,TypeNo], [1,0], null, fm2.all('TopTypeNo').value, 'SuperTypeNo', 1);" onkeyup="return showCodeListKey('TaskTypeNo', [this,TypeNo], [1,0], null, fm2.all('TopTypeNo').value, 'SuperTypeNo', 1);"-->
        	<input class="code" name="TypeNoname" readonly ondblclick="return showCodeList('TaskTypeNo', [this,TypeNo], [1,0], null, fm2.subTypeNoCondition.value, 'SuperTypeNo', '1');" onkeyup="return showCodeListKey('TaskTypeNo', [this,TypeNo], [1,0], null, fm2.subTypeNoCondition.value, 'SuperTypeNo', '1');">
        </TD> 
      </tr>
<%
   }
%>
      <tr class= common> 
        <td  class= title> 批注 </td>
        <td  class= input colspan="3">
		<textarea class= common name="RemarkContent" cols="118%" rows="3"></textarea> 
        </td>
      </tr>
    </table>
    	<input type="button" class=cssButton name="Send" value="转  交" onclick="setDeliverType(0); submitForm();">&nbsp;
	<!--	<input type="button" class=cssButton name="Review" value="送审批" onclick="setDeliverType(1); submitForm();">&nbsp;-->
		<input type="button" class=cssButton name="Return" value="返  回" onclick="parent.window.close();">
	 </div>
	 <input type=hidden name = "subTypeNoCondition">
	 <input type=hidden name="loadFlag" value="">
	 <input type=hidden name="WorkNo" value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
