<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskChangeStatus.jsp
//程序功能：工单管理转为待办数据保存页面
//创建日期：2005-01-19 20:14:18
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	
	//输入参数
	VData tVData = new VData();
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	
	tLGWorkSchema.setWorkNo(request.getParameter("WorkNo"));
	tLGWorkSchema.setStatusNo(request.getParameter("StatusNo"));
	tLGWorkSchema.setCurrDoing(request.getParameter("CurrDoing"));
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");	

	tVData.add(tLGWorkSchema);
	tVData.add(tGI);
	
	TaskChangeStatusBL tTaskChangeStatusBL = new TaskChangeStatusBL();
	if (tTaskChangeStatusBL.submitData(tVData, "ok") == false)
	{
		FlagStr = "Fail";
	}
	else 
	{
		Content = "工单状态改变成功";
	}
	//设置显示信息
	VData tRet = tTaskChangeStatusBL.getResult();
	
	String loadFlag = request.getParameter("loadFlag");
%> 
<html>
<script language="javascript">
  if("<%=loadFlag%>" == "TASKVIEWTOPBUTTON")
  {
    parent.fraMenu.afterSubmit("<%=FlagStr%>","<%=Content%>");
  }
  else
  {
	  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  }
</script>
</html>

