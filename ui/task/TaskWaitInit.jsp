<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：TaskWaitInit.jsp
//程序功能：工单管理转为代办页面初始化
//创建日期：2005-01-24 14:47:34
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
	
	System.out.println("\n\n\n转为待办时间: " + PubFun.getCurrentDate() + " "
	  + PubFun.getCurrentTime() + ", 操作人" + tGI.Operator);

	//如果有复选框用复选框传参数,否则用隐藏域传参数
	//用CheckBox来判断是否是从checkbox中接收参数
  String CheckBox = request.getParameter("CheckBox");
  String tWorkNo = request.getParameter("WorkNo");
  String xWorkNo = "";
  String[] WorkNo;
  
  if ((CheckBox != null) && (CheckBox.equals("YES")))
  {
  	xWorkNo = request.getParameter("mWorkNo");
  	WorkNo = xWorkNo.split("a");
  }
  else if ((tWorkNo != null) && (!tWorkNo.equals("")))
	{
		WorkNo = new String[1];
		WorkNo[0] = tWorkNo;
	}
	else
	{
		WorkNo = new String[1];
		WorkNo[0] = (String) session.getAttribute("VIEWWORKNO");
	}
	
	if (WorkNo != null)
	{
		session.setAttribute("WORKNO", WorkNo);
	}
%>
<script language="JavaScript">
var WorkNo = "<%=WorkNo[0]%>";

//初始化表单
function initForm()
{
	initElementtype();
	<%
	  //只有一条记录时显示记录
  	  if (WorkNo.length == 1) 
      {
	%>
	initTaskInfo();	
	<%
	  }
	%>
}
</script>