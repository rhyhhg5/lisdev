 <%
//程序名称：TaskEasyEdor.jsp
//程序功能：初始化页面信息
//创建日期：2005-04-10
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="javascript">	
var turnPage = new turnPageClass();
var mCustomerNo = "<%=request.getParameter("CustomerNo")%>";

//初始化页面
function initForm()
{
	try
	{
		initTaskInfo();
		queryCustomerInfo();
		initContGrid();
		queryContGrid();
		queryAddressInfo();
		initLCContGrid();
		queryLCCont();
		showCodeName();
		showAllCodeName();
	}
	catch(ex)
	{
		alert("在TaskEasyEdorInit.jsp-->initForm()中发生异常" + ex);
	}
}

//初始化工单信息，由前一页面带入
function initTaskInfo()
{
	fm.CustomerNo.value = "<%=request.getParameter("CustomerNo")%>";
	fm.IDNo.value = "<%=request.getParameter("IDNo")%>";
	fm.PriorityNo.value = "<%=request.getParameter("PriorityNo")%>";
	fm.TopTypeNo.value = "03";
	fm.TypeNo.value = "<%=request.getParameter("TypeNo")%>";
	fm.DateLimit.value = "<%=request.getParameter("DateLimit")%>";
	fm.ApplyName.value = "<%=request.getParameter("ApplyName")%>";
	fm.ApplyTypeNo.value = "<%=request.getParameter("ApplyTypeNo")%>";
	fm.AcceptWayNo.value = "<%=request.getParameter("AcceptWayNo")%>";
	fm.Remark.value = "<%=request.getParameter("Remark")%>";
}
	
//个单信息列表的初始化
function initContGrid()
{                         
  var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="0px";            		//列宽
	  iArray[0][2]=200;            			//列最大值
	  iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="保单号";         	  //列名
	  iArray[1][1]="120px";            	//列宽
	  iArray[1][2]=200;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[2]=new Array();
	  iArray[2][0]="印刷号";         	  //列名
	  iArray[2][1]="120px";            	//列宽
	  iArray[2][2]=200;            			//列最大值
	  iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[3]=new Array();
	  iArray[3][0]="投保人";         	  //列名
	  iArray[3][1]="60px";            	//列宽
	  iArray[3][2]=200;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="主被保人";          //列名
	  iArray[4][1]="60px";            		//列宽
	  iArray[4][2]=200;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[5]=new Array();
	  iArray[5][0]="投保日期";         	//列名
	  iArray[5][1]="80px";            		//列宽
	  iArray[5][2]=200;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	
	  iArray[6]=new Array();
	  iArray[6][0]="截止日期";         	//列名
	  iArray[6][1]="80px";            	//列宽
	  iArray[6][2]=200;            		  //列最大值
	  iArray[6][3]=0;              		  //是否允许输入,1表示允许，0表示不允许 
	  
	  iArray[7]=new Array();
	  iArray[7][0]="交至日期";         	//列名
	  iArray[7][1]="80px";            	//列宽
	  iArray[7][2]=200;            		  //列最大值
	  iArray[7][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="保费";         	//列名
	  iArray[8][1]="80px";            	//列宽
	  iArray[8][2]=200;            		  //列最大值
	  iArray[8][3]=0;              		  //是否允许输入,1表示允许，0表示不允许
	  
	  iArray[9]=new Array();
	  iArray[9][0]="保额";         	//列名
	  iArray[9][1]="80px";            	//列宽
	  iArray[9][2]=200;            		  //列最大值
	  iArray[9][3]=3;              		  //是否允许输入,1表示允许，0表示不允许        
	  
	  iArray[10]=new Array();
	  iArray[10][0]="保单状态";         		  //列名
	  iArray[10][1]="60px";            		//列宽
	  iArray[10][2]=200;            			//列最大值
	  iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许             
	
	  ContGrid = new MulLineEnter("fm", "ContGrid"); 
	  //设置Grid属性
	  ContGrid.mulLineCount = 0;
	  ContGrid.displayTitle = 1;
	  ContGrid.locked = 1;
	  ContGrid.canSel = 1;
	  ContGrid.canChk = 0;
	  ContGrid.hiddenSubtraction = 1;
	  ContGrid.hiddenPlus = 1;
	  ContGrid.selBoxEventFuncName = "queryAddressInfo";
	  ContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert(ex);
  }
}

function initLCContGrid()
{
  var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";         			//列宽
	  iArray[0][2]=10;          			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="保单号";    	//列名1
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[2]=new Array();                                                       
	  iArray[2][0]="联系地址";    	//列名1                                              
	  iArray[2][1]="200px";            		//列宽                                   
	  iArray[2][2]=100;            			//列最大值                               
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许    
	                                                                             
	  iArray[3]=new Array();                                                       
	  iArray[3][0]="邮政编码";    	//列名1                                              
	  iArray[3][1]="50px";            		//列宽                                   
	  iArray[3][2]=100;            			//列最大值                               
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	
	
	  LCContGrid = new MulLineEnter( "fm" , "LCContGrid" ); 
	  //这些属性必须在loadMulLine前
	  LCContGrid.mulLineCount = 0;   
	  LCContGrid.displayTitle = 1;
	  LCContGrid.canChk=1;
	  LCContGrid.hiddenPlus = 1;
	  LCContGrid.hiddenSubtraction = 1;
	  LCContGrid.selBoxEventFuncName ="reportDetailClick";
	  LCContGrid.loadMulLine(iArray);  
	  LCContGrid.detailInfo="单击显示详细信息";
  }
  catch(ex)
  {
    alert(ex);
  }
}
</script>
    
    
    
    
    
    
    