<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskViewTopButton.jsp
//程序功能：工单管理工单查看页面顶部悬浮操作按钮
//创建日期：2005-04-03
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
</head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskViewTopButton.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>操作按钮 </title>
  <%
  	String CustomerNo = (String)session.getAttribute("CUSTOMERNO");
  	String WorkNo = request.getParameter("WorkNo");
   %>
<head>
<body onload="initForm();">
<form name=fm action="./TaskPersonalBoxSave.jsp" target=fraSubmit method=post>
    <div style="display: ''">
    <input type=hidden name="CustomerNo" value="<%=CustomerNo%>">
    <input type=hidden name="WorkNo" value="<%=WorkNo%>">
    <input type="button" class=cssButton name="view" value="查  看" onclick="viewTask();">
    <input type="button" style="display: none" class=cssButton name="dobus" value="经  办" onclick="doBusiness();">
    <input type="button" style="display: none" class=cssButton name="deliver" value="转  交" onclick="deliverTask();">
    <input type="button" style="display: none" class=cssButton name="wait" value="转为待办" onclick="waitTask();">
    <input type="button" style="display: none" class=cssButton name="unWait" value="取消待办" onclick="unWaitTask();">
    <input type="button" style="display: none" class=cssButton name="remark" value="批  注" onclick="remarkTask();">
    <input type="button" style="display: none" class=cssButton name="viewRemark" value="查看批注" onclick="viewRemarkTask();">
    <input type="button" style="display: none" class=cssButton name="unite" value="作业合并" onclick="uniteTask();">
    <input type="button" class=cssButton name="Scan" value="查看扫描/取消" onclick="viewScan();">
    <!--<input type="button" class=cssButton name="cancel" value="取消查看" onclick="cancelScan();">
    <input type="button" style="display: none" class=cssButton name="edit" value="编  辑" onclick="editTask();">--> 
    <input type="button" style="display: none" class=cssButton name="save" value="保  存" onclick="saveTask();"> 
    <input type="button" style="display: none" class=cssButton name="confirm" value="审  批" onclick="confirmTask();">
    <input type="button" style="display: none" class=cssButton name="finish" value="结  案" onclick="finishTask();">
    <input type="button" class=cssButton name="taskPrint" value="打  印"  onclick="printTask();">
    <INPUT TYPE=button Class="cssButton" name="" VALUE="函  件" onclick="letter();">
    <input type="button" style="display: none" class=cssButton name="goBack" value="返  回" onclick="returnTask();">
    </div>
</form>
</body>
</html>