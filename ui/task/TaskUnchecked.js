//程序名称：TaskUnchecked.jsp
//程序功能：个人送核件清单界面
//创建日期：2008-1-16
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

/* 查询工单在列表中显示结果 */
function queryTask()
{
  var strSql = "select a.WorkNo, a.AcceptNo, CodeName('priorityno',a.PriorityNo), "
             + "    (select WorkTypeName from LGWorkType where WorkTypeNo = a.TypeNo), a.CustomerNo, " 
             + "    a.CustomerName, '记录', a.acceptorNo, b.InDate, "
             + "    (select GroupName from lggroup where GroupNo = a.AcceptCom), " 
             + "    case when a.DateLimit is null then '' else trim(char(days(current date) - days(b.InDate))) || '/' || a.DateLimit end, " 
             + "    CodeName('taskstatusno' , a.StatusNo), CodeName('appedorstate' , c.EdorState), "
             + "    (Select count(NodeNo) from LGWorkRemark where WorkNo = a.WorkNo), " 
             + "    case ScanFlag when '0' then '新扫描件' when '1' then '旧扫描件' else '无扫描件' end, " 
             + "    a.detailWorkNo, a.StatusNo, (select (case when DefaultOperator is null then '核保池中' else DefaultOperator end) from LWMission where ActivityID='0000001180' and MissionProp1=a.workno) " 
             + "from LGWork a, LGWorkTrace b, LPEdorApp c " 
             + "where  a.WorkNo = b.WorkNo and a.NodeNo = b.NodeNo and c.EdorAcceptNo = a.workNo " 
             + "    and b.WorkBoxNo = (select WorkBoxNo from LGWorkBox where OwnerNo = '" + operator + "' and OwnerTypeNo = '2') " 
             + "    and c.EdorState = '10' ";
	
  turnPage2.pageDivName = "divPage2";
  turnPage2.pageLineNum = 15;
  turnPage2.queryModal(strSql, TaskGrid);
  return true;
}

/* 查看选中的工单 */
function viewTask()
{
  var pageName = "TaskView.jsp";
  var param;
  var chkNum = 0;
  var WorkNo;
  var CustomerNo;
  var DetailWorkNo;
	
  for (i = 0; i < TaskGrid.mulLineCount; i++)
  {
    if (TaskGrid.getChkNo(i)) 
    {
      chkNum = chkNum + 1;
      if (chkNum == 1)
      {
        WorkNo = TaskGrid.getRowColData(i, 1);
        CustomerNo = TaskGrid.getRowColData(i, 5);
        DetailWorkNo = TaskGrid.getRowColData(i, 15);   
      }
    }
  }
  if (chkNum == 0)
  {
    alert("请选择一条工单！");
    return false;
  }
  if (chkNum > 1)
  {
    alert("只能选择一条工单！");
    return false;
  }
  if (WorkNo)
  {
    var width = screen.availWidth - 10;
    var height = screen.availHeight - 28;
    win = window.open("TaskViewMain.jsp?loadFlag=PERSONALBOX&WorkNo="+WorkNo+"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + DetailWorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);
    win.focus();
  }
}

//查询
function Query(){
	if(!checktabledata()){
	return false;
	}
	var strSql = "select a.WorkNo, a.AcceptNo, CodeName('priorityno',a.PriorityNo), "
             + "    (select WorkTypeName from LGWorkType where WorkTypeNo = a.TypeNo), a.CustomerNo, " 
             + "    a.CustomerName, '记录', a.acceptorNo, b.InDate, "
             + "    (select GroupName from lggroup where GroupNo = a.AcceptCom), " 
             + "    case when a.DateLimit is null then '' else trim(char(days(current date) - days(b.InDate))) || '/' || a.DateLimit end, " 
             + "    CodeName('taskstatusno' , a.StatusNo), CodeName('appedorstate' , c.EdorState), "
             + "    (Select count(NodeNo) from LGWorkRemark where WorkNo = a.WorkNo), " 
             + "    case ScanFlag when '0' then '新扫描件' when '1' then '旧扫描件' else '无扫描件' end, " 
             + "    a.detailWorkNo, a.StatusNo, (select (case when DefaultOperator is null then '核保池中' else DefaultOperator end) from LWMission where ActivityID='0000001180' and MissionProp1=a.workno  fetch first 1 row only) " 
             + "from LGWork a, LGWorkTrace b, LPEdorApp c ,lpedoritem d " 
             + "where  a.WorkNo = b.WorkNo and a.NodeNo = b.NodeNo and a.workno=d.edorno and c.EdorAcceptNo = a.workNo and c.EdorState = '10'  " 
			 + getWherePart('d.edorappdate', 'StartDate','>=')
             + getWherePart('d.edorappdate', 'EndDate','<=')
             + getWherePart('c.ManageCom', 'ManageCom','like')
             + getWherePart('c.ManageCom', 'GIManageCom','like')
             + getWherePart('d.ContNo', 'ContNo')
             + getWherePart('a.WorkNo', 'WorkNo')
             + " with ur "
             ;
	
  turnPage2.pageDivName = "divPage2";
  turnPage2.pageLineNum = 15;
  turnPage2.queryModal(strSql, TaskGrid);
	
}

function checktabledata(){
	var startDate = fm.all('StartDate').value;
	var endDate = fm.all('EndDate').value;
	var tContNo = fm.all('ContNo').value;
	var tWorkNo = fm.all('WorkNo').value;

	if((tContNo ==null||tContNo =="")&&(tWorkNo ==null||tWorkNo =="")){
		if (startDate ==null||startDate =="")
		{
			alert("保单号和工单号均未录入,需要输入受理时间起期！");
			return false;
		}
		if (endDate ==null||endDate =="")
		{
			alert("保单号和工单号均未录入,需要输入受理时间止期！");
			return false;
		}
		var t1 = new Date(startDate.replace(/-/g,"\/")).getTime();
  	  	var t2 = new Date(endDate.replace(/-/g,"\/")).getTime();
 	   var tMinus = (t2-t1)/(24*60*60*1000);  
 	   if(tMinus>92 )
 	   {
		  alert("查询起止时间不能超过3个月！")
		  return false;
 	   }
    }
   	   return true;
}
