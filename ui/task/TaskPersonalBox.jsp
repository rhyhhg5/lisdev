<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="TaskCheckPersonalBox.jsp"%>
<%
//程序名称：TaskPersonalBox.jsp
//程序功能：工单管理个人信箱主界面
//创建日期：2005-01-15 14:10:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskPersonalBox.js"></SCRIPT>
  <%@include file="TaskPersonalBoxInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>个人信箱 </title>
</head>
<body  onload="initForm();">
  <span id="show"></span>
	<form name=fm action="./TaskPersonalBoxSave.jsp" target=fraSubmit method=post>
	<Input type="hidden" class= common name="WorkNo">
	<Input type="hidden" class= commpn name="AcceptNo">
	<!-- 用CheckBox隐藏域来标记是否使用CheckBox传参数 -->
	<Input type="hidden" class= commpn name="CheckBox" value="YES">
		<Div  id= "divTaskOption" style= "width: 939px; height: 73px">
		<INPUT VALUE="新  建" TYPE=button Class="cssButton" name="input" onclick="inputTask();">
		<INPUT VALUE="查  看" TYPE=button Class="cssButton" name="view" onclick="viewTask();">
		<INPUT VALUE="经  办" TYPE=button Class="cssButton" onclick="doBusiness();">
		<INPUT VALUE="转  交" TYPE=button Class="cssButton" name="deliver" onclick="deliverTask();">
		<INPUT VALUE="批  注" TYPE=button Class="cssButton" name="remark" onclick="remarkTask();">
		<INPUT VALUE="查看批注" TYPE=button Class="cssButton" name="viewRemark" onclick="viewRemarkTask();">
		<INPUT VALUE="转为待办" TYPE=button Class="cssButton" name="wait" onclick="waitTask();">
		<INPUT VALUE="取消待办" TYPE=button Class="cssButton" name="unwait" onclick="unWait();">
		<INPUT VALUE="查  询" TYPE=button Class="cssButton" name="query" onclick="queryTask()">
		<INPUT VALUE="作业历史" TYPE=button Class="cssButton" name="history" onclick="historyTask();">
		<INPUT VALUE="客服信息" TYPE=button Class="cssButton" name="customer" onclick="customerTask();">
		<br>
		<input value="简易保全" type="button" class=cssButton name="edor" onclick="easyEdor();">
		<!--<INPUT VALUE="岗位权限" TYPE=button Class="cssButton" name="purview" onclick="dealPostPurview();">-->
		<!--<INPUT VALUE="电话催缴" TYPE=button Class="cssButton" name="hasten" onclick="phoneHasten();">-->
		<INPUT VALUE="催缴日处理" TYPE=button Class="cssButton" name="Hasten" onclick="CreateHastenTask();">
		<INPUT VALUE="函件超时" TYPE=button Class="cssButton" name="LetterOverTime" onclick="CreateLetterOverTime();">
		<INPUT VALUE="待办件超时" TYPE=button Class="cssButton" name="OverTime" onclick="WaitOverTime();">
		<INPUT VALUE="保全交费结案" TYPE=button Class="cssButton" name="Finish" onclick="bqFinishTask();">
		<INPUT VALUE="续期催缴" TYPE=button Class="cssButton" name="NextFeeHasten" onclick="CreatNextFeeHasten();">
		<INPUT VALUE="补发函件" TYPE=button Class="cssButton" name="ReissueLetter" onclick="ReissueLetterClick();">
		<INPUT VALUE="满期结算" TYPE=button Class="cssButton" name="MJTask" onclick="CreatMJTask();">
		<!-- INPUT VALUE="定期结算到帐确认" TYPE=button Class="cssButton" name="btnBalPayConfirm" onclick="BalPayConfirm();"-->
		<INPUT VALUE="团单退保试算" TYPE=button Class="cssButton" name="gEdorCalZTTestButton" onclick="gEdorCalZTTest();">
		<INPUT VALUE="个单退保试算" TYPE=button Class="cssButton" name="pEdorCalZTTestButton" onclick="pEdorCalZTTest();">
		<Input value='小组信箱取件' Class="cssButton" type="button" class= common name="TeamBox" onclick="getTeamBox();">
		<p>
		<select name="order" style="width: 128; height: 23" onchange="easyQueryClick();" >
			<option selected value = "0">--排序--</option>
			<option value = "1">按优先级</option>
			<!--<option value = "1b">按优先级降序</option>-->
			<option value = "2">按受理号降序</option>
			<option value = "2b">按受理号升序</option>
			<option value = "3">按经办日期降序</option>
			<option value = "3b">按经办日期升序</option>
		</select>&nbsp;
		<select name="condition" style="width: 128; height: 23" onchange="easyQueryClick();" >
			<option value = "0">--按工单状态查询--</option>
			<option value = "1">未经办/正经办/审核</option>
			<option value = "2">未经办</option>
			<option value = "3">正经办</option>
			<option value = "4">审核</option>
			<option value = "5">待办（所有）</option>
			<option value = "6">待办（定期）</option>
			<option value = "7">待办（无期）</option>
			<option value = "8">结案</option>
			<option value = "9">存档</option>
			<option value = "10">过应办期</option>
			<option value = "11">被合并</option>
			<option value = "12">已撤销</option>
			<option value = "99">审批通过</option>
			<option value = "88">审批不通过</option>
		</select>&nbsp;
		<select name="condition3" style="width: 128; height: 23" onchange="easyQueryClick();" >
			<option value = "-1">--按业务状态查询--</option>
			<!--<option value = "3">录入完成</option>-->
			<option value = "1">保全申请</option>
			<!--<option value = "4">试算成功</option>-->
			<option value = "2">理算确认</option>
			<option value = "4">函件待下发</option>
			<option value = "5">函件待回销</option>
			<option value = "6">函件回销待处理</option>
			<option value = "7">函件已回销</option>
			<option value = "8">函件已超时</option>
			<option value = "9">待收费</option>
			<option value = "10">已送核</option>
			<option value = "11">核保完毕</option>
			<option value = "12">延期结案</option>
			<option value = "0">保全确认</option>
		</select>&nbsp;
		<select name="condition6" style="width: 128; height: 23" onchange="easyQueryClick();" >
			<option value = "0">--按所属渠道查询--</option>
			<option value = "1">个险</option>
			<option value = "2">团险</option>
			<option value = "3">银保</option>			
     </select>&nbsp; 
      
      <span id="spanSelect"  style="display: ''; position:absolute; slategray"></span>&nbsp;
      <!--
      <select name="condition2" style="width: 128; height: 23" onchange="changeCondition2(); easyQueryClick();" >
      <option value = "0">--按业务类型查询--</option>
      <option value = "1">咨询</option>
      <option value = "2">投诉</option>
      <option value = "3">保全</option>
      <option value = "4">理赔</option>
      </select>
      -->
	<br>
	</DIV>
  	<table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskList);"> 
      </td>
      <td width="185" class= titleImg>工单列表 </td>
    </tr>
	</table>
	<Div  id= "divTaskList" style= "display: ''" align=center>  
		<table  class= common>
	 		<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanTaskGrid" >
				</span> 
		  	</td>
		</tr>
	</table>
	 <Div id= "divPage" align=center style= "display: 'none' ">
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage1.firstPage();">      
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage1.nextPage();">       
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage1.lastPage();">       
	 </Div>
	</Div>
	<br>
	&nbsp;
	<Input type=hidden class=common name="Num" size="20" >
	</Form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	<!--<Input value='查看小组未完成清单' Class="cssButton" type="button" class= common name="TeamUnfinished" onclick="teamTest();">-->
	
</body>
</html>
