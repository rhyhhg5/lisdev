<%
//程序名称：TaskCheckTeamBox.jsp
//程序功能：检查个人信箱是否存在
//创建日期：2005-03-25 17:16:43
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
<%
	//从session中得到全局参数
	GlobalInput tCheckGI = new GlobalInput();
	tCheckGI = (GlobalInput) session.getValue("GI");
	VData tCheckVData = new VData();
	tCheckVData.add(tCheckGI);
	TaskCheckTeamBoxBL tTaskCheckTeamBoxBL = new TaskCheckTeamBoxBL();
	tTaskCheckTeamBoxBL.submitData(tCheckVData, "");
	VData tCheckRet = tTaskCheckTeamBoxBL.getResult();
	String tPass = (String)tCheckRet.getObject(0);
%>
<%
	if (tPass.equals("false"))
	{
%>
	<SCRIPT>
	location.replace("TaskShowError.jsp?errorType=NOTEAMBOX");
	</SCRIPT>
<%
	}
%>