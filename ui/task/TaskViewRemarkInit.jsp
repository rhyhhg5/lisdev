<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskTeamBoxInit.jsp
//程序功能：工单管理个人信箱页面初始化
//创建日期：2005-01-17 9:40:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tWorkNo = null;

	String loadFlag = request.getParameter("loadFlag");
	//if(loadFlag != null && loadFlag.equals("TaskViewTopButton"))
	//{
	//    //从察看页面调用
  //      tWorkNo = (String) session.getAttribute("VIEWWORKNO");
	//}
    //else
    //{
        tWorkNo = request.getParameter("WorkNo");
    //}
%>
<script language="JavaScript">
var WorkNo = "<%=tWorkNo%>";

//表单初始化
function initForm()
{
	try
	{
		initGrid();
		easyQueryClick();
	}
	catch(re)
	{
		alert("InitForm函数中发生异常:初始化界面错误!");
	}
}

// 保单信息列表的初始化
function initGrid()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="批注类型";         	  //列名
		iArray[1][1]="80px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="批注人";         	//列名
		iArray[2][1]="100px";            		//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
		iArray[2][4]="TaskMemberNo";
		iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[2][9]="成员编号|code:TaskMemberNo&NOTNULL";
		iArray[2][18]=250;
		iArray[2][19]= 0 ;    
		
		iArray[3]=new Array();
		iArray[3][0]="批注时间";         	  //列名
		iArray[3][1]="150px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="批注内容";          //列名
		iArray[4][1]="450px";            		//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
	
		
		RemarkGrid = new MulLineEnter("fm", "RemarkGrid"); 
		//设置Grid属性
		RemarkGrid.mulLineCount = 10;
		RemarkGrid.displayTitle = 1;
		RemarkGrid.locked = 1;
		RemarkGrid.canSel = 0;
		RemarkGrid.canChk = 0;
		RemarkGrid.hiddenSubtraction = 1;
		RemarkGrid.hiddenPlus = 1;
		RemarkGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
	  	alert(ex);
	}
}


</script>