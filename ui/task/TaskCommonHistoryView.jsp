<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：TaskCommonHistoryView.jsp
//程序功能：工单管理工单基本信息显示页面
//创建日期：2005-04-26
//创建人  Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <%@include file="TaskCommonHistoryViewInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>查看电话回访问卷 </title>
</head>

<body onload="initForm();">
	<form action="" method=post name=fm>
		<!--<Div  id= "divHistoryList" style= "display: ''" align="center">  
  		<table  class= common>
  	 		<tr  class= common>
  		  		<td text-align: left colSpan=1>
					<span id="spanHistoryGrid" >
					</span> 
			  	</td>
				</tr>
			</table>
		</Div>-->
		<table id="tableTaskInfo">
	      <tr>
		      <td  class=common>
		      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskHistory);">
		      </td>
	      <td class= titleImg>作业历史信息&nbsp;</td>
		  </tr>
	    </table>
	    <Div id="divTaskHistory" style="display: ''"> 
	    <table class=common>
	      <tr class=common>
	        <TD class= title> 受理号 </TD>
	        <TD class= input>
	      		<Input class="readonly" name="AcceptNo" readonly >
	        </TD>
	        <TD class= title> 节点号 </TD>
	        <TD class= input>
		    	<Input class="readonly" name="nodeNo" readonly > 
		    </TD>  
		    <TD class= title> 所在信箱 </TD>
	        <TD class= input>
	        	<input class="readonly" name="workBox">
	        </TD>
	      </tr>
	      <tr>
	        <TD class= title> 进入日期 </TD>
	        <TD class= input>
	        	<input class="readonly" name="InDate" readonly>
	        </TD>
	        <TD class= title> 发出机构 </TD>
	        <TD class= input>
	        	<input class="readonly" name="ManageCom" type=hidden>
	        	<input class="readonly" name="ManageComName" readonly ondblclick="return showCodeList('station', [this, TypeNo], [1, 0]);" onkeyup="return showCodeListKey('station', [this, TypeNo], [1, 0]);">
	        </TD>
	        <TD class= title> 发出人 </TD>
	        <TD class= input>
	        	<input class="readonly" name="sendPerson" readonly >
	        </TD>
	      </tr>	   
	      <tr>
	        <TD class= title> 产生方式 </TD>
	        <TD class= input colspan="5" >
	        	<input class="readonly" name="inMethod" readonly>
	        </TD>
	      </tr>	   
	    </table>
	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>