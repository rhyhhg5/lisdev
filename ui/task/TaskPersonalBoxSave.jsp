<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskPersonalBoxSave.jsp
//程序功能：工单管理经办页面
//创建日期：2005-02-24 15:54:20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	String tUrl = "";
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	//得到经办路径
	String tWorkNo = (String) request.getParameter("WorkNo");
	
	if ((tWorkNo == null) || (tWorkNo.equals("")))
	{
		tWorkNo = (String) session.getAttribute("VIEWWORKNO");
	}
	
	VData tVData = new VData();
	tVData.add(tWorkNo);
	tVData.add(tGI);
	TaskDoBusinessBL tTaskDoBusinessBL = new TaskDoBusinessBL();
	if (tTaskDoBusinessBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
	}
	VData tRet = tTaskDoBusinessBL.getResult();
	tUrl = (String) tRet.getObjectByObjectName("String", 0);
	
	//如果是电话催缴则需得到原保全受理号
	if(tUrl.indexOf("050013") != -1)
	{
		String sql = "select EdorAcceptNo "
					+ "from LGPhoneHasten "
					+ "where workNo='" + tWorkNo + "' ";
		ExeSQL tExeSQL = new ExeSQL();
		SSRS tSSRS = tExeSQL.execSQL(sql);
		
		if(tSSRS.getMaxRow() > 0)
		{
			tUrl += "&EdorAcceptNo=" + tSSRS.GetText(1, 1);
		}
	}
	
	System.out.println("URL:"+tUrl);
	
	//如果工单还没有结案，则改变工单状态为经办，并设置当前用户经办标志
	//若工单为通过审批则此时状态不能转为正经办，否则后面的处理将会出错
	String status = tTaskDoBusinessBL.getStatus();
	System.out.println("TaskPersonalBoxSave.jsp中status ="+status);
	if(!status.equals(Task.WORKSTATUS_DONE) && !status.equals(Task.WORKSTATUS_ARCHIVE) 
	  && !status.equals(Task.WORKSTATUS_UNITED) && !status.equals(Task.WORKSTATUS_CANCEL) 
	  && !status.equals(Task.WORKSTATUS_CONFIRMPASS) && !status.equals(Task.WORKSTATUS_CONFIRMFAIL))
	{
		LGWorkSchema tLGWorkSchema = new LGWorkSchema();
		tLGWorkSchema.setWorkNo(tWorkNo);
		tLGWorkSchema.setStatusNo("3");
		tLGWorkSchema.setCurrDoing("1");
		tVData.clear();
		tVData.add(tLGWorkSchema);
		tVData.add(tGI);
		TaskChangeStatusBL tTaskChangeStatusBL = new TaskChangeStatusBL();
		if (tTaskChangeStatusBL.submitData(tVData, "") == false)
		{
			FlagStr = "Fail";
		}
	}
	
	//重定向链接
	if(!tUrl.equals("None"))
	{
		response.sendRedirect(tUrl);
	}
	else
	{%>
		<script language="javascript">
			alert("没有找到相应的业务数据");
		</script>
	<%}
%> 
<html>
</html>

