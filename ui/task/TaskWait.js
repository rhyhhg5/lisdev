//程序名称：TaskBack.js
//程序功能：工单管理转为待办页面
//创建日期：2005-01-20 13:38:50
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}

function limitDate()
{
	document.all("tdHide1").style.display = "";
	document.all("tdHide2").style.display = "";
	document.all("tdHide3").style.display = "";
	document.all("tdHide4").style.display = "";
	fm2.WakeDate.value = "";
	fm2.WakeTimeHour.value = "";
	fm2.WakeTimeHour.value = "";
	fm2.PauseNo.value = "";
}

function foreverDate()
{
	document.all("tdHide1").style.display = "none";
	document.all("tdHide2").style.display = "none";
	document.all("tdHide3").style.display = "none";
	document.all("tdHide4").style.display = "none";
	fm2.WakeDate.value = "9999-12-31";
	fm2.WakeTimeHour.value = "24";
	fm2.WakeTimeHour.value = "59";
	fm2.PauseNo.value = "forever";
}

function beforeSubmit()
{
	if (fm2.TaskWaitType[0].checked)
	{
	    if(!verifyInput2())
	    {
	        return false;
	    }
	    if(!checkFormat())
	    {
	        return false;
	    }
	    
		var tWaitDate = fm2.all("WakeDate").value;
		var arr = tWaitDate.split("-");
		var today = new Date();
				
		if(arr[0] < today.getFullYear() || 
				(arr[0] == today.getFullYear() && arr[1] < today.getMonth() + 1) || 
				((arr[0] == today.getFullYear() && arr[1] == today.getMonth() + 1) && arr[2] < today.getDate()))
		{
			alert("待办日期比现在早，请重新输入");
			fm2.all("WakeDate").focus();
			return false;
		}
	}
	return true;
}

//校验录入数据的格式
function checkFormat()
{
    var WakeTimeHour = fm2.WakeTimeHour.value;
    var WakeTimeMinute = fm2.WakeTimeMinute.value;
    
    if(WakeTimeHour != "" && !isInteger(WakeTimeHour))
    {
        alert("小时录入有误");
        return false;
    }
    
    if(WakeTimeHour != "" && isInteger(WakeTimeHour))
    {
        var timeHour = parseInt(WakeTimeHour);
        if(timeHour > 24)
        {
	        alert("小时不能大于24");
            return false;
        }
        if(WakeTimeMinute != "" && !isInteger(WakeTimeMinute))
        {
            alert("分钟录入有误");
            return false;
        }
        if(WakeTimeMinute != "")
        {
            var timeMinute = parseInt(WakeTimeMinute);
            if(timeMinute > 59)
            {
    	        alert("分钟不能大于59");
                return false;
            }
        }
    }
    else if(WakeTimeHour == "" && WakeTimeMinute != "")
    {
        alert("请先录入小时");
        return false;
    }
    
    return true;
}

/* 提交表单 */
function submitForm()
{
	if (beforeSubmit())
	{
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, 
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm2.submit(); //提交
	}
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");		
		
		//top.focus();
		if(top.loadFlag != null && top.loadFlag == "OPRTAFTERDOBUSI")
		{
			top.opener.top.close();
		}
		else
		{
			top.opener.location.reload();
		}
		top.close();
	}
}