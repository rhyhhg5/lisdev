//程序名称：TaskBack.js
//程序功能：程序功能：工单管理工单收回页面
//创建日期：2005-1-17 11:03:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

/* 查询工单在列表中显示结果 */
function easyQueryClick()
{
	//初始化表格
	initGrid();
	
	//书写SQL语句
	var strSQL;
	strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, w.TypeNo, w.CustomerNo, " +
			 "       (Select Name from LDPerson where CustomerNo = w.CustomerNo), '记录', " +
			 "       w.AcceptorNo, w.AcceptCom, char(t.InDate) || ' ' || t.InTime, " +
			 "       Case When w.DateLimit Is Null then '' Else  trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
			 "       w.StatusNo, (Select count(NodeNo) from LGWorkRemark where WorkNo = w.WorkNo), '' " +
			 "from   LGWork w, LGWorkTrace t " +
	         "where  w.WorkNo = t.WorkNo " +
	      	 "and    w.NodeNo = t.NodeNo " +
			 "and    w.CustomerNo = '" + mCustomerNo + "' " +
			 "order by t.InDate desc ";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
	  
	//判断是否查询成功
	if (!turnPage.strQueryResult) 
	{
		return false;
	}
	
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = TaskGrid;    
	      
	//保存SQL语句
	turnPage.strQuerySql = strSQL; 
	
	//设置查询起始位置
	turnPage.pageIndex = 0;  
	
	//在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	return true;
}


