<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：2005-06-02
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="TaskReissueLetter.js"></SCRIPT>
  <%@include file="TaskReissueLetterInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>岗位权限管理</title>
</head>
<body  onload="initForm();">
	<form name=fm action="" target=fraSubmit method=post>
		<table  class=common align=center>
			<tr align=right>
				<td class=title >补发类型</td>
				<td class= input>
					<Input name=ReissueType class="codeNo" ondblclick="return showCodeList('reissuetype', [this,ReissueTypeName],[0,1],null,null,null,1,300);" onkeyup="return showCodeListKey('reissuetype', [this,ReissueTypeName],[0,1],null,null,null,1,300);" verify="补发类型|notnull&code:reissuetype"><Input name=ReissueTypeName class="codeName" elementtype=nacessary readonly >
				</td>
				<td class= input></td>
				<td class=title >客户号</td>
				<td class= input>
					<input class="common" name="CustomerNo" verify="客户号|notnull" >
				</td>
				<td class= input></td>	
				<td class= input>					
    			<input type="button" Class="cssButton" name="search" value="查  询" onclick="queryLetter();">
				</td>
			</tr>
		</table>
		
		<table id="TableId" >			
    	<tr>
			  <td width="17"> 
			  <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divLetterList);"> 
			  </td>
			  <td width="185" class= titleImg>函件清单</td>
			</tr>
    </table>
  	<div id="divLetterList" style="display: ''">
  		<table  class= common>
 				<tr  class= common>
			  		<td text-align: left colSpan=1>
							<span id="spanLetterGrid" ></span> 
			  		</td>
				</tr>
			</table>
			<Div id= "divPage" align=center style= "display: 'none' ">
  			<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">  
 			</Div>
  	</div>
  	<br>
  	<hr>
  	<br>
    <input type="button" Class="cssButton" name="queryDetail" value="明细查询" onclick="queryDetailClick(true);">
    <input type="button" Class="cssButton" name="letterReissue" value="函件补发" onclick="queryDetailClick(false);">
    <input type="button" Class="cssButton" name="return" value="返    回" onclick="returnParent();">
    
    <br>
  	<hr>
  	<br>
    
    <!--显示函件图片-->
		<iframe id="pictureId" src="" width="100%" height="0" MARGINWIDTH=0 MARGINHEIGHT=0 scrolling="auto" frameborder="0"></iframe>
			
  	<input type=hidden id="fmtransact" name="fmtransact" value="">
	</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
