<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskCommonCustomerInit.jsp
//程序功能：工单管理客服信息初始化页面
//创建日期：2005-03-20 15:20:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<SCRIPT>
var turnPage = new turnPageClass();
var condition =  "	and w.statusNo  in('3') ";
//查看作业明细
function viewTask()
{	
	var chkNum = 0;
	var WorkNo;
	var customerNo = "";
	
	for (i = 0; i < CustomerGrid.mulLineCount; i++)
	{
		if (CustomerGrid.getChkNo(i)) 
		{
			chkNum = chkNum + 1;
			WorkNo = CustomerGrid.getRowColData(i, 2);
			CustomerNo = CustomerGrid.getRowColData(i, 1);
		}
	}
	if(chkNum > 1)
	{
		alert("您只能选择一条工单");
	}
	else if(chkNum == 0)
	{
		alert("请先选择一条工单！"); 
	}
	else if (WorkNo)
	{
		var width = screen.availWidth - 10;
	    var height = screen.availHeight - 28;
		win = window.open("TaskViewMain.jsp?WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + CustomerNo, 
						  "view", "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
		win.focus();
	}
}

function viewTaskAll()
{
  condition =  "	and w.statusNo not in( '6', '7') ";
  queryCustomerGrid();
}

//查询客服信息
function queryCustomerGrid()
{
	var CustomerNo;
	//var CustomerNo = "<%=request.getParameter("CustomerNo")%>";
	try
	{
		//本段程序主要是为了支持工单信息的修改而做
		CustomerNo = fm.CustomerNo.value;
	}
	catch(e)
	{
		CustomerNo = "<%=request.getParameter("CustomerNo")%>";
	}
	if (CustomerNo == "null")
	{
		return false;
	}

	//书写SQL语句
	var strSQL;

		strSQL = "select w.CustomerNo, w.AcceptNo, w.PriorityNo, "
		      + "     (select WorkTypeName from LGWorkType where WorkTypeNo = w.TypeNo), "
		      + "     w.WorkNo, (Select Name from LDPerson where CustomerNo = w.CustomerNo), "
		      + "     '记录', " +
				 	"       (select UserName from LDUser where UserCode = t.SendPersonNo), t.InDate, "
				 	+ "     (select Name from LDCom where ComCode = (select ComCode from LDUser where UserCode = t.SendPersonNo)), " +
				 	"       Case When w.DateLimit Is Null then '' Else  trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
				 	"       w.StatusNo, (select count(*) from LGWorkRemark r where r.WorkNo=w.AcceptNo), " + 
				 //"       w.StatusNo, '', '' " + 
					"		case ScanFlag " +
				 "			when '0' then '新扫描件' " +
				 "			when '1' then '旧扫描件' " +
				 "			else '无扫描件' " +
				 "		end " +
				 "from   LGWork w, LGWorkTrace t " +
		         "where  w.WorkNo = t.WorkNo " +
		      	 "	and w.NodeNo = t.NodeNo " +
				 "	and w.CustomerNo = '" + CustomerNo + "' " +
				 condition + 
				 "	and CustomerNo != '' " + 
				 "order by w.workNo desc ";

	turnPage.queryModal(strSQL, CustomerGrid);
	return true;
}

//客服信息初始化
function initCustomerGrid()
{                         
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="客户名";         	  //列名
      iArray[1][1]="100px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许               
                           
      iArray[2]=new Array();                     
      iArray[2][0]="受理号";         	  //列名
      iArray[2][1]="130px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="优先级";          //列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="业务类型";         	//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
  
      iArray[5]=new Array();
      iArray[5][0]="受理号";         	//列名
      iArray[5][1]="80px";            	//列宽
      iArray[5][2]=200;            		  //列最大值
      iArray[5][3]=3;              		  //是否允许输入,1表示允许，0表示不允许 
      
      iArray[6]=new Array();
      iArray[6][0]="姓名";         	//列名
      iArray[6][1]="80px";            	//列宽
      iArray[6][2]=200;            		  //列最大值
      iArray[6][3]=3;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="记录";         	//列名
      iArray[7][1]="80px";            	//列宽
      iArray[7][2]=200;            		  //列最大值
      iArray[7][3]=3;              		  //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="经办人";         	//列名
      iArray[8][1]="60px";            	//列宽
      iArray[8][2]=200;            		  //列最大值
      iArray[8][3]=0;              		  //是否允许输入,1表示允许，0表示不允许  
      
      iArray[9]=new Array();
      iArray[9][0]="经办日期";              //列名
      iArray[9][1]="135px";            	//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="受理机构";         		  //列名
      iArray[10][1]="120px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
  	  iArray[11]=new Array();
      iArray[11][0]="时间限制";         		  //列名
      iArray[11][1]="60px";            	  //列宽
      iArray[11][2]=200;            		  //列最大值
      iArray[11][3]=0;              		  //是否允许输入,1表示允许，0表示不允许  
      
      iArray[12]=new Array();
      iArray[12][0]="处理状态";         		  //列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=200;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[13]=new Array();
      iArray[13][0]="批注数";         	  //列名
      iArray[13][1]="40px";            	//列宽
      iArray[13][2]=200;            			//列最大值
      iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[14]=new Array();
      iArray[14][0]="扫描";         	  //列名
      iArray[14][1]="40px";            	//列宽
      iArray[14][2]=200;            			//列最大值
      iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      CustomerGrid = new MulLineEnter("fm", "CustomerGrid"); 
      //设置Grid属性
      CustomerGrid.mulLineCount = 0;
      CustomerGrid.displayTitle = 1;
      CustomerGrid.locked = 1;
      CustomerGrid.canSel = 0;
      CustomerGrid.canChk = 1;
      CustomerGrid.hiddenSubtraction = 1;
      CustomerGrid.hiddenPlus = 1;
      CustomerGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
  
  queryCustomerGrid();
}
</SCRIPT>