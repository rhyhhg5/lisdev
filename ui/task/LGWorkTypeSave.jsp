<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LGWorkTypeSave.jsp
//程序功能：
//创建日期：2005-02-26 15:30:19
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LGWorkTypeSchema tLGWorkTypeSchema   = new LGWorkTypeSchema();
  OLGWorkTypeUI tOLGWorkTypeUI   = new OLGWorkTypeUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tWorkTypeNo;
  String tSuperTypeNo = request.getParameter("SuperTypeNo");
  if (transact.equals("INSERT||MAIN"))
  {
  	tWorkTypeNo = tSuperTypeNo +  PubFun1.CreateMaxNo("TASKTYPE", 4);
  }
  else
  {
  	tWorkTypeNo = request.getParameter("WorkTypeNo");
  }
    tLGWorkTypeSchema.setWorkTypeNo(tWorkTypeNo);
    tLGWorkTypeSchema.setWorkTypeName(request.getParameter("WorkTypeName"));
    tLGWorkTypeSchema.setSuperTypeNo(tSuperTypeNo);
    tLGWorkTypeSchema.setDateLimit(request.getParameter("DateLimit"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLGWorkTypeSchema);
  	tVData.add(tG);
    tOLGWorkTypeUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLGWorkTypeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	if (transact.equals("INSERT||MAIN"))
    	{
    		VData tRet = tOLGWorkTypeUI.getResult();
  			tLGWorkTypeSchema.setSchema((LGWorkTypeSchema) tRet.getObjectByObjectName("LGWorkTypeSchema", 0));
    		Content += "新增业务类型编号为" + tLGWorkTypeSchema.getWorkTypeNo(); 
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=transact%>");
</script>
</html>
