<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//程序名称：FrameMain.jsp
//程序功能：工单管理框架页面
//创建日期：2006-11-20 15:33
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%
  String initPage = request.getParameter("InitPage");
  System.out.println(initPage);
%>

<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<html>
<head>
<title>扫描件打印</title>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="no" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="40,0,0,*,0" cols="*">
    <frame id="fraMenu" name="fraMenu" noresize scrolling="no" src="">
    <frame id="fraInterface" name="fraInterface" scrolling="auto" src= "<%=initPage%>">
	  <frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>

