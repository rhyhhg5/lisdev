<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：PWSubServiceTypeMInput.jsp
//程序功能：工单子业务类型修改
//创建日期：2016-12-27 09:36:56
//创建人  ：Yu ZhiWei

%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="PWSubServiceTypeMInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./PWSubServiceTypeMInit.jsp"%>
  
  <title>工单子业务类型修改 </title>
  <script>
  	var ManageCom = "<%=tGI.ManageCom%>";  //管理机构
  </script>
</head>

<body  onload="initForm();">
  <form action="./PWSubServiceTypeMSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
    		<td>
    		  <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGGroup1);">
    		</td>
        <td class= titleImg>
          子业务类型信息
        </td>   		 
    	</tr>
    </table>  
    <Div  id= "divsubservice" style= "display: ''">
    <table class=common align='center'>
      <tr class = common>
        <TD class= title> 管理机构 </TD>
        <TD class= input>
	    	<Input class='readonly' name='ManagementOrganization' style='width:100' readonly >
	    </TD>
	    <TD class= title> 受理号 </TD>
        <TD class= input>
	    	<Input class=common name="AcceptNo"  elementtype="nacessary" verify="受理号|notnull&len>6">
	  		
	    </TD>
      </tr>
      <tr>
        <TD class= title> 业务类型 </TD>
        <TD class= input>
        	<input class="code" name="TopTypeNo" type="hidden">
        	<input class="readonly" name="TopTypeNoname" readonly >
        </TD> 
        <TD class= title> 子业务类型 </TD>
        <TD class= input>
        	<input class="code" name="TypeNo" type="hidden">
        	<input class="code" name="TypeNoname" readonly ondblclick="return showCodeList('TaskTypeNo', [this,TypeNo], [1,0], null, fm.TopTypeNo.value, 'SuperTypeNo', 1, null);" onkeyup="return showCodeListKey('TaskTypeNo', [this,TypeNo], [1,0], null, fm.TopTypeNo.value, 'SuperTypeNo', 1, null);" >
        </TD> 
      </tr>
   	  <tr >
        <td  colspan="6" align="left">
          <input type="button" class="cssButton" name="saveButton" value="查  询" onclick="querySubServiceType();">&nbsp;&nbsp;
          <input type="button" class="cssButton" name="modifyButton" value="修  改" onclick="modifySubServiceType();">
        </td>
      </tr>
    </table>
 	</Div>
 	<input type=hidden id="lgWorkAcceptCom" name="lgWorkAcceptCom">
	<input type=hidden id="fmtransact" name="fmtransact">
  </form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>


</html>
