//程序名称：TaskRuleSetting.js
//程序功能：
//创建日期：2005-05-27 13:38:50
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容


var showInfo;

var ruleContent;
var ruleTarget;

//查询转到目标
function queryTarget()
{
	//if(fm.goalType.value == "0")
	//{
		showInfo = window.open("TaskSearchBox.html");
	//}
}

//function checkData()
//{
//	if((ruleContent == "" || ruleContent == null) || (ruleTarget == "" || ruleTarget == null))
//	{
//		alert("规则内容和目标均不能为空");
//		return false;
//	}
//	
//	var sql = 	"select * " +
//				"from LGAutoDeliverRule " +
//				"where 	RuleContent='" + ruleContent + "' and " +
//						"target='" + ruleTarget + "' ";
//	
//	var result = easyExecSql(sql);
//	try
//	{
//		if(result)
//		{
//			alert("本规则已经存在");
//			return false;
//		}
//		
//		sql = 	"select * " +
//				"from LGAutoDeliverRule " +
//				"where 	RuleContent='" + ruleContent + "' ";
//				
//		result = easyExecSql(sql);
//		if(result)
//		{
//			var check = confirm("本规则内容所对应的目标已经存在，您真的要再增加一条规则吗？");
//			if(check == false)
//			{
//				return false;
//			}
//		}
//	}
//	catch(e)
//	{
//		alert(e);
//	}
//	
//	return true;
//}

//保存新的规则
function save()
{
	ruleContent = fm.ruleContent.value;
	ruleTarget = fm.target.value;
	
	//if(!checkData())
	//{
	//	return false;
	//}
	
	fm.action = "TaskRuleSettingSave.jsp";
	return submitForm();
}

//执行修改操作
function updateClick()
{
	ruleContent = fm.ruleContent.value;
	ruleTarget = fm.target.value;
	
	//if(!checkData())
	//{
	//	return false;
	//}
	
	fm.fmtransact.value="UPDATE||MAIN";
	
	return submitForm();
}

//修改规则
function queryClick()
{
	//if(fm.goalType.value == "0")
	//{
		showInfo = window.open("TaskSearchRuleMain.jsp");
	//}
}

//删除规则
function deleteClick()
{
	fm.fmtransact.value="DELETE||MAIN";
	submitForm();
}

/* 设置规则信息,由规则查询页面返回时调用 */
function afterQuery(result)
{
	fm.target.value = result;
}

/* 设置规则信息,由规则查询页面返回时调用 */
function afterQuery2(result)
{	
	fm.ruleNo.value = result[0][0];
	fm.ruleContent.value = result[0][1];
	fm.all("ruleContent").readOnly = true;
	fm.SourceComCode.value = result[0][8];
	fm.SourceComCodeName.value = result[0][8];
	fm.all("SourceComCodeName").readOnly = true;
	fm.goalType.value = result[0][9];
	fm.goalTypeName.value = result[0][9];
	fm.all("goalTypeName").readOnly = true;
	fm.target.value = result[0][4]
	fm.ruleDescription.value = result[0][6];
	
	showCodeName();
}

/* 提交表单 */
function submitForm()
{
	if(fm.goalType.value == null || fm.goalType.value == "")
	{
		alert("项目类型不能为空");
		return false;
	}
	if(fm.goalType.value == "" || fm.goalType.value == null)
	{
		fm.ruleContent.value = " ";
	}
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit(); //提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		window.focus();
		window.location.reload();
	}
}