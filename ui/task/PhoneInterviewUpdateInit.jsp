<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：PhoneInterviewUpdateInit.jsp
//程序功能:
//创建日期：2005-04-02
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="javascript">
var turnPage = new turnPageClass();

var DetailWorkNo = "<%=request.getParameter("DetailWorkNo")%>";

function initForm()
{
	try
	{
		initInfo();
	}
	catch(re)
	{
		alert("InitForm函数中发生异常:初始化界面错误!");
	}
}

//初始化工单信息
function initInfo()
{
	var strSql = "select * " +
			 		"from LGPhoneAnwser " +
			 		"where phoneWorkNo= '" + DetailWorkNo + "' ";
	var arrResult = easyExecSql(strSql);
	
	if (!arrResult)
	{
		alert("没有找到符合条件的记录!");
		return false;
	}
	
	
	var answer;
	var remark;
	var checkedFlag;
	
	for(var k = 0; k < arrResult.length; k++)
	{
		//设置第k个问题的答案
		answer = "answer";
		answer = answer + (k + 1);
		checkedFlag = arrResult[k][3];
		if(checkedFlag == "0" || checkedFlag == "1")
		{
			fm.all(answer)[checkedFlag].checked = true; 
		}
		
		
		//设置第k个问题的备注
		remark = "remark";
		remark = remark + (k + 1);
		fm.all(remark).value = arrResult[k][4];
		
		//显示该问题
		var div = "div" + (k + 1);
		document.all(div).style.display = "";
	}

	return true;
}

</script>