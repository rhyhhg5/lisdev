<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-02-22 17:32:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="LGGroupInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LGGroupInputInit.jsp"%>
  <script>
  	var ManageCom = "<%=tGI.ManageCom%>";  //操作员编号
  </script>
</head>
<body  onload="initForm();" >
  <form action="./LGGroupSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    	<tr>
    		<td>
    		  <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGGroup1);">
    		</td>
        <td class= titleImg>
          小组信息
        </td>   		 
    	</tr>
    </table>
    <Div  id= "divLGGroup1" style= "display: ''">
		<table  class= common align='center' >
      <TR  class= common>
        <TD  class= title>
        小组编号
        </TD>
        <TD  class= input>
        <Input class= "readonly" name=GroupNo readonly >
        </TD>
        <TD  class= title>
        上级机构
        </TD>
        <TD  class= input>
         <input class="readonly" name="SuperGroupNo" readonly  >
         <!--<input class="code" name="SuperGroupNo" ondblclick="return showCodeList('station', [this]);" onkeyup="return showCodeListKey('station', [this]);" verify="机构|notnull&code:station">-->
        </TD>
      </TR>
        <TR  class= common>
        <TD  class= title>
          小组名称
        </TD>
        <TD  class= input>
          <Input class= 'common' name=GroupName elementtype="nacessary" verify="小组名称|notnull&len<=100" >
        </TD>
        <TD  class= title>
          业务类型
        </TD>
        <TD  class= input>
          <Input class="codeNo" name="WorkTypeNo" readOnly verify="上级分类|notnull&code:TaskTopTypeNo" ondblclick="return showCodeList('TaskTopTypeNo',[this,WorkTypeNoName], [0,1]);" onkeyup="return showCodeListKey('TaskTopTypeNo',[this,WorkTypeNoName], [0,1]);" ><Input class="codeName" name="WorkTypeNoName" elementtype="nacessary"  readonly >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          小组描述
        </TD>
        <td class=input colspan="3">
          <textarea classs ="common" name="GroupInfo" cols="80%" rows="3" verify="小组描述|len<70"></textarea>
        </td>     
      </TR>
		</table>
		<!--
		<center>
			<br><br>请为该小组分配成员
		<table class= common align='center' width="80%">
			<tr>
				<td class= common align="right">小组现有成员
				</td>				
				<td class= common>
				</td>
				<td class= common align="left">
					尚未分配小组的成员
				</td>
			</tr>
			<tr>
				<td class=common align="right">
					<select name="newMember" class=common size=8 style="background-color: #F7F7F7; border: none; width: 200;">					
      				</select>
				</td>				
				<td class=common align="center">
					<input type="button" value=" << "">
					<input type="button" value=" >> "">
				</td>
				<td class=common align="left">	
					<select name="newMember" class=common size=8 style="background-color: #F7F7F7; border: none; width: 200;">					
      				</select>				
				</td>
			</tr>
		</table>
		</center>-->
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
  </form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
