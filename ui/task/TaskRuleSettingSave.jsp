 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskRuleSettingSave.jsp
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-05-26 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput();
	
	//获得公共输入信息
	tG=(GlobalInput)session.getValue("GI");
	transact = request.getParameter("fmtransact");
	System.out.println("transact: " + transact);

	LGAutoDeliverRuleSchema tLGAutoDeliverRuleSchema = new LGAutoDeliverRuleSchema();
	
	tLGAutoDeliverRuleSchema.setRuleNo(request.getParameter("ruleNo"));
	tLGAutoDeliverRuleSchema.setSourceComCode(request.getParameter("SourceComCode"));
	tLGAutoDeliverRuleSchema.setRuleContent(request.getParameter("ruleContent"));
	tLGAutoDeliverRuleSchema.setTarget(request.getParameter("target"));
	tLGAutoDeliverRuleSchema.setGoalType(request.getParameter("goalType"));
	tLGAutoDeliverRuleSchema.setRuleDescription(request.getParameter("ruleDescription"));
	System.out.println(tLGAutoDeliverRuleSchema.getRuleContent() + ", " + tLGAutoDeliverRuleSchema.getSourceComCode() + ", "
		+ tLGAutoDeliverRuleSchema.getGoalType() + ", " + tLGAutoDeliverRuleSchema.getTarget());
	
	// 准备传输数据 VData
	VData tVData = new VData();  	
	tVData.add(tLGAutoDeliverRuleSchema);
	tVData.add(tG);

	TaskAutoDeliverRuleBL tTaskAutoDeliverRuleBL = new TaskAutoDeliverRuleBL();
  	if (tTaskAutoDeliverRuleBL.submitData(tVData, transact) == false)
	{
		FlagStr = "Fail";
		Content = tTaskAutoDeliverRuleBL.mErrors.getErrContent();
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}  
	
	Content = PubFun.changForHTML(Content);
%>
<html>
<script language="javascript">	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>