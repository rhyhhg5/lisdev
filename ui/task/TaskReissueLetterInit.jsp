<%
//程序名称：TaskPostPurviewInit.jsp
//程序功能：项目权限设置界面
//创建日期：2005-07-05
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.task.GetMemberInfo"%>

<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String memberNo = tG.Operator;
	String postNo = GetMemberInfo.getPostNo(memberNo).toLowerCase();
	String comCode = tG.ComCode;
%>

<script language="javascript">
	
var comCode;

function initForm()
{
	comCode='<%=comCode%>';
	initLetterGrid();	
	//showCodeName();
}



// 项目权限信息列表的初始化
function initLetterGrid()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="函件类型代码";         	  //列名
		iArray[1][1]="40px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		
		iArray[2]=new Array();
		iArray[2][0]="函件类型";         	  //列名
		iArray[2][1]="60px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="函件编号";         	  //列名
		iArray[3][1]="50px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]= 0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="函件生成日期";         	  //列名
		iArray[4][1]="80px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]= 0;              			//是否允许输入,1表示允许，0表示不允许		

		
		iArray[5]=new Array();
		iArray[5][0]="最近打印日期"     	  //列名
		iArray[5][1]="80px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="打印人";         	  //列名
		iArray[6][1]="55px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		LetterGrid = new MulLineEnter("fm", "LetterGrid"); 
		//这些属性必须在loadMulLine前
		LetterGrid.mulLineCount = 0;
		LetterGrid.displayTitle = 1;
		LetterGrid.locked = 1;
		LetterGrid.canSel = 1;
		LetterGrid.canChk = 0;
		LetterGrid.hiddenSubtraction = 1;
		LetterGrid.hiddenPlus = 1;
		//LetterGrid.addEventFuncName = "initBnfGrid";
		LetterGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert("项目权限信息列表初始化错误" + ex);
	}
}

// 项目权限信息列表的初始化
function initLetterGrid01()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="函件类型代码";         	  //列名
		iArray[1][1]="40px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		
		iArray[2]=new Array();
		iArray[2][0]="函件类型";         	  //列名
		iArray[2][1]="60px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="函件编号";         	  //列名
		iArray[3][1]="50px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]= 0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="函件生成日期";         	  //列名
		iArray[4][1]="80px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]= 0;              			//是否允许输入,1表示允许，0表示不允许		

		
		iArray[5]=new Array();
		iArray[5][0]="最近打印日期"     	  //列名
		iArray[5][1]="80px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="打印人";         	  //列名
		iArray[6][1]="55px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		LetterGrid = new MulLineEnter("fm", "LetterGrid"); 
		//这些属性必须在loadMulLine前
		LetterGrid.mulLineCount = 0;
		LetterGrid.displayTitle = 1;
		LetterGrid.locked = 1;
		LetterGrid.canSel = 1;
		LetterGrid.canChk = 0;
		LetterGrid.hiddenSubtraction = 1;
		LetterGrid.hiddenPlus = 1;
		//LetterGrid.addEventFuncName = "initBnfGrid";
		LetterGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert("项目权限信息列表初始化错误" + ex);
	}
}

// 项目权限信息列表的初始化
function initLetterGrid02()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="函件类型代码";         	  //列名
		iArray[1][1]="40px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		
		iArray[2]=new Array();
		iArray[2][0]="函件类型";         	  //列名
		iArray[2][1]="60px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="函件编号";         	  //列名
		iArray[3][1]="50px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]= 0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="函件流水号";         	  //列名
		iArray[4][1]="40px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="函件生成日期";         	  //列名
		iArray[5][1]="80px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]= 0;              			//是否允许输入,1表示允许，0表示不允许		
		
		iArray[6]=new Array();
		iArray[6][0]="最近打印日期"     	  //列名
		iArray[6][1]="80px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="打印人";         	  //列名
		iArray[7][1]="55px";            	//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
				
		iArray[8]=new Array();
		iArray[8][0]="letterType";         	  //列名
		iArray[8][1]="10px";            	//列宽
		iArray[8][2]=200;            			//列最大值
		iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[9]=new Array();
		iArray[9][0]="addressNo";         	  //列名
		iArray[9][1]="10px";            	//列宽
		iArray[9][2]=200;            			//列最大值
		iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[10]=new Array();
		iArray[10][0]="backFlag";         	  //列名
		iArray[10][1]="10px";            	//列宽
		iArray[10][2]=200;            			//列最大值
		iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[11]=new Array();
		iArray[11][0]="backDate";         	  //列名
		iArray[11][1]="10px";            	//列宽
		iArray[11][2]=200;            			//列最大值
		iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许

		LetterGrid = new MulLineEnter("fm", "LetterGrid"); 
		//这些属性必须在loadMulLine前
		LetterGrid.mulLineCount = 0;
		LetterGrid.displayTitle = 1;
		LetterGrid.locked = 1;
		LetterGrid.canSel = 1;
		LetterGrid.canChk = 0;
		LetterGrid.hiddenSubtraction = 1;
		LetterGrid.hiddenPlus = 1;
		//LetterGrid.addEventFuncName = "initBnfGrid";
		LetterGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert("项目权限信息列表初始化错误" + ex);
	}
}

</script>