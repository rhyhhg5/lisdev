<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskLetterTop.jsp
//程序功能：工单管理工单查看页面顶部悬浮操作按钮
//创建日期：2005-08-25
//创建人  : Yang Yaln
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String ttWorkNo = request.getParameter("workNo");
	String op = "";
	ExeSQL ttExeSQL = new ExeSQL();
	String tsql = "select 1 from LGWork where workno ='" + ttWorkNo + "'" +"and statusNo in ('4','5','6','7','8','99')";
	SSRS ttSSRS  = ttExeSQL.execSQL(tsql);
	if(ttSSRS.getMaxRow() > 0)
	{
			op = "disabled";	
	}
%> 
<html>
</head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskLetterTop.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>操作按钮 </title>
<head>
<body onload="">
<form name=fm action="./TaskPersonalBoxSave.jsp" target=fraSubmit method=post>
    <div style="display: ''">
    	<INPUT VALUE="新  建" TYPE=button Class="cssButton"  id="IDcreateLetter"  <%=op%> name="createLetter" onclick="create();">
		<INPUT VALUE="下  发" TYPE=button Class="cssButton" id="IDsendOutLetter" <%=op%> name="sendOutLetter" onclick="sendLetter();">
		<INPUT VALUE="回  销" TYPE=button Class="cssButton" id="IDtakeBackLetter" <%=op%> name="takeBackLetter" onclick="takeBack();">
		<INPUT VALUE="强制回销" TYPE=button Class="cssButton" <%=op%> name="forceTakeBackLetter" onclick="forceTakeBack();">
		<INPUT VALUE="察看扫描/取消" TYPE=button Class="cssButton"  id="IDforceTakeBackLetter" name="viewScanLetter" onclick="viewScan();">
		<INPUT VALUE="打  印" TYPE=button Class="cssButton" id="IDprint" name="print" onclick="printLetter();">
		<INPUT VALUE="删  除" TYPE=button Class="cssButton" id="IDdelete" <%=op%> name="delete" onclick="deleteLetter();">
    </div>
</form>
</body>
</html>