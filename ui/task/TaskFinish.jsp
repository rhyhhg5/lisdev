<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskFinish.jsp
//程序功能：工单管理工单结案页面
//创建日期：2005-01-19 18:12:42
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskFinish.js"></SCRIPT>
  <%@include file="TaskFinishInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单转交</title>
</head>
<body  onload="initForm();" >
  <form action="" method="post" name="fm1">
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskInfo);">
	      </td>
        <td class= titleImg>工单信息&nbsp;</td>
	    </tr>
    </table>
  <Div id="divTaskInfo" style="display: ''"> 
    <table class=common>
      <tr class=common>
        <TD class= title> 作业号 </TD>
        <TD class= input>
	    	<Input class="common" name="WorkNo" readonly> 
	    </TD>  
        <TD class= title> 受理号 </TD>
        <TD class= input>
      		<Input class="common" name="AcceptNo" readonly>
        </TD>
      </tr>
      <tr class=common>
        <TD class= title> 客户号 </TD>
        <TD class= input>
	    	<Input class="common" name="CustomerNo" readonly> 
	    </TD>  
        <TD class= title> 合同号</TD>
        <TD class= input>
      		<Input class="common" name="ContNo" readonly>
        </TD>
      </tr>
      <tr>
        <TD class= title> 业务类型</TD>
        <TD class= input>
        	<input class="common" name="TypeNo" readonly>
        </TD> 
        <TD class= title> 时限</TD>
        <TD class= input>
        	<input class="common" name="DateLimit" readonly>
        </TD>
      </tr>
      <tr>
        <TD class= title> 申请人 </TD>
        <TD class= input>
        	<input class="common" name="ApplyTypeNo" readonly>
        </TD> 
        <TD class= title> 优先级别 </TD>
        <TD class= input>
        	<input class="common" name="PriorityNo" readonly>
        </TD>
      </tr>
      <tr>
        <TD class= title> 受理途径 </TD>
        <TD class= input>
        	<input class="common" name="AcceptWayNo" readonly>
        </TD>
        <TD class= title>受理日期 </TD>
        <TD class= input>
        	<Input class="common" name="AcceptDate" readonly>
        </TD>    
      </tr>
      <tr>
        <TD class= title> 受理机构</TD>
        <TD class= input>
            <Input class="common" name="AcceptWayNo" readonly>
        </TD>
        <TD class= title> 受理人 </TD>
        <TD class= input>
        	<Input class="common" name="Acceptor" readonly>
        </TD>    
      </tr>
      <tr>
        <TD  class= title> 备注</TD>
        <TD colspan="3"  class= input>
        	<textarea name="Remark" cols="60" rows="5" readonly></textarea> 
        </TD>
      </tr>
    </table>
   </Div>
   </form>
   
 <form action="./TaskFinishSave.jsp" method="post" name="fm2" target="fraSubmit">
   <Input type="hidden" class= common name="WorkNo">
    <br>
    <table>
      <tr>
        <td class=common>
	      	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskSend);">
	      </td>
        <td class= titleImg>工单结案&nbsp;</td>
	    </tr>
    </table>
  <Div id="divTaskSend" style="display: ''"> 
    <table class= common>
      <tr>
        <td class= title> 结案日期</td>
        <td class= input> 
			<Input class= common name=SendTime readonly></td>
      </tr>
      <tr class= common> 
        <td  class= title> 结案意见</td>
        <td colspan="3"  class= input>
		<textarea name="RemarkContent" cols="60" rows="5"></textarea> 
        </td>
      </tr>
    </table>
    	<input type="button" class=cssButton name="Send" value="结  案" onclick="submitForm();">&nbsp;
		<input type="button" class=cssButton name="Return" value="返  回" onclick="javascript:history.back();">
	</div>
  </form>
 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
