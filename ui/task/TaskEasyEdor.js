//程序名称：TaskEasyEdor.js
//程序功能：简易保全
//创建日期：2005-04-03
//创建人  ：Yang yalin
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var showInfo;

//判断按下的是否是回车键
function isEnterDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if (keycode == "13")
	{
		return true;
	}
	else
	{
		return false;
	}
}

//按客户号查询
function queryByCustomerNo()
{
	if (isEnterDown())
	{
		var strSql = "select * from LDPerson where 1 = 1 " + 
				      getWherePart('CustomerNo');
		var arrResult = easyExecSql(strSql);
		if (arrResult)
		{
			afterQuery(arrResult);
		}
	}
}

//按证件号查询
function queryByIDNo()
{
	if (isEnterDown())
	{
		var strSql = "select * from LDPerson where 1 = 1 " + 
				      getWherePart('IDNo');
		var arrResult = easyExecSql(strSql);
		if (arrResult)
		{
			afterQuery(arrResult);
		}
	}
}

//选中下拉项后执行的操作
function afterCodeSelect(codeType)
{
	if (codeType == "CheckPostalAddress")
	{
		if (fm.CheckPostalAddress.value == "1") 
		{
			fm.all('PostalAddress').value = fm.all('CompanyAddress').value;
			fm.all('ZipCode').value =fm.all('CompanyZipCode').value;
			fm.all('Phone').value = fm.all('CompanyPhone').value;
			fm.all('Phone').value = fm.all('CompanyPhone').value;   
			fm.all('Fax').value = fm.all('CompanyFax').value;                
		}      
		else if (fm.CheckPostalAddress.value == "2") 
		{      
			fm.all('PostalAddress').value = fm.all('HomeAddress').value;
			fm.all('ZipCode').value = fm.all('HomeZipCode').value;
			fm.all('Phone').value = fm.all('HomePhone').value;
			fm.all('Fax').value = fm.all('HomeFax').value;                
		}
		else if (fm.CheckPostalAddress.value == "3") 
		{
			fm.all('PostalAddress').value = "";
			fm.all('ZipCode').value = "";
			fm.all('Phone').value = "";
			fm.all('Fax').value = "";
		}
	}
 	
	return true;
}

/* 查询客户基本信息 */
function queryCustomerInfo()
{
	var customerNo = fm.all("CustomerNo").value;
	
	var sql = "select Name, Sex, Birthday, IDType, IDNo, Marriage " + 
	          "from LDPerson " +
				    "where CustomerNo='" + customerNo + "'";
	var arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		return false;
	}
	try
	{
		fm.all("Name").value = arrResult[0][0];
		fm.all("Sex").value = arrResult[0][1];
		fm.all("Birthday").value = arrResult[0][2];
		fm.all("IDType").value = arrResult[0][3];
		fm.all("IDNoReadonly").value = arrResult[0][4];
		fm.all("Marriage").value = arrResult[0][5];
	}
	catch (ex)
	{
		alert(ex);
	}
}

/* 查询客户投保保单列表 */
function queryContGrid()
{
	var customerNo = fm.all("CustomerNo").value;
	
	strSQL = "select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, '', " +
	         "       a.PaytoDate, a.Prem, a.Amnt, a.AppFlag " +
	         "from   LCCont a " +
	         "where  1 = 1 " +
	         "and    a.AppntNo = '" + customerNo + "' " +
	         "and    a.AppFlag = '1' ";
	turnPage.pageLineNum = 100;
	turnPage.queryModal(strSQL, ContGrid);
	return true;
}

/* 查看保单明细 */
function viewContInfo()
{
	var selNo = ContGrid.getSelNo();
	if (selNo) 
	{
		var	ContNo = ContGrid.getRowColData(selNo - 1, 1);
		var PrtNo = ContGrid.getRowColData(selNo - 1, 2);
		if (ContNo)
		{
			win = window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&IsCancelPolFlag=0&LoadFlag=TASK", "ContWin");
			win.focus();
		}
	}
	else
	{
		alert("请选择一个保单！"); 
	}
}

/* 查询客户地址信息 */
function queryAddressInfo()
{
	var selNo = ContGrid.getSelNo();
	if (selNo)
	{
		var	contNo = ContGrid.getRowColData(selNo - 1, 1);
		fm.ContNo.value = contNo; //当前保单号
		var strSQL = "select * from LCAddress " +
		             "where CustomerNo = '" + mCustomerNo + "' " +
		             "and   AddressNo = (select AddressNo from LCAppnt where ContNo = '" + contNo + "') ";
		var arrResult = easyExecSql(strSQL);
		if (!arrResult)
		{
			alert("未找到客户保单地址信息！");
			return false;
		}
		try
		{
			fm.all("PostalAddress").value = arrResult[0][2];		
			fm.all("ZipCode").value = arrResult[0][3];
			fm.all("Phone").value = arrResult[0][4];
			fm.all("Fax").value = arrResult[0][5];
			fm.all("HomeAddress").value = arrResult[0][6];
			fm.all("HomeZipCode").value = arrResult[0][7];
			fm.all("HomePhone").value = arrResult[0][8];
			fm.all("HomeFax").value = arrResult[0][9];
			fm.all("CompanyAddress").value = arrResult[0][10];
			fm.all("CompanyZipCode").value = arrResult[0][11];
			fm.all("CompanyPhone").value = arrResult[0][12];
			fm.all("CompanyFax").value = arrResult[0][13];
			fm.all("Mobile").value = arrResult[0][14];
			fm.all("E-MAIL").value = arrResult[0][16];
		}
		catch (ex)
		{
			alert(ex);
		}
		//默认关联选中的保单
		var contNo = ContGrid.getRowColData(selNo - 1, 1);
		LCContGrid.checkBoxAllNot(this, LCContGrid.colCount);
		for(var i = 0; i < LCContGrid.mulLineCount; i++)
		{	
			if(LCContGrid.getRowColData(i, 1) == contNo)
			{
				LCContGrid.checkBoxSel(i + 1);
				return;
			}
		}
	}
}

//查询关联保单
function queryLCCont()
{
	var sql = "select a.ContNo, b.PostalAddress, b.ZipCode " +
	          "from LCCont a, LCAppnt c left join  LCAddress b on c.AddressNo = b.AddressNo  " +
	          "where b.CustomerNo = a.AppntNo " + 
	          "		and a.ContNo = c.ContNo " +
	          "		and c.AddressNo = b.AddressNo " +
	          "		and a.AppntNo = '" + mCustomerNo + "' " +
	          "		and a.Appflag = '1' " +
	          "order by contNo ";
	turnPage.pageLineNum = 100;
	turnPage.queryModal(sql, LCContGrid); 
}

/* 设置客户号,由客户查询页面返回时调用 */
function afterQuery(arrQueryResult)
{
	fm.all("CustomerNo").value = arrQueryResult[0][0]; 
	fm.all("IDNo").value = arrQueryResult[0][5]; 
	mCustomerNo = arrQueryResult[0][0]; 
	queryCustomerInfo();
	queryContGrid();
	queryAddressInfo();
	queryLCCont();
	showCodeName();
}  


//校验输入数据的合法性
function beforeSubmit()
{
	if (!verifyInput2()) 
	{
		return false;
	}	
	if ((fm.all("TopTypeNo").value == "03") && (fm.all("CustomerNo").value == ""))
	{
		alert("客户号不能为空！");
		fm.all("CustomerNo").focus();
		return false;
	}

//	var contNoQuery = "select contNo " +
//										"from LCAppnt " +
//										"where appntNo='" + fm.CustomerNo.value + "'" +
//										" and AppFlag = '1'";
//
//	var result = easyExecSql(contNoQuery);
//	if(!result)
//	{
//		alert("该用户没有有效保单,不能执行简易保全");
//		return false;
//	}
//	else
//	{
//		fm.ContNo.value = result[0][0];
//	}
	
	//检查是否选择一个保单
	if (ContGrid.getSelNo() == 0)
	{
		alert("请选择一个保单！");
		return false;
	}
	
	//检查是否有选择关联保单
  var checked = false;
  for (i = 0; i < LCContGrid.mulLineCount; i++)
  {
  	if (LCContGrid.getChkNo(i)) 
		{
			checked = true;
			break;
		}
	}
	if (checked == false)
	{
		alert("请选择关联保单！");
		return false;
	}
	
	return true;
}

//提交数据
function submitForm()
{ 
  if (beforeSubmit())
   {
   
	if (window.confirm("确认保存变更?"))
	{
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, 
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm.submit();
	}
	}
}

//提交数据后执行的操作
function afterSubmit(FlagStr, content, edorAcceptNo)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		//清除初始化的数据
		clearTaskInfo();
		clearCustomerInfo();
		ContGrid.clearData();
		clearAddressInfo();
		LCContGrid.clearData();
	}
	
	//window.location.reload();
}

//清除工单信息
function clearTaskInfo()
{
	fm.CustomerNo.value = "";
	fm.IDNo.value = "";
	fm.PriorityNo.value = "";
	fm.TopTypeNo.value = "03";
	fm.TypeNo.value = "";
	fm.DateLimit.value = "";
	fm.ApplyName.value = "";
	fm.ApplyTypeNo.value = "";
	fm.AcceptWayNo.value = "";
	fm.Remark.value = "";
}

/* 清除客户基本信息 */
function clearCustomerInfo()
{
	fm.all("Name").value = "";
	fm.all("Sex").value = "";
	fm.all("Birthday").value = "";
	fm.all("IDType").value = "";
	fm.all("IDNoReadonly").value = "";
	fm.all("Marriage").value = "";
}

//清除客户地址信息
function clearAddressInfo()
{
	fm.all("PostalAddress").value = "";		
	fm.all("ZipCode").value = "";
	fm.all("Phone").value = "";
	fm.all("Fax").value = "";
	fm.all("HomeAddress").value = "";
	fm.all("HomeZipCode").value = "";
	fm.all("HomePhone").value = "";
	fm.all("HomeFax").value = "";
	fm.all("CompanyAddress").value = "";
	fm.all("CompanyZipCode").value = "";
	fm.all("CompanyPhone").value = "";
	fm.all("CompanyFax").value = "";
	fm.all("Mobile").value = "";
	fm.all("E-MAIL").value = "";
}