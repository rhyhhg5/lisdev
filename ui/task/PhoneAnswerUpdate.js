//程序名称：PhoneInterview.js
//程序功能：程序功能：工单管理工单收回页面
//创建日期：2005-03-31
//创建人  ：Yang Yallin
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var showInfo;
var total = 1;	//已展开的问题总数

//显示但前问题的提示信息
function showCue(str)
{
	var n = parseInt(str.substring(3, str.length ));	//得到当前题号
	var div = "div" + (n + 1);
	
	total = n;
/*	
	if(document.all(div).style.display == "")
	{
		alert("次操作将取消问题 " + n + " 以下的答案及备注，恢复请点击问题 " + n + " 的确定按钮");
	}
*/
	if(n < 3)
	for(var i = n + 1; i < 11; i++)
	{
		document.all("div" + i).style.display="none";
	}
	document.all(str).style.display="";
}

//显示下一问题
function nextOne(str)
{
	var n = str.substring(3, str.length );	//得到下一题号
	total = n;	
	
	if(n < 3)
	{
		hideCue("cue" + (n - 1));	//隐藏当前的提示语	
		document.all(str).style.display="";	//显示下一题
	}
	else
	{
		hideCue("cue" + (n - 1));	//隐藏当前的提示语	
		
		for(var i = 3; i < 11; i++)
		{
			var s = "div" + i;
			document.all(s).style.display="";	//显示下一题
		}
	}
}

//隐藏提示信息
function hideCue(str)
{
	document.all(str).style.display="none";
}

function invokeSubmit(workNo, tTypeNo)
{	
	submitForm(workNo, tTypeNo);
	return true;
}

//提交数据
function submitForm()
{	
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//location.reload();
		
		top.close();
		top.opener.focus();
		top.opener.location.reload();
	}
}  

function setContInfoForInterview(contNo)
{
  var sql = "select appntName, appntSex from LCCont where contNo = '" + contNo + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    //问题一
    document.all("question1").innerHTML 
      = "一、您好，请问您是" + result[0][0] 
        + (result[0][1] == "0" ? "先生" : "女士/小姐") + "吗？";
  }
  
  setLCPolInfo(contNo);
  
  //联系方式,问题9
  sql = "  select b.PostalAddress, b.ZipCode "
        + "from LCAppnt a, LCAddress b "
        + "where a.appntNo = b.customerNo "
        + "   and a.addressNo = b.addressNo "
        + "   and a.contNo = '" + contNo + "' ";
  result = easyExecSql(sql);
  if(result)
  {
    document.all("question9").innerHTML 
      = "九、为了方便以后联系，再与您确认一下，您的联系地址是 " + result[0][0] 
        + " 邮编是 " + result[0][1] + " 吗？";
  }
}

//设置险种信息
function setLCPolInfo(contNo)
{
  //险种信息: 问题6
  var sql = "  select a.insuredName, b.riskCode, b.riskName, a.prem, a.payYears "
        + "from LCPol a, LMRisk b "
        + "where a.riskCode = b.riskCode "
        + "   and a.contNo = '" + contNo + "' "
        + "order by insuredName";

  turnPage.queryModal(sql, LCPolGrid);
}