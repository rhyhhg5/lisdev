<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskPersonalBoxSava.jsp
//程序功能：工单管理取消待办页面
//创建日期：2005-02-24 15:54:20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
<%
	String FlagStr = "Succ";
	String Content = "";
	VData tVData = new VData();
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	String[] tWorkNo = request.getParameterValues("TaskGridChk");
	String tAccpetNo = request.getParameter("AcceptNo");
	System.out.println("ACCEPT" + tAccpetNo);
	tVData.add(tGI);
	tVData.add(tWorkNo);
	tVData.add(tAccpetNo);
	TaskUniteBL tTaskUniteBL = new TaskUniteBL();
	if (tTaskUniteBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
	}
	else
	{
		VData tRet = tTaskUniteBL.getResult();
		String tAcceptNo = (String) tRet.getObject(0);
		//设置显示信息
		Content = "工单合并成功，合并后的受理号为：" + tAcceptNo;
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit3("<%=FlagStr%>", "<%=Content%>");
</script>
</html>