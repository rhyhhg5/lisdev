<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskPersonalBoxSava.jsp
//程序功能：工单管理取消待办页面
//创建日期：2005-02-24 15:54:20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	String tUrl = "";
	
	VData tVData = new VData();
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	String[] tWorkNo = request.getParameterValues("TaskGridChk");
	System.out.println(tWorkNo.length);

	tVData.add(tGI);
	tVData.add(tWorkNo);
	TaskUnWaitBL tTaskUnWaitBL = new TaskUnWaitBL();
	if (tTaskUnWaitBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
	}
	else
	{
		//设置显示信息
		Content = "取消待办成功";
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit1("<%=FlagStr%>","<%=Content%>");
</script>
</html>

