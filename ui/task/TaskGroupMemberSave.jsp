<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskGroupMemberSave.jsp
//程序功能：
//创建日期：2005-02-23 10:20:45
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
	//接收信息，并作校验处理。
	//输入参数
	LGGroupMemberSchema tLGGroupMemberSchema   = new LGGroupMemberSchema();
	OLGGroupMemberUI tOLGGroupMemberUI   = new OLGGroupMemberUI();
	
	LGWorkBoxSchema tLGWorkBoxSchema   = new LGWorkBoxSchema();
  LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
  OLGWorkBoxUI tOLGWorkBoxUI   = new OLGWorkBoxUI();
  String tWorkBoxNo = null;
	
	//输出参数
	CErrors tError = null;
	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	
	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");
	tLGGroupMemberSchema.setGroupNo(request.getParameter("GroupNo"));
	tLGGroupMemberSchema.setMemberNo(request.getParameter("MemberNo"));
	tLGGroupMemberSchema.setpostNo(request.getParameter("PostNo"));
	if(tLGGroupMemberSchema.getpostNo()!= null && !"null".equals(tLGGroupMemberSchema.getpostNo()) && "PA04".equals(tLGGroupMemberSchema.getpostNo()))
	{
	  tLGGroupMemberSchema.setSuperManager(request.getParameter("SuperManager1"));
	  tLGGroupMemberSchema.setSuperManager2(request.getParameter("SuperManager2"));
	  tLGGroupMemberSchema.setSuperManager3(request.getParameter("SuperManager3"));
	}
	else
	{
	  tLGGroupMemberSchema.setSuperManager(request.getParameter("SuperManager"));
	  tLGGroupMemberSchema.setSuperManager2(request.getParameter("SuperManager"));
	  tLGGroupMemberSchema.setSuperManager3(request.getParameter("SuperManager"));
	}
	try
	{
	// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tLGGroupMemberSchema);
		tVData.add(tG);
		tOLGGroupMemberUI.submitData(tVData,transact);
	}
	catch(Exception ex)
	{
	  Content = "保存失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
	}
  
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
	  tError = tOLGGroupMemberUI.mErrors;
	  if (!tError.needDealError())
	  {                          
	  	Content = " 保存成功! ";
	  	FlagStr = "Success";
	  }
	  else                                                                           
	  {
	  	Content = " 保存失败，原因是:" + tError.getFirstError();
	  	FlagStr = "Fail";
	  }
	}
	
	//----------------------------------------------------------------------
	  
  if(FlagStr.equals("Success"))
  {    
    //信箱编号
    tLGWorkBoxDB.setOwnerNo(request.getParameter("MemberNo"));
    LGWorkBoxSet tLGWorkBoxSet = tLGWorkBoxDB.query(); 
    
         if(tLGWorkBoxSet==null||tLGWorkBoxSet.size()==0)
         {
           if (transact.equals("INSERT||MAIN"))
           {
  	         tWorkBoxNo = PubFun1.CreateMaxNo("TASKBOX", 6);
             tLGWorkBoxSchema.setWorkBoxNo(tWorkBoxNo);
             tLGWorkBoxSchema.setOwnerTypeNo("2");
             tLGWorkBoxSchema.setOwnerNo(request.getParameter("MemberNo"));
	           System.out.println("WorkBoxNo:"+tWorkBoxNo);
	           System.out.println("OwnerNo:"+request.getParameter("MemberNo"));
             try
             {
                 // 准备传输数据 VData
  	             VData tVData1 = new VData();
	               tVData1.add(tLGWorkBoxSchema);
  	             tVData1.add(tG);
                 tOLGWorkBoxUI.submitData(tVData1,transact);
             }
             catch(Exception ex)
             {
                 Content = "保存失败，原因是:" + ex.toString();
                 FlagStr = "Fail";
             }
           }
         }
         else
	     {
	         Content = "用户" + request.getParameter("MemberNo") + "已有个人信箱,编号为" + tLGWorkBoxSet.get(1).getWorkBoxNo();
    	     FlagStr = "Fail";
	     } 
  }
	   if (!FlagStr.equals("Fail"))
     {
         tError = tOLGWorkBoxUI.mErrors;
         if (!tError.needDealError())
         {                          
    	       Content = " 保存成功! ";
    	       FlagStr = "Success";
    	       if (transact.equals("INSERT||MAIN"))
    	       {
    		         VData tRet1 = tOLGWorkBoxUI.getResult();
  			         tLGWorkBoxSchema.setSchema((LGWorkBoxSchema) tRet1.getObjectByObjectName("LGWorkBoxSchema", 0));
    		         Content += "新增信箱编号为" + tLGWorkBoxSchema.getWorkBoxNo(); 
    	       }
          }
       } 
	//----------------------------------------------------------------------
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=transact%>");
</script>
</html>
