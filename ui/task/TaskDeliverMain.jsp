<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	//如果有复选框用复选框传参数,否则用隐藏域传参数
	//用CheckBox来判断是否是从checkbox中接收参数
	String CheckBox = request.getParameter("CheckBox");
	String tWorkNo = request.getParameter("WorkNo");
	String[] WorkNo;
	String callFlag = request.getParameter("callFlag");
	
	if ((CheckBox != null) && (CheckBox.equals("YES")))
	{
		WorkNo = request.getParameterValues("TaskGridChk");
	}
	else if ((tWorkNo != null) && (!tWorkNo.equals("")))
	{
		WorkNo = new String[1];
		WorkNo[0] = tWorkNo;
	}
	else
	{
		WorkNo = new String[1];
		WorkNo[0] = (String) session.getAttribute("VIEWWORKNO");
	}
	
	if (WorkNo != null)
	{
		session.setAttribute("WORKNO", WorkNo);
	}
%>

<html>
<head>
<title>工单转交</title>
<script language="javascript">
	//var intPageWidth = 730;
	//var intPageHeight = 600;
	//window.resizeTo(intPageWidth, intPageHeight);
	window.focus();
</script>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="yes" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="0,*,0" cols="*">
		<!--菜单区域-->
		<frame name="fraMenu" scrolling="yes" noresize src="about:blank">
		<!--交互区域-->
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="./TaskDeliver.jsp?callFlag=taskPesonalBox">
    	<!--下一步页面区域-->
    	<frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
