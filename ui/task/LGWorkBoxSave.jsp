<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LGWorkBoxSave.jsp
//程序功能：
//创建日期：2005-02-23 09:59:55
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.task.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LGWorkBoxSchema tLGWorkBoxSchema   = new LGWorkBoxSchema();
  LGWorkBoxDB tLGWorkBoxDB = new LGWorkBoxDB();
  OLGWorkBoxUI tOLGWorkBoxUI   = new OLGWorkBoxUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  String tWorkBoxNo;
  //一个人只能有一个信箱 qulq 2007-3-30 
  tLGWorkBoxDB.setOwnerNo(request.getParameter("OwnerNo"));
  System.out.println("...............transact"+transact);
  LGWorkBoxSet tLGWorkBoxSet = tLGWorkBoxDB.query();
  if(tLGWorkBoxSet.size()!=0&&transact.equals("INSERT||MAIN"))
  {	    
  		Content = "保存失败，原因是: 该用户已有信箱，不能新建" ;
    	FlagStr = "Fail";
  }
else
	{
  if (transact.equals("INSERT||MAIN"))
  {
  	tWorkBoxNo = PubFun1.CreateMaxNo("TASKBOX", 6);
  }
  else
  {
  	tWorkBoxNo = request.getParameter("WorkBoxNo");
  }
  
    tLGWorkBoxSchema.setWorkBoxNo(tWorkBoxNo);
    tLGWorkBoxSchema.setOwnerTypeNo(request.getParameter("OwnerTypeNo"));
    tLGWorkBoxSchema.setOwnerNo(request.getParameter("OwnerNo"));
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.add(tLGWorkBoxSchema);
  	tVData.add(tG);
    tOLGWorkBoxUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
	}
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tOLGWorkBoxUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    	if (transact.equals("INSERT||MAIN"))
    	{
    		VData tRet = tOLGWorkBoxUI.getResult();
  			tLGWorkBoxSchema.setSchema((LGWorkBoxSchema) tRet.getObjectByObjectName("LGWorkBoxSchema", 0));
    		Content += "新增信箱编号为" + tLGWorkBoxSchema.getWorkBoxNo(); 
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=transact%>");
</script>
</html>
