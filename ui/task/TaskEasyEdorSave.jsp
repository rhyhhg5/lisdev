<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskEasyEdorSave.jsp
//程序功能：
//创建日期：2005-04-12
//创建人  ：Yang Yalin
//更新记录：更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.bq.mm.face.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.utility.*"%>  
<%  
    
    GlobalInput tG = (GlobalInput)session.getValue("GI");            
    String flag = "";
    String content = "";

	
  
  //得到业务类型
	String tTypeNo = request.getParameter("TypeNo");
	if ((tTypeNo == null) || (tTypeNo.equals("")))
	{
		tTypeNo = request.getParameter("TopTypeNo");
	}
  
  //得到工单信息
  LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	tLGWorkSchema.setCustomerNo(request.getParameter("CustomerNo"));
	tLGWorkSchema.setCustomerCardNo(request.getParameter("IDNo"));
	tLGWorkSchema.setPriorityNo(request.getParameter("PriorityNo"));
	tLGWorkSchema.setTypeNo(tTypeNo);
	tLGWorkSchema.setDateLimit(request.getParameter("DateLimit"));
	tLGWorkSchema.setApplyTypeNo(request.getParameter("ApplyTypeNo"));
	tLGWorkSchema.setApplyName(request.getParameter("ApplyName"));
	tLGWorkSchema.setAcceptWayNo(request.getParameter("AcceptWayNo"));
	tLGWorkSchema.setAcceptDate(request.getParameter("AcceptDate"));
	tLGWorkSchema.setRemark(request.getParameter("Remark"));
	
	//得到客户投保信息
	LPAppntSchema tLPAppntSchema = new LPAppntSchema();
	tLPAppntSchema.setContNo(request.getParameter("ContNo"));
	tLPAppntSchema.setAppntNo(request.getParameter("CustomerNo"));
  	 
	//得到客户保单联系地址
	LCAddressSchema tLCAddressSchema = new LCAddressSchema();
	tLCAddressSchema.setCustomerNo(request.getParameter("CustomerNo"));  
	tLCAddressSchema.setHomeAddress(request.getParameter("HomeAddress"));
	tLCAddressSchema.setHomeZipCode(request.getParameter("HomeZipCode"));
	tLCAddressSchema.setHomePhone(request.getParameter("HomePhone"));
	tLCAddressSchema.setHomeFax(request.getParameter("HomeFax"));
	tLCAddressSchema.setCompanyAddress(request.getParameter("CompanyAddress"));
	tLCAddressSchema.setCompanyZipCode(request.getParameter("CompanyZipCode"));
	tLCAddressSchema.setCompanyPhone(request.getParameter("CompanyPhone"));
	tLCAddressSchema.setCompanyFax(request.getParameter("CompanyFax"));
	tLCAddressSchema.setPostalAddress(request.getParameter("PostalAddress"));
	tLCAddressSchema.setZipCode(request.getParameter("ZipCode"));
	tLCAddressSchema.setPhone(request.getParameter("Phone"));
	tLCAddressSchema.setFax(request.getParameter("Fax"));
	tLCAddressSchema.setMobile(request.getParameter("Mobile"));
	tLCAddressSchema.setEMail(request.getParameter("E-MAIL"));
	
	//得到关联保单
	LPContSet tLPContSet = new LPContSet();
	String tContNo[] = request.getParameterValues("LCContGrid1");       
	String tChk[] = request.getParameterValues("InpLCContGridChk");
	for(int i = 0; i < tChk.length; i++)
	{
	  if (tChk[i].equals("1"))
	  {
      LPContSchema tLPContSchema = new LPContSchema();
      tLPContSchema.setEdorNo(request.getParameter("EdorNo"));
      tLPContSchema.setEdorType(request.getParameter("EdorType")); 
      tLPContSchema.setContNo(tContNo[i]); 
      tLPContSet.add(tLPContSchema);
	  }           
	}   
  		
	VData data = new VData();
	data.add(tG);
	data.add(tLGWorkSchema);
	data.add(tLPAppntSchema);
	data.add(tLCAddressSchema);
	data.add(tLPContSet);
	
	EasyEdorUI tEasyEdorUI = new EasyEdorUI();
	if (!tEasyEdorUI.submitData(data))
	{
		flag = "Fail";
		content = "修改客户联系地址失败！" + tEasyEdorUI.getError();
	}
	else
	{
		flag = "Succ";
		content = "修改客户联系地址成功！生成的工单号为" + tEasyEdorUI.getEdorAcceptNo() + "的受理。";
	}
	content = PubFun.changForHTML(content);
	String edorAcceptNo = tEasyEdorUI.getEdorAcceptNo();
	
	
	//<!-- 增加短信邮件通知功能之联系方式变更 -->
    //获得界面的更改信息 
    IMMControl control = new MMControlImpl();
     if(control.isOpen("G","BQ")){
	    String HomePhone = request.getParameter("HomePhone") ;
	    String CompanyPhone = request.getParameter("CompanyPhone") ;
	    String Phone = request.getParameter("Phone") ;
	    String Mobile = request.getParameter("Mobile") ;
	    String CustomerNo = request.getParameter("CustomerNo") ;
	    String sql = "select ld.AppntName,ld.contNo,lc.Email,lc.mobile  from lcAppnt ld,LCAddress lc " 
	                    +"where AppntNo = '"+CustomerNo+"' and CustomerNO = '"+CustomerNo+"'";
	    SSRS ssrs = new ExeSQL().execSQL(sql);
	    String content1="";
	    String content2="";
	    String content3="";
	    String content4="";
	    String content5="";
	    //设置短信内容
	    //System.out.println(HomePhone.trim()+"55555555555555555555555555555555555555555555555555555555555555555555555");
	    LIWaitSendMessageSchema soonSend = new LIWaitSendMessageSchema() ;
	    if(!HomePhone.trim().equals("")){
	          content1 = "[ 尊敬的 "+ssrs.GetText(1,1)+" 先生/女士，根据您的申请，本公司已将您 "+ssrs.GetText(1,2)+"号保单的家庭电话改为"+HomePhone+" ]";
	          
	    }
	    if(!CompanyPhone.trim().equals("")){
	          content2 = "[ 尊敬的 "+ssrs.GetText(1,1)+" 先生/女士，根据您的申请，本公司已将您 "+ssrs.GetText(1,2)+"号保单的公司电话改为"+CompanyPhone+" ]";
	          
	    }
	    if(Phone.trim().trim().equals("")){
	          content3 = "[ 尊敬的 "+ssrs.GetText(1,1)+" 先生/女士，根据您的申请，本公司已将您 "+ssrs.GetText(1,2)+"号保单的电话改为"+Phone+" ]";
	          
	    }
	    if(Mobile.trim().trim().equals("")){
	          content4 = "[ 尊敬的 "+ssrs.GetText(1,1)+" 先生/女士，根据您的申请，本公司已将您 "+ssrs.GetText(1,2)+"号保单的移动电话改为"+Mobile+" ]";
	          
	    }
	    
	    
	    
		soonSend.setBussNo(tEasyEdorUI.getEdorAcceptNo());
		soonSend.setCustomerNo(CustomerNo) ;
		soonSend.setEmail(ssrs.GetText(1,3)) ;
		soonSend.setFunctionCode("G");//简易保全
	    soonSend.setMakeDate(PubFun.getCurrentDate()) ;
		soonSend.setMakeTime(PubFun.getCurrentTime()) ;
		//这里主键是怎么生成
		soonSend.setMessageId(PubFun1.CreateMaxNo("C0000",4)) ;
		soonSend.setMobile(ssrs.GetText(1,4)) ;
		soonSend.setModelCode("BQ") ;
		soonSend.setModifyDate(PubFun.getCurrentDate()) ;
		soonSend.setModifyTime(PubFun.getCurrentTime());
		soonSend.setName(ssrs.GetText(1,1)) ;
		soonSend.setOperator(tG.Operator) ;
		soonSend.setSendTime(PubFun.getCurrentTime()) ;
		soonSend.setSendDate(PubFun.getCurrentDate());
		ICommonMM mm = new CommonMMImpl() ;
		try{
		    //mm.sendMMSoon(tG,soonSend) ;
		    //插入到待发信息表里
		    if(content1 != ""){
		        soonSend.setMessageId(PubFun1.CreateMaxNo("C0000",15)) ;
			    soonSend.setContent(content1) ;
			    mm.sendMMLate(tG,soonSend) ;
		    }
		    if(content2 != ""){
		        soonSend.setMessageId(PubFun1.CreateMaxNo("C0000",15)) ;
			    soonSend.setContent(content2) ;
			    mm.sendMMLate(tG,soonSend) ;
		    }
		    if(content3 != ""){
		        soonSend.setMessageId(PubFun1.CreateMaxNo("C0000",15)) ;
			    soonSend.setContent(content3) ;
			    mm.sendMMLate(tG,soonSend) ;
		    }
		    if(content4 != ""){
		        soonSend.setMessageId(PubFun1.CreateMaxNo("C0000",15)) ;
			    soonSend.setContent(content4) ;
			    //应该把现在的信息发送到已经更改的移动电话号码上去
			    soonSend.setEmail(Mobile) ;
			    mm.sendMMLate(tG,soonSend) ;
		    }
		    
		 }catch(Exception e){
		    e.printStackTrace() ;
		 }
	   }
    
    
    //<!-- 增加短信邮件通知功能之联系方式变更 完毕 -->
	
	
%>
<html>
<script language="javascript">
		parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>", "<%=edorAcceptNo%>");
</script>
</html>
