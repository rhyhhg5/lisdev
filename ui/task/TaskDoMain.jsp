<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskMain.jsp
//程序功能：工单管理框架页面
//创建日期：2005-04-03
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	//得到Url
	String pageUrl;
	String pageName = request.getParameter("pageName");
	String param = request.getParameter("param");
	if ((pageName != null) && (!pageName.equals("")))
	{
		if (param == null)
		{
			param = "";
		}
		param = param.replace('|', '&');
		pageUrl = pageName + '?' + param;
	}
	else
	{
		pageUrl = "about:blank";
	}
%>
<html>
<head>
<title> 经办 </title>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="no" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="0,0,0,*" cols="*">
    	<!--菜单区域-->
	    <frame id="fraMenu" name="fraMenu" noresize scrolling="no" src="about:blank">
	    <!--交互区域-->	    
		<frame id="common" name="common" scrolling="auto" src="about:blank">
		<frame id="picture" name="picture" scrolling="auto" src="about:blank">
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="<%=pageUrl%>">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
