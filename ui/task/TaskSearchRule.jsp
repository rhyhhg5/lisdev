<%
//程序名称：
//程序功能：功能描述
//创建日期：2005-05-27 10:20:45
//创建人  ：Yang Yalin
//更新人  ：  
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="TaskSearchRule.js"></SCRIPT>
  <%@include file="TaskSearchRuleInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm action="" target=fraSubmit>
  
  <!-- 导入按钮界面 -->
  <%@include file="../common/jsp/InputButton.jsp"%>
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLGGroupMemberGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      规则内容
    </TD>
    <TD  class= input>
      <input class="common" name="RuleContent">
    </TD>
    <TD  class= title>
      规则类型
    </TD>
    <TD  class= input>
      <input class="codeNo" name="GoalType" value="" 
	        readOnly 
	        ondblclick="return showCodeList('ruletype',[this,GoalTypeName],[0,1]);" 
	        onkeyup="return showCodeListKey('ruletype',[this,GoalTypeName],[0,1]);"><Input class="codeName" name="GoalTypeName" readonly>
    </TD>
    <TD  class= title>
      规则目标
    </TD>
    <TD  class= input>
      <input class="common" name="RuleTarget">
    </TD>
  </TR>
</table>
  </Div>
  
    <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="easyQueryClick();"> 
    <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="returnParent();">   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGGroupMember1);">
  		</td>
  		<td class=titleImg>
  			 小组成员信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLGGroupMember1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLGGroupMemberGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage(); showCodeName();"> 
    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage(); showCodeName();"> 					
    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage(); showCodeName();"> 
    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage(); showCodeName();">
  </Div>
  
  <INPUT CLASS=cssButton VALUE="设为默认" TYPE=button onclick="setAsDefault();">
  
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
