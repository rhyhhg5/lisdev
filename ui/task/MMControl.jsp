<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskGroupMember.jsp
//程序功能：
//创建日期：2005-02-23 09:59:55
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./MMControl.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body  onload="" >
  <form action="./MMControlSave.jsp" method=post name=fm target="_self">
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGWorkBox1);">
    		</td>
    		 <td class= titleImg>
        		 模块和功能点的相应信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLGWorkBox1" style= "display: ''">
		<table  class= common align='center' >
		  <TR  class= common>
		    <TD  class= title>
		     模块名称
		    </TD>
		    <TD  class= input>
		    <select name="ModelCode">
		        <option value="BQ">保全</option>
		        <!-- 
		        <option value="TB">契约</option>
		        <option value="HM">健管</option>
		        <option value="XQ">续期续保</option>
		        <option value="LA">销售</option>
		         -->
		    </select>
		    </TD>
		    <TD class= title>
		     功能点名称
		    </TD>
		    <TD  class= input>
		      <select name="FunctionCode">
		        <option value="A">A:寄发批单通知</option>
		        <option value="B">B:交费提醒</option>
		        <option value="C">C:保单失效通知</option>
		        <option value="D">D:转帐退费通知</option>
		        <option value="E">E:收费成功通知（保单服务）</option>
		        <option value="F">F:收费成功通知（续期续保）</option>
		        <option value="G">G:简易保全通知</option>
		        <option value="H">H:客户服务短信</option>
		    </select>
		    </TD>
		  </TR>
		  <TR  class= common>
		    <TD  class= title>
		      开 / 关
		    </TD>
		    <TD  class= input>
		       <select name="onOff">
		           <option value="1">开</option>
		           <option value="2">关</option>
		       </select>
		      <br>
		    </TD>
		  </TR>
		  <TR  class= common>
		    <TD  class= title>
		      保       存
		    </TD>
		    <TD  class= input>
		     <input class="cssButton" type="Submit" value="更  改">
		      <br>
		    </TD>
		  </TR>
		</table>
    </Div><br>
	</form>
</body>
</html>
