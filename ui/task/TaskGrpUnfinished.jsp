<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="TaskCheckPersonalBox.jsp"%>
<%
//程序名称：TaskGrpUnfinished.jsp
//程序功能：本小组成员未完成任务清单界面
//创建日期：2008-1-16
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskGrpUnfinished.js"></SCRIPT>
  <%@include file="TaskGrpUnfinishedInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>本小组成员未完成任务清单</title>
</head>
<body  onload="initForm();">
  <form name=fm target=fraSubmit>
    <input type="hidden" class= common name="WorkNo">
    <input type="hidden" class= commpn name="AcceptNo">
    <!-- 用CheckBox隐藏域来标记是否使用CheckBox传参数 -->
    <input type="hidden" class= commpn name="CheckBox" value="YES">
    <input VALUE="工单查看" TYPE=button Class="cssButton" name="view" onclick="viewTask();">
  <table>
    <tr>
      <td width="17">
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskList);">
      </td>
      <td width="185" class= titleImg>工单列表 </td>
    </tr>
  </table>
  <div  id= "divTaskList" style= "display: ''" align=center>
    <table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanTaskGrid" ></span></td>
      </tr>
    </table>
  </div>
  <div id="divPage2" align=center style="display: 'none' ">
    <input CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
    <input CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
    <input CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
    <input CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  </form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
