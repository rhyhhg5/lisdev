

//程序名称：TaskPostPurview.js
//程序功能：
//创建日期：2005-07-06
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;

var wherePart = "";
var sqlStr;
var stringForOrder = "";

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//构造查询Sql语句的Where部分
function createWherePart()
{
	if(fm.ProjectNo.value != null && fm.ProjectNo.value != "")
	{
		wherePart = wherePart 
					+ "	and ProjectNo='" + fm.ProjectNo.value + "' ";
	}
	if(fm.ProjectType.value != "9")
	{
		wherePart = wherePart 
					+ "	and ProjectType='" + fm.ProjectType.value + "' "
	}
	if(fm.DealOrganization.value != "9")
	{
		wherePart = wherePart
					+ "	and DealOrganization='" + fm.DealOrganization.value + "' ";
	}
	if(fm.PostNo.value != null && fm.PostNo.value != "")
	{
		wherePart = wherePart
					+ " and PostNo='" + fm.PostNo.value + "' ";
	}
	if(fm.MoneyPurview.checked == true && fm.ConfirmPurview.checked == false)
	{
		wherePart = wherePart
					+ " and ConfirmFlag='2' ";
	}
	if(fm.MoneyPurview.checked == false && fm.ConfirmPurview.checked == true)
	{
		wherePart = wherePart
					+ " and (ConfirmFlag='0' or ConfirmFlag='1') ";
	}
	if(fm.MemberNo.value != null && fm.MemberNo.value != "")
	{
		wherePart = wherePart
					+ " and MemberNo='" + fm.MemberNo.value + "' ";
	}
	if(fm.RiskCode.value != null && fm.RiskCode.value != "")
	{
		wherePart = wherePart
					+ " and RiskCode='" + fm.RiskCode.value + "' ";
	}
}

//改变排序方式
function changeOrder()
{
	var orderSql = stringForOrder.substring(0, stringForOrder.indexOf("order by ")) 
				  + "order by " + fm.orderFlag.value;
	
	turnPage.pageDivName = "divPage";
	turnPage.pageLineNum = 15;
	turnPage.queryModal(orderSql, PurviewGrid);   
	
	showCodeName();	  
	
	changePurviewFull(); //将录入时全权存为9999999999的值显示为全权
}

//按条件查询权限
function queryPurview()
{
	if(postNo == "pa03" || postNo == "pa05" || postNo == "pa06" || postNo == "pa07" || postNo == "pa08" || postNo == "pa09")
	{
		alert("对不起，您没有查询权限的权限。");
		return false;
	}
	initPurviewGrid();
	createWherePart();
	
	sqlStr = "";
	sqlStr =  sql
			+ wherePart
			+ "order by " + fm.orderFlag.value;
			
	turnPage.pageDivName = "divPage";
	turnPage.pageLineNum = 14;
	turnPage.queryModal(sqlStr, PurviewGrid);   
	
	showCodeName();
	
	wherePart = "";

	stringForOrder = sqlStr;
	
	//将录入时全权存为9999999999的值显示为全权
	changePurviewFull();
}

function updateDate()
{
	var row = PurviewGrid.getSelNo();
	if(row != null && row > 0)
	{
		var purviewNo = PurviewGrid.getRowColData(row - 1, 1);
	}
	else
	{
		alert("请选择一条记录");
		
		return false;
	}
	
	window.open("TaskPurviewUpdateMain.jsp?ProjectPurviewNo=" + purviewNo, "");
	
	return true;
}

function deleteData()
{
	var ProjectPurviewNo;
	var row = PurviewGrid.getSelNo() - 1;
	
	if(row > -1)
	{	
	 	ProjectPurviewNo = PurviewGrid.getRowColData(row, 1);
	}
	else
	{
		alert("请选择一条记录");
		return false;
	}
	
	fm.fmtransact.value = "DELETE||MAIN";
	fm.action = "TaskPostPurviewSave.jsp?ProjectPurviewNo=" + ProjectPurviewNo;
	
	submitForm();
}

function submitForm()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr, content)
{
	showInfo.close();
	window.onfocus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
  	if (fm.fmtransact.value == "DELETE||MAIN")
  	{
  	   content = "删除成功";
  	}
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		top.focus();
		window.location.reload();
	}	
}

//将录入时全权存为9999999999的值显示为全权
function changePurviewFull()
{
	for(var i = 0; i < PurviewGrid.mulLineCount; i++)
	{
		if(PurviewGrid.getRowColData(i, 9) == '9999999999')
		{
			PurviewGrid.setRowColData(i, 9, "全权");
		}
		if(PurviewGrid.getRowColData(i, 10) == '9999999999')
		{
			PurviewGrid.setRowColData(i, 10, "全权");
		}
		if(PurviewGrid.getRowColData(i, 11) == '9999999999')
		{
			PurviewGrid.setRowColData(i, 11, "全权");
		}
	}
}