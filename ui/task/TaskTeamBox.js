//程序名称：TaskPersonalBox.js
//程序功能：工单管理小组信箱页面
//创建日期：2005-01-24 14:10:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var mType = 0;

/* 改变查询条件 */
function changeCondition() 
{
	mType = Number(fm.condition.value);
}

/* 查询工单在列表中显示结果 */
function easyQueryClick()
{	
	//得到查询条件
	var partSQL = "";
	switch (mType) 
	{
		case 0:   //未经办/正经办/审核(默认)
			partSQL = "and w.StatusNo in ('2', '3', '4')";
			break;
		case 1:   //未经办/正经办/审核
			partSQL = "and w.StatusNo in ('2', '3', '4') ";
			break;
		case 2:  //未经办
			partSQL = "and w.StatusNo = '2' ";
			break;
		case 3:  //正经办
			partSQL = "and w.StatusNo = '3' ";
			break;
		case 4:  //审核
			partSQL = "and w.StatusNo = '4' ";
			break;
		case 5:  //待办（所有）
			partSQL = "and w.StatusNo in ('0', '1') ";
			break;
		case 6:  //待办（定期）
			partSQL = "and w.StatusNo = '0' ";
			break;
		case 7:  //待办（无期）
			partSQL = "and w.StatusNo = '1' ";
			break;
		case 8:  //结案
			partSQL = "and w.StatusNo = '5' ";
			break;
		case 9:  //存档
			partSQL = "and w.StatusNo = '6' ";
			break;
	    case 10: //过应办期
			partSQL = "and days(current date) - days(t.InDate) > w.DateLimit ";
			break;
		default:
		 	partSQL = "";
	}
	
	
	var partSQLStutes = "";
	var myStutesType = fm.condition1.value;
	if(myStutesType != "0")
	{
		partSQLStutes = " and w.TypeNo like ('"+myStutesType+"%') ";
	}
	
	//初始化表格
	initGrid();
	
	//查询SQL语句
	var strSQL;
	strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, w.TypeNo, w.CustomerNo, " +
			 "       (Select Name from LDPerson where CustomerNo = w.CustomerNo), '记录', w.AcceptorNo, w.AcceptCom, w.AcceptDate, " +
			 "       Case When w.DateLimit Is Null then '' Else  trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
			 "       w.StatusNo, '', '' " +
			 "from   LGWork w, LGWorkTrace t " +
	         "where  w.WorkNo = t.WorkNo " +
	      	 "and    w.NodeNo = t.NodeNo " +
			 "and    t.WorkBoxNo in " +
			 "      (select WorkBoxNo from LGWorkBox " + 
			 "       where  OwnerTypeNo = '1' " +  //小组信箱
			 "       and    OwnerNo in " +
			 "             (select GroupNo from LGGroupMember " +
			 "              where  MemberNo = '" + operator + "'" +
			 "              ) " +
			 "       ) " +   
			 partSQL +
			 partSQLStutes +
			 "order by t.InDate desc ";
  turnPage.queryModal(strSQL, TaskGrid);
	showCodeName();
	
	return true;
}

/* 得到被选中项 */
/*
function getCheckNo()
{
	var checkNo = 0;
	for (i=0; i<TaskGrid.mulLineCount; i++) 
	{
		if (TaskGrid.getSelNo(i)) 
		{  
		  	 checkNo = TaskGrid.getSelNo();
		  	 break;
		}
	}
	return checkNo;
}
*/

/* 查看选中的工单 */
//-------------------------------------------------------------------------------

function viewTask()
{
	var pageName = "TaskView.jsp";
	var param;
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	var DetalWorkNo;
	
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = TaskGrid.getRowColData(i, 1);
				CustomerNo = TaskGrid.getRowColData(i, 5);
				DetailWorkNo = WorkNo ;  
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	
	if (WorkNo)
	{
	    var width = screen.availWidth - 10;
	    var height = screen.availHeight - 28;
		win = window.open("TaskViewMain.jsp?loadFlag=PERSONALBOX&WorkNo="+WorkNo 
							+"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + DetailWorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);
		win.focus();
	}
}

//-------------------------------------------------------------------------------

/* 检查表单 */
function beforeSubmit()
{
	var tNum = fm.all("Num").value;
	if (tNum == '')
	{
		alert("请输入取单个数!");
		fm.all("Num").focus();
		return false;
	}
	if ((!isInteger(tNum)) || (Number(tNum) == 0))
	{
		alert("取单个数输入有误！");
		fm.all("Num").focus();
		return false;
	}
	if (Number(tNum) > TaskGrid.mulLineCount)
	{
		alert("取单数不能超过现有工单数！");
		fm.all("Num").focus();
		return false;
	}
	return true;
}

/* 提交表单 */
function submitForm()
{
	if (beforeSubmit())
	{
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm.submit();
	}
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		easyQueryClick();
		//location.reload();
	}
}    

function viewTeamMember()
{  	
	 var chkNum = 0;
	
	  for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  /*if (chkNum == 1)
			{
				WorkNo = TaskGrid.getRowColData(i, 1);
				CustomerNo = TaskGrid.getRowColData(i, 5);
				DetailWorkNo = WorkNo ;  
			}*/
		}
	}
	if (chkNum == 0)
	{
		alert("请选择你要分配的工单！");
		return false;
	}
	
	
	if (!checkMaster())
	{
	    return false;
	}
	
	 var width = screen.availWidth - 400;
	 var height = screen.availHeight - 300;
	showInfo = window.open("TaskTeamMember.jsp","","toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);
	showInfo.focus();
}

function checkMaster()
{ 
	   var strSQL = "select groupno from LGGroupMember where postno='PA05' and MemberNo='"+
	              operator+"'";
	   var arrResult = easyExecSql(strSQL);
	   if(arrResult == null)
	   {
	       alert("对不起，您不是组长，无法给小组成员分配工单!!");
	       return false;	  
	   }
	   return true;
}

function getCheckData()
{
    var checkData =new Array();
    var j =0;
    for(i=0; i<TaskGrid.mulLineCount; i++)
    {
    	 	if(TaskGrid.getChkNo(i))
        	{
        	    checkData[j++]= TaskGrid.getRowColData(i,2);
        	}
    }	
    return checkData;
}
