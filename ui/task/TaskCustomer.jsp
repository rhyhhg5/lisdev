<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：TaskCustomer.jsp
//程序功能：工单管理客户业务页面
//创建日期：2005-01-18 18:34:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <%@include file="TaskCustomerInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>客户业务</title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
  <%@include file="TaskCommonCustomer.jsp"%>
  </Form>
  <!--<input type="button" class=cssButton name="Return" value="返  回" onclick="javascript:location.href='TaskPersonalBox.jsp'">-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>