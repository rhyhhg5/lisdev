//程序名称：TaskInputTopButton.js
//程序功能：工单管理查看顶部操作按钮
//创建日期：2005-04-03
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var viewScanFlag = 0;	//值为0：察看扫描；值为1：取消察看
var doBusinessAfterEdit = 0;
var editButtonClick = 0;    //是否保存按钮按下标志，1：是， 0：否

function initForm()
{
	if(top.loadFlag == "PERSONALBOX")
	{
		fm.dobus.style.display = "";
		fm.deliver.style.display = "";
		fm.deliver.style.display = "";
		fm.wait.style.display = "";
		fm.unWait.style.display = "";
		fm.remark.style.display = "";
		fm.viewRemark.style.display = "";
		fm.unite.style.display = "";
		//fm.edit.style.display = "";
		fm.save.style.display = "";
		fm.confirm.style.display = "";
		fm.finish.style.display = "";
		fm.goBack.style.display = "";
	}
}

function viewTask()
{
	var chkNum = 0;
	var WorkNo = fm.WorkNo.value;
	var CustomerNo = fm.CustomerNo.value;
	//parent.fraInterface.location.replace("./TaskView.jsp?WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo);
	parent.fraInterface.window.location.href = "./TaskView.jsp?loadFlag=TaskViewTopButton&WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo;

	//top.window.resizeTo(screen.availWidth, screen.availHeight);
	
}

/* 经办 */
function doBusiness()
{
  if(!checkDoBusiness())
  {
      return false;
  }
	
	//校验是否是由扫描产生的工单
	if(isSaveTaskAfterEdit())
	{
	  try
	  {
	    //若在察看界面点击经办，因为parent.fraInterface中的页面时taskView.jsp，
	    //其taskView.js中没有saveTaskAfterEdit方法，将会抛出异常
	    parent.fraInterface.operateWhileEdit = "dobusiness";
	    parent.fraInterface.saveTaskAfterEdit();
	    return true;
	  }
	  catch(ex)
	  {
	    alert("本工单由扫描产生，需要录入相应的工单信息。");
	    parent.fraInterface.location.replace("./TaskPersonalBoxSave.jsp?WorkNo=" + fm.WorkNo.value);
	  }
	}
	else
	{
	  parent.fraInterface.location.replace("./TaskPersonalBoxSave.jsp?WorkNo=" + fm.WorkNo.value);
	}
}

//校验本操作是否是先扫描后受理的工单录入
function isSaveTaskAfterEdit()
{
  var sql = "  select customerNo "
            + "from LGWork "
            + "where workNo = '" + fm.WorkNo.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    if(result[0][0] == null || result[0][0] == "")
    {
      sql = "  select b.* "
            + "from ES_DOC_RELATION a, ES_DOC_MAIN b "
            + "where a.docID = b.docID "
            + "   and a.bussNoType = '91' "
            + "   and bussNo = '" + fm.WorkNo.value + "' ";
      result = easyExecSql(sql);
      if(result)
      {
        return true;
      }
    }
  }
  return false;
}

function checkDoBusiness()
{
  var StatusNo = "";  //工单状态
  var LPEdorAPPState = "";  //保全状态
  
  //查询工单状态
  var sql = "  select StatusNo "
            + "from LGWork "
            + "where workNo = '" + fm.WorkNo.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
      StatusNo = result[0][0];
  }
  else
  {
      StatusNo = "";
  }
  
  //查询保全状态
  sql = "  select edorState "
        + "from LPEdorApp "
        + "where edorAcceptNo = '" + fm.WorkNo.value + "' ";
  result = easyExecSql(sql);
  if(result)
  {
      LPEdorAPPState = result[0][0];
  }
  else
  {
      LPEdorAPPState = "";
  }
  //待审批的工单不能经办
  if (StatusNo == "4")
  {
  	alert("该工单不能经办， 请审批");
  	return false;
  }
  if(StatusNo == "0" || StatusNo == "1")
  {
      alert("待办状态的工单不能经办。");
      return false;
  }
  
  if(LPEdorAPPState == "4" || LPEdorAPPState == "5" || LPEdorAPPState == "6")
  {
  	alert("该工单不能经办，因为函件未处理完毕");
  	
  	return false;
  }
	
  return true;
}

/* 转交 */
function deliverTask()
{
  var workInfo = getWorkInfo(fm.WorkNo.value);
  
  if(workInfo == null || workInfo[0][4] != "2" && workInfo[0][4] != "3")
  {
    alert("工单状态不是未经办和正经办，不能转交！");
    return false;
  }
	parent.fraInterface.location.replace("./TaskDeliver.jsp?loadFlag=TaskViewTopButton&WorkNo=" + fm.WorkNo.value);
}

/* 转为待办 */
function waitTask()
{
  var workInfo = getWorkInfo(fm.WorkNo.value);
  
  if(workInfo == null || workInfo[0][4] != "2" && workInfo[0][4] != "3")
  {
    alert("工单状态不是未经办和正经办，不能转为待办！");
    return false;
  }
  
//校验是否是由扫描产生的工单
	if(isSaveTaskAfterEdit())
	{
	  try
	  {
	    //若在察看界面点击经办，因为parent.fraInterface中的页面时taskView.jsp，
	    //其taskView.js中没有saveTaskAfterEdit方法，将会抛出异常
	    parent.fraInterface.operateWhileEdit = "wait";
	    parent.fraInterface.saveTaskAfterEdit();
	    return true;
	  }
	  catch(ex)
	  {
	    alert("本工单由扫描产生，需要录入相应的工单信息。");
	    parent.fraInterface.location.replace("./TaskPersonalBoxSave.jsp?WorkNo=" + fm.WorkNo.value);
	  }
	}
	else
	{
	  parent.fraInterface.location.replace("./TaskWait.jsp?loadFlag=TaskViewTopButton&WorkNo=" + fm.WorkNo.value);
	}
}

/* 取消待办 */
function unWaitTask()
{
	//parent.fraInterface.fm.action = "TaskDeliver.jsp";
	//parent.fraInterface.fm.target = "";
	//parent.fraInterface.fm.submit();
}

/* 批注 */
function remarkTask()
{
	parent.fraInterface.location.replace("./TaskRemark.jsp?loadFlag=TaskViewTopButton&WorkNo=" + fm.WorkNo.value);
}

/* 查看批注 */
function viewRemarkTask()
{
	parent.fraInterface.location.replace("./TaskViewRemark.jsp?loadFlag=TaskViewTopButton&WorkNo=" + fm.WorkNo.value);
}

/* 查看扫描 */
function viewScan()
{
	if(viewScanFlag == 0)
	{
		parent.fraSet.rows = "40,100,350,*,0";
		viewScanFlag = 1;
	}
	else
	{
		parent.fraSet.rows = "40,0,0,*,0";
		viewScanFlag = 0;
	}
}

/* 返回 */
function returnTask()
{
	parent.window.opener.focus();
	parent.window.close();
}

//编辑工单信息，调用TaskView.js实现的接口
function editTask()
{
  if(!checkState())
  {
      return false;
  }
    
  editButtonClick = 1;
	doBusinessAfterEdit = 1;
	top.fraInterface.fm.queryCustomer.style.display = "";
	top.fraInterface.fm.CustomerNo.style.width = "95";
	parent.fraInterface.editTask();
}

function checkState()
{
    var workInfo = getWorkInfo(fm.WorkNo.value);
    var statusNo = workInfo[0][4];
    if(statusNo == "0" || statusNo == "1" || statusNo == "5" || statusNo == "6"
        || statusNo == "7" || statusNo == "8")
    {
        alert("本工单不是待处理工单，不能修改");
        return false;
    }
    
    //若工单是保全，则工单类型编号（workInfo[0][3]）前两位为3
    if(workInfo[0][3].indexOf("03") == 0)
    {
        var LPEdorAppInfo = getLPEdorAppInfo(fm.WorkNo.value);
        if(LPEdorAppInfo == null)
        {
            return true;
        }
        var edorState = LPEdorAppInfo[0][12];
        if(edorState == "5" || edorState == "9")
        {
            alert("本工单不是待处理工单，不能修改");
            return false;
        }
    }
    
    var customerNo = workInfo[0][7];
    var workNo = workInfo[0][0];
    if(customerNo == null || customerNo == "" || customerNo == "null")
    {
      var sql = "  select 1 "
                + "from ES_DOC_RELATION "
                + "where bussNo = " + workNo + "' ";
      var rs = easyExecSql(sql);
      if(rs)
      {
        alert("该工单为先扫描后受理工单，请通过经办录入工单信息");
        return false;
      }
    }
    
    return true;
}

//保存编辑后的信息
function saveTask()
{
  if(isSaveTaskAfterEdit())
	{
	  try
	  {
	    //若在察看界面点击经办，因为parent.fraInterface中的页面时taskView.jsp，
	    //其taskView.js中没有saveTaskAfterEdit方法，将会抛出异常
	    parent.fraInterface.operateWhileEdit = "save";
	    parent.fraInterface.saveTaskAfterEdit();
	    return true;
	  }
	  catch(ex)
	  {
	    alert("本工单由扫描产生，需要录入相应的工单信息。");
	    parent.fraInterface.location.replace("./TaskPersonalBoxSave.jsp?WorkNo=" + fm.WorkNo.value);
	  }
	}
	else
	{
	  alert("工单不是先扫描后受理工单，或已录入工单信息，不能再次录入");
	  return false;
	}
  
  try{parent.fraInterface.saveButtonClick = 1;}catch(ex){}  //原来的查看->编辑->保存方式的处理
  
	parent.fraInterface.submitForm();
}

//查询工单信息
function getWorkInfo(workNo)
{
  var sql = "  select * "
            + "from LGWork "
            + "where workNo = '" + workNo + "' ";
  var result = easyExecSql(sql);
  return result;
}

//得到保全受理件信息
function getLPEdorAppInfo(workNo)
{
    var sql = "  select * "
              + "from LPEdorApp "
              + "where edorAcceptNo = '" + workNo + "' ";
    var result = easyExecSql(sql);
    if(result)
    {
        return result;
    }
    else
    {
        return null;
    }
}

//合并
function uniteTask()
{
	var chkNum = 0;
	var WorkNo = new Array();
	var WorkType = new Array();
	var typeIndex = 0;
	
	for (i = 0; i < parent.fraInterface.CustomerGrid.mulLineCount; i++)
	{
		if (parent.fraInterface.CustomerGrid.getChkNo(i)) 
		{
		  chkNum = chkNum + 1;
			WorkNo[chkNum] = parent.fraInterface.CustomerGrid.getRowColData(i, 2);
			WorkType[typeIndex] = parent.fraInterface.CustomerGrid.getRowColData(i, 4);
			typeIndex = typeIndex + 1;
		}
	}
	if (chkNum != 2)
	{
		alert("您可以且只可以选择两条工单！");
		return false;
	}
	
	if(WorkType[0] != WorkType[1])
	{
		alert("两条工单业务类型不一样，不能进行合并");	
		return false;
	}

	if (WorkNo[1] != "" && WorkNo[1] != null && WorkNo[2] != "" && WorkNo[2] != null)
	{		
		//parent.fraInterface.location.replace("./uniteTask.jsp?WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo);
		parent.fraInterface.
			window.open("./TaskUniteMain.jsp?WorkNo1=" + WorkNo[1] + "&WorkNo2=" + WorkNo[2], 
										"_blank", "height=200,width=300,top=250,left=350,toolbar=no,status=no,menubar=no");
		
	}
}

//审批
function confirmTask()
{
	var sql = 	"select typeNo, statusNo "
				+ "from LGWork "
				+ "where workNo='" + fm.WorkNo.value + "' ";
	var result = easyExecSql(sql);
	
	if(result)
	{
		if(result[0][1] != "4")
		{
			alert("该工单不能审批");
			return false;
		}
		
		var topTypeNo = result[0][0].substring(0, 2);
		
		//保全、续期续保、满期结算、延期结算
		if(topTypeNo == "03" || topTypeNo == "07" || topTypeNo == "05" || topTypeNo == "06" )
		{
			parent.fraInterface.location.replace("TaskConfirm.jsp?WorkNo=" + fm.WorkNo.value);
		}
	}
}

//打印工单信息
function printTask()
{
	var newWindow = window.open("../f1print/TaskGetPrint.jsp?WorkNo=" + fm.WorkNo.value);
}

function letter()
{
    var sql = 	"select typeNo, statusNo "
				+ "from LGWork "
				+ "where workNo='" + fm.WorkNo.value + "' ";
	var result = easyExecSql(sql);
	if(result)
	{
	  var topTypeNo = result[0][0].substring(0, 2);
	    if(topTypeNo == "03" || topTypeNo == "07")
	    {
	        window.open("./TaskLetterMain.jsp?workNo=" + fm.WorkNo.value + "&customerNo=" + fm.CustomerNo.value, "letter");
	    }
	    else
	    {
	        alert("工单类型不是保全也不是续期续保，不能新建函件。");
	        return false;
	    }
	}
    else
    {
        alert("工单信息不完整，请退出重试。");
        return false;
    }
}

//工单结案
function finishTask()
{
	if(!checkTypeNo())
	{
	  return false;
	}
	
	if(!checkStatus())
	{
	  return false;
	}
  
  var temp = fm.action;
  fm.action = "TaskChangeStatus.jsp?loadFlag=TASKVIEWTOPBUTTON&StatusNo=5&CurrDoing=1&WorkNo=" + fm.WorkNo.value;
  
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
		"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.submit();
  fm.action = temp;
}

function afterSubmit(flag, content)
{
  showInfo.close();
	window.focus();
	if (flag == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		top.opener.focus();
		top.opener.location.reload();
		top.close();
	}
}

function checkTypeNo()
{
  var sql = "  select a.typeNo, a.statusNo "
    				+ "from LGWork a, LGPhoneAnwser b "
    				+ "where a.workNo = b.phoneWorkNo "
    				+ "   and a.workNo='" + fm.WorkNo.value + "' ";
	var result = easyExecSql(sql);
	
  if(result == null)
  {
    alert("非新契约回访工单，不能进行本操作。");
    return false;
  }
  return true;
}

//校验本工单是否可以进行结案操作
function checkStatus()
{
  var workInfo = getWorkInfo(fm.WorkNo.value);
  if(workInfo == null)
  {
    alert("没有查询到工单信息。");
    return false;
  }
  var statusNo = workInfo[0][4];
  if(statusNo == "5" || statusNo == "6" || statusNo == "7" || statusNo == "8")
  {
    alert("工单已处理完成，不能在进行结案操作。");
    return false;
  }
  
  return true;
}