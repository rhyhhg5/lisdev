<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="TaskCheckPersonalBox.jsp"%>
<%
	//程序名称：TaskUnchecked.jsp
	//程序功能：个人送核件清单界面
	//创建日期：2008-1-16
	//创建人  ：Zhanggm
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="TaskUnchecked.js"></SCRIPT>
		<%@include file="TaskUncheckedInit.jsp"%>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<title>个人送核件清单</title>
	</head>
	<body onload="initForm();">
		<form name=fm target=fraSubmit>
			<input type="hidden" class=commpn name="GIManageCom">
			<Table class=common>
				<tr>
					<TD class=title>
						管理机构
					</TD>
					<td class=input>
						<Input class="codeno" name=ManageCom
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><Input class=codename name=ManageComName readonly>
					</td>
					<TD class=title>
						受理时间起期:
					</TD>
					<TD class=input>
						<Input class="coolDatePicker" dateFormat="short" name=StartDate>
					</TD>
					<TD class=title>
						受理时间止期:
					</TD>
					<TD class=input>
						<Input class="coolDatePicker" dateFormat="short" name=EndDate>
					</TD>
				</tr>
				<TR class=common>
					<TD class=title>
						工单号：
					</TD>
					<TD class=input>
						<Input class=common name=WorkNo>
					</TD>
					<TD class=title>
						保单号：
					</TD>
					<TD class=input>
						<Input class=common name=ContNo>
					</TD>
				</TR>
			</Table>
			<input class="cssButton" type=button value="查询" name="query"
				ID="query" onclick="Query()">
			<input VALUE="工单查看" TYPE=button Class="cssButton" name="view"
				onclick="viewTask();">
			<table>
				<tr>
					<td width="17">
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this, divTaskList);">
					</td>
					<td width="185" class=titleImg>
						工单列表
					</td>
				</tr>
			</table>
			<div id="divTaskList" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanTaskGrid"></span>
						</td>
					</tr>
				</table>
			</div>
			<div id="divPage2" align=center style="display: 'none' ">
				<input CLASS=cssButton VALUE="首  页" TYPE=button
					onclick="turnPage2.firstPage();">
				<input CLASS=cssButton VALUE="上一页" TYPE=button
					onclick="turnPage2.previousPage();">
				<input CLASS=cssButton VALUE="下一页" TYPE=button
					onclick="turnPage2.nextPage();">
				<input CLASS=cssButton VALUE="尾  页" TYPE=button
					onclick="turnPage2.lastPage();">
			</div>
			<br/>
			<h3><font color=red>本页面第一次打开时默认查询当前登陆用户下已送核的所有工单状态.</font><h3>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
