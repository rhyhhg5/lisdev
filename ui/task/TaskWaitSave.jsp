<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskWaitSava.jsp
//程序功能：工单管理改变工单状态数据保存页面
//创建日期：2005-01-25 20:40:18
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	//输入参数
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	
	String[] tWorkNo = (String[]) session.getAttribute("WORKNO");
	tLGWorkSchema.setStatusNo(request.getParameter("TaskWaitType"));
	tLGWorkSchema.setWakeDate(request.getParameter("WakeDate"));
	tLGWorkSchema.setPauseNo(request.getParameter("PauseNo"));
	
	String wakeHoure = request.getParameter("WakeTimeHour");
	String wakeMinute = request.getParameter("WakeTimeMinute");
	if(wakeHoure.equals(""))
	{
	    wakeHoure = "00";
	}
	if(wakeMinute.equals(""))
	{
	    wakeMinute = "00";
	}
	String WakeTime = wakeHoure + ":" + wakeMinute + ":00";
	
	tLGWorkSchema.setWakeTime(WakeTime);
	
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tWorkNo);
	tVData.add(tLGWorkSchema);
	
	TaskWaitBL tTaskWaitBL = new TaskWaitBL();
	if (tTaskWaitBL.submitData(tVData, "ok") == false)
	{
		FlagStr = "Fail";
		Content = "数据保存失败！";
	}
	else 
	{
		Content = "设置待办成功";
	}
	//设置显示信息
	VData tRet = tTaskWaitBL.getResult();
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

