<%
//程序名称：TaskPostPurviewInit.jsp
//程序功能：项目权限设置界面
//创建日期：2005-07-05
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.task.GetMemberInfo"%>

<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String memberNo = tG.Operator;
	String postNo = GetMemberInfo.getPostNo(memberNo).toLowerCase();
	String comCode = tG.ComCode;
%>

<script language="javascript">
	
var postNo = "<%=postNo%>";

var wherePart = "";
var turnPage = new turnPageClass();
var sql = "";

//只有pa01可以查看岗位级别的权限
if("<%=postNo%>" == "pa01")
{
	sql = "select ProjectPurviewNo, ProjectNo, ProjectNo, "
		+ "		case ProjectType "
		+ "			when '0' then '个险' "
		+ "			when '1' then '团险' "
		+ "		end, "
		+ "		case DealOrganization "
		+ "			when '0' then '总公司' "
		+ "			when '1' then '分公司' "
		+ "		end, PostNo, "
		+ "     case MemberNo "
		+ "			when '00000000000000000000' then '' "
		+ "			else MemberNo "
		+ "		end, "
		+ "		case ConfirmFlag "
		+ "			when '0' then '无确认权' "
		+ "			when '1' then '有确认权' "
		+ "			when '2' then '' "
		+ "		end, "
		+ "     a.riskcode, "
		+ "		MoneyPurview, ExcessPurview, TimePurview, NeedOtherAudit, "
		+ "		Remark, Operator, char(ModifyDate) || ' ' || ModifyTime "
		+ "from LGProjectPurview a "
		+ "where 1 = 1 "
		+ " and (a.stateflag!='2' or a.stateflag is null) ";
}
else
{

	sql = sql 
		+ "select ProjectPurviewNo, ProjectNo, ProjectNo, "
		+ "		case ProjectType "
		+ "			when '0' then '个险' "
		+ "			when '1' then '团险' "
		+ "		end, "
		+ "		case DealOrganization "
		+ "			when '0' then '总公司' "
		+ "			when '1' then '分公司' "
		+ "		end, PostNo, "
		+ "     case MemberNo "
		+ "			when '00000000000000000000' then '' "
		+ "			else MemberNo "
		+ "		end, "
		+ "		case ConfirmFlag "
		+ "			when '0' then '无确认权' "
		+ "			when '1' then '有确认权' "
		+ "			when '2' then '' "
		+ "		end, "
		+ "     a.riskcode, "
		+ "		MoneyPurview, ExcessPurview, TimePurview, NeedOtherAudit, "
		+ "		Remark, a.Operator, char(a.ModifyDate) || ' ' || a.ModifyTime "
		+ "from LGProjectPurview a, LDUser b " 
		+ "where 1=1 "
		+ "		and a.memberNo != '00000000000000000000' "
		+ "		and a.memberNo = b.userCode "
		+ " 	and (a.stateflag!='2' or a.stateflag is null) ";
}

function initForm()
{
	document.all("TableId").style.display = "";
	initPurviewGrid();
	queryAllPurview();
	
	showCodeName();
}

//查询现有权限
function queryAllPurview()
{
	if("<%=postNo%>" == "pa02" || "<%=postNo%>" == "pa04")
	{
		sql = sql
			+"		and postNo > 'PA01' "
	}
	var sqlInit = 	sql	
					+ "order by ProjectNo, ProjectType, PostNo, MemberNo ";
	if("<%=postNo%>" != "pa03" && "<%=postNo%>" != "pa05" && "<%=postNo%>" != "pa06" 
		&& "<%=postNo%>" != "pa07" && "<%=postNo%>" != "pa08" && "<%=postNo%>" != "pa09")
	{
		turnPage.pageDivName = "divPage";
		turnPage.pageLineNum = 14;
		turnPage.queryModal(sqlInit, PurviewGrid);
		
		stringForOrder = sqlInit;
		changePurviewFull(); //将录入时全权存为9999999999的值显示为全权
	}
		
	return true;
}

// 项目权限信息列表的初始化
function initPurviewGrid()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="项目权限编号";         	  //列名
		iArray[1][1]="0px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="项目";         	  //列名
		iArray[2][1]="30px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]= 0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="项目名称";         	  //列名
		iArray[3][1]="120px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]= 2;              			//是否允许输入,1表示允许，0表示不允许		
		iArray[3][4]="edorcode";
		iArray[3][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[3][9]="保全项目|code:edorcode&NOTNULL";
		iArray[3][18]=250;
		iArray[3][19]= 0 ; 
		
		iArray[4]=new Array();
		iArray[4][0]="项目类型";         	  //列名
		iArray[4][1]="55px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="机构类型";         	  //列名
		iArray[5][1]="55px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="岗位级别";         	//列名
		iArray[6][1]="150px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[6][4]="position";
		iArray[6][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[6][9]="操作类型|code:position&NOTNULL";
		iArray[6][18]=250;
		iArray[6][19]= 0 ; 
		
		iArray[7]=new Array();
		iArray[7][0]="岗位";              //列名
		iArray[7][1]="70px";            	//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=2; 
		iArray[7][4]="TaskMemberNo";
		iArray[7][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[7][9]="操作类型|code:TaskMemberNo&NOTNULL";
		iArray[7][18]=250;
		iArray[7][19]= 0 ; 
		
		iArray[8]=new Array();
		iArray[8][0]="确认权";              //列名
		iArray[8][1]="70px";            	//列宽
		iArray[8][2]=200;            			//列最大值
		iArray[8][3]=0; 

		iArray[9]=new Array();
		iArray[9][0]="险种";              //列名
		iArray[9][1]="70px";            	//列宽
		iArray[9][2]=200;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许 		
		
		iArray[10]=new Array();
		iArray[10][0]="金额权限";              //列名
		iArray[10][1]="70px";            	//列宽
		iArray[10][2]=200;            			//列最大值
		iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[11]=new Array();
		iArray[11][0]="超额权限";         	//列名
		iArray[11][1]="70px";            	//列宽
		iArray[11][2]=200;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[12]=new Array();
		iArray[12][0]="时间权限";              //列名
		iArray[12][1]="55px";            	//列宽
		iArray[12][2]=200;            			//列最大值
		iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[13]=new Array();
		iArray[13][0]="是否需互核";              //列名
		iArray[13][1]="20px";            	//列宽
		iArray[13][2]=200;            			//列最大值
		iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[14]=new Array();
		iArray[14][0]="备注";              //列名
		iArray[14][1]="250px";            	//列宽
		iArray[14][2]=200;            			//列最大值
		iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[15]=new Array();
		iArray[15][0]="经办人";              //列名
		iArray[15][1]="190px";            	//列宽
		iArray[15][2]=200;            			//列最大值
		iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[16]=new Array();
		iArray[16][0]="修改时间";              //列名
		iArray[16][1]="190px";            	//列宽
		iArray[16][2]=200;            			//列最大值
		iArray[16][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		PurviewGrid = new MulLineEnter("fm", "PurviewGrid"); 
		//这些属性必须在loadMulLine前
		PurviewGrid.mulLineCount = 0;
		PurviewGrid.displayTitle = 1;
		PurviewGrid.locked = 1;
		PurviewGrid.canSel = 1;
		PurviewGrid.canChk = 0;
		PurviewGrid.hiddenSubtraction = 1;
		PurviewGrid.hiddenPlus = 1;
		//PurviewGrid.addEventFuncName = "initBnfGrid";
		PurviewGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert("项目权限信息列表初始化错误" + ex);
	}
}

</script>