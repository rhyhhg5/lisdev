<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//程序名称：TaskRemarkInit.jsp
//程序功能：工单管理工单批注页面初始化
//创建日期：2005-01-19
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
	
	System.out.println("\n\n\n批注时间: " + PubFun.getCurrentDate() + " "
	  + PubFun.getCurrentTime() + ", 操作人" + tGI.Operator);

	String[] WorkNo = null;
	String loadFlag = request.getParameter("loadFlag");
	if(loadFlag != null && loadFlag.equals("TaskViewTopButton"))
	{
	    //从察看页面调用
    WorkNo = new String[1];
    WorkNo[0] = request.getParameter("WorkNo");
    
    if(WorkNo[0] == null || WorkNo[0].equals(""))
    {
	    WorkNo[0] = (String)session.getAttribute("VIEWWORKNO");
	  }
	}
  else
  {
    WorkNo = (String[])session.getAttribute("WORKNO");
  }
%>
<script language="JavaScript">
var WorkNo = "<%=WorkNo[0]%>";

//初始化表单
function initForm()
{
<%
  //只有一条记录时显示记录
  if (WorkNo.length == 1) 
  {
%>
	 initTaskInfo();
	 initElementtype();
	 fm2.RemarkContent.focus();
<%
  }
%>

fm2.loadFlag.value = "<%=loadFlag%>";
}
</script>