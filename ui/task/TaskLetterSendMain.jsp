<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskLetterSendMain.jsp
//程序功能：工单管理工单新建框架页面
//创建日期：2005-08-19
//创建人  	Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String letterType = request.getParameter("letterType");
	String edorAcceptNo = request.getParameter("edorAcceptNo");
	String serialNumber = request.getParameter("serialNumber");
	String backFlag = request.getParameter("backFlag");
	String backDate = request.getParameter("backDate");
%>
<html>
<head>
<title>函件下发 </title>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" cols="*" frameborder="no" border="1">
	<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="no" src="about:blank" >
	<frame name="fraTitle"  scrolling="no" src="about:blank" >
	<frameset name="fraSet" rows="0,*,0" cols="*">
	    <frame id="fraTopButton" name="fraTopButton" noresize scrolling="no" src="about:blank">
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="TaskLetterSend.jsp?loadFlag=SHOWLETTER&edorAcceptNo=<%=edorAcceptNo%>&letterType=<%=letterType%>&serialNumber=<%=serialNumber%>&backFlag=<%=backFlag%>&backDate=<%=backDate%>">
		<frame id="fraNext" name="fraNext" scrolling="auto" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
