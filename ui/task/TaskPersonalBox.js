//程序名称：TaskPersonalBox.js
//程序功能：工单管理个人信箱页面
//创建日期：2005-01-17 11:03:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mType = 0;
var mType2 = 0;

var sqlForRefresh = "";

/* 查询工单在列表中显示结果 */
function easyQueryClick()
{
    var partMakedate = getMakedate();
    
	//得到查询条件
	var partSQL = "";
	mType = fm.condition.value;
	
	//将按lpedorapp表的状态查询条件放到之前获取 110221
	var partSQL3 = "";
	var mType3 = fm.condition3.value;
	
	switch (mType) 
	{
		case "0":   //未经办/正经办/审核/(默认)
			if(mType3 == "0")
			{
			partSQL = " ";
			break;
			}
			else
			{
			partSQL = "and w.StatusNo in ('2', '3', '4', '99', '88') ";
			break;
			}
		case "1":   //未经办/正经办/审核
			partSQL = "and w.StatusNo in ('2', '3', '4') ";
			break;
		case "2":  //未经办
			partSQL = "and w.StatusNo = '2' ";
			break;
		case "3":  //正经办
			partSQL = "and w.StatusNo = '3' ";
			break;
		case "4":  //审核
			partSQL = "and w.StatusNo = '4' ";
			break;
		case "5":  //待办（所有）
			partSQL = "and w.StatusNo in ('0', '1') ";
			break;
		case "6":  //待办（定期）
			partSQL = "and w.StatusNo = '0' ";
			break;
		case "7":  //待办（无期）
			partSQL = "and w.StatusNo = '1' ";
			break;
		case "8":  //结案
			partSQL = "and w.StatusNo = '5' ";
			break;
		case "9":  //存档
			partSQL = "and w.StatusNo = '6' ";
			break;
	    case "10": //过应办期
			partSQL =  "and w.StatusNo = '2' " +
			           "and days(current date) - days(t.InDate) > int(w.DateLimit) ";
			break;			
		case "11":  //被合并
			partSQL = "and w.StatusNo = '7' ";
			break;
		case "12":  //已撤销
			partSQL = "and w.StatusNo = '8' ";
			break;
		case "99":  //审批通过
			partSQL = "and w.StatusNo = '99' ";
			break;
		case "88":  //审批不通过
			partSQL = "and w.StatusNo = '88' ";
			break;
		default:
		 	partSQL = "";
	}
	
	var partSQL2 = "";
	if (mSelect)  //判断是否有业务类型下拉框
	{
		mType2 = fm.condition2.value;
		if (mType2 == "0")
		{
			partSQL2 = "";
		}
		else
		{
			partSQL2 = "and w.TypeNo = '" + mType2 + "' ";
		}
	}
	

	if(mType3 == "-1")
	{
		//保全申请、理算完成、保全确认、函件待下发、函件回销待处理、函件已回销、核保完毕
		partSQL3 = " and (e.EdorState in ('1', '2', '0', '4', '6', '7', '8', '11') or (w.typeno like '06%%' and w.StatusNo in ('4','99','88'))) ";
	}
	else
	{
		partSQL3 = " and e.EdorState = '" + mType3 + "' "; 
	}
	
	var partSQL41 = "";
	var partSQL42 = "";
	var mType4 = fm.condition6.value;
	  if (mType4=='1') //个
	  {
		partSQL41 = " and (select salechnl from lccont where contno in (select contno from lpedormain where edoracceptno=w.workno) union select salechnl from lbcont where contno in (select contno from lpedormain where edoracceptno=w.workno) fetch first 1 row only) not in ('04','13') ";
		partSQL42 = " and length(w.customerno)=9  ";
	  }
	  else if (mType4=='2') //团
	  {
	    partSQL41 = " and length(w.customerno)!=9 ";
	    partSQL42 = "";
	  }
	  else if (mType4=='3') //银
	  {
	    partSQL41 = " and (select salechnl from lccont where contno in (select contno from lpedormain where edoracceptno=w.workno) union select salechnl from lbcont where contno in (select contno from lpedormain where edoracceptno=w.workno) fetch first 1 row only) in ('04','13') ";
	    partSQL42 = " and length(w.customerno)=9 ";
	  }
	  else
	  {
	    partSQL41 = "";
	    partSQL42 = "";
	  }
	
	
	
	
	
	var orderSQL = "";
	var order = fm.order.value;
	switch (order) 
	{
		case "0":
			orderSQL = "order by InDate desc, WorkNo desc ";
			break;
		case "1":   //按优先级
			orderSQL = "order by PriorityNo ";
			break;
		case "2":   //按工单号
			orderSQL = "order by WorkNo desc ";
			break;
		case "2b":   //按工单号
			orderSQL = "order by WorkNo ";
			break;
		case "3":   //按经办日期
			orderSQL = "order by InDate desc ";
			break;
		case "3b":   //按经办日期
			orderSQL = "order by InDate ";
			break;
		default:
			orderSQL = "order by WorkNo desc ";
			break;
	}
	
	//初始化表格
	initGrid();
	
	//查询SQL语句
	var strSQL;
	
	//已撤销
	if(mType == "12")
	{
		strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, (select worktypename  from  LGWorkType  where worktypeno=w.TypeNo), w.CustomerNo, " +
				 "		w.CustomerName, '记录', w.acceptorNo, t.InDate, w.AcceptCom, " +
				 "		Case When w.DateLimit Is Null then '' Else trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
				 "		w.StatusNo, '', (Select count(NodeNo) from LGWorkRemark where WorkNo = w.WorkNo), " +
				 "		case ScanFlag " +
				 "			when '0' then '新扫描件' " +
				 "			when '1' then '旧扫描件' " +
				 "			else '无扫描件' " +
				 "		end, " +
				 "		w.detailWorkNo, w.StatusNo, '' " +
				 "from LGWork w, LGWorkTrace t " +
		         "where  w.WorkNo = t.WorkNo " +
		      	 "and    w.NodeNo = t.NodeNo " +
				 "and    t.WorkBoxNo = " +
				 "      (select WorkBoxNo from LGWorkBox " + 
				 "       where OwnerNo = '" + operator + "' " +
				 "       and   OwnerTypeNo = '2') " +
				 partSQL +
				 partSQL2 +
				 orderSQL;
	}   
	else
	{   
		//已申请了保全的工单
		strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, (select worktypename  from  LGWorkType  where worktypeno=w.TypeNo), w.CustomerNo, " +
				 "   w.CustomerName, '记录', w.acceptorNo, t.InDate, w.AcceptCom, " +
				 "   Case When w.DateLimit Is Null then '' Else trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
				 "   (select codename from ldcode where codetype='taskstatusno' and code=w.StatusNo), (select codename from ldcode where codetype='appedorstate' and  ltrim(rtrim(code))=e.EdorState), (Select count(NodeNo) from LGWorkRemark where WorkNo = w.WorkNo), " +
				 "   case ScanFlag " +
				 "   	when '0' then '新扫描件' " +
				 "   	when '1' then '旧扫描件' " +
				 "   	else '无扫描件' " +
				 "   end, " +
				 "   w.detailWorkNo, w.StatusNo, e.EdorState ,(case when length(w.customerno)=8 then '团险' else (select (case when salechnl  in ('04','13') then '银保' when salechnl is null then ''  else '个险' end ) from lccont where contno=(select contno from lpedormain where edoracceptno=w.workno fetch first 1 row only ) union select (case when salechnl  in ('04','13') then '银保' when salechnl is null then ''  else '个险' end ) from lbcont where contno=(select contno from lpedormain where edoracceptno=w.workno fetch first 1 row only ) fetch first 1 row only ) end )" +
				 "from LGWork w, LGWorkTrace t, lpedorapp e " +
		         "   where  w.WorkNo = t.WorkNo " +
		      	 "   and    w.NodeNo = t.NodeNo " +
		      	 "   and	 e.EdorAcceptNo=w.workNo " +
				 "   and    t.WorkBoxNo = " +
				 "      (select WorkBoxNo from LGWorkBox " + 
				 "       where OwnerNo = '" + operator + "' " +
				 "       and   OwnerTypeNo = '2') " +
				 partSQL +
				 partSQL2 +
				 partSQL3 + 
				 partSQL41+
				 partSQL42+
				 partMakedate ;
				 
				 if(mType3 == "-1") 
				 {
				    strSQL = strSQL +
    				//其他类型的工单
    				" union all " +
    				"select w.WorkNo, w.AcceptNo, w.PriorityNo, (select worktypename  from  LGWorkType  where worktypeno=w.TypeNo), w.CustomerNo, " +
    				"   w.CustomerName, '记录', w.acceptorNo, t.InDate, w.AcceptCom, " +
    				"   Case When w.DateLimit Is Null then '' Else trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
    				"   (select codename from ldcode where codetype='taskstatusno' and code=w.StatusNo), '', (Select count(NodeNo) from LGWorkRemark where WorkNo = w.WorkNo), " +
    				"   case ScanFlag " +
    				"   	when '0' then '新扫描件' " +
    				"   	when '1' then '旧扫描件' " +
    				"   	else '无扫描件' " +
    				"   end, " +
    				"   w.detailWorkNo, w.StatusNo, '',(case when length(w.customerno)=8 then '团险' else (select (case when salechnl  in ('04','13') then '银保' when salechnl is null then ''  else '个险' end ) from lccont where contno=(select contno from lpedormain where edoracceptno=w.workno fetch first 1 row only ) union select (case when salechnl  in ('04','13') then '银保' when salechnl is null then ''  else '个险' end ) from lbcont where contno=(select contno from lpedormain where edoracceptno=w.workno fetch first 1 row only ) fetch first 1 row only ) end ) " +
    				"from LGWork w, LGWorkTrace t " +
    		        "where  w.WorkNo = t.WorkNo " +
    		      	"   and    w.NodeNo = t.NodeNo " +
    				"   and 	 (typeNo not like '03%%' and typeNo not like '06%%' and typeNo not like '070001%%' " +    //除保全外的工单，js中%需要转义
    				"        or customerNo is null) " +	    //保全受理前扫描产生的工单
    				"   and  t.WorkBoxNo = " +
    				"      (select WorkBoxNo from LGWorkBox " + 
    				"       where OwnerNo = '" + operator + "' " +
    				"       and   OwnerTypeNo = '2') " +
    				partSQL +
    				partSQL2 + 
    				partSQL41+
    				partSQL42+
    				partMakedate ;
    	        }
				 
				 strSQL = strSQL + orderSQL+" with ur";
	}
 	//strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, (select worktypename  from  LGWorkType  where worktypeno=w.TypeNo), w.CustomerNo, " +
	//			 "		w.CustomerName, '记录', w.operator,'', w.AcceptCom,	'ff', " +
	//			 "		w.StatusNo, '', 'ff', 'ff', " +
	//			 "		w.detailWorkNo, w.StatusNo, '' " +
	//			 "from LGWork w  order by w.WorkNo desc ";
	
	sqlForRefresh = strSQL;
	//document.all("show").innerHTML = strSQL;
	turnPage1.pageLineNum = 15;
	turnPage1.queryModal(strSQL, TaskGrid);

	//showCodeName(); //把编码转为汉字显示
	return true;
}

//选中一条工单后跳转到相应的处理页面
function linkToPage(pageName)
{
	var param;
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = TaskGrid.getRowColData(i, 1);
				CustomerNo = TaskGrid.getRowColData(i, 5);
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	
	if (WorkNo)
	{
		//param = "WorkNo=" + WorkNo + "|CustomerNo=" + CustomerNo;
		//window.open("TaskMain.jsp?pageName=" + pageName + "&param=" + param);
		win = window.open(pageName + "WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo, "TaskWin");
		win.focus();
	}
}

/* 新建工单 */
function inputTask()
{
	var pageName = "TaskInput.jsp";
	var param = "";
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	win = window.open("TaskInputMain.jsp?pageName=" + pageName + "&param=" + param, 
					  "InputWin",
					  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
	win.focus();
}

/* 查看选中的工单 */
function viewTask()
{
	var pageName = "TaskView.jsp";
	var param;
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	var DetalWorkNo;
	
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = TaskGrid.getRowColData(i, 1);
				CustomerNo = TaskGrid.getRowColData(i, 5);
				DetailWorkNo = TaskGrid.getRowColData(i, 15);   
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	
	if (WorkNo)
	{
	    var width = screen.availWidth - 10;
	    var height = screen.availHeight - 28;
		win = window.open("TaskViewMain.jsp?loadFlag=PERSONALBOX&WorkNo="+WorkNo 
							+"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + DetailWorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);
		win.focus();
	}
}

/* 查看选中工单的批注 */
function viewRemarkTask()
{
	linkToPage("TaskViewRemark.jsp?");
}

/* 查看工单的作业历史 */
function historyTask()
{
	linkToPage("TaskHistory.jsp?");
}

/* 查看客户的业务 */
function customerTask()
{
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i))
		{
		  	chkNum = chkNum + 1;
		  	if (chkNum == 1)
			{
				WorkNo = TaskGrid.getRowColData(i, 1);
				CustomerNo = TaskGrid.getRowColData(i, 5);
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	if (WorkNo)
	{
		win = window.open("TaskCustomer.jsp?CustomerNo="+CustomerNo, "CustomerWin");
		win.focus();
	}
}

/* 转交选中的工单 */
function deliverTask()
{
	var chkNum = 0;
	var WorkNo;
	var AcceptNo;
	var StatusNo;
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		
		if (TaskGrid.getChkNo(i)) 
		{
			WorkNo = TaskGrid.getRowColData(i, 1);
			try
			{
				AcceptNo = TaskGrid.getRowColData(i, 2);
		  		StatusNo = TaskGrid.getRowColData(i, 17);
		  		if ((StatusNo != "2") && (StatusNo != "3"))
		  		{
		  			alert("受理号为：" + AcceptNo + " 的工单不能转交！");
		  			return false;
		  		}
		  		fm.all("TaskGridChk")[i].value = WorkNo;
		  		chkNum = chkNum + 1;
		  	}
			catch(e)
			{
				fm.all("TaskGridChk").value = WorkNo;
				chkNum = chkNum + 1;
			}
		}
		
	}
	
	if (chkNum == 0)
	{
		alert("请选择需转交的工单！");
		return false;
	}
	fm.action = "TaskDeliverMain.jsp?callFlag=taskPesonalBox";
	fm.target = "_blank";
	fm.submit();
	return true;
}

/* 批注选中的工单 */
function remarkTask()
{
	var chkNum = 0;
	var WorkNo;
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
			WorkNo = TaskGrid.getRowColData(i, 1);
			try{
		  	fm.all("TaskGridChk")[i].value = WorkNo;
		  }
		  catch (ex)
		  {
		  	fm.all("TaskGridChk").value = WorkNo;
		  }
		  chkNum = chkNum + 1;
		}
	}
	
	if (chkNum == 0)
	{
		alert("请选择需批注的工单！");
		return false;
	}
	fm.action = "TaskRemarkMain.jsp";
	fm.target = "_blank";
	fm.submit();
}

/* 转为待办状态 */
function waitTask()
{
	var chkNum = 0;
	var WorkNo;
	var AcceptNo;
	var StatusNo;
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
			WorkNo = TaskGrid.getRowColData(i, 1);
			AcceptNo = TaskGrid.getRowColData(i, 2);
		  	StatusNo = TaskGrid.getRowColData(i, 17);
		  	if ((StatusNo != "2") && (StatusNo != "3"))
		  	{
		  		alert("受理号为：" + AcceptNo + " 的工单不能转为待办！");
		  		return false;
		  	}
		  	if(TaskGrid.mulLineCount == 1)
		  	{
		  	    fm.all("TaskGridChk").value = WorkNo;
		  	}
		    else
		    {
		  	    fm.all("TaskGridChk")[i].value = WorkNo;
		  	}
		  	chkNum = chkNum + 1;
		}
	}
	
	if (chkNum == 0)
	{
		alert("请选择需待办的工单！");
		return false;
	}
	fm.action = "TaskWaitMainFrame.jsp";
	fm.target = "_blank";
	fm.submit();
}

/* 取消代办 */
function unWait()
{
	var chkNum = 0;
	var WorkNo;
	var AcceptNo;
	var StatusNo;
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
			WorkNo = TaskGrid.getRowColData(i, 1);
			AcceptNo = TaskGrid.getRowColData(i, 2);
		  	StatusNo = TaskGrid.getRowColData(i, 17);
		  	if ((StatusNo != "0") && (StatusNo != "1"))
		  	{
		  		alert("受理号为：" + AcceptNo + " 的工单不能取消待办！");
		  		return false;
		  	}
		  	try
		  	{
		  		fm.all("TaskGridChk")[i].value = WorkNo;
		  	}
		  	catch(ex)
		  	{
		  		fm.TaskGridChk.value = WorkNo;
		  	}
		  	chkNum = chkNum + 1;
		}
	}
	if (chkNum == 0)
	{
		alert("请选择需取消待办的工单！");
		return false;
	}
	if (confirm("您要把这" + chkNum + "条工单取消待办吗？"))
	{
		fm.action = "TaskUnWaitSave.jsp";
		submitForm();
	}	
}

/* 工单查询 */
function queryTask()
{
	win = window.open("./TaskQueryMain.jsp", "QueryWin");
	win.focus();
}

/* 作业合并 */
function uniteTask()
{
	var chkNum = 0;
	var WorkNo;
	var AcceptNo;
	var StatusNo;
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
			//WorkNo = TaskGrid.getRowColData(i, 1);
			//AcceptNo = TaskGrid.getRowColData(i, 2);
		  	//StatusNo = TaskGrid.getRowColData(i, 12);
		  	fm.all("TaskGridChk")[i].value = TaskGrid.getRowColData(i, 2);
		  	chkNum = chkNum + 1;
		}
	}
	if (chkNum < 2)
	{
		alert("请选择至少两个合并项！");
		return false;
	}
	
	if (confirm("您要合并这" + chkNum + "条工单吗？"))
	{
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "TaskSelAcceptNo.jsp";
		var width = 300;
		var height = 300;
		var x = (screen.width - width) / 2;
		var y = (screen.height - height) / 2;
		//使弹出框居中
		var status = "scrollbars=no,status=no,width=" + width + ",height=" + height + ",left=" + x + ",top=" + y;
		showInfo = window.open(urlStr, "", status);
				    
		//showInfo = window.showModelessDialog(urlStr, window, 
		//		"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	}
}

/* 提交表单 */
function submitForm()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit(); //提交
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit1(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		location.reload();
	}
}

/* 保存完成后的操作，由弹出窗口调用 */
function afterSubmit2(FlagStr, content, tUrl)
{
	if ((tUrl != null) && (tUrl != ""))
	{
		location.replace(tUrl);
	}
}    

/* 保存完成后的操作，由弹出窗口调用 */
function afterSubmit3(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		location.reload();
	}
}

/* 经办 */
function doBusiness()
{
	var chkNum = 0;
	var WorkNo;
	var CustomerNo;
	var DetailWorkNo;
	
	for (i = 0; i < TaskGrid.mulLineCount; i++)
	{
		if (TaskGrid.getChkNo(i)) 
		{
		  chkNum = chkNum + 1;
		  if (chkNum == 1)
			{
  				WorkNo = TaskGrid.getRowColData(i, 1);
  				CustomerNo = TaskGrid.getRowColData(i, 5);
  				DetailWorkNo = TaskGrid.getRowColData(i, 16);
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择一条工单！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一条工单！");
		return false;
	}
	var StatusNo = getLGWorkStatusNo(WorkNo);  //工单状态
	if(StatusNo == "")
	{
	  return false;  
	}
	
	var LPEdorAPPState = getLPEdorAppState(WorkNo);	//保全状态
	
	//待审批的工单不能经办
	if (StatusNo == "4")
	{
		alert("该工单不能经办， 请审批");
		return false;
	}
	if(StatusNo == "0" || StatusNo == "1")
	{
	    alert("待办状态的工单不能经办。");
	    return false;
	}
	
	if(LPEdorAPPState == "4" || LPEdorAPPState == "5" || LPEdorAPPState == "6")
	{
		alert("该工单不能经办，因为函件未处理完毕");
		return false;
	}
	
	if (LPEdorAPPState == "10") 
	{
	  alert("该工单已送人工核保,不能经办!");
		return false;
	}
	
	//校验工单所送核的所有险种都未通过人工核保,导致保全项目表(LPEdorItem)被删除的情况,提示业务员撤销工单 add by xp 20090218
	if (LPEdorAPPState == "11") 
	{
      var sqllpedor = "  select 1  from LPEdorItem  where EdorNo = '" + WorkNo + "' ";
      var resultlp = easyExecSql(sqllpedor);
      var sqllobedor = " select reason,reasoncode  from LOBEdorItem  where EdorNo = '" + WorkNo + "' and " +
                       " reason is not null and reasoncode is not null ";
      var resultlob = easyExecSql(sqllobedor);
      if((!resultlp)&&resultlob)
        {
           if((resultlob[0][0]!=null&&resultlob[0][0]=="核保终止")&&(resultlob[0][1]!=null&&resultlob[0][1]=="HBZZ"))
           {
             alert("该工单下所有送人工核保险种均未通过核保,请手工撤销该工单。");
             return false;
           }  
        }
	}
	
	if (WorkNo)
	{
		//从查看中经办
		var width = screen.availWidth - 10;
	    var height = screen.availHeight - 28;
		win = window.open("TaskViewMain.jsp?loadFlag=PERSONALBOX&initPage=TaskPersonalBoxSave.jsp&WorkNo=" + WorkNo + "&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + DetailWorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);
		win.focus();
	}
	
	return true;
}


//查询工单状态
function getLGWorkStatusNo(workNo)
{
  var oneWorkInfo = getOneLGWorkInfo(workNo);
  if(oneWorkInfo)
  {
    return oneWorkInfo[0][4];
  }
  else
  {
    return "";
  }
}

//查询工单信息
function getOneLGWorkInfo(workNo)
{
  var sql = "  select * "
            + "froM LGWork "
            + "where workNo = '" + workNo + "' ";
  var result = easyExecSql(sql);
  if(result == null)
  {
    alert("没有查询到工单号为 " + workNo + "的工单信息。")
  }
  
  return result;
}

//查询保全受理状态
function getLPEdorAppState(eodrAcceptNo)
{
  var oneLPEdorAppInfo = getOneLPEdorAppInfo(eodrAcceptNo);
  if(oneLPEdorAppInfo == null)
  {
    return "";
  }
  
  return oneLPEdorAppInfo[0][12];
}

//查询保全受理信息
function getOneLPEdorAppInfo(eodrAcceptNo)
{
  var sql = "  select * "
            + "from LPEdorApp "
            + "where edorAcceptNo = '" + eodrAcceptNo + "' ";
  var result = easyExecSql(sql);
  
  return result;
}

function queryScanType()
{      
	return a[0][0]="2"; 
}

//管理自动审批时的岗位权限
function dealPostPurview()
{
	win = window.open("./TaskPostPurviewMain.jsp", "postPurview");
	win.focus();
}

//电话催缴
function phoneHasten()
{
	win = window.open("TaskHastenCommon.jsp", "phoneHasten");
	fm.target = "fraSubmit";
	win.focus();
}

//电话催缴日处理
function CreateHastenTask()
{
	document.all.Hasten.disabled=true;
	fm.action = "CreateHastenTaskSave.jsp";
	fm.target = "fraSubmit";
	fm.submit();	
}

//函件超时
function CreateLetterOverTime()
{
	document.all.LetterOverTime.disabled=true;
	fm.action = "LetterOverTimeSvae.jsp";
	fm.target = "fraSubmit";
	fm.submit();
}

//待办件超时日处理
function WaitOverTime()
{
	document.all.OverTime.disabled=true;
	fm.action = "TaskWaitOverTimeSave.jsp"
	fm.target = "fraSubmit";
	fm.submit();
}

//保全收费结案日处理
function bqFinishTask()
{
	document.all.Finish.disabled=true;
	fm.action = "./BqFinishTask.jsp";
	fm.target = "fraSubmit";
	fm.submit();
}

//定期结算到帐确认日处理
function BalPayConfirm()
{
	fm.action = "./BalPayConfirmSave.jsp";
	fm.target = "fraSubmit";
	submitForm();
}

/* 简易保全 */
function easyEdor()
{
	fm.action = "TaskEasyEdorMain.jsp";
	fm.target = "_blank";
	fm.submit();
}

//续期催缴
function CreatNextFeeHasten()
{
	document.all.NextFeeHasten.disabled=true;
  fm.action = "CreatNextFeeHastenSvae.jsp";
	fm.target = "fraSubmit";
	
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit(); //提交
}

//补发函件
function ReissueLetterClick()
{
	var pageName = "TaskReissueLetterMain.jsp";
	var param = "";
	var width = screen.availWidth - 10;
	var height = screen.availHeight - 28;
	win = window.open("TaskReissueLetterMain.jsp", 
					  "InputWin",
					  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
	win.focus();

}

//续期催缴
function CreatMJTask()
{
  fm.action = "CreatMJTaskSvae.jsp";
	fm.target = "fraSubmit";
	
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit(); //提交
}

//团单退保试算
function gEdorCalZTTest()
{
  win = window.open("../bq/GEdorBudgetMain.jsp", "gEdorCalZTTest");
  win.focus();
}

//个单退保试算
function pEdorCalZTTest()
{
  win = window.open("../bq/PEdorBudgetMain.jsp", "pEdorCalZTTest");
  win.focus();
}
//小组取单
function getTeamBox()
{
	  if(!checkTeamBox()) {
	      return false;
	  }
	  fm.Num.value="1";
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm.action="./TaskTeamBoxSave.jsp";
		fm.submit();
}

function afterSubmit(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		location.reload();
	}
}  
//检验当前用户所在信箱是否有小组信箱号
function checkTeamBox() {
    var strSQL = "select 1 from LGWorkBox WHERE ownerno=(select groupno from lggroupmember where memberno='"+
                 operator+"') ";
	   var arrResult = easyExecSql(strSQL);
	   if(arrResult == null)
	   {
	       alert("对不起，您的小组没有信箱!!");
	       return false;	  
	   }
	   return true;
}

//20090513 zhanggm 如果三个过滤条件都不选，则查询sql默认最近3个月数据。
function getMakedate()
{
    var partMakedate = " and t.indate >= current date - 3 months "
	//工单状态
	var con1 = false;
	var conditionvalue = fm.condition.value;
	if(conditionvalue == "0")
	{
		con1 = false;
	}
	else
	{
		con1 = true;
	}
	//业务状态
	var con2 = false;
	var condition3value = fm.condition3.value;
	if(condition3value == "-1")
	{
		con2 = false;
	}
	else
	{
		con2 = true;
	}
	//业务类型
	var con3 = false;
	if (mSelect)  //判断是否有业务类型下拉框
	{
	    var condition2value = fm.condition2.value;
		if(condition2value == "0")
		{
			con3 = false;
		}
		else
		{
			con3 = true;
		}
	}
	//渠道类型
	var con4 = false;	
	var condition6value = fm.condition6.value;
	if(condition6value == "0")
	{
		con4 = false;
	}
	else
	{
		con4 = true;
	}
	if(con1 || con2 || con3 || con4)
	{
	    partMakedate = "";
	}
	return partMakedate;
}