<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskFinishSava.jsp
//程序功能：工单管理工单结案数据保存页面
//创建日期：2005-01-20 15:05:24
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	
	//输入参数
	VData tVData = new VData();
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	LGWorkRemarkSchema tLGWorkRemarkSchema = new LGWorkRemarkSchema();
	
	tLGWorkSchema.setWorkNo(request.getParameter("WorkNo"));
	tLGWorkRemarkSchema.setRemarkContent(request.getParameter("RemarkContent"));
	
	
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");	

	tVData.add(tLGWorkSchema);
	tVData.add(tLGWorkRemarkSchema);
	tVData.add(tGI);
	
	TaskFinishBL tTaskFinishBL = new TaskFinishBL();
	if (tTaskFinishBL.submitData(tVData, "ok") == false)
	{
		FlagStr = "Fail";
	}
	
	//设置显示信息
	VData tRet = tTaskFinishBL.getResult();
%> 
<html>
<script language="javascript">
	alert("ok");
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	alert("<%=request.getParameter("WorkNo")%>");
	alert("<%=request.getParameter("RemarkContent")%>");
</script>
</html>

