//程序名称：TaskInputTopButton.js
//程序功能：工单管理新建页面
//创建日期：2005-04-03
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

//经办
function doBusiness()
{
  showButton("doBuss");
	parent.fraInterface.doBusiness();
}

//保存
function saveTask()
{
  showButton("save");
	parent.fraInterface.saveTask();
}

//转交
function deliverTask()
{
	parent.fraInterface.deliverTask();
}

//转为待办
function toWait()
{
	parent.fraInterface.toWait();
}

//简易保全
function easyEdor()
{
	parent.fraInterface.easyEdor();
}

//返回
function returnTask()
{
	parent.fraInterface.returnPersonalBox();
}

function showButton(buttonId)
{
  if(parent.fraInterface.fm.TopTypeNo.value == "05")
  {
    return true;
  }
  document.all.save.disabled = true;
  document.all.doBuss.disabled = true;
}