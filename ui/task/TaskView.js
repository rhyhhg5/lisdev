//程序名称：TaskPersonalBox.js
//程序功能：工单管理个人信箱页面
//创建日期：2005-04-26
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;

var saveButtonClick = 0;	//是否保存按钮按下标志，1：是， 0：否，值在TaskViewTopButt.js中改变
var doBusinessFlag = 0;		//是否编辑后点击经办的标志，1：是， 0：否，值在TaskViewTopButt.js中改变

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}

//察看作业历史
function viewHistory()
{
	var selNumber = HistoryGrid.getSelNo();
	if(selNumber == 0)
	{
		alert("请选择一条历史纪录");
		return  false;
	}	
	else
	{
		var nodeNo = HistoryGrid.getRowColData(selNumber - 1, 1);
		var workNo = HistoryGrid.getRowColData(selNumber - 1, 2);
		
		win = window.open("TaskCommonHistoryView.jsp?NodeNo=" + nodeNo + "&WorkNo=" + workNo, "QueryHistory");
		win.focus();
	}
}

//编辑共担信息，实现taskViewTopButton.js中的edit()接口
function editTask()
{
	fm.all("CustomerNo").readOnly = false;
	fm.all("CustomerNo").className = "common";
	//fm.all("PriorityNo").readOnly = false;
	fm.all("PriorityNoName").className = "code";
	//fm.all("TypeNo").readOnly = false;
	fm.all("TypeNoName").className = "code";
	fm.all("DateLimit").readOnly = false;
	fm.all("DateLimit").className = "common";
	fm.all("ApplyTypeNoName").readOnly = false;
	fm.all("ApplyTypeNoName").className = "code";
	fm.all("ApplyName").readOnly = false;
	fm.all("ApplyName").className = "common";
	//fm.all("AcceptWayNo").readOnly = false;
	fm.all("AcceptWayNoName").className = "code";
	//fm.all("AcceptComName").readOnly = false;
	//fm.all("AcceptComName").className = "code";
	//fm.all("AcceptorNo").readOnly = false;
	//fm.all("AcceptorNo").className = "common";
	//fm.all("AcceptDate").readOnly = false;
	//fm.all("AcceptDate").className = "common";
	fm.all("RemarkContent").readOnly = false;
	fm.all("RemarkContent").className = "common";
	
	fm.all("TopTypeNoName").ondblclick = "";
	fm.all("AcceptComName").ondblclick = ""; 
}

//实现自动填充功能
function afterCodeSelect(cCodeName, Field)
{
	if (cCodeName == "TaskTypeNo")
	{
		//得到时限
		var tSubType = fm.all("TypeNo").value;
	    strSql = "select DateLimit from LGWorkType " +
	    	     "where WorkTypeNo = '" + tSubType + "'";
		var arrResult = easyExecSql(strSql);
		fm.all("DateLimit").value = arrResult[0][0];
	}
}

//检测数据的合法性
function checkData()
{
	if(parseInt(fm.CustomerNo.value) == "NAN")
	{
		alert("您输入的客户号不是数字");
	}
	if(parseInt(fm.DateLimit.value) == "NAN")
	{
		alert("您输入的时限不是数字");
	}
}

//判断是否是回车键
function isEnterDown()
{
	var keycode = event.keyCode;
	
	//回车的ascii码是13
	if (keycode == "13")
	{
		return true;
	}
	else
	{
		return false;
	}
}

//按客户号查询
function queryByCustomerNo()
{
	if (isEnterDown())
	{
		if((fm.CustomerNo.value).length != 8 && (fm.CustomerNo.value).length != 9)
		{
			alert("客户号必须为8位或9位");
		}
		else
		{
			//查询作业历史信息
			queryHistoryGrid();
			//查询客户作业信息
			queryCustomerGrid();
			//查询客户保单信息
			if(fm.CustomerNo.value.length == 9)
			{
				queryContInfo();
			}
			if(fm.CustomerNo.value.length == 8)
			{
				qureyGrpContInfo();
			}
			showCodeName();
		}
	}
}

//保存修改后的工单信息
function submitForm()
{
	fm.action = "UpdateTaskSave.jsp?workNoEdit=" + fm.WorkNo.value;
	
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
		"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	
	fm.submit();
}

/* 工单编辑保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		//showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//location.reload();
		
		if(saveButtonClick == 1)
		{
			window.focus();
			window.location.reload();
			saveButtonClick = 0;
			editButtonClick = 0;
		}
		else
		{
			top.close();
			top.opener.focus();
			top.opener.location.reload();
		}
		if(doBusinessFlag == 1)
		{
			alert("成功编辑工单");
			window.location.replace("./TaskPersonalBoxSave.jsp");
			doBusinessFlag = 0;
		}
	}		
} 

function controlButton()
{
  var otherPrintType = needShowOtherPrint();
  if(otherPrintType != null)
  {
  	try
  	{
    	fm.all("showOtherPrintButton2").style.display = "";
  	}catch(e)
  	{
  		fm.all("showOtherPrintButton").style.display = "";
  	}
  }
}   

function needShowOtherPrint()
{
  var workInfo = getLGWorkInfo(fm.WorkNo.value);
  if(workInfo == null)
  {
    alert("没有查询到工单信息。");
    return null;
  }
  
  if(workInfo[0][3] == "070015")
  {
    return "MJ";
  }
  if(workInfo[0][3] == "070016")
  {
  	return "PC";//个单电话催缴
  }
  if(workInfo[0][3] == "070014")
  {
    return "GC";//团单催缴
  }
  if(workInfo[0][3] == "070013")
  {
    return "BQ";//保全催缴
  }
  if(workInfo[0][3] == "0602")
  {
  	return "DJ"; //定期结算
  }
  if(workInfo[0][3] == "0601")
  {
  	return "TJ"; //提前结算
  }
  return null;
}

//显示非保单服务的明细
function showOtherPrint()
{
  var otherNoPrintType = needShowOtherPrint();
  //根据业务类型确定应显示个界面
  if(otherNoPrintType == "MJ")
  {
    win = window.open("../bq/GEdorTypeMJInputMain.jsp?loadFlag=PERSONALBOXVIEW&WorkNo=" + fm.WorkNo.value + "&DetailWorkNo=" + fm.WorkNo.value);
		win.focus();
  }
  if(otherNoPrintType == "BQ")
  {
    win = window.open("TaskPhoneHasten.jsp?LoadFlag=PERSONALBOXVIEW&WorkNo=" + fm.WorkNo.value + "&DetailWorkNo=" + fm.WorkNo.value+"&CustomerNo="+fm.CustomerNo.value+"&AcceptWayNo="+fm.AcceptWayNo.value+"&AcceptDate="+fm.AcceptDate.value+"&AcceptNo="+fm.AcceptNo.value+"&TypeNo="+fm.TypeNo.value+"&ScanBeforeInput=");
		win.focus();
  }
  if(otherNoPrintType == "PC")
  {
    win = window.open("../operfee/IndiPhoneVisitQuery.jsp?LoadFlag=PERSONALBOXVIEW&WorkNo=" + fm.WorkNo.value + "&DetailWorkNo=" + fm.WorkNo.value+"&CustomerNo="+fm.CustomerNo.value+"&AcceptWayNo="+fm.AcceptWayNo.value+"&AcceptDate="+fm.AcceptDate.value+"&AcceptNo="+fm.AcceptNo.value+"&TypeNo="+fm.TypeNo.value+"&ScanBeforeInput=");
		win.focus();
  }
  if(otherNoPrintType == "GC")
  {
    win = window.open("../operfee/GrpPhoneVisitQuery.jsp?LoadFlag=PERSONALBOXVIEW&WorkNo=" + fm.WorkNo.value + "&DetailWorkNo=" + fm.WorkNo.value+"&CustomerNo="+fm.CustomerNo.value+"&AcceptWayNo="+fm.AcceptWayNo.value+"&AcceptDate="+fm.AcceptDate.value+"&AcceptNo="+fm.AcceptNo.value+"&TypeNo="+fm.TypeNo.value+"&ScanBeforeInput=");
		win.focus();
  }
  if(otherNoPrintType == "DJ")
  { 
  	win = window.open("../bq/BqBalanceTimeInput.jsp?LoadFlag=PERSONALBOXVIEW&WorkNo=" + fm.WorkNo.value + "&DetailWorkNo=" + fm.WorkNo.value+"&CustomerNo="+fm.CustomerNo.value+"&AcceptWayNo="+fm.AcceptWayNo.value+"&AcceptDate="+fm.AcceptDate.value+"&AcceptNo="+fm.AcceptNo.value+"&TypeNo="+fm.TypeNo.value+"&ScanBeforeInput=");
		win.focus();
  }
  if(otherNoPrintType == "TJ")
  { 
  	win = window.open("../bq/BqBalanceTimeInput.jsp?LoadFlag=PERSONALBOXVIEW&WorkNo=" + fm.WorkNo.value + "&DetailWorkNo=" + fm.WorkNo.value+"&CustomerNo="+fm.CustomerNo.value+"&AcceptWayNo="+fm.AcceptWayNo.value+"&AcceptDate="+fm.AcceptDate.value+"&AcceptNo="+fm.AcceptNo.value+"&TypeNo="+fm.TypeNo.value+"&ScanBeforeInput=");
		win.focus();
  }
}

function getLGWorkInfo(workNo)
{
  var sql = "  select * "
            + "from LGWork "
            + "where workNo = '" + workNo + "' ";
  return easyExecSql(sql);
}