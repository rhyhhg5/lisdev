<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="TaskCheckTeamBox.jsp"%>
<%
//程序名称：TaskPersonalBox.jsp
//程序功能：工单管理个人信箱主界面
//创建日期：2005-01-15 14:10:36
//创建人  ：qiuyang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskTeamBox.js"></SCRIPT>
  <%@include file="TaskTeamBoxInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body  onload="initForm();" >
<span id="show"></span>
 <form action="./TaskTeamBoxSave.jsp" name=fm target=fraSubmit method=post>
  <b>请输入取单数：</b>
  <Div id= "divLDPerson3" style= "display: ''"> 
  <table class= common id="table8">
    <tr>
      <TD class= title> 取单数</TD>
      <TD class= input>
      	<Input class=common name="Num" size="20" >&nbsp; 
      	<INPUT VALUE="取  单" Class="cssButton" TYPE=button onclick="submitForm();">  
      </TD>
    </tr>
  </table>
  <br>
  <select name="condition" style="width: 128; height: 23" onchange="changeCondition(); easyQueryClick();" >
  <option value = "0">--请选择查询条件--</option>
  <option value = "1">未经办/正经办/审核</option>
  <option value = "2">未经办</option>
  <option value = "3">正经办</option>
  <option value = "4">审核</option>
  <option value = "5">待办（所有）</option>
  <option value = "6">待办（定期）</option>
  <option value = "7">待办（无期）</option>
  <option value = "8">结案</option>
  <option value = "9">存档</option>
  <option value = "10">过应办期</option>
  </select>
  &nbsp;
  <span id="spanSelect"  style="display: ''; position:absolute; slategray">
  </span>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  
  <INPUT VALUE="查  看" name="view" Class="cssButton" TYPE=button onclick="viewTask();">&nbsp;
  <INPUT VALUE="组长分配" name="deliver" Class="cssButton" TYPE=button onclick="viewTeamMember();">
  </DIV>
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskList);"> 
      </td>
      <td width="185" class= titleImg>工单列表 </td>
    </tr>
  </table>
  <!--
  <Div  id= "divTaskList" style= "display: ''" align=center>  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanTaskGrid" >
				</span> 
		  	</td>
		</tr>
	</table>
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();"> 
  </Div>
  -->
  
  <Div  id= "divTaskList" style= "display: ''" align=center>  
		<table  class= common>
	 		<tr  class= common>
		  		<td text-align: left colSpan=1>
				<span id="spanTaskGrid" >
				</span> 
		  	</td>
		</tr>
	</table>
	 <Div id= "divPage" align=center style= "display: 'none' ">
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();"> 
	 </Div>
	</Div>  
  </Form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
