<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-05-26 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskPurviewInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="TaskPurviewUpdateInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./TaskPurviewUpdateSave.jsp" method=post name=fm target="fraSubmit">
  	<br>
	<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return update('UPDATE');">
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRuleSetting);">
    		</td>
    		 <td class= titleImg>
        		 项目权限设置
       		 </td>
    	</tr>
    </table>
    <Div  id= "divRuleSetting" style= "display: ''">
		<table  class= common align='center' >
  			<TR  class= common>
    			<TD  class= title>
    			  项目名
    			</TD>
    			<TD  class= input>
    			  	<input class="code" name="ProjectNo" type="hidden">
    			  	<input class="common" name="ProjectNoName" readonly>
    			  	<!--input class="code" name="ProjectNoName" readonly ondblclick="return showCodeList('edortypecode', [this,ProjectNo], [1,0]);" onkeyup="return showCodeListKey('edortypecode', [this,edortypecode], [1,0]);" verify="优先级别|&code:edortypecode"-->
    			</TD>
    			 <TD  class= title>
    			  项目类型
    			</TD>
    			<TD  class= input> 
    				<input class="common" name="ProjectTypeName" readOnly>
      			</td>
    			<TD  class=title>
    			  机构类型
    			</TD>
    			<TD  class= input">
					<input class="common" name="DealOrganizationName" readOnly>
    			</TD>
  			</TR>
  			<TR  class= common>  				
  			  	<TD  class= title>
  			  	  岗位级别
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="code" name="PostNo" type="hidden">
    			  	<input class="common" name="PostNoName" readonly>
    			  	<!--input class="code" name="PostNoName" readonly ondblclick="return showCodeList('position', [this,PostNo], [1,0]);" onkeyup="return showCodeListKey('position', [this,PostNo], [1,0]);" verify="优先级别|&code:position"-->
    			</TD>			
  			  	<TD  class= title>
  			  	  金额权限
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="common" name="MoneyPurview" style="width: 130">
    			  	<input type=checkBox name="MoneyPurviewFull" value="1">全权
    			</TD>  
    			<TD  class= title>
  			  	  超额权限
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="common" name="ExcessPurview" style="width: 130">
    			  	<input type=checkBox name="ExcessPurviewFull" value="1">全权
    			</TD>   
  			</TR>
  			<TR  class= common>  				
  			  	<TD  class= title>
  			  	  期限权限
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="common" name="TimePurview" style="width: 130">
    			  	<input type=checkBox name="TimePurviewFull" value="1">全权
    			</TD>  			  	
    			<TD  class= title>
  			  	  确认权
  			  	</TD>
  			  	<TD  class= input>
    			  	<input type=radio name="ConfirmFlag" value="0">无确认权
    			  	<input type=radio name="ConfirmFlag" value="1">有确认权
    			  	<input type=radio name="ConfirmFlag" value="2">金额权
    			</TD>
    			<TD  class= title>
  			  	  岗位
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="code" name="MemberNo" type="hidden">
		      		<input class="common" name="MemberNoName" readonly >
		      		<!--input class="code" name="MemberNoName" readonly ondblclick="return showCodeList('TaskMemberNo', [this,MemberNo], [1,0]);" onkeyup="return showCodeListKey('TaskMemberNo', [this,MemberNo],[1,0]);" verify="成员编号|notnull&code:TaskMemberNo"-->
    			</TD> 
  			</TR>
  						<%-- 为配置险种时使用 	--%>
  			<tr class=common id="RiskCodeID">
  			<td class="title">
						险种代码
					</td>
<%--					<td class="input"><Input class=codeNo id=RiskCode verify="险种编码|notnull" name=RiskCode readOnly ondblclick="return showCodeList('riskprop',[this,RiskName], [0,1],1,1,msql,1);" onkeyup="return showCodeListKey('riskprop',[this,RiskName], [0,1],1,1,msql,1);"><Input class=codename name=RiskName readonly >--%>
						<TD  class= input><Input class=common  name="RiskCode" readOnly >
<%--						<Input class=common name= RiskName readOnly></TD> --%>
  			</tr>
  			<TR  class= common>
  				
  			  	<TD  class= title>
  			  	  备注
  			  	</TD>
  			  	<td class=input colspan="3">
					<textarea classs ="common" name="Remark" cols="80%" rows="3"></textarea>
				</td>     
  			</TR>
		</table>
		<INPUT VALUE="返  回" class= cssButton TYPE=button onclick="parent.close();">	
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact" value="UPDATE||MAIN">
    <input type=hidden id="ruleNo" name="ProjectPurviewNo" value="">
    <input type=hidden id="ruleNo" name="ProjectType" value="">
    <input type=hidden name="DealOrganization" value="">
    <input type=hidden name="ConfirmFlagOld" value="">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>