 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskCommonHistoryViewInit.jsp
//程序功能：工单管理工单基本信息显示页面
//创建日期：2005-04-26
//创建人  Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<script language="javascript">

function initForm()
{
	initBox();
}

//初始化输入框
function initBox()
{
	try
	{
		var sql = 	"select * "
					+ "from LGWorkTrace "
					+ "where workNo='" + "<%=request.getParameter("WorkNo")%>" + "' "
					+ "		and nodeNo='" + "<%=request.getParameter("NodeNo")%>" + "' ";
		
		var result = easyExecSql(sql);
		if(result)
		{
			fm.AcceptNo.value = result[0][0];
			fm.nodeNo.value = result[0][1];
			fm.workBox.value = result[0][2];
			fm.InDate.value = result[0][4];
			fm.ManageCom.value = result[0][6];
			fm.ManageComName.value = result[0][6];
			fm.sendPerson.value = result[0][7];
			fm.inMethod.value = result[0][3];
			showCodeName();
		}
	}
	catch(e)
	{
		alert("TaskCommonHistoryInit.jsp-->InitBox函数中发生异常:初始化界面错误!" + e);
	}
}
</script>