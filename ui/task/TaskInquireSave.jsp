<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskInputSava.jsp
//程序功能：工单管理工单录入数据保存页面
//创建日期：2005-03-28
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	System.out.println("   test");
	String FlagStr;
	String Content;
	String tInquireNo = (String) session.getAttribute("WORKNOINPUT");
	session.setAttribute("WORKNOINPUT", "");
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");

	//设置参数
	LGInquireInfoSchema tLGInquireInfoSchema = new LGInquireInfoSchema();
	tLGInquireInfoSchema.setInquireNo(tInquireNo);
	tLGInquireInfoSchema.setManageCom(request.getParameter("ManageCom"));
	tLGInquireInfoSchema.setName(request.getParameter("Name"));
	tLGInquireInfoSchema.setSex(request.getParameter("Sex"));
	tLGInquireInfoSchema.setEmail(request.getParameter("Email"));
	tLGInquireInfoSchema.setPhone(request.getParameter("Phone"));
	tLGInquireInfoSchema.setTitle(request.getParameter("Title"));
	tLGInquireInfoSchema.setContent(request.getParameter("Content"));
	
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tLGInquireInfoSchema);
	TaskInquireBL tTaskInquireBL = new TaskInquireBL();
	if (tTaskInquireBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
		Content = "数据保存失败！";
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据保存成功！";
	}
%> 
<html>
<script language="javascript">
</script>
</html>

