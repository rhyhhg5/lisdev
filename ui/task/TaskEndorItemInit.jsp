<%
//程序名称：TaskEndorItemInit.jsp
//程序功能：保全项目列表
//创建日期：2005-06-21
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT>
var turnPage3 = new turnPageClass();
var strSQL;
var rowData;

//查询作业历史
function queryEndorItemGrid()
{
	var tableName = "";
	var sql = "select statusNo "
			  + "from LGWork "
			  + "where workNo = '" + WorkNo + "' ";
	var result = easyExecSql(sql);
	
	//工单未撤销
	if(result[0][0] != "8")
	{
		tableName = "LPEdorItem";
	}
	else
	{
		tableName = "LOBEdorItem";
	}
	
	strSQL =  "	select EdorAcceptNo,EdorNo,EdorType,DisplayType,GrpContNo,a.ContNo,InsuredNo, "
				+ "		PolNo,EdorValiDate,GetMoney,a.MakeDate,a.MakeTime,a.ModifyDate,a.Operator, "
				+ "		case b.statusNo "
				+ "			when '8' then '已撤销'"
				+ "			else case EdorState "
				+ "					when '1' then '录入完成' "
				+ "					when '3' then '待录入' "
				+ "					when '0' then '保全确认' "
				+ "					when '2' then '理算完成'  "
				+ "         when '9' then '理算完成' "
				+ "				end "
				+ "		end, EdorType "
			//	+ " '已撤销 '"
				+ "	from " + tableName + " a, LGWork b "
				+ "	where a.EdorAcceptNo = b.workNo "
				+ "		and EdorAcceptNo='" + WorkNo + "' ";
					
	turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(strSQL, EdorItemGrid);
	
	return true;
}

function initOtherElements()
{
	var result = easyExecSql(strSQL);	
}

function getLGWorkInfo()
{
    var sql = "  select * "
              + "from LGWork "
              + "where workNo = '" + WorkNo + "' ";
    return easyExecSql(sql);
}

function showDetailEdorType()
{
    if(EdorItemGrid.mulLineCount == 0)
    {
      alert("没有保全项目。");
      return false;
    }
    
    if(fm.all('EdorType').value == "")
    {
      alert("请选择一个项目。");
      return false;
    }
    
    var result = getLGWorkInfo();
    if(result == null)
    {
        alert("没有查询到工单信息，请退出重试。");
        return false;
    }
    
    if(result[0][4] == "5" || result[0][4] == "6")
    {
        alert("本工单已结案，请通过\"查看理算结果\"的方式查看变更明细");
        return false;
    }
    /*
    if(result[0][4] == "7")
    {
        alert("本工单已被合并，无法察看其明细");
        return false;
    }
    */
    if(result[0][4] == "8")
    {
        alert("本工单已撤销，无法察看其明细");
        return false;
    }
    
    detailEdorType();
}

function edorAppConfirm()
{
	//window.open("../bq/ShowEdorPrint.jsp?EdorAcceptNo=<%=(String)session.getAttribute("VIEWWORKNO")%>", "")
  //window.open("../bq/ShowEdorInfo.jsp?EdorNo=<%=(String)session.getAttribute("VIEWWORKNO")%>&Operate=PRINT", "")
  	window.open("../bq/ShowEdorInfo.jsp?EdorNo="+WorkNo+"&Operate=PRINT", "")
}

//得到选中行的数据并初始化部分隐藏域的值
function getOneRowData()
{
	fm.EdorType.value = EdorItemGrid.getRowColDataByName(EdorItemGrid.getSelNo() - 1, "edorType");
	fm.EdorNo.value = EdorItemGrid.getRowColDataByName(EdorItemGrid.getSelNo() - 1, "edorNo");
	fm.ContType.value = "1";
	fm.EdorAcceptNo.value = EdorItemGrid.getRowColDataByName(EdorItemGrid.getSelNo() - 1, "edorAcceptNo");
	fm.ContNo.value = EdorItemGrid.getRowColDataByName(EdorItemGrid.getSelNo() - 1, "contNo");
	if(fm.EdorType.value == 'CM')
	{
		fm.CustomerNoBak.value = EdorItemGrid.getRowColDataByName(EdorItemGrid.getSelNo() - 1, "insuredNo");
		fm.AppntNo.value = EdorItemGrid.getRowColDataByName(EdorItemGrid.getSelNo() - 1, "insuredNo");
	}
	else
	{
		fm.CustomerNoBak.value = '<%= request.getParameter("CustomerNo")%>';
		fm.AppntNo.value = '<%= request.getParameter("CustomerNo")%>';
	}
	fm.ContNoBak.value = EdorItemGrid.getRowColDataByName(EdorItemGrid.getSelNo() - 1, "contNo");
}

//初始化项目列表
function initEdorItemGrid()
{                               
	var iArray = new Array();
	try
	{
	  	iArray[0]=new Array();
	  	iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  	iArray[0][1]="30px";            		//列宽
	  	iArray[0][2]=10;            			//列最大值
	  	iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
	  	iArray[1]=new Array();
	  	iArray[1][0]="保全受理号";
	  	iArray[1][1]="0px";
	  	iArray[1][2]=100;
	  	iArray[1][3]=3;
	  	iArray[1][21]="edorAcceptNo";
	  	
	  	iArray[2]=new Array();
	  	iArray[2][0]="批单号";
	  	iArray[2][1]="0px";
	  	iArray[2][2]=100;
	  	iArray[2][3]=3;
	  	iArray[2][21]= "edorNo";
	  	
	  	iArray[3]=new Array();
	  	iArray[3][0]="批改类型";
	  	iArray[3][1]="100px";
	  	iArray[3][2]=100;
	  	iArray[3][3]=2;
		iArray[3][4]="EdorCodeForView";
		iArray[3][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[3][9]="批改类型汉字|code:EdorCodeForView&NOTNULL";
		iArray[3][18]=250;
		iArray[3][19]= 0 ;
		iArray[3][21]= "edorTypeName";
	  	
	  	iArray[4]=new Array();
	  	iArray[4][0]="批改类型显示级别";
	  	iArray[4][1]="0px";
	  	iArray[4][2]=100;
	  	iArray[4][3]=3;      
	  	
	  	iArray[5]=new Array();
	  	iArray[5][0]="集体合同号码";
	  	iArray[5][1]="0px";
	  	iArray[5][2]=100;
	  	iArray[5][3]=3;
	  	iArray[5][21]= "grpContNo";
	  	
	  	iArray[6]=new Array();
	  	iArray[6][0]="保单号码";
	  	iArray[6][1]="100px";
	  	iArray[6][2]=100;
	  	iArray[6][3]=0;
	  	iArray[6][21]= "contNo";
	  	
	  	iArray[7]=new Array();
	  	iArray[7][0]="客户号码";
	  	iArray[7][1]="100px";
	  	iArray[7][2]=100;
	  	iArray[7][3]=0;
	  	iArray[7][21]= "insuredNo";
	  	
	  	iArray[8]=new Array();
	  	iArray[8][0]="保单险种号码";
	  	iArray[8][1]="100px";
	  	iArray[8][2]=100;
	  	iArray[8][3]=3;
	  	iArray[8][21]= "polNo";
	  	
	  	iArray[9]=new Array();
	  	iArray[9][0]="批改生效日期";
	  	iArray[9][1]="100px";
	  	iArray[9][2]=100;
	  	iArray[9][3]=3;
	  	iArray[9][21]= "eodrCValidate";
	  	
	  	iArray[10]=new Array();
	  	iArray[10][0]="补退费金额";
	  	iArray[10][1]="100px";
	  	iArray[10][2]=100;
	  	iArray[10][3]=3;
	  	iArray[10][21]= "getMoney";
	  	
	  	iArray[11]=new Array();
	  	iArray[11][0]="生成时间";
	  	iArray[11][1]="80px";
	  	iArray[11][2]=100;
	  	iArray[11][3]=0;
	  	iArray[11][21]= "makeDate";
	  	
	  	iArray[12]=new Array();
	  	iArray[12][0]="生成具体时间";
	  	iArray[12][1]="0px";
	  	iArray[12][2]=100;
	  	iArray[12][3]=3;
	  	
	  	iArray[13]=new Array();
	  	iArray[13][0]="最后保存时间";
	  	iArray[13][1]="80px";
	  	iArray[13][2]=100;
	  	iArray[13][3]=0;
	  	iArray[13][21]= "modifyDate";
	  	
	  	iArray[14]=new Array();
	  	iArray[14][0]="最后处理人";
	  	iArray[14][1]="80px";
	  	iArray[14][2]=100;
	  	iArray[14][3]=2;
		iArray[14][4]="TaskMemberNo";
		iArray[14][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[14][9]="最后处理人|code:TaskMemberNo&NOTNULL";
		iArray[14][18]=250;
		iArray[14][19]= 0 ;
		iArray[14][21]= "operator";
	  	
	  	iArray[15]=new Array();
	  	iArray[15][0]="处理状态";
	  	iArray[15][1]="80px";
	  	iArray[15][2]=100;
	  	iArray[15][3]=0;
	  	iArray[15][21]= "edorState";
	  	
	  	iArray[16]=new Array();
	  	iArray[16][0]="批改类型";
	  	iArray[16][1]="80px";
	  	iArray[16][2]=100;
	  	iArray[16][3]=3;
	  	iArray[16][21]= "edorType";
	
		EdorItemGrid = new MulLineEnter( "fm" , "EdorItemGrid" ); 
		//这些属性必须在loadMulLine前
		EdorItemGrid.mulLineCount = 0;   
		EdorItemGrid.displayTitle = 1;
		EdorItemGrid.canSel =1;
		EdorItemGrid.selBoxEventFuncName ="getOneRowData" ;     //点击RadioBox时响应的JS函数
		EdorItemGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		EdorItemGrid.hiddenSubtraction=1;
		EdorItemGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
	queryEndorItemGrid();
	initOtherElements()
}
</SCRIPT>