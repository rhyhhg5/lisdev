<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskInquireViewInit.jsp
//程序功能：工单管理客户咨询信息初始化页面
//创建日期：2005-03-29
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT>
//var WorkNo = "<%=(String) session.getAttribute("VIEWWORKNO")%>";
//if(WorkNo == null || WorkNo == "")
//{
	WorkNo = <%=request.getParameter("DetailWorkNo")%>;
//}

var InquireNo = WorkNo;

function initForm()
{
	try
	{
		initInquireInfo();
	}
	catch(re)
	{
		alert("InitForm函数中发生异常:初始化界面错误!");
	}
}

//初始化工单信息
function initInquireInfo()
{
	strSql = "select InquireNo, ManageCom, Name, Sex, " +
			 "       Email, Phone, Title, Content " +
			 "from   LGInquireInfo " +
			 "where  InquireNo = '" + InquireNo + "' ";
	var arrResult = easyExecSql(strSql);
	
	if (!arrResult)
	{
		alert("没有找到符合条件的记录!");
		return false;
	}

	fm.all("InquireNo").value = arrResult[0][0];
	fm.all("ManageCom").value = arrResult[0][1];
	fm.all("Name").value = arrResult[0][2];
	fm.all("Sex").value = arrResult[0][3];
	fm.all("Email").value = arrResult[0][4];
	fm.all("Phone").value = arrResult[0][5];
	fm.all("Title").value = arrResult[0][6];
	fm.all("Content").value = arrResult[0][7];
	
	return true;
}
</SCRIPT>