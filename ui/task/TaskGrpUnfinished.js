//程序名称：TaskGrpUnfinished.js
//程序功能：本小组成员未完成任务清单界面
//创建日期：2008-1-16
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

/* 查询工单在列表中显示结果 */
function queryTask()
{
  var strSql = "select a.WorkNo, a.AcceptNo, CodeName('priorityno',a.PriorityNo), "
             + "    (select WorkTypeName from LGWorkType where WorkTypeNo = a.TypeNo), a.CustomerNo, " 
             + "    a.CustomerName, '记录', a.acceptorNo, b.InDate, "
             + "    (select GroupName from LGGroup where GroupNo = a.AcceptCom), " 
             + "    case when a.DateLimit is null then '' else trim(char(days(current date) - days(b.InDate))) || '/' || a.DateLimit end, " 
             + "    CodeName('taskstatusno' , a.StatusNo), "
             + "    case (select count(EdorAcceptNo) from LPEdorApp where EdorAcceptNo = a.WorkNo) "
             + "    when 0 then '' else CodeName('appedorstate', (select EdorState from LPEdorApp where EdorAcceptNo = a.WorkNo)) end, " 
             + "    (Select count(NodeNo) from LGWorkRemark where WorkNo = a.WorkNo), " 
             + "    case ScanFlag when '0' then '新扫描件' when '1' then '旧扫描件' else '无扫描件' end, " 
             + "    a.DetailWorkNo, a.StatusNo, " 
             + "    case (select count(EdorAcceptNo) from LPEdorApp where EdorAcceptNo = a.WorkNo) "
             + "    when 0 then '' else CodeName('appedorstate', (select EdorState from LPEdorApp where EdorAcceptNo = a.WorkNo)) end " 
             + "from LGWork a, LGWorkTrace b " 
             + "where  a.WorkNo = b.WorkNo and a.NodeNo = b.NodeNo "
             + "    and a.AcceptorNo in (select c.memberno from LGGroupMember c, LGGroupMember d where c.GroupNo=d.GroupNo and d.MemberNo = '" + operator + "') "
             + "    and a.StatusNo != '5' "
             + "order by a.acceptorNo ";
	
  turnPage2.pageDivName = "divPage2";
  turnPage2.pageLineNum = 15;
  turnPage2.queryModal(strSql, TaskGrid);
  return true;
}

/* 查看选中的工单 */
function viewTask()
{
  var pageName = "TaskView.jsp";
  var param;
  var chkNum = 0;
  var WorkNo;
  var CustomerNo;
  var DetailWorkNo;
	
  for (i = 0; i < TaskGrid.mulLineCount; i++)
  {
    if (TaskGrid.getChkNo(i)) 
    {
      chkNum = chkNum + 1;
      if (chkNum == 1)
      {
        WorkNo = TaskGrid.getRowColData(i, 1);
        CustomerNo = TaskGrid.getRowColData(i, 5);
        DetailWorkNo = TaskGrid.getRowColData(i, 15);   
      }
    }
  }
  if (chkNum == 0)
  {
    alert("请选择一条工单！");
    return false;
  }
  if (chkNum > 1)
  {
    alert("只能选择一条工单！");
    return false;
  }
  if (WorkNo)
  {
    var width = screen.availWidth - 10;
    var height = screen.availHeight - 28;
    win = window.open("TaskViewMain.jsp?loadFlag=PERSONALBOX&WorkNo="+WorkNo+"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + DetailWorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);
    win.focus();
  }
}
