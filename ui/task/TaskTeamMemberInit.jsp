<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskTeamMemberInit.jsp
//程序功能：工单管理个人信箱页面初始化
//创建日期：2005-01-17 9:40:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>

<script language="JavaScript">
var operator = "<%=tGI.Operator%>";  //操作员编号

//表单初始化
function initForm()
{
	try
	{
		initGrid();
		//initSelect();
		easyQueryClick();
		
	}
	catch(re)
	{
		alert("TaskPersonalBoxInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

// 保单信息列表的初始化
function initGrid()
{                         
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="成员编号";         	  //列名
      iArray[1][1]="40px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="成员用户号";         	  //列名
      iArray[2][1]="40px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="成员姓名";          //列名
      iArray[3][1]="40px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[3][4]="userCode";
      
      TaskGrid = new MulLineEnter("fm", "TaskGrid"); 
      //设置Grid属性
      TaskGrid.mulLineCount = 15;
      TaskGrid.displayTitle = 1;
      TaskGrid.locked = 1;
      TaskGrid.canSel = 0;
      TaskGrid.canChk = 1;
      TaskGrid.hiddenSubtraction = 1;
      TaskGrid.hiddenPlus = 1;
      TaskGrid.loadMulLine(iArray);
      
  }
  catch(ex)
  {
  	  alert(ex);
  }
}


</script>