<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<title>客户信息查询</title>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <script src="./TaskCustomerQuery.js"></script> 
  <%@include file="TaskCustomerQueryInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body  onload="initForm();">
<!--登录画面表格-->
<form name=fm target=fraSubmit method=post>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>  客户号码 </TD>
          <TD  class= input> <Input class= common name=CustomerNo > </TD>
          <TD  class= title> 姓名 </TD>
          <TD  class= input> <Input class= common name=Name > </TD>
          <TD  class= title> 性别 </TD>
          <TD  class= input> <Input name=Sex class="code" MAXLENGTH=1 ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);" > </TD>       
          
       </TR>             
      	<TR  class= common>
          <TD  class= title> 出生日期 </TD>
          <TD  class= input>  <Input class="coolDatePicker" dateFormat="short" name=Birthday >              </TD>
          <TD  class= title> 证件类型 </TD>
          <TD  class= input> <Input class="code" name=IDType ondblclick="return showCodeList('IDType',[this]);">              </TD>
          <TD  class= title> 证件号码 </TD>
          <TD  class= input>  <Input class= common name=IDNo >   </TD>
       </TR>   
   </Table>  
      <INPUT VALUE="查  询" class= cssButton TYPE=button onclick="easyQueryClick();"> 
      <INPUT VALUE="返  回" class= cssButton TYPE=button onclick="returnParent();">   
    <Table>
    	<TR>
        	<TD class=common>
	           <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    		</TD>
    		<TD class= titleImg>
    			 客户信息
    		</TD>
    	</TR>
    </Table>    	
 <Div  id= "divLDPerson1" style= "display: ''" align=center>
   <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanPersonGrid" ></span> 
  	</TD>
      </TR>
    </Table>					
      <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="getLastPage();"> 
 </Div>					
 <p>
	<input type="button" class=cssButton name="view" value="详细信息" onclick="">&nbsp;
	<input type="button" class=cssButton name="input" value="新建受理件" onclick="javascript:location.href='TaskInput.jsp'">
	<input type="button" class=cssButton name="test" value="退保测试" onclick="">
 </p>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>					
</Form>
</body>
</html>
