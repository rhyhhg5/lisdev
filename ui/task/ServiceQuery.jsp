<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
	<head>
		<title>客户信息查询</title>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<script src="./ServiceQuery.js"></script>

		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="./ServiceQueryInit.jsp"%>

	</head>
	<body onload="initForm();">
		<!--登录画面表格-->
		<form name=fm target=fraSubmit method=post>
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						客户号码：
					</TD>
					<TD class=input>
						<Input class=common name=CustomerNo onkeydown="">
					</TD>
					<TD class=title>
						客户姓名：
					</TD>
					<TD class=input>
						<Input class=common name="CustomerName" onkeydown="">
					</TD>
					<TD class=title>
						短信记录号
					</TD>
					<TD class=input>
						<Input class="common" name="MessageId" onkeydown="">
					</TD>

				</TR>
				<TR class=common>
					<TD class=title>
						发送时间：
					</TD>
					<TD class=input>
						<Input class="coolDatePicker" dateFormat="short" name="SendTime"
							onkeydown="">
					</TD>
					<TD class=title>
						手机号码：
					</TD>
					<TD class=input>
						<Input class="common" name="Mobile" ondblclick="" elementtype="nacessary">
					</TD>
					<TD class=title>
						邮箱号码：
					</TD>
					<TD class=input>
						<Input class=common name="Email" onkeydown="" elementtype="nacessary">
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						业务关联类型：
					</TD>
					<TD class=input>
						<select name='ServiceType'>
						<option value=''></option>
							<option value='A'>
								寄发批单通知
							</option>
							<option value='B'>
								交费提醒
							</option>
							<option value='C'>
								保单失效通知
							</option>
							<option value='D'>
								转帐退费通知
							</option>
							<option value='E'>
								收费成功通知（保单服务）
							</option>
							<option value='F'>
								收费成功通知（续期服务）
							</option>
							<option value='G'>
								简易保全通知
							</option>
							<option value='H'>
								客户服务短信
							</option>
							
						</select>
					</TD>
					<TD class=title>
						业务关联号码：
					</TD>
					<TD class=input>
						<Input class="common" name="assoNo" 
							value="">
					</TD>
				</TR>
			</Table>
			<INPUT VALUE="查  询" class=cssButton TYPE=button
				onclick="easyQueryClick();">
			<INPUT VALUE="取  消" class=cssButton TYPE=button
				onclick="returnParent();">
			<Table>
				<TR>
					<TD class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor: hand;"
							OnClick="showPage(this,divLDPerson1);">
					</TD>
					<TD class=titleImg>
						短信邮件信息
					</TD>
				</TR>
			</Table>
			<Div id="divLDPerson1" style="display: ''" align=center>
				<Table class=common>
					<TR class=common>
						<TD text-align: left colSpan=1><span id="spanPersonGrid"></span>
						</TD>
					</TR>
				</Table>
				<INPUT VALUE="首  页" class=cssButton TYPE=button
					onclick="getFirstPage();">
				<INPUT VALUE="上一页" class=cssButton TYPE=button
					onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class=cssButton TYPE=button
					onclick="getNextPage();">
				<INPUT VALUE="尾  页" class=cssButton TYPE=button
					onclick="getLastPage();">
			</Div>

			<span id="spanCode" style="display: none; position: absolute;"></span>
		</Form>
	</body>
</html>
