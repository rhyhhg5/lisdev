<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskRemark.jsp
//程序功能：工单管理转为待办页面
//创建日期：2005-01-19 15:15:15
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskWait.js"></SCRIPT>
  <%@include file="TaskWaitInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>转为待办</title>
</head>
<body  onload="initForm();" >
<% 
   if (WorkNo.length == 1) 
   {
%>
  <form method=post name=fm>
  <%@include file="TaskCommonView.jsp"%>
  </form>
<% 
  }
%>
   <form action="./TaskWaitSave.jsp" method=post name=fm2 target="fraSubmit">
    <table>
      <tr>
        <td class=common>
	      	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskRemark);">
	      </td>
        <td class= titleImg>工单批注&nbsp;</td>
	    <td class=common>
	     <%if (WorkNo.length != 1) 
          {
        		out.print("(工单数:"+WorkNo.length+")");
          }
        %>
        </td>
       </tr>
    </table>
  <Div id="divTaskRemark" style="display: ''"> 
    <table class= common>
      <tr>
        <td class= title> 待办类型</td>
        <td class= input > 
			<input type="radio" name="TaskWaitType" value="0" OnClick="limitDate();" checked>定期</input>&nbsp;&nbsp;
 			<input type="radio" name="TaskWaitType" value="1" OnClick="foreverDate();">无期</input>
 		</td>
    <td class= title id="tdHide1"> 处理日期</td>
    <td class= input id="tdHide2"> 
      <input type=hidden name="PauseNo" value="">
			<Input class="coolDatePicker" name="WakeDate" elementtype="nacessary" value="" DateFormat="short" verify="处理日期|Date&notnull">
		</td>
		<td class= title id="tdHide3" style="display:''"> 处理时间(小时)
		</td>
    	<td class= input id="tdHide4" style="display:''"> 
			<Input class=common name=WakeTimeHour style="width: 30" verify="小时|int">&nbsp;时
			<Input class=common name=WakeTimeMinute style="width: 30" verify="分钟|int">&nbsp;分
		</td>
      </tr>
    </table>
    	<br>
    <input type="button" class=cssButton name="Send" value="保  存" onclick="submitForm();">&nbsp;
		<input type="button" class=cssButton name="Return" value="返  回" onclick="parent.window.close();">
		<input type="hidden" name="PauseNo">	
	</div>
  </form>
 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
