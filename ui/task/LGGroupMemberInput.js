//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交前的校验、计算  
function beforeSubmit()
{
	if (!verifyInput2()) 
	{
		return false;
	}
	
	var strSql = "select GroupNo from LGGroupMember " +
				 "where  MemberNo = '" + fm.all("MemberNo").value + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult)
	{
		var tGroupNo = arrResult[0][0];
		alert("该成员已经属于" + tGroupNo + "，不能新增！");
		return false;
	}
	return true;
}           


//提交，保存按钮对应操作
function submitForm()
{
	if (beforeSubmit())
	{
		fm.fmtransact.value = "INSERT||MAIN" ;
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.submit(); //提交
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, transact)
{
	if (transact == "INSERT||MAIN")
	{
	    fm.all('GroupNo').value = "";
	    fm.all('MemberNo').value = "";
	}
	if (transact == "DELETE||MAIN")
	{
	    fm.all('GroupNo').value = "";
	    fm.all('MemberNo').value = "";
	}

	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		showDiv(operateButton,"true"); 
		showDiv(inputButton,"false"); 
		//执行下一步操作
		window.location.reload();
	}
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("在LGGroupMember.js-->resetForm函数中发生异常:初始化界面错误!");
	}
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
	if(cDebug=="1")
	{
		parent.fraMain.rows = "0,0,50,82,*";
	}
	else {
		parent.fraMain.rows = "0,0,0,82,*";
	}
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
	//下面增加相应的代码
	//mOperate="INSERT||MAIN";
	showDiv(operateButton,"false"); 
	showDiv(inputButton,"true"); 
	fm.fmtransact.value = "INSERT||MAIN" ;
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (!verifyInput2()) 
	{
		return false;
	} 
	//下面增加相应的代码
	if (confirm("您确实想修改该记录吗?"))
	{
		var i = 0;
		var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		//showSubmitFrame(mDebug);
		fm.fmtransact.value = "UPDATE||MAIN";
		fm.submit(); //提交
	}
	else
	{
		//mOperate="";
		alert("您取消了修改操作！");
	}
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  showInfo=window.open("./LGGroupMemberQuery.html");
}      
     
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
	if (!verifyInput2()) 
	{
		return false;
	} 
	
	//下面增加相应的删除代码
	if (confirm("您确实想删除该记录吗?"))
	{
		var i = 0;
		var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		//showSubmitFrame(mDebug);
		fm.fmtransact.value = "DELETE||MAIN";
		fm.submit(); //提交
		initInpBox();
	}
	else
	{
		alert("您取消了删除操作！");
	}
}    
       
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
        fm.all('GroupNo').value= arrResult[0][0];
        fm.all('GroupNoName').value= arrResult[0][0];
        fm.all('MemberNo').value= arrResult[0][1];
        fm.all('MemberNoName').value= arrResult[0][1];
        fm.all('postNo').value= arrResult[0][2];
        fm.all('postNoName').value= arrResult[0][2];
        fm.all('SuperManager').value= arrResult[0][3];
        fm.all('SuperManagerName').value= arrResult[0][3];
	}
	showCodeName();
}               
        
