//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

// 查询按钮
function easyQueryClick()
{	
	var strSQL = "";
	var conditionCsm = fm.CustomerNo.value;
	
	// 初始化表格
	initAgentGrid();
	
	strSQL = "select g.name deptname, a.name agentname "
	        +"from laagent a, labranchgroup g where g.agentgroup in "
	        +"( select agentgroup from laagent where agentcode in "
	        +"(select agentcode from lccont where AppntNo='"+fm.CustomerNo.value
	        +"')) and a.agentgroup=g.agentgroup and agentcode in "
	        +"(select agentcode from lccont where AppntNo='"+fm.CustomerNo.value+"')";
  
  turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, AgentGrid); 
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = AgentGrid.getSelNo();
	tRow = AgentGrid.getSelNo() - 1;
	
	
	if( tSel == 0 || tSel == null )
	{
			alert( "请先选择一条记录，再点击返回按钮。" );
	}
	else
	{
		try
		{
			//arrReturn = AgentGrid.getRowColData(tRow, 1);
			//alert(AgentGrid.getRowColData(tRow, 2));
			arrReturn[0] = AgentGrid.getRowColData(tRow, 1);
			arrReturn[1] = AgentGrid.getRowColData(tRow, 2);
			//alert(arrReturn);
			top.opener.afterAgentQuery( arrReturn );
			top.close();
		}
		catch(ex)
		{
			//alert( "请先选择一条非空记录，再点击返回按钮。");
			alert( "没有发现父窗口的afterAgentQuery接口。" + ex );
		}
	}	
}




