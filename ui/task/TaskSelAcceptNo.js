//程序名称：TaskSelAcceptNo.js
//程序功能：工单管理个人信箱页面
//创建日期：2005-03-15 11:03:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

//初始化受理号选择单选组
function initSelAcceptNo()
{
	var tAcceptNo;
	var tChecked;  //radiobox初始化时选择标志
	var j = 0;     //计数
	for (i=0; i<window.opener.TaskGrid.mulLineCount; i++)
	{
		if (window.opener.fm.all("TaskGridChk")[i].checked == true)
		{
			if (j == 0) 
			{
				tChecked = " checked";
			}
			else
			{
				tChecked = "";
			}
			tAcceptNo = window.opener.fm.all("TaskGridChk")[i].value;
			document.write("<input type='radio' name='AcceptNo' value='" + 
					tAcceptNo + "' " + tChecked + ">  " + tAcceptNo + "<br>");
			j++;
		}
	}
}

//提交合并的工单
function submitForm()
{
	for (i=0; i<fm.all("AcceptNo").length; i++)
	{
		if (fm.all("AcceptNo")[i].checked)
		{
			window.opener.fm.all("AcceptNo").value = fm.all("AcceptNo")[i].value;
		}
	}
	window.opener.fm.action = "TaskUniteSave.jsp"
	window.opener.fm.submit();
	window.opener.focus();
}

//取消合并
function cancelUnite()
{
	window.close();
}