//程序名称：Appeal.js
//程序功能：投诉管理
//创建日期：2005-05-10
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容


var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//检测输入数据的合法性
function invokeCheck()
{
	var check = 0;	
	for(var i = 0; i < fm.AppealWayNo.length; i++)
	{
		if(fm.AppealWayNo[i].checked == false)
		{
			check++;
		}
		else
		{
			break;
		}
	}
	if(check == fm.AppealWayNo.length)
	{
		alert("投诉来源不能为空");
		return false;
	}
	if (fm.all("AcceptorName").value == "" || fm.AppealObjName.value == "")
	{
		alert("投诉受理人姓名和投诉对象姓名均不能为空");
		return false;
	}
  
	if (!verifyInput2())
	{
		return false;
	}
	return true;
}

//实现TaskInputTopButton.js中的invokeSubmit()接口,调用本地的submitForm()
function invokeSubmit(workNo, tTypeNo)
{	
	submitForm(workNo, tTypeNo);
	return true;
}



//提交，保存按钮对应操作
function submitForm(workNo, typeNo)
{
	if(fm.fmtransact.value == "INSERT||MAIN")	
	{
		fm.WorkNo.value = workNo;
	}
	fm.TypeNo.value = typeNo;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//location.reload();
		
		top.close();
		top.opener.focus();
		top.opener.location.reload();
		
	}	
}