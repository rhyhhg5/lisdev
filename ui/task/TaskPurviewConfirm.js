//程序名称：TaskPurviewConfirm.js
//程序功能：
//创建日期：2005-09-22
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
	//校验项目和项目类型是否相符
	var sql = "  select * "
	          + "from LMEdorItem "
	          + "where edorCode = '" + fm.ProjectNo.value + "' "
	          + "    and appObj = '" + (fm.ProjectType.value == "0" ? "I" : "G") + "' ";
	var result = easyExecSql(sql);
	if(result == null)
	{
	    alert("项目和项目类型不相符，请确认。");
	    return false;
	}
	return true;
}



//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
  {
    return false;
  }
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		window.location.reload();
		top.opener.location.reload();
	}	
}