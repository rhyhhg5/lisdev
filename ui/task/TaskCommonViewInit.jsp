<%
//程序名称：TaskCommonViewInit.jsp
//程序功能：工单管理工单基本信息初始化页面
//创建日期：2005-03-20 15:52:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT>
	
//初始化工单信息
function initTaskInfo()
{
	strSql = "select AcceptNo, CustomerNo, TypeNo, DateLimit, " +
			 "       ApplyTypeNo, ApplyName, PriorityNo, AcceptWayNo, " +
			 "       AcceptDate, AcceptCom, AcceptorNo, Remark " +
			 "from   LGWork " +
			 "where  WorkNo = '" + WorkNo + "' ";
	
	var arrResult = easyExecSql(strSql);
	
	if (!arrResult)
	{
		alert("没有找到符合条件的记录!");
		return false;
	}
	
	fm.all("WorkNo").value = WorkNo;
	fm.all("AcceptNo").value = arrResult[0][0];
	fm.all("CustomerNo").value = arrResult[0][1];
	fm.all("TopTypeNo").value = arrResult[0][2].substring(0, 2);
	fm.all("TopTypeNoName").value = arrResult[0][2].substring(0, 2);
	fm.all("TypeNo").value = arrResult[0][2];
	fm.all("TypeNoName").value = arrResult[0][2];
	fm.all("DateLimit").value = arrResult[0][3];
	fm.all("ApplyTypeNo").value = arrResult[0][4];
	fm.all("ApplyTypeNoName").value = arrResult[0][4];
	fm.all("ApplyName").value = arrResult[0][5];
	fm.all("PriorityNo").value = arrResult[0][6];
	fm.all("PriorityNoName").value = arrResult[0][6];
	fm.all("AcceptWayNo").value = arrResult[0][7];
	fm.all("AcceptWayNoName").value = arrResult[0][7];
	fm.all("AcceptDate").value = arrResult[0][8];
	fm.all("AcceptCom").value = arrResult[0][9];
	fm.all("AcceptComName").value = arrResult[0][9];
	fm.all("AcceptorNo").value = arrResult[0][10];
	fm.all("RemarkContent").value = arrResult[0][11];	 
	
	return true;
}

//实现系统自动调用的借口，在选择申请人类型为投保人是自动填充申请人姓名
function afterCodeSelect(cCodeName, Field)
{
	if(cCodeName == "ApplyTypeNo")
	{
		if(fm.ApplyTypeNo.value == "0" && fm.CustomerNo.value.length == 9)
		{
			var sql = 	"select Name "
							+ "from LDPerson " 
							+ "where CustomerNo='" + fm.CustomerNo.value + "' ";
			var result = easyExecSql(sql);
			
			if(result)
			{			
				fm.ApplyName.value = result[0][0];
			}
			else
			{
				alert("没有该客户");
			}
		}
		else
		{
			fm.ApplyName.value = "";
		}
	}
	if (cCodeName == "TaskTopTypeNo")
	{   
		fm2.TypeNo.value = "";
		fm2.TypeNoname.value = "";
	    try
	    {
		    //这段代码在TaskDeliver.jsp中调用
    		mType = fm2.all("TopTypeNo").value;
    		fm2.subTypeNoCondition.value = fm2.TopTypeNo.value + "," + fm.CustomerNo.value;
		}
		catch(e)
		{
		    //若该页面被其他页面调用，则上面的代码可能会报异常
		}
	}
}

</SCRIPT>