<%
//程序名称：TaskPhoneHastenInit.jsp
//程序功能：工单管理工单收回页面初始化
//创建日期：2005-07-22
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<script language="JavaScript">
function initForm()
{
	initHastenInfo();
	initCustomerInfo();
	
	//初始化客户业务信息
	initCustomerGrid();
	showCodeName();
}

//初始化催缴记录的信息
function initHastenInfo()
{
	var sql = 	"select a.MakeDate, a.WorkNo, a.PayReason, b.Name, a.CustomerNo, a.EdorAcceptNo,"
				+ "		a.OldBankCode, a.OldBackAccNo, a.OldPayDate, a.OldPayMode "
				+ "from LGPhoneHasten a, LDPerson b "
				+ "where a.CustomerNo=b.CustomerNo "
				+ "		and WorkNo='<%=request.getParameter("AcceptNo")%>'"
				+ "order by int(TimesNo) desc ";
	var result = easyExecSql(sql);

    if(result == null)
    {
        alert("没有查询到电话催缴信息，数据可能已丢失。");
        return false;
    }
	fm.MakeDate.value = result[0][0];
	fm.WorkNo.value = result[0][1];
	fm.PayReason.value = result[0][2];
	fm.Name.value = result[0][3];
	fm.AppntNo.value = result[0][4];
	fm.PrtNo.value = result[0][5];
	fm.EdorAcceptNo.value = result[0][5];
	if(result[0][9] == 4)
	{
		fm.PayMode[1].checked = true;
	}
	else
	{
		fm.PayMode[0].checked = true;
	}
	fm.OldPayDate.value = result[0][8];
	
	//查询缴费通知号
	sql = 	"select GetNoticeNo "
			+ "from LJSPay a, LGWork b "
			+ "where a.otherNo=b.workNo "
			+ "		and OtherNo='" + fm.EdorAcceptNo.value + "' "; 
	
	var resultArr = easyExecSql(sql);
	if(resultArr)
	{
		fm.GetNoticeNo.value = resultArr[0][0];
	}
	
	//得到工单状态，若为结案则只读
	sql = 	"select StatusNo "
			+ "from LGWork "
			+ "where workNo='" + fm.WorkNo.value + "' "; 
	
	resultArr = easyExecSql(sql);
	if(resultArr)
	{
		if(resultArr[0][0] == '5')
		{
			sql2 =  "select BankAccNo, PayDate "
					+ "from LJSPay "
					+ "where OtherNo='" + fm.EdorAcceptNo.value + "' ";
			resultArr2 = easyExecSql(sql2);
			
			fm.PayMode1.disabled = true;
			fm.PayMode2.disabled = true;
			fm.PayDate.readOnly = true;
			
			fm.cancel.style.display = "none";
			fm.saveHasten.style.display = "none";
			fm.create.style.display = "none";
			
			var sql3 = "select remark from LGTraceNodeOp "
			         + "where workNo = '" + fm.WorkNo.value + "' "
			         + "order by operatorNo desc ";
			var result = easyExecSql(sql3);
			if(result)
			{
			    fm.Remark.value = result[0][0];
			    fm.Remark.readOnly = true;
			}
			
			if(resultArr2)
			{
				if(resultArr2[0][0] != null && resultArr2[0][0] != "")
				{
					fm.PayMode[1].checked = true;
				}
				else
				{
					fm.PayMode[0].checked = true;
				}
				fm.PayDate.value = resultArr2[0][1];
			}
		}
	}
}

//初始化客户信息
function initCustomerInfo()
{
	//未退保
	var sql =
		"	select HomePhone, CompanyPhone, Mobile "
		+ "	from LCAddress a, LCAppnt b "
		+ "	where a.CustomerNo = b.AppntNo "
		+ "		and a.AddressNo = b.AddressNo "
		+ "		and b.ContNo="
		+ "			(select ContNo "
		+ "			from LPEdorMain "
		+ "			where edorAcceptNo='" + fm.EdorAcceptNo.value + "')";
		
	var result = easyExecSql(sql);
	
	//已退保
	var sql =
		"	select HomePhone, CompanyPhone, Mobile "
		+ "	from LCAddress a, LBAppnt b "
		+ "	where a.CustomerNo = b.AppntNo "
		+ "		and a.AddressNo = b.AddressNo "
		+ "		and b.ContNo="
		+ "			(select ContNo "
		+ "			from LPEdorMain "
		+ "			where edorAcceptNo='" + fm.EdorAcceptNo.value + "')";
	var result2 = easyExecSql(sql);
	
	if(result)
	{
		fm.HomePhone.value = result[0][0];
		fm.CompanyPhone.value = result[0][1];
		fm.MobilePhone.value = result[0][2];
	}
	else if(result2)
	{
		fm.HomePhone.value = result2[0][0];
		fm.CompanyPhone.value = result2[0][1];
		fm.MobilePhone.value = result2[0][2];
	}
}
</script>