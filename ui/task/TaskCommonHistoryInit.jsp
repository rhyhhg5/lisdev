<%
//程序名称：TaskCommonCustomerInit.jsp
//程序功能：工单管理客服信息初始化页面
//创建日期：2005-03-20 15:20:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<SCRIPT>
var turnPage2 = new turnPageClass();

//查询作业历史
function queryHistoryGrid()
{
	var strSql = 
		"select a.NodeNo, OperatorNo, a.WorkNo, OperatorType, char(a.MakeDate) || ' ' || a.MakeTime, "
		+ "a.Operator, char(FinishDate) || ' ' || FinishTime, a.Remark "
		+ "from LGTraceNodeOp a, LGWorkTrace b "
		+ "where a.workNo=b.workNo "
		+ "		and a.nodeNo=b.nodeNo "
		+ "		and a.workNo='" + WorkNo + "' "
		+ "order by int(b.nodeNo) desc, int(operatorNo) desc";
	
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, HistoryGrid);
	
	return true;
}

// 保单信息列表的初始化
function initHistoryGrid()
{                         
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=200;            			//列最大值
		iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="轨迹结点号";         	  //列名
		iArray[1][1]="35px";            	//列宽
		iArray[1][2]=200;            			//列最大值
		iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="轨迹具体信息节点号";         	  //列名
		iArray[2][1]="35px";            	//列宽
		iArray[2][2]=200;            			//列最大值
		iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="受理号";         	  //列名
		iArray[3][1]="120px";            	//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="操作类型";         	//列名
		iArray[4][1]="80px";            	//列宽
		iArray[4][2]=200;            			//列最大值
		iArray[4][3]=2;              			//是否允许输入,1表示允许，0表示不允许
		iArray[4][4]="operatortype";
		iArray[4][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[4][9]="操作类型|code:operatortype&NOTNULL";
		iArray[4][18]=250;
		iArray[4][19]= 0 ; 
		
		iArray[5]=new Array();
		iArray[5][0]="生成时间";              //列名
		iArray[5][1]="120px";            	//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[6]=new Array();
		iArray[6][0]="处理人";         	//列名
		iArray[6][1]="80px";            	//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		iArray[6][4]="TaskMemberNo";
		iArray[6][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[6][9]="处理人|code:TaskMemberNo&NOTNULL";
		iArray[6][18]=250;
		iArray[6][19]= 0 ; 
		
		iArray[7]=new Array();
		iArray[7][0]="完成时间";              //列名
		iArray[7][1]="120px";            	//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
		
		iArray[8]=new Array();
		iArray[8][0]="批注";              //列名
		iArray[8][1]="190px";            	//列宽
		iArray[8][2]=200;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		HistoryGrid = new MulLineEnter("fm", "HistoryGrid"); 
		//这些属性必须在loadMulLine前
		HistoryGrid.mulLineCount = 10;
		HistoryGrid.displayTitle = 1;
		HistoryGrid.locked = 1;
		HistoryGrid.canSel = 1;
		HistoryGrid.canChk = 0;
		HistoryGrid.hiddenSubtraction = 1;
		HistoryGrid.hiddenPlus = 1;
		HistoryGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
	
	queryHistoryGrid();
}
</SCRIPT>