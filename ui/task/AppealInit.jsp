 <%
//程序名称：AppealInit.jsp
//程序功能：
//创建日期：2005-03-29
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            
<%
   GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");	
	
%>
<script language="JavaScript">
	

function initInputBox()
{ 
	var DetailWorkNo = "<%=request.getParameter("DetailWorkNo")%>";
	
	var ss = 	"select max(to_number(dealNo)) " +
						"from LGAppeal " +
						"where appealNo='" + DetailWorkNo + "'";
	var arrDealNo = easyExecSql(ss);
	var dealNo = arrDealNo[0][0];	
	
	var sqlSelect = "select AppealWayNo, AcceptorName, AcceptorNo, AcceptorPost, AppealObjName, " +
										"AppealObjNo, AppealObjPhone, AppealObjDep, DirectBoss, AppealContent, " +
										"AppealFeeback, CustomerOpinion, DealTwiseFlag, ServiceMangerOpinion " +
									"from LGAppeal " +
									"where AppealNo='" + DetailWorkNo + "' and " +
											"dealNo='" + dealNo + "'";
	var result = easyExecSql(sqlSelect);
	
	if(result)
	{
		if(result[0][0])
		{
			fm.AppealWayNo[result[0][0]].checked = true;
		}
		fm.AcceptorName.value =result[0][1];
		fm.AcceptorNo.value =result[0][2];
		fm.AcceptorPost.value =result[0][3];
		fm.AppealObjName.value =result[0][4];
		fm.AppealObjNo.value =result[0][5];
		fm.AppealObjPhone.value =result[0][6];
		fm.AppealObjDep.value =result[0][7];
		fm.DirectBoss.value =result[0][8];
		fm.AppealContent.value =result[0][9];
		fm.AppealFeeback.value =result[0][10];
		fm.CustomerOpinion.value = result[0][11];
		fm.ServiceMangerOpinion.value =result[0][13];		
		if(result[0][12] != "" && result[0][12] != null)
		{
			fm.DealTwiseFlag[result[0][12]].checked = true;
		}
		fm.WorkNo.value = DetailWorkNo;
		
		document.all("forButton").style.display = "";
		document.all("fmtransact").value = "UPDATE||MAIN";
		
		showCodeName();
	}
	else
	{
  	try
  	{ 
  		fm.AppealWayNo[0].checked = false;
  		fm.AppealWayNo[1].checked = false;
			fm.AcceptorName.value ="";
			fm.AcceptorNo.value ="";
			fm.AcceptorPost.value ="";
			fm.AppealObjName.value ="";
			fm.AppealObjNo.value ="";
			fm.AppealObjPhone.value ="";
			fm.AppealObjDep.value ="";
			//fm.AppealObjDepName.value ="";
			fm.DirectBoss.value ="";
			fm.AppealContent.value ="";
			fm.AppealFeeback.value ="";
			fm.CustomerOpinion.value = "";
			fm.ServiceMangerOpinion.value ="";
			document.all("fmtransact").value = "INSERT||MAIN";
  	}
  	catch(ex)
  	{
  	  alert("AppealInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  	}
  }
}

function initForm()
{
  try
  {
    initInputBox();  
    initElementtype();
  }
  catch(re)
  {
    alert("LGGroupInputInit.jsp-->InitForm函数中发生异常:初始化界面错误: " + re);
  }
}
</script>
