<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskPersonalBox.jsp
//程序功能：工单管理工单查询界面
//创建日期：2005-3-23 19:55:21
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="TaskQuery.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <%@include file="TaskQueryInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body  onload="initForm();" >
 <form name=fm target=fraSubmit method=post>
<br></br>
  <table  class= common align=center>
  	<TR  class= common>
      <TD  class= title> 受理号 </TD>
      <TD  class= input> <Input class= common name="AcceptNo" > </TD>
      <TD  class= title> 客户号 </TD>
      <TD  class= input> <Input class= common name="CustomerNo" > </TD>

			<TD class= title> 受理人 </TD>
	        <TD class= input>
	        	<input class="code" name="usercode" type="hidden">
	        	<input class="code" name="username" readonly ondblclick="return showCodeList('usercode', [this,usercode], [1,0]);" onkeyup="return showCodeListKey('usercode', [this,usercode], [1,0]);" verify="受理人|&code:usercode">
	        </TD>
    </TR>
  	<TR  class= common>

     <TD class= title>
         受理机构 
     </TD>
     <TD class= input>
        	<input class="code" name="Groupno" type="hidden">
        	<input class="code" name="Groupname"  readonly ondblclick="return showCodeList('acceptcom', [this,Groupno], [1,0]);" onkeyup="return showCodeListKey('acceptcom', [this,Groupno], [1,0]);" verify="受理机构|&code:acceptcom">
     </TD>
     <TD class= title>
        业务类型
     </TD>
     <TD class=input>
         	<input class="codeno" name="Businessno" ondblclick="return showCodeList('tasktoptypeno', [this,Businessname], [0,1]);" onkeyup="return showCodeListKey('tasktoptypeno', [this,Businessname], [0,1]);" verify="业务类型|&code:tasktoptypeno" ><input class="codename" name="Businessname"  readonly >
     </TD>
     <TD class= title>
        子业务类型
     </TD>
     <TD class=input>
         	<input class="codeno" name="SubBusinessno" ondblclick="fm.SuperTypeNo.value = fm.Businessno.value;return showCodeList('tasktypeno', [this,SubBusinessName], [0,1],null,fm.SuperTypeNo.value,'SuperTypeNo', 1);" onkeyup="fm.SuperTypeNo.value = fm.Businessno.value;return showCodeListKey('TaskTypeNo', [this,SubBusinessName], [0,1], null, fm.SuperTypeNo.value, 'SuperTypeNo', 1);" ><input class="codename" name="SubBusinessName"  readonly >
     </TD>
    </TR>
     <TR class=common>
     <TD  class= title> 当前经办人 </TD>
     <TD  class= input> <Input class= common name="CurrentHandlePerpon" > </TD>
       <TD  class= title>当前经办小组 </TD>
      <TD  class= input> <Input class= common name="CurrentHandleGroup" > </TD>
     <TD class= title>
        处理状态
     </TD>
     <TD class=input>
         	<input class="codeno" name="TaskStatusNo" ondblclick="return showCodeList('TaskStatusNo', [this,TaskStatusName], [0,1]);" onkeyup="return showCodeListKey('TaskStatusNo', [this,TaskStatusName], [0,1]);" verify="处理状态|&code:TaskStatusNo"><input class="codename" name="TaskStatusName"  readonly >
     </TD>
    </TR>
    <TR class=common>
      <TD class= title>
        保全状态
     </TD>
     <TD class=input>
         	<input class="codeno" name="BQStatusNo" ondblclick="return showCodeList('appedorstate', [this,BQStatusName], [0,1]);" onkeyup="return showCodeListKey('appedorstate', [this,BQStatusName], [0,1]);" verify="保全状态|&code:appedorstate"><input class="codename" name="BQStatusName"  readonly >
     </TD>
     <td  class= title> 
     受理时间起始 
     	</td>
     <td  class= input>
     <input class="coolDatePicker" dateFormat="short" name="startDate" >
     </td>
     <td class = title>
      受理时间截至
    	</td> 
     <td  class= input>
     <input class="coolDatePicker" dateFormat="short" name="endDate" >
     </td> 
    <TD> 
    <INPUT VALUE="重 置" TYPE=button Class="cssButton" name="query" onclick="clearData();">  
    </TD>
   </TR> 	  
  
   </Table>  
   <INPUT VALUE="查  询" TYPE=button Class="cssButton" name="query" onclick="easyQueryClick();">
   <INPUT VALUE="查看明细" TYPE=button Class="cssButton" name="query" onclick="viewTask();">
   <input type="button" class=cssButton name="return" value="返  回" onclick="GoBack();">
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskList);"> 
      </td>
      <td width="185" class= titleImg>工单列表 </td>
    </tr>
  </table>
  <Div  id= "divTaskList" style= "display: ''" align=center>  
  	<table  class= common>
   		<tr  class= common>
  	  		<td text-align: left colSpan=1>
				<span id="spanResultGrid" >
				</span> 
		  	</td>
		</tr>
	</table>
	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage(); showCodeName();"> 
	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage(); showCodeName();"> 					
	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage(); showCodeName();"> 
	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage(); showCodeName();"> 
  </Div>
  <input class="common" name="SuperTypeNo" type="hidden">
  </Form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
