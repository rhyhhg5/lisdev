<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：TaskInputQuery.jsp
//程序功能：工单管理咨询录入页面
//创建日期：2005-02-21 16:10:42
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskInputQuery.js"></SCRIPT>
  <%@include file="TaskInputQueryInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>工单录入 </title>
  <script>
  	var operator = "<%=tGI.Operator%>";  //操作员编号
  </script>
</head>
<body  onload="initForm();">
  <form action="./TaskInputSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divTaskInfo);">
	      </td>
        <td class= titleImg>咨询信息&nbsp;</td>
	    </tr>
    </table>
  	<Div id="divTaskInfo" style="display: ''"> 
 <table class="common" id="table2">
		<tr class="common8">
			<td class="title8">客户号码</td>
			<td class="input8"><input class="common" name="CustomerNo"></td>
			<td class="title8">客户名称</td>
			<td class="input8"><input class="common" name="CustomerName"></td>
			<td class="title8">客户类型</td>
			<td class="input8"><input class="code8" name="CustomerType"></td>
		</tr>
		<tr class="common8">
			<td class="title8">是否咨询专家</td>
			<td class="input8"><input class="common" name="ExpertFlag"></td>
			<td class="title8">专家编号</td>
			<td class="input8"><input class="common" name="ExpertNo"></td>
			<td class="title8">专家姓名</td>
			<td class="input8"><input class="common" name="ExpertName"></td>
		</tr>
		<tr class="common8">
			<td class="title8">疾病代码</td>
			<td class="input8"><input class="code8" name="DiseaseCode"></td>
			<td class="title8">期待回复日期</td>
			<td class="input8"><input class="common" name="ExpRDate"></td>
		</tr>
		<tr class="common8">
			<td class="title8">咨询级别</td>
			<td class="input8"><input class="common" name="AskGrade"></td>
			<td class="title8">咨询有效标志</td>
			<td class="input8"><input class="common" name="	AvaiFlag"></td>
		</tr>
		<tr class="common">
			<td class="input" colSpan="6">咨询主题</td>
		</tr>
		<tr class="common">
			<td class="input" colSpan="6">
			<textarea class="common" name="CSubject" cols="100" witdh="25%" rows="1"></textarea> 
			</td>
		</tr>
		<tr class="common">
			<td class="title" colSpan="6">咨询内容</td>
		</tr>
		<tr class="common">
			<td class="input" colSpan="6">
			<textarea class="common" name="CContent" rows="3" cols="100" witdh="25%"></textarea> 
			</td>
		</tr>
		<tr class="common">
			<td class="input" colSpan="6">客户现状</td>
		</tr>
		<tr class="common">
			<td class="input" colSpan="6">
			<textarea class="common" name="CustStatus" rows="3" cols="100" witdh="25%"></textarea> 
			</td>
		</tr>
		<tr class="common8">
			<input class="common" type="hidden" name="PeplyState">
		</tr>
	</table>
	<div>
		<input class="cssButton" onclick="insertCust()" type="button" value="保 存">
		<input class="cssButton" onclick="deleteCust()" type="button" value="删 除"> 
	</div>

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
 
</body>
</html>
