<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskInputSava.jsp
//程序功能：工单管理工单录入数据保存页面
//创建日期：2005-01-17 14:27:43
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%
	String FlagStr;
	String Content;
	String tWorkNo = "";
	String tAccpetNo = "";
	String tDetailWorkNo = "";
	
	//得到业务类型
	String tTypeNo = request.getParameter("TypeNo");
	session.setAttribute("TypeNo", tTypeNo);
	if ((tTypeNo == null) || (tTypeNo.equals("")))
	{
		tTypeNo = request.getParameter("TopTypeNo");
	}
	String statusNo = "2";
	String temp = request.getParameter("statusNo");
	if(temp != null && temp.equals("3"))
	{
		statusNo = "3";
	}

	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	//输入参数
	LGWorkSchema tLGWorkSchema = new LGWorkSchema();
	tLGWorkSchema.setCustomerNo(request.getParameter("CustomerNo"));
	tLGWorkSchema.setCustomerCardNo(request.getParameter("IDNo"));
	tLGWorkSchema.setStatusNo(statusNo);
	tLGWorkSchema.setPriorityNo(request.getParameter("PriorityNo"));
	tLGWorkSchema.setTypeNo(tTypeNo);
	tLGWorkSchema.setDateLimit(request.getParameter("DateLimit"));
	tLGWorkSchema.setApplyTypeNo(request.getParameter("ApplyTypeNo"));
	tLGWorkSchema.setApplyName(request.getParameter("ApplyName"));
	tLGWorkSchema.setAcceptWayNo(request.getParameter("AcceptWayNo"));
	tLGWorkSchema.setAcceptDate(request.getParameter("AcceptDate"));
	tLGWorkSchema.setRemark(request.getParameter("Remark"));
	//代理人标记
	String agentFlag=request.getParameter("AgentFlag");
	if(null!=agentFlag && !"".equals(agentFlag)){
		tLGWorkSchema.setAgentFlag(agentFlag);
		if(agentFlag.equals("0")){
			tLGWorkSchema.setAgentName(request.getParameter("ProxyName"));
			tLGWorkSchema.setAgentIDType(request.getParameter("ProxyIDType"));
			tLGWorkSchema.setAgentIDNo(request.getParameter("ProxyIDNo"));
			tLGWorkSchema.setAgentIDStartDate(request.getParameter("ProxyIDStartDate"));
			tLGWorkSchema.setAgentIDEndDate(request.getParameter("ProxyIDEndDate"));
			tLGWorkSchema.setAgentPhone(request.getParameter("ProxyPhone"));
		}
	}
	System.out.println(request.getParameter("AgentFlag")+"************代理标记");
	VData tVData = new VData();
	tVData.add(tLGWorkSchema);
	tVData.add(tGI);
	
	TaskInputBL tTaskInputBL = new TaskInputBL();
	if (tTaskInputBL.submitData(tVData, "") == false)
	{
		FlagStr = "Fail";
		Content = "数据保存失败！";
	}
	else
	{
		//设置显示信息
		VData tRet = tTaskInputBL.getResult();
		LGWorkSchema mLGWorkSchema = new LGWorkSchema();
		mLGWorkSchema.setSchema((LGWorkSchema) tRet.getObjectByObjectName("LGWorkSchema", 0));
		
		tWorkNo = mLGWorkSchema.getWorkNo();
		tAccpetNo = mLGWorkSchema.getAcceptNo();
		tDetailWorkNo = mLGWorkSchema.getDetailWorkNo();
		
		//把WorkNo保存到session中
		session.setAttribute("WORKNOINPUT", tWorkNo);
		
		FlagStr = "Succ";
		Content = "数据保存成功，录入工单的受理号为：" + tAccpetNo;
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>", "<%=tWorkNo%>", "<%=tDetailWorkNo%>", "<%=tTypeNo%>");
</script>
</html>

