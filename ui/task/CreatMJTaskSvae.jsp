<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CreatMJTaskSvae.jsp
//程序功能：
//创建日期：2006-01-18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.taskservice.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
    String flag = null;
    String content = null;
    
    GEdorMJTask task = new GEdorMJTask();
    task.oneCompany((GlobalInput)session.getValue("GI"));
    if(task.mErrors.needDealError())
    {
        flag = "Fail";
        content = PubFun.changForHTML(task.mErrors.getErrContent());
    }
    else
    {
        flag = "Succ";
        content = "续期催缴成功";
    }
%>

<html>
<script language="javascript">
    parent.fraInterface.afterSubmit1("<%=flag%>", "<%=content%>");
</script>
</html>