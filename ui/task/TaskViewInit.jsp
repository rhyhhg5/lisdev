<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ScanContInit.jsp
//程序功能：工单管理工单查看页面
//创建日期：2005-01-19
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  String tWorkNo = "";
	String loadFlag = request.getParameter("loadFlag");
	System.out.println("\n\n\n\nloadFlag:" + loadFlag);
	
	tWorkNo = request.getParameter("WorkNo");  
	
	//从察看页面调用
  //if(loadFlag != null && loadFlag.equals("TaskViewTopButton"))
	//{
	//  tWorkNo = request.getParameter("WorkNo");
	//}
	//else if(loadFlag != null && loadFlag.equals("QUERY"))  //该页面被查询页面调用
	//{
	//	tWorkNo = request.getParameter("WorkNo");
	//}
  //else
	//{	
	//  tWorkNo = (String) session.getAttribute("VIEWWORKNO");
	//}
	
%>
<script language="JavaScript">
var WorkNo = "<%=tWorkNo%>";
var customerNo = "<%=request.getParameter("CustomerNo")%>";

//初始化表单
function initForm()
{
	initTaskInfo();     //初始化工单信息，在TaskCommonViewInit.jsp页面定义
	initHistoryGrid();  //初始化作业历史信息，在TaskCommonHistoryInit.jsp页面定义
	if(customerNo.length == 9)
	{
		initEdorItemGrid();	//个单，初始化项目信息列表，在TaskEndorItem.jsp中定义
		controlButton();
	}
	else	
	{
		initGrpEdorItemGrid();	//团单，初始化项目信息列表，在TaskGrpEndorItem.jsp中定义
		controlButton();
	}
	initCustomerGrid(); //初始化客服信息，在TaskCommonCustomerInit.jsp页面定义
	initContGrid();   //初始化个单信息， 在TaskCommonCont.jsp中定义
	initGrpContGrid();	//初始化个单信息， 在TaskCommonCont.jsp中定义
	initUWErrGrid();
	initManuUWGrid();
	showCodeName();
}

</script>