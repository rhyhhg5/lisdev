<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskCommonView.jsp
//程序功能：工单管理工单基本信息显示页面
//创建日期：2005-03-20 15:52:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<%@include file="TaskCommonViewInit.jsp"%>
	<Input type="hidden" class= common name="WorkNo">
    <table id="tableTaskInfo">
      <tr>
	      <td  class=common>
	      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTaskInfo);">
	      </td>
      <td class= titleImg>工单信息&nbsp;</td>
	  </tr>
    </table>
    <Div id="divTaskInfo" style="display: ''"> 
    <table class=common>
      <tr class=common>
        <TD class= title> 受理号 </TD>
        <TD class= input>
      		<Input class="readonly" name="AcceptNo" readonly >
        </TD>
        <TD class= title> 客户号 </TD>
        <TD class= input>
	    	<Input class="readonly" name="CustomerNo" onkeydown="queryByCustomerNo();"  readonly > 
	    	<input class=cssbutton type=button name="queryCustomer" style="display: none" value="查  看" onclick="window.open('../sys/GrpPolQueryInput.jsp?CustomerNo=' + fm.CustomerNo.value,'');">
	    </TD>  
	    <TD class= title> 优先级别 </TD>
        <TD class= input>
        	<input class="readonly" name="PriorityNo" type=hidden>
        	<input class="readonly" name="PriorityNoName" ondblclick="return showCodeList('PriorityNo', [this, PriorityNo], [1, 0]);" onkeyup="return showCodeListKey('PriorityNo', [this, PriorityNo], [1, 0]);" readonly>
        </TD>
      </tr>
      <tr>
        <TD class= title> 业务类型 </TD>
        <TD class= input>
        	<input class="readonly" name="TopTypeNo" type=hidden>
        	<input class="readonly" name="TopTypeNoName" readonly ondblclick="return showCodeList('TaskTopTypeNo', [this]);" onkeyup="return showCodeListKey('TaskTopTypeNo', [this]);">
        </TD>
        <TD class= title> 子业务类型 </TD>
        <TD class= input>
        	<input class="readonly" name="TypeNo" type=hidden>
        	<input class="readonly" name="TypeNoName" readonly ondblclick="return showCodeList('TaskTypeNo', [this, TypeNo], [1, 0], null, '03', 'SuperTypeNo', 1);" onkeyup="return showCodeListKey('TaskTypeNo', [this, TypeNo], [1, 0], null, fm.all('TopTypeNo').value, 'SuperTypeNo', 1);">
        </TD>
        <TD class= title> 时限 </TD>
        <TD class= input>
        	<input class="readonly" name="DateLimit" readonly >
        </TD>
      </tr>
      <tr>
        <TD class= title> 申请人类型 </TD>
        <TD class= input>
        	<input class="readonly" name="ApplyTypeNo" type=hidden>
        	<input class="readonly" name="ApplyTypeNoName" readonly ondblclick="return showCodeList('ApplyTypeNo', [this, ApplyTypeNo], [1, 0]);" onkeyup="return showCodeListKey('ApplyTypeNo', [this, ApplyTypeNo], [1, 0]);">
        </TD>
        <TD class= title> 申请人姓名 </TD>
        <TD class= input>
        	<input class="readonly" name="ApplyName" readonly >
         </TD> 
        <TD class= title> 受理途径 </TD>
        <TD class= input>
        	<input class="readonly" name="AcceptWayNo" type=hidden>
        	<input class="readonly" name="AcceptWayNoName" ondblclick="return showCodeList('AcceptWayNo', [this, AcceptWayNo], [1, 0]);" onkeyup="return showCodeListKey('AcceptWayNo', [this, AcceptWayNo], [1, 0]);">
        </TD>    
      </tr>
      <tr>
        <TD class= title> 受理机构 </TD>
        <TD class= input>
        	<input class="readonly" name="AcceptCom" type=hidden>
            <input class="readonly" name="AcceptComName" readOnly ondblclick="return showCodeList('AcceptCom', [this, AcceptCom], [1, 0]);" onkeyup="return showCodeListKey('AcceptCom', [this, AcceptCom], [1, 0]);">
        </TD>
        <TD class= title> 受理人 </TD>
        <TD class= input>
        	<Input class="readonly" name="AcceptorNo" readonly >
        </TD>
        <TD class= title> 受理日期 </TD>
        <TD class= input>
        	<Input class="readonly" name="AcceptDate" readonly >
        </TD>        
      </tr>
      <tr>
        <TD  class= title> 备注 </TD>
        <TD  colspan="7"  class= input>
        	<textarea class="readonly" readonly style="border: none; overflow: hidden; background-color: #F7F7F7;" name="RemarkContent" cols="90%" rows="3"></textarea> 
        </TD>
      </tr>
    </table>
    </Div>