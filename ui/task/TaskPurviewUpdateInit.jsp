<%
//程序名称：TaskPurviewUpdateInit.jsp
//程序功能：项目权限设置界面
//创建日期：2005-07-05
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script language="javascript">
var turnPage = new turnPageClass();
var ProjectPurviewNo = "<%=request.getParameter("ProjectPurviewNo")%>";

function initForm()
{
	initBox();
	initElementtype();
	showCodeName();
}

function initBox()
{
	//查询并显示项目权限信息
	var sql =
	 	"select distinct ProjectPurviewNo, b.EdorName, "
		+ "		case ProjectType "
		+ "			when '0' then '个险' "
		+ "			when '1' then '团险' "
		+ "		end, "
		+ "		case DealOrganization "
		+ "			when '0' then '总公司' "
		+ "			when '1' then '分公司' "
		+ "		end, "
		+ "		CodeName, MoneyPurview, ExcessPurview, TimePurview, NeedOtherAudit, "
		+ "     Remark, ConfirmFlag, "
		+ "     (select UserName "
		+ "     from LDUser "
		+ "     where userCode = a.MemberNo),a.riskcode "
		+ "from LGProjectPurview a, LMEdorItem b, LDCode c "
		+ "where a.ProjectNo = b.EdorCode "
		+ "    and a.PostNo = c.Code "
		+ "    and c.codeType = 'position' "
		+ "    and ProjectPurviewNo='" + ProjectPurviewNo + "' ";
	var result = easyExecSql(sql);
	
	if(result)
	{
		fm.ProjectPurviewNo.value = ProjectPurviewNo;
		fm.ProjectNo.value = result[0][1];
		fm.RiskCode.value = result[0][12];
		fm.ProjectNoName.value = result[0][1];
		fm.ProjectTypeName.value = result[0][2];
		if(result[0][2]=="个险")
		{
			fm.ProjectType.value = 0;
		}
		else
		{
			fm.ProjectType.value = 1;
		}
		fm.DealOrganizationName.value = result[0][3];
		if(fm.DealOrganizationName.value == "总公司")
		{
			fm.DealOrganization.value = 0;
		}
		else
		{
			fm.DealOrganization.value = 0;
		}
		fm.PostNo.value = result[0][4];
		fm.PostNoName.value = result[0][4];
		
		if(result[0][5] == '9999999999')
		{
			fm.MoneyPurviewFull.checked = true;
		}
		else
		{
			fm.MoneyPurview.value = result[0][5];
		}
		
		if(result[0][6] == '9999999999')
		{
			fm.ExcessPurviewFull.checked = true;
		}
		else
		{
			fm.ExcessPurview.value = result[0][6];
		}
		
		if(result[0][7] == '9999999999')
		{
			fm.TimePurviewFull.checked = true;
		}
		else
		{
			fm.TimePurview.value = result[0][7];
		}
		fm.Remark.value = result[0][9];
		
		//权限类型
		fm.all("ConfirmFlag")[result[0][10]].checked = true;
		if(result[0][10] != "2")
		{
		    //不是金额权
		    fm.MoneyPurview.readOnly = true;
		    fm.ExcessPurview.readOnly = true;
		    fm.TimePurview.readOnly = true;
		    
		    fm.MoneyPurviewFull.disabled = true;
		    fm.ExcessPurviewFull.disabled = true;
		    fm.TimePurviewFull.disabled = true;
		    fm.ConfirmFlag[2].disabled = true;
		}
	    else
	    {
	        fm.ConfirmFlag[0].disabled = true;
	        fm.ConfirmFlag[1].disabled = true;
	    }
		
		fm.ConfirmFlagOld.value = result[0][10];
		if(result[0][11] == "00000000000000000000")
		{
			result[0][11] = "";
		}
        fm.MemberNo.value = result[0][11];
        fm.MemberNoName.value = result[0][11];
	}
}

//保存修改


</script>