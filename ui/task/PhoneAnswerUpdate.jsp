<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：PhonAnswerUpdate.jsp
//程序功能：电话回访修改页面
//创建日期：2005-04-03
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<%@include file="PhoneAnswerUpdateInit.jsp"%>
	<SCRIPT src="PhoneAnswerUpdate.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>电话回访经办 </title>
</head>

<body onload="initForm();">
 <form action="PhoneInterviewSave.jsp" method=post name=fm target="fraSubmit">
 	<!-- 工单接口标志, 值为"TASK"表明工单系统调用 "TASKFRAME" 表明在框架里调用 -->
  <input type=hidden id="LoadFlag" name="LoadFlag">
 	<input type=button class="cssbutton" value="保  存" onclick="submitForm();">
 	<input type=hidden name="TypeNo" value="">
 	<input type=hidden name="phoneWorkNo" value="">
 	<br><br>
  <table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divStart);"> 
      </td>
      <td width="100%" class= titleImg>开篇语</td>
    </tr>
  </table>
  <Div  id= "divStart" style= "display: ''" align=center> 
  	<div id="div1" style="display:''"> 
  	<table  class= common>
  		<tr class=commonstyle="display:''">
  			<td colspan=2 class=commom>
  				<input type=hidden name="item1" value="1">
  				<div id = "question1">一、您好，请问您是***先生/女士/小姐吗？</div>
  			</td>
  		</tr>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer1" value="0" onclick="nextOne('div2');">是
					<input type=radio name="answer1" value="1" onclick="showCue('cue1');">不是
				</td>
			</tr>
			<tr class= common id="cue1" style="display: none">
				<td class=common colspan=2>
					&nbsp;&nbsp;“不好意思打扰了，我们以后再联系吧”。<br>
					&nbsp;&nbsp;“对不起／谢谢您，打扰了。再见！”<br>
					&nbsp;&nbsp;“您能告诉我他／她什么时候回来？可以联系到他／她的电话／手机？”
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark1" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
	
		<div id="div2" style="display:none"> 
  	<table  class= common>
  		<tr class=common>
  		<td colspan=2>
  			<input type=hidden name="item2" value="2">
  				二、我是人保健康险公司电话回访人员,代表人保健康公司和您的业务员,
  			感谢您购买我们的产品，您的保单已经生效，为了维护您的权益，与您做一次电话回访，
  			现在可以吗？
  			</td>
  		</tr>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer2" value="0" onclick="nextOne('div3');">方便
					<input type=radio name="answer2" value="1" onclick="showCue('cue2');">不方便
				</td>
			</tr>
			<tr class= common id="cue2" style="display: none">
				<td class=common colspan=2>
					&nbsp;&nbsp;“对不起，打扰了。”<br>
					&nbsp;&nbsp;“那什么时间比较方便呢？谢谢！我在**时间再与您联络。再见！”
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark2" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
	</div>
		
	<table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQuestionary);"> 
      </td>
      <td width="100%" class= titleImg>问卷</td>
    </tr>
  </table>
  <Div  id= "divQuestionary" style= "display: ''" align=center> 
		<div id="div3" style="display:none"> 
  	<table  class= common>
  		<tr class=common>
  			<td class=common colspan=2>
  				<input type=hidden name="item3" value="3">
  				三、那您在《人身保险投保提示函》上签过字了吧？
  			</td>
  		</tr>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer3" value="0" onclick="nextOne('div4');">是
					<input type=radio name="answer3" value="1" onclick="showCue('cue3');">不是
					<input type=radio name="answer3" value="2" onclick="showCue('cue3');">非一年期险种
				</td>
			</tr>
			<tr class= common id="cue3" style="display: none">
				<td class=common colspan=2>
					&nbsp;&nbsp;"我们会通知业务员上门为您办理补签。"
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark3" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="div4" style="display:none"> 
  	<table  class= common>
			<tr class=common>
				<td class=commmon colspan=2>
					<input type=hidden name="item4" value="4">
					四、请问投保书是您和被保人亲自签名的吗？
				</td>
			</tr>
    	<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer4" value="0" onclick="nextOne('div5');">是
					<input type=radio name="answer4" value="1" onclick="showCue('cue4');">不是
				</td>
			</tr>
			<tr class= common id="cue4" style="display: none">
				<td class=common colspan=2>
					&nbsp;&nbsp;投保书的签名对您很重要，您可以到我公司来补签名，
					也可以安排业务员上门为您办理，您看怎样方便？”（在第10题中记录客户要求）
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark4" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="div5" style="display:none"> 
  	<table  class= common>
			<tr class=common>
				<td class=common colspan=2>
					<input type=hidden name="item5" value="5">
					五、请问您已签收保单了吗？
				</td>
			</tr>
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer5" value="0" onclick="nextOne('div6');">收到并亲自签收
					<input type=radio name="answer5" value="1" onclick="nextOne('div6');">收到非亲自签收
					<input type=radio name="answer5" value="2" onclick="showCue('cue5');">未收到保单
				</td>
			</tr>
			<tr class= common id="cue5" style="display: none">
				<td class=common colspan=2>
					&nbsp;&nbsp;“您的保单现正处在送达过程中，我们会再帮您催促一下的。”
				</td>
			</tr>			 
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark5" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="div6" style="display:none"> 
  	<table  class= common>
			<tr class=common>
				<td class=common colspan=2>
					<input type=hidden name="item6" value="6">
					<table>
            <tr> 
              <td class= common> 
              	<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divQuestion6);"> 
              </td><br>
              <td class= titleImg>六、请问您购买的是以下保险吗？</td>
              <td class= common> 
                &nbsp;&nbsp;
              </td>
            </tr>
          </table>
					<div id = "divQuestion6">
					  <table  class= common>
           		<tr  class= common>
          	  		<td text-align: left colSpan=1>
          				<span id="spanLCPolGrid" >
          				</span> 
          	  		</td>
          		</tr>
          	</table>
          	<Div id= "divPage" align="center" style= "display: 'none' ">
          		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
          		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
          		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
          		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 
          	</Div>
					</div>
				</td>
    	</tr>  
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer6" value="0" onclick="nextOne('div7');">对
					<input type=radio name="answer6" value="1" onclick="nextOne('div7');showCue('cue6');">不对
				</td>
			</tr>
			<tr class= common id="cue6" style="display: none">
				<td class=common colspan=2>
					&nbsp;&nbsp;那么，请您仔细阅读您的保险条款，同时我们安排您的业务员和您再作确认可以吗？<br>
					您看什么时间比较合适？”
				</td>
			</tr>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark6" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>	
		
		<div id="div7" style="display:none"> 
  	<table  class= common>
			<tr class=common>
				<td class=common colspan=2>
					<input type=hidden name="item7" value="7">
					七、请问您对这份保险的内容是否了解呢？特别是关于保险责任、责任免除及退保方面的内容？
				</td>
    	</tr>  
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer7" value="0" onclick="nextOne('div8');">是
					<input type=radio name="answer7" value="1" onclick="nextOne('div8'); showCue('cue7');">不是
				</td>
			</tr>
			<tr class= common id="cue7" style="display: none">
				<td class=common colspan=2>
				&nbsp;&nbsp;“那请您可以抽空看一下条款，以帮助您更好的了解这份产品。如果有什么不清楚的，
				可随时拔打我们的服务电话4006695518（或北京地区也可拨打95518）咨询。”
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark7" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="div8" style="display:none"> 
  	<table  class= common>
			<tr class=common>
				<td class=common colspan=2>
					<input type=hidden name="item8" value="8">
					八、顺便提醒您，在您收到保单后享有十天犹豫期
				</td>
    	</tr>  
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer8" value="0" onclick="nextOne('div9');">知道
					<input type=radio name="answer8" value="1" onclick="nextOne('div9'); showCue('cue8');">不知道
				</td>
			</tr>			
			<tr class= common id="cue8" style="display: none">
				<td class=common colspan=2>
							&nbsp;&nbsp;解释：“十天犹豫期是指您在收到保单后的十天里，如果觉得这份保险不适合您，
					可以向公司申请放弃本次保障，公司将退还您所交保费。”<br>
							&nbsp;&nbsp;如客户还想进一步了解犹豫期的内容，提示语：“请您阅读一下条款说明，
					或与业务员或公司服务热线4006695518（或北京地区也可拨打95518）联系，进行咨询。”
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark8" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="div9" style="display:none"> 
  	<table  class= common>
			<tr class=common>
				<td class=common colspan=2>
					<input type=hidden name="item9" value="9">
					<div id="question9">九、为了方便以后联系，再与您确认一下，您的联系地址是……邮编是……
				  </div>
				</td>
    	</tr>  
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer9" value="0" onclick="nextOne('div10');">是
					<input type=radio name="answer9" value="1" onclick="nextOne('div10'); showCue('cue9');">不是
				</td>
			</tr>
			<tr class= common id="cue9" style="display: none">
				<td class=common colspan=2>
							&nbsp;&nbsp;记录新的地址和邮编
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark9" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
		
		<div id="div10" style="display:none"> 
  	<table  class= common>
			<tr class=common>
				<td class=common colspan=2>
					<input type=hidden name="item10" value="10">
					十、请问您对我公司还有什么意见或建议吗？(客户要求预约服务的请记录)
				</td>
    	</tr>  
			<tr class=common>
				<td class=input colspan=2>
					<input type=radio name="answer10" value="0">有
					<input type=radio name="answer10" value="1">没有
					<input type=radio name="answer10" value="2">客户有预约
				</td>
			</tr>
			<tr class=common>
				<td class=input valign="top">
					&nbsp;备注
				</td>
				<td>
					<textarea classs ="common" name="remark10" cols="80%" rows="3"></textarea>
				</td>
			</tr>
		</table>
		</div>
	</div>	
	
	
	<table>
    <tr> 
      <td width="17"> 
      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEnd);"> 
      </td>
      <td width="100%" class= titleImg>结束语</td>
    </tr>
  </table>
  <Div  id= "divEnd" style= "display: ''" align=center> 
  <table>
  	<tr class=common>
  		<td class=common colspan=2>
  			&nbsp;&nbsp;“非常感谢您的配合，如果以后您有保险方面的需求，或者您的联系地址发生变更，
  			请直接联系您的业务员或拨打我公司的咨询电话4006695518（或北京地区95518）。祝您健康，再见！”
  		</td>
  	</tr>
	</table>
  		
	<input type=hidden id="fmtransact" name="fmtransact" value="UPDATE||MAIN">
  <input type=hidden id="fmAction" name="fmAction">
  <input type=hidden id="workNO" name="workNo">
  <input type=hidden id="contNo" name="contNo">
  </Div>
  </Form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>