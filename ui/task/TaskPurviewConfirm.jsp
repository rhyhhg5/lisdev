<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：TaskPurviewConfirm.jsp
//程序功能：快速新建岗位级别确认权
//创建日期：2005-09-22 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskPurviewConfirm.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<script>
  function initForm()
  {
    try
    {
      initElementtype();
    }
    catch(ex)
    {
      alert("页初始化错误，请关闭后重新打开！");
    }
  }
</script>
<body onload="initForm();" >
  <form action="./TaskPurviewConfirmSave.jsp" method=post name=fm target="fraSubmit">
  	<br>
	  <INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="submitForm();">
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRuleSetting);">
    		</td>
    		 <td class= titleImg>
        		 项目权限设置
       		 </td>
    	</tr>
    </table>
    <Div  id= "divRuleSetting" style= "display: ''">
		<table  class= common align='center' >
  			<TR  class= common>
    			<TD  class= title>
    			  项目名
    			</TD>
    			<TD  class= input>
    			  	<input class="codeNo" name="ProjectNo" ondblclick="return showCodeList('edortypecode', [this,ProjectNoName], [0,1]);" onkeyup="return showCodeListKey('edortypecode', [this,ProjectNoName], [0,1]);" verify="项目名|notnull&code:edortypecode"><input class="codeName" name="ProjectNoName" elementtype="nacessary" readonly >
    			</TD>
    			 <TD  class= title>
    			  项目类型
    			</TD>
    			<TD  class= input>
    			  <input class="codeNo" name="ProjectType"  CodeData="0|^1|现金^4|银行^9|其它"  ondblclick="return showCodeList('ProjectType', [this,ProjectTypeName], [0,1]);" onkeyup="return showCodeListKey('ProjectType', [this,ProjectTypeName], [0,1]);" verify="项目类型|notnull&code:ProjectType"><input class="codeName" name="ProjectTypeName" elementtype="nacessary" readonly >
      		</td>
    			<!--
    			<TD  class=title>
    			  机构类型
    			</TD>
    			<TD  class= input">
    				<select name="DealOrganization" style="width: 160; height: 23">
    					<option value = "9"></option>
      					<option value = "0">总公司</option>
      					<option value = "1">分公司</option>      					
      				</select>
    			</TD>
      			-->
  			</TR>
  			<TR  class= common>	  				
  			  	<TD  class= title>
  			  	  确认权
  			  	</TD>
  			  	<TD  class= input colspan=3>
      				<input type=checkBox name="ConfirFlag01" value="1">PA01&nbsp;
      				<input type=checkBox name="ConfirFlag02" value="1">PA02&nbsp;
      				<input type=checkBox name="ConfirFlag03" value="1">PA03&nbsp;
      				<input type=checkBox name="ConfirFlag04" value="1">PA04&nbsp;
      				<input type=checkBox name="ConfirFlag05" value="1">PA05&nbsp;
      				<input type=checkBox name="ConfirFlag06" value="1">PA06&nbsp;
      				<input type=checkBox name="ConfirFlag07" value="1">PA07&nbsp;
      				<input type=checkBox name="ConfirFlag08" value="1">PA08&nbsp;
      				<input type=checkBox name="ConfirFlag09" value="1">PA09&nbsp;
    			</TD> 
  			</TR>
  			<tr class=common id="Post" style="display: none">
  				<TD  class= title colspan=2></td>
  				<TD  class= title colspan=4>pa01&nbsp; pa02&nbsp; pa03&nbsp; pa04&nbsp; pa05&nbsp; pa06&nbsp; pa07&nbsp; pa08&nbsp; pa09</TD>
  			</tr>
  			<tr class=common id="CheckFlag" style="display: none">
  				<TD  class= title colspan=2></td>
  				<TD  class= input colspan=4 style="display: ''">
  					<input type=checkBox name=PA01 value=1>&nbsp;
  					<input type=checkBox name=PA02 value=1>&nbsp;
  					<input type=checkBox name=PA03 value=1>&nbsp;
  					<input type=checkBox name=PA04 value=1>&nbsp;
  					<input type=checkBox name=PA05 value=1>&nbsp;
  					<input type=checkBox name=PA06 value=1>&nbsp;
  					<input type=checkBox name=PA07 value=1>&nbsp;
  					<input type=checkBox name=PA08 value=1>&nbsp;
  					<input type=checkBox name=PA09 value=1>&nbsp;
  				</TD>
  			</tr>
  			<!--为个人设置确认权时使用-->
  			<tr class=common id="MemberPurviewId" style="display: none">
  				<TD  class= title colspan=2></td>
  				<TD  class= input colspan=4 style="display: ''">
  					<input type=radio name="hasConfirmFlag" value="1">有确认权&nbsp;
  					<input type=radio name="hasConfirmFlag" value="0">无确认权&nbsp;
  				</TD>
  			</tr>
  			<TR  class= common>		
          <TD  class= title>
            备注
          </TD>
          <td class=input colspan="3">
            <textarea classs ="common" name="Remark" cols="80%" rows="3"></textarea>
          </td>     
  			</TR>
		</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact" value="INSERT||MAIN">
    <input type=hidden id="ruleNo" name="ProjectPurviewNo" value="">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>