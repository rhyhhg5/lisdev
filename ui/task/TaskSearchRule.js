//
//程序名称：
//程序功能：功能描述
//创建日期：2005-05-27 10:20:45
//创建人  ：Yang Yalin
//更新人  ：  
//

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) 
{
  showInfo.close();
  window.focus();
  
  if (FlagStr == "Fail" ) 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else 
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
    easyQueryClick();
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() 
{			    
	var strSql = 	"select ruleNo, RuleContent, sourceComCode, goalType, Target, "
					+ "	(select ownerNo from LGWorkBox where workBoxNo = target), RuleDescription, "
					+ "	case when defaultFlag='1' then '默认' else '' end, sourceComCode, goalType, "
					+ "	(select ownerNo from LGWorkBox where workBoxNo = target)" 
					+ "from LGAutoDeliverRule where 1=1 "
    			  	+ getWherePart("RuleContent", "RuleContent")
    			  	+ getWherePart("GoalType", "GoalType")
    			  	+ getWherePart("RuleTarget", "RuleTarget");
  //alert(strSql);
	turnPage.queryModal(strSql, LGGroupMemberGrid);
	
  	showCodeName();
}
function showOne(parm1, parm2) 
{	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}

function returnParent()
{
    var arrReturn = new Array();
	var tSel = LGGroupMemberGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
	{
		window.opener.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	}
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery2( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LGGroupMemberGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LGGroupMemberGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}

//将选中信箱设为默认目标
function setAsDefault()
{
	var row = LGGroupMemberGrid.getSelNo();
	
	if(row < 1)
	{
		alert("请选择需要设为默认的信箱");
		return false;
	}
	
	fm.action = "TaskRuleSettingSave.jsp"
	  + "?target=" + LGGroupMemberGrid.getRowColDataByName(row - 1, "target")
	  + "&ruleNo=" + LGGroupMemberGrid.getRowColDataByName(row - 1, "ruleNo")
	  + "&SourceComCode=" + LGGroupMemberGrid.getRowColDataByName(row - 1, "SourceComCode")
	  + "&goalType=" + LGGroupMemberGrid.getRowColDataByName(row - 1, "goalType");
		
	fm.fmtransact.value = "SETDEFAULT||MAIN";
	submitForm();
}

function submitForm(workNo, typeNo)
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	window.focus();
	fm.submit();
}