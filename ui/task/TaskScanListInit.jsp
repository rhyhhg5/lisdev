<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskCommonCustomerInit.jsp
//程序功能：工单管理扫描件列表初始化页面
//创建日期：2005-04-10
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%> 

<%
  String loadFlag = request.getParameter("loadFlag");
%>
<SCRIPT>

var turnPage = new turnPageClass();
var scanTypeChangeFlag = 0;		//是否需要改变扫描件类型，0：不需要，1：需要

//表单初始化
function initForm()
{
  try 
  {
    initScanGrid();
  }
  catch (ex)
  {
    alert(ex);
  }
}

//显示被点中行的扫描件
function showPic()
{
	var row = ScanGrid.getSelNo();
	
	top.fraPic.service.src = top.fraPic.arrPicName[row-1];
	if(scanTypeChangeFlag == 0)
	{
		window.open("TaskScanStatusSave.jsp?workNo=" + "<%=(String)session.getAttribute("VIEWWORKNO")%>","", "height=1,width=1,top=0,left=0");
	}
	scanTypeChangeFlag = 1;
}

function queryScanGrid()
{
	var WorkNo = "<%=request.getParameter("WorkNo")%>";
				
	//if (DetailWorkNo == null)
	if (WorkNo == null)
	{
		return false;
	}  
	
	var sql = "  select a.DocID, a.PageCode, (select Name from LDCom where ComCode = a.ManageCom), "
	          + "   b.BussNo, a.MakeDate, a.MakeTime, a.Operator, "
	          + "   (select Name from LDCom x, LDUser y "
	          + "   where x.ComCode = y.ComCode and y.UserCode=a.Operator) "
				    + "from ES_Doc_Pages a, ES_Doc_Relation b "
				    + "where a.docID = b.docID "
				    + "   and bussNo = '" + WorkNo + "'";
	if("<%=loadFlag%>" == "itemDetail")
	{
	  sql = sql 
	        + "		and (subType='BQ01' or subType='BQ02') ";
	}
  else
  {
    sql = sql
          + "		and subType='BQ01' ";
  }
  
	turnPage.queryModal(sql, ScanGrid);
	return true;		
}


//扫描件列表初始化
function initScanGrid()
{                         
	var iArray = new Array();
	try
	{
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0";            		//列宽
    iArray[0][2]=200;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="扫描流水号";  
    iArray[1][1]="50px";   
    iArray[1][2]=200;     
    iArray[1][3]=0;
    iArray[1][21]="DocID";
    
    iArray[2]=new Array();
    iArray[2][0]="页码";  
    iArray[2][1]="20px";   
    iArray[2][2]=200;     
    iArray[2][3]=0;		
    iArray[2][21]="PageCode";		
    
    iArray[3]=new Array();
    iArray[3][0]="受理机构";         	  //列名
    iArray[3][1]="100px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="ManageCom";
    
    iArray[4]=new Array();
    iArray[4][0]="业务受理号";              //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="BussNo";
    
    iArray[5]=new Array();
    iArray[5][0]="扫描日期";              //列名
    iArray[5][1]="50px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="MakeDate";
    
    iArray[6]=new Array();
    iArray[6][0]="扫描时间";         	//列名
    iArray[6][1]="50px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="MakeTime";
    
    iArray[7]=new Array();
    iArray[7][0]="扫描人";         	//列名
    iArray[7][1]="50px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21]= "Operator";
    
    iArray[8]=new Array();
    iArray[8][0]="扫描人机构";         	//列名
    iArray[8][1]="100px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21]= "OperatorCom";
    
    ScanGrid = new MulLineEnter("fm", "ScanGrid"); 
    //这些属性必须在loadMulLine前
    ScanGrid.mulLineCount = 6;
    ScanGrid.displayTitle = 1;
    ScanGrid.locked = 1;
    ScanGrid.canSel = 1;
    ScanGrid.canChk = 0;
    ScanGrid.hiddenSubtraction = 1;
    ScanGrid.selBoxEventFuncName = "showPic";
    ScanGrid.hiddenPlus = 1;
    ScanGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert(ex);
	}
	queryScanGrid();
	showCodeName();
}
</SCRIPT>