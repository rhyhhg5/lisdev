
<%
//程序名称：PhoneInterview.jsp
//程序功能：工单管理工单收回页面初始化
//创建日期：2005-03-31
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>              

<script language="JavaScript">
//表单初始化
function initForm()
{
	fm.all("TypeNO").value=parent.fraInterface.fm.all("TypeNo").value;
	
	initTextarea();
	
  initLCPolInfo();
  
  var rowOfSelectedCont = parent.ContGrid.getSelNo() - 1;
  if(rowOfSelectedCont != undefined && rowOfSelectedCont != null 
    && rowOfSelectedCont != null && rowOfSelectedCont != ""
    && rowOfSelectedCont >= 0)
  {
    setContInfoForInterview(parent.ContGrid.getRowColDataByName(rowOfSelectedCont,"contNo"));
  }
}

function initTextarea()
{
	for(var i = 1; i < fm.all.length; i++)
	{
	}
}

function initLCPolInfo()
{
  var iArray = new Array();
  try
  {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="0px";            		//列宽
      iArray[0][2]=200;            			//列最大值
      iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[0][21]="index";
      
      iArray[1]=new Array();
      iArray[1][0]="被保人";         	  //列名
      iArray[1][1]="30px";            	//列宽
      iArray[1][2]=200;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="insuredName";
      
      iArray[2]=new Array();
      iArray[2][0]="险种代码";         	  //列名
      iArray[2][1]="30px";            	//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="riskCode";
      
      iArray[3]=new Array();
      iArray[3][0]="险种名称";         	  //列名
      iArray[3][1]="120px";            	//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][21]="riskName";
      
      iArray[4]=new Array();
      iArray[4][0]="期交保费";          //列名
      iArray[4][1]="30px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[4][21]="prem";
      
      iArray[5]=new Array();
      iArray[5][0]="缴费年期";         	//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      iArray[5][21]="payYears";
    
      LCPolGrid = new MulLineEnter("fm", "LCPolGrid"); 
      //设置Grid属性
      LCPolGrid.mulLineCount = 0;
      LCPolGrid.displayTitle = 1;
      LCPolGrid.locked = 1;
      LCPolGrid.canSel = 0;
      LCPolGrid.canChk = 0;
      LCPolGrid.hiddenSubtraction = 1;
      LCPolGrid.hiddenPlus = 1;
      LCPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	  alert(ex);
  }
}
</script>