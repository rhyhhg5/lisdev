<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：工单管理工单转交数据保存页面
//创建日期：2005-05-26 20:14:18
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="TaskPurviewInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="TaskPurviewInputInit.jsp"%>
  <Script>
  var msql=" 1 and exists (select 1 from lmriskapp where riskcode=lmrisk.riskcode and  subriskflag=#M# and riskprop=#I#) ";
</Script>
</head>
<body  onload="initForm();" >
  <form action="./TaskPurviewInputSave.jsp" method=post name=fm target="fraSubmit">
  	<br>
  	<INPUT class=cssButton VALUE="快速新建岗位级别确认权"  TYPE=button onclick="return createConfirm();">
	<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return save();">
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRuleSetting);">
    		</td>
    		 <td class= titleImg>
        		 项目权限设置
       		 </td>
    	</tr>
    </table>
    <Div  id= "divRuleSetting" style= "display: ''">
		<table  class= common align='center' >
  			<TR  class= common>
    			<TD  class= title>
    			  项目名
    			</TD>
    			<TD  class= input>
    			  	<input class="code" name="ProjectNo" type="hidden">
    			  	<input class="code" name="ProjectNoName" elementtype="nacessary" readonly ondblclick="return showCodeList('edortypecode', [this,ProjectNo], [1,0]);" onkeyup="return showCodeListKey('edortypecode', [this,edortypecode], [1,0]);" verify="优先级别|&code:edortypecode">
    			</TD>
    			 <TD  class= title>
    			  项目类型
    			</TD>
<%--    			<TD  class= input colspan="3">--%>
<%--    				<select name="ProjectType" elementtype="nacessary" style="width: 160; height: 23">--%>
<%--    					<option value = "9"></option>--%>
<%--      					<option value = "0">个险</option>--%>
<%--      					<option value = "1">团险</option>      					--%>
<%--      				</select>--%>
<%--      				--%>
<%--      			</td>--%>
      			<TD class= input><input class="codeNo" name=ProjectType CodeData="0|^1|团险|^0|个险" ondblclick="return showCodeListEx('ProjectType',[this,ProjectTypeName],[0,1]);" ><input class=codename name=ProjectTypeName elementtype="nacessary" readonly ></TD>
    			<!--
    			<TD  class=title>
    			  机构类型
    			</TD>
    			<TD  class= input">
    				<select name="DealOrganization" style="width: 160; height: 23">
    					<option value = "9"></option>
      					<option value = "0">总公司</option>
      					<option value = "1">分公司</option>      					
      				</select>
    			</TD>
      			-->
  			</TR>
  			<TR  class= common>  				
  			  	<TD  class= title>
  			  	  岗位级别
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="code" name="PostNo" type="hidden">
    			  	<input class="code" name="PostNoName" elementtype="nacessary" readonly ondblclick="return showCodeList('position', [this,PostNo], [1,0]);" onkeyup="return showCodeListKey('position', [this,PostNo], [1,0]);" verify="优先级别|&code:position">
    			</TD>			
  			  	<TD  class= title>
  			  	  金额权限
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="common" name="MoneyPurview" maxLength = 10 style="width: 130">
    			  	<input type=checkBox name="MoneyPurviewFull" value="1">全权
    			</TD>  
    			<TD  class= title>
  			  	  超额权限
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="common" name="ExcessPurview" maxLength = 10 style="width: 130">
    			  	<input type=checkBox name="ExcessPurviewFull" value="1">全权
    			</TD>   
  			</TR>
  			<TR  class= common>  				
  			  	<TD  class= title>
  			  	  期限权限
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="common" name="TimePurview" maxLength = 10 style="width: 130">
    			  	<input type=checkBox name="TimePurviewFull" value="1">全权
    			</TD>  			  				
  			  	<TD  class= title>
  			  	  确认权
  			  	</TD>
  			  	<TD  class= input colspan="3">
  			  		<!--
    			  	<select name="ConfirmFlag" style="width: 160; height: 23" onChange="showConFirmFlag();">
      					<option value = "2">金额权</option>
      					<option value = "1">确认权</option>      					
      				</select>
      				-->
      				<input type=radio name="ConfirmFlag" value="0" onclick="clearMoney();">无确认权
      				<input type=radio name="ConfirmFlag" value="1" onclick="clearMoney();">有确认权
      				<input type=radio name="ConfirmFlag" value="2" checked>金额权
    			</TD> 
  			  	<!--
    			<TD  class= title>
  			  	  岗位
  			  	</TD>
  			  	<TD  class= input>
    			  	<input class="code" name="MemberNo" type="hidden">
		      		<input class="code" name="MemberNoName" readOnly style="width: 100" ondblclick="return showCodeList('TaskMemberNo', [this,MemberNo], [1,0]);" onkeyup="return showCodeListKey('TaskMemberNo', [this,MemberNo],[1,0]);" verify="成员编号|notnull&code:TaskMemberNo">
    			  	<input type="button" Class="cssButton" name="search" value="查  询" onclick="queryMember();">
    			</TD> 
    			-->
  			</TR>
  			<tr class=common id="Post" style="display: none">
  				<TD  class= title colspan=2></td>
  				<TD  class= title colspan=4>pa01&nbsp; pa02&nbsp; pa03&nbsp; pa04&nbsp; pa05&nbsp; pa06&nbsp; pa07&nbsp; pa08&nbsp; pa09</TD>
  			</tr>
  			<tr class=common id="CheckFlag" style="display: none">
  				<TD  class= title colspan=2></td>
  				<TD  class= input colspan=4 style="display: ''">
  					<input type=checkBox name=PA01 value=1>&nbsp;
  					<input type=checkBox name=PA02 value=1>&nbsp;
  					<input type=checkBox name=PA03 value=1>&nbsp;
  					<input type=checkBox name=PA04 value=1>&nbsp;
  					<input type=checkBox name=PA05 value=1>&nbsp;
  					<input type=checkBox name=PA06 value=1>&nbsp;
  					<input type=checkBox name=PA07 value=1>&nbsp;
  					<input type=checkBox name=PA08 value=1>&nbsp;
  					<input type=checkBox name=PA09 value=1>&nbsp;
  				</TD>
  			</tr>
  			<!--为个人设置确认权时使用-->
  			<tr class=common id="MemberPurviewId" style="display: none">
  				<TD  class= title colspan=2></td>
  				<TD  class= input colspan=4 style="display: ''">
  					<input type=radio name="hasConfirmFlag" value="1">有确认权&nbsp;
  					<input type=radio name="hasConfirmFlag" value="0">无确认权&nbsp;
  				</TD>
  			</tr>
			<%-- 为配置险种时使用 	--%>
  			<tr class=common id="RiskCodeID" style="display: none">
  			<td class="title">
						险种代码
					</td>
					<TD  class= input><Input class=codeno  name="RiskCode" ondblclick="return showCodeList('riskcode',[this,RiskName],[0,1],null,msql,1);" onkeyup="return showCodeListKey('riskcode',[this,RiskName],[0,1],null,msql,1);" ><Input class=codename name= RiskName></TD>
<%--						<TD  class= input><Input class=codeno  name="RiskCode" onClick="showCodeList('riskprop2',[this,RiskName],[0,1],null,1,msql,1,250);" onkeyup="showCodeListKeyEx('riskprop2',[this,RiskName],[0,1],null,1,msql,1,250);" ><Input class=codename name= RiskName></TD> --%>
  			</tr>
  			<TR  class= common>
  				
  			  	<TD  class= title>
  			  	  备注
  			  	</TD>
  			  	<td class=input colspan="3">
					<textarea classs ="common" name="Remark" cols="80%" rows="3"></textarea>
				</td>     
  			</TR>
		</table>
    </Div>
    <input type=hidden id="fmtransact" name="fmtransact" value="INSERT||MAIN">
    <input type=hidden id="ruleNo" name="ProjectPurviewNo" value="">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>