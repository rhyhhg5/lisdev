//程序名称：TaskBack.js
//程序功能：工单管理工单录入页面
//创建日期：2005-01-18 18:34:36
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mOption;  			//表示工单提交后是转交还是待办
var mContFlag = -1;		//表示是个单还是团单
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();


var operateWhileEdit = "";  //编辑工单时的操作方式：经办"dobusiness"、保存"save"、转为待办"wait"
var doBusinessFlag = 0;	//判断是保存按钮还是经办按钮被按下，在提交时把工单置成不同的状态。
						//1：经办，0：保存
var oprtAfterDobusi = 0;//判断是转交、待作是否是在经办之后进行的操作。
						//1：是，不需生成新的工单；0：不是，生成新的工单
var workNoReturn = "";	//保存生成的工单号

//判断是否是回车键
function isEnterDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if (keycode == "13")
	{
		return true;
	}
	else
	{
		return false;
	}
}

function showProxyInfo(){
	
	if(top.fraTopButton.document.all.doBuss.disabled){
		return true;
	}
	
	if(document.all("isProxy").checked){
		document.all("ProxyInfo").style.display = "";//显示代理人信息
		
		fm.all('AgentFlag').value="0";
		//修改代理人信息为必录项
//		fm.all('ProxyName').elementtype="nacessary";
//		fm.all('ProxyIDType').elementtype="nacessary";
//		fm.all('ProxyIDNo').elementtype="nacessary";
//		fm.all('ProxyIDStartDate').elementtype="nacessary";
//		fm.all('ProxyIDEndDate').elementtype="nacessary";
//		fm.all('ProxyPhone').elementtype="nacessary";
		
		fm.all('ProxyName').verify="代理人姓名|notnull";
		fm.all('ProxyIDType').verify="证件类型|notnull&code:IDType";
		fm.all('ProxyIDNo').verify="证件号码|notnull&len<20";
		fm.all('ProxyIDStartDate').verify="证件生效日期|notnull&date";
		fm.all('ProxyIDEndDate').verify="证件失效日期|notnull&date";
		fm.all('ProxyPhone').verify="代理人联系电话|notnull&len<20";
	}else{
		document.all("ProxyInfo").style.display = "none";
		
		fm.all('AgentFlag').value="1";
		//不勾选则不录入
//		fm.all('ProxyName').elementtype="";
//		fm.all('ProxyIDType').elementtype="";
//		fm.all('ProxyIDNo').elementtype="";
//		fm.all('ProxyIDStartDate').elementtype="";
//		fm.all('ProxyIDEndDate').elementtype="";
//		fm.all('ProxyPhone').elementtype="";

		fm.all('ProxyName').verify="";
		fm.all('ProxyIDType').verify="";
		fm.all('ProxyIDNo').verify="";
		fm.all('ProxyIDStartDate').verify="";
		fm.all('ProxyIDEndDate').verify="";
		fm.all('ProxyPhone').verify="";
	}
}

//按客户号查询
function queryByCustomerNo()
{
	if (isEnterDown())
	{
        subTypeNoCondition = fm.TopTypeNo.value + "," + fm.CustomerNo.value; //查询子业务类型的条件,Init.jsp中定义

		if(((fm.CustomerNo.value).length != 8) && ((fm.CustomerNo.value).length != 9))
		{
			alert("客户号必须为8位或9位");
			return false;
		}
		
		mContFlag = "0";
		
		//查询个单客户信息
		var strSql = "select * from LDPerson where 1 = 1 " + 
				      getWherePart('CustomerNo');
		var arrResult = easyExecSql(strSql);		
		
		if (arrResult)
		{			
			//显示个体客户信息
			document.all("CustomerTable").style.display = "";
			document.all("divCustomerInfo").style.display = "";
			//隐藏团体客户信息
			document.all("GroupTable").style.display = "none";
			document.all("divGroupInfo").style.display = "none";
			//显示个单信息
			document.all("ContTable").style.display = "";
			document.all("divContList").style.display = "";
			document.all("divPage2").style.display = "";	
			//隐藏团单信息
			document.all("GrpContTable").style.display = "none";
			document.all("divGrpContList").style.display = "none";
			document.all("divPage3").style.display = "none";
			afterQuery(arrResult);
		}
		else
		{
			mContFlag = "1";
			
			//查询团单客户信息
			strSql = 	"select * " +
						"from LDGrp " +
						"where 1 = 1 " +
						getWherePart('CustomerNo'); 
			arrResult = easyExecSql(strSql);
			
			if (arrResult)
			{
				//隐藏个体客户信息
				document.all("CustomerTable").style.display = "none";
				document.all("divCustomerInfo").style.display = "none";
				//显示团体客户信息
				document.all("GroupTable").style.display = "";
				document.all("divGroupInfo").style.display = "";
				//隐藏个单信息
				document.all("ContTable").style.display = "none";
				document.all("divContList").style.display = "none";
				document.all("divPage2").style.display = "";	
				//显示团单信息
				document.all("GrpContTable").style.display = "";
				document.all("divGrpContList").style.display = "";
				document.all("divPage3").style.display = "none";
				
				afterQuery(arrResult);
			}
		  else
		  {
		    alert("找不到该客户信息！");
		  }
		}
	}
}

//按保单号查询
function queryByContNo()
{
	
}

//按证件号查询
function queryByIDNo()
{
	if (isEnterDown())
	{
		var strSql = "select * from LDPerson where 1 = 1 " + 
				      getWherePart('IDNo');
		var arrResult = easyExecSql(strSql);
		if (arrResult)
		{
			mContFlag = 0;
			afterQuery(arrResult);
		}
	  else
	  {
	    alert("找不到该证件对应的客户信息！");
	  }
	}
}

/* 查询个单客户号 */
function queryCustomerNo()
{
	showInfo = window.open("LDPersonQueryMain.jsp?typeNo=" + fm.TopTypeNo.value);
	//显示个体客户信息
	document.all("CustomerTable").style.display = "";
	document.all("divCustomerInfo").style.display = "";
	//隐藏团体客户信息
	document.all("GroupTable").style.display = "none";
	document.all("divGroupInfo").style.display = "none";
	//显示个单信息
	document.all("ContTable").style.display = "";
	document.all("divContList").style.display = "";
	document.all("divPage2").style.display = "";	
	//隐藏团单信息
	document.all("GrpContTable").style.display = "none";
	document.all("divGrpContList").style.display = "none";
	document.all("divPage3").style.display = "none";
	
	mContFlag = "0"; //个单 
}

/* 查询团单客户号 */
function queryGrpCustomerNo()
{	
	showInfo = window.open("GroupMain.html");
	//隐藏个体客户信息
	document.all("CustomerTable").style.display = "none";
	document.all("divCustomerInfo").style.display = "none";
	//显示团体客户信息
	document.all("GroupTable").style.display = "";
	document.all("divGroupInfo").style.display = "";
	//隐藏个单信息
	document.all("ContTable").style.display = "none";
	document.all("divContList").style.display = "none";
	document.all("divPage2").style.display = "";	
	//显示团单信息
	document.all("GrpContTable").style.display = "";
	document.all("divGrpContList").style.display = "";
	document.all("divPage3").style.display = "none";
	
	mContFlag = "1";  //团单
}                                                              

/* 查询保单号 */
function queryContNo()
{
	//showInfo = window.open("");   
} 


/* 查询工单在列表中显示结果 */
function easyQueryClick1()
{
	var mCustomerNo = fm.all("CustomerNo").value;
	
	//书写SQL语句
	var strSQL;
	strSQL = "select w.WorkNo, w.AcceptNo, w.PriorityNo, w.TypeNo, w.CustomerNo, " +
			 "       (Select Name from LDPerson where CustomerNo = w.CustomerNo), '记录', t.SendPersonNo, t.InDate, w.AcceptCom, " +
			 "       Case When w.DateLimit Is Null then '' Else  trim(char(days(current date) - days(t.InDate))) || '/' || w.DateLimit End, " +
			 "       w.StatusNo, '', '' " +
			 "from   LGWork w, LGWorkTrace t " +
	         "where  w.WorkNo = t.WorkNo " +
	      	 "and    w.NodeNo = t.NodeNo " +
			 "and    w.CustomerNo = '" + mCustomerNo + "' " +
			 "and    w.TypeNo not in('5', '6') " + 
			 "order by t.workNo desc ";

	turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, CustomerGrid);

	return true;
}

/* 查询客户保单信息 */
function queryContInfo()
{
	var customerNo = fm.all("CustomerNo").value;

	//书写SQL语句
	var strSQL;  //分投保人，主被保人和其它被保人三种情况
	strSQL = "select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, a.cinvalidate, " +
	         "       a.PaytoDate, a.Prem, a.Amnt, codeName('stateflag', StateFlag), b.PostalAddress, case when a.contNo in (select contNo from LGPhoneAnwser) then '是' else '否' end " +
	         "from   LCCont a, LCAddress b " +
	         "where  a.AppntNo = b.CustomerNo " +
	         "and    (a.AppntNo = '" + customerNo + "' " +
	         " or    a.InsuredNo = '" + customerNo + "') " +
	         "and    a.AppFlag = '1' " +
	         "and    b.AddressNo = (select AddressNo from LCAppnt where ContNo = a.ContNo) " +
	         "and   (a.StateFlag is null or a.StateFlag not in('0')) "  //未终止
	         //保全回退需要查询退保保单

	         +" union " +  
					 " select distinct a.ContNo, a.PrtNo, a.AppntName, a.InsuredName, a.CValiDate, (select edorvalidate from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno),"+
					 " a.PaytoDate, a.Prem, a.Amnt,(select case edortype when 'WT' THEN '犹豫期退保' when 'CT' then '解约' end from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno), b.PostalAddress, "+
					 " case when a.contNo in (select contNo from LGPhoneAnwser) then '是' else '否' end "+
					 " from LBCont a, LCAddress b "+
					 " where  a.AppntNo = b.CustomerNo and a.appntno = '"+ customerNo +"' "+
					 " and b.AddressNo = (select AddressNo from LBAppnt where ContNo = a.ContNo) and exists ( "+
					 " select 1 from LPEdorItem where ContNo = a.contno and edortype in ('WT','CT','') and edorno = a.edorno)"
					;     
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, ContGrid);
	
	//for(var i = 0; i < ContGrid.mulLineCount; i++)
	//{
	//  var sql = "  select a.codeName "
	//            + "from LDCode a, LCContState b, LOPrtManager c "
	//            + "where a.code = c.code "
	//            + "   and a.codeType = 'pausecode'"
	//            + "   and b.otherNo = c.standbyFlag3 "
	//            + "   and (b.endDate is null or endDate > current date) "
	//            + "   and b.contNo = '" + ContGrid.getRowColDataByName(i, "contNo") + "' ";
	//  var rs = easyExecSql(sql);
	//  if(rs)
	//  {
	//    ContGrid.setRowColDataByName(i, "PolState", rs[0][0]);
	//  }
	//  else
	//  {
	//    ContGrid.setRowColDataByName(i, "PolState", "未失效");
	//  }
	//}
	
	return true;
}

//查看作业明细
function viewTask()
{
	var pageName = "TaskView.jsp";
	var WorkNo;
	var CustomerNo;
	var DetalWorkNo;
	
	var selNo = CustomerGrid.getSelNo();
	if (selNo)
	{ 
		WorkNo = CustomerGrid.getRowColData(selNo - 1, 1);
		CustomerNo = CustomerGrid.getRowColData(selNo - 1, 5);
		DetailWorkNo = WorkNo;
	}
	else 
	{
		alert("请先选择一条工单！"); 
	}
	
	if (WorkNo)
	{
	    var width = screen.availWidth - 10;
	    var height = screen.availHeight - 28;
		win = window.open("TaskViewMain.jsp?WorkNo="+WorkNo +"&CustomerNo=" + CustomerNo + "&DetailWorkNo=" + DetailWorkNo, 
						  "ViewWin",
						  "toolbar=no,menubar=no,status=no,resizable=yes,top=0,left=0,width="+width+",height="+height);
		win.focus();
	}
	
}

//查看个单信息
function viewCont()
{
	var selNo = ContGrid.getSelNo();
	if (selNo) 
	{
		var	ContNo = ContGrid.getRowColData(selNo - 1, 1);
		var PrtNo = ContGrid.getRowColData(selNo - 1, 2);
		if (ContNo)
		{
			win = window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&IsCancelPolFlag=0&LoadFlag=TASK&ContType=1", "ContWin");
			win.focus();
		}
	}
	else
	{
		alert("请先选择一条工单！"); 
	}
}

//查看团单信息
function viewGrpCont()
{
	var selNo = GrpContGrid.getSelNo();
	if (selNo) 
	{
		var	GrpContNo = GrpContGrid.getRowColData(selNo - 1, 1);
		if (GrpContNo)
		{
			//劉鑫
			var strSql = "SELECT prtno from lcgrppol l where grpcontno='"+GrpContNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) ";
	        var arrResult = easyExecSql(strSql);
	        if (arrResult != null) 
	        {
   			    win = window.open("../sys/GrpPolULIDetailQueryMain.jsp?LoadFlag=16&polNo="+GrpContNo+"&prtNo="+arrResult[0][0]+"&ContType=1", "", "status=no,resizable=yes,scrollbars=yes ");    
	        }else
	        {
				win = window.open("../sys/GrpPolDetailQueryMain.jsp?LoadFlag=TASK&ContNo="+GrpContNo + "&&LoadFlag=TASK&ContType=1", "ContWin");
			}
			win.focus();
		}
	}
	else
	{
		alert("请先选择一条工单！"); 
	}
}

//业务类型改变时显示相应的信息录入页面
function afterCodeSelect(cCodeName, Field)
{
	if (cCodeName == "TaskTopTypeNo")
	{
		var tType = fm.all("TopTypeNo").value;
		fm.all("TypeNo").value = "";
		fm.all("TypeNoName").value = "";
		fm.all("DateLimit").value = "";
		
		switch (tType)
		{
    		case '01':	//咨询
    			document.all("fraInterface").height = "200";
            	document.all("fraInterface").src = "./TaskInquire.jsp?" +
              	       "&Name=" + fm.all("Name").value +
            	       "&Sex=" + fm.all("Sex").value +
            	       "&EMail=" + fm.all("EMail").value +
            	       "&HomePhone=" + fm.all("HomePhone").value + 
            	       "&CustomerNo=" + fm.all("CustomerNo").value;
    			break;
    		case '02':	//投诉
    			document.all("fraInterface").height = "650";
           	    document.all("fraInterface").src = "Appeal.jsp";
           	    fm.all("PriorityNo").value = "1";
    			break;
    		case '03':	//保全
    			document.all("fraInterface").height = "0";
    			document.all("fraInterface").src = "";
    
    			break;
    		case '04':	//理赔
    			document.all("fraInterface").height = "0";
    			document.all("fraInterface").src = "";
    	//		document.all("fraInterface").height = "500";
        //    	document.all("fraInterface").src = "../case/ReportInput.jsp";
    			break;
    		case '05':	//电话回访
    			document.all("fraInterface").height = "700";
        	document.all("fraInterface").src = "PhoneInterview.jsp";
    			break;
    		default:
		}
		
		subTypeNoCondition = fm.TopTypeNo.value + "," + fm.CustomerNo.value; //查询子业务类型的条件,Init.jsp中定义
	}

	if (cCodeName == "TaskTopTypeNo")
	{
		mType = fm.all("TopTypeNo").value;
	}
	if (cCodeName == "TaskTypeNo")
	{
		//得到时限
		var tSubType = fm.all("TypeNo").value;
	    strSql = "select DateLimit from LGWorkType " +
	    	     "where WorkTypeNo = '" + tSubType + "'";
		var arrResult = easyExecSql(strSql);
		fm.all("DateLimit").value = arrResult[0][0];
	}
	if(cCodeName == "ApplyTypeNo")
	{
		if(fm.ApplyTypeNo.value == "0")
		{
			fm.ApplyName.value = fm.ApplyNameHidden.value;
		}
		else
		{
			fm.ApplyName.value = "";
		}
	}
}

/* 数据提交前校验 */
function beforeSubmit()
{
	/*
	if (!verifyInput2()) 
	{
		return false;
	}
	*/
	if ((fm.all("TopTypeNo").value == "03" || fm.TopTypeNo.value == "05") && (fm.all("CustomerNo").value == ""))
	{
		alert("客户号不能为空！");
		fm.all("CustomerNo").focus();
		return false;
	}

	//if(!haveCont())
	//{
	//	alert("该客户没有有效投保单，不能进行保全操作。");
	//	renewInput();
	//	return false;
	//}
	if(((fm.CustomerNo.value).length != 8) && ((fm.CustomerNo.value).length != 9))
	{
		alert("客户号必须为8位或9位");
		return false;
	}
	
	return true;
}

/* 提交表单 */
function submitForm()
{
	document.all.isProxy.disabled = true;
	if (beforeSubmit())
	{		

		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		if(doBusinessFlag == 1)
		{
			//fm.action = "./TaskInputSave.jsp?statusNo=3&contNoForPHone=" + contNoForPHone;
			fm.action = "./TaskInputSave.jsp?statusNo=3";
			doBusinessFlag = 0;
		}
		else
		{
			//fm.action = "./TaskInputSave.jsp?contNoForPHone=" + contNoForPHone;	 
			fm.action = "./TaskInputSave.jsp";	 
		}
		fm.submit(); //提交
	}

}

/* 工单信息和业务信息一起提交 */
function submitAll()
{
	//校验工单信息
	if (!beforeSubmit())
	{
		return false;
	}
	//校验具体业务信息
	if (fm.all("TopTypeNo").value != "03")
	{
		if (!fraInterface.invokeCheck())
		{
			return false;
		}
	}
	if(fm.all("TopTypeNo").value == "05")
	{
	  /*
		rowNo = ContGrid.getSelNo() - 1;
		
		if(rowNo >= 0)
		{
			contNoForPHone = ContGrid.getRowColData(rowNo, 1); 
		}
		else
		{
			alert("请选择将要回访的保单");
			return false;				
		}
		*/
		//alert(top.fraInterface.name);
		if(document.fraInterface.fm.contNo.value == "")
		{
		  alert("请选择一张保单。");
		  return false;
		}
	
	}
	submitForm();
}   
    
/* 保存完成后的操作，由子窗口调用 */
function afterSubmit(FlagStr, content, workNo, detailWorkNo, tTypeNo)
{
	workNoReturn = workNo;
	
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	if (mOption == "DOBUSINESS")  //经办
	{
		//parent.fraInterface.fraInterface.invokeSubmit();
		loadWorkPage(detailWorkNo);   //装载业务处理页面
	}
	if (mOption == "SAVE")    //保存
	{
		if (fm.all("TopTypeNo").value != "03")
		{
			parent.fraInterface.fraInterface.invokeSubmit(workNo, tTypeNo);
		}
		returnTask();
		top.opener.location.reload();
	}
	if (mOption == "DELIVER")
	{
		if (fm.all("TopTypeNo").value != "03")
		{
			parent.fraInterface.fraInterface.invokeSubmit();
		}
		returnTask();
		top.opener.location.reload();
		win = window.open("TaskDeliverMain.jsp?WorkNo="+workNo);
		win.focus();
	}
	if (mOption == "TOWAIT")
	{
		if (fm.all("TopTypeNo").value != "03")
		{
			parent.fraInterface.fraInterface.invokeSubmit();
		}
		returnTask();
		top.opener.location.reload();
		win = window.open("TaskWait.jsp?WorkNo="+workNo);
		win.focus();
	}
}   

//显示该客户团单信息
function qureyGrpContInfo(cn)
{
	var sq = "select a.GrpContNo, a.PrtNo, a.GrpName, a.Peoples2, " +
					 "  a.PolApplyDate, a.CValiDate, a.PayMode, a.Prem, a.Amnt, " +
					 "  codeName('stateflag', a.StateFlag),  b.GrpAddress, " +
					 "  case when exists (select 1 from LCCont where grpContNo = a.grpContNo and polType = '1') then '无名单' else '非无名单' end " +
					 "from LCGrpCont a, LCGrpAddress b " +
					 "where a.AppntNo = b.CustomerNo " +
					 "and   a.AddressNo = b.AddressNo " +
					 "and   a.AppntNo ='" + cn + "' " +
					 "and   (a.StateFlag is null or a.StateFlag not in('0')) "+  //未终止
					 "and   a.AppFlag = '1'" +
	         "    and (state is null or state not in('03030002')) " + //非续保终止
	         "    and not exists " + 
	         "      (select 1 from LPEdorEspecialData a, LGWork b " +
	         "      where a.EdorNo = b.WorkNo and b.ContNo = a.GrpContNo " + 
	         "        and a.EdorType = 'MJ' " +
	         "        and a.DetailType = 'MJSTATE' " +
	         "        and a.EdorValue != '1' and a.EdorValue != '0')";  //已满期理算
	
	turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(sq, GrpContGrid);
	
	//查询保单状态
	//for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	//{
	//  var sql = "  select a.codeName "
	//            + "from LDCode a, LCGrpContState b, LOPrtManager c "
	//            + "where a.code = c.code "
	//            + "   and a.codeType = 'pausecode'"
	//            + "   and b.otherNo = c.standbyFlag3 "
	//            + "   and (b.endDate is null or endDate > current date) "
	//            + "   and b.grpContNo = '" + GrpContGrid.getRowColDataByName(i, "GrpContNo") + "' ";
	//  var rs = easyExecSql(sql);
	//  if(rs)
	//  {
	//    GrpContGrid.setRowColDataByName(i, "PolStateGrp", rs[0][0]);
	//  }
	//  else
	//  {
	//    GrpContGrid.setRowColDataByName(i, "PolStateGrp", "未失效");
	//  }
	//}
}

/* 设置客户号,由客户查询页面返回时调用 */
function afterQuery(arrQueryResult)
{
	window.focus();
	
	//选择团单客户时的操作
	if(mContFlag == "1")
	{
		customerNo = arrQueryResult[0][0];
		fm.all('CustomerNo').value = arrQueryResult[0][0];
		easyQueryClick1();
		qureyGrpContInfo(customerNo);
				
		var sql = "select a.customerNo, a.GrpName, a.BusinessType, a.GrpNature, a.Peoples, a.RgtMoney, a.Asset, " +
									"a.MainBussiness, a.Corporation, b.Fax1, a.Phone, a.Satrap, b.E_Mail1, a.FoundDate, " +
									"a.BankAccNo, a.State, b.linkMan1, addressNo " +
							"from LDGrp a, LCGrpAddress b " +
							"where a.customerNo='" + customerNo + "' and " +
								"b.customerNo='" + customerNo + "' " +
								"order by addressNo desc";		
		var result = easyExecSql(sql);
		
		if(result)
		{
			fm.all('ApplyNameHidden').value= result[0][16];
			fm.GGrpName.value = result[0][1];
			fm.GBusinessType.value = result[0][2];
			fm.GGrpNature.value = result[0][3];
			fm.GPeoples.value = result[0][4];
			fm.GRgtMoney.value = result[0][5];
			fm.GAsset.value = result[0][6];
			fm.GMainBussiness.value = result[0][7];
			fm.GCorporation.value = result[0][8];
			fm.GFax.value = result[0][9];
			fm.GPhone.value = result[0][10];
			fm.GSatrap.value = result[0][11];
			fm.GEMail.value = result[0][12];
			fm.GFoundDate.value = result[0][13];
			fm.GBankAccNo.value = result[0][14];
			fm.GState.value = result[0][15];
		}
		
		transferGrpCodeToName();
		showCodeName();
		subTypeNoCondition = fm.TopTypeNo.value + "," + fm.CustomerNo.value; //查询子业务类型的条件,Init.jsp中定义
		
		return;
	}
	else
	{
		//选择个单客户时的操作
		try
		{	      
			fm.all('CustomerNo').value = arrQueryResult[0][0];
			easyQueryClick1();
			if (mContFlag == "0")
			{
				//查询保单信息
			  	queryContInfo();
			  	
			  	//查询客户信息
			  	queryCustomerInfo(arrQueryResult[0][0]);
			  	
			  	//查询客户地址信息
				var strSQL =
						"select * from LCAddress "
						+ "where customerNo='" + fm.CustomerNo.value + "' "
						+ "and int(addressNo) = "
						+ "		(select max(int(addressNo)) "
						+ "		from LCAddress "
						+ "		where customerNo='" + fm.CustomerNo.value + "')";
				
				var arrResult = new Array();
			    arrResult = easyExecSql(strSQL);
			    fm.all('HomeAddress').value= arrResult[0][6];
			    fm.all('ZipCode').value= arrResult[0][3];
			    fm.all('PostalAddress').value= arrResult[0][7];
			    fm.all('HomePhone').value= arrResult[0][8];
			    fm.all('CompanyAddress').value= arrResult[0][10];
			    fm.all('CompanyPhone').value= arrResult[0][12];
			    fm.all('EMail').value= arrResult[0][16];
			    fm.all('Mobile').value= arrResult[0][14];
			}
		    
		    showCodeName(); //把编码转为汉字显示
    	}
		catch(ex)
		{
		}
		subTypeNoCondition = fm.TopTypeNo.value + "," + fm.CustomerNo.value; //查询子业务类型的条件,Init.jsp中定义

	}	
	
	mContFlag = -1;	
}  

function transferGrpCodeToName()
{
	var sql = "select codeName "
    	      + "from LDCode "
    	      + "where codeType = 'businesstype' "
    	      + "    and code = '" + fm.GMainBussiness.value + "' ";
	var result = easyExecSql(sql);
	if(result)
	{
	    fm.GMainBussiness.value = result[0][0];
	}
	
	sql = "select codeName "
	      + "from LDCode "
	      + "where codeType = 'grpnature' "
	      + "    and code = '" + fm.GGrpNature.value + "' ";
	result = easyExecSql(sql);
	if(result)
	{
	    fm.GGrpNature.value = result[0][0];
	}
}                                                                        

//查询个人客户信息
function queryCustomerInfo(cstmNo)
{
	var sql = " select customerNo, name, sex, marriage, birthday,workType,occupationCode, "
			  + "   (select codeName "
			  + "   from LDCode "
			  + "   where codeType='idtype' "
			  + "       and code=LDPerson.idtype), "
			  + "   IDNo, rgtAddress, grpNo "
			  + "from LDPerson "
			  + "where customerNo='" + cstmNo + "' ";
	var result = easyExecSql(sql);
	
	
	if(result)
	{
		fm.all('ApplyNameHidden').value= result[0][1];
		fm.all('Name').value= result[0][1];                            
		fm.all('Sex').value= (result[0][2] == "0" ? '男' : '女');
		fm.all('Marriage').value= result[0][3];                         
		fm.all('Birthday').value= result[0][4];
		fm.all('Occupation').value= result[0][6];
		fm.all('OccupationCode').value= result[0][6];
		fm.all('IDType').value= result[0][7];     
		fm.all("IDNo").value = result[0][8];                     
		fm.all('IDNo2').value= result[0][8];      
		fm.all('RgtAddress').value= result[0][9];                                      
		fm.all('GrpNo').value= result[0][10]; 
	}
	
	sql = "  select OccupationCode, trim(OccupationName)||'-'||workname, OccupationType "
	      + "from LDOccupation "
	      + "where OccupationCode = '" + fm.OccupationCode.value + "' "
	result = easyExecSql(sql);
	if(result)
	{
	    fm.Occupation.value = result[0][1];
	}
}   
    
    
//装载业务处理页面
function loadWorkPage(DetailWorkNo)
{   
	var tType = fm.all("TopTypeNo").value;
	var tSubType = fm.all("TypeNo").value;
	var tCustomer = fm.all("CustomerNo").value //用客户号判断团单个单
	switch (tType)
	{
	case '01':	//咨询
		document.all("fraInterface").height = "500";
    	document.all("fraInterface").src = "../case/LLConsultInput.jsp";
		break;
	case '02':	//投诉
		document.all("fraInterface").height = "500";
    	document.all("fraInterface").src = "./Appeal.jsp";
		break;
	case '03':	//保全
		if (tCustomer.length == 8) //团单
		{
			document.all("fraInterface").height = "600";
	    	document.all("fraInterface").src = "../bq/GEdorInput.jsp?LoadFlag=TASKFRAME" +
	    		   "&DetailWorkNo="+ DetailWorkNo +
	    	       "&CustomerNo="  + fm.all("CustomerNo").value +
	    	       "&TypeNo="      + fm.all("TypeNo").value +
	    	       "&ApplyTypeNo=" + fm.all("ApplyTypeNo").value +
	    		   "&ApplyName="   + fm.all("ApplyName").value + 
	    		   "&AcceptWayNo=" + fm.all("AcceptWayNo").value;
	    }
	    else if (tCustomer.length == 9) //个单
	    {
	    	document.all("fraInterface").height = "600";
	    	document.all("fraInterface").src = "../bq/PEdorAppInput.jsp?LoadFlag=TASKFRAME" +
	    		   "&DetailWorkNo="+ DetailWorkNo +
	    	       "&CustomerNo="  + fm.all("CustomerNo").value +
	    	       "&TypeNo="      + fm.all("TypeNo").value +
	    	       "&ApplyTypeNo=" + fm.all("ApplyTypeNo").value +
	    		   "&ApplyName="   + fm.all("ApplyName").value + 
	    		   "&AcceptWayNo=" + fm.all("AcceptWayNo").value;
	    }
		break;
	case '04':	//理赔
		document.all("fraInterface").height = "500";
    	document.all("fraInterface").src = "../case/ReportInput.jsp";
		break;
	case '05':	//电话回访
		document.all("fraInterface").height = "500";
    	document.all("fraInterface").src = "PhoneInterview.jsp";
		break;
	default:
	}
}

//经办
function doBusiness()
{
 if( verifyInput2() == false ) 
  {
    top.fraTopButton.document.all.save.disabled = false;
    top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
    top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
    return false;
  }
  if(!checkInput())
  {
    return false;
  }
  if(!hasValidateCont(fm.CustomerNo.value))
  {
    return false;
  }
  
  if(!riskLevelAlert(fm.CustomerNo.value)){
	  return false;
  }
  
  
  //add by luomin  -提示保单失效或者满期终止

  var selNo = ContGrid.getSelNo();
  //alert("selNo===="+selNo);
  if(selNo>=1){
  var	ContNo = ContGrid.getRowColData(selNo - 1, 1);
  //alert("ContNo===="+ContNo);
  if(ContNo!=null&&ContNo!="")
  {
  var sql = "select StateType,State from LCContState where contno='"+ContNo+"' and PolNo='000000'";
	var result = easyExecSql(sql);
	if(result){
	//alert("result[0][0]===="+result[0][0]);
	  if(result[0][0]=="Available"&&result[0][1]=="1")
		alert("保单"+ContNo+"处于失效状态!");
		if(result[0][0]=="Terminate"&&result[0][1]=="1")
		alert("保单"+ContNo+"处于失满期终止状态!");
	}
	}
	}


	var selNo = GrpContGrid.getSelNo();
	//alert("selNo===="+selNo);
	if(selNo>=1){
	var grpcontno=GrpContGrid.getRowColData(selNo - 1, 1);
	//alert("grpcontno===="+grpcontno);
	if(grpcontno!=null&&grpcontno!="")
  {
	var sql = "select StateType,State from LCGrpContState where GrpContNo='"+grpcontno+"' and GrpPolNo='000000'";
	var result = easyExecSql(sql);
	if(result){
	//alert("result[0][0]===="+result[0][0]);
	if(result[0][0]=="Terminate"&&result[0][1]=="1")
		alert("保单"+grpcontno+"处于满期终止状态!");
		if(result[0][0]=="Pause"&&result[0][1]=="2")
		alert("保单"+grpcontno+"处于暂停状态!");
	}
	}
  }
  
	//隐藏个人信息
	document.all("divCustomerInfo").style.display = "none";
	
	//隐藏团体信息
	document.all("divGroupInfo").style.display = "none";
	
	//隐藏客户服务信息
	document.all("divTaskList").style.display = "none";
	
	//隐藏客户保单信息
	document.all("divContList").style.display = "none";
	
	doBusinessFlag = 1;
	oprtAfterDobusi = 1;
	
	submitForm();
	mOption = "DOBUSINESS";
}

function checkInput()
{
  if(fm.TopTypeNo.value == "05")
  {
    alert("请使用保存按钮。");
    return false;
  }
  return true;
}

//保存
function saveTask()
{
  if( verifyInput2() == false ) 
  {
    top.fraTopButton.document.all.save.disabled = false;
    top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
    top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
    return false;
  }
  if(!hasValidateCont(fm.CustomerNo.value))
  {
    return false;
  }
	submitAll();
	mOption = "SAVE";
}

//校验被保人是否有有效保单
function hasValidateCont(appntNo)
{
  if(appntNo.length != "8" && appntNo.length != "9")
  {
    alert("客户号必须为8位或9位。");
    try
    {
      top.fraTopButton.document.all.save.disabled = false;
      top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
      top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
    }
    catch(ex)
    {
    }
    return false;
  }
  
  if(document.all("isProxy").checked){
	  	var errFlag=false;
	  	var errlog="";
		var IDStartDate=fm.all("ProxyIDStartDate").value;
		var IDEndDate=fm.all("ProxyIDEndDate").value;
		var tDayDiff1=dateDiff(IDStartDate,IDEndDate,"D");
		var myDate = new Date();//系统当前时间
		var myDate=myDate.getFullYear()+"-"+(myDate.getMonth()+1)+"-"+myDate.getDate();
		var tDayDiff2=dateDiff(myDate,IDEndDate,"D");
		if(tDayDiff1<0){
			alert("代理人证件失效日期不能早于生效日期！");
			try
			  {
			    top.fraTopButton.document.all.save.disabled = false;
			    top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
			    top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
			  }
			  catch(ex)
			  {
			  }
			  return false;
		}
		if(tDayDiff2<0){
			alert("代理人证件失效日期不能早于当前日期！");
			try
			  {
			    top.fraTopButton.document.all.save.disabled = false;
			    top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
			    top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
			  }
			  catch(ex)
			  {
			  }
			  return false;
		}
		var iIdType=fm.all("ProxyIDType").value;
		var iIdNo=fm.all("ProxyIDNo").value;
		if (iIdType=="0" && (iIdNo.length!=15) && (iIdNo.length!=18))
		{
			alert("输入的身份证号位数错误");
			try
			  {
			    top.fraTopButton.document.all.save.disabled = false;
			    top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
			    top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
			  }
			  catch(ex)
			  {
			  }
			  return false;
		}
		
		var iProxyName=fm.all("ProxyName").value;
		if(iIdType=="0" || iIdType=="5"){
			var strCheckIdNo=checkIdNo(iIdType,iIdNo,"","");
			if (""!=strCheckIdNo && strCheckIdNo!=null)
			{
				alert(strCheckIdNo);
				return false;
			}
		}
	}
  
  var sql;
  if(appntNo.length == "8")
  {
    sql = "  select grpContNo "
          + "from LCGrpCont "
          + "where appntNo = '" + appntNo + "' "
          + "   and appFlag = '1' "
          + "   and (StateFlag is null or StateFlag not in('0')) " 
          //qulq 2008-1-7 10:37 保全回退添加
    			+ " union select grpcontno from LBGrpCont b where appntNo = '" + appntNo + "' "
    			+ " and exists (select 1 from lpgrpedoritem where grpcontno =b.grpcontno and edorno = b.edorno and edortype in ('WT','CT',''))"
    			;
  }
  else
  {
    sql = "  select contNo "
          + "from LCCont "
          + "where appntNo = '" + appntNo + "' "
          + "   and appFlag = '1' "
          + "   and (StateFlag is null or StateFlag not in('0')) "
          //qulq 2008-1-7 10:37 保全回退添加
    			+ " union select contno from lbcont b where appntNo = '" + appntNo + "' " 
    			+ " and exists (select 1 from lpedoritem where contno = b.contno and edorno = b.edorno and edortype in ('WT','CT','XT'))"
    			;
  }
  var result = easyExecSql(sql);
  if(result)
  {
    return true;
  }
  
  alert("客户没有有效保单或可回退保单。");
  try
  {
    top.fraTopButton.document.all.save.disabled = false;
    top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
    top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
  }
  catch(ex)
  {
  }
  
  return false;
}

//编辑工单后保存
function saveTaskAfterEdit()
{
  if(!hasValidateCont(fm.CustomerNo.value))
  {
    return false;
  }
  
  fm.fmtransact.value = "UPDATE||MAIN";
  
  fm.action = "UpdateTaskSave.jsp?WorkNoEdit=" + fm.WorkNoEdit.value;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
		"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

//编辑工单后的反馈信息展现
function afterEditSubmit(flag, content)
{
  showInfo.close();
	window.focus();
	if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		if(operateWhileEdit == "dobusiness")
		{
		  window.location.replace("./TaskPersonalBoxSave.jsp");
	  }
	  else if(operateWhileEdit == "save")
	  {
	    returnTask();
	    top.opener.location.reload();
	  }
	  else if(operateWhileEdit == "wait")
	  {
	    parent.fraInterface.location.replace("./TaskWait.jsp?loadFlag=TaskViewTopButton");
	  }
	}
}

//转交
function deliverTask()
{
	if(oprtAfterDobusi == 0)
	{
		submitAll();
		mOption = "DELIVER";
	}
	else
	{
		top.close();
		win = window.open("TaskDeliverMain.jsp?WorkNo=" + workNoReturn);
		win.focus();
	}
}

//转为待办
function toWait()
{
	if(oprtAfterDobusi == 0)
	{
		submitAll();
		mOption = "TOWAIT";
	}
	else
	{
		win = window.open("TaskWaitMainFrame.jsp?loadFlag=OPRTAFTERDOBUSI&WorkNo=" + workNoReturn);
		win.focus();
	}
}

/* 简易保全 */
function easyEdor()
{
	fm.action = "TaskEasyEdorMain.jsp";
	fm.target = "_blank";
	fm.submit();
}

//返回
function returnTask()
{
	top.close();
	top.opener.focus();
}

//检测该用户是否有有效保单
//返回值：true：有，false：无
//function haveCont()
//{
//	if(fm.TopTypeNo.value == "03")
//	{
//		var sql = 	"select * "
//					+ "from LCCont c "
//					+ "where c.appntNo='" + fm.all("CustomerNo").value + "' " 
//					+ "		and c.appflag='1'";
//		var result = easyExecSql(sql);
//		
//		if(!result)
//		{
//			return false;
//		}
//	}
//	return true;
//}

//恢复工单录入初始化界面，在客户无有效保单时调用
function renewInput()
{
	//显示个人信息
	document.all("divCustomerInfo").style.display = "";

	//显示团体信息
	document.all("divGroupInfo").style.display = "none";
	
	//显示客户服务信息
	document.all("divTaskList").style.display = "";
	
	//显示客户保单信息
	document.all("divContList").style.display = "";
}

//返回个人信箱
function returnPersonalBox()
{
	top.close();
	top.opener.focus();
	top.opener.location.reload();
}

function setContInfoForInterview()
{
  if(fm.TopTypeNo.value == "05")
  {
    document.fraInterface.setContInfoForInterview(ContGrid.getRowColDataByName(ContGrid.getSelNo() - 1, "contNo"));
  }
}

//个单退保试算
function pEdorCalZTTest()
{
  var row = ContGrid.getSelNo() - 1;
  if(row == -1)
  {
    alert("请选择保单");
    return false;
  }
  
  win = window.open("../bq/PEdorBudgetMain.jsp?contNo=" + ContGrid.getRowColData(row, 1), 
                    "pEdorCalZTTest");
  win.focus();
}

//团单退保试算
function gEdorCalZTTest()
{
  var row = GrpContGrid.getSelNo() - 1;
  if(row == -1)
  {
    alert("请选择保单");
    return false;
  }
  
  win = window.open("../bq/GEdorBudgetMain.jsp?grpContNo=" + GrpContGrid.getRowColData(row, 1), 
                    "gEdorCalZTTest");
  win.focus();
}


function queryAngent()
{
	if(fm.CustomerNo.value == ""){
	    alert("你还没有选择客户!!");
	    return false;
	}
   showInfo = window.open("LPAgentQuery.jsp?CustomerNo=" + fm.CustomerNo.value);
}

function afterAgentQuery(arrQueryResult)
{
    window.focus();
    try{
        fm.ApplyName.value = arrQueryResult[0]+" "+arrQueryResult[1];	
        showCodeName();
    }catch(ex){}
}
/**
 * 客户风险等级查询
 */
function riskLevelSelect(customerNo){
	var level = null;
	if(customerNo.length == 9){
		var riskLevelSql = "select max(HighestEvaLevel) "
			+" from fx_FXQVIEW "
			+"where AOGTYPE = '1' "
			+"and AOGNAME = "
			+"(select name from ldperson where customerNo = '"+customerNo+"' fetch first 1 rows only) "
			+"and APPNTIDTYPE = "
			+"(select idtype from ldperson where customerNo = '"+customerNo+"' fetch first 1 rows only) "
			+"and APPNTIDNO = "
			+"(select idno from ldperson where customerNo = '"+customerNo+"' fetch first 1 rows only) "
			+"and APPNTBIRTHDAY = "
			+"(select BIRTHDAY from ldperson where customerNo = '"+customerNo+"' fetch first 1 rows only) "
			+"and APPNTSEX = "
			+"(select SEX from ldperson where customerNo = '"+customerNo+"' fetch first 1 rows only)";
		level = easyExecSql(riskLevelSql);
	}
	if(customerNo.length == 8){
		var riskLevelSql = " select max(HighestEvaLevel) "
						+ " from fx_FXQVIEW"
						+ "  where AOGNAME = (select grpname from ldgrp where customerno = '"+customerNo+"' fetch first 1 rows only)"
						+ "  and AOGTYPE = '2'";
		level = easyExecSql(riskLevelSql);
	}
	return level;
}
/**
 * 客户风险等级提示
 * @param customerNo
 * @return
 */
function riskLevelAlert(customerNo){
	var riskLevel = riskLevelSelect(customerNo);
	  if(riskLevel == "2"){
		  if(confirm("客户"+customerNo+"洗钱风险等级为中等,是否继续操作申请工单?")){
			  return true;
		  }else{
			  try
			  {
				  top.fraTopButton.document.all.save.disabled = false;
				  top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
				  top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
			  }
			  catch(ex)
			  {
			  }	
			  return false;
		  }
	  }
	  if(riskLevel == "3"){
		  if(confirm("客户"+customerNo+"洗钱风险等级为高等,是否继续操作申请工单?")){
			  return true;
		  }else{
			  try
			  {
				  top.fraTopButton.document.all.save.disabled = false;
				  top.fraTopButton.document.all.doBuss.disabled = false;  //新建中的经办按钮doBuss
				  top.fraTopButton.document.all.dobuss.disabled = false;  //经办中的经办按钮dobuss
			  }
			  catch(ex)
			  {
			  }	
			  return false;
		  }
	  }
	  return true;
}