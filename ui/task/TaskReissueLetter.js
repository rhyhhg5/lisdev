

//程序名称：TaskPostPurview.js
//程序功能：
//创建日期：2005-07-06
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;

var wherePart = "";
var sqlStr;
var stringForOrder = "";

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function submitForm()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr, content)
{
	showInfo.close();
	window.onfocus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
  	if (fm.fmtransact.value == "DELETE||MAIN")
  	{
  	   content = "删除成功";
  	}
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		top.focus();
		window.location.reload();
	}	
}


//函件查询
function queryLetter()
{
	var sql='';

	if (!verifyInput2())
    return false;  

	if(fm.ReissueType.value=='01')
	{
		sql = "select '"+fm.ReissueType.value
					+"',(select codename from ldcode where codetype='reissuetype' and code='"+fm.ReissueType.value
					+"'), b.edorno edoracceptno,b.makedate,b.modifydate,b.operator "
					+"from lgwork a,LPEdorPrint b,LPEdorApp c "
					+"where a.workno=b.edorno and b.edorno=c.edoracceptno "
					+"and a.customerno='"+fm.CustomerNo.value+"' and a.statusno='5' "
					+"and (select Comcode from lduser where usercode=a.operator) like '"+comCode+"%%' ";
					//+"Order by b.edorno";
		sql = sql+ " union ";
		sql = sql
					+"select '"+fm.ReissueType.value
					+"',(select codename from ldcode where codetype='reissuetype' and code='"+fm.ReissueType.value
					+"'), b.edoracceptno edoracceptno,b.makedate,b.modifydate,b.operator "
					+"from lgwork a,LPEdorAppPrint b,LPEdorApp c "
					+"where a.workno=b.edoracceptno and b.edoracceptno=c.edoracceptno "
					+"and a.customerno='"+fm.CustomerNo.value+"' and a.statusno='5' "
					+"and (select Comcode from lduser where usercode=a.operator) like '"+comCode+"%%' "
					+"Order by edoracceptno";
		initLetterGrid01();
	}
	if(fm.ReissueType.value=='02')
	{
		sql = "select '"+fm.ReissueType.value
					+"',(select codename from ldcode where codetype='reissuetype' and code='"+fm.ReissueType.value
					+"'), b.edoracceptno,b.serialNumber,b.makedate,b.modifydate,b.operator, "
					+"b.letterType,b.addressNo,b.backFlag,b.backDate "
					+"from lgwork a,lgletter b "
					+"where a.workno=b.edoracceptno "
					+"and a.customerno='"+fm.CustomerNo.value+"' and b.state in ('3','4','5','6') "
					+"and (select Comcode from lduser where usercode=a.operator) like '"+comCode+"%%' "
					+"Order by b.edoracceptno ,SerialNumber";
		initLetterGrid02();
	}
	
	turnPage.pageDivName = "divPage";
	turnPage.pageLineNum = 10;
	turnPage.queryModal(sql, LetterGrid);
	showCodeName();
	
	document.all("pictureId").src = "";	
	return true;
}

//明细查询
function queryDetailClick(blnPreview)
{
	var reissueType;
	var row = LetterGrid.getSelNo() - 1;

	if(row <0 )
	{			
		alert("请选择一条记录");
		return false;
	}
	
	reissueType = LetterGrid.getRowColData(row, 1);

	if(reissueType=='01')
	{
		reissueType01(blnPreview);
		return true;
	}
	
	if(reissueType=='02')
	{
		reissueType02(blnPreview);
		return true;
	}
		
}

//补发类型为01保单服务批单
function reissueType01(blnPreview)
{	
	var row = LetterGrid.getSelNo() - 1;	
 	var letterNo = LetterGrid.getRowColData(row, 3);

	var sql = "select OtherNoType,edorState from LPEdorApp " +
	          "where EdorAcceptNo = '" + letterNo + "' ";
	
	var arrResult = easyExecSql(sql, 1, 0);
	if (!arrResult)
	{
		alert("没有找到保全信息！");
		return false;
	}

	if(arrResult[0][1]!='0')
	{
		alert("批单状态不正确！");
		return false;
	}
	
	var appletWidth = screen.availWidth - 50;
	var appletHeight = screen.availHeight;
	
	document.all("pictureId").width = appletWidth;
	document.all("pictureId").height = appletHeight;

	if(arrResult[0][0]=='1')
	{
		if(blnPreview==true)
		{
			document.all("pictureId").src = "../bq/ShowEdorInfo.jsp?EdorNo="+letterNo+"";
		}else
		{
			window.open("../f1print/AppEndorsementF1PJ1.jsp?EdorAcceptNo="+letterNo+"");
		}
	}
	
	if(arrResult[0][0]=='2')
	{
		if(blnPreview==true)
		{
			document.all("pictureId").src = "../bq/ShowGrpEdorInfo.jsp?EdorNo=" + letterNo;
		}else
		{
			window.open("../f1print/GrpEndorsementF1PJ1.jsp?EdorNo=" + letterNo);
		}		
	}
}


function reissueType02(blnPreview)
{
	var row = LetterGrid.getSelNo() - 1;	
 	var letterNo = LetterGrid.getRowColData(row, 3);
	var	serialNumber=LetterGrid.getRowColData(row, 4);
	var letterType=LetterGrid.getRowColData(row, 8);
	var addressNo=LetterGrid.getRowColData(row, 9);
	var backFlag=LetterGrid.getRowColData(row, 10);
	var backDate=LetterGrid.getRowColData(row, 11);
	
	
	var appletWidth = screen.availWidth - 50;
	var appletHeight = screen.availHeight;
	
	document.all("pictureId").width = appletWidth;
	document.all("pictureId").height = appletHeight;
	
	if(blnPreview==true)
	{
		document.all("pictureId").src="TaskPreviewLetter.jsp?loadFlag=SHOWLETTER"
			+"&edorAcceptNo="+letterNo
			+"&serialNumber=" + serialNumber
			+"&letterType="+letterType
			+"&addressNo="+addressNo
			+"&backFlag="+backFlag
			+"&backDate="+backDate;
	}else
	{
		window.open("TaskPreviewLetter.jsp?loadFlag=SHOWLETTER&showToolBar=true"
			+ "&edorAcceptNo=" + letterNo 
			+ "&serialNumber=" + serialNumber
			+ "&letterType=" + letterType
			+ "&addressNo=" + addressNo
			+ "&backFlag=" + backFlag
			+ "&backDate=" + backDate);
	}	
}

function returnParent()
{
	document.all("pictureId").src="";
	top.close();
	top.opener.focus();
	//top.opener.location.reload();
}