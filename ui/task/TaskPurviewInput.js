//程序名称：
//程序功能：
//创建日期：2005-07-05
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//选择确认权时引发的操作
function showConFirmFlag()
{
	if(fm.ConfirmFlag.value == 1)
	{
		document.all("Post").style.display = "";
		document.all("CheckFlag").style.display = "";
	}
	else
	{
		document.all("Post").style.display = "none";
		document.all("CheckFlag").style.display = "none";
		document.all("MemberPurviewId").style.display = "none";
	}
}

//选择个人确认权时触发的事件
function afterCodeSelect(cCodeName, Field)
{
	//if (cCodeName == "TaskMemberNo" && fm.ConfirmFlag.value != "2")
	//{
	//	document.all("Post").style.display = "none";
	//	document.all("CheckFlag").style.display = "none";
	//	document.all("MemberPurviewId").style.display = "";
	//}
	if((fm.ProjectType.value=='0')&&(fm.ProjectNo.value=='CT'||fm.ProjectNo.value=='WT')){
		document.all("RiskCodeID").style.display = "";
	}else{
		document.all("RiskCodeID").style.display = "none";
		fm.RiskCode.value=='';
	}
	
//	if(cCodeName=='riskprop2'&&fm.RiskCode.value!=''){
//		fm.ExcessPurviewFull.disabled=true;
//		fm.TimePurviewFull.disabled=true;
//		fm.all("ConfirmFlag")[0].disabled=true;
//		fm.all("ConfirmFlag")[1].disabled=true; 
//		fm.ExcessPurview.disabled=true;
//		fm.TimePurview.disabled=true;
//	}else if(cCodeName=='riskprop2'&&fm.RiskCode.value==''){
//		fm.ExcessPurviewFull.disabled=false;
//		fm.TimePurviewFull.disabled=false;
//		fm.all("ConfirmFlag")[0].disabled=false;
//		fm.all("ConfirmFlag")[1].disabled=false; 
//		fm.ExcessPurview.disabled=false;
//		fm.TimePurview.disabled=false;
//	}
	
}

function save()
{
	fm.fmtransact.value == "INSERT||MAIN";
	if(!checkData())
	{
		return false
	}
	
	submitForm();
}

function update(flag)
{
	if(flag == "UPDATE")
	{
		fm.fmtransact.value = "UPDATE||MAIN";
		
		if(!checkInputData())
		{
		    return false;
		}
		
		if(fm.ConfirmFlagOld.value == "2" && fm.all("ConfirmFlag")[2].checked == true
			|| fm.ConfirmFlagOld.value != "2" && fm.all("ConfirmFlag")[2].checked == false)
		{
		}
		else
		{
			alert("您录入的权限类别与原类别不同，请确认。");
			return false;
		}
	
		submitForm();
	}
}

//校验录入数据的合法性
function checkData()
{
	if(fm.ProjectNo.value == null || fm.ProjectNo.value == "" )
	{
	    alert("项目名不能为空");
	    return false;
	}
	if(fm.ProjectType.value == "")
	{
		alert("项目类型不能为空");		
		return false;
	}
	if(fm.PostNo.value == null  || fm.PostNo.value == "")
	{
		alert("岗位级别不能为空");
		return false;
	}
	
	//校验项目和项目类型是否相符
	var sql = "  select * "
	          + "from LMEdorItem "
	          + "where edorCode = '" + fm.ProjectNo.value + "' "
	          + "    and appObj = '" + (fm.ProjectType.value == "0" ? "I" : "G") + "' ";
	var result = easyExecSql(sql);
	if(result == null)
	{
	    alert("项目和项目类型不相符，请确认。");
	    return false;
	}
	
	if(!checkInputData())
	{
	    return false;
	}
	
	return true;
}

function checkInputData()
{
  if(fm.ConfirmFlag[2].checked == true)
	{
    	if(fm.MoneyPurview.value != "" 
    	  && fm.MoneyPurviewFull.checked == false 
    	  && !isNumeric(fm.MoneyPurview.value))
    	{
    	    alert("金额权限应为正整数。");
    	    return false;
    	}
    	if(fm.ExcessPurview.value != "" 
    	  && fm.ExcessPurviewFull.checked == false
    	  && !isNumeric(fm.ExcessPurview.value))
    	{
    	    alert("超额权限应为正整数。");
    	    return false;
    	}
    	if(fm.TimePurview.value != "" 
    	  && fm.TimePurviewFull.checked == false
    	  &&!isNumeric(fm.TimePurview.value))
    	{
    	    alert("期限权限应为正整数。");
    	    return false;
    	}
	}
  
  if(fm.RiskCode.value==null||fm.RiskCode.value==''){
	  return true;
  }else{
		if(fm.ExcessPurviewFull.checked==true||(fm.ExcessPurview.value!=''&&fm.ExcessPurview.value!='0')
//			||(fm.TimePurview.value!=''&&fm.TimePurview.value!='0')||fm.TimePurviewFull.checked==true
			||fm.all("ConfirmFlag")[0].checked == true||fm.all("ConfirmFlag")[1].checked == true){
			alert("特殊险种的权限配置只允许录入金额权和期限权限");
			return false;
		}
  }
  
	return true;
}

function clearMoney()
{
    fm.MoneyPurview.value = "";
    fm.MoneyPurviewFull.checked = false;
    fm.ExcessPurview.value = "";
    fm.ExcessPurviewFull.checked = false;
    fm.TimePurview.value = "";
    fm.TimePurviewFull.checked = false;
}

//提交，保存按钮对应操作
function submitForm()
{
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.onfocus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		window.location.reload();
		top.opener.location.reload();
		
	}	
}

function queryMember()
{
	showInfo = window.open("./TaskQueryMemberMain.jsp", "QueryMember");
}

//处理查询按钮返回值
function afterQuery(result)
{
	fm.MemberNo.value = result;
	fm.MemberNoName.value = result;
	
	showCodeName();
}

//快速新建岗位级别确认权
function createConfirm()
{
	window.open("TaskPurviewConfirmMain.jsp", "");
}