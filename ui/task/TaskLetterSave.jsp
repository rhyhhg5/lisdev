<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：TaskLetterSave.jsp
//程序功能：
//创建日期：2005-08-22
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="com.sinosoft.task.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
	String FlagStr = "";
	String Content = "";
	
	try
	{	
		if(request.getParameter("showLetterFlag").equals("1"))
		{
			throw new Exception("您正在预览函件，不能保存");
		}
		
		LGLetterSchema tLGLetterSchema = new LGLetterSchema();
		tLGLetterSchema.setEdorAcceptNo(request.getParameter("workNo"));
		tLGLetterSchema.setLetterType(request.getParameter("letterType"));
		tLGLetterSchema.setLetterSubType(request.getParameter("letterSubType"));
		tLGLetterSchema.setSendObj(request.getParameter("sendObjct"));
		tLGLetterSchema.setAddressNo(request.getParameter("addressNo"));
		tLGLetterSchema.setBackFlag(request.getParameter("backFlag"));
		tLGLetterSchema.setBackDate(request.getParameter("backDate"));
		tLGLetterSchema.setLetterInfo(request.getParameter("letterInfo"));
	  tLGLetterSchema.setSerialNumber(request.getParameter("SerialNumber"));
	   GlobalInput tG = (GlobalInput)session.getValue("GI");
	    
		VData tVData = new VData();
    	tVData.add(tG);
    	tVData.add(tLGLetterSchema);
    	       
    	PrtLetterUI tPrtLetterUI = new PrtLetterUI("Save");
		XmlExport txmlExport = new XmlExport();    
    	if(!tPrtLetterUI.submitData(tVData,"PRINT"))
    	{
    		FlagStr = "Fail";
    		if(tPrtLetterUI.mErrors.needDealError())
    		{
    	   		Content = tPrtLetterUI.mErrors.getErrContent();
    	   	}
    	}
    	else
    	{
    		FlagStr = "Succ";
    		Content = "成功保存函件信息";
    		System.out.println("成功保存函件信息");
		}
	}
	catch(Exception e)
	{
		FlagStr = "Fail";
		Content = e.toString();
	}
	
	Content = PubFun.changForHTML(Content);
%>

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>