//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select LCRnewStateLog.PrtNo,LCRnewStateLog.ProposalNo,LCRnewStateLog.PolNo,LCPol.RiskCode,LCPol.InsuredNo,LCPol.InsuredName,LCPol.uwflag,LCPol.ManageCom from LCPol ,LCRnewStateLog where 1=1  and LCPol.proposalno = LCRnewStateLog.proposalno and  LCPol.appflag='9' "			 
				  + "and   LCRnewStateLog.state in('1','2','3') "
				 + getWherePart( 'LCRnewStateLog.PrtNo','PrtNo' )
				 + getWherePart( 'LCRnewStateLog.PolNo','PolNo' )
				 + getWherePart( 'LCRnewStateLog.ProposalNo','ProposalNo' )
				 + getWherePart( 'LCRnewStateLog.RiskCode','RiskCode' )
				 + getWherePart( 'LCRnewStateLog.InsuredNo','InsuredNo' )
				 + getWherePart( 'LCRnewStateLog.ManageCom','ManageCom','like' )
				 + getWherePart( 'LCRnewStateLog.MakeDate','EdorAppDate');
					
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("无处于待发续保催收通知书的续保保单！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}


//查询按钮
function QueryClick()
{
	initPolGrid();
	fm.all('Transact').value ="QUERY|EDOR";
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	//alert(arrSelected[0][0]);
	return arrSelected;
}

	//撤销批单
function CancelEdor()
{
	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
	
			
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
		else
		{
			arrReturn = getQueryResult();
			fm.all('PrtNoHide').value = PolGrid.getRowColData(tSel-1,1);
			fm.all('PolNoHide').value = PolGrid.getRowColData(tSel-1,2);
			//---------------------------------------
			//cEdorNo = PolGrid.getRowColData(tSel-1,1);
			cPolNo = PolGrid.getRowColData(tSel-1,2);
			 alertflag = "1";//10-29
			//-----------------------------------
			fm.all("Transact").value = "DELETE||Rnew"
			// showSubmitFrame(mDebug);
		    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			//easyQueryClick();   //10-22
			fm.submit();  //10_22
		}
}


function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	
	//********dingzhong*************
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	//*******************************
/*	
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	//alert(arrSelected[0][0]);
*/	
	return arrSelected;
}


function afterSubmit( FlagStr, content )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
	fm.all('PrtNoHide').value ="";
	fm.all('PolNoHide').value="";	  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   fm.all('PrtNoHide').value ="";
   fm.all('PolNoHide').value="";	
    easyQueryClick();
  }
  	
  
}