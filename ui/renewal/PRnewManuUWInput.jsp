<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：PEdorManuUWInput.jsp.
//程序功能：保全个人人工核保
//创建日期：2003-12-29 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String tPolNo = "";
	tPolNo = request.getParameter("PolNo");
	session.putValue("PolNo",tPolNo);
%>
<html>
<%
  //个人下个人
	String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var grpPolNo = "<%=tGrpPolNo%>";      //个人单的查询条件.
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
	var uwgrade = "";
    var appgrade= "";
    var uwpopedomgrade = "";
    var codeSql = "code";
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="PRnewManuUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PRnewManuUWInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./PRnewManuUWCho.jsp">
    <!-- 保单查询条件 -->
    <table class= common border=0 width=100%>
    	<tr>
		<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 投保单号 </TD>
          <TD  class= input> <Input class= common name=QProposalNo > </TD>
					<TD  class= title> 险种编码</TD><TD  class= input>
					<Input class= "codeno"  name=QRiskCode  ondblclick="return showCodeList('RiskCode',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,null,null,1);" ><Input class=codename  name=RiskName></TD>          
					<TD  class= title> 管理机构</TD><TD  class= input>
					<Input class= "codeno"  name=QManageCom  ondblclick="return showCodeList('station',[this,comcodename],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('station',[this,comcodename],[0,1],null,null,null,1);" ><Input class=codename  name=comcodename></TD>
		 		
        </TR>
        <TR  class= common>
					<TD  class= title8>核保级别</TD>
					<TD  class= input8> <input class=codeno name=UWGradeStatus value= "1" CodeData= "0|^1|本级保单^2|下级保单"  ondblclick="return showCodeListEx('Grade',[this,UWGradeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Grade',[this,UWGradeName],[0,1],null,null,null,1);"><input class=codename name=UWGradeName></TD>
					<TD  class= title>  投保人名称 </TD>
          <TD  class= input> <Input class=common name=QAppntName > </TD>
          <TD  class= title>  被保人名称 </TD>
          <TD  class= input> <Input class=common name=QInsuredName > </TD>          
        </TR>
        <TR  class= common>
					<TD  class= title8>保单状态</TD>
					<TD  class= input8> <input class=codeno readonly name=State value= "" CodeData= "0|^1|未人工核保^2|核保已回复^3|核保未回复"  ondblclick="return showCodeListEx('State',[this,StateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('State',[this,StateName],[0,1],null,null,null,1);"><input class=codename name=StateName></TD>
					<TD  class= title>  核保人  </TD>
          <TD  class= input>   <Input class= common name=QOperator value= "">   </TD>
          <TD  class= title>  印刷号码  </TD>
          <TD  class= input> <Input class=common name=QPrtNo> </TD>
        </TR>
        <TR  class= common>
          <TD  class= input>   <Input  type= "hidden" class= Common name = QComCode >  </TD>
       </TR>
    </table>
          <INPUT VALUE="查 询" class=common TYPE=button onclick="easyQueryClick();"> 
          <!--INPUT VALUE="撤单申请查询" class=common TYPE=button onclick="withdrawQueryClick();"--> 
          <INPUT type= "hidden" name= "Operator" value= ""> 
    <!-- 保单查询结果部分（列表） -->
    <Div  id= "divLCPol1" style= "display: ''">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 投保单查询结果
    		</td>
    	</tr>
    </table>
    
      	<table  class= common>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=common TYPE=button onclick="getLastPage();">      
    </div>
   <Div  id= "divMain" style= "display: 'none'">
    <!--附加险-->
    <Div  id= "divLCPol2" style= "display: 'none'">
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol2);">
    		</td>
    		<td class= titleImg>
    			 投保单查询结果
    		</td>
    	    </tr>
        </table>	
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolAddGrid" onclick="getPolGridCho();">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <INPUT VALUE="首页" class=common TYPE=button onclick="getFirstPage();"> 
        <INPUT VALUE="上一页" class=common TYPE=button onclick="getPreviousPage();"> 					
        <INPUT VALUE="下一页" class=common TYPE=button onclick="getNextPage();"> 
        <INPUT VALUE="尾页" class=common TYPE=button onclick="getLastPage();"> 					
    </Div>
    <!-- 保全核保 -->
    <table class= common border=0 width=100%>
    	 <tr>
	        <td class= titleImg align= center>续保核保信息：</td>
	     </tr>
    </table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title> 投保单号码</TD>
          <TD  class= input><Input class="readonly" readonly name=ProposalNo ></TD>
          <TD  class= title>险种编码</TD>
          <TD  class= input><Input class="readonly" readonly name=RiskCode ></TD>
          <TD  class= title>险种版本</TD>
          <TD  class= input><Input class="readonly" readonly name=RiskVersion ></TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 管理机构</TD>
          <TD  class= input><Input class="readonly" readonly name=ManageCom ></TD>
          <TD  class= title> 投保人客户号</TD>
          <TD  class= input><Input class="readonly" readonly name=AppntNo ></TD>
          <TD  class= title>投保人姓名</TD>
          <TD  class= input><Input class="readonly" readonly name=AppntName ></TD>
        </TR>
        <TR  class= common>
          <TD  class= title> 被保人客户号</TD>
          <TD  class= input><Input class="readonly" readonly name=InsuredNo ></TD>
          <TD  class= title> 被保人姓名</TD>
          <TD  class= input><Input class="readonly" readonly name=InsuredName ></TD>
          <TD  class= title>被保人性别</TD>
          <TD  class= input><Input class="readonly" readonly name=InsuredSex ></TD>
        </TR>
        <TR  class= common>
          <TD  class= title>份数</TD>
          <TD  class= input><Input class="readonly" readonly name=Mult ></TD>          
          <TD  class= title>保费</TD>
          <TD  class= input><Input class="readonly" readonly name=Prem ></TD>          
          <TD  class= title>保额</TD>
          <TD  class= input> <Input class="readonly" readonly name=Amnt ></TD>
        </TR>
        <TR  class= common>
          <TD  class= input>
          <INPUT  type= "hidden" class= Common name= UWGrade value= "">
          <INPUT  type= "hidden" class= Common name= AppGrade value= "">
          <INPUT  type= "hidden" class= Common name= PrtNo  value= "">
          <INPUT  type= "hidden" class= Common name= PolNo  value= "">
          <INPUT  type= "hidden" class= Common name= MissionID  value= "">
          <INPUT  type= "hidden" class= Common name= SubMissionID  value= "">
          <INPUT  type= "hidden" class= Common name= SubNoticeMissionID  value= "">
         </TD>
       </TR>
	</table>
	<table class= common border=0 width=100% >
	  <hr>
	  </hr>
          <INPUT VALUE="投保单明细信息" class=common TYPE=button onclick="showPolDetail();"> 
          <INPUT VALUE="扫描件查询" class=common  TYPE=button onclick="ScanQuery();"> 
          <INPUT VALUE="既往投保信息" class=common TYPE=button onclick="showApp();"> 
          <!--INPUT VALUE="以往核保记录" class=common TYPE=button onclick="showOldUWSub();"-->
          <INPUT VALUE="最终核保信息" class=common TYPE=button onclick="showNewUWSub();">
          <br><br>
          <tr></tr>
          <span id= "divAddButton1" style= "display: ''">
          	<!--input value="体检资料查询" class=common type=button onclick="showHealthQ();" width="200"-->
          </span>
          <input value="体检资料查询" class=common type=button onclick="showHealthQ();" >         
          <INPUT VALUE="生存调查查询" class=common TYPE=button onclick="RReportQuery();">
          <!--INPUT VALUE="保全明细查询" class= common TYPE=button onclick="PrtEdor();"--> 
          <!--INPUT VALUE="保全项目查询" class= common TYPE=button onclick="ItemQuery();"--> 
          <INPUT  VALUE="既往理赔查询" class=common TYPE=button onclick="ClaimGetQuery();"> 
          
          <span  id= "divAddButton2" style= "display: ''">
          	<!--INPUT VALUE="生存调查查询" class=common TYPE=button onclick="RReportQuery();"-->
          </span>         
          <!--input value="问题件查询" class=common type=button onclick="QuestQuery();"-->
          <span  id= "divAddButton3" style= "display: ''">
          	<!--input value="撤单申请查询" class=common type=button onclick="BackPolQuery();"-->
          </span>
          <span  id= "divAddButton4" style= "display: ''">
          	<!--input value="催办查询" class=common type=button onclick="OutTimeQuery();"-->
          </span>
          <tr>   <hr> </hr>  </tr>
          <span  id= "divAddButton5" style= "display: ''">
          <!--input value="体检资料录入" class=common type=button onclick="showHealth();" width="200"-->          
          </span>
          <input value="体检资料录入" class=common type=button onclick="showHealth();" width="200">
          <input value="生调请求说明" class=common type=button onclick="showRReport();">
          <span  id= "divAddButton6" style= "display: ''">
          	<!--input value="生调请求说明" class=common type=button onclick="showRReport();"-->
          </span>
          <span  id= "divAddButton9" style= "display: ''">
          	<!--input value="承保计划变更" class=common type=button onclick="showChangePlan();"-->
          </span>
          <!--INPUT VALUE="问题件录入" class=common TYPE=button onclick="QuestInput();"-->
          <input value="加费承保录入" class=common type=button name= "AddFee"  onclick="showAdd();"> 
          <input value="特约承保录入" class=common type=button onclick="showSpec();"> 
           <input value="发核保通知书" class=common type=button onclick="SendNotice();"> 
          <!--tr> <hr> </hr>  </tr-->
          <span  id= "divAddButton7" style= "display: ''">
          	<!--INPUT VALUE="发首期交费通知书" class=common TYPE=button onclick="SendFirstNotice();"-->
          </span>
          <!--<input value="发催办通知书" type=button onclick="SendPressNotice();">-->
          <br><br>   
          <span  id= "divAddButton8" style= "display: ''">
          	<!--input value="发核保通知书" class=common type=button onclick="SendNotice();"-->
          </span>
          <!--input value="发体检通知书" class=common type=button onclick="SendHealth();"-->
          <input value="核保分析报告录入" class=common type=button onclick="showReport();" width="200">                         
          <input value="记事本" class=common type=button onclick="showNotePad();" width="200">
          <span  id= "divAddButton10" style= "display: ''">
          	<!--input value="承保计划变更结论录入" class=common type=button onclick="showChangeResultView();"-->
          </span>
    </table>
    	  <input type="hidden" name= "PrtNoHide" value= "">
    	  <input type="hidden" name= "PolNoHide" value= "">
    	  <input type="hidden" name= "MainPolNoHide" value= "">
    	  <input type="hidden" name= "EdorNoHide" value= "">
    	  <input type="hidden" name= "EdorTypeHide" value= "">
      <div id = "divUWResult" style = "display: ''">    
    	  <!-- 核保结论 -->
    	  <table class= common border=0 width=100%>
    	  	<tr>
			<td class= titleImg align= center>续保核保结论：</td>
	  	</tr>
	  </table>
    
  	  <table  class= common align=center>
    
    	  	<TR  class= common>
          		<TD height="29"  class= title>
          		 	<!--span id= "UWResult"> 保全核保结论 <Input class="code" name=edoruwstate value= "1" CodeData= "0|^1|本级保单^2|下级保单" ondblClick="showCodeListEx('edoruwstate',[this,''],[0,1]);" onkeyup="showCodeListKeyEx('edoruwstate',[this,''],[0,1]);">  </span-->      
          		 	<span id= "UWResult">续保核保结论<Input class="code" name=edoruwstate ondblclick="return showCodeList('uwstate',[this]);" onkeyup="return showCodeListKey('uwstate',[this]);">  </span>          		
	   			    <span id= "UWDelay" style = "display: ''">延长时间<Input class="common" name=UWDelay value= ""> </span>          		
	   		        <span id= "UWPopedom" style = "display: ''">上报核保<Input class="code" name=UWPopedomCode ondblclick="showCodeList('UWPopedomCode', [this,''], [0, 1],null,codeSql,'1',1);" onkeyup="showCodeListKey('UWPopedomCode', [this,''], [0, 1],null,codeSql,'1',1);"> </span> 
	   		 	    <span id= "Delay" style = "display: 'none'">本单核保师<Input class="common" name=UWOperater value= ""> </span>  
	   		 	   </TD>
          	</TR>
		<tr></tr>
          	<TD height="24"  class= title>
            		续保核保意见
          	</TD>
		<tr></tr>          
      		<TD  class= input> <textarea name="UWIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
	  </table>
          
  	<p>
    		<INPUT VALUE="确定" class=common TYPE=button onclick="submitForm();">
    		<INPUT VALUE="取消" class=common TYPE=button onclick="cancelchk();">
  	</p>
    </div>
    <div id = "divChangeResult" style = "display: 'none'">
      	  <table  class= common align=center>
          	<TD height="24"  class= title>
            		承保计划变更结论录入:
          	</TD>
		<tr></tr>          
      		<TD  class= input><textarea name="ChangeIdea" cols="100%" rows="5" witdh=100% class="common"></textarea></TD>
    	 </table>
    	 <INPUT VALUE="确定" class=common TYPE=button onclick="showChangeResult();">
    	 <INPUT VALUE="取消" class=common TYPE=button onclick="HideChangeResult();">
    </div>
  </Div>
  <table  class= common align=center>
      	
	</table>          
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
