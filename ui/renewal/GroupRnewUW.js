//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  alert("submit");
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    alert(content);
  }
  else
  { 

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    alert(content);
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//既往投保信息
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  cInsureNo = fm.InsuredNo.value;
  //showModalDialog("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,"window1");
  showInfo.close();
}                

//以往核保记录
function showOldUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  //showModalDialog("./UWSubMain.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./UWSubMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}

//当前个人核保记录
function showNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  window.open("./UWErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}

//当前集体核保记录
function showGNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fmQuery.GrpProposalNo.value;
  window.open("./UWGErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}
           
                   

//保单明细信息
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  window.open("../app/ProposalDisplay.jsp?PolNo="+cProposalNo,"window1");
  showInfo.close();
}

//体检资料
function showHealth()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  cProposalNo=fm.ProposalNo.value;
  if (cProposalNo != "")
  {
  	window.open("./UWManuHealthMain.jsp?ProposalNo1="+cProposalNo,"window1");
  	showInfo.close();
  }
  else
  {
  	showInfo.close();
  	alert("保单号不能为空!");
  }
}                      

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initPolGrid();
  fmQuery.submit(); //提交
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  showModalDialog("./ProposalDuty.jsp",window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=13cm");
  
}           

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  showModalDialog("./ProposalFee.jsp",window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");
  
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//查询集体单
//查询个人单
function querygrp()
{
	// 初始化表格                                                                                                                                                           
	
	// 书写SQL语句
	var str = "";
	var GrpProposalNo = fmQuery.all('GrpProposalNo').value;
	var mOperate = fmQuery.all('Operator').value;
	//alert("grp:"+GrpProposalNo);
	if (GrpProposalNo == "")
	{
	}
	else
	{
		str = " and LCGrpPol.GrpProposalNo = '" + GrpProposalNo + "'";
	}
	
	
	var strsql = "";
	strsql = "select LCGrpPol.GrpProposalNo,LCGrpPol.GrpName,LCGrpPol.RiskCode,LCGrpPol.RiskVersion,LCGrpPol.ManageCom from LCGrpPol,LCGUWMaster where 1=1 "
				 + " and LCGrpPol.AppFlag='0'"
				 + " and LCGrpPol.ApproveFlag = '9' "
				 + " and LCGrpPol.UWFlag in ('2','5','6')"
				 + " and LCGrpPol.ManageCom like '" + manageCom + "%%'"
				 + " and LCGrpPol.contno = '00000000000000000000'"				          //自动核保待人工核保
				 + str
				 + " and LCGrpPol.GrpPolNo = LCGUWMaster.GrpPolNo"
				 + " and LCGUWMaster.passflag in ('0','5','6')"			 				 
				 + " and (LCGUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') or LCGUWMaster.appgrade is null)";
								 				 
	//alert(strsql);
	
	//execEasyQuery( strSQL );
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件集体单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

//查询个人单
function queryPol()
{
	// 初始化表格                                                                                                                                                           
	
	// 书写SQL语句
	
	var GrpProposalNo = fmQuery.all('GrpProposalNo').value;
	var mOperate = fmQuery.all('Operator').value;
	//alert("grp:"+GrpProposalNo);
	
	var strsql = "";
	strsql = "select LCPol.ProposalNo,LCPol.AppntName,LCPol.RiskCode,LCPol.RiskVersion,LCPol.InsuredName,LCPol.ManageCom from LCPol,LCGUWMaster where 1=1 "
				 + " and LCPol.AppFlag='2'"
				 + " and LCPol.UWFlag in ('2','5','6')"         //自动核保待人工核保
				 + " and LCPol.contno = '00000000000000000000'"
				 + " and LCPol.GrpPolNo = '"+GrpProposalNo+"'"				 
				 + " and LCPol.GrpPolNo = LCGUWMaster.GrpPolNo"
				 + " and LCGUWMaster.passflag in ('0','5','6')"			 				 
				 + " and (LCGUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') or LCGUWMaster.appgrade is null)";
								 				 
	//alert(strsql);
	
	//execEasyQuery( strSQL );
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("此单下个人单核保全部通过！");
    return "";
  }
  
  showDiv(divPerson,"true");
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

//选择要人工核保保单
function getPolGridCho()
{
	//setproposalno();
	fmQuery.submit();
}

//个人单核保确认
function manuchk()
{
	cProposalNo=fm.ProposalNo.value;
	flag = fm.all('UWState').value;
	tUWIdea = fm.all('UWIdea').value;
	//alert("flag:"+flag);
	
	if (flag == "0"||flag == "1"||flag == "4"||flag == "6"||flag == "9")
	{
		showModalDialog("./UWManuNormMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	}
	
	if (flag == "2") //延期
	{
		//showModalDialog("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
		window.open("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
	}
	
	if (flag == "3") //条件承保
	{
		//showModalDialog("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
		window.open("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
	}
}

//团体整单确认
function gmanuchk()
{
	cProposalNo=fmQuery.GrpProposalNo.value;
	flag = fm.all('GUWState').value;
	tUWIdea = fm.all('GUWIdea').value;
	//alert("flag:"+flag);
	
	if (flag == "1"||flag == "4"||flag == "6"||flag == "9")
	{
		showModalDialog("./UWGrpManuNormMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	}
	
	if (flag == "2") //延期
	{
		//showModalDialog("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
		window.open("./UWManuDateMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
	}
	
	if (flag == "3") //条件承保
	{
		//showModalDialog("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
		window.open("./UWManuSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+flag+"&UWIdea="+tUWIdea,"window1");
	}
	
	if (flag == "7")
	{
		QuestInput();
	}
	
	initPolBox();
	initPolGrid();
	//initGrpGrid();
}


function question()
{
	cProposalNo = fmQuery.GrpProposalNo.value;
	cQuest = fm.Quest.value;
	
	showModalDialog("./UWManuQuesMain.jsp?ProposalNo1="+cProposalNo+"&question="+cQuest,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	

	initPolBox();
	initPolGrid();
	//initGrpGrid();
	
}

function QuestInput()
{
	cProposalNo = fmQuery.GrpProposalNo.value;  //保单号码
	
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	
	//initInpBox();
    	//initPolBox();
	//initPolGrid();
	
}

function QuestQuery()
{
	cProposalNo = fmQuery.GrpProposalNo.value;  //保单号码
	//showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
	window.open("./QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
	
	//initInpBox();
    	//initPolBox();
	//initPolGrid();
	
}

function cancelchk()
{
	fm.all('UWState').value = "";
	fm.all('UWIdea').value = "";	
}
