
<%@page import="com.sinosoft.lis.xb.GRnewDueFeeUI"%><%
//程序名称：GRnewDueFeeSave.jsp
//程序功能：
//创建日期：2018-08-07
//创建人  ：lzj
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
  <%@page import="java.util.*"%>
  <%@page import="java.lang.*"%>  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>     
<%@page contentType="text/html;charset=GBK" %>
<% 
  //保存保单号  
  String PrtNo = request.getParameter("PrtNo"); 
  String GrpContNo=request.getParameter("ProposalGrpContNo");//说明团单表的主键是GrpContNo,用它传值到后台处理
  String StartDate=request.getParameter("StartDate");
  String EndDate=request.getParameter("EndDate");//传入后台逻辑处理层做校验
  String ManageCom=request.getParameter("ManageCom");//传入后台逻辑处理层做校验
  
// 输出参数
   CErrors tError = null;          
   String FlagStr = "";
   String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    //集体保单表
    LCGrpContSchema tLCGrpContSchema = new LCGrpContSchema();
    tLCGrpContSchema.setGrpContNo(GrpContNo);
    tLCGrpContSchema.setPrtNo(PrtNo);//Yangh于2005-07-19将以前的注销
    TransferData tempTransferData=new TransferData();
    tempTransferData.setNameAndValue("StartDate",StartDate);
    tempTransferData.setNameAndValue("EndDate",EndDate);
	tempTransferData.setNameAndValue("ManageCom",ManageCom);
	  
    VData tVData = new VData(); 
    tVData.add(tLCGrpContSchema);
    tVData.add(tGI);
    tVData.add(tempTransferData);

    GRnewDueFeeUI tGRnewDueFeeUI = new GRnewDueFeeUI(); 
    tGRnewDueFeeUI.submitData(tVData,"INSERT");
    if (!tGRnewDueFeeUI.mErrors.needDealError())
     {                          
       Content = " 处理成功";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content =" 失败，原因是:" + tGRnewDueFeeUI.mErrors.getFirstError();
       FlagStr = "Fail";
     }       
  }//页面有效区
  
%>                                      
<html>
  <script language="javascript">
     parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
<body>
</body>
</html>