<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="PRnewUWAppCancel.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PRnewUWAppCancelInit.jsp"%>
  <title>续保查询 </title>
</head>
<body  onload="initForm();">
  <form action="./PRnewUWAppCancelSubmit.jsp" method=post name=fm target="fraSubmit" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入个人续保查询条件：
			</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            保单印刷号
          </TD>
          <TD  class= input>
            <Input class= common name=PrtNo >
          </TD>
          <TD  class= title>
            原保单号
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo >
          </TD>
          <TD  class= title>
            新投保单号
          </TD>
          <TD  class= input>
            <Input class= common name=ProposalNo >
          </TD>
        </TR>
        <TR  class= common>
					<TD  class= title> 管理机构</TD><TD  class= input>
					<Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('station',[this,comcodename],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('station',[this,comcodename],[0,1],null,null,null,1);" ><Input class=codename  name=comcodename></TD>
					<TD  class= title> 险种编码</TD><TD  class= input>
					<Input class= "codeno"  name=RiskCode  ondblclick="return showCodeList('RiskCode',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RiskCode',[this,RiskName],[0,1],null,null,null,1);" ><Input class=codename  name=RiskName></TD>
          <TD  class= title>
            被保人客户号码
          </TD>
          <TD  class= input>
            <Input class=common name=InsuredNo >
          </TD>
          </TR>
        <TR  class= common>
          <TD  class= title>
            申请日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate >
          </TD>
        </TR>
    </table>  
    <input type="hidden" name= "PrtNoHide" value= "">
    <input type="hidden" name= "PolNoHide" value= "">
          <table class= common align=center>
          <INPUT class = common VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 保单的信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
     	</table>		
       <input type=hidden name=Transact >
      <INPUT class = common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class = common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class = common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class = common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	  			
       
   	<table class= common align=center>
    		<tr class= common>
    			<td  class= input>
    				<INPUT class= common VALUE="撤销续保催收" TYPE=button onclick="CancelEdor();">     			
    			</td>
    			<td>
    			</td>
    		</tr>
    	</table>
       			
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
