<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%
//程序名称：AllPBqQuerySubmit.jsp
//程序功能：
//创建日期：2003-1-20
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.xb.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LCPolSchema tLCPolSchema = new LCPolSchema();
  PRnewUWAppCancelUI tPRnewUWAppCancelUI = new PRnewUWAppCancelUI();
  
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
   String Operator  = tGI.Operator ;  //保存登陆管理员账号
   String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  
  CErrors tError = null;
  String tBmCert = "";
  
  //后面要执行的动作：添加，修改，删除
  String transact=request.getParameter("Transact");
  System.out.println("transact:"+transact); 

    tLCPolSchema.setPrtNo(request.getParameter("PrtNoHide"));
    tLCPolSchema.setPolNo(request.getParameter("PolNoHide"));        
        
 try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tGI);
   tVData.addElement(tLCPolSchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    if (!tPRnewUWAppCancelUI.submitData(tVData,transact))
    {
        Content = " 续保撤销失败，原因是:";
        Content = Content + tPRnewUWAppCancelUI.mErrors.getError(0).errorMessage;
        FlagStr = "";
	}
	else
	{
	tError = tPRnewUWAppCancelUI.mErrors;
	if (!tError.needDealError())
	{
	Content ="续保撤销成功！";
	FlagStr = "Succ";}
	else
	{
	Content = "续保撤销成功！系统提示:" + tError.getFirstError();
	FlagStr = "Fail";
	}
	}
	}
  catch(Exception ex)
  {
    Content = "续保撤销失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
 
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

