var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick()
{
	initPolGrid();
	var strXB = "";
	var strXQ = "";
	var strSQL = "";
	if(fm.all('StartDate').value.length<2 || fm.all('EndDate').value.length<2 )
	{
		alert("请输入正确的时间范围！");
		return;
	}
	
	strXB = "select PolNo,PrtNo,RiskCode,PaytoDate,PayEndDate,'续保' XBXQ,decode((select distinct(PolNo) from LPEdormain where polno=LCPol.PolNo and abs(LPEdormain.paytodate - edorappdate)<=60),NULL,'普通','保全') BQ,decode((SELECT polno FROM ldsystrace WHERE polstate='4001' AND valiflag='1' AND polno=LCPol.PolNo),NULL,'普通','已有死亡报案') LiPei from LCPol "
	    	   + "where appflag='1' and GrpPolNo='00000000000000000000' "
	     	   + "and PaytoDate=PayEndDate "
	       	   + "and PaytoDate>='"+fm.all('StartDate').value+"' and PaytoDate<='"+fm.all('EndDate').value
	           + "' and nvl(stopflag,0)=0 and not exists (select polno from LCRnewStateLog where LCRnewStateLog.polno = LCpol.polno) "
	           + "and exists (select riskcode from LMRiskRnew where LMRiskRnew.riskcode=LCPol.riskcode) "
	           + "and RnewFlag=-1";
	
	strXQ = "select PolNo,PrtNo,RiskCode,PaytoDate,PayEndDate,'续期' XBXQ,decode((select distinct(PolNo) from LPEdormain where polno=LCPol.PolNo and abs(LPEdormain.paytodate-edorappdate)<=60),NULL,'普通','保全') BQ,decode((SELECT polno FROM ldsystrace WHERE polstate='4001' AND valiflag='1' AND polno=LCPol.PolNo),NULL,'普通','已有死亡报案') LiPei from LCPol "
	      + "where AppFlag='1' and GrpPolNo='00000000000000000000' "
	      + "and PaytoDate < PayEndDate and PaytoDate>='"+fm.all('StartDate').value+"' and PaytoDate<='"+fm.all('EndDate').value
	      +"' and nvl(StopFlag,0)=0 and PayIntv>0 "
	      +"and not exists ( select otherno from ljspay where ljspay.OtherNo = LCPol.PolNo and OtherNoType='2') ";
	
	if(fm.all('Flag').value == "1")
	{
	    strXB = strXB + " and exists (select polno from LPEdormain where LPEdormain.polno=LCPol.PolNo and abs(LPEdormain.paytodate - edorappdate)<=60))";
	    strXQ = strXQ + " and exists (select polno from LPEdormain where LPEdormain.polno=LCPol.PolNo and abs(LPEdormain.paytodate - edorappdate)<=60))";
	}
    
    if( fm.all('xbFlag').value == "1" )
       strSQL = strXB + " order by PrtNo";
    else
       strSQL = "select PolNo,PrtNo,RiskCode,PaytoDate,PayEndDate,XBXQ,BQ,LiPei from (( " + strXB + " ) UNION ALL ( " + strXQ +" )) order by PrtNo";

   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有待续保个人单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}
