var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initPolGrid();

	var strSQL = "";
	strSQL = "select PrtNo,PolNo,GrpPolNo,RiskCode,makedate,ProType,Operator from LCGrpRnewErrLog where 1=1 "
				 + getWherePart( 'makedate','StartDate' )
				 + getWherePart( 'RiskCode' )
				 + getWherePart( 'PrtNo' )
				 + " order by PrtNo,MakeDate ";

  //alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

  if (!turnPage.strQueryResult) {
    alert("没有续保错误日志信息！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function easyQueryAddClick()
{
	var tSelNo = PolGrid.getSelNo()-1;
	fm.PolNoHide.value = PolGrid.getRowColData(tSelNo,2);	
   fm.Remark.value = "";

	var strSQL = "";
	strSQL = "select errinfo from LCGrpRnewErrLog where 1=1 "
				 + getWherePart( 'PolNo', 'PolNoHide')
				 + " ";
  //alert(strSQL);
  turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  

  if (!turnPage2.strQueryResult) {

    alert("没有待续保日志信息！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult); 
  fm.Remark.value = turnPage2.arrDataCacheSet[0][0];	  
  return true;
}