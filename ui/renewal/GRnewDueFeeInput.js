//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  fm.all('singleDue').disabled=false;  	
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
  }
  resetForm();
   
}




//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{
    var tSel = GrpContGrid.getSelNo();	
    if( tSel == 0 || tSel == null )
    {
	alert( "请先选择一条记录，再点击【生成催收记录】按钮。" );
	return false;
    }
    var tRow = GrpContGrid.getSelNo() - 1; 
    fm.PrtNo.value=GrpContGrid.getRowColData(tRow, 2);
    fm.all('ProposalGrpContNo').value=GrpContGrid.getRowColData(tRow, 1);
    return true;
}           

         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//保单个案催收
function dealWMD()
{
  if(CheckGrpPolNo())
  {
    GrpSingle();
  }
}


//处理个案催收
function GrpSingle()
{
  var showStr="程序正在处理，请耐心等待。";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('singleDue').disabled=true;  	  
  fm.submit();
}

function CheckDate()
{
  if(!isDate(fmMulti.all('StartDate').value)||!isDate(fmMulti.all('EndDate').value))  
    return false;
 
  var flag=compareDate(fmMulti.all('StartDate').value,fmMulti.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

// 查询按钮
function easyQueryClick()
{
	if(!verifyInput2())
		return;
	// 初始化表格
	initGrpContGrid();
	//选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	  

	var strSQL11 = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
	 strSQL11 = " and  agentcode=db2inst1.getAgentCode('"+fm.AgentCode.value+"') ";
	}
	var strSQL = "select grpcontno,prtno,grpname,CValiDate,nvl(SumPrem,0),"
				+ "nvl((select AccGetMoney from LCAppAcc where CustomerNo=LCGrpCont.appntno),0),"
				+ "nvl((select sum(prem) from lccont where LCGrpCont.grpcontno = grpcontno),0),"
				+ "(SELECT min(paytodate) from lcgrppol a where a.grpcontno=LCGrpCont.grpcontno), "
				+ "(select codename from ldcode where codetype='paymode' and code=LCGrpCont.PayMode), "
				+ "db2inst1.ShowManageName(LCGrpCont.ManageCom),"
				+ "db2inst1.getUniteCode(LCGrpCont.AgentCode) "
				+ "from "
				+ "lcgrpcont "
				+ "where appflag = '1' "
				+ "and (StateFlag is null or StateFlag = '1') "
				+ "and not exists (select 1 from ljspayb where otherno = grpcontno and dealstate = '2' and CancelReason = '0') "
				+ "and grpcontno in (select grpcontno from lcgrppol where riskcode = '162801' "
				+ "	and grpcontno=lcgrpcont.grpcontno "
				+ " and GrpPolNo not in (select GrpPolNo from LJSPayGrp where GrpContNo = LCGrpCont.GrpContNo)"
				+ getWherePart( 'GrpContNo','ProposalGrpContNo' )
				+ "and paytodate between '"+CurrentTime+"' and '"+SubTime+"'"
				+ ") "
				+ getWherePart( 'ManageCom', 'ManageCom','like')
				+ strSQL11
				+ getWherePart( 'PrtNo','PrtNo' );


	turnPage.queryModal(strSQL, GrpContGrid); 
	initGrpPolGrid();  
	if(GrpContGrid.mulLineCount == 0)
	{
	  alert("没有符合条件的可催收保单信息");
	}
}


// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		var cGrpContNo = GrpContGrid.getRowColData( tSel - 1, 1 );
		try
		{
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ cGrpContNo+"&ContType=1");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}
function getpoldetail()//被选中后显示的集体险种应该是符合条件的。++
{
    var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}	
	else
	{
		getPolInfo();
    }  	                                          
                         	               	
}

//被选中后显示的集体险种应该是符合条件的。++
function getPolInfo()
{
	  var tRow = GrpContGrid.getSelNo() - 1;	        
		var tGrpContNo=GrpContGrid.getRowColData(tRow,1);  
		fm.all('ProposalGrpContNo').value = tGrpContNo;
		var strSQL = "select contPlanCode, "
		            + " count(1), "
		            + "   (select riskname  from lmrisk where   riskcode=a.riskcode), "
		            + "   a.riskCode, "
		            + "   (select codeName from LDCode where code=char(a.payIntv) and codeType='payintv'), "
		            + "   sum(prem), min(cValiDate), min(payToDate), min(endDate) "
		            + "from LCPol a "
		            + "where grpContNo = '" + tGrpContNo + "' and (a.StateFlag is null or a.StateFlag = '1') "
		            + " and payintv = 0 "
		            + "group by grpContNo, contPlanCode, riskCode, riskCode, payIntv "
		            + "order by contPlanCode, riskCode ";
		
		
		
    turnPage2.queryModal(strSQL, GrpPolGrid); 
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {    
  	
  	afterJisPayQuery();  
	afterContQuery();
	
	  
  }
  catch(re)
  {
  	alert("在LJSPayPersonInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


//催收完成后查询保单
function afterContQuery()
{
	// 初始化表格
	initGrpContGrid();
	//选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量	
  
	var strSQL = "select a.GrpContNo,a.PrtNo,a.GrpName,a.CValiDate,nvl(a.SumPrem,0),nvl((select SumDuePayMoney from ljspaygrp where grpcontno = a.grpcontno and paytype = 'YEL'),0),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),db2inst1.getUniteCode(a.AgentCode),b.GetNoticeNo from LCGrpCont  a, ljspay b,ljspaygrp c where  "
	 + " a.AppFlag='1' and (a.StateFlag is null or a.StateFlag = '1') and b.OtherNo=a.GrpContNo and  b.othernotype='1' and c.GetNoticeNo=b.GetNoticeNo and c.GrpContNo=b.otherno "
	 + getWherePart( 'a.ManageCom', 'ManageCom','like')
	 + getWherePart( 'a.GrpContNo','ProposalGrpContNo' )
	 + " group by a.GrpContNo,a.PrtNo,a.GrpName,a.CValiDate,a.SumPrem,a.appntNo,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode";
	 
	turnPage.queryModal(strSQL, GrpContGrid); 
	initGrpPolGrid();  
	
}


//催收完成后查询催收纪录
function afterJisPayQuery()
{
	// 初始化表格
	initGrpJisPayGrid();
    //选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	
     var strSQL ="	select distinct b.SerialNo,b.MakeDate,'',b.Operator,b.MakeDate from  ljspay b where "
    	 + " b.othernotype='1' and otherno = '" + fm.all('ProposalGrpContNo').value + "' ";

	turnPage4.queryModal(strSQL, GrpJisPayGrid); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  
	SubTime=tempSubTime;          
}



//查询当前线程
function queryProject()
{

	divProject.style.display = '';
	divProject.style.display = '';
	var tempCurrDate = CurrentTime;
	var strSql = "select a.SerialNo,a.Operator,a.MakeDate,a.MakeTime,case when a.DealState='1' then '正在处理中' when a.DealState='2' then '处理错误' when  a.DealState='3' then '处理已完成' end"
	 + " from LCUrgeVerifyLog a"
	 +" where a.MakeDate='"+tempCurrDate+"'  and RiskFlag='1' and OperateType='1' and Operator='"+Operator+"'"
	 + " order by a.ModifyDate,a.ModifyTime  desc  fetch first row only with ur"
	 ;

  var arrReturn = new Array();
	arrReturn = easyExecSql(strSql);
	if (!(arrReturn == null ) ) 
	{
	    fm.all("Projecta").value = "批次号：" + arrReturn[0][0]+"；操作员："+ arrReturn[0][1]+"；操作时间："+arrReturn[0][2]+" " + arrReturn[0][3] +";操作状态："
	    + arrReturn[0][4];
	} 
	
}


//打印PDF前往打印管理表插入一条数据
function newprintInsManage()
{
	if(!checkPrint())
	{
	  return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}

	var tGetNoticeNo=GrpContGrid.getRowColData(tChked[0],12);  
		fm.action = "../uw/PDFPrintSave.jsp?Code=91&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
		fm.submit();

}

function checkPrint()
{
  if (GrpJisPayGrid.mulLineCount == 0)
  {
    alert("没有查询到可打印的催收记录，请先催收！");
    return false;
  }
  if (GrpContGrid.mulLineCount == 0)
  {
    alert("打印列表没有数据");
    return false;
  }
  return true;
}

//PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}