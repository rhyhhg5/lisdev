<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PRnewUWAutoChk.jsp
//程序功能：个人续保催收通知书
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.xb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  	//接收信息
  	// 投保单列表
	LCPolSet tLCPolSet = new LCPolSet();
	LCPolSchema tLCPolSchema = new LCPolSchema();	
	String tCountSQL = request.getParameter("SQLCountHide");
	String tSQL = request.getParameter("SQLHide");
	boolean flag = false;

	if (!tSQL.equals("") )
	{
		System.out.println("tSQL:"+tSQL);
		flag = true;
	}

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tCountSQL );
		tVData.add( tSQL );
		tVData.add( tG );
		
		// 数据传输
		PRnewAutoDunUI tPRnewAutoDunUI   = new PRnewAutoDunUI();
		if (tPRnewAutoDunUI.submitData(tVData,"INSERT") == false)
		{
			int n = tPRnewAutoDunUI.mErrors.getErrorCount();
			//for (int i = 0; i < n; i++)
			//System.out.println("Error: "+tPRnewAutoDunUI.mErrors.getError(i).errorMessage);
			Content = " 生成续保通知书失败，原因是: " + tPRnewAutoDunUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPRnewAutoDunUI.mErrors;
		    //tErrors = tPRnewAutoDunUI.mErrors;
		    Content = " 生成续保通知书成功! ";
		    if (!tError.needDealError())
		    {                          
		    	//Content = "生成续保通知书成功!";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{    			      
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			   }

		    FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 生成续保通知书失败 ";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      Content = Content+" 原因:" + tError.getError(0).errorMessage.trim();
			    }
		    	FlagStr = "Fail";
		    }
		}
	}
	else
	{
		Content = "请选择投保单！";
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	<%
	//parent.fraInterface.fm.submit();
	%>
</script>
</html>
