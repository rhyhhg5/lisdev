<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GroupUWAutoChk.jsp
//程序功能：集体自动核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
System.out.println("Auto-begin:");
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  if(tG == null) 
  {
	System.out.println("session has expired");
	return;
   }
   
 	// 投保单列表
	LCGrpPolSet tLCGrpPolSet = new LCGrpPolSet();
	String tGrpProposalNo = request.getParameter("GrpProposalNo");
	String tGrpPrtNo = request.getParameter("GrpPrtNo");
	boolean flag = false;
	
	if (tGrpProposalNo != null && tGrpProposalNo != "")
	{
		System.out.println("GrpProposalNo:"+":"+tGrpProposalNo);
	  	LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
   		tLCGrpPolSchema.setGrpPolNo( tGrpProposalNo );
		tLCGrpPolSet.add( tLCGrpPolSchema );
   		flag = true;
	}
		
	
try{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLCGrpPolSet );
		tVData.add( tG );
		
		// 数据传输
		GrpUWAutoChkUI tGrpUWAutoChkUI   = new GrpUWAutoChkUI();
		if (tGrpUWAutoChkUI.submitData(tVData,"INSERT") == false)
		{
			int n = tGrpUWAutoChkUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			{
			  System.out.println("Error: "+tGrpUWAutoChkUI.mErrors.getError(i).errorMessage);
			  Content = " 自动核保失败，原因是: " + tGrpUWAutoChkUI.mErrors.getError(0).errorMessage;
			}
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tGrpUWAutoChkUI.mErrors;
		    System.out.println("tError.getErrorCount:"+tError.getErrorCount());
		    if (!tError.needDealError())
		    {                          
		    	Content = " 自动核保成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 自动核保失败，原因是:";
		    	int n = tError.getErrorCount();
    			if (n > 0)
    			{
			      for(int i = 0;i < n;i++)
			      {
			        //tError = tErrors.getError(i);
			        Content = Content.trim() +i+". "+ tError.getError(i).errorMessage.trim()+".";
			      }
			}
		    	FlagStr = "Fail";
		    }
		}
	}
	else
	{
		Content = "请选择保单！";
	}
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
  
%>                      
<html>
<script language="javascript">
    //top.close();
	//alert("<%=Content%>");
	//top.close();
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
