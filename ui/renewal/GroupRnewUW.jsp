<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
  //个人下个人
	//String tGrpPolNo = "00000000000000000000";
	String tContNo = "00000000000000000000";
	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var contNo = "<%=tContNo%>";          //个人单的查询条件.
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
</script>
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="GroupRnewUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../uw/GroupUWInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="../uw/GroupUWCho.jsp" method=post name=fmQuery target="fraSubmit">
    <!-- 集体查询条件 -->
    <TR  class= common>
      <TD  class= titles>
        集体投保单号码
      </TD>
      <TD  class= input>
        <Input class= common name=GrpProposalNo >
      </TD>
    </TR>
       <INPUT VALUE="查询" TYPE=button onclick="querygrp();">
       <INPUT type= "hidden" name= "Operator" value= "">
    <!-- 查询未过集体单（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 集体投保单查询结果
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanGrpGrid" onclick= "getPolGridCho();">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	
    <hr></hr>
    <Div id = "divPerson" style = "display: 'none'">
    <!-- 集体投保单查询结果部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 个人投保单查询结果
    		</td>
    	</tr>
    </table>
    
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" onclick= "getPolGridCho();">
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();">
      			
  	</div>
  </form>
  <form action="./GroupUWSave.jsp" method=post name=fm target="fraSubmit">
    <!-- 核保 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>核保信息：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            投保单号码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ProposalNo >
          </TD>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=RiskVersion >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=ManageCom >
          </TD>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntNo >
          </TD>
          <TD  class= title>
            投保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=AppntName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            被保人客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredNo >
          </TD>
          <TD  class= title>
            被保人姓名
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredName >
          </TD>
          <TD  class= title>
            被保人性别
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=InsuredSex >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            份数
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Mult >
          </TD>
          <TD  class= title>
            保费
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Prem >
          </TD>
          <TD  class= title>
            保额
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Amnt >
          </TD>
        </TR>
	</table>
	<table class= common border=0 width=100% >
          <INPUT VALUE="个单保单明细信息" TYPE=button onclick="showPolDetail();"> 
          <INPUT VALUE="个单既往投保信息" TYPE=button onclick="showApp();"> 
          <INPUT VALUE="个单以往核保记录" TYPE=button onclick="showOldUWSub();">
          <INPUT VALUE="个单最终核保信息" TYPE=button onclick="showNewUWSub();">
          <INPUT VALUE="个单体检资料" TYPE=button onclick="showHealth();"> 
          
    </table>
    <!-- 个人单核保结论 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>个人单核保结论：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            个人单核保结论
          </TD>
          <TD  class= input>
            <Input class="code" name=UWState ondblclick="return showCodeList('UWState',[this]);">
          </TD>
          <TD  class= title>
            个人单核保意见
          </TD>
          <TD  class= input>
            <Input class= common name=UWIdea >
          </TD>
    </table>
          <INPUT VALUE="个单核保确定" TYPE=button onclick="manuchk();">
    </Div>
          <p></p>
          <hr></hr>
          <INPUT VALUE="集体单最终核保信息" TYPE=button onclick= "showGNewUWSub();">
          <INPUT VALUE="集体问题件查询" TYPE=button onclick="QuestQuery();">
    <!-- 集体单核保结论 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>集体单核保结论：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>

				<TD  class= title8>集体单核保结论</TD>
				<TD  class= input8> <input class=codeno name=GUWState CodeData= "0|^1|拒保^4|通融承保^6|待上级核保^7|问题件录入^9|正常承保" ondblclick="return showCodeListEx('cond',[this,condName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('cond',[this,condName],[0,1],null,null,null,1);"><input class=codename name=condName></TD>


          <TD  class= title>
            集体单核保意见
          </TD>
          <TD  class= input>
            <Input class= common name=GUWIdea >
          </TD>
    </table>
    	  		
          <INPUT VALUE="集体单整单确认" TYPE=button onclick="gmanuchk();">
  <Input "hidden"  name=ProposalGrpContNo>
	<INPUT type= "hidden" name= "PrtNoHide" value= "">
	<INPUT type= "hidden" name= "Operator" value= "">

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
