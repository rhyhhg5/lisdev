<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>

<%
//程序名称：PolicyPrintQuery.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  // 保单信息部分
  LCPolSchema tLCPolSchema   = new LCPolSchema();

    tLCPolSchema.setPolNo(request.getParameter("PolNo"));
    tLCPolSchema.setRiskCode(request.getParameter("RiskCode"));
    tLCPolSchema.setRiskVersion(request.getParameter("RiskVersion"));
    tLCPolSchema.setManageCom(request.getParameter("ManageCom"));
    tLCPolSchema.setAgentCode(request.getParameter("AgentCode"));
    tLCPolSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLCPolSchema.setAgentGroup(request.getParameter("AgentGroup"));
	// 已签单的部分
    tLCPolSchema.setAppFlag( "1" ); 
    
  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLCPolSchema);

  // 数据传输
  ProposalQueryUI tProposalQueryUI   = new ProposalQueryUI();
	if (!tProposalQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tProposalQueryUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tProposalQueryUI.getResult();
		
		// 显示
		LCPolSet mLCPolSet = new LCPolSet(); 
		mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
		int n = mLCPolSet.size();
		if (n > 10)
			n = 10;
		for (int i = 1; i <= n; i++)
		{
		  	LCPolSchema mLCPolSchema = mLCPolSet.get(i);
		   	%>
		   	<script language="javascript">
		   		parent.fraInterface.fmSave.PolGrid1[<%=i-1%>].value="<%=mLCPolSchema.getPolNo()%>";
		   		parent.fraInterface.fmSave.PolGrid2[<%=i-1%>].value="<%=mLCPolSchema.getPrtNo()%>";
		   		parent.fraInterface.fmSave.PolGrid3[<%=i-1%>].value="<%=mLCPolSchema.getRiskCode()%>";
		   		parent.fraInterface.fmSave.PolGrid4[<%=i-1%>].value="<%=mLCPolSchema.getRiskVersion()%>";
		   		parent.fraInterface.fmSave.PolGrid5[<%=i-1%>].value="<%=mLCPolSchema.getAppntName()%>";
		   		parent.fraInterface.fmSave.PolGrid6[<%=i-1%>].value="<%=mLCPolSchema.getInsuredName()%>";
			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tProposalQueryUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

