//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var queryString="";
//查询条件验证
function checkQuery()
{
	if(fm.cusNo.value == ''&&fm.insNo.value == '')
	{
	  alert("请输入保单号或者客户号来进行查询");
	  return false;
	  
	}
	else
		return true;
}


//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  //	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	
  }
}

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
}

           
// 查询按钮
function easyQueryClick()
{
  if(	checkQuery()){
  var strSQL = "select distinct a.GrpContNo,a.GrpName,a.HandlerDate, "
  				 + "	(select min(payToDate) from LCCont where grpContNO = a.grpContNo), "
  				 + "	case a.AppFlag when '0' then '未签单' when '1' then '已签单' end, "
  				 + "	a.prem ,getUniteCode(a.AgentCode) "
		       + " from LCGrpCont a "
           + " where 1=1"
		       + getWherePart( 'a.GrpContNo ','insNo' ) 
		       + getWherePart( 'a.AppntNo ','cusNo' ) 
		       + " 	and a.AppFlag='1' "
		       + "    and not exists "
		       + "      (select 1 from LCGrpContState "
		       + "      where grpcontNo = a.grpcontNo and grppolNo in('000000', '00000000000000000000') "
		       + "        and (endDate is null or endDate > current Date)) "
		       + " group by a.GrpContNo,a.prem, a.GrpName,a.HandlerDate,a.AppFlag,a.AgentCode"
		       ;
		 turnPage.pageLineNum = 15;
	   turnPage.queryModal(strSQL, GrpInsListGrid); 
	   	if(GrpInsListGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
  	 return true;
  	}
  	else
  		return false;
}

//点击保单列表的响应事件
function insClickEvent()
{
   var  selNo=  GrpInsListGrid.getSelNo();
	 if (selNo==null)
	 {
		 alert("请选择一条记录");
	 }else
	 {
		var tGrpContNo=GrpInsListGrid.getRowColData(selNo-1,1);
		//查询帐户余额
		var accontSql = " select a.AccBala "
									+ " from LCAppAcc a, LCGrpCont b "
									+ " where a.CustomerNo = b.AppntNo and b.GrpContNo = '"
									+ tGrpContNo
									+ "'" 
		var mArray = easyExecSql(accontSql);
		if(mArray != null)
		{
			fm.AccBala.value = mArray[0][0];
		}
		else
			{
				fm.AccBala.value = "";
			}		
		

		//var strSQL = "select  a.GetNoticeNo,a.PayDate,a.SumDuePayMoney,b.SumActuPayMoney,c.dif,min(d.PayToDate),b.MakeDate,b.EnterAccDate,'' "
	  //           + " from  ljSpayb a,ljapay b,LCGrpCont c,LCCont d" 
		//           + " where  b.Incometype='1' AND a.OtherNoType='1'"
	  //           + " and b.IncomeNo='" +tGrpContNo + "'"
		//           + " and a.OtherNo=b.IncomeNo and b.IncomeNo=c.GrpContNo and d.GrpContNo=c.GrpContNo"
		//           + " group by a.GetNoticeNo,a.PayDate,a.SumDuePayMoney,b.SumActuPayMoney,c.dif,b.MakeDate,b.EnterAccDate"
	  //           ;
	  var strSQL = " select distinct a.GetNoticeNo, min(b.LastPayToDate), "
	  					 + " 		sum(b.sumDuePayMoney), sum(b.sumActUPayMoney), min(b.CurPayToDate),"
	  					 + " 		a.MakeDate,min(b.EnterAccDate), (select codeName from LDCode where codetype='dealstate' and code = a.dealstate) "
	             + " from ljspayb a, ljapayperson b "   
	             + " where a.OtherNo='" + tGrpContNo + "' " 
	             + " and a.OtherNoType='1' "
	             + " and a.dealState in( '1' , '5') " 
	             + " and  exists(select contNo from LCRnewStateLog where GrpContNo = a.OtherNo and state in('6')) "
	             + " and a.getNoticeNO = b.getNoticeNO "  
	             + " group by a.makeDate, a.sumDuePayMoney,a.GetNoticeNo,a.dealstate "
	             + " order by a.getnoticeno desc "
	             ; 
	  //var strSQL = "select a.getNoticeNO, (select min(lastPayToDate) from LJAPayPerson where getNoticeNO = a.getNoticeNO),"
	  //						+ " (select sum(SumDuePayMoney) from LJAPayGrp where getNoticeNO = a.getNoticeNO),"
	  //						+ "	(select sum(SumActuPayMoney) from LJAPayGrp where getNoticeNO = a.getNoticeNO),"
	  //						+ "	(select min(payToDate) from LJAPayPerson where getNoticeNO = a.getNoticeNO), "
	  //						+ "	a.MakeDate,d.EnterAccDate, a.dealState"
	  //						+ "from LJSPayB a ";
	  turnPage2.queryModal(strSQL, GrpPaymentGrid);
	if(GrpPaymentGrid.mulLineCount == 0)
  {
    alert("没有查询到数据");
    return false;
  }
	 }  
	 
}

//作废应收记录按钮
function cancelRecord()
{
	var  selNo = GrpPaymentGrid.getSelNo();
	if (selNo==null || selNo==0 )
	 {
		 alert("请选择一条应收记录");
		 return false;
	 }else
	{
		var tPayNo = GrpPaymentGrid.getRowColData(selNo - 1,1)
		fm.all('SubmitPayNo').value=tPayNo; 		
		var showStr="正在进行回退，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit(); //提交
	}
}