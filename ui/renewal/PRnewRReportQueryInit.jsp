<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorRReportQueryInit.jsp
//程序功能：生存调查报告查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
<%
  String tProposalNo = "";
  String tFlag = "";

  tProposalNo = request.getParameter("ProposalNo2");
  tFlag = request.getParameter("Flag");


  System.out.println("ProposalNo:"+tProposalNo);
  System.out.println("Flag:"+tFlag);


%>            
<script language="JavaScript">


// 输入框的初始化（单记录部分）
function initInpBox(tProposalNo,tFlag)
{ 
try
  {                                   
    fm.all('ProposalNo').value = tProposalNo;
    fm.all('IfReply').value = '';
    if (tFlag == "5")
    {
    	fm.all('BackObj').value = '1';
    }
    else
    {
    	fm.all('BackObj').value = '';
    }
    
    fm.all('ManageCom').value = '';
    fm.all('OManageCom').value = '';
    
    if (tFlag == "5")
    {
    	fm.all('OperatePos').value = '5';
    }
    else if(tFlag == "3"||tFlag == "1")
    {
    	fm.all('OperatePos').value = '1';	    
    }
    else if(tFlag == "4")
    {
    	fm.all('OperatePos').value = '1';
    }
    else
    {
    	fm.all('OperatePos').value = '';
    }
    fm.all('Content').value = '';
    fm.all('ReplyResult').value = '';
  }
  catch(ex)
  {
    alert("在RReportQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }   
}

function initForm(tProposalNo)
{
  try
  {	
	
	initQuestGrid();		
	initHide(tProposalNo);
	easyQueryClick(tProposalNo);	
	

  }
  catch(re)
  {
    alert("UWSubInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 责任信息列表的初始化
function initQuestGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="投保单号";    	//列名
      iArray[1][1]="180px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="被保人";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="录入人";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
                           
      iArray[4]=new Array();
      iArray[4][0]="录入日期";         			//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="回复人";         			//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="回复日期";         			//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=60;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="是否回复";         			//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=60;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
      iArray[8]=new Array();
      iArray[8][0]="流水号";         			//列名
      iArray[8][1]="0px";            		//列宽
      iArray[8][2]=60;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许            
      
      QuestGrid = new MulLineEnter( "fm" , "QuestGrid" ); 
      //这些属性必须在loadMulLine前                            
      QuestGrid.mulLineCount = 1;
      QuestGrid.displayTitle = 1;
      QuestGrid.canSel = 1;
      QuestGrid.loadMulLine(iArray);
      
      QuestGrid. selBoxEventFuncName = "easyQueryChoClick";
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initHide(tProposalNo)
{
	fm.all('ProposalNoHide').value=tProposalNo;		
}

function initCodeData(tProposalNo)
{
	if (tFlag == "5")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员";
		fm.all('OperatePos').CodeData = "0|^5|复核";
	}
	if (tFlag == "1")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|机构^3|保户";
		fm.all('OperatePos').CodeData = "0|^1|核保";
	}
	if (tFlag == "2")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员";
		fm.all('OperatePos').CodeData = "0|^5|复核^1|核保";
	}
	if (tFlag == "3")
	{
		fm.all('BackObj').CodeData = "0|^2|机构^3|保户";
		fm.all('OperatePos').CodeData = "0|^1|核保";
	}
	if (tFlag == "4")
	{
		fm.all('BackObj').CodeData = "0|^1|操作员^2|机构^3|保户";
		fm.all('OperatePos').CodeData = "0|^1|核保";
	}	
}

</script>


