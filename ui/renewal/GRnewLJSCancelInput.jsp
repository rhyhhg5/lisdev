<A HREF=""></A> <html> 
<%
//程序名称：GRnewLJSCancelInput.jsp
//程序功能：应收记录作废，对已经催收的集体保单进行作废操作
//创建日期：2018-06-26
//创建人  ：qqy
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	
        GlobalInput tGI = new GlobalInput();
        //PubFun PubFun=new PubFun();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("1"+tGI.Operator);
//	System.out.println("2"+tGI.ManageCom);
        String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        String SubDate=tD.getString(AfterDate);  
        String tSubYear=StrTool.getVisaYear(SubDate);
        String tSubMonth=StrTool.getVisaMonth(SubDate);
        String tSubDate=StrTool.getVisaDay(SubDate);                	               	
      //  String MaxManageCom=PubFun.RCh(tGI.ManageCom,"9",8);
      //  String MinManageCom=PubFun.RCh(tGI.ManageCom,"0",8);  	
%>
<head >
  <SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;
var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="GRnewLJSCancelInput.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="GRnewLJSCancelInputInit.jsp"%>
   <script type="text/javascript">
   function easyQueryClick2()
   {
   	
   	// 初始化表格
   	initGrpContGrid();

     var noName = " ";
     if(loadFlag == "WMD")
     {
       noName = " and exists (select 1 from LCCont "
   	     + "      where polType = '1' and appFlag = '1' and grpContNo = a.grpContNo) ";  //无名单
     }
     else
     {
       noName = " and not exists (select 1 from LCCont "
   	     + "      where polType = '1' and appFlag = '1' and grpContNo = a.grpContNo) ";  //无名单
     }
     
   		var strSQL = "select distinct a.GrpContNo,a.GrpName,a.CValiDate,min(b.PayToDate),'1已签单',c.SumDuePayMoney,getAgentName(a.AgentCode)"
   		   +" from LCGrpCont a,LCCont b,LJSPay c"
              + " where 1=1"
   		   + getWherePart( 'a.GrpContNo ','GrpContNo' ) 
   		   + getWherePart( 'a.AppntNo ','AppntNo' ) 
   		   + getWherePart( 'c.GetNoticeNo ','GetNoticeNo' )
   		   + noName
   		   + " and c.OtherNoType='1'"			   
   		   + " and a.GrpContNo=b.GrpContNo"
   		   + " and c.OtherNo=b.GrpContNo"
   		   + " and a.AppFlag='1' and b.AppFlag='1'"
   		   + " group by a.GrpContNo,a.GrpName,a.CValiDate,a.AppFlag,c.SumDuePayMoney,a.AgentCode"
   		;
   	      
   	turnPage.queryModal(strSQL, GrpContGrid); 
   	initGrpJisPayGrid();
   	
   }
   
   function afterLjsPayQuery()
   {
   	// 初始化表格
   	initGrpJisPayGrid();
     //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
     var  selNo=  GrpContGrid.getSelNo();
     var peoples2;
     if(loadFlag == "WMD")
     {
       peoples2 = " (select sum(peoples2Input) from LCNoNamePremTrace where GetNoticeNo = b.GetNoticeNo) ";
     }
     else
     {
       peoples2 = " (select peoples2 from LCGrpCont where grpContNo = b.otherNo) ";
     }
   	 if (selNo==null)
   	 {
   		 alert("请选择一条记录");
   	 }else
   	{
   		var tGrpContNo=  GrpContGrid.getRowColData(selNo-1,1);
   		var strSQL ="	select distinct b.GetNoticeNo,min(c.LastPayToDate),b.SumDuePayMoney," + peoples2 + ", b.MakeDate,(select b.codeName from LJSPayB a, LDCode b where a.dealState = b.code and b.codeType = 'dealstate' and b.GetNoticeNo=a.GetNoticeNo)"
   	       + " from  ljspay b,LJSPayPerson c where  b.othernotype='1'  "
   	       + " and b.OtherNo='" +tGrpContNo + "'"
   		   + " and c.GetNoticeNo=b.GetNoticeNo"
   	       + " and c.RiskCode  in (select RiskCode from LMRisk where RnewFlag='N' or CPayFlag != 'N')"
   		   + " group by b.GetNoticeNo,b.SumDuePayMoney,b.MakeDate,b.MakeDate,b.otherNo"
   	  ;
   	}  
   	 
   	
   	turnPage4.queryModal(strSQL, GrpJisPayGrid); 
   }
   
   function cancelRecord()
   {
   	var  selNo=  GrpJisPayGrid.getSelNo();
   	if (selNo==null || selNo==0 )
   	 {
   		 alert("请选择一条应收记录");
   		 return false;
   	 }else
   	{
   		var tNoticeNo = GrpJisPayGrid.getRowColData(selNo - 1,1)
   		fm.all('SubmitNoticeNo').value=tNoticeNo;
           fm.all('SubmitCancelReason').value=fm.all('cancelMode').value;
   		if (fm.all('SubmitCancelReason').value =='')
   		{
   			alert("请选择作废原因");
   			return false;

   		}
   		var showStr="正在作废应收纪录，请您稍候并且不要修改屏幕上的值或链接其他页面";
   		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   		fm.submit(); //提交
   	}
   }
   </script>
</head>

<body onload="initForm();">
<form name=fm action=./GRnewLJSCancelQuery.jsp target=fraSubmit method=post>
  <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
	     <tr class=common>
          <TD  class= title>
          客户号
          </TD>
          <TD  class= input>
            <Input class="common"  name=AppntNo  >
          </TD>
          <TD  class= title>
          保单号
          </TD>
          <TD  class= input>
            <Input class="common"  name=GrpContNo >
          </TD>  
		   <TD  class= title>
           应收记录号
          </TD>
          <TD  class= input>
            <Input class="common"  name=GetNoticeNo >
          </TD> 
		  <TD  class= input>
             <!-- <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick2();">  -->
             <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick2();"> 
          </TD>	  
		</tr>
  </table>          
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单列表
    		</td>
			<td class= titleImg><td>
			<td class= titleImg><td>
			<td class= titleImg><td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
   <center>    	
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
  </center>  	
  </div>
   <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			应缴记录：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpJisPayGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>     
	<center>      				
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</center>  
	</Div>	
	<br>
	 <table class= common>
    	<tr class= common>
        	<TD  class= title>作废原因</TD>
			  <TD  class= input>
			   <Input class="codeNo" name="cancelMode" CodeData="0|^0|客户终止缴费^1|暂缓收费" verify="作废原因|&code:cancelMode"  ondblclick="return showCodeListEx('cancelMode',[this,CancelReason],[0,1]);" onkeyup="return showCodeListEx('cancelMode',[this,CancelReason],[0,1]);"><Input class="codeName" name="CancelReason"  elementtype="nacessary" readonly>
			  </TD>			 
			<TD  ></TD>
			<TD ></TD>
			<TD ></TD>
    	</tr>
    </table>
	<br>
     <INPUT VALUE="作废应收记录" class = cssbutton TYPE=button onclick="cancelRecord();"> 
     <Input type=hidden name=SubmitNoticeNo> 
     <Input type=hidden name=SubmitCancelReason> 
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
