<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorReprintSava.jsp
//程序功能：保全人工核保补打通知书
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.xb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
<%
  //输出参数
  CErrors tError = null;
 String FlagStr = "Fail";
 String Content = "";
  GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
	if(tG == null) {
		out.println("session has expired");
		return;
	}
  	//接收信息
	TransferData tTransferData = new TransferData();
	LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
	String tPrtSeq = request.getParameter("PrtSeq");
	String tCode = request.getParameter("Code");
	String tPolNo = request.getParameter("PolNo");
	String tPrtNo = request.getParameter("PrtNo");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");	
	
	System.out.println("polno:"+tPolNo);
	System.out.println("tMissionID:"+tMissionID);
	System.out.println("tSubMissionID:"+tSubMissionID);
	boolean flag = true;
	if (!tPolNo.equals("")&& !tMissionID.equals("") && !tSubMissionID.equals(""))
	{
		//准备公共传输信息
		tLOPRTManagerSchema.setPrtSeq(tPrtSeq);
		tTransferData.setNameAndValue("PolNo",tPolNo);
		tTransferData.setNameAndValue("PrtNo",tPrtNo);
		tTransferData.setNameAndValue("MissionID",tMissionID);	
		tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
		tTransferData.setNameAndValue("LOPRTManagerSchema",tLOPRTManagerSchema);	
	}
	else
	{
		flag = false;
		Content = "数据不完整!";
	}	
	System.out.println("flag:"+flag);
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		String tOperate = new String();
		if(tCode.trim().equals("43"))	
	      tOperate = "0000000114";//打印体检通知书任务节点编码
	   if(tCode.trim().equals("44"))	
	      tOperate = "0000000116";  //打印面见通知书任务节点编码
	   if(tCode.trim().equals("45"))	
	      tOperate = "0000000115"; //打印核保通知书任务节点编码  
	    System.out.println("tOperate"+tOperate);  
		VData tVData = new VData();
		tVData.add( tTransferData);
		tVData.add( tG );
		
		// 数据传输
		PRnewManuUWWorkFlowUI tPRnewManuUWWorkFlowUI   = new PRnewManuUWWorkFlowUI();
		if (!tPRnewManuUWWorkFlowUI.submitData(tVData,tOperate))
		{
			int n = tPRnewManuUWWorkFlowUI.mErrors.getErrorCount();
			Content = " 补打通知书数据提交失败，原因是: " + tPRnewManuUWWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPRnewManuUWWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = "补打通知书数据提交成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 补打通知书数据提交失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		     }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
