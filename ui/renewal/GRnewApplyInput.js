//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;


//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}

function submitForm2()
{
  var i = 0;
  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm2.submit(); //提交

}

function submitForm3()
{
  var i = 0;
  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm3.submit(); //提交

}

function submitForm4()
{
  var i = 0;
  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm4.submit(); //提交

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

 
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function Step1_easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid1();
	//if(fm.all('GrpPolNo').value==null&&fm.all('PrtNo').value==null)
	//{
	//  alert("必须录入团体保单号或者印刷号");	
	//  return false;
	//}
	
	// 书写SQL语句
	k++;
	var strSQL = "";

	strSQL = "select grppolno,prtno,RiskCode,CValiDate,GrpName,peoples2 from lcgrppol where "+k+"="+k				 	
				 +" and appflag='1' "
				 + getWherePart( 'GrpPolNo','GrpPolNo' )
				 + getWherePart( 'PrtNo','PrtNo' )
				 + " and payintv!=-1"
				 + " and payenddate>=SYSDATE-30 "
				 + " and payenddate-30<=SYSDATE "
				 + " and GrpPolNo not in (select grppolno from LCGrpRnewLog where state <>'6' )"
				 + " and ManageCom like '" + ManageCom + "%%'"
				 + " order by prtno";
  
   //alert(strSQL);
   
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有符合条件的保单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpPolGrid1;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


// 查询按钮
function Step2_easyQueryClick()
{
	// 初始化表格
	initGrpPolGrid2();
	Step2_initGrpParm();
	k++;
	var strSQL = "";

	strSQL = "select grppolno,prtno,RiskCode,MakeDate,GrpName,peoples2,ChangeType,GrpProposalNo from LCGrpRnewLog where "+k+"="+k				 	
				 + getWherePart( 'GrpPolNo','GrpPolNo' )
				 + getWherePart( 'PrtNo','PrtNo' )
				 + " and State='0'"
				 + " and ManageCom like '" + ManageCom + "%%'"
				 + " order by prtno";
  
   //alert(strSQL);
   
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有符合条件的保单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = GrpPolGrid2;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


// 查询按钮
function Step2_easyQueryClick_Pol(GrpPolNo)
{
	
	// 初始化表格
	initPolGrid1();
	//Step2_initGrpParm();
	k++;
	var strSQL = "";

	strSQL = "select a.PolNo,a.InsuredName,a.InsuredSex,a.InsuredBirthday,a.InsuredAppAge from lcpol a  where "+k+"="+k	;			 	

	if(fm3.all('fm3_PolNo').value!=null&&fm3.all('fm3_PolNo').value!="")
    {
		strSQL=strSQL+" and a.PolNo='"+fm3.all('fm3_PolNo').value+"'"	;
    }
	if(fm3.all('fm3_InsuredName').value!=null&&fm3.all('fm3_InsuredName').value!="")
    {
		strSQL=strSQL+" and a.InsuredName='"+fm3.all('fm3_InsuredName').value+"'"	;
    }			 
				 strSQL=strSQL+ " and a.GrpPolNo='"+GrpPolNo+"'"				 				 
				              + " and a.ManageCom like '" + ManageCom + "%%'"			 
				              + " order by polno";
  
 //  alert(strSQL);
   
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有符合条件的保单！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid1;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function Step1_Save()
{

var num=GrpPolGrid1.getSelNo();
if(num==0)
{
 alert("请选择一条记录!");
 return false;	
}

if(fm.all('CValidate').value==null||fm.all('CValidate').value=='')
{
 alert("请录入生效日期!");
 return false;		
}

if(fm.all('InsuYear').value==null||fm.all('InsuYear').value=='')
{
 alert("请录入保险期间!");
 return false;		
}

var value=GrpPolGrid1.getRowColData(num-1,1);

fm.all('GrpPolNo_Select').value=value;
fm.all('Operate').value="INSERT";
submitForm();
Step1_easyQueryClick();
	
}

function Step2_DelApply()
{
	var num=GrpPolGrid2.getSelNo();
    if(num==0)
	{
 	 alert("请选择一条记录!");
 	 return false;	
	}
    var GrpProposalNo=GrpPolGrid2.getRowColData(num-1,8);
    fm.all('GrpPolNo_Select').value=GrpProposalNo;
	fm.all('Operate').value="DELETE";
    Step2_initGrpParm();
    submitForm();
    Step2_easyQueryClick();

}

function Step2_QueryParm()
{
	var num=GrpPolGrid2.getSelNo();
    if(num==0)
	{
 	 alert("请选择一条记录!");
 	 return false;	
	}
    var ChangeType=GrpPolGrid2.getRowColData(num-1,7);
    var GrpProposalNo=GrpPolGrid2.getRowColData(num-1,8);
    var GrpName=GrpPolGrid2.getRowColData(num-1,5);   
    var GrpPolNo=GrpPolGrid2.getRowColData(num-1,1);  
    if(ChangeType=='0')
    {
       divGrpParm.style.display='';		     
       fm2.all('GrpProposalNo').value=GrpProposalNo; 
       fm2.all('PolNo').value='00000000000000000000';
       fm2.all('GrpPolNo2').value=GrpPolNo;        
       var strSql = "select PremFlag,PremNum,AmntFlag,AmntNum,ManageFeeFlag,ManageFeeNum from LCGrpRnewPersonChangeParm where GrpProposalNo= '" + GrpProposalNo + "' and PolNo='00000000000000000000'";
       var arrCardRisk = easyExecSql(strSql);
       if (arrCardRisk==null) return;
       fm2.all('PremFlag').value=arrCardRisk[0][0];
       fm2.all('PremNum').value=arrCardRisk[0][1];
       fm2.all('AmntFlag').value=arrCardRisk[0][2];
       fm2.all('AmntNum').value=arrCardRisk[0][3];
       fm2.all('ManageFeeFlag').value=arrCardRisk[0][4];
       fm2.all('ManageFeeNum').value=arrCardRisk[0][5];      
    }
    else
    {
       divPolParm.style.display='';		     	
       Step2_easyQueryClick_Pol(GrpPolNo);
       fm3.all('GrpProposalNo').value=GrpProposalNo; 
       fm3.all('GrpPolNo2').value=GrpPolNo;
       fm3.all('PolNo').value="";
	     	
    }

}

function Step2_initGrpParm()
{      
       fm2.all('GrpProposalNo').value=""; 
       fm2.all('PolNo').value="";
       fm2.all('GrpPolNo2').value="";   
       fm2.all('Operate').value="";
       divGrpParm.style.display='none';	
       divPolParm.style.display='none';
}


function Step2_SaveGrpParm()
{		
	
	if(fm2.all('GrpProposalNo').value=="")
	{
		alert("请先选择一条数据"); 
		return false;
	}

    if(fm2.all('PremFlag').value!='0')
    {
		if(fm2.all('PremNum').value==null||fm2.all('PremNum').value=='')
		{
			alert("请录入保费变动量");
			return ;
		}    	
    }
        if(fm2.all('AmntFlag').value!='0')
    {
		if(fm2.all('AmntNum').value==null||fm2.all('AmntNum').value=='')
		{
			alert("请录入保额变动量");
			return ;			
		}    	
    }
        if(fm2.all('ManageFeeFlag').value!='0')
    {
		if(fm2.all('ManageFeeNum').value==null||fm2.all('ManageFeeNum').value=='')
		{
			alert("请录入管理费变动量");
			return ;
		}    	
    }     
	fm2.all('Operate').value="INSERT||UPDATE";
	 
	submitForm2();

}

function Step2_SavePolParm()
{		

    if(fm3.all('PremFlag').value!='0')
    {
		if(fm3.all('PremNum').value==null||fm3.all('PremNum').value=='')
		{
			alert("请录入保费变动量");
			return ;
		}    	
    }
        if(fm3.all('AmntFlag').value!='0')
    {
		if(fm3.all('AmntNum').value==null||fm3.all('AmntNum').value=='')
		{
			alert("请录入保额变动量");
			return ;			
		}    	
    }
        if(fm3.all('ManageFeeFlag').value!='0')
    {
		if(fm3.all('ManageFeeNum').value==null||fm3.all('ManageFeeNum').value=='')
		{
			alert("请录入管理费变动量");
			return ;
		}    	
    }     
	fm3.all('Operate').value="INSERT||UPDATE";
	fm3.all('PolNo').value=""; 
	submitForm3();

}


function Step2_SaveDefaultPolParm()
{		
	fm3.all('PolNo').value='00000000000000000000';
	       
    if(fm3.all('PremFlag').value!='0')
    {
		if(fm3.all('PremNum').value==null||fm3.all('PremNum').value=='')
		{
			alert("请录入保费变动量");
			return ;
		}    	
    }
        if(fm3.all('AmntFlag').value!='0')
    {
		if(fm3.all('AmntNum').value==null||fm3.all('AmntNum').value=='')
		{
			alert("请录入保额变动量");
			return ;			
		}    	
    }
        if(fm3.all('ManageFeeFlag').value!='0')
    {
		if(fm3.all('ManageFeeNum').value==null||fm3.all('ManageFeeNum').value=='')
		{
			alert("请录入管理费变动量");
			return ;
		}    	
    }     
	fm3.all('Operate').value="INSERT||UPDATE";
    fm3.all('PolNo').value="00000000000000000000"; 
	submitForm3();

}

function Step3_SavePolApply()
{
	
	if(fm4.all('GrpPolNo').value==null||fm4.all('GrpPolNo').value=="")
    {
			alert("请录入团体保单号");
			return ;  	
    }
    	submitForm4();
}
