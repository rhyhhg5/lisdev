//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var turnPage4 = new turnPageClass();
var turnPage1 = new turnPageClass();
var tInNoticeNo ="";
var SqlPDF = "";

//提交，保存按钮对应操作
function submitForm()
{
  //提交前的检验
  if(beforeSubmit())
  {
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  //	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	

//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
  resetForm();
   
}




//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false");
     
}
 
//提交前的校验、计算  
function CheckGrpPolNo()
{
    var tSel = GrpContGrid.getSelNo();	
    if( tSel == 0 || tSel == null )
    {
	alert( "请先选择一条记录，再点击'保费转出'按钮。" );
	return false;
    }
    var tRow = GrpContGrid.getSelNo() - 1; 
    //说明应该在这里把窗体相关值填充！杨红于2005-07-19注释

    fm.all('ProposalGrpContNo').value=GrpContGrid.getRowColData(tRow, 1);
    return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

          
         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function CheckDate()
{
  if(!isDate(fmMulti.all('StartDate').value)||!isDate(fmMulti.all('EndDate').value))  
    return false;
 
  //if BeginDate>EndDate return 1 
  var flag=compareDate(fmMulti.all('StartDate').value,fmMulti.all('EndDate').value);
  if(flag==1)
    return false;

  return true;	
	
}

/*********************************************************************
 *  查询应收纪录信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 初始化表格
	initGrpJisPayGrid();
	// 初始化表格
	initGrpContGrid();
	// 初始化表格
   initSpecGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var tempCurrentTime=CurrentTime; //临时变量
	var tempSubTime=SubTime;  //临时变量
	if(fm.all("StartDate").value!="")
	{
		//alert(fm.all("StartDate").value);
		CurrentTime=fm.all("StartDate").value;
	}
	if(fm.all("EndDate").value!="")
	{
		SubTime=fm.all("EndDate").value;
	}	   
	
	if(fm.all('ManageCom').value < managecom)
	{
		alert("请选择下级科目");
		return false;
	}	   
	
	
	var strSQL = "select distinct a.SerialNo,a.MakeDate,a.Operator,ShowManageName(a.ManageCom),'',sum(a.SumDuePayMoney)  from LJSPayB a where  "
			 + "  a.othernotype='1' " 
             + " and  exists(select contNo from LCRnewStateLog where GrpContNo = a.otherNo ) "
			 + " and a.MakeDate>='"+CurrentTime+"' and a.MakeDate<='"+SubTime+"'"
			 + getWherePart( 'a.ManageCom', 'ManageCom','like' )
			 + getWherePart( 'a.Operator','AgentCode' )
			 + dealWMD.replace("a.grpContNo", "a.otherNo")
			 + " group by  a.SerialNo,a.MakeDate,a.Operator,a.ManageCom"
			 + " order by a.SerialNo desc"
			;
	turnPage.queryModal(strSQL, GrpJisPayGrid); 
	contCurrentTime=CurrentTime;
	contSubTime=SubTime;
	fm.all("StartDate").value=contCurrentTime;   //Yangh添加，目的是将该值传入bl层做校验动作！
	fm.all("EndDate").value=contSubTime;
	CurrentTime=tempCurrentTime;  //杨红于2005-07-16添加，恢复初始变量CurrentTime
	SubTime=tempSubTime;          //杨红于2005-07-16添加，恢复初始变量SubTime  
	
}

/*********************************************************************
 *  查询应收团体保单信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function contQueryClick()
{
	// 初始化表格
	initGrpContGrid();
	// 初始化表格
   initSpecGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
	var rowNum=GrpJisPayGrid.mulLineCount;
	var tCount = 0;
	
	if(GrpJisPayGrid.mulLineCount == 0)
	{
	  alert("请先查询批次信息");
	  return false;
	}
    for(var i=0;i<rowNum;i++)
   {                                                                   
       if(GrpJisPayGrid.getChkNo(i))
       {
		   tCount = tCount+1;

		   if (tCount == 1)
		   {
			   tInNoticeNo = "'" + GrpJisPayGrid.getRowColData( i , 1 )+ "'";

		   }else
		   {
               tInNoticeNo = tInNoticeNo + ",'" + GrpJisPayGrid.getRowColData( i , 1 )+ "'";
		   }
       }
   }
   if(tInNoticeNo == "")
   {
      alert("请选择应收纪录");
      return false;
   }
   
    var strState = fm.all('DealState').value //催收状态 5：全部
    
    if (strState=='5')
	{
	    var strSQL = "select  a.GrpContNo,a.PrtNo,a.GrpName,'',a.CValiDate,a.SumPrem,value((select sum(abs(sumDuePayMoney)) from LJSPayGrpB where getNoticeNo = c.getNoticeNo and payType='YEL'), 0),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),db2inst1.getUniteCode(a.AgentCode),b.GetNoticeNo,"+
			"(select codename  from ldcode where codetype='dealstate' and code=b.dealstate), (select sum(Peoples2Input) from LCNoNamePremTrace where getNoticeNo = b.getNoticeNo) "
			+ "from LCGrpCont a,ljspayB b ,ljspaygrpB c where  "
			 + " a.GrpContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo and c.GrpContNo=b.otherno" 
             + " and  exists(select contNo from LCRnewStateLog where GrpContNo = a.GrpContNo ) "
			 + " and  b.SerialNo in (" + tInNoticeNo + ")"
			 + dealWMD
			 + " group by a.appntNo,a.GrpContNo,c.getNoticeNo,a.PrtNo,a.GrpName,a.CValiDate,a.SumPrem,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate"
       + " order by b.GetNoticeNo desc";
	}else
	{
		var strSQL = "select  a.GrpContNo,a.PrtNo,a.GrpName,'',a.CValiDate,a.SumPrem,value((select sum(abs(sumDuePayMoney)) from LJSPayGrpB where getNoticeNo = c.getNoticeNo and payType='YEL'), 0),b.SumDuePayMoney,min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),db2inst1.getUniteCode(a.AgentCode),b.GetNoticeNo,"
		   +"(select codename  from ldcode where codetype='dealstate' and code=b.dealstate), (select sum(PeoPles2Input) from LCNoNamePremTrace where getNoticeNo = b.getNoticeNo) "
		   + "from LCGrpCont a,ljspayB b,ljspaygrpB c where  "
			 + " a.GrpContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo and c.GrpContNo=b.otherno" 
			  + " and  exists(select contNo from LCRnewStateLog where GrpContNo = a.GrpContNo ) "
			 + " and  b.SerialNo in (" + tInNoticeNo + ")"
			 + " and  b.DealState='" +strState+ "' "
			 + dealWMD
			 + " group by a.appntNo, a.GrpContNo,c.getNoticeNo,a.PrtNo,a.GrpName,a.CValiDate,a.SumPrem,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate"
       + " order by b.GetNoticeNo desc";
	}
	SqlPDF = strSQL;
	turnPage1.queryModal(strSQL, GrpContGrid); 

}

/*********************************************************************
 *  查询个案应收纪录信息、
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function singleQueryClick()
{
  
  //将批次号设定为空串，预防打印清单是得到早些时候查询的批次信息
  tInNoticeNo = "";
  
	// 初始化表格
	initGrpJisPayGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收
   initSpecGrid();
	// 初始化表格
	initGrpContGrid();
  //新添加的选择 开始时间 ，终止时间 的窗体 支持选择任意时间范围的催收

  // DealState==3 全部应收记录
    var strState = fm.all('DealState').value //催收状态 3：全部
    if (strState=='5')
	{
        var strSQL = "select  a.GrpContNo,a.PrtNo,a.GrpName,'',a.CValiDate,a.SumPrem,value((select sum(sumDuePayMoney) from LJSPayGrpB where getNoticeNo = c.getNoticeNo and payType='YEL'), 0),(select sum(abs(sumDuePayMoney)) from LJSPayGrpB where getNoticeNo = c.getNoticeNo and payType='ZC'),min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),db2inst1.getUniteCode(a.AgentCode),b.GetNoticeNo,"
        +"(select codename  from ldcode where codetype='dealstate' and code=b.dealstate), (select sum(peoples2Input) from LCNoNamePremTrace where getNoticeNo = b.getNoticeNo) "
	      + " from LCGrpCont a,ljspayB b,ljspayGrpB c where 1=1 "
			  + " and   b.othernotype='1' and a.GrpContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo and c.GrpContNo=b.otherno" 
			  + " and a.ManageCom like '" + managecom + "%%' "
             + " and  exists(select contNo from LCRnewStateLog where GrpContNo = a.GrpContNo ) "
			  + getWherePart( 'a.GrpContNo','ProposalGrpContNo' )
			  + getWherePart( 'b.AppntNo','AppNo' )
			  + dealWMD
			  + " group by a.appntNo, a.GrpContNo,c.getNoticeNo,a.PrtNo,a.GrpName,a.CValiDate,a.SumPrem,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate"
        + " order by b.GetNoticeNo desc";
	}else
	{
		 var strSQL = "select  a.GrpContNo,a.PrtNo,a.GrpName,'',a.CValiDate,a.SumPrem,value((select sum(sumDuePayMoney) from LJSPayGrpB where getNoticeNo = c.getNoticeNo and payType='YEL'), 0),(select sum(abs(sumDuePayMoney)) from LJSPayGrpB where getNoticeNo = c.getNoticeNo and payType='ZC'),min(c.LastPayToDate),(select codename from ldcode where codetype='paymode' and code=a.PayMode),ShowManageName(a.ManageCom),db2inst1.getUniteCode(a.AgentCode),b.GetNoticeNo,"
		    +"(select codename  from ldcode where codetype='dealstate' and code=b.dealstate), (select sum(peoples2Input) from LCNoNamePremTrace where getNoticeNo = b.getNoticeNo) "
	      + " from LCGrpCont a,ljspayB b,ljspayGrpB c where 1=1 "
			  + " and   b.othernotype='1' and a.GrpContNo =b.OtherNo and c.GetNoticeNo=b.GetNoticeNo and c.GrpContNo=b.otherno" 
			  + " and  b.DealState='" +strState+ "'"
			  + " and a.ManageCom like '" + managecom + "%%' "
		      + " and  exists(select contNo from LCRnewStateLog where GrpContNo = a.GrpContNo ) "
			  + getWherePart( 'a.GrpContNo','ProposalGrpContNo' )
			  + getWherePart( 'b.AppntNo','AppNo' )
			  + dealWMD
			  + " group by a.appntNo, a.GrpContNo,c.getNoticeNo,a.PrtNo,a.GrpName,a.CValiDate,a.SumPrem,b.SumDuePayMoney,a.ManageCom,a.AgentCode,b.GetNoticeNo,a.PayMode,b.dealstate"
        + " order by b.GetNoticeNo desc";
	}
	SqlPDF = strSQL;
	turnPage1.queryModal(strSQL, GrpContGrid); 
	
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		var cGrpContNo = GrpContGrid.getRowColData( tSel - 1, 1 );
		try
		{
			//window.open("./GrpPolQuery.jsp");
			window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ cGrpContNo+"&ContType=1");
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
	}
}
function getpoldetail()//被选中后显示的集体险种应该是符合条件的。++
{
    var arrReturn = new Array();
	var tSel = GrpContGrid.getSelNo();	
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
	}	
	else
	{
		getPolInfo();
		//getSpecInfo();
    }  	                                          
                         	               	
}

//被选中后显示的集体险种应该是符合条件的。++
function getPolInfo()
{
	  var tRow = GrpContGrid.getSelNo() - 1;	        
		var tGrpContNo=GrpContGrid.getRowColData(tRow,1);  
		fm.all('ProposalGrpContNo').value = tGrpContNo;
		var strSQL = "select a.contplancode ,"
		//+" (select count(InsuredNo)  from lcinsured where grpcontno=a.grpcontno and contplancode=a.contplancode),"
		+" count(1) ,"
		+ "(select riskname  from lmrisk where riskcode=b.riskcode) , b.riskcode , "
		+ "	 (select codeName from LDCode where codeType = 'payintv' and code = char(b.PayIntv)) ," 
		//+ "(select sum(prem) from lcpol" 
    //+ " where  grppolno = b.grppolno and contplancode = a.contplancode and riskcode = b.riskcode), "
    + " sum(b.prem), "
		+ " b.CValiDate ,b.PaytoDate, (select cinvalidate from lcgrpcont where grpcontno=a.grpcontno) "  
		+ "  from lccontplanRisk a,lcpol b where 1=1"
		+ "  and a.grpcontno=b.grpcontno"
		+ "  and a.riskCode = b.riskCode "
		+ " and a.contplancode<>'11'"
		+ " and a.contplancode = b.contplancode  "
		+ "  and b.grpcontno='" + tGrpContNo + "' "		
	//	+ "  and b.PayIntv<>0  "	
        + " and  exists(select contNo from LCRnewStateLog where GrpPolNo = b.GrpPolNo ) "
		+ "  and b.managecom like '"+managecom+"%%'"
	//	+ "  and b.RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y') " 续期
	//	+ "and exists (select riskcode from lmrisk WHERE rnewFlag!='N' and lmrisk.riskcode=b.riskcode) "
   //     + "and exists (select riskCode from LMRiskApp where riskCode = b.riskCode and riskType5 = '2')"
		+ " group by  a.contplancode, b.CValiDate ,b.PaytoDate,a.grpcontno, b.riskcode,b.PayIntv "
		+ " with ur"

		;
	
      turnPage2.queryModal(strSQL, GrpPolGrid); 
        
      if(fm.loadFlag.value == "WMD")
      {
        for(var i = 0; i < GrpPolGrid.mulLineCount; i++)
        {
          var sql = "  select peoples2 from LCContPlan "
                    + "where grpContNo = '" + tGrpContNo + "' "
                    + "   and contPlanCode = '" + GrpPolGrid.getRowColData(i, 1) + "' ";
          var rs = easyExecSql(sql);
          if(rs && rs[0][0] != "" && rs[0][0] != "null")
          {
            GrpPolGrid.setRowColData(i, 2, rs[0][0]);
          }
        }
      }
}

//被选中后显示的集体险种应该是符合条件的。++
function getSpecInfo()
{
	 var tRow = GrpContGrid.getSelNo() - 1;	        
	 var tGrpContNo=GrpContGrid.getRowColData(tRow,1);  
	 fm.all('ProposalGrpContNo').value = tGrpContNo;
	 var strSQL = "select RiskCode,GrpPolNo from LCGrpPol where GrpContNo='"+ tGrpContNo+"'"	           
		 + " and PaytoDate>='"+contCurrentTime+"' and PaytoDate<='"+contSubTime+"' and PaytoDate<PayEndDate "
		 +" and managecom like '"+managecom+"%%'"
		 + " and PayIntv>0 and appflag='1'"
		 + " and GrpPolNo not in (select GrpPolNo from LJSPayGrpB)"
		 + " and RiskCode  in (select RiskCode from LMRiskPay where UrgePayFlag='Y')"
	 turnPage3.queryModal(strSQL, SpecGrid); 
   
}

//打印应收记录清单
function printList()
{
	if( GrpContGrid.mulLineCount == 0)
	{
		alert("没有应收团体保单");
		return false;
	}
	if (tInNoticeNo == null || tInNoticeNo == "" )
	{
		//个案查询应收明细
		window.open("../operfee/GrpDueFeeListPrint.jsp?InNoticeNo=1&GrpContNo="+ fm.all('ProposalGrpContNo').value+"&AppNo="+ fm.all('AppNo').value+"&DealState="+ fm.all('DealState').value + "&loadFlag=" + fm.loadFlag.value, "printLists");
	}else
	{
	    window.open("../operfee/GrpDueFeeListPrint.jsp?InNoticeNo="+ tInNoticeNo + "&loadFlag=" + fm.loadFlag.value, "printLists");
	}
	

}

//催收完成后打印通知书
function printNotice()
{
	var tRow = GrpContGrid.getSelNo() ;	
	if (tRow == 0 || tRow == null)
	{
		alert("请选择一个保单");
	}else{
		 var tGetNoticeNo=GrpContGrid.getRowColData(tRow - 1,13);  
		 window.open("../operfee/GrpPrintSel.jsp?GetNoticeNo="+ tGetNoticeNo);
	}
}


//打印PDF前往打印管理表插入一条数据
function printInsManage()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}

	var tGetNoticeNo=GrpContGrid.getRowColData(tChked[0],13);  
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '91' and standbyflag2='"+tGetNoticeNo+"'");
	
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		fm.all('GetNoticeNo').value = tGetNoticeNo;
		fm.action="./GrpDueFeeInsForPrt.jsp";
		fm.submit();
	}
	else
	{
		//window.open("../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
		fm.action = "../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
		fm.submit();
	}
}

function printPDF()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}
	var tGetNoticeNo=GrpContGrid.getRowColData(tChked[0],13);  
	var PrtSeq = easyExecSql(" select distinct prtseq from LOPRTManager where code = '91' and standbyflag2='"+tGetNoticeNo+"'");
	if(PrtSeq==null || PrtSeq=="" || PrtSeq=="null")
	{
		alert("提取数据失败-->PrtSeq为空");
		return false;
	}
	//window.open("../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
	fm.action = "../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq;
	fm.submit();
}


function printInsManageBat()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.action="./GrpDueFeeAllBatPrt.jsp?strsql="+SqlPDF;
		fm.submit();
	}
	else
	{
		fm.action="./GrpDueFeeBatPrt.jsp";
		fm.submit();
	}
}
//打印PDF前往打印管理表插入一条数据
function newprintInsManage()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行查看");
		return false;	
	}

	var tGetNoticeNo=GrpContGrid.getRowColData(tChked[0],13);  
		//window.open("../uw/PrintPDFSave.jsp?Code=091&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&PrtNo="+tGetNoticeNo+"&PrtSeq="+PrtSeq );	
		fm.action = "../uw/PDFPrintSave.jsp?Code=91&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+tGetNoticeNo;
		fm.submit();
}
////PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}
function newprintInsManageBat()
{
	if (GrpContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < GrpContGrid.mulLineCount; i++)
	{
		if(GrpContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}

	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	if(tChked.length == 0)
	{
		fm.action="./GrpDueFeeAllBatPrt.jsp?strsql="+SqlPDF;
		fm.submit();
	}
	else
	{
		fm.action="./GrpDueFeeBatPrt.jsp";
		fm.submit();
	}
}