<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuHealthQChk.jsp
//程序功能：保全人工核保体检资料查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
 	if(tG == null) {
		out.println("session has expired");
		return;
	}
 
  	//接收信息
  	// 投保单列表
	LPPENoticeSchema tLPPENoticeSchema = new LPPENoticeSchema();		
	String tPolNo = request.getParameter("PolNo");
	String tEdorNo = request.getParameter("EdorNo");
	String tInsureNo = request.getParameter("InsuredNoHide");
	String tHospital = request.getParameter("Hospital");
	String tIfEmpty = request.getParameter("IfEmpty");
	String tEDate = request.getParameter("EDate");
	String tNote = request.getParameter("Note");
	String tRemark = request.getParameter("Content");
	
	String tSerialNo[] = request.getParameterValues("HealthGridNo");
	String thealthcode[] = request.getParameterValues("HealthGrid1");
	String thealthname[] = request.getParameterValues("HealthGrid2");
	String tIfCheck[] = request.getParameterValues("HealthGrid3");
	//String tChk[] = request.getParameterValues("InpHealthGridChk");
	
	System.out.println("polno:"+tPolNo);
	System.out.println("hospital:"+tHospital);
	System.out.println("note:"+tNote);
	System.out.println("ifempty:"+tIfEmpty);
	System.out.println("insureno:"+tInsureNo);
	System.out.println("EDATE:"+tEDate);
	
	boolean flag = true;
	int ChkCount = 0;
	if(tSerialNo != null)
	{		
		ChkCount = tSerialNo.length;
	}
	System.out.println("count:"+ChkCount);
	if (ChkCount == 0 || tPolNo.equals("") || tInsureNo.equals("")||tRemark.equals(""))
	{
		Content = "数据不完整!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{
 		    //体检资料一
		    tLPPENoticeSchema.setPolNo(tPolNo);	 
		    tLPPENoticeSchema.setEdorNo(tEdorNo);	   		
	    	tLPPENoticeSchema.setPEAddress(tHospital);
	    	tLPPENoticeSchema.setPEDate(tEDate);
	    	tLPPENoticeSchema.setPEBeforeCond(tIfEmpty);
	    	tLPPENoticeSchema.setRemark(tNote);
	    	tLPPENoticeSchema.setInsuredNo(tInsureNo);
	    	tLPPENoticeSchema.setPEResult(tRemark);	    		
	}
	
	System.out.println("flag:"+flag);
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tLPPENoticeSchema);
		tVData.add( tG );		
		// 数据传输
		PEdorUWAutoHealthQUI tPEdorUWAutoHealthQUI   = new PEdorUWAutoHealthQUI();
		if (tPEdorUWAutoHealthQUI.submitData(tVData,"UPDATA") == false)
		{
			int n = tPEdorUWAutoHealthQUI.mErrors.getErrorCount();
			Content = " 自动核保失败，原因是: " + tPEdorUWAutoHealthQUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPEdorUWAutoHealthQUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 保全人工核保体检结果确认成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 保全人工核保体检结果确认失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		    }
		}
	} 
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
