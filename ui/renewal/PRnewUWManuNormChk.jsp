<%
//程序名称：PRnewUWManuNornChk.jsp
//程序功能：续保人工核保确认
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.xb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
 //接收信息，并作校验处理。
  String FlagStr = "Fail";
  String Content = "";
  String transact = "";
  String mContType = "";
  CErrors tError = null;
  boolean flag = true ;
  String tPolNo = request.getParameter("ProposalNo");
  String tPrtNo = request.getParameter("PrtNo");
  String tMissionID = request.getParameter("MissionID");
  String tSubMissionID = request.getParameter("SubMissionID");
  String tUWIdea = request.getParameter("UWIdea");
  String tUWState= request.getParameter("edoruwstate");
  String tUWDelay = request.getParameter("UWDelay");
  String tAppUser= request.getParameter("UWPopedomCode");
  TransferData tTransferData = new TransferData();
	   
   GlobalInput tGlobalInput = new GlobalInput();
  tGlobalInput = (GlobalInput)session.getValue("GI");
  
 System.out.println("-----"+tGlobalInput.Operator);
 System.out.println("-----tPolNo"+tPolNo);
 System.out.println("-----tPrtNo"+tPrtNo);
 System.out.println("-----tUWIdea"+tUWIdea);
  //输出参数 
  //个人批改信息
  if (tUWIdea == "" || tUWIdea.equals("") || tPolNo.equals("") || tPrtNo.equals(""))
	{
		Content = "条件录入不完整!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{	    	
		 // 准备传输工作流数据 VData
		 LCUWMasterSchema tLCUWMasterSchema = new LCUWMasterSchema();		
		 tLCUWMasterSchema.setProposalNo(tPolNo);
		 tLCUWMasterSchema.setUWIdea(tUWIdea);
		 if(tUWDelay != null && !tUWDelay.equals(""))
		 tLCUWMasterSchema.setPostponeDay(tUWDelay);
		 tLCUWMasterSchema.setPassFlag(tUWState);
		 //tLPUWMasterMainSchema.setAppGrade(tAppUser);//通过指定上报核保师编码
		 
		 // 准备传输工作流数据 VData
    	 tTransferData.setNameAndValue("PolNo",tPolNo);
	    tTransferData.setNameAndValue("PrtNo",tPrtNo);
	    tTransferData.setNameAndValue("AppUser",tAppUser);
	    tTransferData.setNameAndValue("MissionID",tMissionID);
	    tTransferData.setNameAndValue("SubMissionID",tSubMissionID);
	    tTransferData.setNameAndValue("LCUWMasterSchema",tLCUWMasterSchema);
	    flag = true;
	    
	}	 	

    if (flag == true)
  	{
     // 准备传输数据 VData  
  	 VData tVData = new VData();   
	 tVData.add(tGlobalInput);
	 tVData.add(tTransferData);	
	 // 数据传输
	PRnewManuUWWorkFlowUI tPRnewManuUWWorkFlowUI   = new PRnewManuUWWorkFlowUI();
	if (tPRnewManuUWWorkFlowUI.submitData(tVData,"0000000110") == false)
	 {
		int n = tPRnewManuUWWorkFlowUI.mErrors.getErrorCount();
		Content = " 续保核保确认失败，原因是:";
		Content = Content + tPRnewManuUWWorkFlowUI.mErrors.getError(0).errorMessage;
		FlagStr = "Fail";
	 }
	 //如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "Fail")
	{
		tError = tPRnewManuUWWorkFlowUI.mErrors;
		if (!tError.needDealError())
		{                          
		    Content = " 续保核保确认成功! ";
		    FlagStr = "Succ";
		}
	   else                                                                           
		{
		    Content = " 续保核保确认失败，原因是:" + tError.getFirstError();
		    FlagStr = "Fail";
		 }
	 }
	}

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>");
</script>
</html>

