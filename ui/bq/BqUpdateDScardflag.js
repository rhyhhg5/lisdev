var arrDataSet
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();
var PolNo;

window.onfocus = myonfocus;

// 查询按钮
function easyQueryClick() {
	initPolGrid();// 这是那个 XxxInit.jsp 中初始化 MulLine 的方法
	if (!verifyInput2())// 通用录入校验:检验之后将光标置于错误出,并且颜色变黄(?没有参数怎么校验的?)
		return false;
	if (fm.ContNo.value == "") {// 至少有一个是不为空的
		alert("请输入保单号！");
		return false;
	}
	// 发现一个小经验,要判断哪个值是不是空的时候,先声明一下,然后赋值"",然后判断是不是"",不用再去判断null
	var strSQL = "";
	// 这里的 sql 的参数使用 getWherePart('字段名','要传入的值'[,'操作符']),还是纳闷?没有参数怎么取得值,难道和 Struts2 差不多?
	strSQL = " select a.contno, " +
			" (select codename from ldcode where codetype='paymode' and code=a.paymode), " +
			" a.paytodate, " +
			" (select codename from ldcode where codetype='stateflag' and code=a.stateflag), " +
			" a.cardflag " +
			" from lccont a " +
			" where 1=1 " +
			" and a.conttype='1' " +
			" and a.appflag='1' " +
			" and a.cardflag in ('0','b') "
			+ getWherePart('a.contno', 'ContNo');
	// 一个页面要显示的记录数
	turnPage.pageLineNum = 50;
	// easyQueryVer3的查询结果 = 输入一个 SQL 语句,返回约定格式的查询结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSQL);
	// 用到这么多,为啥不提成一个变量呢
	if (!turnPage.strQueryResult) {
		alert("没有查询到要处理的业务数据！");
		return false;
	}

	// decodeEasyQueryResult 拆分后的二维数组 = 清空数据容器,返回一个空数组(两个不同查询共用一个 turnPage 对象时必须使用,容错)
	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	// 将约定格式的字符串拆分成二维数组,查询成功则拆分字符串,失败返回二维数组,这里的参数是 easyQueryVer3 的查询结果(easyQueryVer3 的查询结果是一个字符串)
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	// 设置初始化过的 MULTILINE 对象,PolGrid 为在初始化页中定义的全局变量,应该来自 initPolGrid()
	turnPage.pageDisplayGrid = PolGrid;
	// 保存SQL语句
	turnPage.strQuerySql = strSQL;
	// 设置查询起始位置
	turnPage.pageIndex = 0;
	// 在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
	// 调用 Multiline 功能模块显示二维数组(arrDataSet:一个二维数组[,multilineGrid:初始化过的Multiline对象],otherTurnPage:使用其它的翻页对象,必须是一个 turnPageClass 的对象)
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	fm.querySql.value = strSQL;
}

/*
 * 我理解的是:判断 mulline 中单选按钮是否选中,然后取值,放入指定的区域
 */
function Polinit() {
	var i = 0;
	var checkFlag = 0;
	// 得到 MulLine 行数
	for (i = 0; i < PolGrid.mulLineCount; i++) {
		if (PolGrid.getSelNo(i)) {// 判断该行的 Radio 单选框是否被选中
			checkFlag = PolGrid.getSelNo();// 返回被选中的行的行号,行号是从1开始,如果没有选中行,返回0
			break;
		}
	}
	if (!checkFlag) {
		alert("请先选择一条保单信息！");
	}
}

/*
 * 表单提交前进行一次校验,是否选了 MulLine 中一行,是否修改了数据
 */
function updateDScardflag() {
	fm.myFlag.value = 0;
	var mSelNo = PolGrid.getSelNo();// 判断该行的 Radio 单选框被选中,行号是从1开始,如果没有选中行,返回值是0
	if (mSelNo == 0) {
		alert("请选择一条数据！");
		return false;
	}
	fm.fmtransact.value = "UPDATE";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	// 打开一个可以载入页面的子窗口,功能有点像 window.open,这个方法打开的子窗口,用户可以随机切换输入焦点,对主窗口没有任何影响,最多是被挡住一下
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function disUpdateDScardflag() {
	fm.myFlag.value = 1;
	var mSelNo = PolGrid.getSelNo();// 判断该行的 Radio 单选框被选中,行号是从1开始,如果没有选中行,返回值是0
	if (mSelNo == 0) {
		alert("请选择一条数据！");
		return false;
	}
	fm.fmtransact.value = "UPDATE";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	// 打开一个可以载入页面的子窗口,功能有点像 window.open,这个方法打开的子窗口,用户可以随机切换输入焦点,对主窗口没有任何影响,最多是被挡住一下
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showContInfo();
	}
}
