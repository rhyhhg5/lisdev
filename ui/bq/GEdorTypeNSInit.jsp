<%
//程序名称：GEdorTypeNSInput.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>      
                 
<script language="JavaScript">

function initEdor() {
	fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
	fm.all('GrpPolNo').value = top.opener.fm.all('GrpPolNo').value;
	fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
	fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
	
	showOneCodeName("EdorCode", "EdorTypeName");
}                                    

//新增被保人列表
function initLPInsuredGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种代码";         			//列名
      iArray[1][1]="160px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="新增保单号";         			//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      LPInsuredGrid = new MulLineEnter( "fm" , "LPInsuredGrid" ); 
      //这些属性必须在loadMulLine前
      LPInsuredGrid.mulLineCount = 1;   
      LPInsuredGrid.displayTitle = 1;
      //SubInsuredGrid.tableWidth = 200;
      LPInsuredGrid.hiddenPlus = 1;
      LPInsuredGrid.hiddenSubtraction = 1;
      LPInsuredGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面

      }
      catch(ex)
      {
        alert(ex);
      }
}

function initForm() 
{
	initEdor();

	initLPInsuredGrid();

  initQuery();
		
}

</script>