<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorConfirmOptionInit.jsp
//程序功能：页面初始化
//创建日期：2005-04-19
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT>

var mEdorAcceptNo = "<%=request.getParameter("EdorAcceptNo")%>";

//初始化表单
function initForm()
{
	fm.all("EdorAcceptNo").value = mEdorAcceptNo;
	queryMenoy(mEdorAcceptNo);
	queryAccount();
	getNoticeNo();
	initElementtype();
}

</SCRIPT>