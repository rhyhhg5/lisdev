
<%
	//程序名称：
	//程序功能：个人保全
	//创建日期：2011-10-31
	//创建人  ：XP
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">  
function initInpBox()
{ 
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
 
  	if (flag == "TASK")
  	{
 		fm.save.style.display = "none";
		fm.cal.style.display = "none";
  		fm.goBack.style.display = "none";
  		fm.goBack1.style.display = "none";
  	}
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;

    var sql = "";
    //查询保单机构是否有特殊配置
    var checkSql = "select * from ldcode where codetype = 'loancom' and code = " +
        " (select case when managecom = '86' then managecom else substr(managecom,1,4) end " +
         " from lccont where contno='"+fm.ContNo.value+"') with ur  " ; 
    var checkResult = easyExecSql(checkSql);
    if(checkResult){
	   sql = "  select rate from lminterestrate a where a.startdate<=current date and a.enddate>=current date   and a.busstype='L' "
              + " and a.comcode = (select case when managecom = '86' then managecom else substr(managecom,1,4) end "
              + "  from  lccont where contno='"+fm.ContNo.value+"') "
              + " and int(minpremlimit)<= (select prem from lccont where contno ='"+fm.ContNo.value+"' ) "
              + " and (int(nvl(maxpremlimit,0)) > (select prem from lccont where contno ='"+fm.ContNo.value+"') or maxpremlimit is null or maxpremlimit='') "
              + " and a.riskcode =(select riskcode from lcpol where contno='" + fm.ContNo.value + "' and stateflag='1' " 
              + " and riskcode in (select riskcode from LMLoan) order by riskcode fetch first 1 row only) ";
        }else{
	   sql = "  select rate from lminterestrate a where a.startdate<=current date and a.enddate>=current date   and a.busstype='L' "
              + " and a.comcode = '86' and a.riskcode =(select riskcode from lcpol where contno='" + fm.ContNo.value + "' and stateflag='1' " 
              + " and int(minpremlimit)<= (select prem from lccont where contno ='"+fm.ContNo.value+"' ) "
              + " and (int(nvl(maxpremlimit,0)) > (select prem from lccont where contno ='"+fm.ContNo.value+"') or maxpremlimit is null or maxpremlimit='') "
               + " and riskcode in (select riskcode from LMLoan) order by riskcode fetch first 1 row only) ";
            }
    var result = easyExecSql(sql);
    if(result!=null&&result.length==1)
    {
        fm.all('Rate').value = result[0][0];
    }else{
    	alert("获取当前贷款利率失败!");
    	return false;
    }
    
    var sql2 = "select loandate, payoffdate from lploan where edorno='" + fm.EdorNo.value + "' "; 
    var rs2 = easyExecSql(sql2);
    if(rs2)
    {
	    fm.all('LoanDate').value = rs2[0][0];
	    fm.all('PayOffDate').value = rs2[0][1];    	
    }else{
		var sql1 = "select Current Date + 3 days , Current Date + 3 days + 6 months from dual "; 
	    var rs1 = easyExecSql(sql1);
    	fm.all('LoanDate').value = rs1[0][0];
	    fm.all('PayOffDate').value = rs1[0][1];
    }
	    showOneCodeName("EdorCode", "EdorTypeName"); 
}

function initForm()
{
  try
  {
    initInpBox();
    initPolGrid(); 
    getPolInfo();
    initElementtype();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}


function initPolGrid()
{
    var iArray = new Array();

      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="险种编码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="险种名称";
        iArray[2][1]="200px";
        iArray[2][2]=100;
        iArray[2][3]=0;

        iArray[3]=new Array();
        iArray[3][0]="保额";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="保费";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="险种当前现金价值";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;        
        
        iArray[6]=new Array();
        iArray[6][0]="可贷款额度";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="实际申请额度";
        iArray[7][1]="100px";
        iArray[7][2]=100;
        iArray[7][3]=2;
        
        iArray[8]=new Array();
        iArray[8][0]="续期保费未收标志";
        iArray[8][1]="100px";
        iArray[8][2]=100;
        iArray[8][3]=3;
        
        iArray[9]=new Array();
        iArray[9][0]="险种号";
        iArray[9][1]="100px";
        iArray[9][2]=100;
        iArray[9][3]=3;
        
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      PolGrid.hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
