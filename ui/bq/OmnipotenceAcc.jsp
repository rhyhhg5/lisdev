<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：OmnipotenceAcc.jsp
//程序功能：万能账户信息界面
//创建日期：2007-12-20
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<%
  String tPolNo = request.getParameter("PolNo");
  String tInsuAccNo = request.getParameter("InsuAccNo");
%>
<script>
  var mPolNo = "<%=tPolNo%>"; 
  var mInsuAccNo = "<%=tInsuAccNo%>";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <%@include file="OmnipotenceAccInit.jsp"%> 
  <SCRIPT src="OmnipotenceAcc.js"></SCRIPT>
  <title>万能账户信息</title>
</head>

<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form name=fm target="fraSubmit">
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>账户建立时间</td>
        <td  class= input><Input class=common  name=AccFoundDate readonly=true></td>
        <td  class= title>账户结束时间</td>
        <td  class= input><input class=common name=AccEndDate readonly=true></td>
        <td  class= title>账户当前金额</td>
        <td  class= input><input class=common name=InsuAccBala  readonly=true></td>
      </tr>
      <tr  class= common>
        <td class= title>最近结算月份</td>
        <td  class= input><input class=common name=BalaMonth  readonly=true></td>
        <td class= title>最近结算时间</td>
        <td  class= input><input class=common name=BalaDate  readonly=true></td>
      </tr>
    </table>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divOmnipotenceAccTrace);"></td>
	    <td class=titleImg>账户价值历史信息:</td>
	  </tr>
  </table>
  <!-- 信息（列表） -->
  <div id="divOmnipotenceAccTrace" style="display:''">
	  <table class=common>
      <tr class=common>
	      <td text-align:left colSpan=1><span id="spanOmnipotenceAccTraceGrid"></span></td>
	    </tr>
    </table>
  </div>

  <div id="divPage2" align=center style="display: 'none' ">
	  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	  <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	  <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	  <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  <!-- <input class=cssButton  VALUE="测试用"  TYPE=button onclick="queryAccTrace()"> -->
  </form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
