<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：GEdorTypeZTCaltypeSave.jsp
//程序功能：
//创建日期：2005-09-21
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%
    String content = null;
    String flag = null;
    GlobalInput gi = (GlobalInput)session.getValue("GI");
    LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
    tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
    String feeRate = request.getParameter("FeeRate");
    EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tLPGrpEdorItemSchema);
    String calTime = request.getParameter("CalTime");
    
    String PayMode = request.getParameter("PayMode");
  	if(PayMode==null){
  		PayMode="";
  	}
	String PayModeName = request.getParameter("PayModeName");
	if(PayModeName==null){
		PayModeName="";
  	}

	//银行代码和名称
	String BankCode = request.getParameter("BankCode");
	if(BankCode==null){
		BankCode="";
	}
	String BankCodeName = request.getParameter("BankCodeName");
	if(BankCodeName==null){
		BankCodeName="";
	}
	//银行账号
	String BankAccNo = request.getParameter("BankAccNo");
	if(BankAccNo==null){
		BankAccNo="";
	}
	//银行开户名
	String AccName = request.getParameter("AccName");
	if(AccName==null){
		AccName="";
	}
	tEdorItemSpecialData.add("PayMode", PayMode);
	tEdorItemSpecialData.add("PayModeName", PayModeName);
	tEdorItemSpecialData.add("BankCode", BankCode);
	tEdorItemSpecialData.add("BankCodeName", BankCodeName);
	tEdorItemSpecialData.add("BankAccNo", BankAccNo);
	tEdorItemSpecialData.add("AccName", AccName);
	
    tEdorItemSpecialData.add("CalTime", request.getParameter("CalTime")); 
    tEdorItemSpecialData.add("FeeRate", String.valueOf(Double.parseDouble("0") / 100));
    tEdorItemSpecialData.add(BQ.DETAILTYPE_BALAPAYTYPE, request.getParameter("BalaPayWay"));
		VData data = new VData();
		data.add(gi);
		data.add(tLPGrpEdorItemSchema);
		data.add(tEdorItemSpecialData);
		GrpEdorTQDetailUI tGrpEdorTQDetailUI = new GrpEdorTQDetailUI();
		if (!tGrpEdorTQDetailUI.submitData(data))
		{
			flag = "Fail";
			content = "数据保存失败！原因是:" + tGrpEdorTQDetailUI.getError();
		}
		else
		{
			flag = "Succ";
			content = "数据保存成功。";
		}
		String message = tGrpEdorTQDetailUI.getMessage();
		if ((message != null) && (!message.equals("")))
		{
			content += message;
		}
    content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
    parent.fraInterface.afterSubmitCalType("<%=flag%>","<%=content%>");
</script>
</html>