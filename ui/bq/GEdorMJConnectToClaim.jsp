<%
//程序名称：GEdorMJConnectToClaim.jsp
//程序功能：
//创建日期：2006-01-09
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  String calFlag = request.getParameter("calFlag");
  String grpPolNo = request.getParameter("grpPolNo");
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("calFlag", calFlag);
  tTransferData.setNameAndValue("grpPolNo", grpPolNo);
  
  EdorConnectToClaim tEdorConnectToClaim = 
    new EdorConnectToClaim(tTransferData, (GlobalInput)session.getValue("GI"));
  String grpContStatet = tEdorConnectToClaim.getGrpPolClaimState();
%>
<html>
<script language="javascript">
  if("<%=calFlag%>" == "grpPolState")
  {
	  parent.fraInterface.afterConnectToClaim("<%=grpContStatet%>");
	}
</script>
</html>