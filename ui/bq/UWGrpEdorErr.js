//程序名称：UWGrpEdorErrInit.jsp
//程序功能：集体核保未过原因查询
//创建日期：2004-12-31 11:10:36
//创建人  ：ZhangRong
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


//选择要人工核保保单
function queryUWErr()
{
	//showSubmitFrame(mDebug);
	fm.submit();
}

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	initUWErrGrid();
	
	// 书写SQL语句
	var strSQL = "";
	var GrpContNo = fm.all('GrpContNo').value;
	

	strSQL = "select a.EdorNo,a.EdorType,a.GrpContNo,a.GrpPolNo,b.RiskCode,a.UWNo,a.UWError,a.ModifyDate from  LPGUWError a ,LCGrpPol b where "
	         + " a.GrpPolNo = b.GrpPolNo and a.GrpContNo ='" + GrpContNo + "'"
			 + " and (a.UWNo = (select max(e.UWNo) from LPGUWError e where e.GrpContNo = '" + GrpContNo + "' and e.GrpPolNo = a.GrpPolNo))"
			 + " union "
			 + "select c.EdorNo,c.EdorType,c.GrpContNo,'','',c.UWNo,c.UWError,c.ModifyDate from LPGCUWError c where "
			 + " c.GrpContNo ='" + GrpContNo + "'"
			 + " and (c.UWNo = (select max(d.UWNo) from LPGCUWError d where d.GrpContNo = '" + GrpContNo + "'))"
//			 + " order by c.SerialNo"
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				UWErrGrid.setRowColData( i, j + 1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}
