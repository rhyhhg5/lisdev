<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LJSUnlock.jsp
//程序功能：续期收付费信息修改录入界面
//创建日期：2009-04-10
//创建人  ：zhanggm
//页面写的比较恶心，没办法，需求就这么订的，4个功能竟然放在一个页面。
//更新记录：  更新人 zhanggm   更新日期   2009-5-7  更新原因/内容 保全续期页面拆分
%>
<script>
  var tsql = "1 and code in (select code from ldcode where codetype = #paymodebqpay#)";
  var tsq2 = "1 and code in (select code from ldcode where codetype = #paymodebqget#)";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!―页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="RiskUnlock.js"></SCRIPT> 
  <%@include file="RiskUnlockInit.jsp"%> 
  
  <title>分红险付费方式信息修改</title>
</head>
<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form action="RiskUnlockSave.jsp" method=post name=fm target="fraSubmit">
    <table class = common align = center>
	  <tr  class= common>		    	
		<td  class= input>
		<input type="radio" name="Type2" id="Type2" value="1" >付费项目</td>
		<td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </table>
  
  
  <div id="divBQGet" align=left style="display: '' ">
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class= "codeno"  name=ManageCom2  ondblclick="return showCodeList('comcode',[this,ManageComName2],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName2],[0,1],null,null,null,1);" ><Input class=codename readonly name=ManageComName2></td>
        <td  class= title>保单号  </td>
        <td  class= input><input class=common  name=Contno2 ></td>
        <td  class= title>付费凭证号</td>
        <td  class= input><input class=common  name=ActuGetNo2 ></td>
      </tr>
      <tr class= common>
      <td class=button><input class=cssButton  VALUE="查  询"  TYPE=button onclick="queryClick()"></td>
      <td class=button><input class=cssButton  VALUE="批单打印数据查询"  TYPE=button onclick="queryPrintClick()"></td>
      </tr>
    </table>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLJAGetRiskGrid);"></td>
	  <td class=titleImg>转账失败清单</td>
	</tr>
  </table>
  <div id="divLJAGetRiskGrid" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1><span id="spanLJAGetRiskGrid"></span></td>
	  </tr>
	</table>
  </div>
  <div id="divPage2" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  <br>
  <table  class= common align=center>
      <tr  class= common>
        <td  class= title>付费凭证号</td>
        <td  class= input><input class=readonly  name=ActuGetNoR2 readonly></td>
        <td  class= title>保单号  </td>
        <td  class= input><input class=readonly  name=WorkNoR2 readonly></td>
        <td  class= title>付费金额</td>
        <td  class= input><input class=readonly  name=GetMoney2 readonly></td>
      </tr>
      <tr class= common>
        <td class= title >失败原因</td>
        <td class= input colspan=5><input class=readonly  name=Reason2 readonly></td>
      </tr>
    </table>
    <br>
    <table  class= common align=center>
      <tr  class= common>
        <td class= title>付费方式</td>    
		<td class= input><Input class="codeNo" name="PayMode2"  verify="交付费方式|notnull&code:PayMode"  ondblclick="return showCodeList('PayMode',[this,PayModeName2],[0,1],null,tsq2,'1',1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName2],[0,1],null,null,'1',1);"><Input class="codeName" name="PayModeName2" readonly></td>
        <td  class= title>对方姓名</td>
        <td  class= input><Input class= common name=Drawer2 ></td>
        <td  class= title>对方身份证号</td>
        <td  class= input><Input class= common  name=DrawerID2 ></td>
      </tr>
      <tr  class= common>
        <td  class= title>对方开户银行</td>
        <td  class= input><Input class="codeNo" name=BankCode2 ondblclick="return showCodeList('Bank',[this,BankCodeName2],[0,1]);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName2],[0,1]);"><Input class="codeName" name=BankCodeName2 readonly ></td>
        <td  class= title>对方银行帐号</td>
        <td  class= input><Input class= common  name=BankAccNo2 onblur="confirmBankAccNo(this)"></td>
        <td class=title>对方帐户名</td>
        <td class=input><Input class= common  name=AccName2 ></td>
      </tr>
    </table>
  </div> 
    <div align=right>
      <!-- <input class=cssButton  VALUE="测试"  TYPE=button onclick=checkSubmit("BQPAY")> -->
      <input class=cssButton  VALUE="确  认"  TYPE=button onclick="submitForm()">
    </div>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divNoBankPrintGrid);"></td>
	  <td class=titleImg>非银行转账数据菜单打印</td>
	</tr>
  </table>  
  <div id="divNoBankPrintGrid" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1><span id="spanNoBankPrintGrid"></span></td>
	  </tr>
	</table>
  </div>
  <div id="divPage3" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage3.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage3.lastPage();">
  </div>
  <br>
  <div align=right>
      <input class=cssButton  VALUE="批单打印"  TYPE=button onclick="submitPrint()">
    </div>
    
</form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
