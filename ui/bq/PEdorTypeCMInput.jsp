<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人 杨天政   更新日期  2011-03-23   更新原因/内容 添加“与投保人的关系”
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK"> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeCM.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeCMInit.jsp"%>
  <title>客户基本资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeCMSubmit.jsp" method=post name=fm target="fraSubmit"> 
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
      <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" type="hidden" readonly name=EdorType>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>
 <Div  id= "divLPInsuredDetail" style= "display:''">
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
        客户详细信息
      </td>
   </tr>
   </table>
  	<Div  id= "divDetail" style= "">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name verify="姓名|notnull&len<=60"  elementtype=nacessary >
            <Input class= hidden type=hidden readonly name=NameBak >
          </TD>
				  <TD  class= title>
						性别
				  </TD>
				  <TD  class= input>
					 	<Input class="codeNo" name=Sex verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName], [0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName], [0,1]);"><Input class="codeName" name=SexName elementtype=nacessary readonly >
		        <Input class= hidden type=hidden readonly name=SexBak >
				  </TD>
				</TR>
      	<TR  class= common>
				  <TD  class= title>
				   	出生日期
				  </TD>
				  <TD  class= input>
						<Input class= common name=Birthday elementtype=nacessary verify="出生日期|notnull&date" >
		        <Input class= hidden type=hidden readonly name=BirthdayBak >
				  </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class= codeNo name="IDType"  verify="证件类型|notnull&code:IDType" ondblclick="return showCodeList('IDType',[this,IDTypeName], [0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName], [0,1]);"><Input class= codeName name="IDTypeName" elementtype=nacessary readonly >
            <Input class= hidden type=hidden readonly name=IDTypeBak >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name=IDNo elementtype=nacessary verify="证件号码|notnull&len<20">
            <Input class= hidden type=hidden readonly name=IDNoBak >
         </TD>
        </TR>       
      <TR id="idCardShowOrNot" class= common>
				  <TD  class= title>
				   	证件生效日期
				  </TD>
				  <TD  class= input>
						<Input class= common name=IDStartDate  verify="证件生效日期|date" >
		        <Input class= hidden type=hidden readonly name=IDStartDateBak >
				  </TD>
				  <TD  class= title>
				   	证件失效日期
				  </TD>
				  <TD  class= input>
						<Input class= common name=IDEndDate  verify="证件失效日期|date" >
		        <Input class= hidden type=hidden readonly name=IDEndDateBak >
				  </TD>
<!--          <TD  class= title>-->
<!--            证件号码-->
<!--          </TD>-->
<!--          <TD  class= input>-->
<!--            <Input class= common name=IDNo elementtype=nacessary verify="证件号码|len<20">-->
<!--            <Input class= hidden type=hidden readonly name=IDNoBak >-->
<!--         </TD>-->
        </TR>   
        
        <TR  class= common>
          <TD class=title>
					  职业代码 
					</TD>
					<TD class=input>
					 	<Input name=OccupationCode class="codeNo" ondblclick="return showCodeList('OccupationCode', [this,OccupationCodeName,OccupationType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode', [this,OccupationCodeName,OccupationType],[0,1,2],null,null,null,1,300);" verify="职业代码|notnull"><Input name=OccupationCodeName class="codeName" elementtype=nacessary readonly >
						<Input class= hidden type=hidden readonly name=OccupationCodeBak >
					</TD>
					<TD class=title>
					  职业类别 
					</TD>
					<TD class=input>
					  <Input name="OccupationType" class="readonly" readonly>
						<Input class= hidden type=hidden readonly name=OccupationTypeBak >
					</TD>
					<TD CLASS=title>
						婚姻状况
					</TD>
					<TD CLASS=input>
						<Input class="codeNo" name="Marriage" verify="婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this,MarriageName], [0,1]);" onkeyup="return showCodeListKey('Marriage',[this,MarriageName], [0,1]);" ><Input class="codeName" name="MarriageName"  readonly >
						<Input class= hidden type=hidden readonly name=MarriageBak >
					</TD>
         </TR> 
         <TR>
         	<TD CLASS=title>
						与主被保人关系
					</TD>
					<TD CLASS=input>
						<Input class="codeNo" name="Relation"  verify="与主被保人关系|code:Relation" ondblclick="return showCodeList('Relation',[this,RelationName], [0,1]);" onkeyup="return showCodeListKey('Relation',[this,RelationName], [0,1]);" ><Input class="codeName" name="RelationName" readonly >
						<Input class= hidden type=hidden readonly name=RelationBak >
					</TD>
					
					 <TD CLASS=title>
						与投保人的关系
					 </TD>
					
					<TD CLASS=input>
						<Input class="codeNo" name="RelationToAppnt"  verify="与投保人的关系|code:Relation" ondblclick="return showCodeList('Relation',[this,RelationToAppntName], [0,1]);" onkeyup="return showCodeListKey('Relation',[this,RelationToAppntName], [0,1]);" ><Input class="codeName" name="RelationToAppntName" readonly >
						<Input class= hidden type=hidden readonly name=RelationToAppntBak >
		            </TD>
		            
					<TD CLASS=title>
						在职状态
					</TD>
					<TD CLASS=input>
						<Input class="codeNo" name="InsuredState"  ondblclick="return showCodeList('workstate',[this,InsuredStateName], [0,1]);" onkeyup="return showCodeListKey('workstate',[this,InsuredStateName], [0,1]);" ><Input class="codeName" name="InsuredStateName" readonly >
					</TD>
       	 </TR>
       	 <TR>
         	<TD CLASS=title>国籍</TD>
			  <TD  class= input>
			  <input class=codeNo name="nationality" verify="投保人国籍|code:NativePlace" ondblclick="return showCodeList('NativePlace',[this,AppntNativePlaceName],[0,1]);" onkeyup="return showCodeListKey('NativePlace',[this,AppntNativePlaceName],[0,1]);"><input class=codename name=AppntNativePlaceName readonly=true>   
			  <Input class= hidden type=hidden readonly name=NativePlaceBak >
              </TD>
            </TD>
	
			<TD CLASS=title>岗位职务</TD>
			  <TD CLASS=input>
				<Input class="common" name="position" >
				<Input class= hidden type=hidden readonly name=positionBak >
		       </TD>
		     </TD>
			
			<TD CLASS=title>年薪（必须输入数值数据）</TD>
			  <TD CLASS=input>
				<Input class="common" name="salary">（单位：万）
				<Input class= hidden type=hidden readonly name=salaryBak >
			  </TD>
			</TD>
       	 </TR>
       </table>
      </Div>
      <Div id = "DivTaxInfo">
      	<table>
      		<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivTaxInfoDetail);">
				</td>
				<td class="titleImg">税优标识信息
				</td>
			</tr>
      	</table>
      </Div>
      <div id = "DivTaxInfoDetail">
      	<table class= common>
	       	 <TR>
		  	      <TD CLASS=title>个税征收方式</TD>
		          <TD  CLASS= input>
		      			<input NAME="TaxPayerType" CLASS=codeNo ondblclick="return showCodeList('taxpayertype',[this,TaxPayerTypeName],[0,1],null,'1','1',1);" onkeyup="return showCodeListKey('taxpayertype',[this,TaxPayerTypeName],[0,1],null,'1','1',1);" onblur = "changeType();"verify="个税征收方式|code:taxpayertype"><input class=codename name=TaxPayerTypeName readonly=true >
		         </TD>
	  	      </TR> 
	  	      <tr id = "pTax">
		  	      <TD  class= title>
		            	个人税务登记证代码
		          </TD>
		          <TD  class= input>
		      			<Input NAME=TaxNo VALUE="" CLASS=common >
		         </TD>
		         <TD  class= title>
		            	个人社会信用代码
		          </TD>
		          <TD  class= input>
		      			<Input NAME=CreditCode VALUE="" CLASS=common >
		         </TD> 
	  	      </tr>   
	  	      <tr id = "gTax">
		          <TD  class= title>
		            	单位税务登记证代码
		          </TD>
		          <TD  class= input>
		      			<Input NAME=GTaxNo VALUE=""   CLASS=common >
		         </TD>
		         <TD  class= title>
		            	单位社会信用代码
		          </TD>
		          <TD  class= input>
		      			<Input NAME=GOrgancomCode VALUE=""   CLASS=common >
		         </TD>
	  	      </tr>   
      	</table>
      </div>
		<DIV id=DivGUniversal STYLE="display:'none'"> 
			<table  class= common>
				<TR CLASS=common>
					<TD CLASS=title>服务年数起始日</TD>
					<TD CLASS=input>
					<Input class= "coolDatePicker" dateFormat="short" name=JoinCompanyDate>
					</TD>
					<TD CLASS=title >级别</TD>
					<TD CLASS=input>
						
						<Input class="codeNo" name="PositionU"  ondblclick="return showCodeList('Position1',[this,PositionUName],[0,1],null,fm.Prtno.value,'Prtno',1);" onkeyup="return showCodeListKey('Position1',[this,PositionUName],[0,1],null,fm.Prtno.value,'Prtno',1);" ><Input class="codeName" name="PositionUName" readonly >
						
					</TD>
					<TD CLASS=title width="109" >老年护理金领取方式</TD>
					<TD CLASS=input>
						<Input class="codeNo" name="GetDutyKind" CodeData="0|^A|一次性领取"   ondblclick="return showCodeListEx('GetDutyKindU',[this,GetDutyKindName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('GetDutyKindU',[this,GetDutyKindName],[0,1],null,null,null,1);" ><Input class="codeName" name="GetDutyKindName" readonly >
						
					</TD>
				</TR>
				
			</Table> 	          
		</DIV>
		<DIV id=DivGinPhone STYLE="display:'none'"> 
			<table  class= common>
				<TR class=common>
					<TD class=title>
						被保险人联系电话
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=GrpInsuredPhone  verify="被保险人联系电话|len<=20">
					</TD>
					<TD class=title></TD>
					<TD class=input></TD>
					<TD class=title></TD>
					<TD class=input></TD>
				</TR>
			</Table> 	          
		</DIV>  
		<DIV id=DivAppntHead STYLE="display:'none'">   
			<table>
					<tr>
						<td>
						<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivAppntInfo);">
						</td>
						<td class="titleImg">投保人信息
						</td>
					</tr>
			</table>
		</Div> 
		<DIV id=DivAppntInfo STYLE="display:'none'"> 
		<table class=common>
				<TR class=common>
					<TD class=title>
						姓名
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=AppntName elementtype="nacessary" verify="投保人姓名|notnull&len<=600">
					</TD>
					<TD class=title>
						性别
					</TD>
					<TD class=input>
						<Input class=codeNo name=AppntSex  verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,AppntSexName],[0,1]);" onkeyup="return showCodeListKey('Sex',[this,AppntSexName],[0,1]);"><input class=codename name=AppntSexName readonly=true elementtype=nacessary>    
					</TD>
					<TD class=title>
						出生日期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="AppntBirthday"  elementtype="nacessary" verify="投保人出生日期|notnull">
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						证件类型
					</TD>
					<TD class=input>
						<Input class=codeNo name="AppntIDType" verify="投保人证件类型|notnull&code:IDType" ondblclick="return showCodeList('IDType',[this,AppntIDTypeName],[0,1]);" onkeyup="return showCodeListKey('IDType',[this,AppntIDTypeName],[0,1]);"><input class=codename name=AppntIDTypeName readonly=true elementtype=nacessary>    
					</TD>
					<TD class=title>
						证件号码
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=AppntIDNo elementtype="nacessary" verify="投保人证件号码|notnull&len<=30">
					</TD>

					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>
		</div>
	<DIV id=DivClaimHead STYLE="display:''">   
	<table>
			<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivClaim);">
				</td>
				<td class="titleImg">理赔金帐户
				</td>
			</tr>
	</table>
	</Div>      

<DIV id=DivClaim STYLE="display:''"> 
	<table  class= common>
		<TR CLASS=common>
			<TD CLASS=title>开户银行</TD>
			<TD CLASS=input>
				<Input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=20  ondblclick="return showCodeList('bank',[this,BankCodeName], [0,1]);" onkeyup="return showCodeListKey('bank',[this,BankCodeName], [0,1]);" ><Input NAME=BankCodeName VALUE="" CLASS="codeName" readonly >
				<input name="BankCodeBak" type=hidden>
			</TD>
			<TD CLASS=title >户名</TD>
			<TD CLASS=input>
				<Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=50 verify="户名|len<=50" >
				<input name="AccNameBak" type=hidden>
			</TD>
			<TD CLASS=title width="109" >账号</TD>
			<TD CLASS=input>
				<Input NAME=BankAccNo VALUE="" CLASS=common verify="银行帐号|len<=40">
				<input name="BankAccNoBak" type=hidden>
			</TD>
		</TR>
		<TR><TD><input type=button value="银行查询" class=cssButton onclick="querybank();"></TD></TR>   
	</Table> 	          
</DIV> 
 
	</Div>
	<div id="divCont">
		<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);">
	      </td>
	      <td class= titleImg>
	        关联保单信息
	      </td>
		  </tr>
		</table>
		<Div  id= "divLCCont" style= "display: ''">
			<table  class= common>
		 		<tr  class= common>
			  		<td text-align: left colSpan=1>
					<span id="spanContGrid" >
					</span> 
			  	</td>
			</tr>
		</table>					
	</div>  
	</div>
	  <br>
	  <hr>
		<Input class= cssButton type=Button value="保  存" onclick="edorTypeCMSave()">
		<Input class= cssButton type=Button value="取  消" onclick="initForm()">
		<Input type=Button value="返  回" class = cssButton onclick="returnParent()">
		<input type=hidden id="TypeFlag" name="TypeFlag">
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="ContType" name="ContType">
		<input type=hidden name="EdorAcceptNo">
		<input type=hidden name="AppntNo">
		<input type=hidden id="Prtno" name="ContType">
		<input type=hidden id="GUFlag" name="GUFlag">
		<input type=hidden id="PrtNo" name="PrtNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>