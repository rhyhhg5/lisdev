<html> 
<% 
//程序名称：BqAppAccConfirm.jsp
//程序功能：保全使用投保人超收帐户余额确认
//创建日期：2006-02-15 16:49:22
//创建人  ：mojiao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>   
  <SCRIPT src="./BqAppAccConfirm.js"></SCRIPT>   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BqAppAccConfirmInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./BqAppAccConfirmSubmit.jsp" method=post name=fm target="fraSubmit">
		<TABLE class=common>
			<tr class=common>
				<td class=title> 客户：</td>
				<td class= input>
				  <Input type="input" class="readonly" readonly name=CustomerName>
				</td>
			</tr>
			<tr>
				<td class=title>超收保费余额为：</td>
				<td class= input>
				  <Input type="input" class="readonly" readonly name=AccBala>
				</td>			
				<td class=title>可领金额为：</td>
				<td class= input>
				  <Input type="input" class="readonly" readonly name=AccGetBala>
				</td>
			</tr>
		</TABLE>
		<% 
		if(accType.equals("0"))
		{
		%>
			是否使用超收保费余额抵扣?
		<% 
		}else
		{%>
			本次退费是否转到超收保费余额帐户?
		<% }
		%>		
		<br>
	  <input type="radio" name="chkYesNo" value="yes">是
		<input type="radio" name="chkYesNo" value="no">否
    <table class= common> 
    	<tr>
    		<td align="center"> 
    			<input type =button class=cssButton value="确    定" onclick="clickOK();">
    		</td>
    	</tr>
    </table>
			<input type=hidden name="fmtransact">
			<input type=hidden name="Transact">
			<input type=hidden name = "ContType">			
			<input type=hidden name = "CustomerNo">
			<input type=hidden name = "AccType">
			<input type=hidden name = "EdorAcceptNo">
			<input type=hidden name = "DestSource">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>