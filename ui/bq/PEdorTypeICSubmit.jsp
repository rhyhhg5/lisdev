<%
//程序名称：PGrpEdorTypeICSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
	System.out.println("-----ICsubmit---");
	LPInsuredSchema tLPInsuredSchema   = new LPInsuredSchema();
	LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();
	LPAddressSchema tLPAddressSchema = new LPAddressSchema();
	LPGrpEdorItemSchema tLPGrpEdorItemSchema   = new LPGrpEdorItemSchema();

	PEdorICDetailUI tPEdorICDetailUI   = new PEdorICDetailUI();

	CErrors tError = null;
	//后面要执行的动作：添加，修改

	String tRela  = "";                
	String FlagStr = "";
	String Content = "";
	String transact = "";
	String Result="";
	String addrFlag= "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("fmtransact");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
	
	addrFlag=request.getParameter("addrFlag");
  
//	tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
//	tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
//	tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
//	tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
	//个人保单批改信息
	tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
	tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
	tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	tLPEdorItemSchema.setInsuredNo(request.getParameter("CustomerNo"));

	tLPInsuredSchema.setContNo(request.getParameter("ContNo"));
	tLPInsuredSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPInsuredSchema.setEdorType(request.getParameter("EdorType"));
	tLPInsuredSchema.setInsuredNo(request.getParameter("CustomerNo"));
	
	tLPInsuredSchema.setName(request.getParameter("Name"));
	tLPInsuredSchema.setSex(request.getParameter("Sex"));
	tLPInsuredSchema.setBirthday(request.getParameter("Birthday"));
	tLPInsuredSchema.setIDType(request.getParameter("IDType"));
	tLPInsuredSchema.setIDNo(request.getParameter("IDNo"));

	//修改理赔金帐号 2005-5-24 16:43 add by lanjun 
  tLPInsuredSchema.setBankCode(request.getParameter("BankCode"));
  tLPInsuredSchema.setBankAccNo(request.getParameter("BankAccNo"));  
  System.out.println("BankCode"+request.getParameter("BankCode"));              
  tLPInsuredSchema.setAccName(request.getParameter("AccName"));	
	
	
	
	//个人客户地址表信息 合并自BB
  tLPAddressSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPAddressSchema.setEdorType(request.getParameter("EdorType"));
  tLPAddressSchema.setCustomerNo(request.getParameter("CustomerNo"));
  tLPAddressSchema.setAddressNo(request.getParameter("AddressNo"));  
  tLPAddressSchema.setHomeAddress(request.getParameter("HomeAddress"));  
  tLPAddressSchema.setPostalAddress(request.getParameter("PostalAddress"));
  tLPAddressSchema.setZipCode(request.getParameter("ZipCode"));
  tLPAddressSchema.setPhone(request.getParameter("Phone"));
  tLPAddressSchema.setMobile(request.getParameter("Mobile"));

	try
	  {
		// 准备传输数据 VData

		VData tVData = new VData();

		//保存个人保单信息(保全)
		tVData.addElement(addrFlag);
		tVData.addElement(tG);
		tVData.addElement(tLPEdorItemSchema);
		tVData.addElement(tLPGrpEdorItemSchema);
		tVData.addElement(tLPAddressSchema);
		tVData.addElement(tLPInsuredSchema);
		tPEdorICDetailUI.submitData(tVData,transact);

	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}			
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tPEdorICDetailUI.mErrors;
		if (!tError.needDealError())
		{                          
			Content = " 保存成功";
			FlagStr = "Success";
			if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))
			{
				if (tPEdorICDetailUI.getResult()!=null&&tPEdorICDetailUI.getResult().size()>0)
				{
					Result = (String)tPEdorICDetailUI.getResult().get(0);
					if (Result==null||Result.trim().equals(""))
					{
						FlagStr = "Fail";
						Content = "查询失败或未查询到相应记录!!";
					}
					System.out.println(Result);
				}
			}
		}
		else                                                                           
		{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
	//添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

