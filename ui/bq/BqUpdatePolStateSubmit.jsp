<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpNiDeleteSubmit.jsp
//程序功能：删除导入清单中的被保人
//创建日期：2005-08-02
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%
	String FlagStr = "Succ";
	String Content = "";
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");	
	
	//获取选中的险种号
	String tpolnoStr = request.getParameter("polnoStr");
	System.out.println("tpolnoStr = " + tpolnoStr);
	String[] polnoArr = tpolnoStr.split(","); 
	LCPolSet tLCPolSet = new LCPolSet();
	for(int i = 0 ; i < polnoArr.length ; i++){
		LCPolSchema tLCPolSchema = new LCPolSchema();
		tLCPolSchema.setContNo(polnoArr[i]);
		tLCPolSet.add(tLCPolSchema);
		System.out.println("polnoArr = " + polnoArr[i]);
	}
		
	//输入参数
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tLCPolSet);
	
	BqUpdatePolStateUI tBqUpdatePolStateUI = new BqUpdatePolStateUI();
	if (!tBqUpdatePolStateUI.submitData(tVData, "UPDATE"))
	{
		FlagStr = "Fail";
		Content = tBqUpdatePolStateUI.mErrors.getFirstError();
	}
	else
	{
		FlagStr = "Succ";
		Content = "数据修改成功！";
	} 
%> 

<html>
<script language="javascript">
	parent.fraInterface.initForm();
</script>
</html>

