<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PEdorConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%  
    String flag = "";
    String content = "";
		String customerNo = request.getParameter("CustomerNo");
		String accType = request.getParameter("AccType");
		String edorAcceptNo = request.getParameter("EdorAcceptNo");
		String destSource=request.getParameter("DestSource");
		String contType=request.getParameter("ContType");
	  String fmtransact = request.getParameter("fmtransact");
	  String fmtransact2 = request.getParameter("fmtransact2");
		String payMode = request.getParameter("PayMode");
		GlobalInput gi = (GlobalInput)session.getValue("GI");
		
		String failType = null;
		String autoUWFail = null;
		
		EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorAcceptNo, "YE");
		tSpecialData.add("CustomerNo", customerNo);
		tSpecialData.add("AccType", accType);
		tSpecialData.add("OtherType", "10");
		tSpecialData.add("OtherNo", edorAcceptNo);
		tSpecialData.add("DestSource", destSource);
		tSpecialData.add("ContType", contType);
	  tSpecialData.add("Fmtransact2", fmtransact2);
	  		
  //在tSpecialData对象中存入银行转帐帐户信息
  tSpecialData.add("AccName", request.getParameter("AccName"));
  tSpecialData.add("Bank", request.getParameter("Bank"));
  tSpecialData.add("BankAccno", request.getParameter("BankAccno"));
  tSpecialData.add("PayDate", request.getParameter("PayDate"));
    
	  LCAppAccTraceSchema tLCAppAccTraceSchema=new LCAppAccTraceSchema();
	  tLCAppAccTraceSchema.setCustomerNo(customerNo);
	  tLCAppAccTraceSchema.setAccType(accType);
	  tLCAppAccTraceSchema.setOtherType("10");//个人批改号
	  tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
	  tLCAppAccTraceSchema.setDestSource(destSource);
	  tLCAppAccTraceSchema.setOperator(gi.Operator);
	  
    VData tVData = new VData();
    tVData.add(tLCAppAccTraceSchema);
		tVData.add(tSpecialData);
		tVData.add(gi);
		PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
		if (!tPEdorAppAccConfirmBL.submitData(tVData, "INSERT||Param"))
		{
			flag = "Fail";
			content = "处理帐户余额失败！";
		}
  	else
  	{
	    BqConfirmUI tBqConfirmUI = new BqConfirmUI(gi, edorAcceptNo, BQ.CONTTYPE_P,"");
	    if (!tBqConfirmUI.submitData())
	    {
	      flag = "Fail";
	      content = tBqConfirmUI.getError();
	    }
	    else
	    {			
	    	System.out.println("交退费通知书" + edorAcceptNo);
				TransferData tTransferData = new TransferData();
				tTransferData.setNameAndValue("payMode", payMode);	
				tTransferData.setNameAndValue("endDate", request.getParameter("EndDate"));
				tTransferData.setNameAndValue("payDate", request.getParameter("PayDate"));
				tTransferData.setNameAndValue("bank", request.getParameter("Bank"));
				tTransferData.setNameAndValue("bankAccno", request.getParameter("BankAccno"));
				tTransferData.setNameAndValue("accName", request.getParameter("AccName"));
				tTransferData.setNameAndValue("chkYesNo", request.getParameter("ChkYesNo"));
	  		
	  		//生成交退费通知书
				FeeNoticeVtsUI tFeeNoticeVtsUI = new FeeNoticeVtsUI(edorAcceptNo);
				if (!tFeeNoticeVtsUI.submitData(tTransferData))
				{
					flag = "Fail";
					content = "生成批单失败！原因是：" + tFeeNoticeVtsUI.getError();
				} 		
	    
				VData data = new VData();
				data.add(gi);
				data.add(tTransferData);
				SetPayInfo spi = new SetPayInfo(edorAcceptNo);
				if (!spi.submitDate(data, fmtransact))
				{
					System.out.println("设置收退费方式失败！");
					flag = "Fail";
					content = "设置收退费方式失败！原因是：" + spi.mErrors.getFirstError();
				}
	    
	  		flag = "Succ";
				content = "保全确认成功！";
	    }
	  	String message = tBqConfirmUI.getMessage();
			if ((message != null) && (!message.equals("")))
			{
			  content += "\n" + tBqConfirmUI.getMessage();
			}
	    content = PubFun.changForHTML(content);
	    char b='%';
      char c='％';
      content=content.replace(b,c);
	    System.out.println(content);
	    failType = tBqConfirmUI.getFailType();  //是否审批通过
	    autoUWFail = tBqConfirmUI.autoUWFail();  //是否自核通过
	    autoUWFail = autoUWFail == null ? "" : autoUWFail;
	    System.out.println("autoUWFail:" + autoUWFail);
	  }
%>                      
<html>
<script language="javascript">
	if(<%=failType%> == "1")
	{
		//自动审批未通过
		parent.fraInterface.fm.failType.value = 1;
	}
  else
  {
  	parent.fraInterface.document.all.redo.disabled=true;
    //自动核保失败
    if("<%=autoUWFail%>" == 0)
    {
		  parent.fraInterface.fm.autoUWFlagFail.value = 1;
    }
  	else
  	{
  		parent.fraInterface.document.all.redo.disabled=true;
  	}
  }
  parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>", "<%=edorAcceptNo%>");
</script>
</html>

