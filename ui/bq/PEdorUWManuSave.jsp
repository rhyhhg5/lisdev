<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%
//程序名称：PEdorUWManuSave.jsp
//程序功能：保全个单人工核保核保结果提交
//创建日期：2005-08-25
//创建人  ：QiuYang
//更新记录：更新人    更新日期     更新原因/内容
%>
<%
		String flag = "";
	  String content = "";
		GlobalInput tGI = (GlobalInput)session.getValue("GI");
		
		LPUWMasterSchema tLPUWMasterSchema = new LPUWMasterSchema();
		tLPUWMasterSchema.setEdorNo(request.getParameter("EdorNo"));
		tLPUWMasterSchema.setEdorType(request.getParameter("EdorType"));
		tLPUWMasterSchema.setContNo(request.getParameter("ContNo"));
    tLPUWMasterSchema.setPolNo(request.getParameter("PolNo"));
    tLPUWMasterSchema.setInsuredNo(request.getParameter("InsuredNo"));
    tLPUWMasterSchema.setAppntNo(request.getParameter("AppntNo"));
    System.out.println("InsuredNo" + request.getParameter("InsuredNo"));
    tLPUWMasterSchema.setPassFlag(request.getParameter("PassFlag"));
    tLPUWMasterSchema.setUWIdea(request.getParameter("UWIdea"));
    tLPUWMasterSchema.setDisagreeDeal(request.getParameter("DisagreeDeal"));
    tLPUWMasterSchema.setAddPremFlag(request.getParameter("AddFeeFlag"));
    tLPUWMasterSchema.setSpecFlag(request.getParameter("SpecFlag"));
    tLPUWMasterSchema.setSubMultFlag(request.getParameter("SubMultFlag"));
    tLPUWMasterSchema.setSubAmntFlag(request.getParameter("SubAmntFlag"));
    tLPUWMasterSchema.setMult(request.getParameter("InputMult"));
    tLPUWMasterSchema.setAmnt(request.getParameter("InputAmnt"));
    
		//设置加费信息
		LPPremSchema tLPPremSchema = new LPPremSchema();
		tLPPremSchema.setEdorNo(request.getParameter("EdorNo")); 
		tLPPremSchema.setEdorType(request.getParameter("EdorType"));
		tLPPremSchema.setContNo(request.getParameter("ContNo"));
		tLPPremSchema.setPolNo(request.getParameter("PolNo"));
		tLPPremSchema.setAppntNo(request.getParameter("AppntNo"));
		tLPPremSchema.setPrem(request.getParameter("AddFeeGrid1"));
		tLPPremSchema.setRate(request.getParameter("AddFeeGrid2"));
		tLPPremSchema.setPayStartDate(request.getParameter("AddFeeGrid3"));
		System.out.println(request.getParameter("AddFeeGrid3"));
		System.out.println(request.getParameter("AddFeeGrid4"));
		tLPPremSchema.setPayEndDate(request.getParameter("AddFeeGrid4"));
		tLPPremSchema.setPaytoDate(request.getParameter("AddFeeGrid4"));
		
		//设置免责信息
		String specCode[] = request.getParameterValues("SpecGrid1");
		String specContent[] = request.getParameterValues("SpecGrid2");            
		String specStartDate[] = request.getParameterValues("SpecGrid3");          
		String specEndDate[] = request.getParameterValues("SpecGrid4");         
		String serialNo[] = request.getParameterValues("SpecGrid5");  
		
		//保全生效日期
		String edorValiDate = request.getParameter("EdorValiDate");
		EdorItemSpecialData tEdorItemSpecialData = 
				new EdorItemSpecialData(request.getParameter("EdorNo"), request.getParameter("EdorType"));
		tEdorItemSpecialData.setPolNo(request.getParameter("PolNo"));
		tEdorItemSpecialData.add("EdorValiDate", edorValiDate);
		
		int size = 0; //避免null	
		if (specCode != null)
		{
			size = specCode.length;
		}
		
		LPSpecSet tLPSpecSet = new LPSpecSet();	
		for (int i = 0; i < size; i++)
		{
			LPSpecSchema tLPSpecSchema = new LPSpecSchema();
			tLPSpecSchema.setEdorNo(request.getParameter("EdorNo")); 
			tLPSpecSchema.setEdorType(request.getParameter("EdorType"));
			tLPSpecSchema.setContNo(request.getParameter("ContNo"));
			tLPSpecSchema.setPolNo(request.getParameter("PolNo"));
			tLPSpecSchema.setSpecCode(specCode[i]);
			tLPSpecSchema.setSpecContent(specContent[i]);
			tLPSpecSchema.setSpecStartDate(specStartDate[i]);
			tLPSpecSchema.setSpecEndDate(specEndDate[i]);
			tLPSpecSchema.setSerialNo(serialNo[i]);
			tLPSpecSet.add(tLPSpecSchema);
		}
		
		VData data = new VData();
		data.add(tGI);
		data.add(tLPUWMasterSchema);
		data.add(tLPPremSchema);
		data.add(tLPSpecSet);
		data.add(tEdorItemSpecialData);
		PEdorManuUWPolUI tPEdorManuUWPolUI = new PEdorManuUWPolUI();
		if (!tPEdorManuUWPolUI.submitData(data))
		{
			flag = "Fail";
			content = tPEdorManuUWPolUI.getError();
		}
		else
		{
			flag = "Succ";
			content = "数据保存成功！";
		}
		content = PubFun.changForHTML(content);
%>
<SCRIPT>
		parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</SCRIPT>
