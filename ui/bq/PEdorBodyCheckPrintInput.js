//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();
var mPrtNo;

function queryHealthPrint()
{
  var sql = "select PrtSeq, EdorNo, CustomerNo, Name, AppName, ManageCom, " +
            "       case when PrintFlag = '1' then '未打印' " +
            "            when PrintFlag = '2' then '已打印' end " + 
            "from LPPENotice " +
            "where 1 = 1 "  +
            getWherePart("PrintFlag", "HealthPrintFlag") +
            getWherePart("ManageCom", "ManageCom") +
            getWherePart("CustomerNo", "InsuredNo") +
            getWherePart("EdorNo", "EdorNo") +
            getWherePart("PrtSeq", "PrtNo") +
            "order by PrtSeq desc ";
  turnPage.queryModal(sql, HealthGrid);
}

function onClickedPrint()
{
  var selNo = HealthGrid.getSelNo() - 1;
  mPrtNo = HealthGrid.getRowColData(selNo, 1);
}

function printNotice()
{
  window.open("./PEdorBodyCheckPrintMain.jsp?PrtNo=" + mPrtNo);
}