<%
//程序名称：PEdorTypeZXInit.jsp
//程序功能：
//创建日期：2010-07-30
//创建人  ：xp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
function initInpBox()
{
  try
  {
    //判断是否是在工单查看中查看项目明细，若是则没有保存按钮
  	var flag;
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
 
  	if (flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  	}
  	try
  {
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
   // fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;                 
  }
  catch(ex)
  {
    alert("在PEdorTypeZXInit.jspInitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initLADiscountGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;  
    
    iArray[1]=new Array();
	iArray[1][0]="服务项目";          		//列名
	iArray[1][1]="120px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[1][4]="serclass1";              	        //是否引用代码:null||""为不引用
	iArray[1][5]="1|8";              	                //引用代码对应第几列，'|'为分割符
	iArray[1][6]="1|0";
	//iArray[1][21]="serclass1";   
    
    iArray[2]=new Array();
	iArray[2][0]="具体项目";          		//列名
	iArray[2][1]="220px";      	      		//列宽
	iArray[2][2]=20;            			//列最大值
	iArray[2][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[2][4]="serclass2";              	        //是否引用代码:null||""为不引用
	iArray[2][5]="2|9";              	                //引用代码对应第几列，'|'为分割符
	iArray[2][6]="1|0";
	iArray[2][15]= "serclass1";  //要依赖的列的名称        
    iArray[2][17]= "8";     //该列的列号,
    //iArray[2][21]="serclass2";
    
    iArray[3]=new Array();
	iArray[3][0]="地点";          		//列名
	iArray[3][1]="80px";      	      		//列宽
	iArray[3][2]=20;            			//列最大值
	iArray[3][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	//iArray[4][5]="4|11";              	                //引用代码对应第几列，'|'为分割符
	//iArray[4][6]="1|0"; 
	iArray[3][10]="Address";  //引用代码："CodeName"为传入数据的名称
    iArray[3][11]="0|^8611|北京|^8621|辽宁|^8631|上海|^8612|天津|^8633|浙江"; //"CodeContent" 是传入要下拉显示的代码 
    //iArray[3][11]="0|^北京|8611|^辽宁|8621|^上海|8631|^天津|8612|^浙江|8633"; //"CodeContent" 是传入要下拉显示的代码
    iArray[3][21]="Address";
	  
   
    iArray[4]=new Array();
	iArray[4][0]="档次";          		//列名
	iArray[4][1]="120px";      	      		//列宽
	iArray[4][2]=20;            			//列最大值
	iArray[4][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[4][4]="serclass3";              	        //是否引用代码:null||""为不引用
	iArray[4][5]="4|10";              	                //引用代码对应第几列，'|'为分割符
	iArray[4][6]="1|0"; 
	iArray[4][15]= "serclass2|serclass1|comcode";  //要依赖的列的名称        
    iArray[4][17]= "9|8|3";     //该列的列号,  
    //iArray[3][21]="serclass3";  
  
    

  
    
    iArray[5]=new Array();
	iArray[5][0]="金额";          		//列名
	iArray[5][1]="80px";      	      		//列宽
	iArray[5][2]=20;            			//列最大值
	iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[5][21]="money";
	
	
	iArray[6]=new Array();
	iArray[6][0]="单位";          		//列名
	iArray[6][1]="50px";      	      		//列宽
	iArray[6][2]=20;            			//列最大值
	iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	//iArray[1][15]= "列名称1|列名称2|…..";  //要依赖的列的名称        
    //iArray[1][17]= "列号1|列号2|…..";    //列名称的列号,
    iArray[6][21]="danWei";
	
  
   
                                       
    iArray[7]=new Array();
    iArray[7][0]="份数"; //列名
    iArray[7][1]="50px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许
    iArray[7][21]="count";
    
    iArray[8]=new Array();
	iArray[8][0]="服务项目";          		//列名
	iArray[8][1]="80px";      	      		//列宽
	iArray[8][2]=20;            			//列最大值
	iArray[8][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	//iArray[8][4]="serclass1";              	        //是否引用代码:null||""为不引用
	iArray[8][21]="serclass1"; 
	
	iArray[9]=new Array();
	iArray[9][0]="具体项目";          		//列名
	iArray[9][1]="80px";      	      		//列宽
	iArray[9][2]=20;            			//列最大值
	iArray[9][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	//iArray[2][4]="serclass2";              	        //是否引用代码:null||""为不引用       
    iArray[9][21]="serclass2";
    
    iArray[10]=new Array();
	iArray[10][0]="档次";          		//列名
	iArray[10][1]="80px";      	      		//列宽
	iArray[10][2]=20;            			//列最大值
	iArray[10][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	//iArray[3][4]="serclass3";              	        //是否引用代码:null||""为不引用
    iArray[10][21]="serclass3"; 
    
    iArray[11]=new Array();
	iArray[11][0]="标志";          		//列名
	iArray[11][1]="80px";      	      		//列宽
	iArray[11][2]=20;            			//列最大值
	iArray[11][3]=3;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[11][21]="seqno";

    
        
    LADiscountGrid = new MulLineEnter( "fm" , "LADiscountGrid" );
   // LADiscountGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    LADiscountGrid.displayTitle = 1;
    //SetGrid.hiddenSubtraction =0;
    //SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
   // LADiscountGrid.canSel=1;
    LADiscountGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在LADiscountInit.jspinitSetGrid函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
	
  try
  {
    
    //debugger;  
    initInpBox();    
    initElementtype();
    initLADiscountGrid();
    queryList()
    queryMoney();
    
  }
  catch(re)
  {
    alert("在PEdorTypeZXInit.jspInitForm函数中发生异常:初始化界面错误!");
  }
}

</script>