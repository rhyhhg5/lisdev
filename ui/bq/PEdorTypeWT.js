//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 

function edorTypeWTReturn()
{
	top.opener.getInsuredPolInfo(fm.all('ContNo').value);
	top.opener.getEdorItem();
	top.opener.focus();
	top.close();
}
function edorTypeWTQuery()
{
	alert("Wait...");
}

//查询险种信息
function getPolInfo(tContNo)
{
    //alert(tContNo);
    //var tContNo;		  
    // 书写SQL语句
    var strSQL ="  select contNo, polNo, riskSeqNo, riskCode, "
                + "  (select riskName from LMRisk a where a.riskCode = LCPol.riskCode), "
                + "  insuredNo, insuredName, amnt, mult, prem, cValiDate, "
                + "  (select customGetPolDate from lccont where contno=LCpol.contno), signDate "
                + "from LCPol "
                + "where ContNo='" + tContNo + "' ";
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
    
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象

    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
    
    for(var i = 0; i < PolGrid.mulLineCount; i++)
    {
    	if(PolGrid.getRowColData(i, 12) == "0")
    	{
    		PolGrid.setRowColData(i, 12, "");
    	}
    }
}

//查询体检费信息
function getTestGrid()
{
    var sql = "select a.ProposalContNo, a.PrtSeq, a.customerNo, a.name, sum(decimal(c.MedicaItemPrice, 20, 2)) "
            + "from LCPENotice a, LCPENoticeItem b, LDTestPriceMgt c "
            + "where a.ProposalContNo = b.ProposalContNo "
	        + "     and a.PrtSeq = b.PrtSeq "
	        + "     and b.peItemCode = c.MedicaItemCode "
	        + "     and a.pEAddress = c.hospitCode "
	        + "     and a.contNo = '" + fm.ContNo.value + "' "
	        + "     and (a.peState is null or a.peState <> '06') "
	        + " and exists (select 1 from LHContItem where ContraNo = c.ContraNo and ContraItemNo = c.ContraItemNo and ConTraType = '02' and DutyState = '1') "
            + "group by a.CustomerNo, a.name, a.ProposalContNo, a.PrtSeq ";
    turnPage2.pageDivName = "divPage2";
    turnPage2.queryModal(sql, TestGrid);
}

//选中已被选择的险种
function chkPol()
{
    var tContno=fm.all('ContNo').value;
    var tEdorNo=fm.all('EdorNo').value;
    var tEdorType=fm.all('EdorType').value;
    var strSQL="select polno from lppol where edorno='"+tEdorNo+"' and edortype='"+tEdorType+"' and contno='"+tContno+"'";        
    var arrResult2=easyExecSql(strSQL);
    var m=0;
  	var n=0;
  	
  	if(arrResult2!=null)
  	{
  		var q=arrResult2.length;
  		for(m=0;m<PolGrid.mulLineCount;m++)
	  	{
	  		for(n=0;n<q;n++)
	  	   {
	  			if(PolGrid.getRowColData(m,2)==arrResult2[n][0])
	  			{
	  				PolGrid.checkBoxSel(m+1);
	  			}
	  		}
	  	}				
  	}
}

//选中已被选择的体检费信息
function chkTest()
{
    var sql = "  select ProposalContNo, PrtSeq "
              + "from LPPENotice "
              + "where edorAcceptNo = '" + fm.EdorNo.value + "' "
              + "    and edorNo = '" + fm.EdorNo.value + "' "
              + "    and edorType = '" + fm.EdorType.value + "' "
              + "    and contNo = '" + fm.ContNo.value + "' ";
    var result = easyExecSql(sql);
    if(result)
    {
        for(var i = 0; i < result.length; i++)
        {
            for(var j = 0; j < TestGrid.mulLineCount; j++)
            {
                if(result[i][0] == TestGrid.getRowColData(j, 1) 
                    && result[i][1] == TestGrid.getRowColData(j, 2))
                {
                    TestGrid.checkBoxSel(j + 1);
                }
                
            }
        }
    }
}

//选中已被选择的工本费信息
function chkGBInfo()
{
    var sql = "  select edorValue "
              + "from LPEdorEspecialData "
              + "where edorAcceptNo = '" + fm.EdorNo.value + "' "
              + "    and edorNo = '" + fm.EdorNo.value + "' "
              + "    and edorType = '" + fm.EdorType.value + "' "
              + "    and detailType = 'GB' ";
    var result = easyExecSql(sql);

    if(result)
    {
        fm.all("costCheck").checked = true;
        fm.all("cost").value = result[0][0];
    }
}

function easyQueryClick(tContNo)
{
  var tContNo;		  
  // 书写SQL语句
  var strSQL = "";
  var strSQL ="select distinct a.CustomerNo,a.Name,a.Sex,a.Birthday,a.IdNo,b.contno,b.grpcontno from LDPerson a,LCCont b where b.ContNo='"+tContNo+"'"
                    +" and a.CustomerNo in (select insuredno from LCInsured where ContNo='"+tContNo+"')";
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = InsuredGrid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
}


function edorTypeWTSave()
{
    //校验是否选择了险种
    if(!polChecked())
    {
      return false;
    }
    
    if(!checkInput())
    {
      return false;
    }
    if(!checkJianGuan())
    {
      return false;
    }
    
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.submit();
}

//保单下只有建管险种有效，则不能进行退保
function checkJianGuan()
{
 var wherePart="";
 var rowCount = 0;
 for (i = 0; i < PolGrid.mulLineCount; i++)
 {
		if (PolGrid.getChkNo(i)) 
		{
		    if(rowCount!=0)
		    {
		       wherePart=wherePart+",";
		    }
		    rowCount = rowCount + 1;
		  	var value=PolGrid.getRowColData(i,2);
		  	wherePart=wherePart+"'"+value+"'";
		}
  }
  var strSQL = "  select 1 from lcpol where contno = '"+fm.ContNo.value+"' "
             + "  and PolNo not in ("+wherePart+")";
  var arrResult = easyExecSql(strSQL);
  if (arrResult != null) //放开保单下险种都解约的情况
  {
   //剩下的险种中，存在statefla in ('1','2')的健管险种
    var strSQL1 = "  select 1 from lcpol a where contno = '"+fm.ContNo.value+"' "
             + " and riskcode in (select riskcode from lmriskapp where risktype ='M' )"
             + " and PolNo not in ("+wherePart+") and ContType = '1' and AppFlag = '1' and stateflag in ('1','2') ";
    var arrResult1 = easyExecSql(strSQL1);
  
    //剩下的险种中，存在非健管险种
    var strSQL2 = "  select 1 from lcpol a where contno = '"+fm.ContNo.value+"' "
             + " and riskcode not in (select riskcode from lmriskapp where risktype ='M' )"
             + " and PolNo not in ("+wherePart+") and ContType = '1' and AppFlag = '1' ";
    var arrResult2 = easyExecSql(strSQL2);
    
    if (arrResult1 != null && arrResult2 == null) //对保单下只有建管险种不解约的进行校验
    {           
      alert("健管险种不能单独承保，请同时选择健管险种！")
      return false;
    }
  }
  return true;
}

//校验是否选择了险种
function polChecked()
{
  var rowCount = 0;
  for (i = 0; i < PolGrid.mulLineCount; i++)
	{
		if (PolGrid.getChkNo(i)) 
		{
		  	rowCount = rowCount + 1;
		}
	}
	if(rowCount == 0)
	{
    alert("请选择险种");
	  return false;
	}
	
  return true;
}

function checkInput()
{
  if(fm.reason_tb.value == "")
  {
    alert("退保原因不能为空。");
    return false;
  }
  
  return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  top.focus();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   // alert(FlagStr);
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
    
    edorTypeWTReturn();
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function showWTReason()
{
  var sql = "  select ReasonCode "
            + "from LPEdorItem "
            + "where edorNo = '" + fm.EdorNo.value + "' "
            + "    and edorType = '" + fm.EdorType.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    fm.reason_tb.value = result[0][0];
    
    sql = "  select codeName "
          + "from LDCode "
          + "where codeType = 'reason_tb' "
          + "   and code = '" + result[0][0] + "' ";
    result = easyExecSql(sql);
    if(result)
    {
      fm.reason_tbName.value = result[0][0];
    }
  }
}

function ShowDate()
{
	
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		var tcustomGetPolDate=PolGrid.getRowColDataByName(i,'customGetPolDate');
		
		document.getElementById('xx').value=tcustomGetPolDate;
		//document.getElementById('yy'),value=signDate;

	}
}	

function ShowSingDate()
{
	
	for(var i = 0; i < PolGrid.mulLineCount; i++)
	{
		var signDate=PolGrid.getRowColDataByName(i,'signDate');
		
		document.getElementById('yy').value=signDate;
	}
}