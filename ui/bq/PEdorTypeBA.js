//               该文件中包含客户端需要处理的函数和事件

var showInfo1;
var mDebug="1";

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPagePrem = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPageAcc = new turnPageClass();
var turnPagePolState = new turnPageClass();
var turnPagePol = new turnPageClass();

var mCurAmnt; //主险保额
var mAppMoney; //主险申请金额
var mChangeAmnt = false;

function returnParent()
{
	try
	{
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {}
}

function save()
{
  mChangeAmnt = false;
  for(var i=0; i<PolGrid.mulLineCount; i++)
  {
    var arrSelected = new Array();
    arrSelected[0] = new Array();
    arrSelected[0] = PolGrid.getRowData(i);
    setAppMoney(arrSelected,i);
  }
  
  if(!mChangeAmnt)
  {
    alert("调整后的基本保额未做改变");
    return false;
  }
  
  for(var j=0; j<PolGrid.mulLineCount; j++)
  {
    var arrSelected1 = new Array();
    arrSelected1[0] = new Array();
    arrSelected1[0] = PolGrid.getRowData(j);
    if(!checkAppMoney(arrSelected1))
    {
	  return false;
    }
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  returnParent();
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function setAppMoney(arrQueryResult,i)
{
  var arrResult = arrQueryResult;
  var riskcode = arrResult[0][3];
  var tCurAmnt = Number(arrResult[0][8]);
  var tAppMoney = Number(arrResult[0][9]);

  var isMainPol = false; //是否主险
  var sql = "select 1 from lmriskapp where riskcode = '" + riskcode + "' and risktype4 = '4' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    mCurAmnt = tCurAmnt;
    mAppMoney = tAppMoney;
  }
  if(tCurAmnt != tAppMoney)
  {
    mChangeAmnt = true;
  }
  if(tAppMoney > tCurAmnt )
  {
    PolGrid.setRowColDataByName(i,"DetailType","AA");
  }
  else
  {
    PolGrid.setRowColDataByName(i,"DetailType","PT");
  }
}

function checkAppMoney(arrQueryResult)
{
  var arrResult = arrQueryResult;
  var riskcode = arrResult[0][3];

  var tPrem = Number(fm.Prem.value);
  var tCurAmnt = Number(arrResult[0][8]);
  var tAppMoney = Number(arrResult[0][9]);
  
  if (tAppMoney < 10000)
  {
    alert("险种" + riskcode + "调整后的基本保额不能低于10000元");
    return false;
  }
  
  if ((tAppMoney % 1000)!= 0)
  {
    alert("险种" + riskcode + "基本保额必须为1000元的整数倍");
    return false;
  }
  else if((tAppMoney-tCurAmnt)%1000!=0)
  {
    alert("险种" + riskcode + "增加减少的风险保额需为1000元整数倍");
    return false;
  }
    
  var isMainPol = false; //是否主险
  var sql = "select 1 from lmriskapp where riskcode = '" + riskcode + "' and risktype4 = '4' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    isMainPol = true;
  }
  else
  {
    isMainPol = false;
  }
 
  if(isMainPol)
  {
    if(tAppMoney>tPrem*20)
    {
      alert("险种" + riskcode + "基本保额不能超过期缴保费的20倍");
      return false;  			
    }
    //增加保额校验
    if(tAppMoney > tCurAmnt )
    {
      if(!isLoanState())
      {    
        return false;
      }
      //校验鸿运天使少儿险保险期间须为“至被保险人75周岁”的
      if(!check333901(riskcode)){
    	  return false;
      }
      if(!checkPolYear(60,riskcode))
      {    
        return false;
      }
    }
  }
  else
  {
    if (tAppMoney > mAppMoney)
    {
      alert("附加险" + riskcode + "基本保额不能高于主险保额");
      return false;
    }
    //增加保额校验
    if(tAppMoney > tCurAmnt )
    {
      if(!isLoanState())
      {    
        return false;
      }
      
      if(!checkPolYear(55,riskcode))
      {    
        return false;
      }
    }
  }
  
  return true;
}

//校验鸿运天使少儿险保险期间须为“至被保险人75周岁”的
function check333901(riskcode){
	if(riskcode=='333901'){
		var checkInsureYear = "select 1 from lcpol where contno='"+fm.all('ContNo').value+"' and riskcode ='"+riskcode+"' and insuyearflag='A' and insuyear=75 with ur ";
		var rs = easyExecSql(checkInsureYear);
		if(rs==null||rs[0][0]!="1"){
			alert("鸿运天使少儿险的保险期间必须为“至被保险人75周岁”才能做保障调整的增加保额");
			return false;
		}
	}
	return true;
}

function initPolInfo()
{
  try
  {  
    var tContNo=fm.all('ContNo').value;
	var tSQL = "select Amnt,CValiDate,prem,insuredno from lcpol a where ContNo='"+tContNo+"' and polno=mainpolno "
	         +"and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') "; 
	turnPagePrem.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
	turnPagePrem.arrDataCacheSet = decodeEasyQueryResult(turnPagePrem.strQueryResult);
	    
    fm.all('CvaliDate').value = turnPagePrem.arrDataCacheSet[0][1];      
    fm.all('Prem').value = turnPagePrem.arrDataCacheSet[0][2];   
    fm.all('InsuredNo').value = turnPagePrem.arrDataCacheSet[0][3];   	        
  }
  catch(re)
  {
    alert("初始化险种信息错误!");
  }

	    
}

function initAccInfo()
{
	  try
  {
	   var tSQL="select InsuAccBala from lcinsureacc "
	           +"where contno='"+fm.all('ContNo').value+"'";   
	    turnPageAcc.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
	    turnPageAcc.arrDataCacheSet = decodeEasyQueryResult(turnPageAcc.strQueryResult);
      fm.all('AccBala').value = turnPageAcc.arrDataCacheSet[0][0]; 
  }
  catch(re)
  {
    alert("初始化帐户信息错误!");
  }	    
}

function initPayState()
{
	
	try
  {
  	  var tPolStateSQL="select (select a.codename from ldcode a where a.codetype='paystate' "
  	                  +"and a.code=value(p.StandbyFlag1,'0') ),p.paytodate,p.payintv ,p.payendDate from  lcpol p "
  	                  +"where  p.ContNo='"+fm.all('ContNo').value+"' and polno=mainpolno "
  	                  +"and exists (select 1 from lmriskapp where riskcode = p.riskcode and risktype4 = '4') ";      
      turnPagePol.strQueryResult  = easyQueryVer3(tPolStateSQL, 1, 0, 1);
	    turnPagePol.arrDataCacheSet = decodeEasyQueryResult(turnPagePol.strQueryResult);
	    fm.all('PayState').value =turnPagePol.arrDataCacheSet[0][0];  
	    fm.all('PayToDate').value =turnPagePol.arrDataCacheSet[0][1]; 
	    fm.all('PayEndDate').value =turnPagePol.arrDataCacheSet[0][3]; 
	    fm.PayIntv.value= turnPagePol.arrDataCacheSet[0][2];  
  }
  catch(re)
  {
  }	 
}
function initAppMoney()
{
  try
  {
  	  var sql = "select PolNo, EdorValue from LPEdorEspecialData where EdorAcceptNo='"+fm.all('EdorNo').value+"' and edortype='"+ fm.all('EdorType').value+"' ";
  	  var rs = easyExecSql(sql);
  	  if(rs)
  	  {
  	    for(var i=0; i<rs.length; i++)
  	    {
  	      for(var j=0; j<PolGrid.mulLineCount; j++)
  	      {
  	        var polno = PolGrid.getRowColDataByName(j,"PolNo");
  	        if(polno == rs[i][0])
  	        {
  	          PolGrid.setRowColDataByName(j,"AppMoney",rs[i][1]);
  	        }
  	      }
  	    }
  	  }
  	  
  }
  catch(re)
  {
    alert("初始化保障调整信息错误!");
  }	 
}

function isLoanState()
{
  
  var tPayToDate=fm.PayToDate.value;
  var tPayEndDate=fm.PayEndDate.value;
  if(tPayToDate == tPayEndDate)
  {
  	return true;
  }
  var tCurrDate=getCurrentDate();
  var tDayDiff=dateDiff(tPayToDate,tCurrDate,"D");
  //说明到目前保单已经欠费,且保单处于自动缓交 或者 缓交状态	
  if(tDayDiff>0)
  {	
   	alert("有保费未缴纳，不增加保额!");
    return false;    		
  }	
  
  return true;
}

//增加保额需满足条件：在被保险人60周岁对应的保单周年日之前申请，附加险55周岁。
function checkPolYear(year,riskcode)
{
  var tContNo = fm.ContNo.value;
  //取60周岁对应的保单周年日
  var sql = "select case when '2009' || substr(varchar(a.cvalidate), 5, 6) > '2009' || substr(varchar(a.insuredbirthday), 5, 6) "
          + "then substr(varchar(a.insuredbirthday + " + year + " years),1,4) || substr(varchar(a.cvalidate), 5, 6) "
 		  + "else substr(varchar(a.insuredbirthday + " + year + " years + 1 year),1,4) || substr(varchar(a.cvalidate), 5, 6) end "
 		  + "from lcpol a where contno = '" + tContNo + "' and mainpolno = polno "
 		  +"and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') ";      
  var result = easyExecSql(sql);
  var tPolDate = result[0][0];
  
  
  
  //保全申请日
  var sql1 = "select EdorAppDate from LPEdorApp where EdorAcceptNo = '" + fm.EdorNo.value + "' ";
  var result1 = easyExecSql(sql1);
  var tEdorAppDate = result1[0][0];  
  
  var tDayDiff=dateDiff(tEdorAppDate,tPolDate,"D"); 
  if(tDayDiff<0)
  {
    alert("已经超过被保人"+year+"周岁对应的保单周年日："+result[0][0]+"\n险种"+riskcode+"不能申请增加保额！");
  	return false;  
  }
  return true;
}

function inputImpart(){
	var tInsuredNo=fm.all('InsuredNo').value;
	var tEdorNo=fm.all('EdorNo').value;
	var tEdorType=fm.all('EdorType').value;
	var openUrl = "./PEdorHealthImpartMain.jsp?EdorNo=" + tEdorNo +
	              "&EdorType=" + tEdorType +
	              "&ContNo=" + fm.ContNo.value +
	              "&InsuredNo=" + tInsuredNo +
	              "&Flag=1";
	window.open(openUrl);
}

function queryPol()
{
  var sql = "select a.ContNo, a.PolNo, a.RiskSeqNo, a.RiskCode, " 
          + "(select RiskName from LMRisk where RiskCode = a.RiskCode), a.InsuredNo, a.InsuredName, a.Prem, " 
          + "a.Amnt, a.Amnt, " 
          + "(select EdorValue from LPEdorEspecialData where EdorNo = '" + fm.EdorNo.value + "' " 
          + "and EdorType = '" + fm.EdorType.value + "' and PolNo = a.PolNo) 类型 " 
          + "from LCPol a " 
          + "where a.ContNo = '" + fm.ContNo.value + "' and exists (SELECT RiskCode FROM LMRiskApp " 
          + "WHERE RiskCode = a.RiskCode AND (RiskType4 = '4' OR riskcode in (select code from ldcode where codetype='extralrisk'))) " 
          + "order by a.RiskSeqNo ";
  turnPage1.pageLineNum = 100;
  turnPage1.queryModal(sql, PolGrid);
}

