<%
//程序名称：GEdorTypeNSSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>
  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //个人批改信息
  System.out.println("---NS submit---");
    
  //后面要执行的动作：添加，修改
  String FlagStr = "";
  String Content = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
  
  String transact = request.getParameter("fmtransact");
  String EdorNo = request.getParameter("EdorNo");
  String EdorType = request.getParameter("EdorType");
  String GrpPolNo = request.getParameter("GrpPolNo");
  System.out.println("transact: " + transact); 
   
  //批改信息
  LPGrpEdorMainSchema tLPGrpEdorMainSchema = new LPGrpEdorMainSchema();
  tLPGrpEdorMainSchema.setEdorNo(EdorNo);
  tLPGrpEdorMainSchema.setGrpPolNo(GrpPolNo);
  tLPGrpEdorMainSchema.setEdorType(EdorType);
  
  VData tVData = new VData();
  tVData.add(tGlobalInput);
  tVData.add(tLPGrpEdorMainSchema);
  
  PGrpEdorNSDetailUI PGrpEdorNSDetailUI1 = new PGrpEdorNSDetailUI();
  if (!PGrpEdorNSDetailUI1.submitData(tVData, transact)) {
    VData rVData = PGrpEdorNSDetailUI1.getResult();
    System.out.println("Submit Failed! " + (String)rVData.get(0));
    Content = transact + "失败，原因是:" + (String)rVData.get(0);
    FlagStr = "Fail";
  }
  else {
    System.out.println("Submit Succed!");
    Content = "保存成功";
    FlagStr = "Success";
  }
%>   
                   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>

