//程序名称：OmnipotenceAcc.js
//程序功能：万能账户信息界面
//创建日期：2007-12-20
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var showInfo;
window.onfocus = myonfocus;
function test()
{
  alert(mPolNo);
  alert(mInsuAccNo);
}
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}

//查询万能帐户信息 包括：账户建立时间、账户当前金额、最近结算月份、最近结算时间
function queryAcc()
{
  /*
  var sql = "select a.AccFoundDate, a.InsuAccBala, "
          + "    ltrim(rtrim(char(year(b.DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(b.DueBalaDate - 1 day)))), "
          + "    b.MakeDate,DueBalaDate "
          + "from LCInsureAcc a , LCInsureAccBalance b "
          + "    where a.PolNo = '" + mPolNo + "' "
          + "        and  a.InsuAccNo = '" + mInsuAccNo + "' "
          + "        and  b.PolNo=a.PolNo and b.InsuAccNo = a.InsuAccNo "
          + "union all select a.AccFoundDate, a.InsuAccBala, "
          + "    ltrim(rtrim(char(year(b.DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(b.DueBalaDate - 1 day)))), "
          + "    b.MakeDate,DueBalaDate  "
          + "from LBInsureAcc a , LCInsureAccBalance b "
          + "    where a.PolNo = '" + mPolNo + "' "
          + "        and  a.InsuAccNo = '" + mInsuAccNo + "' "
          + "        and  b.PolNo=a.PolNo and b.InsuAccNo = a.InsuAccNo "
          + "    order by DueBalaDate desc ";
  var arrReturn = easyExecSql(sql);
  fm.all('AccFoundDate').value = arrReturn[0][0];
  fm.all('InsuAccBala').value = arrReturn[0][1];
  fm.all('BalaMonth').value = arrReturn[0][2];
  fm.all('BalaDate').value = arrReturn[0][3];
  */
  //拆了
  var sql = "select a.AccFoundDate, a.InsuAccBala "
          + "from LCInsureAcc a "
          + "where a.PolNo = '" + mPolNo + "' "
          + "and  a.InsuAccNo = '" + mInsuAccNo + "' "
          + "union all select a.AccFoundDate, a.InsuAccBala "
          + "from LBInsureAcc a "
          + "where a.PolNo = '" + mPolNo + "' "
          + "and  a.InsuAccNo = '" + mInsuAccNo + "' ";
  var arrReturn = easyExecSql(sql);
  if(arrReturn != null)
  {
    fm.all('AccFoundDate').value = arrReturn[0][0];
    fm.all('InsuAccBala').value = arrReturn[0][1];
  }
  
      sql = "select "
          + "ltrim(rtrim(char(year(b.DueBalaDate - 1 day))))||'-'||ltrim(rtrim(char(month(b.DueBalaDate - 1 day)))), "
          + "b.MakeDate,DueBalaDate "
          + "from  LCInsureAccBalance b "
          + "where b.PolNo = '" + mPolNo + "' "
          + "and  b.InsuAccNo = '" + mInsuAccNo + "' "
          + "order by DueBalaDate desc ";
  var arrReturn1 = easyExecSql(sql);
  if(arrReturn1 != null)
  {        
    fm.all('BalaMonth').value = arrReturn1[0][0];
    fm.all('BalaDate').value = arrReturn1[0][1];
  }
  return true;
}

//查询帐户结束时间
function queryEndDate()
{
  var sql = "select b.EdorValiDate from LBPol a ,LPEdorItem b "
          + "    where a.PolNo = '" + mPolNo + "'"
          + "        and b.PolNo = a.PolNo and b.ContNo = a.ContNo ";
  var arrReturn = easyExecSql(sql);
  if(arrReturn != null)
  {
    fm.all('AccEndDate').value = arrReturn[0][0];
  }
  return true;
}

//查询帐户历史信息
function queryAccTrace()
{
  var sql = "select a.PayDate x, a.MoneyType y, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    (case when a.OtherType = '6' " 
          + "             then ltrim(rtrim(char(year(a.PayDate - 1 day))))||'-'||ltrim(rtrim(char(month(a.PayDate - 1 day)))) "
          + "          else '' end), "
          + "    max(a.OtherNo), "
          + "    varchar(nvl((case when sum(a.Money) >= 0 then sum(a.Money) end),0)), "
          + "    varchar(nvl((case when sum(a.Money) <= 0 then abs(sum(a.Money)) end),0)), "
          + "    varchar(nvl((select sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and (PayDate < a.PayDate or (paydate=a.paydate and  moneytype<a.moneytype))), 0) + sum(a.Money)), "
          + "    max(a.SerialNo)  "
          + "from LCInsureAccTrace a, LCInsureAcc b "
          + "where a.PolNo = '" + mPolNo + "'" 
          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    	and a.serialno not in  (select cc.serialno from lcinsureacctrace cc where cc.PolNo = a.PolNo and riskcode='331201' and moneytype='B' and cc.paydate in ( "
          + "   select dd.paydate from lcinsureacctrace dd where dd.PolNo = a.PolNo and dd.makedate='2016-01-01' )) "
          + "    and a.OtherType != '1' "
          + "    group by a.PayDate, a.MoneyType, a.OtherType, b.LastAccBala, a.InsuAccNo, a.PolNo "
          
          + " union all select a.PayDate x, a.MoneyType y, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    (case when a.OtherType = '6' " 
          + "             then ltrim(rtrim(char(year(a.PayDate - 1 day))))||'-'||ltrim(rtrim(char(month(a.PayDate - 1 day)))) "
          + "          else '' end), "
          + "    max(a.OtherNo), "
          + "    varchar(nvl((case when sum(a.Money) >= 0 then sum(a.Money) end),0)), "
          + "    varchar(nvl((case when sum(a.Money) <= 0 then abs(sum(a.Money)) end),0)), "
          + "    varchar(nvl((select sum(Money) from LBInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and (PayDate < a.PayDate or (paydate=a.paydate and  moneytype<a.moneytype))), 0) + sum(a.Money)), "
          + "    max(a.SerialNo)  "
          + "from LBInsureAccTrace a, LBInsureAcc b "
          + "where a.PolNo = '" + mPolNo + "'" 
          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType != '1' "
          + "    group by a.PayDate, a.MoneyType, a.OtherType, b.LastAccBala, a.InsuAccNo, a.PolNo "
          
//          + "union all select a.PayDate, a.MoneyType, "
//          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
//          + "    (case when a.OtherType = '6' " 
//          + "             then ltrim(rtrim(char(year(a.PayDate - 1 day))))||'-'||ltrim(rtrim(char(month(a.PayDate - 1 day)))) "
//          + "          else '' end), "
//          + "    a.OtherNo, "
//          + "    varchar(nvl((case when a.Money >= 0 then a.Money end),0)), "
//          + "    varchar(nvl((case when a.Money <= 0 then abs(a.Money) end),0)), "
//          + "    varchar(nvl((select sum(Money) from LBInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
//          + "    a.SerialNo x "
//          + "from LBInsureAccTrace a, LBInsureAcc b "
//          + "where a.PolNo = '" + mPolNo + "'" 
//          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
//          + "    and a.PolNo = b.PolNo "
//          + "    and  a.InsuAccNo = b.InsuAccNo "
//          + "    and a.OtherType != '1' "
//          + "    group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "

          + "union all "
          
          //契约进帐户的轨迹
          + "select a.PayDate x, a.MoneyType y, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    '', "
          + "    a.OtherNo, "
          + "    varchar(nvl((case when a.Money >= 0 then a.Money end),0)), "
          + "    varchar(nvl((case when a.Money <= 0 then abs(a.Money) end),0)), "
          + "    varchar(nvl((select sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
          + "    a.SerialNo x "
          + "from LCInsureAccTrace a, LCInsureAcc b "
          + "where a.PolNo = '" + mPolNo + "'" 
          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType = '1' "
          + "group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "
          //契约进帐户的轨迹，B表
          + "union all select a.PayDate x, a.MoneyType y, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    '', "
          + "    a.OtherNo, "
          + "    varchar(nvl((case when a.Money >= 0 then a.Money end),0)), "
          + "    varchar(nvl((case when a.Money <= 0 then abs(a.Money) end),0)), "
          + "    varchar(nvl((select sum(Money) from LBInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
          + "    a.SerialNo x "
          + "from LBInsureAccTrace a, LCInsureAcc b "
          + "where a.PolNo = '" + mPolNo + "'" 
          + "    and a.InsuAccNo = '" + mInsuAccNo + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType = '1' "
          + "group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "
//          + "order by a.PayDate, a.MoneyType ";          
          + "order by x,y ";
  turnPage2.pageDivName = "divPage2";
  turnPage2.queryModal(sql, OmnipotenceAccTraceGrid);
  return true;
}

