<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorManuUWCho.jsp
//程序功能：保全个人人工核保
//创建日期：2002-09-24 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.bq.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
//1-得到所有纪录，显示纪录值
  int index=0;
  int TempCount=0;
  //输出参数
  CErrors tError = null;
  String FlagStr = "Succ";
  String Content = "";
  
  GlobalInput tGlobalInput = new GlobalInput();
  
  tGlobalInput=(GlobalInput)session.getValue("GI");	  
  if(tGlobalInput == null) {
	out.println("session has expired");
	return;
  }
  System.out.println("GlobalInput.Operator:"+tGlobalInput.Operator);
  
  String tEdorNo = request.getParameter("EdorNo");
  String tContNo = request.getParameter("ContNo");
  String tMissionID = request.getParameter("MissionID");
  String tSubMissionID = request.getParameter("SubMissionID");
  
  
  //保单表 
  LCContSchema mLCContSchema = new LCContSchema();
  LPUWMasterMainSchema mLPUWMasterMainSchema = new LPUWMasterMainSchema();
  if(tEdorNo != null && tEdorNo != "")//如果不是空纪录	
  {
  			//查询并显示单条信息
    		LCContSchema tLCContSchema = new LCContSchema();
    		tLCContSchema.setContNo(tContNo);
    		
    		// 准备传输数据 VData2
			VData tVData2 = new VData();			
			mLPUWMasterMainSchema.setEdorNo(tEdorNo) ;
			mLPUWMasterMainSchema.setContNo("00000000000000000000");
			tVData2.addElement(mLPUWMasterMainSchema);
			// 准备传输数据 VData
			VData tVData = new VData();
			tVData.addElement(tLCContSchema);
			// 数据传输
  			ProposalQueryUI tProposalQueryUI   = new ProposalQueryUI();
  			LPUWMasterMainQueryUI tLPUWMasterMainQueryUI = new LPUWMasterMainQueryUI();  
			if (!tProposalQueryUI.submitData(tVData,"QUERY||MAIN"))
			{
				Content = " 保全人工核保查询失败，原因是: " + tProposalQueryUI.mErrors.getError(0).errorMessage;
      			FlagStr = "Fail";
			}
			else if (!tLPUWMasterMainQueryUI.submitData(tVData2,"QUERY||MAIN"))
			{
				Content = " 保全人工核保查询失败，原因是: " + tLPUWMasterMainQueryUI.mErrors.getError(0).errorMessage;
				FlagStr = "Fail2";
				System.out.println("查询失败LPUWMasterMainQueryUI");
			}
			else
			{
				tVData.clear();
				tVData = tProposalQueryUI.getResult();
		        tVData2 = tLPUWMasterMainQueryUI.getResult();
				// 显示
				LCPolSet mLCPolSet = new LCPolSet(); 
				LPUWMasterMainSet mLPUWMasterMainSet = new LPUWMasterMainSet();
				mLCPolSet.set((LCPolSet)tVData.getObjectByObjectName("LCPolSet",0));
				mLPUWMasterMainSet.set((LPUWMasterMainSet)tVData2.getObjectByObjectName("LPUWMasterMainSet",0));				
				if (mLCPolSet.size() == 1)
				{
					mLCContSchema = mLCPolSet.get(1);
				}
				if (mLPUWMasterMainSet.size() == 1)
				{
					mLPUWMasterMainSchema = mLPUWMasterMainSet.get(1);
				}
			
				if(mLCContSchema.getContNo().equals(mLCContSchema.getMainContNo()))
				{
%>
<html>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="PEdorManuUW.js"></SCRIPT>
</head>
<script language="javascript">
showAddButton();
</script>
</html>			
<%
				}
				else
				{
%>
<html>
<head>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="PEdorManuUW.js"></SCRIPT>
</head>
<script language="javascript">
hideAddButton();
</script>
</html>				
<%
				}									
%>
<script language="javascript">
		parent.fraInterface.fm.ProposalNo.value="<%=mLCContSchema.getContNo()%>";
		parent.fraInterface.fm.RiskCode.value="<%=mLCContSchema.getRiskCode()%>";
		parent.fraInterface.fm.RiskVersion.value="<%=mLCContSchema.getRiskVersion()%>";
		parent.fraInterface.fm.ManageCom.value="<%=mLCContSchema.getManageCom()%>";
		parent.fraInterface.fm.AppntNo.value="<%=mLCContSchema.getAppntNo()%>";
		parent.fraInterface.fm.AppntName.value="<%=mLCContSchema.getAppntName()%>";
		parent.fraInterface.fm.InsuredNo.value="<%=mLCContSchema.getInsuredNo()%>";
		parent.fraInterface.fm.InsuredName.value="<%=mLCContSchema.getInsuredName()%>";
		parent.fraInterface.fm.InsuredSex.value="<%=mLCContSchema.getInsuredSex()%>";
		parent.fraInterface.fm.Mult.value="<%=mLCContSchema.getMult()%>";
		parent.fraInterface.fm.Prem.value="<%=mLCContSchema.getPrem()%>";
		parent.fraInterface.fm.Amnt.value="<%=mLCContSchema.getAmnt()%>";
		parent.fraInterface.fm.PrtNoHide.value="<%=mLCContSchema.getPrtNo()%>";
        parent.fraInterface.fm.UWGrade.value="<%=mLPUWMasterMainSchema.getUWGrade()%>";
        parent.fraInterface.fm.AppGrade.value="<%=mLPUWMasterMainSchema.getAppGrade()%>";
               
</script>
         
<%
			} // end of if
  			System.out.println("---2---"+FlagStr);
			//如果在Catch中发现异常，则不从错误类中提取错误信息
			if (!FlagStr.equals("Succ"))
			{   
			    System.out.println("---3---"+FlagStr);
			    if(FlagStr.equals("Fail3") )
				   tError = tEdorWorkFlowUI.mErrors;
				if(FlagStr.equals("Fail") )
				   tError = tProposalQueryUI.mErrors;
				if(FlagStr.equals("Fail2") )
				   tError = tLPUWMasterMainQueryUI.mErrors;
				
				if (!tError.needDealError())
				{                          
			    		Content = " 查询成功! ";
			    		FlagStr = "Succ";
				}
				
			 	else                                                                           
 			 	{
 			   		Content = " 查询失败，原因是:" + tError.getFirstError();
  			  		FlagStr = "Fail";
  			 	}
 			}
 			
    		index=index+1;
}
   
%> 

<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html> 