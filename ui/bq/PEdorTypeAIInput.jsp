<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeAI.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeAIInit.jsp"%>
  
  <title>补发告知信息 </title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeAISubmit.jsp" method=post name=fm target="fraSubmit"> 
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
      	  <input class = "readonly" readonly name=EdorType>
        </TD>
     
        <TD class = title > 保单号 </TD>
        <TD class = input >
      	  <input class = "readonly" readonly name=PolNo>
        </TD>   
      </TR>
    </TABLE>   
    
    <table>
    	<tr>
          <td class=common>
	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
    	  </td>
    	  <td class= titleImg>
    	    告知信息
    	  </td>
    	</tr>
    </table>
    
    <Div  id= "divLCImpart1" style= "display: ''">
      <table  class= common>
        <tr  class= common>
    	  <td text-align: left colSpan=1>
	    <span id="spanImpartGrid" >
	    </span> 
	  </td>
	</tr>
      </table>
    </div>  
    
    <table class = common>
      <TR class= common>
        <TD  class= input width="26%"> 
       	  <Input class= common type=Button value="保存申请" onclick="edorTypeAISave()">
     	</TD>
     	<TD  class= input width="26%"> 
          <Input class= common type=Button value="取消" onclick="edorTypeAIReturn()">
     	</TD>
      </TR>
    </table>  
	
    <hr>
	
    <Input type=Button class= common value="返回" onclick="returnParent()">
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="ContType" name="ContType">  
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>

</html>