<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="./PEdorAppInput.js"></SCRIPT>
  <SCRIPT src="PEdor.js"></SCRIPT>     
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEdorAppInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./PEdorAppSave.jsp" method=post name=fm target="fraSubmit">
  <!-- 工单接口标志, 值为"TASK"表明工单系统调用 "TASKFRAME" 表明在框架里调用 -->
  <input type=hidden id="LoadFlag" name="LoadFlag">
  <Div  id= "divLPEdorApp" style= "display: ''">
		<TABLE class=common>
			<tr class=common>
				<td class=title colspan="7">
				  受理号
				</td>
				<td class= input>
				  <Input type="input" class="readonly" readonly name=EdorAcceptNo>
				</td>
				</td>
			</tr>         
		</TABLE>   
   </Div>    
   
    <Div  id= "divEdorAppInfo" style= "display: 'none'" >
      <TABLE class=common>
        <tr class=common>
          <td class=title >
              客户/保单号
          </td>
          <td class= input>
              <Input class="common"  name=OtherNo>
          </td>
          <td class=title >
              号码类型
          </td>
          <td class= input>
              <Input type="input" class="codeNo" name=OtherNoType value=1 CodeData="0|^1|个人客户号^3|个人保单号" ondblClick="showCodeListEx('111',[this,OtherNoTypeName],[0,1]);" onkeyup="showCodeListKeyEx('111',[this,OtherNoTypeName],[0,1]);"><Input type="input" class="codeName" name=OtherNoTypeName  readonly >
          </td>
          <td class=title>
              申请人姓名
          </td>
          <td class= input>
              <Input type="input" class="common" name=EdorAppName>
          </td>
        </tr>
        <tr class=common>
          <td class= title>
              申请方式
          </td>
          <td class= input >
              <Input type="input" class="codeNo" name=AppType CodeData="0|^1|客户上门办理^2|业务员代办^3|其它人代办^4|信函^5|电话申请^6|部门转办" ondblClick="showCodeListEx('AppType',[this,AppTypeName],[0,1]);" onkeyup="showCodeListKeyEx('AppType',[this,AppTypeName],[0,1]);"><Input type="input" class="codeName" name=AppTypeName readonly >
          </td>
          <TD  class= title>
              申请日期
          </TD>
          <TD  class= input colspan="4">
              <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate >
          </TD>  
        </tr>
     </TABLE> 
   </Div> 
   
	  <table>
	    <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCContGrid);">
	      </td>
	      <td class= titleImg>
	        客户投保信息
	      </td>
	    </tr>
	  </table>
    <Div  id= "divLCContGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLCContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>		
     <Div id="divPage" style= "display: 'none'">
			<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPageCont.firstPage();"> 
			<INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPageCont.previousPage();"> 					
			<INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPageCont.nextPage();"> 
			<INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPageCont.lastPage();">
		</Div>					
    </div>
    <table class = common>
      <TR>
        <TD  class= input> 
           <Input class=cssButton type=Button value="保单明细" onclick="viewContInfo()">
           <Input class=cssButton type=Button value="万能账户信息" name="omnipotenceAcc" id ="omnipotenceAcc" style="display: 'none'"  onclick="showOmAcc();">
        </TD>
      </TR>
    </table>
    
    <table>
      <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
	      </td>
	      <td class= titleImg>
	        保单详细信息
	      </td>
    	</tr>
    </table>
    <Div  id= "divLCPol" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=ContNo1 >
          </TD>
          <TD  class= title>
            投保人
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AppntName >
          </TD>
          <TD  class= title>
            投保人客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AppntNo1 >
          </TD>
        </TR>        
        <TR>
           <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AppntSex >
          </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AppntIDType >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AppntIDNo >
          </TD>
          
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Phone >
          </TD>
          <TD  class= title>
            手机
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Mobile >
          </TD>           
          <TD  class= title>
            电子邮件
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=EMail >
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=PostalAddress >
          </TD>
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=ZipCode >
          </TD>           
          <TD  class= title>
            缴费方式
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=PayMode >
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title>
            银行编码
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=BankCode >
          </TD>
          <TD  class= title>
            银行帐号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=BankAccNo >
          </TD>           
          <TD  class= title>
            银行帐户名
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AccName >
          </TD>       
        </TR>
      </table>
    </Div>
    <table>
      <tr>
        <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
        </td>
        <td class= titleImg>
            保单险种信息
        </td>
      </tr>
    </table>
    <Div  id= "divPolGrid" style= "display: ''">
      <table  class= common>
      	<tr  class= common>
      		<td text-align: left colSpan=1>
    			<span id="spanPolGrid" >
    			</span> 
    	  	</td>
      	</tr>
      </table>					
    </div>
		<Div  id= "divLPGrpPol" style= "display: ''">
			<table  class= common>
				<tr  class= common>
					<TD  class= title>批改类型</TD>
					<TD  class= input>
						<Input class= "codeNo" name=EdorType verify="批改类型|notnull&code:EdorCode" ondblclick="initEdorType(this);" onkeyup="initEdorType(this);" ><Input class= "codeName" name=EdorTypeName readonly >
					</TD>
					<TD class= title>
					    保全生效日期
					</TD>
					<TD  class= input>
					    <Input class= "coolDatePicker" dateFormat="short" name=EdorValiDate >
					</TD>
					<TD  class= title>
						操作员
					</TD>
					<TD  class= input>
						<Input class= "readonly" readonly name=Operator>
					</TD>
				</tr>	
			</table>
		</div>
	<Div  id= "divInsuredInfo" style= "display: 'none'">
	  <table>
			<tr>
			  <td>
			   <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divInsuredGrid);">
			  </td>
				<td class= titleImg>
				    客户信息
				</td>
			</tr>
	  </table>
    <Div  id= "divInsuredGrid" style= "display: ''">
      <table  class= common>
        <tr  class= common>
         <td text-align: left colSpan=1>
          <span id="spanInsuredGrid" >
          </span> 
        </td>
        </tr>
      </table>					
     </div>    
    </Div>
    <table class= common> 
    	<tr>
    		<td> 
    			<input type =button class=cssButton value="添加保全项目" onclick="addRecord();">
    			<input type =button class=cssButton value="删除保全项目" onclick="delRecord();"> 
    		</td>
    	</tr>
    </table>

    <table>
	    <tr>
	     <td>
	      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEdorItemGrid);">
	     </td>
	     <td class= titleImg>
	      保全项目信息
	     </td>
	    </tr>
    </table>
    <Div  id= "divEdorItemGrid" style= "display: ''">
      <table  class= common>
      	<tr  class= common>
	    		<td text-align: left colSpan=1>
	  			<span id="spanEdorItemGrid" >
	  			</span> 
	  	  	</td>
      	</tr>
      </table>					   
    </div> 
    <div id = "divappconfirm" style = "display:''">
    <table class = common>
      <TR>
        <TD  class= input> 
        		<Input type =button class=cssButton value="保全项目明细" onclick="detailEdor();">	
            <Input class=cssButton type=Button value="保全理算" onclick="edorAppConfirm()">
        </TD>
      </TR>
    </table>
    </div>
			<input type=hidden id="currentDay" name="currentDay">
			<input type=hidden id="dayAfterCurrent" name="dayAfterCurrent">
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="Transact" name="Transact">
			<input type=hidden id ="ContType" name = "ContType">
			<input type=hidden id ="EdorState" name = "EdorState">
			<input type=hidden id ="GrpContNo" name = "GrpContNo">
			<input type=hidden id ="DisplayType" name = "DisplayType">
			<input type=hidden id ="AppntNo" name = "AppntNo">
			<input type=hidden id ="CustomerNoBak" name = "CustomerNoBak">
			<input type=hidden id ="ContNoBak" name = "ContNoBak">
			<input type=hidden id ="ContNo" name = "ContNo">
			<input type=hidden id ="EdorNo" name = "EdorNo">
			<input type=hidden id ="DelFlag" name = "DelFlag">
			<input type=hidden id ="MakeDate" name = "MakeDate">
			<input type=hidden id ="MakeTime" name = "MakeTime">
			<!--added by suyanlin at 2007-11-19 15:06-->	  	       
			<input type=hidden id ="CValiDate" name = "CValiDate">		
			<input type=hidden id ="monAfterCurrent" name = "monAfterCurrent">  	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>