
<%@include file="../common/jsp/UsrCheck.jsp"%>
<% 
//程序名称：个险投保人变更
//程序功能：
//创建日期：2005-09-22
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeEW.js"></SCRIPT>
  <%@include file="PEdorTypeEWInit.jsp"%>
  <link href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <link href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>上海医保账户微信平台保全项目变更 </title> 
</head>
<body onload="initForm();">
	<form action="./PEdorTypeEWSubmit.jsp" method=post name=fm target="fraSubmit">  
   <table class=common>
   		<tr class= common> 
	      <td class="title"> 受理号 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=EdorNo >
	      </td>
	      <td class="title"> 批改类型 </td>
	      <td class="input">
	      	<input class="readonly" type="hidden" readonly name=EdorType>
	      	
	      	<input class="readonly" readonly name=EdorTypeName>
	      </td>
	      <td class="title"> 保单号 </td>
	      <td class="input">
	      	<input class = "readonly" readonly name=ContNo>
	      </td>   
	           
	      <td class="input">
	      	<input class = "readonly" readonly name=CustomerNo type="hidden" >     	
	      </td>  
	      
	         <td class="input">
	      	<input class = "readonly" readonly name=prtno type="hidden">     	
	      </td> 
	       
    	</tr>
  	</table> 
  	<table>
	    <tr>
	      <td><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divCustomerInfo);"></td>
	      <td class= titleImg>变更前信息</td>
	    </tr>
   	</table>
   	
		<Div id="divCustomerInfo" style="display: ''">
       <table  class= common>
      	<tr  class= common>
          <td class= title>
            医保卡号
          </td>
          <td class= input>
            <Input class= common name=Name elementtype="nacessary" >
          </td>

				  <td class= title>
				   是否自动续保
				  </td>			 
				<td class= input>
					<Input class="codeNo"  name="IfAutoPay" CodeData="0|^0|否^1|是" ondblclick="return showCodeListEx('IfAutoPay',[this,IfAutoPayName],[0,1]);" onkeyup="return showCodeListEx('IfAutoPay',[this,IfAutoPayName],[0,1]);"><Input class="codeName" name="IfAutoPayName"  readonly>
				</td>
				<td class= title>
					  交费方式
					</td>    
					<td class= input>
					  <Input class="codeNo" name="PayMode" CodeData="0|^1|医保账户优先，不足银行卡全额扣款^2|仅医保卡扣款^3|仅银行卡扣款" ondblclick="return showCodeListEx('PayMode',[this,PayModeName],[0,1]);" onkeyup="return showCodeListKeyEx('PayMode',[this,PayModeName],[0,1]);"><Input class="codeName" name="PayModeName"  readonly>
					</td>
			  </tr>
			  

  </table>
    </DIV>
      
 <Div  id= "divLPAppntIndDetail" style= "display: ''">
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
        更新后的信息
      </td>
   </tr>
   </table>
  	<Div  id= "divDetail" style= "display: ''">
      <table  class= common>
 	<tr  class= common>
          <td class= title>
            医保卡号
          </td>
          <td class= input>
            <Input class= common name="Name1" elementtype="nacessary" >
          </td>

				  <td class= title>
				   是否自动续保
				  </td>
				 
				<td class= input>
				    <Input class="codeNo"  name="IfAutoPay1" verify="是否自动续保|notnull&code:IfAutoPay1" CodeData="0|^0|否^1|是" ondblclick="return showCodeListEx('IfAutoPay1',[this,IfAutoPayName1],[0,1]);" onkeyup="return showCodeListEx('IfAutoPay1',[this,IfAutoPayName1],[0,1]);"><Input class="codeName" name="IfAutoPayName1"  readonly>
				</td>
				<td class= title>
					  交费方式
					</td>    
					<td class= input>
					  <Input class="codeNo" name="PayMode1" CodeData="0|^1|医保账户优先，不足银行卡全额扣款^2|仅医保卡扣款^3|仅银行卡扣款"  ondblclick="return showCodeListEx('PayMode1',[this,PayModeName1],[0,1]);" onkeyup="return showCodeListEx('PayMode1',[this,PayModeName1],[0,1]);"><Input class="codeName" name="PayModeName1"   readonly>
					</td>
					
					 
			  </tr>
	 
	</table>
	<div  id= "divLCCont" style= "display: ''">

	</div>
	</DIV> 
	<br>
 
<div>
	<Input name=save class= cssButton type=Button value="保  存" onclick="edorTypeEWSave()">
	<Input name=cancel class= cssButton type=Button value="取  消" onclick="edorTypeADReturn()">
	<Input name=goBack type=Button class= cssButton value="返  回" onclick="returnParent()">
</div>
	 <input type=hidden id="AddressNo" name="AddressNo">
	 <input type=hidden id="PAddressNo" name="PAddressNo">	      	
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 
	 <input type=hidden name="Name2">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>