//程序功能：
//创建日期：2012/8/31
//创建人  ：LCY
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus; 
var turnPage = new turnPageClass();

var RelaGridTurnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	 if(showInfo!=null)
	 {
		   try
		   {
		     showInfo.focus();  
		   }
		   catch(ex)
		   {
		     showInfo=null;
		   }
	 }
}
//删除
function lmRateDelete(){
	if(fm.BonusYear.value!=tYear){
		alert("无法修改、保存或删除不是今年公布的数据！");
		return false;
	}
	if(!checkAfterJuly()){
		alert("已经过了7月1日,无法再进行修改或删除");
		return false;
	}
	var arrReturn = new Array();
	var tSel = BonusRateGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点删除按钮。" );
		return false;
	}
	var result = new Array();
	result[0] = new Array();
	result[0] = BonusRateGrid.getRowData(tSel-1);
		if(confirm("确定要删除所选记录吗？")){
			var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.fmtransact.value = "DELETE||MAIN";
			fm.action = "LMRiskBonusRateSave.jsp";
			fm.submit();
		}else{
			alert("您取消了删除操作");
		}
}
//查询
function LMRiskBonusRateQuery(){
	// 书写SQL语句
	
	var strSQL = "select riskcode,BonusYear,appYear,InsuYears,PayYears,MinAge,MaxAge,HLRate from LMRiskBonusRate where 1=1";
	
	if(fm.RiskCode.value != ""){
		strSQL = strSQL +" and RiskCode="+"'"+fm.RiskCode.value+"'";
	}
	if(fm.BonusYear.value != ""){
		strSQL = strSQL + " and BonusYear="+"'"+fm.BonusYear.value+"'";
	}
	if(fm.AppYear.value != ""){
		strSQL = strSQL + " and appYear="+"'"+fm.AppYear.value+"'";
	}
	if(fm.InsuYears.value != ""){
		var rateNum = parseInt(fm.InsuYears.value);
		strSQL = strSQL + " and InsuYears="+rateNum;
	}
	if(fm.PayYears.value != ""){
		var rateNum = parseInt(fm.PayYears.value);
		strSQL = strSQL + " and PayYears="+rateNum;
	}
	if(fm.MinAge.value != ""){
		var rateNum = parseInt(fm.MinAge.value);
		strSQL = strSQL + " and MinAge="+rateNum;
	}
	if(fm.MaxAge.value != ""){
		var rateNum = parseInt(fm.MaxAge.value);
		strSQL = strSQL + " and MaxAge="+rateNum;
	}
	if(fm.HLRate.value != ""){
		var rateNum = parseFloat(fm.HLRate.value);
		strSQL = strSQL + " and HLRate="+rateNum;
	}
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	BonusRateGrid.clearData();
        alert("未查询到满足条件的数据！");
        return false;
    }
    turnPage.pageDivName="divturnPage";
    turnPage.queryModal(strSQL, BonusRateGrid);
}
//整数校验
function checkInt(obj){
	var year = obj.getAttribute("value");
	if(year != ""){
		if(/^(-|\+)?\d+$/.test(year)){
			;
		}else{
			window.alert("输入必须是整数");
		}
	}
}
//整数校验
function checkIntZero(obj){
	var year = obj.getAttribute("value");
	if(year != ""){
		if(/^(-|\+)?\d+$/.test(year)){
			;
		}else{
			window.alert("输入必须是整数");
		}
	}
	if(year<=0){
		window.alert("保险期间或缴费期间输入必须大于0");
	}else{
		;
	}
}
//年度校验
function  checkYear(obj){
	window.alert(obj.getAttribute("value"));
	var year = obj.getAttribute("value");
	if(year != ""){
		if( /^\d{4}$/.test(year)){
			;
		}else{
			window.alert("年份应是四位数字");
		}
	}
}
//累积生息利率校验
function checkRate(){
	var num=document.getElementById("HLRate").value;
	  //判断输入的是不是数字
	 if(num != "") {
	     if(/^(\+|-)?\d+($|\.\d+$)/.test(num)) {
	        var numInt=parseInt(num);
	        if(numInt < 0 || numInt > 1){
	           window.alert("分红率必须是0到1之间的数字"); 
	           return false;  
	        }      
	     }else{
	          window.alert("您输入的不是数字！"); 
	          return false; 
	     } 
     }
	return true;
}
//修改
function LMRiskBonusRateUpdate(){
	if(fm.BonusYear.value!=tYear){
		alert("无法修改、保存或删除不是今年公布的数据！");
		return false;
	}
	if(!checkAfterJuly()){
		alert("已经过了7月1日,无法再进行修改或删除");
		return false;
	}
	var arrReturn = new Array();
	var tSel = BonusRateGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点修改按钮。" );
		return false;
	}
	var result = new Array();
	result[0] = new Array();
	result[0] = BonusRateGrid.getRowData(tSel-1);
	if(!beforeSubmit()){
		return false;
	}
	if (confirm("您确实想修改该记录吗?"))
	  {
	  var i = 0;
	  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.fmtransact.value = "UPDATE||MAIN";
	  fm.action = "LMRiskBonusRateSave.jsp";
	  fm.submit(); //提交
	  }
	  else
	  {
	    alert("您取消了修改操作！");
	  }
}
//提交前的校验
function beforeSubmit(){
	var reg=/^\d*\.\d*$/;
	if(fm.RiskCode.value=="" ||fm.BonusYear.value==""|| fm.AppYearAfter.value=="" || fm.AppYear.value=="" || fm.InsuYears.value==""||
		fm.PayYears.value=="" || fm.HLRate.value=="" || fm.MinAge.value=="" || fm.MaxAge.value==""){
		alert("请输入必填信息！");
		return false;
	}
	if(fm.BonusYear.value<fm.AppYearAfter.value){
		alert("保单承保年度不能大于利率公布年度!");
		return false;
	}
	if(fm.AppYear.value>fm.AppYearAfter.value){
		alert("利率公布年度起期不能大于利率公布年度止期!");
		return false;
	}
	return true;
}
//保存
function LMRiskBonusRateSave(){
	if(fm.BonusYear.value!=tYear){
		alert("无法修改、保存或删除不是今年公布的数据！");
		return false;
	}
	if(!beforeSubmit()){
		return false;
	}
	//数据类型转换
	var insuYears = parseInt(fm.InsuYears.value);
	var payYears = parseInt(fm.PayYears.value);
	var AppYear = parseInt(fm.AppYear.value);
	var AppYearAfter = parseInt(fm.AppYearAfter.value);
	var minAge = parseInt(fm.MinAge.value);
	var maxAge = parseInt(fm.MaxAge.value);
	if(minAge > maxAge){
	  window.alert("最小投保年龄不能大于最大投保年龄");
	  return false;
	}
	for(i=AppYear;i<=AppYearAfter;i++){
		//校验年龄段是否交叉
		var str = "select riskcode,BonusYear,appYear,InsuYears,PayYears from LMRiskBonusRate where riskcode = '"+
			fm.RiskCode.value +"' and BonusYear='"+fm.BonusYear.value+"' and AppYear='"+i+"' and InsuYears="+insuYears+
			" and PayYears="+payYears+" fetch first 1 rows only";
		turnPage.strQueryResult  = easyQueryVer3(str, 1, 1, 1);  
	    if (!turnPage.strQueryResult ) {//录入的第一条数据
	    	if(minAge != 0){
		    	alert("承保日期为"+i+"对应的最小投保年龄必须为0！");
		        return false;
	    	}
	        
	    }else{  
		    var str2 = "select max(MaxAge) from LMRiskBonusRate where riskcode = '"+fm.RiskCode.value +
		    "' and BonusYear='"+fm.BonusYear.value+"' and AppYear='"+i+"' and InsuYears="+
		    insuYears+" and PayYears="+payYears;
		    var results = easyExecSql(str2);
		    var intResult = parseInt(results[0]);
		    //window.alert(intResult);	
		    if(intResult != 0 && minAge != intResult+1){
		     window.alert("承保日期为"+i+"对应的最小投保年龄必须为"+(intResult+1));
		     return false;
		    }	
	    }
	}
    	
	if(confirm("确认保存吗？")){	
		fm.fmtransact.value="INSERT||MAIN";
		var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.action = "LMRiskBonusRateSave.jsp";
		fm.submit();
	}else{
		alert("取消保存！");
	}
	
}
//操作后查询
function queryAgain(){
	var strSQL = "select riskcode,BonusYear,appYear,InsuYears,PayYears,MinAge,MaxAge,HLRate from LMRiskBonusRate where BonusYear='"+tYear+"'";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	BonusRateGrid.clearData();
        alert("未查询到满足条件的数据！");
        return false;
    }
    turnPage.pageDivName="divturnPage";
    turnPage.queryModal(strSQL, BonusRateGrid);

}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr,content,transact){

	if (transact == "DELETE||MAIN")
	{
	    if (FlagStr != "Fail" )
	    {
	        content = "删除成功";
	    }
	}
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		//执行下一步操作
	}
	queryAgain();
}

function checkAfterJuly(){
	 var sql = "select 1 from dual where month(current date)>=7 with ur";
	  if(easyExecSql(sql)==null){
		  return true;
	  }else{
		  return false;
	  }
}

//将字符串日期转换成日期格式
function strToDate(str)
{
  var arys= new Array();
  arys=str.split('-');
  var newDate=new Date(arys[0],arys[1],arys[2]);  
  return newDate;
}  
//当选择一条记录时，在页面显示其具体信息
function ShowDetail(){
	var arrReturn = getSelectedResult();
  	afterSelected(arrReturn);
}
function getSelectedResult(){
	var arrSelected = null;
	tRow =BonusRateGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	{
	  return arrSelected;
	}
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = BonusRateGrid.getRowData(tRow-1);
	return arrSelected;
}	
function afterSelected(arrSelectedResult){
	
	
	var arrResult = new Array();	
	if(arrSelectedResult!= null )
	{
	  arrResult = arrSelectedResult;
	  fm.RiskCode.value = arrResult[0][0];
	  fm.BonusYear.value = arrResult[0][1];
	  fm.AppYear.value = arrResult[0][2];
	  fm.AppYearAfter.value = arrResult[0][2];
	  fm.InsuYears.value = arrResult[0][3];
	  fm.PayYears.value = arrResult[0][4];
	  fm.MinAge.value = arrResult[0][5];
	  fm.MaxAge.value = arrResult[0][6];
	  fm.HLRate.value = arrResult[0][7];
	  
	  var sql = "select riskcode,riskname from lmriskapp where riskcode = '"+fm.RiskCode.value+"' with ur";
	  var riskname = easyExecSql(sql);
	  if(fm.RiskCode.value!=""){
	  	fm.RiskName.value = riskname[0][1];
	  }
	  document.getElementById("RiskCode").readOnly=true;
	  document.getElementById("BonusYear").readOnly=true;
	  document.getElementById("AppYear").readOnly=true;
	  document.getElementById("AppYearAfter").readOnly=true;
	  document.getElementById("InsuYears").readOnly=true;
	  document.getElementById("PayYears").readOnly=true;
	  document.getElementById("MinAge").readOnly=true;
	  document.getElementById("MaxAge").readOnly=true;
	}
	
}