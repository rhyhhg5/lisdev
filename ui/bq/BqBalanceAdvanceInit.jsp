<%
//程序名称：BqBalanceAdvanceInit.jsp
//程序功能：实现保全结算提前结算
//创建日期：2006-03-10 15:10:36
//创建人  ：Huxl
//更新记录：更新人    更新日期     更新原因/内容

/*********************************************************************
 *  执行扫描的easyQueryClick
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 %>
 <script language="JavaScript">
 function iniForm()
 {
  try{			
  		iniBalancePolGrid();
  }
  catch(re)
  {
    alert("BqBalanceAdvanceInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
 }
 function iniBalancePolGrid()
 {
 	 var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=30;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="保单机构";         		//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=30;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="本期结算金额";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="处理状态";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[5]=new Array();
      iArray[5][0]="起始日期";         		//列名
      iArray[5][1]="0px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
             
      iArray[6]=new Array();
      iArray[6][0]="终止日期";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[7]=new Array();
      iArray[7][0]="结算日期";         		//列名
      iArray[7][1]="0px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[8]=new Array();
      iArray[8][0]="下一结算日";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许   
      
      iArray[9]=new Array();
      iArray[9][0]="结算频次";         		//列名
      iArray[9][1]="0px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=3;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[10]=new Array();
      iArray[10][0]="提前结算标志";         		//列名
      iArray[10][1]="0px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[11]=new Array();
      iArray[11][0]="已结算次数";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=30;            			//列最大值
      iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      BalancePolGrid = new MulLineEnter( "fm" , "BalancePolGrid" ); 
      //这些属性必须在loadMulLine前
      BalancePolGrid.mulLineCount = 10;   
      BalancePolGrid.displayTitle = 1;
      BalancePolGrid.locked = 1;
      BalancePolGrid.canSel = 1;
      BalancePolGrid.hiddenPlus=1;   
    	BalancePolGrid.hiddenSubtraction=1;
      BalancePolGrid.loadMulLine(iArray);        
      
      //这些操作必须在loadMulLine后面
      //GrpGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }   
 }
</script>