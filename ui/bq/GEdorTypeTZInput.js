//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var GrpContNo = "";
var mSwitch = parent.VD.gVSwitch;
var ImportPath;
var arrResult;
var specialRiskFlag = false;

//初始化险种信息和已录入信息
function initQuery()
{
	var strsql = "select edorstate from lpedorapp where edoracceptno = '" + fm.all("EdorNo").value + "'" ;
	var arrResult = easyExecSql(strsql,1,0);
	if(arrResult == "1"){
		statesql = "and state = '0' ";
	}
	else{
		statesql = "and state = '1' ";
	}
  var sql = "select InsuredId, State, InsuredName, Sex, Birthday, IdType, IDNo  " +
           "from LCInsuredList " +
           "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
           "and EdorNo = '" + fm.all("EdorNo").value + "' " + statesql +
           "order by Int(InsuredId) ";
	turnPage.queryModal(sql, InsuredListGrid); 
}

//按险种打印应收记录清单
function showList()
{
	window.open("../bq/GEdorTypeTZListPrint.jsp?edorno="+ fm.all("EdorNo").value);
}

//删除导入清单中的被保人
function deleteInsured()
{
		if (InsuredListGrid.mulLineCount == 0)
	{
		alert("删除列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < InsuredListGrid.mulLineCount; i++)
	{	
		if(InsuredListGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 0)
	{
		alert("请选择要删除的记录");
		return false;	
	}
	fm.action = "GrpTZDeleteSubmit.jsp"
	fm.submit();
}  


function getImportPath () {
  // 书写SQL语句
  var strSQL = "";

  strSQL = "select SysvarValue from ldsysvar where sysvar ='XmlPath'";

  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未找到上传路径");
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  ImportPath = turnPage.arrDataCacheSet[0][0];

}

function InsuredListUpload() {
  getImportPath();

  ImportFile = fm1.all('FileName').value;
  var prtno = fm.all("EdorNo").value;
  var tprtno = ImportFile;

  if ( tprtno.indexOf("\\")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("\\")+1);
  if ( tprtno.indexOf("/")>0 )
    tprtno =tprtno.substring(tprtno.lastIndexOf("/")+1);
  if ( tprtno.indexOf("_")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("_"));
  if ( tprtno.indexOf(".")>0)
    tprtno = tprtno.substring( 0,tprtno.indexOf("."));
alert(ImportPath);
  if ( prtno!=tprtno ) {
    alert("文件名与工单号不一致,请检查文件名!");
    return ;
  } else {
    var showStr="正在上载数据……";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm1.action = "../bq/BQDiskApplySave.jsp?ImportPath="+ImportPath+"&GrpContNo="+fm.all("GrpContNo").value+"&EdorNo="+fm.all("EdorNo").value;
    fm1.submit(); //提交
  }
}

//保存申请
function saveEdor()
{
	if (InsuredListGrid.mulLineCount == 0)
	{
		alert("没有有效的导入数据！");
		return false;
	}
	if(!checkInsuredListGrid())
    {
        return false;
    }
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
	fm.fmtransact.value = "CONTPLAN";
	fm.action = "GrpTZCheckSubmit.jsp?GrpContNo="+fm.all("GrpContNo").value+"&EdorNo="+fm.all("EdorNo").value+"&fmtransact="+fm.all("fmtransact").value;
	fm.submit();
}

//判断是否已有保障计划，如果姓名，性别，生日，证件类型，证件号码都一样被认为是同一客户
function checkContPlan(arrResult)
{
  //判断客户是否已有保险计划
	for (i = 0; i < arrResult.length; i++)
	{
		var insuredId = arrResult[i][0];
	  var insruedName = arrResult[i][1];
	  var sex = arrResult[i][3];
	  var birthday = arrResult[i][4];
	  sql = "select InsuredNo from LCInsured " +
			    "where GrpContNo = '" + fm.GrpContNo.value + "' ";
			    "and Name = '" + insruedName + "' " +
			    "and Sex = '" + sex + "' " +
			    "and Birthday = '" + birthday + "' " +
			    "and ContPlanCode is not null ";
		var arrResult2 = easyExecSql(sql, 1, 0);
		if (arrResult2)  //如果有客户已有保障计划
		{
			var insuredNo = arrResult2[0][0];
			var info = "被保人" + insruedName + "（客户号" + insuredNo + "）已有有效保障计划，是否自动删除？";
			if (confirm(info))
			{
				fm.InsuredId.value = insuredId;
				fm.action = "GrpNiInvalidSubmit.jsp";
				fm.submit();
			}
		}
	}
}

//判断客户资料是否存在
function checkInsured(arrResult)
{
	for (i = 0; i < arrResult.length; i++)
	{
		var insuredId = arrResult[i][0];
	  var insruedName = arrResult[i][1];
	  var sex = arrResult[i][2];
	  var birthday = arrResult[i][3];
	  var idType = arrResult[i][4];
	  var idNo = arrResult[i][5];
		sql = "select * from LCInsured " +
					"where Name = '" + insruedName + "' " +
					"and Sex = '" + sex + "' " +
					"and Birthday = '" + birthday + "' " +
					"union " +
					"select * from LCInsured " +
					"where IdNo = '" + idNo + "' ";
		var arrResult2 = easyExecSql(sql, 1, 0);
		if (arrResult2)  //如果有重复的客户信息
		{
			alert(arrResult2);
			window.open("");
			return false;
		}
	}
}

//回显校验结果(滥用js是一件很失败的事情,糟糕的架构)
function afterChecked(flag, paramArray)
{
	if (flag == "CONTPLAN") //保障计划
	{
		for (var i = 0; i < paramArray.length; i++)
		{
			var info = "被保人" + paramArray[i][1] + "（客户号" + paramArray[i][0] + "）已有有效保障计划，导入无效！";
			try { showInfo.close(); } catch(e) {};
			alert(info);
			var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  			var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  			showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			var insuredName = paramArray[i][1];
			var idType = paramArray[i][2];
			//var idNo = paramArray[i][3];
			var sex= paramArray[i][3];
			var birthday= paramArray[i][4];
								
			//得到InsuredId
			var sql = "select InsuredId from LCInsuredList " +
			          "where GrpContNo = '" + fm.GrpContNo.value + "' " +
			          "and InsuredName = '" + insuredName + "' " + 
			          "and Sex = '" + sex + "' " + 
			          "and birthday = '" + birthday + "' ";
			
			var arrResult = easyExecSql(sql,1,0);
						
			if (arrResult)
			{
				fm.InsuredId.value = arrResult[0][0];
				fm.action = "GrpNiInvalidSubmit.jsp";
				fm.submit();
			}
		}
		initQuery();
		fm.fmtransact.value = "WAITPERIOD";
		fm.action = "GrpNiCheckSubmit.jsp";
		fm.submit();
		return;
	}
	else if (flag == "WAITPERIOD") //险种等待期
	{
		//for (var i = 0; i < paramArray.length; i++)
		//{
		//	var info = paramArray[i][1] + "（险种代码" + paramArray[i][0] + " ）剩余保险期间不在等待期内，是否继续？";
		//	if (!confirm(info))
		//	{
		//		top.close();
		//	}
		//}
		//fm.fmtransact.value = "INSURED";
		//fm.action = "GrpNiCheckSubmit.jsp";
		//fm.submit();
		//return;
	}
	else if (flag == "INSURED")
	{
		window.open("GrpNiSameInsured.jsp");
		return;
	}
	else if (flag == "RELATION")
	{
		var rs ="";
		for (var i = 0; i < paramArray.length; i++)
		{
			rs += paramArray[i][0] + "号被保人"+paramArray[i][1]+"  ";
		}
		rs += "导入清单中与员工关系录入错误";
		fm.all("divErrorMes").innerHTML = "<font color='#FF0000' size='+1'>" + rs + "</font>";
		try { showInfo.close(); } catch(e) {};
		return;
	}
	else if (flag == "OOXX")
	{
		showInfo.close();
		var showStr = "万能增人不会产生费用，请调整模板后重新导入模板。";
  		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		return;
	}
		var sql = " select a.insuredid,a.contplancode,a.riskcode from lcinsuredlistpol a where "
						+ " a.riskcode not in (select riskcode from lccontplanrisk where grpcontno = a.grpcontno and contplancode = a.contplancode) "
						+ " and a.contplancode !='FM'  and a.grpcontno = '"+fm.GrpContNo.value+"'"
						;
		var error = easyExecSql(sql, 1, 0);
		if (error)  //如果有重复的客户信息
		{
			var rs ="";
			for (var i = 0; i < error.length; i++)
			{
				rs += error[i][0] + "号被保人,保障计划"+error[i][1]+"中导入了该保单中不存在的险种"+error[i][2]+"<br>";
			}
			fm.all("divErrorMes").innerHTML = "<font color='#FF0000' size='+1'>" + rs + "</font>";
			try { showInfo.close(); } catch(e) {};
			return ;
		}
	fm.action = "./GEdorTypeTZSubmit.jsp?GrpContNo="+fm.all("GrpContNo").value+"&EdorNo="+fm.all("EdorNo").value+"&EdorType="+fm.all("EdorType").value;
	fm.submit();
}

//显示提交结果
function afterSubmit(FlagStr, content)
{
	try { showInfo.close(); } catch(e) {};
	window.focus();
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showInfo = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showInfo = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		returnParent();
	}
}

function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmitDisk( FlagStr, content ) {
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  } else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
  initForm();
}



//2005-5-10增加,导入被保人清单调用
function importInsured()
{
	var strUrl = "TZDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + "&GrpContNo=" + fm.all("GrpContNo").value;
	showInfo = window.open(strUrl, "被保人清单导入", "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no");
}

//查询特殊数据
function querySpecialData()
{
  var sql = "select DetailType, EdorValue from LPEdorESpecialData " +
            "where EdorNo = '" + fm.EdorNo.value + "' " +
            "and EdorType = '" + fm.EdorType.value + "' " +
            "and PolNo = '000000'";
  var result = easyExecSql(sql);
  if (result == null)
  {
    return false;
  }
//  for (var i = 0; i < result.length; i++)
//  {
//    var detailType = result[i][0];
//    var edorValue = result[i][1];
//    if (detailType == "CREATEACCFLAG")
//    {
//      fm.CreateAccFlag.value = edorValue;
//    }
//    else if (detailType == "INPUTTYPE")
//    {
//      fm.InputType.value = edorValue;
//    }
//    else if (detailType == "SOURCE")
//    {
//      fm.Source.value = edorValue;
//    }
//    else if (detailType == "PREM")
//    {
//      fm.Prem.value = edorValue;
//    }
//    else if (detailType == "CALTYPE")
//    {
//      fm.CalType.value = edorValue;
//    }
//    else if (detailType == "REVERTTYPE")
//    {
//      fm.RevertType.value = edorValue;
//    }
//    else if (detailType == "CALTIME")
//  	{
//  		if (edorValue == "1")
//  		{
//  			fm.CalTime[0].checked = "true";
//  		}
//  		else if (edorValue == "2")
//  		{
//  			fm.CalTime[1].checked = "true";
//  		}
//  	}
//  }

  if (specialRiskFlag == true)
  {
    if (fm.CreateAccFlag.value == "1")
    {
       document.all("trSetAcc").style.display = "";
    }
    else if (fm.CreateAccFlag.value == "2")
    {
      document.all("trSetAcc").style.display = "none";
      fm.InputType.value = "";
      fm.InputTypeName.value = "";
      fm.Source.value = "";
      fm.SourceName.value = "";
      fm.Prem.value = "";
    }
    if (fm.InputType.value == "1")
    {
      document.all("tdPrem1").style.display = "";
      document.all("tdPrem2").style.display = "";
    }
    else if (fm.InputType.value == "2")
    {
      document.all("tdPrem1").style.display = "none";
      document.all("tdPrem2").style.display = "none";
      fm.Prem.value = "";
    }
  }
}

//20090327 zhanggm 校验新增被保人清单数据格式，如果格式不对不允许保存，生日
function checkInsuredListGrid()
{
    var content = "错误信息：<br>";
    var flagAll = "R";
    for (var i = 0; i < InsuredListGrid.mulLineCount; i++)
    {
      var name = InsuredListGrid.getRowColData(i, 3);
	  var sex = InsuredListGrid.getRowColData(i, 4);
	  var birthday = InsuredListGrid.getRowColData(i, 5);
	  var idType = InsuredListGrid.getRowColData(i, 6);
	  var info = name+"：";
	  var flag = "R";
	  if(sex=="")
	  {
	    info += "性别为空 ";
	    flag = "W";
	  }
	  else
	  {
	    var sql = "select 1 from ldcode where codetype = 'sex' and code = '" + sex + "' ";
	    if(!easyQueryVer3(sql))
	    {
	      info += "性别为非法字符串 ";
	      flag = "W";
	    }
	  }
	  if(birthday==null || birthday=="")
	  {
	    info += "生日为空 ";
	    flag = "W";
	  }
	  if(idType==null || idType=="")
	  {
	    info += "证件类型为空 ";
	    flag = "W";
	  }
	  else
	  {
	    var sql = "select 1 from ldcode where codetype = 'idtype' and code = '" + idType + "' ";
	    if(!easyQueryVer3(sql))
	    {
	      info += "证件类型为非法字符串 ";
	      flag = "W";
	    }
	  }
	  if(flag == "W")
	  {
	    content+=info;
	    content+="<br>";
	    flagAll = "W";
	  }
	}
	if(flagAll == "W")
	{
	  alert("导入的被保人清单数据不完整，见页面底部");
	  fm.all("ErrorsInfo").innerHTML = content;
	  return false;
	}
	return true;
}


//打开险种录入界面
function afterCodeSelect(cCodeName, Field) 
{
	if (InsuredListGrid.mulLineCount >= 500)
	{
		alert("超出该批次最大处理范围，请重新增加一个保全项目！");
		return;
	}

	if (cCodeName == "EdorRisk")
	{
		//showInfo = window.open("./ProposalMain.jsp?risk="+Field.value+"&prtNo="+prtNo+"&loadFlag=7");
	}
}