<%
//程序名称：PEdorTypeMFInit.jsp
//程序功能：
//创建日期：2016-11-23 16:54
//创建人  ：LC
//更新记录：  更新人    更新日期     更新原因/内容
%>                         

<script language="JavaScript">  

function initForm()
{
  try
  {
    initInpBox();
    initPolGrid();   
    getPolInfo(fm.all('ContNo').value);
    checkSelectPol();
    showTips();
   // chkPol();  
    //chkGBInfo(); 
  }
  catch(re)
  {
    alert("PEdorTypeMFInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
{                               
    var iArray = new Array();
      
    try
    {
       
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1] = "0px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "险种序号";
			iArray[1][1] = "60px";
			iArray[1][2] = 100;
			iArray[1][3] = 0;

			iArray[2] = new Array();
			iArray[2][0] = "险种名称";
			iArray[2][1] = "200px";
			iArray[2][2] = 100;
			iArray[2][3] = 0;

			iArray[3] = new Array();
			iArray[3][0] = "被保人";
			iArray[3][1] = "80px";
			iArray[3][2] = 100;
			iArray[3][3] = 0;

			iArray[4] = new Array();
			iArray[4][0] = "保额";
			iArray[4][1] = "80px";
			iArray[4][2] = 100;
			iArray[4][3] = 0;

			iArray[5] = new Array();
			iArray[5][0] = "档次";
			iArray[5][1] = "40px";
			iArray[5][2] = 100;
			iArray[5][3] = 0;

			iArray[6] = new Array();
			iArray[6][0] = "保费";
			iArray[6][1] = "80px";
			iArray[6][2] = 100;
			iArray[6][3] = 0;

			iArray[7] = new Array();
			iArray[7][0] = "险种状态";
			iArray[7][1] = "90px";
			iArray[7][2] = 100;
			iArray[7][3] = 0;

			iArray[8] = new Array();
			iArray[8][0] = "生效日期";
			iArray[8][1] = "90px";
			iArray[8][2] = 100;
			iArray[8][3] = 0;

			iArray[9] = new Array();
			iArray[9][0] = "保险期满日";
			iArray[9][1] = "90px";
			iArray[9][2] = 100;
			iArray[9][3] = 0;

			iArray[10] = new Array();
			iArray[10][0] = "交至日期";
			iArray[10][1] = "90px";
			iArray[10][2] = 100;
			iArray[10][3] = 0;

			iArray[11] = new Array();
			iArray[11][0] = "险种保单号";
			iArray[11][1] = "0px";
			iArray[11][2] = 100;
			iArray[11][3] = 0;
			iArray[11][21] = "polno";

			iArray[12] = new Array();
			iArray[12][0] = "被保人号";
			iArray[12][1] = "0px";
			iArray[12][2] = 100;
			iArray[12][3] = 0;

			PolGrid = new MulLineEnter("fm", "PolGrid");
			//这些属性必须在loadMulLine前
			PolGrid.mulLineCount = 0;
			PolGrid.displayTitle = 1;
			PolGrid.canChk = 1;
			PolGrid.hiddenPlus = 1; //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
			PolGrid.hiddenSubtraction = 1;
			PolGrid.loadMulLine(iArray);
			//这些操作必须在loadMulLine后面

		} catch (ex) {
			alert(ex);
		}
	}
</script>