<%
//程序名称：GEdorTypeACSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----ANsubmit---");
  LPGrpEdorItemSchema tLPGrpEdorItemSchema=new LPGrpEdorItemSchema();
  LPGrpAppntSchema tLPGrpAppntSchema=new LPGrpAppntSchema();
 // LPGrpSchema tLPGrpSchema=new LPGrpSchema();
 // LPGrpAddressSchema tLPGrpAddressSchema = new LPGrpAddressSchema();

  
  //LPGrpPolSchema tLPGrpPolSchema   = new LPGrpPolSchema();
 
//  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();

  //PGrpEdorANDetailUI tPGrpEdorANDetailUI   = new PGrpEdorANDetailUI();
  PGrpEdorANDetailUI tPGrpEdorANDetailUI  = new PGrpEdorANDetailUI();
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String fmAction = "";
  String Result="";
  String addrFlag="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  fmAction = request.getParameter("fmAction");
  addrFlag=request.getParameter("addrFlag");
  System.out.println("------fmAction:"+addrFlag);
	GlobalInput tG = new GlobalInput();
	    System.out.println("------------------begin ui");
	tG = (GlobalInput)session.getValue("GI");
	
//	tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
	tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
  tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
//	tLPGrpEdorItemSchema.setEdorAppNo(request.getParameter("EdorAppNo"));	
	
  
        //团单投保人信息  LCGrpAppnt
      tLPGrpAppntSchema.setEdorNo(request.getParameter("EdorNo"));
      tLPGrpAppntSchema.setEdorType(request.getParameter("EdorType"));
      tLPGrpAppntSchema.setGrpContNo(request.getParameter("GrpContNo"));     //集体投保单号码
	    tLPGrpAppntSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	
	    tLPGrpAppntSchema.setName(request.getParameter("GrpName"));



	

try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();  
  	
	 //保存个人保单信息(保全)
	 tVData.addElement(addrFlag);	
	 tVData.addElement(tG);
	 	
	 	tVData.addElement(tLPGrpEdorItemSchema);
  	tVData.addElement(tLPGrpAppntSchema);
    
    tPGrpEdorANDetailUI.submitData(tVData,fmAction);
	  
	  
	  
	}
	catch(Exception ex)
	{
		Content = fmAction+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPGrpEdorANDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (fmAction.equals("QUERY||MAIN")||fmAction.equals("QUERY||DETAIL"))
    	{
    	if (tPGrpEdorANDetailUI.getResult()!=null&&tPGrpEdorANDetailUI.getResult().size()>0)
    	{
    		Result = (String)tPGrpEdorANDetailUI.getResult().get(0);
    		if (Result==null||Result.trim().equals(""))
    		{
    			FlagStr = "Fail";
    			Content = "提交失败!!";
    		}
    	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

