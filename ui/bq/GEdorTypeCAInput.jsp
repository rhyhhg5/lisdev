<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<% 
//程序名称：GEdorTypeCAInput.jsp
//程序功能：团体保全明细总页面
//创建日期：2003-12-03 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeCA.js"></SCRIPT>
  <%@include file="GEdorTypeCAInit.jsp"%>
  
  <title>团体保全明细总页面</title> 
</head>

<body  onload="initForm();" >
  <form action="./GEdorTypeCASubmit.jsp" method=post name=fm target="fraSubmit">    
  
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=EdorType type=hidden>
        	<input class = "readonly" readonly name=EdorTypeName>
        </TD>
       
        <TD class = title > 集体保单号 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=GrpPolNo>
        </TD>   
      </TR>
    </TABLE> 
    
    <br>
    
    <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          转出帐户 <INPUT VALUE="查询" class = common TYPE=button onclick="queryClick();">
        </td>
      </tr>
    </table>
    
    <table class = common>
      <tr class = common>
        <td class = title>
                证件号码
        </td>
        <td class = input>
                <Input class = common name = IDNo1>
        </td>
        <td class = title>
                出生日期
        </td>
        <td class = input>
                <Input class = common name = Birthday1>
        </td>
        <td class = title>
      		客户姓名
      		</td>
      	<td class = input>
      		<input class = common  name=Name1>
          </TD>  
      </tr>
    </table>
    
    <Div  id= "divLPInsured" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage" align=center style= "display: 'none' ">
        <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
        <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 			
      </Div>		
  	</div>

	  <br><br>
	  
	  <hr><br> 
	  
	  <table class = common>
      <tr class = common>
        <td class = title>
      		<INPUT VALUE="转出（右边输入金额）" class = common TYPE=button onclick="OutputMoney();">
      		</td>
      	<td class = input>
      		<input class = common  name=OutMoney>
          </TD>	
      	<td class = title>
      		<INPUT VALUE="转入（右边输入金额）" class = common TYPE=button onclick="InputMoney();">
      		</td>
      	<td class = input>
      		<input class = common  name=InMoney>
          </TD>	
      	<td class = title>
      		转移剩余金额
      		</td>
      	<td class = input>
      		<input class = common readonly  name=SumMoney>
          </TD>

      </tr>
    </table>    
	  
	  <br>
	  
	  <hr> 
	  
	  <br>
	  <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          转入帐户 <INPUT VALUE="查询" class = common TYPE=button onclick="queryClick2();">
        </td>
      </tr>
    </table>
    
    <table class = common>
      <tr class = common>
        <td class = title>
                证件号码
        </td>
        <td class = input>
                <Input class = common name = IDNo2>
        </td>
        <td class = title>
                出生日期
        </td>
        <td class = input>
                <Input class = common name = Birthday2>
        </td>
        <td class = title>
      		客户姓名
      		</td>
      	<td class = input>
      		<input class = common  name=Name2>
          </TD>  
      </tr>
    </table>
    
	  <Div  id= "divLPInsured2" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsured2Grid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage2" align=center style= "display: 'none' ">
        <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">					
      </Div>
  	</div>
	  
	  <br>
	  
	  <!--Input type=Button value="进入个人保全" class = common onclick="PEdorDetail()">
	  <Input type=Button value="撤销个人保全" class = common onclick="cancelPEdor()">  
	  <Input type=Button value="返回" class = common onclick="returnParent()"-->
	  
	  <input type=hidden id="PolNo" name="PolNo">
	  <input type=hidden id="GetMoney" name="GetMoney">
	  <input type=hidden id="InsuredNo" name="InsuredNo">
	  <input type=hidden id="RiskCode" name="RiskCode">
	  <input type=hidden id="InsuAccNo" name="InsuAccNo">
	  <!-- add ContType for PEdor GT -->
	  <input type=hidden id="ContType" name="ContType">
	  <input type=hidden id="Transact" name="Transact">
	  <!-- add  for MA -->
	  <input type=hidden id="nOutCount" name="nOutCount">
	  <input type=hidden id="nInCount" name="nInCount">
	  
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>

<script>
  window.focus();
</script>
