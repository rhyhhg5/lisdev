<%
//程序名称：PEdorTypeAASubmit.jsp
//程序功能：
//创建日期：2005-04-13 16:49:22
//创建人  ：liuzhy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    //接收信息，并作校验处理
    System.out.println("-----Detail submit---");                 
    String FlagStr = "";
    String Content = "";
    String transact = "";
    String Result="";  
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
    transact=(String)request.getParameter("Transact");
             
    LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();  
	tLPEdorItemSchema.setPolNo(request.getParameter("PolNo"));
	tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
	tLPEdorItemSchema.setInsuredNo(request.getParameter("InsuredNo"));
	tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
    tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));    
    LPDutySchema tLPDutySchema   = new LPDutySchema();	
    if (transact.equals("INSERT||EDOR"))
    {  
        String tGetMoney=request.getParameter("GetMoney");
        String tDutyCode[] = request.getParameterValues("LCDutyGrid3"); 
        String tChk[] = request.getParameterValues("InpLCDutyGridSel");        
        System.out.println("tChk.length"+tChk.length);
        for(int index=0;index<tChk.length;index++)
        {
            if(tChk[index].equals("1"))
           {            
	            
	        tLPDutySchema.setDutyCode(tDutyCode[index]);
	        tLPDutySchema.setAmnt(tGetMoney);	     
	        }           
        }
    }
  	
  PEdorAADetailUI tPEdorAADetailUI   = new PEdorAADetailUI();
  try {
    // 准备传输数据 VData
  	    VData tVData = new VData();  
	 	tVData.addElement(tG);	 	
	 	tVData.addElement(tLPEdorItemSchema);
	 	tVData.addElement(tLPDutySchema);		 	
	 	
	 	//保存个人保单信息(保全)	
    tPEdorAADetailUI.submitData(tVData, transact);
	}
	catch(Exception ex) {
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="") {
    CErrors tError = new CErrors(); 
    tError = tPEdorAADetailUI.mErrors;
    
    if (!tError.needDealError()) {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	
    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))	{
      	if (tPEdorAADetailUI.getResult()!=null && tPEdorAADetailUI.getResult().size()>0) {
      		Result = (String)tPEdorAADetailUI.getResult().get(0);
      		
      		if (Result==null||Result.trim().equals(""))	{
      			FlagStr = "Fail";
      			Content = "提交失败!!";
      		}
      	}
    	}
    }
    else {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>");
</script>
</html>

