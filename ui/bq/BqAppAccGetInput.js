//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();


function afterCodeSelect( cCodeName, Field )
{
	try	
	{
		if( cCodeName == "paymode" )
		{
			controlTransferShow();
		}
	}
	catch( ex ) {
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  {
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    viewAppAccInfo();
  }  
}

/* 设置客户号,由客户查询页面返回时调用 */
function afterQuery(arrQueryResult)
{
	window.focus();
	var customerNo = arrQueryResult[0][0];
	
	fm.CustomerNo.value=customerNo;
	clearQuery();
	
}

//查询团险客户信息
function queryBasicGrpCustomerInfo(customerNo)
{
	var sql = "select a.customerNo, a.GrpName, a.BusinessType, a.GrpNature, a.Peoples, a.RgtMoney, a.Asset, " +
									"a.MainBussiness, a.Corporation, b.Fax1, a.Phone, a.Satrap, b.E_Mail1, a.FoundDate, " +
									"a.BankAccNo, a.State, b.linkMan1, addressNo " +
							"from LDGrp a, LCGrpAddress b " +
							"where a.customerNo='" + customerNo + "' and " +
								"b.customerNo='" + customerNo + "' " +
								"order by addressNo desc";		
	var result = easyExecSql(sql);
	if (!result)
	{
		return false;
	}
	fm.GGrpName.value = result[0][1];
	fm.GBusinessType.value = result[0][2];
	fm.GGrpNature.value = result[0][3];
	fm.GPeoples.value = result[0][4];
	fm.GRgtMoney.value = result[0][5];
	fm.GAsset.value = result[0][6];
	fm.GMainBussiness.value = result[0][7];
	fm.GCorporation.value = result[0][8];
	fm.GFax.value = result[0][9];
	fm.GPhone.value = result[0][10];
	fm.GSatrap.value = result[0][11];
	fm.GEMail.value = result[0][12];
	fm.GFoundDate.value = result[0][13];
	fm.GBankAccNo.value = result[0][14];
	fm.GState.value = result[0][15];
	return true;
}


//查询个人客户信息
function queryBasicCustomerInfo(cstmNo)
{
	var sql = " select customerNo, name, sex, marriage, birthday,workType,occupationCode, "
			  + "   (select codeName "
			  + "   from LDCode "
			  + "   where codeType='idtype' "
			  + "       and code=LDPerson.idtype), "
			  + "   IDNo, rgtAddress, grpNo "
			  + "from LDPerson "
			  + "where customerNo='" + cstmNo + "' ";
	var result = easyExecSql(sql);
	if(!result)
	{
		return false;
	}
	fm.all('Name').value= result[0][1];                            
	fm.all('Sex').value= (result[0][2] == "0" ? '男' : '女');
	fm.all('Marriage').value= result[0][3];                         
	fm.all('Birthday').value= result[0][4];
	fm.all('Occupation').value= result[0][6];
	fm.all('OccupationCode').value= result[0][6];
	fm.all('IDType').value= result[0][7];                        
	fm.all('IDNo2').value= result[0][8];      
	fm.all('RgtAddress').value= result[0][9];                                      
	fm.all('GrpNo').value= result[0][10]; 
  
	sql = "  select OccupationCode, trim(OccupationName)||'-'||workname, OccupationType "
	      + "from LDOccupation "
	      + "where OccupationCode = '" + fm.OccupationCode.value + "' "
	result = easyExecSql(sql);
	if (result)
	{
	   fm.Occupation.value = result[0][1];
	}
	return true;
}

//查询个险客户信息
function queryMoreCustomerInfo(customerNo)
{
	var strSQL =
			"select * from LCAddress "
			+ "where customerNo='" + fm.CustomerNo.value + "' "
			+ "and int(addressNo) = "
			+ "		(select max(int(addressNo)) "
			+ "		from LCAddress "
			+ "		where customerNo='" + fm.CustomerNo.value + "')";
	
	var arrResult = new Array();
    arrResult = easyExecSql(strSQL);
    fm.all('HomeAddress').value= arrResult[0][6];
    fm.all('ZipCode').value= arrResult[0][3];
    fm.all('PostalAddress').value= arrResult[0][7];
    fm.all('HomePhone').value= arrResult[0][8];
    fm.all('CompanyAddress').value= arrResult[0][10];
    fm.all('CompanyPhone').value= arrResult[0][12];
    fm.all('EMail').value= arrResult[0][16];
    fm.all('Mobile').value= arrResult[0][14];	    
}

/* 查询个单客户号 */
function queryCustomerNo()
{
	showInfo = window.open("../task/LDPersonQueryMain.jsp?");
}

/* 查询团单客户号 */
function queryGrpCustomerNo()
{	
	showInfo = window.open("../task/GroupMain.html");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}


	//打印批单
function PrtEdor()
{
	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
			
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击查看按钮。" );
		else
		{
			var state =PolGrid. getRowColData(tSel-1,6) ;
	        
			if (state=="正在申请")
				alert ("所选批单处于正在申请状态，不能打印！");
			else{
			  var EdorAcceptNo=PolGrid. getRowColData(tSel-1,1) ;
				
				window.open("../f1print/AppEndorsementF1PJ1.jsp?EdorAcceptNo="+EdorAcceptNo+"");
				//fm.target="f1print";	
			
			//	fm.submit();

			}
		}
}

function viewAppAccInfo()
{
	//选择团单客户时的操作
	if (fm.ContType.value == "1")
	{
		if (!queryBasicGrpCustomerInfo(fm.CustomerNo.value))
		{
			alert("未找到客户信息！");
			return false;
		}
	}
		
	if (fm.ContType.value == "0")
	{
   	if (!queryBasicCustomerInfo(fm.CustomerNo.value))
   	{
   		alert("未找到客户信息！");
			return false;
   	}
    //queryMoreCustomerInfo(fm.CustomerNo.value);
    //showCodeName(); //把编码转为汉字显示
	}
	clearQuery();
	queryAppAccInfo();
	queryAppAccTraceInfo();
}

function queryAppAccInfo()
{
	var strSQL = "select customerno,accfounddate,accfoundtime,accbala,accgetmoney,state," +
						"case state when '1' then '有效' when '0' then '无效' end case, operator" +
						" from lcappacc where customerno='"+fm.CustomerNo.value+"'";
	turnPage.queryModal(strSQL, AppAccGrid);
	if (AppAccGrid.mulLineCount > 0)
	{
		if (AppAccGrid.mulLineCount == 1)
		{
		  fm.AppAccGridSel.checked = "true";
		}
		else
		{
		  fm.AppAccGridSel[0].checked = "true";
		}
	}
	fm.AccGetMoney.value = AppAccGrid.getRowColData(AppAccGrid.getSelNo() - 1, 5);
}

function queryAppAccTraceInfo()
{
	var strSQL = "select customerno,case AccType when '0' then '抵扣或领取' when '1' then '转入' end case ,serialno," +
						"otherno,case othertype when '12' then '投保人客户号' when '10' then '个人批单号' "+
						"when '8' then '团体通知书号' when '6' then '个人通知书号' when '3' then '团体批单号' when '2' then '个人保单号' when '1' then '集体保单号'  end case," +
						"destsource,ldcode.codename,money,accbala,case state when '1' then '有效' when '0' then '无效' end case,confirmdate,confirmtime,operator" +
						" from lcappacctrace,ldcode where customerno='"+fm.CustomerNo.value+"' and ldcode.codetype='appaccdestsource' and lcappacctrace.destsource=ldcode.code " +
						"order by int(serialno) ";
	turnPage1.pageDivName = "divPage1";
	turnPage1.queryModal(strSQL, AppAccTraceGrid);
}

function queryAppAccGetTraceInfo(customerNo,serialNo)
{
	var strSQL = "select customerno,serialno,noticeno,accgetmoney,accbala,"+
				"appdate,name,idtype,idno,"+
				"case accgetmode when '1' then '现金' when '4' then '银行转帐' end case,"+
				"bankcode,bankaccno,accname,transferdate,accgetdate,operator "+
				"from lcappaccgettrace where customerno='"+customerNo+"' and serialno='"+serialNo+"'";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSQL, AppAccGetTraceGrid);
}

function showAppAccGetInfo()
{
	var tRow = AppAccTraceGrid.getSelNo();
	if (tRow == 0)
  {
  	fm.all("AppAccGetInfo").style.display='none';
    return false;
  }
  if(AppAccTraceGrid.getRowColData(tRow-1, 6)=='04')
	{
		fm.all("AppAccGetInfo").style.display='';
		var customerNo=AppAccTraceGrid.getRowColData(tRow-1, 1);
		var serialNo=AppAccTraceGrid.getRowColData(tRow-1, 3);
		queryAppAccGetTraceInfo(customerNo,serialNo);
    return false;
	}
	else
	{
		fm.all("AppAccGetInfo").style.display='none';
	}	
}

function clearForm()
{
	fm.reset();
	clearQuery();
}

//清空领取人信息
function clearAccGetInfo()
{
	fm.AccGetName.value="";
	fm.GetIDNo.value="";
	fm.PayMode.value="";
	fm.PayModeName.value="";
	fm.BankCode.value="";
	fm.BankCodeName.value="";
	fm.AccName.value="";
	fm.BankAccNo.value="";
	fm.AccGetMoney.value="";
	controlTransferShow();
	fm.all("AppAccGetInfo").style.display='none';
}

function clearQuery()
{
	initAppAccGrid();
  initAppAccTraceGrid();
  clearAccGetInfo();
}

//打印投保人帐户历史轨迹清单
function printAppAccTraceList()
{
	var tRow = AppAccGrid.getSelNo();
	if (tRow == 0)
  {
    alert("请选择一个客户！");
    return false;
  }
  var customerNo=AppAccTraceGrid.getRowColData(tRow-1, 1);
  window.open("./BqAppAccListMain.jsp?ContType="+fm.ContType.value+"&CustomerNo=" + fm.CustomerNo.value);
	
}

//打印投保人帐户领取单
//function printAppAccGet()
//{
//	var tRow = AppAccTraceGrid.getSelNo();
//	if (tRow == 0)
//  {
//    alert("请选择一个帐户轨迹信息！");
//    return false;
//  }
//  if(AppAccTraceGrid.getRowColData(tRow-1, 6)!='04')
//	{
//		alert("该记录不是领取轨迹！");
//    return false;
//	}
//	
//	var customerNo=AppAccTraceGrid.getRowColData(tRow-1, 1);
//	var serialNo=AppAccTraceGrid.getRowColData(tRow-1, 3);
//	window.open("./BqAppAccGetMain.jsp?ContType="+fm.ContType.value+"&CustomerNo=" + customerNo+"&SerialNo="+serialNo);
//}

function printAppAccGet()
{
	var tRow = AppAccTraceGrid.getSelNo();
	if (tRow == 0)
  {
    alert("请选择一个帐户轨迹信息！");
    return false;
  }
  if(AppAccTraceGrid.getRowColData(tRow-1, 6)!='04')
	{
		alert("该记录不是领取轨迹！");
    return false;
	}
	
	var customerNo=AppAccTraceGrid.getRowColData(tRow-1, 1);
	var serialNo=AppAccTraceGrid.getRowColData(tRow-1, 3);
	var ac = fm.action;
//alert(serialNo);
	//window.open("./BqAppAccGetMain.jsp?ContType="+fm.ContType.value+"&CustomerNo=" + customerNo+"&SerialNo="+serialNo);
	fm.action = "../uw/PDFPrintSave.jsp?Code=86&RePrintFlag=&MissionID=&SubMissionID=&ContNo=&PrtFlag=&fmtransact=PRINT&StandbyFlag2="+serialNo+"&StandbyFlag3="+customerNo;
	fm.submit();
	fm.action=ac;
	
}

function getAppAcc()
{
	if(checkInput()==false)
	{
		return false;
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	fm.submit();
}

function checkInput()
{
	var tRow = AppAccGrid.getSelNo();
	if (tRow == 0)
  {
    alert("请选择一个客户！");
    return false;
  }

	if(AppAccGrid.getRowColData(tRow-1, 6)!='1')
	{
		alert("客户状态无效！");
    return false;
	}	
	if( verifyInput2() == false ) return false;
	if(fm.PayMode.value=='4')
	{
		if(fm.BankCode.value=='')
		{
			alert('开户银行不能为空！');
			fm.BankCode.focus();
			return false
		}
		if(fm.AccName.value=='')
		{
			alert('户名不能为空！');
			fm.AccName.focus();
			return false
		}
		if(fm.BankAccNo.value=='')
		{
			alert('账号不能为空！');
			fm.BankAccNo.focus();
			return false
		}
		if(fm.TransferDate.value=='')
		{
			alert('转帐日期不能为空！');
			fm.TransferDate.focus();
			return false
		}
	}
	if (fm.GetIDType.value == "0")
	{
		if(!checkIdCard(fm.GetIDNo.value))
		{
			return false;
		}
	}
	if (fm.GetIDType.value == "0" || fm.GetIDType.value == "5")
	{
		var strCheckIdNo = checkIdNo(fm.GetIDType.value,fm.GetIDNo.value, "","");

		if ("" != strCheckIdNo&& strCheckIdNo!=null)
		{
			alert(strCheckIdNo);
			return false;
		}
	}
	return true;
}

function controlTransferShow()
{
	if(fm.PayMode.value=='4')
	{
		fm.all("ForTransfer1").style.display='';
		fm.all("ForTransfer2").style.display='';
	}else
	{
		fm.all("ForTransfer1").style.display='none';
		fm.all("ForTransfer2").style.display='none';
	}
}

