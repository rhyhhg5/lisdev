<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<% 
//程序名称：GEdorTypeMultiDetailInput.jsp
//程序功能：团体保全明细总页面
//创建日期：2003-12-03 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>   
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./GEdorTypeSGDetail.js"></SCRIPT>
  <%@include file="GEdorTypeSGDetailInit.jsp"%>
  
  
  <title>团体保全明细总页面</title> 
</head>

<body  onload="initForm();" >
  <form action="./GEdorTypeMultiSGSubmit.jsp" method=post name=fm target="fraSubmit">    
  <input type=hidden readonly name=EdorAcceptNo >
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 申请批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 被保人客户号 </TD>
        <TD class = input >
      		<input class = "readonly" readonly name=InsuredNo>
        </TD>
        <TD class = title > 保全项目 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=EdorType>
        </TD>   
      </TR>
    </TABLE> 
      <%@include file="SpecialInfoCommon.jsp"%>
    <br><hr>
    
    <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPbnf);">
        </td>
        <td class= titleImg>
          受益人信息
        </td>
      </tr>
    </table>
    <Div  id= "divLPbnf" style= "display: ''">
      	<table  class= common>
      		<tr>
			    	<td><font color=red>填写说明：出生日期录入格式统一为:yyyy-mm-dd ；受益份额录入值范围为0-1，并且同一类受益人的受益份额总和为1。</font></td>
			</tr>
			<tr>
			    	<td><font color=red>示例：出生日期为2011年7月7日，则录入 2011-07-07  ；受益份额为30%，则录入 0.3。</font></td>
			</tr>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanBnfGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage" align=center style= "display: 'none' ">
        <INPUT class=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage(); changeNullToEmpty();"> 					
        <INPUT class=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage(); changeNullToEmpty();"> 
        <INPUT class=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage(); changeNullToEmpty();"> 			
        </Div>		
  	</div>
	  <br><hr> 
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDWFee);">
    		</td>
    		<td class= titleImg>
    			 个人给付信息部分
    		</td>
    	</tr>
    	</table>
 		<table class=common> 
	        <TR  class= common>
	          <TD  class= title8>
	            付费方式
	          </TD>
	          <TD  class= input8>
	          <Input class=codeNo name=PayMode  VALUE="1" MAXLENGTH=1 verify="付费方式|notnull&code:PayMode" ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('GrpPayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"><input class=codename name=PayModeName value ='现金' readonly=true elementtype=nacessary>
	          </TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	        </TR>
	    </table>
	        
 	 <div id='divBankAcc' align=left style= "display:'none'">
		<table  class= common>
				<TR  class= common8>
				  
						<TD  class= title8>银行编码</TD><TD  class= input8><Input class="codeNo"  elementtype=nacessary name=BankCode ondblclick="return showCodeList('llbank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('llbank',[this,BankCodeName],[0,1]);" ><input class=codename name=BankCodeName></TD>
						<TD  class= title8>账号</TD><TD  class= input8><input class= common name="BankAccNo"></TD>
						<TD  class= title8>账户名</TD><TD  class= input8><input class= common name="AccName"></TD>
				
				</TR>
		</table>
	</div>
	<br><hr>
	<table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDeathDate);">
        </td>
        <td class= titleImg>
          被保人身故信息
        </td>
      </tr>
    </table>
    <Div  id= "divDeathDate" style= "display: ''">
    <table class=common>
      <tr>
      	  <TD  class= title8>
             被保人身故日期
          </TD>
          <TD  class= input8>
            <Input class="coolDatePicker"  dateFormat="short" name=deathDate verify=" 被保人身故日期|date" >
          </TD>
          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
	          <TD  class= title8></TD>
      </tr>
    </table>
    </Div>
  <br><hr> 
	  <Input type=hidden name="goIntoItem" value="进入个人保全" class=cssButton onclick="pEdorMultiDetail()">
	  <Input type=hidden name="goDiskImport" value="磁盘导入" id="goDiskImport" class=cssButton onclick="diskImport()">
	  <Input type=hidden name="cancel" value="撤销个人保全" class=cssButton onclick="cancelPEdor()"> 
	 <!-- <Input type=Button value="保  存" class=cssButton onclick="saveEdor()"> --->
	 
	 <!--将原来的返回改成保存并返回，主要目的为更新edorState 为1-录入完成。------------>
	  <Input type=button name="save" value="保存申请" class=cssButton onclick="returnParent()">
	  <Input type=button name="save" value="返    回" class=cssButton onclick="top.close();">
	  <Input type=hidden  value="修改归属比例" class=cssButton onclick="return submitForm();">
	  <input type=hidden id="ContNo" name="ContNo">
	  <!-- add ContType for PEdor GT -->
	  <input type=hidden id="ContType" name="ContType"> 
	  <input type=hidden id="Transact" name="Transact">
	  <div id="test"></div>
	  <hr>
	  <div id="ErrorsInfo"></div> 
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  window.focus();
</script>
