<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：GEdorTypeZTCaltypeSave.jsp
//程序功能：
//创建日期：2005-09-21
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.util.*"%>
<%
    String content = null;
    String flag = null;
    GlobalInput gi = (GlobalInput)session.getValue("GI");
    //死亡日期deathDate
    String deathDate = request.getParameter("deathDate");
    
    System.out.println(request.getParameter("ContNo")+"========contno");
    
    //保全批改表
    LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    
    LPBnfSet tLPBnfSet = new LPBnfSet();
    
    tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
    tLPEdorItemSchema.setInsuredNo(request.getParameter("InsuredNo"));
    tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
    EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(tLPEdorItemSchema);
    
    String PayMode = request.getParameter("PayMode");
  	if(PayMode==null){
  		PayMode="";
  	}
	//String PayModeName = request.getParameter("PayModeName");
	String PayModeName ="";
	if(PayModeName==null){
		PayModeName="";
  	}
	if(PayMode!=null && !PayMode.equals("")){
		LDCodeDB tLDCodeDB = new LDCodeDB();
		tLDCodeDB.setCode(PayMode);
		tLDCodeDB.setCodeType("paymode");
		if(tLDCodeDB.getInfo()){
			PayModeName = tLDCodeDB.getCodeName();
		}
	}
	
	//银行代码和名称
	String BankCode = request.getParameter("BankCode");
	if(BankCode==null){
		BankCode="";
	}
	//String BankCodeName = request.getParameter("BankCodeName");
	String BankCodeName = "";
	if(BankCodeName==null){
		BankCodeName="";
	}
	if(BankCode!=null && !BankCode.equals("")){
		LDBankDB tLDBankDB = new LDBankDB();
		tLDBankDB.setBankCode(BankCode);
		if(tLDBankDB.getInfo()){
			BankCodeName = tLDBankDB.getBankName();
		}
	}
	
	//银行账号
	String BankAccNo = request.getParameter("BankAccNo");
	if(BankAccNo==null){
		BankAccNo="";
	}
	//银行开户名
	String AccName = request.getParameter("AccName");
	if(AccName==null){
		AccName="";
	}
	
	tEdorItemSpecialData.add("PayMode", PayMode);
	tEdorItemSpecialData.add("PayModeName", PayModeName);
	tEdorItemSpecialData.add("BankCode", BankCode);
	tEdorItemSpecialData.add("BankCodeName", BankCodeName);
	tEdorItemSpecialData.add("BankAccNo", BankAccNo);
	tEdorItemSpecialData.add("AccName", AccName);
	tEdorItemSpecialData.add("DeathDate", deathDate);
	
	//获得grid中的数据
	String tName[] = request.getParameterValues("BnfGrid1");
	String tSex[] = request.getParameterValues("BnfGrid2");
	String tBirthday[] = request.getParameterValues("BnfGrid3");
	String tBnfgrade[] = request.getParameterValues("BnfGrid4");
	String tBnflot[] = request.getParameterValues("BnfGrid5");
	String tIdType[] = request.getParameterValues("BnfGrid6");
	String tIdNo[] = request.getParameterValues("BnfGrid7");
	String tRelationToInsured[] = request.getParameterValues("BnfGrid8");
	
	 int bnfcount = 0;
	 bnfcount=tName.length;
	 for(int i=0;i<bnfcount;i++){
			//受益人表
		    LPBnfSchema tLPBnfSchema = new LPBnfSchema();
		    tLPBnfSchema.setEdorNo(request.getParameter("EdorNo"));
		    tLPBnfSchema.setEdorType(request.getParameter("EdorType"));
		    tLPBnfSchema.setInsuredNo(request.getParameter("InsuredNo"));
		    System.out.println(request.getParameter("ContNo")+"========contno");
		    tLPBnfSchema.setContNo(request.getParameter("ContNo"));
		    tLPBnfSchema.setPolNo("000000");
		    tLPBnfSchema.setName(tName[i]);
		    tLPBnfSchema.setSex(tSex[i]);
		    tLPBnfSchema.setBirthday(tBirthday[i]);
		    tLPBnfSchema.setBnfNo(i+1);
		    tLPBnfSchema.setBnfGrade(tBnfgrade[i]);
		    tLPBnfSchema.setBnfLot(tBnflot[i]);
		    tLPBnfSchema.setIDType(tIdType[i]);
		    tLPBnfSchema.setIDNo(tIdNo[i]);
		    tLPBnfSchema.setRelationToInsured(tRelationToInsured[i]);
		    tLPBnfSchema.setBnfType("1");//1 代表死亡
		    tLPBnfSchema.setAccName(AccName);
		    tLPBnfSchema.setBankAccNo(BankAccNo);
		    tLPBnfSchema.setBankCode(BankCode);
		    tLPBnfSchema.setOperator(gi.Operator);
		    tLPBnfSchema.setModifyDate(PubFun.getCurrentDate());
		    tLPBnfSchema.setModifyTime(PubFun.getCurrentTime());
		    tLPBnfSchema.setMakeDate(PubFun.getCurrentDate());
		    tLPBnfSchema.setMakeTime(PubFun.getCurrentTime());
		    tLPBnfSet.add(tLPBnfSchema);
	 }
	
		VData data = new VData();
		data.add(gi);
		data.add(tLPEdorItemSchema);
		data.add(tEdorItemSpecialData);
		data.add(tLPBnfSet);
		
		GrpEdorSGDetailUI tGrpEdorSGDetailUI = new GrpEdorSGDetailUI();
		
		if (!tGrpEdorSGDetailUI.submitData(data))
		{
			flag = "Fail";
			content = "数据保存失败！原因是:" + tGrpEdorSGDetailUI.getError();
		}
		else
		{
			flag = "Succ";
			content = "数据保存成功。";
		}
		String message = tGrpEdorSGDetailUI.getMessage();
		if ((message != null) && (!message.equals("")))
		{
			content += message;
		}
    content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
    parent.fraInterface.afterSubmitCalType("<%=flag%>","<%=content%>");
</script>
</html>