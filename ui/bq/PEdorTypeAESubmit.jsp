<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PEdorTypeAESubmit.jsp
//程序功能：
//创建日期：2005-09-22
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%        
	String flag;
	String content;
	
	GlobalInput gi = (GlobalInput) session.getValue("GI");
	//得到被保人信息
	LPInsuredSet mLPInsuredSet = new LPInsuredSet();
	//得到投保人信息
	LPAppntSchema tLPAppntSchema = new LPAppntSchema();
	tLPAppntSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPAppntSchema.setEdorType(request.getParameter("EdorType"));
	tLPAppntSchema.setContNo(request.getParameter("ContNo"));
	tLPAppntSchema.setAppntName(request.getParameter("Name"));
	tLPAppntSchema.setAppntSex(request.getParameter("Sex"));
	tLPAppntSchema.setAppntBirthday(request.getParameter("Birthday"));
	tLPAppntSchema.setIDType(request.getParameter("IDType"));
	tLPAppntSchema.setIDNo(request.getParameter("IDNo"));
	tLPAppntSchema.setOccupationCode(request.getParameter("OccupationCode"));
	tLPAppntSchema.setOccupationType(request.getParameter("OccupationType"));
    tLPAppntSchema.setBankCode(request.getParameter("BankCode"));
    tLPAppntSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLPAppntSchema.setAccName(request.getParameter("AccName"));
    //得到投保人保单地址信息
    LPAddressSchema tLPAddressSchema = new LPAddressSchema();
    tLPAddressSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPAddressSchema.setEdorType(request.getParameter("EdorType"));
    tLPAddressSchema.setPhone(request.getParameter("Phone"));
    tLPAddressSchema.setZipCode(request.getParameter("ZipCode"));
    tLPAddressSchema.setPostalAddress(request.getParameter("Address"));
    //把剩余数据放入TransferData中
    TransferData td = new TransferData();
  
  td.setNameAndValue("Relation", request.getParameter("Relation"));
  td.setNameAndValue("PayMode", request.getParameter("PayMode"));
  
   
     //String tGridNo[] = request.getParameterValues("ObjGridNo");  //得到序号列的所有值
     //String tGrid1 [] = request.getParameterValues("RelationToAppntInsuredGrid1"); //得到第1列的所有值
    // String tGrid5 [] = request.getParameterValues("RelationToAppntInsuredGrid5"); //得到第2列的所有值
    // int  Count = tGrid1.length; //得到接受到的记录数
	//   for(int index=0;index< Count;index++)
   //  {
     //System.out.println("GridNO="+ tGridNo [index]);
   // System.out.println("Grid 1="+ tGrid1 [index]);
    // System.out.println("Grid 5="+ tGrid5 [index]);
    // }
    
   
   
  String tChk[] = request.getParameterValues("InpRelationToAppntInsuredGridChk"); 
   String tGrid1 [] = request.getParameterValues("RelationToAppntInsuredGrid1"); //得到第1列的所有值
   String tGrid5 [] = request.getParameterValues("RelationToAppntInsuredGrid5"); //得到第2列的所有值
   
//参数格式=” Inp+MulLine对象名+Chk”
  
        for(int index=0;index<tChk.length;index++)
        {
          LPInsuredSchema tLPInsuredSchema= new LPInsuredSchema();
        
          if(tChk[index].equals("1"))           
           { 
            System.out.println("该行被选中"+index);
            System.out.println("Grid 1="+ tGrid1[index]);
            System.out.println("Grid 5="+ tGrid5 [index]);
            //td.setNameAndValue("'"+"Insuredno"+index+"'",tGrid1[index]);
           // td.setNameAndValue("'"+"RelationToAppnt"+index+"'",tGrid5 [index]);
            tLPInsuredSchema.setInsuredNo(tGrid1[index]);
            tLPInsuredSchema.setRelationToAppnt(tGrid5[index]);
            mLPInsuredSet.add(tLPInsuredSchema);
           }
          if(tChk[index].equals("0"))      
            System.out.println("该行未被选中"+index);
        }               

	VData data = new VData();
	data.add(gi);
	data.add(tLPAppntSchema);
	data.add(tLPAddressSchema);
	data.add(mLPInsuredSet);
	data.add(td);
	
	PEdorAEDetailUI tPEdorAEDetailUI = new PEdorAEDetailUI();
	if (!tPEdorAEDetailUI.submitData(data))
	{
		flag = "Fail";
		content = "数据保存失败！原因是：" + tPEdorAEDetailUI.getError();
	}
  else
	{
		flag = "Succ";
		content = "数据保存成功！";
		
		//显示老客户提示信息
		String message = tPEdorAEDetailUI.getMessage();
		if (message != null)
		{
		  content += message;
		}
	}
	content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>
