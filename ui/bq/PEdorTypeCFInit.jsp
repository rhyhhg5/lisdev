<script language="JavaScript">  

function initForm()
{
  try
  {
    initInpBox();
    initOldPolGrid();
    initNewPolGrid();
    initLastPolGrid();
    initOldPolGrid234();
    queryOldPol();
    queryNewPol();
    queryLastPol();
    
      queryOldPol234();
      
     fm.all('NewAppntNo').value = '';
  }
  catch(e)
  {
    alert("初始化界面错误，请重新登录!");
  }
}



function initInpBox()
{   
  fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
  fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
  fm.all('ContNo').value = top.opener.fm.all('ContNoBak').value;
  fm.all('AppntNo').value = top.opener.fm.all('AppntNo').value;
  fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
  //制定汉化
  showOneCodeName("EdorCode", "EdorTypeName");  
} 

function initOldPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="被保人客户号";    			//列名
    iArray[1][1]="15px";            	//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="被保人姓名";    	//列名
    iArray[2][1]="20px";            	//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="证件类型";    			//列名
    iArray[3][1]="10px";            	//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[4]=new Array();
    iArray[4][0]="证件号";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="20px";         		//列宽
    iArray[4][2]=100;          				//列最大值
    iArray[4][3]=0;            				//是否允许输入,1表示允许，0表示不允许  
    
    
    OldPolGrid = new MulLineEnter("fm", "OldPolGrid"); 
    OldPolGrid.mulLineCount = 0;
    OldPolGrid.displayTitle = 1;
    OldPolGrid.canSel = 0;
    OldPolGrid.canChk = 1;
    OldPolGrid.hiddenSubtraction = 1;
    OldPolGrid.hiddenPlus = 1;
    OldPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}

function initOldPolGrid234()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="被保人客户号";    			//列名
    iArray[1][1]="15px";            	//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="被保人姓名";    	//列名
    iArray[2][1]="20px";            	//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      iArray[3]=new Array();
      iArray[3][0]="与投保人的关系";      	   		//列名
      iArray[3][1]="10px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=2;                       //是否允许输入,1表示允许，0表示不允许 ,2表示可以下拉
      iArray[3][4]="Relation"; 
      iArray[3][5]=3;
      iArray[3][9]="与投保人的关系";           // 当为主被保人时这个条件需要添加 |code:Relation&NOTNULL
      iArray[3][18]=250;
      iArray[3][19]= 0 ;
      iArray[3][21]="RelationToAppntInsured";
    
    OldPolGrid234 = new MulLineEnter("fm", "OldPolGrid234"); 
    OldPolGrid234.mulLineCount = 0;
    OldPolGrid234.displayTitle = 1;
    OldPolGrid234.canSel = 0;
    OldPolGrid234.canChk = 1;
    OldPolGrid234.hiddenSubtraction = 1;
    OldPolGrid234.hiddenPlus = 1;
    OldPolGrid234.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}


function initLastPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="28px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="60px";         		//列宽
    iArray[1][2]=100;          				//列最大值
    iArray[1][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="被保人客户号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="55px";         		//列宽
    iArray[2][2]=100;          				//列最大值
    iArray[2][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="被保人姓名";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="55px";         		//列宽
    iArray[3][2]=100;          				//列最大值
    iArray[3][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="险种名称";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="120px";         		//列宽
    iArray[4][2]=100;          				//列最大值
    iArray[4][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="险种代码";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[5][1]="40px";         		//列宽
    iArray[5][2]=100;          				//列最大值
    iArray[5][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="保额";    			//列名
    iArray[6][1]="30px";            	//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="保费";    	//列名
    iArray[7][1]="30px";            	//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="生效日期";    			//列名
    iArray[8][1]="100px";            	//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[9]=new Array();
    iArray[9][0]="满期日期";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[9][1]="80px";         		//列宽
    iArray[9][2]=100;          				//列最大值
    iArray[9][3]=0;            				//是否允许输入,1表示允许，0表示不允许  
    
    LastPolGrid = new MulLineEnter("fm", "LastPolGrid"); 
    LastPolGrid.mulLineCount = 0;
    LastPolGrid.displayTitle = 1;
    LastPolGrid.canSel = 0;
    LastPolGrid.canChk = 0;
    LastPolGrid.hiddenSubtraction = 1;
    LastPolGrid.hiddenPlus = 1;
    LastPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}

function initNewPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="28px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="60px";         		//列宽
    iArray[1][2]=100;          				//列最大值
    iArray[1][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="投保人号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="55px";         		//列宽
    iArray[2][2]=100;          				//列最大值
    iArray[2][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="被保人客户号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="55px";         		//列宽
    iArray[3][2]=100;          				//列最大值
    iArray[3][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="被保人姓名";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="55px";         		//列宽
    iArray[4][2]=100;          				//列最大值
    iArray[4][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="险种名称";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[5][1]="120px";         		//列宽
    iArray[5][2]=100;          				//列最大值
    iArray[5][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="险种代码";    			//列名
    iArray[6][1]="40px";            	//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="保额";    	//列名
    iArray[7][1]="30px";            	//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="保费";    			//列名
    iArray[8][1]="30px";            	//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
    iArray[9]=new Array();
    iArray[9][0]="生效日期";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[9][1]="80px";         		//列宽
    iArray[9][2]=100;          				//列最大值
    iArray[9][3]=0;            				//是否允许输入,1表示允许，0表示不允许  
    
    iArray[10]=new Array();
    iArray[10][0]="满期日期";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[10][1]="80px";         		//列宽
    iArray[10][2]=100;          				//列最大值
    iArray[10][3]=0;            				//是否允许输入,1表示允许，0表示不允许  
    
    NewPolGrid = new MulLineEnter("fm", "NewPolGrid"); 
    NewPolGrid.mulLineCount = 0;
    NewPolGrid.displayTitle = 1;
    NewPolGrid.canSel = 0;
    NewPolGrid.canChk = 0;
    NewPolGrid.hiddenSubtraction = 1;
    NewPolGrid.hiddenPlus = 1;
    NewPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}
</script>