<%
//PEdorTypeICInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">  
var cancelCount = 0;
//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divLPInsuredDetail.style.left=ex;
  	divLPInsuredDetail.style.top =ey;
   	detailQueryClick();
}

function initInpBox()
{   

  fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
  fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
  fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
  getContNo();
  getCustomerNo();
  fm.all('AppntNo').value = top.opener.fm.all('AppntNo').value;

}                                 

function initForm()
{
  try
  {
		initInpBox();
		initContGrid();
		getCustomerInfo(fm.CustomerNo.value);
		showAllCodeName();
		
    if(cancelCount == 0)
    {
      initElementtype();
      cancelCount = cancelCount + 1;
   }
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function initContGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号码";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="投保人";  
    iArray[2][1]="100px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    
    iArray[3]=new Array();
    iArray[3][0]="主被保险人";
    iArray[3][1]="100px";
    iArray[3][2]=100;
    iArray[3][3]=0;
    
    iArray[4]=new Array();
    iArray[4][0]="生效日期";
    iArray[4][1]="100px";
    iArray[4][2]=100;
    iArray[4][3]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="保费";
    iArray[5][1]="100px";
    iArray[5][2]=100;
    iArray[5][3]=0;
    
    iArray[6]=new Array();
    iArray[6][0]="保额";
    iArray[6][1]="50px";
    iArray[6][2]=100;
    iArray[6][3]=0;  
    
    iArray[7]=new Array();
    iArray[7][0]="集体保单号";
    iArray[7][1]="50px";
    iArray[7][2]=100;
    iArray[7][3]=3;    
    
    ContGrid = new MulLineEnter("fm", "ContGrid"); 
    //这些属性必须在loadMulLine前
    ContGrid.mulLineCount = 0;   
    ContGrid.displayTitle = 1;
    //ContGrid.canChk=1;
    ContGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    ContGrid.hiddenSubtraction=1;
    ContGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}

function initQuery()
{	
	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.all('fmtransact').value = "QUERY||DETAIL";
	mFlag = "N";
	fm.submit();	  	 	 
}


function initBankQuery()
{	
	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.all('fmtransact').value = "QUERY||MAIN";
	mFlag = "N";
	fm.submit();	  	 	 
}

function detailQueryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
	fm.all('fmtransact').value = "QUERY||DETAIL";
	
	var tSel=LCInsuredGrid.getSelNo();
	
	if (tSel==0||tSel==null)
		alert("不能是空记录!");
	else
	{
		var tCustomerNo =LCInsuredGrid.getRowColData(tSel-1,2);
		fm.all('CustomerNo1').value =tCustomerNo;
		fm.all('ContNo').value =LCInsuredGrid.getRowColData(tSel-1,1);
		fm.submit();
	}
}
function initDiv()
{
	divLPInsuredDetail.style.display ='none';
	divDetail.style.display='none';
}

</script>