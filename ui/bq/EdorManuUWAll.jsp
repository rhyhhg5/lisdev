<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：EdorManuUWAll.jsp.
//程序功能：个单保全人工核保
//创建日期：2005-3-14 
//创建人  ：FanXiang
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%
	String tFlag = "";
	tFlag = request.getParameter("type");
%>
<html>
<%	
  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operFlag = "<%=tFlag%>";		//区分团险和个险的标志
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";     //记录登陆机构
</script>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="EdorManuUWAll.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="EdorManuUWAllInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit" action="./EdorManuUWAllChk.jsp"> 
  <Div  id= "divApplyButton" style= "display: ''" style="float: right"> 	
      <table  class= common>
  		<TR>
  			<td class="titleImg">核保申请
  			</td>
  		</TR>
  		<TR>
          <TD  class= title>
            申请件数
          </TD>
          <TD  class= input>
            <Input class=common name="ApplyNo" >
          </TD>
          <TD>  			
          <INPUT class=cssButton id="riskbutton" VALUE="申  请" TYPE=button onClick="ApplyUW();">
          (每次申请最多为10件)
  				</TD>
          <TD  class= title>
            待申请件数
          </TD>
          <TD  class= input>
            <Input CLASS="readonly" readonly name="UWNo" >
          </TD>  				
  		</TR>
      </table>
  </DIV>
   <DIV id=DivLCContInfo STYLE="display:''"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 待核保保单：
    		</td>
    	</tr>  	
    </table>
    </Div>
         <Div  id= "divLCPol1" style= "display: ''" align = center>
       		<tr  class=common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();">    
    	</table>    
    </div>
    <table>
    	 <Input type=hidden name="ApplyType" >
    </table>

</body>
</html>
