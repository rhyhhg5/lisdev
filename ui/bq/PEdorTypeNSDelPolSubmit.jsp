<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PEdorTypeNSDelPolSubmit.jsp
//程序功能：
//创建日期：20061010
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%

  //输出参数
  String FlagStr = "";
  String Content = "";
   
  GlobalInput tGI = (GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  System.out.println("tGI"+tGI.Operator);
  
  if(tGI == null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    String fmAction=request.getParameter("fmAction");
    
    LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
    tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
    tLPEdorItemSchema.setInsuredNo(request.getParameter("InsuredNo"));
    tLPEdorItemSchema.setPolNo(request.getParameter("Polno"));

    VData tVData = new VData();
    tVData.add(tLPEdorItemSchema);
    tVData.add(tGI);
    
    PEdorNSDetailDelPolUI tPEdorNSDetailDelPolUI   = new PEdorNSDetailDelPolUI();
    if(!tPEdorNSDetailDelPolUI.submitData(tVData,fmAction))
    {
      System.out.println("Submit Failed! " + tPEdorNSDetailDelPolUI.mErrors.getErrContent());
      Content = "删除失败，原因是:\n" + tPEdorNSDetailDelPolUI.mErrors.getErrContent();
      FlagStr = "Fail";
    }
    else
    {
      System.out.println("Submit Succed!");
      Content = "删除成功";
      FlagStr = "Succ";
    }
  }//页面有效区
  
  Content = PubFun.changForHTML(Content);
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

