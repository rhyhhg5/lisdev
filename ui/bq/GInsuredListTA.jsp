<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2006-02-07
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%
    LDSysVarDB tLDSysVarDB = new LDSysVarDB();
    String strSql1 = "select * from ldsysvar where Sysvar='VTSFilePath'";
    LDSysVarSet tLDSysVarSet = tLDSysVarDB.executeQuery(strSql1);    
    LDSysVarSchema tLDSysVarSchema = tLDSysVarSet.get(1);
    String strFilePath = tLDSysVarSchema.getV("SysVarValue");
    String strRealPath = application.getRealPath("/").replace('\\', '/') + "/";
    String strVFFileName = strFilePath + FileQueue.getFileName()+".vts";
    String strVFPathName = strRealPath + strVFFileName;
	String strEdorNo = request.getParameter("EdorNo");
	String strErrInfo = "";
		
   if ( strEdorNo != null && !strEdorNo.equals("")) 
   {  // 合并VTS模板文件与数据文件存入服务器磁盘中
        // 建立数据库连接
      try
      {
			InputStream ins = null;
			LPEdorPrint2DB temp = new LPEdorPrint2DB();
			temp.setEdorNo(strEdorNo);
			if(!temp.getInfo())
			{
			   strErrInfo  = "查询失败";
			}
		  	ins = temp.getEdorInfo();
	        //合并VTS文件 
	        String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
	        
	        ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
	        CombineVts tcombineVts = new CombineVts(ins,strTemplatePath);
	        tcombineVts.output(dataStream);
	        
	        //把dataStream存储到磁盘文件
	        AccessVtsFile.saveToFile(dataStream, strVFPathName);
	        response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath=" + strVFPathName);
      }
      catch (Exception ex)
      {
          ex.printStackTrace();
      }
    }else{
		strErrInfo = "没有输入保全号";
	}
%>