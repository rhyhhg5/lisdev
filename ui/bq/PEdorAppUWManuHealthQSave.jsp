<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuHealthQChk.jsp
//程序功能：人工核保体检资料查询
//创建日期：2005-4-18 16:08
//创建人  ：lanjun
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
 
  String tEdorAcceptNo = request.getParameter("EdorAcceptNo");
 	String tContNo = request.getParameter("ContNo");
 	String tProposalContNo =request.getParameter("ContNo");
 	String tCustomerNo=request.getParameter("InsureNo");
 	String tPrtSeq=request.getParameter("PrtSeq");
 	System.out.println("tContNo="+tContNo);
 	System.out.println("tCustomerNo="+tCustomerNo);
 	System.out.println("tPrtSeq="+tPrtSeq);
 	
 	LPAppPENoticeResultSchema tLPAppPENoticeResultSchema ;
 	LPAppPENoticeResultSet tLPAppPENoticeResultSet = new LPAppPENoticeResultSet();
 	
	int lineCount = 0;
	String arrCount[] = request.getParameterValues("DisDesbGridNo");

	if(arrCount!=null)
	{
		String tDisDesb[]=request.getParameterValues("DisDesbGrid1");
		String tDisResult[]=request.getParameterValues("DisDesbGrid2");
		String tICDCode[]=request.getParameterValues("DisDesbGrid3");
		
		lineCount = arrCount.length;
		System.out.println("lineCount="+lineCount);
		for(int i = 0;i<lineCount;i++)
		{
		
			tLPAppPENoticeResultSchema = new LPAppPENoticeResultSchema();
			
			tLPAppPENoticeResultSchema.setEdorAcceptNo(tEdorAcceptNo);
			tLPAppPENoticeResultSchema.setContNo(tContNo);
			tLPAppPENoticeResultSchema.setProposalContNo(tProposalContNo);
			//tLPAppPENoticeResultSchema.setPrtSeq(tPrtSeq);
			tLPAppPENoticeResultSchema.setCustomerNo(tCustomerNo);
			tLPAppPENoticeResultSchema.setDisDesb(tDisDesb[i]);
			System.out.println("tDisDesb="+tDisDesb[i]);
			System.out.println("tDisResult="+tDisResult[i]);
			System.out.println("tICDCode="+tICDCode[i]);
			tLPAppPENoticeResultSchema.setDisResult(tDisResult[i]);
			tLPAppPENoticeResultSchema.setICDCode(tICDCode[i]);
			tLPAppPENoticeResultSet.add(tLPAppPENoticeResultSchema);
		}
	}
	else
	{
	}
	
	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";

	tVData.add(tG);
	tVData.add(tLPAppPENoticeResultSet);
	
	PEdorAppDisDesbUI tPEdorAppDisDesbUI = new PEdorAppDisDesbUI();
	
	try{
		System.out.println("this will save the data!!!");
		tPEdorAppDisDesbUI.submitData(tVData,"");
	}
	catch(Exception ex){
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
	
	if (!FlagStr.equals("Fail")){
		tError = tPEdorAppDisDesbUI.mErrors;
		if (!tError.needDealError()){
			Content = " 保存成功! ";
			FlagStr = "Succ";
		}
		else{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
