<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="GEdorTypeNIInput.js"></SCRIPT>
	<%@include file="GEdorTypeNIInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<title>新增被保人 </title>
</head>
<body  onload="initForm();">
  <form action="./GEdorTypeNISubmit.jsp" method=post name=fm target="fraSubmit">
	<table class=common>
		<TR  class= common> 
		  <TD  class= title > 受理号</TD>
		  <TD  class= input > 
		    <input class="readonly" readonly name=EdorNo >
		  </TD>
		  <TD class = title > 批改类型 </TD>
		  <TD class = input >
		  	<input class = "readonly" readonly name=EdorType type=hidden>
		  	<input class = "readonly" readonly name=EdorTypeName>
		  </TD>
		  <TD class = title > 集体保单号 </TD>
		  <TD class = input >
		  	<input class = "readonly" readonly name=GrpContNo>
		  </TD>
		</TR>
	 </TABLE> 
	 <%@include file="SpecialInfoCommon.jsp"%>
	 <%
  	if (specialRiskFlag == true)
  	{
  	  out.print("<script>specialRiskFlag = true;</script>");
  	}
   %>
   <div align=left id="divSpecialInfo1" style="display: 'none' ">
    <font color="#FF0000">该保单同时承保特需险种和普通险种，"个人账户资金设置"录入项目无效。增加被保险人时，导入清单中填入的保费无效，普通险种只能由系统计算保费。对此单的特需险种在增人时只建立个人特需账户，所增人的特需账户的金额默认为零。如果增加被保险人需要追加特需保费，则必须先增加了被保险人后再通过“追加保费”项目的形式来向账户中注入金额。</font>
   </div>
	 <br>
   <table>
     <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
      </td>
      <td class= titleImg>
        新增被保人清单
      </td>
      <td> 
        <input type=Button name="plan" class= cssButton value="保障计划" onclick="queryPlan();">
      </td>
    </tr>
   </table>
    <Div id= "divLPInsured" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanInsuredListGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>	
  	</div>	 
  	<br>
  	<table class="common">
  	<tr>
  	 <td>
  	 <p> 
  	<font color="#ff0000"> 增人时选择&ldquo;保费计算方式&rdquo;按月计算和按日计算的计算公式不一样，因此算出来的费用也不一样。按月计算的计算公式是：期交保费*剩余月份/12 &nbsp;&nbsp;&nbsp; 按日计算的计算公式是： 期缴保费*（交至日期-保全生效日期）/（交至日期-原交至日期）。</font>
  	 </p>
  	 </td>
  	</tr>
  	</table>
  	<table class="common">
  	  <tr class="common">
  	    <td class="title">
  	      保费计算方式
  	    </td>
  	    <td class="input">
  	      <input class="codeNo" name="CalType" value="2" CodeData="0|^1|未满一年月份保费表^2|剩余时间比例"  ondblclick="return showCodeListEx('CalType',[this,CalTypeName],[0,1]);" onkeyup="return showCodeListEx('CalType',[this,CalTypeName],[0,1]);"><Input class="codeName" name="CalTypeName" readonly>
  	    </td>
  	    <td class="title" style="display:'none'">
  	      非一年期保费还原
  	    </td>
  	    <td class="input" style="display:'none'">
  	      <input type='hidden' class="codeNo" name="RevertType" value="1" CodeData="0|^1|还原一年期保费^2|维持非一年期保费"  ondblclick="return showCodeListEx('RevertType',[this,RevertTypeName],[0,1]);" onkeyup="return showCodeListEx('RevertType',[this,RevertTypeName],[0,1]);"><Input  type='hidden' class="codeName" name="RevertTypeName" readonly>
  	    </td>
  	    <td class="input">
  	      按<input type="radio" name="CalTime" value="1" checked>月<input type="radio" name="CalTime" value="2">日计算
  	    </td>
  	  </tr>
  	</table>
  	<br>
	<Div  id= "divSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="doImport" class=cssButton value="磁盘导入"  onclick="importInsured();">
	   		 <input type=Button name="del" class= cssButton  value="删  除" onclick="deleteInsured();">
	   		 <input type=Button name="save" class= cssButton value="保  存" onclick="saveEdor();">
	   		 <input type=Button name="goBack" class= cssButton value="返  回" onclick="returnParent();">
			</TR>
	 	</table> 
	</Div>
	<Div id = "divErrorMes" ></Div>
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="ContType" name="ContType">
	<input type=hidden id="ContNo" name="ContNo">
	<input type=hidden id="EdorValiDate" name="EdorValiDate">
	<input type=hidden id="InsuredId" name="InsuredId">
	<div id="ErrorsInfo"></div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>
</html>