<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html> 
<% 
//程序名称：
//程序功能：集体保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeWT.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeWTInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeWTSubmit.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 集体合同号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpContNo>
      </TD>   
    </TR>
  </TABLE> 
  
  <Div  id= "divLPAppntGrpDetail" style= "display: ''">
  <table>
    	<tr>
    		<td class= titleImg>
    			 集体保单资料
    		</td>
    	</tr>
    </table>
    <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
       <TR>
          <TD  class= title>
            单位客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" name=AppntNo readonly >
          </TD>  
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class="readonly" name=GrpName readonly >
          </TD>
          <TD  class= title>
            签单日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=SignDate readonly >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            签单机构
          </TD>
          <TD  class= input>
            <Input class="readonly" name=SignCom readonly >
          </TD> 
          <TD  class= title>
            生效日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=CValiDate readonly >
          </TD>   
          <TD  class= title>
            截止日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=CInValiDate readonly >
          </TD>     
        </TR>
        <TR  class= common>
          <TD  class= title>
            投保人数
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=Peoples2 readonly >
          </TD>
          <TD  class= title>
            总保费
          </TD>
          <TD  class= input>
            <Input class="readonly" name=Prem readonly >
          </TD>
          <TD  class= title>
            总保额
          </TD>
          <TD  class= input>
            <Input class="readonly" name=Amnt readonly >
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title>
            投保日期
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=PolApplyDate readonly >
          </TD>
          <TD  class= title>
            回执签收日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=CustomGetPolDate readonly >
          </TD>
          <TD  class= title>
            撤保日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=EdorValidate readonly >
          </TD>       
        </TR>
      </table>

    <!--险种信息-->   
    <Div  id= "divPolInfo" style= "display: ''">
        <table>
            <tr>
                <td>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpPolGrid);">
                </td>
                <td class= titleImg>
                    险种选择
                </td>
            </tr>
        </table>
	    <Div  id= "divLPGrpPolGrid" style= "display: ''">
        <table  class= common>
          <tr  class= common>
            <td text-align: left colSpan=1>
  	    		  <span id="spanLCGrpPolGrid" ></span> 
  	    	  </td>
  	    	</tr>
        </table>
            <Div id= "divPage2" align="center" style= "display: 'none' ">
	        <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();"> 
	        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
	        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
	        <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
	        </Div>				
  	    </div>
    </div>
  	
  	<Div  id= "divPolInfo" style= "display: ''">
      <table>
        <tr>
            <td>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTestGrid);">
            </td>
            <td class= titleImg>
                体检费扣除
            </td>
        </tr>
      </table>
      <Div  id= "divTestGrid" style= "display: ''">
        <table  class= common>
          <tr  class= common>
          	<td text-align: left colSpan=6>
  	  			  <span id="spanTestGrid" ></span> 
  	  	  	</td>
  	  	  </tr>
  	  	  <tr>
  	  	    <td class=common colspan='6'>
        	  	<div id= "divPage3" align="center" style= "display: 'none' ">
      	        <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage3.firstPage();"> 
      	        <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage3.previousPage();"> 					
      	        <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage3.nextPage();"> 
      	        <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage3.lastPage();"> 
      	      </div>
      	    </td>
    	    </tr>
          <tr class = common >
            <TD  class= title > 
                <Input type=checkBox name="costCheck" value="1">工本费
            </TD>
	    	    <TD  class= input> 
	    		    <Input class= "common" name="cost" value="10"> 元
	    	    </TD> 
            <TD class= input colspan=4>
            </TD>   
          </tr>
          <TR class= common>
            <TD class= title>
              退保原因
            </TD>
            <TD class= input>
  			      <input class="codeNo" name="reason_tb" readOnly verify="退保原因|notnull&code:reason_tb&len<=10" ondblclick="showCodeList('reason_tb',[this,reason_tbName],[0,1]);" onkeyup="showCodeListKey('reason_tb',[this,reason_tbName],[0,1]);"><Input class="codeName" name=reason_tbName readonly elementtype=nacessary>
            </TD>  
            <TD class= input colspan=4>
            </TD>   
          </TR>
        </table>
      </div>
    </div>
    
    <br>
  <Input name="save1" class= cssButton type=Button hidden value="部分体检费扣除" onclick="">
  <Input name="save2" class= cssButton type=Button hidden value="全部体检费扣除" onclick="">
  <Input name="save3" class= cssButton type=Button hidden value="全部体检费清除" onclick="">
	<Input name="save" class= cssButton type=Button hidden value="保  存" onclick="edorTypeWTSave()">
	<Input name="goBack" class= cssButton type=Button value="返  回" onclick="returnParent()">
 
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
