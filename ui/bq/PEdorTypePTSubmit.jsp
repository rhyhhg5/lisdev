<%
//程序名称：PEdorTypePTSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//            lanjun    2005-6-6 10:49
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  System.out.println("-----PT submit---");
  LPAppntIndSchema tLPAppntIndSchema   = new LPAppntIndSchema();
  LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();
  PEdorPTDetailUI tPEdorPTDetailUI   = new PEdorPTDetailUI();
  
  CErrors tError = null;
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  transact = request.getParameter("fmtransact");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  
  
  //个人批改信息
	tLPEdorItemSchema.setPolNo(request.getParameter("PolNo"));
  tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	tLPEdorItemSchema.setInsuredNo(request.getParameter("InsuredNo"));
	tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
	tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));

  try
  {
  	VData tVData = new VData();  
	 	tVData.addElement(tG);
	 	tVData.addElement(tLPEdorItemSchema);

	  LPPolSchema tLPPolSchema = new LPPolSchema();
		tLPPolSchema.setPolNo(request.getParameter("PolNo"));
	  tLPPolSchema.setEdorNo(request.getParameter("EdorNo"));
		tLPPolSchema.setEdorType(request.getParameter("EdorType"));
		tLPPolSchema.setAmnt(request.getParameter("Amnt"));
		tLPPolSchema.setMult(request.getParameter("Mult"));
		tLPPolSchema.setPrem(request.getParameter("Prem"));

    	if (transact.equals("INSERT||MAIN")) 
    	{
    	   System.out.println("--------CalMode-------" + request.getParameter("CalMode"));
			   if (request.getParameter("CalMode").equals("O"))
			   {
			   tLPPolSchema.setMult(request.getParameter("RemainMulti"));	
			   		   
			   }
			   else
			   { 
			   tLPPolSchema.setAmnt(request.getParameter("Amnt"));
			   tLPPolSchema.setMult(request.getParameter("Mult"));
			   }
			   LPDutySchema tLPDutySchema = new LPDutySchema();
			   tLPDutySchema.setPolNo(request.getParameter("PolNo"));
		     tLPDutySchema.setEdorNo(request.getParameter("EdorNo"));
			   tLPDutySchema.setEdorType(request.getParameter("EdorType"));
			   tLPDutySchema.setAmnt(request.getParameter("Amnt"));		   
    	}
    	tVData.addElement(tLPPolSchema);
    	
    	System.out.println("start UI....");
    	tPEdorPTDetailUI.submitData(tVData,transact);
	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  if (FlagStr=="")
  {
    tError = tPEdorPTDetailUI.mErrors;
    if (!tError.needDealError())
    {                         
      System.out.println("not need"); 
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))
    	{
    	if (tPEdorPTDetailUI.getResult()!=null&&tPEdorPTDetailUI.getResult().size()>0)
    	{
    		Result = (String)tPEdorPTDetailUI.getResult().get(0);
    		if (Result==null||Result.trim().equals(""))
    		{
    			FlagStr = "Fail";
    			Content = "提交失败!!";
    		}
    	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

