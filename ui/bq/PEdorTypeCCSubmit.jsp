<%
//程序名称：PEdorTypeCCSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<% 
  String flag = "";
  String content = "";
  
	GlobalInput tG = (GlobalInput) session.getValue("GI");
  
  //个人合同信息
  LPContSchema tLPContSchema = new LPContSchema();
  tLPContSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPContSchema.setEdorType(request.getParameter("EdorType"));
  tLPContSchema.setContNo(request.getParameter("ContNo"));
  tLPContSchema.setPayMode(request.getParameter("PayMode"));
  tLPContSchema.setBankCode(request.getParameter("BankCode"));
  tLPContSchema.setBankAccNo(request.getParameter("BankAccNo"));
  tLPContSchema.setAccName(request.getParameter("AccName"));
  
  VData data = new VData();
  data.add(tG);
  data.add(tLPContSchema);

 	PEdorCCDetailUI tPEdorCCDetailUI = new PEdorCCDetailUI();
 	if (!tPEdorCCDetailUI.submitData(data))
 	{
    content = " 保存失败，原因是:" + tPEdorCCDetailUI.getError();
  	flag = "Fail";                      
  }
  else                                                                           
  {
    content = " 保存成功";
  	flag = "Success";
  }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>

