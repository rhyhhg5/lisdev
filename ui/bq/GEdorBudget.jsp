<html> 
<% 
//程序名称：GEdorBudget.jsp
//程序功能：退保试算
//创建日期：2006-04-19
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  
  <SCRIPT src="./GEdorBudget.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="GEdorBudgetInit.jsp"%>
  <title>退保试算</title>
</head>
<body  onload="initForm();" >
  <center><H2>团单退保试算<H2></center>
  <form action="./GEdorBudgetSave.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 保单号</TD>
        <TD  class= input > 
          <Input class= common name=grpContNoTop onkeydown="queryGrpContOnKeyDown();">
        </TD>
        <TD class = title > 客户号 </TD>
        <TD class = input >
        	<Input class= common name=appntNoTop onkeydown="queryGrpContOnKeyDown();">
        </TD>
        <TD class = title > 单位名称 </TD>
        <TD class = input >
        	<Input class= common name=grpNameTop onkeydown="queryGrpContOnKeyDown();">
        </TD>
        <TD class = title >
          <Input type =button class=cssButton value="查  询" onclick="queryGrpCont();">
        </TD>
      </TR>
    </TABLE>
    
    <table>
	  	<tr>
	        <td>
	          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpContGrid);">
	        </td>
	        <td class= titleImg> 有效保单信息 </td>
	  	</tr>
    </table> 
    <Div  id= "divGrpContGrid" style= "display: ''">
      <table  class= common>
      	<tr  class= common>
        		<td text-align: left colSpan=1>
      			<span id="spanLCGrpContGrid" >
      			</span> 
      	  	</td>
      	</tr>
      </table>	
      <Div id= "divPage" align="center" style= "display: 'none' ">	  		
        <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="turnPage1.firstPage();"> 
        <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="turnPage1.previousPage();"> 					
        <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="turnPage1.nextPage();"> 
        <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="turnPage1.lastPage();"> 
      </Div>
    </Div>
    
    <table>
    	<tr>
    		<td class= titleImg>
    			 集体保单资料
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrpContInfo" style= "display: ''">
      <table  class= common>
        <TR>
          <TD  class= title > 保单号</TD>
          <TD  class= input > 
            <input class="readonly" readonly name=grpContNo >
          </TD>
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class="readonly" name=grpName readonly >
          </TD>  
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" name=appntNo readonly >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            投保日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=polApplyDate readonly >
          </TD>
          <TD  class= title>
            生效日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=cValiDate readonly >
          </TD>
      
          <TD  class= title>
            交至日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=payToDate readonly >
          </TD>      
        </TR>
        <TR  class= common>
          <TD  class= title>
            满期日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=cInValiDate readonly >
          </TD> 
<!--          
          <TD  class= title>
            缴费频次
          </TD>
          <TD  class= input>
            <Input class="readonly" name=payIntv readonly >
          </TD>  
-->          
          <TD  class= title>
            期交保费
          </TD>
          <TD  class= input>
            <Input class="readonly" name=prem readonly >
          </TD>     
        </TR>
        <TR class= common>
          <TD class= title>
            理赔情况
          </TD> 
          <TD class= title>
            <Input class="readonly" name=claimState readonly >
          </TD> 
          <TD class= title>
            保全情况
          </TD>
          <TD class= title colspan=3>
            <Input class="readonly" name=edorState readonly >
          </TD>
        </TR>
      </table>
    </Div> 
    

         
    <table>
      <tr>
        <td class=common>
          <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCGrpPol);">
        </td>
        <td class=titleImg>险种选择</td>
      </tr>
    </table>
    <Div id="divLCGrpPol" style="display: ''">
      <table class=common>
        <tr class=common>
          <td text-align: left colSpan=1>
            <span id="spanLCGrpPolGrid">  </span>
          </td>
        </tr>
      </table>
      <Div id= "divPage2" align="center" style= "display: 'none' ">	  		
        <INPUT VALUE="首  页" class= cssButton TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT VALUE="上一页" class= cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT VALUE="下一页" class= cssButton TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT VALUE="尾  页" class= cssButton TYPE=button onclick="turnPage2.lastPage();"> 
      </Div>
    </div>
        <table class=common>
      <TR  class= common> 
        <TD  class= title > 预计退保生效日期 </TD>
        <TD  class= input id="divEdorValiDate"> 
          <Input class= "coolDatePicker" dateFormat="short" name=edorValiDate >
        </TD>
        <TD  class= input id="divEdorValiDatePassIn"> 
          <Input class= "readonly" readonly name=edorValiDatePassIn >
        </TD>
        <TD class = title> 长期险预计交至日期 </TD>
        <TD class = input>
        	<Input class= "coolDatePicker" dateFormat="short" name=payToDateLongPol >
        </TD>
        <TD class = title ></TD>
        <TD class = input>
      </TR>
    </TABLE>
    <Input type =button class=cssButton value="退保试算" onclick="edorBudget()">
    <!--Input type =button class=cssButton value="保  存" onclick="edorTypeCTSave()"-->
    <Input type =button class=cssButton value="返  回" onclick="returnParent()">
    
    <input type=hidden id="edorNo" name="edorNo">
	  <input type=hidden id="edorType" name="edorType">
	  <div id="test"></div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
