//程序名称：PEdorUWManuHealthQ.js
//程序功能：保全人工核保体检资料查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容

//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var flag;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");      
    showInfo.close();
    alert(content);
    //parent.close();
  }
  else
  { 
	var showStr="操作成功";
  	showInfo.close();
  	alert(showStr);
  	//parent.close();
  	
    //执行下一步操作
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
         

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


function manuchkhealthmain()
{
	fm.submit();
}

// 查询单条
function easyQueryClickSingle()
{

	// 书写SQL语句
	var strsql = "";
	var tPolNo = "";
	var tEdorNo = "";
	tPolNo = fm.PolNo.value;
	tEdorNo = fm.EdorNo.value;
	//tInsuredNo = fm.InsuredNo.value;
	
	strsql = "select polno,insuredname,pedate,peaddress,PEBeforeCond,remark,printflag,insuredno,PEResult from LPPENotice where 1=1"
				 + " and polno = '"+ tPolNo + "'"
				 + " and edorno = '"+ tEdorNo + "'";
				 //+ " and InsuredNo = '"+ tInsuredNo +"'";
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert("没有体检资料！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult); 
  fm.PolNo.value = turnPage.arrDataCacheSet[0][0];
  fm.InsureNo.value = turnPage.arrDataCacheSet[0][1];
  fm.EDate.value = turnPage.arrDataCacheSet[0][2];
  fm.Hospital.value = turnPage.arrDataCacheSet[0][3];
  fm.IfEmpty.value = turnPage.arrDataCacheSet[0][4];
  fm.Note.value = turnPage.arrDataCacheSet[0][5];
  fm.PrintFlag.value = turnPage.arrDataCacheSet[0][6];
  fm.InsuredNoHide.value = turnPage.arrDataCacheSet[0][7];
  fm.Content.value = turnPage.arrDataCacheSet[0][8];
  return true;
}


// 查询按钮
function easyQueryClick()
{
	// 初始化表格                                                                                                                                                           
	//initUWHealthGrid();
	// 书写SQL语句
	var strsql = "";
	var tPolNo = "";
	var tEdorNo = "";
	tPolno = fm.PolNo.value;
	tEdorNo = fm.EdorNo.value;
	//tInsuredNo = fm.InsuredNo.value;
	strsql = "select peitemcode,peitemname,freepe from LPPENoticeItem where 1=1"
				 + " and polno = '"+ tPolno +"'"
				 + " and EdorNo= '"+ tEdorNo +"'";
				 //+ " and InsuredNo = '"+ tInsuredNo +"'";
				 				 

  //查询SQL，返回结果字符串
  turnPage.strQueryResult = easyQueryVer3(strsql, 1, 1, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert("查询失败！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = HealthGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strsql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


function initInsureNo(tPolNo)
{
	var i,j,m,n;
	var returnstr;
	
	var strSql = "select CustomerNo,name from lcinsured where 1=1 "
	       + "and polno = " +tPolNo;
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  
  //判断是否查询成功
  //alert(turnPage.strQueryResult);
  if (turnPage.strQueryResult == "")
  {
    alert("查询失败！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  //turnPage.pageDisplayGrid = VarGrid;    
          
  //保存SQL语句
  //turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  //turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  var returnstr = "";
  var n = turnPage.arrDataCacheSet.length;
  //alert("N:"+n);
  if (n > 0)
  {
  	for( i = 0;i < n; i++)
  	{
  		m = turnPage.arrDataCacheSet[i].length;
  		//alert("M:"+m);
  		if (m > 0)
  		{
  			for( j = 0; j< m; j++)
  			{
  				if (i == 0 && j == 0)
  				{
  					returnstr = "0|^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i == 0 && j > 0)
  				{
  					returnstr = returnstr + "|" + turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j == 0)
  				{
  					returnstr = returnstr+"^"+turnPage.arrDataCacheSet[i][j];
  				}
  				if (i > 0 && j > 0)
  				{
  					returnstr = returnstr+"|"+turnPage.arrDataCacheSet[i][j];
  				}
  				
  			}
  		}
  		else
  		{
  			alert("查询失败!!");
  			return "";
  		}
  	}
}
else
{
	alert("查询失败!");
	return "";
}
  //alert("returnstr:"+returnstr);		
  fm.InsureNo.CodeData = returnstr;
  return returnstr;
}

//查询被保人信息
function queryInsured()
{
  var sql = "select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday, " +
            "       a.IDType, a.IDNo, '', c.PrtSeq, " +
            "       case when c.PEState is null then '体检通知书未录入' " +
            "            when c.PEState = '01' then '体检通知书已录入' " +
            "            when c.PEState = '02' then '体检通知书已打印' end " +
            "from LCInsured a left join LPPENotice c " +
            "     on a.InsuredNo = c.CustomerNo " +
            "     and c.EdorNo = '" + fm.EdorNo.value + "', " + 
            "     LCCont b " +
            "where a.ContNo = b.ContNo " +
            "and a.AppntNo = '" + fm.AppntNo.value + "' " +
            "and b.AppFlag = '1'";
  turnPage.pageLineNum = 100;
  turnPage.queryModal(sql, LCInsuredGrid);
}

//查询体检项目
function queryHealth()
{
  var sql = "select PEItemCode, PEItemName, FreePE from LPPENoticeItem " +
            "where EdorNo = '" + fm.EdorNo.value + "' " +
            "and PrtSeq = '" + fm.PrtSeq.value + "' ";
  turnPage2.pageDivName = "divPage2";
  turnPage2.pageLineNum = 100 ; 
  turnPage2.queryModal(sql, HealthGrid);
}

function queryDisDesb()
{
  var sql = "select DisDesb, DisResult, ICDCode from LPPENoticeResult " +
            "where EdorNo = '" + fm.EdorNo.value + "' " +
            "and PrtSeq = '" + fm.PrtSeq.value + "' ";
  turnPage3.pageDivName = "divPage3";
  turnPage3.pageLineNum = 100 ; 
  turnPage3.queryModal(sql, DisDesbGrid);
}

function queryOtherInfo()
{
  var sql = "select Remark from LPPENoticeResult " +
          "where EdorNo = '" + fm.EdorNo.value + "' " +
          "and PrtSeq = '" + fm.PrtSeq.value + "' ";
  var result = easyExecSql(sql);
  if (result != null)
  {
    fm.Note.value = result[0][0];
  }
}

function onClickedInsured()
{

  var selNo = LCInsuredGrid.getSelNo() - 1;
  fm.ContNo.value = LCInsuredGrid.getRowColData(selNo, 1);
  fm.InsuredNo.value = LCInsuredGrid.getRowColData(selNo, 2);
  fm.InsuredName.value = LCInsuredGrid.getRowColData(selNo, 3);
  fm.PrtSeq.value = LCInsuredGrid.getRowColData(selNo, 9);
  //fm.PEState.value = LCInsuredGrid.getRowColData(selNo, 10);
  queryHealth();
  queryDisDesb();
  queryOtherInfo();
  parent.fraPic.location.href = "../common/EasyScanQuery/EasyScanQuery.jsp?prtNo=" + fm.PrtSeq.value + "&BussNoType=92&BussType=BQ&SubType=BQ03";
}

function saveDisDesb()
{
  if  (DisDesbGrid.mulLineCount<1)
  {
    alert("疾病结果栏不能为空！");
    return false;		
  }	
  var selNo = LCInsuredGrid.getSelNo();
  if (selNo == 0)
  {
		alert("请选择一条记录！");
		return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   	
  
  fm.action= "./PEdorUWManuHealthQSave.jsp";
  fm.submit(); //提交
}