<html> 
<% 
//程序名称：PEdorTypeLQInput.jsp
//程序功能：
//创建日期：2008-04-10
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdor.js"></SCRIPT>
	<SCRIPT src="./PEdorTypeLQ.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="PEdorTypeLQInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./PEdorTypeLQSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common> 
        <td class="title"> 受理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 批改类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
          <input class="readonly" type="text" readonly name="ContNo" >
        </td>
    	</tr>
    </table> 
	<%@include file="ULICommon.jsp"%> 
	<!-- 
    <Div  id= "divPolInfo" style= "display: ''">
    <table>
      <tr>
       <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);"></td>
       <td class= titleImg>保单险种信息</td>
      </tr>
    </table>
    <Div  id= "divPolGrid" style= "display: ''">
    <table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanPolGrid" ></span></td>
      </tr>
    </table>					
    </Div>
  </Div>
   -->
       <div id="divInsuAccGrid" style="display:''">
    	<table class=common>
		  <tr class=common>
			<td text-align: left colSpan=1>
					<span id="spanInsuAccGrid"></span>
			</td>
		 </tr>
		</table>
		
   </div>   
    <br>
   <table class = "common"> 
    <tr  class= "common"> 
      <td class="title">本次申领金额</td>
      <td class= input><Input class= "common" name="AppMoney" verify="申领金额|notnull" > 元</td>
      <td class="title"></td>
      <td class= input></td>
      <td class="title"></td>
      <td class= input></td>
    </tr>
  </table> 
  <Div  id= "divSubmit" style="display:''">
    <table class = common>
      <TR class= common>
        <input type=Button name="save" class= cssButton value="保  存" onclick="saveDate();">
        <input type=Button name="goBack" class= cssButton value="返  回" onclick="returnParent();">
        <!-- 
        <input type=Button name="returnButton" class= cssButton value="测试" onclick="checkAppMoney();">
         -->
      </TR>
    </table>
  </Div>
  <br>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="ContType" name="ContType">
  <input type=hidden name="EdorAcceptNo">
  <input type=hidden name="PolNo">
  <input type=hidden name="InsuredNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
