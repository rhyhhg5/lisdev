<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BalanceListPrint.jsp
//程序功能：
//创建日期：2008-03-21 11:05上午
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>

<%@page import="java.util.*"%>
<%@include file="../common/jsp/Download.jsp"%>


<%
  boolean operFlag = true;
  String flagStr = "";
  String content = "";

  GlobalInput tG = (GlobalInput)session.getValue("GI");
  boolean errorFlag = false;
  String templateName = "PrtBalanceList.vts";
  String sql = request.getParameter("PrintSql");
  String tContNo = request.getParameter("ContNo");
  if(sql == null || sql.equals(""))
  {
    sql = "select a.PayDate, a.MoneyType, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    (case when a.OtherType = '6' " 
          + "             then ltrim(rtrim(char(year(a.PayDate - 1 day))))||'-'||ltrim(rtrim(char(month(a.PayDate - 1 day)))) "
          + "          else '' end), "
          + "    a.OtherNo, "
          + "    varchar(nvl((case when a.Money >= 0 then a.Money end),0)), "
          + "    varchar(nvl((case when a.Money <= 0 then abs(a.Money) end),0)), "
          + "    varchar(nvl((select sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
          + "    a.SerialNo x "
          + "from LCInsureAccTrace a, LCInsureAcc b "
          + "where a.PolNo = '" + request.getParameter("PolNo") + "'" 
          + "    and a.InsuAccNo = '" + request.getParameter("InsuAccNo") + "'"
          + "    and a.PolNo = b.PolNo and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType not in ('1','5') "
          //+ "        and a.PayDate < b.BalaDate "
          + "    group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "
          
          + "union all "
          
          //契约进帐户的轨迹
          + "select a.PayDate, a.MoneyType, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    '', "
          + "    a.OtherNo, "
          + "    varchar((select Prem from LCPrem where PolNo = a.PolNo)), "
          + "    varchar((select Prem from LCPrem where PolNo = a.PolNo) - a.Money), "
          + "    varchar(nvl((select sum(Money) from LCInsureAccTrace where PolNo = a.PolNo and InsuAccNo = a.InsuAccNo and SerialNo < a.SerialNo), 0) + a.Money), "
          + "    a.SerialNo x "
          + "from LCInsureAccTrace a, LCInsureAcc b "
          + "where a.PolNo = '" + request.getParameter("PolNo") + "'" 
          + "    and a.InsuAccNo = '" + request.getParameter("InsuAccNo") + "'"
          + "    and a.PolNo = b.PolNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType = '1' "
          + "group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.PolNo, a.SerialNo "
          + "union all "
          
          //理赔的轨迹
          + "select a.PayDate, a.MoneyType, "
          + "    (select CodeAlias from LDCode where CodeType = 'accmanagefee' and CodeName = a.MoneyType fetch first 1 rows only), "
          + "    '', "
          + "    a.OtherNo, "
          + "    '0.00', "
          + "    varchar(-a.Money), "
          + "    varchar(nvl((select sum(Money) from LCInsureAccTrace where ContNo = a.ContNo and InsuAccNo = a.InsuAccNo ), 0)), "
          + "    a.SerialNo x "
          + "from LCInsureAccTrace a, LCInsureAcc b "
          + "where a.ContNo = '" + tContNo + "'" 
          + "    and a.InsuAccNo = '" + request.getParameter("InsuAccNo") + "'"
          + "    and a.ContNo = b.ContNo "
          + "    and  a.InsuAccNo = b.InsuAccNo "
          + "    and a.OtherType = '5' "
          + "group by a.PayDate, a.MoneyType, a.OtherType, a.OtherNo, a.Money, b.LastAccBala, a.InsuAccNo, a.ContNo, a.SerialNo "
          + "order by x ";
  }
  
	System.out.println("打印查询:"+sql);
	
    //设置表头
    String[][] tTitle = {{"序号", "发生日期", "业务类型", "结算月度", "业务员",
                         "增加金额", "减少金额", "最终金额"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8};
    
    //String tAppntName = new ExeSQL().getOneValue("select AppntName from LCCont where ContNo = '" + tContNo + "' ");

	String sheetNAME =  "contno_"+ tContNo;
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();

    String[] sheetName ={sheetNAME};
    
	  String date=PubFun.getCurrentDate().replaceAll("-","");
	  String time=PubFun.getCurrentTime().replaceAll(":","");
    
	  
	  
    String downLoadFileName = "万能账户历史信息_"+tG.Operator+"_"+ date + time + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
  
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(sql,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
  
        //返回客户端
        if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
        out.clear();
        out = pageContext.pushBody();
        if(errorFlag)
        {
  
%>
<html>
<script language="javascript">	
	alert("<%=content%>");
	top.close();
</script>
</html>
<%
  	}
%>