<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LLWSDiskImportSave.jsp
//程序功能：磁盘导入上传
//创建日期：2005-12-13
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="com.jspsmart.upload.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%
	String flag = "";
	String content = "";
	String path = "";
	String fileName = "";  //上传文件名
	
	//得到全局变量
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	
  //得到excel文件的保存路径
 	path = application.getRealPath(CommonBL.getXmlPath()) + "/";

	//上传excel文件并保存，得到文件名
	SmartUpload loader = new SmartUpload();;
	try
	{ 
		loader.initialize(config, request, response);
		loader.setTotalMaxFileSize(10000000);
		loader.upload();
		loader.save(path);
		fileName = path + loader.getFiles().getFile(0).getFileName();
	}
	catch (Exception e)
	{
		e.printStackTrace();
	}
	
	String edorNo = loader.getRequest().getParameter("EdorNo");
	System.out.println("edorNo="+edorNo);
	String edorType = loader.getRequest().getParameter("EdorType");
	String grpContNo = loader.getRequest().getParameter("GrpContNo");
	String configFileName = path + BQ.FILENAME_IMPORT;
	
  LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
  tLPDiskImportSchema.setEdorNo(edorNo);
  tLPDiskImportSchema.setEdorType(edorType);
  tLPDiskImportSchema.setGrpContNo(grpContNo);
  
  VData data = new VData();
  data.add(tGI);
  data.add(tLPDiskImportSchema);
 
	//从磁盘导入被保人清单
	System.out.println(fileName+configFileName+edorType);
	LLWSImportBL tLLWSImportBL = new LLWSImportBL(fileName, configFileName,edorType);
	if (!tLLWSImportBL.submitData(data))
	{
	  flag = "Fail";
		content = "磁盘导入失败，原因是：" + tLLWSImportBL.getError() +
		          "\n请检查导入模版的格式和数据！";
	}
	else
	{
	  flag = "Succ";
		content = "磁盘导入成功！";
	}
  content = PubFun.changForHTML(content);
  System.out.println(content);
%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>

