<html> 
<% 
//程序名称：PEdorTypeBAInput.jsp
//程序功能：
//创建日期：2008-04-10
//创建人  pst
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdor.js"></SCRIPT>
	<SCRIPT src="./PEdorTypeBA.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="PEdorTypeBAInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./PEdorTypeBASubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common> 
        <td class="title"> 受理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 批改类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
          <input class="readonly" type="text" readonly name="ContNo" >
        </td>
    	</tr>
    </table>  
    <Div  id= "divPolInfo" style= "display: ''">
    <table>
      <tr>
       <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolBase);"></td>
       <td class= titleImg>基本信息</td>
      </tr>
     
    </table>
  </Div>
   <Div id="divPolBase" style= "display: '' ">
   <table class = "common"> 
   	 <tr  class= "common"> 
      <td class="title">投保日期</td>
      <td class= input><input class="readonly" type="text" readonly name="CvaliDate" ></td>
      <td class="title">当前账户金额</td>
      <td class= input><input class="readonly" type="text" readonly name="AccBala" ></td>
      <td class="title">交费状态</td>
      <td class= input><input class="readonly" type="text" readonly name="PayState" ></td>
    </tr>
    <tr  class= "common"> 
      <td class="title">保单保费</td>
      <td class= input><input class="readonly" type="text" readonly name="Prem" ></td>
      <td ></td>
      <td ></td>
      <td ></td>
      <td ></td>
    </tr>
  </table>
  <br>
  <div id= "divNewPol" style= "display: ''">
    <table class= common>
      <tr  class= common>
        <td text-align: left colSpan=1>
          <span id="spanPolGrid"></span> 
        </td>
      </tr>
    </table>			
	<div id= "divPage" align=center style= "display: 'none' ">
		<input value="首  页" class=cssButton type=button onclick="turnPage1.firstPage();"> 
		<input value="上一页" class=cssButton type=button onclick="turnPage1.previousPage();"> 					
		<input value="下一页" class=cssButton type=button onclick="turnPage1.nextPage();"> 
		<input value="尾  页" class=cssButton type=button onclick="turnPage1.lastPage();"> 
	</div>
	<font color='red'>请在“新保额”下面录入需要调整后的保额。如果<b>增加</b>保额，请录入健康告知。</font>		
  </div>
  <br>
  <div  id= "divSubmit" style="display:''">
    <table class = common>
      <tr class= common>
        <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
        <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
        <input type="button" Class="cssButton"  value="健康告知录入" onclick="inputImpart();" >
      </tr>
    </table>
  </div>
  <br>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="ContType" name="ContType">
  <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
  <input type=hidden id="EdorValiDate" name="EdorValiDate">
  <input type=hidden id="EdorAppDate" name="EdorAppDate">
  <input type=hidden id="PayToDate" name="PayToDate">
  <input type=hidden id="PayStartDate" name="PayStartDate">
  <input type=hidden id="PayIntv" name="PayIntv">
  <input type=hidden id="DetailType" name="DetailType">
  <input type=hidden id="InsuredNo" name="InsuredNo">
  <input type=hidden id="PayEndDate" name="PayEndDate">
  <!-- 
  <input type=hidden id="CurAmnt" name="CurAmnt">
  <input type=hidden id="AppMoney" name="AppMoney">
   -->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
