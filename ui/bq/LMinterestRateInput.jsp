<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="java.util.*"%> 
<html>
	<%
		//程序名称：
		//程序功能：个人保全
		//创建日期：2011-10-31
		//创建人  ：XP
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	
	<%	
		GlobalInput tG = new GlobalInput(); 
	  	tG=(GlobalInput)session.getValue("GI");
	  	String Operator=tG.Operator;
	%>
	<script>
		var msql=" 1 and comcode in (select code from ldcode where codetype=#loancom#)";
	</script>	
	<head>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/CommonTools.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="./PEdor.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="./LMinterestRateInput.js"></SCRIPT>
		<%@include file="../common/jsp/UsrCheck.jsp"%>
		<%@include file="LMinterestRateInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="./LMinterestRateSave.jsp" method=post name=fm
			target="fraSubmit">
			
			<div align="right">
				<br>
				<Input type=button name="save" class=cssButton value="保  存"
					onclick="lmRateSave()">&nbsp;&nbsp;&nbsp;
				<Input type=button name="update" class=cssButton value="修  改"
					onclick="lmRateUpdate()">&nbsp;&nbsp;&nbsp;
				<Input type=Button name="query" class=cssButton value="查  询"
					onclick="lmRateQuery()">&nbsp;&nbsp;&nbsp;
				<Input type=Button name="delete" class=cssButton value="删  除"
					onclick="lmRateDelete()">
			</div>
			<table class=common>
				<tr class=common>
					<td class="title">
						险种
					</td>
					<td class="input"><Input class=codeNo verify="险种编码|notnull" name=RiskCode readOnly ondblclick="return showCodeList('loanriskcode',[this,RiskName], [0,1],null,null,null,1);" onkeyup="return showCodeListKey('loanriskcode',[this,RiskName], [0,1],null,null,null,1);"><Input class=codename name=RiskName readonly >
					<font style=color:red;valign:right> *</font> </td>
					<td class="title">
						利率起始日期
					</td>
					<td class="input">
						<input class=coolDatePicker dateFormat="short" name=rateStartDate readonly>
						<font style=color:red;valign:right> *</font>
					</td>
					<td class="title">
						利率终止日期
					</td>
					<td class="input">
						<input class=coolDatePicker dateFormat="short" name=rateEndDate  readonly>
						<font style=color:red;valign:right> *</font>
					</td>
				</tr>
				<tr class=common>
  		    <TD  class= title> 管理机构 </TD>
          <TD  class= input >
            <Input class= "codeno" name=aManageCom readOnly  verify="管理机构|notnull" ondblclick="return showCodeList('comcode10',[this,ManageComName],[0,1],null,msql,1);" onkeyup="return showCodeListKey('comcode10',[this,ManageComName],[0,1],null,msql,1);" ><Input class=codename readonly name=ManageComName>
            <font style=color:red;valign:right> *</font>
          </TD>
					<td class="title">
						最小保费金额
					</td>
					<td class=input>
						<input class=common  name=MinPremLimit >
					</td>
					<td class="title">
						最大保费金额
					</td>
					<td class=input>
						<input class=common  name=MaxPremLimit >
					</td>
				</tr>
				<tr class=common>
					<td class="title">
						利率
					</td>
					<td class=input>
						<input class=common  name=Rate >
						<font style=color:red;valign:right> *</font>
					</td>
					<td class="title">
						确认利率
					</td>
					<td class="input">
						<input class=common name=confirmRate >
						<font style=color:red;valign:right> *</font>
					</td>
				</tr>
				<tr>
					<td class=button>
						<input value="重  置" type=button onclick="reSet();"class=cssButton >
					</td>
				</tr>	
			</table>
			<Div id="divRateInfo" style="display: ''">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divRateGrid);">
						</td>
						<td class=titleImg>
							查询结果
						</td>
					</tr>
				</table>
				 <!-- 信息（列表） -->
				  <div id="divRateGrid" style="display:''">
					<table class=common>
				      <tr class=common>
					    <td text-align:left colSpan=1><span id="spanRateGrid"></span></td>
					  </tr>
					</table>
				  </div>
				<Div  id= "divturnPage" style="display:none">
					<div align="center">
				      <INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage.firstPage();"> 
				      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage();"> 					
				      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage();"> 
				      <INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage.lastPage();"> 	
				    </div>
					</table>
				</DIV>
			<input type=hidden id="fmtransact" name="fmtransact" >
			<input type="hidden" id="Serialno" name="Serialno">
			<input type=hidden id="DRiskCode" name="DRiskCode" >  
			<input type=hidden id="DrateStartDate" name="DrateStartDate" >  
			<input type=hidden id="DrateEndDate" name="DrateEndDate" >  
			<input type="hidden" id="DManageCom" name = "DManageCom">
			<input type=hidden id="DRate" name="DRate" >    
			<input type=hidden id="Operator" name="Operator" value='<%=Operator%>'>
			</Div>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
