<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2003-01-14 16:49:22
//创建人  ：Dingzhong
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script>
    var tsql = "1 and code in (select code from ldcode where codetype = #paymodebqpay#" +
    " union all select 8 from dual )";
</script>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeCC.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeCCInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeCCSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
      <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" type="hidden" readonly name=EdorType>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>
  <br>
  <table>
   	<tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
      </td>
      <td class= titleImg>
        保单信息
      </td>
   	</tr>
  </table> 
	<Div  id= "divLCPol" style= "display: ''"> 
		<input type=hidden name="ManageCom"/>
  <table  class= common>    
		<TR>
			<TD class = title>
			 	投保人客户号
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=AppntNo >
			</TD>
			<TD class= title>
				投保人姓名
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=AppntName >
			</TD>
			<TD class= title>
				生效日期
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=CValidate >
			</TD>      
		</TR> 
		<TR>
			<TD class= title>
				交至日期
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=PayToDate >
			</TD>
			<TD class= title>
				总保费
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=Prem >
			</TD>   
			<TD class= title>
				总保额
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=Amnt >
			</TD>
		</TR>	  
		<TR>  
			<!--TD class= title>
				交费频率
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=PayIntv >
			</TD-->    
			<TD class= title>
			  交费方式
			</TD>    
			<TD class= input colspan="5">
			  <Input class="codeNo" name="PayMode"  verify="交付费方式|notnull&code:PayMode"  ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" onkeyup="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"><Input class="codeName" name="PayModeName"  elementtype="nacessary" readonly>
			</TD>		       	       
		</TR>
		<Div  id="DivPayMode" style= "display: ''"> 
		<TR id="TrBank" style="display:''"> 
			<TD class= title>
				转帐银行
			</TD>
			<TD class= input>
				<input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=20 ondblclick=" return showCodeList('bankcode',[this,BankCodeName],[0,1],null,fm.PayMode.value,fm.ManageCom.value,1);" onkeyup="return showCodeListKey('bankcode',[this,BankCodeName],[0,1],null,fm.PayMode.value,fm.ManageCom.value,1);" ><Input class= codeName name="BankCodeName" elementtype=nacessary readonly >
			</TD>	
			<TD class= title>
			  转账帐号
			</TD>     
			<TD class= input>
			  <Input class= "common" name=BankAccNo verify="转账帐号|int">
			</TD>		      
			<TD class= title>
			  账户名
			</TD>    
			<TD class= input>
			  <Input class= "common" name=AccName >
			</TD>		       
    </TR>
    <TR><TD><input type=button value="银行查询" class=cssButton onclick="querybank();"></TD></TR>
   </Div>
	</Table>
	</div>
    <br>
		<div>
			<Input class= cssButton type=Button value="保  存" onclick="edorTypeCCSave()">
			<Input class= cssButton type=Button value="取  消" onclick="initForm()">
			<Input class= cssButton type=Button value="返  回" onclick="returnParent()">
		</div>
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="hPayIntv" name="hPayIntv">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
