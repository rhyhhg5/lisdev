<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>
<html>
  <%
    //程序名称：
    //程序功能：证件失效投保人清单查询下载
    //创建日期：2018-5-25
    //创建人  ：李怀宇
    //更新记录：  更新人    更新日期     更新原因/内容
  %>
  
  <%@ page language="java" contentType="text/html; charset=GBK"
	pageEncoding="GBK"%>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@page import = "com.sinosoft.lis.schema.*"%>
  <%@page import = "com.sinosoft.lis.vschema.*"%>
  <%@page import = "com.sinosoft.lis.bank.*"%>
  <%@page import = "com.sinosoft.lis.pubfun.*"%>

  <%@include file="../common/jsp/UsrCheck.jsp"%>

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=GBK">
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <script src="./CustomerIDEndDateQuery.js"/>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <%@include file='./CustomerIDEndDateQueryInit.jsp'%>
    <title>证件失效客户清单查询下载</title>
  </head>
  <body onload="initForm();">
    <form action="./CustomerIDEndDateSubmit.jsp" method=post name=fm target="fraSubmit">
      <div id="divselect">
      	<table class="common">
      	  <tr class="common">
      	    <td class="title">
      	          管理机构
      	    </td>
      	    <td class="input">
      	      <Input class="codeNo" name="ManageCom" readOnly ondblclick="return showCodeList('comcode',[this,ManageComName], [0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName], [0,1]);" ><Input class="codeName" name="ManageComName" elementtype="nacessary" value="" readonly >
      	    </td>
      	    <!--
      	    <td class="title">
      	          查询层次
      	    </td>
      	    <td class="input">
      	      <Input class=codeno name=SelectType CodeData="0|^0|客户层^1|保单层"  ondblclick="return showCodeListEx('SelectType',[this,SelectName],[0,1],null,null,null,1);"
				onkeyup="return showCodeListKeyEx('SelectType',[this,SelectName],[0,1],null,null,null,1);"><input class=codename name=SelectName readonly=true>
      	    </td>
      	    -->
      	    <TD>
      	      <input class=cssButton type=button value="查  询" onclick="showList()">
            </TD> 
      	  </tr>
      	</table>
      	<!-- 
      	<table>
    	  <tr>
            <td class="common">
		      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divList1);">
    	    </td>
            <td class="titleImg">
    	  	  客户层清单列表
    	    </td>
    	  </tr>
        </table>
        <div id="divList1">
          <table class="common">
       		<tr class="common">
      	  	  <td text-align: left colSpan=1>
  			    <span id="spanList1Grid">
  			    </span> 
  			  </td>
  			</tr>
    	  </table>
    	  <Div id= "divPage2" align=center style= "display: '' ">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
		  </Div>
        </div>
        -->
        <table>
    	  <tr>
            <td class="common">
		      <IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divList2);">
    	    </td>
            <td class="titleImg">
    	  	  清单列表
    	    </td>
    	  </tr>
        </table>
        <div id="divList2">
          <table class="common">
       		<tr class="common">
      	  	  <td text-align: left colSpan=1>
  			    <span id="spanList2Grid">
  			    </span> 
  			  </td>
  			</tr>
    	  </table>
    	  <Div id= "divPage3" align=center style= "display: '' ">
		    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
		  </Div>
        </div>
      </div>
      <input class=cssButton type=button value="打印清单" onclick="printList();">
	  <input type=hidden id="sql" name="sql" value="">
    </form>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </body>
</html>