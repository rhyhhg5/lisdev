<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>	

<%
//程序名称：OmnipotenceAccInit.jsp
//程序功能：万能账户信息界面初始化
//创建日期：2007-12-20
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<% 
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>

<script language="JavaScript">
//显示框的初始化 
function initBox()
{
  try
  {
    fm.all('AccFoundDate').value = "";
    fm.all('AccEndDate').value = "";
    fm.all('InsuAccBala').value = "";
    fm.all('BalaMonth').value = "";
    fm.all('BalaDate').value = "";
    queryAcc();
    queryEndDate();
    queryAccTrace();
  }
  catch(ex)
  {
    alert("在OmnipotenceAccInit.jsp-->InitBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initOmnipotenceAccTraceGrid(); 
    initBox();
  }
  catch(re) 
  {
    alert("在OmnipotenceAccInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//Mulline的初始化
function initOmnipotenceAccTraceGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=3;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="发生时间";      //列名PayDate
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
   
    iArray[2]=new Array();
    iArray[2][0]="业务类型号";      //列名MoneyType
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=3;   
    
    iArray[3]=new Array();
    iArray[3][0]="业务类型";      //列名MoneyType汉字
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[4]=new Array();
    iArray[4][0]="结算月份";      //列名BalanMonth
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[5]=new Array();
    iArray[5][0]="业务号";      //列名OtherNo
    iArray[5][1]="180px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="增加金额";      //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[7]=new Array();
    iArray[7][0]="减少金额";      //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[8]=new Array();
    iArray[8][0]="最终金额";      //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[9]=new Array();
    iArray[9][0]="生成日期";      //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=3;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[10]=new Array();
    iArray[10][0]="生成时间";      //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=200;           //列最大值
    iArray[10][3]=3;             //是否允许输入,1表示允许，0表示不允许
    
    OmnipotenceAccTraceGrid = new MulLineEnter("fm", "OmnipotenceAccTraceGrid"); 
	  //设置Grid属性
    OmnipotenceAccTraceGrid.mulLineCount = 10;
    OmnipotenceAccTraceGrid.displayTitle = 1;
    OmnipotenceAccTraceGrid.locked = 1;
    OmnipotenceAccTraceGrid.canSel = 0;	
    OmnipotenceAccTraceGrid.canChk = 0;
    OmnipotenceAccTraceGrid.hiddenSubtraction = 1;
    OmnipotenceAccTraceGrid.hiddenPlus = 1;
    OmnipotenceAccTraceGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在OmnipotenceAccInit.jsp-->initOmnipotenceAccTraceGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
