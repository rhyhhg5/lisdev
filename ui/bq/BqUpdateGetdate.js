//程序名称：BqUpdateGetdate.js
//程序功能：应付日期修改
//创建日期：20170426
//创建人  ：ys

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  initBQGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showContInfo();
	}	
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function load(){
    document.getElementById("query").onclick();
   // document.getElementById("target").click();
}


// 查询按钮
function easyQueryClick()
{
	initBQGrid();
	var tEdorAcceptNo = fm.EdorAcceptNo.value;
	if(tEdorAcceptNo==""){
		alert("保单号为空,请输入保单号")
		return "";
	}
	// 书写SQL语句
	var strSQL = "";
	    strSQL = "select b.actugetno 给付凭证号,a.edoracceptno 保全受理号,(case when b.othernotype ='10' then '个人保全' when b.othernotype ='3' then '团体保全' else '其他' end) 业务类型,"
	    	   + "a.EdorAppDate 申请日期,a.confdate 结案日期,b.SumGetMoney  退费金额,b.ShouldDate 应付日期,b.confdate 实付日期,BankOnTheWayFlag 银行在途 "
	    	   + "from lpedorapp a,ljaget b "
	    	   + "where a.edoracceptno=b.otherno "
	    	   + "and a.edorstate='0' "
	    	   + "and b.confdate is null "
	    	   + "and a.edoracceptno='"+fm.EdorAcceptNo.value+"' "
	    	   + "and (b.BankOnTheWayFlag is null or b.BankOnTheWayFlag<>'1') "
	    	   + "with ur";
	
	  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert(turnPa);
    alert("查询失败！");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BQGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}

function BQinit() {
	for (i = 0; i < BQGrid.mulLineCount+1; i++) {
		if(BQGrid.getSelNo()==i){
			var beforeMarkettype = BQGrid.getRowColData(BQGrid.getSelNo()-1, 7);
			fm.all('beforeShouldDate').value = beforeMarkettype;
			fm.flag.value=BQGrid.getSelNo();
			break;
		}
	}
	
}

function updateShouldDate() {
	var mSelNo = BQGrid.getSelNo();// 判断该行的 Radio 单选框被选中,行号是从1开始,如果没有选中行,返回值是0
	if (mSelNo == 0) {
		alert("请选择一条数据！");
		return false;
	}
	var afterShouldDate = "";
	afterShouldDate = fm.afterShouldDate.value;
	if (afterShouldDate == "") {
		alert("请选择修改的日期！");
		return false;
	}
	fm.fmtransact.value = "UPDATE";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initBQGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				TempFeeGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

function getStatus()
{
  var i = 0;
  var showStr="正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面22222";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,*,0,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}