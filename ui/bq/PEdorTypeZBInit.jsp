<%
//程序名称：PEdorTypeZBInit.jsp
//程序功能：
//创建日期：2008-04-10
//创建人  ：pst
//更新记录：  更新人    更新日期     更新原因/内容
%>  
<script language="JavaScript">  
function initInpBox()
{ 
	//判断是否是在工单查看中查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
 
  	if (flag == "TASK")
  	{
  		var st=document.getElementById("saveButton");
  		st.style.display  = "none";
  		var rt=document.getElementById("returnButton");
  		rt.style.display  = "none";
  		divSubmit.style.display  = "none";
  	}else{
  		fm.all('EdorValiDate').value = top.opener.fm.all('EdorValiDate').value;   
    	fm.all('EdorAppDate').value = top.opener.fm.all('EdorAppDate').value;
  	}
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;     
    showOneCodeName("EdorCode", "EdorTypeName"); 

  //税优验收临时修改
	var mySql = "select 1 from lcpol a,lmriskapp b where a.conttype='1' and a.contno='" + fm.ContNo.value + "'" 
				+" and a.riskcode=b.riskcode and b.taxoptimal='Y'";
    var result = easyExecSql(mySql);
    if(result != "null" && result != "" && result != null)
    {
    	fm.all('AppMoneyFee').value="0％";
    }
}
                                       

function initForm()
{
  try
  {
    initInpBox();
    initPremInfo();
    initAccInfo();
    initLoanPremGrid();
    initPayState();
    //queryLoanPremInfo();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function initLoanPremGrid()
{                               
	var iArray = new Array();
	try
	{
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="欠交保费期数";
	  iArray[1][1]="200px";
	  iArray[1][2]=100;
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array();
	  iArray[2][0]="金额";
	  iArray[2][1]="230px";
	  iArray[2][2]=100;
	  iArray[2][3]=0;  
	  
	  iArray[3]=new Array();
	  iArray[3][0]=feeName1;  
	  iArray[3][1]="370px";
	  iArray[3][2]=100;
	  iArray[3][3]=0;
	  
	  iArray[4]=new Array();
	  iArray[4][0]=feeName2;  
	  iArray[4][1]="370px";
	  iArray[4][2]=100;
	  iArray[4][3]=0;
	  
	  iArray[5]=new Array();
	  iArray[5][0]="交费日期";  
	  iArray[5][1]="0px";
	  iArray[5][2]=100;
	  iArray[5][3]=3;
	  
	  LoanPremGrid = new MulLineEnter( "fm" , "LoanPremGrid" ); 
	  //这些属性必须在loadMulLine前
	  LoanPremGrid.mulLineCount = 0;   
	  LoanPremGrid.displayTitle = 1;
	  LoanPremGrid.canSel=0;
	  LoanPremGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	  LoanPremGrid.hiddenSubtraction=1;
	  LoanPremGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}
</script>