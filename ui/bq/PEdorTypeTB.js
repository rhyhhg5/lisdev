var showInfo;

function queryCustomerName()
{
	var sql = "select AppntName from LCAppnt " +
						"where ContNo = '" + fm.ContNo.value + "'";
	var result = easyExecSql(sql); 					
	if (result != null)
	{
		fm.AppntName.value = result[0][0];
	}
	var sql = "select Name from LCInsured " +
						"where ContNo = '" + fm.ContNo.value + "'";
	var result = easyExecSql(sql); 
	var insuredName = "";						
	if (result != null)
	{
		for (i = 0; i < result.length; i++)
		{
			insuredName = insuredName + result[i][0];
			if (i < result.length - 1)
			{
				insuredName = insuredName + "，";
			}
		}
	}
	fm.InsuredName.value = insuredName;
}

function initTbInfo()
{
	var sql = "select SignName, Impart, Condition, RemarkFlag, Remark from LPTbInfo " +
	          "where EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value +  "' " +
	          "and ContNo = '" + fm.ContNo.value + "'";
	var result = easyExecSql(sql);
	if (result != null)
	{
		fm.SignName.value = result[0][0];
		fm.Impart.value = result[0][1];
		fm.Condition.value = result[0][2];
		var remarkFlag = result[0][3];
		fm.Remark.value = result[0][4];
		if (fm.SignName.value != "")
		{
			fm.SignNameFlag.checked = "true";
			showSignName();
		}
		if (fm.Impart.value != "")
		{
			fm.ImpartFlag.checked = "true";
			showImpart();
		}
		if (fm.Condition.value != "")
		{
			fm.ConditionFlag.checked = "true";
			showCondition();
		}
		if (remarkFlag == "1")
		{
			fm.RemarkFlag.checked = "true";
			showRemark();
		}
		else
		{
			sql = "select Remark from LCCont " +
			      "where ContNo = '" + fm.ContNo.value + "'";
			result = easyExecSql(sql);
			if (result != null)
			{ 
				fm.Remark.value = result[0][0];
			}
		}
	}
	else
	{
		sql = "select Remark from LCCont " +
		      "where ContNo = '" + fm.ContNo.value + "'";
		result = easyExecSql(sql);
		if (result != null)
		{ 
			fm.Remark.value = result[0][0];
		}
	}
}

//显示客户签名
function showSignName()
{
	if (fm.SignNameFlag.checked == true)
	{
		document.all("trSignName").style.display = "";
	}
  else
  {
  	document.all("trSignName").style.display = "none";
  }
}

//显示投保告知
function showImpart()
{
	if (fm.ImpartFlag.checked == true)
	{
		fm.ConditionFlag.checked = false;
		document.all("trCondition").style.display = "none";
		document.all("trImpart").style.display = "";
	}
  else
  {
  	document.all("trImpart").style.display = "none";
  }
}

//显示承保条件
function showCondition()
{
	if (fm.ConditionFlag.checked == true)
	{
		fm.ImpartFlag.checked = false;
		document.all("trImpart").style.display = "none";
		document.all("trCondition").style.display = "";
	}
  else
  {
  	document.all("trCondition").style.display = "none";
  }
}

//显示特别约定
function showRemark()
{
	if (fm.RemarkFlag.checked == true)
	{
		document.all("trRemark").style.display = "";
	}
  else
  {
  	document.all("trRemark").style.display = "none";
  }
}

function beforeSubmit()
{
	if (fm.SignNameFlag.checked == true)
	{
		if (fm.SignName.value == "")
		{
			alert("请录入客户签名！");
			fm.SignName.focus();
			return false;
		}
	}
	if (fm.ImpartFlag.checked == true)
	{
		if (fm.Impart.value == "")
		{
			alert("请录入投保告知！");
			fm.Impart.focus();
			return false;
		}
	}
	if (fm.ConditionFlag.checked == true)
	{
		if (fm.Condition.value == "")
		{
			alert("请录入承保条件！");
			fm.Condition.focus();
			return false;
		}
	}
	if (fm.RemarkFlag.checked == true)
	{
		var oldRemark = "";
		var sql = "select Remark from LCCont " +
							"where ContNo = '" + fm.ContNo.value + "'";
		var result = easyExecSql(sql);
		if (result != null)
		{ 
			oldRemark = result[0][0];
		}
		if (fm.Remark.value == oldRemark)
		{
			alert("特别约定未发生改变！");
			fm.Remark.focus();
			return false;
		}
	}
  if (!verifyInput2())
  {
    return false;
  }
	return true;
}

function save()
{
	if (!beforeSubmit())
	{
		return false;
	}
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorTypeTBSubmit.jsp";
	fm.submit();
}

function afterSubmit(flag, content)
{
  try
  {
    showInfo.close();
    window.focus();
  }
  catch (ex) {}

  if (flag == "Fail")
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    returnParent();
  }
}

function returnParent()
{	
  try
	{
    top.opener.focus();
	  top.opener.getEdorItem();
	  top.close();
	}
	catch (ex) {}
}