<%
//程序名称：GEdorAutoUWSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
  PGrpEdorAutoUWUI tPGrpEdorAutoUWUI   = new PGrpEdorAutoUWUI();
 
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
  System.out.println("-----"+tG.Operator);
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
    
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "INSERT||AUTOUWENDORSE";
  
String tEdorNo[] = request.getParameterValues("PGrpEdorMainGrid1");
String tGrpContNo[] = request.getParameterValues("PGrpEdorMainGrid2");
String tChk[] = request.getParameterValues("InpPGrpEdorMainGridChk");
int n = tChk.length;
for (int i = 0; i < n; i++)
{
  if ("1".equals(tChk[i]))
  {
	tLPGrpEdorMainSchema.setGrpContNo(tGrpContNo[i]);
	tLPGrpEdorMainSchema.setEdorNo(tEdorNo[i]);
	try
	{
		// 准备传输数据 VData
	  
		VData tVData = new VData();   
		//保存个人保单信息(保全)	
		 
		tVData.addElement(tLPGrpEdorMainSchema);
		tVData.addElement(tG);
		tPGrpEdorAutoUWUI.submitData(tVData,transact);
    }
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}			
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tPGrpEdorAutoUWUI.mErrors;
		if (!tError.needDealError())
		{                          
			Content = " 保存成功:自动核保完成";
			FlagStr = "Success";

			VData mResult = new VData();
			mResult=tPGrpEdorAutoUWUI.getResult();
			try
			{
				String tUWState=(String)((TransferData)mResult.getObjectByObjectName("TransferData",0)).getValueByName("UWFlag");
				if ("5".equals(tUWState))
				{
					Content = "没有通过自动核保,需要进行人工核保";
					FlagStr = "Succ";
				}
				else
				{
					Content = "通过自动核保。";
					FlagStr = "Succ";
				}
			}
			catch (Exception ex)
			{
			   Content = "获得自动核保结果失败！";
			}
		}
		else                                                                           
		{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
  }
}
  

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

