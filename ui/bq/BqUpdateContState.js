//程序名称：BqUpdateContState.js
	//程序功能：保单状态维护
	//创建日期：20170426
	//创建人  ：wxd

var showInfo;
var mDebug = "1";
var turnPage = new turnPageClass();

// 提交，保存按钮对应操作
function submitForm() {
	var i = 0;
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	initContStateGrid();
	fm.submit(); // 提交
}

// 提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showContInfo();
	}
}

// 显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}

// 查询按钮
function easyQueryClick() {
	// alert("into easyQueryClick!!!"); 
	// 初始化表格
	initContStateGrid();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select contno,insuredno,polno,riskcode,(select riskname from lmriskapp where riskcode=a.riskcode),"
	 		+"prem,cvalidate,paytodate,payenddate,stateflag "
	 		+"from lcpol a where conttype='1' and appflag='1' and stateflag='3' and contno='"+fm.ContNo.value+"' with ur";
	// 查询SQL，返回结果字符串
	turnPage.strQueryResult = easyQueryVer3(strSQL);
	// 判断是否查询成功
	if (!turnPage.strQueryResult) {
		//alert(turnPa);
		alert("没有查询到数据！");
		return "";
	}


	// 查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

	// 设置初始化过的MULTILINE对象，ContStateGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = ContStateGrid;
	// 保存SQL语句
	turnPage.strQuerySql = strSQL;

	// 设置查询起始位置
	turnPage.pageIndex = 0;

	// 在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet,
			turnPage.pageIndex, MAXSCREENLINES);
	 //alert(arrDataSet);
	// 调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	return true;
}

function updateContState() {
		//alert(fm.ContNo.value);
	//var mSelNo = ContStateGrid.getSelNo();// 判断该行的 Radio 单选框被选中,行号是从1开始,如果没有选中行,返回值是0

		var j=0;
		var i=0;
		var num=0;
		var checkFlag=0;
		for(j=0;j<ContStateGrid.mulLineCount;j++){//查出几条数据循环几次
			if(ContStateGrid.getChkNo(j)){
				num++;
			}
		}
	//	alert(mChkNo);
	if (num==0) {
		alert("请选择一条数据！");
		return false;}
	
	if (true) {
		var r=confirm("确认修改？")
		  if (r==true) {
			  fm.fmtransact.value = "UPDATE";
			  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
			  showInfo = window.showModelessDialog(urlStr, window,
			  "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			  
			  fm.submit();
		  } else {
			  return flase;
		  }
	}
}


function displayEasyResult(arrResult) {
	var i, j, m, n;

	if (arrResult == null)
		alert("没有找到相关的数据!");
	else {
		// 初始化表格
		initContStateGrid();

		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for (i = 0; i < n; i++) {
			m = arrResult[i].length;
			for (j = 0; j < m; j++) {
				ContStateGrid.setRowColData(i, j + 1, arrResult[i][j]);
			} // end of for
		} // end of for
		// alert("result:"+arrResult);
	} // end of if
}

function getStatus() {
	var i = 0;
	var showStr = "正在传送数据，请您稍候并且不要修改屏幕上的值或链接其他页面22222";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window,
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); // 提交
}

/*******************************************************************************
 * 显示frmSubmit框架，用来调试 参数 ： 无 返回值： 无
 * ********************************************************************
 */
function showSubmitFrame(cDebug) {
	if (cDebug == "1")
		parent.fraMain.rows = "0,0,*,0,*";
	else
		parent.fraMain.rows = "0,0,0,72,*";
}