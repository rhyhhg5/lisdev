<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：downLoad.jsp
	//程序功能：用户手册,模板下载界面
	//创建日期：2007-11-23
	//创建人  ：shaoax
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
	<%
		GlobalInput tGI = new GlobalInput();
		tGI = (GlobalInput) session.getValue("GI");
	%>
	<script>
		var operator = "<%=tGI.Operator%>";   //记录操作员
		var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	
	</script>

	<head>
		<meta http-equiv="Content-Type" content="text/html charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<SCRIPT src = "./UpdateBlobInput.js"></SCRIPT> 
<%@include file="./UpdateBlobInit.jsp"%>
</head>
<body onload="initForm();initElementtype();"  >
  <form action="" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data"> 
   <table>
   	  <tr>
        <td class=common><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" 
        	OnClick= "showPage(this,divLLReport1);"></td>
    	<td class= titleImg>基本信息</td></tr>
    </table>
  
  <Div >
	<br>
   	<Table class= common>
   		<TR class= common>
 			<TD class= title>
          	工单号
        </TD>
        <TD class= input>
        	<Input class= common name="Edorno" elementtype=nacessary verify="工单号|NOTNULL" > 
        </TD>
        <TD class= title>
          	保单类型
        </TD>
        
        	<TD class= input >
   				<input class="codeno" name="ContType" 
	            ondblclick="return showCodeList('conttype',[this,ContTypeName],[0,1],null,null,null,1);"
	            onkeyup="return showCodeListKey('conttype', [this,ContTypeName],[0,1],null,null,null,1);" verify="保单类型|NOTNULL" ><Input 
          		class= codename name= 'ContTypeName' elementtype=nacessary  > 
   			</TD>
        
      </TR> 
      <TR>
      		<TD class= title>
   				文件类型
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "FileType" CodeData="0|^1|批单^2|清单" 
          ondblClick="showCodeListEx('FileType',[this,FileTypeName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('FileType',[this,FileTypeName],[0,1],null,null,null,1);" verify="文件类型|NOTNULL" ><Input 
          class= codename name= 'FileTypeName' elementtype=nacessary >
           			
      </TR>  
            		      		<TD class= title id="updateTd1" style="display:none;">
   				选择文件
        </TD>		
					<td class= input id="updateTd2" style="display:none;">
						<input class=common type="file" name="fileName" size="100">
					</td>

					<td id="updateTd3" style="display:none;">
						<input value="提  交" class=cssButton type=button name = submitbutton onclick="submitData();">
					</td>
          
     </Table>
  </div>
   	<br>

  <Div >
    <input class=cssButton id="updateButton1" VALUE="下载" TYPE=button onClick="downloadForm();">
  	<input class=cssButton id="updateButton2" VALUE="上传" TYPE=button onClick="updateForm();">
  	
  </Div> 
 </form>  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
  
</body>
</html>