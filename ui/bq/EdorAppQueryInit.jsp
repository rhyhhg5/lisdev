<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%> 

<script language="JavaScript">

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

function initForm()
{
  try
  {
    initInpBox();
    initCodeGrid();
  }
  catch(re)
  {
    alert("EdorQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initCodeGrid()    							//函数名为init+MulLine的对象名ObjGrid
{        
	var iArray = new Array(); 						//数组放置各个列
	try
	{
    	iArray[0]=new Array();
    	iArray[0][0]="序号";         			//列名
    	iArray[0][1]="30px";         			//列宽
    	iArray[0][2]=100;         				//列最大值
    	iArray[0][3]=3;         					//1表示允许该列输入，0表示只读且不响应Tab键
    																		//2表示为容许输入且颜色加深
    																		
        iArray[1]=new Array();
	    iArray[1][0]="批单号";    			//列名（序号列，第1列）
	    iArray[1][1]="120px";  						//列宽
	    iArray[1][2]=100;      						//列最大值
	    iArray[1][3]=0;   								//1表示允许该列输入，0表示只读且不响应Tab键
	                   										//2表示为容许输入且颜色加深.

	    iArray[2]=new Array();
	    iArray[2][0]="保全业务类型";    			//列名（序号列，第1列）
	    iArray[2][1]="100px";  						//列宽
	    iArray[2][2]=100;      						//列最大值
	    iArray[2][3]=0;   								//1表示允许该列输入，0表示只读且不响应Tab键
	                   										//2表示为容许输入且颜色加深.
	                                    	
	    iArray[3]=new Array();          	
	    iArray[3][0]="保单号";  					//列名（第2列）
	    iArray[3][1]="110px";  						//列宽
	    iArray[3][2]=100;        					//列最大值
	    iArray[3][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	       																//后续可以添加N列，如上设置
	    iArray[4]=new Array();          	
	    iArray[4][0]="投保人";  				//列名（第2列）
	    iArray[4][1]="80px";  						//列宽
	    iArray[4][2]=100;        					//列最大值
	    iArray[4][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	       																//后续可以添加N列，如上设置
	
	    iArray[5]=new Array();          	
	    iArray[5][0]="投保人客户号";  				//列名（第2列）
	    iArray[5][1]="80px";  						//列宽
	    iArray[5][2]=100;        					//列最大值
	    iArray[5][3]=0;       						//是否允许输入,1表示允许，0表示不允许	   

	    iArray[6]=new Array();          	
	    iArray[6][0]="保单印刷号";  				//列名（第2列）
	    iArray[6][1]="100px";  						//列宽
	    iArray[6][2]=100;        					//列最大值
	    iArray[6][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	       																//后续可以添加N列，如上设置
		iArray[7]=new Array();          	
	    iArray[7][0]="业务类型";  				//列名（第2列）
	    iArray[7][1]="80px";  						//列宽
	    iArray[7][2]=100;        					//列最大值
	    iArray[7][3]=0;       						//是否允许输入,1表示允许，0表示不允许

		iArray[8]=new Array();          	
	    iArray[8][0]="承保时间";  				//列名（第2列）
	    iArray[8][1]="80px";  						//列宽
	    iArray[8][2]=100;        					//列最大值
	    iArray[8][3]=0;       						//是否允许输入,1表示允许，0表示不允许

		iArray[9]=new Array();          	
	    iArray[9][0]="保全受理日期";  				//列名（第2列）
	    iArray[9][1]="80px";  						//列宽
	    iArray[9][2]=100;        					//列最大值
	    iArray[9][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[10]=new Array();          	
	    iArray[10][0]="保全生效日期";  				//列名（第2列）
	    iArray[10][1]="80px";  						//列宽
	    iArray[10][2]=100;        					//列最大值
	    iArray[10][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[11]=new Array();          	
	    iArray[11][0]="保全结案日期";  				//列名（第2列）
	    iArray[11][1]="80px";  						//列宽
	    iArray[11][2]=100;        					//列最大值
	    iArray[11][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[12]=new Array();          	
	    iArray[12][0]="保全金额";  				//列名（第2列）
	    iArray[12][1]="80px";  						//列宽
	    iArray[12][2]=100;        					//列最大值
	    iArray[12][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[13]=new Array();          	
	    iArray[13][0]="收付费标识";  				//列名（第2列）
	    iArray[13][1]="70px";  						//列宽
	    iArray[13][2]=100;        					//列最大值
	    iArray[13][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[14]=new Array();          	
	    iArray[14][0]="批单状态";  				//列名（第2列）
	    iArray[14][1]="70px";  						//列宽
	    iArray[14][2]=100;        					//列最大值
	    iArray[14][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[15]=new Array();          	
	    iArray[15][0]="分公司代码";  				//列名（第2列）
	    iArray[15][1]="80px";  						//列宽
	    iArray[15][2]=100;        					//列最大值
	    iArray[15][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[16]=new Array();          	
	    iArray[16][0]="分公司名称";  				//列名（第2列）
	    iArray[16][1]="120px";  						//列宽
	    iArray[16][2]=100;        					//列最大值
	    iArray[16][3]=0;       						//是否允许输入,1表示允许，0表示不允许
	    
	    //生成对象区，规则：对象名=new MulLineEnter(“表单名”,”对象名”); 
	    CodeGrid= new MulLineEnter( "fm" , "CodeGrid" ); 
	    //设置属性区 (需要其它特性，在此设置其它属性)
	    CodeGrid.mulLineCount = 0 ; 		//行属性：设置行数=10    
	    CodeGrid.displayTitle = 1;   		//标题属性：1显示标题(缺省值), 0隐藏标题        
	    //对象初始化区：调用对象初始化方法，属性必须在此前设置
	    CodeGrid.hiddenSubtraction=1;
	    CodeGrid.hiddenPlus=1;
	    //CodeGrid.canChk=1;
	    CodeGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{ alert(ex); }
}

function initInpBox()
{ 
  try
  {
	fm.all('ManageCom').value = manageCom;
	var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
    fm.all('ManageComName').value = easyExecSql(sql1);
	var sql1 = "select Current Date - 3 month from dual ";
	fm.all('AppStartDate').value = easyExecSql(sql1);
	fm.all('AppEndDate').value=tCurrentDate;
	fm.all('ContType').value="0";
	fm.all('ContType1').value="0";
	showAllCodeName();
  }
  catch(ex)
  {
    alert("BankDataQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}    
    
</script>