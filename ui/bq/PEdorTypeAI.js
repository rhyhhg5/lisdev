//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeAIReturn() {
  initForm();
}
//校验函数
function CheckImpartContent()
{ 
  var i;
  //alert("mulLineCount"+ImpartGrid.mulLineCount);
  var ImpartContentArray;
  for(i=1; i <= ImpartGrid.mulLineCount ; i++)
  {
      ImpartContentArray = ImpartGrid.getRowColData(i-1,4);
      if(ImpartContentArray == null || ImpartContentArray == "")
      {
      	  //alert("ImpartContentArray : "+ImpartContentArray);
      	  alert("请填写第" + i + "行告知内容！");
          return false;
      }
  }	

  return true;
}

function edorTypeAISave() {
	
  if(!CheckImpartContent())
  {
        return ;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}

function returnParent()
{
  top.close();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
//  	alert(tTransact);
	if (tTransact=="QUERY||MAIN")
	{
	    var iArray;
	    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  	  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  	  //如果没有数据，则返回
  	  if (Result=="0|0^") return;
	    //保存查询结果字符串
 	    turnPage.strQueryResult  = Result;
  	    //使用模拟数据源，必须写在拆分之前
  	    turnPage.useSimulation   = 1;  
            //查询成功则拆分字符串，返回二维数组
  	    var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
	    turnPage.arrDataCacheSet =chooseArray(tArr,[3,2,,7,6]);
	    //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	    turnPage.pageDisplayGrid = ImpartGrid;    
	    //设置查询起始位置
	    turnPage.pageIndex = 0;
	    //在查询结果数组中取出符合页面显示大小设置的数组
	    var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	    //调用MULTILINE对象显示查询结果
	    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	   
	}

        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");    
        
  }

}