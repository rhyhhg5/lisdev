<%
//程序名称：AscriptionRuleInput.jsp
//程序功能：
//创建日期：2004-04-10
//创建人  ：mqhu
//更新记录：  更新人    更新日期    更新原因/内容 
%>


<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
String GrpContNo = request.getParameter("GrpContNo");
String LoadFlag = request.getParameter("LoadFlag");

%>
<head>
<script>
GrpContNo="<%=GrpContNo%>";
var LoadFlag ="<%=request.getParameter("LoadFlag")%>";
var ScanFlag = "<%=request.getParameter("ScanFlag")%>";
if (ScanFlag == "null") ScanFlag = "0";
var scantype = "<%=request.getParameter("scantype")%>";
var oldContNo ="<%=request.getParameter("oldContNo")%>";
var EdorNo ="<%=request.getParameter("EdorNo")%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT> 
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="GEdorTypeGB.js"></SCRIPT>
<SCRIPT src="./PEdor.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%if(request.getParameter("scantype")!=null&&request.getParameter("scantype").equals("scan")){%>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<%}%> 
<%@include file="GEdorTypeGBInit.jsp"%>
<title>团体险种归属规则定制 </title>
</head>
<body onload="initForm();initElementtype();">
	<form method=post name=fm target="fraSubmit" action="AscriptionRuleSave.jsp">	
	 <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 集体合同号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpContNo1>
      </TD>   
    </TR>
  </TABLE> 		
		<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>合同信息</td>
			</tr>
		</table>
		<table  class= common align=center>  
			<TR  class= common>
				<TD  class= title>团体合同号</TD>
				<TD  class= input>
					<Input class= readonly readonly name=GrpContNo>
					<Input type=hidden name=ProposalGrpContNo>
					<input type=hidden name=mOperate>
					<input type=hidden id="ContType" name="ContType">
				</TD>
				<TD  class= title>管理机构</TD>
				<TD  class= input>
					<Input class=readonly readonly name=ManageCom>
				</TD>
			</TR>
			<TR  class= common>
				<TD  class= title>保单位客户号</TD>
				<TD  class= input>
					<Input class= readonly readonly name=AppntNo>
				</TD>
				<TD  class= title>保单位名称</TD>
				<TD  class= input>
					<Input class= readonly readonly name=GrpName>
				</TD>
			</TR>
			</table>
			<!--input type=button class="cssButton" value="kick me" onclick="GrpPerPolDefineOld();">
		<input name="ff"-->
 
		<Div  id= "divAscriptionRuleOld" style= "display: ''">	
		<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 人员级别定义
    		</td>
    	</tr>
    	    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanAscriptionRuleGradeGrid" >
					</span> 
				</td>
			</tr>
		</table>
	<Div  id= "divAscriptionRuleSave" style= "display: ''" align= left> 
		<input class=cssButton type=button value="保  存" onclick="this.disabled=true; setEnable('submitButton');Submit();" id="submitButton" > 	
	</Div>
	 	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 已添加的归属规则
    		</td>
    	</tr>
    	    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanAscriptionRuleOldGrid" >
					</span> 
				</td>
			</tr>
		</table>
		<div id="divAscriptionRuleGrid" style="display:'none'">
				<table>
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class= titleImg>归属规则详细信息</td>
			</tr>
		</table>
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanAscriptionRuleGrid" ></span>
				</td>
			</tr>
		</table>
		</div>
       </table>
       
       </Div>
       		<!--input type=button class="cssButton" value="kick me" onclick="GrpPerPolDefine();">
		<input name="ff"-->
		
		<Div  id= "divAscriptionRule" style= "display: ''">
	 	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
    			 定制归属规则
    		</td>
    	</tr>
       </table>
       			<TR  class= common>
				<TD  class= title>归属规则代码</TD>
				<TD  class= input>
					<Input class=common name=AscriptionRuleCode maxlength=2 elementtype=nacessary verify="员工类别|notnull">
				</TD>
				<TD  class= title>归属规则名称</TD>
				<TD  class= input>
					<Input class=common name=AscriptionRuleName>
				</TD>
			</TR>
    	<table  class= common>
    	    <tr>
    	    	<td><font color=red>填写说明：员工服务年数录入值范围为0-99（单位：年）；归属规则比例录入值范围为0-1。</font>
    	    	</td>
    	    </tr>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanAscriptionRuleNewGrid" >
					</span> 
				</td>
			</tr>
		</table>
	</div>
		<INPUT VALUE="返回" class =cssButton  TYPE=button onclick="edorTypeGBReturn()">
		<INPUT VALUE="重置" class =cssButton style="display: none" TYPE=button onclick="reload();" >	
		<INPUT VALUE="归属规则修改"  align= right class =cssButton  TYPE=button onclick="updateClick();" name="updateButton">
	<Div  id= "divRiskPlanSave" style= "display: ''" align= left> 
		<INPUT VALUE="归属规则保存" class =cssButton style="display: none" TYPE=button onclick="submitForm();" name="sumbitButton">
		<INPUT VALUE="归属规则修改" class =cssButton  style="display: none" TYPE=button onclick="updateClick();" name="updateButton">
		<INPUT VALUE="归属规则删除" class =cssButton  style="display: none" TYPE=button onclick="DelContClick();" name="DeleteButton">
	</Div>
		<div id="autoMoveButton" style="display: none">
	<input type="button" name="autoMoveInput" value="随动定制确定" onclick="submitAutoMove('888');" class=cssButton>
	<%--<input type="button" name="Next" value="下一步" onclick="location.href='ContInsuredInput.jsp?LoadFlag='+tLoadFlag+'&prtNo='+prtNo+'&checktype=2&scantype='+scantype" class=cssButton>	
        --%>
        <INPUT VALUE="返回定制投保人界面" class=cssButton TYPE=button onclick="returnparent();">     
        <input type=hidden id="" name="autoMoveFlag">
        <input type=hidden id="" name="autoMoveValue">   
        <input type=hidden id="" name="pagename" value="888">                         
      </div>  
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>