<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeAD.js"></SCRIPT>
  <script src="../certify/CertifyFun.js"></script>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeADInit.jsp"%>
  <title>投保人联系方式变更 </title> 
</head>
<body  onload="initForm()" >
  <form action="./PEdorTypeADSubmit.jsp" method=post name=fm target="fraSubmit">    
   <table class=common>
   		<tr class= common> 
	      <td class="title"> 受理号 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=EdorNo >
	      </td>
	      <td class="title"> 批改类型 </td>
	      <td class="input">
	      	<input class="readonly" type="hidden" readonly name=EdorType>
	      	<input class="readonly" readonly name=EdorTypeName>
	      </td>
	      <td class="title"> 保单号 </td>
	      <td class="input">
	      	<input class = "readonly" readonly name=ContNo>
	      </td>   
    	</tr>
  	</table> 
  
 	<Div  id= "divLPAppntIndDetail" style= "display: ''">
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
       客户地址信息
      </td>
   </tr>
   </table>
  </Div>
   	<Div  id= "divDetail" style= "display: 'none'">
    <table  class= common>
      <TR  class= common>
        <TD  class= title>
          客户号
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name=CustomerNo >
        </TD>
        <TD  class= title>
          客户姓名
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name=Name >
        </TD>
        <TD  class= title>
          性别
        </TD>
        <TD  class= input>
           <Input class= "readonly" name=Sex readonly >
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          出生日期
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name=Birthday >
        </TD>
        <TD  class= title>
          证件类型
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name="IDType"  >
        </TD>
        <TD  class= title>
          证件号码
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name="IDNo" >
        </TD>          
      </TR>
    </table>
    </Div>
    <Div  id= "divDetail1" style= "display: ''">
      <table  class= common>
      <TR  class= common>
        <TD  class= title>
          家庭地址
        </TD>
        <TD  class= input colspan=3 >
          <Input class= "readonly" readonly name="HomeAddress">
        </TD>
        <TD  class= title>
          家庭邮政编码
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name="HomeZipCode">
        </TD>
      </TR>
      <TR  class= common>
        <TD  class= title>
          家庭电话
        </TD>
        <TD  class= input>
        <input class= "readonly" readonly name="HomePhone">
        </TD>
        <TD  class= title>
          家庭传真
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name="HomeFax">
        </TD>
      </TR>        
      <TR  class= common>
        <TD  class= title>
          单位地址
        </TD>
        <TD  class= input colspan=3 >
          <Input class= "readonly" readonly name="CompanyAddress" >
        </TD> 
        <TD  class= title>
          单位邮政编码
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name="CompanyZipCode" >
        </TD>                   
      </TR>        
      <TR  class= common>
        <TD  class= title>
          单位电话
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name="CompanyPhone">
        </TD>       
        <TD  class= title>
          单位传真
        </TD>
        <TD  class= input>
          <Input class= "readonly" readonly name="CompanyFax">
        </TD>                    
      </TR>
      </table>
    </DIV>
    <Div  id= "divDetail2" style= "display: ''">
      <table  class= common>                
        <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= "readonly3" readonly name="PostalAddress" >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name="ZipCode">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
          <input class= "readonly" readonly name="Phone" >
          </TD>
          <TD  class= title>
            通讯传真
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name="Fax">
          </TD>           
          <TD  class= title>
            移动电话
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name="Mobile">
          </TD>         
        </TR>
        <TR class= common>
        	<TD class= title>
        		E-MAIL	
        	</TD>
        		<TD class= input>
        		<Input class= "readonly" readonly name="E-MAIL">	
        	</TD>
        </TR>   
      </table>
    </DIV>
      
	<Div  id= "divLPAppntIndDetail" style= "display: ''">
  		<table>
   			<tr>
     			<td>
        			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      			</td>
      			<td class= titleImg>
       			 更新后客户地址信息
      			</td>
   			</tr>
   		</table>
  	<Div  id= "divDetail" style= "display: ''">
      	<table  class= common>
      		<%--<TR style= "display: 'none'">
          		<TD class= title8 >
            	家庭地址
          		</TD>
          		<TD  class= input colspan="8">
					<Input class="codeno" name=Province verify="家庭地址|notnull" ondblclick="return showCodeList('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,appnt_PostalProvince],[0,1],null,'0','Code1',1);" onblur="return chagePostalAddress();"><input class=codename style="width:90px" name=appnt_PostalProvince readonly=true ><font color=red>&nbsp;*</font>省（自治区直辖市）
					<Input class="codeno" name=City verify="家庭地址|notnull" ondblclick="return showCodeList('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,appnt_PostalCity],[0,1],null,fm.Province.value,'Code1',1);" onblur="return chagePostalAddress();"><input class=codename style="width:90px" name=appnt_PostalCity readonly=true ><font color=red>&nbsp;*</font>市
					<Input class="codeno" name=County verify="家庭地址|notnull" ondblclick="return showCodeList('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,appnt_PostalCounty],[0,1],null,fm.City.value,'Code1',1);" onblur="return chagePostalAddress();"><input class=codename style="width:90px" name=appnt_PostalCounty readonly=true ><font color=red>&nbsp;*</font>县（区）
					<input  class= common10 style="width:120px"  name=appnt_PostalStreet verify="家庭地址|notnull" onblur="return chagePostalAddress();"><font color=red>&nbsp;*</font> 乡镇（街道）
			    	<input  class= common10 style="width:120px"  name=appnt_PostalCommunity verify="家庭地址|notnull" onblur="return chagePostalAddress();"><font color=red>&nbsp;*</font> 村（社区）
	      		</TD>	    
        	</TR>
        	--%><TR  class= common style="display:''">
          		<TD  class= title >
            	家庭地址
          		</TD>
          		<TD  class= input colspan=3 >
            		<Input class= common3 name="PHomeAddress" verify="投保人家庭地址|len<=100" >
          		</TD>
        	</TR>
        
        	<TR  class= common>
          		<TD  class= title>
            	家庭邮政编码
          		</TD>
          		<TD  class= input>
            		<Input class= common name="PHomeZipCode" verify="投保人家庭邮政编码|zipcode" >
          		</TD>
          		<TD  class= title>
           	 家庭电话
          		</TD>
          		<TD  class= input>
          			<input class= common name="PHomePhone" verify="投保人家庭电话|len<=18" >
          		</TD>
          		<TD  class= title>
            	家庭传真
          		</TD>
          		<TD  class= input>
            		<Input class= common name="PHomeFax" verify="投保人家庭传真|len<=15" >
          		</TD>
        	</TR>
        	<%--<TR style= "display: 'none'">
          		<TD class= title8>
            	单位地址
          		</TD>
          		<TD  class= input colspan="8">
					<Input class="codeno" name=Province2 verify="单位地址|notnull" ondblclick="return showCodeList('Province1',[this,appnt_PostalProvince2],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province1',[this,appnt_PostalProvince2],[0,1],null,'0','Code1',1);" onblur="return chagePostalAddress();"><input class=codename style="width:90px" name=appnt_PostalProvince2 readonly=true ><font color=red>&nbsp;*</font>省（自治区直辖市）
					<Input class="codeno" name=City2 verify="单位地址|notnull" ondblclick="return showCodeList('City1',[this,appnt_PostalCity2],[0,1],null,fm.Province2.value,'Code1',1);" onkeyup="return showCodeListKey('City1',[this,appnt_PostalCity2],[0,1],null,fm.Province2.value,'Code1',1);" onblur="return chagePostalAddress();"><input class=codename style="width:90px" name=appnt_PostalCity2 readonly=true ><font color=red>&nbsp;*</font>市
					<Input class="codeno" name=County2 verify="单位地址|notnull" ondblclick="return showCodeList('County1',[this,appnt_PostalCounty2],[0,1],null,fm.City2.value,'Code1',1);" onkeyup="return showCodeListKey('County1',[this,appnt_PostalCounty2],[0,1],null,fm.City2.value,'Code1',1);" onblur="return chagePostalAddress();"><input class=codename style="width:90px" name=appnt_PostalCounty2 readonly=true ><font color=red>&nbsp;*</font>县（区）
					<input  class= common10 style="width:120px"  name=appnt_PostalStreet2 verify="单位地址|notnull" onblur="return chagePostalAddress();"><font color=red>&nbsp;*</font> 乡镇（街道）
			    	<input  class= common10 style="width:120px"  name=appnt_PostalCommunity2 verify="单位地址|notnull" onblur="return chagePostalAddress();"><font color=red>&nbsp;*</font> 村（社区）
	      		</TD>	    
        	</TR>        
        	--%><TR  class= common style="display:''">
          		<TD  class= title >
            	单位地址
          		</TD>
          		<TD  class= input colspan=3 >
            		<Input class= common3 name="PCompanyAddress" verify="投保人单位地址|len<=100" >
          		</TD> 
        	</TR>        
        	<TR  class= common>
          		<TD  class= title>
            	单位邮政编码
          		</TD>
          		<TD  class= input>
            		<Input class= common name="PCompanyZipCode" verify="投保人单位邮政编码|zipcode" >
          		</TD>                   
          		<TD  class= title>
            		单位电话
          		</TD>
          		<TD  class= input>
            		<Input class= common name="PCompanyPhone" verify="投保人单位电话|len<=18" >
          		</TD>       
          		<TD  class= title>
            	单位传真
          		</TD>
          		<TD  class= input>
            		<Input class= common name="PCompanyFax" verify="单位传真|len<=18" >
          		</TD>                    
        	</TR>
        	<TR  class= common>        
          		<TD  class= title>
            		联系地址选择
          		</TD>
          		<TD  class= input>
            		<Input class="codeNo" name="PCheckPostalAddress" CodeData="0|^1|单位地址^2|家庭地址^3|其它"  ondblclick="return showCodeListEx('CheckPostalAddress',[this,PCheckPostalAddressName], [0,1]);" onkeyup="return showCodeListKeyEx('CheckPostalAddress',[this]);FillPostalAddress()" onfocus="FillPostalAddress();"><Input class="codeName" name="PCheckPostalAddressName" readonly >                      
          		</TD>   
          		<TD  class= title colspan=2>
            		<font color=red>(在填写单位地址或家庭地址后实现速填)</font>
          		</TD> 
        	</TR>
    	</table>
	</DIV>
    <Div  id= "divDetail4" style= "display: ''">
	    <table  class= common>
	    	<TR id="otherAddress" style="display:none">
          		<TD class= title8>
            	联系地址
          		</TD>
          		<TD  class= input colspan="8">
					<Input class="codeno" id=Province3 name=Province3  ondblclick="showCodeList('Province1',[this,appnt_PostalProvince3],[0,1],null,'0','Code1',1),changeAddress(1)" onkeyup="return showCodeListKey('Province1',[this,appnt_PostalProvince3],[0,1],null,'0','Code1',1),changeAddress(1)" onblur="return chagePostalAddress();" readonly=true><input class=codename style="width:90px" name=appnt_PostalProvince3 readonly=true ><font color=red>&nbsp;*</font>省（自治区直辖市）
					<Input class="codeno" id=City3 name=City3  ondblclick="return showCodeList('City1',[this,appnt_PostalCity3],[0,1],null,fm.Province3.value,'Code1',1),changeAddress(2)" onkeyup="return showCodeListKey('City1',[this,appnt_PostalCity3],[0,1],null,fm.Province3.value,'Code1',1),changeAddress(2)" onblur="return chagePostalAddress();" readonly=true><input class=codename style="width:90px" name=appnt_PostalCity3 readonly=true ><font color=red>&nbsp;*</font>市
					<Input class="codeno" id=County3 name=County3 ondblclick="return showCodeList('County1',[this,appnt_PostalCounty3],[0,1],null,fm.City3.value,'Code1',1),changeAddress(3)" onkeyup="return showCodeListKey('County1',[this,appnt_PostalCounty3],[0,1],null,fm.City3.value,'Code1',1),changeAddress(3);" onblur="return chagePostalAddress();" readonly=true><input class=codename style="width:90px" name=appnt_PostalCounty3 readonly=true ><font color=red>&nbsp;*</font>县（区）
					<input  class= common10 style="width:120px" id=appnt_PostalStreet3 name=appnt_PostalStreet3 onchange="changeAddress(4)" onblur="return chagePostalAddress()"><font color=red>&nbsp;*</font> 乡镇（街道）
			    	<input  class= common10 style="width:120px" id=appnt_PostalCommunity3 name=appnt_PostalCommunity3  onblur="return chagePostalAddress();"><font color=red>&nbsp;*</font> 村（社区）
	      		</TD>	    
        	</TR>
        	<TR  class= common id=P_OtherAddress style="display:'none'">
		      <TD  class= title>
		        	详细联系地址
		      </TD>
		      <TD  class= input colspan=3 >
		        <Input class= common3 id=POtherAddress name="POtherAddress" elementtype=nacessary  readonly>
		       	<font color=red>&nbsp;*</font>
		      </TD>
		    </TR>                          
		    <TR id = "postal_Address" >
		      <TD  class= title>
		        联系地址
		      </TD>
		      <TD  class= input colspan=3 >
		        <Input class= common3 name="PPostalAddress" elementtype=nacessary verify="投保人联系地址|notnull&len<=100" readonly>
		       	<font color=red>&nbsp;*</font>
		      </TD>
		    </TR>
		    
		    <TR  class= common>
		      <TD  class= title>
		        邮政编码
		      </TD>
		      <TD  class= input>
		        <Input class= common name="PZipCode" verify="投保人邮政编码|zipcode" >
		      </TD>
		      <TD  class= title>
		        联系电话
		      </TD>
		      <TD  class= input>
		      <input class= common name="PPhone" verify="投保人联系电话|len<=18" >
		      </TD>
		      <TD  class= title>
		        通讯传真
		      </TD>
		      <TD  class= input>
		        <Input class= common name="PFax" verify="传真|len<=15" >
		      </TD>           
		    </TR>
		     <TR class= common>
		      <TD  class= title>
		        移动电话
		      </TD>
		      <TD  class= input>
		        <Input class= common name="PMobile" verify="投保人移动电话|len<=15" >
		      </TD>         
		    	<TD class= title>
		    		E-MAIL	
		    	</TD>
		    	<TD class= input>
		    		<Input class= "common" name="PE-MAIL" verify="E-MAIL|EMAIL">	
		    	</TD>
		    <TR>   
	  </table> 
  </DIV>
	<table>
		<tr>
			<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);">
			</td>
			<td class= titleImg>
				相关合同信息
			</td>
		</tr>
	</table> 
	<div  id= "divLCCont" style= "display: ''">
	<table  class= common>
		<tr  class= common>
			<td text-align: left colSpan=1>
				<span id="spanLCContGrid" >
				</span>
			</td>
		</tr>
	</table>
	</div>
	</DIV> 
	<br>
<div>
	<Input name=save class= cssButton type=Button value="保  存" onclick="edorTypeADSave()">
	<Input name=cancel class= cssButton type=Button value="取  消" onclick="edorTypeADReturn()">
	<Input name=goBack type=Button class= cssButton value="返  回" onclick="returnParent()">
</div>
	 <input type=hidden id="AddressNo" name="AddressNo">
	 <input type=hidden id="PAddressNo" name="PAddressNo">	      	
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 
	 <input type=hidden name="Name2">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
