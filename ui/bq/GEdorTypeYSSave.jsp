<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%    
    String flag = "";
    String content = "";
    GlobalInput gi = (GlobalInput)session.getValue("GI");    
    String customerNo = request.getParameter("CustomerNo");
    String grpContNo = request.getParameter("GrpContNo");
    String accFee = request.getParameter("AccFee");
    String edorNo = request.getParameter("EdorNo");
    String edorType = request.getParameter("EdorType");
    
    LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
		tLPGrpEdorItemSchema.setEdorNo(edorNo);
		tLPGrpEdorItemSchema.setEdorType(edorType);
		tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
    
	  LCAppAccTraceSchema tLCAppAccTraceSchema = new LCAppAccTraceSchema();
	  tLCAppAccTraceSchema.setCustomerNo(customerNo);
	  tLCAppAccTraceSchema.setAccType("1");
	  tLCAppAccTraceSchema.setOtherType("3");    //团险保全
	  tLCAppAccTraceSchema.setOtherNo(edorNo);
	  tLCAppAccTraceSchema.setMoney(accFee);
	  tLCAppAccTraceSchema.setBakNo(grpContNo);
	  tLCAppAccTraceSchema.setOperator(gi.Operator);
	  
    //生成打印数据
    VData tVData = new VData();
    tVData.add(tLPGrpEdorItemSchema);
    tVData.add(tLCAppAccTraceSchema);
		tVData.add(gi);
		GEdorYSDetailUI tGEdorYSDetailUI = new GEdorYSDetailUI();
		if (!tGEdorYSDetailUI.submitData(tVData, ""))
		{
			flag="Fail";
			content = "数据保存失败！原因是" + tGEdorYSDetailUI.getError();
		}
		else
		{
			flag = "Succ";
			content = "数据保存成功。";
		}
    content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">	
		parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>