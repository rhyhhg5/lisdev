//程序名称：BqBalanceAdvanceInput.js
//程序功能：实现保全结算提前结算
//创建日期：2006-03-10 11:10:36
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

//根据保单号进行提前结算查询
function easyQueryClick()
{
	if(fm.all('grpcontno').value==""&&fm.all('EndDate').value=="")
	{
		alert("保单号或结算日期必须选择一个！");
	  return;
  }
  var strSql = "select a.grpcontno,b.managecom, a.balmoney,case when a.state = '0' then '可以结算' when a.state = '1' then '正在结算' else '已结算待收费' end ,BalStartDate,BalEndDate,BalToDate,NextDate,BalIntv,Flag,BalTimes "
             +"from LCGrpBalPlan a, lcgrpcont b where 1=1 "
             + "  and a.grpcontno = b.grpcontno "
             + "  and balintv != 0 "
             + "  and (b.StateFlag is null or b.StateFlag = '1') "
             + "  and b.managecom like '"
             +comCode+"%%'"
             +getWherePart('a.grpcontno','grpcontno')
             +getWherePart('NextDate','EndDate','<=');   
  turnPage.queryModal(strSql,BalancePolGrid);
  calBalmoney();
}


//提前结算
function balanceAdvance()
{
	var i = 0;
  var checkFlag = BalancePolGrid.getSelNo();
  if (checkFlag) 
  { 
  	fm.all('grpcontno').value =  BalancePolGrid.getRowColData(checkFlag - 1, 1);
  	var state = BalancePolGrid.getRowColData(checkFlag - 1, 4);
  	if(state!="可以结算")
  	{
  		alert("该集体合同目前状态为待结算或结算待操作,不能重复操作!");
  		return;
  	}
  	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.submit();	
  }
  else{
    alert("请先选择一条保单信息！"); 
  }
		
}

//日处理
function balanceDailyBatch()
{
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.action = "../bq/BqBalanceBatchSave.jsp"
  fm.submit();
}

/* 保存完成后的操作，由子窗口调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	//iniForm();
	iniBalancePolGrid();
} 
function calBalmoney(){
for( i = 1; i <= BalancePolGrid.mulLineCount;i++)
  { 
  	if(BalancePolGrid.getRowColData(i - 1, 4)=="可以结算"||BalancePolGrid.getRowColData(i - 1, 4)=="正在结算")
  	{
  		strSql = "select  nvl(sum(money),0) from ( select b.EdorAppDate,b.edorno,b.EdorType,b.getMoney money from  ljapay a ,lpgrpedoritem b "+
  							 "where a.incometype='B' and a.IncomeNo=b.edorno and b.grpcontno='"+BalancePolGrid.getRowColData(i - 1, 1)+"'"+
								 " union "+
								 "select b.EdorAppDate,b.edorno,b.EdorType,b.getMoney money from  ljaget a ,lpgrpedoritem b "+
								 "where a.OtherNo=b.edorno and a.PayMode='B' and b.grpcontno = '"+BalancePolGrid.getRowColData(i - 1, 1)+"' ) as y";
  	var turnPage3 = new turnPageClass();
    var arrayResult= easyExecSql(strSql, null, null, null, null, null, turnPage3);    
     
    //var arrayResult = easyExecSql(strSql);
    
//     alert(arrayResult[1][0]+"   "+BalancePolGrid.getRowColData(i - 1, 1)); 
  	 BalancePolGrid.setRowColData(i-1,3,arrayResult[1][0]);
  	}
	}
}