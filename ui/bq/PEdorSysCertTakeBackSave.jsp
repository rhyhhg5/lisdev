<%
//程序名称：PEdorSysCertTakeBackSave.jsp
//程序功能：
//创建日期：2002-10-25
//创建人  ：周平
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<SCRIPT src="./CQueryValueOperate.js"></SCRIPT>
<SCRIPT src="IndiDunFeeInput.js"></SCRIPT>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.certify.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.bq.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%@page contentType="text/html;charset=gb2312" %>

<%!
	String buildMsg(boolean bFlag, String strMsg) {
		String strReturn = "";
		
		strReturn += "<html><script language=\"javascript\">";
		
		if( bFlag ) {
			strReturn += "  parent.fraInterface.afterSubmit('Succ', '操作成功完成');";
		} else {
			strReturn += "  parent.fraInterface.afterSubmit('Fail','" + strMsg + "');";
		}
		strReturn += "</script></html>";
		
		return strReturn;
	}
%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  boolean bContinue = true;

    GlobalInput tGI = new GlobalInput(); 
    tGI=(GlobalInput)session.getValue("GI");  
    
   String tCertifyNo = request.getParameter("CertifyNo"); 
   String tCertifyCode = request.getParameter("CertifyCode"); 
   String tEdorNo = request.getParameter("EdorNo"); 
   String tEdorAcceptNo = request.getParameter("EdorAcceptNo");
   String tPolNo = request.getParameter("PolNo"); 
   String tMissionID = request.getParameter("MissionID"); 
   String tSubMissionID = request.getParameter("SubMissionID"); 
   String tTakeBackOperator=  request.getParameter("TakeBackOperator") ;
   String tTakeBackMakeDate = request.getParameter("TakeBackMakeDate") ;
   String tSendOutCom = request.getParameter("SendOutCom") ;
   String tReceiveCom = request.getParameter("ReceiveCom") ;
      
   System.out.println("tCertifyCode:"+tCertifyCode);
   System.out.println("tCertifyNo:"+tCertifyNo);
   System.out.println("tMissionID:"+tMissionID);
   System.out.println("tEdorNo:"+tEdorNo);
   System.out.println("tEdorAcceptNo:"+tEdorAcceptNo);
   System.out.println("tPolNo:"+tPolNo);
   System.out.println("tTakeBackOperator:"+tTakeBackOperator);
   System.out.println("TakeBackMakeDate:"+tTakeBackMakeDate);
   
  if( tGI == null ) {
  	out.println( buildMsg(false, "网页超时或者没有操作员信息") );
  	return;
  } else {
  }
  //内容待填充
	try {
	
		  LZSysCertifySchema tLZSysCertifySchema = new LZSysCertifySchema();
	    tLZSysCertifySchema.setCertifyCode(tCertifyCode);
			tLZSysCertifySchema.setCertifyNo(tCertifyNo);
			tLZSysCertifySchema.setTakeBackOperator(tTakeBackOperator);
			tLZSysCertifySchema.setTakeBackMakeDate(tTakeBackMakeDate);
			tLZSysCertifySchema.setSendOutCom(tSendOutCom);
			tLZSysCertifySchema.setReceiveCom(tReceiveCom);	
			
	if ("7777".equals(tCertifyCode)) 
	{  					
		  // 准备传输数据 VData
		  String tOperate = new String();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("CertifyNo",tCertifyNo);
			tTransferData.setNameAndValue("CertifyCode",tCertifyCode) ;
			tTransferData.setNameAndValue("EdorAcceptNo",tEdorNo) ;
			tTransferData.setNameAndValue("MissionID",tMissionID) ;
			tTransferData.setNameAndValue("SubMissionID",tSubMissionID) ;
			tTransferData.setNameAndValue("LZSysCertifySchema",tLZSysCertifySchema);
		  tOperate = "0000000027"; //回收核保通知书任务节点编码       
		  VData tVData = new VData();
		  tVData.add(tTransferData);
		  tVData.add(tGI);

	    EdorWorkFlowUI tEdorWorkFlowUI = new EdorWorkFlowUI();
			if (!tEdorWorkFlowUI.submitData(tVData,tOperate))//执行保全核保工作流单证回收
			{
			    out.println( buildMsg(false, " 保存失败，原因是: " + tEdorWorkFlowUI.mErrors.getFirstError()));
			  	return;
			}
	}
	else
	{  	
			// 准备传输数据 VData
		  String tOperate = new String();
			TransferData tTransferData = new TransferData();
			tTransferData.setNameAndValue("CertifyNo",tCertifyNo);
			tTransferData.setNameAndValue("CertifyCode",tCertifyCode) ;
			tTransferData.setNameAndValue("PolNo",tPolNo) ;
			tTransferData.setNameAndValue("EdorNo",tEdorNo) ;
			tTransferData.setNameAndValue("MissionID",tMissionID) ;
			tTransferData.setNameAndValue("SubMissionID",tSubMissionID) ;
			tTransferData.setNameAndValue("LZSysCertifySchema",tLZSysCertifySchema);
		   if(tCertifyCode == "7773" || tCertifyCode.trim().equals("7773"))	
		      tOperate = "0000000011";//回收体检通知书任务节点编码
		   if(tCertifyCode == "7775" || tCertifyCode.trim().equals("7775"))	
		      tOperate = "0000000012"; //回收核保通知书任务节点编码    
		           
		   VData tVData = new VData();
		   tVData.add(tTransferData);
		   tVData.add(tGI);
		   System.out.println("tOperate:"+tOperate);
	       // 数据传输
				PEdorManuUWWorkFlowUI tPEdorManuUWWorkFlowUI   = new PEdorManuUWWorkFlowUI();
				if (!tPEdorManuUWWorkFlowUI.submitData(tVData,tOperate))//执行保全核保工作流单证回收
				{
			    out.println( buildMsg(false, " 保存失败，原因是: " + tPEdorManuUWWorkFlowUI.mErrors.getFirstError()));
			  	return;
			  }
	}
	  
	  out.println( buildMsg(true, "") );
	  
	} catch(Exception ex) {
		ex.printStackTrace( );
	 	out.println( buildMsg(false, " 保存失败，原因是: " + ex.toString()));
	 	return;
}
%>