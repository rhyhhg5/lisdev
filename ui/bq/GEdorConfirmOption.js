//业务类型改变时显示相应的信息录入页面
function afterCodeSelect()
{
	//缴费方式变更
	var payMode = fm.all("PayMode").value
	if ((payMode == "1") || (payMode == "2") || (payMode == "3")) //自行缴费
	{
		if (fm.fmtransact.value == "1") //交费
		{
			document.all("trEndDate").style.display = "";
		}
	  else //退费不设日期
	  {
	  	document.all("trEndDate").style.display = "none";
	  }
		document.all("trPayDate").style.display = "none";
		document.all("trBank").style.display = "none";
		document.all("trAccount").style.display = "none";
	}
	if (payMode == "4") //银行转帐
	{
		document.all("trEndDate").style.display = "none";
		document.all("trPayDate").style.display = "";
		document.all("trBank").style.display = "";
		document.all("trAccount").style.display = "";
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
	try { showInfo.close(); } catch(re) { }
	
	if (fm.all("printMode")[0].checked)
	{
		//	top.opener.parent.fraPrint.F1Book1.book.filePrint(true);
		window.open("ShowGrpEdorPrint.jsp?EdorAcceptNo="+mEdorAcceptNo);
	}   
	else if (fm.all("printMode")[1].checked) //批量打印 暂时没有实现
	{   
	}
	else if (fm.all("printMode")[2].checked)
	{   
	}
	
	try
	{
	    top.opener.top.opener.top.opener.location.reload();
	    top.opener.top.opener.top.close();
	}
	catch (e) 
	{
	    top.opener.top.opener.location.reload();
	}
	top.opener.top.close();
	top.close();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmitAsk( FlagStr, content)
{
  showInfo.close();
  window.focus();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
}
function boforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  var getMoney = getGetMoney(mEdorAcceptNo);
  if (getMoney != 0)
  {
    if (fm.PayMode.value == "")
    {
        alert("交费/付费方式不能为空。");
        return false;
    }
    if (fm.PayMode.value == "4")
    {
        if(fm.PayDate.value == "")
        {
            alert("转帐日期不能为空。");
            return false;
        }
        if(!isDate(fm.PayDate.value))
        {
            alert("转帐日期录入有误。");
            return false;
        }
    }
    if (getMoney > 0)
    {
        if (fm.PayMode.value == "1" || fm.PayMode.value == "2") 
        {
          if (fm.EndDate.value == "")
          {
            alert("截止日期不能为空");
            return false;
          }
          if (!isDate(fm.EndDate.value))
          {
            alert("截止日期录入有误。");
            return false;
          }
        }
    }
  }
  
  if (fm.PayMode.value == "4")
  {
    if (fm.Bank.value == "")
    {
      alert("转帐银行为空不能选择银行转帐！");
      return false;
    }
    if (fm.BankAccno.value == "")
    {
      alert("转帐帐号为空不能选择银行转帐！");
      return false;
    }
  }
  return true;
}

//提交
function submitForm()
{
  if (!boforeSubmit())
  {
    return false;
  }
	fm.submit();
}

function getGetMoney(edorAcceptNo)
{
	var	strSql = "select GetMoney from LPGrpEdorMain where EdorAcceptNo = '" + edorAcceptNo + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("查询保全主表信息出错！");
	  return 0;
	}
	var getMoney = arrResult[0][0];
	return getMoney;
}


function checkOverFlow(mEdorAcceptNo)
{
	var strSql1="select grpcontno from LPGrpEdorMain where edoracceptno='"+mEdorAcceptNo+"'";
	var arrResult1 = easyExecSql(strSql1);
	if(arrResult1 == null||arrResult1=='')
	{
		alert("查询保全信息出错！");
	  return -1;
	}
	var grpContNo=arrResult1[0][0];
	
	var strSql="select case when (select coalesce(sum(summoney),0) from ("+
			"select  distinct a.incomeno no,a.sumactupaymoney summoney from LJAPay a,LJAGetEndorse b "+
			"where a.PayNo=b.ActuGetNo and b.GrpContNo='"+grpContNo+"' and a.IncomeType='B' "+
			"union "+
			"select  distinct a.otherno no,a.sumgetmoney*-1 summoney from LJAGet a,LJAGetEndorse b where a.ActuGetNo=b.ActuGetNo and b.GrpContNo='0000029701' and a.paymode='B'"+
			") as temp)>"+
			"(select prem*0.15 from lcgrpcont where grpcontno='"+grpContNo+"') then 1 else 0 end case "+
			"from dual";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null||arrResult=='')
	{
		alert("查询结算信息出错！");
	  return -1;
	}
	if(arrResult[0][0]=='1')
	{
		if(confirm("是否生成提前结算任务？"))
		{
			fm.action="./BqBalanceAdvanceAskSave.jsp?grppolno="+grpContNo;
			fm.submit();
		}
		return 1;
	}else
	{
		return 0;
	}
}

//费用为0则不设交费方式
function queryMenoy(edorAcceptNo)
{
	var getMoney = getGetMoney(edorAcceptNo);	
	if (getMoney == null)
	{
		return false;
	}
	if (getMoney == 0)//费用为0则不设交费方式
	{
		trPayMode.style.display = "none";
		return true;
	}else
	{
		var ret=isExistBalanceInfo(edorAcceptNo);//查询保单是否已经定义了结算频次
		if(ret=='-1')//查询出错
			return false;
		else if(ret=='1')//已经定义结算频次
		{
			var balIntv=getBalanceIntv(edorAcceptNo);
			if(balIntv=='-1')//查询出错
			{
				return false;
			}
			if(balIntv!='0')//定期结算
			{
				trPayMode.style.display = "none";
				return true;
			}else
			{
				return controlBalanceAnyTimeShow(getMoney,edorAcceptNo);
			}
		}else//没有定义结算频次,随时结算
		{
			return controlBalanceAnyTimeShow(getMoney,edorAcceptNo);
		}
	}
}

//
function controlBalanceAnyTimeShow(getMoney,edorAcceptNo)
{
	if (getMoney > 0) //交费
	{
		//var accMoney=queryAccMenoy(edorAcceptNo);
		//if(accMoney=='')
		//{
		//	alert('查询帐户余额表出错！');
		//	return false;
		//}
		//if(Number(accMoney)*-1>=Number(getMoney))
		//{
		//	trPayMode.style.display = "none";
		//	return true;	
		//}else
		//{
			document.all("FeeFlag").style.display = "";
			fm.fmtransact.value = "1";
			return true;	
	}else //退费
	{
		//var accMoney=queryAccMenoy(edorAcceptNo);
		//if(accMoney=='')
		//{
		//	alert('查询帐户余额表出错！');
		//	return false;
		//}
    //
		//if(Number(accMoney)>0)
		//{
		//	trPayMode.style.display = "none";
		//	return true;					
		//}else
		//{
			fm.fmtransact.value = "0";
			return true;
		}			
}

//查询保单是否已经定义了结算频次
function isExistBalanceInfo(edorNo)
{
	var sql = "select count(1) from lcgrpbalplan where grpcontno="+
						"(select grpcontno from LPGrpEdorMain where edoracceptno='"+edorNo+
						"')";
	var arrResult = easyExecSql(sql, 1, 0);
	if(arrResult==null||arrResult=='')
	{
		alert("查询结算计划信息失败！");
		return '-1';
	}
	if(arrResult[0][0]=='0')
	{
		return '0';
	}else
	{
		return '1';
	}
}

function getBalanceIntv(endorNo)
{
	var sql = "select balIntv from lcgrpbalplan where grpcontno="+
						"(select grpcontno from LPGrpEdorMain where edoracceptno='"+endorNo+
						"')";
	var arrResult = easyExecSql(sql, 1, 0);
	if (arrResult==null||arrResult=='')
	{	
		alert("查询结算计划信息失败！");
		return '-1';
	}else
	{
		return arrResult[0][0];
	}
}

//得到投保人帐户的变更金额
function queryAccMenoy(edorAcceptNo)
{
	var	strSql = "select coalesce(sum(money),0) from lcappacctrace where otherno = '" + edorAcceptNo + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult==null)
	{
		return '';
	}
	var getMoney = arrResult[0][0];
	return getMoney;
}

//得到交费记录号
function getNoticeNo()
{
	var strSql = "select getnoticeno from LJSGetEndorse a, LPGrpEdorMain b " +
	             "where a.EndorSementNo = b.EdorNo and b.EdorAcceptNo = '" + mEdorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.GetNoticeNo.value = arrResult[0][0];
	}
}

//得到交费银行和帐号
function queryAccount()
{
	var strSql = "select a.BankCode, a.BankAccno, a.AccName " +
				 "from LCGrpCont a, LPGrpEdorMain b " +
				 "where a.GrpContNo = b.GrpContNo " +
				 "and   b.EdorAcceptNo = '" + mEdorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.all("Bank").value = arrResult[0][0];
		fm.all("BankName").value = arrResult[0][0];
		fm.all("BankAccno").value = arrResult[0][1];
		fm.all("AccName").value = arrResult[0][2];
		showOneCodeName("Bank", "BankName");  
	}
}

function cancel()
{
    try
    {
        //从个人信箱直接到批单页面时找不到top.opener.top.opener.top.opener，将会抛出异常
        top.opener.top.opener.top.opener.focus();
        top.opener.top.opener.top.opener.location.reload();
        top.opener.top.opener.top.close();
    }
    catch(e)
    {
        top.opener.top.opener.focus();
        top.opener.top.opener.location.reload();
    }
    
    top.opener.top.close();
    top.close();
}