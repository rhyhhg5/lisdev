<%
//程序名称：AllPBqQueryInit.jsp
//程序功能：
//创建日期：2002-12-16
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>  
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	String loadFlag = request.getParameter("LoadFlag");
%>                               

<script language="JavaScript">
var loadFlag = "<%=loadFlag%>";
// 输入框的初始化（单记录部分）
function initInpBox()
{                                 
  // 保单查询条件
  fm.all('EdorAcceptNo').value = '';
  fm.all('EdorAppState').value = '';
  fm.all('ManageCom').value = "<%=tG.ManageCom%>";
}
                                   

function initForm()
{
  try
  {
    initInpBox();
    initItemGrid();
    initMainGrid();
    initAppGrid();  
    initElementtype();
    
    //在电话催缴中调用撤销时直接查询处数据
    if("<%=loadFlag%>" == ("TaskPhoneHasten"))
    {
    	fm.EdorAcceptNo.value = "<%=request.getParameter("EdorAcceptNo")%>";
    	easyQueryClick();
    }  
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initItemGrid()
{                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		        //列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批单号";         		//列名
      iArray[1][1]="135px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="合同号码";         		//列名
      iArray[2][1]="135px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="保全项目";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=50;            		//列最大值
      iArray[3][3]=0; 
      
      iArray[4]=new Array();
      iArray[4][0]="批改状态";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=65;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="入机日期";         		//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="入机时间";         		//列名
      iArray[6][1]="90px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;                   //是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="被保人号";         		//列名
      iArray[7][1]="0px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许      
      
      iArray[8]=new Array();
      iArray[8][0]="险种";         		//列名
      iArray[8][1]="0px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许      



      
      EdorItemGrid = new MulLineEnter( "fm" , "EdorItemGrid" ); 
      
      //这些属性必须在loadMulLine前
      EdorItemGrid.mulLineCount = 0;   
      EdorItemGrid.displayTitle = 1;
      EdorItemGrid.locked = 1;
      EdorItemGrid.canSel = 1;
      EdorItemGrid.hiddenPlus=1;           //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      EdorItemGrid.hiddenSubtraction=1;    //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
      EdorItemGrid.loadMulLine(iArray);  


      //这些操作必须在loadMulLine后面
      //EdorItemGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initMainGrid()
{                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		        //列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批单号";         		//列名
      iArray[1][1]="135px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="合同号码";         		//列名
      iArray[2][1]="135px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="批改状态";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=50;            		//列最大值
      iArray[3][3]=0; 
      
      iArray[4]=new Array();
      iArray[4][0]="入机日期";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=65;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="入机时间";         		//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许




      
      EdorMainGrid = new MulLineEnter( "fm" , "EdorMainGrid" ); 
      
      //这些属性必须在loadMulLine前
      EdorMainGrid.mulLineCount = 0;   
      EdorMainGrid.displayTitle = 1;
      EdorMainGrid.locked = 1;
      EdorMainGrid.canSel = 1;
      EdorMainGrid.selBoxEventFuncName="getItemDetail";//必须放在cansel属性下边定义才有效
      EdorMainGrid.hiddenPlus=1;           //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      EdorMainGrid.hiddenSubtraction=1;    //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
      EdorMainGrid.loadMulLine(iArray);  


      //这些操作必须在loadMulLine后面
      //EdorMainGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initAppGrid()
{                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		        //列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保全受理号";         		//列名
      iArray[1][1]="135px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="客户号";         		//列名
      iArray[2][1]="135px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="申请人名称";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=50;            		//列最大值
      iArray[3][3]=0; 
      
      iArray[4]=new Array();
      iArray[4][0]="批改状态";         		//列名
      iArray[4][1]="60px";            		//列宽
      iArray[4][2]=65;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="申请日期";         		//列名
      iArray[5][1]="90px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      







      
      EdorAppGrid = new MulLineEnter( "fm" , "EdorAppGrid" ); 
      
      //这些属性必须在loadMulLine前
      EdorAppGrid.mulLineCount = 0;   
      EdorAppGrid.displayTitle = 1;
      EdorAppGrid.locked = 1;
      EdorAppGrid.canSel = 1;
      EdorAppGrid.selBoxEventFuncName="getMainDetail";//必须放在cansel属性下边定义才有效
      EdorAppGrid.hiddenPlus=1;           //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      EdorAppGrid.hiddenSubtraction=1;    //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
      EdorAppGrid.loadMulLine(iArray);  


      //这些操作必须在loadMulLine后面
      //EdorAppGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>