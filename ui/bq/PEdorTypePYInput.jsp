<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2003-08-22
//创建人  ：hezy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT> 
  <SCRIPT src="./PEdorTypePY.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypePYInit.jsp"%>

  
  
  <title>交费年期变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypePYSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>
     
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=PolNo>
      </TD>   
    </TR>
  </TABLE> 
  


  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
        保单交费信息
      </td>
   </tr>
   </table>
  	<Div  id= "divDetail" style= "display: none">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            原交费年期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=PayYearsOld >
          </TD>
         </TR>
        
     </table>
 
      <hr>
      
      <table>
         <tr>
            <td>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
            </td>
            <td class= titleImg>
              修改信息
            </td>
         </tr>
      </table>
      
      <table class = common>
      <TR  class= common>
	          <TD  class= title>
	            交费年期
	          </TD>
	          <TD  class= input>
	             <Input class="code" name=PayYearsNew verify="交费年期|notnull&code:PayYears" ondblclick="return showCodeList('PayYears',[this],null,null,codeSql,'1',1);" onkeyup="return showCodeListKey('PayYears',[this]);">
	          </TD>
     </tr>
	</table>
	        
	    <hr>
	    
      <table class = common>
			<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypePYSave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
     	  <Input class= common type=Button value="返回" onclick="returnParent()">
       		 <!--<Input class= common type=Button value="取消" onclick="edorTypeICReturn()">-->
     	 </TD>
     	 </TR>
     	</table>
    </Div>
	</Div>
	  
	  
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
var tPolNo=top.opener.fm.PolNo.value;
var codeSql = "#1# and riskcode in(select riskcode from LCPol where polno=#"+tPolNo+"#)";
</script>
