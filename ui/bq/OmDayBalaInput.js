//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	 parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function queryClick()
{
  if(!verifyInput2())
  {
    return false;
  }
  var startdate = fm.StartDate.value;
  var enddate = fm.EndDate.value;
  var managecom = trim(fm.ManageCom.value);
  var riskcode = trim(fm.RiskCode.value);
  var riskpart = "";
  if(managecom=="86")
  {
    if(riskcode=="") 
    {
      if(!confirm("管理机构选择“总公司”并且没有选择险种，查询速度会很慢，是否继续？"))
      {
        return false;
      }
    }
    else
    {
      if(!confirm("管理机构选择“总公司”，查询速度会很慢，是否继续？"))
      {
        return false;
      }
    }
  }
  
  if(riskcode!="")
  {
    riskpart = "and x.riskcode = '" + riskcode + "' " ;
  }
  var sql = "with tmp1(comcode,riskcode,moneytype,money) as ( "
          + "select managecom,riskcode,moneytype,sum(money) "
          + "from lcinsureacctrace "
          + "where  paydate between '" + startdate + "' and '" + enddate + "' "
          + "group by managecom,riskcode,moneytype "
          + "union all "
          + "select managecom,riskcode,moneytype,sum(money) "
          + "from lbinsureacctrace "
          + "where  paydate between  '" + startdate + "' and '" + enddate + "' "
          + "group by managecom,riskcode,moneytype "
          + "),tmp2(comcode,riskcode,bf_zf_money,b_money,lx_money,wt_money,ct_money,tm_money,fm_money,lq_money,gl_money) as ( "
          + "select comcode,riskcode, "
          + "sum(case when moneytype in ('BF','ZF') then money else 0 end) bf_zf_money, "
          + "sum(case when moneytype in ('B') then money else 0 end) b_money, "
          + "sum(case when moneytype in ('LX') then money else 0 end) lx_money, "
          + "sum(case when moneytype in ('WT') then -money else 0 end) wt_money, "
          + "sum(case when moneytype in ('CT') then -money else 0 end) ct_money, "
          + "sum(case when moneytype in ('TM') then -money else 0 end) tm_money, "
          + "sum(case when moneytype in ('MF') then -money else 0 end) fm_money, "
          + "sum(case when moneytype in ('LQ') then -money else 0 end) lq_money, "
          + "sum(case when moneytype in ('GL') then -money else 0 end) gl_money "
          + "from tmp1  "
          + "group by comcode,riskcode "
          + "),tmp3(comcode,riskcode,feefinatype,pay) as ( "
          + "select managecom,riskcode,managecom,sum(pay) "
          + "from LJAGetClaim "
          + "where confdate between  '" + startdate + "' and '" + enddate + "' "
          + "group by managecom,riskcode,feefinatype "
          + "),tmp4(comcode,riskcode,sc,sw) as ( "
          + "select comcode,riskcode, "
          + "sum(case when  feefinatype = 'SC' then pay else 0 end) sc, "
          + "sum(case when  feefinatype = 'SW' then pay else 0 end) sw "
          + "from tmp3  "
          + "group by comcode,riskcode "
          + "),tmp5(comcode,riskcode,fee) as ( "
          + "select managecom,riskcode,sum(fee) from ( "
          + "select managecom,riskcode,fee "
          + "from lcinsureaccfeetrace "
          + "where paydate between  '" + startdate + "' and '" + enddate + "' and moneytype= 'GL'  "
          + "union all  "
          + "select managecom,riskcode,fee "
          + "from lbinsureaccfeetrace "
          + "where paydate between  '" + startdate + "' and '" + enddate + "' and moneytype= 'GL'  "
          + ") as xx "
          + "group by managecom,riskcode "
          + "),tmp6(comcode,comname,riskcode,riskname,risktype4) "
          + "as( "
          + "select comcode,name,riskcode,riskname, risktype4 from lmriskapp,ldcom), "
          + "tmp7(comcode,comname,riskcode,riskname,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11) "
          + "as( "
          + "select x.comcode,x.comname,x.riskcode,x.riskname, "
          + "(nvl(bf_zf_money,0)+nvl(fee,0)) c1, " //保费收入
          + "nvl(b_money,0) c2, " //持续奖金
          + "nvl(lx_money,0) c3, " //保单利息
          + "(nvl(gl_money,0)+nvl(fee,0)) c4, " //初始费用
          + "nvl(wt_money,0) c5, " //犹豫期退保
          + "nvl(ct_money,0) c6, " //退保金
          + "nvl(tm_money,0) c7, " //退保费用
          + "nvl(fm_money,0) c8, " //保单管理费
          + "nvl(lq_money,0) c9, " //部分领取
          + "nvl(sc,0) c10, " //重疾保险金
          + "nvl(sw,0) c11 " //身故保险金
          + "from tmp6 x  "
          + "left join tmp2 a on x.riskcode=a.riskcode and x.comcode=a.comcode "
          + "left join tmp4 b on x.riskcode=b.riskcode and x.comcode=b.comcode "
          + "left join tmp5 c on x.riskcode=c.riskcode and x.comcode=c.comcode  "
          + "where x.risktype4 = '4'  "
          + "and x.comcode like '" + managecom + "%' " + riskpart 
          + "and length(trim(x.comcode)) = 8 order by x.comcode,x.riskcode), "
          + "tmp8(comcode,comname,riskcode,riskname,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12) "
          + "as( "
          + "select comcode,comname,riskcode,riskname,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,(c1+c2+c3-c4-c5-c6-c7-c8-c9-c10-c11) from tmp7) "
          + "select '合计','','','',varchar(sum(c1)),varchar(sum(c2)),varchar(sum(c3)),varchar(sum(c4)),varchar(sum(c5)),varchar(sum(c6)),"
          + "varchar(sum(c7)),varchar(sum(c8)),varchar(sum(c9)),varchar(sum(c10)),varchar(sum(c11)),varchar(sum(c12)) from tmp8 "
          + "union all "
          + "select comcode,comname,riskcode,riskname,varchar(c1),varchar(c2),varchar(c3),varchar(c4),varchar(c5),varchar(c6),"
          + "varchar(c7),varchar(c8),varchar(c9),varchar(c10),varchar(c11),varchar(c12) from tmp8 " ;
  turnPage1.pageDivName = "divPage1";
  turnPage1.queryModal(sql, OmDayGrid);
  fm.StartDate1.value = startdate;
  fm.EndDate1.value = enddate;
  fm.ManageCom1.value = managecom;
  fm.RiskCode1.value = riskcode;
}

function clearRisk()
{
  fm.RiskCode.value = "";
  fm.RiskName.value = "";
}

function Download()
{
  if (OmDayGrid.mulLineCount == 0)
  {
    alert("打印列表没有数据，请先查询！");
    return false;
  }
  fm.action = "../bq/OmDayDownload.jsp";
  fm.submit();
}

function afterDownload(outname,outpathname)
{
  var url = "../f1print/download.jsp?filename="+outname+"&filenamepath="+outpathname;
  fm.action=url;
  fm.submit();
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

  
