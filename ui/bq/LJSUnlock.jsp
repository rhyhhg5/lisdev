<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LJSUnlock.jsp
//程序功能：续期收付费信息修改录入界面
//创建日期：2009-04-10
//创建人  ：zhanggm
//页面写的比较恶心，没办法，需求就这么订的，4个功能竟然放在一个页面。
//更新记录：  更新人 zhanggm   更新日期   2009-5-7  更新原因/内容 保全续期页面拆分
%>
<script>
  var tsql = "1 and code in (select code from ldcode where codetype = #paymodebqpay#)";
  var tsq2 = "1 and code in (select code from ldcode where codetype = #paymodebqget#)";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!―页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="LJSUnlock.js"></SCRIPT> 
  <%@include file="LJSUnlockInit.jsp"%> 
  
  
  <title>保全收付费信息修改</title>
</head>
<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form action="LJSUnlockSave.jsp" method=post name=fm target="fraSubmit">
    <table class = common align = center>
	  <tr  class= common>		    	
		<td  class= input><input type="radio" name="Type2" id="Type2" value="0" onclick=getType("3") checked>收费项目
		<input type="radio" name="Type2" id="Type2" value="1" onclick=getType("4")>付费项目</td>
		<td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    </table>
  
  <div id="divBQPay" align=left style="display: '' ">
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class= "codeno"  name=ManageCom1  ondblclick="return showCodeList('comcode',[this,ManageComName1],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName1],[0,1],null,null,null,1);" ><Input class=codename readonly name=ManageComName1></td>
        <td  class= title>工单号  </td>
        <td  class= input><input class=common  name=WorkNo1 ></td>
        <td  class= title>收费凭证号</td>
        <td  class= input><input class=common  name=GetNoticeNo1 ></td>
      </tr>
      <tr class= common>
      <td class=button><input class=cssButton  VALUE="查  询"  TYPE=button onclick=queryClick("BQPAY")></td>
      </tr>
    </table>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLJSPayBQ);"></td>
	  <td class=titleImg>转账失败清单</td>
	</tr>
  </table>
  <div id="divLJSPayBQ" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1><span id="spanLJSPayBQGrid"></span></td>
	  </tr>
	</table>
  </div>
  <div id="divPage1" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
  </div>
  <br>
  <table  class= common align=center>
      <tr  class= common>
        <td  class= title>收费凭证号</td>
        <td  class= input><input class=readonly  name=GetNoticeNoR1 readonly></td>
        <td  class= title>工单号  </td>
        <td  class= input><input class=readonly  name=WorkNoR1 readonly></td>
        <td  class= title>收费金额</td>
        <td  class= input><input class=readonly  name=GetMoney1 readonly></td>
      </tr>
      <tr class= common>
        <td class= title >失败原因</td>
        <td class= input colspan=5><input class=readonly  name=Reason1 readonly></td>
      </tr>
    </table>
    <br>
    <table  class= common align=center>
      <tr  class= common>
        <td class= title>交费方式</td>    
		<td class= input><Input class="codeNo" name="PayMode1"  verify="交付费方式|notnull&code:PayMode"  ondblclick="return showCodeList('PayMode',[this,PayModeName1],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName1],[0,1],null,tsql,'1',1);"><Input class="codeName" name="PayModeName1" readonly></td>
        <td  class= title>对方姓名</td>
        <td  class= input><Input class= common name=Drawer1 ></td>
        <td  class= title>对方身份证号</td>
        <td  class= input><Input class= common  name=DrawerID1 ></td>
      </tr>
      <tr  class= common>
        <td  class= title>对方开户银行</td>
        <td  class= input><Input class="codeNo" name=BankCode1 ondblclick="return showCodeList('Bank',[this,BankCodeName1],[0,1]);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName1],[0,1]);"><Input class="codeName" name=BankCodeName1 readonly ></td>
        <td  class= title>对方银行帐号</td>
        <td  class= input><Input class= common  name=BankAccNo1 onblur="confirmBankAccNo(this)"></td>
        <td class=title>对方帐户名</td>
        <td class=input><Input class= common  name=AccName1 ></td>
      </tr>
    </table>
  </div>
  
  
  <div id="divBQGet" align=left style="display: 'none' ">
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class= "codeno"  name=ManageCom2  ondblclick="return showCodeList('comcode',[this,ManageComName2],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName2],[0,1],null,null,null,1);" ><Input class=codename readonly name=ManageComName2></td>
        <td  class= title>工单号/客户号  </td>
        <td  class= input><input class=common  name=WorkNo2 ></td>
        <td  class= title>付费凭证号</td>
        <td  class= input><input class=common  name=ActuGetNo2 ></td>
      </tr>
      <tr class= common>
      <td class=button><input class=cssButton  VALUE="查  询"  TYPE=button onclick=queryClick("BQGET")></td>
      </tr>
    </table>
  <table>
    <tr>
      <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLJAGetBQ);"></td>
	  <td class=titleImg>转账失败清单</td>
	</tr>
  </table>
  <div id="divLJAGetBQ" style="display:''">
	<table class=common>
      <tr class=common>
	    <td text-align:left colSpan=1><span id="spanLJAGetBQGrid"></span></td>
	  </tr>
	</table>
  </div>
  <div id="divPage2" align=center style="display: 'none' ">
	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  </div>
  <br>
  <table  class= common align=center>
      <tr  class= common>
        <td  class= title>付费凭证号</td>
        <td  class= input><input class=readonly  name=ActuGetNoR2 readonly></td>
        <td  class= title>工单号/客户号  </td>
        <td  class= input><input class=readonly  name=WorkNoR2 readonly></td>
        <td  class= title>付费金额</td>
        <td  class= input><input class=readonly  name=GetMoney2 readonly></td>
      </tr>
      <tr class= common>
        <td class= title >失败原因</td>
        <td class= input colspan=5><input class=readonly  name=Reason2 readonly></td>
      </tr>
    </table>
    <br>
    <table  class= common align=center>
      <tr  class= common>
        <td class= title>付费方式</td>    
		<td class= input><Input class="codeNo" name="PayMode2"  verify="交付费方式|notnull&code:PayMode"  ondblclick="return showCodeList('PayMode',[this,PayModeName2],[0,1],null,tsq2,'1',1);" onkeyup="return showCodeListKey('PayMode',[this,PayModeName2],[0,1],null,tsq2,'1',1);"><Input class="codeName" name="PayModeName2" readonly></td>
        <td  class= title>对方姓名</td>
        <td  class= input><Input class= common name=Drawer2 ></td>
        <td  class= title>对方身份证号</td>
        <td  class= input><Input class= common  name=DrawerID2 ></td>
      </tr>
      <tr  class= common>
        <td  class= title>对方开户银行</td>
        <td  class= input><Input class="codeNo" name=BankCode2 ondblclick="return showCodeList('Bank',[this,BankCodeName2],[0,1]);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName2],[0,1]);"><Input class="codeName" name=BankCodeName2 readonly ></td>
        <td  class= title>对方银行帐号</td>
        <td  class= input><Input class= common  name=BankAccNo2 onblur="confirmBankAccNo(this)"></td>
        <td class=title>对方帐户名</td>
        <td class=input><Input class= common  name=AccName2 ></td>
      </tr>
    </table>
  </div> 
    <input type=hidden id="ywtype" name="ywtype" value = "BQPAY">
    <div align=right>
      <!-- <input class=cssButton  VALUE="测试"  TYPE=button onclick=checkSubmit("BQPAY")> -->
      <p><font color="#ff0000">如果对收付方式有疑问，请您点击&quot;收付方式说明&quot;来查看收付方式的详细说明</font></p>
      <input type =button class=cssButton value="收付方式说明" onclick="payModeHelp();">
      <input class=cssButton  VALUE="确  认"  TYPE=button onclick="submitForm()">
    </div>
    
</form>
<!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
