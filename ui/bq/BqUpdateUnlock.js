/**
 * 
 */
var turnPage = new turnPageClass();
var showInfo;
function query(){
	if(fm.Edorno.value == null || fm.Edorno.value == ""){
		alert("保全受理号不能为空！！");
		return false;
	}
	if(fm.cont.value == null || fm.cont.value == ""){
		alert("请选择个团标志！！");
		return false;
	}
	initBqgdGrid();
	
	if(fm.cont.value == 1){
		var sql = "Select app.edoracceptno, " +
				"item.edortype, " +
				"item.edorappdate, " +
				"item.edorvalidate " +
				"From lpedoritem item, lpedorapp app " +
				"Where  item.edorno = app.edoracceptno " +
				"and app.othernotype = '"+fm.cont.value+"' " +
				"And  app.edorstate != '0' " +
				"and item.edortype not in ('NI','ZT') " +
				"and exists (select 1 from lcurgeverifylog where serialno = item.edorno)"+
				getWherePart( 'item.edorno','Edorno' );
		turnPage.queryModal(sql,BqgdGrid);
	}else{
		var sql = "Select app.edoracceptno," +
				" item.edortype," +
				"item.edorappdate, " +
				"item.edorvalidate " +
				"From lpgrpedoritem item, lpedorapp app " +
				"Where 1=1 " +
				"And item.edorno = app.edoracceptno " +
				"and app.othernotype = '"+fm.cont.value+"' " +
				"and app.edorstate != '0' " +
				"and item.edortype not in ('NI','ZT') " +
				"and exists (select 1 from lcurgeverifylog where serialno = item.edorno)"+
                getWherePart( 'item.edorno','Edorno' );
		turnPage.queryModal(sql,BqgdGrid);
	}
}
function run(){
	var i = 0;
	var check = 0;
	 for (i=0; i<BqgdGrid.mulLineCount; i++)
	  {
	    if (BqgdGrid.getSelNo(i))
	    {
	      check = BqgdGrid.getSelNo();
	      break;
	    }
	  }
	 if(check){
		 fm.edorno.value = BqgdGrid.getRowColData(check - 1, 1); 
	 }else{
		 alert("请选择一条信息！！");
		 return false;
	 } 
	 var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	 showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	 fm.submit();
}

function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		query();
	}	
}
