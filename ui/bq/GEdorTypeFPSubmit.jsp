<%
//程序名称：GEdorTypeACSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----ACsubmit---");
  LPGrpEdorItemSchema tLPGrpEdorItemSchema=new LPGrpEdorItemSchema();
  LCGrpServInfoSet tLCGrpServInfoSet=new LCGrpServInfoSet();
  LCGrpServInfoSchema tLCGrpServInfoSchema1=new LCGrpServInfoSchema();
  LCGrpServInfoSchema tLCGrpServInfoSchema2=new LCGrpServInfoSchema();
  PGrpEdorFPDetailUI tPGrpEdorFPDetailUI   = new PGrpEdorFPDetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String fmAction = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  fmAction = request.getParameter("fmAction");
	GlobalInput tG = new GlobalInput();
	    System.out.println("------------------begin ui");
	tG = (GlobalInput)session.getValue("GI");
	
	//设置保全信息
	tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
	tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpEdorItemSchema.setEdorAppNo(request.getParameter("EdorAppNo"));
	tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
  
  //添加服务频率修改信息，如果没有修改就不加入到SET中
  String servchoose1 =request.getParameter("servchoose1");
  String servchoose1Bak =request.getParameter("servchoose1Bak");
  if(servchoose1 != null && !servchoose1.equals(""))
  {
  	if(servchoose1Bak == null || !servchoose1.equals(servchoose1Bak))
  	{
  		tLCGrpServInfoSchema1.setGrpContNo(request.getParameter("ProposalGrpcontno"));
  		tLCGrpServInfoSchema1.setServKind("1");
		  tLCGrpServInfoSchema1.setServDetail("2");
		  tLCGrpServInfoSchema1.setServChoose(request.getParameter("servchoose1"));
			tLCGrpServInfoSet.add(tLCGrpServInfoSchema1);
		}
	}
	
	String servchoose2 =request.getParameter("servchoose2");
  String servchoose2Bak =request.getParameter("servchoose2Bak");
  if(servchoose2 != null && !servchoose2.equals(""))
  {
  	if(servchoose2Bak == null || !servchoose2.equals(servchoose2Bak))
  	{
  		tLCGrpServInfoSchema2.setGrpContNo(request.getParameter("ProposalGrpcontno"));
			tLCGrpServInfoSchema2.setServKind("1");
			tLCGrpServInfoSchema2.setServDetail("3");
			tLCGrpServInfoSchema2.setServChoose(request.getParameter("servchoose2"));
			tLCGrpServInfoSet.add(tLCGrpServInfoSchema2);
		}
	}

	try
  {
  	//准备传输数据 VData
  	VData tVData = new VData();  
  	
		//保存个人保单信息(保全)
		tVData.addElement(tLPGrpEdorItemSchema);
		tVData.addElement(tLCGrpServInfoSet);
		tVData.addElement(tG);
		tPGrpEdorFPDetailUI.submitData(tVData,fmAction);
	}
	catch(Exception ex)
	{
		Content = fmAction+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPGrpEdorFPDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

