<%
//程序名称：PEdorTypeZTSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
    //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
  //个人批改信息
  System.out.println("-----ZX---Save---");
  CErrors tError = null;
  String transact = "INSERT"; 
  String Content="";
  String FlagStr="";
   
  //后面要执行的动作：添加，修改
 request.setCharacterEncoding("GBK");
 String serclass1Sum[] = request.getParameterValues("LADiscountGrid1");
 String serclass2Sum[] = request.getParameterValues("LADiscountGrid2");
 String serclass3Sum[] = request.getParameterValues("LADiscountGrid4");
 String AddressSum[] = request.getParameterValues("LADiscountGrid3");
 String moneySum[] = request.getParameterValues("LADiscountGrid5");
 String danWeiSum[] = request.getParameterValues("LADiscountGrid6");
 String countSum[] = request.getParameterValues("LADiscountGrid7");
 String seqnoSum[] = request.getParameterValues("LADiscountGrid11");
 String EdorNo=request.getParameter("EdorNo");
 
 	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(EdorNo);
	tLPEdorItemSchema.setEdorNo(EdorNo);
	tLPEdorItemSchema.setEdorType("ZX");
	
   // 准备传输数据 VData
  VData tVData = new VData(); 
  LPEdorEspecialDataSet mLPEdorEspecialDataSet=new LPEdorEspecialDataSet();
  System.out.println(serclass1Sum.length);
  String ZuHe="";
  ZuHe+="一共购买了"+serclass1Sum.length+"个健管服务，明细如下：\n";
 for(int i=0;i<serclass1Sum.length;i++)
 {
	  String serclass1  = serclass1Sum[i];    //服务项目           
	  String serclass2 = serclass2Sum[i];//具体项目
	  String serclass3 = serclass3Sum[i];//档次
	  String Address = AddressSum[i];//服务地点
	  String money = moneySum[i];//服务价格
	  String danWei = danWeiSum[i];//
	  String count = countSum[i];//购买份数
	  String seqno = seqnoSum[i];//唯一标识
	  
	  ZuHe+="（"+(i+1)+"）服务项目:"+serclass1
	             +" 具体项目："+serclass2
	             +" 档次："+serclass3
	             +"\n    服务地点："+Address
	             +" 服务价格："+money+danWei
	             +" 购买份数："+count+";\n";
	  LPEdorEspecialDataSchema tLPEdorEspecialDataSchema   = new LPEdorEspecialDataSchema();
	  tLPEdorEspecialDataSchema.setEdorAcceptNo(EdorNo);
	  tLPEdorEspecialDataSchema.setEdorNo(EdorNo);
	  tLPEdorEspecialDataSchema.setEdorType("ZX");
	  tLPEdorEspecialDataSchema.setDetailType("ZXFWXM");
	  tLPEdorEspecialDataSchema.setEdorValue(count);
	  tLPEdorEspecialDataSchema.setPolNo(seqno);
	  mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
	             
	             
 }	             
//	  LPEdorEspecialDataSchema tLPEdorEspecialDataSchema   = new LPEdorEspecialDataSchema();
//	  tLPEdorEspecialDataSchema.setEdorAcceptNo(EdorNo);
//	  tLPEdorEspecialDataSchema.setEdorNo(EdorNo);
//	  tLPEdorEspecialDataSchema.setEdorType("ZX");
//	  tLPEdorEspecialDataSchema.setDetailType("ZXHealth");
//	  tLPEdorEspecialDataSchema.setEdorValue(ZuHe);
//	  tLPEdorEspecialDataSchema.setPolNo("000000");
//	  mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);

 String ZongJinEr=request.getParameter("ZongJinEr");
 LPEdorEspecialDataSchema tLPEdorEspecialDataSchema2   = new LPEdorEspecialDataSchema();
 tLPEdorEspecialDataSchema2.setEdorAcceptNo(EdorNo);
 tLPEdorEspecialDataSchema2.setEdorNo(EdorNo);
 tLPEdorEspecialDataSchema2.setEdorType("ZX");
 tLPEdorEspecialDataSchema2.setDetailType("AppMoney");
 tLPEdorEspecialDataSchema2.setEdorValue(ZongJinEr); 
 tLPEdorEspecialDataSchema2.setPolNo("000000");
 mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema2); 
 System.out.println("1111111111");
 tVData.addElement(mLPEdorEspecialDataSet);  
 tVData.add(tGlobalInput);
 tVData.add(tLPEdorItemSchema);
 System.out.println("222222222222");

PEdorZXDetailUI tPEdorZXDetailUI=new PEdorZXDetailUI();
try
  {
    tPEdorZXDetailUI.submitData(tVData,transact);
  }
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorZXDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
      FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理
 System.out.println("33333333"+FlagStr+"4444444444444"+Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

