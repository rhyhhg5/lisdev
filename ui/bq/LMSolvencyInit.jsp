
<%
//程序名称：PContTeminateInputInit.jsp
//程序功能：
//创建日期：2010-04-07 16:00
//创建人  ：WangHongLiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
%> 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">  



function initForm()
{
   try
   { 
    initSolvencyGrid(); 
    initImportGrid();
	querySolvencyGrid();
	queryImportGrid();

  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function initSolvencyGrid()
{
    var iArray = new Array();

      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="年份";
        iArray[1][1]="90px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="季度";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;

        iArray[3]=new Array();
        iArray[3][0]="偿付能力充足率";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="是否符合监管要求";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        
       SolvencyGrid = new MulLineEnter("fm" ,"SolvencyGrid" ); 
      //这些属性必须在loadMulLine前
      SolvencyGrid.mulLineCount = 10;   
      SolvencyGrid.displayTitle = 1;
      SolvencyGrid.locked = 1;
      SolvencyGrid.canSel = 1;
      SolvencyGrid.canChk = 0;
      SolvencyGrid.hiddenSubtraction=1;
      SolvencyGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      SolvencyGrid.selBoxEventFuncName ="onQuerySelected" ;
      SolvencyGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert("在LMRiskSolvencyInit.jsp-->SolvencyGrid函数中发生异常:初始化界面错误!");
      }
}
function initImportGrid()
{
    var iArray = new Array();

      try
      {   
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="年份";
        iArray[1][1]="90px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="季度";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;

        iArray[3]=new Array();
        iArray[3][0]="偿付能力充足率";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="是否符合监管要求";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        
        ImportGrid = new MulLineEnter("fm" ,"ImportGrid" ); 
      //这些属性必须在loadMulLine前
      ImportGrid.mulLineCount = 1;   
      ImportGrid.displayTitle = 1;
      ImportGrid.locked = 1;
      ImportGrid.canSel = 0;
      ImportGrid.canChk = 0;
      ImportGrid.hiddenSubtraction=1;
      ImportGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
//      SolvencyGrid.selBoxEventFuncName="ShowDetail";
      ImportGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert("在LMRiskSolvencyInit.jsp-->ImportGrid函数中发生异常:初始化界面错误!");
      }
}

</script>
