  
<%
	//程序名称：PEdorTypeRVSubmit.jsp
	//程序功能：
	//创建日期：2008-07-23
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息
	CErrors tError = null;
	//后面要执行的动作：添加，修改

	String tRela = "";
	String FlagStr = "";
	String Content = "";
	String transact = "";

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");

	transact = request.getParameter("fmtransact");//其值为'deal'
	String edorAcceptNo = request.getParameter("EdorAcceptNo");//其值为''
	String edorNo = request.getParameter("EdorNo");//
	String edorType = request.getParameter("EdorType");//RV
	String contNo = request.getParameter("ContNo");//000917200000004
	
	String appMoney = request.getParameter("AppMoney");//追加的签署
	String loanPremNoney = request.getParameter("PayMoneyFee");
	if(appMoney==null){
	appMoney="0.0";
	}
	if(loanPremNoney ==null){
	loanPremNoney = "0.0";
	}
    TransferData tTransferData = new TransferData();
	if(!("init".equals(transact)))
	{
	tTransferData.setNameAndValue("rates","0.0");
	tTransferData.setNameAndValue("edorAppDate","2008-8-2");
    tTransferData.setNameAndValue("appMoney",appMoney);
    tTransferData.setNameAndValue("LoanMoney",loanPremNoney);
	}	
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	tLPEdorItemSchema.setInsuredNo("000000");
	tLPEdorItemSchema.setPolNo("000000");
	
	// 准备传输数据 VData		
	VData tVData = new VData();
	
	
	PEdorRVDetailUI tPEdorRVDetailUI = new PEdorRVDetailUI();
	//初始化
	if ("init".equals(transact)) {
		tVData.add(tGlobalInput);
		tVData.add(tLPEdorItemSchema);
		if (!tPEdorRVDetailUI.submitData(tVData, transact)) {
			VData rVData = tPEdorRVDetailUI.getResult();
			System.out.println("Submit Failed! "
			+ tPEdorRVDetailUI.mErrors.getErrContent());
			Content = "初始化失败，原因是:"
			+ tPEdorRVDetailUI.mErrors.getFirstError();
			FlagStr = "Fail";
		} else {
			Content = "初始化成功";
			FlagStr = "Success";
		}
	} else {
		tVData.add(tGlobalInput);
		tVData.add(tLPEdorItemSchema);		
        tVData.add(tTransferData);
		if (!tPEdorRVDetailUI.submitData(tVData, transact)) {
			VData rVData = tPEdorRVDetailUI.getResult();
			System.out.println("Submit Failed! "
			+ tPEdorRVDetailUI.mErrors.getErrContent());
			Content = "失败，原因是:"
			+ tPEdorRVDetailUI.mErrors.getFirstError();
			FlagStr = "Fail";
		} else {
			Content = "保存成功";
			FlagStr = "Success";
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=transact%>");
</script>
</html>
