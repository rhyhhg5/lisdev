<%
//程序名称：GEdorTypeNISubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String flag;
	String content;
	
	String calType = request.getParameter("CalType");
	String revertType = request.getParameter("RevertType");
	String calTime = request.getParameter("CalTime");
	if ((calType != null) && (calType.equals("1"))) //一年期险选月份表必须还原
	{
		revertType = "1";
	}
	if ((calType != null) && (calType.equals("2"))) //选月份比例不用还原
	{
		revertType = "2";
	}

	GlobalInput gi = (GlobalInput)session.getValue("GI");
	LPGrpEdorItemSchema itemSchema = new LPGrpEdorItemSchema();
	itemSchema.setEdorNo(request.getParameter("EdorNo"));
	itemSchema.setEdorType(request.getParameter("EdorType"));
	itemSchema.setGrpContNo(request.getParameter("GrpContNo"));
	
	EdorItemSpecialData tSpecialData = new EdorItemSpecialData(itemSchema);
	tSpecialData.add("CalType", "2");//剩余时间比例
//	tSpecialData.add("RevertType","1");//注释掉
	tSpecialData.add("CalTime", "1");//按月计算
  tSpecialData.add("CreateAccFlag", "1");//设置个人账户
  tSpecialData.add("InputType",  "2");//清单导入
  tSpecialData.add("Source",  "1");//追加保费
  tSpecialData.add("Prem",  "");
  
  System.out.println(request.getParameter("CreateAccFlag"));
  System.out.println(request.getParameter("InputType"));
    System.out.println(request.getParameter("Source"));
	
	VData data = new VData();
	data.add(gi);
	data.add(itemSchema);
	data.add(tSpecialData);

	GrpEdorTZDetailUI tGrpEdorTZDetailUI = new GrpEdorTZDetailUI();
	if (!tGrpEdorTZDetailUI.submitData(data))
	{
		flag = "Fail";
		content = "数据保存失败！原因是:" + tGrpEdorTZDetailUI.getError();
	}
	else 
	{
		flag = "Succ";
		content = "数据保存成功。";
	}
	String message = tGrpEdorTZDetailUI.getMessage();
	if ((message != null) && (!message.equals("")))
	{
		content += message;
	}
	content = PubFun.changForHTML(content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>