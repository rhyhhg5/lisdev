<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GEdorConfirmOptionSave.jsp
//程序功能：保全个单缴费方式保存页面
//创建日期：2005-07-21
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
  
<%
	String flag = "Succ";
	String content = "";
	
	//接收参数
	String fmtransact = request.getParameter("fmtransact");
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String payMode = request.getParameter("PayMode");
	String endDate = request.getParameter("EndDate");
	String payDate = request.getParameter("PayDate");
	String bank = request.getParameter("Bank");
	String bankAccno = request.getParameter("BankAccno");
	String accName = request.getParameter("AccName");
	
	GlobalInput gi = (GlobalInput)session.getValue("GI");
  
  //生成交退费通知书
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("payMode", payMode);
  tTransferData.setNameAndValue("endDate", endDate);
  tTransferData.setNameAndValue("payDate", payDate);
  tTransferData.setNameAndValue("bank", bank);
  tTransferData.setNameAndValue("bankAccno", bankAccno);
  tTransferData.setNameAndValue("accName", accName);
	FeeNoticeGrpVtsUI tFeeNoticeGrpVtsUI = new FeeNoticeGrpVtsUI(edorAcceptNo);
	if (!tFeeNoticeGrpVtsUI.submitData(tTransferData))
	{
		flag = "Fail";
		content = "生成批单失败！原因是：" + tFeeNoticeGrpVtsUI.getError();
	}

	//银行转帐
	if ((payMode != null) && (payMode.equals("4")))
	{
		VData data = new VData();
		data.add(gi);
		data.add(tTransferData);
		SetPayInfo spi = new SetPayInfo(edorAcceptNo);
		if (!spi.submitDate(data, fmtransact))
		{
			System.out.println("设置转帐信息失败！");
		}
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>

