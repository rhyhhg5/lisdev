<%
//程序名称：PEdorTypeSCSubmit.jsp
//程序功能：提交特别约定修改
//创建日期：2004-7-19 16:04
//创建人  ：JiangLai
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
//执行客户端的提交处理。（查询和保存）
//准备提交到后台程序的数据，并调用后台程序进行处理客户端的提交的命令

  //准备输入后台的数据  
  System.out.println("==> PEdorTypeSCSubmit.jsp : ------SCsubmit Begin-----");
  LPSpecSchema tLPSpecSchema = new LPSpecSchema();
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
  PEdorSCDetailUI tPEdorSCDetailUI   = new PEdorSCDetailUI();    

  CErrors tError = null;            
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //取得提交执行的动作：insert 添加纪录，query 查询记录
  transact = request.getParameter("fmtransact");
  System.out.println("==> PEdorTypeSCSubmit.jsp : ------transact:"+transact);
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
  //设置个人保单批改信息
  tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
  tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
	
  if (transact.equals("INSERT||MAIN"))
  {
    //设置插入LPSpec表的数据
    tLPSpecSchema.setPolNo(request.getParameter("PolNo"));      
    tLPSpecSchema.setPolType("1");
    tLPSpecSchema.setEndorsementNo(request.getParameter("EdorNo"));
    
    String Path = application.getRealPath("config//Conversion.config");	
    tLPSpecSchema.setSpecContent(StrTool.Conversion(request.getParameter("Speccontent"),Path));
    //tLPSpecSchema.setSpecContent(request.getParameter("Speccontent"));
  }
  
  try
  {
    // 准备传输数据 VData
    VData tVData = new VData();  
  
    //保存个人保单信息(保全)	
    tVData.addElement(tG);
    tVData.addElement(tLPEdorMainSchema);
    tVData.addElement(tLPSpecSchema);
    System.out.println("==> PEdorTypeSCSubmit.jsp : PEdorSCDetailLUI Start...");
  	
    //进行后台处理（查询和保存申请）
    tPEdorSCDetailUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }			
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    System.out.println("==> PEdorTypeSCSubmit.jsp : ------success");
    tError = tPEdorSCDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
      FlagStr = "Success";
      
      //如果是查询，进行对查询结果的处理
      if (transact.equals("QUERY||MAIN"))
      {
        if (tPEdorSCDetailUI.getResult()!=null&&tPEdorSCDetailUI.getResult().size()>0)
        {
          Result = (String)tPEdorSCDetailUI.getResult().get(0);
          System.out.println("==>PEdorTypeSCSubmit : Result = " + Result);
          if (Result==null||Result.trim().equals(""))
          {
            FlagStr = "Fail";
            Content = "提交失败!!";
          }
        }
        else 
        {
          //没有特约信息记录
          Result="NULL";
        }
      }
    }
    else                                                                           
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>