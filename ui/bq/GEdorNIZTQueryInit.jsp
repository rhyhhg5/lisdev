<%
//程序名称：GedorNIZTQueryInit.jsp
//程序功能：
//创建日期：2016-10-09 18:00
//创建人  ：LC
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%> 

<script language="JavaScript">

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

//表单初始化
function initForm()
{
	initInputBox();
  	initContPauseGrid();
  	initElementtype();
}
function initContPauseGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="批次号";       			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[1][1]="100px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;             			//是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="保单号";       			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[2][1]="100px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;                         //是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="投保单位名称";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="申请项目";	   			    //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[4][1]="80px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="被保人人数";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[5][1]="60px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="处理状态";	   			    //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[6][1]="80px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[7]=new Array();
		iArray[7][0]="申请日期";	   				//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[7][1]="80px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;   	           			//是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="完成日期";   			    //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[8][1]="80px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[9]=new Array();
		iArray[9][0]="处理结果";	   				//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[9][1]="80px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[10]=new Array();
		iArray[10][0]="关联工单号";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[10][1]="120px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[11]=new Array();
		iArray[11][0]="备注";	   			    //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[11][1]="220px";            		//列宽
		iArray[11][2]=100;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		ContPauseGrid = new MulLineEnter( "fm" , "ContPauseGrid" );
		//这些属性必须在loadMulLine前
		ContPauseGrid.canSel =1;
		ContPauseGrid.mulLineCount = 0;
		ContPauseGrid.displayTitle = 1;
		//ContPauseGrid.locked = 1;
		ContPauseGrid.hiddenPlus=1;
		ContPauseGrid.hiddenSubtraction=1;
		ContPauseGrid.loadMulLine(iArray);
  
	}
	catch(ex)
	{
		alert("PContPauseInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}   
}
function initInputBox()
{
  try
	{
		fm.all('ManageCom').value = manageCom;
		var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
		fm.all('ManageComName').value = easyExecSql(sql1);
		var sql1 = "select Current Date - 1 month from dual ";
		fm.all('StartDate').value = easyExecSql(sql1);
		fm.all('EndDate').value=tCurrentDate;
		fm.all('DealState').value="0";
		showAllCodeName();
	}
	catch(ex)
	{
		alert("PContPauseInit.jsp-->initInputBox函数中发生异常:初始化界面错误!");
	}      
}
</script>