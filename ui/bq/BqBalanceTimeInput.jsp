<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    

<%
//程序名称：BqBalanceAdvanceInput.jsp
//程序功能：保单结算定期结算界面
//创建日期：2006-03-10
//创建人  ：Huxl
//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
   GlobalInput tG = new GlobalInput();
   tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
   System.out.println("管理机构-----"+tG.ComCode);
%>   

<script>
	var comCode = "<%=tG.ComCode%>";
	var tLoadFlag = "<%=request.getParameter("LoadFlag")%>";
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="BqBalanceTimeInput.js"></SCRIPT> 
<%@include file="BqBalanceTimeInit.jsp"%>

<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>定期结算</title>
</head>
<body  onload= "iniForm();">  
   <form action="../bq/BqBalanceTimeSave.jsp" method=post name=fm target="fraSubmit"> 
    <table class= common >
     <tr class= common >
      <td class= title> 保单号 </td>
      <td class= input>
        <Input class= readonly name =GrpContNo readonly >
      </td>
      <td class= title>投保单位名称 </td>
      <td class= input>
        <Input class= readonly name =AppntName   readonly >
      </td>
      <td class= title>已结算次数 </td>
      <td class= input>
        <Input class= readonly name =baltimes readonly >
      </td>
     </tr>
     <tr>
     	<td class= title>结算频次</td>
      <td class= input >
        	<input class="readonly" name="balintv" readonly >
      </td>
      <td class= title >本期结算期间</td>
      <td class= common >
        <input name= StartDate style="width:180"  class=readonly readonly >
      </td>
      <td class= title>至</td>
      <td class= common >
        <input name= EndDate style="width:180"   class=readonly readonly > 
      <td>
     </tr>
    </table>        
		<table>
				<tr> 
					<td class= common> 
						<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divbalance);"> 
					</td>
					<td class= titleImg>定期结算保单信息</td>
				</tr>
		</table>
		<Div  id= "divbalance" style= "display: ''" >  
		  	<table class= common border=0 width=100% >		
          <TR  class= common>		
           <td> 
            <span id ="spanBalancePolGrid" >
            </span>	
           </td>					
					</TR>
		  </table>
		  <DIV align="center">
			  <INPUT VALUE="首  页" class =  cssButton TYPE=button onclick="getFirstPage();"> 
	      <INPUT VALUE="上一页" class =  cssButton TYPE=button onclick="getPreviousPage();"> 					
	      <INPUT VALUE="下一页" class =  cssButton TYPE=button onclick="getNextPage();"> 
	      <INPUT VALUE="尾  页" class =  cssButton TYPE=button onclick="getLastPage();"> 	
	     </DIV>
	  </Div> 
	  <table>
	  	<tr>
	  	  <td class=common8 width=180>本期结算金额</td>
	  	  <td class= common width=60>
	  	    <input class=Input  name=balprem readonly>
	  	  </td>
	  	  <td class= common8 >
	  	    元
	  	  </td>
	    </tr>
	  </table>
	  <table class=common>
			<tr class=common id="trPayMode">
				<TD  class= title id="trPayTitleShow" style="display: 'none'">交费方式</TD>
				<TD  class= title id="trGetTitleShow" style="display: 'none'">退费方式</TD>
				<TD  class= input>
				  <select  id="trSelectTitleShow"name="PayMode" style="width: 128; height: 23" onchange="afterCodeSelect();" >
						<option value = "1">现金</option>
						<option value = "2">现金支票</option>
						<option value = "3">转帐支票</option>
						<option value = "4">银行转帐</option>
						<option value = "9">其它</option>
				  </select>
			 </TD>
			 <TD  class= title id="trPayEndDate" style = "display: 'none'">交费截止日期</TD>
			 <TD  class= input id="trPayEndDate2" style = "display: 'none'">
					<Input class="coolDatePicker" name="PayEndDate" DateFormat="short"">
			</TD>
			</tr>
		  <tr class=common id="trBank" style="display: 'none'">
				<TD  class= title>转帐银行</TD>
				<TD  class= input>
	      	<input type="hidden" class = "readonly" readonly name="Bank">
	      	<input class = "readonly" readonly name="BankName">
				</TD>		  
				<TD  class= title>转帐帐号</TD>
				<TD  class= input>
					<Input class="readonly" name="BankAccno" readonly>
					<!--帐户名 -->
					<Input type="hidden" name="AccName">
				</TD>
				<TD  class= title>转帐日期</TD>
				<TD  class= input>
					<Input class="coolDatePicker" name="PayDate" DateFormat="short"">
				</TD>
		  </tr>
		  <tr class=common id="trLater" style="display: 'none'">
		  	<TD  class= title>延期结算日期</TD>
		  	<TD  class= input>
					<Input class="coolDatePicker" name="laterPayDate" DateFormat="short"">
				</TD>
			</tr>
		 </table>
	  
	  
	  <hr>
	  <INPUT VALUE="结  算"  id="bal"  name= "bal" class= cssButton TYPE=button onclick="doBalance();">
	  <INPUT VALUE="打印定期结算单" id="printballis" name="printballis" class= cssButton TYPE=button onclick="PrintTimeBal();">
	  <INPUT VALUE="延期结算" id="ballater" name = "ballater" class = cssButton Type= button onclick="balancelater();"> 
	  <!--INPUT VALUE="返    回" class = cssButton TYPE=button onclick="returnparent();" --> 
	  <input type="hidden" name=AcceptNo>
	 </form> 
	 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 