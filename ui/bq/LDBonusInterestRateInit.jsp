<%
//程序名称：PContTeminateInputInit.jsp
//程序功能：
//创建日期：2010-04-07 16:00
//创建人  ：WangHongLiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
%> 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">  

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";
var tYear=tCurrentDate.substring(0,4);
//重置
function BonusInterestBox()
{
  try
  {
	fm.all('RiskCode').value = "";
	fm.all('RiskName').value = "";
	fm.all('bonusYear').value = tYear;
    fm.all('contYear').value = "";
    fm.all('accRate').value = "";
	  document.getElementById("bonusYear").readOnly=false;
	  document.getElementById("contYear").readOnly=false;
	  document.getElementById("contYearAfter").readOnly=false;
    
  }
  catch(ex)
  {
    alert("在LDBonusInterestRateInit.jsp-->BonusInterestBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
   try
   { 
    BonusInterestBox();
    initBonusInterestGrid(); 
    initElementtype();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}


function initBonusInterestGrid()
{
    var iArray = new Array();

      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="险种代码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="利率公布年度";
        iArray[2][1]="200px";
        iArray[2][2]=100;
        iArray[2][3]=0;

        iArray[3]=new Array();
        iArray[3][0]="保单承保年度";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="累积生息利率";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
      
        
      BonusInterestGrid = new MulLineEnter("fm" ,"BonusInterestGrid" ); 
      //这些属性必须在loadMulLine前
      BonusInterestGrid.mulLineCount = 10;   
      BonusInterestGrid.displayTitle = 1;
      BonusInterestGrid.locked = 1;
      BonusInterestGrid.canSel=1;
      BonusInterestGrid.canChk = 0;
      BonusInterestGrid.hiddenSubtraction=1;
      BonusInterestGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      BonusInterestGrid.selBoxEventFuncName="ShowDetail";
      BonusInterestGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert("在LDBonusInterestRateInit.jsp-->BonusInterestGrid函数中发生异常:初始化界面错误!");
      }
}

</script>
