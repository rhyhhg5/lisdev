<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2008-2-25 9:40
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdor.js"></SCRIPT>
	<SCRIPT src="./GEdorTypeUM.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="GEdorTypeUMInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./GEdorTypeUMSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common>
        <td class="title"> 受理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 批改类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
        	<input class = "readonly" readonly name="GrpContNo">
        </td>
    	</tr>
    </table>

    <Div id= "divPolInfo" style= "display: ''">
			<table>
			  <tr>
		      <td>
		      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
		      </td>
		      <td class= titleImg>
		         已满期保单列表
		      </td>
			  </tr>
			</table>
	    <Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
			  		<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span>
				  	</td>
					</tr>
				</table>			
	    </div>
    </DIV>
    <br>
        <Div id= "divInsuredInfo" style= "display: ''">
			<table>
			  <tr>
		      <td class= titleImg>
		         满期人员信息
		      </td>
			  </tr>
			</table>
    <table  class= common>
      <TR class= common>
        <TD class= title> 满期领取方式 </TD>
        <TD  class= input>
		<Input class="codeNo" name="MJType" value="A" CodeData="0|^A|一次性领取"  readOnly ondblclick="return showCodeListEx('MJType',[this,MJTypeName],[0,1]);" onkeyup="return showCodeListEx('MJType',[this,MJTypeName],[0,1]);"><Input class="codeName" value="一次性领取" name="MJTypeName"  readonly>
		 </TD>
		       </TR>
		             <TR class= common>
        <TD  class= title> 被保人号 </TD>
        <TD class=input>
          <Input class= "readonly" name=InsuredNo readonly >
        </TD>
                <TD  class= title> 被保人姓名 </TD>
        <TD class=input>
          <Input class= "readonly" name=InsuredName readonly >
        </TD>
      </TR>
      <TR class= common>
                      <TD  class= title> 个人万能帐户单位缴费余额 </TD>
        <TD class=input>
          <Input class= "readonly" name=GrpSum readonly >
        </TD>
                        <TD  class= title> 个人万能帐户个人缴费余额 </TD>
        <TD class=input>
          <Input class= "readonly" name=InsuredSum readonly >
        </TD>
      </TR>
    </table>
	</DIV>
    <br>
    <div>
	   <Input type=button name="save" class = cssButton value="保  存" onclick="edorTypeUMSave()">
		 <Input  type=Button name="goBack" class = cssButton value="返  回" onclick="edorTypeUMReturn()">
		</div>
		 <input type=hidden id="fmtransact" name="fmtransact">
		 <input type=hidden id="ContType" name="ContType">
		 <input type=hidden name="EdorAcceptNo">
		 <input type=hidden id="Sum" name="Sum">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
