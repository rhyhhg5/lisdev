//该文件中包含客户端需要处理的函数和事件
var showInfo;
var GEdorFlag = true;
var selGridFlag = 0;            //标识当前选中记录的GRID

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();           //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage3 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage4 = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//查询按钮
function queryClick()
{
	var sql = "select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday, " +
	     " a.IDType, a.IDNo, a.OccupationCode, a.OccupationType " +
	     "from LCInsured a, LCCont b " +
	     "where a.ContNo = b.ContNo " +
		   "and not exists (select ContNo from LPEdorItem " +
		   "    where ContNo = a.ContNo " +
		   "    and EdorType = '" + fm.EdorType.value + "' " +
		   "    and EdorNo = '" + fm.EdorNo.value + "')" +
		   "and b.AppFlag = '1' " +
		   "and a.GrpContNo = '" + fm.GrpContNo.value + "' " +
		   getWherePart('a.InsuredNo', 'InsuredNo1') +                  
		   getWherePart('a.Name', 'Name', 'like', '0') +
		   getWherePart('a.IdNo', 'IdNo') + 
		   "order by InsuredNo"; +
	turnPage.queryModal(sql, LCInsuredGrid);
	if (selGridFlag == 1)
	{
		selGridFlag = 0;
	}
}

//查询按钮
function queryClick2()
{
	var sql = "select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday, " +
	       " a.IDType, a.IDNo, a.OccupationCode,a.OccupationType, " +
	       " case b.EdorState when '3' then '未录入' else '已录入' end " +
	       "from LCInsured a, LPEdorItem b " +
	       "where a.ContNo = b.ContNo " +
	       "and b.EdorNo= '" + fm.EdorNo.value + "' " +
	       "and b.EdorType = '" + fm.EdorType.value + "' " +
			   getWherePart('a.InsuredNo', 'InsuredNo2') +
			   getWherePart('a.Name', 'Name2', 'like', '0') +
			   getWherePart('a.IdNo', 'IdNo2') + 
			   " order by a.InsuredNo";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, LCInsured2Grid);
	if (selGridFlag == 2)
	{
		selGridFlag = 0;
	}
}

//单击MultiLine的单选按钮时操作
function reportDetailClick(parm1, parm2)
{
  fm.ContNo.value = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 1);
  fm.InsuredNo.value = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 2);
	//queryGEdorList();
	selGridFlag = 1;
}

//添加保全
function addGEdor()
{
	if (selGridFlag == 0)
	{
		alert("请先选择一个被保人！");
		return;
	}
	if (selGridFlag == 1)
	{
		fm.all("Transact").value = "INSERT||EDOR";
	}
	else
	{
		fm.all("Transact").value = "";
	}
	if(!checkGUFlag()){
		
		alert("该保单是团险万能保单，且该人员已经进行过满期操作，不能做客户资料变更");
		return false;
	}
	fm.action = "./GEdorTypeDetailSubmit.jsp";
	fm.submit();
	
	fm.CustomerNoBak.value = fm.InsuredNo.value;
	fm.ContNoBak.value = fm.ContNo.value;
	window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp?TypeFlag=G");
}

//团险万能满期保单不可以做客户资料变更
function checkGUFlag(){
	
	var tSql = "select 1 from lcpol a where exists (select 1 from lmriskapp where riskcode=a.riskcode "
		 + " and riskprop='G' and risktype4='4') and contno='"+fm.ContNo.value+"' " +
		 		" and insuredno = '"+fm.InsuredNo.value+"' and polstate like ('0306%') fetch first 1 rows only with ur";
	var arrResult = easyExecSql(tSql);
	if(arrResult!= null){
		
		return false; 
	}
		
	return true;
}

//打开个人保全的明细界面
function openGEdorDetail()
{
	if (LCInsured2Grid.getSelNo() == 0)
	{
		alert("请先选择一个被保人！");
		return;
	}

	fm.CustomerNoBak.value = fm.InsuredNo.value;
	fm.ContNoBak.value = fm.ContNo.value;
	window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp?TypeFlag=G");
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result)
{  
  try
  {
    showInfo.close();
    window.focus();
  }
  catch(ex) {}
  
	if (FlagStr == "Fail")
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else
	{
	  queryClick();
		queryClick2();
		GEdorFlag = true;
		if (fm.Transact.value == "I&EDORITEM")
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
		else if (fm.Transact.value == "G&EDORITEM")
		{
		  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
			returnParent();
		}
	}
}

//处理获取焦点事件
function initFocus()
{
  if (GEdorFlag)
  {
		fm.EdorAcceptNo.value = top.opener.fm.EdorAcceptNo.value;
		GEdorFlag = false;
		queryClick();
		queryGEdorList();
		selGridFlag = 0;
  }
}

//查询出申请后的个人保全列表 已经作过保全项目的
function queryGEdorList()
{
	var strSql = "select ContNo, InsuredNo, Name, Sex, Birthday, IDType, IDNo from LcInsured where GrpContNo= '" + fm.all('GrpContNo').value + "' and ContNo in "
			   + " (select ContNo from LPEdorItem where edortype='" + fm.EdorType.value + "' and edorno='" + fm.EdorNo.value + "')"
			   + "order by InsuredNo";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCInsured2Grid);
	if (selGridFlag == 2)
	{
		selGridFlag = 0;
	}
}

//单击MultiLine的单选按钮时操作
function reportDetail2Click(parm1, parm2)
{
  fm.ContNo.value = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 1);
  fm.InsuredNo.value = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 2);
	selGridFlag = 2;
}

//撤销集体下个人保全
function cancelGEdor()
{
	if (LCInsured2Grid.getSelNo() == 0)
	{
		alert("请先选择一个被保人！");
		return;
	}
	fm.all("Transact").value = "I&EDORITEM"
	var strSql = "select MakeDate, MakeTime, EdorState,polno from LPEdorItem where EdorNo='" + fm.EdorNo.value + "' and EdorType = '" + fm.EdorType.value + "'";
	var arrResult = easyExecSql(strSql);     //执行SQL语句进行查询，返回二维数组

	if (arrResult != null)
	{
		var tMakeDate = arrResult[0][0];
		var tMakeTime = arrResult[0][1];
		var tEdorState = arrResult[0][2];
		var tInusredNo = fm.InsuredNo.value;
				
	  var tContNo = fm.ContNo.value;
		var tPolNo = arrResult[0][3];
		var tEdorAcceptNo = fm.EdorAcceptNo.value;
		var tEdorType  = fm.EdorType.value;
		fm.action = "./PEdorAppCancelSubmit.jsp?hEdorAcceptNo="+tEdorAcceptNo
		+"&ContNo="+tContNo+"&EdorType="+tEdorType+"&DelFlag=1&InsuredNo=" + tInusredNo + "&MakeDate=" + tMakeDate + "&MakeTime=" + tMakeTime + "&EdorState=" + tEdorState+"&PolNo="+tPolNo;
		fm.submit();
	}
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent() {
  try 
  {
    top.close();
  	top.opener.focus();
  	top.opener.getGrpEdorItem();
  }
  catch (ex) {}
}

function save()
{
	var sql = "select SerialNo " +
    "from LPDiskImport " +
    "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
    "and EdorNo = '" + fm.all("EdorNo").value + "' " +
    "and EdorType = '" + fm.all("EdorType").value + "' " +
    "and State = '1' " ;
	
	var arrResult = easyExecSql(sql);
  if (LCInsured2Grid.mulLineCount == 0) {
	  if(!arrResult){
		  alert("必须修改后或导入有效被保险人后才能保存申请！");
		    return;
	  }
  }else{
	  var sql = "select EdorState, InsuredNo from LPEdorItem " +
	  "where EdorNo = '" + fm.EdorNo.value + "' " +
	  "and EdorType = '" + fm.EdorType.value + "' ";
	  var result = easyExecSql(sql);
		for (i = 0; i < result.length; i++)
		{
			if (result[i][0] == "3")
			{
			alert("客户号" +　result[i][1] + "未录入保全明细！");
			return false;
			}
		}
  }
  var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.Transact.value = "G&EDORITEM"
	fm.action = "GEdorTypeDetailSave.jsp"
	fm.submit();
}

//磁盘导入 
function importInsured()
{
	if("CM"!=fm.all("EdorType").value){
		alert("非客户资料变更保全不能导入");
		return false;
	}
	var url = "./BqDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + 
	          "&EdorType=" + fm.all("EdorType").value +
	          "&GrpContNo=" + fm.all("GrpContNo").value;
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "客户资料变更导入联系电话", param);
}

//查询导入的数据
function queryImportData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Phone " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '1' " +
	             "order by Int(SerialNo) ";
	turnPage3.pageDivName = "divPage3";
	turnPage3.queryModal(sql, InsuredListGrid); 
}

//查询导入无效的数据
function queryFailData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Phone, ErrorReason " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '0' " +
	             "order by Int(SerialNo) ";
	turnPage4.pageDivName = "divPage4";
	turnPage4.queryModal(sql, FailGrid); 
}
