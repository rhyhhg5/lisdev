
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!--引入必要的java类  -->
<%@page import="com.sinosoft.lis.pubfun.*"%>	
<% 
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var operator = "<%=tGI.Operator%>"; //记录当前登录人
  var tCurrentDate = "<%=tCurrentDate%>";
  var tCurrentTime = "<%=tCurrentTime%>";
 
//输入框的初始化 
function initInpBox()
{
  try
  {
	fm.all('ManageCom2').value = manageCom;
	showAllCodeName();
  }
  catch(ex)
  {
    alert("在RiskUnlockInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();
    initLJAGetRiskGrid(); 
    initNoBankPrintGrid();
  }
  catch(re) 
  {
    alert("在RiskUnlockInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


var LJAGetRiskGrid;
function initLJAGetRiskGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=0;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="管理机构";      //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="保单号";      //列名
    iArray[2][1]="110px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[3]=new Array();
    iArray[3][0]="付费凭证号";      //列名
    iArray[3][1]="90px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="付费金额";      //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[5]=new Array();
    iArray[5][0]="帐号名";      //列名
    iArray[5][1]="70px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[6]=new Array();
    iArray[6][0]="转账银行";      //列名
    iArray[6][1]="120px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="提盘日期";      //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[8]=new Array();
    iArray[8][0]="提盘次数";      //列名
    iArray[8][1]="50px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    LJAGetRiskGrid = new MulLineEnter("fm", "LJAGetRiskGrid"); 
	  //设置Grid属性
    LJAGetRiskGrid.mulLineCount = 0;
    LJAGetRiskGrid.displayTitle = 1;
    LJAGetRiskGrid.locked = 1;
    LJAGetRiskGrid.canSel = 1;	
    //LJAGetRiskGrid.canChk = 0;
    LJAGetRiskGrid.hiddenSubtraction = 1;
    LJAGetRiskGrid.hiddenPlus = 1;
    LJAGetRiskGrid.selBoxEventFuncName = "querylj" ;
    LJAGetRiskGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在RiskUnlockInit.jsp-->LJAGetRiskGrid函数中发生异常:初始化界面错误!");
  }
}

var NoBankPrintGrid;
function initNoBankPrintGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=0;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="管理机构";      //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="保单号";      //列名
    iArray[2][1]="110px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[3]=new Array();
    iArray[3][0]="付费凭证号";      //列名
    iArray[3][1]="90px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="付费金额";      //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="提盘日期";      //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[6]=new Array();
    iArray[6][0]="提盘次数";      //列名
    iArray[6][1]="50px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    NoBankPrintGrid = new MulLineEnter("fm", "NoBankPrintGrid"); 
	  //设置Grid属性
    NoBankPrintGrid.mulLineCount = 0;
    NoBankPrintGrid.displayTitle = 1;
    NoBankPrintGrid.locked = 1;
    NoBankPrintGrid.canSel = 1;	
    NoBankPrintGrid.canChk = 0;
    NoBankPrintGrid.hiddenSubtraction = 1;
    NoBankPrintGrid.hiddenPlus = 1;
//    NoBankPrintGrid.selBoxEventFuncName = "selectOne3" ;
    NoBankPrintGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在RiskUnlockInit.jsp-->NoBankPrintGrid函数中发生异常:初始化界面错误!");
  }
}

</script>
