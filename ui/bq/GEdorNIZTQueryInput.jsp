
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>

<html>
<%
//程序名称：GEdorNIZTQueryInput.jsp
//程序功能：保单失效清单
//创建日期：2010-3-31
//创建人  ：WangHongLiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="GEdorNIZTQueryInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="GEdorNIZTQueryInit.jsp"%>
	</head>

	<body  onload="initForm();" >
  	<form action="./GEdorNIZTPrint.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">    	
      	<Table  class= common>
	    	<tr>
	    	    <td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName elementtype=nacessary>
				</td>
				<td class= title>保单号</td>
				<td class=input>
					<input class=common name=GrpContNo>
				</td>
				<td class= title>处理状态</td>
				<td class=input>				
					<Input class= "codeno" name=DealState CodeData="0|^0|请选择类型^1|审核中^2|处理完毕" ondblclick="return showCodeListEx('DealState',[this,DealStateName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('DealState',[this,DealStateName],[0,1],null,null,null,1);" ><Input class=codename name=DealStateName elementtype=nacessary  readonly  >
				</td>	    		  		
			</tr>
	    	<tr>
	    		<td class= title>申请起期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary verify="申请日期起期|NOTNULL">
				</td class=input>
				<td class= title>申请止期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary verify="申请日期止期|NOTNULL">
				</td>	
				<td class= title>保全项目</td>
				<td class=input>
				    <Input class= "codeno" name=EdorType verify="保全类型|NOTNULL" CodeData="0|^0|全部^1|增加被保险人^2|减少被保险人" ondblclick="return showCodeListEx('EdorType',[this,EdorTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('EdorType',[this,EdorTypeName],[0,1],null,null,null,1);" ><Input class=codename name=EdorTypeName elementtype=nacessary readonly >
				</td>				
	    	</tr>
		</Table>
      <table  class= common>
	  <input class=cssButton type=button value="查  询" onclick="easyQuery();">  
		<input class=cssButton type=button value="被保人清单下载" onclick="exportList();">
<%--	  <input class=cssButton type=button value="导出结果集" onclick="exportList();">  --%>
	  </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 申请清单
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanContPauseGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		  <Div  id= "divPage2" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
		 </Div>
  	</Div>
		
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divStatus);">
    		</td>
    		<td class= titleImg>
    			保全处理结果反馈
    		</td>
    	</tr>
    </table>	
	<Div  id= "divStatus" style= "display: ''">
      	<table  class= common>
       		<tr>
				<td class= title >处理结果</td>
				<td class=input>				
					<Input class= "codeno" name=DealResult CodeData="0|^0|请选择类型^1|成功^2|失败" ondblclick="return showCodeListEx('DealResult',[this,DealResultName],[0,1]);" onkeyup="return showCodeListKeyEx('DealResult',[this,DealResultName],[0,1]);" ><Input class=codename name=DealResultName >
				</td>	
			</tr>
		 </table>
  				
	<!-- 处理成功时，需要录入工单号 --> 
    <Div id="divEdorNo" style="display:none">
    <table class= common> 
      <TR  class= common>
          <TD  class= title >工单号码</TD>          
          <TD class=input>
          <input class=common name=EdorNo>
          </TD>     
      </TR>
    </table>
    </Div>
    
      	<table  class= common>
			<tr>    
				<td class= title >备注</td>
				<td class=input>
					<input class=common name=Remark style="width: '600px';height: '60px'">
				</td>		  		
			</tr>
    	</table>
    	<table  class= common>
	       <input class=cssButton type=button value="处理完毕" onclick="dealComplete();">  
	              
	    </table>
  	</Div>

		 
		<input type=hidden id="sql" name="sql" value="">
		<Input type=hidden name=PrintBatchNo>
		
		
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </form>
    <Form name=fmSaveAll action="./GedorNIZTSubmit.jsp" method=post target="fraSubmit">   
		 <Input type=hidden name=SubmitBatchNo>
	     <Input type=hidden name=SubmitGrpContNo>	
	     <Input type=hidden name=SubmitRemark>
	     <Input type=hidden name=SubmitEdorNo>	
	     <Input type=hidden name=SubmitDealResult>
  </Form>  
</body>
</html>
