<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
	//程序名称：BqUpdateGetdateInput.jsp
	//程序功能：保全应付日期修改
	//创建日期：20170426
	//创建人  ：ys
%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>

	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>
	"; //记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="BqUpdateGetdate.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="BqUpdateGetdateInit.jsp"%>
</head>
<body onload="initForm();">
	<form method=post name=fm target="fraSubmit"
		action="./BqUpdateGetdateChk.jsp">
		<div >
			<h5>保全应付日期修改</h5>
		</div>
		<table class=common >
			<TR class=common>
				<TD class=title >保全受理号:</TD>
				<TD class=input><Input class=common name=EdorAcceptNo value="">
			    <td></td>
			    <td></td>
				</TD>
			</TR>
		</table>
		<br>
		<div align="left">
			<INPUT id="query" VALUE="查  询" class=CssButton TYPE=button
				onclick="easyQueryClick();">
		</div>
		<br>
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor: hand;" OnClick="showPage(this,div1);"></td>
				<td class=titleImg>信息</td>
			</tr>
		</table>
		<Div id="div1" style="display: ''" align=center>
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1><span id="spanBQGrid">
					</span></td>
				</tr>
				<tr>
				<td>
				<INPUT VALUE="首  页" class=CssButton TYPE=button
				onclick="getFirstPage();"> <INPUT VALUE="上一页"
				class=CssButton TYPE=button onclick="getPreviousPage();"> <INPUT
				VALUE="下一页" class=CssButton TYPE=button onclick="getNextPage();">
			<INPUT VALUE="尾  页" class=CssButton TYPE=button
				onclick="getLastPage();">
				</td>
				</tr>
			
			</table>
		</div>

		<br/>
		<br/>
		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>修改前应付日期</td>
				<TD class=input><Input class="beforeShouldDate" readOnly=true
					name=beforeShouldDate></TD>
			<td></td>
			<td></td>
			</tr>

			<tr class=common>
				<td class=title>修改后应付日期</td>
				<td class=input><Input class="coolDatePicker" readOnly=true
					name=afterShouldDate dateFormat="short" verify="扫描时间|date">
				</td>
			<td></td>
				<td></td>
			</tr>
		</table>
		<br>
		<div align="left"><INPUT VALUE="确认修改" class="cssButton" TYPE=button
			onclick="updateShouldDate();"> <input type=hidden
			id="fmtransact" name="fmtransact"> <input type=hidden
			id="flag" name="flag" value="" >
		</div>
		
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
	<br>
	<br>
	<br>
	<p style="color: red;">此功能是针对保全应付日期错误的修改，
维护前请检查并务必作好数据备份。</p>
</body>
</html>