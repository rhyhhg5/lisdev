<%
//程序名称：GEdorTypeWTSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  String FlagStr = "";
  String Content = "";
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType");
  String grpContNo = request.getParameter("GrpContNo");
  
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  System.out.println("\n\n\n GEdorTypeWTSubmit.jsp " + tGI.Operator + " " 
    + PubFun.getCurrentDate() + " "
    + PubFun.getCurrentTime());
                
	//团体项目信息
	LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	tLPGrpEdorItemSchema.setEdorAcceptNo(edorNo);
  tLPGrpEdorItemSchema.setEdorNo(edorNo);
	tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
	tLPGrpEdorItemSchema.setEdorType(edorType);
	tLPGrpEdorItemSchema.setReasonCode(request.getParameter("reason_tb"));
	
	//团体保障下险种信息
	String[] tChk = request.getParameterValues("InpLCGrpPolGridChk");
	
	LPContPlanRiskSet tLPContPlanRiskSet = new LPContPlanRiskSet();
	if(tChk != null)
	{
	  String[] contPlansCode = request.getParameterValues("LCGrpPolGrid1");
	  String[] riskCodes = request.getParameterValues("LCGrpPolGrid3");
	    System.out.println(tChk.length);
	  for(int i = 0; i < tChk.length; i++)
	  {
	    System.out.println("tChk[i]:" + tChk[i]);
	    if(tChk[i].equals("1"))
	    {
        LPContPlanRiskSchema tLPContPlanRiskSchema = new LPContPlanRiskSchema();
        tLPContPlanRiskSchema.setGrpContNo(grpContNo);
        tLPContPlanRiskSchema.setContPlanCode(contPlansCode[i]);
        tLPContPlanRiskSchema.setRiskCode(riskCodes[i]);
        tLPContPlanRiskSet.add(tLPContPlanRiskSchema);
	    }
	  }
	}
	
	//体检费信息
	//String[] testChecks = request.getParameterValues("InpTestGridChk");
	//LPPENoticeSet tLPPENoticeSet = new LPPENoticeSet();
	//if(testChecks != null)
  //  {
  //  	String[] proposalContNos = request.getParameterValues("TestGrid1");
  //  	String[] prtSeqs = request.getParameterValues("TestGrid2");
  //  	String[] customers = request.getParameterValues("TestGrid3");
  //      
  //  	for(int i = 0; i < testChecks.length; i++)
  //  	{
  //  	    if(testChecks[i].equals("1"))
  //  	    {
  //  	        LPPENoticeSchema schema = new LPPENoticeSchema();
  //  	        schema.setEdorAcceptNo(edorNo);
  //  	        schema.setEdorNo(edorNo);
  //  	        schema.setEdorType(edorType);
  //  	        schema.setCustomerNo(customers[i]);
  //  	        schema.setProposalContNo(proposalContNos[i]);
  //  	        schema.setPrtSeq(prtSeqs[i]);
  //  	        tLPPENoticeSet.add(schema);
  //  	    }
  //  	}
	//}
	
	//工本费
	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = null;
	String costCheck = request.getParameter("costCheck");
	if(costCheck != null && costCheck.equals("1"))
	{
    tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
    tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
    tLPEdorEspecialDataSchema.setEdorNo(edorNo);
    tLPEdorEspecialDataSchema.setEdorType(edorType);
    tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_GB);
    tLPEdorEspecialDataSchema.setEdorValue(request.getParameter("cost"));
    tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
	}
	
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tLPGrpEdorItemSchema);
	tVData.add(tLPContPlanRiskSet);
	tVData.add(tLPEdorEspecialDataSchema);
	//tVData.add(tLPPENoticeSet);
	
  GrpEdorWTDetailUI tGrpEdorWTDetailUI   = new GrpEdorWTDetailUI();  
  if (!tGrpEdorWTDetailUI.submitData(tVData, ""))
	{
		Content = "保存失败，原因是:" + tGrpEdorWTDetailUI.mErrors.getFirstError();
		System.out.println(Content);
		FlagStr = "Fail";
	}
	else 
	{
		Content = "保存成功";
		System.out.println(Content);
		FlagStr = "Success";
	} 
    
    Content = PubFun.changForHTML(Content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","");
</script>
</html>

