<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：EdorUWManuHealth.jsp
//程序功能：保全人工核保体检资料录入
//创建日期：2006-2-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdorUWManuHealth.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./PEdorUWManuHealthInit.jsp"%>
  <title>保全体检资料录入</title>
</head>
<body onload="initForm();">
  <form method=post name=fm target="fraSubmit" action="./PEdorUWManuHealthSave.jsp">
    <input type="hidden" name="EdorNo">
    <input type="hidden" name="AppntNo">
    <input type="hidden" name="ContNo">
    <input type="hidden" name="InsuredNo">
  	<table>
      <tr>
        <td><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divInsured);"></td>
        <td class= titleImg>被保人信息</td>
        <td class= titleImg><!--input type=Button class=cssButton value="被保人明细" onclick="showInsuredInfo();"--></td>
      </tr>
    </table>
    <div id= "divInsured" style= "display: ''">
      <table class= common>
        <tr class= common>
        	<td text-align: left colSpan=1>
          	<span id="spanLCInsuredGrid" >
          	</span> 
        	</td>
        </tr>
      </table>
      <div id= "divPage" align=center style= "display: 'none' ">
        <input class=cssButton value="首  页" type=button onclick="turnPage.firstPage();"> 
        <input class=cssButton value="上一页" type=button onclick="turnPage.previousPage();"> 					
        <input class=cssButton value="下一页" type=button onclick="turnPage.nextPage();"> 
        <input class=cssButton value="尾  页" type=button onclick="turnPage.lastPage();"> 			
      </div>		
    </div>
    <table>
      <tr>
        <td><IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divBaseInfo);"></td>
        <td class= titleImg>体检基本信息</td>
      </tr>
    </table>
    <div id= "divBaseInfo" style= "display: ''">
      <table class= common>
      	<tr class= common>
      	  <td class= title>体检人</td>
      	  <td class= input>
      	    <Input class="readonly" readonly name="InsuredName">
      	  </td>
      	  <td class= title>体检日期</td>
      	  <td class= input>
      	    <Input class="readonly" readonly name="EDate"> 
      	  </td>
      	  <td class= title>体检状态</td>
      	  <td class= input>
      	    <input class="readonly" readonly name="PEState"> 
      	  </td>
      	</tr>
      	<tr class= common>
      	  <td class= title>体检医院</td>
      	  <td class= input>
      	    <Input class= 'codeNo' name=Hospital ondblclick="return showCodeList('lhhospitnamehtuw',[this,HospitName],[0,1],null,fm.ContNo.value,'managecom',1,350);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitnameht',[this,HospitName],[0,1],null,fm.ContNo.value,'HospitName',1,350)	; "><Input  class=codename  name=HospitName >
      	  </td>
      	  <td class= title>体检价格</td>
      	  <td class= input>
      	    <input class="readonly" readonly name="TestFee">
      	  </td>
      	  <td class= title>通知书号码</td>
      	  <td class= input>
      	    <input class="readonly" readonly name="PrtSeq">
      	  </td>
      	</tr>
      </table>
    </div>
    <table>
      <tr>
        <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divHealth);"></td>
        <td class= titleImg>体检项目录入</td>
      </tr>
    </table>
    <div id="divHealth" style="display: ''">
      <table class=common>
      	<tr class=common>
        	<td class= title>套餐</td>
           <td class= input>
            <Input class=code name=TestGrpCode readonly ondblclick="return showCodeList('testgrpname',[this,GrpTestCode],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('testgrpname',[this,GrpTestCode],[0,1]);">  </td>
            <input type= "hidden" class= Common name= GrpTestCode value= ""> 
          <td>
        </tr>
      </table>
      <table class= common>
        <tr class= common>
        	<td text-align: left colSpan=1>
          	<span id="spanHealthGrid" >
          	</span> 
        	</td>
        </tr>
      </table>
      <div id= "divPage2" align=center style= "display: 'none' ">
        <input class=cssButton value="首  页" type=button onclick="turnPage2.firstPage();"> 
        <input class=cssButton value="上一页" type=button onclick="turnPage2.previousPage();"> 					
        <input class=cssButton value="下一页" type=button onclick="turnPage2.nextPage();"> 
        <input class=cssButton value="尾  页" type=button onclick="turnPage2.lastPage();"> 			
      </div>		
    </div>
    <table class=common>
      <tr class= common>
       <td class= common>体检原因</td>
      </tr>
      <tr class= common>
       <td class= common>
         <textarea class="common" name="Note" cols="120" rows="3"  verify="体检原因|len<=100"></textarea>
       </td>
      </tr>
    </table>
    <div id="divAddPeItem" style="display: ''">
      <input type="button" name="sure" value="发送通知书" class=CssButton onclick="submitForm()">	
      <!--input type="button" name="sure" value="通知书打印" class=CssButton onclick="print()"-->	
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>