var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var mIdNo=null;
var mName=null;
var mSex=null;
var mBirthday=null;

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  return true;
}
function checkdate()
{
   var ResumeDate=fm.all('ResumeDate').value;
   var sql = "select max(startdate) from lcgrpcontstate where enddate is null and statetype='Stop' and grpcontno= '" + fm.all('GrpContNo').value + "' ";
   var arrResult = easyExecSql(sql);
   if (arrResult)
	{
		if(ResumeDate>=arrResult[0][0])
		{
		   return true;
		}
		else
		{
		  alert("恢复时间必须在本次暂停时间["+arrResult[0][0]+"]之后");
		  return false;
		}
	}
	alert("未查询到保单暂停信息");
	return false;
}
function edorTypeRRSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
  if (!checkdate())
  {
    return false;
  }
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmtransact').value = "INSERT||MAIN";
	fm.submit();

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content,Result )
{
	try{ showInfo.close(); } catch(e) {}
	window.focus();
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var tTransact=fm.all('fmtransact').value; 
		
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
			 
			returnParent();
	  
	}   
}     
                 
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}


function returnParent()
{
	try
	{
      top.opener.focus();
	  top.opener.getGrpEdorItem();
	}
	catch (ex) {}
	top.close();
}


