//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var addrFlag="NEW"

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function edorTypeACReturn()
{
		initForm();
}

function edorTypeACSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
	addrFlag = "MOD";
  fm.all('addrFlag').value=addrFlag;
  associateAlert();
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmAction').value = "INSERT||EDORAC";
  fm.submit();
}

function customerQuery()
{	
	window.open("./LCAppntGrpQuery.html");
}

//关联提示函数
function associateAlert()
{
  var strSql = "";
  var grpContno = fm.all('GrpContNo').value
  var Grpno = fm.all('GrpNo').value;
  strSql = "select GrpContno from lcgrpCont where grpContno <> '" + grpContno 
            + "' and AppntNo = '" + Grpno + "'"; 

  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
  
  //判断是否查询成功
  if (turnPage.strQueryResult) {
 		//查询成功则拆分字符串，返回二维数组
 	 turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
     var alertinfo = "相关联的集体保单如下：\n";
     for (var i = 0; i < turnPage.arrDataCacheSet.length; i++) {
     	alertinfo = alertinfo + turnPage.arrDataCacheSet[i][0] + "\n";
     }
  }	
  	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	parent.focus();
  	var tTransact=fm.all('fmAction').value;
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
  		//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
  		var tArr = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[15,17,18,19,20,21,22,23,24,25,26,27,28,29,42,43,44,45,46,47,48,49,50,30,31,32,33,34,35,36,37,38,39,40,41]);
	    fm.all('GrpNo').value = turnPage.arrDataCacheSet[0][0];
	    fm.all('GrpName').value = turnPage.arrDataCacheSet[0][1];
	    fm.all('GrpAddressCode').value = turnPage.arrDataCacheSet[0][2];
	    fm.all('GrpAddress').value = turnPage.arrDataCacheSet[0][3];
	    fm.all('GrpZipCode').value = turnPage.arrDataCacheSet[0][4];
	    fm.all('BusinessType').value = turnPage.arrDataCacheSet[0][5];
	    fm.all('GrpNature').value = turnPage.arrDataCacheSet[0][6];
//	    fm.all('Peoples2').value =turnPage.arrDataCacheSet[0][7];
//	    fm.all('RgtMoney').value = turnPage.arrDataCacheSet[0][8];
//	   	fm.all('Asset').value =turnPage.arrDataCacheSet[0][9];
//	    fm.all('NetProfitRate').value = turnPage.arrDataCacheSet[0][10];
//	    fm.all('MainBussiness').value = turnPage.arrDataCacheSet[0][11];
//	    fm.all('Corporation').value = turnPage.arrDataCacheSet[0][12];
//	    fm.all('ComAera').value =turnPage.arrDataCacheSet[0][13];
//	    fm.all('Fax').value = turnPage.arrDataCacheSet[0][14];
//	    fm.all('Phone').value = turnPage.arrDataCacheSet[0][15];
//	    fm.all('Satrap').value =turnPage.arrDataCacheSet[0][17];
//	    fm.all('EMail').value =turnPage.arrDataCacheSet[0][18];
//	    fm.all('FoundDate').value =turnPage.arrDataCacheSet[0][19];
	    fm.all('GetFlag').value = turnPage.arrDataCacheSet[0][16];
	    fm.all('BankAccNo').value =turnPage.arrDataCacheSet[0][20];
	    fm.all('BankCode').value =turnPage.arrDataCacheSet[0][21];
	    fm.all('GrpGroupNo').value =turnPage.arrDataCacheSet[0][22];
	    fm.all('LinkMan1').value =turnPage.arrDataCacheSet[0][23];
	    fm.all('Department1').value =turnPage.arrDataCacheSet[0][24];
	    fm.all('HeadShip1').value =turnPage.arrDataCacheSet[0][25];
	    fm.all('Phone1').value =turnPage.arrDataCacheSet[0][26];
	    fm.all('E_Mail1').value =turnPage.arrDataCacheSet[0][27];
	    fm.all('Fax1').value =turnPage.arrDataCacheSet[0][28];
	    fm.all('LinkMan2').value =turnPage.arrDataCacheSet[0][29];
	    fm.all('Department2').value =turnPage.arrDataCacheSet[0][30];
	    fm.all('HeadShip2').value =turnPage.arrDataCacheSet[0][31];
	    fm.all('Phone2').value =turnPage.arrDataCacheSet[0][32];
	    fm.all('E_Mail2').value =turnPage.arrDataCacheSet[0][33];
	    fm.all('Fax2').value =turnPage.arrDataCacheSet[0][34];
		}
		else
		{
		    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		   initForm();
		   
		   //进行关联提示	
		   //associateAlert();
	  }
  }
  returnParent();
}


function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="GetGrpAddressNo")
 {
     
     var strSQL;
     strSQL="select b.AddressNo,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Department1,b.HeadShip1,b.Phone1,b.E_Mail1,b.Fax1,b.LinkMan2,b.Department2,b.HeadShip2,b.Phone2,b.E_Mail2,b.Fax2 from LCGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
     arrResult=easyExecSql(strSQL);
     if (arrResult==null)
     {
        addrFlag="MOD";
        //alert("LP"+addrFlag);
        strSQL="select b.AddressNo,b.GrpAddress,b.GrpZipCode,b.LinkMan1,b.Department1,b.HeadShip1,b.Phone1,b.E_Mail1,b.Fax1,b.LinkMan2,b.Department2,b.HeadShip2,b.Phone2,b.E_Mail2,b.Fax2 from LPGrpAddress b where b.AddressNo='"+fm.GrpAddressNo.value+"' and b.CustomerNo='"+fm.GrpNo.value+"'";
        arrResult=easyExecSql(strSQL);
     }
    else
    {
        addrFlag="NEW";
        //alert("LC"+addrFlag);
    }
     try {fm.all('GrpAddressNo').value= arrResult[0][0]; } catch(ex) { };
     try {fm.all('GrpAddress').value= arrResult[0][1]; } catch(ex) { };
     try {fm.all('GrpZipCode').value= arrResult[0][2]; } catch(ex) { };
     try {fm.all('LinkMan1').value= arrResult[0][3]; } catch(ex) { };
     try {fm.all('Department1').value= arrResult[0][4]; } catch(ex) { };
     try {fm.all('HeadShip1').value= arrResult[0][5]; } catch(ex) { };
     try {fm.all('Phone1').value= arrResult[0][6]; } catch(ex) { };
     try {fm.all('E_Mail1').value= arrResult[0][7]; } catch(ex) { };
     try {fm.all('Fax1').value= arrResult[0][8]; } catch(ex) { };
     try {fm.all('LinkMan2').value= arrResult[0][9]; } catch(ex) { };
     try {fm.all('Department2').value= arrResult[0][10]; } catch(ex) { };
     try {fm.all('HeadShip2').value= arrResult[0][11]; } catch(ex) { };
     try {fm.all('Phone2').value= arrResult[0][12]; } catch(ex) { };
     try {fm.all('E_Mail2').value= arrResult[0][13]; } catch(ex) { };
     try {fm.all('Fax2').value= arrResult[0][14]; } catch(ex) { };
  }
}

function setAddr()
{
  if (fm.GrpAddressNo.value=='')
  {
    addrFlag="MOD";
    setReadOnly('Y');        
  }
}

function setReadOnly(flag)
{
  var tflag=flag
  if (flag=='Y')
  {
    try {fm.all('GrpAddress').readOnly= false; } catch(ex) { };
    try {fm.all('GrpZipCode').readOnly= false; } catch(ex) { };
    try {fm.all('LinkMan1').readOnly= false; } catch(ex) { };
    try {fm.all('Phone1').readOnly= false; } catch(ex) { };
    try {fm.all('E_Mail1').readOnly= false; } catch(ex) { };
    try {fm.all('Fax1').readOnly= false; } catch(ex) { };
    try {fm.all('LinkMan2').readOnly= false; } catch(ex) { };
    try {fm.all('Phone2').readOnly= false; } catch(ex) { };
    try {fm.all('E_Mail2').readOnly= false; } catch(ex) { };
    try {fm.all('Fax2').readOnly= false; } catch(ex) { };    
  }
  else
  {
    try {fm.all('GrpAddress').readOnly= true; } catch(ex) { };
    try {fm.all('GrpZipCode').readOnly= true; } catch(ex) { };
    try {fm.all('LinkMan1').readOnly= true; } catch(ex) { };
    try {fm.all('Phone1').readOnly= true; } catch(ex) { };
    try {fm.all('E_Mail1').readOnly= true; } catch(ex) { };
    try {fm.all('Fax1').readOnly= true; } catch(ex) { };
    try {fm.all('LinkMan2').readOnly= true; } catch(ex) { };
    try {fm.all('Phone2').readOnly= true; } catch(ex) { };
    try {fm.all('E_Mail2').readOnly= true; } catch(ex) { };
    try {fm.all('Fax2').readOnly= true; } catch(ex) { };    
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  
  if (Number(fm.Peoples.value) <= 0)
  {
    alert("员工总人数应大于0");
    fm.Peoples.focus();
    return false;
  }
  
  if (Number(fm.OnWorkPeoples.value) <= 0)
  {
    alert("在职人数应大于0");
    fm.OnWorkPeoples.focus();
    return false;
  }
  
  var total = Number(fm.OnWorkPeoples.value) + Number(fm.OffWorkPeoples.value) + Number(fm.OtherPeoples.value);
  if (fm.Peoples.value != total)
  {
    alert("员工总人数应为在职人数、退休人数、其他人员数之和！");
    fm.Peoples.focus();
    return false;
  }
  
  if (fm.Peoples.value == fm.OnWorkPeoples.value)
  {
    fm.OffWorkPeoples.value = 0;
    fm.OtherPeoples.value = 0;
  }
  if(""!=fm.LegalPersonIDNo.value && null!=fm.LegalPersonIDNo.value){
	  var strCheckLegalPersonIdNo=checkIdNo("0",fm.LegalPersonIDNo.value,"","");
		if ("" != strCheckLegalPersonIdNo && strCheckLegalPersonIdNo!=null)
		{
			alert(strCheckLegalPersonIdNo);
			return false;
		} 
  }
  if(fm.IDType.value == "0" || fm.IDType.value == "5"){
	  var strCheckIdNo=checkIdNo(fm.IDType.value,fm.IDNo.value,"","");
		if ("" != strCheckIdNo && strCheckIdNo!=null)
		{
			alert(strCheckIdNo);
			return false;
		}
  }
  //校验联系地址级联关系
  var checkCityResult = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province.value+"' "
	   +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County.value+"'))");
  if(checkCityResult == null || checkCityResult == ""){
	   alert("投保人联系地址级联关系不正确，请检查！");
	   return false;
  }
  if(!verifyPhone(fm.Phone.value,fm.Phone1.value,fm.Mobile1.value)){
	  return false;
  }
  var regEmail = /^[A-Za-z\d]+([-_.][A-Za-z\d]+)*@([A-Za-z\d]+[-.])+[A-Za-z\d]{2,5}$/;
  var email = fm.E_Mail1.value;
  if(email!=null && email != ''){
	  if(!regEmail.test(email)){
		  alert("联系人电子邮箱格式错误，请输入有效的e-mail地址！");
		  return  false;
	  }	  
  }
  return true;
}  
//校验电话
function verifyPhone(phone,phone1,mobile)
{
	if(mobile!=null&&mobile!="")
	{
		var regMo = /(^13\d{9}$)|(^14\d{9}$)|(^15\d{9}$)|(^17\d{9}$)|(^18\d{9}$)/;
		if(!regMo.test(mobile))
		{
		       alert("联系人手机号需为11位数字且前两位\n必须是13、14、15、17、18，请核查！");
		       return  false;
		}		
		
	}
	
	if(null!=phone&&""!=phone){
		if(phone.length < 7)
		{
			alert("投保团体联系电话不能少于7位，请核查！");
	   		return false;
		}
		if(phone.indexOf("1") == 0){
			var regMo = /(^13\d{9}$)|(^14\d{9}$)|(^15\d{9}$)|(^17\d{9}$)|(^18\d{9}$)/;
			if(!regMo.test(phone))
			{
				alert("投保团体联系电话是手机号需为11位数字且前两位\n必须是13、14、15、17、18，请核查！");
				return  false;
			}
		}else{
			var regPh1 = /^[\d-\(\)]+$/;
			if(!regPh1.test(phone)){
				alert("固定电话仅允许包含数字、括号和“-”！");
			    return  false;
			}
		}
		
	}
	if(null!=phone1&&""!=phone1){
		if(phone1.length < 7)
		{
			alert("投保人联系电话不能少于7位，请核查！");
	   		return false;
		}
		if(phone1.indexOf("1") == 0){
			var regMo = /(^13\d{9}$)|(^14\d{9}$)|(^15\d{9}$)|(^17\d{9}$)|(^18\d{9}$)/;
			if(!regMo.test(phone1))
			{
				alert("投保人联系电话是手机号需为11位数字且前两位\n必须是13、14、15、17、18，请核查！");
				return  false;
			}
		}else{
			var regPh1 = /^[\d-\(\)]+$/;
			if(!regPh1.test(phone1)){
				alert("固定电话仅允许包含数字、括号和“-”！");
				return  false;
			}			
		}
	}
	
	return true;
}        
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		/**
		alert("aa:"+arrResult[0][5]);
		fm.all('Nationality').value = arrResult[0][5];
		fm.all('Marriage').value=arrResult[0][6];
		fm.all('Stature').value=arrResult[0][7];
		fm.all('Avoirdupois').value=arrResult[0][8];
		fm.all('ICNo').value=arrResult[0][9];
		fm.all('HomeAddressCode').value=arrResult[0][10];
		fm.all('HomeAddress').value=arrResult[0][11];
		fm.all('PostalAddress').value=arrResult[0][12];
		fm.all('ZipCode').value=arrResult[0][13];
		fm.all('Phone').value=arrResult[0][14];
		fm.all('Mobile').value=arrResult[0][15];
		fm.all('EMail').value=arrResult[0][16];
		*/
		// 查询保单明细
		queryAppntGrpDetail();
	}
}
function queryAppntGrpDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tPolNo=fm.all('PolNo').value;
	//alert(tPolNo);
	tCustomerNo = fm.all('CustomerNo').value;
	//alert(tCustomerNo);
	//top.location.href = "./AppntGrpQueryDetail.jsp?EdorNo=" + tEdorNo+"&EdorType="+tEdorType+"&PolNo="+tPolNo+"&CustomerNo="+tCustomerNo;
	parent.fraInterface.fm.action = "./AppntGrpQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./GEdorTypeACSubmit.jsp";
}

function returnParent()
{
	try
	{
	  top.opener.focus();
		top.opener.getGrpEdorItem();
		top.close();
	}
	catch (ex)
	{
	}
}

/*********************************************************************
 *  查询投保单位信息
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function getGrpAppnt()
{
	var tEdorNo = fm.all("EdorNo").value;
	var tEdorType = fm.all("EdorType").value;
	var tGrpContNo = fm.all("GrpContNo").value;
	var strSQL;
	var arrResult;
	var tCustomerNo;
	var tAddressNo;
	//先查P表
	var lcgrpcontSQL = "select CustomerNo, AddressNo from LPGrpAppnt " +
    "where GrpContNo = '" + tGrpContNo + "' and edorno = '" + tEdorNo + "'";
	var arrResult = easyExecSql(lcgrpcontSQL, 1, 0);
	if(arrResult){
		tCustomerNo = arrResult[0][0];
		tAddressNo = arrResult[0][1];	
	}else{//P表为空再查C表
		//根据保单号得到客户号和地址
		strSQL = "select CustomerNo, AddressNo from LCGrpAppnt " +
		"where GrpContNo = '" + tGrpContNo + "'";
		arrResult = easyExecSql(strSQL, 1, 0);
		if (!arrResult)
		{
			alert("团体投保人信息查询失败！");
			return false;
		}
		tCustomerNo = arrResult[0][0];
		tAddressNo = arrResult[0][1];		
	}
	
	//先从P表中查出记录，如果没有过才从D表中查
	strSQL = "select GrpName, BusinessType, GrpNature, Peoples, OnWorkPeoples, " +
           "       OffWorkPeoples, OtherPeoples, Phone " +
           "from LPGrp " +
           "where EdorNo = '" + tEdorNo + "' " +
           "and EdorType = '" + tEdorType + "' " +
           "and CustomerNo = '" + tCustomerNo + "' ";
	arrResult = easyExecSql(strSQL, 1, 0);
	if (!arrResult)
	{	
		strSQL = "select GrpName, BusinessType, GrpNature, Peoples, OnWorkPeoples, " +
		         "       OffWorkPeoples, OtherPeoples, Phone " +
		         "from LDGrp " +
		         "where CustomerNo = '" + tCustomerNo + "' ";
		arrResult = easyExecSql(strSQL, 1, 0);
	}
	if (!arrResult)
	{
		alert("团体客户信息查询失败！");
		return false;
	}
  
	try {fm.all('GrpNo').value= tCustomerNo; } catch(ex) { }; 
	try {fm.all('GrpName').value= arrResult[0][0]; } catch(ex) { };     
	try {fm.all('BusinessType').value= arrResult[0][1];} catch(ex) { };        
	try {fm.all('GrpNature').value= arrResult[0][2]; } catch(ex) { };       
	try {fm.all('Peoples').value= arrResult[0][3]; } catch(ex) { };       
	try {fm.all('OnWorkPeoples').value= arrResult[0][4]; } catch(ex) { }; 
	try {fm.all('OffWorkPeoples').value= arrResult[0][5]; } catch(ex) { }; 
	try {fm.all('OtherPeoples').value= arrResult[0][6]; } catch(ex) { };                       
	try {fm.all('Phone').value= arrResult[0][7]; } catch(ex) { };  
  
	//查询地址信息
	strSQL = "select CustomerNo, AddressNo, GrpAddress, GrpZipCode, " +
		      "       LinkMan1, Phone1, Fax1, E_Mail1, Mobile1 " +
		      "from LPGrpAddress " +
		      "where EdorNo = '" + tEdorNo + "' " +
		      "and EdorType = '" + tEdorType + "' " +
		      "and AddressNo = '" + tAddressNo + "'" +
		      "and CustomerNo = '" + tCustomerNo + "' ";
	arrResult = easyExecSql(strSQL, 1, 0);
	if (!arrResult)
	{
		strSQL = "select CustomerNo, AddressNo, GrpAddress, GrpZipCode, " +
		        "       LinkMan1, Phone1, Fax1, E_Mail1, Mobile1 " +
		        "from LCGrpAddress " +
		        "where CustomerNo = '" + tCustomerNo + "' " +
		        "and AddressNo = '" + tAddressNo + "'";
		arrResult = easyExecSql(strSQL, 1, 0);
		if (!arrResult)
		{
			alert("未查到投保单位地址信息！");
			return fasle;
		}
	}
	try {fm.all('GrpNo').value= arrResult[0][0]; } catch(ex) { };
	try {fm.all('GrpAddressNo').value= arrResult[0][1]; } catch(ex) { };
	try {fm.all('GrpAddress').value= arrResult[0][2]; } catch(ex) { };
	try {fm.all('GrpZipCode').value= arrResult[0][3]; } catch(ex) { };
	try {fm.all('LinkMan1').value= arrResult[0][4]; } catch(ex) { };
	try {fm.all('Phone1').value= arrResult[0][5]; } catch(ex) { };
	try {fm.all('Fax1').value= arrResult[0][6]; } catch(ex) { };
	try {fm.all('E_Mail1').value= arrResult[0][7]; } catch(ex) { };
	try {fm.all('Mobile1').value= arrResult[0][8]; } catch(ex) { };
	
	//新增省市县地址反显
	//先查P表
	var lpgrpaddressSql = "select PostalProvince,PostalCity,PostalCounty,DetailAddress from LPGrpAddress " 
					 + "where EdorNo = '" + tEdorNo + "' " 
					 + "and EdorType = '" + tEdorType + "' " 
					 + "and AddressNo = '" + tAddressNo + "'"
					 + "and CustomerNo = '" + tCustomerNo + "' ";
	var addressResult = easyExecSql(lpgrpaddressSql, 1, 0);
	if(addressResult == "" || addressResult == null){//P表为空查C表
		var lcgrpaddressSql = "select PostalProvince,PostalCity,PostalCounty,DetailAddress from LCGrpAddress " 
			 + "where CustomerNo = '" + tCustomerNo + "' "
			 + "and AddressNo = '" + tAddressNo + "'";
		addressResult = easyExecSql(lcgrpaddressSql, 1, 0);
	}
	var postalProvinceSql = "select CodeName from LDCode1 where codetype = 'province1' and code = '" + addressResult[0][0] + "'";
	var postalCitySql = "select CodeName from LDCode1 where codetype = 'city1' and code = '" + addressResult[0][1] + "'";
	var postalCountySql = "select CodeName from LDCode1 where codetype = 'county1' and code = '" + addressResult[0][2] + "'";
	var postalProvince = easyExecSql(postalProvinceSql, 1, 0);
	var postalCity = easyExecSql(postalCitySql, 1, 0);
	var postalCounty = easyExecSql(postalCountySql, 1, 0);
	var detailAddress = addressResult[0][3];
	if(postalProvince != null){
			fm.all('Province').value = addressResult[0][0];
			fm.all('appnt_PostalProvince').value = postalProvince;
	}
	if(postalCity != null){
		fm.all('City').value = addressResult[0][1];
		fm.all('appnt_PostalCity').value = postalCity;
	}
	if(postalCounty != null){
		fm.all('County').value = addressResult[0][2];
		fm.all('appnt_PostalCounty').value = postalCounty;
	}
	if(detailAddress != null){
		fm.appnt_DetailAdress.value = detailAddress;
	}
	//查询联系人相关信息
	strSQL = "select IDtype, IDNo, IDStartDate, IDEndDate, IDLongEffFlag " +
		  	"from LPGrpAppnt " +
		  	"where " +
			"EdorNo = '" + tEdorNo + "' " +
			" and GrpContNo = '" + tGrpContNo + "'";
	arrResult = easyExecSql(strSQL, 1, 0);
	if (!arrResult)
	{
		strSQL = "select IDtype, IDNo, IDStartDate, IDEndDate, IDLongEffFlag " +
	  			"from LCGrpAppnt " +
	  			"where GrpContNo = '" + tGrpContNo + "' order by addressno desc";
		arrResult = easyExecSql(strSQL, 1, 0);
		if (!arrResult)
		{
		alert("未查到联系人信息");
		return fasle;
		}
	}
	
	if(arrResult[0][4] != null && arrResult[0][4] != "" && arrResult[0][4] != "null" && arrResult[0][4]=="Y"){
		fm.IdNoValidity.checked = true;
		fm.IDLongEffFlag.value = arrResult[0][4];
	}
	
	try {fm.all('IDtype').value= arrResult[0][0]; } catch(ex) { };
	try {fm.all('IDNo').value= arrResult[0][1]; } catch(ex) { };
	try {fm.all('IDStartDate').value= arrResult[0][2]; } catch(ex) { };
	try {fm.all('IDEndDate').value= arrResult[0][3]; } catch(ex) { };
	//try {fm.all('IDLongEffFlag').value= arrResult[0][4]; } catch(ex) { };
	
  
   //新增查询法人,联系人相关信息
	strSQL = "select TaxNo,LegalPersonName,LegalPersonIDNo,CustomerNo " +
			"from LPGrpAppnt " +
			"where " +
			"EdorNo = '" + tEdorNo + "' " +
			" and GrpContNo = '" + tGrpContNo + "'";
	arrResult = easyExecSql(strSQL, 1, 0);
	if (!arrResult)
	{
		strSQL = "select TaxNo,LegalPersonName,LegalPersonIDNo,CustomerNo " +
				"from LCGrpAppnt " +
				"where GrpContNo = '" + tGrpContNo + "' order by addressno desc";
		arrResult = easyExecSql(strSQL, 1, 0);
		if (!arrResult)
		{
			alert("未查到法人信息！");
			return fasle;
		}
	}
	
	//  try {fm.all('OrganComCode').value= arrResult[0][0]; } catch(ex) { };
	try {fm.all('TaxNo').value= arrResult[0][0]; } catch(ex) { };
	try {fm.all('LegalPersonName').value= arrResult[0][1]; } catch(ex) { };
	try {fm.all('LegalPersonIDNo').value= arrResult[0][2]; } catch(ex) { };
		
  
}

function setIDLongEffFlag() {

	if (fm.IdNoValidity.checked == true) {
		fm.IDLongEffFlag.value = "Y";
	} else {
		fm.IDLongEffFlag.value = "";
	}

}


/*******************************************************************************
 * 查询地址信息 参数 ： 查询返回的二维数组 返回值： 无
 * ********************************************************************
 */
function getaddresscodedata()
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select AddressNo,GrpAddress from LCGrpAddress where CustomerNo ='"+fm.GrpNo.value+"'"
    + "union select AddressNo,GrpAddress from LpGrpAddress where CustomerNo ='"+fm.GrpNo.value+"' + and EdorNo='"+fm.EdorNo.value+"'";
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    	}
    }
    fm.all("GrpAddressNo").CodeData=tCodeData;
}
function chagePostalAddress(){
	//当省级单位下没有市县级单位或市级单位下没有县级单位的时候改变拼接完整地址的方式
   if(fm.appnt_PostalCity.value == '空' && fm.appnt_PostalCounty.value == '空'){
	   fm.GrpAddress.value=fm.appnt_PostalProvince.value+fm.appnt_DetailAdress.value;
   }else if(fm.appnt_PostalCity.value != '空' && fm.appnt_PostalCounty.value == '空') {
	   fm.GrpAddress.value=fm.appnt_PostalProvince.value+fm.appnt_PostalCity.value+fm.appnt_DetailAdress.value;
   }else if(fm.appnt_PostalCity.value == '空') {
	   fm.GrpAddress.value=fm.appnt_PostalProvince.value+fm.appnt_DetailAdress.value;
   }else{
	   fm.GrpAddress.value=fm.appnt_PostalProvince.value+fm.appnt_PostalCity.value+fm.appnt_PostalCounty.value+fm.appnt_DetailAdress.value;
   }
}
function changeAddress(num){
	if(num==1){
		fm.City.value = "";
		fm.appnt_PostalCity.value = "";
		fm.County.value = "";
		fm.appnt_PostalCounty.value = "";
		fm.appnt_DetailAdress.value = "";		
	}
	if(num==2){
		fm.County.value = "";
		fm.appnt_PostalCounty.value = "";
		fm.appnt_DetailAdress.value = "";		
	}
	if(num==3){
		fm.appnt_DetailAdress.value = "";		
	}
}