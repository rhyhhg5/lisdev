//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();
turnPage.pageLineNum = 20;       //根据需求，显示20行
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var theFirstValue="";
var theSecondValue="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示

function printListing()
{
  if(GetModeGrid.mulLineCount == 0)
  {
    alert("请先查询");
    return false;
  }
  
  if( fm.tempSQL.value == "" )
  {
  	alert("数据错误，请重新查询");
    return false;
  }  

  fm.action="./GrpuliqueryListPrint.jsp";
  fm.target="f1print";
//  fm.target = "_top";
  fm.fmtransact.value="PRINT";
  fm.submit(); //提交
}


function EasyExecSql()
{
	if( verifyInput2() == false ){
         return false;
    }
	
	var strSQL = "select grpcontno,contno" 
		+	",InsuredNo,InsuredName,(select db2inst1.codename('sex',InsuredSex) from dual),InsuredBirthday" 
		+	",nvl((select GetYear"
        +   "     from lcinsuredlist"
        +   "   where '1502704968000' = '1502704968000'"
        +   "      and grpcontno = a.grpcontno"
        +   "      and insuredno = a.insuredno" 
        +   "      union"
        +   "      select GetYear"
        +   "        from lbinsuredlist"
        +   "       where grpcontno = a.grpcontno"
        +   "         and insuredno = a.insuredno fetch first 1 rows only),"
        +   "  0)"
		+	",(select getstartdate from lcget where polno=a.polno and getdutykind is not null and livegettype='0' fetch first 1 rows only)" 
		+	",'满期未领取' from lcpol a where  conttype='2' and appflag='1' and stateflag = '1'  and poltypeflag<>'2'  and polstate ='03060001' " 
		+	getWherePart('a.Grpcontno', 'Grpcontno','=')
		+	"and a.manageCom like "
		+	"'" + document.getElementById("manageCom").value + "%'"
		+	" and exists ( select 1 from lcget where polno=a.polno and getdutykind is not null and livegettype='0')" 
		+	"and exists (select 1 from LMRiskApp where risktype4='4' and riskprop ='G' and riskcode = a.riskCode) order by a.Grpcontno with ur";
  fm.tempSQL.value =strSQL;
  turnPage.queryModal(strSQL, GetModeGrid);
  if(GetModeGrid.mulLineCount == 0)
	{
	    alert("没有查询到保单信息！");
	}     
}

function queryAll()
{
	initGrpContGrid();
	var sql = "select grpcontno,contno,insuredname,cvalidate,cinvalidate from lccont where grpcontno ='"+fm.tGrpContNo.value+"' with ur ";
	turnPage2.queryModal(sql, GrpContGrid);
	return false ;
}

function save()
{
	if (GrpContGrid.mulLineCount ==0)
	{
		alert("没有保单明细信息，不能修改！");
		return false;
	}	
	
	if (!CheckData(fm.tCinvaliDate.value))
	{
		return false ;
	}
	
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}






function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}



//去空格
function trim(s)
{
  var l = s.replace( /^\s*/, ""); //去左空格
  var r = l.replace( /\l*$/, ""); //去右空格
  return r;
}