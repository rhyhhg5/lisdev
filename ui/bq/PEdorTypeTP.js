// 该文件中包含客户端(协议退保)需要处理的函数和事件

var showInfo;
var mDebug="1";
var addrFlag="NEW"
var turnPage = new turnPageClass();//使用翻页功能，必须建立为全局变量
var hasULI = false;

//初始化页面

function edorTypeXTReturn()
{
	initForm();

}

//实现明细保存的功能
function edorTypeTPSave()
{
	if (!verifyInput2())
  {
    return false;
  }
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmAction').value = "INSERT||EDORTP";
	fm.submit();
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.all('addrFlag').value=addrFlag;
	
	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit(); //提交
	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content)
{
	showInfo.close();
	top.focus();
	
	if(FlagStr == "Success")
	{
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
        
        top.opener.focus();
        top.opener.getEdorItem();
        top.close();
	}
    else
    {
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    }
    
}

//提交前的校验、计算

function afterCodeSelect( cCodeName, Field )
{
}

//提交前的校验、计算  

function beforeSubmit()
{
  //添加操作
}           
       
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";  
	}
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
}

/*********************************************************************
 *  改变退费文本框中的值
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
function changeValue1()
{    	
	/*
	if(!isNumber(this.value)){
		alert('请输入数字！');
		this.foucs();
		this.select();
		return;
	}*/
	var a=parseFloat(fm.all('GetMoneyNum').value);
	var b=parseFloat(fm.all('Prem').value);

	fm.all('GetMoneyPro').value=mathRound(a/b*100);   
}

function checknum(){
	if(!checkNumber(GetMoneyNum))
	return;
}
/*********************************************************************
 *  改变退费（按比例）文本框中的值
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
function changeValue2()
{ 
	/*   if(!isNumber(this.value)){
	alert('请输入数字！');
	this.foucs();
	this.select();
	return;
	}      
	*/
	var a=parseFloat(fm.all('GetMoneyPro').value);
	var b=parseFloat(fm.all('Prem').value);
	fm.all('GetMoneyNum').value=mathRound(a*b/100);
       	 
}

//返回主页面

function returnParent()
{
	top.opener.getEdorItem();
	top.opener.focus();
	top.close();
}


function getPolInfo(tContNo)
{  
    // 书写SQL语句
    var strSQL = "  select a.insuredname, a.insuredno, b.riskName,a.riskCode, "
                 + "  a.amnt, a.prem, a.cValiDate,date(a.EndDate) - 1 day,a.payToDate,a.endDate,a.payendyear||a.payendyearflag, " 
                 +"  (select codename from ldcode where codetype ='payintv' and code =a.payintv) "
                 + " from LCPol a, LMRiskApp b "
                 + " where a.riskCode = b.riskCode "
                 + "   and contNo = '" + tContNo + "' "
                 + " order by riskSeqNo ";
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
    
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象

    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
    
}


//得到保全生效日期
function getEdorValiDate()
{
  var sql = "  select edorValiDate "
            + "from LPEdorItem "
            + "where edorNo = '" + fm.EdorNo.value + "' "
            + "   and edorType = '" + fm.EdorType.value + "' "
            + "   and contNo = '" + fm.ContNo.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    return result[0][0];
  }
  return "";
}


//规格化为两位小数，4舍5入
function pointTwoNew( s )
{
  var r = Math.round( parseFloat(s) * 100 )/100;
  var v = r.toString();
  var len = v.length;
  var index = v.indexOf(".");

  if( index==-1 )
  {
    v = v + ".00";
    return v;
  }
  else
  {
    if( len-index==3 )
    {
      return v;
    }
    else if( len-index==2 )
    {
      v = v +"0";
      return v;
    }
    else if( len-index==1 )
    {
      v = v + "00";
      return v;
    }
    else
    {
      return v.substring(0,index+3);
    }
  }
}

//初始化已存在页面数据
function initInpBox()
{ 
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  var flag;
  try
  {
  	flag = top.opener.fm.all('loadFlagForItem').value;
  }
  catch(ex)
  {
  	flag = "";	
  }
  
  if(flag == "TASK")
  {
  	fm.save1.style.display = "none";
  
  	fm.goBack.style.display = "none";

  }
  	
	try
	{    
		
		fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
		fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
		fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
		fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
		fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
		fm.all('ContType').value ='2';
		
		try {fm.all('EdorValiDate').value= getEdorValiDate(); } catch(ex) { }; //总保额
		
		showOneCodeName("EdorCode", "EdorTypeName"); 
	}
	catch(ex)
	{
		alert("在PEdorTypeXTInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}   
	
}   
    