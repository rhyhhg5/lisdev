<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<% 
//程序名称：GEdorBudgetSave.jsp
//程序功能：退保试算
//创建日期：2006-04-19
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String flag = "Succ";
  String content = "退保试算成功";
  String remark = "无";
  LCPolDB tLCPolDB = new LCPolDB();
  LCPolSet tLCPolSet = null;
  long startTime = System.currentTimeMillis();
  
  GlobalInput tGI = (GlobalInput)session.getValue("GI");
  
  String grpContNo = request.getParameter("grpContNo");
  String edorValiDate = request.getParameter("edorValiDate");
  if(edorValiDate == null || edorValiDate.equals("") || edorValiDate.equals("null"))
  {
    edorValiDate = request.getParameter("edorValiDatePassIn");
  }
  String payToDateLongPol = request.getParameter("payToDateLongPol");
  String edorType = request.getParameter("edorType");
  String edorNo = request.getParameter("edorNo");
  
  boolean needResultFlag = (edorNo != null && !edorNo.equals("") 
                            && edorType != null && !edorType.equals(""));
  EdorCTTestBL tEdorCTTestBL = new EdorCTTestBL();
  tEdorCTTestBL.setEdorValiDate(edorValiDate);
  tEdorCTTestBL.setCurPayToDateLongPol(payToDateLongPol);
  tEdorCTTestBL.setGrpContNo(grpContNo);
  tEdorCTTestBL.setOperator(tGI.Operator);
  System.out.println("\n\n\n\n\n\n\n\n" + edorValiDate + " "
    + payToDateLongPol + " " + grpContNo);
  
  String tChecks[] = request.getParameterValues("InpLCGrpPolGridChk");
  if(tChecks != null)
  {
    String[] contPlanCodes = request.getParameterValues("LCGrpPolGrid1");
    String[] riskCodes = request.getParameterValues("LCGrpPolGrid3");
    LPBudgetResultSet tLPBudgetResultSet = new LPBudgetResultSet();
    LGErrorLogSet tLGErrorLogSet = new LGErrorLogSet();
    for(int i = 0; i < tChecks.length; i++)
    {
      if (!tChecks[i].equals("1"))
      {
        continue;
      }
      VData tVData = tEdorCTTestBL.budget(contPlanCodes[i], riskCodes[i], needResultFlag);
      if(tVData == null && tEdorCTTestBL.mErrors.needDealError())
      {
        flag = "Fail";
        content = tEdorCTTestBL.mErrors.getErrContent();
        remark += content;
      }
      else
      {
        double getMoney = Double.parseDouble((String)tVData
                           .getObjectByObjectName("String", 0));
        if(getMoney != 0)
        {
          getMoney = -getMoney;
        }
        tLPBudgetResultSet.add((LPBudgetResultSet)tVData.getObjectByObjectName("LPBudgetResultSet", 0));
  			//qulq 061205 
        tLGErrorLogSet.add((LGErrorLogSet)tVData.getObjectByObjectName("LGErrorLogSet", 0));
        if(tLGErrorLogSet.size()>0)
        {
        	remark ="";
        	for(int j=1;j<=tLGErrorLogSet.size();j++)
        	{
        		remark +=tLGErrorLogSet.get(j).getDescribe();
        	}
        	System.out.println(remark);
        }
  %>      
  <script language="javascript">
        parent.fraInterface.LCGrpPolGrid.setRowColDataByName("<%=i%>", "getMoney", "<%=getMoney%>");
        parent.fraInterface.LCGrpPolGrid.setRowColDataByName("<%=i%>", "remark", "<%=remark%>"); 
  </script>
<%     
      }
      
       
      if(flag.equals("Fail"))
      {
        break;
      }
    }
    if(needResultFlag)
    {
      for(int i = 1; i <= tLPBudgetResultSet.size(); i++)
      {
        tLPBudgetResultSet.get(i).setEdorNo(edorNo);
        tLPBudgetResultSet.get(i).setEdorType(edorType);
        tLPBudgetResultSet.get(i).setOperator(tGI.Operator);
      }
      MMap map = new MMap();
      map.put("delete from LPBudgetResult where edorNo = '" + edorNo 
        + "' and edorType = '" + edorType 
        + "' and grpContNo = '" + grpContNo + "' ", "DELETE");
      map.put(tLPBudgetResultSet, "INSERT");
      
      VData data = new VData();
      data.add(map);
      PubSubmit p = new PubSubmit();
      if(!p.submitData(data, ""))
      {
        flag = "Fail";
        content += ", 存储试算结果出错。";
      }
    }
  }
  System.out.println(System.currentTimeMillis() - startTime);
  content = PubFun.changForHTML(content);
  
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>

