var turnPage = new turnPageClass();      
var turnPage2 = new turnPageClass();      

//查询导入的数据
function queryImportData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Money " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '1' " +
	             "order by Int(SerialNo) ";
	turnPage.queryModal(sql, InsuredListGrid); 
}

//查询导入无效的数据
function queryFailData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Money, ErrorReason " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '0' " +
	             "order by Int(SerialNo) ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, FailGrid); 
}

//磁盘导入 
function importInsured()
{
	var url = "./BqDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + 
	          "&EdorType=" + fm.all("EdorType").value +
	          "&GrpContNo=" + fm.all("GrpContNo").value;
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "特需帐户资金导入", param);
}

function initContInfo(){
	var str1="select cvalidate from lcgrpcont where grpcontno='"+fm.all("GrpContNo").value+"'";
	arrRes1 = easyExecSql(str1,1,0);
	try{fm.all('CValiDate').value= arrRes1[0][0]; }catch(ex){};  //保单生效日期
	
	var str2="select insuAccBala from LCPol a, LCInsureAcc b " +
                "where a.PolNo = b.PolNo " +
                "and a.GrpContNo = b.GrpContNo " +
                "and a.PolTypeFlag = '2' "+ 
                " and a.acctype ='1' "+ 
                " and b.InsuAccNo =(select distinct c.InsuAccNo from LMRiskInsuAcc c, LMRiskToAcc d  " +
			    "	 where c.InsuAccNo = d.InsuAccNo and c.AccType = '001' and d.riskCode = a.riskcode ) " +
                "and a.grpContNo = '" + fm.all("GrpContNo").value + "' ";
	arrRes2 = easyExecSql(str2,1,0);
	try{fm.all('GrpAccMoney').value= arrRes2[0][0]; }catch(ex){};//团体公共保额  
	
	var str3="select sum(insuAccBala) from LCPol a, LCInsureAcc b " +
			    "where a.polNo = b.polNo " + 
			    "and a.grpContNo = b.grpContNo " + 
			    "and a.polTypeFlag != '2' " +
			    "and InsuAccNo =(select distinct c.InsuAccNo from LMRiskInsuAcc c, LMRiskToAcc d  " +
			    "	 where c.InsuAccNo = d.InsuAccNo and c.AccType = '002' and d.riskCode = a.riskcode ) " +
			    "and a.GrpContNo = '" + fm.all("GrpContNo").value + "' ";
	arrRes3 = easyExecSql(str3,1,0);
	try{fm.all('AccMoney').value= arrRes3[0][0]; }catch(ex){};//个人保额  
	
	var str4="select Remark from lcgrpcont where grpcontno='"+ fm.all("GrpContNo").value +"' ";
	arrRes4 = easyExecSql(str4,1,0);
	try{fm.all('Remark').value= arrRes4[0][0]; }catch(ex){};//特别约定  
	
}

//数据提交后的操作
function afterSubmit(flag, content)
{
  try 
  { 
    showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	
	if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		returnParent();
	}
}

//保存
function save()
{
  if (InsuredListGrid.mulLineCount == 0)
	{
		alert("没有有效的导入数据！");
		return false;
	}
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "GEdorTypeGDSubmit.jsp"
	fm.submit();
}

//function insuredList()
//{
//  window.open("../sys/GrpInsuredInput.jsp?ContNo="+fm.all("GrpContNo").value+"&ModeFlag=0&ContLoadFlag=2&ContType=1");
//}

//返回父窗口
function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}