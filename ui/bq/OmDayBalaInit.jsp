<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
//添加页面控件的初始化。
  GlobalInput tGI = (GlobalInput)session.getValue("GI");	
  if(tGI == null) 
  {
    out.println("session has expired");
    return;
  }
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>

<script language="JavaScript">
function initInpBox()
{ 
  try
  {  
    var sql = "select Current Date - 1 month from dual ";    
    fm.all('StartDate').value = easyExecSql(sql);
    sql = "select Current Date from dual ";
    fm.all('EndDate').value = easyExecSql(sql);
    fm.all('ManageCom').value = <%=tGI.ManageCom%>;
    fm.all('RiskCode').value = '';
  }
  catch(ex)
  {
    alert("在OmPayBalaInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initOmDayGrid(); 
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("OmPayBalaInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//Mulline的初始化
function initOmDayGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=0;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="机构编码";      //列名
    iArray[1][1]="60px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
   
    iArray[2]=new Array();
    iArray[2][0]="管理机构";      //列名
    iArray[2][1]="100px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;   
    
    iArray[3]=new Array();
    iArray[3][0]="险种代码";      //列名
    iArray[3][1]="50px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[4]=new Array();
    iArray[4][0]="险种名称";      //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[5]=new Array();
    iArray[5][0]="保费收入";      //列名
    iArray[5][1]="60px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="BF"; 
    
    iArray[6]=new Array();
    iArray[6][0]="持续奖金";      //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="保单利息";      //列名
    iArray[7][1]="60px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="初始费用";      //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[9]=new Array();
    iArray[9][0]="犹豫期退保";      //列名
    iArray[9][1]="60px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[10]=new Array();
    iArray[10][0]="退保金";      //列名
    iArray[10][1]="60px";        //列宽
    iArray[10][2]=200;           //列最大值
    iArray[10][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="退保费用";      //列名
    iArray[11][1]="60px";        //列宽
    iArray[11][2]=200;           //列最大值
    iArray[11][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[12]=new Array();
    iArray[12][0]="保单管理费";      //列名
    iArray[12][1]="60px";        //列宽
    iArray[12][2]=200;           //列最大值
    iArray[12][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[13]=new Array();
    iArray[13][0]="部分领取";      //列名
    iArray[13][1]="60px";        //列宽
    iArray[13][2]=200;           //列最大值
    iArray[13][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[14]=new Array();
    iArray[14][0]="重疾保险金";      //列名
    iArray[14][1]="60px";        //列宽
    iArray[14][2]=200;           //列最大值
    iArray[14][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[15]=new Array();
    iArray[15][0]="身故保险金";      //列名
    iArray[15][1]="60px";        //列宽
    iArray[15][2]=200;           //列最大值
    iArray[15][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[16]=new Array();
    iArray[16][0]="可转投资净额";      //列名
    iArray[16][1]="80px";        //列宽
    iArray[16][2]=200;           //列最大值
    iArray[16][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    OmDayGrid = new MulLineEnter("fm", "OmDayGrid"); 
	//设置Grid属性
    OmDayGrid.mulLineCount = 0;
    OmDayGrid.displayTitle = 1;
    OmDayGrid.locked = 1;
    OmDayGrid.canSel = 0;	
    OmDayGrid.canChk = 0;
    OmDayGrid.locked = 1;
    OmDayGrid.hiddenSubtraction = 1;
    OmDayGrid.hiddenPlus = 1;
    OmDayGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在OmDayBalaInit.jsp-->initOmDayGrid函数中发生异常:初始化界面错误!");
  }
}
</script>