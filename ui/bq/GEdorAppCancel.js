//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 
var turnPageItem = new turnPageClass(); 
var arrDataSet2 = new Array();
var itemMainNo="0";
var mainState="0";



var cEdorNo="";
var cGrpContNo="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
function easyQueryClick()
{          
	// 初始化表格
	
	
	// 书写SQL语句
	var strSQL = "";
	
	/*strSQL = "select LPGrpEdorMain.EdorNo,min(LPGrpEdorMain.GrpPolNo),min(LCGrpPol.RiskCode),min(LPGrpEdorMain.GrpContNo),min(LPGrpEdorMain.InsuredNo),min(LPGrpEdorMain.InsuredName),sum(LPGrpEdorMain.GetMoney),min(LPGrpEdorMain.EdorAppDate),min(LPGrpEdorMain.EdorValiDate),decode(max(LPGrpEdorMain.EdorState)||min(LPGrpEdorMain.EdorState),'00','保全确认','11','正在申请','22','申请确认','21','部分申请确认','20','部分保全确认') from LPGrpEdorMain,LCGrpPol where LPGrpEdorMain.GrpPolNo=LCGrpPol.GrpPolNo and LPGrpEdorMain.EdorState <> '0'"			 
				 + getWherePart( 'LPGrpEdorMain.EdorNo','EdorNo' )
				 + getWherePart( 'LPGrpEdorMain.GrpPolNo','GrpPolNo' )
				 + getWherePart( 'LPGrpEdorMain.GrpContNo','GrpContNo' )
				 + getWherePart( 'LCGrpPol.RiskCode','RiskCode' )
				 + getWherePart( 'LPGrpEdorMain.InsuredNo','InsuredNo' )
				 + getWherePart( 'LPGrpEdorMain.EdorAppDate','EdorAppDate' )
				 //+ getWherePart( 'LPGrpEdorMain.ManageCom','ManageCom' );	
				 + getWherePart( 'LPGrpEdorMain.ManageCom','ManageCom','like' )
				 + " group by LPGrpEdorMain.EdorNo";	*/
  strSQL=" select a.edorno,a.GrpContNo,a.EdorType,a.EdorState,a.MakeDate,a.MakeTime from lpgrpedoritem a where a.EdorNo='" + itemMainNo+ "' and a.edorstate<>0  order by a.MakeDate desc,a.MakeTime desc"
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPageItem.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPageItem.strQueryResult) {
   alert("该批单下项目已全部删除！");
   initPolGrid(); 
    return false;
  }
  
   turnPageItem.arrDataCacheSet = clearArrayElements(turnPageItem.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPageItem.arrDataCacheSet = decodeEasyQueryResult(turnPageItem.strQueryResult,0,0,turnPageItem);
  
  //设置初始化过的MULTILINE对象
  turnPageItem.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPageItem.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPageItem.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet2 = turnPageItem.getData(turnPageItem.arrDataCacheSet, turnPageItem.pageIndex, turnPageItem.pageLineNum);
  arrGrid = arrDataSet2;
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet2, turnPageItem.pageDisplayGrid,turnPageItem);
  fm.all('EdorNo').value="";
}


function easyQueryClickMain()
{          
	// 初始化表格
	initMainGrid();
	
	// 书写SQL语句
	var strSQL = "";
	
	/*strSQL = "select LPGrpEdorMain.EdorNo,min(LPGrpEdorMain.GrpPolNo),min(LCGrpPol.RiskCode),min(LPGrpEdorMain.GrpContNo),min(LPGrpEdorMain.InsuredNo),min(LPGrpEdorMain.InsuredName),sum(LPGrpEdorMain.GetMoney),min(LPGrpEdorMain.EdorAppDate),min(LPGrpEdorMain.EdorValiDate),decode(max(LPGrpEdorMain.EdorState)||min(LPGrpEdorMain.EdorState),'00','保全确认','11','正在申请','22','申请确认','21','部分申请确认','20','部分保全确认') from LPGrpEdorMain,LCGrpPol where LPGrpEdorMain.GrpPolNo=LCGrpPol.GrpPolNo and LPGrpEdorMain.EdorState <> '0'"			 
				 + getWherePart( 'LPGrpEdorMain.EdorNo','EdorNo' )
				 + getWherePart( 'LPGrpEdorMain.GrpPolNo','GrpPolNo' )
				 + getWherePart( 'LPGrpEdorMain.GrpContNo','GrpContNo' )
				 + getWherePart( 'LCGrpPol.RiskCode','RiskCode' )
				 + getWherePart( 'LPGrpEdorMain.InsuredNo','InsuredNo' )
				 + getWherePart( 'LPGrpEdorMain.EdorAppDate','EdorAppDate' )
				 //+ getWherePart( 'LPGrpEdorMain.ManageCom','ManageCom' );	
				 + getWherePart( 'LPGrpEdorMain.ManageCom','ManageCom','like' )
				 + " group by LPGrpEdorMain.EdorNo";	*/
  strSQL=" select a.edorNo,a.grpcontno,a.edorState,a.makedate,a.maketime "
         + "from lpgrpedorMain a, LGWork b, LDUser c,lpedorapp d "
         + "where a.edorNo = b.workNo "
         + "  and d.edoracceptno = a.edorno"
         + "  and b.acceptorNo = c.userCode "
         + "  and d.edorstate<>'0'  "
         + "  and b.typeno not in ('0601','0602') "
         + "  and b.statusno not in ('5','8') "
         + getWherePart( 'a.EdorNo','EdorNo' )
         + getWherePart( 'a.GrpContNo','GrpContNo')
         + getWherePart( 'a.EdorState','EdorState' )
         + "  and c.comCode like '" + fm.ManageCom.value + "%%' "
         +"order by a.MakeDate desc,a.MakeTime desc";


  
  if(!easyQueryVer3(strSQL))
  {
    var cEdorNo = fm.EdorNo.value;
    var cGrpContNo = fm.GrpContNo.value;
    if(!(cEdorNo==""||cEdorNo==null||cEdorNo=="null")&&(cGrpContNo==""||cGrpContNo==null||cGrpContNo=="null"))
    {
    strSQL=" select a.EdorAcceptNo,'',a.edorState,a.makedate,a.maketime "
         + "from LPEdorApp a, LGWork b, LDUser c "
         + "where a.edorAcceptNo = b.workNo "
         + "  and b.acceptorNo = c.userCode "
         + "  and a.edorstate<>'0'  "
         + "  and b.typeno not in ('0601','0602') "
         + "  and b.statusno not in ('5','8') "
         + getWherePart( 'a.EdorAcceptNo','EdorNo' )
         + getWherePart( 'a.EdorState','EdorState' )
         + "  and c.comCode like '" + fm.ManageCom.value + "%%' "
         +"order by a.MakeDate desc,a.MakeTime desc";
    } 
  }
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未查询到满足条件的数据！");
    initMainGrid();
    initPolGrid();
     
    return false;
  }
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = MainGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  arrGrid = arrDataSet;
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

  initPolGrid();
}

function getItemDetail()
{
	
	initPolGrid();
	var tsel=MainGrid.getSelNo();


	var tEdorNo = MainGrid.getRowColData(tsel-1,1);
  mainState=MainGrid.getRowColData(tsel-1,3);
  itemMainNo=MainGrid.getRowColData(tsel-1,1);
 
  
  var strSQL = "";
	
	
  strSQL= " select a.edorNo,a.grpcontno,a.edortype,a.edorstate,a.MakeDate,a.MakeTime "
  		  + "from lpgrpedorItem a "
  		  + "where a.edorNo='" + tEdorNo + "' "
  		  + "	and a.edorstate<>'0' "
  		  + "order by a.MakeDate desc,a.MakeTime desc"
  
	
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  //turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);	


turnPageItem.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

  //判断是否查询成功
  if (!turnPageItem.strQueryResult) {

    alert("该批单下已无批改项目！");
    return false;
  }

  turnPageItem.arrDataCacheSet = clearArrayElements(turnPageItem.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPageItem.arrDataCacheSet = decodeEasyQueryResult(turnPageItem.strQueryResult,0,0,turnPageItem);
  
  //设置初始化过的MULTILINE对象
  turnPageItem.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPageItem.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPageItem.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet2 = turnPageItem.getData(turnPageItem.arrDataCacheSet, turnPageItem.pageIndex, turnPageItem.pageLineNum);
  arrGrid = arrDataSet2;
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet2, turnPageItem.pageDisplayGrid,turnPageItem);

 }
  


/**
 *删除项目
 */

function CancelEdor()
{
  //20080529 modify zhanggm 送审或送核的工单不能撤销
  if(!checkUW())
  {
    return false;
  }
    if(fm.CancelItemReasonCode.value == "")
    {
        alert("撤销原因不能为空");
        return false;
    }
    
	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
	
		if( tSel == 0 || tSel == null )
		{
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
			fm.CancelItemReasonCode.value='';
		  fm.CancelMainReasonCode.value='';
	    fm.delMainReason.value='';
		  fm.delItemReason.value='';
		}
		else if(mainState=="1"||mainState=="2"||mainState=="3")
		{
			
			arrReturn = getQueryResult();
	
			fm.all('hEdorNo').value = PolGrid.getRowColData(tSel-1,1);
			fm.all('hGrpContNo').value=PolGrid.getRowColData(tSel-1,2);
		  fm.all('hEdorType').value=PolGrid.getRowColData(tSel-1,3);
			fm.all('hEdorState').value=PolGrid.getRowColData(tSel-1,4);
			fm.all('MakeDate').value=PolGrid.getRowColData(tSel-1,5);
			fm.all('MakeTime').value=PolGrid.getRowColData(tSel-1,6);			
			fm.all("Transact").value = "G&EDORITEM";
	    fm.all("DelFlag").value="1";  //flag 为1 删除项目

		  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 	    
  		var showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
			fm.submit();
			showInfo.close();
		  fm.CancelItemReasonCode.value='';
		  fm.CancelMainReasonCode.value='';
	    fm.delMainReason.value='';
		  fm.delItemReason.value='';
		}
}


function CancelEdorMain()
{
  //20080529 modify zhanggm 送审或送核的工单不能撤销
  if(!checkUW())
  {
    return false;
  }
    if(fm.CancelMainReasonCode.value == "")
    {
        alert("撤销原因不能为空");
        return false;
    }
    
	var arrReturn = new Array();
				
		var tSel = MainGrid.getSelNo();
	
		if( tSel == 0 || tSel == null )
		{
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
			return false;
		}
		else
		{
			arrReturn = getQueryResult();
	
			fm.all('hEdorNo').value = MainGrid.getRowColData(tSel-1,1);
			fm.all('hGrpContNo').value=MainGrid.getRowColData(tSel-1,2);
			fm.all('hEdorState').value=MainGrid.getRowColData(tSel-1,3);
           		      
			cGrpContNo = MainGrid.getRowColData(tSel-1,2);//12-03
			fm.all("Transact").value = "DELETE||EDOR";
	                fm.all("DelFlag").value="2";     // flag 为2 删除批单 
	        //20140916要求先撤销工单下的保全项目之后再撤销工单      
	        strSQL= "select edortype "
	            	+ "from lpgrpedorItem a "
	            	+ "where a.edorNo='" + fm.all('hEdorNo').value + "' "
	            	 + "	and a.edorstate<>'0' "
	        var itemFlag = easyExecSql(strSQL);
	         if(itemFlag){
	        	 alert("请先申请撤销保全项目！");
	        	 return false;
	         }
			// showSubmitFrame(mDebug);

		    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		

		}
		fm.all("Transact").value = "G&EDORMAIN";
		fm.submit();
		showInfo.close();
		fm.CancelItemReasonCode.value='';
		fm.CancelMainReasonCode.value='';
	  fm.delMainReason.value='';
		fm.delItemReason.value='';
}



function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	
	//********dingzhong*********
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	//**************************
	
/*	
	arrSelected[0] = arrGrid[tRow-1];
	//alert(arrSelected[0][0]);
*/	
	return arrSelected;
}

function afterSubmit( FlagStr, content,Result )
{
  
  //showInfo.close();
  
  if (FlagStr == "Fail" )
  {   
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	         
				fm.all('EdorNo').value ="";
				fm.all('GrpContNo').value ="";	  
                                fm.all('EdorState').value="";
				if(fm.all("DelFlag").value=="2") //删主批单
				{ 	
				//initForm();				
				easyQueryClickMain();
			 }
			 if(fm.all("DelFlag").value=="1") //删项目
				{ 				
				getItemDetail();
			 }
			 				
	
			    
			    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
			
			    //easyQueryClick();
			   
			 
    }
}

//20080529 add zhanggm 送审或送核的工单不能撤销
//20081008 modify zhanggm 送审的工单可以撤销，送核的工单不能撤销
function checkUW()
{
  var tSel = MainGrid.getSelNo();
  if( tSel == 0 || tSel == null )
  {
    alert( "请先选择保全批单信息，再点击申请撤销按钮！" );
    return false;
  }
  var edorno = MainGrid.getRowColData(tSel-1,1);
  //送审
  /*
  var sql = "select 1 from LGWork where StatusNo in ('4') and WorkNo = '" + edorno + "' with ur";
  var result = easyQueryVer3(sql);
  var returnInfo = "送审或送核不能撤销！";
  if(result)
  {
    returnInfo = "保全申请 " + edorno + " 现在是待审批状态，不能撤销！";
    alert(returnInfo);
    return false;
  }
  */
  //送核
  sql = "select 1 from LPEdorApp where UWState in ('5') and EdorAcceptNo = '" + edorno + "' with ur";
  result = easyQueryVer3(sql);
  if(result)
  {
    returnInfo = "保全申请 " + edorno + " 现在是核保状态，不能撤销！";
    alert(returnInfo);
    return false;
  }
  return true;
}