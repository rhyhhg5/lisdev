<%
//程序名称：GEdorTypeMJListMain.jsp
//程序功能：保全确认按钮
//创建日期：2005-09-14
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<title>方式选择 </title>
<script language="javascript">
	var intPageWidth = 500;
	var intPageHeight = 300;
	var letf = (screen.availWidth - intPageWidth) / 2;
	var top = (screen.availHeight - intPageHeight) / 2;
	window.resizeTo(intPageWidth, intPageHeight);
	window.moveTo(letf, top);
	window.focus();
</script>
</head>
<frameset name="fraMain" rows="0,0,0,0,*" frameborder="no" border="1" framespacing="0" cols="*">
<!--标题与状态区域-->
	<!--保存客户端变量的区域，该区域必须有-->
	<frame name="VD" src="../common/cvar/CVarData.html">
	
	<!--保存客户端变量和WebServer实现交户的区域，该区域必须有-->
	<frame name="EX" src="../common/cvar/CExec.jsp">
	
	<frame name="fraSubmit"  scrolling="yes" noresize src="about:blank" >
	<frame name="fraTitle"  scrolling="no" noresize src="about:blank" >
	<frameset name="fraSet" rows="0,*,0" frameborder="no" border="1" framespacing="0" rows="*">
		<!--菜单区域-->
		<frame name="fraMenu" scrolling="yes" noresize src="about:blank">
		<!--交互区域-->
		<frame id="fraInterface" name="fraInterface" scrolling="auto" src="./GEdorTypeMJOption.jsp?EdorAcceptNo=<%=request.getParameter("EdorNo")%>&dealType=<%=request.getParameter("DealType")%>&sumGetMoney=<%=request.getParameter("sumGetMoney")%>">
    	<!--下一步页面区域-->
    	<frame id="fraNext" name="fraNext" noresize scrolling="no" src="about:blank">
	</frameset>
</frameset>
<noframes>
	<body bgcolor="#ffffff">
	</body>
</noframes>
</html>
