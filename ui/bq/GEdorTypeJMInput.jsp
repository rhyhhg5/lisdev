<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK"> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeJM.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeJMInit.jsp"%>
  <title>客户基本资料变更 </title> 
</head>
<body  onload="initForm();" >
  
  <form action="./GEdorTypeJMSubmit.jsp" method=post name=fm target="fraSubmit"> 
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
      <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly"  readonly name=EdorType>
      	<input class = "readonly" readonly type="hidden" name=EdorTypeName>
      </TD>
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>
 <Div  id= "divLPInsuredDetail" style= "display:''">
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
        客户详细信息
      </td>
   </tr>
   </table>
  	<Div  id= "divDetail" style= "">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name verify="姓名|notnull&len<=60"  elementtype=nacessary >
            <Input class= hidden type=hidden readonly name=NameBak >
          </TD>
				  <TD  class= title>
						性别
				  </TD>
				  <TD  class= input>
					 	<Input class="codeNo" name=Sex verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName], [0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName], [0,1]);"><Input class="codeName" name=SexName elementtype=nacessary readonly >
		        <Input class= hidden type=hidden readonly name=SexBak >
				  </TD>
				</TR>
      	<TR  class= common>
				  <TD  class= title>
				   	出生日期
				  </TD>
				  <TD  class= input>
						<Input class= common name=Birthday elementtype=nacessary verify="出生日期|notnull&date" >
		        <Input class= hidden type=hidden readonly name=BirthdayBak >
				  </TD>
          <TD  class= title>
           生效日期
          </TD>
         <TD  class= input>
			<Input class= common name=Cvalidate readonly  >
            <Input class= hidden type=hidden readonly name=CvalidateBak >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name=IDNo elementtype=nacessary verify="证件号码|len<20">
            <Input class= hidden type=hidden readonly name=IDNoBak >
         </TD>
        </TR>       
        <TR  class= common>
         <TD class=title>
					  职业代码 
					</TD>
					<TD class=input>
					 	<Input name=OccupationCode class="codeNo" ondblclick="return showCodeList('OccupationCode', [this,OccupationCodeName,OccupationType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode', [this,OccupationCodeName,OccupationType],[0,1,2],null,null,null,1,300);" ><Input name=OccupationCodeName class="codeName"  readonly >
						<Input class= hidden type=hidden readonly name=OccupationCodeBak >
					</TD>
					<TD class=title>
					  职业类别 
					</TD>
					<TD class=input>
					  <Input name="OccupationType" class="readonly" readonly>
						<Input class= hidden type=hidden readonly name=OccupationTypeBak >
					</TD>
			        <TD  class= title>
			            证件类型
			          </TD>
			          <TD  class= input>
			            <Input class= codeNo name="IDType"  verify="证件类型|notnull&code:IDType" ondblclick="return showCodeList('IDType',[this,IDTypeName], [0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName], [0,1]);"><Input class= codeName name="IDTypeName" elementtype=nacessary readonly >
			            <Input class= hidden type=hidden readonly name=IDTypeBak >
			          </TD>
        
        </TR> 
        <TR  class= common>
          <TD class=title>
					 手机号 
					</TD>
					<TD class=input>
					 	<Input name=Mobile class="common" >
						<Input class= hidden type=hidden readonly name=MobileBak >
					</TD>
					<TD class=title>
					固定电话 
					</TD>
					<TD class=input>
					  <Input name=Phone class="common" >
					  <Input class= hidden type=hidden readonly name=PhoneBak >
					</TD>
					<TD CLASS=title>
						通讯地址
					</TD>
					<TD CLASS=input>
						 <Input name=PostAddress class="common" >
					     <Input class= hidden type=hidden readonly name=PostAddressBak >
					</TD>
         </TR> 
         <TR>
         	<TD CLASS=title>
					邮编
					</TD>
					<TD CLASS=input>
						 <Input name=ZipCode class="common" >
					     <Input class= hidden type=hidden readonly name=ZipCodeBak >
					</TD>
					<TD CLASS=title>
						Email地址
					</TD>
					<TD CLASS=input colspan="3">
						 <Input name=Email class="common" >
					     <Input class= hidden type=hidden readonly name=EmailBak >
					 </TD>
       	 </TR>
       </table>
      </Div>

	</Div>
	<br>
	<br>
	<div id="divCont">
		<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);">
	      </td>
	      <td class= titleImg>
	        关联保单信息
	      </td>
		  </tr>
		</table>
		<Div  id= "divLCCont" style= "display: ''">
			<table  class= common>
		 		<tr  class= common>
			  		<td text-align: left colSpan=1>
					<span id="spanContGrid" >
					</span> 
			  	</td>
			</tr>
		</table>					
	</div>  
	</div>
	  <br>
	
		<Input class= cssButton type=Button value="保  存" onclick="edorTypeJMSave()">
		<Input class= cssButton type=Button value="取  消" onclick="initForm()">
		<Input type=Button value="返  回" class = cssButton onclick="returnParent()">
		<input type=hidden id="TypeFlag" name="TypeFlag">
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="ContType" name="ContType">
		<input type=hidden name="EdorAcceptNo">
		<input type=hidden name="AppntNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>