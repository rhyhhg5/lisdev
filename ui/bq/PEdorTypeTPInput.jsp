<%@page contentType="text/html;charset=GBK" %>
<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2005-4-6
//创建人  ：LHS
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeTP.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeTPInit.jsp"%>
<title>康乐人生转保！</title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeTPSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
     
      <TD class = title > 个人保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE> 
  
  <Div  id= "divLPAppntDetail" style= "display: ''">
  <table>
    	<tr>
    		<td class= titleImg>
    			 康乐人生转保
    		</td>
    	</tr>
   </table>
    
   <Div  id= "divGroupPol2" style= "display: ''">
      <br>
			<Div  id= "divPolInfo" style= "display: ''">
				<table>
				  <tr>
				      <td>
				          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
				      </td>
				      <td class= titleImg>
				          转保前保单险种信息
				      </td>
				  </tr>
				</table>
				<Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
				<tr  class= common>
						<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span> 
				  	</td>
				</tr>
				</table>					
			</div>
    </DIV>
    <Div  id= "divPremInfo" style= "display: 'none'">
      <table  class= common>
       <TR class= common>
          <TD  class= common >
            <Input class= "readonly" name="TotalPremInfo" value = "" readonly style = "width:100%">
            <input type=hidden id="TotalPrem" name="TotalPrem">
          </TD>  
        </TR>
       </table>
    </DIV>
      <br>
     	<Input type =button name=save class=cssButton value="保存" onclick="edorTypeTPSave()">
     	<Input type =button name=goBack class=cssButton value="返    回" onclick="returnParent()">
    </Div>
	</Div>
	 <input type=hidden id="fmAction" name="fmAction">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 <input type=hidden id="addrFlag" name="addrFlag">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
