<% 
//程序名称：附加险增额
//程序功能：个人保全
//创建日期：2004-05-12 
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>

<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeSA.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <%@include file="PEdorTypeSAInit.jsp"%>
  
  <title>附加险增额 </title> 
</head>

<body  onload="initForm();">
  <form action="./PEdorTypeSASubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>
     
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=PolNo>
      </TD>   
    </TR>
  </TABLE> 
  
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCDuty);">
      </td>
      <td class= titleImg>
        保单责任信息
      </td>
   </tr>
   </table>
	 
    <Div  id= "divLCDuty" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCDutyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table> 					
  	</div>
  	
  	<Div  id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div>

  <br>
      
	<div id = "RemainAmntDiv" style= "display: none">	
      <table class = common>
       		<tr  class= common>
		       <TD  class= title > 新保额</TD>
		       <TD  class= input > 
               <input class="common"  name=RemainAmnt >
             </TD>
          </tr>   
     </table>
  </div>  
     
  <div id = "RemainMultiDiv"  style= "display: none">
   <table class= common>
     <tr>  
       <TD  class= title > 新份数</TD>
	   <TD  class= input > 
             <input class="common"  name=RemainMulti >
         </TD>
      </tr>
    </table>
  </div>               
  			
	  <table class = common>	
	  <TR class= common>
			<TD  class= input width="26%"> 
		        <Div id ="divGetEndorse" style="display:''">
	       		 <Input class = common type=Button value="费用明细" onclick="GetEndorseQuery()">
	    		</Div>
	       </TD>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeSASave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="返回" onclick="returnParent()">
     	 </TD>
     </TR>
     </table>

	 <input type=hidden name=DutyCode >
	 <input type=hidden name=Multi >
	 <input type=hidden name=Amnt > 
	  
	 <input type=hidden id="CalMode" name = "CalMode">
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" value= 1 name="ContType">
	 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
