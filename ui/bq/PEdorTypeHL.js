//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var itjj = 1 ;
var mCond;
var tReturn="";
 var tContNo="";
var tInsuredNo="";
//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
     function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmtransact').value="INSERT||MAIN";
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, Result )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
  }
  else
  { 
		var tTransact = fm.all('fmtransact').value;
		//alert(Result);
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			if (Result!=null&&Result!='')
			{
				iArray = decodeEasyQueryResult(Result, tTransact);
			
				tArr=chooseArray(iArray,[4,3,6,9,10,14,15,12]);
				turnPage.pageDisplayGrid = LPGetGrid;
				//调用MULTILINE对象显示查询结果
	   		displayMultiline(tArr, turnPage.pageDisplayGrid);
	   	}
		}
		else if (tTransact=="QUERY||ACCOUNT")
		{
			var iArray;
			if (Result!=null&&Result!='')
			{
				iArray = decodeEasyQueryResult(Result, tTransact);
				tArr=chooseArray(iArray,[1,4,0,11,16,8,9,3]);
				turnPage.pageDisplayGrid = LCInsureAccGrid;
				//调用MULTILINE对象显示查询结果
	   		displayMultiline(tArr, turnPage.pageDisplayGrid);
	   		DivGetAcc.style.display ='';
	    	DivLCInsureAcc.style.display='';
    	}
		}
		else if (tTransact=="QUERY||MANAGE")
		{
			var iArray;
			if (Result!=null&&Result!='')
			{
				iArray = decodeEasyQueryResult(Result, tTransact);
				tArr=chooseArray(iArray,[9,10,12]);
				for (i=0;i<tArr.length;i++)
				{
					if (tArr[i][0]=='2')
					{
						fm.cashFlag1.checked = true;
						fm.CAnnuity.value=tArr[i][1];
						fm.RAnnuity.value=tArr[i][2]*100;
					}
					if (tArr[i][0]=='1')		
					{
						fm.annuityFlag1.checked = true;
						fm.CCash.value=tArr[i][1];
						fm.RCash.value=tArr[i][2]*100;
					}
				}
			}
		}
		else if (tTransact=="INSERT||MAIN")
		{
			initForm();
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		}
		else
		{
		}
  }
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

// 数据返回父窗口
function returnParent()
{
		top.close();
}

function setAnnuityFlag()
{
	if (fm.annuityFlag1.checked)
		fm.all('annuityFlag').value ="1";
	else
		fm.all('annuityFlag').value="0";
	//alert("---"+fm.all('annuityFlag').value);
}

function setCashFlag()
{
	if (fm.cashFlag1.checked)
		fm.all('cashFlag').value ="1";
	else
		fm.all('cashFlag').value="0";
}
/*********************************************************************
 *  选择给付类型后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	try	{
		if( cCodeName == "GetDutyKind" )	
		{
			//detailGetDuty( Field.value );//loadFlag在页面出始化的时候声明
		}
	}
	catch( ex ) {
	}
}
function detailGetDuty(tGetDutyKind)
{
	try{
	var tSel=LPGetGrid.getSelNo();
	var tGetDutyCode =LPGetGrid.getRowColData(tSel,1);
	var tDutyCode = LPGetGrid.getRowColData(tSel,2);
	var tSql = "select * from LMDutyGetAlive where GetdutyCode='"+tGetDutyCode+"' and GetDutyKind='"+tGetDutyKind+"'";
	//alert(tSql);
	var tArr = easyExecSql(tSql,1,0,1);
	if (tArr!=null)
	{
	LPGetGrid.setRowColData(tSel,4,tArr[0][3]);
	LPGetGrid.setRowColData(tSel,5,tArr[0][4]);
	}
	else
	{
		alert("给付类型有误!");
	}
	}
	catch(e)
	{
		alert(e);
	}
}
function verifyCAnnuity()
{
	var tRAnnuity = fm.all('RAnnuity').value;
	var tCAnnuity = fm.all('CAnnuity').value;

	if (tRAnnuity!=null&&tRAnnuity!=''&&tCAnnuity!=null&&tCAnnuity!='')
	{
		alert(tRAnnuity);
		alert("不能同时选择两种变动方式!");
		fm.all('CAnnuity').value='';
	}
}

function verifyRAnnuity()
{
	var tCAnnuity = fm.all('CAnnuity').value;	
	var tRAnnuity = fm.all('RAnnuity').value;

	if (tCAnnuity!=null&&tCAnnuity!=''&&tRAnnuity!=null&&tRAnnuity!='')
	{
		alert("不能同时选择两种变动方式!");
		fm.all('RAnnuity').value='';
	}
}
function verifyCCash()
{
	var tRCash = fm.all('RCash').value;
	var tCCash = fm.all('CCash').value;

	if (tRCash!=null&&tRCash!=''&&tCCash!=null&&tCCash!='')
	{
		alert("不能同时选择两种变动方式!");
		fm.all('CCash').value='';
	}
}
function verifyRCash()
{
	var tCCash = fm.all('CCash').value;	
	var tRCash = fm.all('RCash').value;

	if (tCCash!=null&&tCCash!=''&&tRCash!=null&&tRCash!='')
	{
		alert("不能同时选择两种变动方式!");
		fm.all('RCash').value='';
	}
}


function getRiskByInsuredNo(){
    	var i = 0;
	var j = 0;
	var m = 0;
	var n = 0;
	var strsql = "";
	var tCodeData = "0|";
	strsql = "select a.RiskCode, a.RiskName from LMRiskApp a,LCPol b where b.contno='"+fm.all('ContNo').value
	        +"' and b.InsuredNo='"+fm.all('InsuredNo').value+"' and a.riskcode=b.riskcode"
           + " order by RiskCode";
     alert(strsql);
	turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
	if (turnPage.strQueryResult != "")
	{
		turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		m = turnPage.arrDataCacheSet.length;
		for (i = 0; i < m; i++)
		{
			j = i + 1;
//			tCodeData = tCodeData + "^" + j + "|" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
			tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
		}
	}
    alert(tCodeData);
	return tCodeData;
}
/*********************************************************************
 *  查询给付项信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function QueryGetClick()
{
    
    var tRiskCode=fm.all('RiskCode').value;
     tContNo=fm.all("ContNo").value;
     tInsuredNo=fm.all("InsuredNo").value;
    if(tRiskCode!=null&&tRiskCode!="")
    {
        var strSQL ="select GetDutyCode,DutyCode,GetDutyKind,GetIntv,AddRate,GetStartDate,GetEndDate,ActuGet,PolNo from LCGet where PolNo in" 
                   +"(select polno from lcpol where contno='"+tContNo
                   +"' and InsuredNo='"+tInsuredNo+"' and RiskCode='"+tRiskCode+"')"
//                   +" and CanGet='1' and LiveGetType='0'";
        alert(strSQL);
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = LPGetGrid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
}
/*********************************************************************
 *  MulLine的RadioBox点击事件，获得帐户关联表详细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getInsureAccClass(parm1,parm2)
{
    var tPolNo=fm.all(parm1).all('LPGetGrid9').value;
    var tDutyCode=fm.all(parm1).all('LPGetGrid2').value;
    var tGetDutyCode=fm.all(parm1).all('LPGetGrid1').value;
    
    var strSQL ="select * from LCInsureAccClass where polno='"+tPolNo+"'"
                +" and InsuAccNo in (select InsuAccNo from LCGetToAcc where PolNo='"+tPolNo
                +"' and DutyCode='"+tDutyCode+"' and GetDutyCode='"
                +tGetDutyCode+"') and AccAscription='1'";
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = LCInsureAccClassGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    
}
/*********************************************************************
 *  添加年金转换记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */ 

function addAccMove()
{
   var tEdorNo=fm.all('EdorNo').value;
   var tEdorType=fm.all('EdorType').value ;
   
   
   var getSelNo=LPGetGrid.getSelNo();//给付项表被选中行 
   alert(getSelNo);
   var tPolNo=LPGetGrid.getRowColData(getSelNo-1,9);
   var tDutyCode=LPGetGrid.getRowColData(getSelNo-1,2);
   var tGetDutyCode=LPGetGrid.getRowColData(getSelNo-1,1);
    
   var rowNum=LCInsureAccClassGrid.mulLineCount;
   for (i = 0; i < rowNum; i++)
   {
        if (LCInsureAccClassGrid.getChkNo(i))//如果该行被选中
        {
            LPAccMoveGrid.addOne(); 
            var movRowNum=LPAccMoveGrid.mulLineCount;
            LPAccMoveGrid.setRowColData(movRowNum-1,1,tEdorNo); //批单号
            LPAccMoveGrid.setRowColData(movRowNum-1,2,tEdorType); //批改类型
            LPAccMoveGrid.setRowColData(movRowNum-1,3,tPolNo); //保单险种号码
            LPAccMoveGrid.setRowColData(movRowNum-1,4,LCInsureAccClassGrid.RowColData(i,2)); //保险帐户号码
            //LPAccMoveGrid.setRowColData(movRowNum,4,LCInsureAccClassGrid.RowColData(i,4)); //险种编码
            LPAccMoveGrid.setRowColData(movRowNum-1,5,LCInsureAccClassGrid.RowColData(i,4)); //保险子帐户
            //LPAccMoveGrid.setRowColData(movRowNum,6,LCInsureAccClassGrid.RowColData(i,5)); //险种编码
            LPAccMoveGrid.setRowColData(movRowNum-1,7,LCInsureAccClassGrid.RowColData(i,5)); //账户类型
            LPAccMoveGrid.setRowColData(movRowNum-1,8,LCInsureAccClassGrid.RowColData(i,10)); //对应其它号码
            LPAccMoveGrid.setRowColData(movRowNum-1,9,LCInsureAccClassGrid.RowColData(i,11)); //对应其它号码类型
            LPAccMoveGrid.setRowColData(movRowNum-1,10,LCInsureAccClassGrid.RowColData(i,12)); //账户归属属性
            LPAccMoveGrid.setRowColData(movRowNum-1,11,"2"); //帐户转移类型
            LPAccMoveGrid.setRowColData(movRowNum-1,12,tGetDutyCode); //给付项编码
            //LPAccMoveGrid.setRowColData(movRowNum,13,LCInsureAccClassGrid.RowColData(i,5)); //转换金额
            //LPAccMoveGrid.setRowColData(movRowNum,14,LCInsureAccClassGrid.RowColData(i,5)); //变动保险帐户单位数
            //LPAccMoveGrid.setRowColData(movRowNum,15,LCInsureAccClassGrid.RowColData(i,5)); //转换比例
            //LPAccMoveGrid.setRowColData(movRowNum,16,LCInsureAccClassGrid.RowColData(i,5)); //转换类型
        }
    }
   
} 


function queryClick(){

  var strSQL = "";           
   strSQL="select p.riskcode,(select m.riskname from lmrisk m where m.riskcode=p.riskcode),'个人账户',(select sum(insuaccbala) from lcinsureacc n where n.grpcontno=p.grpcontno and n.polno =p.polno) ,(select distinct baladate from lcinsureacc m where m.insuredno =p.insuredno and m.grpcontno=p.grpcontno),(select distinct name from lcinsured where  grpcontno =p.grpcontno and insuredno =p.insuredno),p.insuredno ,p.polno from lcpol p where p.contno='"+fm.all('ContNo').value+"'and  p.insuredno='"+fm.all("InsuredNo").value+"' and p.poltypeflag !='2'"
			      +"order by baladate desc";	  
	arrResult = easyExecSql(strSQL);
    turnPage.queryModal(strSQL,AccGrid);        

}
 
 
 function queryByLingqu(){
   var strSQL = "";           
   strSQL="select getdutykind,standmoney,getintv from lpget where insuredno='"+fm.InsuredNo.value+"' and getdutycode='671201' and dutycode ='671005'and edorno='"+fm.EdorNo.value+"' and edortype='"+fm.EdorType.value+"'";	
	
	var arrResult = easyExecSql(strSQL);
	if(arrResult!=null && arrResult[0][0]=='A'){
	 fm.PayMode.value="A";
	 fm.BigProjectFlagName.value='一次性领取';
	}else if(arrResult!=null && arrResult[0][0]=='D'){
     fm.PayMode.value="D";
	 fm.BigProjectFlagName.value='按年或按月分次领取';
 	 fm.all('MoneyType').style.display = "";
     fm.all('Fenci').style.display = "";
	 if(arrResult[0][2]!=null && arrResult[0][2]=='12'){

	 fm.PerPayMode.value="0";
	 fm.BigProjectFlagName1.value="按年分次领取金额为";
	 fm.Money.value=arrResult[0][1];
	 }
	 if(arrResult[0][2]!=null && arrResult[0][2]=='1'){
	 fm.PerPayMode.value="1";
	 fm.BigProjectFlagName1.value="按月分次领取金额为";
	 fm.Money.value=arrResult[0][1];
	 }
  	  fm.all('MoneyType').style.display = "";
      fm.all('Fenci').style.display = "";
	}else{
	  strSQL="select getdutykind,standmoney,getintv from lcget where insuredno='"+fm.InsuredNo.value+"' and getdutycode='671201'";	
		var arrResult = easyExecSql(strSQL);
	if(arrResult!=null && arrResult[0][0]=='A'){
	 fm.PayMode.value="A";
	 fm.BigProjectFlagName.value='一次性领取';
	}else if(arrResult!=null && arrResult[0][0]=='D'){
		    fm.PayMode.value="D";
	 fm.BigProjectFlagName.value='按年或按月分次领取';
	 if(arrResult[0][2]!=null && arrResult[0][2]=='12'){
	 fm.PerPayMode.value="0";
	 fm.BigProjectFlagName1.value="按年分次领取金额为";
	 fm.Money.value=arrResult[0][1];
	 }
	 if(arrResult[0][2]!=null && arrResult[0][2]=='1'){
	 fm.PerPayMode.value="1";
	 fm.BigProjectFlagName1.value="按月分次领取金额为";
	 fm.Money.value=arrResult[0][1];
	 }
	}	
	}
 }
function isMixCom()
{
    if(fm.MixComFlag.checked == true)
    {
        fm.all('LXFS').style.display = "";
       
    }
    if(fm.MixComFlag.checked == false)
    {
        fm.all('YearMoney').value ="";
        fm.all('MonthMoney').value ="";
        fm.all('LXFS').style.display = "none";

    }
}   
function afterCodeSelect( cCodeName, Field ) {
  if(cCodeName=="PayMode")
  {
  	if(Field.value=="A")
  	{	
     fm.all('Money').value ="";
     fm.all('Fenci').style.display = "none";
     fm.all('MoneyType').style.display = "none";
  	}else{
  	      fm.all('PerPayMode').value ="";
  	      fm.all('BigProjectFlagName1').value ="";
  	      fm.all('MoneyType').style.display = "";
          fm.all('Fenci').style.display = "";
  	    }
 }

   if(cCodeName=="PerPayMode") //分次领取方式
  {
     fm.all('Money').value ="";
 }

}   