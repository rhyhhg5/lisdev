<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	String tGrpPolNo = "";
	String tEdorNo ="";
	String tEdorType ="";
	try
	{
		tGrpPolNo = request.getParameter("GrpPolNo");
		tEdorNo = request.getParameter("EdorNo");
		tEdorType = request.getParameter("EdorType");
	}
	catch( Exception e )
	{
		tGrpPolNo = "";
	}
%>
<head>
<script>
	var GrpPolNo = "<%=tGrpPolNo%>"; //判断从何处进入保单录入界面,该变量需要在界面出始化前设置.
	var EdorNo = "<%=tEdorNo%>";
	var EdorType="<%=tEdorType%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeNIQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./GEdorTypeNIQueryInit.jsp"%>
  <title>集体投保单下增加被保人查询 </title>
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
  <table class= common>
    <TR  class= common>
      <TD  class= title>
        集体投保单号码
      </TD>
      <TD  class= input>
        <Input class="readonly" readonly name=GrpPolNo >
      </TD>
      <TD  class= title > 批单号</TD>
		      <TD  class= input > 
		        <input class="readonly" readonly name=EdorNo >
		      </TD>
		  <TD class = title > 批改类型 </TD>
		      <TD class = input >
		      	<input class = "readonly" readonly name=EdorType>
		      </TD>
    </TR>
    </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol1);">
    		</td>
    		<td class= titleImg>
    			 集体下个人投保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCGrpPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGrpPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
  	<p>
      <INPUT VALUE="查询保单明细" TYPE=button onclick="showPolDetail();"> 
      <INPUT VALUE="返回" TYPE=button onclick="returnMain();"> 
  	</p>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
