  	 <html> 
<%
//程序名称：IndiDueFeeInput.jsp
//程序功能：修改作废应收状态
//创建日期：2002-07-24 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="./BqUpdatePolState.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BqUpdatePolStateInit.jsp"%> 
  
</head>
<body  onload="initForm();" >
<font size="5">修改作废应收状态</font>
<form name=fm action="./IndiDueFeeQuery.jsp" target=fraSubmit method=post>
  <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
	    <TR  class= common>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class=common name=ContNo>
          </TD>      	
          <TD  class= title>
            个团类型
          </TD>
          <TD class=input >
            <Input class="readonly" name=PolTypeFlag value="个单" readonly>
          </TD>
        </TR> 
    </table>
    <br>
    <INPUT VALUE="查询" class = cssbutton TYPE=button onclick="easyQueryClick();">
    <input type="hidden" value="polnoStr" name="polnoStr">
    <IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divLCPol1);">
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
			<center>
			      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	
			</center>	  
  	</div>  
  	<INPUT VALUE="修改" class = cssbutton TYPE=button onclick="UpdateClick();">	 
  	<p>    
    <INPUT type= "hidden" name= "Operator" value= "">     
    <INPUT type= "hidden" name= "ContNoSelected" value= ""> 
    <Input type= "hidden" name="GetNoticeNo"> 
  	</p>
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>