//程序名称：PEdorRFBudget.js
//程序功能：还款试算
//创建日期：2018-02-23
//创建人  ：feng zhihang
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;

var turnPage = new turnPageClass(); // 使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

// 判断是否是回车键
function isEnterDown() {
	var keycode = event.keyCode;
	// 回车的ascii码是13
	if (keycode == "13") {
		return true;
	} else {
		return false;
	}
}

// 响应回车事件，查询保单
function queryContOnKeyDown() {
	if (isEnterDown()) {
		queryCont();
	}
}
//每次查询时将上一次查询或计算结果清除。
function initQueryCont(){
	fm.GetMoney.value = "";
	fm.contNo.value = "";
	fm.appntName.value = "";
	fm.appntNo.value = "";
	fm.polApplyDate.value = "";
	fm.cValiDate.value = "";
	fm.payToDate.value = "";
	fm.cInValiDate.value = "";
	fm.prem.value = "";
	fm.claimState.value = "";
	fm.edorState.value = "";
}
// 查询保单信息
function queryCont() {
	// alert(2);
//	每次查询将上次计算以及查询结果清零
	initQueryCont();
	if (trim(fm.contNoTop.value) == "" && trim(fm.appntNoTop.value) == ""
			&& trim(fm.LNEdornoTop.value) == "") {
		alert("请录入查询条件。");
		return false;
	}

	initLCContGrid();
	initLCPolGrid();
	var sql = "select a.contNo, a.appntName, a.appntNo, a.polApplyDate, a.cValiDate, "
			+ "	max(b.payToDate) , "
			+ "	max(b.endDate), "
			+ "	'缴费频次', a.prem,c.edorno,c.loandate,c.payoffdate,c.summoney "
			+ "	from LCCont a ,lcpol b,loloan c "
			+ "	where a.appFlag = '1' "
			+ "	and a.contno = b.contno "
			+ "	and a.contType = '1' "
			+ getWherePart("a.contNo", "contNoTop")
			+ getWherePart("a.appntNo", "appntNoTop")
			+ "	and c.polno = b.polno and c.payoffflag='0' and c.loantype='0'  ";
	if (fm.LNEdornoTop.value != null && fm.LNEdornoTop.value != "") {
		sql += getWherePart("c.edorno", 'LNEdornoTop')
				+ "and exists(select 1 from lpedorapp where edoracceptno = '"
				+ fm.LNEdornoTop.value + "' and edorstate = '0') ";
	}
	sql += "  group by a.contNo, a.appntName, a.appntNo, a.polApplyDate, a.cValiDate,a.prem,c.edorno,c.loandate,c.payoffdate,c.summoney ";
	// alert(sql);
	// 没有保单贷款信息提示
	var contResult = easyExecSql(sql);
	if (!contResult) {
		alert("当前查询条件下无贷款信息！");
		return false;
	}
	turnPage1.pageDivName = "divPage";
	turnPage1.queryModal(sql, LCContGrid);
	// alert("contNoTop:"+fm.contNoTop.value);
	// alert("appntNoTop:"+fm.appntNoTop.value);
	// alert("LNEdornoTop:"+fm.LNEdornoTop.value);
	// alert("mulLineCount:"+LCContGrid.mulLineCount);
	if ((trim(fm.contNoTop.value) != "" || trim(fm.appntNoTop.value) != "" || trim(fm.LNEdornoTop.value) != "")
			&& LCContGrid.mulLineCount != 0) {
		// alert("执行setOneContInfo");
		setOneContInfo(0);
	}
}

// 相应保单列表的单选框单击事件
function setContInfoOnClick() {
	var row = LCContGrid.getSelNo() - 1;
	setOneContInfo(row);
}

// 显示选中的保单信息
function setOneContInfo(row) {
	fm.contNo.value = LCContGrid.getRowColDataByName(row, "contNo");
	// alert(fm.contNo.value);
	fm.appntName.value = LCContGrid.getRowColDataByName(row, "appntName");
	fm.appntNo.value = LCContGrid.getRowColDataByName(row, "appntNo");
	fm.polApplyDate.value = LCContGrid.getRowColDataByName(row, "polApplyDate");
	fm.cValiDate.value = LCContGrid.getRowColDataByName(row, "cValiDate");
	fm.payToDate.value = LCContGrid.getRowColDataByName(row, "payToDate");
	fm.cInValiDate.value = LCContGrid.getRowColDataByName(row, "cInValiDate");
	fm.prem.value = LCContGrid.getRowColDataByName(row, "prem");

	// 得到保单保全状态
	sql = "  select 1 " + "from LPEdorApp a, LPEdorItem b "
			+ "where a.edorAcceptNo = b.edorNo " + "   and b.contNo = '"
			+ fm.contNo.value + "' " + "   and a.edorState != '0' "
			+ "   and b.edorNo != '" + fm.edorNo.value + "' ";
	// alert("sql:"+sql);
	result = easyExecSql(sql);
	if (result) {
		fm.edorState.value = "有正在进行保全作业";
	} else {
		fm.edorState.value = "无正在进行保全作业";
	}


	sql = " select 1 from llregister where customerno in (select insuredno from lcinsured where contno = '"
			+ fm.contNo.value + "') and rgtstate != '12' and rgtstate !='14' ";
	result = easyExecSql(sql);
	if (result) {
		fm.claimState.value = "有正在进行理赔作业";
	} else {
		fm.claimState.value = "无正在进行理赔作业";
	}

	// 得到险种信息
	setLCPolInfo(fm.contNo.value);
}

// 得到险种信息
function setLCPolInfo(contNo) {
	var sql = "  select polNo, a.riskSeqNo, a.insuredNo, a.insuredName, a.riskCode, b.riskName, "
			+ "   a.CValiDate, a.amnt, a.mult, a.prem, (select sum(SumActuPayMoney) from LJAPayPerson where PolNo = a.PolNo), a.endDate,"
			+ "case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end "
			+ ", '' "
			+ "from LCPol a, LMRiskApp b "
			+ "where a.riskCode = b.riskCode "
			+ "   and a.ContType = '1' "
			+ "   and contNo = '"
			+ contNo
			+ "' "
			// 只查询出有贷款信息的险种
			+ "   and exists(select 1 from loloan where polno = a.polno and payoffflag='0' and loantype='0') "
			+ "order by riskSeqNo";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, LCPolGrid);
	// -----------by fengzhihang 查询当前贷款利率
	// 查询保单机构是否有特殊配置
	// alert(fm.contNo.value);
	var checkSql = "select * from ldcode where codetype = 'loancom' and code = "
			+ " (select case when managecom = '86' then managecom else substr(managecom,1,4) end "
			+ " from lccont where contno='" + fm.contNo.value + "') with ur  ";
	var checkResult = easyExecSql(checkSql);
	for (var i = 0; i < LCPolGrid.mulLineCount; i++) {
		var sql = "";
		if (checkResult) {
			sql = "  select rate from lminterestrate a where a.startdate<=current date and a.enddate>=current date   and a.busstype='L' "
					+ " and a.comcode = (select case when managecom = '86' then managecom else substr(managecom,1,4) end "
					+ "  from  lccont where contno='"
					+ fm.contNo.value
					+ "') "
					+ " and int(minpremlimit)<= (select prem from lccont where contno ='"
					+ fm.contNo.value
					+ "' ) "
					+ " and (int(nvl(maxpremlimit,0)) > (select prem from lccont where contno ='"
					+ fm.contNo.value
					+ "') or maxpremlimit is null or maxpremlimit='') "
					+ " and a.riskcode ='"
					+ LCPolGrid.getRowColDataByName(i, "riskCode")
					+ "' "
					+ " and a.riskcode in (select riskcode from LMLoan) ";
		} else {
			sql = "  select rate from lminterestrate a where a.startdate<=current date and a.enddate>=current date   and a.busstype='L' "
					+ " and a.comcode = '86' and a.riskcode = '"
					+ LCPolGrid.getRowColDataByName(i, "riskCode")
					+ "' "
					+ " and int(minpremlimit)<= (select prem from lccont where contno ='"
					+ fm.contNo.value
					+ "' ) "
					+ " and (int(nvl(maxpremlimit,0)) > (select prem from lccont where contno ='"
					+ fm.contNo.value
					+ "') or maxpremlimit is null or maxpremlimit='') "
					+ " and riskcode in (select riskcode from LMLoan) ";
		}
		var result = easyExecSql(sql)
		// alert(result);
		if (result) {
			var rate = "";
			rate = result[0][0];
			LCPolGrid.setRowColDataByName(i, "rate", rate);
		} else {
			alert("获取险种：" + LCPolGrid.getRowColDataByName(i, "riskCode")
					+ "当前贷款利率失败");
		}	
	}
}

// 还款试算提交
function edorRF() {
	// alert(1);
	if (!checkData()) {
		// alert(2);
		return false;
	}
	for (var i = 0; i < LCPolGrid.mulLineCount; i++) {
		LCPolGrid.setRowColDataByName(i, "rfMoney", "");
		LCPolGrid.setRowColDataByName(i, "remark", "");
	}
	fm.GetMoney.value = "";
	// alert(3);
	 var showStr = "正在还款试算，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" +
	 showStr;
	 showInfo = window.showModelessDialog(urlStr, window,
	 "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "PEdorRFBudgetSave.jsp";
	fm.submit();
}

function checkData() {
	if (fm.all('RFDate').value==null || fm.all('RFDate').value==""){
	    alert("预计还款日期不能为空!");
	    return false;
	}
	var checkedCount = 0;
	for (var i = 0; i < LCPolGrid.mulLineCount; i++) {
		if (LCPolGrid.getChkNo(i)) {
			checkedCount++;
		}
	}
	if (checkedCount == 0) {
		alert("请选择险种")
		return false;
	}

	return true;
}

function afterSubmit(flag, content) {
	try {
		showInfo.close();
	} catch (ex) {
	}
	window.focus();
	getSumMoney();
	if (flag == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

function getSumMoney() {
	var sumMoney = 0;
	for (var i = 0; i < LCPolGrid.mulLineCount; i++) {
		if (LCPolGrid.getChkNo(i)) {
//			alert(LCPolGrid.getRowColDataByName(i, "rfMoney"));
			if(LCPolGrid.getRowColDataByName(i, "rfMoney") != ""){
				sumMoney += parseFloat(LCPolGrid.getRowColDataByName(i, "rfMoney"),2);
			}
		}
		fm.GetMoney.value = sumMoney;
	}

}

function tofloat(f, dec) {
	if (dec < 0)
		return "Error:dec<0!";
	result = parseInt(f) + (dec == 0 ? "" : ".");
	f -= parseInt(f);
	if (f == 0)
		for (i = 0; i < dec; i++)
			result += '0';
	else {
		for (i = 0; i < dec; i++)
			f *= 10;
		result += parseInt(Math.round(f));
	}
	return result;
}

function afterSubmit2(FlagStr, Content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else if (FlagStr == "PrintError") {
		// window.parent.close();
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content="
				+ Content;
		showModalDialog(urlStr, window,
				"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}