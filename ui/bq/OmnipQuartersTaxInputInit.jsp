<%
//程序名称：OmnipAnnalsTaxInputInit.jsp
//程序功能：
//创建日期：2009-4-29 18:23:59
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
	var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var operator = "<%=tGI.Operator%>"; //记录当前登录人
	var tCurrentDate = "<%=tCurrentDate%>";
  var tCurrentTime = "<%=tCurrentTime%>";
// 输入框的初始化（单记录部分）
//输入框的初始化 
function initInpBox()
{
  try
  {
    fm.all('ManageCom').value = manageCom;
    var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
    fm.all('ManageComName').value = easyExecSql(sql1);
    fm.all('PolYear').value = "";
  }
  catch(ex)
  {
    alert("在OmnipAnnalsTaxInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}



// 下拉框的初始化


function initForm()
{
  try
  {
		initContGrid();
		initElementtype();
		initInpBox();
  }
  catch(re)
  {
    alert("LLAccModifyInput.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 产品查询信息列表的初始化
function initContGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="管理机构";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[2]=new Array();                                                 
      iArray[2][0]="保单号";         		//列名                     
      iArray[2][1]="70px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[3]=new Array();                                                 
      iArray[3][0]="险种号";         		//列名                     
      iArray[3][1]="70px";            		//列宽                             
      iArray[3][2]=100;            			//列最大值                           
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	  iArray[4]=new Array();                                                 
      iArray[4][0]="投保人";         		//列名                     
      iArray[4][1]="60px";            		//列宽                             
      iArray[4][2]=100;            			//列最大值                           
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
              
      iArray[5]=new Array();
      iArray[5][0]="投保日期";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="结息月度";         		//列名
      iArray[6][1]="70px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="季度结息价值";         	//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="业务员姓名";         	//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="业务员代码";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      iArray[10]=new Array();
      iArray[10][0]="业务员部门";         		//列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="投保人手机号";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="销售渠道";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=3; 
              

      ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
      //这些属性必须在loadMulLine前
      //ContGrid.mulLineCount = 5;   
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      //ContGrid.canSel = 1;
      ContGrid.canChk = 1;
      ContGrid.hiddenPlus = 1;
      ContGrid.hiddenSubtraction = 1;
      ContGrid.loadMulLine(iArray);
      //ContGrid.selBoxEventFuncName ="queryAll";
  
      
      //这些操作必须在loadMulLine后面
      //ContGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>