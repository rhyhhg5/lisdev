<%
//程序名称：PEdorTypeTFInit.jsp
//程序功能：
//创建日期：2008-06-02
//创建人  ：Cz
//更新记录：  更新人    更新日期     更新原因/内容
%>                         

<script language="JavaScript">  

function initForm()
{
  try
  {
    initInpBox();
    initPolGrid();   
    getGrpContInfo(fm.all('GrpContNo').value);
    //checkSelectPol();
  }
  catch(re)
  {
    alert("PEdorTypeTFInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initPolGrid()
{                               
    var iArray = new Array();
      
    try
    {
       iArray[0]=new Array();
       iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      	iArray[0][1]="0px";            		//列宽
    		iArray[0][2]=10;            			//列最大值
  			iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

        iArray[1]=new Array();
        iArray[1][0]="投保人名称";
        iArray[1][1]="200px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="生效日期";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="缴费频次";
        iArray[3][1]="60px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="终止日期";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        
	    PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
	    //这些属性必须在loadMulLine前
	    PolGrid.mulLineCount = 0;   
	    PolGrid.displayTitle = 1;
	    PolGrid.canChk=1;
	    PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	    PolGrid.hiddenSubtraction=1;
	    PolGrid.loadMulLine(iArray);
	    //这些操作必须在loadMulLine后面
    }
    catch(ex)
    {
      alert(ex);
    }
}
            
</script>