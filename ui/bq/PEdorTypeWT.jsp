<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<head >
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeWT.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeWTInit.jsp"%> 
</head>

<body onload="initForm();">
	<form action="./PEdorTypeWTSubmit.jsp" method=post name=fm target="fraSubmit">    
		<table class=common>
			<TR  class= common> 
			  <TD  class= title > 批单号</TD>
			  <TD  class= input > 
			    <input class="readonly" readonly name=EdorNo >
			  </TD>
			  <TD class = title > 批改类型 </TD>
			  <TD class = input >
			  	<input class = "readonly" readonly name=EdorType type=hidden>
			  	<input class = "readonly" readonly name=EdorTypeName>
			  </TD>
			  <TD class = title > 个人保单号 </TD>
			  <TD class = input >
			  	<input class = "readonly" readonly name=ContNo>
			  </TD>   
			</TR>
		</TABLE> 

		<table  class= common>
			<TR  class= common>
				<TD  class= title8>退保日期
				</TD>
				<TD  class= input8>
				  <Input class= "readonly" name=EndDate readonly >
				</TD>
				<TD  class= title8>
				  
				</TD>
				<TD  class= input8>
				  <Input class= "readonly"  readonly >
				</TD>
				<TD  class= title8>
				  
				</TD>
				<TD  class= input8>
				  <Input class= "readonly"  readonly >
				</TD>
			</TR>
			<TR  class= common> 
  				<TD class= title> 退保原因 </TD>
				<TD class= input>
					<input class="code" name="reason_tb" type="hidden">
					<input class="code" name="reason_tbName" elementtype=nacessary readonly ondblclick="return showCodeList('reason_tb', [this,reason_tb],[1,0]);" onkeyup="return showCodeListKey('reason_tb', [this,reason_tb], [1,0]);" verify="退保原因|&code:reason_tb">
				</TD>
			</TR>
		</table> 
		
    	<Div  id= "divPolInfo" style= "display: ''">
    	    <table>
    	        <tr>
    	            <td>
    	                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
    	            </td>
    	            <td class= titleImg>
    	                保单险种信息
    	            </td>
    	        </tr>
    	    </table>
    		<Div  id= "divPolGrid" style= "display: ''">
        		<table  class= common>
        			<tr  class= common>
        		  		<td text-align: left colSpan=1>
        					<span id="spanPolGrid" >
        					</span> 
        			  	</td>
        			</tr>
        		</table>					
    		</Div>
    	</DIV>
    	
    	<!--体检费-->
    	<Div  id= "divTestManage" style= "display: ''">
    	    <table>
    	        <tr>
    	            <td>
    	                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTestGrid);">
    	            </td>
    	            <td class= titleImg>
    	                体检费扣除
    	            </td>
    	        </tr>
    	    </table>
    		<Div  id= "divTestGrid" style= "display: ''">
        		<table  class= common>
        			<tr  class= common>
        		  		<td text-align: left colSpan=1>
        					<span id="spanTestGrid" >
        					</span> 
        			  	</td>
        			</tr>
        		</table>					
    		</Div>
    		<Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();	showCodeName();"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage(); showCodeName();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage(); showCodeName();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage(); showCodeName();"> 
	</Div>
    	</DIV>

    	<table class =common>
			<TR  class= common> 
			  <TD  class= title > <Input type=checkBox name="costCheck" value="1">工本费</TD>
			  <TD  class= input > 
					<Input class= "common" name="cost" value="10"> 元
			  </TD>
			  <TD class = title >  </TD>
			  <TD class = input >
				  
			  </TD>
			  <TD class = title >  </TD>
			  <TD class = input >
			  </TD>   
			</TR>
			<TR class= common>
     	 		<TD  class= title colspan=6> 
					<Input type=button name=save class = cssButton value="保  存" onclick="edorTypeWTSave()">
       		 		<Input  type=Button name=goBack class = cssButton value="返回" onclick="edorTypeWTReturn()">
     	 		</TD>
     	 	</TR>
     	</table>
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="ContType" name="ContType">
			<input type=hidden name="EdorAcceptNo">
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
