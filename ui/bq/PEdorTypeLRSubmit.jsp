<%
//程序名称：PEdorTypeLRSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
    <%@page import="com.sinosoft.lis.bq.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<% 	
				
 CErrors tError = null; 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";  

  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	 transact = request.getParameter("fmtransact");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	String getMoney = request.getParameter("GetMoney");
	String needGetMoney=request.getParameter("NeedGetMoney");	


  // 准备传输数据 VData
  		LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
		tLPEdorItemSchema.setEdorNo(edorNo);
		tLPEdorItemSchema.setContNo(contNo);
		tLPEdorItemSchema.setEdorType(edorType);
		if("Yes".equals(needGetMoney)){//判断是否需要交费
		tLPEdorItemSchema.setGetMoney(getMoney);
		}else{
		tLPEdorItemSchema.setGetMoney(0);
		}
		
	//add by hyy begin	
    //工本费
	LPEdorEspecialDataSchema tLPEdorEspecialDataSchema = null;
	String costCheck = request.getParameter("costCheck");
	if(costCheck != null && costCheck.equals("1"))
	{
	    tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
	    //tLPEdorEspecialDataSchema = new LPEdorEspecialDataSchema();
	    tLPEdorEspecialDataSchema.setEdorAcceptNo(edorNo);
	    tLPEdorEspecialDataSchema.setEdorNo(edorNo);
	    tLPEdorEspecialDataSchema.setEdorType(edorType);
	    tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_GB);
	    tLPEdorEspecialDataSchema.setPolNo(BQ.FILLDATA);
	    tLPEdorEspecialDataSchema.setEdorValue(request.getParameter("cost"));
	}
	//end
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);
	// add by hyy
	tVData.add(tLPEdorEspecialDataSchema) ;
    
	PEdorLRDetailUI tPEdorLRDetailUI = new PEdorLRDetailUI();
	if (!tPEdorLRDetailUI.submitData(tVData, transact))
	{
		VData rVData = tPEdorLRDetailUI.getResult();
		System.out.println("Submit Failed! " + tPEdorLRDetailUI.mErrors.getErrContent());
		Content = transact + "失败，原因是:" + tPEdorLRDetailUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}
	else 
	{
		Content = "保存成功";
		FlagStr = "Success";
	}

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

