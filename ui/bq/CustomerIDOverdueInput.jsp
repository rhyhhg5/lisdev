<!-- 获取session中的登录人员信息 -->
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>

<html>
<%
//程序名称：CustomerIDOverdueInput.jsp
//程序功能：客户证件失效清单下载：
//创建日期：2018-2-24
//创建人  ：fengzhihang
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="CustomerIDOverdueInput.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="CustomerIDOverdueInit.jsp"%>
	</head>
	<body  onload="initForm();" >
  	<form action="./CustomerIDOverdueInputPrint.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">    	
      	<Table  class= common>
	    	<tr>
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary>
				</td>
				<td class= title>保单号</td>
				<td class=input>
					<input class=common name=ContNo>
				</td>
			</tr>
	    	<tr>
	    	    <td class= title>业务员代号</td>
				<td class=input>
					<input class=common name=AgentCode>
				</td>
				<td class= title>客户号</td>
				<td class=input>
					<input class=common name=Customerno>
				</td>
	    	</tr>
		</Table>
      <table  class= common>
	  <input class=cssButton type=button value="查  询" onclick="easyQuery();">
	  <input class=cssButton type=button value="下载清单" onclick="easyPrint();">    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
      	  	<!-- 这里mulitline中加载的时候根据ID取，并且去掉前边的span -->
  					<span id="spanIDOverdueGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
          <Div  id= "divPage2" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
		 </Div>
    	</Div>    	   
  	  	<Div  id= "divZhuShi" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
									<span id="spanZhuShi" >
									     <font color=red>
									              </br></br></br></br></br>
									              
												   以上保单的相关证件已失效，请联系客户变更证件有效期。
					                       </font>				
									
									</span> 
		  				</td>
					</tr>
    	</table>
  	</div>
		<input type=hidden id="sql" name="sql" value="">

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </form>
</body>
</html>
