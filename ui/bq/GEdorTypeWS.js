var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 

//控制是否可以导入被保人清单
function canImport()
{
	
	//有理算成功的则不能再次导入
	var sql = "Select count(1) from lpdiskimport where GrpContNo = '" + fm.all("GrpContNo").value + "' " 
					+ "and EdorNo = '" + fm.all("EdorNo").value + "' " 
					+ "and EdorType = '" + fm.all("EdorType").value + "' "
					+ "and State = '2' " 
					;
	var rs = easyExecSql(sql);
	
	if(rs!='0')
	{
		try{
		fm.doImport.disabled = true;
		fm.doImport.title = '清单中已经有被保人理算不能再次导入';
		fm.save.disabled = true;
		fm.save.title = '清单中已经有被保人理算不能再次导入';
		}catch(e)
		{
			alert('清单中已经有被保人理算不能再次导入');
		}
	}
}

//磁盘导入 
function importInsured()
{
	var url = "./BqDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + 
	          "&EdorType=" + fm.all("EdorType").value +
	          "&GrpContNo=" + fm.all("GrpContNo").value;
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "无名单实名化导入", param);
}

//查询保障计划
function queryPlan()
{
  var planUrl = "../app/ContPlan.jsp?GrpContNo=" + fm.all("GrpContNo").value + 
            "&LoadFlag=3&ContType=4";
  window.open(planUrl);
}

//初始化险种信息和已录入信息
function initQuery()
{	
  var sql = "select SerialNo, State, InsuredName, Sex, Birthday, IdType, IDNo, ContPlanCode, " +
           "    OccupationType, BankCode, AccName, BankAccno, EdorValiDate, OthIdNo,othidtype,importfilename " +
           "from LPDiskImport " +
           "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
           "and EdorNo = '" + fm.all("EdorNo").value + "' " +
           "and EdorType = '" + fm.all("EdorType").value + "' " +
           "and State = '1' " +
           "order by Int(SerialNo) ";
	turnPage1.queryModal(sql, InsuredListGrid); 
}

//查询导入无效的数据
function queryFailData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, BankCode,AccName,BankAccNo, ErrorReason " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '0' " +
	             "order by Int(SerialNo) ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, FailGrid); 
}


//数据提交后的操作
function afterSubmit(flag, content)
{
  try 
  { 
    showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	
	if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		returnParent();
	}
}

//保存申请
function saveEdor()
{
	if (InsuredListGrid.mulLineCount == 0)
	{
		alert("没有有效的导入数据！");
		return false;
	}
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "GEdorTypeWSSubmit.jsp";
	fm.submit();
}

//返回父窗口
function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}