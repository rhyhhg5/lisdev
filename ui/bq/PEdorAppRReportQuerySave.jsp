<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ManuHealthQChk.jsp
//程序功能：人工核保体检资料查询
//创建日期：2005-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  
	tG=(GlobalInput)session.getValue("GI");
  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
	System.out.println("ss");
  String tGrpContNo = "000000000000";
 	String tEdorAcceptNo = request.getParameter("EdorAcceptNo");
 	String tProposalContNo =request.getParameter("EdorAcceptNo");
 	String tCustomerNo=request.getParameter("CustomerNo");
 	String tPrtSeq=request.getParameter("PrtSeq");
 	String tReplyContent = request.getParameter("ReplyContent");
 	
 	
 	LPAppRReportSchema tLPAppRReportSchema = new LPAppRReportSchema();
 	LPAppRReportSet tLPAppRReportSet = new LPAppRReportSet();
 	tLPAppRReportSchema.setReplyContent(tReplyContent);
 	//tLPAppRReportSchema.setProposalEdorAcceptNo(tEdorAcceptNo);
 	tLPAppRReportSchema.setPrtSeq(tPrtSeq);
 	
 	
 	LPAppRReportResultSchema tLPAppRReportResultSchema ;
 	LPAppRReportResultSet tLPAppRReportResultSet = new LPAppRReportResultSet();
 	
 	
	int lineCount = 0;
	String arrCount[] = request.getParameterValues("RReportResultGridNo");

	if(arrCount!=null)
	{
		String tRReportResult[]=request.getParameterValues("RReportResultGrid1");
		String tICDcode[]=request.getParameterValues("RReportResultGrid2");
		
		lineCount = arrCount.length;
		
		for(int i = 0;i<lineCount;i++)
		{
			tLPAppRReportResultSchema = new LPAppRReportResultSchema();
			tLPAppRReportResultSchema.setGrpContNo(tGrpContNo);
			tLPAppRReportResultSchema.setEdorAcceptNo(tEdorAcceptNo);
			tLPAppRReportResultSchema.setProposalContNo(tProposalContNo);
			tLPAppRReportResultSchema.setPrtSeq(tPrtSeq);
			tLPAppRReportResultSchema.setCustomerNo(tCustomerNo);
			tLPAppRReportResultSchema.setRReportResult(tRReportResult[i]);
			tLPAppRReportResultSchema.setICDCode(tICDcode[i]);
			tLPAppRReportResultSet.add(tLPAppRReportResultSchema);
		}
		
		// 准备传输数据 VData
		VData tVData = new VData();
		FlagStr="";
  	
		tVData.add(tG);
		tVData.add(tLPAppRReportResultSet);
		tVData.add(tLPAppRReportSchema);
		
		PEdorAppRReportResultUI tPEdorAppRReportResultUI = new PEdorAppRReportResultUI();
		
		
		try{
			System.out.println("this will save the data!!!");
			tPEdorAppRReportResultUI.submitData(tVData,"");
		}
		catch(Exception ex){
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		
		if (!FlagStr.equals("Fail")){
			tError = tPEdorAppRReportResultUI.mErrors;
			if (!tError.needDealError()){
				Content = " 保存成功! ";
				FlagStr = "Succ";
			}
			else{
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
	else
	{
		FlagStr = "Fail";
		Content = "生调信息录入不完整";
	}
	

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
