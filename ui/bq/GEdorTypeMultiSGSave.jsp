
<%@page import="com.sinosoft.lis.db.LPDiskImportDB"%><%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：GEdorTypeMultiDetailSave.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
    //接收信息，并作校验处理
                 
    String FlagStr = "";
    String Content = "";
    String transact = "";
    String Result="";
    //批单号
    String EdorNo="";
    //保单号
    String GrpContNo="";
    //批改类型
    String EdorType="";
    
    EdorNo = request.getParameter("EdorNo");
    GrpContNo = request.getParameter("GrpContNo");
    EdorType = request.getParameter("EdorType");
    
    LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
    tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
    tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
    
    
    TransferData tTransferData = new TransferData(); 
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
    
    
	LPDiskImportDB lDiskImportDB = new LPDiskImportDB();
	LPDiskImportSet lpDiskImportSet = new LPDiskImportSet();
    
	LPDiskImportSet tImportSet = new LPDiskImportSet();
	
	
    //得到列表中的数据条数
    String arrCount[] = request.getParameterValues("LCInsured2GridNo");
    System.out.println("arrCount[]"+arrCount.length);
    if(arrCount!=null){
    	String tContNo[] = request.getParameterValues("LCInsured2Grid1");//保单号
    	String tInsuredNo[] = request.getParameterValues("LCInsured2Grid2");//客户号
    	//String tName[] = request.getParameterValues("LCInsured2Grid3");//姓名
    	//String tSex[] = request.getParameterValues("LCInsured2Grid4");//性别
    	//String tIdNo[] = request.getParameterValues("LCInsured2Grid5");//证件号码
    	//String tBirthday[] = request.getParameterValues("LCInsured2Grid6");//生效日期
    	String tMoney2[] = request.getParameterValues("LCInsured2Grid7");//归属比例
    	//String tGRDW[] = request.getParameterValues("LCInsured2Grid8");//个人账户单位交费余额
    	//String tGRGR[] = request.getParameterValues("LCInsured2Grid9");//个人账户个人交费余额
    	
    	String tChk[] = request.getParameterValues("InpLCInsured2GridChk");	//Chkbox选项
	    	 System.out.println("tChk[]"+tChk.length);
	    	if (tChk!=null&&tChk.length>0){
	    		  for(int i=0;i<tChk.length;i++){
	    			  System.out.println("111");
	    			  System.out.println("tChk[i]"+tChk[i]);
	    			  if(tChk[i].equals("1")) { 
	    				  System.out.println("2222");
	    				  LPDiskImportSchema lDiskImportSchema = new LPDiskImportSchema();
	    				  lDiskImportDB.setEdorNo(EdorNo);
	    				  lDiskImportDB.setEdorType(EdorType);
	    				  lDiskImportDB.setGrpContNo(GrpContNo);
	    				  System.out.println("tInsuredNo[i]"+tInsuredNo[i]);
	    				  lDiskImportDB.setInsuredNo(tInsuredNo[i]);
	    				  lpDiskImportSet = lDiskImportDB.query();
	    				  
	    				  if(lpDiskImportSet.size()>0){
	    					  lDiskImportSchema =  lpDiskImportSet.get(1);
	    					  System.out.println("tMoney2[i]"+tMoney2[i]);
	    					  lDiskImportSchema.setMoney2(tMoney2[i]);
	    					  
	    				  }
	    				  tImportSet.add(lDiskImportSchema);
	    		      }
	    		  }
	    	}
	    }
    
	    GEdorTypeMultiTQUI tGEdorTypeMultiTQUI   = new GEdorTypeMultiTQUI();
	  try {
	    // 准备传输数据 VData
	  	VData tVData = new VData();  
		tVData.addElement(tG);
		tVData.addElement(tImportSet);
		tVData.add(tLPGrpEdorItemSchema);
		tGEdorTypeMultiTQUI.submitData(tVData, "");
		}
		catch(Exception ex) {
			Content = transact+"失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
		}			
		
	  //如果在Catch中发现异常，则不从错误类中提取错误信息
	  if (FlagStr=="") {
	    CErrors tError = new CErrors(); 
	    tError = tGEdorTypeMultiTQUI.mErrors;
	    
	    if (!tError.needDealError()) {                          
	      Content = " 保存成功";
	    	FlagStr = "Success";
	    	
	    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))	{}
	    }
	    else {
	    	Content = " 保存失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	  }

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>");
</script>
</html>

