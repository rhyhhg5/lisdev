<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="GEdorQuery.js"></SCRIPT>
  <SCRIPT src="PEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorQueryInit.jsp"%>
    <%@include file = "ManageComLimit.jsp"%>

  <title>个人批改查询 </title>
</head>
<body  onload="initForm();" >

  <form action="./GEdorQueryOut.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGEdor1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLGEdor1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            批单号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>
        </TR>
        <TR class=common>
          <TD  class= title>
            集体保单号
          </TD>
          <TD  class= input>
            <Input class= common name=GrpContNo >
          </TD>
        </TR>
      </table>
    </Div>
          <INPUT VALUE="查询" TYPE=button class=cssButton onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button class=cssButton onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLGEdor2);">
    		</td>
    		<td class= titleImg>
    			 个人批改信息
    		</td>
    	</tr>
    </table>
    <div id = "divdetail" style = "display:none">
          <INPUT VALUE="查看明细" TYPE=button class=cssButton onclick="detailEdor();"> 
 		</div> 
  	<Div  id= "divLGEdor2" style= "display: ''" align = "center">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanGEdorMainGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button class=cssButton onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button class=cssButton onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button class=cssButton onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button class=cssButton onclick="getLastPage();"> 					
  	</div>
  	<input type=hidden id="EdorType" name="EdorType">
  	<input type=hidden id="ContType" name="ContType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
