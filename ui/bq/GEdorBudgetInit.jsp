<% 
//程序名称：GEdorBudgetInit.jsp
//程序功能：退保试算
//创建日期：2006-04-19
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String currentDate = PubFun.getCurrentDate();
  String grpContNo = request.getParameter("grpContNo");
  if(grpContNo == null || grpContNo.equals("null"))
  {
    grpContNo = "";
  }
  
  String edorNo = request.getParameter("edorNo");
  if(edorNo == null || edorNo.equals("null"))
  {
    edorNo = "";
  }
  
  String edorType = request.getParameter("edorType");
  if(edorType == null || edorType.equals("null"))
  {
    edorType = "";
  }
  
  String edorValiDate = request.getParameter("edorValiDate");
  if(edorValiDate == null || edorValiDate.equals("null"))
  {
    edorValiDate = "";
  }
%>

<script language="JavaScript">  
function initForm()
{
  if("<%=edorValiDate%>" != "")
  {
    fm.all("divEdorValiDate").style.display = "none";
    fm.all("divEdorValiDatePassIn").style.display = "";
  }
  else
  {
    fm.all("divEdorValiDate").style.display = "";
    fm.all("divEdorValiDatePassIn").style.display = "none";
  }
  
  var edorValiDate = "<%=edorValiDate%>";
  if(edorValiDate == "")
  {
    edorValiDate = "<%=currentDate%>";
  }
  fm.grpContNoTop.value = "<%= grpContNo%>";
  fm.edorType.value = "<%=edorType%>";
  fm.edorValiDate.value = edorValiDate;
  fm.edorValiDatePassIn.value = edorValiDate;
  fm.edorNo.value = "<%=edorNo%>";
  
  initLCGrpContGrid();
  initLCGrpPolGrid();
  
  if(fm.edorType.value != "" || fm.grpContNoTop.value != "")
  {
    queryGrpCont();
  }
}

//初始化保单列表
function initLCGrpContGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=0;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";
    iArray[1][1]="60px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    iArray[1][21]="grpContNo";
    
    iArray[2]=new Array();
    iArray[2][0]="单位名称";
    iArray[2][1]="160px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][21]="grpName";
    
    iArray[3]=new Array();
    iArray[3][0]="客户号";
    iArray[3][1]="60px";
    iArray[3][2]=100;
    iArray[3][3]=0;
    iArray[3][21]="appntNo";
        
    iArray[4]=new Array();
    iArray[4][0]="投保日期";
    iArray[4][1]="60px";
    iArray[4][2]=100;
    iArray[4][3]=3;
    iArray[4][21]="polApplyDate";
    
    iArray[5]=new Array();
    iArray[5][0]="生效日期";
    iArray[5][1]="60px";
    iArray[5][2]=100;
    iArray[5][3]=0;
    iArray[5][21]="cValiDate";
    
    iArray[6]=new Array();
    iArray[6][0]="交至日期";
    iArray[6][1]="60px";
    iArray[6][2]=100;
    iArray[6][3]=3;
    iArray[6][21]="payToDate";
        
    iArray[7]=new Array();
    iArray[7][0]="满期日期";
    iArray[7][1]="60px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    iArray[7][21]="cInValiDate";
    
    iArray[8]=new Array();
    iArray[8][0]="缴费频次";
    iArray[8][1]="50px";
    iArray[8][2]=100;
    iArray[8][3]=3;
    iArray[8][21]="payIntv";
    
    iArray[9]=new Array();
    iArray[9][0]="期交保费";
    iArray[9][1]="60px";
    iArray[9][2]=100;
    iArray[9][3]=3;
    iArray[9][21]="prem";
    
    LCGrpContGrid = new MulLineEnter( "fm" , "LCGrpContGrid" ); 
    //这些属性必须在loadMulLine前
    LCGrpContGrid.mulLineCount = 0;   
    LCGrpContGrid.displayTitle = 1;
    LCGrpContGrid.canSel=1;       
    //PolGrid.canChk=1;
    LCGrpContGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    LCGrpContGrid.hiddenSubtraction=1;
    LCGrpContGrid.loadMulLine(iArray);
    LCGrpContGrid.selBoxEventFuncName ="setGrpContInfoOnClick" ; 
  
  }
  catch(ex)
  {
    alert(ex.message);
  }
}

//初始化险种列表
function initLCGrpPolGrid()
{
  var iArray = new Array();
      
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=0;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保障计划";
    iArray[1][1]="60px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    iArray[1][21]="contPlanCode";
        
    iArray[2]=new Array();
    iArray[2][0]="险种序号";
    iArray[2][1]="50px";
    iArray[2][2]=100;
    iArray[2][3]=0;
    iArray[2][21]="riskSeqNo";
    
    iArray[3]=new Array();
    iArray[3][0]="代码";
    iArray[3][1]="30px";
    iArray[3][2]=100;
    iArray[3][3]=0;
    iArray[3][21]="riskCode";
    
    iArray[4]=new Array();
    iArray[4][0]="险种名称";
    iArray[4][1]="130px";
    iArray[4][2]=100;
    iArray[4][3]=0;
    iArray[4][21]="riskName";
    
    iArray[5]=new Array();
    iArray[5][0]="被保人数";
    iArray[5][1]="50px";
    iArray[5][2]=100;
    iArray[5][3]=0;
    iArray[5][21]="insuredCount";
        
    iArray[6]=new Array();
    iArray[6][0]="生效日期";
    iArray[6][1]="60px";
    iArray[6][2]=100;
    iArray[6][3]=0;
    iArray[6][21]="cValiDate";
    
    iArray[7]=new Array();
    iArray[7][0]="保额";
    iArray[7][1]="50px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    iArray[7][21]="amnt";
    
    iArray[8]=new Array();
    iArray[8][0]="档次";
    iArray[8][1]="30px";
    iArray[8][2]=100;
    iArray[8][3]=0; 
    iArray[8][21]="mult"; 
    
    iArray[9]=new Array();
    iArray[9][0]="期交保费";
    iArray[9][1]="55px";
    iArray[9][2]=100;
    iArray[9][3]=0; 
    iArray[9][21]="prem";
    
    iArray[10]=new Array();
    iArray[10][0]="总保费";
    iArray[10][1]="50px";
    iArray[10][2]=100;
    iArray[10][3]=0; 
    iArray[10][21]="sumrem";
    
    iArray[11]=new Array();
    iArray[11][0]="满期日期";
    iArray[11][1]="60px";
    iArray[11][2]=100;
    iArray[11][3]=0;  
    iArray[11][21]="cInValiDate";
    
    iArray[12]=new Array();
    iArray[12][0]="交费年期";
    iArray[12][1]="55px";
    iArray[12][2]=100;
    iArray[12][3]=0;  
    iArray[12][21]="payEndYear";
    
    iArray[13]=new Array();
    iArray[13][0]="交费频次";
    iArray[13][1]="55px";
    iArray[13][2]=100;
    iArray[13][3]=0;  
    iArray[13][21]="payIntv";

    iArray[14]=new Array();
    iArray[14][0]="理赔金额";
    iArray[14][1]="55px";
    iArray[14][2]=100;
    iArray[14][3]=0;  
    iArray[14][21]="claimPay";
    
    iArray[15]=new Array();
    iArray[15][0]="理赔率";
    iArray[15][1]="50px";
    iArray[15][2]=100;
    iArray[15][3]=0;  
    iArray[15][21]="claimPayRate";
    
    iArray[16]=new Array();
    iArray[16][0]="退保金额";
    iArray[16][1]="55px";
    iArray[16][2]=100;
    iArray[16][3]=0;  
    iArray[16][21]="getMoney";
    
    iArray[17]=new Array();
    iArray[17][0]="备注";
    iArray[17][1]="40px";
    iArray[17][2]=100;
    iArray[17][3]=0;  
    iArray[17][21]="remark";
        
    LCGrpPolGrid = new MulLineEnter( "fm" , "LCGrpPolGrid" ); 
    //这些属性必须在loadMulLine前
    LCGrpPolGrid.mulLineCount = 0;   
    LCGrpPolGrid.displayTitle = 1;
    LCGrpPolGrid.canSel=0;       
    LCGrpPolGrid.canChk=1;
    LCGrpPolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    LCGrpPolGrid.hiddenSubtraction=1;
    LCGrpPolGrid.loadMulLine(iArray);
    LCGrpPolGrid.selBoxEventFuncName ="" ; 
  
  }
  catch(ex)
  {
    alert(ex.message);
  }
}

</script>