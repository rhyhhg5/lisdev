//业务类型改变时显示相应的信息录入页面
function afterCodeSelect()
{
	var payMode = fm.all("PayMode").value
	if ((payMode == "1") || (payMode == "2") || (payMode == "3")) //自行缴费
	{			
		if (fm.fmtransact.value == "1") //交费
		{
			document.all("trEndDate").style.display = "";
			fm.EndDate.verify = "截止日期|notnull&date";
		}
	  else //退费不设日期
	  {
	  	document.all("trEndDate").style.display = "none";
	  	fm.EndDate.verify = "";
	  }
		document.all("trPayDate").style.display = "none";
		fm.PayDate.verify = "";
		document.all("trBank").style.display = "none";
		document.all("trAccount").style.display = "none";
	}
	else if (payMode == "4") //银行转帐
	{
		document.all("trEndDate").style.display = "none";
		fm.EndDate.verify = "";
		document.all("trPayDate").style.display = "";
		fm.PayDate.verify = "转帐日期|notnull&date"
		document.all("trBank").style.display = "";
		document.all("trAccount").style.display = "";
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
	try { showInfo.close(); } catch(re) { }

	if (fm.all("printMode")[0].checked)
	{
		//	top.opener.parent.fraPrint.F1Book1.book.filePrint(true);
		var width = screen.availWidth - 10;
	  var height = screen.availHeight - 28;
	  var parma = "top=0,left=0,width="+width+",height="+height;
		window.open("ShowEdorPrint.jsp?EdorAcceptNo="+mEdorAcceptNo, "", parma);
	}   
	else if (fm.all("printMode")[1].checked) //批量打印 暂时没有实现
	{   
	}
	else if (fm.all("printMode")[2].checked)
	{   
	}
	
	try
	{
		top.close();
		top.opener.top.close();
		//top.opener.top.opener.top.close();
	}
	catch(re) { }
}

function boforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if (fm.PayMode.value == "4")
  {
    if (fm.Bank.value == "")
    {
      alert("转帐银行为空不能选择银行转帐！");
      return false;
    }
    if (fm.BankAccno.value == "")
    {
      alert("转帐帐号为空不能选择银行转帐！");
      return false;
    }
    if (compareDate(fm.PayDate.value, getCurrentDate()) == 2)
    {
      alert("转帐日期不能小于今天！");
      return false;
    }
  }
  if ((fm.PayMode.value == "1") && (fm.fmtransact.value == "1"))
  {
    if (compareDate(fm.EndDate.value, getCurrentDate()) == 2)
    {
      alert("截止日期不能小于今天！");
      return false;
    }
  }
  return true;
}

提交
function submitForm()
{
  if (!boforeSubmit())
  {
    return false;
  }
	fm.submit();
}

//费用为0则不设交费方式
function queryMenoy(edorAcceptNo)
{
	var	strSql = "select GetMoney from LPEdorMain where EdorAcceptNo = '" + edorAcceptNo + "'";
	var arrResult = easyExecSql(strSql);
	var getMoney = arrResult[0][0];
	if (getMoney == null)
	{
		return false;
	}
	if (getMoney == 0)
	{
		document.all("trPayMode").style.display = "none";
		document.all("trEndDate").style.display = "none";
		document.all("trPayDate").style.display = "none";
		document.all("trBank").style.display = "none";
		document.all("trAccount").style.display = "none";
		fm.PayMode.verify = "";
		fm.EndDate.verify = "";
		fm.PayDate.verify = "";
	}
	else if (getMoney > 0) //交费
	{
		var accMoney=queryAccMenoy(edorAcceptNo);
		if(accMoney=='')
		{
			alert('查询帐户余额表出错！');
			return false;
		}

		if(Number(accMoney)*-1>=Number(getMoney))
		{
			document.all("trPayMode").style.display = "none";
			document.all("trEndDate").style.display = "none";
			document.all("trPayDate").style.display = "none";
			document.all("trBank").style.display = "none";
			document.all("trAccount").style.display = "none";
			fm.PayMode.verify = "";
			fm.EndDate.verify = "";
			fm.PayDate.verify = "";			
		}else
		{
			document.all("FeeFlag").style.display = "";
		}
		fm.fmtransact.value = "1";
	}
	else //退费
	{
		var accMoney=queryAccMenoy(edorAcceptNo);
		if(accMoney=='')
		{
			alert('查询帐户余额表出错！');
			return false;
		}

		if(Number(accMoney)>0)
		{
			document.all("trPayMode").style.display = "none";
			document.all("trEndDate").style.display = "none";
			document.all("trPayDate").style.display = "none";
			document.all("trBank").style.display = "none";
			document.all("trAccount").style.display = "none";
			fm.PayMode.verify = "";
			fm.EndDate.verify = "";
			fm.PayDate.verify = "";			
		}
		fm.fmtransact.value = "0";
	}
}

//费用为0则不设交费方式
function queryAccMenoy(edorAcceptNo)
{
	var	strSql = "select coalesce(sum(money),0) from lcappacctrace where otherno = '" + edorAcceptNo + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult==null)
	{
		return '';
	}
	var getMoney = arrResult[0][0];
	return getMoney;
}

//得到交费记录号
function getNoticeNo()
{
	var strSql = "select getnoticeno from LJSGetEndorse a, LPEdorMain b " +
	             "where a.EndorSementNo = b.EdorNo and b.EdorAcceptNo = '" + mEdorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.GetNoticeNo.value = arrResult[0][0];
	}
}

//得到交费银行和帐号
function queryAccount()
{
	var strSql = "select a.BankCode, a.BankAccno, a.AccName " +
				 "from LCCont a, LPEdorMain b " +
				 "where a.ContNo = b.ContNo " +
				 "and   b.EdorAcceptNo = '" + mEdorAcceptNo + "' " +
				 "union " +
				 "select a.BankCode, a.BankAccno, a.AccName " +
				 "from LBCont a, LPEdorMain b " +
				 "where a.ContNo = b.ContNo " +
				 "and   b.EdorAcceptNo = '" + mEdorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.all("Bank").value = arrResult[0][0];
		fm.all("BankName").value = arrResult[0][0];
		fm.all("BankAccno").value = arrResult[0][1];
		fm.all("AccName").value = arrResult[0][2];
		showOneCodeName("Bank", "BankName");  
	}
}

//function cancel()
//{
//    top.opener.top.opener.focus();
//    top.opener.top.opener.window.location.reload();
//    top.opener.top.close();
//    top.close();
//}