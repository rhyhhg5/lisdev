<%
//程序名称：PEdorTypeCMSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  String flag = "";
  String content = "";
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  
  String operator = request.getParameter("Operator");
  String edorNo = request.getParameter("EdorNo");
  String appntNo = request.getParameter("NewAppntNo");
  String tContNo =request.getParameter("ContNo");
  String[] splitPolChk = request.getParameterValues("InpOldPolGridChk");
  String[] InsuredNo = request.getParameterValues("OldPolGrid1");
   
   LCInsuredSet tLCInsuredSet = new LCInsuredSet();
  if (operator.equals("SPLIT"))
  {
    for (int i = 0; i < splitPolChk.length; i++)
    {
      if(splitPolChk[i].equals("1"))  
      { 
      LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
      tLCInsuredSchema.setContNo(tContNo);
      tLCInsuredSchema.setInsuredNo(InsuredNo[i]);
      tLCInsuredSchema.setAppntNo(appntNo);
      tLCInsuredSet.add(tLCInsuredSchema);
      }
      if(splitPolChk[i].equals("0")) 
      {     
       System.out.println("第"+ i +"行未被选中");
            continue;
      }
    }
  }
  if (operator.equals("DEL"))
  {}

  VData data = new VData();
  data.add(gi);
  data.add(tLCInsuredSet);
  PEdorCFDetailUI tPEdorCFDetailUI = new PEdorCFDetailUI(edorNo,appntNo,tContNo);
  if (!tPEdorCFDetailUI.submitData(data, operator))
  {
    content = "数据保存失败！原因是：" + tPEdorCFDetailUI.getError();
    flag = "Fail";
  }
  else
  {
    content = "处理成功";
  	flag = "Success";
  }
  
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSplit("<%=flag%>", "<%=content%>");
</script>
</html>