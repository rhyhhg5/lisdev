var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var mIdNo=null;
var mName=null;
var mSex=null;
var mBirthday=null;

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if(!checkDate())
  {
    return false;
  }
  if(!checkQuJian())
  {
    return false;
  }
  return true;
}

function checkQuJian()
{
     var date=fm.all('StopDate').value;
     var grpContNo=fm.all('GrpContNo').value;
     var sql = "select startdate,enddate from lcgrpcontstate where grpcontno='" + fm.all('GrpContNo').value + "' and statetype='Stop' ";
	 var arrResult = easyExecSql(sql);
	 if (arrResult)
	 {   
	    for(i=0;i<arrResult.length;i++)
	    {
		       if(date>=arrResult[i][0]&&date<=arrResult[i][1])
			  {
			    alert("暂停时间不能选在曾经暂停过的时间["+arrResult[i][0]+","+arrResult[i][1]+"]之内");
			    return false;
			  }
		 
	    }
	    return true;
		
	}
	return true;
	  
	  
}


function checkDate()
{
    var date=fm.all('StopDate').value;
    var sql = "select cvalidate,cinvalidate from lcgrpcont where grpcontno='" + fm.all('GrpContNo').value + "' ";
	var arrResult = easyExecSql(sql);
	if (arrResult)
	{
		if(date>arrResult[0][0]&&date<arrResult[0][1])
		{
		   return true;
		}
		else
		{
		  alert("暂停时间必须在保单的保障区间内["+arrResult[0][0]+","+arrResult[0][1]+"]");
		  return false;
		}
	}
	alert("没有查询到团单相应的信息");
	return false;
}

function edorTypeRSSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmtransact').value = "INSERT||MAIN";
	fm.submit();

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content,Result )
{
	try{ showInfo.close(); } catch(e) {}
	window.focus();
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var tTransact=fm.all('fmtransact').value; 
		
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
			 
			returnParent();
	  
	}   
}     
                 
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}


function returnParent()
{
	try
	{
      top.opener.focus();
	  top.opener.getGrpEdorItem();
	}
	catch (ex) {}
	top.close();
}

