//               该文件中包含客户端需要处理的函数和事件
var showInfo1;
var mDebug = "1";
var iArray;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPagePrem = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPageLonePrem = new turnPageClass();
var turnPageAcc = new turnPageClass();
var turnPageSumLoanPrem = new turnPageClass();
var turnPagePolState = new turnPageClass();
var feeName1="初始扣费比例1";
var feeName2="初始扣费比例2";
var turnPage2 = new turnPageClass();
function returnParent() {
	try {
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {
	}
}
function save() {
	//if (!checkAppMoney()) {
	//	return false;
	//}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo1 = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "deal";
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, transact )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
		if(transact=="init"){
			queryLoanPremInfo(); 
			//alert("有欠交保费，请先进行补交保费！");
			
		}
		else{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  		returnParent();
		}
	  
	}
}
function healthInput() {
	if (!selInsured()) {
		alert("\u8bf7\u9009\u62e9\u9669\u79cd\u540e\u518d\u505a\u64cd\u4f5c\uff01");
		return false;
	}
	if (!chkInsured()) {
		alert("\u4e00\u6b21\u53ea\u80fd\u9009\u62e9\u4e00\u4e2a\u88ab\u4fdd\u4eba\uff01");
		return false;
	}
	var sql = "select 1 from LPCustomerImpart where EdorType ='RV' and EdorNo ='" + fm.all("EdorNo").value + "' and CustomerNoType = '1' and CustomerNo ='" + iArray[0] + "'";
	var result = easyExecSql(sql);
	if (result != null) {
		alert("\u60a8\u597d\uff0c\u8be5\u88ab\u4fdd\u4eba\u5728\u6b64\u6b21\u4fdd\u5168\u4e2d\u5df2\u7ecf\u5f55\u5165\u8fc7\u5065\u5eb7\u544a\u77e5\uff0c\u4e0d\u9700\u518d\u5f55\u5165\uff01");
		return false;
	} else {
		var win = window.open("PEdorHealthImpartMain.jsp?EdorNo=" + fm.all("EdorNo").value + "&EdorType=" + fm.all("EdorType").value + "&ContNo=" + fm.all("ContNo").value + "&InsuredNo=" + iArray[0] + "&Flag=1", "", "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0");
		win.focus();
	}
}
//查询失效险种信息
function getPolInfo(tContNo) {    
    // 书写SQL语句 查询条件LCContState表StateType = 'Acaliable'，State = '1'
	var strSQL = " select b.riskSeqNo," + "  (select riskName from LMRisk a where a.riskCode = b.riskCode), " + "  b.insuredName, b.amnt, b.mult, b.prem, codeName('stateflag', b.StateFlag),b.cValiDate, b.EndDate,b.PayToDate,b.polno,b.insuredno " + " from LCPol b " + " where b.contno ='" + tContNo + "' ";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
	if (!turnPage.strQueryResult) {
		return false;
	}
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
	turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
	turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}
//一次只能有一个被保人录入
function chkInsured() {
	var temp = iArray[0];
	for (i = 1; i < iArray.length; i++) {
		if (temp != iArray[i]) {
			return false;
		}
	}
	return true;
}
//取得选择的被保人号
function selInsured() {
	count = 0;
	iArray = new Array();
	//取出选中的保单中的被保人号
	for (i = 0; i < PolGrid.mulLineCount; i++) {
		if (PolGrid.getChkNo(i)) {
			iArray[count] = PolGrid.getRowColData(i, 12);
			count++;
		}
	}
	if (count == 0) {
		return false;
	} else {
		return true;
	}
}
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,72,*";
	}
}
function checkAppMoney() {
	var appMoney = fm.AppMoney.value;	
	if (appMoney < 1000) {
			alert("\u8ffd\u52a0\u4fdd\u8d39\u4e0d\u80fd\u4f4e\u4e8e1000\u5143");
			return false;
	} else {
			if ((appMoney % 100) != 0) {
				alert("\u8ffd\u52a0\u4fdd\u8d39\u5fc5\u987b\u4e3a100\u5143\u7684\u6574\u6570\u500d");
				return false;
			}
	}
	
	return true;
}
function initPremInfo()
{
	  try
  {   //(select a.codename from ldcode a where a.codetype='payintv' and a.code=p.payintv )
	   var tSQL="select (select max(paydate) from ljapay where incomeno=p.contno),prem,(select a.codename from ldcode a where a.codetype='payintv' and a.code=char(p.payintv)),FIRSTPAYDATE,6000*12/payintv from lccont p "
	           +"where p.contno='"+fm.all('ContNo').value+"'";   
	    turnPagePrem.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
	    turnPagePrem.arrDataCacheSet = decodeEasyQueryResult(turnPagePrem.strQueryResult);
	    fm.all('LastPayDate').value =turnPagePrem.arrDataCacheSet[0][0];
      fm.all('Prem').value = turnPagePrem.arrDataCacheSet[0][1];      
      fm.all('PayFreq').value = turnPagePrem.arrDataCacheSet[0][2];
      fm.all('PayStartDate').value = turnPagePrem.arrDataCacheSet[0][3];
      var feeMoney=turnPagePrem.arrDataCacheSet[0][4];
      feeName1=feeMoney+"元保费以下部分扣费比例";
      feeName2=feeMoney+"元保费以上部分扣费比例";
  }
  catch(re)
  {
    alert("初始化保费信息错误!");
  }

	    
}

function initAccInfo() {
	try {
		var tSQL = "select baladate,InsuAccBala from lcinsureacc " + "where contno='" + fm.all("ContNo").value + "'";
		turnPageAcc.strQueryResult = easyQueryVer3(tSQL, 1, 0, 1);
		turnPageAcc.arrDataCacheSet = decodeEasyQueryResult(turnPageAcc.strQueryResult);
		fm.all("LastAccBalDate").value = turnPageAcc.arrDataCacheSet[0][0];
		fm.all("AccBala").value = turnPageAcc.arrDataCacheSet[0][1];
	
	}
	catch (re) {
		alert("\u521d\u59cb\u5316\u5e10\u6237\u4fe1\u606f\u9519\u8bef!");
	}
	var strSQL = "select edorvalue from LPEdorEspecialData where  edorAcceptno='"+fm.all('EdorAcceptNo').value+"' and edorType='RV' and detailType='RV_APPMONEY' with ur";
		var tmoney = easyExecSql(strSQL);
		
		if(tmoney!=null){
		
		fm.AppMoney.value=tmoney;
		}
}
function initPayState() {
	try {
		var tPolStateSQL = "select (select a.codename from ldcode a where a.codetype='paystate' and a.code=value(p.StandbyFlag1,'0') ),p.paytodate,p.payintv,p.polno,(select insuaccno from lmrisktoacc where riskcode=p.riskcode),value(p.StandbyFlag1,'0') from  lcpol p where  p.ContNo='" + fm.all("ContNo").value + "' and polno=mainpolno";
		turnPagePolState.strQueryResult = easyQueryVer3(tPolStateSQL, 1, 0, 1);
		turnPagePolState.arrDataCacheSet = decodeEasyQueryResult(turnPagePolState.strQueryResult);
		fm.all("PayState").value = turnPagePolState.arrDataCacheSet[0][0];
		fm.PayToDate.value = turnPagePolState.arrDataCacheSet[0][1];
		fm.PayIntv.value = turnPagePolState.arrDataCacheSet[0][2];
		fm.PolNo.value = turnPagePolState.arrDataCacheSet[0][3];
		fm.InsuAccNo.value = turnPagePolState.arrDataCacheSet[0][4];
		fm.PayFlag.value = turnPagePolState.arrDataCacheSet[0][5];
		//alert(fm.PayFlag.value);
	}
	catch (re) {
		alert("\u521d\u59cb\u5316\u7f34\u8d39\u72b6\u6001\u4fe1\u606f\u9519\u8bef!");
	}
	try {
		isLoanState();
	}
	catch (re) {
		alert("\u67e5\u8be2\u6b20\u8d39\u4fe1\u606f\u9519\u8bef!");
    }
}
function isLoanState() {
	var tPayFlag = fm.all("PayFlag").value;
	var tPayToDate = fm.PayToDate.value;
	var tCurrDate = getCurrentDate();
	var tDayDiff = dateDiff(tPayToDate, tCurrDate, "D");
    //说明到目前保单已经欠费,且保单处于自动缓交 或者 缓交状态
    //alert(tPayFlag);
   	if (tDayDiff > 0 && (tPayFlag == "2" || tPayFlag == "1")) {
		
		fm.fmtransact.value = "init";
		fm.submit();
	} 
	
}
/**查询欠费信息*/
function queryLoanPremInfo() {
	var tEdorNo = fm.all("EdorNo").value;
	//var tEdorValiDate = fm.all("EdorValiDate").value;
	var tContNo = fm.all("ContNo").value;
	var strSQL = "select '',sum(SumDuePayMoney),'','',paycount from ljspayperson where contno='" + tContNo + "' group by paycount";
	turnPageLonePrem.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
    
	if (!turnPageLonePrem.strQueryResult) {
		return false;
	}
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
	turnPageLonePrem.arrDataCacheSet = decodeEasyQueryResult(turnPageLonePrem.strQueryResult);
    //设置初始化过的MULTILINE对象
	turnPageLonePrem.pageDisplayGrid = LoanPremGrid;    
    //保存SQL语句
	turnPageLonePrem.strQuerySql = strSQL; 
    //设置查询起始位置
	turnPageLonePrem.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPageLonePrem.getData(turnPageLonePrem.arrDataCacheSet, turnPageLonePrem.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPageLonePrem.pageDisplayGrid);  
    
    //分别对相应字段赋值
	var tStartDate = fm.all("PayStartDate").value;
	var tPayIntv = 0;
	var tSumLoanPrem = Number(0);
	tPayIntv = fm.PayIntv.value;
	
	for (var t = 0; t < LoanPremGrid.mulLineCount; t++) {	
		tSumLoanPrem += Number(LoanPremGrid.getRowColData(t, 2));    	
		var tPayTimes = LoanPremGrid.getRowColData(t, 5);
    	
		LoanPremGrid.setRowColData(t, 1, tPayTimes + "");
		strSQL = "select rate from rate330801 where payintv=" + tPayIntv + " and " + tPayTimes + " between mincount and maxcount order by type";
		var rate = easyExecSql(strSQL, 1, 0, 1);
    	
		LoanPremGrid.setRowColData(t, 3, rate[0][0]);
		LoanPremGrid.setRowColData(t, 4, rate[1][0]);
    	//通过查询描叙表去查询每一期的交费比例,暂时没开发
    	//LoanPremGrid.setRowColData(t,2,100);
	}
   
	//fm.all("AppMoney").value = tSumLoanPrem;
	fm.all("PayMoneyFee").value = tSumLoanPrem;
}
function InsuAccTraceQuery() {
	
	var tPolNo = fm.PolNo.value;
	var tInsuAccNo = fm.InsuAccNo.value;
	window.open("../bq/OmnipotenceAcc.jsp?PolNo=" + tPolNo + "&InsuAccNo=" + tInsuAccNo);
}

