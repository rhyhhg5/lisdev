<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListZT.jsp
//程序功能：
//创建日期：2006-02-20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<script>
<%
  String flag = "";
  String content = "";
  
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  String prtNo = request.getParameter("PrtNo");

  PEdorBodyCheckPrintUI tPEdorBodyCheckPrintUI = new PEdorBodyCheckPrintUI(gi, prtNo);
  if (!tPEdorBodyCheckPrintUI.submitData())
  {
    out.print("alert('通知书显示错误!原因是" + tPEdorBodyCheckPrintUI.getError() +"');");
  }
	
  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
  ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
  CombineVts combineVts = new CombineVts(tPEdorBodyCheckPrintUI.getInputStream(), templatePath);
  combineVts.output(dataStream);
  session.putValue("PrintVts", dataStream);
	session.putValue("PrintStream", tPEdorBodyCheckPrintUI.getInputStream());
	response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
%>
</script>