<%
//程序名称：PEdorTypeBDSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----BDsubmit---");
  LPPolSchema tLPPolSchema   = new LPPolSchema();
  LPDutySchema tLPDutySchema = new LPDutySchema();   
  
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
  PEdorBDDetailUI tPEdorBDDetailUI   = new PEdorBDDetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact:"+transact);
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  
  //个人保单批改信息
  tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
  tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
	
  if (transact.equals("INSERT||MAIN")) {
     tLPPolSchema.setPolNo(request.getParameter("PolNo"));
     tLPPolSchema.setEdorNo(request.getParameter("EdorNo"));
     tLPPolSchema.setEdorType(request.getParameter("EdorType"));
     tLPPolSchema.setBonusGetMode(request.getParameter("BonusGetMode"));
     
     tLPDutySchema.setPolNo(request.getParameter("PolNo"));
     tLPDutySchema.setEdorNo(request.getParameter("EdorNo"));
     tLPDutySchema.setEdorType(request.getParameter("EdorType"));
     tLPDutySchema.setBonusGetMode(request.getParameter("BonusGetMode"));  
     
  }
  
  System.out.println("validateFlag:" + request.getParameter("validateFlag"));
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("validateFlag", request.getParameter("validateFlag"));

  try {
     // 准备传输数据 VData
  
  	 VData tVData = new VData();  
  	
	 //保存个人保单信息(保全)	
      tVData.addElement(tG);
      tVData.addElement(tLPEdorMainSchema);
      tVData.addElement(tLPPolSchema);
      tVData.add(tTransferData);
      tVData.addElement(tLPDutySchema);
      
      boolean tag = tPEdorBDDetailUI.submitData(tVData,transact);
      System.out.println("tag is " + tag);
   } catch(Exception ex) {
		Content = transact+"失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}			
    //如果在Catch中发现异常，则不从错误类中提取错误信息
    if (FlagStr=="") {
        System.out.println("------success");
    	tError = tPEdorBDDetailUI.mErrors;
    	if (!tError.needDealError()) {                          
        	Content = " 保存成功";
    		FlagStr = "Success";
    		if (transact.equals("QUERY||MAIN") || transact.equals("QUERY||BonusGetMode")) {
	    		if (tPEdorBDDetailUI.getResult()!=null&&tPEdorBDDetailUI.getResult().size()>0)
	    	        {
	    			Result = (String)tPEdorBDDetailUI.getResult().get(0);
	    		    if (Result==null||Result.trim().equals("")) {
	    				Result = "";
	    				Content = "";
	    		    }
	    	        }
    	    }
         } else  {
    		Content = " 保存失败，原因是:" + tError.getFirstError();
    		FlagStr = "Fail";
    	}
  	}
  //添加各种预处理
    System.out.println("------------Result is------------ " + Result);
%>                      
<html>
<script language="javascript">
   
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

