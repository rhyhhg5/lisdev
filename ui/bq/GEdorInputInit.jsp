<%
//程序名称：LLReportInput.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%
  String CurrentDate = PubFun.getCurrentDate();
  Date dt = PubFun.calDate(new FDate().getDate(CurrentDate), 1, "D", null);
  String ValidDate = CurrentDate;   
  String dayAfterCurrent = new FDate().getString(dt);   
  String CurrentTime = PubFun.getCurrentTime();
  
  GlobalInput mGI = new GlobalInput(); //repair:
  mGI=(GlobalInput)session.getValue("GI");
%>                          

<script language="JavaScript">
//从工单接收数据
var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
var mEdorAcceptNo = "<%=request.getParameter("DetailWorkNo")%>"; //保全号
var mCustomerNo = "<%=request.getParameter("CustomerNo")%>";     //客户号


//工单接口函数，接收工单传来的参数并初始化页面
function initTaskInterface()
{
  if (mLoadFlag.substring(0, 4) == "TASK")
  {
		divQuery.style.display = "none";
		fm.all("LoadFlag").value = mLoadFlag;
		fm.all("EdorAcceptNo").value = mEdorAcceptNo;
		fm.all("EdorNo").value = mEdorAcceptNo;
		initGrpContGrid();
        queryGrpCont2(mCustomerNo);
		getGrpEdorItem();    
  }
}

function initInpBox()
{ 
	try
  {  
    fm.all('EdorValiDate').value = '<%=CurrentDate%>';
    fm.all('Operator').value = "<%=tG.Operator%>";
    fm.all('EdorAppDate').value = '<%=CurrentDate%>';	
    fm.all('currentDay').value = '<%=CurrentDate%>';
  }
  catch(ex)
  {
    alert(ex);
    alert("在GEdorInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

//初始化团单合同列表
function initGrpContGrid()
{                               
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="团体合同号";         		//列名
		iArray[1][1]="150px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="印刷号";         		//列名
		iArray[2][1]="100px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="单位名称";         		//列名
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=200;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="生效日期";         		//列名
		iArray[4][1]="80px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="总人数";         		//列名
		iArray[5][1]="50px";            		//列宽
		iArray[5][2]=200;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="总保费";         		//列名
		iArray[6][1]="80px";            		//列宽
		iArray[6][2]=200;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[7]=new Array();
		iArray[7][0]="总保额";         		//列名
		iArray[7][1]="100px";            		//列宽
		iArray[7][2]=200;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[8]=new Array();
		iArray[8][0]="出单机构";         		//列名
		iArray[8][1]="80px";            		//列宽
		iArray[8][2]=200;            			//列最大值
		iArray[8][3]=3; 
		iArray[8][4]="station";              	        //是否引用代码:null||""为不引用
		iArray[8][5]="6";              	                //引用代码对应第几列，'|'为分割符
		iArray[8][9]="出单机构|code:station&NOTNULL";
		iArray[8][18]=250;
		iArray[8][19]=0 ;
		
		GrpContGrid = new MulLineEnter("fm1" , "GrpContGrid"); 
		//这些属性必须在loadMulLine前
		GrpContGrid.mulLineCount = 0;   
		GrpContGrid.displayTitle = 1;
		GrpContGrid.locked = 1;
		GrpContGrid.canSel = 1;
		GrpContGrid.selBoxEventFuncName ="getGrpContDetail" ;     //点击RadioBox时响应的JS函数
		GrpContGrid.hiddenPlus = 1;
		GrpContGrid.hiddenSubtraction = 1;
		GrpContGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
		alert(ex);
	}
}


function initGrpEdorItemGrid()
{                             
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保全受理号";         		//列名
    iArray[1][1]="100px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="批单号";         		//列名
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="批改类型";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="EdorType";               //列的名字
    
    //为不影响现有成序，将以上全隐藏，显示新的列
    iArray[4]=new Array();
    iArray[4][0]="保单号码";         		
    iArray[4][1]="80px"; 
    iArray[4][2]=100;
    iArray[4][3]=0; 
    
    iArray[5]=new Array();
    iArray[5][0]="客户号";         		
    iArray[5][1]="80px"; 
    iArray[5][2]=100;
    iArray[5][3]=0; 
    
    iArray[6]=new Array();
    iArray[6][0]="保全生效日期";         		//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]=" 处理状态";         		
    iArray[7][1]="80px"; 
    iArray[7][2]=100;
    iArray[7][3]=0;
    
    GrpEdorItemGrid = new MulLineEnter( "fm" , "GrpEdorItemGrid" ); 
    //这些属性必须在loadMulLine前
    GrpEdorItemGrid.mulLineCount = 0;   
    GrpEdorItemGrid.displayTitle = 1;
    GrpEdorItemGrid.canSel =1;
    GrpEdorItemGrid.selBoxEventFuncName ="getEdorItemDetail" ;     //点击RadioBox时响应的JS函数
    GrpEdorItemGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
    GrpEdorItemGrid.hiddenSubtraction=1;
    GrpEdorItemGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //GrpContGrid.setRowColData(1,1,"asdf");
  }
  catch(ex)
  {
    alert(ex);
  }
}


function initSelBox()
{  
  try                 
  {
  
  }
  catch(ex)
  {
    alert("在GEdorInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();  
    initGrpEdorItemGrid();
    initTaskInterface();
   // test();
  }
  catch(re)
  {
    alert("GEdorInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initDiv()
{
	divedortype.style.display ='none';
	divappconfirm.style.display ='none';
	divdetail.style.display = 'none';
}
function test()
{
		var content="test";
 		var urlStr="../common/jsp/ReturnPage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
}
</script>