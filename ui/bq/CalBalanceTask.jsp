<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BPOCont.jsp
//程序功能：外包保单修改
//创建日期：2007-10-19 16:52
//创建人  ：YangYalin
//更新记录：更新人    更新日期     更新原因/内容
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
  
  <title>运行批处理</title>
</head>
<body  onload="" >
  <form action="CalBalanceTaskSave.jsp" method=post name=fm target="fraSubmit">
    <br></br>
    <table>
      <tr>
        <td class=common>保单号 <Input class= common name=ContNo elementtype=nacessary verify="保单号|notnull"></td>
      </tr>
    </table>
    <INPUT VALUE="运  行" class=cssButton TYPE=button onclick="runTask();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

<script>
  
var showInfo;
function runTask()
{
  if(!verifyInput2())
  {
    return false;
  }
  
  var showStr = "正在运行数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
			"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) 
{
	try
	{
	  showInfo.close();
	}
	catch(ex)
	{
	  alert(ex.message);
	}
	
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

</script>

</html>
