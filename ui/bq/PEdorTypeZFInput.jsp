<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2008-2-25 9:40
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdor.js"></SCRIPT>
	<SCRIPT src="./PedorTypeZF.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="PedorTypeZFInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./PEdorTypeZFSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common> 
        <td class="title"> 受理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 批改类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
        	<input class = "readonly" readonly name="ContNo">
        </td>   
    	</tr>
    </table> 

    <Div id= "divPolInfo" style= "display: ''">
			<table>
			  <tr>
		      <td>
		      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
		      </td>
		      <td class= titleImg>
		         保单险种信息
		      </td>
			  </tr>
			</table>
	    <Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
			  		<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span> 
				  	</td>
					</tr>
				</table>					
	    </div>
    </DIV>
    <br>
    <!--
    <table  class= common>
      <TR class= common>
        <TD class= title> 退保原因 </TD>
        <TD class= input>
		      <input class="codeNo" name="reason_tb" readOnly verify="退保原因|notnull&code:reason_tb&len<=10" ondblclick="showCodeList('reason_tb',[this,reason_tbName],[0,1]);" onkeyup="showCodeListKey('reason_tb',[this,reason_tbName],[0,1]);"><Input class="codeName" name=reason_tbName readonly elementtype=nacessary>
        </TD>
        <TD  class= title> 退保日期 </TD>
        <TD class=input>
          <Input class= "readonly" name=EndDate readonly >
        </TD> 
      </TR>
    </table> 
    -->
    <br>
    <div>
	   <Input type=button name="save" class = cssButton value="保  存" onclick="edorTypeZFSave()">
		 <Input  type=Button name="goBack" class = cssButton value="返  回" onclick="edorTypeCTReturn()">
		</div>

		 <input type=hidden id="fmtransact" name="fmtransact">
		 <input type=hidden id="ContType" name="ContType">
		 <input type=hidden name="EdorAcceptNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
