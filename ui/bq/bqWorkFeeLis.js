//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var organcode="";


//校验录入数据的合法性
function checkData()
{
	
   var StartDate= fm.all('StartDate').value;
   var EndDate=fm.all('EndDate').value;
   if(trim(StartDate)== null ||trim(StartDate)== "" ||trim(EndDate)== null||trim(EndDate)=="")
	{
		alert("统计日期不能为空");
		return false;
	}
  if(EndDate<StartDate)
  {
      alert("统计止期不能早于统计起期！");
      return false;
  }
  var t1 = new Date(StartDate.replace(/-/g,"\/")).getTime();
  var t2 = new Date(EndDate.replace(/-/g,"\/")).getTime();
  var tMinus = (t2-t1)/(24*60*60*1000);  
  if(tMinus>180 )
  {
	  alert("查询起止时间不能超过180天！")
	  return false;
  }

}


/*********************************************************************
 *  上下级机构判断
 *  参数  ：  pManageCom String : 管理机构
 *  返回值：  true 可查看该机构保单；false 不可查看
 *********************************************************************
 */
function IsValiCom(pManageCom)
{


	if (comCode =='86' || comCode =='86000000' ) return true;
	if ((pManageCom =='86' || pManageCom =='86000000') && (comCode =='86' || comCode =='86000000') ) return true;
	
	if (pManageCom.length >4 && comCode.substring(0,4) == pManageCom.substring(0,4) 
		&& comCode <= pManageCom  ) return true;
	if (pManageCom.length == 4 && comCode.substring(0,4) == pManageCom.substring(0,4))
	{
		var tManage = pManageCom+ "0000" ;
		if (comCode == tManage||comCode == pManageCom) return true;
		
	}
	return false;
	
}
function getData()
{
   var wherePart="";
   var organcode=fm.all('organcode').value;
   var Groupno=fm.all('Groupno').value;
   var usercode=fm.all('usercode').value;
   var feeType=fm.all('feeType').value;
   var feeState=fm.all('feeState').value;
   var StartDate=fm.all('StartDate').value;
   var EndDate=fm.all('EndDate').value;

}

//提交，保存按钮对应操作
function submitForm()
{		
	organcode=fm.organcode.value;
	
	 
	 if (checkData() == false)
    return false;

	if (verifyInput() == false)
    return false;
    
   if (IsValiCom(organcode) == false)
   {
   	alert("您没有权限");
   	return false;
   	}
   
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	fm.all('op').value = '';
	fm.submit();
	showInfo.close();
}



function lcReload()
{
window.location.reload();
}