<html> 
<%
//程序名称：
//程序功能：团体保全
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
 <%
     GlobalInput tG = new GlobalInput();
     tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
  %>  
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="GEdorInput.js"></SCRIPT>
  <SCRIPT src="PEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file = "ManageComLimit.jsp"%>
  <%@include file="GEdorInputInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LCGrpPolQueryDir.jsp" method=post name=fm1 target="fraSubmit">
	<table>
		<tr>
		  <td>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpContGrid);">
		  </td>
		  <td class= titleImg>
		    客户投保信息
		  </td>
		</tr>
	  </table>
	  <Div id="divGrpContGrid" style= "display: ''">
		<table  class= common>
			<tr  class= common>
		  		<td text-align: left colSpan=1>
					<span id="spanGrpContGrid" >
					</span> 
			  	</td>
			</tr>
		</table>
		<Div id="divPage" style= "display: 'none'">
			<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPage2.firstPage();"> 
			<INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPage2.previousPage();"> 					
			<INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPage2.nextPage();"> 
			<INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPage2.lastPage();">
		</Div>					
	  </div>
	</Div>
	<Div id= "divQuery" style= "display: 'none'">
	  <%@include file="../common/jsp/OperateButton1.jsp"%>
	  <%@include file="../common/jsp/InputButton.jsp"%>
	  <TABLE class=common>
	    <TR  class= common> 
	      <TD  class= title width="11%"> 团体保单号
	      </TD>
	      <TD  class= input width="26%"> 
	        <input class=common name= GrpContNo1 >
	      </TD>
	      <TD  class= input width="26%"> 
	        <Input type =button class=cssButton value="查询团体保单" class= common onclick="queryGrpCont()">
	      </TD>
	    </TR>
	  </TABLE> 
	</Div>	
  </form>
  
  <form action="./GEdorSave.jsp" method=post name=fm target="fraSubmit">
  	<!-- 工单接口标志, 值为"TASK"表明工单系统调用 "TASKFRAME" 表明在框架里调用 -->
	<input type=hidden id="LoadFlag" name="LoadFlag">
	<Input type=hidden name="EdorNo">
    <table>
      <tr>
       <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
       </td>
       <td class= titleImg>
        保单信息
       </td>
      </tr>
    </table>

<Div  id= "divLCGrpPol" style= "display: ''">
    <table  class= common>
        <TR  class= common>
            <TD  class= title>
              团体保单号
            </TD>
            <TD  class= input>
              <Input class="readonly" readonly name=GrpContNo >
            </TD>
            <TD  class= title>
              单位名称
            </TD>
            <TD  class= input>
              <Input class= "readonly" readonly name=GrpName  title='' >
            </TD>
             <TD  class= title>
              联系人
            </TD>
            <TD  class= input>
              <Input class= "readonly" readonly name=LinkMan1 >
            </TD>
        </TR>
        <TR  class= common>
            <TD  class= title>
                总保费
            </TD>
            <TD  class= input>
                <Input class= "readonly" readonly name=Prem >
            </TD>
            <TD  class= title>
                总保额
            </TD>
            <TD  class= input>
                <Input class= "readonly" readonly name=Amnt >
            </TD>
            <TD  class= title>
                参保人数
            </TD>
            <TD  class= input>
                <Input class= "readonly" readonly name=Peoples2 >
            </TD>
        </TR>
        <TR  class= common>
            <TD  class= title>
                保单生效日期
            </TD>
            <TD  class= input>
                <Input class= "readonly" readonly name=CValiDate >
            </TD>
            <TD  class= title>
                业务员
            </TD>
            <TD  class= input>
                <Input class= "readonly" readonly name=AgentCode >
            </TD>
        </TR>
        
        <Input type=hidden name=ManageCom>
        </table>
</Div>

  <table>
    <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
      </td>
      <td class= titleImg>
        保全信息
      </td>
    </tr>
  </table>
	<Div  id= "divGrpEdorMain" style= "display: ''">
		<table  class= common>
			<tr class=common>
				<td class=title>
				    保全受理号
				</td>
				<td class= input colspan="7">
				    <Input class="readonly" readonly name=EdorAcceptNo>
				</td>
        </tr>
          <tr class= common>
          <TD class= title>
          	批改类型
          </TD>
          <TD  class=input>
            <!--<Input class= "code" name=EdorType  ondblclick="return showCodeList('EdorCode',[this], null, null, RiskCode, 'RiskCode');" onkeyup="return showCodeListKey('EdorCode',[this], null, null, RiskCode, 'RiskCode');" >-->
          	<Input class= "codeNo" name=EdorType readonly verify="批改类型|notnull&code:EdorCode" ondblclick="initEdorType(this);" onkeyup="initEdorType(this);" ><Input class= "codeName" name=EdorTypeName readonly >
          </TD>
          <TD class=title>
            保全生效日期
          </TD>
          <TD class=input>
            <Input class= "coolDatePicker" dateFormat="short" name=EdorValiDate >
            <Input type="hidden" name=EdorAppDate >
          </TD>
          <TD  class= title>
            操作员
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Operator >
          </TD>
        </tr>
			</table>
		<br>
		<div id="ZBdiv" style="display: 'none'" align="left">
		<p><font color="#ff0000"><h4>团险万能保全追加保费项目的保全生效日期为保全收费核销次日。</h4></font></p>
		</div>
    <input type =button class=cssButton value="添加保全项目" onclick="addRecord();"> 
    <input type =button class=cssButton value="删除保全项目" onclick="deleteRecord();">   
    <br>
		<table>
		  <tr>
		   <td>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
		   </td>
		   <td class= titleImg>
		    保全项目信息
		   </td>
		  </tr>
		</table>
	<Div  id= "divGrpEdorItemGrid" style= "display: ''">
		<table  class= common>
			<tr  class= common>
				<td text-align: left colSpan=1>
					<span id="spanGrpEdorItemGrid" >
					</span> 
				</td>
			</tr>
		</table>					
	</div>
    <br>
		<Div  id= "divAddDelButton" style= "display: ''" align=left>
		<Input type =button class=cssButton value="保全项目明细" onclick="edorDetail();">
		<Input type =button class=cssButton value="保全理算" onclick="edorAppConfirm()" id="edorAppButton">
		</DIV>     
     <Input  type=hidden name=AppntNo >
   	 <input type=hidden id="currentDay" name="currentDay">
   	 <input type=hidden id="dayAfterCurrent" name="dayAfterCurrent">     
     <input type=hidden id="fmAction" name="fmAction">
     <input type=hidden id="EdorState" name= "EdorState">
     <input type=hidden id="ContType" name="ContType" value=2 >
     <input type=hidden id ="RiskCode" name = "RiskCode">
     
          <!--删除需要的控件-->
     <input type=hidden id="fmtransact" name="fmtransact">
     <input type=hidden id="hEdorNo" name= "hEdorNo">
     <input type=hidden id="hGrpContNo" name= "hGrpContNo">
     <input type=hidden id="ContNo" name= "ContNo"><!--由于团险个险共用Pedor.js文件，所以需要存入该参数 added by huxl 20080410-->
     <input type=hidden id="hEdorType" name= "hEdorType">
     <input type=hidden id="hEdorState" name= "hEdorState">
     <input type=hidden id="MakeDate" name= "MakeDate">
     <input type=hidden id="MakeTime" name= "MakeTime">
     <input type=hidden id="Transact" name= "Transact">
     <input type=hidden id="DelFlag" name= "DelFlag">          
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
