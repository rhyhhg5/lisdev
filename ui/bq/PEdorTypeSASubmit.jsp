<%
//程序名称：PEdorTypeSASubmit.jsp
//程序功能：附加险增额
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  PEdorSADetailUI tPEdorSADetailUI     = new PEdorSADetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  
  //个人批改信息
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
	tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
  tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
    
  try
  {
    // 准备传输数据 VData
  	VData tVData = new VData();  
	 	tVData.addElement(tG);
	 	tVData.addElement(tLPEdorMainSchema);
    
  	if (transact.equals("INSERT||MAIN")) {
  	  System.out.println("++++++++CalMode++++++" + request.getParameter("CalMode"));
	  	
	  	LPDutySchema tLPDutySchema = new LPDutySchema();
  		tLPDutySchema.setPolNo(request.getParameter("PolNo"));
  	  tLPDutySchema.setEdorNo(request.getParameter("EdorNo"));
  		tLPDutySchema.setEdorType(request.getParameter("EdorType"));
  		tLPDutySchema.setDutyCode(request.getParameter("DutyCode"));
  		
  		if (request.getParameter("CalMode").equals("O")) {
  		   tLPDutySchema.setMult(request.getParameter("RemainMulti"));
  		} else { 
  		   tLPDutySchema.setAmnt(request.getParameter("RemainAmnt"));
  		}			
  		
  		tVData.addElement(tLPDutySchema);	
  	}

  	tPEdorSADetailUI.submitData(tVData,transact);
	
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}	
	
	//如果在Catch中发现异常，则不从错误类中提取错误信息
 	tError = tPEdorSADetailUI.mErrors;

  if (tError.getErrType().equals(CError.TYPE_NONEERR))
  {                          
    Content = " 保存成功" + ":"+ Result.trim();
  	FlagStr = "Success";
  }
  
  if (FlagStr.equals(""))
  {
    String ErrorContent = tError.getErrContent();  
        
    if (tError.getErrType().equals(CError.TYPE_ALLOW)) 
    {
      Content = " 保存成功，但是：" + ErrorContent;
  	  FlagStr = "Success";
    }
    else 
    {
    	Content = "保存失败，原因是：" + ErrorContent;
    	FlagStr = "Fail";
    }
    
    Content = PubFun.changForHTML(Content);
  }		
  
  if (FlagStr.equals("Success") && (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL")))
  {
    	if (tPEdorSADetailUI.getResult()!=null && tPEdorSADetailUI.getResult().size()>0)
    	{
    		Result = (String)tPEdorSADetailUI.getResult().get(0);
    		if (Result==null||Result.trim().equals(""))
    		{
    			FlagStr = "Fail";
    			Content = "查询失败!!";
    		}
    	}
  }

%>
                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>");
</script>
</html>

