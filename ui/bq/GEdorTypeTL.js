var turnPage = new turnPageClass();      
var turnPage2 = new turnPageClass();      
var turnPage3 = new turnPageClass();  

//查询导入的数据
function queryImportData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo,InsuAccBala, Money2,Money,InsuAccInterest " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '1' " +
	             "order by Int(SerialNo) ";
	turnPage.queryModal(sql, InsuredListGrid); 
}

//查询导入无效的数据
function queryFailData()
{	
	var sql = "select SerialNo, State, InsuredNo, InsuredName, Sex, Birthday, IdType, IDNo, Money, ErrorReason " +
	             "from LPDiskImport " +
	             "where GrpContNo = '" + fm.all("GrpContNo").value + "' " +
	             "and EdorNo = '" + fm.all("EdorNo").value + "' " +
	             "and EdorType = '" + fm.all("EdorType").value + "' " +
	             "and State = '0' " +
	             "order by Int(SerialNo) ";
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, FailGrid); 
}

//计算退保费用
function calFee(){
	fm.fmtransact.value="CALFEE";
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();	
}

//查询团体险种信息
function queryPolInfo()
{
	  var GrpLQMoney = "0";
	  var maxCanGetFEE = "0";
	  var grpZTFEE = "0";
  var sql = "select DetailType, EdorValue from LPEdorEspecialData " +
            "where EdorNo = '" + fm.all("EdorNo").value + "' " +
            "and EdorType ='" + fm.all("EdorType").value + "' ";
  var arrResult = easyExecSql(sql);
  if (arrResult)
  {
    for (i = 0; i < arrResult.length; i++)
    {
      var detailType = arrResult[i][0];
      var edorValue = arrResult[i][1];
      if (detailType == "GRPLQMONEY")
      {
    	  GrpLQMoney = edorValue;
      }
      if (detailType == "MAXCANGETFEE")
      {
    	  maxCanGetFEE = edorValue;
      }
      if (detailType == "GRPZTFEE")
      {
    	  grpZTFEE = edorValue;
      }
    }
  }
  var grpcontno=fm.GrpContNo.value;
  
  sql = " select a.polno as  险种代码,a.RiskSeqNo as 险种序号, " +
	" a.RiskCode as 险种代码, c.riskname as 险种名称, " +
	" b.insuaccbala as 公共账户余额,'"+maxCanGetFEE+"' as 公共账户最大可领取金额, " +
	" '"+GrpLQMoney+"' as 公共账户部分领取金额, '"+grpZTFEE+"' as 部分领取退保费用  " +
	" from lcpol a ,lcinsureacc b,LMRiskApp c " +
	" where a.grpcontno='"+grpcontno+"' " +
	" and a.poltypeflag='2'  " +
	" and a.grpcontno=b.grpcontno and a.polno=b.polno " +
	" and a.riskcode=c.riskcode and (c.RiskType3 ='8' or c.RiskType8='') " +
	" with ur "
  turnPage3.pageLineNum = 100;
  turnPage3.pageDivName = "divPage3";
  turnPage3.queryModal(sql, PolGrid);
}

//磁盘导入 
function importInsured()
{
	var url = "./BqDiskImportMain.jsp?EdorNo=" + fm.all("EdorNo").value + 
	          "&EdorType=" + fm.all("EdorType").value +
	          "&GrpContNo=" + fm.all("GrpContNo").value;
	var param = "width=400, height=150, top=150, left=250, toolbar=no, menubar=no, scrollbars=no,resizable=no,location=no, status=no";
	window.open(url, "团险万能帐户资金导入", param);
}

//数据提交后的操作
function afterSubmit(flag, content,fmtransact)
{
  try 
  { 
    showInfo.close();
    queryPolInfo();
    queryImportData();
	  window.focus();
	}
	catch (ex) {}
	if (flag == "Fail")
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		initForm();
		if(fmtransact=="CALFEE"){
			fm.all("ErrorsInfo").innerHTML = content;
		}
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		if(fmtransact=="CALFEE"){
			 
			 initForm();
		 }else{
			 returnParent();
		 }
		fm.all("ErrorsInfo").innerHTML  ="";
	}
}

//保存
function save()
{
  //校验是否有录入金额
  if (PolGrid.mulLineCount == 1)
  {
    if ( (fm.PolGrid7.value == 0) && (InsuredListGrid.mulLineCount == 0))
    {
      alert("请录入公共账户或个人账户领取金额！");
      return false;
    }
  }
  else
  {
    var sum = 0;
    for (i = 0; i < PolGrid.mulLineCount; i++)
    {
      sum += fm.PolGrid7[i].value;
    }
    if ((sum == 0) && (InsuredListGrid.mulLineCount == 0))
    {
      alert("请录入公共账户或个人账户领取金额！");
      return false;
    }
  }
  if(InsuredListGrid.mulLineCount>0){
if(InsuredListGrid.mulLineCount==1){
	  if(fm.InsuredListGrid11.value==""){
		  alert("被保险人的个人账户领取金额存在空值，请检查模版并重新导入");
		  return false;
	  }
	  if(fm.InsuredListGrid10.value==""){
		  alert("请先计算退保费用后再保存");
		  return false;			  
	  }
}else{
	
	for(j=0;j<InsuredListGrid.mulLineCount;j++){
		if(fm.InsuredListGrid11[j].value==""){
			alert("被保险人的个人账户领取金额存在空值，请检查模版并重新导入");
			return false;
		}
		if(fm.InsuredListGrid10[j].value==""){
			alert("请先计算退保费用后再保存");
			return false;			  
		}
	}
}
  }
	if (FailGrid.mulLineCount > 0)
	{
		alert("有错误数据，请重新导入！");
		return false;
	}
  if (!PolGrid.checkValue())
  {
    return false;
  }
  
  //校验是否有计算退保费用
  var GrpLQMoney = null;
  var maxCanGetFEE = null;
  var grpZTFEE = null;
var sql = "select DetailType, EdorValue from LPEdorEspecialData " +
        "where EdorNo = '" + fm.all("EdorNo").value + "' " +
        "and EdorType ='" + fm.all("EdorType").value + "' ";
var arrResult = easyExecSql(sql);
if (arrResult)
{
for (i = 0; i < arrResult.length; i++)
{
  var detailType = arrResult[i][0];
  var edorValue = arrResult[i][1];
  if (detailType == "GRPLQMONEY")
  {
	  GrpLQMoney = edorValue;
  }
  if (detailType == "MAXCANGETFEE")
  {
	  maxCanGetFEE = edorValue;
  }
  if (detailType == "GRPZTFEE")
  {
	  grpZTFEE = edorValue;
  }
}
}
if("0"!=GrpLQMoney){
	if(maxCanGetFEE==null||maxCanGetFEE==""){
		alert("请先计算退保费用后再保存");
		return false;
	}
}	
//if(maxCanGetFEE==null||maxCanGetFEE==""){
//	alert("请先计算退保费用后再保存");
//	return false;
//}
if(fm.PolGrid7.value!=GrpLQMoney){
	alert("您录入的领取金额与保存的领取金额不相等，请重新计算退保费用后再保存");
	return false;
}
  
  fm.fmtransact.value="SAVEVALUE";
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
}

//返回父窗口
function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}