<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>

<%

    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "保单即将永久失效清单_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("/");
    System.out.println("filepath................"+filePath);
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String tSQLWherePar = request.getParameter("sql");
    
    
    
    
    String tSQL   = "select "
		          +  "(select Name from ldcom where comcode=a.ManageCom) ," //管理机构名字
				  +  "(select (select name from labranchgroup where agentgroup=substr(b.branchseries,1,12) )	from labranchgroup  b where b.agentgroup=a.AgentGroup),"//营销部门
				  +  "a.ContNo,"//保单号
				  +  "a.AppntName,"//投保人
				  +  "(select (case when c.Phone is null then c.homephone else c.phone end) from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
				  +  " and d.ContNo = a.ContNo fetch first 1 row only ),"//投保人电话
				  +  "(select c.Mobile from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
				  +  " and d.ContNo = a.ContNo fetch first 1 row only)," //投保人移动电话
		          +  "(select c.PostalAddress from lcaddress c,lcappnt d where c.CustomerNo=d.AppntNo and c.AddressNo=d.AddressNo "
				  +  " and d.ContNo = a.ContNo fetch first 1 row only),"//投保人的联系地址
			      +  "a.PaytoDate,"//（交至日）
				  +  "codename('paymode',a.PayMode),"//缴费方式
				  +  "(select max(prtSeq) from LOPRTManager where otherNo = a.ContNo and code='420'),"//（即将永久失效通知书号）
				  +  "(case when	a.SaleChnl='04' then '银保' when a.SaleChnl='13' then '银保' else '个险' end),"//（保单类型）
			  	  +  "getUniteCode(a.AgentCode),"//代理人编码
				  +  "(select Mobile from laagent where AgentCode=a.AgentCode),"//代理人电话
		          +  "(select Name from laagent where AgentCode=a.AgentCode),"//代理人姓名
		          +  "GETSTATEDATE(a.ContNo,'02','Available'),"//(失效日期)
		          +  "(case when (select agentstate from laagent where agentcode=a.agentcode)>='06' then '孤儿单' else '业务员在职' end), "//（保单归属状态）
		          +  "a.riskcode ,"//险种代码
		          +  "a.prem  "//险种保费
				  +  "from lcpol a "
				  +  "where conttype = '1' and stateflag = '2'"
				  +  "and exists (select 1 from lmriskapp where riskcode = a.riskcode and riskperiod = 'L') and  "//保证长期险
				  +  " not exists (select 1 from lpedorapp x ,lpedormain y where x.edoracceptno = y.edoracceptno and x.edorstate<> '0' and y.contno = a.contno ) and "//不存在理赔
				  +  " not exists (select 1 from LCInsured x, LLCase y where y.CustomerNo = x.InsuredNo and rgtstate not in ('11', '12', '14') and ContNo = a.contno)  and "//不存在保全
				  +  tSQLWherePar ;
				  
	System.out.println("打印查询:"+tSQL);
	
    //设置表头
    String[][] tTitle = {{"管理机构", "营销部门", "保单号", "投保人", "投保人电话", "投保人手机号", "投保人联系地址", "交至日", "缴费方式", "即将永久失效通知书号", "保单类型", "代理人编码", "代理人电话", "代理人姓名", "失效日期","保单归属状态","险种代码","险种保费"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(tSQL,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>

<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>