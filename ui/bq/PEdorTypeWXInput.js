//               该文件中包含客户端需要处理的函数和事件

var showInfo1;
var mDebug="1";

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();          //使用翻页功能，必须建立为全局变量


/* 查询工单在列表中显示结果 */
function queryLJS()
{
	var strSQL="select otherno,getnoticeno,(select riskcode from lcpol where contno=a.otherno and riskcode in (select riskcode from lmriskapp where risktype4='4')), " +
				" (select max(c.riskname) from lcpol b,lmriskapp c where b.contno=a.otherno and c.risktype4='4' and b.riskcode=c.riskcode), "+
				" (select max(paydate) from lcinsureacctrace where contno=a.otherno and otherno=a.getnoticeno),'"+fm.all('TBAmnt').value+"', "+
				" sumduepaymoney,(select -money from lcinsureacctrace where contno=a.otherno and otherno=a.getnoticeno and moneytype='GL' ) "+
				" from ljspayb a where otherno='"+fm.all('ContNo').value+"' and dealstate='1' order by getnoticeno desc with ur ";
	turnPage.pageLineNum = 10;
	turnPage.queryModal(strSQL, LJSGrid);
}


function returnParent()
{
	try
	{
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {}
}

function save()
{
	for(i = 0; i <fm.chkfeemode.length; i++){
		if(fm.chkfeemode[i].checked){
			fm.all('feemode').value=fm.chkfeemode[i].value;			
			break;
		}
	}
	if(fm.all('feemode').value==null||fm.all('feemode').value==''){
	alert("请选择保单以后的算费方式!");
	return false;
	}
	if(!verifyInput()){
	return false;
	}
	if(fm.all('getFee').value<=0){
	alert("录入的补费金额必须大于0!");
	return false;
	}
	var tSel = LJSGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
	alert( "请先选择一条记录，再点击保存按钮。" );
	return false;
	}
	if( tSel>1){
	alert( "只能选择最后一次续期进行操作。" );
	return false;
	}
	fm.all('getnoticeno').value=LJSGrid.getRowColDataByName(tSel-1, "GetNoticeNo")
	fm.all('Fee').value=fm.all('getFee').value;
//	alert("fee:"+fm.all('Fee').value+",getnoticeno:"+fm.all('getnoticeno').value+",feemode:"+fm.all('feemode').value);
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  returnParent();
	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function initPolInfo()
{	try
  {
		var sql="select amnt from lcpol  where contno='"+fm.all('ContNo').value+"' and riskcode in (select riskcode from lmriskapp where risktype4='4') ";
		var tarrResult = easyExecSql(sql);
		if(tarrResult==null)
		{
			alert("未查到保单的当前保额！");
		}
		fm.all('Amnt').value=tarrResult[0][0];
		var tbsql = "select Amnt from LPPol where ContNo = '"+fm.all('ContNo').value+"' and riskcode in (select riskcode from lmriskapp where risktype4='4') "
					+" and EdorType = 'BA' order by edorno fetch first 1 row only ";
		var tbarrResult = easyExecSql(tbsql);
 	    if (tbarrResult == null) 
 	    {
			fm.all('TBAmnt').value=fm.all('Amnt').value;
	    }else
	    {
			fm.all('TBAmnt').value= tbarrResult[0][0];
	    }
  }
  catch(re)
  {
    alert("初始化帐户信息错误!");
  }	    
}