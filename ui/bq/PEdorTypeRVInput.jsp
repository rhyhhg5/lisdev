<html> 
<% 
//程序名称：PEdorTypeRVInput.jsp
//程序功能：万能险复效
//创建日期：2008-07-22
//创建人  张彦梅
//更新记录：  更新人    更新日期     更新原因/内容
%>   
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdor.js"></SCRIPT>
	<SCRIPT src="./PEdorTypeRV.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="PEdorTypeRVInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./PEdorTypeRVSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common> 
        <td class="title"> 受理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 批改类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
          <input class="readonly" type="text" readonly name="ContNo" >
        </td>
    	</tr>
    </table>  
    <Div  id= "divPolInfo" style= "display: ''">
    <table>
      <tr>
       <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolBase);"></td>
       <td class= titleImg>险种信息</td>
        <td class= common><input type=Button name="saveButton" class= cssButton value="帐户价值历史查询" onclick="InsuAccTraceQuery();"></td>       
      </tr>     
    </table>
    <Div  id= "divPolGrid" style= "display: ''">
        		<table  class= common>
        			<tr  class= common>
        		  		<td text-align: left colSpan=1>
        					<span id="spanPolGrid" >
        					</span> 
        			  	</td>
        			</tr>
 				<Div id= "divPage2" align="center" style= "display: 'none' ">
    		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage2.firstPage();	"> 
    		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage2.previousPage();"> 					
    		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage2.nextPage();"> 
    		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage2.lastPage();"> 
				</Div>
        		</table>					
    		</Div>
  </Div>
 <Div id="divPolBase" style= "display: '' ">
   <table class = "common"> 
   	 <tr  class= "common"> 
      <td class="title">上一交费日</td>
      <td class= input><input class="readonly" type="text" readonly name="LastPayDate" ></td>
      <td class="title">上一结算日</td>
      <td class= input><input class="readonly" type="text" readonly name="LastAccBalDate" ></td>
      <td class="title">当前帐户价值</td>
      <td class= input><input class="readonly" type="text" readonly name="AccBala" ></td>
    </tr>
    <tr  class= "common"> 
      <td class="title">交费频次</td>
      <td class= input><input class="readonly" type="text" readonly name="PayFreq" ></td>
      <td class="title">期交保费</td>
      <td class= input><input class="readonly" type="text" readonly name="Prem" ></td>
      <td class="title">交费状态</td>
      <td class= input><input class="readonly" type="text" readonly name="PayState" ></td>
    </tr>
  </table>
 </Div>  
    <table>
      <tr>
        <td>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLoanPremGrid);">
        </td>
        <td class= titleImg>
            欠交保费信息
        </td>
      </tr>
    </table>
    <Div  id= "divLoanPremGrid" style= "display: ''">
      <table  class= common>
      	<tr  class= common>
      		<td text-align: left colSpan=1>
    			<span id="spanLoanPremGrid" >
    			</span> 
    	  	</td>
      	</tr>
      </table>	
    <Div id="divPageLonePrem" style= "display: '' ">
			<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="turnPageLonePrem.firstPage();"> 
			<INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="turnPageLonePrem.previousPage();"> 					
			<INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="turnPageLonePrem.nextPage();"> 
			<INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="turnPageLonePrem.lastPage();">
		</Div>				
    </div>
   <br>
    <Div  id= "divPayMoneyFee" >
   
  <table class = "common"> 
    <tr  class= "common"> 
      <td class="title">补交保费金额</td>
      <td class= input><input class="readonly" type="text" readonly name="PayMoneyFee" value="0.0"></td>
      </TR>
    </table>
    
  </Div>
   <Div  id= "divAppMoneyFee" >
  <table class = "common"> 
    <tr  class= "common"> 
      <td class="title">追加保费金额</td>
      <td class= input><Input class= "common" name="AppMoney" value="0.0" verify="追加保费金额|notnull" > 元</td>
      
        <td class="title">追加保费初始扣费比例</td>
      <td class= input><input class="readonly" type="text" readonly name="AppMoneyFee" value="5％"></td>
      </TR>
    </table>
    <table class = "common">
    	        <tr class= "common">
    	            <td class = title>
    	                
    	            </td>
          <TD  class= input colspan="4">
              <Input type= "hidden" dateFormat="short" name=EdorAppDate id='select (select a.codename from ldcode a where a.codetype='paystate' and a.code=value(p.StandbyFlag1,'0') ),p.paytodate,p.payintv,p.polno,(select insuaccno from lmrisktoacc where riskcode=p.riskcode),value(p.StandbyFlag1,'0') from  lcpol p where  p.ContNo='"+fm.all('ContNo').value+"' and polno=mainpolno'>
          </TD> 
          			<td class = title>
    	               
    	           </td>
    	            <TD  class= input >
              		
          				</TD>
          				<TD>
          					<Input class='cssButton' type=Button value="健康告知录入" onclick="healthInput()">
          				</TD>
    	        </tr>
    	    </table>  
  </Div>
      
    
  
  <Div  id= "divSubmit" style="display:''">
    <table class = common>
      <TR class= common>
        <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
        <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
      </TR>
    </table>
  </Div>
  <br>
  <input type=hidden id="fmtransact" name="fmtransact">
  <input type=hidden id="ContType" name="ContType">
  <input type=hidden id="RVFlag" name="RVFlag">
  <input type=hidden name="EdorAcceptNo">
  <input type=hidden id="EdorValiDate" name="EdorValiDate">
  
  <input type=hidden id="SumLoanPrem" name="SumLoanPrem">
  <input type=hidden id="PayToDate" name="PayToDate">
  <input type=hidden id="PayStartDate" name="PayStartDate">
  <input type=hidden id="PayIntv" name="PayIntv">
   <input type=hidden id="PolNo" name="PolNo">
   <input type=hidden id="InsuAccNo" name="InsuAccNo">	
    <input type=hidden id="PayFlag" name="PayFlag">	
  
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
