<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-10-8 16:49:22
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  
  <SCRIPT src="./PEdorTypeBB.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeBBInit.jsp"%>
  
  <%--
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  -->
 	<%--window.open("./LCPolQueryPrem.jsp?PolNo="+tPolNo);--%>
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeBBSubmit.jsp" method=post name=fm target="fraSubmit">    
   <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 批单号 </TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>
     
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
      
    </TR>
  </TABLE> 


   <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
      </td>
      <td class= titleImg>
        被保人信息
      </td>
      <td>       	 <Input class= cssButton type=Button value="查询客户信息" onclick="customerQuery()">
      </td>
   </tr>
   </table>

    <Div  id= "LPInsured" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            客户姓名
          </TD>
          <TD  class= input>
            <Input class= readonly readonly  name=Name >
          </TD>
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class=readonly readonly name=Sex>
          </TD>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Birthday >
          </TD>

        </TR>
        

<!--        
        <TR  class= common>
          <TD  class= title>
            民族
          </TD>
          <TD  class= input>
          <Input class= "code" name=Nationality ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
          </TD>
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
 						<Input class="code" name="Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            身高
          </TD>
          <TD  class= input>
            <Input class= common name=Stature >
          </TD>
          <TD  class= title>
            体重
          </TD>
          <TD  class= input>
            <Input class= common name=Avoirdupois >
          </TD>
        </TR>
-->        
        
        <TR  class= common>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>           
            <Input class="code" name=IDType verify="证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">                         
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name=IDNo >
          </TD>
        </TR>
        
<!--          
        <TR  class= common>
          <TD  class= title>
            家庭地址
          </TD>
          <TD  class= input>
            <Input class=common name=HomeAddress >
          </TD>
          <TD  class= title>
            单位名称
          </TD> 
          <TD class= input>
             <Input class= common name=GrpName >
          </TD>
        </TR>
-->

        <TR  class= common>
          <TD  class= title8>
            地址编码
          </TD>
          <TD  class= input8>
             <Input class="code" name="AddressNo"  onchange="setAddr()" ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this],[0],'', '', '', true);" onkeyup="getaddresscodedata();return showCodeListKeyEx('GetAddressNo',[this],[0],'', '', '', true);">
          </TD>								
          <TD  class= title>
            家庭地址
          </TD>  
          <TD  class= input>
            <Input class= common name=HomeAddress >
          </TD>
          
          
          <TD  class= title>
            通讯地址
          </TD>  
          <TD  class= input>
            <Input class= common name=PostalAddress >
          </TD>
       
        </TR>
        
        <TR  class= in>         
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class=common name=ZipCode >
          </TD>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=Phone >
          </TD>
          <TD  class= title>
            手机
          </TD>
          <TD  class= input>
            <Input class=common name=Mobile>
          </TD>
        </TR>
        
      <!--  <TR  class= common>
          <TD  class= common>
            <Input type=hidden class= common name=HomeAddressCode >
          </TD>
        
        </TR>-->
<!--        
        <TR  class= common>

          <TD  class= title>
            ic卡号
          </TD>
          <TD  class= input>
            <Input class=common name=ICNo >
          </TD>
          
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class= common name=EMail >
          </TD>
        </TR>
-->        
        
      </table>
    <!--/Div-->

    
	<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= cssButton type=Button value="保存申请" onclick="edorTypeBBSave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= cssButton type=Button value="返  回" onclick="edorTypeBBReturn()">
     	 </TD>
     	 </TR>

	 <!--input type=hidden id="PolNo" name="PolNo"-->
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="addrFlag" name="addrFlag">
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">

	
      </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


