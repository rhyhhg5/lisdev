<%
//程序名称：PEdorAcptAppConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
    LPEdorAppSchema tLPEdorAppSchema   = new LPEdorAppSchema();
    PEdorAcceptAppConfirmUI tPEdorAcceptAppConfirmUI   = new PEdorAcceptAppConfirmUI();
    
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
     VData tVData = new VData();   
    CErrors tError = null;   
    String FlagStr = "";
    String Content = "";
    String transact = "INSERT||FINALEDORAPPCONFIRM";
    String prtParams = ""; 
    String tOperate = ""; 
    

    tLPEdorAppSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
    System.out.println("Haha");
    try
    {              
        tVData.addElement(tLPEdorAppSchema);            
        tVData.addElement(tG);                
        tPEdorAcceptAppConfirmUI.submitData(tVData,transact);  
                   
    }
    catch(Exception ex)
	{
        ex.printStackTrace();
        Content ="保全申请确认失败，原因是:" + ex.toString();
        FlagStr = "Fail";
	}			
	
    //如果在Catch中发现异常，则不从错误类中提取错误信息
   
    if (FlagStr.equals(""))
    {
        tError = tPEdorAcceptAppConfirmUI.mErrors;        
        if (tError.getErrType().equals(CError.TYPE_NONEERR))
        {   	  
            Content = " 保存成功";
            FlagStr = "Success";            
        }
        else 
        {
            String ErrorContent = tError.getErrContent(); 
                
            if (tError.getErrType().equals(CError.TYPE_ALLOW)) 
            {
                Content = " 保存成功，但是：" + ErrorContent;
                FlagStr = "Success";
            }
            else 
            {
            	Content = "保存失败，原因是：" + ErrorContent;
            	FlagStr = "Fail";
            }
            
            Content = PubFun.changForHTML(Content);
        }
    }

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=prtParams%>");
</script>
</html>

