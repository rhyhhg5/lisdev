//               该文件中包含客户端需要处理的函数和事件

var showInfo1;
var mDebug="1";

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeUMReturn()
{
	try
	{		
		top.close();
		top.opener.focus();
		top.opener.initEdorItemGrid();
		top.opener.getGrpEdorItem();
	}
	catch (ex) {}
}

function getInfo()
{
    var strSQL ="select contno,insuredno,insuredname,insuredbirthday,codename('sex',insuredsex),(select idno from lcinsured where contno=a.contno),cvalidate,(select sum(insuaccbala) from lcinsureacc where polno=a.polno)," + 
    			"(select sum(insuaccbala) from lcinsureacc where polno=a.polno and insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='4')))," + 
    			"(select sum(insuaccbala) from lcinsureacc where polno=a.polno and  insuaccno in (select insuaccno from LMRiskAccPay where payplancode in (select payplancode from LMDutyPay where AccPayClass='5'))) from lcpol a " +
    		"	where GrpContNo='"+fm.all('GrpContNo').value+"' and poltypeflag!='2' and polstate='03060001' with ur";
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
   
}

function OneInfo()
{
	var tSelNo = PolGrid.getSelNo();
	if (tSelNo == 0)
	{
		alert("请选择满期领取的保单！");
		return false;
	}
	fm.InsuredNo.value = PolGrid.getRowColDataByName(tSelNo-1,"InsuredNo");
	fm.InsuredName.value = PolGrid.getRowColDataByName(tSelNo-1,"InsuredName");
	fm.GrpSum.value = PolGrid.getRowColDataByName(tSelNo-1,"GrpSum");
	fm.InsuredSum.value = PolGrid.getRowColDataByName(tSelNo-1,"InsuredSum");
	fm.Sum.value = PolGrid.getRowColDataByName(tSelNo-1,"Sum");
}

function getInsuredInfo()
{
  var sql = "  select edorvalue "
            + "from lpedorespecialdata "
            + "where edorAcceptNo = '" + fm.all('EdorNo').value + "' "
            + "   and edorType = '" + fm.all('EdorType').value + "' and detailtype='INSUREDNO' ";
  var result = easyExecSql(sql);
  if(result)
  {
	  fm.all('InsuredNo').value = result[0][0];
  }
  sql = "  select edorvalue "
      + "from lpedorespecialdata "
      + "where edorAcceptNo = '" + fm.all('EdorNo').value + "' "
      + "   and edorType = '" + fm.all('EdorType').value + "' and detailtype='INSUREDNAME' ";
  result = easyExecSql(sql);
	if(result)
	{
	fm.all('InsuredName').value = result[0][0];
	}
	  sql = "  select edorvalue "
	      + "from lpedorespecialdata "
	      + "where edorAcceptNo = '" + fm.all('EdorNo').value + "' "
	      + "   and edorType = '" + fm.all('EdorType').value + "' and detailtype='GRPSUM' ";
	  result = easyExecSql(sql);
		if(result)
		{
		fm.all('GrpSum').value = result[0][0];
		}
		  sql = "  select edorvalue "
		      + "from lpedorespecialdata "
		      + "where edorAcceptNo = '" + fm.all('EdorNo').value + "' "
		      + "   and edorType = '" + fm.all('EdorType').value + "' and detailtype='INSUREDSUM' ";
		  result = easyExecSql(sql);
			if(result)
			{
			fm.all('InsuredSum').value = result[0][0];
			}
}

function edorTypeUMSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  edorTypeCTReturn();
	}
}

 
//提交前的校验、计算  
function beforeSubmit()
{
	var tSelNo = PolGrid.getSelNo();
	if (tSelNo == 0)
	{
		alert("请选择满期领取的保单！");
		return false;
	}
	if(fm.InsuredNo.value==null){
		alert("被保人号获取失败！");
		return false;
	}
	if(fm.InsuredName.value==null){
		alert("被保人名称获取失败！");
		return false;
	}
	if(fm.GrpSum.value==null){
		alert("单位缴费余额获取失败！");
		return false;
	}
	if(fm.InsuredSum.value==null){
		alert("个人缴费余额获取失败！");
		return false;
	}
	if(fm.Sum.value==null){
		alert("余额获取失败！");
		return false;
	}
  return true;
}           
      



