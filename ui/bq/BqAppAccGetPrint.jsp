<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListLP.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>

<%
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  XmlExport txmlExport = null;   
    
  String customerNo = request.getParameter("CustomerNo");
  String serialNo=request.getParameter("SerialNo");
  String contType = request.getParameter("ContType");
  
  LCAppAccTraceSchema mLCAppAccTraceSchema = new LCAppAccTraceSchema();
  mLCAppAccTraceSchema.setCustomerNo(customerNo);
  mLCAppAccTraceSchema.setSerialNo(serialNo);
  
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  VData tVData = new VData();
  tVData.add(mLCAppAccTraceSchema);
  tVData.add(tG);
  
         
  PrtAppAccGetUI tPrtAppAccGetUI = new PrtAppAccGetUI(); 
  if(!tPrtAppAccGetUI.submitData(tVData,contType))
  {          
     	operFlag = false;
     	Content = tPrtAppAccGetUI.mErrors.getFirstError(); 
  }
  else
  {             
    VData mResult = tPrtAppAccGetUI.getResult();			
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    
    if(txmlExport==null)
    {
    	operFlag=false;
    	Content="没有得到要显示的数据文件";	  
    }
  }
	
	if (operFlag==true)
	{
	  String templatePath = application.getRealPath("f1print/picctemplate/") + "/";
    ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
    CombineVts tcombineVts = new CombineVts(txmlExport.getInputStream(), templatePath);
    tcombineVts.output(dataStream);
    session.putValue("PrintVts", dataStream);
	  
		session.putValue("PrintStream", txmlExport.getInputStream());
		response.sendRedirect("../f1print/GetF1Print.jsp?showToolBar=true");
	}
	else
	{
    	FlagStr = "Fail";
%>
<html>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();
</script>
</html>
<%
  	}
%>