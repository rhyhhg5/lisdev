<%
//程序名称：PEdorTypeRPInit.jsp
//程序功能：
//创建日期：2003-01-08 
//创建人  ：YangZhao
//更新记录：  更新人    更新日期     更新原因/内容
%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
 <%

%>                            

<script language="JavaScript">  
function initInpBox()
{ 
  try
  {        
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('PolNo').value = top.opener.fm.all('PolNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('RnewFlag').value = '';
    fm.all('ContType').value ="1";
  }
  catch(ex)
  {
    alert("在PEdorTypeRPInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  { 
    fm.all('Operator1').value = "<%=tG.Operator%>";
    var strSql ="select Edorpopedom from LDUser where UserCode ='" + fm.all('Operator1').value +"'" ;
    //查询SQL，返回结果字符串
    turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
    if (turnPage.strQueryResult) 
      {
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  
      }
    //alert(turnPage.arrDataCacheSet);
    if(turnPage.arrDataCacheSet>"D")
        fm.all('RnewFlag').CodeData="0|^0|人工续保|^-1|自动续保|^-2|非续保|";
    else
        fm.all('RnewFlag').CodeData="0|^-1|自动续保|^-2|非续保|";
    fm.all('RnewFlag').value = "-1";//为了让默认为自动续保
  }
  catch(ex)
  {
    alert("在PEdorTypeRPInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
  
    initInpBox();

    initSelBox();  
    
    initSelQuery();
    fm.all('RnewFlag').value = "-1";//为了让默认为自动续保
  }
  catch(re)
  {
    alert("PEdorTypeRPInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initSelQuery()
{
	 var i = 0;
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

         var strSql = "";
         strSql = "select RiskCode,InsuredNo,InsuredName,CValidate,PayToDate,RnewFlag,Prem,Amnt from lcpol where 1 =1" + " " 
              + getWherePart('PolNo');	 

	  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

	  fm.all('RnewFlag').value = "-1";//为了让默认为自动续保
	  //判断是否查询成功
	  if (turnPage.strQueryResult) 
	  {
	    //查询成功则拆分字符串，返回二维数组
	    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
          }	      

         fm.all("RiskCode").value = turnPage.arrDataCacheSet[0][0];
         fm.all("InsuredNo").value = turnPage.arrDataCacheSet[0][1];
         fm.all("InsuredName").value = turnPage.arrDataCacheSet[0][2];
         fm.all("CValidate").value = turnPage.arrDataCacheSet[0][3];
         fm.all("PayToDate").value = turnPage.arrDataCacheSet[0][4];
         fm.all("LCRnewFlag").value = turnPage.arrDataCacheSet[0][5];
         fm.all("Prem").value = turnPage.arrDataCacheSet[0][6];
         fm.all("Amnt").value = turnPage.arrDataCacheSet[0][7];
      
	 fm.all('fmtransact').value = "QUERY||RnewFlag";
	 //alert("----begin---");
         fm.all('RnewFlag').value = "-1";//为了让默认为自动续保
	 fm.submit();
	 	
}

</script>