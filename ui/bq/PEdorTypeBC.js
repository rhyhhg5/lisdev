
//  受益人资料变更--方法
//  create : 0 孙  迪 2005-3-29 15:25
//  modify : 1 

var showInfo;
var turnPage = new turnPageClass();        

function initBnfGrid()
{
	var i = LCBnfGrid.mulLineCount;
	if (i == 1)
	{
	  fm.LCBnfGrid1.value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
	  fm.LCBnfGrid2.value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
		fm.LCBnfGrid3.value = "1";
		fm.LCBnfGrid5.value = "0";
		fm.LCBnfGrid8.value = "1";
	}
  else
	{
	  fm.LCBnfGrid1[i-1].value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 1);
	  fm.LCBnfGrid2[i-1].value = PolGrid.getRowColData(PolGrid.getSelNo() - 1, 2);
		fm.LCBnfGrid3[i-1].value = "1";
		fm.LCBnfGrid5[i-1].value = "0";
		fm.LCBnfGrid8[i-1].value = "1";
	}
}

function queryRisk()
{	
	var contNo = fm.ContNo.value;
	var insuredNo = fm.InsuredNo.value;
 //限制1201-1204等不能做受益人变更的险种，相应的定义在lmriskedoritem
	var strSql = "select riskseqno, polno, riskcode, (select riskname from lmrisk where riskcode=a.riskcode) "
	           + "from lcpol a " 
	           + "where riskcode in (select riskcode from lmriskedoritem where edorcode='BC') "
	           + "and a.ContNo = '" + contNo + "' "
	           + "and a.InsuredNo = '" + insuredNo + "'";
	turnPage.queryModal(strSql, PolGrid);
	if (PolGrid.mulLineCount == 1)
	{
	  fm.PolGridSel.checked = "true";
	}
	else
	{
	  fm.PolGridSel[0].checked = "true";
	}
}

function queryBnfInfo()
{
  var edorState;
  var sql = "select EdorState from LPEdorItem " +
            "where EdorNo = '" + fm.EdorNo.value + "' " +
            "and EdorType = '" + fm.EdorType.value + "' " +
            "and ContNo = '" + fm.ContNo.value + "' " +
            "and InsuredNo = '" + fm.InsuredNo.value + "' ";
  var result = easyExecSql(sql);
  if (result != null)
  {
    edorState = result[0][0];
  }
  if (edorState == "3") //未录入
  {
    sql = "select (select RiskSeqNo from LCPol where PolNo = a.PolNo), PolNo, BnfType, Name, IDType, IDNo, " +
          "       RelationToInsured, BnfGrade, BnfLot, Sex, Birthday " +
          "from LCBnf a " +
          "where ContNo = '" + fm.ContNo.value + "' " +
          "and InsuredNo = '" + fm.InsuredNo.value + "' ";
  }
  if (edorState == "1") //已录入
  {
    sql = "select (select RiskSeqNo from LCPol where PolNo = a.PolNo), PolNo, BnfType, Name, IDType, IDNo, " +
          "       RelationToInsured, BnfGrade, BnfLot, Sex, Birthday " +
          "from LPBnf a " +
          "where EdorNo = '" + fm.EdorNo.value + "' " +
          "and EdorType = '" + fm.EdorType.value + "' " +
          "and ContNo = '" + fm.ContNo.value + "' " +
          "and InsuredNo = '" + fm.InsuredNo.value + "'";
  }
  turnPage.queryModal(sql, LCBnfGrid);     
}

//function reportDetailClick()
//{
//	fm.PolNo.value = PolGrid.getRowColData(PolGrid.getSelNo()-1, 1);
//	easyExecSql("select * from lpgrpedormain where GrpContNo='" + fm.GrpContNo.value + "' and EdorState<>'0'");
//  document.all("divBnf").style.display = "";
//	initQuery();
//}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  try
  {
    showInfo.close();
    window.focus();
  }
  catch (ex) {}
  
  if (FlagStr == "Fail" )
  {      
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
		if (tTransact=="QUERY||MAIN"||tTransact=="QUERY||FIRST"||tTransact=="QUERY||PERSON")
		{ 
			//var iArray;
			////清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		//turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			////保存查询结果字符串
 	    //
 		 	//turnPage.strQueryResult  = Result;
  		////使用模拟数据源，必须写在拆分之前
  		//turnPage.useSimulation   = 1;  
      //
  		////查询成功则拆分字符串，返回二维数组
  		//var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
      //if (tArr != null) {	
      //  turnPage.arrDataCacheSet =chooseArray(tArr,[5,11,14,15,8,7,9]);			
      //  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
      //  turnPage.pageDisplayGrid = LCBnfGrid;    
      //  
      //   //设置查询起始位置
      //  turnPage.pageIndex       = 0;
      //  //在查询结果数组中取出符合页面显示大小设置的数组
      //     var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
      //  //调用MULTILINE对象显示查询结果
      //     displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
      //} 
      //else 
      //{
      //  LCBnfGrid.delBlankLine("LCBnfGrid");
      //}
		} 
		else 
		{
      var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
      returnParent();
  	}
  }
}

function initQuery()
{
   var i = 0;
   if (fm.all('grpsignal').value=="Y"){fm.all('fmtransact').value = "QUERY||FIRST";}
   if (fm.all('grpsignal').value=="N"){fm.all('fmtransact').value = "QUERY||PERSON";}
   fm.submit();
}

function checkRelation()
{
  var count = 0;
  for (i = 0; i < LCBnfGrid.mulLineCount; i++)
  {
    var relation = LCBnfGrid.getRowColData(i, 7);
    if (relation == "00") //本人
    {
      count++;
    }
  }
  if (count > 1)
  {
    alert("受益人不能有多个为本人！");
    return false;
  }
  return true;
}

function beforeSubmit()
{
  if (PolGrid.mulLineCount == 0)
  {
    alert("没有可以操作的险种！");
    return false;
  }

  if (!LCBnfGrid.checkValue())
  {
    return false;
  }
  
  //if (!checkRelation())
 // {
 //   return false;
 // }
  
  if(!checkPrecision())
  {
    return false;  
  }
  if(!checkBnfIdNo())
  {
    return false;  
  }
  
 // if (!chekBnfLot())
 // {
 //   return false;
 // }
  return true;
}

function checkPrecision()
{
  for (i = 0; i < LCBnfGrid.mulLineCount; i++)
  {
    //截去无效的0后转换成字符串，方便后面的精度计算
    var bnfLot = parseFloat(LCBnfGrid.getRowColData(i, 9)) + "";
    var decimalPart = bnfLot.substring(bnfLot.indexOf(".") + 1);

    if(decimalPart.length > 2)
    {
      //alert("序号" + (i + 1) + "的受益份额只能精确到百分位。");
      alert("受益份额只能精确到百分位。");
    }
  }
  
  return true;
}

////校验受益份额
//function chekBnfLot()
//{
//  for (i = 0; i < PolGrid.mulLineCount; i++)
//  {
//    var k = 0;
//    var bnfLot = 0;
//    var riskSeqNo = PolGrid.getRowColData(i, 1);
//    for (j = 0; j < LCBnfGrid.mulLineCount; j++)
//    {
//      if (riskSeqNo == LCBnfGrid.getRowColData(j, 1))
//      {
//         bnfLot += parseFloat(LCBnfGrid.getRowColData(j, 9));
//         k++;
//      }
//    }
//    if ((k > 0) && (bnfLot.toFixed(2) != 1))
//    {
//      alert("险种序号为" + riskSeqNo + "的险种受益份额之和不等于1!");
//      return false;
//    }
//  }
//  return true;
//}

function checkBnfIdNo()
{
  for (i = 0; i < LCBnfGrid.mulLineCount; i++)
  {
	var idType = LCBnfGrid.getRowColData(i, 5);
	var idNo = LCBnfGrid.getRowColData(i, 6);
	var sex = LCBnfGrid.getRowColData(i, 10);
	var birthday = LCBnfGrid.getRowColData(i, 11);

		if(idType == "0" || idType == "5"){
		var strCheckIdNo = checkIdNo(idType,idNo,birthday,sex);
		if ("" != strCheckIdNo && strCheckIdNo!=null)
		{
			alert(strCheckIdNo);
			return false;
		}
	 }
	
  }
  
  return true;
}

//保存提交
function SaveBnf()
{
  if (!beforeSubmit())
  {
    return false;
  }
	var showStr = "正在保存受益人信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	if (fm.all('grpsignal').value=="Y"){fm.all('fmtransact').value = "INSERT||MAIN";}
	if (fm.all('grpsignal').value=="N"){fm.all('fmtransact').value = "INSERT||PERSON";}
//	fm.all("fmtransact").value = "INSERT||MAIN";	
	fm.submit();
}

//返回父窗口
function returnParent()
{
  if (TypeFlag == "G")
  {
  	top.close();
  	return;
  }

  if (fm.all('grpsignal').value=="Y")
  {
  	top.opener.queryClick();
    top.opener.queryClick2()
  }
	  arrSelected = new Array();
		arrSelected[0] = new Array();
		arrSelected[0][0] = fm.all( 'EdorAcceptNo' ).value;
		arrSelected[0][1] = fm.all( 'ContNo' ).value;
		arrSelected[0][2] = '3';
  try
  {
  	top.opener.afterQuery2( arrSelected );
  }catch(Ex){}
    
 	if (fm.all('grpsignal').value!="Y")
 	{
    top.opener.getEdorItem();
  }
  top.close();
}

