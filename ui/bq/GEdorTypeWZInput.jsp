<html> 
<% 
//程序名称：
//程序功能：无名单增人
//创建日期：20060623
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeWZ.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeWZInit.jsp"%>
  <title>无名单增人</title>
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeWZSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 集体保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpContNo>
      </TD>   
    </TR>
  </TABLE> 
  
 	<Div id= "divLCGrpInfo" style= "display: ''">
  <table>
    	<tr>
    		<td class= titleImg>
    			 客户投保信息
    		</td>
    		<td class= titleImg>
    			 <Input type =button class=cssButton value="保障计划查询" id="submitButton" onclick="getLCContPlan()">
    		</td>
    	</tr>
    </table>
    <Div  id= "divGroupPol2" style= "display: ''">
      <table  class= common>
       <TR>
          <TD  class= title>
            集体客户号
          </TD>
          <TD  class= input>
            <Input class="readonly" name=AppntNo readonly >
          </TD>  
          <TD  class= title>
            集体保单号
          </TD>
          <TD  class= input>
            <Input class="readonly" name=GrpContNo2 readonly >
          </TD>   
          <TD  class= title>
            单位名称
          </TD>
          <TD  class= input>
            <Input class="readonly" name=GrpName readonly >
          </TD>  
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            投保日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=HandlerDate readonly >
          </TD>
      
          <TD  class= title>
            生效日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=CValiDate readonly >
          </TD>
          <TD  class= title>
            交至日期
          </TD>
          <TD  class= input>
            <Input class="readonly" name=PayToDate readonly >
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title>
            投保人数
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=Peoples2 readonly >
          </TD>
      
          <TD  class= title>
            总保费
          </TD>
          <TD  class= input>
            <Input class="readonly" name=Prem readonly >
          </TD>
          <TD  class= title8>
            保费余额
          </TD>
          <TD  class= input>
            <Input class="readonly" name=AccBala readonly >
          </TD>       
        </TR>
        <TR  class= common>
          <TD  class= title>
            签单机构
          </TD>
          <TD  class= input>
            <Input class="readonly"  name=SignCom readonly >
          </TD>
      
          <TD  class= title>
          </TD>
          <TD  class= input>
          </TD>
          <TD  class= title8>
          </TD>
          <TD  class= input>
          </TD>       
        </TR>
        <!--保障计划信息-->
        <TR class= common>
          <TD class= title colspan=6>
            <table>
              <tr>
                <td class=common>
                  <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCGrpPol);">
                </td>
                <td class=titleImg>增加被保险人信息</td>
              </tr>
            </table>
            <Div id="divLCGrpPol" style="display: ''">
              <table class=common>
                <tr class=common>
                  <td text-align: left colSpan=1>
                    <span id="spanLCContPlan">  </span>
                  </td>
                </tr>
              </table>
            </div>
          </TD>    
        </TR>
        <TR  class= common>
          <TD  class= title>
            人均期交保费
          </TD>
          <TD  class= input>
            <Input class="common" name=PremWZ onChange="onPremWZChange();">
          </TD>
      
          <TD  class= title>
            增加被保险人数
          </TD>
          <TD  class= input>
            <Input class="common" name=Peoples2WZ onChange="onPeoples2WZChange();">
          </TD>
          <TD  class= title8>
            本次应收保费
          </TD>
          <TD  class= input>
            <Input class="common" name=SumPremWZ onChange="onSumPremWZChange();">
          </TD>    
        </TR>
        <TR class= common>
          <TD  class= title8>
            生效日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EdorValiDate onChange="onEdorValiDateChange();">
          </TD>  
        </TR>
        <!--保障计划下险种信息-->
        <TR class= common>
          <TD class= title colspan=6>
            <table>
              <tr>
                <td class=common>
                  <IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCGrpPol);">
                </td>
                <td class=titleImg>保障计划下险种信息</td>
              </tr>
            </table>
            <Div id="divLCGrpPol" style="display: ''">
              <table class=common>
                <tr class=common>
                  <td text-align: left colSpan=1>
                    <span id="spanLCContPlanRisk">  </span>
                  </td>
                </tr>
              </table>
            </div>
          </TD>    
        </TR>
      </table>
      <br>
   	  <Input type =button class=cssButton value="保  存" id="submitButton" onclick="submitData()">
   	  <Input type =button class=cssButton value="取  消" onclick="cancel()">
   	  <Input type =button class=cssButton value="返  回" onclick="returnParent()">
    </Div>
	</Div>

	 <input type=hidden id="ContPlanCode" name="ContPlanCode">
	 <input type=hidden id="contNo" name="ContNo">
	 <input type=hidden id="LastPayToDate" name="LastPayToDate">
	 <input type=hidden id="CurPayToDate" name="CurPayToDate">
	 <!--人均期交保费-->
	 <input type=hidden id="AvgPrem" name="AvgPrem">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
