<%
//程序名称：GEdorTypeZTSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
  LPPolSet tLPPolSet = new LPPolSet();
  LPPolSchema tLPPolSchema = new LPPolSchema();
   
  PGrpEdorWTDetailUI tPGrpEdorWTDetailUI   = new PGrpEdorWTDetailUI();
 
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
 System.out.println("-----"+tG.Operator);
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
    
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String Result ="";
  String transact = "";
  String mContType = "";
   
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("---------transact:"+transact);
 
  //个人批改信息
 tLPGrpEdorMainSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
 tLPGrpEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
 tLPGrpEdorMainSchema.setEdorType(request.getParameter("EdorType"));
 if (transact.equals("INSERT||MAIN"))
 {
 //准备信息
	 String tGridNo[] = request.getParameterValues("LPPolGridNo");
	 String tPolNo[] = request.getParameterValues("LPPolGrid1");
	 String tInsuredNo[] = request.getParameterValues("LPPolGrid2");
	 String tInsuredName[] = request.getParameterValues("LPPolGrid3");
	 String tCValiDate[] = request.getParameterValues("LPPolGrid4");
   String tPrem[] = request.getParameterValues("LPPolGrid5");
	
			
		int Count = tGridNo.length;
	  String tFlagChk[]=request.getParameterValues("InpLPPolGridChk");
		System.out.println("--count"+Count);
	  for (int i=0;i<Count;i++)
	  {  	
  		if(tFlagChk[i].equals("1"))
  		{
  			tLPPolSchema = new LPPolSchema();
	  		tLPPolSchema.setPolNo(tPolNo[i]);
		  	tLPPolSchema.setInsuredNo(tInsuredNo[i]);
		  	tLPPolSchema.setInsuredName(tInsuredName[i]);
		  	tLPPolSchema.setCValiDate(tCValiDate[i]);
		  	tLPPolSchema.setPrem(tPrem[i]);
		  	tLPPolSet.add(tLPPolSchema);
  		}
  	}
  }
 
 try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();   
	 //保存个人保单信息(保全)	
	  //tVData.addElement(mContType);
  	tVData.addElement(tLPGrpEdorMainSchema);
  	tVData.addElement(tG);
  	if (transact.equals("INSERT||MAIN"))
 		{
  		tVData.addElement(tLPPolSet);
  	}
  
		tPGrpEdorWTDetailUI.submitData(tVData,transact);
    }
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPGrpEdorWTDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "提交成功。";
    	FlagStr = "Success";
    	if (tPGrpEdorWTDetailUI.getResult()!=null&&tPGrpEdorWTDetailUI.getResult().size()>0)
    	{
    		Result = (String)tPGrpEdorWTDetailUI.getResult().get(0);
    		System.out.println("----result:"+Result);
    		if (Result==null||Result.trim().equals(""))
    		{
    			FlagStr = "Fail";
    			Content = "提交失败!!";
    		}
    	}
    }
    else                                                                           
    {
    	Content = "提交失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

