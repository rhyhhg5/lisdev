<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：
//创建日期：2006-01-13
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="GEdorTypeTY.js"></SCRIPT>
	<%@include file="GEdorTypeTYInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css><%--
    2012-5-3 xp 去掉缴费信息的部分
	<script type="text/javascript">
	 var tsql=" 1 and code in (select code from ldcode where codetype=#grppaymode#) ";
	</script>
--%><title>团体万能追加保费 </title>
</head>
<body onload="initForm();">
  <form action="./GEdorTypeTYSubmit.jsp" method=post name=fm target="fraSubmit">
  <table class="common">
    <tr class="common"> 
      <td class="title">受理号</TD>
      <td class="input"> 
        <input class="readonly" readonly name="EdorNo">
      </td>
      <td class="title">批改类型</TD>
      <td class="input">
      	<input class="readonly" readonly name="EdorType" type="hidden">
      	<input class="readonly" readonly name="EdorTypeName">
      </td>
      <td class="title">集体保单号</TD>
      <td class="input">
      	<input class="readonly" readonly name="GrpContNo">
      </td>
    </tr>
  </table>
  <%@include file="SpecialInfoCommon.jsp"%>
  <table>
    <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divGroup);">
      </td>
      <td class= titleImg>公共账户追加金额
      </td>
    </tr>
  </table>
  <div id= "divGroup" style= "display: ''">
    <table class= common>
			<tr class= common>
				<td>
					<span id="spanPolGrid" >
					</span>
				</td>
			</tr>
		</table>
		<div id="divPage3" align=center style="display: 'none' ">
      <input class=cssButton value="首  页" type=button onclick="turnPage3.firstPage();"> 
      <input class=cssButton value="上一页" type=button onclick="turnPage3.previousPage();"> 					
      <input class=cssButton value="下一页" type=button onclick="turnPage3.nextPage();"> 
      <input class=cssButton value="尾  页" type=button onclick="turnPage3.lastPage();">
    </div>	
  </div>
  <table id = "pACCList">
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLPInsured);">
      </td>
      <td class= titleImg>个人账户清单</td>
    </tr>
   </table>
    <Div id= "divLPInsured" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td>
					<span id="spanInsuredListGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>	
  	</div>	 
   <table id = "failDateList">
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFail);">
      </td>
      <td class= titleImg>无效数据</td>
    </tr>
   </table>
    <Div id= "divFail" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanFailGrid">
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage2" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
      </Div>	
  	</div>
    <br><%--
    <!-- 刘鑫新增  begin -->
    2012-5-3 xp 去掉缴费信息的部分
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFee);">
    		</td>
    		<td class= titleImg>
    			 缴费信息
    		</td>
    	</tr>
    </table>

    <Div  id= "divFee" style= "display: ''">  
    
    	<table class=common style = "display: none">
      	<tr class = common>
      		<td class= common>
    			 缴费主体
    		</td>
					<td>
						  <input type="checkbox" name="ImpartCheck2" class="box" onclick="ImpartCheck2Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;投保人全额承担&nbsp;&nbsp;&nbsp;&nbsp;
						  <input type="checkbox" name="ImpartCheck2" class="box" onclick="ImpartCheck2Radio2();">&nbsp;&nbsp;&nbsp;&nbsp;被保险人全额承担&nbsp;&nbsp;&nbsp;&nbsp;
						  <input type="checkbox" name="ImpartCheck2" class="box" onclick="ImpartCheck2Radio3();">&nbsp;&nbsp;&nbsp;&nbsp;双方共同承担，其中投保人承担
						  <input class="common6" name="ImpartCheck2">%,被保险人承担
						  <input class="common6" name="ImpartCheck2">%
					</td>			
				</tr>  
      </table>
      <!-- by gzh -->
    	<table class=common>
      	<tr class = common>
      		<td class= common>
    			 追加保险费信息
    		</td>
			<td>
		    个人账户个人缴费金额<input class="common6" name="num1" verify="个人账户个人缴费金额|num">,
		    个人账户单位缴费金额<input class="common6" name="num2" verify="个人账户单位缴费金额|num">
			</td>			
		</tr>  
      </table>  
    	<!-- by gzh end -->
    	<table>
    	<tr>
    	<td>
    	<font size=2 color="#ff0000">
	     <b>新、旧缴费方式对照说明</b> 
		   &nbsp;&nbsp;1现金--->1现金、3转账支票--->3支票 </font>
    	</td>
    	</tr>
    	</table>
    	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDWFee);">
    		</td>
    		<td class= titleImg>
    			 公共账户、个人账户单位交费部分
    		</td>
    	</tr>
    	</table>
    	<Div  id= "divDWFee" style= "display: ''">
	      <table class=common> 
	        <TR  class= common>
	          <TD  class= title8>
	            缴费方式
	          </TD>
	          <TD  class= input8>
	          	<!--<Input name=PayMode CLASS="code" type="hidden">-->
	          <Input class=codeNo name=PayMode  VALUE="1" MAXLENGTH=1 verify="缴费方式|notnull&code:PayMode" ondblclick="return showCodeList('PayMode',[this,PayModeName],[0,1],null,tsql,'1',1);" onkeyup="return showCodeListKey('GrpPayMode',[this,PayModeName],[0,1],null,tsql,'1',1);"><input class=codename name=PayModeName readonly=true elementtype=nacessary>
	          </TD>
	          <TD  class= title8>
	            开户银行
	          </TD>
	          <TD  class= input8>
	            <Input class=codeNo  name=BankCode style="display:''"  MAXLENGTH=1 verify="开户银行|code:bank&len<=24" ondblclick="return showCodeList('Bank',[this,BankCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankCodeName],[0,1],null,null,null,1);"><input class=codename name=BankCodeName style="display:''" readonly=true >
	          </TD>
	          <TD  class= title8>
	            账&nbsp;&nbsp;&nbsp;号
	          </TD>
	          <TD  class= input8>
	            <Input class= common8 name=BankAccNo style="display:''" verify="帐号|len<=40">
	          </TD>       
	        </TR> 
	        <TR class= common>    
	          <TD  class= title8>
	            户&nbsp;&nbsp;&nbsp;名
	          </TD>
	          <TD  class= input8>
	            <Input class= common8 name=AccName style="display:''" verify="户名|len<=40">
	          </TD>  
	        </TR>  
	        </table>   
 		</div>
 		<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGRFee);">
    		</td>
    		<td class= titleImg>
    			 个人账户个人交费部分
    		</td>
    	</tr>
    	</table>
    	<Div  id= "divGRFee" style= "display: ''">
	      <table class=common> 
	        <TR  class= common>
	          <TD  class= title8>
	            <INPUT TYPE="checkbox" NAME="GRMixComFlag" checked   disabled>单位代扣代交
	          </TD>
	        </TR>  
	        </table>   
 		</div>
    </Div>  
    <br> 
    
    <!-- 刘鑫新增  end  -->
    
	--%><Div  id= "divSubmit" style="display:''">
	  <table class = common>
			<TR class= common>
			   <input type=Button name="importButton" class=cssButton value="导  入"  onclick="importInsured();">
	   		 <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
	   		 <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
			</TR>
	 	</table>
	</Div>
	<input type=hidden id="fmtransact" name="fmtransact"><p align="right"></p>
	<input type=hidden id="ContType" name="ContType">
	<input type=hidden id="ContNo" name="ContNo">
	<input type=hidden id="EdorValiDate" name="EdorValiDate">
	<input type=hidden id="InsuredId" name="InsuredId">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>