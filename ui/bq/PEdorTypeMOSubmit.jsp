<%
//程序名称：PEdorTypeMOSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=gb2312" %>

<%
  //个人批改信息
  System.out.println("---MO submit---");
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = request.getParameter("fmtransact");
  System.out.println("transact: " + transact);
  
  //全局变量
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");
  
  //批改信息
  LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
  tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
  tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));
  
  LPMoveSchema tLPMoveSchema = new LPMoveSchema();
  tLPMoveSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPMoveSchema.setEdorType(request.getParameter("EdorType"));
  tLPMoveSchema.setRiskCode(request.getParameter("RiskCode"));
  tLPMoveSchema.setPolNoOld(request.getParameter("PolNo"));
  tLPMoveSchema.setManageComOld(request.getParameter("ManageCom"));
  tLPMoveSchema.setManageComNew(request.getParameter("ManageCom2"));
  tLPMoveSchema.setOldCoName(request.getParameter("ManageName"));
  tLPMoveSchema.setNewCoName(request.getParameter("ManageName2"));
  tLPMoveSchema.setStandPrem(request.getParameter("Prem"));
  tLPMoveSchema.setLastGetDate(request.getParameter("LastGetDate"));
  tLPMoveSchema.setGetStartDate(request.getParameter("GetStartDate"));
  tLPMoveSchema.setLeavingMoney(request.getParameter("LeavingMoney"));
  tLPMoveSchema.setSumPrem(request.getParameter("SumPrem"));
  tLPMoveSchema.setPaytoDate(request.getParameter("PayToDate"));
  System.out.println("tLPMoveSchema: " + tLPMoveSchema.encode());

  //准备传输数据
  VData tVData = new VData();  
  tVData.add(tGlobalInput);
  tVData.add(tLPEdorMainSchema); 
  tVData.add(tLPMoveSchema); 
      
  CErrors tError = null;                 
  String FlagStr = "";
  String Content = "";
  String Result = ""; 
  String lpMoveResult = ""; 
  
  PEdorMODetailUI tPEdorMODetailUI = new PEdorMODetailUI();
  //操作失败
  if (!tPEdorMODetailUI.submitData(tVData, transact)) {
    VData rVData = tPEdorMODetailUI.getResult();
    System.out.println("Submit Failed! " + (String)rVData.get(0));
    
    FlagStr = "Fail"; 
    if (transact.equals("INSERT")) {   
      Content = "保存失败!" + (String)rVData.get(0);
    }
    else if (transact.equals("QUERY")) {
      Content = "查询失败!" + (String)rVData.get(0);
    }
  }
  //操作成功
  else {
  	FlagStr = "Success";
  	
  	if (transact.equals("INSERT")) {   
      Content = "保存成功!";
    }
    else if (transact.equals("QUERY")) {
      Content = "查询成功!";
      
  		if (tPEdorMODetailUI.getResult()!=null && tPEdorMODetailUI.getResult().size()>0) {
  			Result = (String)tPEdorMODetailUI.getResult().get(0);
  			lpMoveResult = (String)tPEdorMODetailUI.getResult().get(1);
  			
  		  if (Result==null || Result.trim().equals("")) {
  				FlagStr = "Fail"; 
  				Content = "查询失败!";
  		  }
  	  }
	  }
  }
	
  //添加各种预处理
  System.out.println("------------Result is------------\n" + Result);
%>   
                   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>", "<%=lpMoveResult%>");
</script>
</html>

