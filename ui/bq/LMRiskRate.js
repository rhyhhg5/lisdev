
var turnPage = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;

function queryRiskRateGrid() {
	var strSql = "select year,quarter,riskrate "
        + " from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc  "
	    + " with ur";
	var strResult = easyExecSql(strSql);
//	if(!strResult){
//		return true;
//	}
	turnPage.pageLineNum = 10;
	turnPage.queryModal(strSql,RiskRateGrid);
	
}


//保存
function submitForm(){

	if(fm.Year.value==""|| fm.Quarter.value=="" || fm.RiskRate.value=="" ){
		alert("请输入必填信息！");
		return false;
	}

	var Sql = "select Year,quarter "
        + "from  ldriskrate where  codetype='FXDJ' and state='1' order by Year desc,quarter desc fetch first row only  "
	    + " with ur";
	var strResult = easyExecSql(Sql);
	if(strResult){
	
			if(strResult[0][0]==fm.Year.value){
				if(strResult[0][1]=="4"){
					alert("请按照时间顺序依次录入年度或季度");
					return false;
				}else if((strResult[0][1]=="1" && fm.Quarter.value=="2") || (strResult[0][1]=="2" && fm.Quarter.value=="3") || (strResult[0][1]=="3" && fm.Quarter.value=="4"))
					{
					}else{
						alert("请按照时间顺序依次录入年度或季度");
						return false;
					}
			}else{
				var lastYear=parseInt(strResult[0][0]);
				var newYear=parseInt(fm.Year.value);
				if(newYear - lastYear == 1 && fm.Quarter.value=="1" && strResult[0][1]=="4"){
				
				}else {
					alert("请按照时间顺序依次录入年度或季度");
					return false;

				}
			}
	}
	fm.tYear.value=fm.Year.value;
	fm.tQuarter.value=fm.Quarter.value;
	fm.tRiskRateName.value=fm.getRiskRateName.value;
	if(confirm("确认保存吗？")){	
		fm.fmtransact.value="INSERT||MAIN";
		var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.action = "LMRiskRateSave.jsp";
		fm.submit();
	}else{
		alert("取消保存！");
	}
	
}

function updateClick(){
	var arrReturn = new Array();
	var tSel = RiskRateGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录。" );
		return false;
	}
	var result = new Array();
	result[0] = new Array();
	result[0] = RiskRateGrid.getRowData(tSel-1);
	
	fm.all("tYear").value = fm.all("mYear").value;
	fm.all("tQuarter").value = fm.all("mQuarter").value;
	fm.all("tRiskRateName").value = fm.all("RiskRateName").value;

	
	if (confirm("您确实想修改该记录吗?"))
	  {
	  var i = 0;
	  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.fmtransact.value = "UPDATE||MAIN";
	  fm.action = "LMRiskRateSave.jsp";
	  fm.submit(); //提交
	  }
	  else
	  {
	    alert("您取消了修改操作！");
	  }
}


function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}




function onSelected(parm1,parm2)
{ 
    fm.all("mYear").value = RiskRateGrid.getRowColData(RiskRateGrid.getSelNo() - 1, 1);
	fm.all("mQuarter").value = RiskRateGrid.getRowColData(RiskRateGrid.getSelNo() - 1, 2);	
    fm.all("RiskRateName").value = RiskRateGrid.getRowColData(RiskRateGrid.getSelNo() - 1, 3);
    if(fm.all("RiskRateName").value=="A"){
    	fm.all("mRiskRate").value='1';
    }else if(fm.all("RiskRateName").value=="B"){
    	fm.all("mRiskRate").value='2';
    }else if(fm.all("RiskRateName").value=="C"){
    	fm.all("mRiskRate").value='3';
    }else if(fm.all("RiskRateName").value=="D"){
    	fm.all("mRiskRate").value='4';
    }

}