<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="GEdorTypeWD.js"></SCRIPT>
	<%@include file="GEdorTypeWDInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<title>新增被保人 </title>
</head>
<body  onload="initForm();">
  <form action="./GEdorTypeWDSubmit.jsp" method=post name=fm target="fraSubmit">
	<table class=common>
		<TR  class= common> 
		  <TD  class= title > 受理号</TD>
		  <TD  class= input > 
		    <input class="readonly" readonly name=EdorNo >
		  </TD>
		  <TD class = title > 批改类型 </TD>
		  <TD class = input >
		  	<input class = "readonly" readonly name=EdorType type=hidden>
		  	<input class = "readonly" readonly name=EdorTypeName>
		  </TD>
		  <TD class = title > 集体保单号 </TD>
		  <TD class = input >
		  	<input class = "readonly" readonly name=GrpContNo>
		  </TD>
		</TR>
	 </TABLE> 
	 <br>
	 <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          未做保全被保人信息
        </td>
      </tr>
    </table>
    <table class = common>
      <tr class = common>
        <td class = title>
      		姓名
      	</td>
      	<td class = input>
      		<input type="hidden" class = common  name=InsuredNo1>
      		<input class = common  name=Name>
          </TD>
        <td class = title>
      	  证件号码
      		</td>
      	<td class = input>
      		<input class = common  name=IdNo>
        </TD>
        <td class = title>
   				其它号码
      	</td>
      	<td class = input>
					<input class = common  name=OthIdNo>
        </TD>  
      </tr>
    </table>
    <input type=button value="查  询" class=cssButton onclick="queryClick();"> 
    <br><br>
    <Div  id= "divLPInsured" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage" align=center style= "display: 'none' ">
        <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 			
      </Div>		
  	</div>
  	<br>
   <table>
     <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
      </td>
      <td class= titleImg>
        无名单被保人删除清单
      </td>
      <td> 
        <input type=Button name="plan" class= cssButton value="保障计划" onclick="queryPlan();">
      </td>
    </tr>
   </table>
    <Div id= "divLPInsured" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanInsuredListGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>	
  	</div>	 
  	<br>
  <table>
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFail);">
      </td>
      <td class= titleImg>无效数据</td>
    </tr>
   </table>
    <Div id= "divFail" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanFailGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage2" align=center style= "display: 'none'">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
      </Div>	
  	</div>
  	<br>
	<Div  id= "divSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
					<Input type=button value="进入个人保全"  class=cssButton onclick="addEdor();">
	   		 <input type=Button name="doImport" class=cssButton value="磁盘导入"  onclick="importInsured();">
	   		 <!--<input type=Button name="del" class= cssButton  value="删  除" onclick="deleteInsured();">-->
	   		 <Input type=button value="撤销个人保全" class=cssButton onclick="delEdor();">
	   		 <input type=Button name="save" class= cssButton value="保  存" onclick="saveEdor();">
	   		 <input type=Button name="goBack" class= cssButton value="返  回" onclick="returnParent();">
			</TR>
	 	</table> 
	</Div>
	<input type=hidden id="fmtransact" name="fmtransact">
	<input type=hidden id="ContType" name="ContType">
	<input type=hidden id="ContNo" name="ContNo">
	<input type=hidden id="EdorValiDate" name="EdorValiDate">
	<input type=hidden id="InsuredId" name="InsuredId">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>
</html>