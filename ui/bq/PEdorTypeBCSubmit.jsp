<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  LPBnfSet tLPBnfSet = new LPBnfSet();
  LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();
  PEdorBCDetailUI tPEdorBCDetailUI   = new PEdorBCDetailUI();
  
  CErrors tError = null;
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //个人保单批改信息
	tLPEdorItemSchema.setPolNo(request.getParameter("PolNo"));
	tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
	tLPEdorItemSchema.setInsuredNo(request.getParameter("InsuredNo"));
	tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
  tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	System.out.println("tLPEdorItemSchema.getInsuredNo() = "+tLPEdorItemSchema.getInsuredNo());
	
 if (transact.equals("INSERT||MAIN")||transact.equals("INSERT||PERSON"))
 {
   int Count = 0;
   String tGridTest[] = request.getParameterValues("LCBnfGridNo");
      
   if (tGridTest == null) 
       Count = 0;
   else 
	     Count = tGridTest.length;
   if (Count > 0)
	 {
	     String tRiskSeqNo[] = request.getParameterValues("LCBnfGrid1");
	     String tPolNo[] = request.getParameterValues("LCBnfGrid2");
	     String tBnfType[] = request.getParameterValues("LCBnfGrid3");
	     String tName[] = request.getParameterValues("LCBnfGrid4");
	     String tIDType[] = request.getParameterValues("LCBnfGrid5");
	     String tIDNo[] = request.getParameterValues("LCBnfGrid6");
	     String tRelationToInsured[] = request.getParameterValues("LCBnfGrid7");
	     String tBnfGrade[] = request.getParameterValues("LCBnfGrid8");
	     String tBnfLot[] = request.getParameterValues("LCBnfGrid9");
	     String tSex[] = request.getParameterValues("LCBnfGrid10");
	     String tBirthday[] = request.getParameterValues("LCBnfGrid11");
	     for (int i=0; i<Count; i++)
	     {  	
	     
	        LPBnfSchema tLPBnfSchema   = new LPBnfSchema();
	        tLPBnfSchema.setEdorNo(request.getParameter("EdorNo"));
	        tLPBnfSchema.setEdorType(request.getParameter("EdorType"));
	        tLPBnfSchema.setPolNo(tPolNo[i]);
	        tLPBnfSchema.setBnfType(tBnfType[i]);
	        tLPBnfSchema.setName(tName[i]);
	        tLPBnfSchema.setIDType(tIDType[i]);
	        tLPBnfSchema.setIDNo(tIDNo[i]);
	        tLPBnfSchema.setRelationToInsured(tRelationToInsured[i]);
	        tLPBnfSchema.setBnfGrade(tBnfGrade[i]);
	        tLPBnfSchema.setBnfLot(tBnfLot[i]);
	        tLPBnfSchema.setSex(tSex[i]);
	        tLPBnfSchema.setBirthday(tBirthday[i]);
	        tLPBnfSet.add(tLPBnfSchema);
  	   }
  	}
  }

 try
 {
 	 VData tVData = new VData();
   tVData.addElement(tG);
   tVData.addElement(tLPEdorItemSchema);
   tVData.addElement(tLPBnfSet);
   tPEdorBCDetailUI.submitData(tVData,transact);
   
	
	}
	catch(Exception ex)
	{
	    ex.printStackTrace();
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorBCDetailUI.mErrors;
    if (!tError.needDealError())
    {
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||FIRST")||transact.equals("QUERY||PERSON"))
    	{
	    	if (tPEdorBCDetailUI.getResult()!=null&&tPEdorBCDetailUI.getResult().size()>0)
	    	{
	    		Result = (String)tPEdorBCDetailUI.getResult().get(0);
	    		if (Result==null||Result.trim().equals(""))
	    		{
	    			FlagStr = "Fail";
	    			Content = "提交失败!!";
	    		}
	    	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>