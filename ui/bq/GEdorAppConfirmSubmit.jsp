<%
//程序名称：PEdorAppConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT language="javascript">
var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
</SCRIPT>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
    String flag;
    String content;
    String edorNo = request.getParameter("EdorNo");
    GlobalInput gi = (GlobalInput)session.getValue("GI");
    
    //集体批改信息
    LPGrpEdorMainSchema tLPGrpEdorMainSchema  = new LPGrpEdorMainSchema();
    tLPGrpEdorMainSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
    tLPGrpEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPGrpEdorMainSchema.setGrpContNo(request.getParameter("GrpContNo"));
    tLPGrpEdorMainSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
    tLPGrpEdorMainSchema.setEdorAppDate(request.getParameter("EdorAppDate"));
             
    VData data = new VData();  
    data.add(gi);   
    data.add(tLPGrpEdorMainSchema);
    PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI = new PGrpEdorAppConfirmUI();
    if (!tPGrpEdorAppConfirmUI.submitData(data, "INSERT||GEDORAPPCONFIRM"))
    {
      flag = "Fail";
      content = "保全理算失败！原因是：" + tPGrpEdorAppConfirmUI.getError().getErrContent();
      System.out.println(tPGrpEdorAppConfirmUI.getError().getErrContent());
    }
    else
    {
		flag = "Succ";
		content = "保全理算成功。";
    }
    content = PubFun.changForHTML(content);
%>                      
<html>
	<script language="javascript">
	if (mLoadFlag == "TASKFRAME")
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
	}
	else
	{
		parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
	}
</script>
</html>

