<%
//程序名称：GEdorTypeLPSubmit.jsp
//程序功能：
//创建日期：2005-12-26
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String flag;
	String content;

	GlobalInput gi = (GlobalInput)session.getValue("GI");
	String operator = request.getParameter("Operator");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String grpContNo = request.getParameter("GrpContNo");
	String[] addChkNo = request.getParameterValues("InpLCInsuredGridChk");
	String[] contNo = request.getParameterValues("LCInsuredGrid1");
	String[] insuredNo = request.getParameterValues("LCInsuredGrid2");
	String[] delChkNo = request.getParameterValues("InpInsuredListGridChk");
	String[] serialNo = request.getParameterValues("InsuredListGrid1");
	
	LCInsuredSet tLCInsuredSet = new LCInsuredSet();
	LPDiskImportSet tLPDiskImportSet = new LPDiskImportSet();
	
	System.out.println("operator" + operator);
	if (operator.equals("ADD"))
	{
	  if (addChkNo != null)
		{
	    for (int i = 0; i < addChkNo.length; i++)
	    {
	    	if (addChkNo[i].equals("1"))
	    	{
		      LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
		      tLCInsuredSchema.setGrpContNo(grpContNo);
		      tLCInsuredSchema.setContNo(contNo[i]);
		      tLCInsuredSchema.setInsuredNo(insuredNo[i]);
		      tLCInsuredSet.add(tLCInsuredSchema);
		    }
	    }
	  }
	}
	else if(operator.equals("DEL"))
	{
		if (delChkNo != null)
		{
		  for (int i = 0; i < delChkNo.length; i++)
		  {
		  	if (delChkNo[i].equals("1"))
		  	{
		      LPDiskImportSchema tLPDiskImportSchema = new LPDiskImportSchema();
		      tLPDiskImportSchema.setGrpContNo(grpContNo);
		      tLPDiskImportSchema.setSerialNo(serialNo[i]); 
		      tLPDiskImportSet.add(tLPDiskImportSchema);
		    }
		  }
		}
	}
		System.out.println("vvv");
	LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	tLPGrpEdorItemSchema.setEdorNo(edorNo);
	tLPGrpEdorItemSchema.setEdorType(edorType);
	tLPGrpEdorItemSchema.setGrpContNo(grpContNo);
  
	VData data = new VData();
	TransferData mTransferData = new TransferData();
	mTransferData.setNameAndValue("operator",operator);
	data.add(gi);
	data.add(tLCInsuredSet);
	data.add(tLPDiskImportSet);
	data.add(tLPGrpEdorItemSchema);
	data.add(mTransferData);
	
	System.out.println("fff");
  GrpEdorWDDetailUI tGrpEdorWDDetailUI = new GrpEdorWDDetailUI();
	if (!tGrpEdorWDDetailUI.submitData(data, operator))
	{
		flag = "Fail";
		content = "数据保存失败！原因是:" + tGrpEdorWDDetailUI.getError();
	}
	else 
	{
		flag = "Succ";
		content = "数据保存成功。";
		String message = tGrpEdorWDDetailUI.getMessage();
		if (message != null)
		{
		  flag = "Fail";
		  content = message;
		}
	}
	content = PubFun.changForHTML(content);
%>   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>", "<%=operator%>");
</script>
</html>