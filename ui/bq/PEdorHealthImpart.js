
//查询被保人信息
function queryInsured()
{
	var sql = "select Name, Sex, Birthday, IDType, IDNo " + 
	          "from LDPerson " +
				    "where CustomerNo='" + fm.all("InsuredNo").value + "'";
	var arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		return false;
	}
	try
	{
		fm.all("InsuredName").value = arrResult[0][0];
		fm.all("Sex").value = arrResult[0][1];
		fm.all("Birthday").value = arrResult[0][2];
		fm.all("IDType").value = arrResult[0][3];
		fm.all("IDNo").value = arrResult[0][4];
	}
	catch (ex)
	{
		alert(ex);
	}
}

//查询录入告知
function queryImapart()
{
	var sql = "select ImpartContent from LPCustomerImpart " +
	          "where EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "' " +
	         	"and ContNo = '" + fm.ContNo.value + "' " +
	         	"and CustomerNo = '" + fm.InsuredNo.value + "' " +
	          "and ImpartVer = '101' ";
	var arrResult = easyExecSql(sql);
	if (arrResult)
	{
		for (i = 0; i < arrResult.length; i++)
		{
			fm.ImpartContent[i].value = arrResult[i][0];
		}
	}
	
	var sql = "select ImpartParam from LPCustomerImpartParams " +
          "where EdorNo = '" + fm.EdorNo.value + "' " +
          "and EdorType = '" + fm.EdorType.value + "' " +
         	"and ContNo = '" + fm.ContNo.value + "' " +
          "and ImpartVer = '101' " +
          "and CustomerNo = '" + fm.InsuredNo.value + "' " +
          "order by ImpartCode, ImpartParamNo";
  var arrResult = easyExecSql(sql);
  if (arrResult)
  {
		fm.Detail1[arrResult[0][0]].checked = true;
		fm.Detail2[0].value = arrResult[1][0];
		fm.Detail2[parseInt(arrResult[2][0]) + 1].checked = true;
		fm.Detail3[arrResult[3][0]].checked = true;
		fm.Detail4[arrResult[4][0]].checked = true;
		fm.Detail5[arrResult[5][0]].checked = true;
		fm.Detail6[arrResult[6][0]].checked = true;
  }
}

function beforeSubmit()
{
	var isChecked = false;
	for (i = 0; i < fm.Detail1.length; i++)
	{
		if (fm.Detail1[i].checked == true)
		{
			isChecked = true;
		}
	}
	if (isChecked == false)
	{
		alert("请录入告知1！");
		return false;
	}
	if ((fm.Detail1[3].checked == true) && (fm.ImpartContent[0].value == ""))
	{
		alert("请录入告知1的详细说明！");
		return false
	}
	
	if (fm.Detail2[0].value == "")
	{
		alert("请录入告知2！");
		return false;
	}
	if (!isNumeric(fm.Detail2[0].value))
	{
		alert("告知2的收入必须为数字！");
		return false;
	}
	
	isChecked = false;
	for (i = 1; i < fm.Detail2.length; i++)
	{
		if (fm.Detail2[i].checked == true)
		{
			isChecked = true;
		}
	}
	if (isChecked == false)
	{
		alert("请录入告知2！");
		return false;
	}
	if ((fm.Detail2[6].checked == true) && (fm.ImpartContent[1].value == ""))
	{
		alert("请录入告知2的详细说明！");
		return false
	}
	
	isChecked = false;
	for (i = 0; i < fm.Detail3.length; i++)
	{
		if (fm.Detail3[i].checked == true)
		{
			isChecked = true;
		}
	}
	if (isChecked == false)
	{
		alert("请录入告知3！");
		return false;
	}
	if ((fm.Detail3[0].checked == true) && (fm.ImpartContent[2].value == ""))
	{
		alert("请录入告知3的详细说明！");
		return false
	}
	
	isChecked = false;
	for (i = 0; i < fm.Detail4.length; i++)
	{
		if (fm.Detail4[i].checked == true)
		{
			isChecked = true;
		}
	}
	if (isChecked == false)
	{
		alert("请录入告知4！");
		return false;
	}
	if ((fm.Detail4[0].checked == true) && (fm.ImpartContent[3].value == ""))
	{
		alert("请录入告知4的详细说明！");
		return false
	}
	
	isChecked = false;
	for (i = 0; i < fm.Detail5.length; i++)
	{
		if (fm.Detail5[i].checked == true)
		{
			isChecked = true;
		}
	}
	if (isChecked == false)
	{
		alert("请录入告知5！");
		return false;
	}
	if ((fm.Detail5[0].checked == true) && (fm.ImpartContent[4].value == ""))
	{
		alert("请录入告知5的详细说明！");
		return false
	}
	
	isChecked = false;
	for (i = 0; i < fm.Detail6.length; i++)
	{
		if (fm.Detail6[i].checked == true)
		{
			isChecked = true;
		}
	}
	if (isChecked == false)
	{
		alert("请录入告知6！");
		return false;
	}
	if ((fm.Detail6[0].checked == true) && (fm.ImpartContent[5].value == ""))
	{
		alert("请录入告知6的详细说明！");
		return false
	}
	return true;
}

//保存
function saveImpart()
{
	if (!beforeSubmit())
	{
		return false;
	}
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorHealthImpartSave.jsp";
	fm.submit();
}

function afterSubmit(flag, content, operator)
{
  try
  {
    showInfo.close();
    window.focus();
  }
  catch (ex) {}

  if (flag == "Fail")
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    returnParent();
  }
}

function returnParent()
{	
  try
	{
    top.opener.focus();
    top.close();
	}
	catch (ex) {}
}