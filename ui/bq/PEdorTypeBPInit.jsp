<script language="JavaScript">  
function initInpBox()
{ 
	//判断是否是在工单查看中查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
 
  	if (flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  	}
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;  
    fm.all('EdorValiDate').value = top.opener.fm.all('EdorValiDate').value;      
    fm.all('EdorAppDate').value = top.opener.fm.all('EdorAppDate').value; 
    //showOneCodeName("EdorCode", "EdorTypeName"); 
}

function initForm()
{
  try
  {
    initInpBox();
    initPolInfo();
    initAccInfo();
    initPayState();
    initNewPrem();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
</script>