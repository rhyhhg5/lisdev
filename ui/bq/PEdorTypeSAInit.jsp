<%
//PEdorTypeSAInit.jsp
//程序功能：附加险增额
//创建日期：2002-06-19 11:10:36
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>                        

<script language="JavaScript">  
function initInpBox()
{ 
  try
  {      
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    
    fm.all('PolNo').value = top.opener.fm.all('PolNo').value;
    
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;  
  }
  catch(ex)
  {
    alert("在PEdorTypeSAInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在PEdorTypeSAInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    
    initSelBox(); 
    
    initLCDutyGrid();
    
    initQuery();
    
    ctrlGetEndorse();
  }
  catch(re)
  {
    alert("PEdorTypeSAInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initQuery()
{	
	var i = 0;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
	fm.all('fmtransact').value = "QUERY||MAIN";
	//showSubmitFrame("1");
	fm.submit();	  	 	 
}

// 信息列表的初始化
function initLCDutyGrid()
{
    var iArray = new Array();
      
      try
      {
	      var iArray = new Array();
		    iArray[0]=new Array();
		    iArray[0][0]="序号"; 		//列名（此列为顺序号，列名无意义，而且不显示）
		    iArray[0][1]="30px";		//列宽
		    iArray[0][2]=10;			//列最大值
		    iArray[0][3]=0;			//是否允许输入,1表示允许，0表示不允许
		  
		    iArray[1]=new Array();
		    iArray[1][0]="责任代码"; 	        //列名
		    iArray[1][1]="30px";		//列宽
		    iArray[1][2]=40;			//列最大值
		    iArray[1][3]=0;			//是否允许输入,1表示允许，0表示不允许
		    
		    iArray[2]=new Array();
		    iArray[2][0]="责任名称"; 	        //列名
		    iArray[2][1]="50px";		//列宽
		    iArray[2][2]=40;			//列最大值
		    iArray[2][3]=0;			//是否允许输入,1表示允许，0表示不允许		    
		    
		    iArray[3]=new Array();
		    iArray[3][0]="份数"; 	//列名
		    iArray[3][1]="30px";		//列宽
		    iArray[3][2]=40;			//列最大值
		    iArray[3][3]=0;			//是否允许输入,1表示允许，0表示不允许

		    iArray[4]=new Array();
		    iArray[4][0]="总基本保额"; 	//列名
		    iArray[4][1]="50px";		//列宽
		    iArray[4][2]=40;			//列最大值
		    iArray[4][3]=0;			//是否允许输入,1表示允许，0表示不允许

		    iArray[5]=new Array();
		    iArray[5][0]="总保费"; 	//列名（此列为顺序号，列名无意义，而且不显示）
		    iArray[5][1]="30px";		//列宽
		    iArray[5][2]=10;			//列最大值
		    iArray[5][3]=0;			//是否允许输入,1表示允许，0表示不允许		   

		    iArray[6]=new Array();
		    iArray[6][0]="保额类型"; 		//列名
		    iArray[6][1]="0px";		//列宽
		    iArray[6][2]=0;			//列最大值
		    iArray[6][3]=0;			//是否允许输入,1表示允许，0表示不允许

		    iArray[7]=new Array();
		    iArray[7][0]="新保额/份数"; 	//列名
		    iArray[7][1]="30px";		//列宽
		    iArray[7][2]=40;			//列最大值
		    iArray[7][3]=0;			//是否允许输入,1表示允许，0表示不允许
      
        LCDutyGrid = new MulLineEnter( "fm" , "LCDutyGrid" ); 
        //这些属性必须在loadMulLine前
        LCDutyGrid.mulLineCount = 10;   
        LCDutyGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
        LCDutyGrid.hiddenSubtraction=1; //是否隐藏"-"号标志：1为隐藏；0为不隐藏(缺省值)
        LCDutyGrid.displayTitle = 1;
        LCDutyGrid.canSel=1;
        LCDutyGrid.selBoxEventFuncName ="reportDetailClick";
        LCDutyGrid.loadMulLine(iArray);  
        LCDutyGrid.detailInfo="单击显示详细信息";
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>