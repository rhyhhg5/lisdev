<%
//程序名称：GEdorTypeACSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----ACsubmit---");
  LPGrpEdorItemSchema tLPGrpEdorItemSchema=new LPGrpEdorItemSchema();
  LPGrpAppntSchema tLPGrpAppntSchema=new LPGrpAppntSchema();
  LPGrpSchema tLPGrpSchema=new LPGrpSchema();
  LPGrpAddressSchema tLPGrpAddressSchema = new LPGrpAddressSchema();

  
  LPGrpPolSchema tLPGrpPolSchema   = new LPGrpPolSchema();
  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
  PGrpEdorACDetailUI tPGrpEdorACDetailUI   = new PGrpEdorACDetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String fmAction = "";
  String Result="";
  String addrFlag="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  fmAction = request.getParameter("fmAction");
  addrFlag=request.getParameter("addrFlag");
  System.out.println("------fmAction:"+addrFlag);
	GlobalInput tG = new GlobalInput();
	    System.out.println("------------------begin ui");
	tG = (GlobalInput)session.getValue("GI");
	
	tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
	tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpEdorItemSchema.setEdorAppNo(request.getParameter("EdorAppNo"));
	tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
  
	//团单投保人信息  LCGrpAppnt
	tLPGrpAppntSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpAppntSchema.setEdorType(request.getParameter("EdorType"));
	tLPGrpAppntSchema.setGrpContNo(request.getParameter("ProposalGrpContNo"));     //集体投保单号码
	tLPGrpAppntSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	tLPGrpAppntSchema.setPrtNo(request.getParameter("PrtNo"));                 //印刷号码
	tLPGrpAppntSchema.setName(request.getParameter("GrpName"));
	tLPGrpAppntSchema.setPostalAddress(request.getParameter("GrpAddress"));
	tLPGrpAppntSchema.setZipCode(request.getParameter("GrpZipCode"));
	tLPGrpAppntSchema.setAddressNo(request.getParameter("GrpAddressNo"));
	tLPGrpAppntSchema.setPhone(request.getParameter("Phone"));
	tLPGrpAppntSchema.setTaxNo(request.getParameter("TaxNo"));
	tLPGrpAppntSchema.setLegalPersonName(request.getParameter("LegalPersonName"));
	tLPGrpAppntSchema.setLegalPersonIDNo(request.getParameter("LegalPersonIDNo"));
	//添加联系人相关信息
	tLPGrpAppntSchema.setIDType(request.getParameter("IDType"));
	tLPGrpAppntSchema.setIDNo(request.getParameter("IDNo"));
	tLPGrpAppntSchema.setIDStartDate(request.getParameter("IDStartDate"));
	tLPGrpAppntSchema.setIDEndDate(request.getParameter("IDEndDate"));
	tLPGrpAppntSchema.setIDLongEffFlag(request.getParameter("IDLongEffFlag"));

	System.out.println("IDType" + request.getParameter("IDType"));
	System.out.println("IDNo" + request.getParameter("IDNo"));
	System.out.println("IDStartDate" + request.getParameter("IDStartDate"));
	System.out.println("IDEndDate" + request.getParameter("IDEndDate"));
	//System.out.println("IDLongEffFlag" + request.getParameter("IDLongEffFlag"));
	
	//团体客户信息  LPGrp
	tLPGrpSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpSchema.setEdorType(request.getParameter("EdorType"));
	tLPGrpSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	tLPGrpSchema.setGrpName(request.getParameter("GrpName"));             //单位名称
	tLPGrpSchema.setGrpNature(request.getParameter("GrpNature"));         //单位性质
	tLPGrpSchema.setBusinessType(request.getParameter("BusinessType"));   //行业类别
	tLPGrpSchema.setPeoples(request.getParameter("Peoples"));             //总人数
	tLPGrpSchema.setOnWorkPeoples(request.getParameter("OnWorkPeoples"));             //在职人数
	tLPGrpSchema.setOffWorkPeoples(request.getParameter("OffWorkPeoples"));             //退休人数
	tLPGrpSchema.setOtherPeoples(request.getParameter("OtherPeoples"));             //其它人员数
	tLPGrpSchema.setPhone(request.getParameter("Phone"));             //总机
	System.out.println("OtherPeoples" + request.getParameter("OtherPeoples"));
//	System.out.println("request.getParameter:" + request.getParameter("OrgancomCode"));
//	System.out.println("LPGrpSchema.getOrganComCode:" + tLPGrpSchema.getOrganComCode());
	
	//团体客户地址  LCGrpAddress
	tLPGrpAddressSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPGrpAddressSchema.setEdorType(request.getParameter("EdorType"));	    
	tLPGrpAddressSchema.setCustomerNo(request.getParameter("GrpNo"));            //客户号码
	tLPGrpAddressSchema.setAddressNo(request.getParameter("GrpAddressNo"));      //地址号码
	tLPGrpAddressSchema.setGrpAddress(request.getParameter("GrpAddress"));       //单位地址
	tLPGrpAddressSchema.setGrpZipCode(request.getParameter("GrpZipCode"));       //单位邮编
	tLPGrpAddressSchema.setPostalProvince(request.getParameter("Province"));
	tLPGrpAddressSchema.setPostalCity(request.getParameter("City"));
	tLPGrpAddressSchema.setPostalCounty(request.getParameter("County"));
	tLPGrpAddressSchema.setDetailAddress(request.getParameter("appnt_DetailAdress"));
	
	//保险联系人一
	tLPGrpAddressSchema.setLinkMan1(request.getParameter("LinkMan1"));
	tLPGrpAddressSchema.setPhone1(request.getParameter("Phone1"));
	tLPGrpAddressSchema.setFax1(request.getParameter("Fax1"));
	tLPGrpAddressSchema.setE_Mail1(request.getParameter("E_Mail1"));
	tLPGrpAddressSchema.setMobile1(request.getParameter("Mobile1"));


	try
  {
  	//准备传输数据 VData
  	VData tVData = new VData();  
  	
		//保存个人保单信息(保全)
		tVData.addElement(addrFlag);	
		tVData.addElement(tG);
		tVData.addElement(tLPGrpEdorItemSchema);
		tVData.addElement(tLPGrpAppntSchema);
		tVData.addElement(tLPGrpSchema);
		tVData.addElement(tLPGrpAddressSchema);
		tPGrpEdorACDetailUI.submitData(tVData,fmAction);
	}
	catch(Exception ex)
	{
		Content = fmAction+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPGrpEdorACDetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (fmAction.equals("QUERY||MAIN")||fmAction.equals("QUERY||DETAIL"))
    	{
    	if (tPGrpEdorACDetailUI.getResult()!=null&&tPGrpEdorACDetailUI.getResult().size()>0)
    	{
    		Result = (String)tPGrpEdorACDetailUI.getResult().get(0);
    		if (Result==null||Result.trim().equals(""))
    		{
    			FlagStr = "Fail";
    			Content = "提交失败!!";
    		}
    	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

