<%
//PEdorTypeAAInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>                       

<script language="JavaScript">  

function initInpBox() { 
  try { 
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;       
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value; 
    fm.all('PolNo').value = top.opener.fm.all('PolNo').value; 
    fm.all('InsuredNo').value = top.opener.fm.all('CustomerNo').value;   
  }
  catch(ex) {
    alert("在PEdorTypeAAInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                     


function initForm() {
  try {
    initInpBox();
    initLCDutyGrid();
    initQuery1();    
    initLPDutyGrid();
    initQuery2();
  }
  catch(re) {
    alert("PEdorTypeAAInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 信息列表的初始化
function initLCDutyGrid() {
  var iArray = new Array();
      
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号码";    	//列名1
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="保单险种号码";         			//列名2
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="责任编码";         			//列名8
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="保险年期";         		//列名5
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[5]=new Array();
    iArray[5][0]="责任终止日";         		//列名6
    iArray[5][1]="110px";            		//列宽
    iArray[5][2]=110;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="保额";         		//列名6
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="保费";         		//列名6
    iArray[7][1]="50px";            		//列宽
    iArray[7][2]=50;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    

    LCDutyGrid = new MulLineEnter( "fm" , "LCDutyGrid" ); 
    //这些属性必须在loadMulLine前
    LCDutyGrid.mulLineCount = 0;    
    LCDutyGrid.displayTitle = 1;
    LCDutyGrid.canSel = 1;
    LCDutyGrid.hiddenPlus = 1; 
    LCDutyGrid.hiddenSubtraction = 1;
    LCDutyGrid.selBoxEventFuncName = "reportDetailClick";
    LCDutyGrid.detailInfo = "单击显示详细信息";
    LCDutyGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}

// 信息列表的初始化
function initLPDutyGrid() {
  var iArray = new Array();
      
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保单号码";    	//列名1
    iArray[1][1]="120px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="保单险种号码";         			//列名2
    iArray[2][1]="100px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="责任编码流水号";         			//列名8
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[4]=new Array();
    iArray[4][0]="保险年期";         		//列名5
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=60;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

    iArray[5]=new Array();
    iArray[5][0]="责任终止日";         		//列名6
    iArray[5][1]="110px";            		//列宽
    iArray[5][2]=110;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="保额";         		//列名6
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="保费";         		//列名6
    iArray[7][1]="50px";            		//列宽
    iArray[7][2]=50;            			//列最大值
    iArray[7][3]=0;             			//是否允许输入,1表示允许，0表示不允许
    
    
  

    LPDutyGrid = new MulLineEnter( "fm" , "LPDutyGrid" ); 
    //这些属性必须在loadMulLine前
    LPDutyGrid.mulLineCount = 0;   
    LPDutyGrid.displayTitle = 1;
    LPDutyGrid.canSel = 1; 
    LPDutyGrid.hiddenPlus = 1; 
    LPDutyGrid.hiddenSubtraction = 1;
    LPDutyGrid.selBoxEventFuncName = "reportDetail2Click"; 
    LPDutyGrid.detailInfo = "单击显示详细信息";
    LPDutyGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert(ex);
  }
}



</script>