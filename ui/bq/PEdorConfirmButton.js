//
////提交后操作,服务器数据返回后执行的操作
function afterSubmit(flag, content, acceptNo)
{
	try {showInfo.close(); } catch(e) {}
	window.focus();
	if (flag == "Fail")
	{
		if (content.indexOf("需要财务交费") == -1)
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
			showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		}
		else
		{
			//window.open("./PEdorConfirmOptionMain.jsp?EdorAcceptNo="+edorAcceptNo+"&FeeFlag=1", "", "scrollbars=no,status=no");
		  afterSubmit2(flag,content); 
		  document.all.redo.disabled=true;
		}       
		        
		if(fm.failType.value == "1" || fm.autoUWFlagFail.value == "1")
		{
			document.all.redo.disabled=true;
		  var newWindow = window.open("../task/TaskGetPrintMain.jsp?workNo=" + medorAcceptNo);
  	  newWindow.focus();
  	  top.close();
		}       
		        
		document.all.confirmButton.disabled=false;             
		//由于自动审批未通过而产生的操作失败
		if(fm.failType.value == 1)
		{       
			      
			if(fm.loadFlag.value == "EDORAPPCONFIRM")
			{     
				//保全确认界面从PEdorAppInput.jsp调出
				top.opener.focus();
				top.opener.location.reload();
				top.close();
			}     
			else  
			{     
				top.close();
				top.opener.focus();
				top.opener.location.reload();
			}     
		}       
		}         
	else      
	{         
/*	try     
		{       
			//刷新工单页面的保单信息
			top.opener.parent.queryContInfo();
			//更新保单信息，费用
			top.opener.queryLCCont();			
			//刷新客户信息明细
			top.opener.getLCContDetailInfo(top.opener.document.fm.all('ContNo').value);
			//更新客户报单信息列表
			top.opener.getInsuredPolInfo(top.opener.document.fm.all('ContNo').value);
			//更新保全项目状态
			top.opener.getEdorItem(); 
			
		} 
		catch(e) {}
*/

    
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//window.open("./PEdorConfirmOptionMain.jsp?EdorAcceptNo="+edorAcceptNo+"&FeeFlag=0", "", "scrollbars=no,status=no");
		afterSubmit2(flag , content); 
		 
		//产生新受理提示
		document.all.redo.disabled=true;
		var sql = "select workno from LGWork where InnerSource = '" + acceptNo+ "' and TypeNo ='0300003' and AcceptWayNo ='8' and StatusNo='2'";
		var arrResult  = easyExecSql(sql, 1, 0);
		var strWork = "";
		if (arrResult)
	  {
	  	for(var k=0; k < arrResult.length; k++)
	  	{
	  			var result = arrResult[k][0]+"  ";
	  			strWork = strWork +result;
	  	}
			var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + "生成新的工单，工单号为："+strWork ;  
			showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogTop=0px;dialogLeft=0px;dialogWidth:550px;dialogHeight:350px");
	  }
	}
}

//保全确认提交
function edorConfirm()
{
       
	  //黑名单校验 20120606  【OoO?】ytz
        if(!checkBlackName()){
        	return false;
        }
//        //校验康乐人生转保
        if(!checkEdorDate()){
        	return false;
        }
	
        //校验医保卡帐号以及银行帐号 --wujun
        if(fm.PayMode.value=="8"){
        	var bankcode = fm.Bank.value;
        	var StringSql = " select MedicalComCode,MedicalComName from LDMedicalCom where  MedicalComCode='"+bankcode+"' with ur";
        	if(easyExecSql(StringSql)==null){
        		alert("当前缴费方式为“医保个人账户”，请先变更缴费资料");
        		return false;
        	}
        }
        
        if(fm.PayMode.value=="4"){
        	var bankcode = fm.Bank.value;
        	var StringSql1 = " select BankCode, BankName from LDBank where BankCode='"+bankcode+"'  with ur";
        	if(easyExecSql(StringSql1)==null){
        		alert("当前缴费方式为“银行转帐”，请先变更缴费资料");
        		return false;
        	}
        }
        
  //20100126 zhanggm 审批意见为不同意，不允许保全确认
  var edorAcceptNo = fm.EdorAcceptNo.value;
  var strSql = "select statusno from lgwork where workno = '" + edorAcceptNo + "' ";
  var arrResult = easyExecSql(strSql);
  if( arrResult[0][0] == "88")
  {
    alert("保全审批意见为[不同意]，请重新录入保全明细或撤销保全工单！");
    return false;
  }
  if( arrResult[0][0] == "5")
  {
    alert("保全已经确认完毕，请直接查看该工单即可！");
    return false;
  }
    
     //20120312 杨天政，保全锁
  var findSql="select serialno from LCUrgeVerifyLog where serialno='"+edorAcceptNo+"' ";
  var arrResult_1 = easyExecSql(findSql);
  if( arrResult_1)
  {
   alert("正在保全确认或者重复理算操作，请不要再次点击保全确认！");
   return false;
   }  
    
	// modify by fuxin 2008-6-24 15:11:28 使用余额抵扣时，不要录入收付费方式
	var selectValue='';
	for(i = 0; i <fm.ChkYesNo.length; i++)
	{
		if(fm.ChkYesNo[i].checked)
		{
			selectValue=fm.ChkYesNo[i].value;
			break;
		}
	}
  if (mGetMoney != 0 && fm.PayMode.value == "0" && selectValue =="no")
  {
    alert("请选择缴费方式！");
    return false;
  }
  if (!confirm("保全确认后将不可恢复，继续？"))
  {
    return false;
  }
  
	if (!clickOK())
  {
	  if (mGetMoney != 0 && !boforeSubmit())
	  {
	    return false;
	  }
  }
    if (!checkEdorMoney())
  {
  	alert("本次理算后批单生成异常，请重复理算生成本次批单！");
    return false;
  }

	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
	"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.submit();
	document.all.confirmButton.disabled=true;
	//fm.confirmButton.disabled=false;
}

//校验黑名单
function checkBlackName(){
	
	var strSQL = "select contno from lpedoritem where edorno = '" + fm.EdorAcceptNo.value + "'";
	var mContno;
	var arrResult = easyExecSql(strSQL);
	
	if(arrResult != null)
	{
		mContno = arrResult[0][0];
	}
	else 
	{
		alert("没有查到保全项目表");
		return false;
	}
	
	var strSQL2 = "select appntname,appntidtype,appntidno from lpcont where edorno = '" + fm.EdorAcceptNo.value + "'";
	var arrResult2 = easyExecSql(strSQL2);
	if(arrResult2 != null)
	{
		for(var i = 0; i < arrResult2.length; i++)
		{
			var sqlblack = "select 1 from lcblacklist where idnotype is not null and idno is not null  "+
						   "  and name='"+arrResult2[i][0]+"' and idnotype='"+arrResult2[i][1]+"' and idno='"+arrResult2[i][2]+"' and type='0' "+
						   "  union  "+
						   "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
						   "  and name='"+arrResult2[i][0]+"' and idno='"+arrResult2[i][2]+"' and type='0' "+
						   "  union   "+
						   "  select 1 from lcblacklist where (idno is null or  idno='' or  idno='无')  and name='"+arrResult2[i][0]+"' and type='0' ";
			var arrResult21 = easyExecSql(sqlblack);
			if(arrResult21){
				if(!confirm("该保单投保人："+arrResult2[0][0]+"，存在于黑名单中，确认要保全确认吗？")){
					return false;
				}
			}
		}
	} 

	var strSQL3 = "select appntname,appntidtype,appntidno from lccont where contno = '" + mContno + "'";
	var arrResult3 = easyExecSql(strSQL3);
	if(arrResult3 != null)
    {
		var sqlblack2 = "select 1 from lcblacklist where idnotype is not null and idno is not null  "+
					    "  and name='"+arrResult3[0][0]+"' and idnotype='"+arrResult3[0][1]+"' and idno='"+arrResult3[0][2]+"' and type='0' "+
					    "  union  "+
					    "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
					    "  and name='"+arrResult3[0][0]+"' and idno='"+arrResult3[0][2]+"' and type='0' "+
					    "  union   "+
					    "  select 1 from lcblacklist where (idno is null or  idno='' or  idno='无')  and name='"+arrResult3[0][0]+"' and type='0' ";
  		var arrResult31 = easyExecSql(sqlblack2);
  		if(arrResult31){
  			if(!confirm("该保单投保人："+arrResult3[0][0]+"，存在于黑名单中，确认要保全确认吗？")){
  				return false;
  			}
  		}	              		
    }
	
	
	var strSQL4 = "select name,idtype,idno from lcinsured where contno = '" + mContno + "'";
	var arrResult4 = easyExecSql(strSQL4);
	var tInsuNames = "";
	if(arrResult4 != null){
		for(var i = 0; i < arrResult4.length; i++)
		{
			var sqlblack3  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
						 	 "  and name='"+arrResult4[i][0]+"' and idnotype='"+arrResult4[i][1]+"' and idno='"+arrResult4[i][2]+"' and type='0' "+
						 	 "  union  "+
						 	 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
						 	 "  and name='"+arrResult4[i][0]+"' and idno='"+arrResult4[i][2]+"' and type='0' "+
						 	 "  union   "+
						 	 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrResult4[i][0]+"' and type='0' ";
			var arrResult41 = easyExecSql(sqlblack3);
			if(arrResult41){
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ " , "+arrResult4[i][0];
				}else{
					tInsuNames = tInsuNames + arrResult4[i][0];
				}
			}
		}	
	} 

	
	var strSQL5 = "select name,idtype,idno from lpinsured where edorno = '" + fm.EdorAcceptNo.value + "'";
	var arrResult5 = easyExecSql(strSQL5);
	if(arrResult5 != null){
		for(var i = 0; i < arrResult5.length; i++)
		{
			var sqlblack4  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
						 	 "  and name='"+arrResult5[i][0]+"' and idnotype='"+arrResult5[i][1]+"' and idno='"+arrResult5[i][2]+"' and type='0' "+
						 	 "  union  "+
						 	 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
						 	 "  and name='"+arrResult5[i][0]+"' and idno='"+arrResult5[i][2]+"' and type='0' "+
						 	 "  union   "+
						 	 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrResult5[i][0]+"' and type='0' ";
			var arrResult51 = easyExecSql(sqlblack4);
			if(arrResult51){
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ " , "+arrResult5[i][0];
				}else{
					tInsuNames = tInsuNames + arrResult5[i][0];
				}
			}
		}	
	}
	
		
	
	if(tInsuNames != ""){
		if (!confirm("该保单被保人姓名："+tInsuNames+",存在于黑名单中，确认要保全确认吗？"))
		{
			return false;
		}
	}
	
	
	
	return true;
}

//重复理算
function reCal()
{
  //20100126 zhanggm 审批意见为同意，不允许重复理算
  var edorAcceptNo = fm.EdorAcceptNo.value;
  var strSql = "select statusno from lgwork where workno = '" + edorAcceptNo + "' ";
  var arrResult = easyExecSql(strSql);
  if( arrResult[0][0] == "99")
  {
    alert("保全审批意见为[同意]，不能进行重复理算操作！");
    return false;
  }
  if( arrResult[0][0] == "5")
  {
    alert("保全已经确认完毕，请直接查看该工单即可！");
    return false;
  }
  
   //20120312 杨天政，保全锁
  var findSql="select serialno from LCUrgeVerifyLog where serialno='"+edorAcceptNo+"' ";
  var arrResult_1 = easyExecSql(findSql);
  if( arrResult_1)
  {
   alert("正在保全确认或者重复理算操作，请不要再次点击保全确认！");
   return false;
   }  
    
    
	fm.action ="PEdorReCalSubmit.jsp";
	fm.submit();
}

function returnParent()
{
	var edorAcceptNo = fm.EdorAcceptNo.value;
	//top.location.href = "./PEdorAppInputMain.jsp?LoadFlag=TASK&EdorAcceptNo=" + edorAcceptNo;
	
	var customerNo = "";
	var result = easyExecSql("select customerNo from LGWork where workNo = '" + edorAcceptNo + "' ");
	if(result)
	{
		customerNo = result[0][0];
	}
 // var url = "../task/TaskViewMain.jsp?loadFlag=PERSONALBOX&initPage=TaskPersonalBoxSave.jsp&WorkNo=" + edorAcceptNo 
 //						+"&CustomerNo=" + customerNo + "&DetailWorkNo=" + edorAcceptNo;
  var url = "./PEdorAppInput.jsp?LoadFlag=TASK&DetailWorkNo=" + edorAcceptNo + "&CustomerNo=" + customerNo;
	parent.location.href = url;
	//top.fraInterface.location.href = url;
	//top.document.all("fraInterface").src = url;
	//var width = screen.availWidth - 10;
	//var height = screen.availHeight - 28;
	//win = window.open(url, 
	//				  "ViewWin",
	//				  "toolbar=no,menubar=no,status=yes,resizable=yes,top=0,left=0,width="+width+",height="+height);  
	//win.focus();      
}

////业务类型改变时显示相应的信息录入页面
function afterPayModeSelect()
{
	var payMode = fm.all("PayMode").value
	if ((payMode == "1") || (payMode == "2") || (payMode == "3")|| (payMode == "9")) //自行缴费
	{			
	  if (fm.fmtransact.value == "1") //交费
	  {
		 document.all("trEndDate").style.display = "";
		 fm.EndDate.verify = "截止日期|notnull&date";
	  }
	  else //退费不设日期
	  {
	  	document.all("trEndDate").style.display = "none";
	  	fm.EndDate.verify = "";
	  }
	hideAcc();
	}
	else if ((payMode == "0") || (payMode == "9")) //隐藏银行转帐信息
	{
      hideAcc();
	}
	else if (payMode == "4") //银行转帐
	{
      var sql = "select PayMode,a.managecom  " 
              + "from LCCont a, LPEdorMain b " 
              + "where b.ContNo = a.ContNo " 
              + "and   b.EdorAcceptNo = '" + medorAcceptNo + "' " 
              + "union all " 
              + "select PayMode,a.managecom  " 
              + "from LBCont a, LPEdorMain b " 
              + "where b.ContNo = a.ContNo " 
              + "and   b.EdorAcceptNo = '" + medorAcceptNo + "' ";
      var result = easyExecSql(sql);
   	 //在此处对分红险满期领取特殊处理，若领取对象为被保险人且付费方式选"银行转账"，必须录入银行信息
   	 var sql2=" select distinct a.appntno,a.insuredno,c.edorvalue,a.insuredname "
   	 		+ " from LCPol a, LPEdorItem b,LPEdorEspecialData c " 
   	 		+ " where b.ContNo = a.ContNo "  
   	 		+ " and b.edorno=c.edorno "
   	 		+ " and b.EdorNo = '" + medorAcceptNo + "' " 
   	 		+ " and b.edortype='HA' and  c.detailtype='HLGET' with ur" ;
   	 var res = easyExecSql(sql2);
   	 if(res!=null &&　res[0][0]!=res[0][1]  && res[0][2]=="2" ){
   		 
   	 	var managecom = result[0][1];
   	 	fm.ManageCom.value=managecom;
   	 	showAcc(); 
   	 	fm.all("AccName1").value =res[0][3];
   	 	fm.all("AccName").value =fm.all("AccName1").value;
   	 	document.all("trAccName").style.display = "none";
   	 	document.all("trAccName1").style.display = "";
   	 }else if (result[0][0] == '4')
      {
     	 var managecom = result[0][1];
    	 fm.ManageCom.value=managecom;
	    showAcc1();
      } 
      else
      {
     	 var managecom = result[0][1];
    	 fm.ManageCom.value=managecom;
	    showAcc();
      }
	}else if (payMode == "8"){
	      var sql = "select a.PayMode,a.managecom " 
              + "from LCCont a, LPEdorMain b " 
              + "where b.ContNo = a.ContNo " 
              + "and   b.EdorAcceptNo = '" + medorAcceptNo + "' " 
              + "union all " 
              + "select PayMode,a.managecom " 
              + "from LBCont a, LPEdorMain b " 
              + "where b.ContNo = a.ContNo " 
              + "and   b.EdorAcceptNo = '" + medorAcceptNo + "' ";
      var result = easyExecSql(sql);
      if (result[0][0] == '8')
      {
    	 var managecom = result[0][1];
    	 fm.ManageCom.value=managecom;
	    showAcc2();
      } 
      else
      {
     	 var managecom = result[0][1];
    	 fm.ManageCom.value=managecom;
	    showAcc();
      }
	}
}
//
////提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr, content)
{
	try { showInfo.close(); } catch(re) { }

	//续保核保保全受理，且生成了应收数据
	  var sqlxx = "select getNoticeNo from LJSPay a, LPEdorItem b "
	              + "where a.otherNo = b.contNo "
	              + "and b.edorType = 'XB' and b.edorNo = '" + fm.EdorAcceptNo.value + "' ";
	  var rs = easyExecSql(sqlxx);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    for(var i = 0; i < rs.length; i++)
	    {
	      window.open("../f1print/IndiDueFeePrint.jsp?GetNoticeNo="+ rs[i][0]);
	      top.close();
	    }
	    
	    return true;
	  }
	  //续保核保保全受理，未生成了应收数据
	  sqlxx = "  select edorNo from LPEdorItem "
	          + "where edorType = 'XB' "
	          + "   and edorNo = '" + fm.EdorAcceptNo.value + "' ";
		rs = easyExecSql(sqlxx);
	  if(rs && rs[0][0] != "" && rs[0][0] != "null")
	  {
	    top.close();
	    
	    return true;
	  }	
	//保全受理
	if (fm.all("printMode")[0].checked)
	{
		var width = screen.availWidth - 10;
	  var height = screen.availHeight - 28;
	  var parma = "top=0,left=0,width="+width+",height="+height;
		window.open("ShowEdorPrint.jsp?EdorAcceptNo="+fm.EdorAcceptNo.value, "", parma);
	}   
	else if (fm.all("printMode")[1].checked) //批量打印 暂时没有实现
	{   
	}
	else if (fm.all("printMode")[2].checked)
	{   
	}
	
	try
	{
		top.close();
	}
	catch(re) { }
}

function boforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if (fm.PayMode.value == "4")
  {
    if (fm.Bank.value == "")
    {
      alert("转帐银行为空不能选择银行转帐！");
      return false;
    }
    if (fm.BankAccno.value == "")
    {
      alert("转帐帐号为空不能选择银行转帐！");
      return false;
    }
    if (compareDate(fm.PayDate.value, getCurrentDate()) == 2)
    {
      alert("转帐日期不能小于今天！");
      return false;
    }
    //检查两次输入帐户是否一致
    if(!checkAcc())
    {
      return false;
    }
  }
  if ((fm.PayMode.value == "1") && (fm.fmtransact.value == "1"))
  {
    if (compareDate(fm.EndDate.value, getCurrentDate()) == 2)
    {
      alert("截止日期不能小于今天！");
      return false;
    }
  }
  return true;
}

function queryMoney(edorAcceptNo)
{
	var	strSql = "select GetMoney from LPEdorApp where EdorAcceptNo = '" + edorAcceptNo + "'";
	var arrResult = easyExecSql(strSql);
	var getMoney = arrResult[0][0];
	if (getMoney == null)
	{
		return 0;
	}
	if (getMoney == 0)
	{
		document.all("trPayMode").style.display = "none";
		document.all("trEndDate").style.display = "none";
		document.all("trPayDate").style.display = "none";
		document.all("trBank").style.display = "none";
		document.all("trAccount").style.display = "none";
		fm.PayMode.verify = "";
		fm.EndDate.verify = "";
		fm.PayDate.verify = "";
	}
	else if (getMoney > 0) //交费
	{
		//document.all("FeeFlag").style.display = "";
		fm.fmtransact.value = "1";
	}
	else //退费
	{
		fm.EndDate.verify = "";
		fm.PayDate.verify = "";
		fm.fmtransact.value = "0";
	}
	return getMoney;
}

////费用为0则不设交费方式
function queryAccMenoy(edorAcceptNo)
{
	var	strSql = "select coalesce(sum(money),0) from lcappacctrace where otherno = '" + edorAcceptNo + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult==null)
	{
		return '';
	}
	var getMoney = arrResult[0][0];
	return getMoney;
}
//
////得到交费记录号
function getNoticeNo(medorAcceptNo)
{
	var strSql = "select getnoticeno from LJSGetEndorse a, LPEdorMain b " +
	             "where a.EndorSementNo = b.EdorNo and b.EdorAcceptNo = '" + medorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.GetNoticeNo.value = arrResult[0][0];
	}
}

////得到交费银行和帐号，当P表数据与C表不一致时，采用P表的数据显示
function queryAccount(medorAcceptNo){
	var strSql = "select a.BankCode,(select bankname from ldbank where bankcode = a.bankcode), a.BankAccno, a.AccName " +
				 "from LCCont a, LPEdorMain b " +
				 "where a.ContNo = b.ContNo " +
				 "and   b.EdorAcceptNo = '" + medorAcceptNo + "' " +
				 "union " +
				 "select a.BankCode, (select bankname from ldbank where bankcode = a.bankcode),a.BankAccno, a.AccName " +
				 "from LBCont a, LPEdorMain b " +
				 "where a.ContNo = b.ContNo " +
				 "and   b.EdorAcceptNo = '" + medorAcceptNo + "' ";

	var arrResult = easyExecSql(strSql);
	
	
	var strSql2 = "select a.BankCode,(select bankname from ldbank where bankcode = a.bankcode), a.BankAccno, a.AccName " +
                 "from LPCont a, LPEdorMain b " +
                 "where a.ContNo = b.ContNo " +
                 "and a.EdorNo=b.EdorAcceptNo " +
                 "and   b.EdorAcceptNo = '" + medorAcceptNo + "' " +
                 "union " +
                 "select a.BankCode, (select bankname from ldbank where bankcode = a.bankcode),a.BankAccno, a.AccName " +
                 "from LBCont a, LPEdorMain b " +
                 "where a.ContNo = b.ContNo " +
                 "and   b.EdorAcceptNo = '" + medorAcceptNo + "' ";

	var arrResult2 = easyExecSql(strSql2);
	if (arrResult2 != null)
	{
		fm.all("Bank").value = arrResult2[0][0];
		fm.all("BankName").value = arrResult2[0][1];
		fm.all("BankAccno").value = arrResult2[0][2];
		fm.all("AccName").value = arrResult2[0][3];
		showOneCodeName("Bank", "BankName");
	}	
	else if (arrResult != null)
	{
		fm.all("Bank").value = arrResult[0][0];
		fm.all("BankName").value = arrResult[0][1];
		fm.all("BankAccno").value = arrResult[0][2];
		fm.all("AccName").value = arrResult[0][3];
		showOneCodeName("Bank", "BankName");
	}
}

////得到医疗机构和帐号
function queryAccount2(medorAcceptNo){
	var strSql = "select a.BankCode,(select MedicalComName from LDMedicalCom where MedicalComCode = a.bankcode), a.BankAccno, a.AccName " +
	"from LCCont a, LPEdorMain b " +
	"where a.ContNo = b.ContNo " +
	"and   b.EdorAcceptNo = '" + medorAcceptNo + "' " +
	"union " +
	"select a.BankCode, (select MedicalComName from LDMedicalCom where MedicalComCode = a.bankcode),a.BankAccno, a.AccName " +
	"from LBCont a, LPEdorMain b " +
	"where a.ContNo = b.ContNo " +
	"and   b.EdorAcceptNo = '" + medorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.all("Bank").value = arrResult[0][0];
		fm.all("BankName").value = arrResult[0][1];
		fm.all("BankAccno").value = arrResult[0][2];
		fm.all("AccName").value = arrResult[0][3];
		showOneCodeName("Bank", "BankName");
	}
}

////function cancel()
////{
////    top.opener.top.opener.focus();
////    top.opener.top.opener.window.location.reload();
////    top.opener.top.close();
////    top.close();
////}
//
////提交
////function submitForm()
////{
////  if (!boforeSubmit())
////  {
////    return false;
////  }
////	fm.submit();
////}
//
//// 

//是否有补退费
function HaveFee(EdorAcceptNo)
{
	var strSQL ="select getmoney from LPEdorApp where EdorAcceptNo='"+EdorAcceptNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询保全申请主表时出错，保全受理号为"+EdorAcceptNo+"！");
		return "";
	}
	return arrResult[0][0];
}

//帐户余额是否有可领金额是否大于0
function AppAccHaveGetFee(customerNo)
{
	var strSQL ="select accgetmoney from lcappacc where customerNo='"+customerNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询投保人帐户表时出错，客户号为"+customerNo+"！");
		return "";
	}
	return arrResult[0][0];
}

//投保人帐户是否存在
function AppAccExist(customerNo)
{
	var strSQL ="select count(1) from lcappacc where customerNo='"+customerNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询投保人帐户表时出错！");
		return "";
	}
	if(Number(arrResult[0][0])==0)
	{
		return 'No';
	}
	else
	{
		return 'Yes';
	}
}

/*********************************************************************
 *  查询投保人帐户信息
 *  参数：contNo 合同号
 *********************************************************************
 */       
function getAppAccInfo()
{
  var sql = "select OtherNo from LPEdorApp " +
            "where EdorAcceptNo = '" + fm.EdorAcceptNo.value + "' ";
  arrResult = easyExecSql(sql);
	if (arrResult == null) 
	{
		return;
	}
	var customerNo = arrResult[0][0];
	var strSQL ="select customerno,accbala,accgetmoney " +
	            "from LCAppAcc "+	            
	            "where customerno = '" + customerNo + "' " +
	            "and state = '1'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("该客户号的帐户余额功能暂时不能用！");
		return;
	}
	
	//fm.CustomerNo.value=arrResult[0][0];
	fm.AccBala.value=arrResult[0][1];
	fm.AccGetBala.value=arrResult[0][2];
	//fm.CustomerName.value=getCustomerName();
}

function useAcc()
{
  //预收项目不能使用余额
  var sql = "select * from LPEdorItem " +
            "where EdorNo = '" + fm.EdorAcceptNo.value + "' " +
            "and EdorType = 'YS' ";
  var	result = easyExecSql(sql);
	if (((mGetMoney < 0) || ((mGetMoney > 0) && (Number(fm.AccGetBala.value) > 0))) && (result == null))
	{
		document.all("FeeFlag").style.display = "none";                    
		document.all("PayDiv").style.display = "none";    
		fm.ChkYesNo[0].checked = true;
	}
	else
	{
		document.all("FeeFlag").style.display = "";
		document.all("PayDiv").style.display = "";
		fm.ChkYesNo[1].checked = true;
	}
}

function notUseAcc()
{
	if (mGetMoney > 0)
	{
		document.all("FeeFlag").style.display = "";
	}
	document.all("PayDiv").style.display = "";
}

function clickOK()
{
	var selectValue='';
	for(i = 0; i <fm.ChkYesNo.length; i++){
		if(fm.ChkYesNo[i].checked){
			selectValue=fm.ChkYesNo[i].value;
			break;
		}
	}
	
	if (selectValue=='yes')
	{
		if(fm.all('AccType').value=='0')
		{
			fm.all("fmtransact2").value = "INSERT||TakeOut";	
			if (Number(fm.AccGetBala.value) == 0)
			{
				alert("可领金额为零，不能使用帐户余额抵扣！");
				fm.ChkYesNo[1].checked = true;	
				return false;
			}
		}
		if(fm.all('AccType').value=='1')
		{
			fm.all("fmtransact2").value = "INSERT||ShiftTo";	
		}
		return true;
	}
	else
	{
		fm.all("fmtransact2").value = "NOTUSEACC";
		return false;
	}
}

//得到帐户名
function queryAccName(medorAcceptNo)
{
  var sql = "select distinct a.AppntName from LCCont a ,LPEdorMain b " 
          + "    where a.ContNo = b.ContNo "
          + "        and b.EdorAcceptNo = '" + medorAcceptNo + "'";
  var arrResult = easyExecSql(sql);
  if (arrResult != null)
  {
    fm.all("AccName").value = arrResult[0][0];
  }
}

//检查两次输入帐户是否一致
function checkAcc()
{
  var acc1 = fm.BankAccno.value;
  var acc2 = fm.BankAccnoConfirm.value;
  if(acc1 != acc2)
  {
    alert("两次输入的帐号不一致，请校对后重新输入！");
    return false;
  }
  return true;
}

//隐藏银行转帐信息
function hideAcc()
{
  document.all("trAccName").style.display = "none";
  fm.all("AccName").value = "";
  document.all("trBank").style.display = "none";
  fm.all("Bank").value = "";
  fm.all("BankName").value = "";
  document.all("trAccount").style.display = "none";
  fm.all("BankAccno").value = "";
  fm.BankAccno.verify = "";
  document.all("AccConfirm").style.display = "none";
  fm.all("BankAccnoConfirm").value = "";
  document.all("trPayDate").style.display = "none";
  fm.all("PayDate").value = "";
  fm.PayDate.verify = "";
  document.all("trBank1").style.display = "none";
  document.all("trAccount1").style.display = "none";
}

//显示银行转帐信息
function showAcc()
{
  document.all("trPayDate").style.display = "";
  document.all("trBank").style.display = "";
  document.all("trAccount").style.display = "";
  document.all("AccConfirm").style.display = "";
  document.all("trAccName").style.display = "";
  fm.all("AccName").value = "";
  fm.all("BankName").value = "";
  fm.all("Bank").value = "";
  fm.all("BankAccno").value = "";
  fm.all("BankAccnoConfirm").value = "";
  fm.all("PayDate").value = "";
  fm.PayDate.verify = "转帐日期|date";
  fm.BankAccno.verify = "转帐帐号|int";
  queryAccName(medorAcceptNo);
  document.all("trEndDate").style.display = "none";
  fm.EndDate.verify = "";
}

//保单交费方式为银行转帐时，显示银行转帐信息
function showAcc1()
{
  queryAccount(medorAcceptNo);
  document.all("trPayDate").style.display = "";
  fm.all("PayDate").value = "";
  fm.PayDate.verify = "转帐日期|date";
  fm.all("BankName1").value = fm.all("BankName").value;
  fm.all("BankAccno1").value = fm.all("BankAccno").value ;
  document.all("trBank1").style.display = "";
  document.all("trAccount1").style.display = "";
  fm.all("BankAccnoConfirm").value = fm.all("BankAccno").value;
  document.all("trEndDate").style.display = "none";
  fm.EndDate.verify = "";
}

//保单交费方式为个人医疗账户时，显示医疗账户信息
function showAcc2()
{
	queryAccount2(medorAcceptNo);
	document.all("trPayDate").style.display = "";
	fm.all("PayDate").value = "";
	fm.PayDate.verify = "转帐日期|date";
	fm.all("BankName1").value = fm.all("BankName").value;
	fm.all("BankAccno1").value = fm.all("BankAccno").value ;
	document.all("trBank1").style.display = "";
	document.all("trAccount1").style.display = "";
	fm.all("BankAccnoConfirm").value = fm.all("BankAccno").value;
	document.all("trEndDate").style.display = "none";
	fm.EndDate.verify = "";
}

function showPayMode()
{
  if(getContPayMode() == "4")
  {
    fm.PayMode.value = "4";
    afterPayModeSelect();
  }
}

function getContPayMode()
{
  var paymode = "1";
  var sql = "select PayMode " 
              + "from LCCont a, LPEdorMain b " 
              + "where b.ContNo = a.ContNo " 
              + "and   b.EdorAcceptNo = '" + medorAcceptNo + "' " 
              + "union all " 
              + "select PayMode " 
              + "from LBCont a, LPEdorMain b " 
              + "where b.ContNo = a.ContNo " 
              + "and   b.EdorAcceptNo = '" + medorAcceptNo + "' ";
  var result = easyExecSql(sql);
  paymode = result[0][0];
  return paymode;
}

function setRemak()
{
//20100126 zhanggm 如果经过审批，则自动显示审批意见。
  var edorAcceptNo = fm.EdorAcceptNo.value;
  var strSql = "select statusno from lgwork where workno = '" + edorAcceptNo + "' ";
  var arrResult = easyExecSql(strSql);
  if(arrResult[0][0] == "88" || arrResult[0][0] == "99")
  {
    document.all("Remarkdiv").style.display = "";
    strSql = "select remark from lgtracenodeop where workno = '"+edorAcceptNo+"' and operatortype = '6' "
           + "order by makedate desc ,maketime desc ";
    var arrResult1 = easyExecSql(strSql);
    var remark = arrResult1[0][0];
    fm.all('Remark').value = remark;
  }
  else
  {
    document.all("Remarkdiv").style.display = "none";
  }
}


function checkEdorMoney()
{
	var strSql = "select nvl(sum(getmoney),0) from ljsgetendorse where endorsementno= '" + fm.EdorAcceptNo.value + "' ";
	var arrResult = easyExecSql(strSql);
	var getmoney = arrResult[0][0];
	var strSql1 = "select nvl(sum(getmoney),0) from lpedoritem where edorno= '" + fm.EdorAcceptNo.value + "' ";
	var arrResult1 = easyExecSql(strSql1);
	var edormoney = arrResult[0][0];
	if(getmoney!=edormoney)
	{
		return false;
	}
	return true;
}

function checkEdorDate(){
	var myDate = new Date();
	var curYear = myDate.getFullYear(); 
	var curMonth = myDate.getMonth()+1;
	var curDay = myDate.getDate();
	
	if(curMonth<10){
		curMonth = "0"+curMonth;
	}
	if(curDay<10){
		curDay = "0"+curDay;
	}
	var currentDate = curYear+"-"+curMonth+"-"+curDay;
	
	var canSQL = "select edorvalidate from lpedoritem where edoracceptno='"+fm.EdorAcceptNo.value+"' and edortype='TP' ";
	
	var arrResult1 = easyExecSql(canSQL);
	if(arrResult1!=null){
		if(arrResult1[0][0]!=currentDate){
			alert("康乐人生转保保全确认时间必须为保全申请日期");
			return false;
		}
	}
	return true;
}
