<html> 
<%   
//程序名称：
//程序功能：保障调整
//创建日期：2005-12-30
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeBF.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeBFInit.jsp"%>
  <title>保障调整</title> 
</head>
<body onload="initForm();">
  <form action="./PEdorTypeBFSubmit.jsp" method=post name=fm target="fraSubmit">   
   <table class=common>
   		<tr class= common>
	      <td class="title"> 受理号 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=EdorNo >
	      </td>
	      <td class="title"> 批改类型 </td>
	      <td class="input">
	      	<input class="readonly" type="hidden" readonly name=EdorType>
	      	<input class="readonly" readonly name=EdorTypeName>
	      </td>
	      <td class="title"> 保单号 </td>
	      <td class="input">
	      	<input class = "readonly" readonly name=ContNo>
	      </td>   
    	</tr>
  	</table>
		<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divOldPol);">
	      </td>
	      <td class= titleImg>
	        原险种信息
	      </td>
		  </tr>
		</table>
	  <div id= "divOldPol" style= "display: ''">
			<table class= common>
			 		<tr  class= common>
				  		<td text-align: left colSpan=1>
						<span id="spanPolGrid">
						</span> 
				  	</td>
				</tr>
			</table>			
	  	<div id= "divPage" align=center style= "display: 'none' ">
				<input value="首  页" class=cssButton type=button onclick="turnPage.firstPage();"> 
				<input value="上一页" class=cssButton type=button onclick="turnPage.previousPage();"> 					
				<input value="下一页" class=cssButton type=button onclick="turnPage.nextPage();"> 
				<input value="尾  页" class=cssButton type=button onclick="turnPage.lastPage();"> 
			</div>		
		</div>
    <br>
    <input type="button" Class="cssButton"  value="添加保全" onclick="addEdor();" >&nbsp;
    <input type="button" Class="cssButton"  value="撤销保全" onclick="delEdor();" >&nbsp;
    <input type="button" Class="cssButton"  value="健康告知录入" onclick="inputImpart();" >
    <table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divNewPol);">
	      </td>
	      <td class= titleImg>
	        新险种信息
	      </td>
		  </tr>
		</table>
 	  <div id= "divNewPol" style= "display: ''">
			<table class= common>
			 		<tr  class= common>
				  		<td text-align: left colSpan=1>
						<span id="spanNewPolGrid">
						</span> 
				  	</td>
				</tr>
			</table>			
	  	<div id= "divPage" align=center style= "display: 'none' ">
				<input value="首  页" class=cssButton type=button onclick="turnPage2.firstPage();"> 
				<input value="上一页" class=cssButton type=button onclick="turnPage2.previousPage();"> 					
				<input value="下一页" class=cssButton type=button onclick="turnPage2.nextPage();"> 
				<input value="尾  页" class=cssButton type=button onclick="turnPage2.lastPage();"> 
			</div>		
		</div>
<!--
		<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divInsured);">
	      </td>
	      <td class= titleImg>
	        被保人健康告知录入
	      </td>
		  </tr>
		</table>
		<div id= "divInsured" style= "display: ''">
			<table class= common>
			 		<tr  class= common>
				  		<td text-align: left colSpan=1>
						<span id="spanInsuredGrid">
						</span> 
				  	</td>
				</tr>
			</table>
	  	<div id= "divPage" align=center style= "display: 'none' ">
				<input value="首  页" class=cssButton type=button onclick="turnPage3.firstPage();"> 
				<input value="上一页" class=cssButton type=button onclick="turnPage3.previousPage();"> 					
				<input value="下一页" class=cssButton type=button onclick="turnPage3.nextPage();"> 
				<input value="尾  页" class=cssButton type=button onclick="turnPage3.lastPage();"> 
			</div>		
		</div>
-->
		<br>
	 <Input class=cssButton type=Button value="保  存" onclick="save();">
	 <Input class=cssButton type=Button value="取  消" onclick="initForm();">
	 <Input class=cssButton type=Button value="返  回" onclick="returnParent();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
