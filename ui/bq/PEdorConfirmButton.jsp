<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorConfirmButton.jsp
//程序功能：保全确认按钮
//创建日期：2005-04-16
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="./PEdorConfirmButton.js"></SCRIPT>
  <%@include file="./PEdorConfirmButtonInit.jsp"%>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>操作按钮 </title>
</head>
<body onload="initForm();">
<br>
<p><strong><font color="#ff0000">不要在点击&ldquo;保全确认&rdquo;按钮成功后又重复理算，或多次点击保全确认按钮。</font></strong></p>
<form name=fm action="./PEdorConfirmSubmit.jsp" target=fraSubmit method=post>
   <div style="display: ''" align="center">
    <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="GetNoticeNo" name="GetNoticeNo">
    <div id="AccFee" style="display:'none'">
      <TABLE class=common>
			<tr>
				<td class=title>帐户余额</td>
				<td class= input>
				  <Input type="input" class="readonly" readonly name=AccBala>
				</td>
				<td class=title>可领金额</td>
				<td class= input>
				  <Input type="input" class="readonly" readonly name=AccGetBala>
				</td>
				<td class=title>		
					<div id="AccTypeStr"></div>
				</td>
				<td class= input>
				  <input type="radio" name="ChkYesNo" value="yes" onclick="useAcc();">是
					<input type="radio" name="ChkYesNo" value="no" onclick="notUseAcc();" checked>否
				</td>
			</tr>
	  </TABLE>
    </div>
  <div id="FeeFlag" style="display:'none'"><p align="center">需要财务交费</p></div>
  <div id="PayDiv" style="display:'none'">
	<table class=common>
   <tr class=common id="trPayMode">
    <td  class= title>交费/付费方式</td>
		<td  class= input>
			<select name="PayMode" style="width: 128; height: 23" onchange="afterPayModeSelect();" >
				<option value = "0">请选择缴费方式</option>
				<%
				//20080708 zhanggm 整个系统缴费方式统一，改成从ldcode表中取。
				//再增加缴费方式就在payMode数组中增加类型。
				String strSql = "select nvl(GetMoney,0) from LPEdorApp where EdorAcceptNo = '" +request.getParameter("EdorAcceptNo") +"' with ur";
				String sGetMoney = new ExeSQL().getOneValue(strSql);
				double getMoney = Double.parseDouble(sGetMoney);
				System.out.println(getMoney);
					boolean payModeFlag = false;
				if(getMoney<-0.000001)
				{
				String strSQL = "select edortype from lpedoritem where EdorAcceptNo = '" +request.getParameter("EdorAcceptNo") +"' with ur";
				String edortype = new ExeSQL().getOneValue(strSQL);
				String payMODE=null;
				if(edortype!=null&&!"".equals(edortype)&&"WT".equals(edortype)){
    				//校验险种是否是支持医疗个人账户的险种
    				String riskSQL= " select code from ldcode where codetype='medicaloutrisk' ";
    				
    				String strSQL1 = "select 1 from lcpol where contno = (select distinct contno from lpedoritem where EdorAcceptNo = '" +request.getParameter("EdorAcceptNo") +"')" +
    				"and riskcode not in (select code from ldcode1 where CodeType = 'checkappendrisk' " +
    				"and code1 in ("+riskSQL+")) " +
    				"and riskcode not in ("+riskSQL+") with ur";
					String haveFlag = new ExeSQL().getOneValue(strSQL1);
    				
					if(haveFlag!=null&&!"".equals(haveFlag)){
						String wrapSql="select 1 from lccont a,lcriskdutywrap b where a.contno=b.contno "+
						" and a.contno= (select distinct contno from lpedoritem where EdorAcceptNo = '"+request.getParameter("EdorAcceptNo")+"') "+
						" and exists (select 1 from ldcode where codetype ='medicaloutwrap' and code=b.riskwrapcode) with ur";
						String wrapFlag=new ExeSQL().getOneValue(wrapSql);
						if(wrapFlag==null||"".equals(wrapFlag)){
							payModeFlag = true;
						}
					}
				String sqlString1 = "select a.paymode from lccont a where a.contno = (select distinct contno from lpedoritem where EdorAcceptNo = '" +request.getParameter("EdorAcceptNo") +"') with ur"; 	
				payMODE = new ExeSQL().getOneValue(sqlString1);
				if(payMODE==null||!"8".equals(payMODE)){
				System.out.print("-=======不可能");
					payModeFlag = true;
				}
				}else{
					payModeFlag = true;
				}
				if(payModeFlag){
				  String [] payMode = {"1","2","3","4","9"};
				  String tSql = "";
				  String payModeName = "";
				  for(int i=0;i<payMode.length;i++)
				  {
				    tSql = "select codename('paymode','" + payMode[i] + "') from ldcode";
				    payModeName = new ExeSQL().getOneValue(tSql);
				%>
				<option value = <%=payMode[i]%>><%=payModeName%></option>
				<%
					
				}
				  }else{
				  String [] payMode = {"8"};
				  String tSql = "";
				  String payModeName = "";
				  for(int i=0;i<payMode.length;i++)
				  {
				    tSql = "select codename('paymode','" + payMode[i] + "') from ldcode";
				    payModeName = new ExeSQL().getOneValue(tSql);
				%>
				<option value = <%=payMode[i]%>><%=payModeName%></option>
				<%
				}
				  }
				}
				else
				{
				  String [] payMode = {"1","2","3","4","6","9"};
				  String tSql = "";
				  String payModeName = "";
				  for(int i=0;i<payMode.length;i++)
				  {
				    tSql = "select codename('paymode','" + payMode[i] + "') from ldcode";
				    payModeName = new ExeSQL().getOneValue(tSql);
				%>
				<option value = <%=payMode[i]%>><%=payModeName%></option>
				<%
				  }
				}
				%>
			</select>
		</td>
	  </tr>
	  <tr class=common id="trEndDate" style="display: 'none'">
			<td  class= title>截止日期</td>
			<td  class= input>
				<Input class="coolDatePicker" name="EndDate" DateFormat="short" elementtype="nacessary" >
			</td>
	  </tr>
	  </tr>
      <tr class=common id="trAccName" style="display: 'none'">
        <td  class= title>转帐帐户</td>
        <td  class= input><input class="readonly" name="AccName" ></td>
      </tr>
      <tr class=common id="trAccName1" style="display: 'none'">
        <td  class= title>转帐帐户</td>
        <td  class= input><input class="readonly" readonly name="AccName1" ></td>
      </tr>
      <tr class=common id="trBank" style="display: 'none'">
        <td  class= title>转帐银行</td>														
        <td  class= input><input NAME=Bank VALUE="" CLASS="codeNo" MAXLENGTH=20 ondblclick=" return showCodeList('bankcode',[this,BankName],[0,1],null,fm.PayMode.value,fm.ManageCom.value,1);" onkeyup="return showCodeListKey('bankcode',[this,BankName],[0,1],null,fm.PayMode.value,fm.ManageCom.value,1);" ><Input class= codeName name="BankName" elementtype=nacessary readonly ></td>
      </tr>
      <tr class=common id="trBank1" style="display: 'none'">
        <td  class= title>转帐银行</td>
        <td  class= input><input class = "readonly" readonly name="BankName1"></td>
	  </tr>
      <tr class=common id="trAccount" style="display: 'none'">
        <td  class= title>转帐帐号</td>
        <td  class= input><Input class="common" name="BankAccno" elementtype="nacessary"></td>
      </tr>
      <tr class=common id="trAccount1" style="display: 'none'">
        <td  class= title>转帐帐号</td>
        <td  class= input><Input class="readonly" name="BankAccno1" readonly></td>
	  </tr>
      <tr class=common id="AccConfirm" style="display: 'none'" >
        <td  class= title>帐号确认</td>
        <td  class= input><Input class="common" name="BankAccnoConfirm" elementtype="nacessary"></td>
      </tr>
      <tr class=common id="trPayDate" style="display: 'none'">
        <td  class= title>转帐日期</td>
        <td  class= input><Input class="coolDatePicker" name="PayDate" DateFormat="short" elementtype="nacessary"></td>
      </tr>
    </table>
   </div>
    <div id="Printdiv">
  	<table class=common>
	    <tr class=common>
		    <td class= title> 批单打印方式 </td>
	        <td class= input>
	        	<input type="radio" name="printMode" value="1" checked >即时打印
	        	<input type="radio" name="printMode" value="2">批量打印
	        	<input type="radio" name="printMode" value="3">暂不打印
		    </td>
	    </tr>
	  </table>
	</div>
  <div id="Remarkdiv">
    <table class=common>
	  <tr class="common">
		<td class= title>审批意见</td>
		<td class= input colspan="5"><textarea class= common name="Remark" cols="117%" rows="2" readonly></textarea></td>
	  </tr>
    </table>
  </div>
    <br>
		<input type="button" class=cssButton name="confirmButton" id="confirmButton" value="保全确认" onclick="edorConfirm();">
		&nbsp;&nbsp;
		<input type="button" class=cssButton name="redo" id="redo" value="重复理算" onclick="reCal();">
		&nbsp;&nbsp;
  </div>
  <input class="common" name="failType" type="hidden">
  <input class="common" name="autoUWFlagFail" type="hidden">
  <input class="common" name="loadFlag" type="hidden" value="<%=request.getParameter("loadFlag")%>">
  <input type=hidden name = "CustomerNo">
	<input type=hidden name = "AccType">
	<input type=hidden name = "DestSource">
	<input type=hidden name="fmtransact2">
	<input type=hidden name="ManageCom"/>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>