//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();
var IsULI="false";

function returnParent()
{
	try
	{
	  top.opener.focus();
		top.opener.getGrpEdorItem();
		top.close();
	}
	catch (ex) {}
}

function edorTypeWTSave()
{
  if(!checkData())
  {
    return false;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}

/*********************************************************************
 *  查询投保单位信息
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function getGrpCont()
{
  var strSQL = "select * from LCGrpCont  where GrpContNo = '" 
                + fm.GrpContNo.value + "'";
  var arrResult = easyExecSql(strSQL, 1, 0);
  
    if (arrResult != null) 
    {
        try {fm.all('AppntNo').value= arrResult[0][12]; } catch(ex) { }; //客户号码
        try {fm.all('Peoples2').value= arrResult[0][14]; } catch(ex) { }; //投保总人数
        try {fm.all('GrpName').value= arrResult[0][15]; } catch(ex) { }; //单位名称
        try {fm.all('SignCom').value= arrResult[0][48]; } catch(ex) { }; //签单机构
        try {fm.all('SignDate').value= arrResult[0][49]; } catch(ex) { }; //签单日期
        try {fm.all('CValiDate').value= arrResult[0][51]; } catch(ex) { }; //保单生效日期
        try {fm.all('CInValiDate').value= arrResult[0][119]; } catch(ex) { }; //保单截止日期
        try {fm.all('Prem').value= arrResult[0][59]; } catch(ex) { }; //总保费
        try {fm.all('Amnt').value= arrResult[0][60]; } catch(ex) { }; //总保额
        try {fm.all('CustomGetPolDate').value= arrResult[0][81]; } catch(ex) { }; //总保额
        try {fm.all('PolApplyDate').value= arrResult[0][80]; } catch(ex) { }; //总保额
    }
    else
    {
        alert('未查到该集体保单！');
    }     
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  showInfo.close();
  window.focus();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact = fm.all('fmtransact').value;
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
  		//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[1,20,21,31,42])
			//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		 	turnPage.pageDisplayGrid = LPPolGrid;    
		  
		  //设置查询起始位置
		 	turnPage.pageIndex       = 0;
		 	//在查询结果数组中取出符合页面显示大小设置的数组
	  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
			//调用MULTILINE对象显示查询结果
	   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		}
		else
		{
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  	}
  	returnParent();
  }
}

//处理险种相关信息
function queryLCGrpPolGrid()

{
        var verifySql ="select 1  from lcgrppol a where a.grpcontno='"+fm.GrpContNo.value +"' and riskcode in (select riskcode from  lmriskapp where risktype4='4'and Riskprop='G') ";
        var arrVerifyResult = easyExecSql(verifySql);
        var sql ="";    
        if(arrVerifyResult!=null &&arrVerifyResult[0][0]=="1"){
           sql = " select  '',(select riskSeqNo from LCGrPPol where grpContNo = a.grpContNo and riskCode = c.riskCode),c.riskcode ,(select riskname from lmriskapp where riskcode=c.riskcode),a.peoples2, sum(c.prem),"
            + "  a.ProposalGrpContNo, '', '', min(c.cValiDate) from  LCgrpcont a,  LCPol c where a.grpContNo = c.grpContNo and a.grpContNo ='"+fm.GrpContNo.value+"' group by a.grpcontno,a.ProposalGrpContNo,a.peoples2,c.riskcode ";
       IsULI="true";
        }else{
   
   sql = "  select a.contPlanCode, "
            + "   (select riskSeqNo from LCGrPPol where grpContNo = c.grpContNo and riskCode = a.riskCode), "
            + "   a.riskCode, b.riskName, count(distinct c.insuredNo), sum(c.prem), "
            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType, min(c.cValiDate) "
            + "from LCContPlanRisk a, LMRiskApp b, LCPol c "
            + "where a.riskCode = b.riskCode "
            + "   and a.grpContNo = c.grpContNo "
            + "   and a.contPlanCode = c.contPlanCode "
            + "   and a.riskCode = c.riskCode "
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + "group by c.grpContNo, a.contPlanCode, a.riskCode, b.riskName, "
            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType "
            + "order by a.contPlanCode ";
            }
	
	//var sql = "  select a.contPlanCode, f.riskSeqNo, a.riskCode, b.riskName, "
  //          + "   (select count(1) "
  //          + "   from LCInsured c, LCPol d "
  //          + "   where c.contNo = d.contNo "
  //          + "       and c.insuredNo = d.insuredNo "
  //          + "       and c.contPlanCode = a.ContPlanCode "
  //          + "       and d.grpPolNo = f.grpPolNo), "
  //          + "   (select sum(prem) "
  //          + "   from LCInsured c, LCPol d "
  //          + "   where c.contNo = d.contNo "
  //          + "       and c.insuredNo = d.insuredNo "
  //          + "       and c.contPlanCode = a.ContPlanCode "
  //          + "       and d.grpPolNo = f.grpPolNo), "
  //          + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType, f.cValiDate "
  //          + "from LCContPlanRisk a, LMRiskApp b, LCGrpCont e, LCGrpPol f "
  //          + "where a.grpContNo = e.grpContNo "
  //          + "   and a.riskCode = b.riskCode "
  //          + "   and a.grpContNo = f.grpContNo "
  //          + "   and a.riskCode = f.riskCode "
  //          + "   and a.contPlanCode not in('00', '11') "
  //          + "   and b.riskType3 != '7' "
  //          + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
  //          
  //          + "union "
  //          
  //          + "  select a.contPlanCode, f.riskSeqNo, a.riskCode, b.riskName, "
  //          + "   (select count(1) "
  //          + "   from LCInsured c, LCPol d "
  //          + "   where c.contNo = d.contNo "
  //          + "       and c.insuredNo = d.insuredNo "
  //          + "       and c.contPlanCode = a.ContPlanCode "
  //          + "       and d.grpPolNo = f.grpPolNo), "
  //          + "   (select sum(prem) "
  //          + "   from LCInsured c, LCPol d "
  //          + "   where c.contNo = d.contNo "
  //          + "       and c.insuredNo = d.insuredNo "
  //          + "       and c.contPlanCode = a.ContPlanCode "
  //          + "       and d.grpPolNo = f.grpPolNo), "
  //          + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType, f.cValiDate "
  //          + "from LCContPlanRisk a, LMRiskApp b, LCGrpCont e, LCGrpPol f "
  //          + "where a.grpContNo = e.grpContNo "
  //          + "   and a.riskCode = b.riskCode "
  //          + "   and a.grpContNo = f.grpContNo "
  //          + "   and a.riskCode = f.riskCode "
  //          + "   and (e.cardFlag = '0' or riskType3 = '7') "
  //          + "   and a.grpContNo = '" + fm.GrpContNo.value + "' ";
    turnPage.pageLineNum = 100;
	turnPage.queryModal(sql, LCGrpPolGrid);
}


//查询体检费信息
turnPage3 = new turnPageClass();
function getTestGrid()
{
    var sql = "  select a.ProposalContNo, a.PrtSeq, a.customerNo, a.name, "
              + "sum(decimal(c.MedicaItemPrice, 31, 2)) "
              + "from LCPENotice a, LCPENoticeItem b, LDTestPriceMgt c, LCCONT D "
              + "where a.ProposalContNo = b.ProposalContNo "
	          + "    and a.PrtSeq = b.PrtSeq "
	          + "    and b.peItemCode = c.MedicaItemCode "
	          + "    and a.pEAddress = c.hospitCode "
	          + "    and D.grpContNo = '" + fm.GrpContNo.value + "' "
			  + "	  and d.contNo = a.contNo "
	          + "    and (a.peState is null or a.peState <> '06') "
              + "group by a.CustomerNo, a.name, a.ProposalContNo, a.PrtSeq ";

    turnPage3.pageDivName = "divPage3";
    turnPage3.queryModal(sql, TestGrid);
}

//钩选已录入的保障计划
function checkSelectContPlanRisk()
{
	LCGrpPolGrid.checkBoxAllNot(this, LCGrpPolGrid.colCount);
	var sql = "  select ProposalGrpContNo, MainRiskCode, RiskCode, ContPlanCode, PlanType "
	          + "from LPContPlanRisk "
	          + "where edorNo = '" + fm.EdorNo.value + "' "
	          + "   and edorType = '" + fm.EdorType.value + "' "
	          + "   and grpContNo = '" + fm.GrpContNo.value + "' ";
	var result = easyExecSql(sql);
	if(result)
	{
  	for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
  	{
  	  for(var t = 0; t < result.length; t++)
  	  {
    		if(LCGrpPolGrid.getRowColDataByName(i, "contPlanCode") == result[t][3]
    		  && LCGrpPolGrid.getRowColDataByName(i, "riskCode") == result[t][2])
    		{
    			//LCGrpPolGrid.checkBoxSel(i + 1);
    		}
    	}
  	}
  	
  	//LCContGrid.checkBoxAllNot(this, LCContGrid.colCount);
  	//for(var i = 0; i < LCContGrid.mulLineCount; i++)
  	//{
  	//	if(LCContGrid.getRowColData(i, 1) == contNo)
  	//	{
  	//		LCContGrid.checkBoxSel(i + 1);
  	//	}
  	//}
  }
}

//显示退保原因
function setLPGrpEdoItemInfo()
{
  var sql = "  select a.reasonCode, b.codeName, a.edorValiDate "
            + "from LPGrpEdorItem a, LDCode b "
            + "where a.reasonCode = b.code "
            + "   and b.codeType = 'reason_tb' "
            + "   and edorNo = '" + fm.EdorNo.value + "' "
            + "   and edorType = '" + fm.EdorType.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    fm.reason_tb.value = result[0][0];
    fm.reason_tbName.value = result[0][1];
    fm.EdorValidate.value = result[0][2];
  }
}

//保存明细是的数据合法性校验
function checkData()
{
  if(fm.reason_tb.value == "")
  {
    alert("请选择退保原因。");
    return false;
  }
  
  if(!isNumeric(fm.cost.value))
  {
    alert("工本费必须为数字");
    return false;
  }
  
  if(fm.costCheck.checked)
  {
    for (i = 0; i < LCGrpPolGrid.mulLineCount; i++)
  	{
  		if(!LCGrpPolGrid.getChkNo(i)) 
  		{
  		if(IsULI=="false"){
  		  alert("所有保障计划犹豫期撤保才能扣除工本费");
  		  }else{
  		   alert("请选择险种");
  		  }
  		  return false;
  		}
  	}
  }
  
  var selCount = 0;
  for (i = 0; i < LCGrpPolGrid.mulLineCount; i++)
	{
		if(LCGrpPolGrid.getChkNo(i)) 
		{
		  selCount = selCount + 1;
		}
	}
	if(selCount == 0)
	{
	  alert("请选择险种退保");
	  return false;
	}
	
	return true;
}