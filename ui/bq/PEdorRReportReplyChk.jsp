<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorRReportReplyChk.jsp
//程序功能：保全人工核保生存调查报告回复
//创建日期：2002-12-23 11:10:36
//创建人  ：sxy
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.bq.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  	//接收信息
 	LPRReportSchema tLPRReportSchema = new LPRReportSchema();
	TransferData tTransferData = new TransferData();
	String tPolNo = request.getParameter("PolNo");
	String tEdorNo = request.getParameter("EdorNo");
	String tSerialNo = request.getParameter("SerialNo");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	String tReplyResult = request.getParameter("ReplyResult");
	String tPrtSeq = request.getParameter("PrtSeq");	
	
	System.out.println("polno:"+tPolNo);
	System.out.println("tReplyResult:"+tReplyResult);
	
	boolean flag = true;
	if (!tPolNo.equals("")&& !tSubMissionID.equals("")&& !tMissionID.equals("")&&!tSerialNo.equals(""))
	{
		tLPRReportSchema.setPolNo(tPolNo);
		tLPRReportSchema.setSerialNo(tSerialNo);
		tLPRReportSchema.setReplyContente(tReplyResult);
		
		//准备公共传输信息
		tTransferData.setNameAndValue("PolNo",tPolNo);
		tTransferData.setNameAndValue("EdorNo",tEdorNo) ;
		tTransferData.setNameAndValue("MissionID",tMissionID);	
		tTransferData.setNameAndValue("PrtSeq",tPrtSeq);	
		tTransferData.setNameAndValue("SubMissionID",tSubMissionID);	
		tTransferData.setNameAndValue("LPRReportSchema",tLPRReportSchema);	
	}
	else
	{
		flag = false;
		Content = "数据不完整!";
	}	
	System.out.println("flag:"+flag);
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( tTransferData);
		tVData.add( tG );
		
		// 数据传输
		PEdorManuUWWorkFlowUI tPEdorManuUWWorkFlowUI   = new PEdorManuUWWorkFlowUI();
		if (!tPEdorManuUWWorkFlowUI.submitData(tVData,"0000000013"))//执行保全核保生调工作流节点0000000004
		{
			int n = tPEdorManuUWWorkFlowUI.mErrors.getErrorCount();
			Content = " 条件核保失败，原因是: " + tPEdorManuUWWorkFlowUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPEdorManuUWWorkFlowUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 保全人工核保生调回复成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	Content = " 保全人工核保生调回复失败，原因是:" + tError.getFirstError();
		    	FlagStr = "Fail";
		     }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+".提示：异常终止!";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
