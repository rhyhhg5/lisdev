<%
//程序名称：PEdorConfirmRePrintSubmit.jsp
//程序功能：保全确认打印接口
//创建日期：2004-8-4 13:58
//创建人  ：JL
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并提交后台处理。
  //输入参数
  //个人批改信息
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
  
  //提交后台处理类
  PEdorConfirmRePrintUI tPEdorConfirmRePrintUI   = new PEdorConfirmRePrintUI();
 
  //全局变量
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
  System.out.println("==> PEdorConfirmRePrintSubmit.jsp ：Operator---"+tG.Operator);
  
  //输出参数
  CErrors tError = null;               
  String FlagStr = "";
  String Content = "";
  String Result = "";
  String transact = "";
  
  //取得前台动作
  transact = request.getParameter("fmtransact");
  System.out.println("==> PEdorConfirmRePrintSubmit.jsp ：transact---" + transact);
  
 
  //取得个人批改信息
  tLPEdorMainSchema.setEdorAppNo(request.getParameter("EdorNo"));
  
  try
  {
    // 准备传输到后台处理的数据 VData
    VData tVData = new VData();   	

    //此路径当确认项目为LR时用于打印的路径
    String strTemplatePath = application.getRealPath("xerox/printdata") + "/";
    System.out.println("==> PEdorConfirmRePrintSubmit.jsp : strTemplatePath---" + strTemplatePath);    
    
    tVData.addElement(strTemplatePath);  	 
    tVData.addElement(tLPEdorMainSchema);     
    tVData.addElement(tG);

    //提交后台处理		
    tPEdorConfirmRePrintUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorConfirmRePrintUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

