<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：GEdorTypeMJAccBalaSave.jsp
//程序功能：
//创建日期：2006-1-12
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  String flag = "";
  String content = "";
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType");
  
  EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(edorNo, edorType);
  tEdorItemSpecialData.add(BQ.ACCBALA_FIEXD, request.getParameter("accBalaFixed"));
  tEdorItemSpecialData.add(BQ.ACCBALA_GROUP, request.getParameter("accBalaGroup"));
  tEdorItemSpecialData.add(BQ.ACCBALA_INSURED, request.getParameter("totalInsuredAccMoney"));
  if(!tEdorItemSpecialData.insert())
  {
    flag = "Fail";
    content = "满期处理失败。";
  }
  else
  {
    flag = "Succ";
    content = "满期处理成功。";
  }
%>                      
<html>
<script language="javascript">
	//parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>

