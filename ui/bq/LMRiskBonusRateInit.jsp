
<%
//程序名称：PContTeminateInputInit.jsp
//程序功能：
//创建日期：2010-04-07 16:00
//创建人  ：WangHongLiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
%> 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">  

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";
var tYear=tCurrentDate.substring(0,4);
//重置
function BonusRateBox()
{
  try
  {
	fm.all('RiskCode').value = "";
	fm.all('RiskName').value = "";
	fm.all('BonusYear').value = tYear;
    fm.all('AppYear').value = "";
    fm.all('InsuYears').value = "";
    
    fm.all('PayYears').value = "";
	fm.all('HLRate').value = "";
	fm.all('MinAge').value = "";
    fm.all('MaxAge').value = "";
    

	  document.getElementById("BonusYear").readOnly=false;
	  document.getElementById("AppYear").readOnly=false;
	  document.getElementById("AppYearAfter").readOnly=false;
	  document.getElementById("RiskCode").readOnly=false;
	  document.getElementById("InsuYears").readOnly=false;
	  document.getElementById("PayYears").readOnly=false;
	  document.getElementById("MinAge").readOnly=false;
	  document.getElementById("MaxAge").readOnly=false;
    
  }
  catch(ex)
  {
    alert("在LMRiskBonusRateInit.jsp-->BonusRateBox函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
   try
   { 
    BonusRateBox();
    initBonusRateGrid(); 
    initElementtype();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}


function initBonusRateGrid()
{
    var iArray = new Array();

      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="险种代码";
        iArray[1][1]="90px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="红利年度";
        iArray[2][1]="80px";
        iArray[2][2]=100;
        iArray[2][3]=0;

        iArray[3]=new Array();
        iArray[3][0]="保单承保年度";
        iArray[3][1]="80px";
        iArray[3][2]=100;
        iArray[3][3]=0;
        
        iArray[4]=new Array();
        iArray[4][0]="保险期间";
        iArray[4][1]="80px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="缴费期间";
        iArray[5][1]="80px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="最小投保年龄";
        iArray[6][1]="80px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="最大投保年龄";
        iArray[7][1]="80px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="分红率";
        iArray[8][1]="80px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
      
        
      BonusRateGrid = new MulLineEnter("fm" ,"BonusRateGrid" ); 
      //这些属性必须在loadMulLine前
      BonusRateGrid.mulLineCount = 10;   
      BonusRateGrid.displayTitle = 1;
      BonusRateGrid.locked = 1;
      BonusRateGrid.canSel=1;
      BonusRateGrid.canChk = 0;
      BonusRateGrid.hiddenSubtraction=1;
      BonusRateGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      BonusRateGrid.selBoxEventFuncName="ShowDetail";
      BonusRateGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert("在LMRiskBonusRateInit.jsp-->BonusRateGrid函数中发生异常:初始化界面错误!");
      }
}

</script>
