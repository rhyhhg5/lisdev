<%
//程序名称：GEdorTypeDetailSave.jsp
//程序功能：
//创建日期：2005-11-1
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String flag;
	String content;
	
	GlobalInput gi = (GlobalInput)session.getValue("GI");
	LPGrpEdorItemSchema itemSchema = new LPGrpEdorItemSchema();
	itemSchema.setEdorNo(request.getParameter("EdorNo"));
	itemSchema.setEdorType(request.getParameter("EdorType"));
	itemSchema.setEdorState(BQ.EDORSTATE_INPUT);

	ChangeEdorStateUI tChangeEdorStateUI = new ChangeEdorStateUI(gi, itemSchema);
	if (!tChangeEdorStateUI.submitData())
	{
		flag = "Fail";
		content = "数据保存失败！原因是:" + tChangeEdorStateUI.getError();
	}
	else 
	{
		flag = "Succ";
		content = "数据保存成功。";
	}
%>   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>