<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<% 
//程序名称：GEdorTypeMJDetail.jsp
//程序功能：团体保全明细总页面
//创建日期：2006-01-05
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>   
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeMJ.js"></SCRIPT>
  <SCRIPT src="./PolOperation.js"></SCRIPT>
  <%@include file="GEdorTypeMJInit.jsp"%>
  
  <title>团体保全明细总页面</title> 
</head>

<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">    
    <table class=common id="commonInfo" style="display: none">
      <TR  class= common> 
        <TD  class= title > 批单号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
        	<input class = "readonly" type="hidden" readonly name=EdorType>
      		<input class = "readonly" readonly name=EdorTypeName>
        </TD>
        <TD class = title > 集体保单号 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=GrpContNo>
        </TD>   
      </TR>
    </TABLE> 
    <br>
    
    <Div  id= "divLCGrpPol" style= "display: ''" align=center>  
  		<table  class= common>
  	 		<tr  class= common>
  		  	<td text-align: left colSpan=1>
  				  <span id="spanLCGrpPolGrid" >
  				  </span> 
  		  	</td>
  		  </tr>
  	  </table>
  	  <Div id= "divPage" align=center style= "display: 'none' ">
    	  <INPUT VALUE="首  页" class=cssButton TYPE=button onclick="getFirstPage();"> 
    	  <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
    	  <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
    	  <INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="getLastPage();"> 
      </Div>
  	</Div>
    
    <div id="divLCGrpContInfo" style="display:none">
      <table>
        <tr>
          <td><IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLCGrpPolInfo);"></td>
          <td class= titleImg>特需医疗帐户信息</td>
          <td><INPUT class=CssButton VALUE="特需医疗事项查询" TYPE=button onclick="grpSpecQuery();"> </td>
        </tr>
      </table>
      <div id="divLCGrpPolInfo" style="">
        <table class="common">
          <tr class="common">
            <td class="title">保单号</td>
            <TD class = input >
        	    <input class = "readonly" readonly name=GrpContNo2>
            </TD> 
            <td class="title">投保单位名称</td>
            <td class="input">
              <input class="readonly" readonly name="grpName" value="">
            </td>
            <td class="title">利息/利率</td>
            <td class="input" colspan="1">
              <input class="code" name="rateType" value="1" type="hidden">
	        	  <input class="code" name="rateTypename" value="录入利息" style="width:60" readonly CodeData="0|^1|录入利息^2|录入利率^3|默认利率" ondblclick="return showCodeListEx('mjrateinterest', [this,rateType], [1,0]);" onkeyup="return showCodeListEx('mjrateinterest', [this,rateType], [1,0]);"><input class="common" name="accRate" value="" style="width:100">
            </td>
          </tr>
          <tr class="common">
            <td class="title">账户金额总额</td>
            <td class="input">
              <input class="readonly" readonly name="totalAccMoney" value="">
            </td>
            <td class="title">投保时间</td>
            <td class="input">
              <input class="readonly" readonly name="polApplyDate" value="">
            </td>
            <td class="title">满期结算日</td>
            <td class="input">
              <input class="readonly" readonly name="cInValiDate" value="">
            </td>
          </tr>
          <tr class="common">
            <td class="title">团体账户金额</td>
            <td class=input>
              <input class="readonly" readonly name="grpAccMoney" value="">
            </td>
            <td class="title">账户利息</td>
            <td class=input>
              <input class="readonly" readonly name="grpInterest" value="">
            </td>
            <td class="title">账户成立日期</td>
            <td class=input>
              <input class="readonly" readonly name="GrpAccFoundDate" value="">
            </td>
          </tr>
          
          <tr class="common">
            <td class="title">个人账户总金额</td>
            <td class="input">
              <input class="readonly" readonly name="totalInsuredAccMoney" value="">
            </td>
            <td class="title">账户利息</td>
            <td class=input>
              <input class="readonly" readonly name="insuredInterest" value="">
            </td>
            
            <td class="title">个人账户总数</td>
            <td class="input">
              <input class="readonly" readonly name="insuredAccCount" value="">
            </td>
            <!--
            <td class="title">
              <input type=Button class=cssButton value="被保人清单" onclick="insuredList();">
            </td>
            -->
          </tr>
          <tr class="common">
            <td class="title">特别约定</td>
            <td class="input" colspan="5">
              <textarea class="readonly" readonly style="border: none; overflow: hidden; background-color: #F7F7F7;" name="remark" value="" cols="90%" rows="3"></textarea> 
            </td>
          </tr>
          
          <tr class="common">
            <td class="title">当前理赔情况</td>
            <td class="input">
              <input class="readonly" type=hidden readonly name="claimState" value="">
              <input class="readonly" readonly name="claimStateName" value="">
            </td>
            <td class="title">满期处理操作</td>
            <td class="input">
              <!-- 特需险种取消续保功能
              <input class="codeNo" name="dealType" value="" CodeData="0|^1|续保^2|满期终止" readOnly
               -->
              <input class="codeNo" name="dealType" value="" CodeData="0|^2|满期终止" readOnly
    	          ondblclick="return showCodeListEx('dealType',[this,dealTypeName],[0,1]);" 
    	          onkeyup="return showCodeListEx('dealType',[this,dealTypeName],[0,1]);"><Input class="codeName" name="dealTypeName" readonly>
            </td>
            <td class="title">保单状态</td>
            <td class="input">
              <input class="readonly" readonly name="grpContState" value="">
            </td>
          </tr>
          
          <tr class="common">
            <td class="title">处理状态</td>
            <td class="input">
              <input class="readonly" readonly name="MJState" value="">
            </td>
            <td class="title"></td>
            <td class="input">
            </td>
            <td class="title"></td>
            <td class="input">
            </td>
          </tr>
          
          <tr class="common">
            <td class="common" colspan=6>
              <input type=Button class=cssButton id="calButton" value="满期理算" onclick="endDateCalculate();">
              <input type=Button class=cssButton id="dealButton" value="满期处理" onclick="endDateDeal();">
              <!--input type=Button class=cssButton value="打印满期结算单" onclick="printCalList();"-->
              <!--input type=Button class=cssButton value="打印满期结算单" onclick="printCalListPdf();"-->
              <input type=Button class=cssButton value="打印满期结算单" onclick="printCalListPdfNew();">
              <!--input type=Button class=cssButton id="XBDealButton" value="打印续保批单" onclick="printEndDateListPdf('XBDeal');"-->
              <input type=Button class=cssButton id="XBDealButton" value="打印续保批单" onclick="printEndDateListPdfNew('XBDeal');">
              <!--input type=Button class=cssButton id="MJDealButton" value="打印满期终止结算单" onclick="printEndDateListPdf('MJDeal');"-->
              <input type=Button class=cssButton id="MJDealButton" value="打印满期终止结算单" onclick="printEndDateListPdfNew('MJDeal');">
              <input type=Button class=cssButton id="ReturnButton" value="满期理算回退" onclick="returnCal();">
              <input type=Button class=cssButton value="被保人清单" onclick="printInsuredList();">
            </td>
            <td class="common">
            </td>
          </tr>
        </table>
      </div>
    </div>
    <input type=hidden readonly name=grpPolNo >
    <input type=hidden name="accBalaFixed">
    <input type=hidden name="accBalaGroup">
    <div id="test"></div>
    <br>
    <div id="ErrorsInfo"></div>
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  	<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>
</body>
</html>

<script>
  window.focus();
</script>
