<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="PEdorAppConfirm.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorAppConfirmInit.jsp"%>
   <%@include file = "ManageComLimit.jsp"%>

  <title>保全申请确认 </title>
</head>
<body  onload="initForm();" >

  <form action="./PEdorAppConfirmSubmit.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor1);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>

    <Div  id= "divLPEdor1" style= "display: ''">
      <table  class= common>
        <TR class=common>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class= common name=ContNo >
          </TD>
          <TD  class= title>
            批单号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
            <INPUT VALUE="自动核保信息" TYPE=button class=cssButton onclick="QueryUWDetailClick();"> 
          </TD>
        </TR>

      </table>
    </Div>
    <!--     <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();">  -->
         
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor2);">
    		</td>
    		<td class= titleImg>
    			 个人批改信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdor2" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanPEdorAppConfirmGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="getLastPage();"> 
            				
  	</div>
  	<br>
  	<table class= common>
  	 <TR class= common>
         	<TD  class= input > 
  			<INPUT class=cssButton VALUE="申请确认" TYPE=button onclick="edorAppConfirm();"> 
        	</TD>
        	<TD class = input>
            		 <INPUT class=cssButton VALUE="返回" TYPE=button onclick="returnParent();"> 
        	</TD>
        </TR>		
   </table>
        <input type=hidden id="EdorType" name="EdorType">
        <input type=hidden name= fmtransact >	
        <input type=hidden name= OtherNo>
        <input type=hidden name= GetNoticeNo>
        <input type=hidden name= AppntNo>
        <input type=hidden name= SumDuePayMoney>
        <input type=hidden name= prtParams>
        <input type=hidden name= EdorAcceptNo>		
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
