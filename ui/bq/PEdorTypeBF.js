var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function queryOldPol()
{
  var sql = "select ContNo, PolNo, RiskSeqNo, RiskCode, (select RiskName from LMRisk where RiskCode = a.RiskCode), InsuredNo, InsuredName, " +
            "     Amnt, case Mult when 0 then '' else to_char(Mult) end, Prem, CValiDate, PayToDate, EndDate, '', '' " +
            "from LCPol a " +
            "where ContNo = '" + fm.ContNo.value + "' " +
            "order by RiskSeqNo ";
  turnPage.pageLineNum = 100;
  turnPage.queryModal(sql, PolGrid);
}

function queryNewPol()
{
  var sql = "select a.ContNo, a.PolNo, a.RiskSeqNo, a.RiskCode, (select RiskName from LMRisk where RiskCode = a.RiskCode), a.InsuredNo, a.InsuredName, " +
            "    b.Amnt, case when b.Mult > 0 then '' else to_char(a.Amnt) end, case b.Mult when 0 then '' else to_char(b.Mult) end, case a.Mult when 0 then '' else to_char(a.Mult) end, " +
            "    (select EdorValue from LPEdorEspecialData " +
            "     where EdorNo = '" + fm.EdorNo.value + "' " +
            "     and EdorType = '" + fm.EdorType.value + "' " +
            "     and PolNo = a.PolNo), " +
            "     case when exists (select EdorNo from LPCustomerImpart " +
            "     where EdorNo = '" + fm.EdorNo.value + "' " + 
            "     and CustomerNo = a.InsuredNo) then '已录入' else '未录入' end " +
            "from LPPol a, LCPol b " +
            "where a.PolNo = b.PolNo " +
            "and a.ContNo = '" + fm.ContNo.value + "' " +
            "and a.EdorNo = '" + fm.EdorNo.value + "' " +
            "and a.EdorType = '" + fm.EdorType.value + "' " +
            "order by a.RiskSeqNo ";
  turnPage2.pageLineNum = 100;
  turnPage2.queryModal(sql, NewPolGrid);
}

function afterQuery(arrQueryResult)
{
   fm.all("NewAppntNo").value = arrQueryResult[0][0];
   fm.all("NewAppntName").value = arrQueryResult[0][1];
}

function addEdor()
{
  var chkNum = 0;
  for (i = 0; i < PolGrid.mulLineCount; i++)
  {
  	if (PolGrid.getChkNo(i)) 
		{
			var polNo = PolGrid.getRowColData(i, 2);
			try
		  {
				fm.all("PolGridChk")[i].value = polNo;
			}
			catch(e)
			{
				fm.all("PolGridChk").value = polNo;
			}
		  chkNum = chkNum + 1;
		}
	}
	if (chkNum == 0)
	{
		alert("请选择需变更的险种！");
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "./PEdorTypeBFSubmit.jsp?Operator=ADD";
	fm.submit();
}

function delEdor()
{
	var chkNum = 0;
  for (i = 0; i < NewPolGrid.mulLineCount; i++)
  {
  	if (NewPolGrid.getChkNo(i)) 
		{
			var newPolNo = NewPolGrid.getRowColData(i, 2);
			try
		  {
				fm.all("NewPolGridChk")[i].value = newPolNo;
			}
			catch(e)
			{
				fm.all("NewPolGridChk").value = newPolNo;
			}
			chkNum = chkNum + 1;
		}
	}
	if (chkNum == 0)
	{
		alert("请选择需撤销的险种！");
		return false;
	}
	if (!confirm("确定删除此保全变更？"))
	{
		return false;
	}
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "./PEdorTypeBFSubmit.jsp?Operator=DEL";
	fm.submit();
}

function inputImpart()
{
	var insuredNo;
	var chkNum = 0;
  for (i = 0; i < NewPolGrid.mulLineCount; i++)
  {
  	if (NewPolGrid.getChkNo(i)) 
		{
			chkNum = chkNum + 1;
		  if (chkNum == 1)
			{
				 insuredNo = NewPolGrid.getRowColData(i, 6);
			}
		}
	}
	if (chkNum == 0)
	{
		alert("请选择需录入健康告知的被保人！");
		return false;
	}
	if (chkNum > 1)
	{
		alert("只能选择一个险种！");
		return false;
	}
	var openUrl = "./PEdorHealthImpartMain.jsp?EdorNo=" + fm.EdorNo.value +
	              "&EdorType=" + fm.EdorType.value +
	              "&ContNo=" + fm.ContNo.value +
	              "&InsuredNo=" + insuredNo +
	              "&Flag=1";
	window.open(openUrl);
}

function save()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorTypeBFSubmit.jsp?Operator=SAVE";
	fm.submit();
}

function afterSubmit(flag, content, operator)
{
  try
  {
    showInfo.close();
    window.focus();
  }
  catch (ex) {}
	if (operator == "ADD" || operator == "DEL")
	{
		queryNewPol();
	}
	else if (operator == "SAVE")
	{
	  if (flag == "Fail")
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  }
	  else
	  {
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	    returnParent();
	  }
	}
}

function returnParent()
{	
  try
	{
    top.opener.focus();
	  top.opener.getEdorItem();
	  top.close();
	}
	catch (ex) {}
}