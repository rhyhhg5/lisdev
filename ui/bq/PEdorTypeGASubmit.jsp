<%
//程序名称：PEdorTypeGASubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  LPEdorItemSchema tLPEdorItemSchema   = new LPEdorItemSchema();
 // LPGrpEdorItemSchema tLPGrpEdorItemSchema   = new LPGrpEdorItemSchema();

  LPGetSchema tLPGetSchema = new LPGetSchema();
  LPGetSet tLPGetSet = new LPGetSet();
  LCInsureAccSet tLCInsureAccSet = new LCInsureAccSet();
  LCInsureAccSchema tLCInsureAccSchema = new LCInsureAccSchema();
  
  LPAccMoveSet tLPAccMoveSet = new LPAccMoveSet();
  
  
  
  TransferData tTransferData = new TransferData();
  
  PEdorGADetailUI tPEdorGADetailUI   = new PEdorGADetailUI();
 
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
 System.out.println("-----"+tG.Operator);
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
    
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String Result ="";
  String transact = "";
  String mContType = "";
   
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("---------transact:"+transact);
 
// tLPGrpEdorItemSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
// tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
// tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
  //个人批改信息
 tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
 tLPEdorItemSchema.setInsuredNo(request.getParameter("InsuredNo"));
 tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
 tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
 if (transact.equals("QUERY||ACCOUNT")||transact.equals("INSERT||MAIN"))
 {
 //准备给付项信息
//	 String tGridNo[] = request.getParameterValues("LPGetGridNo");
//	 String tGetDutyCode[] = request.getParameterValues("LPGetGrid1");
//	 String tDutyCode[] = request.getParameterValues("LPGetGrid2");
//	 String tGetDutyKind[] = request.getParameterValues("LPGetGrid3");
//	 String tGetIntv[] = request.getParameterValues("LPGetGrid4");
//   String tAddRate[] = request.getParameterValues("LPGetGrid5");
//	 String tGetStartDate[] = request.getParameterValues("LPGetGrid6");
//	 String tGetEndDate[] = request.getParameterValues("LPGetGrid7");
//	 String tGetMoney[] = request.getParameterValues("LPGetGrid8");
//			
//		int Count = tGridNo.length;
//	  String tFlagSel[]=request.getParameterValues("InpLPGetGridSel");
//		System.out.println("--count"+Count);
//	  for (int i=0;i<Count;i++)
//	  {  	
//  		if(tFlagSel[i].equals("1"))
//  		{
//	  		tLPGetSchema.setDutyCode(tDutyCode[i]);
//		  	tLPGetSchema.setGetDutyKind(tGetDutyKind[i]);
//		  	tLPGetSchema.setGetDutyCode(tGetDutyCode[i]);
//		  	tLPGetSchema.setPolNo(request.getParameter("PolNo"));
//		  	tLPGetSchema.setGetIntv(tGetIntv[i]);
//		  	tLPGetSchema.setAddRate(tAddRate[i]);
//		  	tLPGetSchema.setGetStartDate(tGetStartDate[i]);
//		  	
//		  	tLPGetSchema.setGetEndDate(tGetEndDate[i]);
//		  	tLPGetSchema.setActuGet(tGetMoney[i]);
//		  	tLPGetSet.add(tLPGetSchema);
//  		}
//  	}
  }

    if (transact.equals("INSERT||MAIN")||transact.equals("QUERY||MANAGE"))
    {
        //准备帐户转移信息
        String tGridNo1[] = request.getParameterValues("LPAccMoveGridNo");
        String tEdorNo[] =request.getParameterValues("LPAccMoveGrid1"); //批单号
        String tEdorType[] =request.getParameterValues("LPAccMoveGrid2"); //批改类型
        String tPolNo[] =request.getParameterValues("LPAccMoveGrid3"); //保单号码
        String tInsuAccNo[] =request.getParameterValues("LPAccMoveGrid4"); //保险帐户号码
        String tPayPlanCode[] =request.getParameterValues("LPAccMoveGrid5"); //保险子帐户
        String tRiskCode[] =request.getParameterValues("LPAccMoveGrid6"); //险种编码
        String tAccType[] =request.getParameterValues("LPAccMoveGrid7"); //账户类型
        String tOtherNo[] =request.getParameterValues("LPAccMoveGrid8"); //对应其它号码
        String tOtherType[] =request.getParameterValues("LPAccMoveGrid9"); //对应其它号码类型
        String tAccAscription[] =request.getParameterValues("LPAccMoveGrid10"); //账户归属属性
        String tAccMoveType[] =request.getParameterValues("LPAccMoveGrid11"); //帐户转移类型
        String tAccMoveNo[] =request.getParameterValues("LPAccMoveGrid12"); //帐户转移号码
        String tAccMoveBala[] =request.getParameterValues("LPAccMoveGrid13"); //变动保险帐户现金余额
        String tAccMoveUnit[] =request.getParameterValues("LPAccMoveGrid14"); //变动保险帐户单位数
        String tAccMoveRate[] =request.getParameterValues("LPAccMoveGrid15"); //变动保险帐户比例
        String tMoneyMoveType[] =request.getParameterValues("LPAccMoveGrid16"); //资金转换类型

			
		int iCount = tGridNo1.length;
        //String tFlagSel1[]=request.getParameterValues("InpLCInsureAccGridSel");
		System.out.println("--icount"+iCount);
        for (int i=0;i<iCount;i++)
        { 	
  		    LPAccMoveSchema LPAccMoveSchema = new LPAccMoveSchema();
  		    tLPAccMoveSchema.setEdorNo(tEdorNo[i]);
            tLPAccMoveSchema.setEdorType(tEdorType[i]);
            tLPAccMoveSchema.setPolNo(tPolNo[i]);
            tLPAccMoveSchema.setInsuAccNo(tInsuAccNo[i]);
            tLPAccMoveSchema.setPayPlanCode(tPayPlanCode[i]);
            tLPAccMoveSchema.setRiskCode(tRiskCode[i]);
            tLPAccMoveSchema.setAccType(tAccType[i]);
            tLPAccMoveSchema.setOtherNo(tOtherNo[i]);
            tLPAccMoveSchema.setOtherType(tOtherType[i]);
            tLPAccMoveSchema.setAccAscription(tAccAscription[i]);
            tLPAccMoveSchema.setAccMoveType(tAccMoveType[i]);
            tLPAccMoveSchema.setAccMoveNo(tAccMoveNo[i]);
            tLPAccMoveSchema.setAccMoveBala(tAccMoveBala[i]);
            tLPAccMoveSchema.setAccMoveUnit(tAccMoveUnit[i]);
            tLPAccMoveSchema.setAccMoveRate(tAccMoveRate[i]);
            tLPAccMoveSchema.setMoneyMoveType(tMoneyMoveType[i]);
            tLPAccMoveSet.add(tLPAccMoveSchema);
  	    }
	}

 try
  {
  // 准备传输数据 VData
  
  	 VData tVData = new VData();   
	 //保存个人保单信息(保全)	
	  //tVData.addElement(mContType);
  	tVData.addElement(tLPEdorItemSchema);
//  	tVData.addElement(tLPGrpEdorItemSchema);

  	//tVData.addElement(tG);
  	//if (transact.equals("QUERY||ACCOUNT")||transact.equals("INSERT||MAIN")||transact.equals("QUERY||MANAGE"))
 	//	{
  	//	tVData.addElement(tLPGetSchema);
  	//}
  	//if (transact.equals("QUERY||MANAGE"))
  	//{
	//  	tVData.addElement(tLCInsureAccSet);
  	//}
  	//if (transact.equals("INSERT||MAIN"))
  	//{
	//  	tVData.addElement(tLPGetSet);
	//  	tVData.addElement(tLCInsureAccSet);
	//  	tVData.addElement(tTransferData);
	//  }  
    //
	//	tPEdorGADetailUI.submitData(tVData,transact);
    //}
    if (transact.equals("INSERT||MAIN"))
    {
        tVData.addElement(tLPAccMoveSet);
    }
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorGADetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = "提交成功。";
    	FlagStr = "Success";
    	if (tPEdorGADetailUI.getResult()!=null&&tPEdorGADetailUI.getResult().size()>0)
    	{
    		Result = (String)tPEdorGADetailUI.getResult().get(0);
    		//if (Result==null||Result.trim().equals(""))
    		//{
    		//	FlagStr = "Fail";
    			//Content = "提交失败!!";
    		//}
    	}
    }
    else                                                                           
    {
    	Content = "提交失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

