var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var mSql="";

function easyPrint()
{	
	var tManageCom = fm.all('ManageCom').value;			//管理机构代码
	var tSaleChnlType = fm.all('SaleChnlType').value;		//保单类型
	var tDueStartDate = fm.all('DueStartDate').value;			//保全结案起期
	var tDueEndDate = fm.all('DueEndDate').value;				//保全结案止期

	//管理机构校验
	if(tManageCom == "" || tManageCom == null)
	{
		alert("请选择管理机构！");
		return false;
	}
	if(tSaleChnlType == "" || tSaleChnlType == null)
	{
		alert("请选择保单类型！");
		return false;
	}
	//应收时间起期校验
	if(tDueStartDate == "" || tDueStartDate == null)
	{
		alert("请输入保全结案起期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("保全结案起期",tDueStartDate))
		{
			fm.all('DueStartDate').focus();
			return false;
		}
	}
	//应收时间止期校验
	if(tDueEndDate == "" || tDueEndDate == null)
	{
		alert("请输入保全结案止期！");
		return false;
	}
	else
	{
		if(!checkDateFormat("保全结案止期",tDueEndDate)){
			fm.all('DueEndDate').focus();
			return false;
		}
	}
	//应收时间起止期三个月校验
	var t1 = new Date(tDueStartDate.replace(/-/g,"\/")).getTime();
    var t2 = new Date(tDueEndDate.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);  
    if(tMinus>365 )
    {
	  alert("应收时间起止日期不能超过一年！")
	  return false;
    }
	//传递页面信息
	fm.submit();
}
//日期格式校验
function checkDateFormat(tName,strValue) 
{
	if (!isDate(strValue))
	{
		alert("输入的["+tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}