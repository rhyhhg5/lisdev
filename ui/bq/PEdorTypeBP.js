//               该文件中包含客户端需要处理的函数和事件

var showInfo1;
var mDebug="1";

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPagePrem = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPageAcc = new turnPageClass();
var turnPagePolState = new turnPageClass();
var turnPagePol = new turnPageClass();

function initInpBox()
{ 
	//判断是否是在工单查看中查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
 
  	if (flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  	}
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;  
    fm.all('EdorValiDate').value = top.opener.fm.all('EdorValiDate').value;      
    fm.all('EdorAppDate').value = top.opener.fm.all('EdorAppDate').value; 
    //showOneCodeName("EdorCode", "EdorTypeName"); 
}

function initForm()
{
  try
  {
    initInpBox();
    initPolInfo();
    initPayState();
    initNewPrem();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function returnParent()
{
	try
	{
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {}
}


function save()
{
  if(!verifyInput2())
  {
	return false;
  }
  if(!checkNewPrem())
  {
    return false;
  }
//  if(!isLoanState())
//  { 
//    return false;
//  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  returnParent();
	}
}

        

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function checkNewPrem()
{
  var tCurPrem = fm.all('Prem').value;
  var tNewPrem = trim(fm.all('NewPrem').value);
  var tCurAmnt = trim(fm.all('Amnt').value);
  fm.UliInfo.value = "";
  divUliInfo.style.display="none";
  
  var SQLTPHI =" select insuredno from lcpol where riskcode in (select riskcode from lmriskapp where taxoptimal='Y') and contno='"+fm.all('ContNo').value+"'";
  var result1 = easyExecSql(SQLTPHI);
  //税优产品 单独校验
  if (result1 != null) 
  { 
	  if(tCurPrem == tNewPrem)
	  {
		alert("调整后的基本保费未做改变");
		return false;
	  }
  }else{
	  if(tCurPrem == tNewPrem)
	  {
		alert("调整后的基本保费未做改变");
		return false;
	  }
	  else if ((tNewPrem % 1000)!= 0)
	  {
	  	alert("基本保费必须为1000元的整数倍");
	    return false;
	  } 
	  else if((tNewPrem-tCurPrem)%1000!=0)
	  {
	  	alert("增加减少的保费需为1000元整数倍");
	    return false;
	  }
	  else if(tCurAmnt>tNewPrem*20)
	  {
	  	alert("期缴保费不能小于保额的20分之一");
	  	var minPrem = tCurAmnt/20;
	  	fm.UliInfo.value = "期缴保费不能小于" + minPrem + "元";
	    divUliInfo.style.display="";
	    return false;  			
	  }
	  var SQL331701 =" select insuredno from lcpol where riskcode='331701' and contno='"+fm.all('ContNo').value+"'";
	  var arrResult = easyExecSql(SQL331701);
	  //331701--金利宝 单独校验
	  if (arrResult != null) 
	  {
	     //该被保人下所有保单的331701的保费之和不能高于6000元
	     var SQLSumPrem="select nvl(sum(prem),0) from lcpol where riskcode='331701' and appflag='1' and insuredno='"+arrResult[0][0]+"' and contno!='"+fm.all('ContNo').value+"'";
	     var arrResultSumPrem = easyExecSql(SQLSumPrem);
	     if (tNewPrem < 1000)
	     {
	  	   alert("金利宝调整后的基本保费不能低于1000元");
	       return false;
	     } 
	     else if (parseInt(tNewPrem)+parseInt(arrResultSumPrem[0][0]) > 6000)
	     {
	  	   alert(arrResult[0][0]+"号被保人下所有金利宝险种的基本保费不能高于6000元");
	       return false;
	     } 
	        
	  }else
	  {
		 if (tNewPrem < 3000)
	     {
	  	   alert("调整后的基本保费不能低于3000元");
	       return false;
	     } 
	  }
  }

  return true;
}

function initPolInfo()
{
  try
  {  
    var tContNo=fm.all('ContNo').value;
	var tSQL = "select Amnt,CValiDate,prem,polno,riskcode  "
	         + "from lcpol a where ContNo='"+tContNo+"' and polno=mainpolno "
	         +"and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') "; 
	turnPagePrem.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
	turnPagePrem.arrDataCacheSet = decodeEasyQueryResult(turnPagePrem.strQueryResult);
	fm.all('Amnt').value =turnPagePrem.arrDataCacheSet[0][0];
    fm.all('CvaliDate').value = turnPagePrem.arrDataCacheSet[0][1];      
    fm.all('Prem').value = turnPagePrem.arrDataCacheSet[0][2];   
    fm.all('PolNo').value = turnPagePrem.arrDataCacheSet[0][3];  
    fm.all('RiskCode').value = turnPagePrem.arrDataCacheSet[0][4]; 
  }
  catch(re)
  {
    alert("初始化险种信息错误!");
  }
}

function initPayState()
{
  try
  {
  	var tPolStateSQL="select (select a.codename from ldcode a where a.codetype='paystate' and a.code=value(p.StandbyFlag1,'0') ), "
  	                +"p.paytodate,p.payintv from  lcpol p where  p.ContNo='"+fm.all('ContNo').value+"' and polno=mainpolno "
  	                +"and exists (select 1 from lmriskapp where riskcode = p.riskcode and risktype4 = '4') ";     
    turnPagePol.strQueryResult  = easyQueryVer3(tPolStateSQL, 1, 0, 1);
	turnPagePol.arrDataCacheSet = decodeEasyQueryResult(turnPagePol.strQueryResult);
	fm.all('PayState').value =turnPagePol.arrDataCacheSet[0][0];  
	fm.all('PayToDate').value =turnPagePol.arrDataCacheSet[0][1]; 
  }
  catch(re)
  {
    alert("初始化缴费信息错误!");
  }	 
}
function initNewPrem()
{
  try
  {
  	var tPolStateSQL="select coalesce((select EdorValue from LPEdorEspecialData where EdorAcceptNo='"+fm.all('EdorNo').value+"' and edortype='"+ fm.all('EdorType').value+"' and detailtype = 'NEWPREM'),'') from dual";     
    turnPagePolState.strQueryResult  = easyQueryVer3(tPolStateSQL, 1, 0, 1);
	turnPagePolState.arrDataCacheSet = decodeEasyQueryResult(turnPagePolState.strQueryResult);
	if(turnPagePolState.arrDataCacheSet[0][0]==null && turnPagePolState.arrDataCacheSet[0][0]=='')
	{
	  fm.all('NewPrem').value=0;
	}
	else
	{
	  fm.all('NewPrem').value =turnPagePolState.arrDataCacheSet[0][0];   
	}
  }
  catch(re)
  {
    alert("初始化保费调整信息错误!");
  }	 
}

function isLoanState()
{
  var tContNo=fm.all('ContNo').value;
  var SQL="select Paytodate,CValiDate,PolNo from  lcpol a where  ContNo='"+tContNo+"' and polno=mainpolno "
         +"and exists (select 1 from lmriskapp where riskcode = a.riskcode and risktype4 = '4') ";      
  var result = easyExecSql(SQL);
      		                                                    
  var tPayToDate = result[0][0];                            
  var tCvaliDate = result[0][1];                            
  var tCurrDate=getCurrentDate(); 
  var tEdorValiDate = fm.all('EdorValiDate').value;
                                                            
  var tDayDiff1=dateDiff(tPayToDate,tCurrDate,"D");          
  if(tDayDiff1>0)                                            
  {	                                                        
  	alert("该保单有保费未缴纳，不能申请万能保费调整!");                           
  	return false;    		                                    
  }	                                                        

  var tPolNo = result[0][2];
  var sql1 = "select lastpaytodate,curpaytodate from ljapayperson where polno = '"+tPolNo+"' order by curpaytodate desc ";
  var result1 = easyExecSql(sql1);
  var tLastPayToDate = result1[0][0];                            
  var tCurPayToDate = result1[0][1];
  var tDayDiff2=dateDiff(tLastPayToDate,tEdorValiDate,"D");          
  var tDayDiff3=dateDiff(tEdorValiDate,tCurPayToDate,"D");
  if(tDayDiff2<0)                                            
  {	                                                        
  	alert("保全生效日期不能早于保单上次缴费日期！");                           
  	return false;    		                                    
  }	                                                        
  if(tDayDiff3<0)                                           
  {                                                         
  	alert("保全生效日期不能晚于保单本次交至日期！");
  	return false;    	                                      
  }   
  
  var sql2 = "select count(1) from lpedoritem a where contno = '"+tContNo+"' and edortype = 'BP' "
           + "and edorvalidate >= '"+tLastPayToDate+"' and edorvalidate<='" +tCurPayToDate+ "' "
           + "and exists (select 1 from lpedorapp where edoracceptno = a.edoracceptno and edorstate = '0') ";
  var result2 = easyExecSql(sql2);
  if("0"!=result2[0][0])
  {
    alert("该保单本保单年度已经做过期交保费变更，不能再次申请。");
  	return false;  
  }
  return true;
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

