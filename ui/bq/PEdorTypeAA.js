//该文件中包含客户端需要处理的函数和事件
var showInfo;
var pEdorFlag = true;                        //用于实时刷新处理过的数据的列表

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();         //使用翻页功能，必须建立为全局变量
window.onfocus = initFocus;                  //重定义获取焦点处理事件
var arrList1 = new Array();                  //选择的记录列表

/**
 * 初始化
 */
function initQuery1() {
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面1";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  	
	var strSql = "select  ContNo, PolNo, DutyCode, InsuYear, EndDate, Amnt, Prem  from LPDuty where"
		   +" EdorNo='"+fm.all('EdorNo').value+"' and EdorType='"+fm.all('EdorType').value+"'"
		   +" and ContNo='"+fm.all('ContNo').value+"' and PolNo='"+fm.all('PolNo').value+"'";       
    var arrResult = easyExecSql(strSql, 1, 0);  
    if (arrResult == null) 
     {   
     	        	 	 
        var strSql = "select  ContNo, PolNo, DutyCode, InsuYear, EndDate, Amnt, Prem  from LCDuty where"
	       +" ContNo='"+fm.all('ContNo').value+"' and PolNo='"+fm.all('PolNo').value+"'";       
        arrResult = easyExecSql(strSql, 1, 0);
        if (arrResult == null) 
        {
          alert("C无责任信息");     
        }
        else 
        {  
                                    
          turnPage.queryModal(strSql, LCDutyGrid);  
        }
      } 
    else 
     {  
     	alert("P责任信息");  
       turnPage.queryModal(strSql, LCDutyGrid);           
     }   	
	showInfo.close();	 
}

function initQuery2() {
  var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面2";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	var strSql = "select  ContNo, PolNo, DutyCode, InsuYear, EndDate, Amnt, Prem  from LPDuty where"
		  +" EdorNo='"+fm.all('EdorNo').value+"' and EdorType='"+fm.all('EdorType').value+"'"
		  +" and ContNo='"+fm.all('ContNo').value+"' and PolNo='"+fm.all('PolNo').value+"'";       
	turnPage2.queryModal(strSql, LPDutyGrid); 	
	showInfo.close();	 
}

/**
 * 单击MultiLine的复选按钮时操作
 */
function reportDetailClick(parm1, parm2) {	
  if (fm.all(parm1).all('LCDutyGridSel').checked) {
    arrList1[fm.all(parm1).all('LCDutyGridNo').value] = fm.all(parm1).all('LCDutyGrid1').value;
  }
  else {
    arrList1[fm.all(parm1).all('LCDutyGridNo').value] = null;
  }   
    var tprem=fm.all(parm1).all('LCDutyGrid6').value;
    fm.all('Prem_bak').value=tprem; 
     if(fm.all('GetMoney').value==null || fm.all('GetMoney').value=='')
    {
       	fm.all('GetMoney').value='0';
    }
       	
    var a=parseFloat(fm.all('GetMoney').value);
    var b=parseFloat(fm.all('Prem_bak').value);
    fm.all('GetMoneyPro').value=mathRound(a/b*100);
    
   
}
/**
 * 进入多个人保全
 */
function PEdorSave() {
	  //校验输入数据
	  if(fm.all('GetMoney').value =='0'){
	  	alert("请输入增额或增额比例");
	 		return;	
	  }
	  //校验是否选择
    var i = 0;
    var chkFlag=false;
    for (i=0;i<LCDutyGrid.mulLineCount;i++ )
    {
        if (LCDutyGrid.getSelNo(i))
        {
          chkFlag=true;
          break;
        }
        
    }
    if (chkFlag)
    {
        var showStr = "正在申请集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.all("Transact").value = "INSERT||EDOR";
        fm.submit();
    }
    else
    {
        alert("请选择需要操作的记录!");
    }   
  }

/**
 * 提交后操作,服务器数据返回后执行的操作
 */
function afterSubmit(FlagStr, content, Result) 
{
  showInfo.close();
  if (FlagStr == "Fail") 
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else 
  { 
    pEdorFlag = true;    
    if (fm.Transact.value=="INSERT||EDOR") {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   //   initQuery1();
      initQuery2();  
    }   
  }
}

/**
 * 处理获取焦点事件
 */
function initFocus() {
  if (pEdorFlag) {   
    pEdorFlag = false;      
    }
}

/**
 * 单击MultiLine的单选按钮时操作
 */
function reportDetail2Click(parm1, parm2) {	
  if (fm.all(parm1).all('LPDutyGridSel').value=='on' || fm.all(parm1).all('LPDutyGridSel').value=='1') {
    fm.ContNo.value = fm.all(parm1).all('LPDutyGrid1').value;
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent() {
	top.opener.getEdorItem();
	top.close();
}

function changeValue1(){  	
      
        var a=parseFloat(fm.all('GetMoney').value);
        var b=parseFloat(fm.all('Prem_bak').value);
        fm.all('GetMoneyPro').value=mathRound(a/b*100);  
        
}
function checknum(){
	if(!checkNumber(GetMoney))
	return;
}
/*********************************************************************
 *  改变退费（按比例）文本框中的值
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 
function changeValue2(){ 
     
         var a=parseFloat(fm.all('GetMoneyPro').value);
         var b=parseFloat(fm.all('Prem_bak').value);
       	 fm.all('GetMoney').value=mathRound(a*b/100);
       	 
}