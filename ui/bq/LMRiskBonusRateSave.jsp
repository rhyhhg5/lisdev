
<%
	//程序名称：LDBonusInterestRateSave.jsp
	//程序功能：
	//创建日期：2012-8-30
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息

	CErrors tError = null;
	//后面要执行的动作：添加，修改
	String FlagStr = "";
	String Content = "";
	String transact = "";
	
	//执行动作：insert 添加纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");
	transact = request.getParameter("fmtransact");
	String RiskCode = request.getParameter("RiskCode");
	String BonusYear = request.getParameter("BonusYear");
	String AppYear = request.getParameter("AppYear");
	String AppYearAfter = request.getParameter("AppYearAfter");
	String InsuYears = request.getParameter("InsuYears");
	String PayYears = request.getParameter("PayYears");
	String HLRate = request.getParameter("HLRate");
	String MinAge = request.getParameter("MinAge");
	String MaxAge = request.getParameter("MaxAge");
	
	
	LMRiskBonusRateSchema tLMRiskBonusRateSchema = new LMRiskBonusRateSchema();
	tLMRiskBonusRateSchema.setRiskCode(RiskCode);
	tLMRiskBonusRateSchema.setBonusYear(BonusYear);
	tLMRiskBonusRateSchema.setAppYear(AppYear);
	tLMRiskBonusRateSchema.setInsuYears(InsuYears);
	tLMRiskBonusRateSchema.setPayYears(PayYears);
	tLMRiskBonusRateSchema.setHLRate(HLRate);
	tLMRiskBonusRateSchema.setMaxAge(MaxAge);
	tLMRiskBonusRateSchema.setMinAge(MinAge);
	
	LMRiskBonusRateSchema dLMRiskBonusRateSchema = new LMRiskBonusRateSchema();
	dLMRiskBonusRateSchema.setAppYear(AppYearAfter);
	LMRiskBonusRateUI tLMRiskBonusRateUI = new LMRiskBonusRateUI();
	try{
		// 准备传输数据 VData	
		VData tVData = new VData();
		TransferData tTransferData = new TransferData();
		tVData.add(tGlobalInput);
		tTransferData.setNameAndValue("tLMRiskBonusRateSchema",tLMRiskBonusRateSchema);
		tTransferData.setNameAndValue("dLMRiskBonusRateSchema",dLMRiskBonusRateSchema);
		tVData.add(tTransferData);
		tLMRiskBonusRateUI.submitData(tVData,transact);
	}catch(Exception ex){
		Content = "操作失败，原因是:" + ex.toString();
    	FlagStr = "Fail";
	}
	if (FlagStr == "")
  { 
    tError = tLMRiskBonusRateUI.mErrors;
    if (!tError.needDealError())
    {                        

      Content = "操作成功! ";
      FlagStr = "Success";
    }
    else                                                                           
    {
      Content = "操作失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=transact%>");
</script>
</html>

