
<%
	//程序名称：
	//程序功能：个人保全
	//创建日期：2011-11-23
	//创建人  ：ZT
	//更新记录：  更新人    更新日期     更新原因/内容

%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">  

function initRateBox()
{
  try
  {
	fm.all('RiskCode').value = "";
	fm.all('RiskName').value = "";
    fm.all('CanLoanRate').value = "";
    fm.all('aManageCom').value = "";
    fm.all('ManageComName').value = "";
    fm.all('MinPremLimit').value = "";
    fm.all('MaxPremLimit').value = "";

	  //选中后只能修改【利率终止日期】字段
	  fm.all("aManageCom").disabled = false;
	  fm.ManageComName.disabled = false;
	  fm.all("RiskCode").disabled = false;
	  fm.MinPremLimit.disabled = false;
	  fm.MaxPremLimit.disabled = false;
	  fm.all("CanLoanRate").disabled = false;
	  fm.RiskName.disabled = false;
  }
  catch(ex)
  {
    alert("在LMinterestRateInit.jsp-->InitRateBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
   try
   { 
	//initRateBox();
    initLMCanloanComGrid(); 
    initElementtype();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}


function initLMCanloanComGrid()
{
    var iArray = new Array();

      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
        
        iArray[1]=new Array();
        iArray[1][0]="机构代码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=3;
        
        iArray[2]=new Array();
        iArray[2][0]="管理机构";
        iArray[2][1]="100px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="险种代码";
        iArray[3][1]="100px";
        iArray[3][2]=100;
        iArray[3][3]=3;

        iArray[4]=new Array();
        iArray[4][0]="险种名称";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;
        
        iArray[5]=new Array();
        iArray[5][0]="贷款额度比例";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="最小保费金额";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="最大保费金额";
        iArray[7][1]="100px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
      LMCanloanComGrid = new MulLineEnter("fm" ,"LMCanloanComGrid" ); 
      //这些属性必须在loadMulLine前
      LMCanloanComGrid.mulLineCount = 10;   
      LMCanloanComGrid.displayTitle = 1;
      LMCanloanComGrid.locked = 1;
      LMCanloanComGrid.canSel=1;
      LMCanloanComGrid.canChk = 0;
      LMCanloanComGrid.hiddenSubtraction=1;
      LMCanloanComGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      LMCanloanComGrid.selBoxEventFuncName="ShowDetail";
      LMCanloanComGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert("在LMCanloanComSetInit.jsp-->initLMCanloanComGrid函数中发生异常:初始化界面错误!");
      }
}

</script>
