//GEdorTypeWZ.js
var showInfo;

var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//初始化保单信息
function initGrpContInfo()
{
  var sql = "  select appntNo, grpName, handlerDate, cValiDate, "
            + "   (select min(payToDate) "
            + "   from LCCont "
            + "   where grpContNo = a.grpContNo), "
            + "   peoples2, prem, "
            + "   (select AccBala "
            + "   from LCAppAcc "
            + "   where customerNo = a.appntNo), "
            + "   (select name "
            + "   from LDCom "
            + "   where comCode = a.signCom) "
            + "from LCGrpCont a "
            + "where grpContNo = '" + fm.GrpContNo.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    fm.AppntNo.value = result[0][0];
    fm.GrpName.value = result[0][1];
    fm.HandlerDate.value = result[0][2];
    fm.CValiDate.value = result[0][3];
    fm.PayToDate.value = result[0][4];
    fm.Peoples2.value = result[0][5];
    fm.Prem.value = result[0][6];
    fm.AccBala.value = result[0][7];
    fm.SignCom.value = result[0][8];
    
    if(fm.AccBala.value == null || fm.AccBala.value == "" || fm.AccBala.value == "null")
    {
      fm.AccBala.value = "0";
    }
  }
  else
  {
    alert("没有查询保单号为" + fm.GrpContNo.value + "的保单信息");
  }
}

//查询保障计划
function queryLCContPlan()
{
  var sql = "  select distinct a.contPlanCode, a.contPlanName, a.peoples2, "
            + "   (select sum(prem) "
            + "   from LCPol "
            + "   where grpContNo = a.grpcontNo "
            + "       and contPlanCode = a.contPlanCode "
            + "       and polTypeFlag = '1' ), "
            + "   (select sum(amnt) "
            + "   from LCPol "
            + "   where grpContNo = a.grpContNo "
            + "       and contPlanCode = a.contPlanCode "
            + "       and polTypeFlag = '1' ), b.contNo, b.insuredNo "
            + "from LCContPlan a , LCInsured b, LCCont c "
            + "where a.grpContNo = b.grpContNo "
            + "   and a.contPlanCode = b.contPlanCode "
            + "   and b.contNo = c.contNo "
            + "   and c.polType = '1' "  //无名单
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + "   and a.contPlanCode != '11' ";
  turnPage.pageLineNum = 50;
	turnPage.queryModal(sql, LCContPlan);
}

//查询保障计划的险种信息
function queryLCContPlanRisk()
{
  var rows = LCContPlan.getSelNo() - 1;
  var contPlanCode = LCContPlan.getRowColDataByName(rows, "contPlanCode");
  var contNo = LCContPlan.getRowColDataByName(rows, "contNo");
  
  if(contPlanCode == null || contPlanCode == "")
  {
    alert("请选择保障计划信息");
    return false;
  }
  fm.ContPlanCode.value = contPlanCode;
  fm.ContNo.value = contNo;
  
  var sql = "  select a.contPlanCode, a.contPlanName, a.riskCode, b.riskName "
            + "from LCContPlanRisk a, LMRisk b "
            + "where a.riskCode = b.riskCode "
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + "   and a.contPlanCode = '" + contPlanCode + "' ";
  turnPage2.pageLineNum = 50;
	turnPage2.queryModal(sql, LCContPlanRisk);
	
	//查询本期应交日期和交至日期
	sql = "  select distinct lastPayToDate, curPayToDate, payCount "
	      + "from LJAPayPerson "
	      + "where contNo = '" + contNo + "' "
	      + "order by curpaytodate desc ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.LastPayToDate.value = rs[0][0];
    fm.CurPayToDate.value = rs[0][1];
  }
	
	showContPlanWZInfo(contPlanCode);
}

//显示录入的保障计划增人信息
function showContPlanWZInfo(contPlanCode)
{
  var sql = "  select noNamePeoples, a.edorPrem, a.edorValiDate "
            + "from LCInsuredList a, LPCont b "
            + "where a.contNo = b.contNo "
            + "   and a.edorNo = b.edorNo "
            + "   and a.contPlanCode = '" + fm.ContPlanCode.value + "' "
            + "   and a.grpContNo = '" + fm.GrpContNo.value + "' "
            + "   and a.edorNo = '" + fm.EdorNo.value + "' "
            + "   and a.contNo = '" + fm.ContNo.value + "' "
            + "   and b.edorType = '" + fm.EdorType.value + "' ";
            
  var turnPage3 = new turnPageClass();
  var result = easyExecSql(sql);
  if(result)
  {
    fm.Peoples2WZ.value = result[0][0];
    fm.SumPremWZ.value = result[0][1];
    fm.EdorValiDate.value = result[0][2];
    fm.PremWZ.value = pointTwo(parseFloat(fm.SumPremWZ.value) / parseInt(fm.Peoples2WZ.value));
  }
  //若为录入，计算出保障计划下的人均保费
  else
  {
    try
    {
      var rows = LCContPlan.getSelNo() - 1;
      var sumPrem = LCContPlan.getRowColDataByName(rows, "prem");  //保障计划总保费
      var peoples2 = LCContPlan.getRowColDataByName(rows, "peoples2");  //保障计划总人数
      
      fm.PremWZ.value = pointTwo(parseFloat(sumPrem) / parseInt(peoples2));
      fm.AvgPrem.value = fm.PremWZ.value;
      fm.Peoples2WZ.value = "";
      fm.SumPremWZ.value = "";
    }
    catch(ex)
    {}
  }
}

//响应人均保费变更事件
//若人数不为空，则本次应收保费=本次应收保费*人数
function onPremWZChange()
{
  var premWZ = fm.PremWZ.value;
  var peoples2WZ = fm.Peoples2WZ.value;
  
  if(!isNumeric(premWZ))
  {
    alert("人均期交保费必须为非负数");
    fm.PremWZ.value = "";
    return false;
  }
  
  if(peoples2WZ != "" && isInteger(peoples2WZ))
  {
    var sumPremWZ = pointTwo(parseFloat(premWZ) * parseInt(peoples2WZ));
    fm.SumPremWZ.value = sumPremWZ;
  }
}

//响应新增被保人数变更事件
//若本次应收保费不为空，则人均期交保费=本次应收保费/人数
//若本次应收保费为空，且人均期交保费不为空，则本次应收保费=本次应收保费*人数
function onPeoples2WZChange()
{
  var premWZ = fm.PremWZ.value;
  var peoples2WZ = fm.Peoples2WZ.value;
  var sumPremWZ = fm.SumPremWZ.value;
  
  if(!isInteger(peoples2WZ) || parseInt(peoples2WZ) == 0)
  {
    alert("增加被保险人数应为正整数");
    fm.Peoples2WZ.value = "";
    return false;
  }
  
  //若本次应收保费不为空，则人均期交保费=本次应收保费/人数
  if(isNumeric(sumPremWZ))
  {
    fm.PremWZ.value = pointTwo(parseFloat(sumPremWZ) / parseInt(peoples2WZ));
  }
  //若本次应收保费为空，则本次应收保费=本次应收保费*人数
  else if(isNumeric(premWZ))
  {
    var sumPremWZ = pointTwo(parseFloat(premWZ) * parseInt(peoples2WZ));
    fm.SumPremWZ.value = sumPremWZ;
  }
}

//响应本次应收保费变更事件
//若人数存在，则自动计算人均期交保费
function onSumPremWZChange()
{
  var peoples2WZ = fm.Peoples2WZ.value;
  var sumPremWZ = fm.SumPremWZ.value;
  
  if(!isNumeric(sumPremWZ))
  {
    alert("本次应收保费应为非负数");
    fm.SumPremWZ.value = "";
    return false;
  }
  if(isInteger(peoples2WZ))
  {
   fm.PremWZ.value = pointTwo(parseFloat(sumPremWZ) / parseInt(peoples2WZ));
  }
}

//响应生效日期 变更事件
//本次增人生效日期必须在保单生效日和交至日之间
function onEdorValiDateChange()
{
  var edorValiDate = fm.EdorValiDate.value;
  if(!isDate(edorValiDate))
  {
    alert("生效日期必须为日期格式'yyyy-mm-dd'");
    fm.EdorValiDate.value = currentDate;
    return false;
  }
  var sql = "  select 1 "
            + "from dual "
            + "where days('" + fm.LastPayToDate.value + "') > days('" + edorValiDate + "') "
            + "   or days('" + fm.CurPayToDate.value + "') < days('" + edorValiDate + "') ";
  var turnPage3 = new turnPageClass();
  var result = easyExecSql(sql, null, null, null, null, null, turnPage3);
  if(result)
  {
    alert("本次增人生效日期必须在本交费期的应交日期和交至日之间。");
    fm.EdorValiDate.value = currentDate;
    return false;
  }
  return true;
}

//提交时:
//人均期交保费不能为空，必须>=0
//被保人数被保人数不能为空，必须>0
//总保费不能为空,必须>=0
//生效日必须在保单生效日和交至日之间
function checkAllInput()
{
  var premWZ = fm.PremWZ.value;  //人均期交保费
  var peoples2WZ = fm.Peoples2WZ.value;  //总人数
  var sumPremWZ = fm.SumPremWZ.value;  //总保费
  
  if(!isNumeric(premWZ) || !isNumeric(sumPremWZ))
  {
    alert("人均期交保费、增加被保险人数、本次应收保费均应为非负数");
    return false;
  }
  if(!isInteger(peoples2WZ) || parseFloat(peoples2WZ) == 0)
  {
    alert("增加被保险人数应为正整数");
    return false;
  }
  if(!onEdorValiDateChange())
  {
    return false;
  }
  if(premWZ != fm.AvgPrem.value)
  {
    return window.confirm("您录入的人均期交保费与该保障计划下人均期交保费不等，要继续操作么?");
  }
  
  return true;
}

//提交操作
function submitData()
{
  if(!checkAllInput())
  {
    return false;
  }
  
  controlSubmitButton(true);
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//控制保存按钮的显示与否
function controlSubmitButton(flag)
{
  document.all.submitButton.disabled=flag;
}

function afterSubmit(flag, content)
{
  controlSubmitButton(false);
  
  showInfo.close(); 
  window.focus();
    
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  if(flag == "Succ")
  {
    top.opener.getGrpEdorItem();
  }
}

//撤销录入
function cancel()
{
  fm.PremWZ.value = "";
  fm.Peoples2WZ.value = "";
  fm.SumPremWZ.value = "";
  fm.EdorValiDate.value = currentDate;
}

function returnParent()
{
  try
  {
    top.close();
    top.opener.focus();
  }
  catch(ex)
  {
    alert(ex.message);
  }
  
}

function getLCContPlan()
{
  var win = window.open("../sys/ContPlan.jsp?GrpContNo=" + fm.GrpContNo.value + "&LoadFlag=1&ContType=1&LoadFrame=TASK", "contPlan");
  win.focus();
}