<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeIA.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeIAInit.jsp"%>
  
  <title>投保人变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeIASubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>
     
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=PolNo>
      </TD>   
    </TR>
  </TABLE> 

   <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPAppntInd);">
      </td>
      <td class= titleImg>
        投保人信息
      </td>
   </tr>
   </table>
	 
    <Div  id= "divLPAppntInd" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCAppntIndGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
    
    <div id= "divPersonQuery" style= "display: none">
	  <table>
	   <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
	      </td>
	      <td class= titleImg>
	        变更投保人查询
	      </td>
	   </tr>
	   </table>
	   <Div id= "divQuery" style = "display: ''">
	     <table class= common >
	     <tr class = common>
	         <td class= title>
	            客户号码
	         </td>
	         <td class= input>
	            <Input readonly class= common  name=QueryCustomerNo >
	         </td>
	         <td class= input>
	            <Input type = hidden readonly class= common   >
	         </td>

	     </tr>
	     <tr class = common>
	         <td >
	         <Input class= common type=Button value="查 询" onclick="personQuery()">
	         </td>
	         <td >
	         <Input class= common type=Button value="新 增" onclick="personNew()"> 
	         </td>

	     </tr>
<!--	     <tr>
	     <TD>
	     <Input class= common type=Button value="确定" onclick="PersonSelect()">       
	     </td>
	     </tr>
-->	
	     </table>
	   </div>
    </div>
    
       
 	<Div  id= "divLPAppntIndDetail" style= "display: none">
		<TABLE class=common>
		
		  <TR CLASS=common>		    
		    <TD CLASS=title>
		      客户号 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntCustomerNo VALUE="" CLASS=readonly MAXLENGTH=20 >
		    </TD>
		    
		    <TD CLASS=title>
		      姓名 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntName VALUE="" CLASS=readonly MAXLENGTH=20 verify="投保人姓名|notnull" >
		    </TD>
		    <TD CLASS=title>
		      性别 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntSex VALUE="" MAXLENGTH=1 CLASS=readonly  >
		    </TD>
		    </TR>
		    <TR class=common>
		    <TD CLASS=title>
		      出生日期 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntBirthday VALUE="" CLASS=readonly verify="投保人出生日期|notnull&date" >
		    </TD>
		  
		    <TD CLASS=title>
		      与被保人关系 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input NAME=AppntRelationToInsured VALUE="" MAXLENGTH=2 CLASS="code" ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);" verify="投保人与被保险人关系|code:Relation&notnull" >
		    </TD>
		    <TD CLASS=title>
		      证件类型 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntIDType VALUE="" MAXLENGTH=1 CLASS=readonly >
		    </TD>
		    </TR>
		    <TR class= common>
		    
		    <TD CLASS=title>
		      证件号码 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntIDNo VALUE="" CLASS=readonly MAXLENGTH=20 >
		    </TD>
		 
		    <TD CLASS=title>
		      国籍
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntNativePlace CLASS=readonly VALUE="" CLASS=readonly >
		    </TD>
		    <TD CLASS=title>
		      户籍所在地 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntRgtAddress VALUE="" CLASS=readonly MAXLENGTH=80 >
		    </TD>
		    </TR>
		    
		  <TR CLASS=common>
		    <TD CLASS=title>
		      通讯地址
		    </TD>
		    <TD CLASS=input COLSPAN=3>
		      <Input NAME=AppntPostalAddress VALUE="" CLASS=common3 MAXLENGTH=80 >
		    </TD>
		    <TD CLASS=title>
		      通讯地址邮政编码 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input NAME=AppntZipCode VALUE="" CLASS=common MAXLENGTH=6 verify="投保人邮政编码|zipcode" >
		    </TD>
		  </TR>
		
		  <TR CLASS=common>
		    <TD CLASS=title>
		      住址
		    </TD>
		    <TD CLASS=input COLSPAN=3>
		      <Input readonly NAME=AppntHomeAddress VALUE="" CLASS=readonly MAXLENGTH=80 >
		    </TD>
		    <TD CLASS=title>
		      住址邮政编码 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntHomeZipCode VALUE="" CLASS=readonly MAXLENGTH=6 verify="投保人住址邮政编码|zipcode" >
		    </TD>
		  </TR>
		
		  <TR CLASS=common>
		    <TD CLASS=title>
		      联系电话（1）
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input NAME=AppntPhone VALUE="" CLASS=common MAXLENGTH=18 >
		    </TD>
		    <TD CLASS=title>
		      联系电话（2）
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input NAME=AppntPhone2 VALUE="" CLASS=common MAXLENGTH=18 >
		    </TD>
		    <TD CLASS=title>
		      移动电话
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input NAME=AppntMobile VALUE="" CLASS=common MAXLENGTH=15 >
		    </TD>
		  </TR>
		
		  <TR CLASS=common>
		    <TD CLASS=title>
		      电子邮箱
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntEMail VALUE="" CLASS=readonly MAXLENGTH=20 >
		    </TD>
		    <TD CLASS=title>
		      工作单位
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntGrpName VALUE="" CLASS=readonly MAXLENGTH=60 >
		    </TD>
		    <TD CLASS=title>
		      职业（工种） 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntWorkType VALUE="" CLASS=readonly MAXLENGTH=10 >
		    </TD>
		  </TR>
		
		  <TR CLASS=common>
		    <TD CLASS=title>
		      兼职（工种） 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntPluralityType VALUE="" CLASS=readonly MAXLENGTH=10 >
		    </TD>
		    <TD CLASS=title>
		      职业代码 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntOccupationCode VALUE="" MAXLENGTH=10 CLASS=readonly >
		    </TD>
		    <TD CLASS=title>
		      职业类别 
		    </TD>
		    <TD CLASS=input COLSPAN=1>
		      <Input readonly NAME=AppntOccupationType VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=10 verify="被保人职业类别|code:OccupationType" >
		    </TD>
		  </TR>
		
		  <TR CLASS=common>
		  </TR>
		
	  </TABLE>
		
		
      <table class = common>
			<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeIASave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="取消" onclick="edorTypeIAReturn()">
     	 </TD>
     	 </TR>
     	</table>
    </Div>
	</Div>
	
	<hr>
	
	  <Input type=Button class= common value="返回" onclick="returnParent()">
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
