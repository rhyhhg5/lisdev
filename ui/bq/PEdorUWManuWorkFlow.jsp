<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorUWManuWorkFlow.jsp
//程序功能：保全人工核保创建初始节点
//创建日期：2005-09-20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.tb.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  String flag = "";
  String content = "";
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
  
	String tMissionID = request.getParameter("MissionId");
  String tSubMissionID = request.getParameter("SubMissionId");
  String tContNo = request.getParameter("ContNo");
  String tPrtNo = request.getParameter("PrtNo");
  String tActivityID = request.getParameter("ActivityId");

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("ContNo", tContNo);
	tTransferData.setNameAndValue("PrtNo", tPrtNo);
	tTransferData.setNameAndValue("MissionID", tMissionID);
	tTransferData.setNameAndValue("SubMissionID", tSubMissionID);
	
	System.out.println("ActivityID:" + tActivityID);

	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tTransferData);
	TbWorkFlowUI tTbWorkFlowUI = new TbWorkFlowUI();
	if (tTbWorkFlowUI.submitData(tVData, tActivityID) == false)
	{
		flag = "Fail";
		content = tTbWorkFlowUI.mErrors.getFirstError();
		System.out.println("content" + content);
	}
%>                      
<html>
<script language="javascript">
	
</script>
</html>
