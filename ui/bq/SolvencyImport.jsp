<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DiskImport.jsp
//程序功能：磁盘导入
//创建日期：2005-12-13
//创建人 ：QiuYang
//更新记录： 更新人  更新日期   更新原因/内容
%>
<html>
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>磁盘导入</title>
<script>

  //磁盘导入
  function importFile()
  {

  	document.all("Info").innerText = "上传文件导入中，请稍后...";
  	fm.submit();
  }
  
  function afterSubmit(flag, content)
  {
  	document.all("Info").innerHTML = content;
  	if (flag == "Succ")
  	{
  		top.opener.initForm();
  		top.close();
  	}
	}
	
	function initForm()
	{
		fm.all("tYear").value = "<%=request.getParameter("tYear")%>";
		fm.all("tQuarter").value = "<%=request.getParameter("tQuarter")%>";
		fm.all("tSolvency").value = "<%=request.getParameter("tSolvency")%>";
		fm.all("tFlag").value = "<%=request.getParameter("tFlag")%>";
	}
</script>
</head>
<body >
<span id='show'></span>
	<form name="fm" action="./SolvencyImportSave.jsp"  method=post target="fraSubmit" ENCTYPE="multipart/form-data">
		<br><br>
		<table class=common>
			<TR>
				<TD width='15%' style="font:9pt">文件名</TD>
				<TD width='85%'>
					<Input type="file" width="100%" name="FileName">
					<INPUT VALUE="导  入" class="cssButton" TYPE=button onclick="importFile();">
				</TD>
			</TR>
		</table>
		<br><br>
		<table class=common>
			<TR align="center">
				<TD id="Info" width='100%' style="font:10pt"></TD>
			</TR>
		</table>
	</form>
</body>
</html>