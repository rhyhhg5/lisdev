<%
//程序名称：LMInsuAccRateInit.jsp
//程序功能：万能险帐户结算利率录入界面初始化
//创建日期：2007-12-12
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!--引入必要的java类  -->
<%@page import="com.sinosoft.lis.pubfun.*"%>	
<% 
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var operator = "<%=tGI.Operator%>"; //记录当前登录人
  var tCurrentDate = "<%=tCurrentDate%>";
  var tCurrentTime = "<%=tCurrentTime%>";
 
//输入框的初始化 
function initInpBox()
{
  try
  {
	fm.all('RiskCode').value = "";
    fm.all('RiskName').value = "";
    fm.all('InsuAccNo').value = "";
    fm.all('InsuAccName').value = "";
    fm.all('BalaMonth').value = "";
    fm.all('RateType').value = "";
    fm.all('RateTypeName').value = "";
    fm.all('RateIntv').value = "";
    fm.all('RateIntvUnit').value = "";
    fm.all('RateIntvUnitName').value = "";
    fm.all('Rate').value = "";
    fm.all('RateConfirm').value = "";
  }
  catch(ex)
  {
    alert("在LMInsuAccRateInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();
    initLMInsuAccRateGrid(); 
    initElementtype();
  }
  catch(re) 
  {
    alert("在LMInsuAccRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//工单列表Mulline的初始化
var LMInsuAccRateGrid;
function initLMInsuAccRateGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=3;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="险种编码";      //列名RiskCode
    iArray[1][1]="80px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=3;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="险种名称";      //列名RiskCode
    iArray[2][1]="250px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[3]=new Array();
    iArray[3][0]="保险帐户号码";      //列名InsuAccNo
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=3;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="保险帐户名称";      //列名InsuAccNo
    iArray[4][1]="120px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[5]=new Array();
    iArray[5][0]="结算月份";      //列名BalaMonth
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[6]=new Array();
    iArray[6][0]="利率类型";      //列名RateType
    iArray[6][1]="80px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=3;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="利率类型";      //列名RateType
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[8]=new Array();
    iArray[8][0]="利率间隔";      //列名RateIntv
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[9]=new Array();
    iArray[9][0]="利率间隔单位";      //列名RateIntvUnit
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=3;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="利率间隔单位";      //列名RateIntvUnit
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=200;           //列最大值
    iArray[10][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[11]=new Array();
    iArray[11][0]="利率";      //列名Rate
    iArray[11][1]="80px";        //列宽
    iArray[11][2]=200;           //列最大值
    iArray[11][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    LMInsuAccRateGrid = new MulLineEnter("fm", "LMInsuAccRateGrid"); 
	  //设置Grid属性
    LMInsuAccRateGrid.mulLineCount = 10;
    LMInsuAccRateGrid.displayTitle = 1;
    LMInsuAccRateGrid.locked = 1;
    LMInsuAccRateGrid.canSel = 1;	
    LMInsuAccRateGrid.canChk = 0;
    LMInsuAccRateGrid.hiddenSubtraction = 1;
    LMInsuAccRateGrid.hiddenPlus = 1;
    LMInsuAccRateGrid.selBoxEventFuncName = "selectOne" ;
    LMInsuAccRateGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在LMInsuAccRateInit.jsp-->initLMInsuAccRateGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
