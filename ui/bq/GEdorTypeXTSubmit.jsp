<%
//程序名称：GEdorTypeXTSubmit.jsp
//程序功能：
//创建日期：2007-2-1 11:40
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
  String FlagStr = "";
  String Content = "";
  String fmAction = request.getParameter("fmAction");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	
  String edorNo = request.getParameter("EdorNo");
  String edorType = request.getParameter("EdorType");
  
  LPGrpEdorItemSchema tLPGrpEdorItemSchema = new LPGrpEdorItemSchema();
	tLPGrpEdorItemSchema.setEdorNo(edorNo);
	tLPGrpEdorItemSchema.setEdorType(edorType);
	tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
	tLPGrpEdorItemSchema.setReasonCode(request.getParameter("reason_tb"));
	
	EdorItemSpecialData tEdorItemSpecialData = new EdorItemSpecialData(edorNo, edorType);
	
	String tChk[] = request.getParameterValues("InpLCGrpPolGridChk");
  if(tChk != null)
  {
    String[] contPlanCodes = request.getParameterValues("LCGrpPolGrid1");
    String[] riskCodes = request.getParameterValues("LCGrpPolGrid3");
    String[] tXTFee = request.getParameterValues("LCGrpPolGrid12");
    String[] tXTFeeRate = request.getParameterValues("LCGrpPolGrid13");
    for (int i = 0;i < tChk.length; i++)
    {
      //按照险种退保
      if (tChk[i].equals("1"))
      {
      	tEdorItemSpecialData.setGrpPolNo(contPlanCodes[i] + "," + riskCodes[i]);
      	tEdorItemSpecialData.add(BQ.DETAILTYPE_XTFEEG, "-" + String.valueOf(tXTFee[i]));
      	tEdorItemSpecialData.add(BQ.XTFEERATEG, tXTFeeRate[i]);
      }
    }
  }
	
  GrpEdorXTDetailUI tPGrpEdorXTDetailUI = new GrpEdorXTDetailUI();
  try
  {
    // 准备传输数据 VData
    VData tVData = new VData();  
    tVData.addElement(tG);
    tVData.addElement(tLPGrpEdorItemSchema);
    tVData.add(tEdorItemSpecialData);
    tPGrpEdorXTDetailUI.submitData(tVData,fmAction);
	}
	catch(Exception ex)
	{
		Content = fmAction+"失败，原因是:" + ex.toString();
        FlagStr = "Fail";
	}			
    //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    if (!tPGrpEdorXTDetailUI.mErrors.needDealError())
    {                          
      Content = " 保存成功";
  	  FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tPGrpEdorXTDetailUI.mErrors.getFirstError();
    	FlagStr = "Fail";
    }
  }
  

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

