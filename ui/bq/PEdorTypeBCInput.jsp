<%
/*
  受益人资料变更
  create : 0 孙  迪 2005-3-29 15:25
  modify : 1 
*/
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<% String TypeFlag = request.getParameter("TypeFlag");

System.out.println(TypeFlag);
%>
<script>
	var TypeFlag ='<%=TypeFlag%>';
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeBC.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEdorTypeBCInit.jsp"%>
  <title>受益人资料变更 </title> 
</head>
<body  onload="initForm();" > 
<form action="./PEdorTypeBCSubmit.jsp" method=post name=fm target="fraSubmit">
<input type="hidden" readonly name="InsuredNo">
<input type="hidden" readonly name="EdorAcceptNo">
<input type="hidden" readonly name="ContType">
<input type="hidden" readonly name="fmtransact">
<input type="hidden" readonly name="PolNo"> 
<input type="hidden" readonly name="grpsignal">
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
      <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" type="hidden" readonly name=EdorType>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>
    
  <Div id= "divPol" style= "display: ''">
    <table>
    <tr>
        <td> </td>
        <td class= titleImg>险种信息</td></tr>
    </table>
    <table  class= common>
    <tr  class= common>
        <td text-align: left colSpan=1 >
        <span id="spanPolGrid">
        </span>
        </td></tr>
    </table>
    <Div id= "divPage" style= "display: 'none' ">
      <INPUT VALUE="首  页" class="cssbutton" type="button" onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class="cssbutton" type="button" onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class="cssbutton" type="button" onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class="cssbutton" type="button" onclick="getLastPage();"> 	
    </Div>
  </Div>
<br>
<Div id= "divBnf" style= "display: ''">  
  <table>
  	<tr>
      <td><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCBnf);"></td>
      <td class= titleImg>受益人信息</td>
     </tr>
  </table>
    <Div id= "divLCBnf" style= "display: ''">   
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1>
            <span id="spanLCBnfGrid" >
            </span>
          </td>
        </tr>
      </table>
    </Div>			
</div>
<br>
<Input class="cssbutton" name="saveButton" type="button" value="保  存" onclick="SaveBnf()">
<Input class="cssbutton" name="returnButton" type="button" value="取  消" onclick="initForm()">
<Input class="cssbutton" name="returnButton" type="button" value="返  回" onclick="returnParent()">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
