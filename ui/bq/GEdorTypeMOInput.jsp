<% 
//程序名称：GEdorTypeMOInput.jsp
//程序功能：个人保全
//创建日期：2003-01-14 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <SCRIPT src="./GEdorTypeMO.js"></SCRIPT>
  <%@include file="GEdorTypeMOInit.jsp"%> 
</head>

<body  onload="initForm();" >
  <form action="./GEdorTypeMOSubmit.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 
        批单号
      </TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 
        批改类型 
      </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>    
      <TD class = title >  
        保单号 
      </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpPolNo>
      </TD>   
    </TR>
  </TABLE> 
  
  <table>
   	<tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
      </td>
      <td class= titleImg>
        投保人详细信息
      </td>
   	</tr>
  </table> 
   
  <Div  id= "divLCPol" style= "display: ''"> 
  <table  class= common>
   	<TR  class= common>
  	  <TD class = title>
  	     险种编码
  	  </TD>
  	  <TD class= input>
  	    <Input class= "readOnly" readonly  name=RiskCode >
  	  </TD>      
    </TR>
    
    <TR>
      <TD class = title>
         单位编码
      </TD>
      <TD class= input>
        <Input class= "readOnly" readonly  name=GrpNo >
      </TD>     
      <TD class= title>
         单位名称
      </TD>
      <TD class= input>
  	 <Input class= "readOnly" readonly  name=GrpName >
      </TD>      
    </TR> 
    <TR>
      <TD class= title>
	 生效日期
      </TD>
      <TD class= input>
	 <Input class= "readOnly" readonly  name=CValidate >
      </TD>      
      <TD class= title>
         交至日期
      </TD>
      <TD class= input>
	 <Input class= "readOnly" readonly  name=PayToDate >
      </TD>	 
    </TR>
		
    <TR>
       <TD class= title>
           最后一次给付日期
       </TD>
       <TD class= input>
	   <Input class= "readOnly" readonly  name=LastGetDate >
       </TD>      
       <TD class= title>
	   起领日期
       </TD>
       <TD class= input>
           <Input class= "readOnly" readonly  name=GetStartDate >
       </TD>	 
    </TR>
      
    <TR>
       <TD class= title>
       保费
       </TD>
       <TD class= input>
         <Input class= "readOnly" readonly  name=Prem >
       </TD>         
       <TD class= title>
       保额
       </TD>
       <TD class= input>
         <Input class= "readOnly" readonly  name=Amnt >
       </TD>       
     </TR>
     
     <TR> 
		  <TD class = title>
       总累计保费
      </TD>
      <TD class= input>
        <Input class= "readOnly" readonly  name=SumPrem >
      </TD>     
      <TD class= title>
        总余额
      </TD>
  	  <TD class= input>
  	    <Input class= "readOnly" readonly  name=LeavingMoney >
  	  </TD>   	       	       
     </TR>
    </Table>
    
    <br>
    
    <Table class= common>
    <TR> 
		  <TD class = title>
       管理机构代码
      </TD>
      <TD class= input>
        <Input class= "readOnly" readonly  name=ManageCom >
      </TD>     
      <TD class= title>
        管理机构名称
      </TD>
  	  <TD class= input>
  	    <Input class= "readOnly3" readonly  name=ManageName >
  	  </TD>   	       	       
     </TR>
    </Table>
    </Div>
    
    <table>
	   	<tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBonusGetMode);">
	      </td>
	      <td class= titleImg>
	        管理机构变更
	      </td>
	   	</tr>
    </table> 
  
    <DIV id= "divBonusGetMode" style= "display: ''">
	  <table class= common>
      <TR>	  
        <TD class = title>
         新管理机构代码
        </TD>
        <TD class= input>
          <Input name=ManageCom2 CLASS=code ondblclick="return showCodeList('comcodeall', [this,ManageName2],[0,1]);" onkeyup="return showCodeListKey('comcodeall', [this,ManageName2],[0,1]);" verify="新管理机构代码|code:comcodeall">
        </TD>     
        <TD class= title>
         新管理机构名称
        </TD>
    	  <TD class= input>
    	    <Input class= "readOnly3" readonly name=ManageName2 >
    	  </TD>        
      </TR>
	  </table>
    </div>  
    
    <br>
    
    <table class= common>
		<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeMOSave()">
	     	 </TD>
	     	 <TD  class= input width="26%"> 
	       	 <Input class= common type=Button value="返回" onclick="returnParent()">
	     	 </TD>
	    </TR>
    </table>
	
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
	 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
