//程序名称：GEdorBudget.js
//程序功能：退保试算
//创建日期：2006-04-19
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

//判断是否是回车键
function isEnterDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if (keycode == "13")
	{
		return true;
	}
	else
	{
		return false;
	}
}

//响应回车事件，查询保单
function queryGrpContOnKeyDown()
{
  if (isEnterDown())
  {
    queryGrpCont();
  }
}

//查询保单信息
function queryGrpCont()
{
  if(trim(fm.grpContNoTop.value) == "" 
      && trim(fm.appntNoTop.value) == "" 
      && trim(fm.grpNameTop.value) == "")
  {
    alert("请录入查询条件。");
    return false;
  }
  initLCGrpContGrid();
  initLCGrpPolGrid();
  
  var sql = "  select grpContNo, grpName, appntNo, HandlerDate, cValiDate, "
            + "   (select max(payToDate) "
            + "   from LCPol "
            + "   where grpContNo = a.grpContNo), cInValiDate, '缴费频次', prem "
            + "from LCGrpCont a "
            + "where appFlag = '1' "
            + getWherePart("grpContNo", "grpContNoTop")
            + getWherePart("appntNo", "appntNoTop")
            + getWherePart("grpName", "grpNameTop", "like");
  turnPage1.pageDivName = "divPage";
	turnPage1.queryModal(sql, LCGrpContGrid); 
	
	if((trim(fm.grpContNoTop.value) != "" || trim(fm.appntNoTop.value) != ""
	    || trim(fm.grpNameTop.value) != "") && LCGrpContGrid.mulLineCount != 0)
	{
	  setOneGrpContInfo(0);
	}
}

//相应保单列表的单选框单击事件
function setGrpContInfoOnClick()
{
  var row = LCGrpContGrid.getSelNo() - 1;
  setOneGrpContInfo(row);
}

//显示选中的保单信息
function setOneGrpContInfo(row)
{
  fm.grpContNo.value = LCGrpContGrid.getRowColDataByName(row, "grpContNo");
  fm.grpName.value = LCGrpContGrid.getRowColDataByName(row, "grpName");
  fm.appntNo.value = LCGrpContGrid.getRowColDataByName(row, "appntNo");
  fm.polApplyDate.value = LCGrpContGrid.getRowColDataByName(row, "polApplyDate");
  fm.cValiDate.value = LCGrpContGrid.getRowColDataByName(row, "cValiDate");
  fm.payToDate.value = LCGrpContGrid.getRowColDataByName(row, "payToDate");
  fm.cInValiDate.value = LCGrpContGrid.getRowColDataByName(row, "cInValiDate");
  fm.prem.value = LCGrpContGrid.getRowColDataByName(row, "prem");
  fm.payToDateLongPol.value = fm.payToDate.value;
/*  
  var sql = "  select distinct b.codeName "
            + "from LCGrpPol a, LDCode b "
            + "where char(a.payIntv) = b.code "
            + "   and b.codeType = 'grppayintv' "
            + "   and grpContNo = '" + fm.grpContNo.value + "' ";
  var result = easyExecSql(sql);
  if(result)
  {
    if(result.length == 1)
    {
      fm.payIntv.value = result[0][0];
    }
  }
*/  
  //得到保单保全状态
  sql = "  select 1 "
        + "from LPEdorApp a, LPGrpEdorItem b "
        + "where a.edorAcceptNo = b.edorNo "
        + "   and b.grpContNo = '" + fm.grpContNo.value + "' "
        + "   and a.edorState != '0' ";
  result = easyExecSql(sql);
  if(result)
  {
    fm.edorState.value = "有正在进行保全作业";
  }
  else
  {
    fm.edorState.value = "无正在进行保全作业";
  }
  sql = " select 1 "
  		+" from llregister "
  		+" where customerno in (select insuredno from lcinsured where grpcontno = '"+fm.grpContNo.value+"') and rgtstate != '12' and  rgtstate != '14'";
  result = easyExecSql(sql);
  if(result)
  {
    fm.claimState.value = "有正在进行理赔作业";
  }
  else
  {
    fm.claimState.value = "无正在进行理赔作业";
  }
  
  //得到险种信息
  setLCGrpPolInfo(fm.grpContNo.value);
}

//得到险种信息
function setLCGrpPolInfo(grpContNo)
{
  var sql = "  select a.contPlanCode, "
            + "   (select riskSeqNo from LCGrPPol where grpContNo = c.grpContNo and riskCode = a.riskCode), "
            + "   a.riskCode, (select riskName from LMRisk where riskCode = a.riskCode), "
            + "   count(distinct c.insuredNo), min(CValidate), "
            + "   sum(c.amnt), min(c.mult), sum(c.prem), (select sum(SumActuPayMoney) from LJAPayPerson where GrpPolNo = c.GrpPolNo), max(c.endDate) ,"
            + "	case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end "
            + ", '', '', '' "
            + "from LCContPlanRisk a, LCPol c "
            + "where a.grpContNo = c.grpContNo "
            + "   and a.contPlanCode = c.contPlanCode "
            + "   and a.riskCode = c.riskCode "
            + " and c.riskcode not in (select code from ldcode where codetype='tdbc') "
            + "   and a.grpContNo = '" + fm.grpContNo.value + "' "
            + "group by c.grpContNo, c.GrpPolNo, a.contPlanCode, a.riskCode, "
            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType ,payintv,payendyear,payendyearflag "
            + " union "
            + " select a.contPlanCode, "
            + "   (select riskSeqNo from LCGrPPol where grpContNo = c.grpContNo and riskCode = a.riskCode), "
            + "   a.riskCode, (select riskName from LMRisk where riskCode = a.riskCode), "
            + "   count(distinct c.insuredNo), min(CValidate), "
            + "   sum(c.amnt), min(c.mult), sum(c.prem), " 
            + " (select sum(SumActuPayMoney) from LJAPayPerson d,lcpol e where d.GrpPolNo = c.GrpPolNo  and d.PolNo=e.PolNo and e.contPlanCode = a.ContPlanCode), max(c.endDate) ,"
            + "	case payintv when 0 then '-' else trim(char(PayEndYear))||char(PayEndYearFlag) end, case payintv when 1 then '月缴' when 3 then '季缴' when 6 then '半年缴' when 12 then '年缴' when 0 then '趸缴' end "
            + ", '', '', '' "
            + "from LCContPlanRisk a, LCPol c "
            + "where a.grpContNo = c.grpContNo "
            + "   and a.contPlanCode = c.contPlanCode "
            + "   and a.riskCode = c.riskCode "
            + " and c.riskcode in (select code from ldcode where codetype='tdbc') "
            + "   and a.grpContNo = '" + fm.grpContNo.value + "' "
            + "group by c.grpContNo, c.GrpPolNo, a.contPlanCode, a.riskCode, "
            + "   a.ProposalGrpContNo, a.MainRiskCode, a.PlanType ,payintv,payendyear,payendyearflag ";
//            + "order by a.contPlanCode ";
    turnPage2.pageLineNum = 50;
  turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(sql, LCGrpPolGrid);

	for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
	{
	  //计算理赔额
	  sql = "  select sum(a.pay) "
	        + "from LJAGetClaim a, llcase b, LCPol c "
	        + "where a.otherNo = b.caseNo "
	        + "   and a.polNo = c.polNo "
	        + "   and a.GrpcontNo = '" + fm.grpContNo.value + "' "
	        + "   and c.contPlanCode = '" + LCGrpPolGrid.getRowColDataByName(i, "contPlanCode")
	        + "'  and c.riskCode = '" + LCGrpPolGrid.getRowColDataByName(i, "riskCode")
	        + "'  and b.endCaseDate >= '" + LCGrpPolGrid.getRowColDataByName(i, "cValiDate")
	        + "'  and b.endCaseDate <= '" + LCGrpPolGrid.getRowColDataByName(i, "cInValiDate") + "' ";
	  var result = easyExecSql(sql);
	  var claimPay = "0";
	  if(result && result[0][0] != "null")
	  {
	    claimPay = result[0][0];
	  }
	  LCGrpPolGrid.setRowColDataByName(i, "claimPay", claimPay);
	  
	  //计算赔付率
	  var pay = parseFloat(claimPay);
	  var prem = parseFloat(LCGrpPolGrid.getRowColDataByName(i, "sumrem"));
	  var claimPayRate = "0";
	  if(prem != 0)
	  {
	    claimPayRate = (pay/prem);
	  }
	  LCGrpPolGrid.setRowColDataByName(i, "claimPayRate", "" + pointFour(claimPayRate));
	  
	  //20170523,by hhw 管理式医疗产品，实收保费+追加保费+减少保额
	  var isTDBC="select 1 from ldcode where codetype='tdbc' and code='" + LCGrpPolGrid.getRowColDataByName(i, "riskCode")+ "' ";
	  var rs = easyExecSql(isTDBC);
	  if(rs){
		  var tSql = "select sum(a.GetMoney) "
		            + "from LJAGetEndorse a, LCPol b "
		            + "where a.PolNo = b.PolNo "
		            + "   and a.GrpContNo = '" + fm.grpContNo.value + "' "
		            + "   and b.ContPlanCode = '" + LCGrpPolGrid.getRowColDataByName(i, "contPlanCode") + "' "
		            + "   and b.RiskCode = '" +LCGrpPolGrid.getRowColDataByName(i, "riskCode")+ "' " 
		            + "   and a.FeeOperaTionType in ('ZA','ZE') ";
		  rs = easyExecSql(tSql);
		  var zfMoney = '0';
		  if(rs && rs[0][0] != "" && rs[0][0] != "null")
		  {
		    zfMoney = rs[0][0];
		  }
		  var bfMoney = LCGrpPolGrid.getRowColDataByName(i, "sumrem");
		  var sumActuPayMoney = parseFloat(bfMoney)+parseFloat(zfMoney);
		  LCGrpPolGrid.setRowColDataByName(i, "sumrem", String(sumActuPayMoney));
	  }
	}
}

//退保试算提交
function edorBudget()
{
  if(!checkData())
  {
    return false;
  }
  
  var showStr="正在退保试算，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.action = "GEdorBudgetSave.jsp";
  fm.submit();
}

function checkData()
{
  var checkedCount = 0;
  for(var i = 0; i < LCGrpPolGrid.mulLineCount; i++)
  {
    if (LCGrpPolGrid.getChkNo(i))
    {
      checkedCount++;
    }
  }
  if(checkedCount == 0)
  {
    alert("请选择险种")
    return false;
  }
  
  return true;
}

function afterSubmit(flag, content)
{
  try { showInfo.close(); } catch(ex) { }
	window.focus();
	
	if (flag == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

/**返回父页面
 *1、在协议退保时，需要把退保试算数据显示在协议退保明细页面，并关闭本页面
  2、在综合查询菜单中，返回按钮无效
  3、其他情况下，直接关闭本页面
 */
function returnParent()
{
  //把退保试算数据显示在协议退保明细页面
  try
  {
    top.opener.setResultOnXTDetail();
  }
  catch(ex)
  {alert(ex.message);}
  //关闭本窗口
  try
  {
    if(top.parent != undefined)
    {
      top.opener.focus();
      top.close();
    }
  }
  catch(ex)
  {}
}