<%
//程序名称：PEdorTypeNSSubmit.jsp
//程序功能：
//创建日期：20091011
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>
  
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
    
  //后面要执行的动作：添加，修改
  String FlagStr = "";
  String Content = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");
	
  System.out.println("\n\n\n\n--- PEdorTypeNSSubmit.jsp：" 
    + PubFun.getCurrentDate() + " " + PubFun.getCurrentTime() + " "
    + tGlobalInput.Operator);
  
  String transact = request.getParameter("fmtransact");
  String EdorNo = request.getParameter("EdorNo");
  String EdorType = request.getParameter("EdorType");
  String tContNo = request.getParameter("ContNo");
  
  //批改信息
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorNo(EdorNo);
	tLPEdorItemSchema.setContNo(tContNo);
	tLPEdorItemSchema.setEdorType(EdorType);
  
  //得到选择的生效日期
  String tPolNo[]= request.getParameterValues("LCPolAddGrid5");
  String tCValiDate[]= request.getParameterValues("LCPolAddGrid9");
  LPEdorEspecialDataSet mLPEdorEspecialDataSet= new LPEdorEspecialDataSet();
  
  for (int i=0;i<tPolNo.length;i++)
  {
  System.out.println("tPolNo: " + tPolNo[i]);
    LPEdorEspecialDataSchema tLPEdorEspecialDataSchema=new LPEdorEspecialDataSchema();
    tLPEdorEspecialDataSchema.setEdorAcceptNo(EdorNo);
    tLPEdorEspecialDataSchema.setEdorNo(EdorNo);
    tLPEdorEspecialDataSchema.setEdorType(EdorType);
    tLPEdorEspecialDataSchema.setDetailType(BQ.DETAILTYPE_CVALIDATETYPE);
    tLPEdorEspecialDataSchema.setEdorValue(tCValiDate[i]);
    tLPEdorEspecialDataSchema.setPolNo(tPolNo[i]);
    mLPEdorEspecialDataSet.add(tLPEdorEspecialDataSchema);
  }
	
  VData tVData = new VData();
  tVData.add(tGlobalInput);
  tVData.add(tLPEdorItemSchema);
  tVData.add(mLPEdorEspecialDataSet);
  
  PEdorNSDetailUI PEdorNSDetailUI = new PEdorNSDetailUI();
  if (!PEdorNSDetailUI.submitData(tVData, transact))
  {
    System.out.println("Submit Failed! " + PEdorNSDetailUI.mErrors.getErrContent());
    Content = transact + "失败，原因是:\n" + PEdorNSDetailUI.mErrors.getErrContent();
    FlagStr = "Fail";
  }
  else 
  {
    System.out.println("Submit Succed!");
    Content = "保存成功";
    FlagStr = "Success";
  }
  Content = PubFun.changForHTML(Content);
%>   
                   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>");
</script>
</html>

