//该文件中包含客户端需要处理的函数和事件
var showInfo;
var pEdorFlag = true;
var CInsuAccNo;   //InsuAccNo的汉字表示
var NInsuAccNo;   //InsuAccNo的代码
var IOflag = "3";       //金额的导入导出的标记，用来反馈数据是判断上次操作是导出(1)还是导入(2)
var tLeaveMoney = 0.0; 

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//var turnPage3 = new turnPageClass();         //拆分查询数来的数组
//重定义获取焦点处理事件
window.onfocus = initFocus;

//查询按钮
function queryClick() {
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

   var strSql =  "select LPInsureAcc.PolNo, LPInsureAcc.InsuredNo, LCInsured.Name," 
	 + "decode(trim(LPInsureAcc.AccType), '001', '集体公共账户', '002', '个人缴费账户', '003', '个人累积生息账户', '004', '个人红利账户'), "
	 + "decode(trim(LPInsureAcc.InsuAccNo), '000001', '红利复利生息账户', '000002', '年金分红个人储蓄账户(集体交费)', '000003', '年金分红个人储蓄账户(个人交费)', '000004', '年金分红集体帐户', '000005', '公共医疗账户', '000006', '个人医疗账户', '000007', '退连医疗账户'), "
	 + "LPInsureAcc.SumPay, LPInsureAcc.InsuAccBala, LPInsureAcc.RiskCode from LPInsureAcc ,LCInsured where LPInsureAcc.PolNo in "
	 + "(select PolNo from lcpol where grppolno='" + fm.GrpPolNo.value + "') "
	 + "and LPInsureAcc.EdorType = '" + fm.EdorType.value + "' "
         + "and LPInsureAcc.EdorNo = '"+ fm.EdorNo.value +"' "
         + "and LPInsureAcc.PolNo = LCInsured.PolNo  and LPInsureAcc.InsuredNo = LCInsured.CustomerNo "
	 + getWherePart('LCInsured.IDNo', 'IDNo1')
	 + getWherePart('LCInsured.Birthday', 'Birthday1')
	 + getWherePart('LCInsured.Name', 'Name1', 'like', '0')        
         + "  union all "
         + "select LCInsureAcc.PolNo, LCInsureAcc.InsuredNo, LCInsured.Name, " 
	 + "decode(trim(LCInsureAcc.AccType), '001', '集体公共账户', '002', '个人缴费账户', '003', '个人累积生息账户', '004', '个人红利账户'), " 
	 + "decode(trim(LCInsureAcc.InsuAccNo), '000001', '红利复利生息账户', '000002', '年金分红个人储蓄账户(集体交费)', '000003', '年金分红个人储蓄账户(个人交费)', '000004', '年金分红集体帐户', '000005', '公共医疗账户', '000006', '个人医疗账户', '000007', '退连医疗账户'), " 
	 + "LCInsureAcc.SumPay, LCInsureAcc.InsuAccBala, LCInsureAcc.RiskCode from LCInsureAcc, LCInsured where LCInsureAcc.PolNo in "
	 + "(select PolNo from lcpol where grppolno='" + fm.GrpPolNo.value +"') "
	 + "and LCInsureAcc.PolNo = LCInsured.PolNo  and LCInsureAcc.InsuredNo = LCInsured.CustomerNo "
	 + "and LCInsureAcc.PolNo||LCInsureAcc.InsuAccNo not in ( select LPInsureAcc.PolNo||LPInsureAcc.InsuAccNo  from LPInsureAcc where LPInsureAcc.EdorType = '"+fm.EdorType.value+"' and LPInsureAcc.EdorNo = '"+fm.EdorNo.value+"') "
         + getWherePart('LCInsured.IDNo', 'IDNo1')
	 + getWherePart('LCInsured.Birthday', 'Birthday1')
	 + getWherePart('LCInsured.Name', 'Name1', 'like', '0');	           
	turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSql, LCInsuredGrid);
	
	showInfo.close();
}

//查询按钮
function queryClick2() {
	var showStr = "正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
	var strSql = "select LPInsureAcc.PolNo, LPInsureAcc.InsuredNo, LCInsured.Name," 
	 + "decode(trim(LPInsureAcc.AccType), '001', '集体公共账户', '002', '个人缴费账户', '003', '个人累积生息账户', '004', '个人红利账户'), "
	 + "decode(trim(LPInsureAcc.InsuAccNo), '000001', '红利复利生息账户', '000002', '年金分红个人储蓄账户(集体交费)', '000003', '年金分红个人储蓄账户(个人交费)', '000004', '年金分红集体帐户', '000005', '公共医疗账户', '000006', '个人医疗账户', '000007', '退连医疗账户'), "
	 + "LPInsureAcc.SumPay, LPInsureAcc.InsuAccBala, LPInsureAcc.RiskCode from LPInsureAcc ,LCInsured where LPInsureAcc.PolNo in "
	 + "(select PolNo from lcpol where grppolno='" + fm.GrpPolNo.value + "') "
	 + "and LPInsureAcc.EdorType = '" + fm.EdorType.value + "' "
         + "and LPInsureAcc.EdorNo = '"+ fm.EdorNo.value +"' "
         + "and LPInsureAcc.PolNo = LCInsured.PolNo  and LPInsureAcc.InsuredNo = LCInsured.CustomerNo "
         + getWherePart('LCInsured.IDNo', 'IDNo2')
	 + getWherePart('LCInsured.Birthday', 'Birthday2')	           
	 + getWherePart('LCInsured.Name', 'Name2', 'like', '0')
         + "  union all "
         + "select LCInsureAcc.PolNo, LCInsureAcc.InsuredNo, LCInsured.Name, " 
	 + "decode(trim(LCInsureAcc.AccType), '001', '集体公共账户', '002', '个人缴费账户', '003', '个人累积生息账户', '004', '个人红利账户'), " 
	 + "decode(trim(LCInsureAcc.InsuAccNo), '000001', '红利复利生息账户', '000002', '年金分红个人储蓄账户(集体交费)', '000003', '年金分红个人储蓄账户(个人交费)', '000004', '年金分红集体帐户', '000005', '公共医疗账户', '000006', '个人医疗账户', '000007', '退连医疗账户'), " 
	 + "LCInsureAcc.SumPay, LCInsureAcc.InsuAccBala, LCInsureAcc.RiskCode from LCInsureAcc, LCInsured where LCInsureAcc.PolNo in "
	 + "(select PolNo from lcpol where grppolno='" + fm.GrpPolNo.value +"') "
	 + "and LCInsureAcc.PolNo = LCInsured.PolNo  and LCInsureAcc.InsuredNo = LCInsured.CustomerNo "
	 + "and LCInsureAcc.PolNo||LCInsureAcc.InsuAccNo not in ( select LPInsureAcc.PolNo||LPInsureAcc.InsuAccNo  from LPInsureAcc where LPInsureAcc.EdorType = '"+fm.EdorType.value+"' and LPInsureAcc.EdorNo = '"+fm.EdorNo.value+"') "
         + getWherePart('LCInsured.IDNo', 'IDNo2')
	 + getWherePart('LCInsured.Birthday', 'Birthday2')	           
	 + getWherePart('LCInsured.Name', 'Name2', 'like', '0');
	          
	
	turnPage2.pageDivName = "divPage2";
	turnPage2.queryModal(strSql, LCInsured2Grid); 	 
	
	showInfo.close();	 
}

//单击MultiLine的单选按钮时操作
function reportDetailClick() {

       fm.PolNo.value = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 1);
       fm.InsuredNo.value = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 2);
       fm.RiskCode.value = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 8);
       CInsuAccNo = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 5);
       tLeaveMoney = LCInsuredGrid.getRowColData(LCInsuredGrid.getSelNo()-1, 7);
       ChangeInsuAccNoCode();
       fm.InsuAccNo.value = NInsuAccNo;
   
}

/*
//进入个人保全
function PEdorDetail() {
  var showStr = "正在申请集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //window.showModalDialog("GEdorTypeCASubmit.jsp", "", "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame("1");
  fm.all("Transact").value = "INSERT||EDOR"
  fm.submit();
}
*/
/*
//打开个人保全的明细界面
function openPEdorDetail() {
  //var tSel = LCInsuredGrid.getSelNo();
	//if (tSel==0 || tSel==null) {
	if (fm.PolNo.value=="") {
		alert("不能是空记录!");
  }
	else {
		window.open("./PEdorType" + trim(fm.all('EdorType').value) + ".jsp");

		showInfo.close();	 
	}
}
*/

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result) {
  
  showInfo.close();

  if (FlagStr == "Fail") {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else { 
    pEdorFlag = true;
    
    if (fm.Transact.value=="DELETE||EDOR") {
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    }
    else { 
    	//修改所操作的账户信息
        ChangeInsuAccBala(Result);
        queryClick();
        queryClick2();
    }
  }
}

//处理获取焦点事件
function initFocus() {
  if (pEdorFlag) {   
    pEdorFlag = false;
    
    queryPEdorList();
  }
}

//查询出申请后的个人保全列表
function queryPEdorList() {
  var strSql = "select PolNo, CustomerNo, Name, Sex, Birthday, IDType, IDNo from lcinsured where PolNo in "
	           + "(select PolNo from lpedormain where edorno='" + fm.EdorNo.value + "')";
  
  //turnPage2.pageDivName = "divPage2";
	//turnPage2.queryModal(strSql, LCInsured2Grid); 	 
	
}

//单击MultiLine的单选按钮时操作
function reportDetail2Click() {	

       fm.PolNo.value = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 1);
       fm.InsuredNo.value = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 2);
       fm.RiskCode.value = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 8); 
       CInsuAccNo = LCInsured2Grid.getRowColData(LCInsured2Grid.getSelNo()-1, 5);
 
       ChangeInsuAccNoCode();
       fm.InsuAccNo.value = NInsuAccNo;
  
}

//撤销集体下个人保全
function cancelPEdor() {
  fm.all("Transact").value = "DELETE||EDOR"
  
  var showStr = "正在撤销集体下个人保全，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  fm.action = "./PEdorAppCancelSubmit.jsp";
  fm.submit();
  fm.action = "./GEdorTypeCASubmit.jsp";
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent(){
	 top.close();
}

function OutputMoney()  {
  var tSel = LCInsuredGrid.getSelNo();
	
  if( tSel == 0 || tSel == null )
       alert( "请先选择一条记录，再点击申请撤销按钮。" );
  else {
    reportDetailClick();
    var temp1 = 0.0;
    temp1 = fm.all("OutMoney").value;
    
    //对险种211701进行转入转出次数校验
    if(fm.all("RiskCode").value == "211701")
    {
        if(!checkForOut())
        {
    	    return;
        }
    }
    
    if (parseFloat(temp1) > tLeaveMoney)
        alert("录入的转出金额大于账户余额，请重新输入！")
    else {
        IOflag = "1";
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.all("GetMoney").value = "-" + fm.all("OutMoney").value;
        fm.all("Transact").value = "INSERT||EDOR";
        
        
        fm.submit();
    }
  }
}

function checkForOut()
{
   var nIn = parseInt(fm.all("nInCount").value);
   var nOut = parseInt(fm.all("nOutCount").value);
   alert("nIn"+nIn+";nOut"+nOut);
   
   if(nIn!=0)
   {
   	alert("请先转入剩余金额！");
   	return false;
   }
   fm.all("nOutCount").value = nOut + 1;
   return true;
}

function InputMoney()  {
  var tSel = LCInsured2Grid.getSelNo();
	
  if( tSel == 0 || tSel == null )
       alert( "请先选择一条记录，再点击申请撤销按钮。" );
  else {
    reportDetail2Click();
    var temp1 = 0.0;
    var temp2 = 0.0;
    temp1 = fm.all("InMoney").value;
    temp2 = fm.all("SumMoney").value;
    
    //对险种211701进行转入转出次数校验
    if(fm.all("RiskCode").value == "211701")
    {
        if(!checkForIn())
        {
    	    return;
        }
    }
    
    if (parseFloat(temp1) > parseFloat(temp2))
        alert("录入的转入金额大于转移剩余金额，请重新输入！")
    else {
        IOflag = "2";
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");       
        fm.all("GetMoney").value = fm.all("InMoney").value;
        fm.all("Transact").value = "INSERT||EDOR";
        fm.submit();
    }
  }
}

function checkForIn()
{
   var nIn = parseInt(fm.all("nInCount").value);
   var nOut = parseInt(fm.all("nOutCount").value);
   alert("nIn"+nIn+";nOut"+nOut);
 
    temp1 = parseInt(fm.all("InMoney").value*100);
    temp2 = parseInt(fm.all("SumMoney").value*100);	
   
    if(nOut > 1 && temp1 != temp2)
    {
   	alert("请一次转入所有剩余金额！");
   	return false;
    }
    fm.all("nInCount").value = nIn + 1;	
    return true;
}



//对选定的InsuAccNoCode进行汉字向代码的转换
function ChangeInsuAccNoCode() {
   if(CInsuAccNo=='红利复利生息账户') {
      NInsuAccNo = '000001';
   }
   else if(CInsuAccNo=='年金分红个人储蓄账户(集体交费)') {
      NInsuAccNo = '000002';
   }
   else if(CInsuAccNo=='年金分红个人储蓄账户(个人交费)') {
      NInsuAccNo = '000003';
   }
   else if(CInsuAccNo=='年金分红集体帐户') {
      NInsuAccNo = '000004';
   }
   else if(CInsuAccNo=='公共医疗账户') {
      NInsuAccNo = '000005';
   }
   else if(CInsuAccNo=='个人医疗账户') {
      NInsuAccNo = '000006';
   }
   else {
      NInsuAccNo = '000007';
   }
}

//修改所操作的账户信息
function ChangeInsuAccBala(Result) {
     //turnPage3.arrDataCacheSet = clearArrayElements(turnPage3.arrDataCacheSet); 		
     //turnPage3.strQueryResult  = Result;  		
     //turnPage3.useSimulation   = 1;  
     //查询成功则拆分字符串，返回二维数组
     var tArr = decodeEasyQueryResult(Result,0);
     fm.all("SumMoney").value = tArr[0][1];
     alert("fm.all(SumMoney).value" + tArr[0][1]);
          
     //若剩余资金为0，则业务逻辑重新开始
     if(parseInt(fm.all("SumMoney").value*100) == 0)
     {
     	 fm.all('nOutCount').value = 0;
         fm.all('nInCount').value = 0;
     }
     
     if (IOflag == "1") {
          LCInsuredGrid.setRowColData(LCInsuredGrid.getSelNo()-1,7,tArr[1][1]);
     }
     else if (IOflag == "2") {
          LCInsured2Grid.setRowColData(LCInsured2Grid.getSelNo()-1,7,tArr[1][1]);
     }
}

