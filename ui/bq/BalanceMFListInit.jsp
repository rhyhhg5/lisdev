<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!--引入必要的java类  -->
<%@page import="com.sinosoft.lis.pubfun.*"%>	
<% 
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
  String tLoadFlag = request.getParameter("LoadFlag");
%>

<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var operator = "<%=tGI.Operator%>"; //记录当前登录人
  var tCurrentDate = "<%=tCurrentDate%>";
  var tCurrentTime = "<%=tCurrentTime%>";
  var tLoadFlag = "<%= tLoadFlag%>"
;
 
//输入框的初始化 
function initInpBox()
{
  try
  {
	fm.all('ManageCom').value = manageCom;
	var sql = "select Name from LDCom where ComCode = '" + manageCom + "'";
    fm.all('ManageComName').value = easyExecSql(sql);
    fm.all('PolMonthStart').value = "";
    fm.all('PolMonthEnd').value = "";
    var sql = "select Current Date - 3 month from dual ";
    fm.all('RunDateStart').value = easyExecSql(sql);
    fm.all('RunDateEnd').value = tCurrentDate;
    fm.all('SaleChnl').value = "";
    fm.all('SaleChnlName').value = "";
    
    fm.all('LoadFlag').value = tLoadFlag;
  }
  catch(ex)
  {
    alert("在BalanceMFListInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();
    initAccMFGrid(); 
    initElementtype();
  }
  catch(re) 
  {
    alert("在BalanceMFListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//工单列表Mulline的初始化
function initAccMFGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=0;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="管理机构";      //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="保单号";      //列名
    iArray[2][1]="120px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;   
    
    iArray[3]=new Array();
    iArray[3][0]="投保人";      //列名
    iArray[3][1]="50px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[4]=new Array();
    iArray[4][0]="扣费年度";      //列名
    iArray[4][1]="50px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[5]=new Array();
    iArray[5][0]="扣费月度";      //列名
    iArray[5][1]="50px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="扣费金额";      //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[7]=new Array();
    iArray[7][0]="扣费日期";      //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="业务员";      //列名
    iArray[8][1]="50px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[9]=new Array();
    iArray[9][0]="业务员代码";      //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="业务员部门";      //列名
    iArray[10][1]="110px";        //列宽
    iArray[10][2]=200;           //列最大值
    iArray[10][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="投保人手机号";      //列名
    iArray[11][1]="100px";        //列宽
    iArray[11][2]=200;           //列最大值
    iArray[11][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    AccMFGrid = new MulLineEnter("fm", "AccMFGrid"); 
	  //设置Grid属性
    AccMFGrid.mulLineCount = 0;
    AccMFGrid.displayTitle = 1;
    AccMFGrid.locked = 1;
    AccMFGrid.canSel = 0;	
    AccMFGrid.canChk = 0;
    AccMFGrid.hiddenSubtraction = 1;
    AccMFGrid.hiddenPlus = 1;
    AccMFGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在BalanceMFListInit.jsp-->initAccMFGrid函数中发生异常:初始化界面错误!");
  }
}
</script>
