<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：DiskImportTQSave.jsp
//程序功能：磁盘导入上传
//创建日期：2005-10-08
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>


	<%@page import="org.apache.commons.fileupload.*"%>
	<%@page import="java.util.*"%>

<%
	String flag = "";
	String content = "";
	String path = "";      //文件上传路径
	String fileName = "";  //上传文件名
	
	//得到全局变量
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	
    //得到excel文件的保存路径
    String sql = "select SysvarValue "
                + "from ldsysvar "
                + "where sysvar='XmlPath'";
    ExeSQL tExeSQL = new ExeSQL();
    String subPath = tExeSQL.getOneValue(sql);
 		path = application.getRealPath(subPath)+ "/";
 		System.out.println("-----------------"+path);
 

  	String ImportPath = "";
		//上传excel文件并保存，得到文件名   
	  File dir1 = new File(path);
		if (!dir1.exists()){
        dir1.mkdirs();
    }
		DiskFileUpload fu = new DiskFileUpload();
		// 设置允许用户上传文件大小,单位:字节
		fu.setSizeMax(10000000);
		// maximum size that will be stored in memory?
		// 设置最多只允许在内存中存储的数据,单位:字节
		fu.setSizeThreshold(4096);
		// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
		fu.setRepositoryPath(path);
		//开始读取上传信息
		List fileItems = fu.parseRequest(request);
	 
		// 依次处理每个上传的文件
		Iterator iter = fileItems.iterator();
		while (iter.hasNext())
		{
			FileItem item = (FileItem) iter.next();
			if (item.getFieldName().compareTo("ImportPath")==0)
			{
				ImportPath = item.getString();
			}
			//忽略其他不是文件域的所有表单信息
			if (!item.isFormField())
			{
				String name = item.getName();
				
				long size = item.getSize();
				if((name==null||name.equals("")) && size==0)
					continue;
				ImportPath= path + ImportPath;
	 			fileName = name.replace('\\','/');
				fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
	
				//保存上传的文件到指定的目录
				try
				{
					 item.write(new File(ImportPath + fileName));
				}catch(Exception e){
					 e.printStackTrace();
				   System.out.println("upload file error ...");
				}
			}
		}
//	fileName = path +fileName;
	System.out.println("______________fileName:"+fileName);	
	
	
	
	
	TransferData t = new TransferData();
	t.setNameAndValue("edorNo", request.getParameter("edorNo"));
	t.setNameAndValue("grpContNo", request.getParameter("grpContNo"));
	t.setNameAndValue("edorType", request.getParameter("edorType"));
	t.setNameAndValue("path", path);
	t.setNameAndValue("fileName", fileName);
	
	VData v = new VData();
	v.add(t);
	v.add(tGI);

	//从磁盘导入被保人清单
	DiskImportTQUI tDiskImportTQUI = new DiskImportTQUI();
	if (!tDiskImportTQUI.submitData(v, "INSERT||EDOR"))
	{
        flag = "Fail";
        content = tDiskImportTQUI.mErrors.getErrContent();
        System.out.println(tDiskImportTQUI.mErrors.getFirstError());
	}
	else
	{
        flag = "Succ";
        if(tDiskImportTQUI.getImportPersons() != 0)
        {
            int unMainInsuredCount = tDiskImportTQUI.getImportPersons() 
                  - tDiskImportTQUI.getImportPersonsForTQ();
            int notImportPersons =tDiskImportTQUI.getNotImportPersons();
            content = "成功导入" + tDiskImportTQUI.getImportPersons() + "人"
              +(notImportPersons > 0 ? "，其中有" +  notImportPersons+ "人是重复输入的。<br>信息如下：<br>"
              + tDiskImportTQUI.getDiskImportName()+" " : " ")
              + (unMainInsuredCount > 0 ? "，其中有" +  unMainInsuredCount+ "人是连带被保人。" 
                : "");
        }
        else
        {
            content = "清单中没有本保单的客户";
        }
        
        //执行成功，但是部分人员不是本保单客户
        if(tDiskImportTQUI.mErrors.needDealError())
        {
            content += tDiskImportTQUI.mErrors.getErrContent();
        }
	}
	System.out.println(content);
	content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>

