<%
//程序名称：GEdorTypeWSInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<script>
function initForm() 
{
  try
  {
  	initInsuredInfo();
  	initInsuredListGrid();
  	initFailGrid();
  	initQuery();
  	queryFailData();
  	
  }
  catch(ex)
  {
    alert("初始化页面错误！");
  }
}

//新增被保人列表
function initInsuredListGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="姓名";         //列名
		iArray[1][1]="100px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="性别";         			//列名
		iArray[2][1]="50px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="出生日期";         			//列名
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="证件类型";         			//列名
		iArray[4][1]="80px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="证件号码";         			//列名
		iArray[5][1]="150px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="与主被保人关系";         			//列名
		iArray[6][1]="100px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		InsuredListGrid = new MulLineEnter("fm", "InsuredListGrid"); 
		//这些属性必须在loadMulLine前
		InsuredListGrid.mulLineCount = 0;   
		InsuredListGrid.displayTitle = 1;
    	InsuredListGrid.canSel = 0;
    	InsuredListGrid.canChk = 0;
		InsuredListGrid.hiddenPlus = 1;
		InsuredListGrid.hiddenSubtraction = 1;
		//InsuredListGrid.selBoxEventFuncName ="onInsuredGridSelected";
		InsuredListGrid.loadMulLine(iArray);  
	}
	catch (ex)
	{
	  alert(ex);
	}
}

function initFailGrid()
{
  var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";         			//列宽
		iArray[0][2]=10;          			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="姓名";         //列名
		iArray[1][1]="100px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="性别";         			//列名
		iArray[2][1]="50px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="出生日期";         			//列名
		iArray[3][1]="80px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="证件类型";         			//列名
		iArray[4][1]="80px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[5]=new Array();
		iArray[5][0]="证件号码";         			//列名
		iArray[5][1]="150px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[6]=new Array();
		iArray[6][0]="与主被保人关系";         			//列名
		iArray[6][1]="100px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
	 	iArray[7]=new Array();
		iArray[7][0]="无效原因";         			//列名
		iArray[7][1]="300px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		FailGrid = new MulLineEnter("fm", "FailGrid"); 
		//这些属性必须在loadMulLine前
		FailGrid.mulLineCount = 0;   
		FailGrid.displayTitle = 1;
   		FailGrid.canSel = 0;
    	FailGrid.canChk = 0;
		FailGrid.hiddenPlus = 1;
		FailGrid.hiddenSubtraction = 1;
		FailGrid.loadMulLine(iArray);  
	}
	catch (ex)
	{
	  alert(ex);
	}
}

function initInsuredInfo()
{
  mContNo = "<%=request.getParameter("ContNo")%>";
  fm.all('ContNo').value = mContNo;
  fm.all('EdorNo').value = mContNo;
  fm.all('EdorType').value = "WS";
  fm.all('GrpContNo').value = mContNo;
  var sql = "select managecom,(select name from ldcom where comcode = managecom), "
          + "name,codename('sex',sex),birthday,idtype,codename('idtype',idtype),idno from lcinsured a "
          + "where contno = '" + mContNo + "' and relationtomaininsured = '00'";
  var rs = easyExecSql(sql);
  if(rs)
  {
    fm.ManageCom.value = rs[0][0];
    fm.ManageComName.value = rs[0][1];
    fm.InsuredName.value = rs[0][2];
    fm.InsuredSex.value = rs[0][3];
    fm.InsuredBirthDay.value = rs[0][4];
    fm.IDType.value = rs[0][5];
    fm.IDTypeName.value = rs[0][6];
    fm.IDNo.value = rs[0][7];
  }
}
</script>