<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="./LJSGetEndorse.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="LJSGetEndorseInit.jsp"%>
  <title>费用明细查询 </title>
</head>
<body  onload="initForm('');" >

  <form action="./LJSGetEndorse.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
          <INPUT VALUE="按保单排序" TYPE=button onclick="initForm('PolNo');return false;"> 
          <INPUT VALUE="按交费项目排序" TYPE=button onclick="initForm('PayPlanCode');return false;"> 
          <INPUT VALUE="按费用类型排序" TYPE=button onclick="initForm('FeeFinaType');return false;"> 
          <INPUT VALUE="按费用金额排序" TYPE=button onclick="initForm('GetMoney');return false;"> 					
    <table>
    	<tr>
        	<td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLJSGetEndose);">
    		</td>
    		<td class= titleImg>
    			 费用明细信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLJSGetEndorse" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLJSGetEndorseGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();">      
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();">     
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 			
  	</div>
  </form>
  <INPUT class = common VALUE="返回" TYPE=button onclick="returnParent();"> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
