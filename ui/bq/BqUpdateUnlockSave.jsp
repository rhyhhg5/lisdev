<%@ page language="java" contentType="text/html; charset=GBK"
    pageEncoding="GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BqUpdateUnlockSave.jsp
//程序功能：保全工单解锁页面
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.bq.*" %>
<%
        String FlagStr = "";
        String content = "";
        CErrors tError = null;
    try{
    	String edorno = request.getParameter("edorno");
    	TransferData tTransferData = new TransferData();
		VData tVData = new VData();
		tTransferData.setNameAndValue("edorno", edorno);
		System.out.println(edorno);
		tVData.add(tTransferData);
		BqUpdateUnlockUI bq = new BqUpdateUnlockUI();
		if(!bq.submiteData(tVData)){
			FlagStr = "Fail";
			content = "处理失败!";
		}else {
			FlagStr = "Success";
			content = "处理成功！";
		}
    }catch(Exception e) {
    	e.printStackTrace();
    }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=content%>");
</script>
</html>