<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：
//创建日期：2005-11-28
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="GEdorTypeLP.js"></SCRIPT>
	<%@include file="GEdorTypeLPInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<title>团体帐户资金分配 </title>
</head>
<body onload="initForm();" >
  <form action="./GEdorTypeLPSubmit.jsp" method=post name=fm target="fraSubmit">
  <table class="common">
    <tr class="common"> 
      <td class="title">受理号</TD>
      <td class="input"> 
        <input class="readonly" readonly name="EdorNo">
      </td>
      <td class="title">批改类型</TD>
      <td class="input">
      	<input class="readonly" readonly name="EdorType" type="hidden">
      	<input class="readonly" readonly name="EdorTypeName">
      </td>
      <td class="title">集体保单号</TD>
      <td class="input">
      	<input class="readonly" readonly name="GrpContNo">
      </td>
    </tr>
  </table>
  <DIV id=DivClaimHead STYLE="display:''">   
	<table>
			<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivClaim);">
				</td>
				<td class="titleImg">团体理赔金帐户
				</td>
			</tr>
	</table>
	</Div> 
	
	<DIV id=DivClaim STYLE="display:''"> 
		<table  class= common>
			<TR CLASS=common>
				<TD CLASS=title>开户银行</TD>
				<TD CLASS=input>
					<Input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=20 ondblclick="return showCodeList('bank',[this,BankCodeName], [0,1], null, null, null, 1);" onkeyup="return showCodeListKey('bank',[this,BankCodeName], [0,1], null,null,null, 1);" ><Input NAME=BankCodeName VALUE="" CLASS="codeName" readonly >
					<input name="BankCodeBak" type=hidden>
				</TD>
				<TD CLASS=title >户名</TD>
				<TD CLASS=input>
					<Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=40 verify="户名|len<=40" >
					<input name="AccNameBak" type=hidden>
				</TD>
				<TD CLASS=title width="109" >账号</TD>
				<TD CLASS=input>
					<Input NAME=BankAccNo VALUE="" CLASS=common verify="银行帐号|len<=40">
					<input name="BankAccNoBak" type=hidden>
				</TD>
			</TR>   
			<TR CLASS=common>
				<TD CLASS=title>开户银行</TD>
				<TD CLASS=input>
					<Input NAME=BankCode1 VALUE="" CLASS="codeNo" MAXLENGTH=20 ondblclick="return showCodeList('bank',[this,BankCodeName1], [0,1], null, null, null, 1);" onkeyup="return showCodeListKey('bank',[this,BankCodeName1], [0,1], null, null, null, 1);" ><Input NAME=BankCodeName1 VALUE="" CLASS="codeName" readonly >
				</TD>
				<TD CLASS=title >户名</TD>
				<TD CLASS=input>
					<Input NAME=AccName1 VALUE="" CLASS=common MAXLENGTH=40 verify="户名|len<=40" >
				</TD>
				<TD CLASS=title width="109" >账号</TD>
				<TD CLASS=input>
					<Input NAME=BankAccNo1 VALUE="" CLASS=common verify="银行帐号|len<=40">
				</TD>
			</TR>   
		</Table> 	          
	</DIV> 
	<input type=button value="银行查询" class=cssButton onclick="querybank();"> 
	<input type=button value="修    改" class=cssButton onclick="update();"> 
	<br>
  <hr> 
  <br>
  <%@include file="SpecialInfoCommon.jsp"%>
  <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          未做保全被保人信息
        </td>
      </tr>
    </table>
    <table class = common>
      <tr class = common>
        <td class = title>
      		客户号
      	</td>
      	<td class = input>
      		<input class = common  name=InsuredNo1>
          </TD>
        <td class = title>
      	  姓名
      		</td>
      	<td class = input>
      		<input class = common  name=Name>
        </TD>
        <td class = title>
      	  证件号码
      	</td>
      	<td class = input>
      		<input class = common  name=IdNo>
        </TD>  
      </tr>
    </table>
    <input type=button value="查  询" class=cssButton onclick="queryClick();"> 
    <Input type=button value="添加保全"  name="add" class=cssButton onclick="addGEdor();">
    <br><br>
    <Div  id= "divLPInsured" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage" align=center >
        <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 			
      </Div>		
  	</div>
	  <br>
	  <hr> 
	  <br>
  
  <table>
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLPInsured1);">
      </td>
      <td class= titleImg>导入清单</td>
    </tr>
   </table>
   	<Input type=button  value="撤销保全" name="cancel" class=cssButton onclick="cancelGEdor();">
    <Input type=button  value="进入明细" class=cssButton onclick="openGEdorDetail();">  
    <Div id= "divLPInsured1" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanInsuredListGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage1" align=center style= "display: 'none'">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
      </Div>	
  	</div>	 
   <table>
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFail);">
      </td>
      <td class= titleImg>无效数据</td>
    </tr>
   </table>
    <Div id= "divFail" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanFailGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage2" align=center style= "display: 'none'">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
      </Div>	
  	</div>
    <br>
	<Div  id= "divSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="importButton" class=cssButton value="导  入"  onclick="importInsured();">
	   		 <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
	   		 <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
			</TR>
	 	</table>
	</Div>
	<input type=hidden id="fmtransact" name="fmtransact"><p align="right"></p>
	<input type=hidden id="ContType" name="ContType">
	<input type=hidden id="ContNo" name="ContNo">
	<input type=hidden id="EdorValiDate" name="EdorValiDate">
	<input type=hidden id="InsuredNo" name="InsuredNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>