<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：OmnipotenceAcc.jsp
//程序功能：万能账户信息界面
//创建日期：2007-12-20
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>

<html> 
<%
  String tPolNo = request.getParameter("PolNo");
%>
<script>
  var mPolNo = "<%=tPolNo%>"; 
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <%@include file="BonusShareQueryInit.jsp"%> 
  <SCRIPT src="BonusShareQuery.js"></SCRIPT>
  <title>分红信息</title>
</head>

<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form name=fm target="fraSubmit">
    <table  class= common align=center>
      <tr  class= common>
        <td  class="title">分红领取方式</td>
        <td  class="input"><Input class=common  name=BonusShareMethod readonly=true></td>
        <td  class="title">分红帐户余额</td>
        <td  class="input"><Input class=common  name=BonusAcc readonly=true></td>
        <td  class="title">&nbsp;</td>
        <td  class="input">&nbsp;</td>
      </tr>
    </table>
        <br>
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick= "showPage(this,divBonusGrid);">
    		</td>
    		<td class= titleImg>
    			 保单分红信息
    		</td>
    	</tr>
    </table>
      	<Div  id= "divBonusGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBonusGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
		</center>  	
  	</div>
  	    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick= "showPage(this,divAccGrid);">
    		</td>
    		<td class= titleImg>
    			 累积生息帐户历史信息
    		</td>
    	</tr>
    </table>
          	<Div  id= "divAccGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAccGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
		</center>  	
  	</div>
  	</form>
   </body>
</html>
