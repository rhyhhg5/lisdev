<%
//程序名称：PContPauseInit.jsp
//程序功能：
//创建日期：2010-03-31 18:00
//创建人  ：WangHongLiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%> 

<script language="JavaScript">

var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";

//表单初始化
function initForm()
{
	initInputBox();
  	initContPauseGrid();
  	initElementtype();
}
function initContPauseGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px";            		//列宽
		iArray[0][2]=10;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="管理机构";       			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[1][1]="120px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[2]=new Array();
		iArray[2][0]="营销部门";   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[2][1]="180px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[3]=new Array();
		iArray[3][0]="保单号";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[3][1]="120px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[4]=new Array();
		iArray[4][0]="投保人";	   				//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[4][1]="80px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[5]=new Array();
		iArray[5][0]="投保人电话";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[5][1]="110px";            		//列宽
		iArray[5][2]=100;            			//列最大值
		iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[6]=new Array();
		iArray[6][0]="投保人手机号";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[6][1]="110px";            		//列宽
		iArray[6][2]=100;            			//列最大值
		iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[7]=new Array();
		iArray[7][0]="投保人联系地址";	   				//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[7][1]="220px";            		//列宽
		iArray[7][2]=100;            			//列最大值
		iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[8]=new Array();
		iArray[8][0]="期缴保费";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[8][1]="80px";            		//列宽
		iArray[8][2]=100;            			//列最大值
		iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[9]=new Array();
		iArray[9][0]="交至日";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[9][1]="80px";            		//列宽
		iArray[9][2]=100;            			//列最大值
		iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[10]=new Array();
		iArray[10][0]="缴费方式";			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[10][1]="60px";            		//列宽
		iArray[10][2]=100;            			//列最大值
		iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[11]=new Array();
		iArray[11][0]="保单失效/暂停通知书号";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[11][1]="120px";            		//列宽
		iArray[11][2]=100;            			//列最大值
		iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[12]=new Array();
		iArray[12][0]="保单类型";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[12][1]="60px";            		//列宽
		iArray[12][2]=100;            			//列最大值
		iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[13]=new Array();
		iArray[13][0]="代理人编码";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[13][1]="80px";            		//列宽
		iArray[13][2]=100;            			//列最大值
		iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[14]=new Array();
		iArray[14][0]="代理人电话";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[14][1]="110px";            		//列宽
		iArray[14][2]=100;            			//列最大值
		iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[15]=new Array();
		iArray[15][0]="代理人姓名";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[15][1]="80px";            		//列宽
		iArray[15][2]=100;            			//列最大值
		iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[16]=new Array();
		iArray[16][0]="失效日期";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[16][1]="80px";            		//列宽
		iArray[16][2]=100;            			//列最大值
		iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[17]=new Array();
		iArray[17][0]="保单归属状态";	   			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[17][1]="80px";            		//列宽
		iArray[17][2]=100;            			//列最大值
		iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		ContPauseGrid = new MulLineEnter( "fm" , "ContPauseGrid" );
		//这些属性必须在loadMulLine前
		ContPauseGrid.canChk =1 //显示复选框
		ContPauseGrid.mulLineCount = 0;
		ContPauseGrid.displayTitle = 1;
		ContPauseGrid.locked = 1;
		ContPauseGrid.hiddenPlus=1;
		ContPauseGrid.hiddenSubtraction=1;
		ContPauseGrid.loadMulLine(iArray);
	}
	catch(ex)
	{
		alert("PContPauseInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}   
}
function initInputBox()
{
  try
	{
		fm.all('ManageCom').value = manageCom;
		var sql1 = "select Name from LDCom where ComCode = '" + manageCom + "'";
		fm.all('ManageComName').value = easyExecSql(sql1);
		var sql1 = "select Current Date - 1 month from dual ";
		fm.all('StartDate').value = easyExecSql(sql1);
		fm.all('EndDate').value=tCurrentDate;
		fm.all('SaleChnl').value="0";
		fm.all('PrintCount').value="0";
		showAllCodeName();
	}
	catch(ex)
	{
		alert("PContPauseInit.jsp-->initInputBox函数中发生异常:初始化界面错误!");
	}      
}
</script>