<%@include file="../common/jsp/UsrCheck.jsp"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page contentType="text/html;charset=GBK" %>
<script language="JavaScript">

//返回按钮初始化
var str = "";

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
	  fm.all('ManageCom').value = managecom;
	  showAllCodeName();
  }
  catch(ex)
  {
    alert("BonusPrintInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}


function initForm()
{
  try
  {
    initInpBox();
	initTaxRefundGrid();
  }
  catch(re)
  {
    alert("BonusPrintInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 催收记录的初始化
function initTaxRefundGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="客户号";         		//列名
      iArray[1][1]="30px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="保单号";         		//列名
      iArray[2][1]="30px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="年度";         		//列名
      iArray[3][1]="20px";            		//列宽
      iArray[3][2]=200;            		//列最大值
      iArray[3][3]=0;              		//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="管理机构";         		//列名
      iArray[4][1]="30px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="保单生效日期";         		//列名
      iArray[5][1]="30px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="保单终止日期";         		//列名
      iArray[6][1]="30px";            		//列宽
      iArray[6][2]=200;            	        //列最大值
      iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="返还金额";         		//列名
      iArray[7][1]="20px";            		//列宽
      iArray[7][2]=200;            	        //列最大值
      iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

	  TaxRefundGrid = new MulLineEnter( "fm" , "TaxRefundGrid" ); 
      //这些属性必须在loadMulLine前
      TaxRefundGrid.mulLineCount =0;   
      TaxRefundGrid.displayTitle = 1;
      TaxRefundGrid.hiddenPlus = 1;
      TaxRefundGrid.hiddenSubtraction = 1;
      //LjsGetGrid.locked = 1;
      TaxRefundGrid.canChk = 0;
      TaxRefundGrid.canSel = 1;
      TaxRefundGrid.loadMulLine(iArray);  

	  }
      catch(ex)
      {
        alert("BonusPrintInit.jsp-->initLjsGetGrid函数中发生异常:初始化界面错误!");
      }
}

</script>