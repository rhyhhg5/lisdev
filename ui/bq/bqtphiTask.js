var mDebug="0";
var mOperate="";
var showInfo;
var queryCondition="";
var turnPage = new turnPageClass(); 
var turnPage1 = new turnPageClass(); 
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  easyQueryClick();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
   else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	initForm();
  }
  catch(ex)
  {
  	alert("在FailListInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
 
//提交前的校验、计算  
function beforeSubmit()
{
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}
        
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function easyQueryClick()
{
  var tStartDate = fm.all("StartDate").value;
  var tEndDate = fm.all("EndDate").value;
  var strSQL = "select a.ContNo, "
             + "(select managecom from lccont where contno = a.contno union select managecom from lbcont where contno = a.contno fetch first 1 row only), "
             + "a.sgetdate, "
             + "(case a.bonusflag when '1' then '现金' else '累积生息' end), "
             + "a.fiscalyear, "
             + "a.bonusmoney,a.bonusflag,a.polno "
             + " from LOBonusPol a "
			 + " where 1=1 "
             + " and exists (select 1 from lccont where contno = a.contno and ManageCom like '"+ fm.ManageCom.value + "%' union select 1 from lbcont where contno = a.contno and ManageCom like '"+ fm.ManageCom.value + "%' fetch first 1 row only) "
             + getWherePart( 'a.ContNo ','ContNo') 
			 + getWherePart( 'a.bonusflag ','queryType') 
             + " and a.Sgetdate between '" + tStartDate + "' and '" + tEndDate + "' with ur ";        
             
  turnPage1.queryModal(strSQL, BonusGrid); 
  
  fm.sql.value = strSQL;
}

function queryOneClick(){
	if(fm.all("GetContNo").value==null||fm.all("GetContNo").value==""){
		alert("保单号不能为空！");
		return false;
	}
fm.action="BonusGetSave.jsp";
	fm.submit();
}

function queryClick()
{
fm.action="BonusGetSave.jsp";
	fm.submit();
}

function queryRun(runflag){
fm.runIndex.value=	runflag;
	fm.action="BonusBalaSave.jsp";
	fm.submit();
}




function queryOneRun(runflag){
	fm.all('Flag').value=runflag;
	if("1"==runflag){
		if(null==fm.all('queryFlag').value ||""==fm.all('queryFlag').value||"00"==fm.all('queryFlag').value){
			alert("请选择接口类型");
			return false;
		}
	}
	if("2"==runflag){
//		alert("2");
	}
	
	fm.submit();
}

