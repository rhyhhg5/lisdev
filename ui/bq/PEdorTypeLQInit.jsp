<%
//程序名称：PEdorTypeLQInit.jsp
//程序功能：
//创建日期：2008-04-10
//创建人  ：Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.text.*"%>

<script language="JavaScript">  
function initInpBox()
{ 
	//判断是否是在工单查看中查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
 
  	if (flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  	}
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
    //showOneCodeName("EdorCode", "EdorTypeName"); 
}
                                       

function initForm()
{
  try
  {
 
  initInpBox();
  queryMoney();
 	<%
	   boolean Flag=false;
	   String mContNo = request.getParameter("ContNo");
 	   String tsql= "select 1 from LCPol a where ContNo = '" +mContNo+ "' "
 			  + "and riskcode in ('332301','334801') fetch first 1 row only with ur";
  	  
 	   String mRiskCode=new ExeSQL().getOneValue(tsql);
  	   if("1"==mRiskCode || ""!=mRiskCode){
	     Flag=true;
       }
 	 
 	 if(Flag){
 	%>
  	  initInsuAccGrid();
 	  queryInsuAcc();
  	  queryInsuAccList();
  	 <% } %>
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function initInsuAccGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=30;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="险种号码";
		iArray[1][1]="50px";
		iArray[1][2]=200;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="客户号";
		iArray[2][1]="50px";
		iArray[2][2]=200;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="投保日期";
		iArray[3][1]="50px";
		iArray[3][2]=200;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="当前账户余额";
		iArray[4][1]="50px";
		iArray[4][2]=200;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="最近一次结算月度";
		iArray[5][1]="50px";
		iArray[5][2]=200;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="最近公布收益率";
		iArray[6][1]="50px";
		iArray[6][2]=200;
		iArray[6][3]=0;


		InsuAccGrid = new MulLineEnter("fm", "InsuAccGrid"); 
 
		InsuAccGrid.mulLineCount = 0;
		InsuAccGrid.displayTitle = 1;
		InsuAccGrid.canChk = 0;
		InsuAccGrid.canSel = 1;	
		InsuAccGrid.hiddenSubtraction = 1;
		InsuAccGrid.hiddenPlus = 1;
	    
		InsuAccGrid.loadMulLine(iArray);
	}catch(ex){
		alert("在PEdorTypeLQInit.jsp-->initInsuAccGrid函数中发生异常:初始化界面错误!");
	}
	
}
</script>