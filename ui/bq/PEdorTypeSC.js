//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();

//撤消修改
function edorTypeSCCancel()
{
    initForm();
}

//清空特约
function edorTypeSCDelete()
{
    //做一下标示，在保存申请时不往P表中写记录
    fm.all('Speccontent').value = "";
}

//保存申请
function edorTypeSCSave()
{
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
    fm.all('fmtransact').value = "INSERT||MAIN";
    fm.submit();
}

//返回
function returnParent()
{
    top.close();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
    showInfo.close();
    if (FlagStr == "Fail" )
    {             
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    }
    else
    { 
        var tTransact=fm.all('fmtransact').value;
        if (tTransact=="QUERY||MAIN")
        {
            if(Result==null||Result=="")
            {
                //数据丢失
                alert("传入前台特约信息丢失，请重新操作！");
                return ;
            }
            if(Result=="NULL")
            {
                //没有特约记录
                fm.all('Speccontent').value="无特约信息";
                return ;
            }

            //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
            turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
            //保存查询结果字符串
            turnPage.strQueryResult  = Result;
            //使用模拟数据源，必须写在拆分之前
            turnPage.useSimulation   = 1;  
            //查询成功则拆分字符串，返回二维数组
            var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
            turnPage.arrDataCacheSet =chooseArray(tArr,[6]);

            var tSpec = Conversion(turnPage.arrDataCacheSet[0][0]);
            fm.all('Speccontent').value = tSpec;
        }
        else
        {
            var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
            showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
            initForm();
        }
    }
}
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
    if( cDebug == "1" )
        parent.fraMain.rows = "0,0,50,82,*";
    else 
        parent.fraMain.rows = "0,0,0,72,*";
}
