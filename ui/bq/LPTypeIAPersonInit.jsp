<%
//程序名称：LDPersonInit.jsp
//程序功能：
//创建日期：2002-10-25 
//创建人  ：lh程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('CustomerNo').value = '';
    fm.all('Password').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    fm.all('NativePlace').value = '';
    fm.all('Nationality').value = '';
    fm.all('Marriage').value = '';
    fm.all('MarriageDate').value = '';
    fm.all('OccupationType').value = '';
    fm.all('StartWorkDate').value = '';
    fm.all('Salary').value = 0.0;       //float 
    fm.all('Health').value = '';
    fm.all('Stature').value = 0.0;      //float 
    fm.all('Avoirdupois').value = 0.0;   //float
    fm.all('CreditGrade').value = '';
    fm.all('IDType').value = '';
    fm.all('Proterty').value = '';
    fm.all('IDNo').value = '';
    fm.all('OthIDType').value = '';
    fm.all('OthIDNo').value = '';
    fm.all('ICNo').value = '';
    fm.all('HomeAddressCode').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('PostalAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('BankCode').value = '';
    fm.all('BankAccNo').value = '';
    fm.all('JoinCompanyDate').value = '';
    fm.all('Position').value = '';
    fm.all('GrpNo').value = '';
    fm.all('GrpName').value = '';
    fm.all('GrpPhone').value = '';
    fm.all('GrpAddressCode').value = '';
    fm.all('GrpAddress').value = '';
    fm.all('DeathDate').value = '';
    fm.all('Remark').value = '';
    fm.all('State').value = '';
    fm.all('BlacklistFlag').value = ''; 
       
    fm.all('WorkType').value = '';
    fm.all('PluralityType').value = '';
    fm.all('OccupationCode').value = '';
    fm.all('Degree').value = '';
    fm.all('GrpZipCode').value = '';
    fm.all('SmokeFlag').value = '';
    fm.all('RgtAddress').value = '';
    fm.all('HomeZipCode').value = '';
    fm.all('Phone2').value = '';  
    //执行动作 insert,update,delete,不包括query动作,因为有返回值！ 
    fm.all('Transact').value='';

  }
  catch(ex)
  {
    alert("在LDPersonInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("在LDPersonInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("LDPersonInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>