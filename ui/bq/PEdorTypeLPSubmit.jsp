<%
//程序名称：PEdorTypeCMSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
	CErrors tError = null;
            
	String FlagStr = "";
	String Content = "";
	String transact = "";
	String Result = "";
  PEdorLPDetailUI tPEdorLPDetailUI = new PEdorLPDetailUI();
  try
	{
	transact = request.getParameter("fmtransact");
	GlobalInput tG = (GlobalInput) session.getValue("GI");
  
	//理赔金帐号更改  add  by Lanjun 2005-5-24 10:53
	//---------------------------------------------------------------//	
	LPDiskImportSchema tLPDiskImportSchema  = new LPDiskImportSchema();
  tLPDiskImportSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPDiskImportSchema.setGrpContNo(request.getParameter("GrpContNo"));
	tLPDiskImportSchema.setEdorType(request.getParameter("EdorType"));
	tLPDiskImportSchema.setInsuredNo(request.getParameter("CustomerNo"));
	tLPDiskImportSchema.setBankCode(request.getParameter("BankCode"));
  tLPDiskImportSchema.setBankAccNo(request.getParameter("BankAccNo"));                
  tLPDiskImportSchema.setAccName(request.getParameter("AccName"));
 	tLPDiskImportSchema.setSerialNo(request.getParameter("SerialNo"));

  //---------------------------------------------------------------//
	

		VData data = new VData();
		data.addElement(tG);
		data.addElement(tLPDiskImportSchema);

		tPEdorLPDetailUI.submitData(data, transact);
	}
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
	}
	
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tPEdorLPDetailUI.mErrors;
		if (!tError.needDealError())
		{                          
			Content = " 保存成功";
			FlagStr = "Success";
		}
		else                                                                           
		{
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>");
</script>
</html>