<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.schema.LOPRTManagerSchema"%>
<%@page import="com.sinosoft.lis.vschema.LOPRTManagerSet"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.io.*"%>
<%
  System.out.println("--------------------PDFPrintStart------------------");
  CError cError = new CError();
  boolean operFlag = true;
  String FlagStr = "";
  String Content = "";
  
  String OtherNo = request.getParameter("OtherNo");//其它号码
  String OtherNoType = request.getParameter("OtherNoType");//其它号码类型 
  String Code = request.getParameter("Code");//单证类型 （必填值项）
  String StandbyFlag1 = request.getParameter("StandbyFlag1");//年度
  String StandbyFlag2 = request.getParameter("StandbyFlag2");//保单号
  String PatchFlag = request.getParameter("PatchFlag"); //补打标记
  String StandbyFlag3 = request.getParameter("StandbyFlag3");
  String StandbyFlag4 = request.getParameter("StandbyFlag4"); 
  String urgerflag = request.getParameter("urgerflag"); //催办标志
 
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput) session.getValue("GI");//操作员的IP地址
  
  tG.ClientIP = request.getHeader("X-Forwarded-For");
		if(tG.ClientIP == null || tG.ClientIP.length() == 0) { 
		   tG.ClientIP = request.getRemoteAddr(); 
		}
  System.out.println("------操作员的IP地址:"+tG.ClientIP);
  VData tVData = new VData();
  CErrors mErrors = new CErrors();
  
  LOPRTManagerSchema tLOPRTManagerSchema = new LOPRTManagerSchema();
  
  tLOPRTManagerSchema.setOtherNo(OtherNo);
  tLOPRTManagerSchema.setOtherNoType(OtherNoType);
  tLOPRTManagerSchema.setCode(Code);
  tLOPRTManagerSchema.setStandbyFlag1(StandbyFlag1);
  tLOPRTManagerSchema.setStandbyFlag2(StandbyFlag2);
  tLOPRTManagerSchema.setPatchFlag(PatchFlag);
  tLOPRTManagerSchema.setStandbyFlag3(StandbyFlag3);
  tLOPRTManagerSchema.setStandbyFlag4(StandbyFlag4);
  tLOPRTManagerSchema.setUrgerFlag(urgerflag);
  LOPRTManagerSet  mLOPRTManagerSet = new LOPRTManagerSet();
  mLOPRTManagerSet.add(tLOPRTManagerSchema);
  tVData.addElement(mLOPRTManagerSet);
  tVData.addElement(tG);
  

  PDFPrintBatchManagerBL tPDFPrintBatchManagerBL = new PDFPrintBatchManagerBL();
  if(operFlag==true){
    if(!tPDFPrintBatchManagerBL.submitData(tVData,"only")) {           
      FlagStr = "Fail";
      Content = tPDFPrintBatchManagerBL.mErrors.getFirstError().toString();
    }
    else {
       if( tPDFPrintBatchManagerBL.mErrors.getErrorCount()>0){
           Content = tPDFPrintBatchManagerBL.mErrors.getFirstError().toString();
           FlagStr = "PrintError"; 
        }else{
           Content = "打印成功！";
           FlagStr = "Succ"; 
        }         
    }
   }
%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit2("<%=FlagStr%>","<%=Content%>");
</script>
</html>

