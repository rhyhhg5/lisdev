<%
//程序名称：PEdorInputInit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
                      

<script language="JavaScript">  
function initInpBox()
{ 

  try
  {        
  
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('GrpPolNo').value = top.opener.fm.all('GrpPolNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('ContType').value = top.opener.fm.all('ContType').value;
	
    easyQueryClick();
    
    showOneCodeName("EdorCode", "EdorTypeName");

  }
  catch(ex)
  {
    alert("在GEdorTypeGTInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在PEdorTypeGTInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initLPPolGrid();

    initQuery();
    ctrlGetEndorse();
  }
  catch(re)
  {
    alert("PEdorTypeGTInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initLPPolGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";    	//列名
      iArray[1][1]="200px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="客户号";         			//列名
      iArray[2][1]="200px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="客户姓名";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="起保日期";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[5]=new Array();
      iArray[5][0]="总保费";         		//列名
      iArray[5][1]="150px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="退保费";         		//列名
      iArray[6][1]="150px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许


      LPPolGrid = new MulLineEnter( "fm" , "LPPolGrid" ); 
      //这些属性必须在loadMulLine前
      LPPolGrid.mulLineCount = 10;   
      LPPolGrid.displayTitle = 1;
      LPPolGrid.canChk=1;
      LPPolGrid.detailInfo="单击显示详细信息";
      LPPolGrid.loadMulLine(iArray);  
      
      
      }
      catch(ex)
      {
        alert(ex);
      }
}
// 查询按钮
function easyQueryClick()
{
	var tEdorNo;
	var tGrpPolNo;
	var tEdorType;
	
	tEdorNo = top.opener.fm.all('EdorNo').value;
  tGrpPolNo  = top.opener.fm.all('GrpPolNo').value;
  tEdorType = top.opener.fm.all('EdorType').value;
	
	fm.all('GetMoney').value = '';
	  
	// 书写SQL语句
		var strSQL = "";
		strSQL = "select GetMoney from LPGrpEdorMain where EdorNo='"+ tEdorNo +"' and GrpPolNo='" + tGrpPolNo + "' and EdorType='" + tEdorType+"'";
		
		//查询SQL，返回结果字符串
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
		  
	 //判断是否查询成功
		  if (!turnPage.strQueryResult) 
		  {
		    alert("查询失败！");
		  }
		  else
		  {
		  	//查询成功则拆分字符串，返回二维数组
		 	 turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
		 	 fm.all('GetMoney').value = turnPage.arrDataCacheSet[0][0];
		  }

}

function initQuery()
{	
	
	 var i = 0;
	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		
		fm.all('fmtransact').value = "QUERY||MAIN";
		//alert("----begin---");
		//showSubmitFrame(mDebug);
		fm.submit();	  	 	 
}
</script>