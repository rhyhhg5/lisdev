<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：LCGrpPolQueyOut.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：HST
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";

  //保单信息部分
  LCGrpPolSchema tLCGrpPolSchema   = new LCGrpPolSchema();
  tLCGrpPolSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
  // 准备传输数据 VData
  VData tVData = new VData();
	tVData.addElement(tLCGrpPolSchema);
  // 数据传输
  CGrpPolUI tCGrpPolUI   = new CGrpPolUI();
	if (!tCGrpPolUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tCGrpPolUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tCGrpPolUI.getResult();
		
		// 显示
		LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet(); 
		mLCGrpPolSet.set((LCGrpPolSet)tVData.getObjectByObjectName("LCGrpPolBLSet",0));
		int n = mLCGrpPolSet.size();
		System.out.println("get LCGrpPol "+n);
		for (int i = 1; i <= n; i++)
		{
		  	LCGrpPolSchema mLCGrpPolSchema = mLCGrpPolSet.get(i);
		   	%>
		   	<script language="javascript">
		   	  parent.fraInterface.LCGrpPolGrid.addOne("LCGrpPolGrid")
		   		parent.fraInterface.fm.LCGrpPolGrid1[<%=i-1%>].value="<%=mLCGrpPolSchema.getGrpPolNo()%>";
		   		parent.fraInterface.fm.LCGrpPolGrid2[<%=i-1%>].value="<%=mLCGrpPolSchema.getContNo()%>";
		   		parent.fraInterface.fm.LCGrpPolGrid3[<%=i-1%>].value="<%=mLCGrpPolSchema.getGrpProposalNo()%>";
		   	</script>
		<%		
		}
	}
 // end of if  
 
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tCGrpPolUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
//  out.println("<script language=javascript>");
  //out.println("showInfo.close();");
//  out.println("top.close();");
//  out.println("</script>");
%>
