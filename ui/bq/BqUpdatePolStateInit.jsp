<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
function initForm(){
	  initPolGrid();
}
function initPolGrid()
{          
  var iArray = new Array();
    try{
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="0px";            		//列宽
	      iArray[0][2]=10;            			//列最大值
	      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[1]=new Array();
	      iArray[1][0]="保单险种号";         		//列名
	      iArray[1][1]="60px";            		//列宽
	      iArray[1][2]=200;            			//列最大值
	      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[2]=new Array();
	      iArray[2][0]="合同号";         		//列名
	      iArray[2][1]="100px";            		//列宽
	      iArray[2][2]=100;            			//列最大值
	      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
	           
	      iArray[3]=new Array();
	      iArray[3][0]="被保人姓名";         		//列名
	      iArray[3][1]="80px";            		//列宽
	      iArray[3][2]=100;            			//列最大值
	      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	      
	      iArray[4]=new Array();
	      iArray[4][0]="交至日期";         		//列名
	      iArray[4][1]="160px";            		//列宽
	      iArray[4][2]=100;            			//列最大值
	      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	     
	      iArray[5]=new Array();
	      iArray[5][0]="保单状态";         		//列名
	      iArray[5][1]="60px";            		//列宽
	      iArray[5][2]=200;            			//列最大值
	      iArray[5][3]=0; 
		  
		    iArray[6]=new Array();
	      iArray[6][0]="保单险种状态";         		    //列名
	      iArray[6][1]="60px";            		//列宽
	      iArray[6][2]=100;            			//列最大值
	      iArray[6][3]=0;                       //是否允许输入,1表示允许，0表示不允许 
		 
	      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
	      //这些属性必须在loadMulLine前
	      PolGrid.mulLineCount = 6;   
	      PolGrid.displayTitle = 1;
	      PolGrid.locked = 1;
	      PolGrid.canSel = 0;
	      PolGrid.canChk = 1;
		    PolGrid.hiddenSubtraction = 1;
		    PolGrid.hiddenPlus = 1;
	      PolGrid.loadMulLine(iArray);  
	 }catch(ex){
  alert(ex);
}
}

</script>

  
