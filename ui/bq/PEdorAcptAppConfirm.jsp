<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="PEdorAcptAppConfirm.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorAcptAppConfirmInit.jsp"%>
   <%@include file = "ManageComLimit.jsp"%>

  <title>保全受理申请确认 </title>
</head>
<body  onload="initForm();" >

  <form action="./PEdorAcptAppConfirmSubmit.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor1);">
      </td>
      <td class= titleImg>
        保全受理申请信息
      </td>
    	</tr>
    </table>
    <Div  id= "divLPEdor1" style= "display: ''">
      <table  class= common>
        <tr class=common>
        <td class=title>
            保全受理号
        </td>
        <td class= input>
            <Input class="common" name=EdorAcceptNo>
            <!--INPUT VALUE="自动核保信息" TYPE=button class=cssButton onclick="QueryUWDetailClick();"-->
        </td>
        <td class=title>
            申请人姓名
        </td>
        <td class= input>
            <Input class="common" name=EdorAppName>
        </td>
        <td class=title>
            申请方式
        </td>
        <td class= input >
            <Input class="code" name=AppType CodeData="0|^1|客户上门办理^2|业务员代办^3|其它人代办^4|信函^5|电话申请^6|部门转办" ondblClick="showCodeListEx('AppType',[this,AppType],[0,0]);" onkeyup="showCodeListKeyEx('AppType',[this,AppType],[0,0]);">
        </td>
    </tr>
    <tr class=common>
        <TD  class= title>
            保全申请日期
        </TD>
        <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate >
        </TD>
        <td class=title >
            申请号码类型
        </td>
        <td class= input >
            <Input class="code" name=OtherNoType CodeData="0|^1|个人客户号^3|个人保单号" ondblClick="showCodeListEx('OtherNoType',[this,OtherNoType],[0,0]);" onkeyup="showCodeListKeyEx('OtherNoType',[this,OtherNoType],[0,0]);">
        </td> 
        
        <td class=title>
            申请号码
        </td>
        <td class= input>
            <Input class="common"  name=OtherNo><!--Input type=Button value="查询保单" class=cssButton onclick="queryCont()"-->
        </td>
        
    </tr>
      </table>
    </Div>
    <!--     <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();">  -->
         
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdor2);">
    		</td>
    		<td class= titleImg>
    			 个人保全信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdor2" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanPEdorAcptAppConfirmGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class=cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class=cssButton TYPE=button onclick="getLastPage();"> 
            				
  	</div>
  	<br>
  	<table class= common>
  	 <TR class= common>
         	<TD  class= input > 
  			<INPUT class=cssButton VALUE="申请确认" TYPE=button onclick="edorAcptAppConfirm();"> 
        	</TD>
        	<TD class = input>
            		 <INPUT class=cssButton VALUE="返回" TYPE=button onclick="returnParent();"> 
        	</TD>
        </TR>		
   </table>
        <input type=hidden id="EdorType" name="EdorType">
        <input type=hidden name= fmtransact >	
        <input type=hidden name= GetNoticeNo>
        <input type=hidden name= AppntNo>
        <input type=hidden name= SumDuePayMoney>
        <input type=hidden name= prtParams>
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
