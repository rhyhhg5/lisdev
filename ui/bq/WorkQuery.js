var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();  
var turnPage1 = new turnPageClass();  
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  if( verifyInput2() == false ) return false;
  if(!beforeSubmit())
  	return false;
  fm.fmtransact.value="INSERT||MAIN";
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
  
function beforeSubmit()
{
  //添加操作	
  return true;
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}         
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function queryGet()
{
		var sql = 
					"select b.ActuGetNo,b.sumgetmoney,(select codename from ldcode where codetype = 'paymode' and code = b.paymode),(select codename from ldcode b where codetype = 'bank' and b.code = b.bankcode ),"
					+"b.BankAccNo,b.ShouldDate,b.ShouldDate,b.EnterAccDate"
					+" from ljaget b where b.ActuGetNo = '"
					+fm.actuGetNo.value
					+"'"
					;
	turnPage.strQueryResult  = easyQueryVer3(sql, 1, 1, 1);

	if (turnPage.strQueryResult) 
	  {
	  	
	  	
	//查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  

	//保存SQL语句
	turnPage.strQuerySql     = sql; 

 	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage.pageDisplayGrid = getGrid;
	         

	  
	//设置查询起始位置
	turnPage.pageIndex       = 0;  

	  
    //在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	
	if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
	}
		
}
function queryPay()
{
		var sql = 
					"select b.payno,b.SumActuPayMoney,(select codename from ldcode where codetype = 'paymode' and code = c.paymode),(select codename from ldcode b where codetype = 'bank' and b.code = b.bankcode ),"
					+"b.BankAccNo,b.PayDate,b.EnterAccDate"
					+" from ljapay b ,ljtempfeeclass c where c.TempFeeNo = (select TempFeeNo from ljtempfee where otherno=b.incomeno and  othernotype in ('3','10'))  and b.incomeno = (select otherno from ljaget where ActuGetNo = '"
					+fm.actuGetNo.value
					+"')"
					;
	
	
	turnPage1.strQueryResult  = easyQueryVer3(sql, 1, 1, 1);
	if (turnPage1.strQueryResult) 

	  {
	//查询成功则拆分字符串，返回二维数组
	turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
	  
	//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	turnPage1.pageDisplayGrid = payGrid;    
	          
	//保存SQL语句
	turnPage1.strQuerySql     = sql; 
	  
	//设置查询起始位置
	turnPage1.pageIndex       = 0;  
	  
    //在查询结果数组中取出符合页面显示大小设置的数组
	var arrDataSet = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, MAXSCREENLINES);
	//调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage1.pageDisplayGrid);
	if (turnPage1.queryAllRecordCount > turnPage1.pageLineNum) {
    try { window.divPage1.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage1.style.display = "none"; } catch(ex) { }
  }
	}	
	
}