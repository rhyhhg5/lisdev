<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="PEdorCancel.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PEdorCancelInit.jsp"%>
  <title>保全查询 </title>
</head>
<body  onload="initForm();">
  <form action="./PEdorCancelSubmit.jsp" method=post name=fm target="fraSubmit" >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入个人保全查询条件：
			</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            批单号码
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>
          <TD  class= title>
            个人合同号码
          </TD>
          <TD  class= input>
            <Input class= common name=ContNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
          <TD  class= title>
            批改状态
          </TD>
          <TD  class= input>
            <Input class="common" name=EdorState >
          </TD>
        </TR>
    </table>
          <table class= common align=center>
          <INPUT class = cssbutton VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          </table>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 保全信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanMainGrid" >
  					</span> 
  			  	</td>
  			</tr> 		
	
    	</table>
    	       <input type=hidden name=Transact >
      <INPUT class = cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT class = cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT class = cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT class = cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">	 

      
   	<table class= common align=center>
    		<tr class= common>
    			<td  class= input>
    				<INPUT class= cssbutton VALUE="申请撤销" TYPE=button onclick="CancelEdorMain();">     			
    			</td>
    			<td>
    			</td>
    		</tr>
    	</table>
    	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpEdorItem);">
    		</td>
    		<td class= titleImg>
    			 保全项目信息
    		</td>
    	
    	 <!--<td>
    	 <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
    	 </td>-->
    	</tr>
    </table>
  	
  	<input type=hidden name=Transact >
  	<input type=hidden name=DelFlag >
  	<input type=hidden name=EdorType >
  	<input type=hidden name=InsuredNo>  
  	<input type=hidden name=MakeDate>
  	<input type=hidden name=MakeTime>
  	
  	
  	
  	
  	<Div  id= "divLPGrpEdorItem" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>


   	<table class= common align=center>
    		<tr class= common>
    			<td  class= input>
    				<INPUT class= cssbutton VALUE="申请项目撤销" TYPE=button onclick="CancelEdor();">     			
    			</td>
    			<td>
    			</td>
    		</tr>
    	</table>  
       			
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
