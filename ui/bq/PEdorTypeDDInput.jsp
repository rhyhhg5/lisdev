<html> 
<% 
//程序名称：红利领取方式变更
//程序功能：个人保全
//创建日期：2011-12-23 
//创建人  ：【OoO?】杨天政
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeDD.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeDDInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeDDSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
       <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
       </TD>
      <TD class = title > 批改类型 </TD>
       <TD class = input >
      	<input class = "readonly" type="hidden" readonly name=EdorType>
      	<input class = "readonly" readonly name=EdorTypeName>
       </TD>
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>
  <br>
  <table>
   	<tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
      </td>
      <td class= titleImg>
        保单信息
      </td>
   	</tr>
  </table> 
	<Div  id= "divLCPol" style= "display: ''"> 
  <table  class= common>    
		<TR>
			<TD class = title>
			 	投保人客户号
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=AppntNo >
			</TD>
			<TD class= title>
				投保人姓名
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=AppntName >
			</TD>
			<TD class= title>
				生效日期
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=CValidate >
			</TD>      
		</TR> 
		<TR>
			<TD class= title>
				交至日期
			</TD>
			<TD class= input>
				<Input class= "readOnly" readonly  name=PayToDate >
			</TD>
			<TD class= title>
				总保费
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=Prem >
			</TD>   
			<TD class= title>
				总保额
			</TD>
			<TD class= input>
			 <Input class= "readOnly" readonly  name=Amnt >
			</TD>
		</TR>	  
		<TR>  
			<TD class= title>
			  红利领取方式
			</TD>    
			<TD class= input colspan="5">
			  <Input class="codeNo" name="BonusGetMode" CodeData="0|^1|现金领取^2|累积生息" 
			  verify="红利领取方式|notnull&code:BonusGetMode"
			  ondblClick="showCodeListEx('sex',[this,BonusGetModeName],[0,1]);" 
			  onkeyup="showCodeListKeyEx('sex',[this,BonusGetModeName],[0,1]);" ><Input class="codeName" name="BonusGetModeName"  elementtype="nacessary" readonly>
			  <Input class= hidden type=hidden readonly name=BonusGetModeNameBak >
			</TD>		       	       
		</TR>

	</Table>
	</div>
    <br>
		<div>
			<Input class= cssButton type=Button value="保  存" onclick="edorTypeDDSave()">
			<Input class= cssButton type=Button value="取  消" onclick="initForm()">
			<Input class= cssButton type=Button value="返  回" onclick="returnParent()">
		</div>
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="BonusGetMode2" name="BonusGetMode2">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
