<SCRIPT>

var mEdorNo = WorkNo;

//自核信息列表初始化
function initUWErrGrid()
{                               
  var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="0px";         			//列宽
	  iArray[0][2]=10;          			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
		iArray[1]=new Array();
	  iArray[1][0]="项目类型";    	      //列名
	  iArray[1][1]="100px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	  iArray[1][4]="EdorCodeForView";
		iArray[1][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[1][9]="批改类型|code:EdorCodeForView&NOTNULL";
		iArray[1][18]=250;
		iArray[1][19]= 0 ;
	
	  iArray[2]=new Array();
	  iArray[2][0]="保单号";    	      //列名
	  iArray[2][1]="100px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[3]=new Array();
	  iArray[3][0]="被保人";    	      //列名
	  iArray[3][1]="100px";            		//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="险种名称";    	      //列名
	  iArray[4][1]="200px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[5]=new Array();
	  iArray[5][0]="送核原因";         		//列名
	  iArray[5][1]="350px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
  	UWErrGrid = new MulLineEnter("fm", "UWErrGrid"); 
		UWErrGrid.mulLineCount = 0;
		UWErrGrid.displayTitle = 1;
		UWErrGrid.locked = 1;
		UWErrGrid.canSel = 0;
		UWErrGrid.canChk = 0;
		UWErrGrid.hiddenSubtraction = 1;
		UWErrGrid.hiddenPlus = 1;
		UWErrGrid.loadMulLine(iArray);
		queryUWErr();

  }
  catch(ex)
  {
    alert(ex);
  }
}

//人工核保信息初始化
function initManuUWGrid()
{                               
  var iArray = new Array();
  try
  {
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="0px";         			//列宽
	  iArray[0][2]=10;          			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	  iArray[1]=new Array();
	  iArray[1][0]="项目类型";    	      //列名
	  iArray[1][1]="80px";            		//列宽
	  iArray[1][2]=100;            			//列最大值
	  iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	
		iArray[2]=new Array();
	  iArray[2][0]="项目类型";    	      //列名
	  iArray[2][1]="80px";            		//列宽
	  iArray[2][2]=100;            			//列最大值
	  iArray[2][3]=2;              			//是否允许输入,1表示允许，0表示不允许
	  iArray[2][4]="EdorCodeForView";
		iArray[2][5]="3";              	                //引用代码对应第几列，'|'为分割符
		iArray[2][9]="批改类型|code:EdorCodeForView&NOTNULL";
		iArray[2][18]=250;
		iArray[2][19]= 0 ;
	  
	  iArray[3]=new Array();
	  iArray[3][0]="保单号";    	      //列名
	  iArray[3][1]="80px";            	//列宽
	  iArray[3][2]=100;            			//列最大值
	  iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[4]=new Array();
	  iArray[4][0]="险种号";    	      //列名
	  iArray[4][1]="60px";            		//列宽
	  iArray[4][2]=100;            			//列最大值
	  iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[5]=new Array();
	  iArray[5][0]="险种序号";    	      //列名
	  iArray[5][1]="50px";            		//列宽
	  iArray[5][2]=100;            			//列最大值
	  iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[6]=new Array();
	  iArray[6][0]="险种名称";    	      //列名
	  iArray[6][1]="100px";            		//列宽
	  iArray[6][2]=100;            			//列最大值
	  iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[7]=new Array();
	  iArray[7][0]="被保人客户号";         		//列名
	  iArray[7][1]="80px";            		//列宽
	  iArray[7][2]=100;            			//列最大值
	  iArray[7][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[8]=new Array();
	  iArray[8][0]="被保人";         		//列名
	  iArray[8][1]="60px";            		//列宽
	  iArray[8][2]=100;            			//列最大值
	  iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	 
	 	iArray[9]=new Array();                                                 
	  iArray[9][0]="核保结论";         		//列名                           
	  iArray[9][1]="200px";            		//列宽                             
	  iArray[9][2]=100;            			//列最大值                           
	  iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[10]=new Array();                                                 
	  iArray[10][0]="加费标志";         		//列名                           
	  iArray[10][1]="80px";            		//列宽                             
	  iArray[10][2]=100;            			//列最大值                           
	  iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[11]=new Array();                                                 
	  iArray[11][0]="免责标志";         		//列名                           
	  iArray[11][1]="80px";            		//列宽                             
	  iArray[11][2]=100;            			//列最大值                           
	  iArray[11][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[12]=new Array();                                                 
	  iArray[12][0]="降档标志";         		//列名                           
	  iArray[12][1]="80px";            		//列宽                             
	  iArray[12][2]=100;            			//列最大值                           
	  iArray[12][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[13]=new Array();                                                 
	  iArray[13][0]="核保意见";         		//列名                           
	  iArray[13][1]="120px";            		//列宽                             
	  iArray[13][2]=100;            			//列最大值                           
	  iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[14]=new Array();                                                 
	  iArray[14][0]="核保日期";         		//列名                           
	  iArray[14][1]="80px";            		//列宽                             
	  iArray[14][2]=100;            			//列最大值                           
	  iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[15]=new Array();                                                 
	  iArray[15][0]="客户不同意处理";     //列名                           
	  iArray[15][1]="80px";            		//列宽                             
	  iArray[15][2]=100;            			//列最大值                           
	  iArray[15][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[16]=new Array();                                                  
	  iArray[16][0]="客户反馈结果";       //列名                             
	  iArray[16][1]="80px";            		//列宽                               
	  iArray[16][2]=100;            			//列最大值                           
	  iArray[16][3]=0;              			//是否允许输入,1表示允许，0表示不允许

 		ManuUWGrid = new MulLineEnter("fm", "ManuUWGrid"); 
		ManuUWGrid.locked = 1;
		ManuUWGrid.canSel = 1;
		ManuUWGrid.canChk = 0;
		ManuUWGrid.hiddenSubtraction = 1;
		ManuUWGrid.hiddenPlus = 1;
		ManuUWGrid.selBoxEventFuncName="queryAddition";
		ManuUWGrid.loadMulLine(iArray);
		queryManuUW();
		initAddFeeGrid();
		initSpecGrid();
	}
	catch(ex)
	{
	 alert(ex);
	}
}

//加费信息
function initAddFeeGrid() 
{
  var iArray = new Array();
  try 
  {
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="0px";            		//列宽
		iArray[0][2]=30;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[1]=new Array();
		iArray[1][0]="费用值";         	//列名
		iArray[1][1]="20px";            		//列宽
		iArray[1][2]=100;            			//列最大值
		iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[2]=new Array();
		iArray[2][0]="费用比例";         	//列名
		iArray[2][1]="10px";            		//列宽
		iArray[2][2]=100;            			//列最大值
		iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[3]=new Array();
		iArray[3][0]="加费起始日期";         		//列名
		iArray[3][1]="10px";            		//列宽
		iArray[3][2]=100;            			//列最大值
		iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		iArray[4]=new Array();
		iArray[4][0]="加费终止日期";         		//列名
		iArray[4][1]="10px";            		//列宽
		iArray[4][2]=100;            			//列最大值
		iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		
		AddFeeGrid = new MulLineEnter("fm" , "AddFeeGrid");
		//这些属性必须在loadMulLine前
		AddFeeGrid.mulLineCount = 1;
		AddFeeGrid.displayTitle = 1;
		AddFeeGrid.locked = 1;
		AddFeeGrid.canSel = 0;
		AddFeeGrid.hiddenPlus = 1;
		AddFeeGrid.hiddenSubtraction = 1;
		AddFeeGrid.addEventFuncName="checkAddfee";
		AddFeeGrid.loadMulLine(iArray);
	} 
	catch(ex) 
	{
	  alert(ex);
	}
}

function initSpecGrid() 
{
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="免责信息代码";         	//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][4]="speccode";              	//是否引用代码:null||""为不引用
    iArray[1][5]="1|2";              	    //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1";              	 
    iArray[1][19]=1;

    iArray[2]=new Array();
    iArray[2][0]="免责信息";         	//列名
    iArray[2][1]="150px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="免责起始日期";         		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="免责终止日期";         		//列名
    iArray[4][1]="80px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="流水号";         	//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许

    SpecGrid = new MulLineEnter("fm", "SpecGrid");
    //这些属性必须在loadMulLine前
    SpecGrid.mulLineCount = 1;
    SpecGrid.displayTitle = 1;
    SpecGrid.locked = 0;
    SpecGrid.canSel = 0;
    SpecGrid.hiddenPlus = 1;
    SpecGrid.hiddenSubtraction = 1;
    SpecGrid.loadMulLine(iArray);
  } 
  catch(ex) 
  {
    alert(ex);
  }
}

//查询自核错误信息
function queryUWErr()
{
	var sql = "select EdorType, ContNo, InsuredName, (select RiskName from LMRisk where riskCode = (select riskCode from LCPol where PolNo = LPUWERROR.PolNo)), UWError  " +
						"from LPUWERROR " +
						"where EdorNo = '" + mEdorNo + "' ";
	turnPage.queryModal(sql, UWErrGrid);
}

//查询人工核保结论
function queryManuUW() 
{
	var sql = "select EdorType, EdorType, ContNo, PolNo, " +
	          "       (select RiskSeqNo from LCPol where PolNo = LPUWMaster.PolNo) as RiskSeqNo, " +
	          "       (select RiskName from LMRisk where riskCode = (select riskCode from LCPol where PolNo = LPUWMaster.PolNo)), InsuredNo, InsuredName, " +
	          "       SugUWIdea, AddPremFlag, SpecFlag, SubMultFlag, UWIdea, MakeDate, " +
	          "       case when DisagreeDeal = '1' then '终止申请' when DisagreeDeal = '2' then '终止险种效力' end , " +
	          "       case when CustomerReply = '1' then '同意' when CustomerReply = '2' then '不同意' end "+
						"from LPUWMaster " +
						"where EdorNo = '" + mEdorNo + "' " +
						"and AutoUWFlag = '2'" +
						"order by RiskSeqNo";
	turnPage.queryModal(sql, ManuUWGrid);
}


//查询附加条件
function queryAddition() 
{
	var selNo = ManuUWGrid.getSelNo();
	var edorType = ManuUWGrid.getRowColData(selNo - 1, 1);
	var polNo = ManuUWGrid.getRowColData(selNo - 1, 4);
	var passFlag = ManuUWGrid.getRowColData(selNo - 1, 9);
	var addPremFlag = ManuUWGrid.getRowColData(selNo - 1, 10);
	var specFlag = ManuUWGrid.getRowColData(selNo - 1, 11);
	var subMultFlag = ManuUWGrid.getRowColData(selNo - 1, 12);
	if (passFlag == "4") //附加条件
	{
		if (addPremFlag == "1")
		{
			queryPrem(edorType, polNo);
			document.all("divAddFee").style.display = "";
		}
		else
		{
			document.all("divAddFee").style.display = "none";
		}
		if (specFlag == "1")
		{
			querySpec(edorType, polNo);
			document.all("divAddSpec").style.display = "";
		}
		else
		{
			document.all("divAddFee").style.display = "none";
		}
	  if (subMultFlag == "1")
	  {
	  }
	}
	else
	{
		document.all("divAddFee").style.display = "none";
		document.all("divAddSpec").style.display = "none";
	}
}

//查询加费信息
function queryPrem(edorType, polNo)
{
	var sql = "select Prem, Rate, PayStartDate, PayEndDate from LPPrem " + 
	          "where EdorNo = '" + mEdorNo + "' " +
	          "and EdorType = '" + edorType + "' " +
	          "and PolNo = '" + polNo + "' " +
	          "and PayPlanCode like '000000%%' ";
	turnPage.queryModal(sql, AddFeeGrid);
}

//查询免责信息
function querySpec(edorType, polNo)
{
	var sql = "select specCode, specContent, specStartDate, specEndDate, SerialNo " +
	          "from LPSpec " + 
	          "where EdorNo = '" + mEdorNo + "' " +
	          "and EdorType = '" + edorType + "' " +
	          "and PolNo = '" + polNo + "' ";
	turnPage.queryModal(sql, SpecGrid);
}

</SCRIPT>


