var turnPage = new turnPageClass();  

function queryClick()
{
	var sql = "select GrpName from LDGrp " +
	          "where CustomerNo = '" + fm.CustomerNo.value + "' ";
	var result = easyExecSql(sql);
	if (!result)
	{
		alert("未找到客户信息!");
		return false;
	}
	fm.CustomerName.value = result[0][0];
	sql = "select AccBala from LCAppAcc " +
	      "where CustomerNo = '" + fm.CustomerNo.value + "' ";
	result = easyExecSql(sql);
	if (!result)
	{
		alert("未找到客户帐户信息!");
	}
	else
	{
	  fm.AccBala.value = result[0][0];
	}
	sql = "select GrpContNo, CValiDate, PayIntv, Prem, AgentCode, " +
	      "       (select Name from LAAgent where AgentCode = LCGrpCont.AgentCode) " +
	      "from LCGrpCont " +
	      "where AppntNo = '" + fm.CustomerNo.value + "' " +
	      "and GrpContNo = '" + fm.GrpContNo.value + "' " +
	      "and AppFlag = '1' ";
  turnPage.queryModal(sql, ContGrid);  
  sql = "select Money from LCAppAccTrace " +
        "where CustomerNo = '" + fm.CustomerNo.value + "' " +
        "and OtherNo = '" + fm.EdorNo.value + "' ";
  result = easyExecSql(sql);
	if (result)
	{
		fm.AccFee.value = result[0][0];
	}
}

function selCont()
{
	fm.ContNo.value = ContGrid.getRowColData(ContGrid.getSelNo() - 1, 1);
}

function save()
{
  if (fm.AccFee.value<=0)
	{
		alert("预交保费金额不能为负数和0，请您重新填写预交保费金额!");
		return false;
	}
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./GEdorTypeYSSave.jsp";
	fm.submit();
}

function afterSubmit(flag, content)
{
  try
  {
    showInfo.close();
    window.focus();
  }
  catch (ex) {}
  if (flag == "Fail")
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
    returnParent();  
  }
}

function returnParent()
{
	try
	{
		top.opener.getGrpEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {};
}
