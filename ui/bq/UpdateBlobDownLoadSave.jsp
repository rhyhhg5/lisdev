<%

%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%>
    <%@page import="org.apache.commons.fileupload.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="java.io.*"%>
<%@page import="java.net.*"%>
<%@page contentType="text/html;charset=GBK" %>


<%
  System.out.println("开始执行合同保存Save页面");
  
  GlobalInput globalInput = new GlobalInput( );
  globalInput.setSchema( (GlobalInput)session.getValue("GI") );
  //select * from LDSYSVAR
  CErrors tError = null;
  String Content = "";
  String FlagStr = "";
  String mEdorno = request.getParameter("Edorno");
  String mContType = request.getParameter("ContType");
  String mFileType = request.getParameter("FileType");
 
 
     		//临时保存解析出的数据的保存路径
		String path = application.getRealPath("").replace('\\','/')+'/';
		String importPath = path + "temp/upload";
  
  		DiskFileUpload fu = new DiskFileUpload();
		//设置请求消息实体内容的最大允许大小
		fu.setSizeMax(100 * 1024 * 1024);
		//设置是否使用临时文件保存解析出的数据的那个临界值
		fu.setSizeThreshold(1 * 1024 * 1024);
		//设置setSizeThreshold方法中提到的临时文件的存放目录
		fu.setRepositoryPath(importPath);
		//判断请求消息中的内容是否是“multipart/form-data”类型
		//fu.isMultipartContent(request);
		//设置转换时所使用的字符集编码
		//fu.setHeaderEncoding(encoding);
 
 					//依次处理每个上传的文件
					List fileItems = null;
							try
		{
			//解析出FORM表单中的每个字段的数据，并将它们分别包装成独立的FileItem对象
			//然后将这些FileItem对象加入进一个List类型的集合对象中返回
			fileItems = fu.parseRequest(request);
		}
		catch(Exception ex)
		{
			fileItems = new ArrayList();
			ex.printStackTrace();
		}
		Iterator iter = fileItems.iterator();

		HashMap hashMap = new HashMap();
		while (iter.hasNext()) 
		{
		
			FileItem item = (FileItem) iter.next();
			
			//判断FileItem类对象封装的数据是否属于一个普通表单字段，还是属于一个文件表单字段
			if(item.isFormField())
			{
				//返回表单字段元素的name属性值、将FileItem对象中保存的主体内容作为一个字符串返回
				hashMap.put(item.getFieldName(), item.getString());
				//System.out.println(item.getFieldName() + " : " + item.getString());
			}
		}
		if(mEdorno==null||"".equals(mEdorno)){
			 mEdorno = (String)hashMap.get("Edorno");
		}
		if(mContType==null||"".equals(mContType)){
			 mContType = (String)hashMap.get("ContType");
		}
  		if(mFileType==null||"".equals(mFileType)){
			 mFileType = (String)hashMap.get("FileType");
		}	
 
  
  		String pathInfo = "";
		String fileName = "";
  
 String contextPath = request.getContextPath();
 System.out.println(contextPath);
 String serverPath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
 System.out.println(serverPath); 
  
          pathInfo = "temp";
		System.out.println("file path : " + pathInfo + " file name : " + mEdorno+"export.xml");
		serverPath = application.getRealPath("").replace('\\','/')+'/';
		String filePath = serverPath + pathInfo + "/" + mEdorno+"export"+new Date()+".xml";
		System.out.println("file path : " + filePath);
  
   fileName = filePath;
  String tableName = "";
  String fieldName = "";
  

  			


  if("1".equals(mContType)){
	  
    tableName = "LPEdorAppPrint";//个单
    fieldName = "EdorAcceptNo";//个单
  }
  else if("2".equals(mContType)){
	  fieldName = "EdorNo";//团单
	  
	  if("1".equals(mFileType)){
		  
		  tableName = "LPEdorPrint";//团单
	      
	  }
	  else{
		  tableName = "LPEdorPrint2";//团单增人清单
		  
	  }
	    
	  } 
  
  UpdateBlobBL mUpdateBlobBL = new UpdateBlobBL(fileName,tableName,fieldName,mEdorno);
  VData tVData = new VData();

	  if(mUpdateBlobBL.exportBlob()){
	  	  FlagStr = "Succ";
		Content = "数据保存成功" ;
	  }else{
    Content = "失败" ;
    FlagStr = "Fail";
  }
  if("Succ".equals(FlagStr)){
  	  //用户下载到自己选定的目录
	  	BufferedOutputStream output = null;
		BufferedInputStream input = null;  
  		try
		{
			//filePath = new String(filePath.getBytes("ISO-8859-1"),"gbk");
			
			File file = new File(filePath);
			System.out.println("下载地址是----》》》"+filePath+"==|||"+file.getName());
			response.reset();
			response.setContentType("application/octet-stream"); 
			response.setHeader("Content-Disposition","attachment; filename=" + URLEncoder.encode(mEdorno+"export.xml","GBK"));
			response.setContentLength((int) file.length());

			byte[] buffer = new byte[4096];  
			//写缓冲区
			output = new BufferedOutputStream(response.getOutputStream());
			input = new BufferedInputStream(new FileInputStream(file));

			int len = 0;
			while((len = input.read(buffer)) >0)
			{
				output.write(buffer,0,len);
			}
			Content = "下载成功！";
			out.clear();
			out = pageContext.pushBody();

			input.close();
			output.close(); 
		}
		catch(FileNotFoundException fe)
		{
		
			Content = "没有要下载的文件，请确认文件是否存在！";
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			System.out.println("下载失败！");
			
			Content = "下载失败";
		}
		finally {
			if (input != null) input.close();
			
			if (output != null) output.close();
		}  
 }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>