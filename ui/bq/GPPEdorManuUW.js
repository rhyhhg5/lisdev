//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var itjj = 1 ;
var mCond;
//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initPEdorMainGrid();
  // showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//按照保全项目进行人工核保
function edorTypeManuUW()
{

	if (fm.all('UWState').value == '') {
		alert('请输入核保结论');
		return;
	}
		
	if (window.confirm("是否核保本次申请?"))
	{
	  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	  fm.all('fmtransact').value = "UPDATE||MANUUWENDORSE";
	  mCond = '1';
	  fm.submit();
	  DivEdorManuUWAct.style.display = 'none';
	}
}

//人工核保保全
function edorManuUW()
{

	var tPolNo ;
	var tEdorNo;
	
	tPolNo = fm.all('PolNo').value;
	tEdorNo = fm.all('EdorNo').value;
	
	if ((tPolNo==''||tPolNo==null)&& (tEdorNo ==''||tEdorNo==null))
		alert("请录入要核保的保单申请!");
	else
	{
		if (window.confirm("是否核保本次申请?"))
		{
		  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 		  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 		  fm.all('fmtransact').value = "ALL||MANUUWENDORSE";
  		  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		  fm.submit();
		}
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   // alert("success!");
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//    alert(mCond);
    if ( mCond=='1')
    {
	    if (LoadFlag=='G')
	   	grpQueryClick();
	    else
		easyQueryClick();
    }
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

// 查询按钮
function easyQueryClick()
{
	var tEdorState;
	tEdorState='2';
	
	var tUWGrade;
	tUWGrade = fm.all('uwgrade').value;
	var tUWGradeFlag;
	tUWGradeFlag = fm.all('uwGradeFlag').value;
		
	// 初始化表格
	initPEdorMainGrid();
	var tReturn= parseManageComLimit();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select EdorNo,PolNo,InsuredNo,EdorType,EdorValiDate,EdorAppDate,GetMoney from LPEdorMain where EdorState="+ tEdorState+" and uwstate in ('5','6')"+" "+" and"+tReturn
				 +" and edorno in (select distinct edorno from lpuwmaster where passflag <> '9' and appgrade <= '"+tUWGrade+"')"
				 + getWherePart( 'PolNo' )
				 + getWherePart('EdorNo')+ " order by edorno,polno";
	if (tUWGradeFlag==null||tUWGradeFlag=='')
	{
		if (window.confirm("是否只处理本核保级别的批单?(确定：仅对本操作员级别进行核保；取消：对本级别以及下级进行核保)"))
		{
			strSQL = "select EdorNo,PolNo,InsuredNo,EdorType,EdorValiDate,EdorAppDate,GetMoney from LPEdorMain where EdorState="+ tEdorState+" and uwstate in ('5','6')"+" "+" and"+tReturn
					 +" and edorno in (select distinct edorno from lpuwmaster where passflag <> '9' and appgrade = '"+tUWGrade+"')"
					 + getWherePart( 'PolNo' )
					 + getWherePart('EdorNo')+ " order by edorno,polno";
			fm.all('uwGradeFlag').value = 'P';
		}	
		else
		{
					fm.all('uwGradeFlag').value = 'A';
		}		
	}
	else
	{
		if (tUWGradeFlag=='P')
		{
			strSQL = "select EdorNo,PolNo,InsuredNo,EdorType,EdorValiDate,EdorAppDate,GetMoney from LPEdorMain where EdorState="+ tEdorState+" and uwstate in ('5','6')"+" "+" and"+tReturn
					 +" and edorno in (select distinct edorno from lpuwmaster where passflag <> '9' and appgrade = '"+tUWGrade+"')"
					 + getWherePart( 'PolNo' )
					 + getWherePart('EdorNo')+ " order by edorno,polno";
		}
	} 
//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initPEdorMainGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		//alert(n);
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			//alert(i);
			for( j = 0; j < m; j++ )
			{
				//alert("j;"+j);
				PEdorMainGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}
//显示自动核保信息
function edorClicked()
{
	var arrReturn = new Array();
	var tEdorNo;
	var tEdorType;
	var tPolNo;
			
	var tSel = PEdorMainGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再查看" );
	else
	{
		if (PEdorMainGrid.getRowColData(tSel-1,1)==null||PEdorMainGrid.getRowColData(tSel-1,1)=='')
			{
				alert("请单击查询按钮!");
				DivEdorManuUWAct.style.display ='none';
				return;
			}
		arrReturn = getQueryResult();
		 tEdorNo = arrReturn[0][0];
		 tPolNo = arrReturn[0][1];
		 tEdorType = arrReturn[0][3];
		 
		 		 
		detailUWInfo(tEdorNo,tEdorType,tPolNo);
	}
}

function detailUWInfo(tEdorNo,tEdorType,tPolNo)
{
	var i,m,n,j;
	var tReturn;
	var strSql;
	n=0;	
	
	strSql = "select modifydate,uwgrade,uwerror from lpuwError where edorno='"+tEdorNo+"' and edortype='"+ tEdorType+ "' and polno='" +tPolNo+ "' order by uwgrade,modifydate";

	//alert(strSql);
	
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 0, 1);  
	
	 //判断是否查询成功
	
	  if (turnPage.strQueryResult!=false)
	   {
	   	
	   	   //查询成功则拆分字符串，返回二维数组
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);	
	  n = turnPage.arrDataCacheSet.length;
	   }
  
	  if (n>0)
	  	
	  {	
	  	tReturn = "";
	  for (i = 0;i< n;i++)
	  {
	  	m = turnPage.arrDataCacheSet[i].length;
	  	for (j =0;j<m;j++)
	  	{
	  		if (j==1)
	  		{	
	  			if (tReturn!=null&&tReturn!="")
		  			turnPage.arrDataCacheSet[i][j] =  " 核保级别("+ turnPage.arrDataCacheSet[i][j]+")--->";
			  }
	  		tReturn = tReturn + turnPage.arrDataCacheSet[i][j];
	  		
	  		if (j%3==0)
	  		{	if (tReturn!=null&&tReturn!="")
		  			tReturn = tReturn + " 核保结果:";
			}
		}
		if (tReturn!=null&&tReturn!="")
			tReturn = tReturn + "；" +"\n";
	  }
	}
	if (tReturn==""||tReturn==null)
		tReturn = "无";
	//alert(tReturn);
	fm.all('AutoUWIdea').value = tReturn;
	fm.all('UWstate').value = "";
	fm.all('ManuUWIdea').value = "";
	fm.all('EdorNo').value = tEdorNo;
	fm.all('EdorType').value = tEdorType;
	fm.all('PolNo').value = tPolNo;
}
	
function getQueryResult()
{
	var arrSelected = null;
	tRow = PEdorMainGrid.getSelNo();
	//alert(tRow);
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	
	return arrSelected;
}
// 数据返回父窗口
function returnParent()
{
	if (LoadFlag =='G')
	{	
		//alert("LoadFlag:"+LoadFlag);
		top.close();
	}
	else
		alert("请选择其他菜单操作!");	
}