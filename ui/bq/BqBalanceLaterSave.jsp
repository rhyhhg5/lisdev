<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BqBalanceLaterSave.jsp
//程序功能：延期结算处理类
//创建日期：2005-09-07 10:56:43
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%
	String FlagStr;
	String Content;
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	String GrpContNo = request.getParameter("GrpContNo");
	String DetailWorkNo = request.getParameter("AcceptNo");
	String laterPayDate = request.getParameter("laterPayDate");
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("GrpNo",GrpContNo);
	tTransferData.setNameAndValue("DetailWorkNo",DetailWorkNo);
	tTransferData.setNameAndValue("LaterPayDate",laterPayDate);
	
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tTransferData);
	

	GrpBalanceOnLaterBL tGrpBalanceOnLaterBL= new GrpBalanceOnLaterBL();
	if(!tGrpBalanceOnLaterBL.submitData(tVData, ""))
	{
		FlagStr = "Fail";
		Content = tGrpBalanceOnLaterBL.mErrors.getFirstError();
	}
	else
	{		
	  //设置显示信息
		FlagStr = "Succ";
		Content = "数据保存成功!";
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>