<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     GlobalInput tG = new GlobalInput();
     tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
     System.out.println("-----"+tG.ManageCom);
 %>    
<html>
<%
	String tGrpPolNo = "";
	String tGrpEdorNo = "";
	String tGrpEdorType = "";
	
	try
	{
		tGrpPolNo = request.getParameter("GrpPolNo");
		tGrpEdorNo = request.getParameter("GrpEdorNo");
		tGrpEdorType = request.getParameter("GrpEdorType");
	 }
	catch( Exception e )
	{
		tGrpPolNo = "";
		tGrpEdorNo = "";
		tGrpEdorType = "";
	}

//得到界面的调用位置,默认为1,表示个人保单申请直接录入.
// I -- 个人保单保全核保
// G -- 集体下个人保全核保

	String tLoadFlag = "";
	try
	{
		tLoadFlag = request.getParameter( "LoadFlag" );
		//默认情况下为个人保单直接录入
		if( tLoadFlag == null || tLoadFlag.equals( "" ))
			tLoadFlag = "I";
	}
	catch( Exception e1 )
	{
		tLoadFlag = "I";
	}
System.out.println("LoadFlag:" + tLoadFlag);
%>
<head>
<script>
	var LoadFlag = "<%=tLoadFlag%>"; //判断从何处进入保单申请界面,该变量需要在界面出始化前设置.
	var GrpPolNo = "<%=tGrpPolNo%>";
	var GrpEdorNo = "<%=tGrpEdorNo%>";
	var GrpEdorType = "<%=tGrpEdorType%>";
</script>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="./GPPEdorManuUW.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GPPEdorManuUWInit.jsp"%>
    <%@include file = "ManageComLimit.jsp"%>

  <title>保全团单下个单人工核保 </title>
</head>
<body  onload="initForm();" >

  <form action="./GPPEdorManuUWSubmit.jsp" method=post name=fm target="fraSubmit">
    <!-- 个人信息部分 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPQuery);">
      </td>
      <td class= titleImg>
        请您输入查询条件：
      </td>
    	</tr>
    </table>
	
   <Div id = "divGrpEdor" style = "display: none">
    	<table class = common>
    	<tr>
    	<td class = title>
    		集体保单号
    		</td>
    	<TD  class= input>
            <Input class= readonly readonly name=GrpPolNo >
        </TD>	
    	<td class = title>
    		申请批单号
    		</td>
    	<TD  class= input>
            <Input class= readonly  readonly name=GrpEdorNo >
        </TD>	
        <td class = title>
    		保全项目
    	</td>
    	<TD  class= input>
            <Input class= readonly  readonly name=GrpEdorType >
        </TD>	
        </tr>
        </table>
     </Div>  
     
    <Div  id= "divLPQuery" style= "display: ''">
      <table  class= common>
       	 <TR class=common>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class= common name=PolNo >
          </TD>
        </TR>
        <TR class=common>
          <TD  class= title>
            申请批单号
          </TD>
          <TD  class= input>
            <Input class= common name=EdorNo >
          </TD>
        </TR>
      </table>
       <INPUT VALUE="查询" TYPE=button class=common onclick="easyQueryClick();"> 
    </Div>
         
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEdorManuUW);">
    		</td>
    		<td class= titleImg>
    			 个人批改信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divEdorManuUW" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  				<span id="spanPEdorMainGrid" >
  				</span> 
  			</td>
  		</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button class=common onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button class=common onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button class=common onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button class=common onclick="getLastPage();"> 	
      	
      	<table>
  	 <TR>
  	 <!--
     	  <TD class = input>
     	  
     	  <Div  id= "divBTedorManuUW" style= "display: ''">
     	 	<Input id = "BTedorManuUW" style= "display: ''"  type=Button value= "申请核保确认" onclick ="edorManuUW();">
	  </Div> 
	 </TD>
	 --> 
	 	<td class = titleImg width = 26%>
    		当前核保员级别为&nbsp<Input class = "readonly" readonly name=uwgrade >&nbsp级。
    	</td>
	    </tr>
	    <tr>	 
     	 <TD>
  		<INPUT VALUE="返回" TYPE=button class=common onclick="returnParent();"> 
    	</TD>	
    	</TR>
    	</table>			
  </div>
  <div id="DivEdorManuUWAct" style="display : none">
  <table class = common>
  
    	<tr>
        	<td class= titleImg>
    			 个单保全人工核保操作:
    		</td>
    	</tr>
    		
    	
 	<tr><td class = title>
	自动核保未通过原因:
	</td></tr>	
	<tr>
	<td class = input>
	<textarea name="AutoUWIdea" cols="80" rows="4" readonly="readonly" dir="ltr"></textarea> 
  	</td>
	</tr>	
	<TR >
          
        <TD  class= title> 核保结论: </TD>
		</tr>
		<tr>
          <TD  class= input>
            <Input class="code" name=UWState ondblclick="return showCodeList('edoruwstate',[this]);" onkeyup="return showCodeListKey('edoruwstate',[this]);">
          </TD>
	</tr>
	<tr>
		  <TD  class= title>
            人工核保意见:
          </TD>
	</tr>
    <tr>
	<td class = input>
		    <textarea name="ManuUWIdea" cols="80" rows="4" dir="ltr"></textarea>
	</td>
	</TR>	
	</table>
	<Input type=Button value= "核保确认" class=common onclick ="edorTypeManuUW();">

  </div>
  	<input type=hidden id="EdorType" name="EdorType">
    <input type=hidden id="ContType" name="ContType">
    <input type=hidden id="fmtransact" name="fmtransact">
   	<input type=hidden id="uwGradeFlag" name="uwGradeFlag">

  <input type=hidden id="LoadFlag" name="LoadFlag">	
    </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>

</html>
