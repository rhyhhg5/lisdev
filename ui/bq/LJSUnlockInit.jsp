
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!--引入必要的java类  -->
<%@page import="com.sinosoft.lis.pubfun.*"%>	
<% 
  //获取传入的参数
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
  String tCurrentDate = PubFun.getCurrentDate();
  String tCurrentTime = PubFun.getCurrentTime();
%>
<script language="JavaScript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  var comcode = "<%=tGI.ComCode%>";//记录登陆机构
  var operator = "<%=tGI.Operator%>"; //记录当前登录人
  var tCurrentDate = "<%=tCurrentDate%>";
  var tCurrentTime = "<%=tCurrentTime%>";
 
//输入框的初始化 
function initInpBox()
{
  try
  {
	fm.all('ManageCom1').value = manageCom;
	fm.all('ManageCom2').value = manageCom;
	showAllCodeName();
  }
  catch(ex)
  {
    alert("在LJSUnlockInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

//初始化表单
function initForm()
{ 
  try 
  {
    initInpBox();
    initLJSPayBQGrid(); 
    initLJAGetBQGrid();
  }
  catch(re) 
  {
    alert("在LJSUnlockInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

//保全收费
var LJSPayBQGrid;
function initLJSPayBQGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=0;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="管理机构";      //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="工单号";      //列名
    iArray[2][1]="110px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[3]=new Array();
    iArray[3][0]="收费凭证号";      //列名
    iArray[3][1]="90px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="收费金额";      //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[5]=new Array();
    iArray[5][0]="帐号名";      //列名
    iArray[5][1]="70px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[6]=new Array();
    iArray[6][0]="转账银行";      //列名
    iArray[6][1]="120px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="提盘日期";      //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[8]=new Array();
    iArray[8][0]="提盘次数";      //列名
    iArray[8][1]="50px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="转账失败原因";      //列名
    iArray[9][1]="50px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    LJSPayBQGrid = new MulLineEnter("fm", "LJSPayBQGrid"); 
	  //设置Grid属性
    LJSPayBQGrid.mulLineCount = 0;
    LJSPayBQGrid.displayTitle = 1;
    LJSPayBQGrid.locked = 1;
    LJSPayBQGrid.canSel = 1;	
    LJSPayBQGrid.canChk = 0;
    LJSPayBQGrid.hiddenSubtraction = 1;
    LJSPayBQGrid.hiddenPlus = 1;
    LJSPayBQGrid.selBoxEventFuncName = "selectOne1" ;
    LJSPayBQGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在LJSUnlockInit.jsp-->initLJSPayBQGrid函数中发生异常:初始化界面错误!");
  }
}

//保全付费
var LJAGetBQGrid;
function initLJAGetBQGrid()     
{                         
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";   //列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="0px";    //列宽
    iArray[0][2]=200;      //列最大值
    iArray[0][3]=0;        //是否允许输入,1表示允许，0表示不允许
      
    iArray[1]=new Array();
    iArray[1][0]="管理机构";      //列名
    iArray[1][1]="100px";        //列宽
    iArray[1][2]=200;           //列最大值
    iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="工单号";      //列名
    iArray[2][1]="110px";        //列宽
    iArray[2][2]=200;           //列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许
        
    iArray[3]=new Array();
    iArray[3][0]="付费凭证号";      //列名
    iArray[3][1]="90px";        //列宽
    iArray[3][2]=200;           //列最大值
    iArray[3][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="付费金额";      //列名
    iArray[4][1]="80px";        //列宽
    iArray[4][2]=200;           //列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[5]=new Array();
    iArray[5][0]="帐号名";      //列名
    iArray[5][1]="70px";        //列宽
    iArray[5][2]=200;           //列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许
 
    iArray[6]=new Array();
    iArray[6][0]="转账银行";      //列名
    iArray[6][1]="120px";        //列宽
    iArray[6][2]=200;           //列最大值
    iArray[6][3]=0;             //是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="提盘日期";      //列名
    iArray[7][1]="80px";        //列宽
    iArray[7][2]=200;           //列最大值
    iArray[7][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    iArray[8]=new Array();
    iArray[8][0]="提盘次数";      //列名
    iArray[8][1]="50px";        //列宽
    iArray[8][2]=200;           //列最大值
    iArray[8][3]=0;             //是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="转账失败原因";      //列名
    iArray[9][1]="50px";        //列宽
    iArray[9][2]=200;           //列最大值
    iArray[9][3]=0;             //是否允许输入,1表示允许，0表示不允许
      
    LJAGetBQGrid = new MulLineEnter("fm", "LJAGetBQGrid"); 
	  //设置Grid属性
    LJAGetBQGrid.mulLineCount = 0;
    LJAGetBQGrid.displayTitle = 1;
    LJAGetBQGrid.locked = 1;
    LJAGetBQGrid.canSel = 1;	
    LJAGetBQGrid.canChk = 0;
    LJAGetBQGrid.hiddenSubtraction = 1;
    LJAGetBQGrid.hiddenPlus = 1;
    LJAGetBQGrid.selBoxEventFuncName = "selectOne2" ;
    LJAGetBQGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
  	alert("在LJSUnlockInit.jsp-->initLJAGetBQGrid函数中发生异常:初始化界面错误!");
  }
}

</script>
