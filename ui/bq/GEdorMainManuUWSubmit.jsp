<%
//程序名称：PEdorConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
	<%@page import="com.sinosoft.utility.*"%>
	<%@page import="com.sinosoft.lis.schema.*"%>
	<%@page import="com.sinosoft.lis.vschema.*"%>
	<%@page import="com.sinosoft.lis.db.*"%>
	<%@page import="com.sinosoft.lis.vdb.*"%>
	<%@page import="com.sinosoft.lis.sys.*"%>
	<%@page import="com.sinosoft.lis.bq.*"%>
	<%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
  PGrpEdorManuUWUI tPGrpEdorManuUWUI   = new PGrpEdorManuUWUI();
 
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
    
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  
 
  //个人批改信息
 tLPGrpEdorMainSchema.setGrpContNo(request.getParameter("GrpContNo"));
 tLPGrpEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
 tLPGrpEdorMainSchema.setUWState(request.getParameter("MainUWState"));
 	
 try
  {
  // 准备传输数据 VData
  
	VData tVData = new VData();   
	 //保存个人保单信息(保全)	
	 
  	tVData.addElement(tLPGrpEdorMainSchema);
    tVData.addElement(tG);
       	
	tPGrpEdorManuUWUI.submitData(tVData,transact);
	
    }
	catch(Exception ex)
	{
		Content = transact+"失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
	}			
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
	tError = tPGrpEdorManuUWUI.mErrors;
	if (!tError.needDealError())
	{                          
	  Content = " 保存成功。";
		FlagStr = "Success";
	}
	else                                                                           
	{
		Content = " 核保失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
	}
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

