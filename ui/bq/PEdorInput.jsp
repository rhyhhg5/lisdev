<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  

<head >
	 <%@include file="../common/jsp/UsrCheck.jsp"%>
	 <%@include file="../common/jsp/AccessCheck.jsp"%>
 
		<%
     GlobalInput tG = new GlobalInput();
     tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
     System.out.println("-----"+tG.ManageCom);
  	%>    
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  
  <SCRIPT src="./PEdorInput.js"></SCRIPT>
  <SCRIPT src="PEdor.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/OperateButton1.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  <%@include file="PEdorInputInit.jsp"%>



</head>
<body  onload="initForm();" >
  <form action="./LCPolQueryDir.jsp" method=post name=fm1 target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title width="11%"> 保单号</TD>
      <TD  class= input width="26%"> 
        <input class=common name=ContNo verify="个人保单号|NOTNULL" >
      </TD>
      <TD  class= input width="26%"> 
        <Input type=Button value="查询" class=cssButton onclick="queryCont()">
      </TD>
    </TR>
  </TABLE> 
 </form>
  <form action="./PEdorSave.jsp" method=post name=fm target="fraSubmit">
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
      </td>
      <td class= titleImg>
        申请保单信息
      </td>
    	</tr>
    </table>


    <Div  id= "divLCPol" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            保单号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=ContNo >
          </TD>
          <TD  class= title>
            投保人
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AppntName >
          </TD>
          <TD  class= title>
            主被保人
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=InsuredName >
          </TD>
        </TR>        
        <TR>
          <TD  class= title>
            实际保费
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Prem >
          </TD>
          <TD  class= title>
            实际保额
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=Amnt >
          </TD>
          <TD  class= title>
            业务员
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=AgentCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保单送交日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=GetPolDate >
          </TD>
          <TD  class= title>
            生效日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=ValiDate >
          </TD>           
          <TD  class= title>
            交至日期
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=PaytoDate >
          </TD>       
        </TR>
      </table>
     
    </Div>
    <Div  id= "divLPEdorApp" style= "display: ''">
    <tr class=common>
        <td class=title>
            保全受理号
        </td>
        <td>
            <Input class="readonly" readonly name=EdorAcceptNo>
        </td>
        <td class=title>
            申请人姓名
        </td>
        <td>
            <Input class="readonly" readonly name=EdorAppName>
        </td>
        <td class=title>
            申请方式
        </td>
        <td>
            <Input class="readonly" readonly name=AppType>
        </td>
    </tr>
    <tr class=common>
        <td class=title>
            申请号码
        </td>
        <td>
            <Input class="readonly" readonly name=OtherNo>
        </td>
        <td class=title>
            申请号码类型
        </td>
        <td>
            <Input class="readonly" readonly name=OtherNoType>
        </td>
    </tr>
</Div>   
    <table class = common>
        <TR>
            <TD  class= input width="26%"> 
                <div id = "divappconfirm" style = "display:none">
                    <Input class=cssButton type=Button value="申请确认" onclick="edorAppConfirm()">
                </div>
            </TD>
		 
        </TR>
    </table>
   
   
    <!--个人保全信息部分（列表） -->
    <table>
    	<tr>
        <td class=common>
	    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPPol);">
    	</td>
    	<td class= titleImg>
    		 保全申请
    	</td>
    	</tr>
    </table>

    
    
	<Div  id= "divLPPol" style= "display: ''">
    	<table  class= common>
        <tr class=common>
            <td class=title>
                批单号
            </td>
            <td class= input>
                <Input class="readonly" readonly name=EdorNo>
            </td>
            <TD  class= title>
                操作员
            </TD>
            <TD  class= input>
                <Input class= "readonly" readonly name=Operator >
            </TD>
            <TD  class= title>
                保全申请日期
            </TD>
            <TD  class= input>
                <Input class= "coolDatePicker" dateFormat="short" name=EdorAppDate >
            </TD>
            
            
        </tr>
        <tr  class= common>
    	  	 <TD  class= title>
            		变动保费
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=ChgPrem >
          	 </TD>
          	 <TD  class= title>
            		补/退费金额
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=GetMoney >
          	 </TD>
          	 
            <TD  class= title>
                保全生效日期
            </TD>
            <TD  class= input>
                <Input class= "coolDatePicker" dateFormat="short" name=EdorValiDate >
            </TD>
		</tr>
    </table>
<table>
      <tr>
       <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
       </td>
       <td class= titleImg>
        保全项目信息
       </td>
      </tr>
</table>
<Div  id= "divEdorItemGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanEdorItemGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>					
</div>
<!--table>
    	<tr>
           <td class=common>
	      <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGrpPol);">
    	   </td>
    	   <td class= titleImg>
    	      保全申请
    	   </td>
    	</tr>
</table-->
        <Div  id= "divLPGrpPol" style= "display: ''">
    	   <table  class= common>
    		
        	<tr  class= common>
    	 	 <TD  class= title>
            		批改类型
          	 </TD>
          	 <TD  class= input>
          		  <!--<Input class= "code" name=EdorType  ondblclick="return showCodeList('EdorCode',[this], null, null, RiskCode, 'RiskCode');" onkeyup="return showCodeListKey('EdorCode',[this], null, null, RiskCode, 'RiskCode');" >-->
          			<Input class= "code" name=EdorType verify="批改类型|notnull&code:EdorCode" ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);" >

          	 </TD>
          	 <TD  class= title>
            		操作员
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=Operator1 >
          	 </TD>
		</tr>
		<!--tr  class= common>
    	  	 <TD  class= title>
            		变动保费
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=ChgPrem >
          	 </TD>
          	 <TD  class= title>
            		补/退费金额
          	 </TD>
          	 <TD  class= input>
            		<Input class= "readonly" readonly name=GetMoney >
          	 </TD>
		</tr>
	
     	 	</TR-->
       	</table>
     </div>
    <Div  id= "divInsuredInfo" style= "display: 'none'">
        <table>
            <tr>
                <td>
                 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
                </td>
            <td class= titleImg>
                被保险人信息
            </td>
        </tr>
        </table>
        <Div  id= "divInsuredGrid" style= "display: ''">
      	    <table  class= common>
       		    <tr  class= common>
      	  		    <td text-align: left colSpan=1>
  				    	<span id="spanInsuredGrid" >
  				    	</span> 
  			  	    </td>
  			    </tr>
    	    </table>					
    </div>
    </Div>
    <Div  id= "divPolInfo" style= "display: 'none'">
        <table>
            <tr>
                <td>
                    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCGrpPol);">
                </td>
                <td class= titleImg>
                    保单险种信息
                </td>
            </tr>
        </table>
    <Div  id= "divPolGrid" style= "display: ''">
        <table  class= common>
        	<tr  class= common>
          		<td text-align: left colSpan=1>
        			<span id="spanPolGrid" >
        			</span> 
        	  	</td>
        	</tr>
        </table>					
    </div>
</DIV>

<Div  id= "divAddDelButton" style= "display: ''" align=right>

        <input type =button class=cssButton value="添加保全项目" onclick="addRecord();"> 
        <!--input type =button class=cssButton value="删除保全项目" onclick="deleteRecord();"-->
        <Input type =button class=cssButton value="保全项目明细" onclick="detailEdorType();">

</DIV> 		
			<!--table class = common>
					<TR class= common>
	          <TD  class= input width="26%"> 
	          <div id= "divedortype" style= "display:none">
	       		 <Input class= common type=Button value="添加项目" onclick="insertEdorType()">
	     			</div>
	     	 </TD>
	     	  <TD  class= input width="26%"> 
	     	  	<div id = "divdetail" style = "display:none">
	       		 <Input class= common type=Button value="项目明细" onclick="detailEdorType()">
	       		 </div>
	     	 </TD>
	     	 </TR>
	    </table-->
	    
   	 <input type=hidden id="currentDay" name="currentDay">
   	 <input type=hidden id="dayAfterCurrent" name="dayAfterCurrent">
	 
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id ="ContType" name = "ContType">
	  <input type=hidden id ="EdorState" name = "EdorState">
	  <input type=hidden id ="GrpContNo" name = "GrpContNo">
	  <input type=hidden id ="DisplayType" name = "DisplayType">
	  <input type=hidden id ="CustomerNoBak" name = "CustomerNoBak">
	  <input type=hidden id ="ContNoBak" name = "ContNoBak">
	  
	  
      <!--<input type=hidden id ="RiskCode" name = "RiskCode">-->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
