<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BqBalanceTimeSave.jsp
//程序功能：日处理数据保存页面
//创建日期：2005-03-15 10:56:43
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.task.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%
	String FlagStr;
	String Content;

	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	String GrpContNo = request.getParameter("GrpContNo");
	String DetailWorkNo = request.getParameter("AcceptNo");
	String PayMode = request.getParameter("PayMode");
	String Bank = request.getParameter("Bank");
	String BankAccno = request.getParameter("BankAccno");
	String AccName = request.getParameter("AccName");
	String PayDate = request.getParameter("PayDate");
	String PayEndDate = request.getParameter("PayEndDate");
	
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("GrpNo",GrpContNo);
	tTransferData.setNameAndValue("DetailWorkNo",DetailWorkNo);
	tTransferData.setNameAndValue("PayMode",PayMode);
	tTransferData.setNameAndValue("Bank",Bank);
	tTransferData.setNameAndValue("BankAccno",BankAccno);
	tTransferData.setNameAndValue("AccName",AccName);
	tTransferData.setNameAndValue("PayDate",PayDate);
	tTransferData.setNameAndValue("PayEndDate",PayEndDate);
	
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tTransferData);
	
	
	GrpBalanceOnTimeBL tGrpBalanceOnTimeBL= new GrpBalanceOnTimeBL();
	if(!tGrpBalanceOnTimeBL.submitData(tVData, ""))
	{
		FlagStr = "Fail";
		Content = tGrpBalanceOnTimeBL.mErrors.getFirstError();
	}
	else
	{		
	  //设置显示信息
		FlagStr = "Succ";
		Content = "数据保存成功!";
	}
%> 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

