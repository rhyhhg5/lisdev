<%
//程序名称：PEdorTypeIASubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----IAsubmit---");
  LPAppntIndSchema tLPAppntIndSchema   = new LPAppntIndSchema();
  LPEdorMainSchema tLPEdorMainSchema   = new LPEdorMainSchema();
  PEdorIADetailUI tPEdorIADetailUI   = new PEdorIADetailUI();
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result="";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact:"+transact);
	GlobalInput tG = new GlobalInput();
	    System.out.println("------------------begin ui");
	tG = (GlobalInput)session.getValue("GI");
  
  
  //个人批改信息
    tLPEdorMainSchema.setPolNo(request.getParameter("PolNo"));
    tLPEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPEdorMainSchema.setEdorType(request.getParameter("EdorType"));


    //新投保人信息
	tLPAppntIndSchema.setPolNo(request.getParameter("PolNo"));
    tLPAppntIndSchema.setEdorNo(request.getParameter("EdorNo"));

	tLPAppntIndSchema.setEdorType(request.getParameter("EdorType"));

	tLPAppntIndSchema.setCustomerNo(request.getParameter("AppntCustomerNo"));
	tLPAppntIndSchema.setName(request.getParameter("AppntName"));
	tLPAppntIndSchema.setSex(request.getParameter("AppntSex"));
	tLPAppntIndSchema.setBirthday(request.getParameter("AppntBirthday"));
	
	tLPAppntIndSchema.setRelationToInsured(request.getParameter("AppntRelationToInsured"));
	
	tLPAppntIndSchema.setIDType(request.getParameter("AppntIDType"));
	tLPAppntIndSchema.setIDNo(request.getParameter("AppntIDNo"));
	
	
	tLPAppntIndSchema.setNativePlace(request.getParameter("AppntNativePlace"));
	tLPAppntIndSchema.setRgtAddress(request.getParameter("AppntRgtAddress"));

	tLPAppntIndSchema.setPostalAddress(request.getParameter("AppntPostalAddress"));
	tLPAppntIndSchema.setZipCode(request.getParameter("AppntZipCode"));
	tLPAppntIndSchema.setHomeAddress(request.getParameter("AppntHomeAddress"));
	tLPAppntIndSchema.setHomeZipCode(request.getParameter("AppntHomeZipCode"));
	tLPAppntIndSchema.setPhone(request.getParameter("AppntPhone"));
	tLPAppntIndSchema.setPhone2(request.getParameter("AppntPhone2"));
	tLPAppntIndSchema.setMobile(request.getParameter("AppntMobile"));
	tLPAppntIndSchema.setEMail(request.getParameter("AppntEMail"));
    
try
  {
  // 准备传输数据 VData
  
    VData tVData = new VData();  
    //保存个人保单信息(保全)	
    tVData.addElement(tG);
    tVData.addElement(tLPEdorMainSchema);
    tVData.addElement(tLPAppntIndSchema);

    if (transact.equals("INSERT||MAIN")) {
  	//旧投保人信息
  	LPAppntIndSchema tOldLPAppntIndSchema = new LPAppntIndSchema();
	tOldLPAppntIndSchema.setPolNo(request.getParameter("PolNo"));
        tOldLPAppntIndSchema.setEdorNo(request.getParameter("EdorNo"));
	tOldLPAppntIndSchema.setEdorType(request.getParameter("EdorType"));
	
	String tRadio[] = request.getParameterValues("InpLCAppntIndGridSel"); 
		    
	System.out.println("here");
		    
        int index = 0;  
        for(;index<tRadio.length;index++)  {
    	    if(tRadio[index].equals("1"))           
        	break;             
	}
        System.out.println("index : " + index);
		    
	if (index == tRadio.length) {
	    System.out.println("没有选中的投保人");
	    return;
	}
			
	String tCustomerNoArr[] = request.getParameterValues("LCAppntIndGrid1"); 
	System.out.println("tCustomerArr:" + tCustomerNoArr[0]);

	tOldLPAppntIndSchema.setCustomerNo(tCustomerNoArr[0]);
    	    
    	tVData.addElement(tOldLPAppntIndSchema);
    }
    	
    tPEdorIADetailUI.submitData(tVData,transact);
	
 } catch(Exception ex) {
	  Content = transact+"失败，原因是:" + ex.toString();
          FlagStr = "Fail";
 }			
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorIADetailUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Success";
    	if (transact.equals("QUERY||MAIN")||transact.equals("QUERY||DETAIL"))
    	{
    	if (tPEdorIADetailUI.getResult()!=null&&tPEdorIADetailUI.getResult().size()>0)
    	{
    		Result = (String)tPEdorIADetailUI.getResult().get(0);
    		if (Result==null||Result.trim().equals(""))
    		{
    			FlagStr = "Fail";
    			Content = "提交失败!!";
    		}
    	}
    	}
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
//    alert("will afterSubmit");
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=Result%>");
</script>
</html>

