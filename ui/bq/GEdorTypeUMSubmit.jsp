<%
//程序名称：PEdorTypeWTSubmit.jsp
//程序功能：
//创建日期：2008-2-25 10:16
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
 <%@page import="com.sinosoft.lis.bq.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  
  
  CErrors tError = null;
  //后面要执行的动作：添加，修改
 
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput)session.getValue("GI");

	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String GrpcontNo = request.getParameter("GrpContNo");
	String InsuredNo = request.getParameter("InsuredNo");
	String InsuredName = request.getParameter("InsuredName");
	String GrpSum = request.getParameter("GrpSum");
	String InsuredSum = request.getParameter("InsuredSum");
	String Sum = request.getParameter("Sum");
	
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setGrpContNo(GrpcontNo);
	tLPEdorItemSchema.setContNo("000000");
	tLPEdorItemSchema.setEdorType(edorType);
	tLPEdorItemSchema.setInsuredNo(InsuredNo);
	tLPEdorItemSchema.setPolNo("000000");
	
	EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo, edorType);
	tSpecialData.setGrpPolNo("000000");
	tSpecialData.setPolNo("000000");
	tSpecialData.add("INSUREDNO", InsuredNo);
	tSpecialData.add("INSUREDNAME", InsuredName);
	tSpecialData.add("GRPSUM", GrpSum);
	tSpecialData.add("INSUREDSUM", InsuredSum);
	tSpecialData.add("SUM", Sum);
    
        // 准备传输数据 VData		
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);
	tVData.add(tSpecialData);
    
    GEdorUMDetailBL tGEdorUMDetailBL   = new GEdorUMDetailBL();
		if (!tGEdorUMDetailBL.submitData(tVData, ""))
		{
			VData rVData = tGEdorUMDetailBL.getResult();
			System.out.println("Submit Failed! " + tGEdorUMDetailBL.mErrors.getErrContent());
			Content = "失败，原因是:" + tGEdorUMDetailBL.mErrors.getFirstError();
			FlagStr = "Fail";
		}
		else 
		{
			Content = "保存成功";
			FlagStr = "Success";
		} 

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

