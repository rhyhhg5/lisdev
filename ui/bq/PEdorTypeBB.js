//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var arrResult;
var arrResult1;
var turnPage = new turnPageClass(); 
var addrFlag="MOD";

function edorTypeBBReturn()
{
	top.close();
}
function edorTypeBBSave()
{ fm.all('addrFlag').value=addrFlag;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  //showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 // showSubmitFrame(mDebug);
 fm.all( 'fmtransact').value="INSERT||GRPEDORTYPEBB";
 fm.submit();
  //showSubmitFrame(mDebug);

}

function customerQuery()
{
  //window.open("./LCInsuredQuery.html");
  var newWindow = window.open("./LCInsuredQuery.html","LCInsuredQuery",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  newWindow.focus();
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initPEdorMainGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
		
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    initQuery();
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
}           
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{

	
	if( arrQueryResult != null )
	{
		//arrResult = arrQueryResult;
		fm.all( 'Name').value=arrQueryResult[0][1];
		fm.all( 'Sex' ).value = arrQueryResult[0][2];
		fm.all( 'BirthDay').value = arrQueryResult[0][3];
	        fm.all( 'IDTYpe').value = arrQueryResult[0][4];	
	        fm.all( 'IDNo').value = arrQueryResult[0][5];
	        fm.all('AddressNo').value=arrQueryResult[0][6];

	
	}
}

function afterQuery1( arrQueryResult1 )
{
	if( arrQueryResult1 != null )
	{
		fm.all('HomeAddress').value=arrQueryResult1[0][0];
		fm.all('PostalAddress').value=arrQueryResult1[0][1];
		fm.all('ZipCode').value=arrQueryResult1[0][2];
		fm.all('Phone').value=arrQueryResult1[0][3];
		fm.all('Mobile').value=arrQueryResult1[0][4];			
	}
}







function queryInsuredDetail()

{

	
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	fm.submit();

}

function initQuery()
{
    var strSQL=" select insuredno,name,sex,birthday,idType,idNo,addressno from LPInsured where insuredno='" + fm.CustomerNo.value + "' and contNo='" +fm.ContNo.value + "' and EdorNo='" + fm.EdorNo.value + "' and edortype='" + fm.EdorType.value + "' ";
    arrResult = easyExecSql(strSQL,1,0);
    
    if(arrResult == null)
    {
    	
    	strSQL= " select insuredno,name,sex,birthday,idtype,idno,addressno from LcInsured  where insuredno='" + fm.CustomerNo.value + "' ";
    
      arrResult = easyExecSql(strSQL,1,0);
      if(arrResult == null)
      {
      	alert('未查询到被保人信息');
      }	
      else
    	{
    		
    		afterQuery(arrResult);
    	 
    	}	
    }
    else
    {
    	
    	afterQuery(arrResult);
    }
    
    strSQL= "select homeaddress,postaladdress,zipcode,phone,mobile from lpaddress where  customerno= '" + fm.CustomerNo.value + "' and addressno='" + fm.AddressNo.value +"'  and EdorType='" + fm.EdorType.value + "' ";
    arrResult1 = easyExecSql(strSQL,1,0);
   // alert(arrResult1);
    if(arrResult1 == null)
    {
    	strSQL1 = " select  homeaddress,postaladdress,zipcode,phone,mobile from lcaddress where  customerno= '" + fm.CustomerNo.value + "' and addressno='" + fm.AddressNo.value +"'  ";  
      arrResult1 = easyExecSql(strSQL1,1,0);
      if(arrResult1 == null)
      {
      	alert('未查询到被保人地址信息');
      }
      else
      {
      	addrFlag="NEW";
        afterQuery1(arrResult1);
        setReadOnly('Y');	
      }  
    }  	    
    else
    {
    	addrFlag="MOD";    
      afterQuery1(arrResult1);
      setReadOnly('N');	
    }  

    
}	


function getaddresscodedata()
{
    var i = 0;
    var j = 0;
    var m = 0;
    var n = 0;
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select AddressNo,HomeAddress from LCAddress where CustomerNo ='"+fm.CustomerNo.value+"'"
    + "union select AddressNo,HomeAddress from LpAddress where CustomerNo ='"+fm.CustomerNo.value+"' + and EdorNo='"+fm.EdorNo.value+"'";
    //alert("strsql :" + strsql);
    turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);  
    if (turnPage.strQueryResult != "")
    {
    	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	m = turnPage.arrDataCacheSet.length;
    	for (i = 0; i < m; i++)
    	{
    		j = i + 1;
    		tCodeData = tCodeData + "^" + turnPage.arrDataCacheSet[i][0] + "|" + turnPage.arrDataCacheSet[i][1];
    	}
    }
    //alert ("tcodedata : " + tCodeData);
    //return tCodeData;
    fm.all("AddressNo").CodeData=tCodeData;
}

function setAddr()
{

    if (fm.AddressNo.value=='')
    {
        addrFlag="MOD";
        setReadOnly('N');        
    }
}    


function setReadOnly(flag)
{
   
    var tflag=flag
    if (flag=='Y')
    {
        

        try {fm.all('HomeAddress').readOnly= true; } catch(ex) { };
        
        try {fm.all('PostalAddress').readOnly= true; } catch(ex) { };
        try {fm.all('ZipCode').readOnly= true; } catch(ex) { };

        try {fm.all('Phone').readOnly= true; } catch(ex) { };
        try {fm.all('Mobile').readOnly= true; } catch(ex) { };
        
    }
    else
    {

        try {fm.all('HomeAddress').readOnly= false; } catch(ex) { };
        
        try {fm.all('PostalAddress').readOnly= false; } catch(ex) { };
        try {fm.all('ZipCode').readOnly= false; } catch(ex) { };

        try {fm.all('Phone').readOnly= false; } catch(ex) { };
        try {fm.all('Mobile').readOnly= false; } catch(ex) { };
 

    }
}

function afterCodeSelect( cCodeName, Field )
{
 if(cCodeName=="GetAddressNo")
 {
     var arrResult1;
     var strSQL;
     strSQL="select b.AddressNo,b.HomeAddress,b.PostalAddress,b.ZipCode,b.Phone,b.mobile from LCAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.CustomerNo.value+"'";
   
     arrResult1 = easyExecSql(strSQL);
     //alert(arrResult1[0]);
     
     if (arrResult1 == null)
     {
        addrFlag="MOD";
        strSQL="select b.AddressNo,b.HomeAddress,b.PostalAddress,b.ZipCode,b.Phone,b.mobile from LPAddress b where b.AddressNo='"+fm.AddressNo.value+"' and b.CustomerNo='"+fm.CustomerNo.value+"'";
        arrResult1 = easyExecSql(strSQL);
     }
    else
    {
        addrFlag="NEW";
    }
     
   
     try {fm.all('AddressNo').value= arrResult1[0][0]; } catch(ex) { };
     
     try {fm.all('HomeAddress').value= arrResult1[0][1]; } catch(ex) { };
     try {fm.all('PostalAddress').value= arrResult1[0][2]; } catch(ex) { };
     try {fm.all('ZipCode').value= arrResult1[0][3]; } catch(ex) { };
     try {fm.all('Phone').value= arrResult1[0][4]; } catch(ex) { };
     try {fm.all('Mobile').value= arrResult1[0][5]; } catch(ex) { };

     if (addrFlag=="NEW")
        setReadOnly('Y');
     else if (addrFlag=="MOD")
        setReadOnly('N');
     
  }
}