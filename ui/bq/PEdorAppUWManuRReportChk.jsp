<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PedorAppUWManuRReportChk.jsp
//程序功能：保全人工核保生存调查报告录入
//创建日期：2005-4-12 15:04
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.cbcheck.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.workflow.bq.*"%>
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}

  	//接收信息
  	// 投保单列表
	LPAppRReportSchema tLPAppRReportSchema = new LPAppRReportSchema();
  LPAppRReportItemSet tLPAppRReportItemSet = new LPAppRReportItemSet();
  LPAppRReportItemSchema tLPAppRReportItemSchema ;
  
	TransferData tTransferData = new TransferData();
	String tEdorAcceptNo = request.getParameter("EdorAcceptNo");
	String tContNo = "";
	String tPrtNo = request.getParameter("PrtNo");
	String tName = request.getParameter("name");
	String tCustomerNo = request.getParameter("CustomerNo");
  String tProposalContNo = request.getParameter("ProposalNoHide");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	System.out.println("tSubMissionID"+tSubMissionID);
	String tOperator = request.getParameter("Operator");
	String tFlag = request.getParameter("Flag");
	String tContent = request.getParameter("Content");
	
	
	String sql = "select a.* from Lpedormain a, lpedoritem b"
		             +" where a.edoracceptno='"+tEdorAcceptNo
		             +"' and a.contno = b.contno and a.edorno = b.edorno ";
  LPEdorMainSchema tempSchema = new  LPEdorMainSchema();
		
	LPEdorMainDB tLPEdorMainDB = new LPEdorMainDB();
  LPEdorMainSet tPEdorMainSet = new LPEdorMainSet();		
  tPEdorMainSet = tLPEdorMainDB.executeQuery(sql);   
	//System.out.println(sql);
	tempSchema = tPEdorMainSet.get(1);
	tContNo = tempSchema.getContNo();    

	String tSerialNo[] = request.getParameterValues("InvestigateGridNo");
	String tRReportItemCode[] = request.getParameterValues("InvestigateGrid1");
	String tRReportItemName[] = request.getParameterValues("InvestigateGrid2");
		
	boolean flag = true;
	int ChkCount = 0;
	if(tSerialNo != null)
	{		
		ChkCount = tSerialNo.length;
	}
	System.out.println("count:"+ChkCount);
	if (ChkCount == 0 )
	{
		Content = "生调资料信息录入不完整!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{
		if (!tEdorAcceptNo.equals("")&& !tMissionID.equals(""))
		{
			tLPAppRReportSchema.setContNo(tContNo);
			tLPAppRReportSchema.setContent(tContent);
			tLPAppRReportSchema.setEdorAcceptNo(tEdorAcceptNo);
			tLPAppRReportSchema.setProposalContNo(tProposalContNo);
			tLPAppRReportSchema.setName(tName);
			
			//准备公共传输信息
			tTransferData.setNameAndValue("ContNo",tContNo);
			tTransferData.setNameAndValue("EdorAcceptNo",tEdorAcceptNo);
			tTransferData.setNameAndValue("PrtNo",tPrtNo) ;
			tTransferData.setNameAndValue("CustomerNo",tCustomerNo) ;
			tTransferData.setNameAndValue("MissionID",tMissionID);	
			tTransferData.setNameAndValue("SubMissionID",tSubMissionID);	
			tTransferData.setNameAndValue("LPAppRReportSchema",tLPAppRReportSchema);	
			for(int i=0;i<ChkCount;i++)
			{
				tLPAppRReportItemSchema = new LPAppRReportItemSchema();
				
				tLPAppRReportItemSchema.setRReportItemCode(tRReportItemCode[i]);
				tLPAppRReportItemSchema.setRReportItemName(tRReportItemName[i]);
				tLPAppRReportItemSchema.setProposalContNo(tProposalContNo);
				
				tLPAppRReportItemSet.add(tLPAppRReportItemSchema);
			}
			
			tTransferData.setNameAndValue("LPAppRReportItemSet",tLPAppRReportItemSet);
			System.out.println("len="+tLPAppRReportItemSet.size());
		}
		else
		{
			flag = false;
			Content = "数据不完整!";
		}	
		System.out.println("flag:"+flag);
		try
		{
		  	if (flag == true)
		  	{
				// 准备传输数据 VData
				VData tVData = new VData();
				tVData.add( tTransferData);
				tVData.add( tG );
				
				// 数据传输
				EdorWorkFlowUI tEdorWorkFlowUI   = new EdorWorkFlowUI();
				if (!tEdorWorkFlowUI.submitData(tVData,"0000000022"))//执行承保核保生调工作流节点0000001104
				{
					int n = tEdorWorkFlowUI.mErrors.getErrorCount();
					System.out.println("ErrorCount:"+n ) ;
					Content = " 发核保生调通知书失败，原因是: " + tEdorWorkFlowUI.mErrors.getError(0).errorMessage;
					FlagStr = "Fail";
				}
				//如果在Catch中发现异常，则不从错误类中提取错误信息
				if (FlagStr == "Fail")
				{
				    tError = tEdorWorkFlowUI.mErrors;
				    if (!tError.needDealError())
				    {                          
				    	Content = " 核保人工核保生调成功! ";
				    	FlagStr = "Succ";
				    }
				    else                                                                           
				    {
				    	Content = " 核保人工核保生调失败，原因是:" + tError.getFirstError();
				    	FlagStr = "Fail";
				     }
				}
			} 
		}
		catch(Exception e)
		{
				e.printStackTrace();
				Content = Content.trim()+".提示：异常终止!";
		}
	}

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
