<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%		
    System.out.println("--OmpayBalaPrint.jsp--");
    String FlagStr="";      
    String Content = "";    
    String CurrentDate = PubFun.getCurrentDate();
    String StartDate = request.getParameter("StartDate");
    String EndDate = request.getParameter("EndDate");
    GlobalInput tGI = (GlobalInput)session.getValue("GI");
    String transact = request.getParameter("fmtransact");
    
    int rowcount = Integer.parseInt(request.getParameter("rowcount"));
    String tGridCom[] = request.getParameterValues("OmDayGrid1");
    String tGridRisk[] = request.getParameterValues("OmDayGrid3");
    String tGridBF[] = request.getParameterValues("OmDayGrid5");
    String tGridB[] = request.getParameterValues("OmDayGrid6");
    String tGridLX[] = request.getParameterValues("OmDayGrid7");
    String tGridGL[] = request.getParameterValues("OmDayGrid8");
    String tGridWT[] = request.getParameterValues("OmDayGrid9");
    String tGridCT[] = request.getParameterValues("OmDayGrid10");
    String tGridCM[] = request.getParameterValues("OmDayGrid11");
    String tGridMF[] = request.getParameterValues("OmDayGrid12");
    String tGridLQ[] = request.getParameterValues("OmDayGrid13");
    String tGridSC[] = request.getParameterValues("OmDayGrid14");
    String tGridSW[] = request.getParameterValues("OmDayGrid15");
    String tGridSum[] = request.getParameterValues("OmDayGrid16");
    
    Calendar cal = new GregorianCalendar();
    String year = String.valueOf(cal.get(Calendar.YEAR));
    String month=String.valueOf(cal.get(Calendar.MONTH)+1);
    String date=String.valueOf(cal.get(Calendar.DATE));
    String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String millis = String.valueOf(System.currentTimeMillis());
    String tFileName = year + month + date + hour + min + sec + millis.substring(millis.length()-3, millis.length());
    
    OmnipotenceIntfDataSet tOmnipotenceIntfDataSet = new OmnipotenceIntfDataSet();
    for(int i=0;i<rowcount;i++)
    {
        OmnipotenceIntfDataSchema tOmnipotenceIntfDataSchema = new OmnipotenceIntfDataSchema();
        tOmnipotenceIntfDataSchema.setBussType("O2");
        tOmnipotenceIntfDataSchema.setBussDate(PubFun.getCurrentDate());
        tOmnipotenceIntfDataSchema.setSuffixNo(tGridRisk[i]);
        tOmnipotenceIntfDataSchema.setPolicyValiDate(StartDate);//开始时间
        tOmnipotenceIntfDataSchema.setPolicyStopDate(EndDate);//结束时间
        
        tOmnipotenceIntfDataSchema.setManageCom(tGridCom[i]);
        System.out.println(tGridCom[i]);
        tOmnipotenceIntfDataSchema.setRiskCode(tGridRisk[i]);
        
        tOmnipotenceIntfDataSchema.setS06(tGridBF[i]);//保费
        tOmnipotenceIntfDataSchema.setS07(tGridB[i]);//持续奖励
        tOmnipotenceIntfDataSchema.setS09(tGridLX[i]);//利息
        tOmnipotenceIntfDataSchema.setRemark(tGridGL[i]);//首次管理费
        tOmnipotenceIntfDataSchema.setRemark1(tGridWT[i]);//犹豫期退保
        tOmnipotenceIntfDataSchema.setRemark2(tGridCT[i]);//退保
        tOmnipotenceIntfDataSchema.setRemark3(tGridCM[i]);//退保费用
        tOmnipotenceIntfDataSchema.setRemark4(tGridMF[i]);//管理费
        tOmnipotenceIntfDataSchema.setBussDescribe(tGridLQ[i]);//部分领取
        tOmnipotenceIntfDataSchema.setAgentCode(tGridSC[i]);//重疾保险金
        tOmnipotenceIntfDataSchema.setIntfOperater(tGridSW[i]);//身故保险金
        tOmnipotenceIntfDataSchema.setErrorInfo(tGridSum[i]);//总的
        
        //填充非空字段
        tOmnipotenceIntfDataSchema.setAccBookCode("O2");
        tOmnipotenceIntfDataSchema.setAccBookType("O2");
        tOmnipotenceIntfDataSchema.setYearMonth("O2");
        tOmnipotenceIntfDataSchema.setOperDate(CurrentDate);
        tOmnipotenceIntfDataSchema.setPayGetFlag("O2");
        tOmnipotenceIntfDataSchema.setPayGetMode("O2");
        tOmnipotenceIntfDataSchema.setPayIntv("O2");
        tOmnipotenceIntfDataSchema.setCurrencyType("RMB");
        tOmnipotenceIntfDataSchema.setDebitItem("O2");
        tOmnipotenceIntfDataSchema.setCreditItem("O2");
        tOmnipotenceIntfDataSchema.setDebDirectionother("O2");
        tOmnipotenceIntfDataSchema.setDebDirectionotherValue("O2");
        tOmnipotenceIntfDataSchema.setCreDirectionother("O2");
        tOmnipotenceIntfDataSchema.setCreDirectionotherValue("O2");
        tOmnipotenceIntfDataSchema.setState("O2");
        tOmnipotenceIntfDataSchema.setDataCheckFLag("O2");
        
        //临时数据名
        tOmnipotenceIntfDataSchema.setOtherNo(tFileName);
        tOmnipotenceIntfDataSet.add(tOmnipotenceIntfDataSchema);
    }
    
    
    TransferData tTransferData = new TransferData();
    tTransferData.setNameAndValue("tOmnipotenceIntfDataSet", tOmnipotenceIntfDataSet);
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tTransferData);
	OmDayPrintBL tOmDayPrintBL = new OmDayPrintBL();
	if(!tOmDayPrintBL.submitData(tVData,"save"))
    {
      FlagStr = "Fail";
    }
    else
    {
      FlagStr = "Succ";
    }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSave("<%=FlagStr%>","<%=tFileName%>","<%=transact%>");
</script>
</html>
