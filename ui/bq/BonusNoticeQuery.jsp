<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	//程序名称：BonusNoticeQuery.jsp
	//程序功能：万能账户信息界面
	//创建日期：2007-12-20
	//创建人  ：zhanggm
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	//获取传入的参数
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
	String curDate = PubFun.getCurrentDate();
	//System.out.println(curDate);
%>
<script language="JavaScript">
var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
var tCurrentDate = "<%=curDate%>";
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<!—页面编码方式-->
<!--以下是引入的公共文件-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
<%@include file="BonusNoticeQueryInit.jsp"%>
<SCRIPT src="BonusNoticeQuery.js"></SCRIPT>
<title>分红信息</title>
</head>
<body onload="initForm();">
	<!--通过initForm方法给页面赋初始值-->
	<form name=fm action="./BonusNoticeQueryPrint.jsp" method=post
		target="fraSubmit">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif"
					style="cursor:hand;" OnClick="showPage(this, BonusLis);"></td>
				<td class=titleImg>红利分配清单</td>
			</tr>
		</table>
		<Div id="BonusLis" style="display: ''">
			<table class=common border=0 width=100%>
				<TR class=common>
					<TD class=title>机构</TD>
					<TD class=input><Input class="codeno" name=organcode readonly
						ondblclick="return showCodeList('comcode',[this,organname],[0,1],null,null,null,1);"
						onkeyup="return showCodeListKey('comcode',[this,organname],[0,1],null,null,null,1);"
						verify="管理机构|NOTNULL"><Input class=codename
						name=organname elementtype=nacessary>
					</TD>

					<td class=title>红利领取方式</td>
					<td class=input><input class="codeNo" name="queryType"
						value="1" CodeData="0|^1|现金红利方式^2|累积生息方式"
						ondblclick="return showCodeListEx('queryType',[this,queryTypeName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListEx('queryType',[this,queryTypeName],[0,1],null,null,null,1);"
						verify="红利领取方式|NOTNULL" readonly><Input class="codeName"
						name="queryTypeName" readonly>
					</td>
				</TR>
				<TR class=common>
				</TR>
				<TR class=common>
					<TD class=title>红利应派发起期</TD>
					<TD class=input><Input name=StartDate style="width:160"
						class='coolDatePicker' dateFormat='short' elementtype=nacessary>
					</TD>
					<TD class=title>红利应派发止期</TD>
					<TD class=input><Input name=EndDate style="width:160"
						class='coolDatePicker' dateFormat='short' elementtype=nacessary>
					</TD>
				</TR>
				<TR class=common>
				</TR>
				<TR class=common>
					<td class=title>红利派发情况</td>
					<td class=input><input class="codeNo" name="getType" value="1"
						CodeData="0|^1|尚未派发红利^2|红利已派发" readOnly
						ondblclick="return showCodeListEx('getType',[this,getTypeName],[0,1],null,null,null,1);"
						onkeyup="return showCodeListEx('getType',[this,getTypeName],[0,1],null,null,null,1);"
						readonly elementtype=nacessary verify="红利派发情况|NOTNULL"><Input
						class="codeName" name="getTypeName" readonly>
					</td>
				</TR>
				<BR>
			</table>
			<INPUT VALUE="重 置" TYPE=button style="width:100" Class="cssButton"
				name="query" onclick="initBox();"> <INPUT VALUE="打  印"
				class="cssButton" TYPE="button" onclick="download()"> <br>
		</Div>
		<div id="divInfo" style="display : ''"></div>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</form>
</body>
</html>
