<%
//程序名称：PEdorTypeCMSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
  <%
  String flag = "";
  String content = "";
  String typeFlag = request.getParameter("TypeFlag");
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  
  String startDate = request.getParameter("ImpartCheck5");
  String getWay = request.getParameter("getWay");
  String GrpContNo = request.getParameter("GrpContNo");
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("startDate",startDate);
  tTransferData.setNameAndValue("getWay",getWay);
  tTransferData.setNameAndValue("GrpContNo",GrpContNo);
  
  System.out.println(startDate+"----------------------"+getWay);
  
  LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
  tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
  tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
  tLPEdorItemSchema.setInsuredNo(request.getParameter("AppntNo"));
  
  LDPersonSchema tLDPersonSchema = new LDPersonSchema();
  tLDPersonSchema.setCustomerNo(request.getParameter("CustomerNo"));
  tLDPersonSchema.setName(request.getParameter("Name"));
  tLDPersonSchema.setSex(request.getParameter("Sex"));
  tLDPersonSchema.setBirthday(request.getParameter("Birthday"));
  tLDPersonSchema.setIDType(request.getParameter("IDType"));
  tLDPersonSchema.setIDNo(request.getParameter("IDNo"));
  tLDPersonSchema.setOccupationCode(request.getParameter("OccupationCode"));
  tLDPersonSchema.setOccupationType(request.getParameter("OccupationType"));
  tLDPersonSchema.setMarriage(request.getParameter("Marriage"));
  
  //理赔金账户
  LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
  tLCInsuredSchema.setBankCode(request.getParameter("BankCode2"));
  tLCInsuredSchema.setBankAccNo(request.getParameter("BankAccNo2"));                
  tLCInsuredSchema.setAccName(request.getParameter("AccName2"));	
  tLCInsuredSchema.setRelationToMainInsured(request.getParameter("Relation"));
  tLCInsuredSchema.setRelationToAppnt(request.getParameter("RelationToAppnt"));
  tLCInsuredSchema.setInsuredStat(request.getParameter("InsuredState"));
  tLCInsuredSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
  tLCInsuredSchema.setPosition(request.getParameter("Position"));
  
	System.out.println("InsuredState" + request.getParameter("InsuredState"));
  
  //个人交费账户
  LCContSchema tLCContSchema = new LCContSchema();
  tLCContSchema.setBankCode(request.getParameter("BankCode"));
  tLCContSchema.setBankAccNo(request.getParameter("BankAccNo"));
  tLCContSchema.setAccName(request.getParameter("AccName"));
  
	
  VData data = new VData();
  data.add(typeFlag);
  data.add(gi);
  data.add(tLPEdorItemSchema);
  data.add(tLDPersonSchema);
  data.add(tLCInsuredSchema);
  data.add(tLCContSchema);
  data.add(tTransferData);
  GEdorMCDetailUI tGEdorMCDetailUI = new GEdorMCDetailUI();
  if (!tGEdorMCDetailUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tGEdorMCDetailUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！";
  }
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>