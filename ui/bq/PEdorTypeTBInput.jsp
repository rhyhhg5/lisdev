<html> 
<%   
//程序名称：
//程序功能：保障调整
//创建日期：2005-12-30
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeTB.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeTBInit.jsp"%>
  <title>投保事项变更</title> 
</head>
<body onload="initForm();">
  <form action="./PEdorTypeBFSubmit.jsp" method=post name=fm target="fraSubmit">    
   <table class=common>
   		<tr class= common>
	      <td class="title"> 受理号 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=EdorNo >
	      </td>
	      <td class="title"> 批改类型 </td>
	      <td class="input">
	      	<input class="readonly" type="hidden" readonly name=EdorType>
	      	<input class="readonly" readonly name=EdorTypeName>
	      </td>
	      <td class="title"> 保单号 </td>
	      <td class="input">
	      	<input class = "readonly" readonly name=ContNo>
	      </td>   
    	</tr>
    	<tr class= common>
	      <td class="title"> 投保人 </td>
	      <td class=input>
	        <input class="readonly" type="text" readonly name=AppntName >
	      </td>
	      <td class="title"> 被保人 </td>
	      <td class="input" colspan='5'>
	      	<input class="readonly" type="text"  readonly name=InsuredName>
	      </td>  
    	</tr>
  	</table>
  	
  	<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divDetail);">
	      </td>
	      <td class= titleImg>
	      	投保事项
	      </td>
		  </tr>
		</table>
		<div id= "divDetail" style= "display: ''">
	  	<table class=common>
	   		<tr class= common>
		      <td class="title"> 变更类型 </td>
		      <td class=input>
		        <input type="checkbox" name="SignNameFlag" value="1" onclick="showSignName();"> 客户补签名
		        <input type="checkbox" name="ImpartFlag" value="1" onclick="showImpart();"> 投保告知补遗
		        <input type="checkbox" name="ConditionFlag" value="1" onclick="showCondition();"> 重审承保条件
		        <input type="checkbox" name="RemarkFlag" value="1" onclick="showRemark();"> 修改特别约定
		      </td>
	    	</tr>
	    	<tr class= common id="trSignName" style="display:'none'">
	    		<td class="title"> 客户签名 </td>
		      <td class=input>
		      	<input class="common" type="text" name="SignName" maxlength="30" verify="客户签名|len<=30">
		      	（姓名以逗号分割）
		      </td>
		    </tr>
		    <tr class= common id="trImpart" style="display:'none'">
	    		<td class="title"> 投保告知 </td>
		      <td class=input>
		      	<textarea class="common" name="Impart" cols="111%" rows="3" verify="投保告知|len<=300"></textarea> 
		      </td>
		    </tr>
		   	<tr class= common id="trCondition" style="display:'none'">
	    		<td class="title"> 承保条件 </td>
		      <td class=input>
		      	<textarea class="common" name="Condition" cols="111%" rows="3" verify="承保条件|len<=300"></textarea> 
		      </td>
		    </tr>
		    <tr class= common id="trRemark" style="display:'none'">
	    		<td class="title"> 特别约定 </td>
		      <td class=input>
		      	<textarea class="common" name="Remark" cols="111%" rows="3" verify="特别约定|len<=300"></textarea> 
		      </td>
		    </tr>
	  	</table>
	  </div>
		<br>
	 <Input class=cssButton type=Button value="保  存" onclick="save();">
	 <Input class=cssButton type=Button value="取  消" onclick="initForm();">
	 <Input class=cssButton type=Button value="返  回" onclick="returnParent();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
