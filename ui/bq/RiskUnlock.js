var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage3 = new turnPageClass(); 
var turnPage2 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
  if (showInfo != null) 
  {
    try 
    {
      showInfo.focus();
    }
    catch (ex) 
    {
      showInfo = null;
      alert(ex.message);
    }
  }
}

function querylj(){
	 
	 var arrReturn = getquerynew();
		afterquerynew(arrReturn);
}
function getquerynew(){
		var arrSelected = null;
		var tRow = LJAGetRiskGrid.getSelNo();
		if (tRow == 0 || tRow == null) {
			return arrSelected;
		}
		arrSelected = new Array();
		//设置需要返回的数组
		arrSelected[0] = new Array();
		arrSelected[0] = LJAGetRiskGrid.getRowData(tRow - 1);
		return arrSelected;
}
function afterquerynew(arrQueryResult){
	var arrResult = new Array();
		if (arrQueryResult != null) {
			arrResult = arrQueryResult;
			fm.ActuGetNoR2.value = arrResult[0][2];
			fm.WorkNoR2.value = arrResult[0][1];
			fm.GetMoney2.value = arrResult[0][3];
			fm.BankCodeName2.value = arrResult[0][5];
		}
		var sql = "select (select bankunsuccreason from LYReturnFromBankB where paycode = a.actugetno " +
		"order by SendDate desc fetch first 1 row only), " +
		"paymode,drawer,drawerid,bankcode,bankaccno, accname " +
		"from ljaget a where " +
		"actugetno = '" +
		arrResult[0][2] +
		"' with ur";
		var rs = easyExecSql(sql);
		if (rs) {
			fm.all('Reason2').value = rs[0][0];
			fm.all('PayMode2').value = rs[0][1];
			fm.all('Drawer2').value = rs[0][2];
			fm.all('DrawerID2').value = rs[0][3];
			fm.all('BankCode2').value = rs[0][4];
			fm.all('BankAccNo2').value = rs[0][5];
			fm.all('AccName2').value = rs[0][6];
		}
		showAllCodeName();
}

//查询能够用来打印的非银行转账批单打印的数据
function queryPrintClick(){
  initNoBankPrintGrid();
  var strSql = "select (select name from ldcom where comcode=a.managecom),otherno,actugetno,sumgetmoney, "
               + "(select max(SendDate) from LYReturnFromBankB where PayCode=a.ActuGetNo), SendBankCount "
               + "from LJAGet a "
               + "where 1=1 "
               + getWherePart('ManageCom','ManageCom2','like')
	           + getWherePart('OtherNo','Contno2','=')
               + getWherePart('ActuGetNo', 'ActuGetNo2','=')
               + "   and PayMode <> '4' and othernotype='7' and confdate is null and bankonthewayflag is null "
//			   + " and exists (select 1 from lcpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2') )"
               + " and exists (select 1 from lcpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2') "
               + " union select 1 from lbpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2')) "
               + "order by ActuGetNo with ur";
    turnPage3.pageDivName = "divPage3";
    turnPage3.queryModal(strSql, NoBankPrintGrid);
}

//选择一条数据进行打印
function submitPrint(){
	if(NoBankPrintGrid.mulLineCount == 0)
    {
      alert("请先查询！");
      return false;
    }
	var row = NoBankPrintGrid.getSelNo();

    
	var arrSelected = null;
    tRow = NoBankPrintGrid.getSelNo();
   if( tRow == 0 || tRow == null )
   {
      alert("请选择需要打印的数据!");
      return false;
   }
//   var count = 0;
//	var tChked = new Array();
//	for(var i = 0; i < NoBankPrintGrid.mulLineCount; i++)
//	{
//		if(NoBankPrintGrid.getChkNo(i) == true)
//		{
//			tChked[count++] = i;
//		}
//	}
//	if(tChked.length != 1)
//	{
//		alert("只能选择一条信息进行打印");
//		return false;	
//	}
	var mtActugetno=NoBankPrintGrid.getRowColData(row-1,3);
   var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action="../uw/PDFPrintSave.jsp?Code=RL000&StandbyFlag2="+mtActugetno+"&OtherNoType='7'&OtherNo="+NoBankPrintGrid.getRowColData(row-1,2);
  fm.submit();

}

function returnQuery(){
	if(LJAGetRiskGrid.mulLineCount == 0)
    {
      alert("没有查询到数据");
      return false;
    }
	return true;
}


//提交，保存按钮对应操作
function submitForm()
{

  if(!checkNew1())
  {
    return false;
  }
//  if(!beforeSubmit())
//  {
//    return false;
//  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
 
  fm.submit(); //提交
}

function afterSubmit(FlagStr, content,actugetno)
{
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
//    easyQueryClick();
//    resetDisplay(); 
    initNoBankPrintGrid();
  var strSql = "select (select name from ldcom where comcode=a.managecom),otherno,actugetno,sumgetmoney, "
               + "(select max(SendDate) from LYReturnFromBankB where PayCode=a.ActuGetNo), SendBankCount "
               + "from LJAGet a "
               + "where 1=1 "
               + " and actugetno='"+actugetno+"'"
               + "   and PayMode <> '4' and othernotype='7' and confdate is null and bankonthewayflag is null "
//			   + " and exists (select 1 from lcpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2') )"
               + " and exists (select 1 from lcpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2') "
       		   + " union select 1 from lbpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2')) "
               + "order by ActuGetNo with ur";
    turnPage3.pageDivName = "divPage3";
    turnPage3.queryModal(strSql, NoBankPrintGrid);
	resetDisplay(); 
	initLJAGetRiskGrid();
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  
}

//Click事件，当点击“查询”按钮时触发该函数
function queryClick(){
	if (!easyQueryClick()) {
		return false;
	}
	resetDisplay();
	if (!returnQuery()) {
		return false;
	}
	
}
	function easyQueryClick(){
        initLJAGetRiskGrid();
		var strSql = "select (select Name from LDCom where ComCode = a.ManageCom),   " +
		"OtherNo, ActuGetNo, SumGetMoney, AccName, " +
		"(select BankName from LDBank where BankCode = a.BankCode), " +
		"(select max(SendDate) from LYReturnFromBankB where PayCode=a.ActuGetNo), SendBankCount " +
		"from LJAGet a " +
		"where 1=1 " +
		getWherePart('ManageCom', 'ManageCom2', 'like') +
		getWherePart('OtherNo', 'Contno2', '=') +
		getWherePart('ActuGetNo', 'ActuGetNo2', '=') +
		" and (CanSendBank = '1' or cansendbank is null) and OtherNoType = '7'  and PayMode = '4' and confdate is null " +
//		" and exists (select 1 from lcpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2') )" +
		" and exists (select 1 from lcpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2') " +
		" union select 1 from lbpol b where contno=a.otherno and exists (select 1 from lmriskapp where riskcode=b.riskcode and risktype4='2'))" +
		"order by ActuGetNo with ur";
		turnPage2.pageDivName = "divPage2";
		turnPage2.queryModal(strSql, LJAGetRiskGrid);
		
		return true;
	}
	
	
	
	function beforeQuery(){
	
		var tManageCom2 = trim(fm.ManageCom2.value);
		var tContno2 = trim(fm.Contno2.value);
		var tActuGetNo2 = trim(fm.ActuGetNo2.value);
		if (tManageCom2 == "" && tContno2 == "" && tActuGetNo2 == "") {
			alert("至少一个查询条件不能为空!");
			return false;
		}
		
		return true;
	}
	
	//去左空格
	function ltrim(s){
		return s.replace(/^\s*/, "");
	}
	//去右空格
	function rtrim(s){
		return s.replace(/\s*$/, "");
	}
	//左右空格
	function trim(s){
		return rtrim(ltrim(s));
	}
	
	//进行两次输入的校验
	var theFirstValue = "";
	var theSecondValue = "";
	function confirmBankAccNo(aObject){
		if (theFirstValue != "") {
			theSecondValue = aObject.value;
			if (theSecondValue == "") {
				alert("请再次录入银行帐号！");
				aObject.value = "";
				aObject.focus();
				return;
			}
			if (theSecondValue == theFirstValue) {
				aObject.value = theSecondValue;
				theSecondValue = "";
				theFirstValue = "";
				return;
			}
			else {
				alert("两次录入帐号不符，请重新录入！");
				theFirstValue = "";
				theSecondValue = "";
				aObject.value = "";
				aObject.focus();
				return;
			}
		}
		else {
			theFirstValue = aObject.value;
			theSecondValue = "";
			if (theFirstValue == "") {
				return;
			}
			alert("请重新录入银行帐号！");
			aObject.value = "";
			aObject.focus();
			return;
		}
	}
	
	function resetDisplay(){
		fm.ActuGetNoR2.value = "";
		fm.WorkNoR2.value = "";
		fm.GetMoney2.value = "";
		fm.Reason2.value = "";
		fm.PayMode2.value = "";
		fm.PayModeName2.value = "";
		fm.Drawer2.value = "";
		fm.DrawerID2.value = "";
		fm.BankCode2.value = "";
		fm.BankCodeName2.value = "";
		fm.BankAccNo2.value = "";
		fm.AccName2.value = "";
	}

	function checkNew1(){
//		alert("checkSubmit");
		if (LJAGetRiskGrid.mulLineCount == 0) {
			alert("请先查询！");
			return false;
		}
//		if (trim(fm.ActuGetNoR2.value) == "") {
//			alert("请选择需要修改的数据!");
//			return false;
//		}
		if (fm.PayMode2.value == "4") {
			if (fm.Drawer2.value == "") {
				alert("付费方式为银行转账时对方姓名不能为空!");
				fm.Drawer2.focus();
				return false;
			}
			if (fm.DrawerID2.value == "") {
				alert("付费方式为银行转账时对方身份证号不能为空!");
				fm.DrawerID2.focus();
				return false;
			}
			if (fm.BankCode2.value == "") {
				alert("付费方式为银行转账时对方开户银行不能为空!");
				fm.BankCode2.focus();
				return false;
			}
			if (fm.BankAccNo2.value == "") {
				alert("付费方式为银行转账时对方银行帐号不能为空!");
				fm.BankAccNo2.focus();
				return false;
			}
			if (fm.AccName2.value == "") {
				alert("付费方式为银行转账时对方帐户名不能为空!");
				fm.AccName2.focus();
				return false;
			}
			
		}
		//	  验证是否是银行在途状态
			var sql = "select count(1) from ljaget where actugetno= '" + fm.ActuGetNoR2.value + "' and bankonthewayflag='1' and confdate is null ";
			var rs = easyExecSql(sql);
			if ('0' != rs[0][0]) {
				alert("该笔付费已经银行在途，不能进行付费方式修改");
				return false;
			}
       return true;
	}
	
	function afterCodeSelect(cCodeName, Field){
		try {
			if (cCodeName == "PayMode") {
				if (fm.PayMode1.value != "4") {
					fm.Drawer1.value = "";
					fm.DrawerID1.value = "";
					fm.BankCode1.value = "";
					fm.BankCodeName1.value = "";
					fm.BankAccNo1.value = "";
					fm.AccName1.value = "";
				}
			}
		} 
		catch (ex) {
		}
	}
	////PDF打印提交后返回调用的方法。
function afterSubmit2(FlagStr,Content)
{
//	alert("aaaaaaaaaa");
	//showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if (FlagStr=="Succ"){
//   	alert("bbbbbbb");
   	showInfo.close();
	alert("打印成功");
   }
}
