//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var mOperation = "";
window.onfocus=myonfocus;
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if(verifyInput() == true) {
	  var i = 0;
	  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
      
      if(fm.CertifyNo.value==""||fm.MissionID.value==null||fm.MissionID.value=="")
      {
      	showInfo.close();
      	alert("请点击 ？按钮，通过查询后返回方式来回收单证！");
      	return false;
      }
      
      //确认在回收之前相关的补打的通知书已打印
      /*var strSql = "select count(*) from LOPRTManager where StateFlag<>'1' and  OldPrtSeq in(select oldprtseq from LOPRTManager where PrtSeq='"+fm.CertifyNo.value+"')";
      var arrResult = easyExecSql(strSql);
      //alert(arrResult);
     if (arrResult != null) 
     {   	
     	showInfo.close();
     	alert("该单证的相关副本未打印，请打印后再回收该单证.");
          return false;
      }*/
	  // showSubmitFrame(mDebug);
	  fm.hideOperation.value = "INSERT||MAIN";
	  fm.submit(); //提交
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    content="保存成功！";
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1") {
	parent.fraMain.rows = "0,0,50,82,*";
  }	else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperation = "QUERY||MAIN";
  fm.sql_where.value = "";
  showInfo = window.open("./PEdorSysCertTakeBackQuery.html");
}

function query()
{
	mOperation = "QUERY||MAIN";
	fm.sql_where.value = " StateFlag = '0' ";
	showInfo = window.open("PEdorSysCertTakeBackQuery.html");
}

//查询返回时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
function afterQuery(arrResult)
{
	try {
	  if(arrResult!=null)
	   {	  	   
			fm.CertifyCode.value = arrResult[0][0];
			fm.CertifyNo.value = arrResult[0][1];
			fm.ValidDate.value = arrResult[0][2];
			fm.SendOutCom.value = arrResult[0][3];
			fm.ReceiveCom.value = arrResult[0][4];
			fm.Handler.value = arrResult[0][5];
			fm.HandleDate.value = arrResult[0][6];
			fm.SendNo.value = arrResult[0][9];
			fm.TakeBackNo.value = arrResult[0][10];
			fm.Operator.value = arrResult[0][7];
			fm.MakeDate.value = arrResult[0][8];
			fm.EdorNo.value = arrResult[0][11];
			fm.EdorAcceptNo.value = arrResult[0][11];
			fm.PolNo.value = arrResult[0][12];
			fm.MissionID.value  = arrResult[0][13];
			fm.SubMissionID.value  = arrResult[0][14];
		}
	} catch (ex) {
		alert("在afterQuery中发生错误");
	}
}

