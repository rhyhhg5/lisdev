<html>
<%
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%String TypeFlag = request.getParameter("TypeFlag");
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>

  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypePT.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypePTInit.jsp"%>

  <title>减额处理 </title>
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypePTSubmit.jsp" method=post name=fm target="fraSubmit">
  <table class=common>
  	<TR  class= common>
      <TD class = title > 受理号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorAcceptNo>
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>
    </TR>
  </TABLE>
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
      </td>
      <td class= titleImg>
        险种信息
      </td>
   </tr>
  </table>
    <Div  id= "divPolGrid" style= "display: ''">
        <table  class= common>
        	<tr  class= common>
          		<td text-align: left colspan="2">
        			<span id="spanPolGrid" >
        			</span>
        	  	</td>
        	</tr>
        </table>
    </Div>
   <!--保单的详细信息-->
   <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPol);">
      </td>
      <td class= titleImg>
        保额信息
      </td>
   </tr>
   </table>

    <Div  id= "divPol" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
		       <TD  class= title > 投保人客户号</TD>
		       <TD  class= input >
               <input class="readonly" readonly name=AppntNo >
               </TD>

		       <TD  class= title > 投保人名称</TD>
		       <TD  class= input >
               <input class="readonly" readonly name=AppntName >
               </TD>
  			</tr>
       		<tr  class= common>
		       <TD  class= title > 保额</TD>
		       <TD  class= input >
               <input class="common" name=Amnt >
               </TD>
		       <TD  class= title > 保费</TD>
		       <TD  class= input >
               <input class="readonly" readonly name=Prem >
               </TD>
  			</tr>
  			<tr><td class= title >减保原因</td>
  			<td colspan=3><textarea name="reason" cols="90%" rows="3" class="common" ></textarea></td>
  				
  				</tr>
    	</table>
  	</div>
    <Div  id= "divMult" style= "display: 'none'">
           <table  class= common>
       		<tr  class= common>
		       <TD  class= title > 份数 </TD>
		       <TD  class= input >
               <input class="common" name=Mult>
               </TD>
  			</tr>
  		</table>
   </Div>

	<div id = "RemainAmntDiv" style= "display: none">
      <table class = common>
       		<tr  class= common>
		       <TD  class= title > 退后保额</TD>
		       <TD  class= input >
               <input class="common"  name=RemainAmnt >
               </TD>
            </tr>
       </table>
    </div>
    <div id = "RemainMultiDiv"  style= "display: none">
	   <table class= common>
	     <tr>
	       <TD  class= title > 退后份数</TD>
		   <TD  class= input >
               <input class="common"  name=RemainMulti >
           </TD>
        </tr>
      </table>
   </div>

<Div  id= "divChooseDuty0" style= "display: ''">
	<table>
		<tr>
			<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPPolGrid1);">
			</td>
			<td class= titleImg>
			减保信息
			</td>
		</tr>
	</table>
</Div>
<Div  id= "divLPPolGrid1" style= "display: ''">
    <table  class= common>
    	<tr  class= common>
      		<td text-align: left colspan="2">
    			<span id="spanLPPolGrid" >
    			</span>
    	  	</td>
    	</tr>
    </table>
</Div>
<br>
<Div>
    <Input class= cssButton name=save type=Button value="保  存" onclick="edorTypePTSave()">&nbsp;
    <Input class= cssButton type=Button name=goBack value="返  回" onclick="returnParent()">
</Div>

   <input class="readonly" readonly  type="hidden" name=EdorNo >
	 <input type=hidden id="CalMode" name = "CalMode">
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" value= 1 name="ContType">
	 <input type=hidden id="PolNo" name="PolNo">
	 <input type=hidden id="InsuredNo" name="InsuredNo">
     <input  type="hidden" id="TypeFlag" name="TypeFlag" value="<%=TypeFlag%>"/>
	 <input type=hidden id="RiskCode" name="RiskCode">

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>
</html>
