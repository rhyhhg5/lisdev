//程序名称：ManuUWAll.js
//程序功能：个人人工核保
//创建日期：2005-01-24 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnConfirmPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约人工核保的EasyQuery
 *  描述:查询显示对象是合同保单.显示条件:合同未进行人工核保，或状态为待核保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 书写SQL语句
	var strSQL ="";
	var tSql = "";
	var arrSelected = new Array();
	k++;
	if(operFlag == "1")
	{
		 strSQL = "select t.missionprop1,t.missionprop2,case t.activitystatus when '1' then '未人工核保'  when '3' then '核保已回复' when '2' then '核保未回复' end,case t.activityid when '0000000017' then '保全服务' end,t.missionid,t.submissionid,t.activityid from lwmission t where 1=1 "
								+ " and t.activityid in ('0000000017')"
								+ " and t.defaultoperator ='" +operator+ "'"
								+ " order by t.modifydate asc,t.modifytime asc"
								;
		 tSql = "select count(1) from lwmission t where 1=1 "
		 				+" and t.activityid in ('0000000017') "
		 				+" and t.defaultoperator is null "
		 				;
	}
		
	/**if(operFlag == "2")
	{
		 strSQL = "select t.missionprop2,t.missionprop1,t.missionprop7,'',case t.activityid when '0000002004' then '新契约' end,t.missionid,t.submissionid,t.activityid from lwmission t where 1=1 "
								+ " and t.activityid in ('0000002004')"
								+ " and t.defaultoperator ='" + operator + "'"
								+ " order by t.modifydate asc,t.modifytime asc"
								;	
		 tSql = "select count(1) from lwmission t where 1=1 "
		 				+" and t.activityid in ('0000002004') "
		 				+" and t.defaultoperator is null "
		 				;	
	}
	if(operFlag == "3")
	{
		 strSQL = "select t.missionprop2,t.missionprop1,t.missionprop7,case t.activitystatus when '1' then '未人工核保'  when '3' then '核保已回复' when '2' then '核保未回复' end,case t.activityid when '0000006004' then '询价' end,t.missionid,t.submissionid,t.activityid from lwmission t where 1=1 "
								+ " and t.activityid in ('0000006004')"
								+ " and t.defaultoperator ='" + operator + "'"
								+ " order by t.modifydate asc,t.modifytime asc"
								;		
		 tSql = "select count(1) from lwmission t where 1=1 "
		 				+" and t.activityid in ('0000006004') "
		 				+" and t.defaultoperator is null "
		 				;		
	}*/

	turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 0, 1); 
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  
  fm.UWNo.value = arrSelected[0][0];
	//查询SQL，返回结果字符串
	
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert(turnPage.strQueryResult);
    alert("没有需要核保的保全申请!");
    return "";
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //alert(arrDataSet);
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //查询待核保件数

  return true;
}

/*********************************************************************
 *  执行新契约人工核保的EasyQueryAddClick
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryAddClick()
{
	var tSel = PolGrid.getSelNo();
	var activityid = PolGrid.getRowColData(tSel - 1,7);
	var EdorAcceptNo = PolGrid.getRowColData(tSel - 1,1);
	var MissionID = PolGrid.getRowColData(tSel - 1,5);
	var SubMissionID = PolGrid.getRowColData(tSel - 1,6);

	if(activityid == "0000000017")
	{
		window.location="./PGrpEdorAppManuUWInput.jsp?EdorAcceptNo="+EdorAcceptNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&Type=1";
	}
	//if(activityid == "0000002004")
	//{
	//	window.location="./GroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
	//}
	//if(activityid == "0000006004")
	//{
	//	window.location="../askapp/AskGroupUW.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+activityid; 
	//}
}

/*********************************************************************
 *  申请核保
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyUW()
{
	var cApplyNo = fm.ApplyNo.value;
	
	if(cApplyNo >10)
	{
		alert("每次申请最大数为10件");
		return;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	//fm.ApplyNo.value ="5";     //每次申请为5条
	fm.action = "./EdorManuUWAllChk.jsp";
  fm.submit(); //提交
}

/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  

  if (FlagStr == "Fail" )
  {                 
    alert(content);
  }
  else
  { 
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");     
    //执行下一步操作
  }
  easyQueryClick();

}