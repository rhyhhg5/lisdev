<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initRiskRateGrid();
    queryRiskRateGrid();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
}

var RiskRateGrid;
function initRiskRateGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="年度";         	  		//列名
    iArray[1][1]="60px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="季度";         	
    iArray[2][1]="120px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;   
     
    iArray[3]=new Array();
    iArray[3][0]="风险综合等级";         	
    iArray[3][1]="120px";            	
    iArray[3][2]=200;            		 
    iArray[3][3]=0;    
    
    RiskRateGrid = new MulLineEnter("fm","RiskRateGrid"); 
    //设置Grid属性
    RiskRateGrid.mulLineCount = 10;
    RiskRateGrid.displayTitle = 1;
    RiskRateGrid.locked = 1;
    RiskRateGrid.canSel = 1;
    RiskRateGrid.canChk = 0;
    RiskRateGrid.hiddenSubtraction = 1;
    RiskRateGrid.hiddenPlus = 1;
    RiskRateGrid.selBoxEventFuncName ="onSelected" ;
    RiskRateGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
