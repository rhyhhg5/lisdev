<%
//程序名称：PEdorTypeXTSubmit.jsp
//程序功能：
//创建日期：2005-4-6 
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息
	
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	
	String fmAction = request.getParameter("fmAction");
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	
	//保全项目信息
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
	tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
	tLPEdorItemSchema.setEdorAppNo(request.getParameter("EdorAppNo"));
	tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
	tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
	
	PEdorTPDetailUI tPEdorTPDetailUI = new PEdorTPDetailUI();
	try
	{
	    // 准备传输数据 VData
	    VData tVData = new VData();  
	    tVData.add(tG);
	    tVData.add(tLPEdorItemSchema);
	    tPEdorTPDetailUI.submitData(tVData, fmAction);
	}
	catch(Exception ex)
	{
		Content = fmAction+"失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}			
	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
	    tError = tPEdorTPDetailUI.mErrors;
	    if (!tError.needDealError())
	    {                          
	        Content = " 保存成功";
		    FlagStr = "Success";
	    }
	    else                                                                           
	    {
	    	Content = " 保存失败，原因是:" + tError.getFirstError();
	    	FlagStr = "Fail";
	    }
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

