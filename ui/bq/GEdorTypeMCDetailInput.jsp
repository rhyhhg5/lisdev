<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<% 
//程序名称：GEdorTypeDetailInput.jsp
//程序功能：团体保全明细总页面
//创建日期：2003-12-03 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html> 
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeMCDetail.js"></SCRIPT>
  <%@include file="GEdorTypeMCDetailInit.jsp"%>
  <title>团体保全明细总页面</title> 
</head>

<body  onload="initForm();" >
  <form action="./GEdorTypeDetailSave.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
      <TR  class= common> 
        <TD  class= title > 受理号</TD>
        <TD  class= input > 
          <input class="readonly" readonly name=EdorNo >
        </TD>
        <TD class = title > 批改类型 </TD>
        <TD class = input >
        	<input class = "readonly" type="hidden" readonly name=EdorType>
        	<input class = "readonly" readonly name=EdorTypeName>
        </TD>
        <TD class = title > 集体保单号 </TD>
        <TD class = input >
        	<input class = "readonly" readonly name=GrpContNo>
        </TD>
      </TR>
    </table> 
    <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPInsured);">
        </td>
        <td class= titleImg>
          未做保全被保人信息
        </td>
      </tr>
    </table>
    <table class = common>
      <tr class = common>
        <td class = title>
      		客户号
      	</td>
      	<td class = input>
      		<input class = common  name=InsuredNo1>
          </TD>
        <td class = title>
      	  姓名
      		</td>
      	<td class = input>
      		<input class = common  name=Name>
        </TD>
        <td class = title>
      	  证件号码
      	</td>
      	<td class = input>
      		<input class = common  name=IdNo>
        </TD>  
      </tr>
    </table>
    <input type=button value="查  询" class=cssButton onclick="queryClick();"> 
    <Input type=button value="添加保全"  name="add" class=cssButton onclick="addGEdor();">
    <br><br>
    <Div  id= "divLPInsured" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCInsuredGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage" align=center style= "display: 'none' ">
        <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
        <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 			
      </Div>		
  	</div>
	  <br>
	  <hr> 
	  <br>
	  <table>
      <tr>
        <td>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divLPInsured2);">
        </td>
        <td class= titleImg>
          已做保全被保人信息
        </td>
      </tr>
    </table>
    
    <table class = common>
      <tr class = common>
      	<td class = title>
      		客户号
      		</td>
      	<td class = input>
      		<input class = common  name=InsuredNo2>
          </TD>
        <td class = title>
      		姓名
      		</td>
      	<td class = input>
      		<input class = common  name=Name2>
          </TD>
        <td class = title>
      		证件号码
      		</td>
      	<td class = input>
      		<input class = common  name=IdNo2>
         </TD>  
      </tr>
    </table>
    <input type=button  value="查  询" class=cssButton onclick="queryClick2();"> 
    <Input type=button  value="撤销保全" name="cancel" class=cssButton onclick="cancelGEdor();">
    <Input type=button  value="进入明细" class=cssButton onclick="openGEdorDetail();">  
    <br><br>
	  <Div  id= "divLPInsured2" style= "display: ''">
      <table  class= common>
        <tr  class= common>
          <td text-align: left colSpan=1>
            <span id="spanLCInsured2Grid"></span> 
          </td>
        </tr>
      </table>
    	<Div  id= "divPage2" align=center style= "display: 'none' ">
        <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
        <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">					
      </Div>
  	</div>
	  <hr>
	  <Input type=Button name="saveButton" class=cssButton value="保  存" onclick="save();">
	  <input type=Button name="return" class=cssButton value="返  回" onclick="returnParent();">
    <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	  <input type=hidden id="ContNo" name="ContNo" >
	  <input type=hidden id="AppntNo" name="AppntNo">
	  <input type=hidden id="InsuredNo" name="InsuredNo">
	  <input type=hidden id="ContNoBak" name="ContNoBak">
	  <input type=hidden id="CustomerNoBak" name="CustomerNoBak">
	  <input type=hidden id="ContType" name="ContType">
	  <input type=hidden id="Transact" name="Transact">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>