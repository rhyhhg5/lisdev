<%
//程序名称：PEdorTypeBAInit.jsp
//程序功能：
//创建日期：2008-04-10
//创建人  ：pst
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">  
function initInpBox()
{ 
	//判断是否是在工单查看中查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
 
  	if (flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  	}
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('ContNo').value = top.opener.fm.all('ContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;  
    fm.all('EdorValiDate').value = top.opener.fm.all('EdorValiDate').value;      
    fm.all('EdorAppDate').value = top.opener.fm.all('EdorAppDate').value; 
}
                                       

function initForm()
{
  try
  {
    initInpBox();
    initPolInfo();
    initLJSGrid();
    queryLJS();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

function initLJSGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="80px";         		//列宽
    iArray[1][2]=100;          				//列最大值
    iArray[1][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="应收号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="80px";         		//列宽
    iArray[2][2]=100;          				//列最大值
    iArray[2][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    iArray[2][21]="GetNoticeNo"; 
    
    iArray[3]=new Array();
    iArray[3][0]="险种";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="60px";         		//列宽
    iArray[3][2]=100;          				//列最大值
    iArray[3][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="险种名称";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="200px";         		//列宽
    iArray[4][2]=100;          				//列最大值
    iArray[4][3]=0;            				//是否允许输入,1表示允许，0表示不允许  
    iArray[4][21]="RiskCode"; 
    
    iArray[5]=new Array();
    iArray[5][0]="续期核销时间";    			//列名
    iArray[5][1]="60px";            	//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="保单投保时保额";    			//列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="当期保费金额";    	//列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="续期扣费金额";    	//列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    LJSGrid = new MulLineEnter("fm", "LJSGrid"); 
    LJSGrid.mulLineCount = 0;
    LJSGrid.displayTitle = 1;
    LJSGrid.canSel = 1;
    LJSGrid.canChk = 0;
    LJSGrid.hiddenSubtraction = 1;
    LJSGrid.hiddenPlus = 1;
    LJSGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}
</script>