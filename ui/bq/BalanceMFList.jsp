<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：BalanceMFList.jsp
//程序功能：万能管理费扣除清单查询
//创建日期：2007-12-17 10:15上午
//创建人  ：YangYalin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">  <!—页面编码方式-->
<!--以下是引入的公共文件-->
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<!--以下是包含进来的新开发功能的*.js和*Init.jsp文件-->
  <SCRIPT src="BalanceMFList.js"></SCRIPT> 
  <%@include file="BalanceMFListInit.jsp"%> 
 
  
  <title>万能管理费扣除清单查询</title>
</head>
<body  onload="initForm();" > 
<!--通过initForm方法给页面赋初始值-->
  <form action="BalanceListSave.jsp" method=post name=fm target="fraSubmit">
    <table  class= common align=center>
      <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class="codeNo"  name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary ></td>
        <td  class= title>结算月度起</td>
        <td  class= input><input class=common name=PolMonthStart verify="结算月度|int&notnull&VALUE>200000&VALUE<210000" onkeydown="queryByKeyDown();" elementtype="nacessary"  style="width:60"><font color="#FF0000">(整数:yyyymm)</font></td>
        <td  class= title>结算月度止</td>
        <td  class= input><input class=common name=PolMonthEnd verify="结算月度|int&notnull&VALUE>200000&VALUE<210000" onkeydown="queryByKeyDown();" elementtype="nacessary"  style="width:60"><font color="#FF0000">(整数:yyyymm)</font></td>
      </tr>
      <tr  class= common>
        <td class= title>批次处理日期起</td>
        <td  class= input><input class=coolDatePicker3 name="RunDateStart" verify="批次处理日期起|date&notnull" elementtype="nacessary"  style="width:130"></td>
        <td class= title>止</td>
        <td  class= input><input class=coolDatePicker3 name="RunDateEnd" verify="批次处理日期止|date&notnull"  elementtype="nacessary"  style="width:130"></td>
        <td  class= title>销售渠道</td>
        <td  class= input><input class="codeNo"  name=SaleChnl ondblclick="return showCodeList('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('LCSaleChnl',[this,SaleChnlName],[0,1],null,null,null,1);"><input class=codename name=SaleChnlName readonly=true></td>
      </tr>
      <tr  class= common>
        <td class= title>保单号</td>
        <td  class= input><input class=common name="ContNo" style="width:130"></td>
        <td class= title>客户姓名</td>
        <td  class= input><input class=common name="AppntName" style="width:130"></td>
      </tr>
      <tr class= common>
        <td class=button><input class=cssButton value="查  询" type=button onclick="queryList();"></td>
        <td class=button><input class=cssButton  value="重  置"  type=button onclick="initInpBox();"></td>
      </tr>
    </table>
    <table>
      <tr>
        <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divAccMF);"></td>
  	    <td class=titleImg>查询结果</td>
  	  </tr>
    </table>
    <!-- 信息（列表） -->
    <div id="divAccMF" style="display:''">
    	<table class=common>
        <tr class=common>
    	    <td text-align:left colSpan=1><span id="spanAccMFGrid"></span></td>
    	  </tr>
    	</table>
    </div>

    <div id="divPage2" align=center style="display: 'none' ">
  	<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();">
  	<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
  	<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
  	<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
    </div>
    <table>
      <tr class=common>
        <td class=button><input class=cssButton value="打  印" type=hidden onclick="submitForm();"></td>
        <td class=button><input class=cssButton value="下载清单" type=button onclick="downloadClick();"></td>
      </tr>
    </table>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden name=Sql>
    <input type=hidden name=LoadFlag>
  </form>
  <!--下面这一句必须有，这是下拉选项及一些特殊展现区域-->
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
