<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
  <%
     GlobalInput tG = new GlobalInput();
     tG=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
     System.out.println("-----"+tG.ManageCom);
     //String tEdorNo=request.getParameter("EdorNo");
  %>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="GEdorAppConfirm.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorAppConfirmInit.jsp"%>
  <%@include file = "ManageComLimit.jsp"%>
  <title>保全申请确认 </title>
</head>
<body  onload="initForm();" >
  <form action="./GEdorConfirmSubmit.jsp" method=post name=fm target="fraSubmit">
   <div id="AccFee" style="display:'none'">
    <TABLE class=common>
			<tr>
				<td class=title>帐户余额</td>
				<td class= input>
				  <Input type="input" class="readonly" readonly name=AccBala>
				</td>			
				<td class=title>可领金额</td>
				<td class= input>
				  <Input type="input" class="readonly" readonly name=AccGetBala>
				</td>
				<td class=title>		
					<div id="AccTypeStr"></div>
				</td>
				<td class= input>
				  <input type="radio" name="ChkYesNo" value="yes" onclick="useAcc();" >是
					<input type="radio" name="ChkYesNo" value="no" onclick="notUseAcc();" checked>否
				</td>
			</tr>
		</TABLE>
		</div>
 <div id="FeeFlag" style="display:'none'"><p align="center">需要财务交费</p></div>
 <div id="PayDiv">
	<table class=common>
   <tr class=common id="trPayMode">
    <TD  class= title>交费/付费方式</TD>
		<TD  class= input>
		  <select id="PayMode" name="PayMode" style="width: 128; height: 23" onchange="afterCodeSelect();" >
  			<option value = "0">请选择缴费方式</option>
				<%
				//20080708 zhanggm 整个系统缴费方式统一，改成从ldcode表中取。
				//再增加缴费方式就在payMode数组中增加类型。
				String [] payMode = {"1","2","3","4","9"};
				String tSql = "";
				String payModeName = "";
				for(int i=0;i<payMode.length;i++)
				{
				  tSql = "select codename('paymode','" + payMode[i] + "') from ldcode";
				  payModeName = new ExeSQL().getOneValue(tSql);
				%>
				<option value = <%=payMode[i]%>><%=payModeName%></option>
				<%
				}
				%>
		  </select>
	  </tr>
	  <tr class=common id="trEndDate" style="display: 'none'">
			<TD  class= title>截止日期</TD>
			<TD  class= input>
				<Input class="coolDatePicker" name="EndDate" DateFormat="short" elementtype="nacessary">
			</TD>
	  </tr>
	  <tr class=common id="trPayDate" style="display: 'none'">
			<TD  class= title>转帐日期</TD>
			<TD  class= input>
				<Input class="coolDatePicker" name="PayDate" DateFormat="short"">
			</TD>
	  </tr>
	  <tr class=common id="trBank" style="display: 'none'">
			<TD  class= title>转帐银行</TD>
			<TD  class= input>
      	<input type="hidden" class = "readonly" readonly name="Bank">
      	<input class = "readonly" readonly name="BankName">
			</TD>
	  </tr>
	  <tr class=common id="trAccount" style="display: 'none'">
			<TD  class= title>转帐帐号</TD>
			<TD  class= input>
				<Input class="readonly" name="BankAccno" readonly>
				<!--帐户名 -->
				<Input type="hidden" name="AccName">
			</TD>
	  </tr>
    </table>
    <div id="BalanceMethod" style="display:'none'">
    	<table class =common>
			 <tr class=common id="trBalanceMethod">
   		   <TD  class= title>结算方式选择</TD>
				   <TD  class= input>
		       	<select name="BalanceMethodValue" style="width: 128; height: 23" onchange="afterCodeSelect2();" >
		          <option value = "0">定期结算</option>
  						<option value = "1">即时结算</option>
		  			</select>
		  		</TD>
	    	</tr>    
	    </table>		
    </div>
  	<div id="BalanceInfo" style="display:'none'" >
		<table class=common>
			<TR>
				<TD  class= title align="center">
	        结算频次
	      </TD>	      
	      <TD  class= input>
	      	<Input class=readonly name=BalIntv>
	      </TD>
	      <TD  class= title align="center">
	        下一结算日期
	      </TD>	      
	      <TD  class= input>
	      	<Input class=readonly name=NextDate>
	      </TD>
	      <TD  class= title align="center">
	        上一结算日
	      </TD>	      
	      <TD  class= input>
	      	<Input class=readonly name=BalDate>
	      </TD>
	    </TR>
	  </table>
  </div>
  </div>
  <TABLE class=common>
   	<tr class=common>
	    <TD class= title> 批单打印方式 </TD>
      <TD class= input>
        <input type="radio" name="printMode" value="1" checked >即时打印
        <input type="radio" name="printMode" value="2">批量打印
        <input type="radio" name="printMode" value="3">暂不打印
	    </TD>
		</tr>
  </TABLE>
  <div id="Remarkdiv">
    <table class=common>
	  <tr class="common">
		<td class= title>审批意见</td>
		<td class= input colspan="5"><textarea class= common name="Remark" cols="117%" rows="2" readonly></textarea></td>
	  </tr>
    </table>
  </div>    
		<div style="display: ''" align="center">
		<p><font color="#ff0000">如果对收付方式有疑问，请您点击&quot;收付方式说明&quot;来查看收付方式的详细说明</font></p>
        <input type =button class=cssButton value="收付方式说明" onclick="payModeHelp();">	 	 	 	
		  		 	 	 	
		<INPUT class = cssButton name="confirmButton" ID ="confirmButton"  VALUE="保全确认" TYPE=button onclick="edorConfirm();" >
		<INPUT class = cssButton name="redo" ID ="redo" VALUE="重复理算" TYPE=button onclick="preReCal()">
		<INPUT class = cssButton name="insuredList" ID ="insuredList" VALUE="增人被保人清单" TYPE=button onclick="showInsuredList()" style="display: none">
		<INPUT class = cssButton name="insuredListZT" ID ="insuredListZT" VALUE="减人被保人清单" TYPE=button onclick="showInsuredListZT('ZT')" style="display: 'none'">
		<INPUT class = cssButton name="insuredListCT" ID ="insuredListCT" VALUE="解约被保人清单" TYPE=button onclick="showInsuredListZT('CT')" style="display: 'none'">
		<INPUT class = cssButton name="insuredListZB" ID ="insuredListZB" VALUE="追加保费清单" TYPE=button onclick="showInsuredListZB()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListTY" ID ="insuredListTY" VALUE="团体万能追加保费清单" TYPE=button onclick="showInsuredListTY()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListTZ" ID ="insuredListTZ" VALUE="团体万能增人清单" TYPE=button onclick="showInsuredListTZ()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListTL" ID ="insuredListTL" VALUE="团体万能部分领取清单" TYPE=button onclick="showInsuredListTL()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListTQ" ID ="insuredListTQ" VALUE="团体万能离职领取清单" TYPE=button onclick="showInsuredListTQ()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListTA" ID ="insuredListTA" VALUE="团体万能资金分配清单" TYPE=button onclick="showInsuredListTA()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListGA" ID ="insuredListGA" VALUE="账户资金分配清单" TYPE=button onclick="showInsuredListGA()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListGD" ID ="insuredListGD" VALUE="管理式医疗保险金额分配清单" TYPE=button onclick="showInsuredListGD()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListLP" ID ="insuredListLP" VALUE="理赔金帐户变更清单" TYPE=button onclick="showInsuredListLP()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListWT" ID ="insuredListWT" VALUE="犹豫期退保清单" TYPE=button onclick="showInsuredListWT()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListWS" ID ="insuredListWS" VALUE="无名单实名化清单" TYPE=button onclick="showInsuredListWS()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListWD" ID ="insuredListWD" VALUE="无名单已实名化被保人删除清单" TYPE=button onclick="showInsuredListWD()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListLQ" ID ="insuredListLQ" VALUE="账户资金部分领取清单" TYPE=button onclick="showInsuredListLQ()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListGX" ID ="insuredListGX" VALUE="重疾复效清单" TYPE=button onclick="showInsuredListGX()"  style="display: 'none'">
		<INPUT class = cssButton name="insuredListCM" ID ="insuredListCM" VALUE="联系电话导入清单" TYPE=button onclick="showInsuredListCM()"  style="display: 'none'">
		<INPUT class = cssButton name="insuredListZA" ID ="insuredListZA" VALUE="管理式医疗追加保费清单" TYPE=button onclick="showInsuredListZA()" style="display: 'none'">
		<INPUT class = cssButton name="insuredListZE" ID ="insuredListZE" VALUE="管理式医疗减少保险金额清单" TYPE=button onclick="showInsuredListZE()" style="display: 'none'">
		</div>
    <input type=hidden id="EdorType" name="EdorType">	
    <input type=hidden name= transAct >	
    <input type=hidden name= fmtransact >	
    <input type=hidden name= GrpContNo>
    <input type=hidden name= EdorNo>
    <input type=hidden name= EdorAcceptNo>	
    <input type=hidden name= EdorValiDate>
    <input type=hidden name= EdorAppDate>
    <input type=hidden name= CheckBalance value="0">
  	<input type=hidden id="GetNoticeNo" name="GetNoticeNo">
	<input type=hidden name= Operate>
	<input type=hidden name= failType>
	<input type=hidden name= loadFlag value="<%=request.getParameter("loadFlag")%>">
  <input type=hidden name = "CustomerNo">
	<input type=hidden name = "AccType">
	<input type=hidden name = "DestSource">
	<input type=hidden name="fmtransact2">
	</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
