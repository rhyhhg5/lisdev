var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function verifyPage()
{ 
	//以年龄和性别校验身份证号
	if (fm.all("IDType").value == "0") 
	var strChkIdNo = chkIdNo(fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);

	if (strChkIdNo!="" && strChkIdNo!=null)
	{
		alert(strChkIdNo);
		return false;
	}
	return true;
}

//提交前的校验、计算  
function beforeSubmit()
{
  if(!checkPermitUpdate()){
  	return false;
  }
  if (!verifyInput2())
  {
    return false;
  } 
  if(!verifyElementWrap2("被保险人级别|code:PositionCode",fm.all("Position").value,"fm.Position")){
			return false;
   }
  if (fm.IDType.value == "0")
  {
    if (fm.IDNo.value == "")
    {
      fm.IDNo.focus();
      return false;
    }
    if (!verifyPage())
    {
      fm.IDNo.focus();
      return false;
    }
  }
  if(!verifyRiskInfo())
  {
  	return false;
  }
  return true;
}

function edorTypeCMSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
//	if (fm.Name.value == fm.NameBak.value && fm.Sex.value == fm.SexBak.value &&
//		fm.Birthday.value == fm.BirthdayBak.value && fm.IDType.value == fm.IDTypeBak.value &&
//		fm.IDNo.value == fm.IDNoBak.value&&fm.OccupationCode.value == fm.OccupationCodeBak.value&&
//		fm.BankCodeBak.value == fm.BankCode.value&&
//   	fm.AccNameBak.value == fm.AccName.value&&
//    fm.BankAccNoBak.value == fm.BankAccNo.value&&
//    fm.Marriage.value == fm.MarriageBak.value&&
//    fm.Relation.value == fm.RelationBak.value
//		)
//	{
//		alert('被保人资料未发生变更！');
//		return;
//	}
//
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmtransact').value = "INSERT||MAIN";
	fm.submit();

}

function customerQuery()
{	
	window.open("./LCInsuredQuery.html");
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content,Result )
{
	try{ showInfo.close(); } catch(e) {}
	window.focus();
	if (fm.all("TypeFlag").value == "G")
	{
	  top.opener.queryClick2();
	}
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var tTransact=fm.all('fmtransact').value;
	  if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
	  	//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
	  	var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[29,4,14,15,16,11]);
			
			fm.all('BankCode').value = turnPage.arrDataCacheSet[0][0];
		}
	  
	//	else if (tTransact=="QUERY||DETAIL")
	//	if (tTransact=="QUERY||MAIN")
		if (tTransact=="QUERY||DETAIL")
		{
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
			turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
			//	alert(Result);
			turnPage.strQueryResult  = Result;
			//使用模拟数据源，必须写在拆分之前
			turnPage.useSimulation   = 1;  

			//查询成功则拆分字符串，返回二维数组
			var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,tTransact);
			turnPage.arrDataCacheSet =chooseArray(tArr,[2,3,4,5,6,7,27,28,9,12]);
			//turnPage.arrDataCacheSet =chooseArray(tArr,[4,14,15,16,17,18,36,37,19,22]);
			
			fm.all('CustomerNo').value = turnPage.arrDataCacheSet[0][0];
			fm.all('Name').value = turnPage.arrDataCacheSet[0][1];
			fm.all('NameBak').value = turnPage.arrDataCacheSet[0][1];
			fm.all('Sex').value = turnPage.arrDataCacheSet[0][2];
			fm.all('SexBak').value = turnPage.arrDataCacheSet[0][2];
			fm.all('Birthday').value = turnPage.arrDataCacheSet[0][3];
			fm.all('BirthdayBak').value = turnPage.arrDataCacheSet[0][3];
			fm.all('IDType').value = turnPage.arrDataCacheSet[0][4];
			fm.all('IDTypeBak').value = turnPage.arrDataCacheSet[0][4];
			fm.all('IDNo').value = turnPage.arrDataCacheSet[0][5];
			fm.all('IDNoBak').value = turnPage.arrDataCacheSet[0][5];
			fm.all('OccupationType').value = turnPage.arrDataCacheSet[0][6];
			fm.all('OccupationTypeBak').value = turnPage.arrDataCacheSet[0][6];
			fm.all('OccupationCode').value = turnPage.arrDataCacheSet[0][7];
			fm.all('OccupationCodeBak').value = turnPage.arrDataCacheSet[0][7];
		//	fm.all('NativePlace').value = turnPage.arrDataCacheSet[0][8];
			fm.all('Marriage').value = turnPage.arrDataCacheSet[0][9];
			
			var customerNo = turnPage.arrDataCacheSet[0][0];
			var sql = "select RelationToMainInsured from LCInsured where InsuredNo = '" + customerNo + "' ";
			var arrResult = easyExecSql(sql);
			if (arrResult)
			{
				fm.all('Relation').value = arrResult[0][0];
			}
		    var sql = "select RelationToAppnt from LCInsured where InsuredNo = '" + customerNo + "' ";
			var arrResult = easyExecSql(sql);
			if (arrResult)
			{
				fm.all('RelationToAppnt').value = arrResult[0][0];
			}
			divLPInsuredDetail.style.display ='';
			divDetail.style.display='';
			queryBank();
			queryCont();
      showAllCodeName();
		} 
		else
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
			returnParent();
	  }
	}   
   getOccupationType(fm.OccupationCode.value);
}     
                 
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		// 查询保单明细
		queryInsuredDetail();
	}
}

function queryInsuredDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	tEdorType=fm.all('EdorType').value;
	tPolNo=fm.all('PolNo').value;
	tCustomerNo = fm.all('CustomerNo').value;
	parent.fraInterface.fm.action = "./InsuredQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorTypeICSubmit.jsp";
}

function returnParent()
{
	try
	{
   // top.opener.updatClick(); 
    top.opener.focus();
	  top.opener.getEdorItem();
	}
	catch (ex) {}
	top.close();
}

function queryCont()
{
	var customerNo = fm.all('CustomerNo').value;
	var appntNo = fm.all('AppntNo').value;
	var contNo = fm.all('ContNo').value;
  var strSQL;
  if (fm.TypeFlag.value == "G") //团单
  {
  	strSQL = "select a.ContNo, a.AppntName, a.InsuredName, a.CValiDate, a.Prem, a.Amnt, a.GrpContNo " +
  	             "from LCCont a " +
  	             "where a.ContNo = '" + contNo + "' ";
  }
  else
  {
  	//分为投保人和被保人两种情况	      
  	strSQL = "select a.ContNo, a.AppntName, a.InsuredName, a.CValiDate, a.Prem, a.Amnt, a.GrpContNo " +
  	             "from LCCont a,LCAppnt b " +
  	             "where AppFlag = '1' " +
  	             "and a.AppntNo = '" + appntNo + "' " +
  	             "and a.ContNo = b.ContNo " +
  	             "union " + 
  	             "select a.ContNo, a.AppntName, a.InsuredName, a.CValiDate, a.Prem, a.Amnt, a.GrpContNo " +
  	             "from LCCont a , LCInsured b " +
  	             "where a.ContNo = b.ContNo " + 
  	             "and a.AppFlag = '1' " +
  	             "and b.AppntNo = '" + appntNo + "' " +
  	             "and b.InsuredNo = '" + customerNo + "' ";
  }
	turnPage.queryModal(strSQL, ContGrid);
}

//查询理赔金帐号
function queryBank()
{
  var customerNo = fm.all('CustomerNo').value;
  var contNo = fm.all('ContNo').value; 

  //先从P表查，再查c表
	var sql = "select BankCode,(select bankname from ldbank where bankcode=a.bankcode) ,AccName, BankAccNo from LPInsured a " +
	          "where insuredno = '" + customerNo + "' " +
	          "and ContNo = '" + contNo + "' " +
	          "and EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "'";		        
	var arrResult  = easyExecSql(sql, 1, 0);
	if (!arrResult) 
	{
	   sql = "select BankCode,(select bankname from ldbank where bankcode=a.bankcode) ,AccName, BankAccNo from lcinsured a " +
	         "where InsuredNo = '" + customerNo + "' " +
	         "and ContNo = '" + contNo + "'";
	   arrResult = easyExecSql(sql, 1, 0); 
	}
	if (arrResult)
	{
		fm.BankCode2.value = arrResult[0][0];
		fm.BankCodeName2.value = arrResult[0][1];
		fm.AccName2.value = arrResult[0][2];
		fm.BankAccNo2.value = arrResult[0][3];
	}
}
//查询个人缴费帐号
function queryBank2()
{
  var customerNo = fm.all('CustomerNo').value;
  var contNo = fm.all('ContNo').value; 
  //先从P表查，再查c表
	var sql = "select BankCode, (select bankname from ldbank where bankcode=a.bankcode) ,AccName, BankAccNo from lpcont a " +
	          "where insuredno = '" + customerNo + "' " +
	          "and ContNo = '" + contNo + "' " +
	          "and EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "'";		        
	var arrResult  = easyExecSql(sql, 1, 0);
	if (!arrResult) 
	{
	   sql = "select BankCode, (select bankname from ldbank where bankcode=a.bankcode) ,AccName, BankAccNo from lccont a " +
	         "where InsuredNo = '" + customerNo + "' " +
	         "and ContNo = '" + contNo + "'";
	   arrResult = easyExecSql(sql, 1, 0); 
	}
	if (arrResult)
	{
		fm.BankCode.value = arrResult[0][0];
		fm.BankCodeName.value = arrResult[0][1];
		fm.AccName.value = arrResult[0][2];
		fm.BankAccNo.value = arrResult[0][3];
	}
}

//根据职业代码得到职业类型和职业名称
function getOccupationType(occupationCode)
{
	if (fm.OccupationType.value == "" || fm.OccupationCodeName.value == "")
  {
		var sql = "select OccupationType,OccupationName||'-'||workname from LDOccupation " +
		          "where OccupationCode = '" + occupationCode + "' ";
		var arrResult = easyExecSql(sql);
		if (arrResult)
		{
			fm.OccupationType.value = arrResult[0][0];
			fm.OccupationCodeName.value = arrResult[0][1];
		}
	}
}
//根据职业代码得到职业类型和职业名称
function getPositonCodeName()
{
	if (fm.Position.value != "")
  {
  
		var sql= "select Gradecode,Gradename from lcgrpposition where  Gradecode='"+fm.Position.value+"' and Grpcontno= '"      
                    + fm.GrpContNo.value
                    + "' order by Gradecode";
		var arrResult = easyExecSql(sql);
		if (arrResult)
		{
			fm.Position.value = arrResult[0][0];
		
			fm.PositionCodeName.value = arrResult[0][1];
		}
	}
}
//查询客户基本信息
function getCustomerInfo(customerNo)
{
	var contNo = fm.all("ContNo").value
	var edorNo = fm.all("EdorNo").value; 
	var edorType = fm.all("EdorType").value;
	//先从P表中查询，再查C表
	var sql1 =
					 /* "select CustomerNo, Name, Sex, Birthday, IDType, IDNo, " +
            "       OccupationType, OccupationCode, Marriage " +
            "from LPPerson " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
            "and CustomerNo = '" + customerNo + "' ";
            */
            "select InsuredNo, Name, Sex, Birthday, IDType, IDNo, " +
            "       OccupationType, OccupationCode, Marriage, InsuredStat,Position,JoinCompanyDate " +
            "from LPInsured " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
            "and ContNo = '" + contNo + "' " +
            "and InsuredNo = '" + customerNo + "' ";
            
            
   var sql2 =
            "select AppntNo, AppntName, AppntSex, AppntBirthday, IDType, IDNo, " +
            "       OccupationType, OccupationCode, Marriage " +
            "from LPAppnt " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
            "and ContNo = '" + contNo + "' " +
            "and AppntNo = '" + customerNo + "' ";

           
  var arrResult1 = easyExecSql(sql1); 
  var arrResult2 = easyExecSql(sql2); 
  if ((!arrResult1)&&(!arrResult2))
	{
		sql1 = 
		     /*"select CustomerNo, Name, Sex, Birthday, IDType, IDNo, " +
		      "       OccupationType, OccupationCode, Marriage " +
		      "from LDPerson where CustomerNo = '" + customerNo + "' ";
		      */
		      "select InsuredNo, Name, Sex, Birthday, IDType, IDNo, " +
            "OccupationType, OccupationCode, Marriage, InsuredStat,Position,JoinCompanyDate  " +
            "from LCInsured " +
            "where  ContNo = '" + contNo + "' " +
            "and InsuredNo = '" + customerNo + "' ";
            
		sql2 =  "select AppntNo, AppntName, AppntSex, AppntBirthday, IDType, IDNo, " +
            " OccupationType, OccupationCode, Marriage " +
            "from LCAppnt " +
            "where ContNo = '" + contNo + "' "+
            "and AppntNo = '" + customerNo + "' "
            ;

		arrResult1 = easyExecSql(sql1);
		arrResult2 = easyExecSql(sql2);
		if ((!arrResult1)&&(!arrResult2))
		{
			alert("未查到该客户资料！");
			return false;
		}
	}
	if (arrResult1)
	{
		fm.all('CustomerNo').value = arrResult1[0][0];
		fm.all('Name').value = arrResult1[0][1];
		fm.all('NameBak').value = arrResult1[0][1];
		fm.all('Sex').value = arrResult1[0][2];
		fm.all('SexBak').value = arrResult1[0][2];
		fm.all('Birthday').value = arrResult1[0][3];
		fm.all('BirthdayBak').value = arrResult1[0][3];
		fm.all('IDType').value = arrResult1[0][4];
		fm.all('IDTypeBak').value = arrResult1[0][4];
		fm.all('IDNo').value = arrResult1[0][5];
		fm.all('IDNoBak').value = arrResult1[0][5];
		fm.all('OccupationType').value = arrResult1[0][6];
		fm.all('OccupationTypeBak').value = arrResult1[0][6];
		fm.all('OccupationCode').value = arrResult1[0][7];
		fm.all('OccupationCodeBak').value = arrResult1[0][7];
		fm.all('Marriage').value = arrResult1[0][8];
		fm.all('MarriageBak').value = arrResult1[0][8];
		fm.all('InsuredState').value = arrResult1[0][9];
		fm.all('Position').value = arrResult1[0][10];
	
		fm.all('JoinCompanyDate').value = arrResult1[0][11];
	}
  if((!arrResult1)&&arrResult2)
  { 
  	fm.all('CustomerNo').value = arrResult2[0][0];
		fm.all('Name').value = arrResult2[0][1];
		fm.all('NameBak').value = arrResult2[0][1];
		fm.all('Sex').value = arrResult2[0][2];
		fm.all('SexBak').value = arrResult2[0][2];
		fm.all('Birthday').value = arrResult2[0][3];
		fm.all('BirthdayBak').value = arrResult2[0][3];
		fm.all('IDType').value = arrResult2[0][4];
		fm.all('IDTypeBak').value = arrResult2[0][4];
		fm.all('IDNo').value = arrResult2[0][5];
		fm.all('IDNoBak').value = arrResult2[0][5];
		fm.all('OccupationType').value = arrResult2[0][6];
		fm.all('OccupationTypeBak').value = arrResult2[0][6];
		fm.all('OccupationCode').value = arrResult2[0][7];
		fm.all('OccupationCodeBak').value = arrResult2[0][7];
		fm.all('Marriage').value = arrResult2[0][8];
		fm.all('MarriageBak').value = arrResult2[0][8];
  }
	//得到被保人关系
	var sql = "select RelationToMainInsured from LPInsured " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
	          "and InsuredNo = '" + customerNo + "' " +
					  "and ContNo = '" + contNo + "' ";
	var arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		sql = "select RelationToMainInsured from LCInsured " +
		      "where InsuredNo = '" + customerNo + "' " +
					"and ContNo = '" + fm.ContNo.value + "' ";
	  arrResult = easyExecSql(sql);
	}
	if (arrResult)
	{
		fm.all('Relation').value = arrResult[0][0];
		fm.all('RelationBak').value = arrResult[0][0];
	}
    //得到与投保人的关系
    //2011-03-21
    //【OoO?】杨天政
    var sql = "select RelationToAppnt from LPInsured " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
	          "and InsuredNo = '" + customerNo + "' " +
					  "and ContNo = '" + contNo + "' ";
	var arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		sql = "select RelationToAppnt from LCInsured " +
		      "where InsuredNo = '" + customerNo + "' " +
					"and ContNo = '" + fm.ContNo.value + "' ";
	  arrResult = easyExecSql(sql);
	}
	if (arrResult)
	{
		fm.all('RelationToAppnt').value = arrResult[0][0];
		fm.all('RelationToAppntBak').value = arrResult[0][0];
	}




	divLPInsuredDetail.style.display ='';
	divDetail.style.display='';

	queryBank();
	queryBank2();
	queryCont();
	getPositonCodeName();
	getOccupationType(fm.OccupationCode.value);
}
function afterQuery( arrReturn )
{
	fm.all('BankCode').value = arrReturn[0];
	fm.all('BankCodeBak').value = arrReturn[0];
	fm.all('BankCodeName').value = arrReturn[1];
}
function querybank()
{
	showInfo = window.open("LDBankQueryMain.jsp");
}
//add by Pangxy 20101209 万能险被保险人校验
function verifyRiskInfo()
{
	var tCustomerNo = fm.all('CustomerNo').value;
	//校验变更的客户是否为万能险的被保人(是：继续校验；否：校验通过)
	var tSql = "select count(1) "
			 + "  from lcpol "
			 + " where insuredno = '" + tCustomerNo + "'"
			 + "   and stateflag = '1' "
			 + "   and riskcode in ('330801','331801','332001')";
	var arrResult1 = easyExecSql(tSql);
	if(arrResult1[0][0] == "0")
	{
		return true;
	}
	//校验变更后信息是否与其他被保人客户信息一致(是：继续校验；否：校验通过)
	var tName = trim(fm.all('Name').value);
	var tSex = trim(fm.all('Sex').value);
	var tBirthday = trim(fm.all('Birthday').value);
	var tIDType = trim(fm.all('IDType').value);
	var tIDNo = trim(fm.all('IDNo').value);
	tSql = "select a.insuredno "
		 + "  from lcinsured a"
		 + " where a.name = '" + tName + "' "
		 + "   and a.sex = '" + tSex + "' "
		 + "   and a.Birthday = '" + tBirthday + "' "
		 + "   and a.IDType = '" + tIDType + "'"
		 + "   and a.IDNo = '" + tIDNo + "'"
		 + "   and exists (select 'Y' from lccont b where b.contno=a.contno and b.stateflag='1')"
		 + "   and a.insuredno <> '" + tCustomerNo + "'";
	var arrResult2 = easyExecSql(tSql);
	if(!arrResult2)
	{
		return true;
	}
	//校验其他被保人所属险种是否为万能险(是：校验失败，提示信息；否：校验通过)
	var tRecCnt = arrResult2.length;
	for(var i=0;i<tRecCnt;i++)
	{
		tSql = "select 'Y' "
			 + "  from lcpol "
			 + " where insuredno = '" + arrResult2[i][0] + "'"
			 + "   and stateflag = '1' "
			 + "   and riskcode in ('330801','331801','332001')";
		var arrResult3 = easyExecSql(tSql);
		if(arrResult3)
		{
			alert("变更后的被保险人有正在生效的万能险保单，不能进行变更操作！");
			return false;
		}
	}
	return true;
}

function ImpartCheckSameRadio1(){
	if(fm.ImpartCheckSame.checked == true) {
	
		divBankInfo.style.display='none';
   		fm.BankCode2.value = fm.BankCode.value;
		fm.BankCodeName2.value = fm.BankCodeName.value;
		fm.AccName2.value = fm.AccName.value;
		fm.BankAccNo2.value = fm.BankAccNo.value;
  	}else{
  		divBankInfo.style.display='';
  		queryBank();
  		//window.location.href="./GEdorTypeMCMain.jsp?TypeFlag=G";
  	}
}

function ImpartCheck6Radio1() {
  if(fm.ImpartCheck6[0].checked == true) {
  	fm.ImpartCheck6[1].checked = false;
  	fm.ImpartCheck6[2].checked = false;
  	fm.ImpartCheck6[3].checked = false;
  	fm.getWay.value="A"
  }else{
  	fm.getWay.value="";
  }
}
function ImpartCheck6Radio2() {
  if(fm.ImpartCheck6[1].checked == true) {
  	fm.ImpartCheck6[0].checked = false;
  	fm.ImpartCheck6[2].checked = false;
  	fm.ImpartCheck6[3].checked = false;
  	fm.getWay.value="B"
  }else{
  	fm.getWay.value="";
  }
}
function ImpartCheck6Radio3() {
  if(fm.ImpartCheck6[2].checked == true) {
  	fm.ImpartCheck6[0].checked = false;
  	fm.ImpartCheck6[1].checked = false;
  	fm.ImpartCheck6[3].checked = false;
  	fm.getWay.value="C"
  }else{
  	fm.getWay.value="";
  }
}
function ImpartCheck6Radio4() {
  if(fm.ImpartCheck6[3].checked == true) {
  	fm.ImpartCheck6[0].checked = false;
  	fm.ImpartCheck6[1].checked = false;
  	fm.ImpartCheck6[2].checked = false;
  	fm.getWay.value="D"
  }else{
  	fm.getWay.value="";
  }
}

function checkISTheSame(){
	var sql = "select BankCode,(select bankname from ldbank where bankcode=a.bankcode) ,AccName, BankAccNo from LPInsured a " +
	          "where insuredno = '" + customerNo + "' " +
	          "and ContNo = '" + contNo + "' " +
	          "and EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "'";		        
	var arrResult  = easyExecSql(sql, 1, 0);
	
	
	var sql2 = "select BankCode,(select bankname from ldbank where bankcode=a.bankcode) ,AccName, BankAccNo from LPInsured a " +
	          "where insuredno = '" + customerNo + "' " +
	          "and ContNo = '" + contNo + "' " +
	          "and EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "'";		        
	var arrResult2  = easyExecSql(sql2, 1, 0);
	
	if(!arrResult&&!arrResult2){
		
	}else{
		if((arrResult[0][0]==arrResult2[0][0])&&(arrResult[0][1]==arrResult2[0][1])&&(arrResult[0][2]==arrResult2[0][2])&&(arrResult[0][3]==arrResult2[0][3])){
			divBankInfo.style.display='none';
			fm.ImpartCheckSame.checked=true;
		}else{
			divBankInfo.style.display='';
			fm.ImpartCheckSame.checked=false;
			queryBank2();
		}
	}
}

function checkISSame(){
	var flag=false;
	if(fm.BankCode.value==fm.BankCode2.value){
		flag=true;
	}else{
		flag=false;
	}
	if(fm.BankCodeName.value==fm.BankCodeName2.value){
		flag=true;
	}else{
		flag=false;
	}
	if(fm.AccName.value==fm.AccName2.value){
		flag=true;
	}else{
		flag=false;
	}
	if(fm.BankAccNo.value==fm.BankAccNo2.value){
		flag=true;
	}else{
		flag=false;
	}
	
	if(flag){
		divBankInfo.style.display='none';
		fm.ImpartCheckSame.checked=true;
	}else{
		divBankInfo.style.display='';
		fm.ImpartCheckSame.checked=false;	
	}
}
function checkPermitUpdate(){
	var contNo = fm.all('ContNo').value;
	var age;
	var enddate;
	var value;
	var value2;
	var sqll = "select getdutykind from lpget where edorno='"+fm.EdorNo.value+"' and grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y' order by dutycode asc";
	var Resultt = easyExecSql(sqll);
	if(Resultt!=null){
		value = Resultt[0][0];
	}else{
		var sql = "select getdutykind from lcget where grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y' order by dutycode asc";
		var Result = easyExecSql(sql);
		value = Result[0][0];
	}
	//取出年龄
	
	var sql11 = "select birthday from lpinsured where edorno='"+fm.EdorNo.value+"' and insuredno='"+fm.CustomerNo.value+"' and grpcontno='"+fm.GrpContNo.value+"'";
	var Result11 = easyExecSql(sql11);
	if(Result11!=null){
		age = Result11[0][0].substr(0,4);
	}else{
		var sql1 = "select birthday from lcinsured where insuredno='"+fm.CustomerNo.value+"' and grpcontno='"+fm.GrpContNo.value+"'";
		var Result1 = easyExecSql(sql1);
		if(Result1==null){
			alert("数据异常！");
		}
		age = Result1[0][0].substr(0,4);
	}
	
	
	
	
	var sql22 = "select getstartdate from lpget where edorno='"+fm.EdorNo.value+"' and grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y'";
	var Result22 = easyExecSql(sql22);
	if(Result22!=null){
		enddate = Result22[0][0].substr(0,4);
	}else{
		var sql2 = "select getstartdate from lcget where grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y'";
		var Result2 = easyExecSql(sql2);
		if(Result2==null){
			alert("数据异常！");
		}
		enddate = Result2[0][0].substr(0,4);
	}
	
	var sql3 = "select "+enddate+"-"+age+"-"+1+" from dual";
	var Result3 = easyExecSql(sql3);
	if(Result3[0][0].substr(0,4)<0){
		alert("数据异常");
	}
	value2 = Result3[0][0].substr(0,4);


	var sql = "select ImpartParamModle from LcCustomerImpart where GrpContNo='"+fm.GrpContNo.value+"' and ImpartVer in( '009','008') and ImpartCode = '010'";
	Result = easyExecSql(sql);
	var resultArray = new Array();
	var resultArray2 = new Array();
	resultArray = Result[0][0].split(",");
	resultArray2 = Result[1][0].split(",");
	if(resultArray[3]!='Y'){
		if(fm.ImpartCheck5.value!=value2){
			alert("保单信息中，护理保险金领取日期不允许进行修改!");
			window.location.href="./GEdorTypeMCMain.jsp?TypeFlag=G";
			return false;
		}
	}
	if(resultArray2[4]!='Y'){
		if(fm.getWay.value!=value){
			alert("保单信息中，护理保险金领取方式不允许进行修改!");
			window.location.href="./GEdorTypeMCMain.jsp?TypeFlag=G";
			return false;
		}
	}
	return true;
}

function checkIsChoose(){
	var flag=false;
	if(fm.ImpartCheck6[0].checked = true){
		flag=true;
	}else{
		flag=false;
	}
	if(fm.ImpartCheck6[1].checked = true){
		flag=true;
	}else{
		flag=false;
	}
	if(fm.ImpartCheck6[2].checked = true){
		flag=true;
	}else{
		flag=false;
	}
	if(fm.ImpartCheck6[3].checked = true){
		flag=true;
	}else{
		flag=false;
	}
	
	return flag;
}
function checkIsChoose2(){
	var count=0;
	if(fm.ImpartCheck6[0].checked = true){
		count=1;
	}else{
		count=0;
	}
	if(fm.ImpartCheck6[1].checked = true){
		count=2;
	}else{
		count=0;
	}
	if(fm.ImpartCheck6[2].checked = true){
		count=3;
	}else{
		count=0;
	}
	if(fm.ImpartCheck6[3].checked = true){
		count=4;
	}else{
		count=0;
	}
	if(count==0){
		
	}
	return count;
}

function initLNJInfo(){
	var contNo = fm.all('ContNo').value;
	var age;
	var enddate;
	var sqll = "select getdutykind from lpget where edorno='"+fm.EdorNo.value+"' and grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y' order by dutycode asc";
	var Resultt = easyExecSql(sqll);
	if(Resultt!=null){
		if(Resultt[0][0]=='A'){
			fm.ImpartCheck6[0].checked = true
			fm.getWay.value = 'A';
		}
		if(Resultt[0][0]=='B'){
			fm.ImpartCheck6[1].checked = true
			fm.getWay.value = 'B';
		}
		if(Resultt[0][0]=='C'){
			fm.ImpartCheck6[2].checked = true
			fm.getWay.value = 'C';
		}
		if(Resultt[0][0]=='D'){
			fm.ImpartCheck6[3].checked = true
			fm.getWay.value = 'D';
		}
	}else{
		var sql = "select getdutykind from lcget where grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y' order by dutycode asc";
		var Result = easyExecSql(sql);
		if(Result[0][0]=='A'){
			fm.ImpartCheck6[0].checked = true
			fm.getWay.value = 'A';
		}
		if(Result[0][0]=='B'){
			fm.ImpartCheck6[1].checked = true
			fm.getWay.value = 'B';
		}
		if(Result[0][0]=='C'){
			fm.ImpartCheck6[2].checked = true
			fm.getWay.value = 'C';
		}
		if(Result[0][0]=='D'){
			fm.ImpartCheck6[3].checked = true
			fm.getWay.value = 'D';
		}
	}
	//取出年龄
	
	var sql11 = "select birthday from lpinsured where edorno='"+fm.EdorNo.value+"' and insuredno='"+fm.CustomerNo.value+"' and grpcontno='"+fm.GrpContNo.value+"'";
	var Result11 = easyExecSql(sql11);
	if(Result11!=null){
		age = Result11[0][0].substr(0,4);
	}else{
		var sql1 = "select birthday from lcinsured where insuredno='"+fm.CustomerNo.value+"' and grpcontno='"+fm.GrpContNo.value+"'";
		var Result1 = easyExecSql(sql1);
		if(Result1==null){
			alert("数据异常！");
		}
		age = Result1[0][0].substr(0,4);
	}
	
	
	
	
	var sql22 = "select getstartdate from lpget where edorno='"+fm.EdorNo.value+"' and grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y'";
	var Result22 = easyExecSql(sql22);
	if(Result22!=null){
		enddate = Result22[0][0].substr(0,4);
	}else{
		var sql2 = "select getstartdate from lcget where grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y'";
		var Result2 = easyExecSql(sql2);
		if(Result2==null){
			alert("数据异常！");
		}
		enddate = Result2[0][0].substr(0,4);
	}
	
	var sql3 = "select "+enddate+"-"+age+"-"+1+" from dual";
	var Result3 = easyExecSql(sql3);
	if(Result3[0][0].substr(0,4)<0){
		alert("数据异常");
	}
	fm.ImpartCheck5.value = Result3[0][0].substr(0,4);
}

function initLNJInfo2(){
	var contNo = fm.all('ContNo').value;
	var age;
	var enddate;
	var value;
	var value2;
	var sqll = "select getdutykind from lpget where edorno='"+fm.EdorNo.value+"' and grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y' order by dutycode asc";
	var Resultt = easyExecSql(sqll);
	if(Resultt!=null){
		value = Resultt[0][0];
	}else{
		var sql = "select getdutykind from lcget where grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y' order by dutycode asc";
		var Result = easyExecSql(sql);
		value = Result[0][0];
	}
	//取出年龄
	
	var sql11 = "select birthday from lpinsured where edorno='"+fm.EdorNo.value+"' and insuredno='"+fm.CustomerNo.value+"' and grpcontno='"+fm.GrpContNo.value+"'";
	var Result11 = easyExecSql(sql11);
	if(Result11!=null){
		age = Result11[0][0].substr(0,4);
	}else{
		var sql1 = "select birthday from lcinsured where insuredno='"+fm.CustomerNo.value+"' and grpcontno='"+fm.GrpContNo.value+"'";
		var Result1 = easyExecSql(sql1);
		if(Result1==null){
			alert("数据异常！");
		}
		age = Result1[0][0].substr(0,4);
	}
	
	
	
	
	var sql22 = "select getstartdate from lpget where edorno='"+fm.EdorNo.value+"' and grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y'";
	var Result22 = easyExecSql(sql22);
	if(Result22!=null){
		enddate = Result22[0][0].substr(0,4);
	}else{
		var sql2 = "select getstartdate from lcget where grpcontno='"+fm.GrpContNo.value+"' and contno='"+contNo+"' and state='Y'";
		var Result2 = easyExecSql(sql2);
		if(Result2==null){
			alert("数据异常！");
		}
		enddate = Result2[0][0].substr(0,4);
	}
	
	var sql3 = "select "+enddate+"-"+age+"-"+1+" from dual";
	var Result3 = easyExecSql(sql3);
	if(Result3[0][0].substr(0,4)<0){
		alert("数据异常");
	}
	value2 = Result3[0][0].substr(0,4);
}

