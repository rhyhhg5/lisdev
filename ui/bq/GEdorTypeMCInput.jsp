<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK"> 
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeMC.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeMCInit.jsp"%>
  <title>团险万能客户资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeMCSubmit.jsp" method=post name=fm target="fraSubmit"> 
  <table class=common>
    <TR class= common> 
      <TD class= title > 受理号 </TD>
      <TD class= input>
        <input class="readonly" type="text" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" type="hidden" readonly name=EdorType>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
      <TD class = title > 合同保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>
 <Div  id= "divLPInsuredDetail" style= "display:''">
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
        客户详细信息
      </td>
   </tr>
   </table>
  	<Div  id= "divDetail" style= "">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>
            <Input class= "readonly" readonly name=CustomerNo >
          </TD>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class= common name=Name verify="姓名|notnull&len<=60"  elementtype=nacessary >
            <Input class= hidden type=hidden readonly name=NameBak >
          </TD>
				  <TD  class= title>
						性别
				  </TD>
				  <TD  class= input>
					 	<Input class="codeNo" name=Sex verify="性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this,SexName], [0,1]);" onkeyup="return showCodeListKey('Sex',[this,SexName], [0,1]);"><Input class="codeName" name=SexName elementtype=nacessary readonly >
		        <Input class= hidden type=hidden readonly name=SexBak >
				  </TD>
				</TR>
      	<TR  class= common>
				  <TD  class= title>
				   	出生日期
				  </TD>
				  <TD  class= input>
						<Input class= common name=Birthday elementtype=nacessary verify="出生日期|notnull&date" >
		        <Input class= hidden type=hidden readonly name=BirthdayBak >
				  </TD>
          <TD  class= title>
            证件类型
          </TD>
          <TD  class= input>
            <Input class= codeNo name="IDType"  verify="证件类型|notnull&code:IDType" ondblclick="return showCodeList('IDType',[this,IDTypeName], [0,1]);" onkeyup="return showCodeListKey('IDType',[this,IDTypeName], [0,1]);"><Input class= codeName name="IDTypeName" elementtype=nacessary readonly >
            <Input class= hidden type=hidden readonly name=IDTypeBak >
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name=IDNo elementtype=nacessary verify="证件号码|len<20">
            <Input class= hidden type=hidden readonly name=IDNoBak >
         </TD>
        </TR>       
        <TR  class= common>
          <TD class=title>
					  职业代码 
					</TD>
					<TD class=input>
					 	<Input name=OccupationCode class="codeNo" ondblclick="return showCodeList('OccupationCode', [this,OccupationCodeName,OccupationType],[0,1,2],null,null,null,1,300);" onkeyup="return showCodeListKey('OccupationCode', [this,OccupationCodeName,OccupationType],[0,1,2],null,null,null,1,300);" verify="职业代码|notnull"><Input name=OccupationCodeName class="codeName" elementtype=nacessary readonly >
						<Input class= hidden type=hidden readonly name=OccupationCodeBak >
					</TD>
					<TD class=title>
					  职业类别 
					</TD>
					<TD class=input>
					  <Input name="OccupationType" class="readonly" readonly>
						<Input class= hidden type=hidden readonly name=OccupationTypeBak >
					</TD>
					<TD CLASS=title>
						婚姻状况
					</TD>
					<TD CLASS=input>
						<Input class="codeNo" name="Marriage" verify="婚姻状况|code:Marriage" ondblclick="return showCodeList('Marriage',[this,MarriageName], [0,1]);" onkeyup="return showCodeListKey('Marriage',[this,MarriageName], [0,1]);" ><Input class="codeName" name="MarriageName"  readonly >
						<Input class= hidden type=hidden readonly name=MarriageBak >
					</TD>
         </TR> 
         <TR>
         	<TD CLASS=title>
						与主被保人关系
					</TD>
					<TD CLASS=input>
						<Input class="codeNo" name="Relation"  verify="与主被保人关系|code:Relation" ondblclick="return showCodeList('Relation',[this,RelationName], [0,1]);" onkeyup="return showCodeListKey('Relation',[this,RelationName], [0,1]);" ><Input class="codeName" name="RelationName" readonly >
						<Input class= hidden type=hidden readonly name=RelationBak >
					</TD>
			<TD class=title>服务年数起始日</TD>
			<TD class=input>
				<Input name="JoinCompanyDate" class="coolDatePicker" dateFormat="short">
			</TD>
					<TD CLASS=title>
						在职状态
					</TD>
					<TD CLASS=input colspan="3">
						<Input class="codeNo" name="InsuredState"  ondblclick="return showCodeList('workstate',[this,InsuredStateName], [0,1]);" onkeyup="return showCodeListKey('workstate',[this,InsuredStateName], [0,1]);" ><Input class="codeName" name="InsuredStateName" readonly >
					</TD>					
       	     </TR>    	 		
       	  <TR>
       	  <TD CLASS=title>
						与投保人的关系
					</TD>
					<TD CLASS=input>
						<Input class="codeNo" name="RelationToAppnt"  verify="与投保人的关系|code:Relation" ondblclick="return showCodeList('Relation',[this,RelationToAppntName], [0,1]);" onkeyup="return showCodeListKey('Relation',[this,RelationToAppntName], [0,1]);" ><Input class="codeName" name="RelationToAppntName" readonly >
						<Input class= hidden type=hidden readonly name=RelationToAppntBak >
		   </TD>
       	   <TD class=title>级别</TD>
       	   <TD class=input>
			<Input class="codeno" name="Position" verify="被保险人级别|code:PositionCode" ondblclick="return showCodeList('PositionCode',[this,PositionCodeName],[0,1],null,fm.GrpContNo.value,'grpcontno','1');" onkeyup="return showCodeListKey('PositionCode',[this,PositionCodeName],[0,1],null,fm.GrpContNo.value,'grpcontno','1');" onfocus="showCodeName();"><input name=PositionCodeName class=codename readonly=true>
		    </TD>
			</TR>
       </table>
      </Div>

       <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divgetinfo);">
    		</td>
    		<td class= titleImg>
    			老年护理金
    		</td>
    	</tr>
 	   </table>
 	   <Div  id= "divgetinfo" style= "display: ''">
   	<table class=common>
      	<tr class = common>
      		<td class= common style="weith:20%;aglin:right">
    			 老年护理保险金领取起始日
    		</td>
    		<td style="aglin:left">
		     <input class="common6" name="ImpartCheck5">周岁
			</td>
			<td>
		</tr>  
      </table>
      <table class=common>
      	<tr class = common>
      		<td class= common>
    			 老年护理金领取方式
    		</td>
			<td>
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio1();">&nbsp;&nbsp;&nbsp;&nbsp;A一次性领取
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio2();">&nbsp;&nbsp;&nbsp;&nbsp;B按年或按月分期领取
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio3();">&nbsp;&nbsp;&nbsp;&nbsp;C部分分期领取
		    <input type="checkbox" name="ImpartCheck6" class="box" onclick="ImpartCheck6Radio4();">&nbsp;&nbsp;&nbsp;&nbsp;D按年或按月分次领取
		    <input type="hidden" name="getWay">
			</td>			
		</tr>  
      </table>  
      </Div>  



	<DIV id=DivClaimHead STYLE="display:''">   
	<table>
			<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivClaim);">
				</td>
				<td class="titleImg">个人缴费帐户
				</td>
			</tr>
	</table>
	</Div>      

<DIV id=DivClaim STYLE="display:''"> 
	<table  class= common>
		<TR CLASS=common>
			<TD CLASS=title>开户银行</TD>
			<TD CLASS=input>
				<Input NAME=BankCode VALUE="" CLASS="codeNo" MAXLENGTH=20  ondblclick="return showCodeList('bank',[this,BankCodeName], [0,1]);" onkeyup="return showCodeListKey('bank',[this,BankCodeName], [0,1]);" ><Input NAME=BankCodeName VALUE="" CLASS="codeName" readonly >
				<input name="BankCodeBak" type=hidden>
			</TD>
			<TD CLASS=title >户名</TD>
			<TD CLASS=input>
				<Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=50 verify="户名|len<=50" >
				<input name="AccNameBak" type=hidden>
			</TD>
			<TD CLASS=title width="109" >账号</TD>
			<TD CLASS=input>
				<Input NAME=BankAccNo VALUE="" CLASS=common verify="银行帐号|len<=40">
				<input name="BankAccNoBak" type=hidden>
			</TD>
		</TR>
	</Table> 	          
</DIV> 



	<DIV id=DivClaim2Head STYLE="display:''">   
	<table>
			<tr>
				<td>
				<img src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,DivClaim2);">
				</td>
				<td class="titleImg">理赔金帐户
				</td>
			</tr>
	</table>
	</Div>      

<DIV id=DivClaim2 STYLE="display:''"> 
	<table  class= common>
		<tr CLASS=common>
			
		</tr>
		<tr CLASS=common>
			
		</tr>
		<tr class=common>
			<td>
			<input type="checkbox" name="ImpartCheckSame" class="box" onclick="ImpartCheckSameRadio1();">&nbsp;&nbsp;&nbsp;&nbsp;同个人缴费账户信息
			</td>
		</tr>
		<tr class=common>
			<td>
			</td>
		</tr>
		<TR CLASS=common >
			<table class=common id='divBankInfo' style= "display: ''" >
			<tr>
			<TD CLASS=title>开户银行</TD>
			<TD CLASS=input>
				<Input NAME=BankCode2 VALUE="" CLASS="codeNo" MAXLENGTH=20  ondblclick="return showCodeList('bank',[this,BankCodeName2], [0,1]);" onkeyup="return showCodeListKey('bank',[this,BankCodeName2], [0,1]);" ><Input NAME=BankCodeName2 VALUE="" CLASS="codeName" readonly >
				<input name="BankCodeBak2" type=hidden>
			</TD>
			<TD CLASS=title >户名</TD>
			<TD CLASS=input>
				<Input NAME=AccName2 VALUE="" CLASS=common MAXLENGTH=50 verify="户名|len<=50" >
				<input name="AccNameBak2" type=hidden>
			</TD>
			<TD CLASS=title width="109" >账号</TD>
			<TD CLASS=input>
				<Input NAME=BankAccNo2 VALUE="" CLASS=common verify="银行帐号|len<=40">
				<input name="BankAccNoBak2" type=hidden>
			</TD>
			</tr>
			</table>
		</TR>
	</Table> 	          
</DIV> 
 
	</Div>
	<div id="divCont">
		<table>
		  <tr>
	      <td>
	        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCCont);">
	      </td>
	      <td class= titleImg>
	        关联保单信息
	      </td>
		  </tr>
		</table>
		<Div  id= "divLCCont" style= "display: ''">
			<table  class= common>
		 		<tr  class= common>
			  		<td text-align: left colSpan=1>
					<span id="spanContGrid" >
					</span> 
			  	</td>
			</tr>
		</table>					
	</div>  
	</div>
	  <br>
	  <hr>
		<Input class= cssButton type=Button value="保  存" onclick="edorTypeCMSave()">
		<Input class= cssButton type=Button value="取  消" onclick="initForm()">
		<Input type=Button value="返  回" class = cssButton onclick="returnParent()">
		<input type=hidden id="TypeFlag" name="TypeFlag">
		<input type=hidden id="fmtransact" name="fmtransact">
		<input type=hidden id="ContType" name="ContType">
		<input type=hidden id="GrpContNo" name="GrpContNo">
		<input type=hidden name="EdorAcceptNo">
		<input type=hidden name="AppntNo">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>