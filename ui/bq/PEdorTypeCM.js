var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量


function verifyPage()
{ 
	//以年龄和性别校验身份证号
	if (fm.all("IDType").value == "0") 
	var strChkIdNo = chkIdNo(fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);

	if (strChkIdNo!="" && strChkIdNo!=null)
	{
		alert(strChkIdNo);
		return false;
	}
	//校验身份证号(新)
	if (fm.all("IDType").value == "0" || fm.all("IDType").value == "5") {
		var strCheckIdNo = checkIdNo(fm.all("IDType").value,fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);

		if ("" != strCheckIdNo && strCheckIdNo!=null)
		{
			alert(strCheckIdNo);
			return false;
		}
	}
	return true;
}

function verifyAppntInfo()
{
	if (fm.AppntIDType.value == "0")
	 {
		var strChkIdNo = chkIdNo(fm.all("AppntIDNo").value, fm.all("AppntBirthday").value, fm.all("AppntSex").value);

		if (strChkIdNo!="" && strChkIdNo!=null)
		{
			alert("投保人信息有误："+strChkIdNo);
			return false;
		}
	}
	
	if(fm.all("Name").value.length < 2)
    {
    	alert("客户姓名长度不能小于2!");
    	return false;
    }
	return true;
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if (!verifyAppntInfo()){
	  return false;
  }
  if (fm.IDType.value == "0" || fm.IDType.value == "5")
  {
    if (fm.IDNo.value == "")
    {
      fm.IDNo.focus();
      return false;
    }
    if (!verifyPage())
    {
      fm.IDNo.focus();
      return false;
    }
  }
  if(!verifyRiskInfo())
  {
  	return false;
  }
  if(checkIsTax()){
	  if(!checkTaxInfo()){
		  return false;
	  }
  }
  return true;
}

function edorTypeCMSave()
{
  if (!beforeSubmit())
  {
    return false;
  }
  
  if (!intSalary_20120104())
  {
    return false;
  }
//	if (fm.Name.value == fm.NameBak.value && fm.Sex.value == fm.SexBak.value &&
//		fm.Birthday.value == fm.BirthdayBak.value && fm.IDType.value == fm.IDTypeBak.value &&
//		fm.IDNo.value == fm.IDNoBak.value&&fm.OccupationCode.value == fm.OccupationCodeBak.value&&
//		fm.BankCodeBak.value == fm.BankCode.value&&
//   	fm.AccNameBak.value == fm.AccName.value&&
//    fm.BankAccNoBak.value == fm.BankAccNo.value&&
//    fm.Marriage.value == fm.MarriageBak.value&&
//    fm.Relation.value == fm.RelationBak.value
//		)
//	{
//		alert('被保人资料未发生变更！');
//		return;
//	}
//  		
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.all('fmtransact').value = "INSERT||MAIN";
	fm.submit();

}

function customerQuery()
{	
	window.open("./LCInsuredQuery.html");
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content,Result )
{
	try{ showInfo.close(); } catch(e) {}
	window.focus();
	if (fm.all("TypeFlag").value == "G")
	{
	  top.opener.queryClick2();
	}
	
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var tTransact=fm.all('fmtransact').value;
	  if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
	  	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
	  	//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
	  	var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[29,4,14,15,16,11]);
			
			fm.all('BankCode').value = turnPage.arrDataCacheSet[0][0];
		}
	  
	//	else if (tTransact=="QUERY||DETAIL")
	//	if (tTransact=="QUERY||MAIN")
		if (tTransact=="QUERY||DETAIL")
		{
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
			turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
			//	alert(Result);
			turnPage.strQueryResult  = Result;
			//使用模拟数据源，必须写在拆分之前
			turnPage.useSimulation   = 1;  

			//查询成功则拆分字符串，返回二维数组
			var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,tTransact);
			turnPage.arrDataCacheSet =chooseArray(tArr,[2,3,4,5,6,7,27,28,9,12]);
			//turnPage.arrDataCacheSet =chooseArray(tArr,[4,14,15,16,17,18,36,37,19,22]);
			
			fm.all('CustomerNo').value = turnPage.arrDataCacheSet[0][0];
			fm.all('Name').value = turnPage.arrDataCacheSet[0][1];
			fm.all('NameBak').value = turnPage.arrDataCacheSet[0][1];
			fm.all('Sex').value = turnPage.arrDataCacheSet[0][2];
			fm.all('SexBak').value = turnPage.arrDataCacheSet[0][2];
			fm.all('Birthday').value = turnPage.arrDataCacheSet[0][3];
			fm.all('BirthdayBak').value = turnPage.arrDataCacheSet[0][3];
			fm.all('IDType').value = turnPage.arrDataCacheSet[0][4];
			fm.all('IDTypeBak').value = turnPage.arrDataCacheSet[0][4];
			fm.all('IDNo').value = turnPage.arrDataCacheSet[0][5];
			fm.all('IDNoBak').value = turnPage.arrDataCacheSet[0][5];
			fm.all('OccupationType').value = turnPage.arrDataCacheSet[0][6];
			fm.all('OccupationTypeBak').value = turnPage.arrDataCacheSet[0][6];
			fm.all('OccupationCode').value = turnPage.arrDataCacheSet[0][7];
			fm.all('OccupationCodeBak').value = turnPage.arrDataCacheSet[0][7];
		//	fm.all('NativePlace').value = turnPage.arrDataCacheSet[0][8];
			fm.all('Marriage').value = turnPage.arrDataCacheSet[0][9];
			
			var customerNo = turnPage.arrDataCacheSet[0][0];
			var sql = "select RelationToMainInsured from LCInsured where InsuredNo = '" + customerNo + "' ";
			var arrResult = easyExecSql(sql);
			if (arrResult)
			{
				fm.all('Relation').value = arrResult[0][0];
			}
		    var sql = "select RelationToAppnt from LCInsured where InsuredNo = '" + customerNo + "' ";
			var arrResult = easyExecSql(sql);
			if (arrResult)
			{
				fm.all('RelationToAppnt').value = arrResult[0][0];
			}
			divLPInsuredDetail.style.display ='';
			divDetail.style.display='';
			queryBank();
			queryCont();
      showAllCodeName();
		} 
		else
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
			returnParent();
	  }
	}   
   getOccupationType(fm.OccupationCode.value);
}     
                 
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		// 查询保单明细
		queryInsuredDetail();
	}
}

function queryInsuredDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	tEdorType=fm.all('EdorType').value;
	tPolNo=fm.all('PolNo').value;
	tCustomerNo = fm.all('CustomerNo').value;
	parent.fraInterface.fm.action = "./InsuredQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorTypeICSubmit.jsp";
}

function returnParent()
{
	try
	{
   // top.opener.updatClick(); 
    top.opener.focus();
	  top.opener.getEdorItem();
	}
	catch (ex) {}
	top.close();
}

function queryCont()
{
	var customerNo = fm.all('CustomerNo').value;
	var appntNo = fm.all('AppntNo').value;
	var contNo = fm.all('ContNo').value;
  var strSQL;
  if (fm.TypeFlag.value == "G") //团单
  {
  	strSQL = "select a.ContNo, a.AppntName, a.InsuredName, a.CValiDate, a.Prem, a.Amnt, a.GrpContNo " +
  	             "from LCCont a " +
  	             "where a.ContNo = '" + contNo + "' ";
  }
  else
  {
  	//分为投保人和被保人两种情况	      
  	strSQL = "select a.ContNo, a.AppntName, a.InsuredName, a.CValiDate, a.Prem, a.Amnt, a.GrpContNo " +
  	             "from LCCont a,LCAppnt b " +
  	             "where AppFlag = '1' " +
  	             "and a.AppntNo = '" + appntNo + "' " +
  	             "and a.ContNo = b.ContNo " +
  	             "union " + 
  	             "select a.ContNo, a.AppntName, a.InsuredName, a.CValiDate, a.Prem, a.Amnt, a.GrpContNo " +
  	             "from LCCont a , LCInsured b " +
  	             "where a.ContNo = b.ContNo " + 
  	             "and a.AppFlag = '1' " +
  	             "and b.AppntNo = '" + appntNo + "' " +
  	             "and b.InsuredNo = '" + customerNo + "' ";
  }
	turnPage.queryModal(strSQL, ContGrid);
}

function intSalary_20120104()
{
	if (fm.all('salary').value!= "" && isNaN(fm.all('salary').value))
	{
		alert(fm.all('salary').value + "不是有效的数字!\n");
		return false;
	}
	return true;
}

//查询理赔金帐号
function queryBank()
{
  var customerNo = fm.all('CustomerNo').value;
  var contNo = fm.all('ContNo').value; 

  //先从P表查，再查c表
	var sql = "select BankCode, AccName, BankAccNo from LPInsured " +
	          "where insuredno = '" + customerNo + "' " +
	          "and ContNo = '" + contNo + "' " +
	          "and EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "'";		        
	var arrResult  = easyExecSql(sql, 1, 0);
	if (!arrResult) 
	{
	   sql = "select BankCode, AccName, BankAccNo from lcinsured " +
	         "where InsuredNo = '" + customerNo + "' " +
	         "and ContNo = '" + contNo + "'";
	   arrResult = easyExecSql(sql, 1, 0); 
	}
	if (arrResult)
	{
		fm.BankCode.value = arrResult[0][0];
		fm.AccName.value = arrResult[0][1];
		fm.BankAccNo.value = arrResult[0][2];
	}
}

//根据职业代码得到职业类型和职业名称
function getOccupationType(occupationCode)
{
	if (fm.OccupationType.value == "" || fm.OccupationCodeName.value == "")
  {
		var sql = "select OccupationType,OccupationName||'-'||workname from LDOccupation " +
		          "where OccupationCode = '" + occupationCode + "' ";
		var arrResult = easyExecSql(sql);
		if (arrResult)
		{
			fm.OccupationType.value = arrResult[0][0];
			fm.OccupationCodeName.value = arrResult[0][1];;
		}
	}
}

//查询客户基本信息
function getCustomerInfo(customerNo)
{
	var contNo = fm.all("ContNo").value
	var edorNo = fm.all("EdorNo").value; 
	var edorType = fm.all("EdorType").value;
	//先从P表中查询，再查C表
	var sql1 =
					 /* "select CustomerNo, Name, Sex, Birthday, IDType, IDNo, " +
            "       OccupationType, OccupationCode, Marriage " +
            "from LPPerson " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
            "and CustomerNo = '" + customerNo + "' ";
            */
            "select InsuredNo, Name, Sex, Birthday, IDType, IDNo, " +
            "       OccupationType, OccupationCode, Marriage, InsuredStat,NativePlace,Position,Salary,IDStartDate,IDEndDate,GrpInsuredPhone " +
            "from LPInsured " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
            "and ContNo = '" + contNo + "' " +
            "and InsuredNo = '" + customerNo + "' ";
            
            
   var sql2 =
            "select AppntNo, AppntName, AppntSex, AppntBirthday, IDType, IDNo, " +
            "       OccupationType, OccupationCode, Marriage,NativePlace,Position,Salary,IDStartDate,IDEndDate " +
            "from LPAppnt " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
            "and ContNo = '" + contNo + "' " +
            "and AppntNo = '" + customerNo + "' ";

           
  var arrResult1 = easyExecSql(sql1); 
  var arrResult2 = easyExecSql(sql2); 

  if ((!arrResult1)&&(!arrResult2))
	{
		sql1 = 
		     /*"select CustomerNo, Name, Sex, Birthday, IDType, IDNo, " +
		      "       OccupationType, OccupationCode, Marriage " +
		      "from LDPerson where CustomerNo = '" + customerNo + "' ";
		      */
		      "select InsuredNo, Name, Sex, Birthday, IDType, IDNo, " +
            "OccupationType, OccupationCode, Marriage, InsuredStat,NativePlace,Position,Salary ,IDStartDate,IDEndDate,GrpInsuredPhone " +
            " from LCInsured " +
            " where  ContNo = '" + contNo + "' " +
            "and InsuredNo = '" + customerNo + "' ";
            
		sql2 =  "select AppntNo, AppntName, AppntSex, AppntBirthday, IDType, IDNo, " +
            " OccupationType, OccupationCode, Marriage,NativePlace,Position,Salary,IDStartDate,IDEndDate " +
            "from LCAppnt " +
            "where ContNo = '" + contNo + "' "+
            "and AppntNo = '" + customerNo + "' "
            ;

		arrResult1 = easyExecSql(sql1);
		arrResult2 = easyExecSql(sql2);
		if ((!arrResult1)&&(!arrResult2))
		{
			alert("未查到该客户资料！");
			return false;
		}
	}
	if (arrResult1)
	{
		fm.all('CustomerNo').value = arrResult1[0][0];
		fm.all('Name').value = arrResult1[0][1];
		fm.all('NameBak').value = arrResult1[0][1];
		fm.all('Sex').value = arrResult1[0][2];
		fm.all('SexBak').value = arrResult1[0][2];
		fm.all('Birthday').value = arrResult1[0][3];
		fm.all('BirthdayBak').value = arrResult1[0][3];
		fm.all('IDType').value = arrResult1[0][4];
		fm.all('IDTypeBak').value = arrResult1[0][4];
		fm.all('IDNo').value = arrResult1[0][5];
		fm.all('IDNoBak').value = arrResult1[0][5];
		fm.all('OccupationType').value = arrResult1[0][6];
		fm.all('OccupationTypeBak').value = arrResult1[0][6];
		fm.all('OccupationCode').value = arrResult1[0][7];
		fm.all('OccupationCodeBak').value = arrResult1[0][7];
		fm.all('Marriage').value = arrResult1[0][8];
		fm.all('MarriageBak').value = arrResult1[0][8];
		fm.all('InsuredState').value = arrResult1[0][9];
		fm.all('nationality').value = arrResult1[0][10];
        fm.all('NativePlaceBak').value=arrResult1[0][10];
		fm.all('position').value = arrResult1[0][11];
		fm.all('salary').value = arrResult1[0][12];
		fm.all('IDStartDate').value = arrResult1[0][13];
		fm.all('IDEndDate').value = arrResult1[0][14];
		fm.all('GrpInsuredPhone').value = arrResult1[0][15];
	}
  if((!arrResult1)&&arrResult2)
  {
  	fm.all('CustomerNo').value = arrResult2[0][0];
		fm.all('Name').value = arrResult2[0][1];
		fm.all('NameBak').value = arrResult2[0][1];
		fm.all('Sex').value = arrResult2[0][2];
		fm.all('SexBak').value = arrResult2[0][2];
		fm.all('Birthday').value = arrResult2[0][3];
		fm.all('BirthdayBak').value = arrResult2[0][3];
		fm.all('IDType').value = arrResult2[0][4];
		fm.all('IDTypeBak').value = arrResult2[0][4];
		fm.all('IDNo').value = arrResult2[0][5];
		fm.all('IDNoBak').value = arrResult2[0][5];
		fm.all('OccupationType').value = arrResult2[0][6];
		fm.all('OccupationTypeBak').value = arrResult2[0][6];
		fm.all('OccupationCode').value = arrResult2[0][7];
		fm.all('OccupationCodeBak').value = arrResult2[0][7];
		fm.all('Marriage').value = arrResult2[0][8];
		fm.all('MarriageBak').value = arrResult2[0][8];
		fm.all('nationality').value = arrResult2[0][9];
        fm.all('NativePlaceBak').value=arrResult2[0][9];
		fm.all('position').value = arrResult2[0][10];
		fm.all('salary').value = arrResult2[0][11];
		fm.all('IDStartDate').value = arrResult2[0][12];
		fm.all('IDEndDate').value = arrResult2[0][13];
  }
	
	//得到被保人关系
	var sql = "select RelationToMainInsured from LPInsured " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
	          "and InsuredNo = '" + customerNo + "' " +
					  "and ContNo = '" + contNo + "' ";
	var arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		sql = "select RelationToMainInsured from LCInsured " +
		      "where InsuredNo = '" + customerNo + "' " +
					"and ContNo = '" + fm.ContNo.value + "' ";
	  arrResult = easyExecSql(sql);
	}
	if (arrResult)
	{
		fm.all('Relation').value = arrResult[0][0];
		fm.all('RelationBak').value = arrResult[0][0];
	}
    
     //得到与投保人的关系
    //2011-03-21
    //【OoO?】杨天政
    var sql = "select RelationToAppnt from LPInsured " +
            "where EdorNo = '" + edorNo + "' " +
            "and EdorTYpe = '" + edorType + "' " +
	          "and InsuredNo = '" + customerNo + "' " +
					  "and ContNo = '" + contNo + "' ";
	var arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		sql = "select RelationToAppnt from LCInsured " +
		      "where InsuredNo = '" + customerNo + "' " +
					"and ContNo = '" + fm.ContNo.value + "' ";
	  arrResult = easyExecSql(sql);
	}
	if (arrResult)
	{
		fm.all('RelationToAppnt').value = arrResult[0][0];
		fm.all('RelationToAppntBak').value = arrResult[0][0];
	}
	//如果团单被保人有导入联系电话，则显示导入的
	if("G" == fm.TypeFlag.value){
		var InPhone = "select Phone from lpdiskimport " +
		  "where InsuredNo = '" + customerNo + "' " +
				"and edorno = '" + edorNo + "' ";
		var GinPhone = easyExecSql(InPhone);
		if (GinPhone)
		{
			if(""!=GinPhone[0][0]){
				fm.all('GrpInsuredPhone').value = GinPhone[0][0];
			}
		}
	}
	divLPInsuredDetail.style.display ='';
	divDetail.style.display='';
	queryBank();
	queryCont();
	getOccupationType(fm.OccupationCode.value);
	
	//学平险汇交件保单显示投保人信息
	if("G" == fm.TypeFlag.value){
		var strSQL="select grpcontno from lcgrpcont where grpcontno="
				+ " (select grpcontno from lccont where contno='"+fm.ContNo.value+"') "
				+ " and ContPrintType='5' with ur";
		var schFlag = easyExecSql(strSQL);
		if(schFlag){
			DivAppntHead.style.display ='';
			DivAppntInfo.style.display ='';
			
			strSQL="select appntName,AppntSex,Appntbirthday,AppntIdtype,AppntIdNo from lcinsuredlist "
				 + " where EdorNo='"+ edorNo +"' and grpcontno='"+ edorNo +"' "
				 + " and insuredno='"+ customerNo +"' with ur "
			var appntInfo = easyExecSql(strSQL);
			
			if(!appntInfo)
			{
				strSQL="select appntName,AppntSex,Appntbirthday,AppntIdtype,AppntIdNo from lbinsuredlist "
					+ " where grpcontno='"+ schFlag[0][0] +"' and insuredno='"+ customerNo +"' with ur "
				appntInfo = easyExecSql(strSQL);
			}
			fm.all('AppntName').value = appntInfo[0][0];
			fm.all('AppntSex').value = appntInfo[0][1];
			fm.all('AppntBirthday').value = appntInfo[0][2];
			fm.all('AppntIDType').value = appntInfo[0][3];
			fm.all('AppntIDNo').value = appntInfo[0][4];
		}else{
			fm.all('AppntName').verify="";
			fm.all('AppntSex').verify="";
			fm.all('AppntBirthday').verify="";
			fm.all('AppntIDType').verify="";
			fm.all('AppntIDNo').verify="";
		}
	}else{
		fm.all('AppntName').verify="";
		fm.all('AppntSex').verify="";
		fm.all('AppntBirthday').verify="";
		fm.all('AppntIDType').verify="";
		fm.all('AppntIDNo').verify="";
	}
}
function afterQuery( arrReturn )
{
	fm.all('BankCode').value = arrReturn[0];
	fm.all('BankCodeBak').value = arrReturn[0];
	fm.all('BankCodeName').value = arrReturn[1];
}
function querybank()
{
	showInfo = window.open("LDBankQueryMain.jsp");
}
//add by Pangxy 20101209 万能险被保险人校验
function verifyRiskInfo()
{
	var tCustomerNo = fm.all('CustomerNo').value;
	//校验变更的客户是否为万能险的被保人(是：继续校验；否：校验通过)
	var tSql = "select count(1) "
			 + "  from lcpol "
			 + " where insuredno = '" + tCustomerNo + "'"
			 + "   and stateflag = '1' "
			 + "   and riskcode in ('330801','331801','332001')";
	var arrResult1 = easyExecSql(tSql);
	if(arrResult1[0][0] == "0")
	{
		return true;
	}
	//校验变更后信息是否与其他被保人客户信息一致(是：继续校验；否：校验通过)
	var tName = trim(fm.all('Name').value);
	var tSex = trim(fm.all('Sex').value);
	var tBirthday = trim(fm.all('Birthday').value);
	var tIDType = trim(fm.all('IDType').value);
	var tIDNo = trim(fm.all('IDNo').value);
	tSql = "select a.insuredno "
		 + "  from lcinsured a"
		 + " where a.name = '" + tName + "' "
		 + "   and a.sex = '" + tSex + "' "
		 + "   and a.Birthday = '" + tBirthday + "' "
		 + "   and a.IDType = '" + tIDType + "'"
		 + "   and a.IDNo = '" + tIDNo + "'"
		 + "   and exists (select 'Y' from lccont b where b.contno=a.contno and b.stateflag='1')"
		 + "   and a.insuredno <> '" + tCustomerNo + "'";
	var arrResult2 = easyExecSql(tSql);
	if(!arrResult2)
	{
		return true;
	}
	//校验其他被保人所属险种是否为万能险(是：校验失败，提示信息；否：校验通过)
	var tRecCnt = arrResult2.length;
	for(var i=0;i<tRecCnt;i++)
	{
		tSql = "select 'Y' "
			 + "  from lcpol "
			 + " where insuredno = '" + arrResult2[i][0] + "'"
			 + "   and stateflag = '1' "
			 + "   and riskcode in ('330801','331801','332001')";
		var arrResult3 = easyExecSql(tSql);
		if(arrResult3)
		{
			alert("变更后的被保险人有正在生效的万能险保单，不能进行变更操作！");
			return false;
		}
	}
	return true;
}


//判断是否为团险万能险种,并各种 赋值  
function getGUniversalFlag(){
	
	var tSql = "select 'Y',a.Prtno,b.joincompanydate,b.position,"
		 + "(select gradename from lcgrpposition where prtno=a.prtno and gradecode=b.position),"
		 + "(select distinct getdutykind from lpget where edorno=b.edorno and" 
		 +" getdutykind is not null and insuredno=a.insuredno and contno=a.contno fetch first 1 rows only)"
		 + "  from lcpol a,lpinsured b "
		 + " where a.prtno=b.prtno and a.insuredno=b.insuredno and b.edorNo = '" + fm.EdorNo.value + "' and "
		 + " a.insuredno = '" + top.opener.fm.all('CustomerNoBak').value + "'"
		 + "   and a.contno = '"+top.opener.fm.all('ContNoBak').value+"' "
		 + "   and exists (select 1 from lmriskapp where riskcode=a.riskcode "
		 + " and riskprop='G' and risktype4='4') fetch first 1 rows only with ur";
	var arrResult = easyExecSql(tSql);
	if(arrResult==null){
		
		tSql = "select 'Y',a.Prtno,b.joincompanydate,b.position,"
			 + "(select gradename from lcgrpposition where prtno=a.prtno and gradecode=b.position),"
			 + "(select distinct getdutykind from lcget where " 
			 +" getdutykind is not null and insuredno=a.insuredno and contno=a.contno fetch first 1 rows only)"
			 + "  from lcpol a,lcinsured b "
			 + " where a.prtno=b.prtno and a.insuredno=b.insuredno and "
			 + " a.insuredno = '" + top.opener.fm.all('CustomerNoBak').value + "'"
			 + "   and a.contno = '"+top.opener.fm.all('ContNoBak').value+"' "
			 + "   and exists (select 1 from lmriskapp where riskcode=a.riskcode "
			 + " and riskprop='G' and risktype4='4') fetch first 1 rows only with ur";
		 arrResult = easyExecSql(tSql);
	}
	 
	if(arrResult)
	{
		fm.Prtno.value = arrResult[0][1];
		fm.JoinCompanyDate.value = arrResult[0][2];
		fm.PositionU.value = arrResult[0][3];
		fm.PositionUName.value = arrResult[0][4];
		fm.GetDutyKind.value = arrResult[0][5];
		
		
		showOneCodeName("GetDutyKind", "GetDutyKindName"); 
		
		return "Y";
	}
	return "N";
}
function checkTaxFlag(){
	//先查P表
	var checkLPContSub = "select taxFlag,taxPayerType,taxNo,creditCode,gTaxNo,gOrgancomCode,PrtNo from lpcontSub where prtno = (select prtno from lccont where contno = '"+fm.ContNo.value+"') and edortype = 'CM' and edorno = '"+fm.all("EdorNo").value+"'";
	var checkResult = easyExecSql(checkLPContSub);
	if(checkResult==null||checkResult==""){
		//P表没有再查C表
		var checkSql = "select taxFlag,taxPayerType,taxNo,creditCode,gTaxNo,gOrgancomCode,PrtNo from lccontSub where prtno = (select prtno from lccont where contno = '"+fm.ContNo.value+"')";
		checkResult = easyExecSql(checkSql);		
	}
	var taxFlag = checkResult[0][0];
	var taxPayerType = checkResult[0][1];
	var taxNo = checkResult[0][2];
	var creditCode = checkResult[0][3];
	var gTaxNo = checkResult[0][4];
	var gOrgancomCode = checkResult[0][5];
	var PrtNo = checkResult[0][6];
	fm.PrtNo.value = PrtNo;
	if(taxFlag == "2"){
		document.getElementById("DivTaxInfo").style.display="none";
		document.getElementById("DivTaxInfoDetail").style.display="none";
	}else{
		if(taxPayerType == "01"){
			fm.TaxPayerType.value = "01";
			document.getElementById("pTax").style.display="none";
			document.getElementById("gTax").style.display="block";
			fm.GTaxNo.value = gTaxNo;
			fm.GOrgancomCode.value = gOrgancomCode;
		}
		if(taxPayerType == "02"){
			fm.TaxPayerType.value = "02";
			document.getElementById("gTax").style.display="none";
			document.getElementById("pTax").style.display="block";
			fm.TaxNo.value = taxNo;
			fm.CreditCode.value = creditCode;
		}
	}
	
}
function changeType(){
	if(fm.TaxPayerType.value == "01"){
		document.getElementById("pTax").style.display="none";
		document.getElementById("gTax").style.display="block";
		fm.GTaxNo.value = "";
		fm.GOrgancomCode.value = "";
	}
	if(fm.TaxPayerType.value == "02"){
		document.getElementById("gTax").style.display="none";
		document.getElementById("pTax").style.display="block";
		fm.TaxNo.value = "";
		fm.CreditCode.value = "";
	}
}
function checkIsTax(){
	var checkTaxRiskcode = "select 1 from lmriskapp where riskType4 = '4' and TaxOptimal = 'Y' and riskcode in (select riskcode from lcpol where contno = '"+fm.ContNo.value+"')";
	var result = easyExecSql(checkTaxRiskcode);
	if(result == "1"){
		return true;
	}
	return false;
}
function checkTaxInfo(){
	if(fm.TaxPayerType.value == "01"){
		var gTaxNo = fm.GTaxNo.value.trim();
		var gOrgancomCode = fm.GOrgancomCode.value.trim();
		if((gTaxNo==null||gTaxNo=="")&&(gOrgancomCode==null||gOrgancomCode=="")){
			alert("个税征收方式选择为“代扣代缴”时,则“单位税务登记证代码”和“单位社会信用代码”不能同时为空！");
			return false;
		}
		if(gTaxNo!=""&&gTaxNo!=null&&gTaxNo.length!=15&&gTaxNo.length!=18&&gTaxNo.length!=20){
            alert("单位税务登记证代码长度不符合要求！");
            return false;
        }
		if(gOrgancomCode!=""&&gOrgancomCode!=null&&gOrgancomCode.length!=18){
            alert("单位社会信用代码长度不符合要求！");
            return false;
        } 
	}
	if(fm.TaxPayerType.value == "02"){
		var taxno = fm.TaxNo.value.trim();
		var creditcode = fm.CreditCode.value.trim();
		if((taxno==null||taxno=="")&&(creditcode==null||creditcode=="")){
			alert("个税征收方式选择为“自行申报”时,则“个人税务登记证代码”和“个人社会信用代码”不能同时为空！");
			return false;
		}
		if(taxno!=""&&taxno!=null&&taxno.length!=15&&taxno.length!=18&&taxno.length!=20){
			 alert("个人税务登记证代码长度不符合要求！");
             return false;
		}
		if(creditcode!=""&&creditcode!=null&&creditcode.length!=18){
            alert("个人社会信用代码长度不符合要求！");
            return false;
          }
	}
	return true;
}