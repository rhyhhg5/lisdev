//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPEdorAppConfirmGrid();
  fm.submit(); //提交
}

//确认保全确认
function edorConfirm()
{
	//新增针对保全项目有没有配置需要扫描件结案的校验
//	if(!checkScanning()) return false;
  //20100126 zhanggm 审批意见为不同意，不允许保全确认
  var edorAcceptNo = fm.EdorAcceptNo.value;
  var strSql = "select statusno from lgwork where workno = '" + edorAcceptNo + "' ";
  var arrResult = easyExecSql(strSql);
  if( arrResult[0][0] == "5")
  {
    alert("保全已经确认，请不要重复确认！");
    return false;
  }
  if( arrResult[0][0] == "88")
  {
    alert("保全审批意见为[不同意]，请重新录入保全明细或撤销保全工单！");
    return false;
  }
  //20110614 杨天政，保全锁
  var findSql="select serialno from LCUrgeVerifyLog where serialno='"+edorAcceptNo+"' ";
  var arrResult_1 = easyExecSql(findSql);
  if( arrResult_1)
  {
   alert("正在保全确认或者重复理算操作，请不要再次点击保全确认！");
   return false;
   }
  if (!confirm("保全确认后将不可恢复，继续？"))
  {
    return false;
  }
  
   if(!clickBalanceMethodValue())
  {
  	return false;
  }

  
	if (!clickOK())
  {
  	if (!boforeSubmit())
	  {
	    return false;
	  }
  }
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, 
	"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	fm.submit();
	document.all.confirmButton.disabled=true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content , prtParams)
{
	try {showInfo.close(); } catch(e) {}
	window.focus();
  if (FlagStr == "Fail" )
  {      
  		if (content.toString() == "保全项目增人时出现金额错误,请点击[重复理算]回退到保全明细后重新添加[NI]项目明细")
  		{
  			alert(content.toString());
  			return false;
  		}       
		if (content.indexOf("需要财务交费") == -1)
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
			showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		}
		else
		{
			//window.open("./PEdorConfirmOptionMain.jsp?EdorAcceptNo="+edorAcceptNo+"&FeeFlag=1", "", "scrollbars=no,status=no");
		  afterSubmit2(flag ,content); 
		  document.all.redo.disabled=true;
		}    
		//由于自动审批未通过而产生的操作失败
		if(fm.failType.value == 1)
		{
			var newWindow = window.open("../task/TaskGetPrintMain.jsp?workNo="+mEdorNo);
			newWindow.focus();
			document.all.redo.disabled=true;
			top.close();
			top.opener.top.opener.location.reload();
//			if(fm.loadFlag.value == "GEDORAPPCONFIRM")
//			{
//				//保全确认界面从PEdorAppInput.jsp调出
//				top.opener.top.opener.focus();
//				top.opener.top.opener.location.reload();
//				top.opener.top.close();
//				top.close();
//			}
//			else
//			{
//				top.close();
//				top.opener.focus();
//				top.opener.location.reload();
//			}
		}
  }
	else
	{

		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		afterSubmit2(FlagStr , content); 
		document.all.redo.disabled=true;
	}
}

//打印批单
function PrtEdor(prtParams)
{
	if (fm.all("EdorNo").value == "0") {
		alert("由于数据出错，无法打印批单。");
		return;
	}	
	//提交表单，打印主批单
	parent.fraInterface.fm.action = "../f1print/GrpEndorsementF1P.jsp";
	parent.fraInterface.fm.target="f1print";	
	fm.submit();
	//打印费用清单	
	parent.fraInterface.fm.action = "./GEdorAppConfirmSubmit.jsp";
	parent.fraInterface.fm.target="fraSubmit";
}  

//针对定期结算的保单，在期保险期间结束即超过团单满期日期后，申请任何涉及收付费的保全项目，在保全确认时必须选择“即时结算”。

function dateCompare(startdate,enddate)   
{   
var arr=startdate.split("-");   
var starttime=new Date(arr[0],arr[1],arr[2]);    
var starttimes=starttime.getTime();  

var arrs=enddate.split("-");    
var lktime=new Date(arrs[0],arrs[1],arrs[2]);  
var lktimes=lktime.getTime(); 
  
if(starttimes>=lktimes)    
{   
return false;   
}   
else  
return true;   
}  

function clickBalanceMethodValue()
{
	var balanceMode = fm.all("BalanceMethodValue").value;
  	if(fm.all('CheckBalance').value=="1"&&balanceMode=="0")
  	{
  	 var balanceTime = getCurrentDate();
  	 var cXXXXc="select b.cinvalidate from lpgrpedoritem a ,lcgrpcont b where a.grpcontno=b.grpcontno and edorno='"+mEdorNo+"' ";
     var theTimexxa = easyExecSql(cXXXXc);
     var theTimexxx = theTimexxa.toString();    
     if(!(dateCompare(balanceTime,theTimexxx)))
       {
        alert("定期结算的团单满期日期后，申请任何保全项目在保全确认时必须选择“即时结算”。");
        return false;
       }
  	}
  	return true;
}

//*************************
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick()
{	
	// 初始化表格
	initGEdorAppConfirmGrid();
	
	var tReturn = parseManageComLimit();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select EdorNo,GrpContNo,EdorType,EdorValiDate,EdorAppDate from LPGrpEdorItem where EdorState='1' "//+" and"+tReturn
				 + getWherePart( 'EdorNo' );
	//alert(strSQL);
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGEdorAppConfirmGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GEdorAppConfirmGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

//显示被保人清单
function showInsuredList()
{
	window.open("./ShowInsuredList.jsp?EdorAcceptNo="+mEdorNo);
}

//减人：显示被保人清单
function showInsuredListZT(edorType)
{
	window.open("./GInsuredListZTMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=" + edorType);
}

//特需追加保费清单
function showInsuredListZB(edorType)
{
	window.open("./GInsuredListZBMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=ZB");
}

//团体医疗追加保费清单
function showInsuredListZA(edorType)
{
	window.open("./GInsuredListZAMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=ZA");
}

//团体万能追加保费清单 by gzh 
function showInsuredListTY()
{
	window.open("./GInsuredListTYMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=TY");
}

//团体万能增人清单 by xp 
function showInsuredListTZ()
{
	window.open("./GInsuredListTZMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=TZ");
}

//特需账户资金分配清单
function showInsuredListGA()
{
	window.open("./GInsuredListGAMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=GA");
}

//补充A公共保额分配清单
function showInsuredListGD()
{
	window.open("./GInsuredListGDMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=GD");
}

//万能团体账户资金分配清单
function showInsuredListTA()
{
	window.open("./GInsuredListTAMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=TA");
}

//被保险人理赔金帐户变更清单
function showInsuredListLP()
{
	window.open("./GInsuredListLPMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=LP");
}

function showInsuredListWT()
{
  window.open("./GInsuredListWTMain.jsp?edorAcceptNo=" + mEdorNo);
}


//无名单实名化清单
function showInsuredListWS()
{
  window.open("./GInsuredListWSMain.jsp?edorAcceptNo=" + mEdorNo);
}

//团险万能部分领取清单
function showInsuredListTL()
{
  window.open("./GInsuredListTLMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=TL");
}

//团险万能离职领取清单
function showInsuredListTQ()
{
  window.open("./GInsuredListTQMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=TQ");
}

//无名单实名化删除清单
function showInsuredListWD()
{
  window.open("./GInsuredListWDMain.jsp?edorAcceptNo=" + mEdorNo);
}

//无名单实名化删除清单
function showInsuredListLQ()
{
  window.open("./GInsuredListLQMain.jsp?edorAcceptNo=" + mEdorNo);
}

//显示重疾复效清单
function showInsuredListGX()
{
	window.open("./GInsuredListGXMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=GX");
}

//显示客户资料变更导入
function showInsuredListCM()
{
	window.open("./GInsuredListCMMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=CM");
}

//管理式医疗减少保额
function showInsuredListZE()
{
	window.open("./GInsuredListZEMain.jsp?edorAcceptNo=" + mEdorNo + "&edorType=ZE");
}


// 数据返回父窗口
function returnParent()
{
    //top.opener.focus();
    //top.opener.getGrpEdorItem();
	  //top.close();
	var customerNo = "";
	var result = easyExecSql("select customerNo from LGWork where workNo = '" + mEdorNo + "' ");
	if(result)
	{
		customerNo = result[0][0];
	}
  var url = "./GEdorInput.jsp?LoadFlag=TASK&DetailWorkNo=" + mEdorNo + "&CustomerNo=" + customerNo;
	parent.location.href = url;
}

function preReCal()
{
  //20100126 zhanggm 审批意见为同意，不允许重复理算
  var edorAcceptNo = fm.EdorAcceptNo.value;
  var strSql = "select statusno from lgwork where workno = '" + edorAcceptNo + "' ";
  var arrResult = easyExecSql(strSql);
  if( arrResult[0][0] == "99")
  {
    alert("保全审批意见为[同意]，不能进行重复理算操作！");
    return false;
  }
  if( arrResult[0][0] == "5")
  {
    alert("已经保全确认，不能进行重复理算操作！");
    return false;
  }
  //保全加锁
   var findSql="select serialno from LCUrgeVerifyLog where serialno='"+edorAcceptNo+"' ";
  var arrResult_1 = easyExecSql(findSql);
  if( arrResult_1)
  {
   alert("正在保全确认或者重复理算操作，请不要再次点击重复理算！");
   return false;
   }
     
	fm.fmtransact.value = "GRP";
	fm.action ="GEdorReCalSubmit.jsp";
	fm.submit();
}

function ctrlBalanceInfo()
{
	var sql = "select BalIntv,nextdate,baltodate from lcgrpbalplan where grpcontno="+
						"(select grpcontno from LPGrpEdorMain where edoracceptno='"+mEdorNo+
						"')";
	var arrResult = easyExecSql(sql, 1, 0);
	if (!arrResult)
	{	
		fm.all('BalanceInfo').style.display = "none";
		return;
	}
	var balInv=arrResult[0][0];
	if(balInv=='1')
	{
		//fm.all('BalanceInfo').style.display = "";
		fm.all('BalIntv').value='月度';
	}
	if(balInv=='3')
	{
		//fm.all('BalanceInfo').style.display = "";
		fm.all('BalIntv').value='季度';
	}
	if(balInv=='6')
	{
		//fm.all('BalanceInfo').style.display = "";
		fm.all('BalIntv').value='半年度';
	}
	if(balInv=='0')
	{
		fm.all('BalanceInfo').style.display = "none";
		fm.all('BalIntv').value='随时';
	}
	if(balInv=='12')
	{
		//fm.all('BalanceInfo').style.display = "";
		fm.all('BalIntv').value='年度';
	}
	fm.all('NextDate').value=arrResult[0][1];
	fm.all('BalDate').value=arrResult[0][2];
	return ;
}

//控制按钮的显示
function ctrlButton()
{
	var sql = "select EdorType from LPGrpEdorItem " +
	        "where EdorNo = '" + mEdorNo + "' ";
	var arrResult = easyExecSql(sql, 1, 0);
	if (!arrResult)
	{
		alert("没有找到保全信息！");
		return false;
	}
	
	var hasNI = false;
	var hasZT = false;
	var hasCT = false;
	var hasLP	= false;
	var hasZB	= false;
	var hasGA	= false;
	var hasGD = false;
	var hasWT = false;
	var hasWS = false;
	var hasWD = false;
	var hasLQ = false;
	var hasGX = false;
	// by gzh 万能险追加保费
	var hasTY = false;
	var hasTA = false;
	// by xp 万能险增人
	var hasTZ = false;
		// by xp 万能险部分领取
	var hasTL = false;
		// by xp 万能险离职领取
	var hasTQ = false;
	
	var hasCM = false;
	
	var hasZA = false;
	
	var hasZE = false;
	for (var i = 0; i < arrResult.length; i++)
	{
		//增人
		if (arrResult[i][0] == "NI")
		{
			hasNI = true;
		}
		//减人
		if(arrResult[i][0] == "ZT")
		{
			hasZT = true;
		}
		//退保
		if(arrResult[i][0] == "CT")
		{
			hasCT = true;
		}
		//被保险人理赔金帐户变更
		if(arrResult[i][0] == "LP")
		{
			hasLP = true;
		}
		//团险万能部分领取
		if(arrResult[i][0] == "TL")
		{
			hasTL = true;
		}
		//团险万能离职领取
		if(arrResult[i][0] == "TQ")
		{
			hasTQ = true;
		}
		//追加保费
		if(arrResult[i][0] == "ZB")
		{
			hasZB = true;
		}
		//团体医疗追加保费
		if(arrResult[i][0] == "ZA")
		{
			hasZA = true;
		}
		//by gzh 20110211 万能险追加保费
		if(arrResult[i][0] == "TY")
		{
			hasTY = true;
		}
		//团体万能资金分配
		if(arrResult[i][0] == "TA")
		{
			hasTA = true;
		}
		//团险万能增人
		if(arrResult[i][0] == "TZ")
		{
			hasTZ = true;
		}
		//资金分配
		if(arrResult[i][0] == "GA")
		{
			hasGA = true;
		}
		//保额分配
		if(arrResult[i][0] == "GD")
		{
			hasGD = true;
		}
		//犹豫期退保
		if(arrResult[i][0] == "WT")
		{
			hasWT = true;
		}
		//无名单实名化
		if(arrResult[i][0] == "WS")
		{
			hasWS = true;
		}
		//无名单实名化删除
		if(arrResult[i][0] == "WD")
		{
			hasWD = true;
		}
		//无名单实名化删除
		if(arrResult[i][0] == "LQ")
		{
			hasLQ = true;
		}
		//重疾复效
		if(arrResult[i][0] == "GX")
		{
			hasGX = true;
		}
		//客户资料变更导入联系方式
		if(arrResult[i][0] == "CM")
		{
			hasCM = true;
		}
		//管理式医疗减额
		if(arrResult[i][0] == "ZE")
		{
			hasZE = true;
		}
		if(hasNI && hasZT && hasCT && hasLP && hasZB && hasZA && hasGA && hasGD && hasWT && hasWS && hasWD && hasLQ && hasGX && hasTY && hasTZ && hasTA && hasTL && hasTQ && hasCM && hasZE)
		{
			break;
		}
	}
	
	if (hasNI == true)
	{
		fm.all('insuredList').style.display = "";
	}
	if(hasZT == true)
	{
		fm.all('insuredListZT').style.display = "";
	}
	if(hasCT == true)
	{
		fm.all('insuredListCT').style.display = "";
	}
	if(hasLP == true)
	{
		fm.all('insuredListLP').style.display = "";
	}
	if(hasZB == true)
	{
		fm.all('insuredListZB').style.display = "";
	}
	if(hasZA == true)
	{
		fm.all('insuredListZA').style.display = "";
	}
	//by gzh 20110211 万能险追加保费
	if(hasTY == true)
	{
		fm.all('insuredListTY').style.display = "";
	}
	if(hasTZ == true)
	{
		fm.all('insuredListTZ').style.display = "";
	}
	if(hasTA == true)
	{
		fm.all('insuredListTA').style.display = "";
	}
	if(hasGA == true)
	{
		fm.all('insuredListGA').style.display = "";
	}
	if(hasGD == true)
	{
		fm.all('insuredListGD').style.display = "";
	}
		if(hasTL == true)
	{
		fm.all('insuredListTL').style.display = "";
	}
		if(hasTQ == true)
	{
		fm.all('insuredListTQ').style.display = "";
	}
	if(hasWT == true)
	{
		fm.all('insuredListWT').style.display = "";
	}
	if(hasWS == true)
	{
		fm.all('insuredListWS').style.display = "";
	}
	if(hasWD == true)
	{
		fm.all('insuredListWD').style.display = "";
	}
	if(hasLQ == true)
	{
		fm.all('insuredListLQ').style.display = "";
	}
	if(hasGX == true)
	{
		fm.all('insuredListGX').style.display = "";
	}
	if(hasCM == true)
	{
		fm.all('insuredListCM').style.display = "";
	}
	if(hasZE == true)
	{
		fm.all('insuredListZE').style.display = "";
	}
	//控制保全确认和重复理算按钮的显示
	//var sqlConfirm = "  select edorState "
	//                 + "from LPEdorApp "
	//                 + "where edorAcceptNo = '" + mEdorNo + "' ";
	//var result = easyExecSql(sqlConfirm);
	//if(result)
	//{
	//    if(result[0][0] == "9")
	//    {
	//        fm.edorbutton.style.display = "none";
	//        fm.reConfirm.style.display = "none";
	//    }
	//}
	
	return true;
}
//业务类型改变时显示相应的信息录入页面
function afterCodeSelect()
{
	//缴费方式变更
	var payMode = fm.all("PayMode").value
	if ((payMode == "1") || (payMode == "2") || (payMode == "3") || (payMode == "9")) //自行缴费
	{
		var getMoney = getGetMoney(fm.EdorNo.value);
		if(getMoney != null && getMoney > 0){
			fm.fmtransact.value = "1";
		}else if(getMoney < 0){
			fm.fmtransact.value = "0";
		}
		if (fm.fmtransact.value == "1") //交费并且为即时结算
		{
			document.all("trEndDate").style.display = "";
		}
	  else //退费不设日期
	  {
	  	document.all("trEndDate").style.display = "none";
	  }
		document.all("trPayDate").style.display = "none";
		document.all("trBank").style.display = "none";
		document.all("trAccount").style.display = "none";
	}
	if (payMode == "4") //银行转帐
	{
		document.all("trEndDate").style.display = "none";
		document.all("trPayDate").style.display = "";
		document.all("trBank").style.display = "";
		document.all("trAccount").style.display = "";
	}
}

function afterCodeSelect2(){
	var balanceMethodValue = fm.all("BalanceMethodValue").value;
	if(balanceMethodValue == "1"){
		fm.all('BalanceInfo').style.display = "none";
		fm.all('trPayMode').style.display = "";
		fm.PayMode.value = "";
	}else{
		fm.all('BalanceInfo').style.display = "";
		fm.all('trPayMode').style.display = "none";
		document.all("trEndDate").style.display = "none";
	}
} 

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr, content)
{	
	try { showInfo.close(); } catch(re) { }
	if (fm.all("printMode")[0].checked)
	{
		//	top.opener.parent.fraPrint.F1Book1.book.filePrint(true);
		var width = screen.availWidth - 10;
	  var height = screen.availHeight - 28;
	  var parma = "top=0,left=0,width="+width+",height="+height;
		window.open("ShowGrpEdorPrint.jsp?EdorAcceptNo="+fm.EdorAcceptNo.value, "", parma);
	}   
	else if (fm.all("printMode")[1].checked) //批量打印 暂时没有实现
	{   
	}
	else if (fm.all("printMode")[2].checked)
	{   
	}
	
	try
	{
		top.close();
	}
	catch(re) { }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmitAsk( FlagStr, content)
{
  showInfo.close();
  window.focus();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
}
function boforeSubmit()
{
	if(checkISTQ()){
		if(!checkIsChangePay()){
			alert("缴费方式应该与在详细页面中录入保持一致");
			return false;
		}
	}
	if(checkLQZB(fm.EdorAcceptNo.value))
	{
		alert("部分领取、追加保费、解约、协议退保项目不得进行定期结算，请单独操作并进行即时结算");
		return false;
	}
  if (!verifyInput2())
  {
    return false;
  }
  
  var getMoney = getGetMoney(mEdorAcceptNo);
  if (getMoney != 0 &&fm.all("BalanceMethodValue").value == "1")
  {
    if (fm.PayMode.value == "0"||fm.PayMode.value == "")
    {
        alert("交费/付费方式不能为空。");
        return false;
    }
    if (fm.PayMode.value == "4")
    {
        if(fm.PayDate.value == "")
        {
            alert("转帐日期不能为空。");
            return false;
        }
        if(!isDate(fm.PayDate.value))
        {
            alert("转帐日期录入有误。");
            return false;
        }
    }
    if (getMoney > 0)
    {
        if (fm.PayMode.value == "1" || fm.PayMode.value == "2") 
        {
          if (fm.EndDate.value == "")
          {
            alert("截止日期不能为空");
            return false;
          }
          if (!isDate(fm.EndDate.value))
          {
            alert("截止日期录入有误。");
            return false;
          }
        }
    }
  }
  
  if (fm.PayMode.value == "4")
  {
    if (fm.Bank.value == "")
    {
      alert("转帐银行为空不能选择银行转帐！");
      return false;
    }
    if (fm.BankAccno.value == "")
    {
      alert("转帐帐号为空不能选择银行转帐！");
      return false;
    }
  }
  return true;
}


function getGetMoney(edorAcceptNo)
{
	var	strSql = "select GetMoney from LPGrpEdorMain where EdorAcceptNo = '" + edorAcceptNo + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("查询保全主表信息出错！");
	  return 0;
	}
	var getMoney = arrResult[0][0];
	return getMoney;
}


function checkOverFlow(mEdorAcceptNo)
{
	var strSql1="select grpcontno from LPGrpEdorMain where edoracceptno='"+mEdorAcceptNo+"'";
	var arrResult1 = easyExecSql(strSql1);
	if(arrResult1 == null||arrResult1=='')
	{
		alert("查询保全信息出错！");
	  return -1;
	}

	var grpContNo=arrResult1[0][0];
	var strSql="select case when (select coalesce(sum(summoney),0) from ("+
			"select  distinct a.incomeno no,a.sumactupaymoney summoney from LJAPay a,LJAGetEndorse b "+
			"where a.PayNo=b.ActuGetNo and b.GrpContNo='"+grpContNo+"' and a.IncomeType='B' "+
			"union "+
			"select  distinct a.otherno no,a.sumgetmoney*-1 summoney from LJAGet a,LJAGetEndorse b where a.ActuGetNo=b.ActuGetNo and b.GrpContNo='0000029701' and a.paymode='B'"+
			") as temp)>"+
			"(select prem*0.15 from lcgrpcont where grpcontno='"+grpContNo+"') then 1 else 0 end case "+
			"from dual";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null||arrResult=='')
	{
		alert("查询结算信息出错！");
	  return -1;
	}

	if(arrResult[0][0]=='1')
	{
		if(confirm("是否生成提前结算任务？"))
		{
			fm.action="./BqBalanceAdvanceAskSave.jsp?grppolno="+grpContNo;
			fm.submit();
		}
		return 1;
	}
	else
	{
		return 0;
	}
}

//费用为0则不设交费方式
function CtrlBalanceSel(edorAcceptNo)
{
	var getMoney = getGetMoney(edorAcceptNo);	
	if (getMoney == null)
	{
		return false;
	}
	if (getMoney == 0)//费用为0则不设交费方式
	{
		trPayMode.style.display = "none";
		return true;
	}else
	{
		var ret=isExistBalanceInfo(edorAcceptNo);//查询保单是否已经定义了结算频次
		if(ret=='-1')//查询出错
			return false;
		else if(ret=='1')//已经定义结算频次
		{
			var balIntv=getBalanceIntv(edorAcceptNo);
			if(balIntv=='-1')//查询出错
			{
				return false;
			}
			if(balIntv!='0')//如果是定期结算，隐藏结算方式、显示定期结算信息
			{
				trPayMode.style.display = "none";
				fm.all('CheckBalance').value="1";
				fm.all('BalanceMethod').style.display = "";
				var mEdorType =getEdorType();
				if(trim(mEdorType)=="YS")
				{
				   fm.all("BalanceMethodValue").value="1";
				   var zanShi="<table class =common> <tr class=common id='trBalanceMethod'> <TD  class= title>结算方式选择</TD> <TD  class= input>	<select name='BalanceMethodValue' style='width: 128; height: 23' onchange='afterCodeSelect2();' ><option value = '1'>即时结算</option></select>	</TD></tr>     </table>";
				   document.getElementById("BalanceMethod").innerHTML=zanShi;
				   afterCodeSelect2();
				}
				fm.all('BalanceInfo').style.display = "";
				return true;
			}else
			{
				return controlBalanceAnyTimeShow(getMoney,edorAcceptNo);
			}
		}else//没有定义结算频次,随时结算
		{
			return controlBalanceAnyTimeShow(getMoney,edorAcceptNo);
		}
	}
}

//控制即时结算的显示方式
function controlBalanceAnyTimeShow(getMoney,edorAcceptNo)
{
  fm.all("BalanceMethodValue").value = "1";
  
	if (getMoney > 0) //交费
	{
			document.all("FeeFlag").style.display = "";
			fm.fmtransact.value = "1";
			return true;	
	}else //退费
	{
			fm.fmtransact.value = "0";
			return true;
	}			
}

//查询保单是否已经定义了结算频次
function isExistBalanceInfo(edorNo)
{
	var sql = "select count(1) from lcgrpbalplan where grpcontno="+
						"(select grpcontno from LPGrpEdorMain where edoracceptno='"+edorNo+
						"')";
	var arrResult = easyExecSql(sql, 1, 0);
	if(arrResult==null||arrResult=='')
	{
		alert("查询结算计划信息失败！");
		return '-1';
	}
	if(arrResult[0][0]=='0')
	{
		return '0';
	}else
	{
		return '1';
	}
}

function getBalanceIntv(endorNo)
{
	var sql = "select balIntv from lcgrpbalplan where grpcontno="+
						"(select grpcontno from LPGrpEdorMain where edoracceptno='"+endorNo+
						"')";
	var arrResult = easyExecSql(sql, 1, 0);
	if (arrResult==null||arrResult=='')
	{	
		alert("查询结算计划信息失败！");
		return '-1';
	}
	else
	{
		return arrResult[0][0];
	}
}

//得到投保人帐户的变更金额
function queryAccMenoy(edorAcceptNo)
{
	var	strSql = "select coalesce(sum(money),0) from lcappacctrace where otherno = '" + edorAcceptNo + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult==null)
	{
		return '';
	}
	var getMoney = arrResult[0][0];
	return getMoney;
}

//得到交费记录号
function getNoticeNo()
{
	var strSql = "select getnoticeno from LJSGetEndorse a, LPGrpEdorMain b " +
	             "where a.EndorSementNo = b.EdorNo and b.EdorAcceptNo = '" + mEdorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.GetNoticeNo.value = arrResult[0][0];
	}
}

//得到交费银行和帐号
function queryAccount()
{
	var strSql = "select a.BankCode, a.BankAccno, a.AccName " +
				 "from LCGrpCont a, LPGrpEdorMain b " +
				 "where a.GrpContNo = b.GrpContNo " +
				 "and   b.EdorAcceptNo = '" + mEdorAcceptNo + "' ";
	var arrResult = easyExecSql(strSql);
	if (arrResult != null)
	{
		fm.all("Bank").value = arrResult[0][0];
		fm.all("BankName").value = arrResult[0][0];
		fm.all("BankAccno").value = arrResult[0][1];
		fm.all("AccName").value = arrResult[0][2];
		showOneCodeName("Bank", "BankName");  
	}
}

//function cancel()
//{
//    try
//    {
//        //从个人信箱直接到批单页面时找不到top.opener.top.opener.top.opener，将会抛出异常
//        top.opener.top.opener.top.opener.focus();
//        top.opener.top.opener.top.opener.location.reload();
//        top.opener.top.opener.top.close();
//    }
//    catch(e)
//    {
//        top.opener.top.opener.focus();
//        top.opener.top.opener.location.reload();
//    }
//    
//    top.opener.top.close();
//    top.close();
//}


//是否有补退费
function HaveFee(EdorAcceptNo)
{
	var strSQL ="select getmoney from LPEdorApp where EdorAcceptNo='"+EdorAcceptNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询保全申请主表时出错，保全受理号为"+EdorAcceptNo+"！");
		return "";
	}
	return arrResult[0][0];
}

//帐户余额是否有可领金额是否大于0
function AppAccHaveGetFee(customerNo)
{
	var strSQL ="select accgetmoney from lcappacc where customerNo='"+customerNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询投保人帐户表时出错，客户号为"+customerNo+"！");
		return "";
	}
	return arrResult[0][0];
}

//投保人帐户是否存在
function AppAccExist(customerNo)
{
	var strSQL ="select count(1) from lcappacc where customerNo='"+customerNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询投保人帐户表时出错！");
		return "";
	}
	if(Number(arrResult[0][0])==0)
	{
		return 'No';
	}
	else
	{
		return 'Yes';
	}
}

/*********************************************************************
 *  查询投保人帐户信息
 *  参数：contNo 合同号
 *********************************************************************
 */       
function getAppAccInfo()
{
  var sql = "select OtherNo from LPEdorApp " +
            "where EdorAcceptNo = '" + fm.EdorAcceptNo.value + "' ";
  arrResult = easyExecSql(sql);
	if (arrResult == null) 
	{
		return;
	}
	var customerNo = arrResult[0][0];
	var strSQL ="select customerno,accbala,accgetmoney " +
	            "from LCAppAcc "+	            
	            "where customerno = '" + customerNo + "' " +
	            "and state = '1'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("该客户号的帐户余额功能暂时不能用！");
		return;
	}
	
	fm.CustomerNo.value=customerNo;
	fm.AccBala.value=arrResult[0][1];
	fm.AccGetBala.value=arrResult[0][2];
	//fm.CustomerName.value=getCustomerName();
}

function useAcc()
{
	//预收项目不能使用余额
  var sql = "select * from LPGrpEdorItem " +
            "where EdorNo = '" + fm.EdorAcceptNo.value + "' " +
            "and EdorType = 'YS' ";
  var	result = easyExecSql(sql);
	if (((mGetMoney < 0) || ((mGetMoney > 0) && (Number(fm.AccGetBala.value) > 0))) && (result == null))
	{
		document.all("FeeFlag").style.display = "none";                    
		document.all("PayDiv").style.display = "none";    
		fm.ChkYesNo[0].checked = true;
	}
	else
	{
	    if(Number(fm.AccGetBala.value)<=0)
	    {
	       alert ("账户余额小于等于零时，不能使用账户余额抵充！");
	    }
		document.all("FeeFlag").style.display = "";
		document.all("PayDiv").style.display = "";
		fm.ChkYesNo[1].checked = true;
	}            
}

function notUseAcc()
{
	if (mGetMoney > 0)
	{
		document.all("FeeFlag").style.display = "";
	}
	document.all("PayDiv").style.display = "";       
}

function clickOK()
{
	var selectValue='';
	for(i = 0; i <fm.ChkYesNo.length; i++){
		if(fm.ChkYesNo[i].checked){
			selectValue=fm.ChkYesNo[i].value;			
			break;
		}
	}

	if (selectValue=='yes')
	{
		if(fm.all('AccType').value=='0')
		{
			fm.all("fmtransact2").value = "INSERT||TakeOut";	
		}
		if(fm.all('AccType').value=='1')
		{
			fm.all("fmtransact2").value = "INSERT||ShiftTo";	
		}
		return true;
	}
	else
	{
		fm.all("fmtransact2").value = "NOTUSEACC";	
		return false;
	}
}
// qulq 2007-4-5 特需险种在做LQ,ZB 时不能选择定期结算
// 有LQ，ZB 并且选择定期结算 return true
function checkLQZB(edorno)
{
	var sql = "select 1 from LPGrpEdorItem " +
            "where EdorNo = '" + edorno + "' " +
            "and EdorType in ('LQ','ZB','CT','XT')";
  var	result = easyExecSql(sql);
  if(result == null)
  {
  	return false;
  }
  else
  {
  	if(fm.all("BalanceMethodValue").value==0)
  	{
  		return true;
  	}
  	return false;
  }

}

function setRemak()
{
//20100126 zhanggm 如果经过审批，则自动显示审批意见。
  var edorAcceptNo = fm.EdorAcceptNo.value;
  var strSql = "select statusno from lgwork where workno = '" + edorAcceptNo + "' ";
  var arrResult = easyExecSql(strSql);
  if(arrResult[0][0] == "88" || arrResult[0][0] == "99")
  {
    document.all("Remarkdiv").style.display = "";
    strSql = "select remark from lgtracenodeop where workno = '"+edorAcceptNo+"' and operatortype = '6' "
           + "order by makedate desc ,maketime desc ";
    var arrResult1 = easyExecSql(strSql);
    var remark = arrResult1[0][0];
    fm.all('Remark').value = remark;
  }
  else
  {
    document.all("Remarkdiv").style.display = "none";
  }
}

//获取 edortype
function getEdorType(){
	var sql = "select EdorType from LPGrpEdorItem " +
	        "where EdorNo = '" + mEdorNo + "' ";
	var arrResult = easyExecSql(sql, 1, 0);
	if (!arrResult)
	{
		alert("没有找到保全信息！");
		return false;
	}
	return arrResult[0][0]
}
function payModeHelp(){
	window.open("../finfee/PayModeHelp.jsp");
}

function initTQPayInfo(edortype){
	var specialSql = "select detailtype,edorvalue from LPEdorEspecialData where edorno='"+mEdorNo+"' and edortype='"+edortype+"'";
	var specialResult = easyExecSql(specialSql);
	if(specialResult==null){
			alert("数据异常，请联系技术人员!");
			return ;
			
	}else{
		for(var i=0;i<specialResult.length;i++){
			if(specialResult[i][0]=='PAYMODE'){
				fm.all("PayMode").value=specialResult[i][1];
			}
			if(specialResult[i][0]=='PAYMODENAME'){
				fm.all("PayMode").text=specialResult[i][1];
			}
			if(specialResult[i][0]=='BANKCODE'){
				fm.Bank.value=specialResult[i][1];
			}
			if(specialResult[i][0]=='BANKCODENAME'){
				fm.BankName.value=specialResult[i][1];
			}
			if(specialResult[i][0]=='BANKACCNO'){
				fm.BankAccno.value=specialResult[i][1];
			}
			if(specialResult[i][0]=='ACCNAME'){
				fm.AccName.value=specialResult[i][1];
			}
		}
		if(fm.PayMode.value==3 || fm.PayMode.value==4){
			document.all("trPayDate").style.display = '';
			document.all("trBank").style.display = '';
			document.all("trAccount").style.display = '';
			document.all("trEndDate").style.display = 'none';
		}
		document.getElementById('trPayMode').style.display = 'none';
	}
}

function checkIsChangePay(){
	var payMode="";
	var payModeName="";
	//判断团险万能离职领取是否产生收退费
	var maneySQL = " select 1 from lpgrpedoritem where edoracceptno='"+mEdorNo+"' and edortype='TQ' and getmoney<>0 with ur " ;
	var moneyFlag = easyExecSql(maneySQL);
	if(moneyFlag==null||moneyFlag==""){
		return true;
	}
	
	var specialSql = "select detailtype,edorvalue from LPEdorEspecialData where edorno='"+mEdorNo+"' and edortype='TQ'";
	var specialResult = easyExecSql(specialSql);
	if(specialResult==null){
			alert("数据异常，请联系技术人员!");
			return ;
			
	}else{
		for(var i=0;i<specialResult.length;i++){
			if(specialResult[i][0]=='PAYMODE'){
				payMode=specialResult[i][1];
			}
			if(specialResult[i][0]=='PAYMODENAME'){
				payModeName=specialResult[i][1];
			}
		}
	}
	if((fm.PayMode.value!=payMode) || (fm.PayMode.text!=payModeName)){
		return false;
	}
	return true;
}

function checkISTQ(){
	var sql = "select EdorType from LPGrpEdorItem " +
	        "where EdorNo = '" + mEdorNo + "' ";
	
	var arrResult = easyExecSql(sql, 1, 0);
	if (!arrResult)
	{
		alert("没有找到保全信息！");
		return false;
	}
	
	for (var i = 0; i < arrResult.length; i++)
	{
		if (arrResult[i][0] == "TQ")
		{
			return true;
		}
	}
	return false;
}

/**
 * 新增保全结案时校验需不需要扫描件
 * @return
 */
function checkScanning(){
	var getOtherNoTypeSql = "select OtherNoType from lpedorapp where edoracceptno = '"+fm.EdorAcceptNo.value+"' with ur";
	var arrOtherNoType = easyExecSql(getOtherNoTypeSql);
	var getEdorTypeSql = "";
	var appObj = "";
	if(arrOtherNoType[0][0]==1){
		getEdorTypeSql = "select edortype from lpedoritem where edorno = '"+fm.EdorAcceptNo.value+"' with ur";	
		appObj = "I";
	}else if(arrOtherNoType[0][0]==2){
		getEdorTypeSql = "select edortype from lpgrpedoritem where edorno = '"+fm.EdorAcceptNo.value+"' with ur";
		appObj = "G";
	}
	var arrEdorType = easyExecSql(getEdorTypeSql);
	for(var i=0;i<arrEdorType.length;i+=1){
		var edorType = arrEdorType[i][0];
		var geteasyExecSqlsql = "select IsNeedScanning,EdorName from LMEdorItem where edorcode = '"+edorType+"' and appobj = '"+appObj+"' with ur";
		var arrIsNeedScanning = easyExecSql(geteasyExecSqlsql);
		var isNeedScanning = arrIsNeedScanning[0][0];
		if(isNeedScanning==1){
			var sql = "select 1 from es_doc_relation where bussno = '"+fm.EdorAcceptNo.value+"'";
			var arrResult = easyExecSql(sql);
			if(!arrResult){
				alert("保全项目："+arrIsNeedScanning[0][1]+"，需要扫描件才能结案！");
				return false;
			}
		}					
	}
	return true; 
}