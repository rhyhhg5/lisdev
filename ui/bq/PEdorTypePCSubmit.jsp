<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PEdorTypePCSubmit.jsp
//程序功能：
//创建日期：2005-12-30
//创建人  QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String flag;
  String content;
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  String edorNo = request.getParameter("EdorNo");
  String contNo = request.getParameter("ContNo");
  String polNo[] = request.getParameterValues("PolGrid1");
  String payIntv[] = request.getParameterValues("PolGrid8");
  
  LCPolSet tLCPolSet = new LCPolSet();
  for (int i = 0; i < polNo.length; i++)
  {
    LCPolSchema tLCPolSchema = new LCPolSchema();
    tLCPolSchema.setPolNo(polNo[i]);
    tLCPolSchema.setPayIntv(payIntv[i]);
    tLCPolSet.add(tLCPolSchema);
  }
  VData data = new VData();
  data.add(tLCPolSet);
  PEdorPCDetailUI tPEdorPCDetailUI = new PEdorPCDetailUI(gi, edorNo, contNo);
  if (!tPEdorPCDetailUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tPEdorPCDetailUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！";
  }
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>

