<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GInsuredListWT.jsp
//程序功能：
//创建日期：2005-05-24
//创建人  ：Yang Yalin
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="java.io.*"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bl.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>

<%@page import="oracle.sql.*"%>
<%@page import="oracle.jdbc.driver.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  boolean operFlag = true;
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
    //生成文件名
  Calendar cal = new GregorianCalendar();
  String min=String.valueOf(cal.get(Calendar.MINUTE));
  String sec=String.valueOf(cal.get(Calendar.SECOND));
  String downLoadFileName = "犹豫期退保附批单_"+tG.Operator+"_"+ min + sec + ".xls";
  String filePath = application.getRealPath("/");
  System.out.println("filepath................"+filePath);
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  String edorAcceptNo = request.getParameter("edorAcceptNo");
    
  LPGrpEdorItemDB tLPGrpEdorItemDB = new LPGrpEdorItemDB();
  tLPGrpEdorItemDB.setEdorAcceptNo(edorAcceptNo);
  tLPGrpEdorItemDB.setEdorType(BQ.EDORTYPE_WT);
  LPGrpEdorItemSet tLPGrpEdorItemSet = tLPGrpEdorItemDB.query();
    LPGrpEdorItemSchema schema = tLPGrpEdorItemSet.get(1);
    //为了在保全确认后查询，需要连LCInsured和LBInsured
    String sql = "select a.insuredName, a.insuredNo, "
                         + "  (select codeName from LDCode "
                         + "  where codeType = 'sex' and code = a.insuredSex),"
                         + "  a.insuredBirthday, b.IDType, b.IDNo,sum(a.prem) "
                         + "from LPPol a, LCInsured b "
                         + "where a.contNo = b.contNo "
                         + "  and a.insuredNo = b.insuredNo "
                         + "  and a.edorNo = '" + schema.getEdorNo() + "' "
                         + "  and a.edorType='" + schema.getEdorType() + "' "
                         + "  and a.grpContNo='" + schema.getGrpContNo() + "' "
                         + "group by a.insuredName, a.insuredNo, a.insuredSex, "
                         + "  a.insuredBirthday, b.IDType, b.IDNo "
                         + "union "

                         + "select a.insuredName, a.insuredNo, "
                         + "  (select codeName from LDCode "
                         + "  where codeType = 'sex' and code = a.insuredSex),"
                         + "  a.insuredBirthday, b.IDType, b.IDNo,sum(a.prem)"
                         + "from LPPol a, LBInsured b "
                         + "where a.contNo = b.contNo "
                         + "  and a.insuredNo = b.insuredNo "
                         + "  and a.edorNo = b.edorNo "
                         + "  and a.edorNo = '" + schema.getEdorNo() + "' "
                         + "  and a.edorType = '" + schema.getEdorType() + "' "
                         + "  and a.grpContNo = '" + schema.getGrpContNo() + "'"
                         + "group by a.insuredName, a.insuredNo, a.insuredSex, "
                         + "  a.insuredBirthday, b.IDType, b.IDNo ";
    System.out.println(sql);
    //设置表头
    String[][] tTitle = {{"被保人", "客户号", "性别", "出生日期", "证件类型", "证件号码", "退费金额"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
//        createexcellist.setRowColOffset(row+1,0);//设置偏移
    if(createexcellist.setData(sql,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
    if(errorFlag)
    {
%>
<html>
<script language="javascript">	
	alert("打印失败");
	top.close();
</script>
</html>
<%
  	}
%>