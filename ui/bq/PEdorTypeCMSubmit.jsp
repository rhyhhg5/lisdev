<%
//程序名称：PEdorTypeCMSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
  <%
  String flag = "";
  String content = "";
  String typeFlag = request.getParameter("TypeFlag");
  String tGUFlag = request.getParameter("GUFlag");
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  
  LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
  tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
  tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
  tLPEdorItemSchema.setInsuredNo(request.getParameter("AppntNo"));
  
  LDPersonSchema tLDPersonSchema = new LDPersonSchema();
  tLDPersonSchema.setCustomerNo(request.getParameter("CustomerNo"));
  tLDPersonSchema.setName(request.getParameter("Name"));
  tLDPersonSchema.setSex(request.getParameter("Sex"));
  tLDPersonSchema.setBirthday(request.getParameter("Birthday"));
  tLDPersonSchema.setIDType(request.getParameter("IDType"));
  tLDPersonSchema.setIDNo(request.getParameter("IDNo"));
  tLDPersonSchema.setOccupationCode(request.getParameter("OccupationCode"));
  tLDPersonSchema.setOccupationType(request.getParameter("OccupationType"));
  tLDPersonSchema.setMarriage(request.getParameter("Marriage"));
  //【OoO?】2011-11-01添加字段
  tLDPersonSchema.setNativePlace(request.getParameter("nationality"));
  tLDPersonSchema.setSalary(request.getParameter("salary"));
  tLDPersonSchema.setPosition(request.getParameter("position"));
  
  
  //理赔金账户
  LCInsuredSchema tLCInsuredSchema = new LCInsuredSchema();
  tLCInsuredSchema.setBankCode(request.getParameter("BankCode"));
  tLCInsuredSchema.setBankAccNo(request.getParameter("BankAccNo"));                
  tLCInsuredSchema.setAccName(request.getParameter("AccName"));	
  tLCInsuredSchema.setRelationToMainInsured(request.getParameter("Relation"));
  tLCInsuredSchema.setRelationToAppnt(request.getParameter("RelationToAppnt"));
  tLCInsuredSchema.setInsuredStat(request.getParameter("InsuredState"));
  //【OoO?】2011-11-01添加字段
   tLCInsuredSchema.setNativePlace(request.getParameter("nationality")); // 国籍
   tLCInsuredSchema.setPosition(request.getParameter("position")); // 岗位职务
   tLCInsuredSchema.setSalary(request.getParameter("salary")); // 年薪
   
   tLCInsuredSchema.setIDStartDate(request.getParameter("IDStartDate")); // 证件生效日期
   tLCInsuredSchema.setIDEndDate(request.getParameter("IDEndDate")); // 证件失效日期
   //被保险人联系方式变更
   tLCInsuredSchema.setGrpInsuredPhone(request.getParameter("GrpInsuredPhone"));
   
	//学平险记录投保人信息
	LCInsuredListSchema tLCInsuredListSchema=new LCInsuredListSchema();
	tLCInsuredListSchema.setAppntName(request.getParameter("AppntName"));
	tLCInsuredListSchema.setAppntSex(request.getParameter("AppntSex"));
	tLCInsuredListSchema.setAppntIdNo(request.getParameter("AppntIDNo"));
	tLCInsuredListSchema.setAppntIdType(request.getParameter("AppntIDType"));
	tLCInsuredListSchema.setAppntBirthday(request.getParameter("AppntBirthday"));
  
	System.out.println("InsuredState" + request.getParameter("InsuredState"));
	System.out.println("nationality：" + request.getParameter("nationality"));
	System.out.println("position：" + request.getParameter("position"));
	System.out.println("salary：" + request.getParameter("salary"));
	LCGetSchema tLCGetSchema = new LCGetSchema();
	if("Y".equals(tGUFlag)){
		
		tLCInsuredSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate")); // 服务年数起始日
		tLCInsuredSchema.setPosition(request.getParameter("PositionU")); // 级别
		tLCGetSchema.setGetDutyKind(request.getParameter("GetDutyKind"));//老年护理金领取方式
	}
  VData data = new VData();
  data.add(typeFlag);
  data.add(tGUFlag);
  data.add(gi);
  data.add(tLPEdorItemSchema);
  data.add(tLDPersonSchema);
  data.add(tLCInsuredSchema);
  data.add(tLCGetSchema);
  data.add(tLCInsuredListSchema);
	/**
	*税优客户资料变更增加税务登记证代码和社会信用代码  2018-2-22 liuzhenjiang
	*/
	if(request.getParameter("TaxPayerType")!=null&&!"".equals(request.getParameter("TaxPayerType"))){
		LPContSubSchema lpContSubSchema = new LPContSubSchema();
		lpContSubSchema.setPrtNo(request.getParameter("PrtNo"));
		lpContSubSchema.setTaxPayerType(request.getParameter("TaxPayerType"));
		lpContSubSchema.setTaxNo(request.getParameter("TaxNo"));
		lpContSubSchema.setCreditCode(request.getParameter("CreditCode"));
		lpContSubSchema.setGTaxNo(request.getParameter("GTaxNo"));
		lpContSubSchema.setGOrgancomCode(request.getParameter("GOrgancomCode"));
		data.add(lpContSubSchema);
	}
  PEdorCMDetailUI tPEdorCMDetailUI = new PEdorCMDetailUI();
  if (!tPEdorCMDetailUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tPEdorCMDetailUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！";
  }
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>