<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：PEdorAppConfirmSubmit.jsp
//程序功能：
//创建日期：2006-02-16
//创建人  ：MoJiao
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%    
    String flag = "";
    String content = "";
    String fmtransact=request.getParameter("fmtransact");
    
    GlobalInput gi = (GlobalInput)session.getValue("GI");    
    
    String customerNo = request.getParameter("CustomerNo");
    String accType = request.getParameter("AccType");
    String edorAcceptNo = request.getParameter("EdorAcceptNo");
    String destSource=request.getParameter("DestSource");
    String contType=request.getParameter("ContType");
    
	  LCAppAccTraceSchema tLCAppAccTraceSchema=new LCAppAccTraceSchema();
	  tLCAppAccTraceSchema.setCustomerNo(customerNo);
	  tLCAppAccTraceSchema.setAccType(accType);
	  if(contType.equals("0"))//个人
	  {
	  	
	  	tLCAppAccTraceSchema.setOtherType("10");//个人批改号
		}
		if(contType.equals("1"))//团体
		{
			
			tLCAppAccTraceSchema.setOtherType("3");//团体批改号
		}
	  
	  tLCAppAccTraceSchema.setOtherNo(edorAcceptNo);
	  tLCAppAccTraceSchema.setDestSource(destSource);
	  tLCAppAccTraceSchema.setOperator(gi.Operator);
	  
	   
	    //生成打印数据
	    VData tVData = new VData();
	    tVData.add(tLCAppAccTraceSchema);
			tVData.add(gi);
			PEdorAppAccConfirmBL tPEdorAppAccConfirmBL = new PEdorAppAccConfirmBL();
			if (!tPEdorAppAccConfirmBL.submitData(tVData, fmtransact))
			{
				flag="Fail";
				content = "保全理算成功。但处理帐户余额失败！";
			}
		
    content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">	
		parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
</script>
</html>