//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="1";

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function edorTypeICReturn() {
		initForm();
}

function verifyPage() { 
  var strSql = "select edorvalidate from lpedormain where polno='" + fm.PolNo.value + "' and (edortype='IC' or edortype='BB') and edorstate='0'";
  var arrResult = easyExecSql(strSql);
  if (arrResult!=null) {
    if (arrResult.length == 1) {
      if (dateDiff(arrResult[0][0], getCurrentDate('-'), 'M') <= 12) {
        alert("根据规定，该保全项目一年内只能做一个变更，该保单于（" + arrResult[0][0] + "）已做过一次姓名或性别或年龄的变更！");
        return false;
      }
    }
    
    if (arrResult.length == 2) {
      alert("根据规定，该保全项目做三次以上变更必须提交保全人工核保！");
    }
  }
  
  if (fm.Sex.value!=fm.Sex2.value && fm.Birthday.value!=fm.Birthday2.value) {
    alert("根据规定，该保全项目一年内只能做一个变更，因此不能性别和出生日期同时修改！");
    return false;
  }
  
  if (fm.Sex.value==fm.Sex2.value && fm.Birthday.value==fm.Birthday2.value) {
    alert("没有做任何变更不能保存！");
    return false;
  }
  
  if (fm.IDType.value=="0" && fm.IDNo.value==fm.IDNo2.value) {
    alert("身份证号码需要作相应变更！");
    return false;
  }
  
  return true;
}

function edorTypeICSave()
{
  if (!verifyInput()) return false;
  
  if (!verifyPage()) return false;
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
 	//showSubmitFrame(mDebug);
  fm.submit();
  //showSubmitFrame(mDebug);

}

function customerQuery()
{	
	window.open("./LCInsuredQuery.html");
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initLCInsuredGrid();
 //  showSubmitFrame(mDebug); 
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, Result, lapseFlag )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
  	//alert(lapseFlag);
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
    
 	 		//查询成功则拆分字符串，返回二维数组
	  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
	  		fm.all('PayYearsOld').value = tArr[0][240]+tArr[0][241]+"*"+tArr[0][189];
	  		fm.all('PayYearsNew').value = tArr[0][110]+tArr[0][111]+"*"+tArr[0][59];
	  		
		} else {
    	var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   		initForm();
  	}
  	
  }

}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		/**
		alert("aa:"+arrResult[0][5]);
		fm.all('Nationality').value = arrResult[0][5];
		fm.all('Marriage').value=arrResult[0][6];
		fm.all('Stature').value=arrResult[0][7];
		fm.all('Avoirdupois').value=arrResult[0][8];
		fm.all('ICNo').value=arrResult[0][9];
		fm.all('HomeAddressCode').value=arrResult[0][10];
		fm.all('HomeAddress').value=arrResult[0][11];
		fm.all('PostalAddress').value=arrResult[0][12];
		fm.all('ZipCode').value=arrResult[0][13];
		fm.all('Phone').value=arrResult[0][14];
		fm.all('Mobile').value=arrResult[0][15];
		fm.all('EMail').value=arrResult[0][16];
		*/
		// 查询保单明细
		queryInsuredDetail();
	}
}
function queryInsuredDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tPolNo=fm.all('PolNo').value;
	//alert(tPolNo);
	tCustomerNo = fm.all('CustomerNo').value;
	//alert(tCustomerNo);
	//top.location.href = "./InsuredQueryDetail.jsp?EdorNo=" + tEdorNo+"&EdorType="+tEdorType+"&PolNo="+tPolNo+"&CustomerNo="+tCustomerNo;
	parent.fraInterface.fm.action = "./InsuredQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorTypeICSubmit.jsp";
}
function returnParent()
{
	top.close();
}

function edorTypePYSave()
{
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}
