
<%
	//程序名称：PEdorTypeLNSubmit.jsp
	//程序功能：
	//创建日期：2002-07-19 16:49:22
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	//个人批改信息

	CErrors tError = null;
	//后面要执行的动作：添加，修改

	String FlagStr = "";
	String Content = "";

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");

	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String LoanMoney = request.getParameter("LoanMoney");
	String PolNo = request.getParameter("PolNo");

	// 准备传输数据 VData		
	VData tVData = new VData();
	tVData.add(tGlobalInput);
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("LoanMoney", LoanMoney);
	tTransferData.setNameAndValue("EdorNo", edorNo);
	tTransferData.setNameAndValue("EdorType", edorType);
	tTransferData.setNameAndValue("PolNo", PolNo);
	tVData.add(tTransferData);

	PEdorLNDetailUI tPEdorLNDetailUI = new PEdorLNDetailUI();
	if (!tPEdorLNDetailUI.submitData(tVData, "")) {
		System.out.println("Submit Failed! " + tPEdorLNDetailUI.mErrors.getErrContent());
		Content = "保存失败，原因是:" + tPEdorLNDetailUI.mErrors.getFirstError();
		FlagStr = "Fail";
	} else {
		Content = "保存成功";
		FlagStr = "Success";
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","2");
</script>
</html>

