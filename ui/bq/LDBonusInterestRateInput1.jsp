<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%@page import="java.util.*"%> 
<html>
	<%
		//程序名称：
		//程序功能：个人保全
		//创建日期：2011-10-31
		//创建人  ：XP
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	
	<%	
		GlobalInput tG = new GlobalInput(); 
	  	tG=(GlobalInput)session.getValue("GI");
	  	String Operator=tG.Operator;
	%>
	<head>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/CommonTools.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="./PEdor.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="./LDBonusInterestRateInput1.js"></SCRIPT>
		<%@include file="../common/jsp/UsrCheck.jsp"%>
		<%@include file="LDBonusInterestRateInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="./LDBonusInterestRateSave.jsp" method=post name=fm
			target="fraSubmit">
			<div align="right">
				<br>
				<Input type=button name="update" class=cssButton value="修  改"
					onclick="ldBonusInterestRateUpdate()">&nbsp;&nbsp;&nbsp;
				<Input type=button name="save" class=cssButton value="保  存"
					onclick="ldBonusInterestRateSave()">&nbsp;&nbsp;&nbsp;
				<Input type=Button name="query" class=cssButton value="查  询"
					onclick="ldBonusInterestRateQuery()">&nbsp;&nbsp;&nbsp;
				<Input type=Button name="delete" class=cssButton value="删  除"
					onclick="lmRateDelete()">
			</div>
			<table class=common>
				<tr class=common>
					<td class="title">
						险种代码
					</td>
					<td class="input"><Input class=codeNo id=RiskCode verify="险种编码|notnull" name=RiskCode readOnly ondblclick="return showCodeList('bonusriskcode',[this,RiskName], [0,1],null,null,null,1);" onkeyup="return showCodeListKey('bonusriskcode',[this,RiskName], [0,1],null,null,null,1);"><Input class=codename name=RiskName readonly >
					<font style=color:red;valign:right> *</font> </td>
					<td class="title">
						利率公布年度
					</td>
					<td class="input">
						<input class=common  name=bonusYear id=bonusYear onblur="checkYear(this)" readonly>
						<font style=color:red;valign:right> *</font>
					</td>
					<td class="title">
						保单承保年度起期
					</td>
					<td class="input">
						<input class=common  name=contYear  id=contYear onblur="checkYear(this)">
						<font style=color:red;valign:right> *</font>
					</td>
				</tr>
				<tr class=common>
					<td class="title">
						保单承保年度止期
					</td>
					<td class="input">
						<input class=common  name=contYearAfter  id=contYearAfter onblur="checkYear(this)">
						<font style=color:red;valign:right> *</font>
					</td>
					<td class="title">
						累积生息利率
					</td>
					<td class=input>
						<input class=common  name=accRate id=accRate onblur="checkRate()">
						
					</td>
					
				</tr>
				<tr>
					<td class=button>
						<input value="重  置" type=button onclick="BonusInterestBox();"class=cssButton >
					</td>
				</tr>	
			</table>
			<Div id="divRateInfo" style="display: ''">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divBonusInterestGrid);">
						</td>
						<td class=titleImg>
							查询结果
						</td>
					</tr>
				</table>
				 <!-- 信息（列表） -->
				  <div id="divBonusInterestGrid" style="display:''" >
					<table class=common>
				      <tr class=common>
					    <td text-align:left colSpan=1><span id="spanBonusInterestGrid"></span></td>
					  </tr>
					</table>
				  </div>
				<Div  id= "divturnPage" style="display:none" align="center">
					<div align="center">
				      <INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage.firstPage();"> 
				      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage();"> 					
				      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage();"> 
				      <INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage.lastPage();"> 	
				    </div>
				</DIV>
			<input type=hidden id="fmtransact" name="fmtransact" >
			<input type=hidden id="DRiskCode" name="DRiskCode" >  
			<input type=hidden id="DrateStartDate" name="DrateStartDate" >  
			<input type=hidden id="DrateEndDate" name="DrateEndDate" >  
			<input type=hidden id="DRate" name="DRate" >    
			<input type=hidden id="Operator" name="Operator" value='<%=Operator%>'>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
