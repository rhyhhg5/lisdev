<%
//程序名称：ReportManuInit.jsp
//程序功能：
//创建日期：2002-06-19 11:10:36
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	DivEdorManuUWAct.style.left=ex;
  	DivEdorManuUWAct.style.top =ey;
   	
    DivEdorManuUWAct.style.display ='';
    edorClicked();
}

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	fm.all('ContType').value='1';
	fm.all('EdorNo').value='';
	fm.all('PolNo').value = '';
	fm.all('uwGradeFlag').value='';
	fm.all('LoadFlag').value = LoadFlag;
	if (LoadFlag == 'G')
	{
		divGrpEdor.style.display = '';
		divLPQuery.style.display = 'none';
		fm.all('GrpEdorNo').value = GrpEdorNo;
		fm.all('GrpPolNo').value = GrpPolNo;
		fm.all('GrpEdorType').value = GrpEdorType;
		//divBTedorManuUW.style.display = 'none';
	}
	else
	{
		divGrpEdor.style.display = 'none';
		divLPQuery.style.display = '';
	}	

  }
  catch(ex)
  {
    alert("在GPPEdorManuInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在GPPEdorManuInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
 
    initInpBox();
    
    initSelBox();    
    initPEdorMainGrid();
		initQuery();
    if (LoadFlag =='G')
	    grpQueryClick();
    
  }
  catch(re)
  {
    alert("PEdorManuInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 报案信息列表的初始化
function initPEdorMainGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批单号";    	//列名
      iArray[1][1]="150px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="保单号";         			//列名
      iArray[2][1]="150px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

       iArray[3]=new Array();
      iArray[3][0]="客户号";         			//列名
      iArray[3][1]="150px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="批改类型";         			//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[5]=new Array();
      iArray[5][0]="生效日期";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用

      iArray[6]=new Array();
      iArray[6][0]="申请日期";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="交退费金额";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      PEdorMainGrid = new MulLineEnter( "fm" , "PEdorMainGrid" ); 
      //这些属性必须在loadMulLine前
      PEdorMainGrid.mulLineCount = 10;   
      PEdorMainGrid.displayTitle = 1;
      PEdorMainGrid.canSel=1;
      PEdorMainGrid.selBoxEventFuncName ="reportDetailClick";
      //PEdorMainGrid.canChk=1;
      PEdorMainGrid.loadMulLine(iArray);  
      //PEdorMainGrid.detailInfo="单击显示详细信息";
      
      //PEdorMainGrid.detailClick=reportDetailClick;
      //这些操作必须在loadMulLine后面
      //SubReportGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
// 查询按钮
function grpQueryClick()
{
	var tEdorState;
	tEdorState ='2';
	
	var tUWGradeFlag ;
	tUWGradeFlag = top.opener.fm.all('uwGradeFlag').value;
	
	var tUWGrade;
	tUWGrade = fm.all('uwgrade').value;
	
	// 初始化表格
	initPEdorMainGrid();
	//alert("222");
	// 书写SQL语句
	var strSQL = "";
	var tReturn = parseManageComLimit();

	itjj++;
	strSQL = "select EdorNo,PolNo,InsuredNo,EdorType,EdorValiDate,EdorAppDate,GetMoney from LPEdorMain where "+itjj+"="+itjj+" and EdorState="+ tEdorState+" and uwstate in ('5','6')"+" "+" and"+tReturn
				 + getWherePart( 'EdorType','GrpEdorType','=','0') 
				 + getWherePart( 'GrpPolNo' )
				 + getWherePart('EdorNo','GrpEdorNo','=','0') + " order by edorno,polno";
	if (tUWGradeFlag=='P')
	{
		strSQL = "select EdorNo,PolNo,InsuredNo,EdorType,EdorValiDate,EdorAppDate,GetMoney from LPEdorMain where "+itjj+"="+itjj+" and EdorState="+ tEdorState+" and uwstate in ('5','6')"+" and"+tReturn
				 +" and edorno in (select distinct edorno from lpuwmaster where passflag <> '9' and appgrade = '"+tUWGrade+"')"
				 + getWherePart( 'EdorType','GrpEdorType','=','0') 
				 + getWherePart( 'GrpPolNo' )
				 + getWherePart('EdorNo','GrpEdorNo','=','0') + " order by edorno,polno";
	}
	else
	{
		strSQL = "select EdorNo,PolNo,InsuredNo,EdorType,EdorValiDate,EdorAppDate,GetMoney from LPEdorMain where "+itjj+"="+itjj+" and EdorState="+ tEdorState+" and uwstate in ('5','6')"+" and"+tReturn
				 +" and edorno in (select distinct edorno from lpuwmaster where passflag <> '9' and appgrade <= '"+tUWGrade+"')"
				 + getWherePart( 'EdorType','GrpEdorType','=','0') 
				 + getWherePart( 'GrpPolNo' )
				 + getWherePart('EdorNo','GrpEdorNo','=','0') + " order by edorno,polno";
	}
	//alert(strSQL);	
	itjj++;				 
	//查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  
	  //判断是否查询成功
	  if (turnPage.strQueryResult) 
	  {
	    //查询成功则拆分字符串，返回二维数组
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  
	  arrGrid = turnPage.arrDataCacheSet;
	  
	  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
	  turnPage.pageDisplayGrid = PEdorMainGrid;    
	          
	  //保存SQL语句
	  turnPage.strQuerySql     = strSQL; 
	  
	  //设置查询起始位置
	  turnPage.pageIndex       = 0;  
	  
	  //在查询结果数组中取出符合页面显示大小设置的数组
	  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
	  
	  //调用MULTILINE对象显示查询结果
	  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
	  }
	  
	  
}
function initQuery()
{
	
	
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select UWPopedom from lduser where usercode = '<%=tG.Operator%>'";
	
	//alert(strSQL);
				 
	//查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  //alert('aaa');
	  
	  //判断是否查询成功
	  if (turnPage.strQueryResult) 
	  {
	    //alert('aaa');
	    //查询成功则拆分字符串，返回二维数组
	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	  //alert("---usr:"+turnPage.arrDataCacheSet[0][0]);
	  fm.all('uwgrade').value = turnPage.arrDataCacheSet[0][0];	 	 
		}
}
</script>