//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

var arrDataSet;
var tDisplay;
var turnPage = new turnPageClass();
turnPage.pageLineNum = 20;       //根据需求，显示20行
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var theFirstValue="";
var theSecondValue="";
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示


function EasyExecSql()
{
	if( verifyInput2() == false ){
         return false;
    }
	var Quarter=fm.QuarterCode.value;
	var SqlStr = "";
	if(null == Quarter || ""==Quarter){
		alert("请选择结算季度");
		return false;
	}else if(Quarter == "1"){
		SqlStr = " and month(c.DueBalaDate - 1 days) = '3' ";
	}else if(Quarter == "2"){
		SqlStr = " and month(c.DueBalaDate - 1 days) = '6' ";
	}else if(Quarter == "3"){
		SqlStr = " and month(c.DueBalaDate - 1 days) = '9' ";
	}else if(Quarter == "4"){
		SqlStr = " and month(c.DueBalaDate - 1 days) = '12' ";
	}
	var strSQL = "select b.ManageCom, b.ContNo, c.PolNo, b.AppntName, signdate,month(c.DueBalaDate - 1 days) "
             + " ,nvl((select sum(money) From lcinsureacctrace where contno = b.ContNo and paydate <= c.DueBalaDate),0)"
             + " ,e.Name, e.GroupAgentCode "
             + " ,(select Name from LABranchGroup where AgentGroup = b.AgentGroup)"
             + " ,(select min(Mobile) from LCAppnt x, LCAddress y where x.AppntNo = y.CustomerNo and x.AddressNo = y.AddressNo and x.ContNo = b.ContNo) "
             + " ,(select CodeAlias from LDCode1 where CodeType = 'salechnl' and Code=b.SaleChnl) "
             + " from LCCont b, LCInsureAccBalance c, LAAgent e "
             + " where 1 = 1 "
             + " and b.ContNo = c.ContNo "
             + " and b.AgentCode = e.AgentCode and b.ManageCom like '"+fm.all('ManageCom').value+"%'"
             + " and year(DueBalaDate - 1 days) = " + fm.PolYear.value
             + " and length(sequenceno)<>14 "
             + getWherePart('b.SaleChnl', 'SaleChnl','=')
             + getWherePart('b.ContNo', 'ContNo')
             + SqlStr
             + "   and c.BalaCount > 0 "
             + "   and exists (select 1 from lmriskapp l,lcpol p where p.polno=c.polno and  l.riskcode=p.riskcode and l.risktype4='4' and l.taxoptimal='Y' union select 1 from lmriskapp l, lbpol pb where pb.polno = c.polno and l.riskcode = pb.riskcode and l.risktype4 = '4' and l.taxoptimal='Y') "
             + " group by  b.ManageCom, b.ContNo, c.PolNo, b.AppntName, signdate, c.PolMonth, c.InsuAccBalaAfter,c.SequenceNo,c.DueBalaDate, e.Name, e.GroupAgentCode,b.AgentGroup,b.SaleChnl "
             + " fetch first 2000 rows only with ur "
             ;
  fm.tempSQL.value =strSQL;
  turnPage.queryModal(strSQL, ContGrid);  
  if(ContGrid.mulLineCount == 0)
	{
	    alert("没有查询到保单信息！");
	}     
}

function save()
{
	if (GrpContGrid.mulLineCount ==0)
	{
		alert("没有保单明细信息，不能修改！");
		return false;
	}	
	
	if (!CheckData(fm.tCinvaliDate.value))
	{
		return false ;
	}
	
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

/**
*  日期格式校验，防止录入错误格式日期。
*/


function CheckData(cform){
	
 if (fm.tCinvaliDate.value=="" || fm.tCinvaliDate.value==null)
 {
    alert("截止日期不能为空，请输入！");
 		return false;
 } 
 if (!formatTime(fm.tCinvaliDate.value))
 {
    alert("截至日期格式错误！");
    return false;
 } 
   return true;
}

function formatTime(str)
{
  var   r   =   str.match(/^(\d{1,4})(-|\/)(\d{1,2})\2(\d{1,2})$/);     
  if(r==null) return   false;     
  var  d=  new  Date(r[1],   r[3]-1,   r[4]);     
  return  (d.getFullYear()==r[1]&&(d.getMonth()+1)==r[3]&&d.getDate()==r[4]);   
}

// 清单打印
function PrintList()
{
    if(!printAlert())
	{
	  return false;
	}
	if(ContGrid.mulLineCount==0)
	{
		alert("没有查询列表，不可以打印清单！");
		return false;
	}
    fm.action = "../bq/OmnipQuartersTaxPrtList.jsp";
    fm.submit();
}


function printOneNoticeNew()
{
	if (ContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < ContGrid.mulLineCount; i++)
	{
		if(ContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}
	if(tChked.length != 1)
	{
		alert("只能选择一条信息进行打印");
		return false;	
	}

	var tPolNo=ContGrid.getRowColData(tChked[0],3);
	var showStr="正在准备打印数据，请稍后...";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="../uw/PDFPrintSave.jsp?Code=SY003&OtherNo="+tPolNo+"&StandbyFlag3="+fm.PolYear.value+"&StandbyFlag1="+fm.QuarterCode.value;
	fm.submit();
}

//批量打印
function printAllNotice()
{
	if (ContGrid.mulLineCount == 0)
	{
		alert("打印列表没有数据");
		return false;
	}

	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < ContGrid.mulLineCount; i++)
	{	
		if(ContGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
	}


	if(tChked.length == 1)
	{
		alert("请选择多条记录");
		return false;	
	}
	
	if(tChked.length == 0)
	{
		fm.target = "fraSubmit";
		fm.action="../bq/OmnipQuartersAllBatPrt.jsp";
		fm.submit();
	}
	else
	{
		fm.target = "fraSubmit";
		fm.action="../bq/OmnipQuartersForBatPrt.jsp";
		fm.submit();
	}

}

function afterSubmit2(FlagStr,Content)
{
	showInfo.close();
	if (FlagStr == "Fail" ){
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }else if(FlagStr =="PrintError"){
   	 //window.parent.close(); 
   	 var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   }
}


//去空格
function trim(s)
{
  var l = s.replace( /^\s*/, ""); //去左空格
  var r = l.replace( /\l*$/, ""); //去右空格
  return r;
}