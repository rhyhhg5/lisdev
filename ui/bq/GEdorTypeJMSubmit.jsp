<%
//程序名称：PEdorTypeJMSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
  <%
  String flag = "";
  String content = "";
  String typeFlag = request.getParameter("TypeFlag");
  GlobalInput gi = (GlobalInput) session.getValue("GI");
  
  LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
  tLPEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPEdorItemSchema.setEdorType(request.getParameter("EdorType"));
  tLPEdorItemSchema.setContNo(request.getParameter("ContNo"));
  tLPEdorItemSchema.setInsuredNo(request.getParameter("AppntNo"));
  
  LICardActiveInfoListSchema tLICardActiveInfoListSchema=new LICardActiveInfoListSchema();
  tLICardActiveInfoListSchema.setName(request.getParameter("Name"));
  tLICardActiveInfoListSchema.setSex(request.getParameter("Sex"));
  tLICardActiveInfoListSchema.setBirthday(request.getParameter("Birthday"));
  tLICardActiveInfoListSchema.setCValidate(request.getParameter("Cvalidate"));
  tLICardActiveInfoListSchema.setIdNo(request.getParameter("IDNo"));
  tLICardActiveInfoListSchema.setMobile(request.getParameter("Mobile"));
  tLICardActiveInfoListSchema.setPhone(request.getParameter("Phone"));
  tLICardActiveInfoListSchema.setPostalAddress(request.getParameter("PostAddress"));
  tLICardActiveInfoListSchema.setZipCode(request.getParameter("ZipCode"));
  tLICardActiveInfoListSchema.setEMail(request.getParameter("Email"));
  tLICardActiveInfoListSchema.setIdType(request.getParameter("IDType"));
  tLICardActiveInfoListSchema.setOccupationCode(request.getParameter("OccupationCode"));
  tLICardActiveInfoListSchema.setOccupationType(request.getParameter("OccupationType"));
 
  String tCustomerNo=request.getParameter("CustomerNo");//客户号
  
  VData data = new VData();
  data.add(typeFlag);
  data.add(tCustomerNo);
  data.add(gi);
  data.add(tLPEdorItemSchema);
  data.add(tLICardActiveInfoListSchema);
  
  
  PEdorJMDetailUI tPEdorJMDetailUI = new PEdorJMDetailUI();
  if (!tPEdorJMDetailUI.submitData(data))
  {
    flag = "Fail";
    content = "数据保存失败！原因是：" + tPEdorJMDetailUI.getError();
  }
  else
  {
    flag = "Succ";
    content = "数据保存成功！";
  }
  content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>