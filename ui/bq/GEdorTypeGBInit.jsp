<%
//程序名称：GrpHealthFactoryQueryInit.jsp
//程序功能：
//创建日期：2005-04-10
//创建人  ：mqhu
//更新记录：  更新人 更新日期    更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="AscriptionRuleInput.js"></SCRIPT>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox(){
  try{
  
 

	// 保单查询条件
  }
  catch(ex)
  {
    alert("AscriptionRuleInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox(){
  try{
  }
  catch(ex)
  {
    alert("AscriptionRuleInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(){
  try{
    // alert("here 0");
    initInpBox();
   // alert("here 1");
    initSelBox();
   //  alert(" here 2");
    if(this.LoadFlag=="16")
    {
       divRiskPlanSave.style.display="none";
    }
       fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
        fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
        fm.all('GrpContNo1').value = top.opener.fm.all('GrpContNo').value;
        fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
        fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
        fm.all('ContType').value = top.opener.fm.all('ContType').value;
    //alert("here 3");
   GrpPerPolDefine();
   //alert("here 4");
    GrpPerPolDefineOld();
    GrpAscriptionRuleGradeGrid();
    initAscriptionRuleGrid();
    if(this.LoadFlag == "99"){
        //显示代码选择中文
   autoMoveButton.style.display="";     
    }  
     if(scantype=="scan")
  {
    setFocus();
  }  
  }
  catch(re)
  {
    alert("AscriptionRuleInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var ContPlanGrid;


function initAscriptionRuleGradeGrid(){
	var iArray = new Array();
	try{
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=10;
	iArray[0][3]=0;
	//alert("not here");
        iArray[1]= new Array();
	iArray[1][0]="级别代码";
	iArray[1][1]="10px";
	iArray[1][2]=2;
	iArray[1][3]=2;
	iArray[1][10]="test";
	iArray[1][11]= "0|^1|^2|^3|^4|^5|^6|^7|^8|^9|^10|^11|^12|^13|^14|^15|^16|^16|^18|^19|^20|" ; 
	
	iArray[2]=new Array();
	iArray[2][0]="级别名称";
	iArray[2][1]="30px";
	iArray[2][2]=10;
	iArray[2][3]=1;
	
	     AscriptionRuleGradeGrid = new MulLineEnter( "fm" , "AscriptionRuleGradeGrid" );
      //这些属性必须在loadMulLine前
      AscriptionRuleGradeGrid.mulLineCount = 0;
      AscriptionRuleGradeGrid.displayTitle = 1;
      AscriptionRuleGradeGrid.hiddenPlus =0;
      AscriptionRuleGradeGrid.hiddenSubtraction = 0;
      
      AscriptionRuleGradeGrid.addEventFuncName = "addClick";
      AscriptionRuleGradeGrid.loadMulLine(iArray);
	}
	    catch(ex) {
      alert(ex);
    }
    }

function initAscriptionRuleOldGrid(){
	var iArray = new Array();
	try{
	iArray[0]=new Array();
	iArray[0][0]="序号";
	iArray[0][1]="30px";
	iArray[0][2]=10;
	iArray[0][3]=0;
	//alert("not here");
        iArray[1]= new Array();
	iArray[1][0]="归属规则代码";
	iArray[1][1]="100px";
	iArray[1][2]=10;
	iArray[1][3]=0;
	
	iArray[2]=new Array();
	iArray[2][0]="归属规则名称";
	iArray[2][1]="300px";
	iArray[2][2]=10;
	iArray[2][3]=0;
	
	     AscriptionRuleOldGrid = new MulLineEnter( "fm" , "AscriptionRuleOldGrid" );
      //这些属性必须在loadMulLine前
      AscriptionRuleOldGrid.mulLineCount = 0;
      AscriptionRuleOldGrid.displayTitle = 1;
      AscriptionRuleOldGrid.hiddenPlus = 1;
      AscriptionRuleOldGrid.hiddenSubtraction = 1;
      AscriptionRuleOldGrid.canSel=1;
      AscriptionRuleOldGrid.selBoxEventFuncName = "ShowAscriptionRule"; 
      AscriptionRuleOldGrid.loadMulLine(iArray);
	}
	    catch(ex) {
      alert(ex);
    }
    }
// 要约信息列表的初始化
function initAscriptionRuleNewGrid(tImpGrpContNo) {
    var iArray = new Array();

    try {
      iArray[0] = new Array();
      iArray[0][0] = "序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1] = "30px";            		//列宽
      iArray[0][2] = 10;            			//列最大值
      iArray[0][3] = 0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[1] = new Array();
      iArray[1][0] = "险种编码";    	        //列名
      iArray[1][1] = "60px";            		//列宽
      iArray[1][2] = 100;            			//列最大值
      iArray[1][3] = 2;                       //是否允许输入,1表示允许，0表示不允许 2表示代码选择
      iArray[1][10] = "RiskRuleCode";         //引用代码："CodeName"为传入数据的名称
      iArray[1][11] = tImpGrpContNo;
      iArray[1][12] = "1|2|3|4|5|6|7|8|9|10";	//multine上的列位
      iArray[1][13] = "0|3|3|3|3|3|3|3|2|3";	//查询字段的位置
      iArray[1][18] = "270";
      iArray[1][19] = 1;
    

      iArray[2] = new Array();
      iArray[2][0] = "要素类别";    	        //列名
      iArray[2][1] = "60px";            		//列宽
      iArray[2][2] = 100;            			//列最大值
      iArray[2][3] = 2;                       //是否允许输入,1表示允许，0表示不允许 2表示代码选择
      iArray[2][4] = "RiskAscriptionRuleFactoryType";
      iArray[2][5] = "2|3|4|5|6|7|8";
      iArray[2][6] = "0|3|3|3|3|3|2";
      iArray[2][15] = "RiskCode";
      iArray[2][17] = "1";
      iArray[2][19] = 1;


      iArray[3] = new Array();
      iArray[3][0] = "要素目标编码";         		//列名
      iArray[3][1] = "80px";            		//列宽
      iArray[3][2] = 60;            			//列最大值
      iArray[3][3] = 2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][4] = "RiskAscriptionRuleFactoryNo";
      iArray[3][5] = "3|4|5|6";
      iArray[3][6] = "0|2|2|2";
      iArray[3][9] = "要素目标编码|len<=6";
      iArray[3][15] = "RiskCode";
      iArray[3][17] = "8";
      iArray[3][19] = 1;
      
      iArray[4] = new Array();
      iArray[4][0] = "要素计算编码";         		//列名
      iArray[4][1] = "80px";            		//列宽
      iArray[4][2] = 60;            			//列最大值
      iArray[4][3] = 2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][4] = "RiskAscriptionRuleFactory";
      iArray[4][5] = "4|5|6|7|10";
      iArray[4][6] = "0|1|2|3|2";
      iArray[4][9] = "要素计算编码|len<=4";
      iArray[4][15] = "RiskCode";
      iArray[4][17] = "8";      
      iArray[4][18] = "420";


      iArray[5] = new Array();
      iArray[5][0] = "要素内容";         		//列名
      iArray[5][1] = "300px";            		//列宽
      iArray[5][2] = 200;            			//列最大值
      iArray[5][3] = 0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6] = new Array();
      iArray[6][0] = "要素值";         		//列名
      iArray[6][1] = "80px";            		//列宽
      iArray[6][2] = 150;            			//列最大值
      iArray[6][3] = 1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7] = new Array();
      iArray[7][0] = "要素名称";         		//列名
      iArray[7][1] = "0px";            		//列宽
      iArray[7][2] = 150;            			//列最大值
      iArray[7][3] = 3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8] = new Array();
      iArray[8][0] = "查询条件";         		//列名
      iArray[8][1] = "0px";            		//列宽
      iArray[8][2] = 150;            			//列最大值
      iArray[8][3] = 3;              			//是否允许输入,1表示允许，0表示不允许

      iArray[9] = new Array();
      iArray[9][0] = "团体保单险种号码";         		//列名
      iArray[9][1] = "0px";            		//列宽
      iArray[9][2] = 150;            			//列最大值
      iArray[9][3] = 3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10] = new Array();
      iArray[10][0] = "辅助列";
      iArray[10][1] = "60px";            	
      iArray[10][2] = 150;            		
      iArray[10][3] = 3;    

      AscriptionRuleNewGrid = new MulLineEnter( "fm" , "AscriptionRuleNewGrid" );
      //这些属性必须在loadMulLine前
      AscriptionRuleNewGrid.mulLineCount = 1;
      AscriptionRuleNewGrid.displayTitle = 1;
      AscriptionRuleNewGrid.loadMulLine(iArray);
    }
    catch(ex) {
      alert(ex);
    }
}
function initAscriptionRuleGrid(tImpGrpContNo) {
    var iArray = new Array();

    try {
      iArray[0] = new Array();
      iArray[0][0] = "序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1] = "30px";            		//列宽
      iArray[0][2] = 10;            			//列最大值
      iArray[0][3] = 0;           			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1] = new Array();
      iArray[1][0] = "规则名称";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1] = "30px";            		//列宽
      iArray[1][2] = 10;            			//列最大值
      iArray[1][3] = 0;              			//是否允许输入,1表示允许，0表示不允许

       iArray[2] = new Array();
      iArray[2][0] = "规则说明";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1] = "130px";            		//列宽
      iArray[2][2] = 10;            			//列最大值
      iArray[2][3] = 0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3] = new Array();
      iArray[3][0] = "规则内容";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[3][1] = "250px";            		//列宽
      iArray[3][2] = 10;            			//列最大值
      iArray[3][3] = 0;              			//是否允许输入,1表示允许，0表示不允许
      
         iArray[4] = new Array();
      iArray[4][0] = "所含要素的值";         		//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[4][1] = "50px";            		//列宽
      iArray[4][2] = 10;            			//列最大值
      iArray[4][3] = 0;              			//是否允许输入,1表示允许，0表示不允许
      
      
      


      AscriptionRuleGrid = new MulLineEnter( "fm" , "AscriptionRuleGrid" );
      //这些属性必须在loadMulLine前
      AscriptionRuleGrid.mulLineCount = 1;
      AscriptionRuleGrid.displayTitle = 1;
      AscriptionRuleGrid.loadMulLine(iArray);
    }
    catch(ex) {
      alert("Ascriptionrulegrid error");
    }
}


</script>