//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 
var turnPage4 = new turnPageClass(); 

var PolNo = "";
var prtNo = "";

//初始状态，初始化主险信息
//function initQuery()
//{
//	var sql = "select polno,insuredno,riskcode from LCPol where mainpolno=polno and ContNo='"+fm.ContNo.value+"' and appflag='1'";
//  var arrResult = easyExecSql(sql);
//	if(arrResult == null || arrResult== "")
//	{
//			alert("该保单下没有主险");
//	}
//	else
//	{
//		fm.PolNo.value  = arrResult[0][0];
//		fm.InsuredNo.value = arrResult[0][1];
//		fm.RiskCode.value = 	arrResult[0][2];
//	}
//}

//保存本次申请
function edorSave() {
  
  if(!checkEdorSave())
  {
    return false;
  }

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
  fm.action = "PEdorTypeNSSubmit.jsp";
  fm.all('fmtransact').value = "INSERT";
  fm.submit();
}

//校验保存的新增险种信息是否合法
function checkEdorSave()
{
  var sqlStr = "   select 1 "
                + "from LCPol "
                + "where contNo = '" + fm.ContNo.value + "' "
                + "   and appFlag = '2' ";
  var result = easyExecSql(sqlStr);
  if(result == null || result[0][0] == "" || result[0][0] == "null")
  {
    alert("请录入险种");
    return false;
  }
  
  for (i=0; i<LCPolAddGrid.mulLineCount; i++) 
  {
    var cValiDate = LCPolAddGrid.getRowColDataByName(i, "cValiDateType");
    if (cValiDate == "") 
    {
      alert("必须完成所有的险种录入才能保存申请！");
      return;
    }
    
    var sql = "select RiskType5 "
            + "from LMRiskApp "
            + "where riskCode = '" + LCPolAddGrid.getRowColDataByName(i, "riskCode") + "' ";
    var rs = easyExecSql(sql);
    if(rs&&LCPolAddGrid.getRowColDataByName(i, "riskCode") != "340601")
    {
      //长期险只能下一周年日生效
      if((rs[0][0] == '3' || rs[0][0] == '4') && cValiDate != "3")
      {
        alert("长期险生效日期编码只能为3："
          + LCPolAddGrid.getRowColDataByName(i, "insuredName") + " "
          + LCPolAddGrid.getRowColDataByName(i, "riskCode"));
        return false;
      }
      //短期险
      if((rs[0][0] == '1' || rs[0][0] == '2'))
      {
        if(cValiDate == "3")
        {
          alert("短期险生效日期不能为3：" 
            + LCPolAddGrid.getRowColDataByName(i, "insuredName") + " "
            + LCPolAddGrid.getRowColDataByName(i, "riskCode"));
          return false;
        }
        
      }
    }
    
    var sql1 = "select 1 "
        + "from lcpol "
        + "where contno= '" + fm.all('ContNo').value + "' and riskcode in ('340101','340301','340401') and appflag='1' and stateflag='1' with ur";
    var rs1 = easyExecSql(sql1);
    if(rs1&&LCPolAddGrid.getRowColDataByName(i, "riskCode") == "332301"){
    
//    	 if(cValiDate != "3")
//         {
           alert("金生无忧险种或美满今生险种或美满一生险种不能添加附加万能B款之外的险种");
           return false;
//         }
        
    }
    
    var sql2 = "select 1 "
        + "from lcpol "
        + "where contno= '" + fm.all('ContNo').value + "' and riskcode in ('340101','340301','340401') and appflag='1' and stateflag='1' with ur";
    var rs2 = easyExecSql(sql1);
    if(rs2&&LCPolAddGrid.getRowColDataByName(i, "riskCode") == "334801"){
    
    	 if(cValiDate != "3")
         {
           alert("添加附加万能险种的时候生效日类型只能为3-下一周年日生效");
           return false;
         }
        
    }
    var sql3 = "select 1 "
        + "from lcpol "
        + "where contno= '" + fm.all('ContNo').value + "' and riskcode in ('340301','340401','335301','335302','730503') and appflag='1' and stateflag='1' with ur";
    var rs3 = easyExecSql(sql3);
    if(rs3&&LCPolAddGrid.getRowColDataByName(i, "riskCode") == "340601"){
    
    	 if(cValiDate != "1")
         {
           alert("添加健康一生万能险种的时候生效日类型只能为1-即时");
           return false;
         }
        
    }
    
  }
  return true;
}

function RiskCodeSelect(obj) {
    //alert("gfsfdg");
	//showCodeListEx('EdorCode',[obj],null,null,null,null,1);
}

//显示提交结果
function afterSubmit( FlagStr, content ) {
	showInfo.close();
	
	try
	{
	  window.focus();
	}
	catch(ex)
	{}
	
	if(fm.all('fmAction').value=="DELETE||INSUREDRISK")
	{
		queryAddedPols();
		fm.action  = "./PEdorTypeNSSubmit.jsp";
	}

	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
	showInfo = showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
}

//返回，关闭当前窗口
function returnParent() {
	try
	{
		top.opener.getEdorItem();
	  top.close();
  }
  catch(ex)
  {
  	top.opener.focus();
  	top.close();
  }
}



/*********************************************************************
 *  查询已经承保的险种
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************/

function getPolDetail()
{
   var strSQL ="select contno,InsuredNo,InsuredName,"
              + "   (select riskname from lmriskapp where riskcode=lcpol.riskcode), "
              + "   RiskCode,PolNo,amnt,prem, "
              
              + "   CValiDate,PayToDate,GrpContNo "
              + "from LCPol "
              + "where ContNo = '" + fm.ContNo.value + "' "
              + "   and appflag = '1' "
              + "   and insuredNo = '" + fm.InsuredNo.value + "' ";
  
    turnPage3.pageDivName = "divPage3";
    turnPage3.pageLineNum = 100;
    turnPage3.queryModal(strSQL, PolGrid);
}

//查询新增的险种
function queryAddedPols()
{
	//显示已经增加的附加险
	  LCPolAddGrid.clearData("LCPolAddGrid");
		var strSql = "select  InsuredNo,InsuredName,riskcode,"
		             + "  (select riskname from lmriskapp where riskcode=lcpol.riskcode), "
		             + "  polno,amnt,mult,prem,'','', contNo,mainpolno,appFlag "
		             + "from lcpol "
		             + "where ContNo = '" + fm.all('ContNo').value + "' "
		             //+ "    and insuredNo = '" + fm.InsuredNo.value + "' "
		             + "    and appflag='2' "
		             + "order by proposalno ";
		turnPage4.pageDivName = "divPage4";
	  turnPage4.pageLineNum = 1000;
	  turnPage4.queryModal(strSql, LCPolAddGrid);
	  
	  for(var i = 0; i < LCPolAddGrid.mulLineCount; i++)
	  {
	    var sql = "select a.code, a.codeName "
	              + "from LDCode a, LPEdorEspecialData b "
	              + "where a.code = b.edorValue "
	              + "   and a.codeType = 'cvalidatetype' "
	              + "   and b.edorAcceptNo = '" + fm.EdorNo.value + "' "
	              + "   and b.edorNo = '" + fm.EdorNo.value + "' "
	              + "   and b.edorType = '" + fm.EdorType.value + "' "
	              + "   and b.detailType = 'CVALIDATETYPE' "
	              + "   and b.polNo = '" + LCPolAddGrid.getRowColDataByName(i, "polNo") + "' ";
	    var rs = easyExecSql(sql);
	    if(rs)
	    {
	      LCPolAddGrid.setRowColDataByName(i, "cValiDateType", rs[0][0]);
	      LCPolAddGrid.setRowColDataByName(i, "cValiDateTypeName", rs[0][1]);
	    }
	    var sql1 = "select 1 "
	        + "from lcpol "
	        + "where contno= '" + fm.all('ContNo').value + "' and riskcode in ('340101','340301','730201','340401') and appflag='1' and stateflag='1' with ur";
	    var rs1 = easyExecSql(sql1);
	    if((rs1&&LCPolAddGrid.getRowColDataByName(i, "riskCode") == "332301") || (rs1&&LCPolAddGrid.getRowColDataByName(i, "riskCode") == "334801")){
	    	var sql2 = "select code,codename from ldcode where codetype='cvalidatetype' and code='3'";
	    	var rs2 = easyExecSql(sql2);
		    if(rs2)
		    {
		      LCPolAddGrid.setRowColDataByName(i, "cValiDateType", rs2[0][0]);
		      LCPolAddGrid.setRowColDataByName(i, "cValiDateNameType", rs2[0][1]);
		    }
	        
	    }
	    if(LCPolAddGrid.getRowColDataByName(i, "riskCode") == "340501" || LCPolAddGrid.getRowColDataByName(i, "riskCode") == "340601"){
		    var sql2 = "select 1 "
		        + "from lcpol "
		        + "where contno= '" + fm.all('ContNo').value + "' and riskcode in ('340301','340401','335301','335302','730503') and appflag='1' and stateflag='1' with ur";
		    var rs2 = easyExecSql(sql2);
		    if(rs2&&LCPolAddGrid.getRowColDataByName(i, "riskCode") == "340501"){
		    	var sql3 = "select code,codename from ldcode where codetype='cvalidatetype' and code='3'";
		    	var rs3 = easyExecSql(sql3);
			    if(rs3)
			    {
			      LCPolAddGrid.setRowColDataByName(i, "cValiDateType", rs3[0][0]);
			      LCPolAddGrid.setRowColDataByName(i, "cValiDateNameType", rs3[0][1]);
			    }
		        
		    }
		    if(rs2&&(LCPolAddGrid.getRowColDataByName(i, "riskCode") == "340601" )){
		    	var sql4 = "select code,codename from ldcode where codetype='cvalidatetype' and code='1'";
		    	var rs4 = easyExecSql(sql4);
			    if(rs4)
			    {
			      LCPolAddGrid.setRowColDataByName(i, "cValiDateType", rs4[0][0]);
			      LCPolAddGrid.setRowColDataByName(i, "cValiDateNameType", rs4[0][1]);
			    }
		        
		    }
	    }
	  }
}


/*********************************************************************
 *  进入附加险信息录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function intoRiskInfo()
{
	//alert(fm.RiskCode.value);
	delInsuredVar();

	if(!addInsuredVar())
	{
	    //alert("没有查到被保险人信息！请确保数据的完整性！");
	    return;
	}

	if(fm.CValiDate.value == "")
	{
			fm.CValiDate.value = getCurrentDate();
	}
	//alert(fm.CValiDate.value);

  try{mSwitch.addVar('CValiDate','',fm.CValiDate.value);}catch(ex){};

	//选择险种单进入险种界面带出已保存的信息
	try{mSwitch.deleteVar('addedPolNo')}catch(ex){};
	//try{mSwitch.addVar('addedPolNo','',fm.PolNo.value);}catch(ex){};

	parent.fraInterface.window.location = "../app/ProposalInput.jsp?ScanFlag=0&LoadFlag=18&ContType=1&scantype=0&MissionID=&SubMissionID=&BQFlag=3&EdorType=NS&checktype=1";

}


/*********************************************************************
 *  进入附加险信息录入
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
//function modfiyRiskInfo()
//{
//	//alert(fm.RiskCode.value);
//	if(LCPolAddGrid.mulLineCount<1)
//	{
//	  alert("没有需要修改的附加险！");
//	  return;
//	}
//	var selIndex = LCPolAddGrid.getSelNo()-1;
//	if( selIndex < 0)
//	{
//	   alert("请选择需要修改的附加险！");
//	   return;
//	}
//	var addedPolNo = LCPolAddGrid.getRowColData(selIndex,5);
//
//
//	delInsuredVar();
//
//	if(!addInsuredVar())
//	{
//	    //alert("没有查到被保险人信息！请确保数据的完整性！");
//	    return;
//	}
//
//	if(fm.CValiDate.value == "")
//	{
//			fm.CValiDate.value = getCurrentDate();
//	}
//	//alert(fm.CValiDate.value);
//
//  try{mSwitch.addVar('CValiDate','',fm.CValiDate.value);}catch(ex){};
//
//	//选择险种单进入险种界面带出已保存的信息
//	try{mSwitch.deleteVar('addedPolNo')}catch(ex){};
//	try{mSwitch.addVar('addedPolNo','',addedPolNo);}catch(ex){};
//
//	parent.fraInterface.window.location = "../app/ProposalInput.jsp?ScanFlag=0&LoadFlag=18&ContType=1&scantype=0&MissionID=&SubMissionID=&BQFlag=3&EdorType=NS&checktype=1";
//
//}


/*
函数功能：直接调用承保时的删除功能
Lanjun 2005-7-21 15:24
*/
function delRecord()
{

	if(LCPolAddGrid.mulLineCount<1)
	{
	  alert("没有新增加的险种信息！");
	  return;
	}
	var selIndex = LCPolAddGrid.getSelNo()-1;
	if( selIndex < 0)
	{
	   alert("请选择需要删除的险种信息！");
	   return;
	}
	else
  {

			if(window.confirm("确定删除已经添加的附加险吗？"))
		  {
			    var addedPolNo = LCPolAddGrid.getRowColDataByName(selIndex,"polNo");
			    var insuredNo =  LCPolAddGrid.getRowColDataByName(selIndex,"insuredNo");

					fm.all('fmAction').value="DELETE||INSUREDRISK";
					fm.action="PEdorTypeNSDelPolSubmit.jsp?Polno="+addedPolNo+"&InsuredNo="+insuredNo+"&flag=BQ";

					var showStr="正在删除新增附加险信息，请您稍候并且不要修改屏幕上的值或链接其他页面";
					var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
					showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	 			  fm.submit(); //提交
	 			  fm.action  = "./PEdorTypeNSSubmit.jsp";
      }
	}
}






/*********************************************************************
 *  删除缓存中被保险人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function delInsuredVar()
{

    try{mSwitch.deleteVar('ContNo');}catch(ex){};
    try{mSwitch.deleteVar('InsuredNo');}catch(ex){};
    try{mSwitch.deleteVar('PrtNo');}catch(ex){};
    try{mSwitch.deleteVar('GrpContNo');}catch(ex){};

 //   try{mSwitch.deleteVar('AppntNo');}catch(ex){};
 //   try{mSwitch.deleteVar('ManageCom');}catch(ex){};
    try{mSwitch.deleteVar('ExecuteCom');}catch(ex){};
    try{mSwitch.deleteVar('FamilyType');}catch(ex){};
    try{mSwitch.deleteVar('RelationToMainInsure');}catch(ex){};
    try{mSwitch.deleteVar('RelationToAppnt');}catch(ex){};
    try{mSwitch.deleteVar('AddressNo');}catch(ex){};
    try{mSwitch.deleteVar('SequenceNo');}catch(ex){};
    try{mSwitch.deleteVar('Name');}catch(ex){};
    try{mSwitch.deleteVar('Sex');}catch(ex){};
    try{mSwitch.deleteVar('Birthday');}catch(ex){};
    try{mSwitch.deleteVar('IDType');}catch(ex){};
    try{mSwitch.deleteVar('IDNo');}catch(ex){};
    try{mSwitch.deleteVar('RgtAddress');}catch(ex){};
    try{mSwitch.deleteVar('Marriage');}catch(ex){};
    try{mSwitch.deleteVar('MarriageDate');}catch(ex){};
    try{mSwitch.deleteVar('Health');}catch(ex){};
    try{mSwitch.deleteVar('Stature');}catch(ex){};
    try{mSwitch.deleteVar('Avoirdupois');}catch(ex){};
    try{mSwitch.deleteVar('Degree');}catch(ex){};
    try{mSwitch.deleteVar('CreditGrade');}catch(ex){};
    try{mSwitch.deleteVar('BankCode');}catch(ex){};
    try{mSwitch.deleteVar('BankAccNo');}catch(ex){};
    try{mSwitch.deleteVar('AccName');}catch(ex){};
    try{mSwitch.deleteVar('JoinCompanyDate');}catch(ex){};
    try{mSwitch.deleteVar('StartWorkDate');}catch(ex){};
    try{mSwitch.deleteVar('Position');}catch(ex){};
    try{mSwitch.deleteVar('Salary');}catch(ex){};
    try{mSwitch.deleteVar('OccupationType');}catch(ex){};
    try{mSwitch.deleteVar('OccupationCode');}catch(ex){};
    try{mSwitch.deleteVar('WorkType');}catch(ex){};
    try{mSwitch.deleteVar('PluralityType');}catch(ex){};
    try{mSwitch.deleteVar('SmokeFlag');}catch(ex){};
    try{mSwitch.deleteVar('ContPlanCode');}catch(ex){};
    try{mSwitch.deleteVar('Operator');}catch(ex){};
    try{mSwitch.deleteVar('MakeDate');}catch(ex){};
    try{mSwitch.deleteVar('MakeTime');}catch(ex){};
    try{mSwitch.deleteVar('ModifyDate');}catch(ex){};
    try{mSwitch.deleteVar('ModifyTime');}catch(ex){};
    try{mSwitch.deleteVar('mainRiskPolNo');}catch(ex){};
    try{mSwitch.deleteVar('AgentCom');}catch(ex){};
    try{mSwitch.deleteVar('AgentType');}catch(ex){};
    try{mSwitch.deleteVar('AgentCode1');}catch(ex){};
    try{mSwitch.deleteVar('AgentCode');}catch(ex){};
    try{mSwitch.deleteVar('AgentGroup');}catch(ex){};
    try{mSwitch.deleteVar('AppntNo');}catch(ex){};
    try{mSwitch.deleteVar('AppntSex');}catch(ex){};
    try{mSwitch.deleteVar('RiskCode');}catch(ex){};
}

/*********************************************************************
 *  将被保险人信息加入到缓存中
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addInsuredVar()
{
	  var strSql = "select a.ContNo,a.insuredno,a.grpcontno,a.prtno,a.executecom,b.familytype,a.relationtomaininsured,a.relationtoappnt,"
               +"a.addressno,a.sequenceno,a.name,a.sex,a.birthday,a.idtype,a.idno,a.rgtaddress,a.marriage,a.marriagedate,a.health,a.stature,"
               +"a.avoirdupois,a.degree,a.creditgrade,a.bankcode,a.bankaccno,a.accname,a.joincompanydate,a.startworkdate,a.position,"
               +"a.salary,a.occupationtype,a.occupationcode,a.worktype,a.PluralityType,a.SmokeFlag,a.contplancode,a.insuredpeoples,"
               +"a.operator,a.makedate,a.maketime,a.modifydate,a.modifytime,b.SaleChnl,b.agentCom,b.agentType,b.agentCode1,"
               + "b.agentCode,b.agentGroup,b.appntNo,b.appntName,b.appntSex "
               +"from LCInsured a ,LCCont b "
	             +" where a.ContNo=b.ContNo and  a.contno='"+fm.ContNo.value
	             +"' and a.InsuredNo='"+fm.InsuredNo.value+"'";

		var arrResult = easyExecSql(strSql);
		if(arrResult == null || arrResult== "")
		{
			alert("没有查询到相应的被保险人信息！");
			return false;
		}

    try{mSwitch.addVar('ContNo','',arrResult[0][0]);}catch(ex){};
    //alert(arrResult[0][0]);
    try{mSwitch.addVar('InsuredNo','',arrResult[0][1]);}catch(ex){};
    try{mSwitch.addVar('PrtNo','',arrResult[0][2]);}catch(ex){};
    try{mSwitch.addVar('GrpContNo','',fm.GrpContNo.value);}catch(ex){};

    	//alert(fm.GrpContNo.value);
   //   try{mSwitch.addVar('AppntNo','',fm.AppntNo.value);}catch(ex){};
   //   try{mSwitch.addVar('ManageCom','',fm.ManageCom.value);}catch(ex){};
    try{mSwitch.addVar('ExecuteCom','',arrResult[0][4]);}catch(ex){};
    try{mSwitch.addVar('FamilyType','',arrResult[0][5]);}catch(ex){};
    try{mSwitch.addVar('RelationToMainInsure','',arrResult[0][6]);}catch(ex){};
    try{mSwitch.addVar('RelationToAppnt','',arrResult[0][7]);}catch(ex){};
    try{mSwitch.addVar('AddressNo','',arrResult[0][8]);}catch(ex){};
    try{mSwitch.addVar('SequenceNo','',arrResult[0][9]);}catch(ex){};
    try{mSwitch.addVar('Name','',arrResult[0][10]);}catch(ex){};

       // alert(arrResult[0][10]);

    try{mSwitch.addVar('Sex','',arrResult[0][11]);}catch(ex){};
    try{mSwitch.addVar('Birthday','',arrResult[0][12]);}catch(ex){};
    try{mSwitch.addVar('IDType','',arrResult[0][13]);}catch(ex){};
    try{mSwitch.addVar('IDNo','',arrResult[0][14]);}catch(ex){};
    try{mSwitch.addVar('RgtAddress','',arrResult[0][15]);}catch(ex){};
    try{mSwitch.addVar('Marriage','',arrResult[0][16]);}catch(ex){};
    try{mSwitch.addVar('MarriageDate','',arrResult[0][17]);}catch(ex){};
    try{mSwitch.addVar('Health','',arrResult[0][18]);}catch(ex){};
    try{mSwitch.addVar('Stature','',arrResult[0][19]);}catch(ex){};
    try{mSwitch.addVar('Avoirdupois','',arrResult[0][20]);}catch(ex){};
    try{mSwitch.addVar('Degree','',arrResult[0][21]);}catch(ex){};
    try{mSwitch.addVar('CreditGrade','',arrResult[0][22]);}catch(ex){};
    try{mSwitch.addVar('BankCode','',arrResult[0][23]);}catch(ex){};
    try{mSwitch.addVar('BankAccNo','',arrResult[0][24]);}catch(ex){};
    try{mSwitch.addVar('AccName','',arrResult[0][25]);}catch(ex){};
    try{mSwitch.addVar('JoinCompanyDate','',arrResult[0][26]);}catch(ex){};
    try{mSwitch.addVar('StartWorkDate','',arrResult[0][27]);}catch(ex){};
    try{mSwitch.addVar('Position','',arrResult[0][28]);}catch(ex){};

    try{mSwitch.addVar('Salary','',arrResult[0][29]);}catch(ex){};
    try{mSwitch.addVar('OccupationType','',arrResult[0][30]==""?" ":arrResult[0][30]); }catch(ex){};
    try{mSwitch.addVar('OccupationCode','',arrResult[0][31]);}catch(ex){};
    try{mSwitch.addVar('WorkType','',arrResult[0][32]);}catch(ex){};
    try{mSwitch.addVar('PluralityType','',arrResult[0][33]);}catch(ex){};
    try{mSwitch.addVar('SmokeFlag','',arrResult[0][34]);}catch(ex){};
    try{mSwitch.addVar('ContPlanCode','',arrResult[0][35]);}catch(ex){};
    try{mSwitch.addVar('InsuredPeoples','',arrResult[0][36]);}catch(ex){};
    try{mSwitch.addVar('Operator','',arrResult[0][37]);}catch(ex){};
    try{mSwitch.addVar('MakeDate','',arrResult[0][38]);}catch(ex){};
    try{mSwitch.addVar('MakeTime','',arrResult[0][39]);}catch(ex){};
    try{mSwitch.addVar('ModifyDate','',arrResult[0][40]);}catch(ex){};
    try{mSwitch.addVar('ModifyTime','',arrResult[0][41]);}catch(ex){};
    try{mSwitch.addVar('SaleChnl','',arrResult[0][42]);}catch(ex){};
    try{mSwitch.addVar('AgentCom','',arrResult[0][43]);}catch(ex){};
    try{mSwitch.addVar('AgentType','',arrResult[0][44]);}catch(ex){};
    try{mSwitch.addVar('AgentCode1','',arrResult[0][45]);}catch(ex){};
    try{mSwitch.addVar('AgentCode','',arrResult[0][46]);}catch(ex){};
    try{mSwitch.addVar('AgentGroup','',arrResult[0][47]);}catch(ex){};
    try{mSwitch.addVar('AppntNo','',arrResult[0][48]);}catch(ex){};
    //try{mSwitch.addVar('AppntName','',arrResult[0][49]);}catch(ex){};
    try{mSwitch.addVar('AppntSex','',arrResult[0][50]);}catch(ex){};
    try{mSwitch.addVar('RiskCode','',fm.RiskCode.value);}catch(ex){};
    //alert(fm.RiskCode.value);
    try{mSwitch.addVar('mainRiskPolNo','',fm.PolNo.value);}catch(ex){};
    //alert(mSwitch.getVar("mainRiskPolNo"));

    return true;
    //alert(arrResult[0][41]);
}



/**
 * FUNCTION:addCustomerImpart()
 * DESC:告知录入
 */
function addCustomerImpart()
{
  if(fm.InsuredNo.value == "")
  {
    alert("请选择被保人");
    return false;
  }
  
  //打开扫描件录入界面
  //sFeatures = "";
  //winOpen = window.open("./LPAddCustomerImpartMain.jsp?prtNo="+prtNo+"&EdorAcceptNo="+top.opener.fm.EdorAcceptNo.value+"&Subtype="+subType +"&BussNoType="+bussNoType+"&ManageCom="+top.opener.ManageCom+"&uwFlag="+uwFlag,"","status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  
  var openUrl = "./PEdorHealthImpartMain.jsp?EdorNo=" + fm.EdorNo.value
	              + "&EdorType=" + fm.EdorType.value
	              + "&ContNo=" + fm.ContNo.value
	              + "&InsuredNo=" + fm.InsuredNo.value
	              + "&Flag=1";
	window.open(openUrl);
}



//初始化保全试算相关的
function initPEdorTry()
{
	try{		
		if(top.opener.fm.all('TryFlag').value!=null
		&&top.opener.fm.all('TryFlag').value==1)
		{			
			fm.addImp.disabled = true;
		}
	}
  catch(ex)
  {
 //   alert("保全试算初始化错误"+ex.message);
  }
}



//------------以下为新增代码yyl-----------
//查询被保人信息
function queryLCInsured()
{
  var sql = "select insuredNo, name, sex, birthDay, idType, idNo "
            + "from LCInsured "
            + "where contNo = '" + fm.ContNo.value + "' ";
  turnPage2.pageDivName = "divPage1";
  turnPage2.pageLineNum = 100;
  turnPage2.queryModal(sql, LCInsuredGrid);
}

//查询被保人险种，在选择险种时触发
function queryInsuredInfo()
{
  var row = LCInsuredGrid.getSelNo() - 1;  //选择的行
  fm.InsuredNo.value = LCInsuredGrid.getRowColDataByName(row, "insuredNo");
  
  //已经投保的险种
  initPolGrid();
  getPolDetail();
}

function queryPolInfo()
{
  var row = LCPolAddGrid.getSelNo() - 1;  //选择的行
  fm.InsuredNo.value = LCPolAddGrid.getRowColDataByName(row, "insuredNo");
}