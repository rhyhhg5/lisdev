//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";

var turnPage = new turnPageClass();

function edorTypeGTReturn()
{
	top.close();
}

function edorTypeGTQuery()
{
	alert("Wait...");
}

function edorTypeGTSave()
{
	for (var i = 0; i < PolGrid.mulLineCount; i++) {
		var sumprem = parseFloat(PolGrid.getRowColData(i,3));
	  if(PolGrid.getRowColData(i,5) != "")
	  {
		var ratio = parseFloat(PolGrid.getRowColData(i,5));
		if(isNaN(ratio)){
			alert("请输入正确的退保比率(0--1)!");
			return;
		}
		else{
			var returnprem = sumprem * ratio;
			var str = String(returnprem);
			returnprem = pointTwo(str);
            PolGrid.setRowColData(i,4,returnprem);
		}
	  } 
		returnprem = PolGrid.getRowColData(i,4);
		//控制实际退保金额最大为可退金额的两倍
		var dblSumprem = sumprem * 2; 
		var dblReturnprem = returnprem * 1;
		
		if (dblSumprem < dblReturnprem) {
			alert("险种为" + PolGrid.getRowColData(i,1) + "的应退金额大于可退金额的两倍，风险控制，要继续请与信息技术部联系！");
			return;
		}
		else if (sumprem < dblReturnprem) {
			if (!confirm("险种为" + PolGrid.getRowColData(i,1) + "的应退金额大于可退金额，继续吗？"))
			return;
		}	
		
	}
	
	//团险保全复用，第一次进入时提供录入功能
	if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==false) {
	  //接收录入数据
	  for (var i = 0; i < PolGrid.mulLineCount; i++) {
	    top.opener.GTArr.push(PolGrid.getRowColData(i,4));
	  }
	  
	  top.opener.GrpBQ = true;
	  top.opener.pEdorMultiDetail();
	  top.close();
	} 
	//**************************************
	else {
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
    fm.all('fmtransact').value = "INSERT||MAIN";
    fm.submit();
  }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
//  alert(Result);
  showInfo.close();
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
 
  	if (tTransact=="QUERY||MAIN") {
      //       alert(tTransact);
  		var iArray;
  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet); 		
  		turnPage.strQueryResult  = Result;  		
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
  
      PolGrid.clearData("PolGrid");
  		for (var i = 0; i < tArr.length; i++) {
  			PolGrid.addOne("PolGrid");
  			PolGrid.setRowColData(i,1,tArr[i][11]);
  			PolGrid.setRowColData(i,2,tArr[i][1]);
  			PolGrid.setRowColData(i,3,tArr[i][42]); //Prem
  			PolGrid.setRowColData(i,4,tArr[i][43]); //SumPrem
        PolGrid.setRowColData(i,5,"");
  		}
  		
  		//团险保全复用，自动填写数值，并提交
    	if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==true) {
    	  for (var i = 0; i < PolGrid.mulLineCount; i++) {
    	    PolGrid.setRowColData(i, 4, top.opener.GTArr[i]);
    	  }
    	  
    	  edorTypeGTSave();
     	}
    }
    else {
      //团险保全复用，提交成功后，再次调用，以循环所有选择
      if (typeof(top.opener.GrpBQ)=="boolean" && top.opener.GrpBQ==true) {
			  top.opener.PEdorDetail();
			  top.close();
			} 		
	//add by jianglai at 2004-6-23 11:18
       var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
       showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
    }
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}
