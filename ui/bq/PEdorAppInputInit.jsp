<%
//程序名称：PEdorInputInit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>
<%
  String CurrentDate = PubFun.getCurrentDate();
  Date dt = PubFun.calDate(new FDate().getDate(CurrentDate), 1, "D", null);
  String ValidDate = CurrentDate;   
  Date mt = PubFun.calDate(new FDate().getDate(CurrentDate), 1, "M", null);
  GregorianCalendar mCalendar = new GregorianCalendar();
  mCalendar.setTime(mt);
  int mYears = mCalendar.get(Calendar.YEAR);
  int mMonths = mCalendar.get(Calendar.MONTH) + 1;
  String monAfterCurrent = mYears + "-" + mMonths + "-"  + 1;
  String dayAfterCurrent = new FDate().getString(dt);   
  String CurrentTime = PubFun.getCurrentTime();
	GlobalInput tG = (GlobalInput)session.getValue("GI");
%>                             

<script language="JavaScript">  
//从工单接收数据
var mLoadFlag = nullToEmpty("<%=request.getParameter("LoadFlag")%>");
var mEdorAcceptNo = nullToEmpty("<%=request.getParameter("DetailWorkNo")%>"); //保全号
var mOtherNo = nullToEmpty("<%=request.getParameter("CustomerNo")%>");        //客户号
var mOtherNoType = nullToEmpty("<%=request.getParameter("OtherNoType")%>");   //号码类型
var mEdorAppName = nullToEmpty("<%=request.getParameter("ApplyName")%>");      //申请人姓名
var mAppType = nullToEmpty("<%=request.getParameter("AcceptWayNo")%>");        //申请方式

//把null的字符串转为空
function nullToEmpty(string)
{
	if ((string == "null") || (string == "undefined"))
	{
		string = "";
	}
	return string;
}

//工单接口函数
function initTaskInterface()
{
	//工单管理接口
	if (mLoadFlag.substring(0, 4)  == "TASK")
	{
		divEdorAppInfo.style.display = "none";		
		fm.all("LoadFlag").value = mLoadFlag;
		fm.all("EdorAcceptNo").value = mEdorAcceptNo;
		fm.all("OtherNo").value = mOtherNo;
		fm.all("OtherNoType").value = "1";
		fm.all("EdorAppName").value = mEdorAppName;
		fm.all("AppType").value = mAppType;
		queryLCCont();
		getEdorItem();
	}
}

function initInpBox()
{ 
    var mySql = "select current date,current time from dual" ;
    var result = easyExecSql(mySql);
    tCurrentDate=result[0][0];
    tCurrentTime=result[0][1];
	fm.all("EdorValiDate").value = tCurrentDate;
	fm.all('EdorAppDate').value =tCurrentDate;
	fm.all('Operator').value = "<%=tG.Operator%>";
	fm.all('ContType').value ='1';
	fm.all('currentDay').value = '<%=CurrentDate%>';
	fm.all('dayAfterCurrent').value = '<%=dayAfterCurrent%>';    
	fm.all("monAfterCurrent").value = '<%=monAfterCurrent%>';
}

function initLCContGrid()
{                               
	var iArray = new Array();
	try
	{
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="保单号";
	  iArray[1][1]="100px";
	  iArray[1][2]=100;
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array();
	  iArray[2][0]="状态";
	  iArray[2][1]="50px";
	  iArray[2][2]=100;
	  iArray[2][3]=3;  
	  iArray[2][21]='state';
	  
	  iArray[3]=new Array();
	  iArray[3][0]="投保人";  
	  iArray[3][1]="100px";
	  iArray[3][2]=100;
	  iArray[3][3]=3;
	  
	  iArray[4]=new Array();
	  iArray[4][0]="主被保险人";
	  iArray[4][1]="100px";
	  iArray[4][2]=100;
	  iArray[4][3]=0;
	  
	  iArray[5]=new Array();
	  iArray[5][0]="生效日期";
	  iArray[5][1]="100px";
	  iArray[5][2]=100;
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array();
	  iArray[6][0]="交至日期";
	  iArray[6][1]="100px";
	  iArray[6][2]=100;
	  iArray[6][3]=0;
	  
	  iArray[7]=new Array();
	  iArray[7][0]="交费间隔";
	  iArray[7][1]="80px";
	  iArray[7][2]=100;
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array();
	  iArray[8][0]="期交保费";
	  iArray[8][1]="80px";
	  iArray[8][2]=100;
	  iArray[8][3]=0;
	  
	  iArray[9]=new Array();
	  iArray[9][0]="业务员代码";
	  iArray[9][1]="80px";
	  iArray[9][2]=100;
	  iArray[9][3]=0;
	  
	  iArray[10]=new Array();
	  iArray[10][0]="集体合同号";
	  iArray[10][1]="80px";
	  iArray[10][2]=100;
	  iArray[10][3]=3;
	  
	  iArray[11]=new Array();
	  iArray[11][0]="业务员姓名";
	  iArray[11][1]="70px"; 
	  iArray[11][2]=100;
	  iArray[11][3]=0;
	  
	  iArray[12]=new Array();
	  iArray[12][0]="保单状态";
	  iArray[12][1]="70px"; 
	  iArray[12][2]=100;
	  iArray[12][3]=0;
	  
	  LCContGrid = new MulLineEnter( "fm" , "LCContGrid" ); 
	  //这些属性必须在loadMulLine前
	  LCContGrid.mulLineCount = 0;   
	  LCContGrid.displayTitle = 1;
	  LCContGrid.canSel=1;
	  LCContGrid.selBoxEventFuncName ="getLCContDetail" ;     //点击RadioBox时响应的JS函数
	  LCContGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	  LCContGrid.hiddenSubtraction=1;
	  LCContGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}

function initEdorItemGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=10;            			//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="保全受理号";
    iArray[1][1]="100px";
    iArray[1][2]=100;
    iArray[1][3]=0;
    
    iArray[2]=new Array();
    iArray[2][0]="批单号";
    iArray[2][1]="80px";
    iArray[2][2]=100;
    iArray[2][3]=3;
    
    iArray[3]=new Array();
    iArray[3][0]="批改类型";
    iArray[3][1]="80px";
    iArray[3][2]=100;
    iArray[3][3]=0;   
    
    iArray[4]=new Array();
    iArray[4][0]="保单号";
    iArray[4][1]="80px";
    iArray[4][2]=100;
    iArray[4][3]=0;
    
    iArray[5]=new Array();
    iArray[5][0]="投保人";
    iArray[5][1]="80px";
    iArray[5][2]=100;
    iArray[5][3]=3;
    
    iArray[6]=new Array();
    iArray[6][0]="客户号";
    iArray[6][1]="80px";
    iArray[6][2]=100;
    iArray[6][3]=0;
    
    iArray[7]=new Array();
    iArray[7][0]="保全生效日期";
    iArray[7][1]="100px";
    iArray[7][2]=100;
    iArray[7][3]=0;
    
    iArray[8]=new Array();
    iArray[8][0]="处理状态";
    iArray[8][1]="80px";
    iArray[8][2]=100;
    iArray[8][3]=0;
    
    iArray[9]=new Array();
    iArray[9][0]="险种号";
    iArray[9][1]="80px";
    iArray[9][2]=100;
    iArray[9][3]=3;
  
	  EdorItemGrid = new MulLineEnter( "fm" , "EdorItemGrid" ); 
	  //这些属性必须在loadMulLine前
	  EdorItemGrid.mulLineCount = 0;   
	  EdorItemGrid.displayTitle = 1;
	  EdorItemGrid.canSel =1;
	  EdorItemGrid.selBoxEventFuncName ="getEdorItemDetail" ;     //点击RadioBox时响应的JS函数
	  EdorItemGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	  EdorItemGrid.hiddenSubtraction=1;
	  EdorItemGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
}


function initInusredGrid()
{                               
	var iArray = new Array();
	try
	{
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="30px";            		//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
	  iArray[1]=new Array();
	  iArray[1][0]="客户号码";
	  iArray[1][1]="80px";
	  iArray[1][2]=100;
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array();
	  iArray[2][0]="姓名";
	  iArray[2][1]="80px";
	  iArray[2][2]=100;
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array();
	  iArray[3][0]="性别";
	  iArray[3][1]="50px";
	  iArray[3][2]=100;
	  iArray[3][3]=0;
	  
	  iArray[4]=new Array();
	  iArray[4][0]="出生日期";
	  iArray[4][1]="100px";
	  iArray[4][2]=100;
	  iArray[4][3]=0;        
	  
	  iArray[5]=new Array();
	  iArray[5][0]="证件号码";
	  iArray[5][1]="120px";
	  iArray[5][2]=120; 
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array();
	  iArray[6][0]="保单号码";
	  iArray[6][1]="100px";
	  iArray[6][2]=100;
	  iArray[6][3]=3;
	  
	  iArray[7]=new Array();
	  iArray[7][0]="集体保单号码";
	  iArray[7][1]="100px";
	  iArray[7][2]=100;
	  iArray[7][3]=3;
	  
	  InsuredGrid = new MulLineEnter( "fm" , "InsuredGrid" ); 
	  //这些属性必须在loadMulLine前
	  InsuredGrid.mulLineCount = 0;   
	  InsuredGrid.displayTitle = 1;
    InsuredGrid.canChk=1;
    InsuredGrid.canSel =0;
	  InsuredGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
	  InsuredGrid.hiddenSubtraction=1;
	  InsuredGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}

function initPolGrid()
{                               
	var iArray = new Array();
	try
	{
	  iArray[0]=new Array();
	  iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	  iArray[0][1]="0px";            		//列宽
	  iArray[0][2]=10;            			//列最大值
	  iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	  
	  iArray[1]=new Array();
	  iArray[1][0]="险种序号";
	  iArray[1][1]="50px"; 
	  iArray[1][2]=30;
	  iArray[1][3]=0;
	  
	  iArray[2]=new Array();
	  iArray[2][0]="被保险人号码";
	  iArray[2][1]="80px";
	  iArray[2][2]=100;
	  iArray[2][3]=0;
	  
	  iArray[3]=new Array();
	  iArray[3][0]="被保险人姓名";
	  iArray[3][1]="80px";
	  iArray[3][2]=100;
	  iArray[3][3]=0;
	
	  iArray[4]=new Array();
	  iArray[4][0]="险种代码";
	  iArray[4][1]="50px";
	  iArray[4][2]=100;
	  iArray[4][3]=0; 
	  
	  iArray[5]=new Array();
	  iArray[5][0]="险种名称";
	  iArray[5][1]="180px";
	  iArray[5][2]=100;
	  iArray[5][3]=0;
	  
	  iArray[6]=new Array();
	  iArray[6][0]="保额/份数";
	  iArray[6][1]="80px";
	  iArray[6][2]=100;
	  iArray[6][3]=0;
	  

    iArray[7]=new Array();
	  iArray[7][0]="档次";
	  iArray[7][1]="80px";
	  iArray[7][2]=100;
	  iArray[7][3]=0;
	  
	  iArray[8]=new Array();
	  iArray[8][0]="期交保费";
	  iArray[8][1]="80px";
	  iArray[8][2]=100;
	  iArray[8][3]=0;        
	  
	  iArray[9]=new Array();
	  iArray[9][0]="生效日期"
	  iArray[9][1]="80px";
	  iArray[9][2]=100;
	  iArray[9][3]=0;
	  
	  iArray[10]=new Array();
	  iArray[10][0]="交至日期";
	  iArray[10][1]="80px";
	  iArray[10][2]=100;
	  iArray[10][3]=0;
	  
	  iArray[11]=new Array();
	  iArray[11][0]="保单号码";
	  iArray[11][1]="80px";
	  iArray[11][2]=100;
	  iArray[11][3]=3;
	  
	  iArray[12]=new Array();
	  iArray[12][0]="集体保单号码";
	  iArray[12][1]="100px";
	  iArray[12][2]=100;
	  iArray[12][3]=3;
	  
	  iArray[13]=new Array();
	  iArray[13][0]="险种状态";
	  iArray[13][1]="70px";
	  iArray[13][2]=100;
	  iArray[13][3]=0;
	  
	  iArray[14]=new Array();
	  iArray[14][0]="被保人出生日期";
	  iArray[14][1]="100px";
	  iArray[14][2]=100;
	  iArray[14][3]=3;
	  
		PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
		//这些属性必须在loadMulLine前
		PolGrid.mulLineCount = 0;   
		PolGrid.displayTitle = 1;
		//PolGrid.canChk=1;
		PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
		PolGrid.hiddenSubtraction=1;
		PolGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
	  alert(ex);
	}
}                                

function initForm()
{ 
  initInpBox();
  initLCContGrid();
  initPolGrid();
  initEdorItemGrid();
  initTaskInterface();
  showAllCodeName();
}
</script>