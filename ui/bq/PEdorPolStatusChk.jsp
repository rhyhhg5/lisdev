<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PolStatusChk.jsp
//程序功能：保全核保状态查询
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //输出参数
  CErrors tError = null;
  //CErrors tErrors = new CErrors();
  String FlagStr = "Fail";
  String EdorNo = "";
  String Content = "";
   GlobalInput tG = new GlobalInput();  
	tG=(GlobalInput)session.getValue("GI");  
  	if(tG == null) {
		out.println("session has expired");
		return;
	}
  	// 投保单列表
	LCPolSet tLCPolSet = new LCPolSet();
	String tProposalNo[] = request.getParameterValues("PolGrid1");
	String tEdorNo[] = request.getParameterValues("PolGrid2");
	String tSel[] = request.getParameterValues("InpPolGridSel");
	boolean flag = false;
	int proposalCount = tProposalNo.length;	
	for (int i = 0; i < proposalCount; i++)
	{
		if (!tProposalNo[i].equals("") && tSel[i].equals("1"))
		{
			System.out.println("ProposalNo:"+i+":"+tProposalNo[i]);
	  		LCPolSchema tLCPolSchema = new LCPolSchema();	
		    tLCPolSchema.setPolNo( tProposalNo[i] );	    
		    tLCPolSet.add( tLCPolSchema );
             EdorNo=tEdorNo[i];
              flag = true;
		}
	}

try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add( EdorNo);
		tVData.add( tLCPolSet );
		tVData.add( tG );		
		// 数据传输
		PEdorPolStatusChkUI tPRnewPolStatusChkUI   = new PEdorPolStatusChkUI();
		if (tPRnewPolStatusChkUI.submitData(tVData,"INSERT") == false)
		{
			int n = tPRnewPolStatusChkUI.mErrors.getErrorCount();
			for (int i = 0; i < n; i++)
			Content = " 查询失败，原因是: " + tPRnewPolStatusChkUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPRnewPolStatusChkUI.mErrors;
		    //tErrors = tPRnewPolStatusChkUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	FlagStr = "Succ";
		    	
		    	LMCalModeSet tLMCalModeSet = new LMCalModeSet();
		    	VData tResult = tPRnewPolStatusChkUI.getResult();
		    	if(tResult != null)
		    	{
		    		tLMCalModeSet = (LMCalModeSet)tResult.getObjectByObjectName("LMCalModeSet",0);
		    	}
		    	
		    	if(tLMCalModeSet!=null && tLMCalModeSet.size() > 0)
		    	{
%>
				<script language="javascript">					
					parent.fraInterface.PolStatuGrid.clearData ();				
				</script>         				
<%
		    	
		    		for(int i = 1;i <= tLMCalModeSet.size();i++)
		    		{
		    			LMCalModeSchema tLMCalModeSchema = new LMCalModeSchema();
		    			tLMCalModeSchema = tLMCalModeSet.get(i);
%>
					<script language="javascript">					
						parent.fraInterface.PolStatuGrid.addOne();
						parent.fraInterface.PolStatuGrid.setRowColData(<%=i-1%>,1,"<%=tLMCalModeSchema.getRemark()%>");						
                    			</script>         
<%		    			
		    		}
		    	}
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}
	}
	else
	{
		Content = "请选择保单！";
	}  
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim() +" 提示:异常退出.";
}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
