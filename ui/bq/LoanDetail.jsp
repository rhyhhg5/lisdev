<%
//程序名称：LoanDetaile.jsp
//程序功能：个险续期达成率统计
//创建日期：2012-06-06 
//创建人  ：【OoO?】ytz
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%GlobalInput tGI = (GlobalInput) session.getValue("GI");%> 
<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT type="text/javascript">
  var manageCom = "<%=tGI.ManageCom%>"; //记录管理机构
  function initForm(){fm.all('ManageCom').value = manageCom;}
  function easyPrint(){fm.submit();}
  </SCRIPT>
  
</head>
<body  onload="initForm();" >
	<form method=post name=fm action="LoanDetailSave.jsp" >
		<Table  class= common>
	    	<tr>
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary readonly>
				</td>
			</tr>
		</Table>
		<Table class= common>
		   <tr>
			<td>
				<input value="下载" class = cssButton TYPE=button onclick="easyPrint();">
			</td>
			</tr>
		</Table>
	</form>
	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
