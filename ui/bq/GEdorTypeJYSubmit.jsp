<%
//程序名称：GEdorTypeACSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
 //接收信息，并作校验处理。
  //输入参数
  //个人批改信息
  System.out.println("-----ACsubmit---");
  LPGrpEdorItemSchema tLPGrpEdorItemSchema=new LPGrpEdorItemSchema();

  GrpEdorJYDetailUI tGrpEdorJYDetailUI   = new GrpEdorJYDetailUI();

  CErrors tError = null;
  //后面要执行的动作：添加，修改

  String FlagStr = "";
  String Content = "";
  String fmAction = "";

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  fmAction = request.getParameter("fmAction");
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  tLPGrpEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
  tLPGrpEdorItemSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPGrpEdorItemSchema.setEdorType(request.getParameter("EdorType"));
  tLPGrpEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo"));
  tLPGrpEdorItemSchema.setReasonCode(request.getParameter("reason_tb"));

  
  VData tVData = new VData();
  EdorItemSpecialData tEdorItemSpecialData =new EdorItemSpecialData(request.getParameter("EdorAcceptNo"),request.getParameter("EdorNo"),request.getParameter("EdorType"));
  tEdorItemSpecialData.add("REASON_TB",request.getParameter("reason_tb"));
  tEdorItemSpecialData.add("REASON_TBNAME",request.getParameter("reason_tbName"));
  //我将退保原因存在特殊表里		

  try
  {
    // 准备传输数据 VData

    tVData.addElement(tG);
    tVData.addElement(tLPGrpEdorItemSchema);
    tVData.add(tEdorItemSpecialData);

    tGrpEdorJYDetailUI.submitData(tVData,fmAction);
	}
	catch(Exception ex)
	{
		Content = fmAction+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tGrpEdorJYDetailUI.mErrors;
    if (!tError.needDealError())
    {
        Content = " 保存成功";
      FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript" type="">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

