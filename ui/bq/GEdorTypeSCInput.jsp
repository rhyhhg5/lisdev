<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./GEdorTypeSC.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="GEdorTypeSCInit.jsp"%>
  
  <title>投保人重要资料变更 </title> 
</head>
<body  onload="initForm();" >
  <form action="./GEdorTypeSCSubmit.jsp" method=post name=fm target="fraSubmit">    
  <table class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType type=hidden>
      	<input class = "readonly" readonly name=EdorTypeName>
      </TD>
    </TR>
    <TR  class= common>
      <TD class = title > 集体保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpPolNo>
      </TD>   
      <TD  class= title>单位编码</TD>
      <TD  class= input>
        <Input class= "readonly" readonly name=	GrpNo   >
      </TD>
    </TR>
   <TD class = title > 投保单位名称 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=GrpName>
      </TD> 
  </TABLE> 
  
<Div  id= "divDetail" style= "display: ''">
  <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDetail);">
      </td>
      <td class= titleImg>
        特别约定内容
      </td>
   </tr>
   </table>
  <Div  id= "divLPAppntGrpDetail" style= "display: ''">
      <table>

    <TR  class= common>
      <TD  class= title>
      <textarea name="GrpSpec" cols="120" rows="8" class="common" >
      </textarea></TD>
    </TR>
  </table>


      <table class = common>
			<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeSCSave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="取消" onclick="edorTypeSCReturn()">
     	 </TD>
     	 </TR>
     	</table>
    </Div>
	</Div>
	
	  <Input type=Button value="返回" onclick="returnParent()">
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
