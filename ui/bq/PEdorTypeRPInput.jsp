<html> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2003-11-05 16:49:22
//创建人  ：YangZhao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
  <%     GlobalInput tG = new GlobalInput();
         tG=(GlobalInput)session.getValue("GI");
      %>    

<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  
  <SCRIPT src="./PEdorTypeRP.js"></SCRIPT>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeRPInit.jsp"%>
  
  <!--
  <%@include file="../common/jsp/OperateButton.jsp"%>
  <%@include file="../common/jsp/InputButton.jsp"%>
  -->
 	<%--window.open("./LCPolQueryPrem.jsp?PolNo="+tPolNo);--%>
</head>
<body  onload="initForm();" >
  <form action="./PEdorTypeRPSubmit.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>
     
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=PolNo>
      </TD>   
    </TR>
  </TABLE> 
  
  	<table>
   	<tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol);">
      </td>
      <td class= titleImg>
        被保人详细信息
      </td>
   	</tr>
   </table> 
   
    <Div  id= "divLCPol" style= "display: ''">
   
   
   
   <table  class= common>
     	<TR  class= common>
      	  <TD class = title>
      	     险种编码
      	  </TD>
      	  <TD class= input>
      	    <Input class= "readOnly" readonly  name=RiskCode >
      	  </TD>

      	  <TD class = title>
      	     客户号
      	  </TD>
      	  <TD class= input>
      	    <Input class= "readOnly" readonly  name=InsuredNo >
      	  </TD>
      	</TR> 
      	
      	<TR>
      	 <TD class= title>
      	    被保人姓名
      	 </TD>
		 <TD class= input>
		    <Input class= "readOnly" readonly  name=InsuredName >
		 </TD>      
      	 
		 <TD class= title>
		    生效日期
		 </TD>
		 <TD class= input>
		    <Input class= "readOnly" readonly  name=CValidate >
		 </TD>      
		 
      </TR>  
      
      <TR>
         <TD class= title>
		    交至日期
		 </TD>
		 <TD class= input>
		    <Input class= "readOnly" readonly  name=PayToDate >
		 </TD>
		 <TD class= title>
		     续保方式
		 </TD>    
		 <TD class= input>
		    <Input class= "readOnly" readonly  name=LCRnewFlag >
		 </TD>		       
      </TR>
      
      <TR>
         <TD class= title>
         总保费
         </TD>
         <TD class= input>
             <Input class= "readOnly" readonly  name=Prem >
         </TD>    
         
         <TD class= title>
         总保额
         </TD>
         <TD class= input>
             <Input class= "readOnly" readonly  name=Amnt >
         </TD>    
         
     </TR>
    </Table>
    </Div>
    
    
  	<table>
   	<tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divRnewFlag);">
      </td>
      <td class= titleImg>
        续保方式变更
      </td>
   	</tr>
   </table> 
  
<DIV id= "divRnewFlag" style= "display: ''">
  <table class= common >
     
      <TR>	  
          <TD  class= title>
            续保方式
          </TD>
          <TD  class= input >
             <Input class="code"  name=RnewFlag    ondblClick="showCodeListEx('RnewFlag',[this]);" onkeyup="showCodeListKeyEx('RnewFlag',[this]);">
          </TD>
      </TR>
    
      <TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeRPSave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="返回" onclick="returnParent()">
     	 </TD>
      </TR>
    
    </table>
</div>
	 <Input type=hidden class="readonly" readonly name=Operator1 >
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="validateFlag" name="validateFlag" value="0">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
