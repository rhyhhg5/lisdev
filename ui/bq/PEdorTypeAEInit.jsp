<%
//PEdorTypeAEInit.jsp
//程序功能：
//创建日期：2005-09-22
//创建人  ：QiuYang
//更新记录：  更新人 【OoO？】杨天政    更新日期2011-04-07     更新原因/内容：添加被人于投保人的关系。
%>
<SCRIPT>
function initForm()
{   
    
	initInpBox();
	queryLPAppnt();
	showAllCodeName();
	initRelationToAppntInsuredGrid();
	queryLogRTA(); 
	
}

function initInpBox()
{
  fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
  fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
  fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
  fm.all('ContNo').value = top.opener.fm.all('ContNoBak').value;
  showOneCodeName("EdorCode", "EdorTypeName"); 
}

function initRelationToAppntInsuredGrid()
  {                               
    var iArray = new Array();      
    try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="40px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;  
      iArray[0][21]="orderno";       
                  			              //是否允许输入,1表示允许，0表示不允许
   
      iArray[1]=new Array();
      iArray[1][0]="被保人号码";      	   		//列名
      iArray[1][1]="70px";            			//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[1][21]="NoInsured";
      
      iArray[2]=new Array();
      iArray[2][0]="被保人姓名";      	   		//列名
      iArray[2][1]="60px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[2][21]="NameInsured";
      
      iArray[3]=new Array();
      iArray[3][0]="被保人性别";      	   		//列名
      iArray[3][1]="30px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0;     
      iArray[3][21]="SexInsured";                     //是否允许输入,1表示允许，0表示不允许  
               			               
      iArray[4]=new Array();
      iArray[4][0]="被保人生日";      	   		//列名
      iArray[4][1]="50px";            			//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[4][21]="BirthdayInsured";

      iArray[5]=new Array();
      iArray[5][0]="与投保人的关系";      	   		//列名
      iArray[5][1]="50px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=2;                       //是否允许输入,1表示允许，0表示不允许 ,2表示可以下拉
      iArray[5][4]="Relation"; 
      iArray[5][5]=3;
      iArray[5][9]="与投保人的关系";           // 当为主被保人时这个条件需要添加 |code:Relation&NOTNULL
      iArray[5][18]=250;
      iArray[5][19]= 0 ;
      iArray[5][21]="RelationToAppntInsured";
      
      iArray[6]=new Array();
      iArray[6][0]="是否为主被保险人";      	   		//列名
      iArray[6][1]="40px";            			//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      iArray[6][21]="BirthdayInsured";
        
        
      
      RelationToAppntInsuredGrid = new MulLineEnter( "fm" , "RelationToAppntInsuredGrid" ); 
      RelationToAppntInsuredGrid.mulLineCount = 2;   //初始显示2行
      RelationToAppntInsuredGrid.displayTitle = 1;
      RelationToAppntInsuredGrid.hiddenPlus = 1;
      RelationToAppntInsuredGrid.hiddenSubtraction = 1;
      RelationToAppntInsuredGrid.canSel = 0;
      RelationToAppntInsuredGrid.canChk = 1;
      RelationToAppntInsuredGrid.loadMulLine(iArray);  
      // FeedGrid.selBoxEventFuncName = "selectOne2";
       RelationToAppntInsuredGrid.locked = 1;
      }
    catch(ex)
    {
      
      alert(ex);
      
    }
}

</SCRIPT>