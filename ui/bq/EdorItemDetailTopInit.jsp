<%
    String edorAcceptNo = request.getParameter("edorAcceptNo");
	String itemDetailUrl = request.getParameter("itemDetailUrl");
	
	//查询扫描件相关类型
	String[] BussNoType = null;
	String[] BussType = null;
	String[] SubType = null;
	
	SSRS tSSRS = new SSRS();
	ExeSQL tExeSQL = new ExeSQL();
	String sql = 	"select distinct BussNoType, BussType, SubType "
					+ "from ES_DOC_RELATION "
					+ "where bussNo='" + edorAcceptNo + "' "
					+ "   and (subType = 'BQ01' or subType = 'BQ02') "
					+ "   and bussNoType = '91' ";
	
	tSSRS = tExeSQL.execSQL(sql);
	if(tSSRS != null)
	{
	  BussNoType = new String[tSSRS.getMaxRow()];
	  BussType = new String[tSSRS.getMaxRow()];
	  SubType = new String[tSSRS.getMaxRow()];
	  
	  for(int i = 1; i <= tSSRS.getMaxRow(); i++)
	  {
  		BussNoType[i -1] = tSSRS.GetText(i, 1);
  		BussType[i -1] = tSSRS.GetText(i, 2);
  		SubType[i -1] = tSSRS.GetText(i, 3);
		}
		session.setAttribute("BussNoType", BussNoType);
		session.setAttribute("BussType", BussType);
		session.setAttribute("SubType", SubType);
	}
%>

<script language=javascript>
    /* 查看扫描 */
function viewScan()
{
	if(viewScanFlag == false)
	{
	    alert(parent.fraPic.src);
	    parent.fraPic.src = "../common/EasyScanQuery/EasyScanQuery.jsp?MULTI=MULTI&Order=DESC&prtNo=<%=edorAcceptNo%>&BussNoType=<%=BussNoType%>&BussType=<%=BussType%>&SubType=<%=SubType%>"
		parent.fraSet.rows = "20,100,300,*,0";
		viewScanFlag = true;
	}
	else
	{
		parent.fraSet.rows = "20,0,0,*,0";
		viewScanFlag = false;
	}
}
</script>