var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var inOut = false;		//false代表迁出机构操作
var	inCom ="";
//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if (fm.IDType.value == "0")
  {
    if (fm.IDNo.value == "")
    {
      fm.IDNo.focus();
      return false;
    }
    if (!verifyPage())
    {
      fm.IDNo.focus();
      return false;
    }
  }
  return true;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content,Result )
{
	try{ showInfo.close(); } catch(e) {}
	window.focus();

	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		if(Result=='1')
		{
			//防止转交后继续操作
			try{top.opener.top.opener.top.fraInterface.initForm(); }catch(ex){};
			try{top.opener.top.close();}catch(ex){};
			try{top.close();}catch(ex){}; 
		}
		if(Result=='2')
		{
			//关闭明细窗口
			try{top.close();}catch(ex){};  
		}   
  }
}     


//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent()
{
	try
	{
   // top.opener.updatClick(); 
    top.opener.focus();
	  top.opener.getEdorItem();
	}
	catch (ex) {}
	top.close();
}

//2007-9-28 15:45
function printHospitalListButton()
{

	var sql = "select count(1) from ldcode where codetype='printriskcode' and codealias='1' and code in (select riskcode from LCPol where ContNo='"
         + fm.all('ContNo').value + "')"
         ;
  var arrResult = easyExecSql(sql,1,0);
  if(arrResult[0]=="0")
  {
	  if (!confirm("该保单下没有需要打印清单的险种，是否继续打印"))
	  {
	    return false;
	  }
  }
 
	window.open("../operfee/IndiHospitalQueryMain.jsp");

}
function saveButton()
{
  var sql = "select edorvalue from lpedorespecialdata where edorno = '" + fm.all('EdorAcceptNo').value 
          + "' and edortype = 'PR' and detailtype = 'DEALSTATE'";
  var arrResult = easyExecSql(sql);
  if(arrResult != null && arrResult != "null" && arrResult[0][0]=="1")
  {
    alert("该工单已经进行迁出处理，请直接关闭此页面！");
    return false;
  }
	if(fm.InCom.value=="")
	{
		alert("迁入机构不能为空");
		return false;
	}
	if(fm.OutCom.value == fm.InCom.value)
	{
		alert("迁入机构不能是原保单管理机构");
		return false;
	}
	fm.action = "./PEdorTypePROutSave.jsp";
	fm.submit();
}
function removeOutButton()
{
	//函件下发后才能迁出
	var sql = "select count(1) from lgletter where state != '5' and edoracceptno ='"
         + fm.all('EdorAcceptNo').value + "'"
         ;
  var arrResult = easyExecSql(sql,1,0);
  if(arrResult[0]!="0")
  {
		alert("有函件未处理完毕，不能迁出处理！");
		return false;
	}
	fm.action = "./PEdorTypePROut.jsp";
	fm.submit();
}
function removeInButton()
{
		//函件下发后才能迁入
	var sql = "select count(1) from lgletter where state not in ('3','5','4') and edoracceptno ='"
         + fm.all('EdorAcceptNo').value + "'"
         ;
  var arrResult = easyExecSql(sql,1,0);
  if(arrResult[0]!="0")
  {
		alert("有函件未处理完毕，不能迁入处理！");
		return false;
	}
	if(fm.InCom.value!=inCom)
	{
		alert("迁入机构不能修改");
		return false;
	}
	if(fm.ConfManageCom.value != fm.InCom.value)
	{
		alert("机构确认录入有误");
		return false;
	}
	if(fm.ConfManageCom.value != fm.InCom.value)
	{
		alert("机构确认录入有误");
		return false;
	}
	if(fm.AgentCode.value == "")
	{
		alert("请指定服务业务员");
		return false;
	}
	fm.action = "./PEdorTypePRIn.jsp";
	fm.submit();
}

function displayCtrl()
{
	//如果是迁出机构操作则不显示迁入机构的信息
	if(!inOut)
	{
		DivInInfo.style.display = "none";
		fm.removeIn.style.display = "none";
		fm.hosList.style.display = "none";
	}
	else
	{
		DivInInfo.style.display = "";
		fm.save.style.display = "none";
		fm.removeOut.style.display = "none";
	}
}

function queryAgent()
{
	window.open("./LDAgentQueryMain.jsp");
}

function initQuery()
{
		var dealState = 'A';//代表没有数据
		
		var agentCode ="" ;
		var sql = " select detailtype,EDORVALUE from lpedorespecialdata where edortype ='PR' and edorno ='"
						+	fm.all('EdorAcceptNo').value+ "'"	
						;
		arrResult = easyExecSql(sql, 1, 0);
		if( arrResult != null)
		{				
				for(var i=0;i<arrResult.length;i++)
				{
					if(arrResult[i][0]=='DEALSTATE')
						dealState = arrResult[i][1];
					if(arrResult[i][0]=='INCOM')
						inCom = arrResult[i][1];
					if(arrResult[i][0]=='AGENTCODE')
						agentCode = arrResult[i][1];
				}
				//控制操作按钮显示
				if(dealState!='0' )
				{
					inOut = true;
				}
				sql = " select PostalAddress, ZipCode, Phone " 
								+ " from LPAddress a,lPappnt b " 
								+ " where a.AddressNo = b.AddressNo and contno='" + fm.all('ContNo').value + "'"
								+	" and a.edorno=b.edorno and a.edortype=b.edortype and a.edortype = 'PR' and a.edorno ='"+fm.all('EdorAcceptNo').value+"'";    
				arrResult = easyExecSql(sql, 1, 0);    
				if( arrResult == null)
				{
					try{fm.all('Postaladdress').value= "";}catch(ex){};      
					try{fm.all('ZipCode').value= "";}catch(ex){};            
					try{fm.all('Phone').value= "";}catch(ex){};    
				}
				else
				{
					try{fm.all('Postaladdress').value= arrResult[0][0];}catch(ex){};      
					try{fm.all('ZipCode').value= arrResult[0][1];}catch(ex){};            
					try{fm.all('Phone').value= arrResult[0][2];}catch(ex){};              
				}
				sql = "select name from ldcom where comcode='"+inCom+"'";
				arrResult = easyExecSql(sql, 1, 0);
				
				try{fm.all('AgentCode').value= agentCode;}catch(ex){};
				sql = "select name from laagent where agentcode='"+agentCode+"'";
				var agentName = easyExecSql(sql, 1, 0); 
				try{fm.all('AgentCodeName').value= agentName[0];}catch(ex){};     
				try{fm.all('InCom').value= inCom;}catch(ex){};            
				try{fm.all('ConfManageCom').value= inCom;}catch(ex){};
				if(arrResult)
				{
					try{fm.all('ManageComName').value= arrResult[0];}catch(ex){};            
					try{fm.all('ConfManageComName').value= arrResult[0];}catch(ex){};
				}
				
		}
}
function afterAgentQuery(re)
{
	try{fm.all('AgentCode').value= re[0];}catch(ex){};
	try{fm.all('AgentCodeName').value= re[1];}catch(ex){};
}
function letter()
{
	var sql = 	"select typeNo, statusNo "
				+ "from LGWork "
				+ "where workNo='" + fm.all('EdorAcceptNo').value + "' ";
	var result = easyExecSql(sql);
	if(result)
	{
     window.open("../task/TaskLetterMain.jsp?workNo=" + fm.all('EdorAcceptNo').value , "letter");
	}
}
