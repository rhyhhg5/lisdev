<HTML> 
<% 
//程序名称：
//程序功能：个人保全
//创建日期：2002-07-19 16:49:22
//创建人  ：Tjj
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<HEAD>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="./PEdor.js"></SCRIPT>
  <SCRIPT src="./PEdorTypeAC.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@include file="PEdorTypeACInit.jsp"%>
<TITLE>投保人重要资料变更 </TITLE> 
</HEAD>
<BODY  onload="initForm();" >
 <FORM action="./PEdorTypeACSubmit.jsp" method=post name=fm target="fraSubmit">  
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title >批单号</TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 批改类型 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>
      <TD class = title > 保单号 </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=ContNo>
      </TD>   
    </TR>
  </TABLE>  
  
   <TABLE>
    <TR>
      <TD>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPAppntDetail);">
      </td>
      <TD class= titleImg>
        投保人详细信息
      </TD>
   </TR>
   </TABLE>
  <Div  id= "divLPAppntDetail" style= "display: ''">
    <TABLE  class= common>
     <TR  class= common>
          <TD  class= title>
            客户号
          </TD>
          <TD  class= input>           
           <Input class= common name=AppntNo elementtype=nacessary verify="投保人客户号|notnull&len<=20" >
          </TD>
          <TD  class= title>
            客户姓名
          </TD>
          <TD  class= input>
           <Input class= common name=AppntName elementtype=nacessary verify="投保人姓名|notnull&len<=20" >
          </TD>
	        <TD  class= title>
	          性别
	        </TD>
	        <TD  class= input>
	           <Input class="code" name=AppntSex elementtype=nacessary verify="投保人性别|notnull&code:Sex" ondblclick="return showCodeList('Sex',[this]);" onkeyup="return showCodeListKey('Sex',[this]);">
	        </TD>
      </TR>
	    <TR  class= common>
	        <TD  class= title>
	         出生日期
	        </TD>
	        <TD  class= input>
	          <input class="coolDatePicker" dateFormat="short" elementtype=nacessary name="AppntBirthday" verify="投保人出生日期|notnull&date" >
	        </TD>
          <TD  class= title>
            民族
          </TD>   
          <TD  class= input>         
             <input class="code" name="Nationality" verify="投保人民族|code:Nationality" ondblclick="return showCodeList('Nationality',[this]);" onkeyup="return showCodeListKey('Nationality',[this]);">
          </TD>
          <TD  class= title>
            婚姻状况
          </TD> 
          <TD  class= input>
  	       	<Input class="code" name="Marriage" elementtype=nacessary verify="投保人婚姻状况|notnull&code:Marriage" ondblclick="return showCodeList('Marriage',[this]);" onkeyup="return showCodeListKey('Marriage',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            证件类型
          </TD>       
          <TD  class= input>
            <Input class="code" name="IDType" verify="投保人证件类型|code:IDType" ondblclick="return showCodeList('IDType',[this]);" onkeyup="return showCodeListKey('IDType',[this]);">
          </TD>
          <TD  class= title>
            证件号码
          </TD>
          <TD  class= input>
            <Input class= common name="IDNo" verify="投保人证件号码|len<=20" onblur="checkidtype();getBirthdaySexByIDNo(this.value);">
          </TD>
          <TD CLASS=title>
			       职业代码 
		      </TD>
		      <TD CLASS=input >
		         <Input class="code" name="OccupationCode" elementtype=nacessary verify="投保人职业代码|notnull&code:OccupationCode" ondblclick="return showCodeList('OccupationCode',[this]);" onkeyup="return showCodeListKey('OccupationCode',[this]);"onfocus="getdetailwork();">
		      </TD>
        </TR>    
        <TR  class= common>
		      <TD CLASS=title>
		        职业类别 
		      </TD>
		      <TD CLASS=input >
		         <Input class="code" name="OccupationType" verify="投保人职业类别|code:OccupationType" ondblclick="return showCodeList('OccupationType',[this]);" onkeyup="return showCodeListKey('OccupationType',[this]);">
		      </TD>
		     </TR>
		      <TR class= common>
          <TD  class= title>
            地址代码
          </TD>
          <TD  class= input>
            <Input class="code" name="AddressNo"  ondblclick="getaddresscodedata();return showCodeListEx('GetAddressNo',[this],[0],'', '', '', true);" onkeyup="getaddresscodedata();return showCodeListKeyEx('GetAddressNo',[this],[0],'', '', '', true);">
          </TD>                  
        	</TR>  
		      <TR  class= common>    
          <TD  class= title>
            家庭地址
          </TD>
          <TD  class= input colspan=3 >
             <Input class= common3 name="HomeAddress" verify="投保人家庭地址|len<=80" >
          </TD>
          <TD  class= title>
            家庭邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name="HomeZipCode" verify="投保人家庭邮政编码|zipcode" >
          </TD>  
         </TR>   
          <TD  class= title>
            家庭电话
          </TD>
          <TD  class= input>
            <input class= common name="HomePhone" verify="投保人家庭电话|NUM&len<=18" >
          </TD>
           <TD  class= title>
            家庭传真
          </TD>
          <TD  class= input>
            <Input class= common name="HomeFax" verify="投保人家庭传真|len<=15" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            单位地址
          </TD>
          <TD  class= input colspan=3>
            <Input class= common3 name="CompanyAddress" verify="投保人单位地址|len<=80" >
          </TD>
          <TD  class= title>
            单位邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name="CompanyZipCode" verify="投保人单位邮政编码|CompanyZipCode" >
          </TD>    
         </TR>    
         <TR  class= common>       
          <TD  class= title>
            单位电话
          </TD>
          <TD  class= input>
            <Input class= common name="CompanyPhone" verify="投保人单位电话|NUM&len<=18" >
          </TD>      
           <TD  class= title>
            单位传真
          </TD>
          <TD  class= input>
             <Input class= common name="CompanyFax" verify="单位传真|len<=18" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
           联系地址选择
          </TD>          
          <TD CLASS=input >
		        <Input class="code" name="CheckPostalAddress"  value='3' CodeData="0|^1|单位地址^2|家庭地址^3|其它"  ondblclick="return showCodeListEx('CheckPostalAddress',[this]);" onkeyup="return showCodeListKeyEx('CheckPostalAddress',[this]);FillPostalAddress()" onfocus="FillPostalAddress();">          
		     </TD>    
        </TR>
       <TR  class= common>
          <TD  class= title>
            联系地址
          </TD>
          <TD  class= input colspan=3 >
            <Input class= common3 name="PostalAddress" verify="投保人联系地址|len<=80" >
          </TD>
           <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name="ZipCode" verify="投保人邮政编码|zipcode" >
          </TD> 
       </TR>
       <TR  class= common>                
          <TD  class= title>
            联系电话
          </TD>
          <TD  class= input>
           <input class= common name="Phone" verify="投保人联系电话|len<=18" >
          </TD>      
          <TD  class= title>
            通讯传真
          </TD>
          <TD  class= input>
            <Input class= common name="Fax" verify="传真|len<=15" >
          </TD>
           <TD  class= title>
            移动电话
          </TD>
          <TD  class= input>
            <Input class= common name="Mobile" verify="投保人移动电话|len<=15" >
          </TD>  
        </TR>        
        <TR CLASS=common>
          <TD CLASS=title width="109" >
	           账号
	        </TD>
	        <TD CLASS=input COLSPAN=1  >
	            <Input NAME=BankAccNo class="code" VALUE="" CLASS=common ondblclick="return showCodeList('accnum',[this],null,null,fm.AppntNo.value,'CustomerNo');" onkeyup="return showCodeListKey('accnum',[this],null,null,fm.AppntNo.value,'CustomerNo');" verify="银行帐号|len<=40" onfocus="getdetail();">
	        </TD>
	        <TD CLASS=title  >
	          授权转帐开户行 
	        </TD>
	        <TD CLASS=input COLSPAN=1  >
	           <Input NAME=BankCode VALUE="" CLASS="code" MAXLENGTH=20 verify="开户行|code:bank" ondblclick="return showCodeList('bank',[this]);" onkeyup="return showCodeListKey('bank',[this]);" >
	        </TD>
	        <TD CLASS=title >
	          户名 
	        </TD>
	        <TD CLASS=input COLSPAN=1  >
	           <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 verify="户名|len<=20" >
	        </TD>
	       </TR>
      </TABLE>
     
      <TABLE class = common>
			  <TR class= common>
          <TD  class= input width="26%"> 
       		 <Input type =button class=cssButton value="保存申请" onclick="edorTypeACSave()">
          </TD>
       	  <TD  class= input width="26%"> 
       		 <Input type =button class=cssButton value="取  消" onclick="edorTypeACReturn()">
     	    </TD>     
     	  <TD  class= input width="26%"> 	    
     	  	<Input type=Button class=cssButton value="返  回" onclick="returnParent()">	 
     	  </TD>
     	  </TR>
     	</TABLE>
    </Div>    
	 	 
	 <input type=hidden id="ContType" name="ContType">	 
	 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
	 <input type=hidden id="addrFlag" name="addrFlag">	 	 	
	 <input type=hidden id="fmtransact" name="fmtransact">		
	 </FORM>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</BODY>
</HTML>
