<%
//程序名称：PEdorAppConfirmSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<%
    LPGrpEdorMainSchema tLPGrpEdorMainSchema   = new LPGrpEdorMainSchema();
    PGrpEdorAppConfirmUI tPGrpEdorAppConfirmUI   = new PGrpEdorAppConfirmUI();
    
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
     VData tVData = new VData();   
    CErrors tError = null;   
    String FlagStr = "";
    String Content = "";
    String fmtransact = "INSERT||FINALGEDORAPPCONFIRM";
    String prtParams = ""; 
    String tOperate = ""; 
    
    //集体批改信息
    fmtransact=request.getParameter("fmtransact");
    //System.out.println(fmtransact);
    tLPGrpEdorMainSchema.setGrpContNo(request.getParameter("GrpContNo"));
    tLPGrpEdorMainSchema.setEdorNo(request.getParameter("EdorNo"));
    tLPGrpEdorMainSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
    tLPGrpEdorMainSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
    tLPGrpEdorMainSchema.setEdorAppDate(request.getParameter("EdorAppDate"));
    //System.out.println("request.getParameter(EdorNo):"+request.getParameter("EdorNo"));
    try
    {              
            
      tVData.addElement(tLPGrpEdorMainSchema);
      tVData.addElement(tG);
      
      tPGrpEdorAppConfirmUI.submitData(tVData,fmtransact);
      
      //prtParams = tPGrpEdorAppConfirmUI.getPrtParams();
      //if(prtParams==null) prtParams="";

    }
		catch(Exception ex)
		{
	    ex.printStackTrace();
	    Content = "保全申请失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
		}			
	
    //如果在Catch中发现异常，则不从错误类中提取错误信息
   
    if (FlagStr.equals(""))
    {
        tError = tPGrpEdorAppConfirmUI.mErrors;
        //如果成功调用自动核保功能
        if (tError.getErrType().equals(CError.TYPE_NONEERR))
        {   	  
            Content = " 保存成功";
            FlagStr = "Success";            
        }
        else 
        {
            String ErrorContent = tError.getErrContent();  
                
            if (tError.getErrType().equals(CError.TYPE_ALLOW)) 
            {
              Content = " 保存成功，但是：" + ErrorContent;
              FlagStr = "Success";
            }
            else 
            {
            	Content = "保存失败，原因是：" + ErrorContent;
            	FlagStr = "Fail";
            }
            
            Content = PubFun.changForHTML(Content);
        }
    }
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

