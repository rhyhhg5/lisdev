<html> 
<% 
//程序名称：PEdorTypeCBInput.jsp
//程序功能：个人保全常无忧B给付补费
//创建日期：2010-07-19 16:49:22
//创建人  ：About Zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head >
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT> 
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
	<SCRIPT src="./PEdor.js"></SCRIPT>
	<SCRIPT src="./PEdorTypeCB.js"></SCRIPT>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%@include file="PEdorTypeCBInit.jsp"%>
</head>
<body  onload="initForm();" >
	<form action="./PEdorTypeCBSubmit.jsp" method=post name=fm target="fraSubmit">    
    <table class=common>
    	<tr class= common> 
        <td class="title"> 受理号 </td>
        <td class=input>
          <input class="readonly" type="text" readonly name="EdorNo" >
        </td>
        <td class="title"> 批改类型 </td>
        <td class="input">
        	<input class="readonly" type="hidden" readonly name="EdorType">
        	<input class="readonly" readonly name="EdorTypeName">
        </td>
        <td class="title"> 保单号 </td>
        <td class="input">
        	<input class = "readonly" readonly name="ContNo">
        </td>   
    	</tr>
    </table> 
		<%@include file="ULICommon.jsp"%> 
    <Div id= "divPolInfo" style= "display: ''">
			<table>
			  <tr>
		      <td>
		      	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPolGrid);">
		      </td>
		      <td class= titleImg>
		         保单险种信息
		      </td>
			  </tr>
			</table>
	    <Div  id= "divPolGrid" style= "display: ''">
				<table  class= common>
					<tr  class= common>
			  		<td text-align: left colSpan=1>
						<span id="spanPolGrid" >
						</span> 
				  	</td>
					</tr>
				</table>					
	    </div>
    </DIV>
    <br>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divJisPay);">
    		</td>
    		<td class= titleImg>
    			 给付任务清单：
    		</td>
    	</tr>
    </table>
  <div  id= "divJisPay" style= "display: ''">
   	<table  class= common>
      <tr  class= common>
        <td text-align: left colSpan=1><span id="spanLjsGetGrid"></span></td>
  	  </tr>
    </table>
  </div> 
  
  <br>
  <div align=left id="divLjsGetDraw" style="display: '' ">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this, divLjsGetDrawGrid);">
    		</td>
    		<td class= titleImg>
    			 给付明细：
    		</td>
    	</tr>
    </table>
    <div align=left id="divLjsGetDrawGrid" style="display: '' ">
      <table>
        <tr  class= common>
          <td text-align: left colSpan=1>
  		    <span id="spanLjsGetDrawGrid" ></span> 
  	      </td>
  	    </tr>
      </table>
    </div>
  </div>
  
  <table>
    <tr>
    	<td class= titleImg>
    		忠诚奖合计：<input class = "common" readonly name="Bonus">
    	</td>
    </tr>
  </table>
  <br>
  <table>
    <tr>
    	<td class= titleImg>
    		重算忠诚奖：
    	</td>
    </tr>
  	<tr >
	  <td class= title>选择公式
	    <input class="codeNo" name="Formula" value='3' CodeData="0|^2|公式二^3|标准公式^4|公式四^5|公式五" 
	           ondblclick="return showCodeListEx('Formula',[this,FormulaName],[0,1],null,null,null,1);" 
	           onkeyup="return showCodeListKeyEx('Formula',[this,FormulaName],[0,1],null,null,null,1);"><Input class="codeName" name="FormulaName" readonly>
	    <input type="checkbox" name="RateFlag" > 息降我不降 &nbsp&nbsp&nbsp&nbsp
	    <input class = cssbutton value="重  算" type=button onclick="calData();">
	  </td>
	</tr>
	<tr>
      <td class= titleImg>
         重算后忠诚奖合计：<input class = "common" readonly name="NewBonus">&nbsp&nbsp&nbsp&nbsp
      </td>
    </tr>
  </table>  
  <br>
  <table>
    <tr>
      <td class= titleImg>
    	补充退费金额：<input class = "common" name="GetMoney"><font color='red'>&nbsp&nbsp支持直接录入</font>
      </td>
    </tr>
  </table>
  <hr>
  
    <br>
    <div>
      <Input type=button name="save" class = cssButton value="保  存" onclick="saveData()">
	  <Input  type=Button name="goBack" class = cssButton value="返  回" onclick="edorReturn()">
	</div>

		 <input type=hidden id="fmtransact" name="fmtransact">
		 <input type=hidden id="ContType" name="ContType">
		 <input type=hidden id="EdorAcceptNo" name="EdorAcceptNo">
		 <input type=hidden id="PolNo" name="PolNo">
		 <input type=hidden id="RateFlagValue" name="RateFlagValue">
		 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
