var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//查询被保人信息
function queryInsured()
{
  var sql = "select a.ContNo, a.InsuredNo, a.Name, a.Sex, a.Birthday, " +
            "       a.IDType, a.IDNo, '', c.PrtSeq, " +
            "       case when c.PrtSeq is null then '体检通知书未录入' " +
            "       else '体检通知书已录入' end " +
            "from LCInsured a left join LPRReport c " +
            "     on a.InsuredNo = c.CustomerNo " +
            "     and c.EdorNo = '" + fm.EdorNo.value + "', " + 
            "     LCCont b " +
            "where a.ContNo = b.ContNo " +
            "and a.AppntNo = '" + fm.AppntNo.value + "' " +
            "and b.AppFlag = '1'";
  turnPage.pageLineNum = 100;
  turnPage.queryModal(sql, LCInsuredGrid);
}

//查询体检项目
function queryItem()
{
  var sql = "select RReportItemCode, RReportItemName, RRItemContent from LPRReportItem " +
            "where EdorNo = '" + fm.EdorNo.value + "' " +
            "and PrtSeq = '" + fm.PrtSeq.value + "' ";
  turnPage2.pageDivName = "divPage2";
  turnPage2.pageLineNum = 100 ; 
  turnPage2.queryModal(sql, InvestigateGrid);
  
  sql = "select Contente from LPRReport " +
        "where EdorNo = '" + fm.EdorNo.value + "' " +
        "and PrtSeq = '" + fm.PrtSeq.value + "' ";
  var result = easyExecSql(sql);
  if (result != null)
  {
    fm.Contente.value = result[0][0];
  }
}

function onClickedInsured()
{
  var selNo = LCInsuredGrid.getSelNo() - 1;
  fm.ContNo.value = LCInsuredGrid.getRowColData(selNo, 1);
  fm.InsuredNo.value = LCInsuredGrid.getRowColData(selNo, 2);
  fm.InsuredName.value = LCInsuredGrid.getRowColData(selNo, 3);
  fm.PrtSeq.value = LCInsuredGrid.getRowColData(selNo, 9);
  queryItem();
}

//提交数据后执行的操作
function afterSubmit(flag, content)
{
	showInfo.close();
	window.focus();
	if (flag == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

function submitForm()
{
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action= "./PEdorUWManuRReportChk.jsp";
  fm.submit();
}