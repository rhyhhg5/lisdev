<%
//程序名称：GEdorTypeGASubmit.jsp
//程序功能：
//创建日期：2005-12-26
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String flag;
	String content;

	GlobalInput gi = (GlobalInput)session.getValue("GI");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String grpContNo = request.getParameter("GrpContNo");
	String[] grpPolNo = request.getParameterValues("PolGrid1");
	String[] groupMoney = request.getParameterValues("PolGrid5");
	
	EdorItemSpecialData tSpecialData = new EdorItemSpecialData(edorNo, edorType);
	for (int i = 0; i < grpPolNo.length; i++)
	{
		tSpecialData.setGrpPolNo(grpPolNo[i]);
  		tSpecialData.add("GroupMoney", groupMoney[i]);
  	}
	
  GEdorZADetailUI tGEdorZADetailUI = 
          new GEdorZADetailUI(gi, edorNo, grpContNo, tSpecialData);
	if (!tGEdorZADetailUI.submitData())
	{
		flag = "Fail";
		content = "数据保存失败！原因是:" + tGEdorZADetailUI.getError();
	}
	else 
	{
		flag = "Succ";
		content = "数据保存成功。";
		String message = tGEdorZADetailUI.getMessage();
		if (message != null)
		{
		  flag = "Fail";
		  content = message;
		}
	}
	content = PubFun.changForHTML(content);
%>   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</script>
</html>