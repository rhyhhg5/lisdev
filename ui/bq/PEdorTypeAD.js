//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var arrResult
var oldSex = "";
var oldBirthday = "";
var oldOccupationType = "";

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeADReturn() {
  initForm();
}

function edorTypeADSave() 
{
  if (!beforeSubmit())
  {
    return false;
  }
  var checked = false;
  for (i = 0; i < LCContGrid.mulLineCount; i++)
  {
  	if (LCContGrid.getChkNo(i)) 
		{
			checked = true;
			break;
		}
	}
	if (checked == false)
	{
		alert("请选择相关合同信息！");
		return false;
	}
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}

function customerQuery()
{	
	window.open("./LCAppntIndQuery.html");
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content, Result)
{
	try
	{
	  showInfo.close();
	  window.focus();
	}
	catch (ex) {}
	
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
  		    //使用模拟数据源，必须写在拆分之前
  		    turnPage.useSimulation   = 1;  
    
  		    //查询成功则拆分字符串，返回二维数组
  		    var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[1,2,8,5]);
			//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		 	turnPage.pageDisplayGrid = LCAppntIndGrid;    
		  
		  //设置查询起始位置
		 	turnPage.pageIndex       = 0;
		 	//在查询结果数组中取出符合页面显示大小设置的数组
	  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
			//调用MULTILINE对象显示查询结果
	   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		}
		else if (tTransact=="QUERY||DETAIL")
		{
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  		//保存查询结果字符串

 		 	turnPage.strQueryResult  = Result;
  		//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,tTransact);
  		turnPage.arrDataCacheSet =chooseArray(tArr,[2,8,9,10,22,24,15,51,12,13,27,28,29,30,31,32,34,35,55,6,56,39]);

  		fm.all('CustomerNo').value = turnPage.arrDataCacheSet[0][0];
  		fm.all('Name').value = turnPage.arrDataCacheSet[0][1];
  		fm.all('Sex').value = turnPage.arrDataCacheSet[0][2];
  		fm.all('Birthday').value = turnPage.arrDataCacheSet[0][3];
  		fm.all('IDType').value = turnPage.arrDataCacheSet[0][4];
  		fm.all('IDNo').value = turnPage.arrDataCacheSet[0][5];
  		fm.all('OccupationType').value = turnPage.arrDataCacheSet[0][6];
  		fm.all('OccupationCode').value = turnPage.arrDataCacheSet[0][7];
  		fm.all('Nationality').value = turnPage.arrDataCacheSet[0][8];
  		fm.all('Marriage').value = turnPage.arrDataCacheSet[0][9];
  		fm.all('PostalAddress').value = turnPage.arrDataCacheSet[0][13];
  		fm.all('ZipCode').value = turnPage.arrDataCacheSet[0][14];
  		fm.all('Phone').value = turnPage.arrDataCacheSet[0][15];
  		fm.all('Mobile').value = turnPage.arrDataCacheSet[0][16];
  		fm.all('E-Mail').value = turnPage.arrDataCacheSet[0][17];
  		fm.all('HomeZipCode').value = turnPage.arrDataCacheSet[0][18];
  		divLPAppntIndDetail.style.display ='';
  		divDetail.style.display='';

  		fm.all('Name2').value = turnPage.arrDataCacheSet[0][1];
  		oldSex = fm.all('Sex').value;
  		oldBirthday = fm.all('Birthday').value;
  		oldOccupationType = fm.all('OccupationType').value;
  		
  		//制定汉化
    	showOneCodeName("Sex", "Sex");  
    	showOneCodeName("IDType", "IDType"); 
		}
		else
		{    
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
	   	returnParent();   
		}
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  //校验联系电话和移动电话
  if(!verifyPhone(fm.all("PPhone").value,fm.all("PMobile").value)){
	  return false;
  }
  
  if(fm.PPostalAddress.value.trim().length<=6){
	  alert("地址信息低于字符限制要求，须提详细地址信息!");
	  fm.PPostalAddress.focus();
	  return false;
  }
  //校验家庭电话
  if(!verifyPhone(fm.all("PHomePhone").value,"")){
	  return false;
  }
  //校验单位电话
  if(!verifyPhone(fm.all("PCompanyPhone").value,"")){
	  return false;
  }
  //校验联系电话和移动电话
  if(!verifyPhone(fm.all("PPhone").value,fm.all("PMobile").value)){
	  return false;
  }
  
  //校验其它联系地址级联关系
  if(fm.PCheckPostalAddress.value=="3"){
	  var checkCityResult = easyExecSql("select 1 from ldcode1 where codetype='province1' and code='"+fm.Province3.value+"' "
			  +" and code in (select code1 from ldcode1 where codetype='city1' and code='"+fm.City3.value+"'  and code in (select code1 from ldcode1 where codetype='county1' and code='"+fm.County3.value+"'))");
	  if(checkCityResult == null || checkCityResult == ""){
		  alert("投保人联系地址级联关系不正确，请检查！");
		  return false;	  
	  }
  }
  return true;
}           
// 20140801 对移动电话和联系电话的校验
function verifyPhone(phone,mobile)
{
	var isInsuFlag=false;
	var isInsuSql = "select 1 from LCCont where ContNo='"+fm.all('ContNo').value+"' and appntno='"+fm.all('CustomerNo').value + "' ";
	var isAppnt = easyExecSql(isInsuSql, 1, 0);
	if(!isAppnt){//非投保人即为被保人
		if("000000"!=fm.all('CustomerNo').value){//没有接收到客户号时默认为投保人
			isInsuFlag=true;
		}
		var tInsuSql = "select InsuredIDNo from lccont where contno = '" + fm.all('ContNo').value + "' and insuredno = '"+fm.all('CustomerNo').value+"'";
		var insuResult = easyExecSql(tInsuSql)
		if(insuResult){
			if(insuResult[0][0] != null && insuResult[0][0] != "" && insuResult[0][0] != "null"){
				var insuIdno = insuResult[0][0];
			}
		}
	}
	
	var tAppSql = "select idno from lcappnt where contno = '" + fm.all('ContNo').value + "' ";
	var appResult = easyExecSql(tAppSql);
	if(appResult){
		if(appResult[0][0] != null && appResult[0][0] != "" && appResult[0][0] != "null" ){
			var appIdno = appResult[0][0];
		}
	}
	if(mobile!=null&&mobile!="")
	{	
		var regMo = /(^13\d{9}$)|(^14\d{9}$)|(^15\d{9}$)|(^17\d{9}$)|(^18\d{9}$)/;
		if(!regMo.test(mobile))
		{
			alert("移动电话需为11位数字且前两位\n必须是13、14、15、17、18，请核查！");
			return  false;
		}

		//当移动电话变更时才校验
		if(fm.all("Mobile").value!=mobile)
		{
			var mobileSql = "select count(distinct customerno) from lcaddress where mobile='" + mobile + "' and Customerno<>'" + fm.all("CustomerNo").value + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno) ";
			var mobileRes = easyExecSql(mobileSql);
			if(mobileRes){
				var mobileCount = mobileRes[0][0];
				if(mobileCount >= 2){
					alert("该投保人移动电话已在三个以上不同投保人的保单中出现！");
					return false;
				}
			}
		}
		if(isInsuFlag){
			var tInsuSql1 = " select agentcode from laagent where idno != '" + insuIdno + "' and (mobile = '" + mobile + "' or phone = '"+ mobile +"') ";
			var insuResult1 = easyExecSql(tInsuSql1);
			if(insuResult1 != null && insuResult1 != "" && insuResult1 != "null" ){
				alert("被保人移动电话不能与业务人员一致！");
				return false;
			}
		}
		var tAppSql1 = " select agentcode from laagent where idno != '" + appIdno + "' and (mobile = '" + mobile + "' or phone = '"+ mobile +"') ";
		var appResult1 = easyExecSql(tAppSql1);
		if(appResult1 != null && appResult1 != "" && appResult1 != "null" ){
			alert("投保人移动电话不能与业务人员一致！");
			return false;
		}
	}
	
	if(null!=phone&&""!=phone){
		if(phone.length < 7)
		{
			alert("投保人联系电话不能少于7位，请核查！");
	   		return false;
		}
//		var regPh = /^(\d+-?\d+)$|^(\(\d+\)-?\d+)$/;
		if(!isInsuFlag){
			var regPh1 = /^[\d-\(\)]+$/;
			if(!regPh1.test(phone)){
				alert("固定电话仅允许包含数字、小括号和“-”！");
				return  false;
			}						
		}

		//当联系电话变更时才校验
		if(fm.all("Phone").value!=phone)
		{
			var phoneSql = "select count(distinct customerno) from lcaddress where phone='" + phone + "' and Customerno<>'" + fm.all("CustomerNo").value + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno) ";
			var phoneRes = easyExecSql(phoneSql);
			if(phoneRes){
				var phoneCount = phoneRes[0][0];
				if(phoneCount >= 2){
					alert("该投保人联系电话已在三个以上不同投保人的保单中出现！");
					return false;
				}
			}
		}
		if(isInsuFlag){
			var tInsuSql2 = " select agentcode from laagent where idno != '" + insuIdno + "' and (phone = '" + phone + "' or mobile = '" + phone + "') ";
			var insuResult2 = easyExecSql(tInsuSql2);
			if(insuResult2 != null && insuResult2 != "" && insuResult2 != "null" ){
				alert("被保人联系电话不能与业务人员一致！");
				return false;
			}
		}
		var tAppSql2 = " select agentcode from laagent where idno != '" + appIdno + "' and (phone = '" + phone + "' or mobile = '" + phone + "') ";
		var appResult2 = easyExecSql(tAppSql2);
		if(appResult2 != null && appResult2 != "" && appResult2 != "null" ){
			alert("投保人联系电话不能与业务人员一致！");
			return false;
		}
	}
	
	return true;
}
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		// 查询保单明细
		queryAppntIndDetail();
	}
}
function queryAppntIndDetail()
{
	var tEdorNO;
	var tEdorType;
	var tContNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tContNo=fm.all('ContNo').value;
	//alert(tContNo);
	tCustomerNo = fm.all('CustomerNo').value;
	//alert(tCustomerNo);
	//top.location.href = "./AppntIndQueryDetail.jsp?EdorNo=" + tEdorNo+"&EdorType="+tEdorType+"&ContNo="+tContNo+"&CustomerNo="+tCustomerNo;
	parent.fraInterface.fm.action = "./AppntIndQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PedorTypeADSubmit.jsp";
}

function returnParent()
{
	try
	{
		top.opener.getEdorItem();
		top.opener.focus();
		top.close();
	}
	catch (ex) {}
}


function initQuery()
{
	var isInsuFlag=false;
	var isInsuSql = "select 1 from LCCont where ContNo='"+fm.all('ContNo').value+"' " +
	"and appntno='"+fm.all('CustomerNo').value + "' ";
	var isAppnt = easyExecSql(isInsuSql, 1, 0);
	if(!isAppnt){//非投保人即为被保人
		if("000000"!=fm.all('CustomerNo').value){//没有接收到客户号时默认为投保人
			isInsuFlag=true;
		}
	}
	var tStrold = "select * from LCAppnt where ContNo='"+fm.all('ContNo').value+"'"; 
	arrResult = easyExecSql(tStrold,1,0);
	if(arrResult == null)
	{
		alert("没有投保人");
		return false;
	}
	else
	{
		if(!isInsuFlag){//兼容简易保全
			try{fm.all('CustomerNo').value= arrResult[0][3]; }catch(ex){};
		}
		try{fm.all('Name').value= arrResult[0][5]; }catch(ex){};            
		try{fm.all('Sex').value= arrResult[0][6]; }catch(ex){};             
		try{fm.all('Birthday').value= arrResult[0][7]; }catch(ex){};        
		try{fm.all('IDType').value= arrResult[0][10]; }catch(ex){};          
		try{fm.all('IDNo').value= arrResult[0][11]; }catch(ex){}; 
		
		//制定汉化
        showOneCodeName("Sex", "Sex");  
		showOneCodeName("IDType", "IDType"); 
		
		var sql = "select AddressNo, PostalAddress, ZipCode, Phone, Fax, HomeAddress, " +
		          "       HomeZipCode, HomePhone, HomeFax, CompanyAddress, CompanyZipCode, " + 
		          "       CompanyPhone,CompanyFax,Mobile,Email " +
		          "from LCAddress " +
		          "where CustomerNo = '"+fm.all('CustomerNo').value + "' " +
		          "and AddressNo = (select AddressNo from lcappnt where contno='" + fm.all('ContNo').value + "')";
		if(isInsuFlag){
			sql = "select AddressNo, PostalAddress, ZipCode, Phone, Fax, HomeAddress, " +
	          "       HomeZipCode, HomePhone, HomeFax, CompanyAddress, CompanyZipCode, " + 
	          "       CompanyPhone,CompanyFax,Mobile,Email " +
	          "from LCAddress " +
	          "where CustomerNo = '"+fm.all('CustomerNo').value + "' " +
	          "and AddressNo = (select AddressNo from lcinsured where contno='" + fm.all('ContNo').value + "' and insuredno='"+fm.all('CustomerNo').value + "')";
		}
		arrResult = easyExecSql(sql, 1, 0);    
		if( arrResult == null)
		{
			alert("原地址错误");
			return false;
		}
		else
		{
			try{fm.all('AddressNo').value= arrResult[0][0];}catch(ex){};     	
			try{fm.all('PostalAddress').value= arrResult[0][1];}catch(ex){};      
			try{fm.all('ZipCode').value= arrResult[0][2];}catch(ex){};            
			try{fm.all('Phone').value= arrResult[0][3];}catch(ex){};              
			try{fm.all('Fax').value= arrResult[0][4];}catch(ex){};                
			try{fm.all('HomeAddress').value= arrResult[0][5];}catch(ex){};        
			try{fm.all('HomeZipCode').value= arrResult[0][6];}catch(ex){};        
			try{fm.all('HomePhone').value= arrResult[0][7];}catch(ex){};          
			try{fm.all('HomeFax').value= arrResult[0][8];}catch(ex){};           
			try{fm.all('CompanyAddress').value= arrResult[0][9];}catch(ex){};     
			try{fm.all('CompanyZipCode').value= arrResult[0][10];}catch(ex){};     
			try{fm.all('CompanyPhone').value= arrResult[0][11];}catch(ex){};       
			try{fm.all('CompanyFax').value= arrResult[0][12];}catch(ex){};
			try{fm.all('Mobile').value= arrResult[0][13];}catch(ex){};
			try{fm.all('E-MAIL').value= arrResult[0][14];}catch(ex){};
		}     	
	} 	
	
	var tStrnew = "select * from LPAppnt where EdorNo='"+fm.all('EdorNO').value+"' and EdorType='"+fm.all('EdorType').value+"' and ContNo='"+fm.all('ContNo').value+"'";
	if(isInsuFlag){
		tStrnew="select * from LPInsured where EdorNo='"+fm.all('EdorNO').value+"' and EdorType='"+fm.all('EdorType').value+"' " +
				"and ContNo='"+fm.all('ContNo').value+"' and insuredno='"+fm.all('CustomerNo').value + "'";
	}
	arrResult = easyExecSql(tStrnew,1,0);
	if (arrResult == null)
	{
    try{fm.all('PAddressNo').value= "";}catch(ex){}; 	
    try{fm.all('PPostalAddress').value= fm.all('PostalAddress').value;}catch(ex){};      
    try{fm.all('PZipCode').value= fm.all('ZipCode').value;}catch(ex){};            
    try{fm.all('PPhone').value= fm.all('Phone').value;}catch(ex){};              
    try{fm.all('PFax').value= fm.all('Fax').value;}catch(ex){};                
    try{fm.all('PHomeAddress').value=fm.all('HomeAddress').value;}catch(ex){};        
    try{fm.all('PHomeZipCode').value= fm.all('HomeZipCode').value;}catch(ex){};        
    try{fm.all('PHomePhone').value= fm.all('HomePhone').value;}catch(ex){};          
    try{fm.all('PHomeFax').value= fm.all('HomeFax').value;}catch(ex){};           
    try{fm.all('PCompanyAddress').value= fm.all('CompanyAddress').value;}catch(ex){};     
    try{fm.all('PCompanyZipCode').value= fm.all('CompanyZipCode').value;}catch(ex){};     
    try{fm.all('PCompanyPhone').value= fm.all('CompanyPhone').value;}catch(ex){};       
    try{fm.all('PCompanyFax').value= fm.all('CompanyFax').value;}catch(ex){};  
    
    try{fm.all('PMobile').value= fm.all('Mobile').value;}catch(ex){}; 
    try{fm.all('PE-MAIL').value= fm.all('E-MAIL').value;}catch(ex){};
	}
	else
	{ 
		var tStrnewaddress= "select AddressNo,PostalAddress,ZipCode,Phone,Fax,HomeAddress,HomeZipCode,HomePhone,HomeFax,CompanyAddress,CompanyZipCode,CompanyPhone,CompanyFax,Mobile,Email from LPAddress where EdorNo='"+fm.all('EdorNO').value+"' and CustomerNo='"+fm.all('CustomerNo').value+"'";  
		arrResult = easyExecSql(tStrnewaddress,1,0);
		if(arrResult==null)
		{
		}
		else
		{
		    displayAddress(arrResult[0]); 	
		}
	}

	var strSQL= "  select a.contno, b.PostalAddress, b.ZipCode from "
	            + "lccont a, LcAddress b, lcappnt c "
	            + "where a.appntno = '" + fm.all('CustomerNo').value + "' "
	            + "   and b.CustomerNo = a.appntno "
	            + "   and c.appntno = a.appntno "
	            + "   and c.contno = a.contno "
	            + "   and b.AddressNo = c.AddressNo "
	            + "   and a.appflag = '1' "
	            + "order by contNo" ;
	if(isInsuFlag){
		strSQL= "  select a.contno, b.PostalAddress, b.ZipCode from "
            + "lccont a, LcAddress b, lcinsured c "
            + "where c.insuredno = '" + fm.all('CustomerNo').value + "' "
            + "   and b.CustomerNo = a.insuredno "
            + "   and c.contno = a.contno "
            + "   and b.AddressNo = c.AddressNo "
            + "   and a.appflag = '1' "
            + "order by contNo" ;
	}
	turnPage.queryModal(strSQL, LCContGrid);
	chkLCCont();  
	
	//默认选中添加项目页面选择的合同
	selectCheckBox();
}

function chkLCCont()
{
    var strSQL = "  select contno "
                 + "from lpappnt "
                 + "where appntno = '" + fm.all('CustomerNo').value + "' "
                 + "    and edortype = '" + fm.all('EdorType').value + "' "
                 + "    and edorno = '" + fm.all('EdorNo').value + "' ";        
    var arrResult2=easyExecSql(strSQL);
    var m=0;
  	var n=0;
  	
  	if(arrResult2!=null)
  	{
  		var q=arrResult2.length;
  		
  		for(m=0;m<LCContGrid.mulLineCount;m++)
	  	{
	  		for(n=0;n<q;n++)
	  	   {
	  			if(LCContGrid.getRowColData(m,1) == arrResult2[n][0])
	  			{
	  				LCContGrid.checkBoxSel(m + 1);
	  			}
	  		}
	  	}
  	}
}

//默认选中添加项目页面选择的合同
function selectCheckBox()
{
	var contNo = top.opener.fm.all('ContNo').value;
	LCContGrid.checkBoxAllNot(this, LCContGrid.colCount);
	for(var i = 0; i < LCContGrid.mulLineCount; i++)
	{
		if(LCContGrid.getRowColData(i, 1) == contNo)
		{
			LCContGrid.checkBoxSel(i + 1);
		}
	}
}

function displayAddress()
{
    try{fm.all('PAddressNo').value= arrResult[0][0];}catch(ex){};	
    try{fm.all('PPostalAddress').value= arrResult[0][1];}catch(ex){};      
    try{fm.all('PZipCode').value= arrResult[0][2];}catch(ex){};            
    try{fm.all('PPhone').value= arrResult[0][3];}catch(ex){};              
    try{fm.all('PFax').value= arrResult[0][4];}catch(ex){};                
    try{fm.all('PHomeAddress').value= arrResult[0][5];}catch(ex){};        
    try{fm.all('PHomeZipCode').value= arrResult[0][6];}catch(ex){};        
    try{fm.all('PHomePhone').value= arrResult[0][7];}catch(ex){};          
    try{fm.all('PHomeFax').value= arrResult[0][8];}catch(ex){};           
    try{fm.all('PCompanyAddress').value= arrResult[0][9];}catch(ex){};     
    try{fm.all('PCompanyZipCode').value= arrResult[0][10];}catch(ex){};     
    try{fm.all('PCompanyPhone').value= arrResult[0][11];}catch(ex){};       
    try{fm.all('PCompanyFax').value= arrResult[0][12];}catch(ex){}; 	
    try{fm.all('PMobile').value= arrResult[0][13];}catch(ex){};                                        
    try{fm.all('PE-MAIL').value= arrResult[0][14];}catch(ex){};                                     
}

function FillPostalAddress()
{
	if(fm.PCheckPostalAddress.value=="1") 
	 {
			fm.all('PPostalAddress').value=fm.all('PCompanyAddress').value;
			fm.all('PZipCode').value=fm.all('PCompanyZipCode').value;
			fm.all('PPhone').value= fm.all('PCompanyPhone').value;
			fm.all('PPhone').value= fm.all('PCompanyPhone').value;   
			fm.all('PFax').value= fm.all('PCompanyFax').value; 
			
	 }      
	 else if(fm.PCheckPostalAddress.value=="2") 
	 {      
			fm.all('PPostalAddress').value=fm.all('PHomeAddress').value;
			fm.all('PZipCode').value=fm.all('PHomeZipCode').value;
			fm.all('PPhone').value= fm.all('PHomePhone').value;
			fm.all('PFax').value= fm.all('PHomeFax').value;    
			
	 }
	 else if(fm.PCheckPostalAddress.value=="3") 
	 {
			fm.all('PPostalAddress').value="";
			fm.all('PZipCode').value="";
			fm.all('PPhone').value= "";
			fm.all('PFax').value=="";
			
	 }
	if(fm.PCheckPostalAddress.value=="1"||fm.PCheckPostalAddress.value=="2"){
		document.getElementById("postal_Address").style.display="block";
		document.getElementById("P_OtherAddress").style.display="none";
		document.getElementById("otherAddress").style.display="none";
		document.getElementById("Province3").verify="";
		document.getElementById("City3").verify="";
		document.getElementById("County3").verify="";
		document.getElementById("appnt_PostalStreet3").verify="";
		document.getElementById("appnt_PostalCommunity3").verify="";
		document.getElementById("POtherAddress").verify="";
	}else if(fm.PCheckPostalAddress.value=="3"){
		initPostalAddress();
		document.getElementById("postal_Address").style.display="none";
		document.getElementById("otherAddress").style.display="block";
		document.getElementById("Province3").verify="联系地址(省（自治区直辖市）)|notnull";
		document.getElementById("City3").verify="联系地址(市)|notnull";
		document.getElementById("County3").verify="联系地址(县（区）)|notnull";
		document.getElementById("appnt_PostalStreet3").verify="联系地址(乡镇（街道）)|notnull";
		document.getElementById("appnt_PostalCommunity3").verify="联系地址(村（社区）)|notnull";
		document.getElementById("POtherAddress").verify="投保人其它地址|notnull&len<=100";
		document.getElementById("P_OtherAddress").style.display="block";
		if(fm.appnt_PostalCity3.value == '空' && fm.appnt_PostalCounty3.value == '空'){
			fm.PPostalAddress.value=fm.appnt_PostalProvince3.value+fm.appnt_PostalStreet3.value+fm.appnt_PostalCommunity3.value;
		}else if(fm.appnt_PostalCity3.value != '空' && fm.appnt_PostalCounty3.value == '空') {
			fm.PPostalAddress.value=fm.appnt_PostalProvince3.value+fm.appnt_PostalCity3.value+fm.appnt_PostalStreet3.value+fm.appnt_PostalCommunity3.value;
		}else{
			fm.PPostalAddress.value=fm.appnt_PostalProvince3.value+fm.appnt_PostalCity3.value+fm.appnt_PostalCounty3.value+fm.appnt_PostalStreet3.value+fm.appnt_PostalCommunity3.value;
		}
	}
}
function chagePostalAddress(){
	//当省级单位下没有市县级单位或市级单位下没有县级单位的时候改变拼接完整地址的方式

	if(fm.appnt_PostalCity3.value == '空' && fm.appnt_PostalCounty3.value == '空'){
		fm.POtherAddress.value=fm.appnt_PostalProvince3.value+fm.appnt_PostalStreet3.value+fm.appnt_PostalCommunity3.value;
	}else if(fm.appnt_PostalCity3.value != '空' && fm.appnt_PostalCounty3.value == '空') {
		fm.POtherAddress.value=fm.appnt_PostalProvince3.value+fm.appnt_PostalCity3.value+fm.appnt_PostalStreet3.value+fm.appnt_PostalCommunity3.value;
	}else if(fm.appnt_PostalCity3.value == '空'){
		fm.POtherAddress.value=fm.appnt_PostalProvince3.value+fm.appnt_PostalStreet3.value+fm.appnt_PostalCommunity3.value;
	}else{
		fm.POtherAddress.value=fm.appnt_PostalProvince3.value+fm.appnt_PostalCity3.value+fm.appnt_PostalCounty3.value+fm.appnt_PostalStreet3.value+fm.appnt_PostalCommunity3.value;
	}
	if(fm.PCheckPostalAddress.value=="3"){
		fm.PPostalAddress.value=fm.POtherAddress.value;
	}
}
function changeAddress(num){
	if(num==1){
		fm.City3.value = '';
		fm.appnt_PostalCity3.value = '';
		fm.County3.value = '';
		fm.appnt_PostalCounty3.value = '';
		fm.appnt_PostalStreet3.value = '';
		fm.appnt_PostalCommunity3.value = '';				
	}
	if(num==2){
		fm.County3.value = '';
		fm.appnt_PostalCounty3.value = '';
		fm.appnt_PostalStreet3.value = '';
		fm.appnt_PostalCommunity3.value = '';		
	}
	if(num==3){
		fm.appnt_PostalStreet3.value = '';
		fm.appnt_PostalCommunity3.value = '';
	}
	if(num==4){
		fm.appnt_PostalCommunity3.value = '';
	}
}
