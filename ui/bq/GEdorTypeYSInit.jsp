<script language="JavaScript">  

function initForm()
{
  try
  {
    initContGrid();
    initInpBox();
  }
  catch(e)
  {
    alert("初始化界面错误，请重新登录!");
  }
}

function initInpBox()
{   
  fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
  fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
  fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
  //fm.all('AppntNo').value = top.opener.fm.all('AppntNo').value;
  fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
  fm.all("CustomerNo").value =  top.opener.fm.all('AppntNo').value;
  //制定汉化
  showOneCodeName("EdorCode", "EdorTypeName");  
  queryClick();
}


function initContGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="80px";         		//列宽
    iArray[1][2]=100;          				//列最大值
    iArray[1][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="生效日期";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="80px";         		//列宽
    iArray[2][2]=100;          				//列最大值
    iArray[2][3]=0;            				//是否允许输入,1表示允许，0表示不允许  
    
    iArray[3]=new Array();
    iArray[3][0]="缴费间隔";    			//列名
    iArray[3][1]="80px";            	//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="期交保费";    			//列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="业务员代码";    	//列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="业务员姓名";    			//列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    ContGrid = new MulLineEnter("fm", "ContGrid"); 
    ContGrid.mulLineCount = 0;
    ContGrid.displayTitle = 1;
    ContGrid.canSel = 0;
    ContGrid.canChk = 0;
    ContGrid.hiddenSubtraction = 1;
    ContGrid.hiddenPlus = 1;
    ContGrid.selBoxEventFuncName ="selCont" ; 
    ContGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}
</script>