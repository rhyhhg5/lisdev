<%
//程序名称：PEdorInputInit.jsp
//程序功能：
//创建日期：2008-2-25 9:51
//创建人  ：fuxin
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">  
function initInpBox()
{ 
	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
 
  	if (flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  	}
      
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
  /*  
    var sql = "  select ReasonCode "
              + "from LPEdorItem "
              + "where edorAcceptNo = '" + fm.EdorNo.value + "' "
              + "   and edorType = '" + fm.EdorType.value + "' ";
    var result = easyExecSql(sql);
    if(result)
    {
        fm.reason_tb.value = result[0][0];
        
        sql = "  select codeName "
              + "from LDCode "
              + "where codeType = 'reason_tb' "
              + "    and code = '" + result[0][0] + "' ";
        result = easyExecSql(sql);
        if(result)
        {
            fm.reason_tbName.value = result[0][0];
        }
    }
    */
   // showOneCodeName("EdorCode", "EdorTypeName"); 
}
                                       

function initForm()
{
  try
  {
    initInpBox();
    //initInusredGrid(); 
    initPolGrid();
    getPolInfo(fm.all('GrpContNo').value);  
    //chkPol();
    initElementtype();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}



function initPolGrid()
{                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
        iArray[0][1]="30px";            		//列宽
        iArray[0][2]=10;            			//列最大值
        iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
        
       
        iArray[1]=new Array();
        iArray[1][0]="保单险种号码";
        iArray[1][1]="100px";
        iArray[1][2]=100;
        iArray[1][3]=0;
        
        iArray[2]=new Array();
        iArray[2][0]="险种代码";
        iArray[2][1]="70px";
        iArray[2][2]=100;
        iArray[2][3]=0;
        
        iArray[3]=new Array();
        iArray[3][0]="险种名称";
        iArray[3][1]="200px";
        iArray[3][2]=100;
        iArray[3][3]=0;

        
        iArray[4]=new Array();
        iArray[4][0]="保费";
        iArray[4][1]="100px";
        iArray[4][2]=100;
        iArray[4][3]=0;        
        
        iArray[5]=new Array();
        iArray[5][0]="保额";
        iArray[5][1]="100px";
        iArray[5][2]=100;
        iArray[5][3]=0;
        
        iArray[6]=new Array();
        iArray[6][0]="生效日期";
        iArray[6][1]="100px";
        iArray[6][2]=100;
        iArray[6][3]=0;
        
        iArray[7]=new Array();
        iArray[7][0]="终止日期";
        iArray[7][1]="100px";
        iArray[7][2]=100;
        iArray[7][3]=0;
        
        iArray[8]=new Array();
        iArray[8][0]="缴至日";
        iArray[8][1]="100px";
        iArray[8][2]=100;
        iArray[8][3]=0;
        
        iArray[9]=new Array();
        iArray[9][0]="集体保单号码";
        iArray[9][1]="100px";
        iArray[9][2]=100;
        iArray[9][3]=3;
        
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 0;   
      PolGrid.displayTitle = 1;
      PolGrid.canChk=0;
      PolGrid.hiddenPlus=1;   //是否隐藏"+"号标志：1为隐藏；0为不隐藏(缺省值)
      PolGrid.hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面

      }
      catch(ex)
      {
        alert(ex);
      }
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;
	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			//alert(m);
			for( j = 0; j < m; j++ )
			{
				 fm.all('GetMoney').value = arrResult[i][j];
				 //alert("---"+fm.all('GetMoney').value);
			} // end of for
		} // end of for
		
	} // end of if
}
</script>