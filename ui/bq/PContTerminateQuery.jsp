
<%
  GlobalInput GI = new GlobalInput();
	GI = (GlobalInput)session.getValue("GI");
%>
<script>
	var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
	var ComCode = "<%=GI.ComCode%>";//记录登陆机构
</script>

<html>
<%
//程序名称：PContPauseInput.jsp
//程序功能：保单失效清单
//创建日期：2010-3-31
//创建人  ：WangHongLiang
//更新记录：  更新人    更新日期     更新原因/内容
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.bank.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	
	<%@include file="../common/jsp/UsrCheck.jsp"%>

	<head >
  	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  	
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="PContTerminateQuery.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<%@include file="PContTerminateQueryInit.jsp"%>
	</head>

	<body  onload="initForm();" >
  	<form action="./PContPausePrint.jsp" method=post name=fm target="fraSubmit">
		<%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LLReport1的信息 -->
    		
    	<Div  id= "divLLReport1" style= "display: ''">    	
      	<Table  class= common>
	    	<tr>
	    		<td class= title>管理机构</td>
	    		<td class=input>
					<Input class= "codeno"  name=ManageCom  verify="管理机构|NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);" ><Input class=codename  name=ManageComName elementtype=nacessary>
				</td>
				<td class= title>保单类型</td>
				<td class=input>
					<Input class= "codeno" name=SaleChnl CodeData="0|^0|全部^1|个险^2|银保" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);" onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1]);" ><Input class=codename name=SaleChnlName readonly  >
				</td>
				<td class= title>保单号</td>
				<td class=input>
					<input class=common name=ContNo>
				</td>
				
			</tr>
	    	<tr>
	    	    <td class= title>业务员代码</td>
				<td class=input>
					<input class=common name=AgentCode>
					</td>
				<td class= title>投保人客户号</td>
				<td class=input>
					<input class=common name=AppntNo>
				</td class=input>
				<td class= title>打印标志</td>
				<td class=input>				
					<Input class= "codeno" name=PrintCount CodeData="0|^0|未打印^1|已打印^2|全部" ondblclick="return showCodeListEx('PrintCount',[this,PrintCountName],[0,1]);" onkeyup="return showCodeListKeyEx('PrintCount',[this,PrintCountName],[0,1]);" ><Input class=codename name=PrintCountName readonly  >
				</td>
	    	</tr>
	    	<tr>
				<td class= title>永久失效起期</td>
				<td class=input> 
					<Input class="coolDatePicker" dateFormat="short" name=StartDate elementtype=nacessary verify="永久失效起期|NOTNULL">
				</td>
				<td class= title>永久失效止期</td>
				<td class=input>
					<Input class="coolDatePicker" dateFormat="short" name=EndDate elementtype=nacessary verify="永久失效起期|NOTNULL">
				</td>
			</tr>
		</Table>
      <table  class= common>
	  <input class=cssButton type=button value="查  询" onclick="easyQuery();">
	  <input class=cssButton type=button value="下载清单" onclick="easyPrint();">
	  <input class=cssButton type=button value="打印PDF通知书" onclick="newprintInsManage();">
	  <input class=cssButton type=button value="批量打印PDF通知书" onclick="printAllNotice();">     

    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 清单列表
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanPContTerminateGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		  <Div  id= "divPage2" align=center style= "display: '' ">     
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">
		 </Div>
  	</Div>
		
		<!-- 
		<input class=cssButton type=button value="全部打印通知书" onclick="printNoticeAll();">
		 -->
		<input type=hidden id="sql" name="sql" value="">

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </form>
</body>
</html>
