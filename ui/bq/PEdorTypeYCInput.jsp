<% 
//程序名称：PEdorTypeYCInput.jsp
//程序功能：个人保全
//创建日期：2004-05-09
//创建人  ：Fanym
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <SCRIPT src="./PEdorTypeYC.js"></SCRIPT>
  <%@include file="PEdorTypeYCInit.jsp"%> 
</head>

<body  onload="initForm();" >
  <form action="./PEdorTypeYCSubmit.jsp" method=post name=fm target="fraSubmit">    
  <TABLE class=common>
    <TR  class= common> 
      <TD  class= title > 
        批单号
      </TD>
      <TD  class= input > 
        <input class="readonly" readonly name=EdorNo >
      </TD>
      <TD class = title > 
        批改类型 
      </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=EdorType>
      </TD>    
      <TD class = title >  
        保单号 
      </TD>
      <TD class = input >
      	<input class = "readonly" readonly name=PolNo>
      </TD>   
    </TR>
  </TABLE> 
  
   <table>
   <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPGet);">
      </td>
      <td class= titleImg>
        责任领取信息
      </td>
   </tr>
   </table>
	 
    <Div  id= "divLPGet" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanLCDutyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
  	
  	<br>
  
     <Div  id= "divSubmit" style= "display:'none'">
      <table class = common>
	<TR class= common>
	 <TD class=title>
	    领取年龄 
	 </TD>
	 <TD class= input>
	    <Input class= common name=GetYear>
	 </TD>
	 <TD class=title>
	    领取年龄标志
	 </TD>
	 <TD class= input>
	    <Input class="code" name=GetYearFlag CodeData="0|^Y|年|^A|岁|^M|月|" ondblClick="showCodeListEx('GetYearFlag',[this]);" onkeyup="showCodeListKeyEx('GetYearFlag',[this]);">
	 </TD>	                           
	</TR>
	<TR class= common>
         <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="保存申请" onclick="edorTypeYCSave()">
     	 </TD>
     	 <TD  class= input width="26%"> 
       		 <Input class= common type=Button value="返回" onclick="returnParent()">
     	 </TD>
     	</TR>
     	</table> 
     </Div>
	
	 <input type=hidden id="fmtransact" name="fmtransact">
	 <input type=hidden id="ContType" name="ContType">
	 <input type=hidden id="DutyCode" name="DutyCode">
	 <input type=hidden id="GetDutyCode" name="GetDutyCode">
         <input type=hidden id="RiskCode" name="RiskCode">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
