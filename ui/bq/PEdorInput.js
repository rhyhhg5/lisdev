//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var aEdorFlag='0';
var mEdorType;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var queryFlag = "N";
function initEdorType(cObj)
{
	//var tRiskCode = fm.all('RiskCode').value;
	var tContNo = fm.all('ContNo').value;
	mEdorType = " 1 and AppObj=#I#" ;
	mEdorType=mEdorType+" and riskcode in (select riskcode from lcpol where ContNo=#"+tContNo+"# ";
	mEdorType=mEdorType+" and SaleChnl in(select #01# from dual where #@#>=#D# union select #02# from dual union select #03# from dual)) "; 
        mEdorType=mEdorType+" and (EdorPopedom is not null and  EdorPopedom<=#@#)";
	showCodeList('EdorCode',[cObj], null, null, mEdorType, "1");
	
}

function actionKeyUp(cObj)
{
	//var tRiskCode = fm.all('RiskCode').value;
	var tContNo = fm.all('ContNo').value;
	mEdorType = " 1 and AppObj=#I#" ;
	mEdorType=mEdorType+" and riskcode in(select riskcode from lcpol where ContNo=#"+tContNo+"# ";
	mEdorType=mEdorType+" and SaleChnl in(select #01# from dual where #@#>=#D# union select #02# from dual union select #03# from dual)) ";
    mEdorType=mEdorType+" and (EdorPopedom is not null and  EdorPopedom<=#@#)";
	showCodeListKey('EdorCode',[cObj], null, null, mEdorType, "1");
}
/*********************************************************************
 *  校验批改类型输入
 *  参数  ：  无
 *  返回值：  boolean
 *********************************************************************/
 //add at 2004-6-24 
 function checkEdorType()
 {
 	var tRiskCode = fm.all('RiskCode').value;
	var tContNo = fm.all('ContNo').value;
	var tEdorType = fm.all('EdorType').value;
 	var arrCode;
 	var arrCount;
 	var bflag=false;
 	
 	mEdorType = " 1 and RiskCode=#"+tRiskCode+"# and AppObj=#I#" ;
	mEdorType=mEdorType+" and riskcode in(select riskcode from lcpol where ContNo=#"+tContNo+"# ";
	mEdorType=mEdorType+" and SaleChnl in(select #01# from dual where #@#>=#D# union select #02# from dual union select #03# from dual)) ";
        mEdorType=mEdorType+" and (EdorPopedom is not null and  EdorPopedom<=#@#)";
        
 	arrCode = searchCode('EdorCode',mEdorType,"1");
 	arrCount=arrCode.length;
 	for(i=0;i<arrCount;i++)
  	{  
           if(tEdorType==arrCode[i][0]){
           	//alert("today_EdorType:" + tEdorType); 
           	bflag=true;
           	break;
           	}
      	}
      	return bflag;
 }
/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
//	alert(cCodeName);
	try	{
		if( cCodeName == "EdorCode" )	
		{
			//CtrlEdorType( Field.value );//loadFlag在页面出始化的时候声明
			var strsql="select DisplayFlag from LMEdorItem where EdorCode='"+Field.value+"' and AppObj='I'";
			
			var tDisplayType =easyExecSql(strsql);
			if (tDisplayType==null||tDisplayType=='')
			{
			    alert("未查到该保全项目显示级别！");    
			    return;
			}
			
			fm.all("DisplayType").value=tDisplayType;
			if (tDisplayType=='1')//只显示保单
			{
			    divInsuredInfo.style.display='none';
			    divPolInfo.style.display='none';  
			}
			else if (tDisplayType=='2')//需要显示被保人
			{
			//    alert("initInusredGrid");
			    initInusredGrid();
			    divInsuredInfo.style.display='';
			    getInsuredInfo();
			}
			else if (tDisplayType=='3')//需要显示险种
			{
			    initInusredGrid();
			    initPolGrid();
			    divInsuredInfo.style.display='';
			    divPolInfo.style.display='';  
			}
		}
		
	    if (cCodeName == "EdorCode") {
	    	var edorType = Field.value;
	    	if (edorType == "AC" || edorType == "BB" || edorType == "BC" || edorType == "IA" || edorType == "CC") {
	    	    fm.all('EdorValiDate').value = fm.all('dayAfterCurrent').value;
	    	} else if (edorType == "WT" || edorType == "ZT" || edorType == "GT") {
	    	    fm.all('EdorValiDate').value = fm.all('currentDay').value;
	    	} else {
	    		fm.all('EdorValiDate').value = fm.all('currentDay').value;
	    	}

	    }			
	}
	catch( ex ) {
	}
}
function CtrlEdorType(tEdorType)
{
//				alert(tEdorType);
				tEdorNo=fm.all('EdorNo').value;
				//alert(tEdorNo);
				if (tEdorNo!=null&&tEdorNo!='')
					divedortype.style.display = '';
				else
					divedortype.style.display = 'none';
						
				//alert('aaa');
				easyQueryClick(tEdorType);
}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  fm.fmtransact.value = "INSERT||EDOR";
//                 alert("11111");
    if (fm.all('ContNo').value==null||fm.all('ContNo').value=='')
    {
        alert('请先查询保单，再进行申请！');
	    return;    
    }                                                         
    if (fm.all('EdorNo').value!='')
    {
        alert('该保全已经申请，不能重复申请！');
	    return;    
    }
    
   if (fm.all('EdorValiDate').value==null||fm.all('EdorValiDate').value=='') 
   {
        alert('请录入保全生效日期');
        return;
    }

   if (fm.all('EdorAppDate').value==null||fm.all('EdorAppDate').value=='') 
   {
	alert('请录入保全申请日期');
	return;
   }
//   alert("12312312");
    fm.all('OtherNo').value = fm.all('ContNo').value;
    fm.all('OtherNoType').value = "3"; //保单号
//    if(!checkEdorType()){	
//        alert("不能保存此项目：项目不存在或你没有权限！");		
//	return;
//    }

    // remark by Minim
    //if (!verifyInput()) 
    //{
    //	return false;	
    //}

    
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}

function submitForm1()
{
  var i = 0;
  var tContNo1 = fm1.all('ContNo').value;
  
  var showStr;
  var urlStr;
  
  //if (aEdorFlag=='0')
  //	{
  //		alert("请单击新增产生新的申请!");
  //		return;
  //	}
  
  if (tContNo1 ==null||tContNo1 =='') 
  {
  	//alert('null');
  	resetForm(); 	
		queryFlag="0";
		var newWindow = window.open("../sys/AllProposalQueryMain.jsp","AllProposalQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
                 //window.open("./LCPolQuery.html");
  	newWindow.focus();
	}
  else
  {
  	//alert('no null');
 	 //showSubmitFrame(mDebug);
 	 var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
 	 fm.all('EdorNo').value='';
	 fm1.submit();
	}
}

function afterSubmit1( FlagStr, content )
{
  //alert(FlagStr);
  try{ showInfo.close(); } catch (e) {};
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
		emptyUndefined();
  	var tTransact = fm.all('fmtransact').value;
  	
		divappconfirm.style.display ='';
		if (tTransact=="QUERY||EDOR")
		{
			 var tEdorType = fm.all('EdorType').value;
			 CtrlEdorType(tEdorType);
		}
		else
		{
			//var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
	    //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  	//parent.fraInterface.initForm();
		}
    //showInfo.close();
    //alert(FlagStr);
    //执行下一步操作
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //alert(content);
  showInfo.close();
    
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
		emptyUndefined();
  	var tTransact = fm.all('fmtransact').value;
  	
		divappconfirm.style.display ='';
		if (tTransact=="QUERY||EDOR")
		{
			 var tEdorType = fm.all('EdorType').value;
			 CtrlEdorType(tEdorType);
		}
		else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  	//parent.fraInterface.initForm();
		}
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //showInfo.close();
    //alert(FlagStr);
    //执行下一步操作
  }
}
//在原来申请的基础上增加保全项目
function insertEdorType()
{
	var tEdorNo ;
	tEdorNo = fm.all('EdorNo').value
	if (tEdorNo=='')
			alert("请重新申请!");
	else
	{
		if (window.confirm("是否添加该保全项目?"))
		{
			fm.fmtransact.value="INSERT||EDORTYPE";
			if (fm.all('EdorValiDate').value==null||fm.all('EdorValiDate').value=='')
			{
				alert('请录入保全生效日期');
				return;
			}
			if (fm.all('EdorAppDate').value==null||fm.all('EdorAppDate').value=='')
			{
				alert('请录入保全申请日期');
				return;
			}
			if(!checkEdorType()){  
			     alert("不能添加此项目：项目不存在或你没有权限！");
			     return ;
			}
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	 		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			verifyInput();
			
	  	//showSubmitFrame(mDebug);
	  	fm.submit(); //提交
		}
	}
	
}

//确认整次申请
function edorAppConfirm()
{
	var tEdorNo ;
	tEdorNo = fm.all('EdorNo').value;
	fm.all('EdorState').value ='1';
	if (tEdorNo=='')
			alert("请重新申请!");
	else
	{
		//window.open("./PEdorAppConfirm.html");
		var newWindow = window.open("./PEdorAppConfirm.html","PEdorAppConfirm",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
 	 newWindow.focus();
	}
}

//Click事件，当点击“查询”图片时触发该函数
function edorQuery()
{
  //下面增加相应的代码
  fm.all('EdorState').value ='2';
  fm.all('fmtransact').value = "QUERY||EDOR";
	queryFlag='2';
  var newWindow = window.open("./PEdorQuery.html","PEdorQuery",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  newWindow.focus();
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的

}           
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在PEdorInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
		aEdorFlag='0';
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
 // alert("add click");
  //下面增加相应的代码
  var currentdate = new Date();
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||EDOR";
  initForm();
  aEdorFlag ='1';
  
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  //alert("update click");
  var i = 0;
  if (fm.all('EdorValiDate').value==null||fm.all('EdorValiDate').value=='')
	{
			alert('请录入保全生效日期');
			return;
	}
	if (fm.all('EdorAppDate').value==null||fm.all('EdorAppDate').value=='')
	{
			alert('请录入保全申请日期');
			return;
	}
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||EDOR"
  verifyInput();
	
  fm.submit(); //提交
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModelessDialog("./ReportQuery.html",window,"dialogWidth=15cm;dialogHeight=12cm");
  //var newWindow = window.open("./ReportQuery.html","ReportQuery",'width=700,height=450,top=150,left=190,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  //newWindow.focus();
  fm.all('EdorState').value ='1';
  fm.all('fmtransact').value = "QUERY||EDOR";
 	queryFlag="1";

//  alert(fm.all('EdorState').value);
  //window.open("./PEdorQuery.html");
  var newWindow = window.open("./PEdorQuery.html","PEdorQuery",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  newWindow.focus();
  //alert("query click");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的

}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
 // alert("delete");
  //下面增加相应的代码
 // alert("delete click");
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.fmtransact.value="DELETE||EDOR"
  fm.submit();
  resetForm();
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{       
	var arrResult = new Array();
	//alert(queryFlag);
	if (queryFlag=="1")
	{        
		if( arrQueryResult != null )
		{
			arrResult = arrQueryResult;
	
			fm.all( 'EdorNo' ).value = arrResult[0][0];
			fm.all( 'ContNo' ).value = arrResult[0][1];
			fm1.all( 'ContNo' ).value = arrResult[0][1];
			fm.all( 'EdorAcceptNo').value = arrResult[0][2];
			
			//alert("queryEdorAppInfo");
			queryEdorAppInfo();
	        divappconfirm.style.display='';
			
			// 查询保单明细
			//queryEdorDetail();
		}
        }
	else if (queryFlag=='0')           //当在保全申请时，没有直接输入保单号的查询走这里
	{
		arrResult = arrQueryResult;
	    fm1.all( 'ContNo' ).value = arrResult[0][0];
	    //var tGrpContNo1=fm1.all( 'GrpContNo1' ).value; 
	    //alert(fm1.all( 'GrpContNo1' ).value);
	    // 查询保单明细
	    queryCont();
	    queryFlag="N";
	    //submitForm1();             
    }
        

}

/*********************************************************************
 *  根据查询返回的信息查询投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryEdorDetail()
{
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	parent.fraInterface.fm.action = "./PEdorQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorSave.jsp";
}

function allConfirm()
{
	//window.open("./PEdorConfirmQuery.html");
	var newWindow = window.open("./PEdorConfirmQuery.html","PEdorConfirmQuery",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
  newWindow.focus();
}

// 查询按钮
function easyQueryClick(tEdorType)
{
	tRiskCode = fm.all('RiskCode').value;
	//tEdorType = fm.all('EdorType').value;
	
	//verifyInput();
	if (tEdorType==null||tEdorType=='')
	{
				alert("请选择批改项目!");
				return;
	}
	if (tRiskCode==null||tRiskCode=='')
	{
				alert("险种为空,请重新查询!");
				return;
	}
	else
	{
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select count(*) from lmriskedoritem where riskcode ='"+tRiskCode+"' and edorcode='"+tEdorType+"' and AppObj='I' and needdetail='1'";
	//alert(strSQL);			 
	//查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  
	  //判断是否查询成功
	  if (!turnPage.strQueryResult) 
	  {
	    alert("查询失败！");
	  }
	  else
	  {
		  //查询成功则拆分字符串，返回二维数组
//		  alert(turnPage.strQueryResult);
		  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  	  
			if (turnPage.arrDataCacheSet[0][0]>0)
			{
				//alert(turnPage.arrDataCacheSet[0][0]);
				divdetail.style.display = '';
			}
			else
			{
				//alert(turnPage.arrDataCacheSet[0][0]);
				divdetail.style.display = 'none';
			}
		}
	}
}

/*********************************************************************
 *  查询保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryCont()
 {
    var i = 0;
    var tContNo1 = fm1.all('ContNo').value;
    var showStr;
    var urlStr;
//    alert(tContNo1)	;
    if (tContNo1==null||tContNo1 =='') 
    {
    	resetForm();
        queryFlag="0";
        var newWindow = window.open("../sys/AllProposalQueryMain.jsp","AllProposalQueryMain",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
        newWindow.focus(); 
    }
    else
    {
        var strSQL= "select a.contno,a.InsuredName,a.AppntName,a.CValiDate,a.PayTodate,a.prem,a.amnt,a.GetPolDate,a.agentcode,a.grpcontno"
                    + " from lccont a where a.contno='"+ tContNo1 + "' and a.appflag='1' ";		
//        alert("strSQL"+strSQL);        
       
 	    arrResult = easyExecSql(strSQL);    
 	    //alert("arrResult"+arrResult);
 	    if (arrResult == null) 
 	    {
                    alert("未查到保单号为"+tContNo1+"的个人保单！");
	    } 
	    else
	    {
 	        try {fm.all('contno').value= arrResult[0][0]; } catch(ex) { };
 	        try {fm.all('InsuredName').value= arrResult[0][1]; } catch(ex) { };
 	        try {fm.all('AppntName').value= arrResult[0][2]; } catch(ex) { };
 	        try {fm.all('ValiDate').value= arrResult[0][3]; } catch(ex) { };
 	        try {fm.all('PayTodate').value= arrResult[0][4]; } catch(ex) { };
 	        try {fm.all('prem').value= arrResult[0][5]; } catch(ex) { };
 	        try {fm.all('amnt').value= arrResult[0][6]; } catch(ex) { };
 	        try {fm.all('GetPolDate').value= arrResult[0][7]; } catch(ex) { };
 	        try {fm.all('agentcode').value= arrResult[0][8]; } catch(ex) { };   
 	        try {fm.all('GrpContNo').value= arrResult[0][9]; } catch(ex) { };    
 	    }
    }
}

/*********************************************************************
 *  查询团体保全申请信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryEdorAppInfo()
 {
    //alert("queryCont");
    //查询险种信息
    queryCont();
    //alert("queryEdorApp");
    //查询保全申请信息
    queryEdorApp();
    //alert("queryEdorMain");
    //查询批改主表信息
    queryEdorMain();
    //alert("getEdorItem");
    //查询保全项目
    getEdorItem();
    
}

/*********************************************************************
 *  查询团体保全申请信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryEdorApp()
 {
    try
    {
        var i = 0;
        var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
        var tContNo = fm.all('ContNo').value;
        var showStr;
        var urlStr;
        	
        
        //查询保全申请信息
        var strSQL= "select * from LPEdorApp where EdorAcceptNo='"
                    + tEdorAcceptNo+"'";		
        //alert("strSQL"+strSQL);        
        
        arrResult = easyExecSql(strSQL);    
        //alert("arrResult"+arrResult);
        if (arrResult == null) 
        {
                    alert("未查到保全申请号为"+tEdorAcceptNo+"的保全申请信息！");
        } 
        else
        {
            try {fm.all('EdorAcceptNo').value= arrResult[0][0]; } catch(ex) { }; //保全受理号
            try {fm.all('OtherNo').value= arrResult[0][1]; } catch(ex) { }; //申请号码
            try {fm.all('OtherNoType').value= arrResult[0][2]; } catch(ex) { }; //申请号码类型
            try {fm.all('EdorAppName').value= arrResult[0][3]; } catch(ex) { }; //申请人名称
            try {fm.all('AppType').value= arrResult[0][4]; } catch(ex) { }; //申请方式
        
        }
    }
    catch(ex)
    {
        alert("在GEdorInput.js-->queryEdorApp函数中发生异常:"+ex);
    }
    
}  
/*********************************************************************
 *  查询批改主表信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryEdorMain()
 { 
    
       
    var i = 0;
    var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
    var tEdorNo = fm.all('EdorNo').value;
    var showStr;
    var urlStr;
    	
    
    //查询批改主表信息
    var strSQL= "select ChgPrem,GetMoney,EdorAppDate,EdorValiDate,Operator from LPEdorMain where EdorAcceptNo='"
                + tEdorAcceptNo + "' and EdorNo='"+tEdorNo+"'";		
//    alert("strSQL"+strSQL);        
    
    arrResult = easyExecSql(strSQL);    
    //alert("arrResult"+arrResult);
    if (arrResult == null) 
    {
          
    } 
    else
    {
        try {fm.all('ChgPrem').value= arrResult[0][0]; } catch(ex) { }; //变动的保费
        try {fm.all('GetMoney').value= arrResult[0][1]; } catch(ex) { }; //补/退费金额
        try {fm.all('EdorAppDate').value= arrResult[0][2]; } catch(ex) { }; //批改申请日期
        try {fm.all('EdorValiDate').value= arrResult[0][3]; } catch(ex) { }; //批改生效日期
        try {fm.all('Operator').value= arrResult[0][4]; } catch(ex) { }; //操作员
        
    }
    
}
/*********************************************************************
 *  查询保全项目，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getEdorItem()
{
    
        var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
        var tEdorNo = fm.all('EdorNo').value;
        //险种信息初始化
        if(tEdorAcceptNo!=null&&tEdorNo!="")
        {
            var strSQL ="select EdorAcceptNo,EdorNo,EdorType,DisplayType,GrpContNo,ContNo,InsuredNo,PolNo,EdorValiDate,GetMoney from LPEdorItem where EdorAcceptNo='"+tEdorAcceptNo+"' and EdorNo='"+tEdorNo+"'";
            //alert(strSQL);
            turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
            //判断是否查询成功
            if (!turnPage.strQueryResult) 
            {
                return false;
            }
            //查询成功则拆分字符串，返回二维数组
            turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
            //设置初始化过的MULTILINE对象
            turnPage.pageDisplayGrid = EdorItemGrid;    
            //保存SQL语句
            turnPage.strQuerySql = strSQL; 
            //设置查询起始位置
            turnPage.pageIndex = 0;  
            //在查询结果数组中取出符合页面显示大小设置的数组
            arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
            //调用MULTILINE对象显示查询结果
            displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
        }
}

/*********************************************************************
 *  添加保全项目
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addRecord()
{
    if (fm.all('EdorNo').value==null||fm.all('EdorNo').value=="")
    {
         alert("请先保存保全申请，再添加保全项目!");
        return false;    
    }
    if (fm.all('EdorType').value==null||fm.all('EdorType').value=="")
    {
         alert("请选择需要添加的保全项目");
        return false;    
    }
    fm.all('fmtransact').value="INSERT||EDORITEM";
    var tActionOld=fm.action;
    fm.action = "../bq/PEdorItemSave.jsp"
    fm.submit();
    fm.action=tActionOld;
}

/*********************************************************************
 *  删除保全项目
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function deleteRecord()
{
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，显示项目明细按钮
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getEdorItemDetail(parm1,parm2)
{
    fm.EdorType.value=fm.all(parm1).all('EdorItemGrid3').value
    fm.CustomerNoBak.value=fm.all(parm1).all('EdorItemGrid7').value
    fm.ContNoBak.value=fm.all(parm1).all('EdorItemGrid6').value
    
}

/*********************************************************************
 *  查询被保险人信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getInsuredInfo()
{
    var ContNo=fm.all("ContNo").value;
    if(ContNo!=null&&ContNo!="")
    {
        var strSQL ="select InsuredNo,Name,Sex,Birthday,IdNo,RelationToMainInsured from LCInsured where ContNo='"+ContNo+"'";
        turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
        //判断是否查询成功
        if (!turnPage.strQueryResult) 
        {
            return false;
        }
        //查询成功则拆分字符串，返回二维数组
        turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
        //设置初始化过的MULTILINE对象
        turnPage.pageDisplayGrid = InsuredGrid;    
        //保存SQL语句
        turnPage.strQuerySql = strSQL; 
        //设置查询起始位置
        turnPage.pageIndex = 0;  
        //在查询结果数组中取出符合页面显示大小设置的数组
        arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
        //调用MULTILINE对象显示查询结果
        displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    }
    
}
