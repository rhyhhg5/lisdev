//               该文件中包含客户端需要处理的函数和事件

var showInfo1;
var mDebug="1";

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeCTReturn()
{
	try
	{
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {}
}

function getPolInfo(tContNo)
{
    var strSQL ="select InsuredNo,InsuredName,PolNo,RiskCode,Prem,Amnt,CValiDate,contno,grpcontno, to_char(LCPol.mult) from LCPol where ContNo='"+tContNo+"'";
    
    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
        return false;
    }
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
    turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
    turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
    
    for(var i = 0; i < PolGrid.mulLineCount; i++)
    {
    	if(PolGrid.getRowColData(i, 10) == "0")
    	{
    		PolGrid.setRowColData(i, 10, "");
    	}
    }
}

//判断付费方式是否为银行转账，paymode=4，显示更改银行信息
function chkPay(){
  	
  	var strSQL2 = "  select paymode from lccont where contno = '"+fm.ContNo.value+"' ";
    var arrPay =easyExecSql(strSQL2);
  	
  	if( arrPay == "4")
  	{
  		divPayInfo.style.display='';
  		DivPayMode.style.display='';
  	}
  	
  	
  	 var isModify = true;
	 var strSQL = "select bankcode,bankaccno,accname,managecom,paymode from lpcont where contNo='"+fm.ContNo.value  + "' and edortype='CT' and edorno='"+fm.EdorNo.value+"'";
     arrResult = easyExecSql(strSQL,1,0);
   

   if(arrResult == null)
   {
   	    isModify = false;
		var strSQL = "select bankcode,bankaccno,accname,managecom,paymode from lccont where contNo='"+fm.ContNo.value  + "'";
	    arrResult = easyExecSql(strSQL,1,0);
   }
   
   if( arrResult != null )
  {
			
	    fm.BankCode.value = arrResult[0][0];
		fm.BankAccNo.value = arrResult[0][1];
		fm.AccName.value = arrResult[0][2];
		fm.ManageCom.value = arrResult[0][3];
		fm.PayMode.value = arrResult[0][4];
		
		
        var sql = "  select BankName "
                + "from LDBank "
                + "where Bankcode = '" + arrResult[0][0] + "' ";
                result = easyExecSql(sql);
                if(result)
                {
                    fm.BankCodeName.value = result[0][0];
                }
  }
  	
}

function chkPol()
{
    var tContno=fm.all('ContNo').value;
    var tEdorNo=fm.all('EdorNo').value;
    var tEdorType=fm.all('EdorType').value;
    var strSQL="select polno from lppol where edorno='"+tEdorNo+"' and edortype='"+tEdorType+"' and contno='"+tContno+"'";        
    var arrResult2=easyExecSql(strSQL);
    var m=0;
  	var n=0;
  	
  	if(arrResult2!=null)
  	{
  		var q=arrResult2.length;
  		for(m=0;m<PolGrid.mulLineCount;m++)
	  	{
	  		
	  		for(n=0;n<q;n++)
	  	   {
	  			if(PolGrid.getRowColData(m,3)==arrResult2[n][0])
	  			{
	  				PolGrid.checkBoxSel(m+1);
	  			}
	  		}
	  	}				
  	}
}

function edorTypeCTSave()
{
  if (!beforeSubmit())
  {
    return false;
  }

  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo1=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	try { showInfo1.close(); } catch(ex) { }
	window.focus();
	if (FlagStr == "Fail" )
	{             
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	}
	else
	{ 
	  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  edorTypeCTReturn();
	}
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  var checked = false;
  for (i = 0; i < PolGrid.mulLineCount; i++)
  {
  	if (PolGrid.getChkNo(i)) 
		{
			checked = true;
			break;
		}
	}
	if (checked == false)
	{
		alert("请选择需解约的险种！");
		return false;
	}
  if (!verifyInput2())
  {
    return false;
  }
  if(!checkJianGuan())
  {
      return false;
  }
  
  if(!beforeChange())
  {
	  return false;
  }
  if(!checkTaxMoney())
  {
	  return false;
  }
  return true;
}         

function verify() {
	  if (trim(fm.BankCode.value)=="0101") {
	    if (trim(fm.BankAccNo.value).length!=19 || !isInteger(trim(fm.BankAccNo.value))) {
	      alert("工商银行的账号必须是19位的数字，最后一个星号（*）不要！");
	      return false;
	    }
	  }
	  // 20170424 lichang 校验账户名称是否相同
  	  var strSQL2 = "  select appntname from lccont where contno = '"+fm.ContNo.value+"' ";
      var arrResult2 = easyExecSql(strSQL2);
      if(trim(fm.AccName.value) != arrResult2){
    	  alert("修改的银行账户名与投保人不同，请重新录入！");
	      return false;
      }
	  return true;
	}

function beforeChange()
{
  if (DivPayMode.style.display=="")
  {
    if (fm.BankCode.value == "")
    {
      alert("请输入转帐银行！");
      fm.BankCode.focus();
      return false
    }
    if (fm.BankAccNo.value == "")
    {
      alert("请输入转帐帐号！");
      fm.BankAccNo.focus();
      return false
    }
    if (fm.AccName.value == "")
    {
      alert("请输入帐户名！");
      fm.AccName.focus();
      return false
    }
    if (!verify()) 
    {
      return false;
    }
  }
  return true;
}

function checkTaxMoney()
{
  if (divSYInfo.style.display=="")
  {
	var regMo = /^\d+(\.\d+)?$/;
    if (fm.TaxMoney.value == "")
    {
      alert("未输入补交税收优惠额度！");
      fm.TaxMoney.focus();
      return false
    }
    else if(!regMo.test(fm.TaxMoney.value))
    {
  	  alert("补交税收优惠额度必须为数字，且不能小于0，请核查！");
  	  return  false;

    }
    else if(fm.TaxMoney.value == 0)
    {
    	if(confirm("补交税收优惠额度为0，是否确认保存？")==false)
    	{
    	      fm.TaxMoney.focus();
    	      return false
    	}
    }
    

    
  }
  return true;
}

//保单下只有建管险种有效，则不能进行退保
function checkJianGuan()
{
 var wherePart="";
 var rowCount = 0;
 for (i = 0; i < PolGrid.mulLineCount; i++)
 {
		if (PolGrid.getChkNo(i)) 
		{
		    if(rowCount!=0)
		    {
		       wherePart=wherePart+",";
		    }
		    rowCount = rowCount + 1;
		  	var value=PolGrid.getRowColData(i,3);
		  	wherePart=wherePart+"'"+value+"'";
		}
  }
  var strSQL = "  select 1 from lcpol where contno = '"+fm.ContNo.value+"' "
             + "  and PolNo not in ("+wherePart+")";
  var arrResult = easyExecSql(strSQL);
  if (arrResult != null) //放开保单下险种都解约的情况
  {
    //剩下的险种中，存在statefla in ('1','2')的健管险种
    var strSQL1 = "  select 1 from lcpol a where contno = '"+fm.ContNo.value+"' "
             + " and riskcode in (select riskcode from lmriskapp where risktype ='M' )"
             + " and PolNo not in ("+wherePart+") and ContType = '1' and AppFlag = '1' and stateflag in ('1','2') ";
    var arrResult1 = easyExecSql(strSQL1);
  
    //剩下的险种中，存在非健管险种
    var strSQL2 = "  select 1 from lcpol a where contno = '"+fm.ContNo.value+"' "
             + " and riskcode not in (select riskcode from lmriskapp where risktype ='M' )"
             + " and PolNo not in ("+wherePart+") and ContType = '1' and AppFlag = '1' ";
    var arrResult2 = easyExecSql(strSQL2);
    
    if (arrResult1 != null && arrResult2 == null) //对保单下只有建管险种不解约的进行校验
    {           
      alert("健管险种不能单独承保，请同时选择健管险种！")
      return false;
    }
   
  }
  return true;
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
	alert("query click");
	  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}
