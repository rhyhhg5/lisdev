<%
//程序名称：PEdorTypeLFSubmit.jsp
//程序功能：
//创建日期：2002-07-19 16:49:22
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page contentType="text/html;charset=GBK" %>

<%
  //个人批改信息
  System.out.println("---LF submit---");
  LPLoanSchema tLPLoanSchema = new LPLoanSchema();
  //LPEdorMainSchema tLPEdorMainSchema = new LPEdorMainSchema();
    
  //后面要执行的动作：添加，修改
  CErrors tError = null;                 
  String FlagStr = "";
  String Content = "";
  String transact = "";
  String Result = "";
  String canLoanMoney = "";
  String thisLoanMoney = "";
  
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("---transact: " + transact);
  System.out.println("\nSumMoney:" + request.getParameter("SumMoney"));
  //System.out.println("\InterestType:" + request.getParameter("InterestType"));
  
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");
  
  //批改信息
  tLPLoanSchema.setPolNo(request.getParameter("PolNo"));
  tLPLoanSchema.setEdorNo(request.getParameter("EdorNo"));
  tLPLoanSchema.setEdorType(request.getParameter("EdorType"));
  tLPLoanSchema.setSumMoney(request.getParameter("SumMoney"));
  
  System.out.println("RChoose:" + request.getParameter("RChoose"));
  if (request.getParameter("RChoose") == "NO")
  {
    tLPLoanSchema.setInterestType("");
    tLPLoanSchema.setInterestMode("");
    tLPLoanSchema.setInterestRate("");
  }
  else
  {
    tLPLoanSchema.setInterestType(request.getParameter("InterestType"));
    tLPLoanSchema.setInterestMode(request.getParameter("InterestMode"));
    tLPLoanSchema.setInterestRate(request.getParameter("InterestRate"));
  }

  System.out.println("tLPLoanSchema:" + tLPLoanSchema.encode());
  
  PEdorLFDetailUI tPEdorLFDetailUI = new PEdorLFDetailUI();

  try 
  {
    // 准备传输数据 VData
    VData tVData = new VData();  
  	
    //保存个人保单信息(保全)	
    tVData.addElement(tG);
    tVData.addElement(tLPLoanSchema);
    
    boolean tag = tPEdorLFDetailUI.submitData(tVData,transact);
    
    if (tag == false) 
    {
      FlagStr = "Fail";
      Content = "操作失败!!" + (String)tPEdorLFDetailUI.getResult().get(0);
    }
  } 
  catch(Exception ex) 
  {
    Content = transact + "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
	}			
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "") 
  {
  	tError = tPEdorLFDetailUI.mErrors;
  	if (!tError.needDealError()) 
  	{                          
      Content = "保存成功";
  		FlagStr = "Success";
  		
  		if (transact.equals("QUERY||MAIN")) 
  		{
    		if (tPEdorLFDetailUI.getResult()!=null && tPEdorLFDetailUI.getResult().size()>0)
    	  {
    			Result = (String)tPEdorLFDetailUI.getResult().get(0);
    			canLoanMoney = (String)tPEdorLFDetailUI.getResult().get(1);
    			thisLoanMoney = (String)tPEdorLFDetailUI.getResult().get(2);
    			
    		  if (Result == null || Result.trim().equals("")) 
    		  {
    				FlagStr = "Fail"; 
    				Content = "查询失败!!";
    		  }
    	  }
  	  }
    } 
    else  
    {
  		Content = " 保存失败，原因是:" + tError.getFirstError();
  		FlagStr = "Fail";
  	}
	}
	
  //添加各种预处理
  System.out.println("------------Result is------------\n" + Result);
%>   
                   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>", "<%=Content%>", "<%=Result%>", "<%=canLoanMoney%>", "<%=thisLoanMoney%>");
</script>
</html>

