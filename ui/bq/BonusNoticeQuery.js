//程序名称：BonusNoticeQuery.js

var turnPage = new turnPageClass();
var mSql = "";
var showInfo;

// 下载清单
function download() {
	if (!verifyInput()) {
		return false;
	}
	var tStartDate = fm.all('StartDate').value; // 红利应派发起期
	var tEndDate = fm.all('EndDate').value; // 红利应派发止期

	// 红利应派发起期校验
	if (tStartDate == "" || tStartDate == null) {
		alert("请输入红利应派发起期！");
		return false;
	} else {
		if (!checkDateFormat("红利应派发起期", tStartDate)) {
			fm.all('StartDate').focus();
			return false;
		}
	}
	// 红利应派发止期校验
	if (tEndDate == "" || tEndDate == null) {
		alert("请输入红利应派发止期！");
		return false;
	} else {
		if (!checkDateFormat("红利应派发止期", tEndDate)) {
			fm.all('EndDate').focus();
			return false;
		}
	}
	// 生效日期起止期三个月校验
	var t1 = new Date(tStartDate.replace(/-/g, "\/")).getTime();
	var t2 = new Date(tEndDate.replace(/-/g, "\/")).getTime();
	var t3 = new Date(tCurrentDate.replace(/-/g, "\/")).getTime();
	if (t2 - t1 < 0) {
		alert("红利应派发起期不能晚于红利应派发止期 ");
		return false;
	}
	var tMinus = (t2 - t1) / (24 * 60 * 60 * 1000);
	if (tMinus > 93) {
		alert("红利应派发起止日期不能超过3个月！");
		return false;
	}
	var tMinuscur = (t2 - t3) / (24 * 60 * 60 * 1000);
	if (tMinuscur > 31) {
		alert("红利应派发止期不能超过当前日期1个月！");
		return false;
	}
	var showStr = "正在下载数据，时间可能较长，请您稍候并且不要修改屏幕上的值或链接其他页面……";
	fm.all("divInfo").style.display = "";
	fm.all("divInfo").innerHTML = "<font color=red size=6>" + showStr
			+ "</font>";
	fm.submit();
}

// 日期格式校验
function checkDateFormat(tName, strValue) {
	if (!isDate(strValue)) {
		alert("输入的[" + tName + "]不正确！\n(格式:YYYY-MM-DD)");
		return false;
	}
	return true;
}
