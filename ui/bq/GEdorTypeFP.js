//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";

function edorTypeFPCancel()
{
		initForm();
}

function edorTypeFPSave()
{
  if (!beforeSubmit())
  {
    return false;
  }	
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmAction').value = "INSERT||EDORFP";
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  	returnParent();
  }
}


//提交前的校验、计算  
function beforeSubmit()
{
	
  if (!verifyInput2())	
  {
    return false;
  }
/*
  if(fm.servchoose1.value!='')
  {
	  var strSQL1 = "select servchoose from LDServChooseInfo where kindcode='2' and servkind='1' and servdetail='2' and servchoose='" + fm.servchoose1.value + "'";
	  var arrResult1 = easyExecSql(strSQL1, 1, 0);
	  if(arrResult1==null)
	  {
	  	alert('提交频次代码不正确，请重新选择！');
	  	fm.servchoose1.focus();
	  	return false;
	  }
  }
  if(fm.servchoose2.value!='')
  {
	  var strSQL2 = "select servchoose from LDServChooseInfo where kindcode='2' and servkind='1' and servdetail='3' and servchoose='" + fm.servchoose2.value + "'";
	  var arrResult2 = easyExecSql(strSQL2, 2, 0);
	  if(arrResult2==null)
	  {
	  	alert('结算频次代码不正确，请重新选择！');
	  	fm.servchoose2.focus();
	  	return false;
	  }
  }  
*/
	if (fm.servchoose1Bak.value != ""&&
   	fm.servchoose1.value == "")
  {
   	alert('提交频次不能设置为空！');
   	fm.servchoose1.focus();
		return false;
  }
  
  if (fm.servchoose2Bak.value != ""&&
   	fm.servchoose2.value == "")
  {
   	alert('结算频次不能设置为空！');
   	fm.servchoose2.focus();
		return false;
  }

  if (fm.servchoose1Bak.value == fm.servchoose1.value&&
   	fm.servchoose2Bak.value == fm.servchoose2.value)
	{
		alert('保单服务约定未发生变更！');
		return false;
	}
  return true;
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

function returnParent()
{
	try
	{
	  top.opener.focus();
		top.opener.getGrpEdorItem();
		top.close();
	}
	catch (ex)
	{
	}
}

function getGrpCont()
{
  var strSQL = "select AppntNo,GrpName,SignDate,CvaliDate,CInValiDate,SignCom,Peoples2 from LCGrpCont  where GrpContNo = '" 
                + fm.GrpContNo.value + "'";
  var arrResult = easyExecSql(strSQL, 1, 0);
  
    if (arrResult != null) 
    {
        try {fm.all('AppntNo').value= arrResult[0][0]; } catch(ex) { }; //客户号码
        try {fm.all('GrpName').value= arrResult[0][1]; } catch(ex) { }; //单位名称
        try {fm.all('SignDate').value= arrResult[0][2]; } catch(ex) { }; //签单日期
        try {fm.all('CValiDate').value= arrResult[0][3]; } catch(ex) { }; //保单生效日期
        try {fm.all('CInValiDate').value= arrResult[0][4]; } catch(ex) { }; //保单生效日期
        try {fm.all('SignCom').value= arrResult[0][5]; } catch(ex) { }; //签单机构
        try {fm.all('Peoples2').value= arrResult[0][6]; } catch(ex) { }; //投保总人数
    }
    else
    {
        alert('未查到该集体保单！');
    }     
}

function getGrpServDetailInfo()
{
  var strSQL = "select a.servdetail, a.servchoose from LCGrpServInfo a,lcgrpcont c "
  							+"where a.proposalgrpcontno=c.proposalgrpcontno and c.grpcontno ='"+fm.GrpContNo.value+"' and servkind='1' order by servdetail"  						
  var arrResult = easyExecSql(strSQL);
  if (arrResult == null) {
  	return false;
  }
  var m = arrResult.length;
  for (i = 0; i < m; i++) {
    //if(arrResult[i][0]=='1')
    //{
    //	try {fm.all('servchoose0').value= arrResult[i][1]; } catch(ex) { }; 
    //	alert(fm.all('servchoose0').value)
    //}
    if(arrResult[i][0]=='2')
    {
    	try {fm.all('servchoose1').value= arrResult[i][1]; } catch(ex) { };
    	try {fm.all('servchoose1Bak').value= arrResult[i][1]; } catch(ex) { };
    }
    if(arrResult[i][0]=='3')
    {
    	try {fm.all('servchoose2').value= arrResult[i][1]; } catch(ex) { }; 
    	try {fm.all('servchoose2Bak').value= arrResult[i][1]; } catch(ex) { };
    }
  }
  strSQL = "select proposalgrpcontno from lcgrpcont where grpcontno = '"+fm.GrpContNo.value+"'";
  arrResult = easyExecSql (strSQL);
  if(arrResult != null)
  {
  	fm.all('ProposalGrpcontno').value = arrResult[0][0];
  }
}