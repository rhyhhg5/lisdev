//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

function  edorAppConfirm()
{
    fm.all("transAct").value = "AppConfirm";
    fm.all("EdorNo").value = GEdorAppConfirmGrid.getRowColData(0,1);
    fm.all("GrpContNo").value = GEdorAppConfirmGrid.getRowColData(0,2);
    fm.all("Operate").value = "PRINT";
    fm.all("EdorValiDate").value=GEdorAppConfirmGrid.getRowColData(0,4);
    fm.all("EdorAppDate").value = GEdorAppConfirmGrid.getRowColData(0,5);
	if (window.confirm("是否确认本次申请?"))
	{
		var showStr="正在计算数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		fm.submit();
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content , prtParams)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //开始执行打印操作
    /**if (window.confirm("是否打印本次保全申请单?")){   //yangzhao 11-26
    	    PrtEdor(prtParams);
    	    top.close();
    }*/
    window.open("./GEdorAppConfirmResult.jsp?EdorNo="+fm.all("EdorNo").value);
  }
}
//打印批单
function PrtEdor(prtParams)
{
	if (fm.all("EdorNo").value == "0") {
		alert("由于数据出错，无法打印批单。");
		return;
	}	
	//提交表单，打印主批单
	parent.fraInterface.fm.action = "../f1print/GrpEndorsementF1P.jsp";
	parent.fraInterface.fm.target="f1print";	
	fm.submit();
	//打印费用清单	
	parent.fraInterface.fm.action = "./GEdorAppConfirmSubmit.jsp";
	parent.fraInterface.fm.target="fraSubmit";
	//alert(fm.all("EdorNo").value);
	//if (prtParams!="") {
	//    window.open("../f1print/EdorFeeF1PSave.jsp?OtherNo="+prtParams);	    	
	//}
	//window.open("../f1print/EdorFeeF1PSave.jsp?OtherNo="+fm.all("EdorNo").value);
	
}  


//*************************
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

// 查询按钮
function easyQueryClick()
{	
	//var tEdorState;

	//tEdorState=top.opener.fm.all('EdorState').value;
		
	
	// 初始化表格
	initGEdorAppConfirmGrid();
	
	var tReturn = parseManageComLimit();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select EdorNo,GrpContNo,EdorType,EdorValiDate,EdorAppDate from LPGrpEdorItem where EdorState='1' "//+" and"+tReturn
				 + getWherePart( 'EdorNo' );
	execEasyQuery( strSQL );
}

function displayEasyResult( arrResult )
{
	var i, j, m, n;

	if( arrResult == null )
		alert( "没有找到相关的数据!" );
	else
	{
		// 初始化表格
		initGEdorAppConfirmGrid();
		
		arrGrid = arrResult;
		// 显示查询结果
		n = arrResult.length;
		for( i = 0; i < n; i++ )
		{
			m = arrResult[i].length;
			for( j = 0; j < m; j++ )
			{
				GEdorAppConfirmGrid.setRowColData( i, j+1, arrResult[i][j] );
			} // end of for
		} // end of for
		//alert("result:"+arrResult);
	} // end of if
}

// 数据返回父窗口
function returnParent()
{
	top.close();
}

