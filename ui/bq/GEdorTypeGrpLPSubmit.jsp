<%
//程序名称：GEdorTypeLPSubmit.jsp
//程序功能：
//创建日期：2005-12-26
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=gb2312" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String flag;
	String content;
	LCGrpAppntSchema tLCGrpAppntSchema=new LCGrpAppntSchema();
	
	GlobalInput gi = (GlobalInput)session.getValue("GI");
	String edorNo = request.getParameter("EdorNo");
	String grpContNo = request.getParameter("GrpContNo");
	tLCGrpAppntSchema.setClaimBankCode(request.getParameter("BankCode"));
	tLCGrpAppntSchema.setClaimBankAccNo(request.getParameter("BankAccNo"));
	tLCGrpAppntSchema.setClaimAccName(request.getParameter("AccName"));
	
	
	//准备传输数据 VData
	VData tVData = new VData();  
	
	//保存个人保单信息(保全)
	tVData.addElement(tLCGrpAppntSchema);
	
  GEdorGrpLPDetailUI tGEdorGrpLPDetailUI = new GEdorGrpLPDetailUI(gi, edorNo, grpContNo);
	if (!tGEdorGrpLPDetailUI.submitData(tVData))
	{
		flag = "Fail";
		content = "数据保存失败！原因是:" + tGEdorGrpLPDetailUI.getError();
	}
	else 
	{
		flag = "Succ";
		content = "数据保存成功。";
	}
	content = PubFun.changForHTML(content);
%>   
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit1("<%=flag%>", "<%=content%>");
</script>
</html>