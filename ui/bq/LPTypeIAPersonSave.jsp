<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LDPersonSave.jsp
//程序功能：  
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDPersonSchema tLDPersonSchema   = new LDPersonSchema();
  LDPersonUI tLDPersonUI   = new LDPersonUI();
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
   String Operator  = tGI.Operator ;  //保存登陆管理员账号
   String ManageCom = tGI.ComCode  ; //保存登陆区站,ManageCom=内存中登陆区站代码
  
  CErrors tError = null;
  String tBmCert = "";
  
  //后面要执行的动作：添加，修改，删除
  String transact=request.getParameter("Transact");
System.out.println("transact:"+transact); 

/*        
  String tLimit="";
  String CustomerNo="";
*/ 
    if(transact.equals("INSERT"))
    {
    //生成客户号
    //tLimit=PubFun.getNoLimit(ManageCom);
/*    
    tLimit = "SN";
    CustomerNo=PubFun1.CreateMaxNo("CustomerNo",tLimit);   
    tLDPersonSchema.setCustomerNo(CustomerNo);
*/
    }
    else{
    tLDPersonSchema.setCustomerNo(request.getParameter("CustomerNo"));    
    }
    tLDPersonSchema.setPassword(request.getParameter("Password"));
    tLDPersonSchema.setName(request.getParameter("Name"));        
    tLDPersonSchema.setSex(request.getParameter("Sex"));
    tLDPersonSchema.setBirthday(request.getParameter("Birthday"));
    tLDPersonSchema.setNativePlace(request.getParameter("NativePlace"));
    tLDPersonSchema.setNationality(request.getParameter("Nationality"));
    tLDPersonSchema.setMarriage(request.getParameter("Marriage"));
    tLDPersonSchema.setMarriageDate(request.getParameter("MarriageDate"));
    tLDPersonSchema.setOccupationType(request.getParameter("OccupationType"));
    tLDPersonSchema.setStartWorkDate(request.getParameter("StartWorkDate"));
    tLDPersonSchema.setSalary(request.getParameter("Salary"));
    tLDPersonSchema.setHealth(request.getParameter("Health"));
    tLDPersonSchema.setStature(request.getParameter("Stature"));
    tLDPersonSchema.setAvoirdupois(request.getParameter("Avoirdupois"));
    tLDPersonSchema.setCreditGrade(request.getParameter("CreditGrade"));
    tLDPersonSchema.setIDType(request.getParameter("IDType"));
    tLDPersonSchema.setProterty(request.getParameter("Proterty"));
    tLDPersonSchema.setIDNo(request.getParameter("IDNo"));
    tLDPersonSchema.setOthIDType(request.getParameter("OthIDType"));
    tLDPersonSchema.setOthIDNo(request.getParameter("OthIDNo"));
    tLDPersonSchema.setICNo(request.getParameter("ICNo"));
    tLDPersonSchema.setHomeAddressCode(request.getParameter("HomeAddressCode"));
    tLDPersonSchema.setHomeAddress(request.getParameter("HomeAddress"));
    tLDPersonSchema.setPostalAddress(request.getParameter("PostalAddress"));
    tLDPersonSchema.setZipCode(request.getParameter("ZipCode"));
    tLDPersonSchema.setPhone(request.getParameter("Phone"));
    tLDPersonSchema.setBP(request.getParameter("BP"));
    tLDPersonSchema.setMobile(request.getParameter("Mobile"));
    tLDPersonSchema.setEMail(request.getParameter("EMail"));
    tLDPersonSchema.setBankCode(request.getParameter("BankCode"));
    tLDPersonSchema.setBankAccNo(request.getParameter("BankAccNo"));
    tLDPersonSchema.setJoinCompanyDate(request.getParameter("JoinCompanyDate"));
    tLDPersonSchema.setPosition(request.getParameter("Position"));
    tLDPersonSchema.setGrpNo(request.getParameter("GrpNo"));
    tLDPersonSchema.setGrpName(request.getParameter("GrpName"));
    tLDPersonSchema.setGrpPhone(request.getParameter("GrpPhone"));
    tLDPersonSchema.setGrpAddressCode(request.getParameter("GrpAddressCode"));
    tLDPersonSchema.setGrpAddress(request.getParameter("GrpAddress"));
    tLDPersonSchema.setDeathDate(request.getParameter("DeathDate"));
    tLDPersonSchema.setRemark(request.getParameter("Remark"));
    tLDPersonSchema.setState(request.getParameter("State"));
    tLDPersonSchema.setBlacklistFlag(request.getParameter("BlacklistFlag"));    
    tLDPersonSchema.setOperator(Operator);
    
    tLDPersonSchema.setWorkType(request.getParameter("WorkType"));
    tLDPersonSchema.setPluralityType(request.getParameter("PluralityType"));
    tLDPersonSchema.setOccupationCode(request.getParameter("OccupationCode"));
    tLDPersonSchema.setDegree(request.getParameter("Degree"));
    tLDPersonSchema.setGrpZipCode(request.getParameter("GrpZipCode"));
    tLDPersonSchema.setSmokeFlag(request.getParameter("SmokeFlag"));
    tLDPersonSchema.setRgtAddress(request.getParameter("RgtAddress"));
    tLDPersonSchema.setHomeZipCode(request.getParameter("HomeZipCode"));
    tLDPersonSchema.setPhone2(request.getParameter("Phone2"));  
    
 try
  {
  // 准备传输数据 VData
   VData tVData = new VData();
   tVData.addElement(tLDPersonSchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  if ( tLDPersonUI.submitData(tVData,transact))
  {
    		if (transact.equals("INSERT"))
		{
		    	System.out.println("------return");
			    	
		    	tVData.clear();
		    	tVData = tLDPersonUI.getResult();
		    	System.out.println("-----size:"+tVData.size());
		    	
		    	LDPersonSchema mLDPersonSchema = new LDPersonSchema(); 
			mLDPersonSchema=(LDPersonSchema)tVData.getObjectByObjectName("LDPersonSchema",0);
			
			System.out.println("test");
			
			if( mLDPersonSchema.getCustomerNo() == null ) {
				System.out.println("null");
			}
			
			String strCustomerNo = mLDPersonSchema.getCustomerNo();
			System.out.println("jsp"+strCustomerNo);
			if( strCustomerNo == null ) {
				strCustomerNo = "123";
				System.out.println("null");
			}
			
			// System.out.println("-----:"+mLAAgentSchema.getAgentCode());
		%>
		<SCRIPT language="javascript">
			parent.fraInterface.fm.all("CustomerNo").value ="<%=strCustomerNo%>";
		</SCRIPT>
		<%
		}
	}
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDPersonUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
   }
}//页面有效区
%>                                       
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

