//程序功能：
//创建日期：2011/9/26
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus; 
var turnPage = new turnPageClass();

var RelaGridTurnPage = new turnPageClass();
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	 if(showInfo!=null)
	 {
		   try
		   {
		     showInfo.focus();  
		   }
		   catch(ex)
		   {
		     showInfo=null;
		   }
	 }
}
//删除
function lmRateDelete(){
	var arrReturn = new Array();
	var tSel = RateGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点删除按钮。" );
		return false;
	}
	var result = new Array();
	result[0] = new Array();
	result[0] = RateGrid.getRowData(tSel-1);
	//只能修改仅能修改某一分公司或总公司机构下【利率终止日期】和【最大保费金额】均为最大
	var sSql = " select a.Serialno,a.ComCode,a.riskcode, " +
			" a.startdate,a.enddate,a.MinPremLimit,a.MaxPremLimit,a.rate from Lminterestrate a " +
			" where comcode='"+result[0][1]+"'  " +
			" and riskcode='"+result[0][3]+"'  " +
			" order by enddate desc " +
			" fetch first 1 rows only with ur  ";
	var sSer = easyExecSql(sSql);
	//只能删除利率终止日期最大的一条记录
	if(sSer[0][4]!=result[0][6]){
		alert("只能删除某一分公司或总公司机构下的某一险种的利率终止日期最大的一条记录");
		return false;
	}
	var startRatedate = result[0][5];
	var date= new Date();
	var year = date.getFullYear();
	var month = date.getMonth()+1;
	var day = date.getDate();
	var LocalDate =year + "-" + month + "-" + day ;
	if(compareDate(startRatedate,LocalDate)!=1){
		alert("删除的利率起始日期必须大于操作当天。");
		return false;
	}
	else
	{
		if(confirm("确定要删除所选记录吗？")){
//		fm.rateStartDate.value = rateStartDate;
			var showStr = "正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
//			showInfo = window.showModelessDialog(urlStr, window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
			fm.fmtransact.value = "DELETE||MAIN";;
			fm.submit();
			initRateBox();
		}else{
			alert("您取消了删除操作");
		}
	}
}

//修改利率
function lmRateUpdate(){
	//只能修改选中的那一行
	var tSel = RateGrid.getSelNo();
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点删除按钮。" );
		return false;
	}
	var result = new Array();
	//只能从该机构下该险种的的最后一行往前删
	result[0] = new Array();
	result[0] = RateGrid.getRowData(tSel-1);
	//只能修改仅能修改某一分公司或总公司机构下【利率终止日期】和【最大保费金额】均为最大
	var sSql = " select a.Serialno,a.ComCode,a.riskcode, " +
			" a.startdate,a.enddate,a.MinPremLimit,a.MaxPremLimit,a.rate from Lminterestrate a" +
			" where comcode='"+result[0][1]+"'  " +
			" and riskcode='"+result[0][3]+"'  " +
			" order by enddate desc,MinPremLimit desc " +
			" fetch first 1 rows only with ur  ";
	var sSer = easyExecSql(sSql);
	if(sSer){
		if(result[0][0]!=sSer[0][0]){
			alert("只能修改该机构的该险种下最新录入的利率记录");
			return false;
		}
	}
	//修改后的【利率终止日期】必须大于利率起始日期
	if(compareDate(fm.rateEndDate.value,result[0][5])==1){
		if(confirm("确定要修改所选记录吗？")){
			fm.fmtransact.value = "UPDATE||MAIN";;
			fm.submit();
			initRateBox();
		}
	}else{
		alert("利率终止日期必须大于等于利率起始日期！");
		return false;
	}
}

//查询
function lmRateQuery(){
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.Serialno,a.ComCode,(select name from ldcom where comcode=a.comcode),a.riskcode, " +
			" ( select riskname from lmriskapp where riskcode = a.riskcode ), " +
			" a.startdate,a.enddate,a.MinPremLimit,a.MaxPremLimit,a.rate from Lminterestrate a where  busstype='L' "
        + getWherePart('riskcode','RiskCode')
        + getWherePart('ComCode','aManageCom')
        //+ getWherePart('enddate','rateEndDate','<=')
        //+ getWherePart('rate','Rate')
        + " order by startdate";

	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  

    //判断是否查询成功
    if (!turnPage.strQueryResult) {
    	RateGrid.clearData();
        alert("未查询到满足条件的数据！");
        return false;
    }
    turnPage.pageDivName="divturnPage";
    turnPage.queryModal(strSQL, RateGrid);
}
//提交前的校验
function beforeSubmit(){
	var rate = fm.Rate.value;
	var reg=/^\d*\.\d*$/;
	var regs=/^(:?(:?\d+.\d+)|(:?\d+))$/;
	if(fm.RiskCode.value==""){
		alert("请输入险种");
		return false
	}
	if(fm.rateStartDate.value==""){
		alert("请输入利率起始日期");
		return false
	}
	if(fm.rateEndDate.value==""){
		alert("请输入利率终止日期");
		return false
	}
	if(fm.Rate.value==""){
		alert("请输入利率");
		return false;
	}
	if(fm.aManageCom.value==""){
		alert("请选择配置机构");
		return false;
	}
	if(fm.MinPremLimit.value==""){
		alert("最小保费金额不能为空");
		return false;
	}
	if(fm.MinPremLimit.value!=""&&fm.MaxPremLimit.value!=""){
		if(!regs.test(fm.MinPremLimit.value)){
			alert("请输入正确的最小保费金额");
			return false;
		}
		if(!regs.test(fm.MaxPremLimit.value)){
			alert("请输入正确的最大保费金额");
			return false;
		}
		if(fm.MinPremLimit.value*1>=fm.MaxPremLimit.value*1){
			alert("最小保费金额不能大于等于最大保费金额");
			return false;
		}
	}	
	
	if(fm.confirmRate.value==""){
		alert("请输入确认利率");
		return false;
	}
	if(fm.Rate.value!=""&&fm.Rate.value>=1){
		alert("请输入小于1的利率");
		return false;
	}
	if(!reg.test(rate)){
		alert("请输入正确的利率");
		return false;
	}
	if(fm.confirmRate.value!="" && fm.Rate.value!=fm.confirmRate.value){
		alert("两次利率输入不一致!");
		return false;
	}
	if(!rateDate()){
		return false;
	}
	return true;
}
//保存
function lmRateSave(){
	if(!beforeSubmit()){
		return false;
	}
	if(fm.all("Rate").disabled){
		alert("请不要保存系统已经存在的数据");
		return false;
	}
	if(confirm("确认保存吗？")){
	
		fm.fmtransact.value="INSERT||MAIN";
		var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
//		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fm.action = "LMinterestRateSave.jsp";
		fm.submit();
	}else{
		alert("取消保存！");
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr,content,transact){
  //lmRateQuery();
	 initForm();
	 initRateBox();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");       
  }
}

//校验利率起始日期和利率终止日期
function rateDate(){
	var ratestartDate=fm.rateStartDate.value;
	var rateendDate=fm.rateEndDate.value;
			if(compareDate(ratestartDate,rateendDate)==2||compareDate(ratestartDate,rateendDate)==0){
				return true;
			}else{
				alert("利率终止日期必须大于等于利率起始日期！");
				return false;
			}
}



//将字符串日期转换成日期格式
function strToDate(str)
{
  var arys= new Array();
  arys=str.split('-');
  var newDate=new Date(arys[0],arys[1],arys[2]);  
  return newDate;
}  
//当选择一条记录时，在页面显示其具体信息
function ShowDetail(){
	var arrReturn = getSelectedResult();
  	afterSelected(arrReturn);
}
function getSelectedResult(){
	var arrSelected = null;
	tRow =RateGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	{
	  return arrSelected;
	}
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = new Array();
	arrSelected[0] = RateGrid.getRowData(tRow-1);
	return arrSelected;
}	
function afterSelected(arrSelectedResult){
	
	
	var arrResult = new Array();	
	if(arrSelectedResult!= null )
	{
	  arrResult = arrSelectedResult;
	  fm.Serialno.value=arrResult[0][0];
	  fm.aManageCom.value=arrResult[0][1];
	  fm.ManageComName.value=arrResult[0][2];
	  fm.RiskCode.value = arrResult[0][3];
	  fm.rateStartDate.value = arrResult[0][5];
	  fm.rateEndDate.value = arrResult[0][6];
	  fm.MinPremLimit.value=arrResult[0][7];
	  fm.MaxPremLimit.value=arrResult[0][8];
	  fm.Rate.value = arrResult[0][9];
	  fm.confirmRate.value = arrResult[0][9];
	  fm.RiskName.value = arrResult[0][4];
	  
	  fm.DManageCom.value=arrResult[0][1];
	  fm.DRiskCode.value = arrResult[0][3];
	  fm.DrateStartDate.value = arrResult[0][5];
	  fm.DrateEndDate.value = arrResult[0][6];
	  fm.DRate.value = arrResult[0][9];
	  //选中后只能修改【利率终止日期】字段
	  fm.all("aManageCom").disabled = true;
	  fm.ManageComName.disabled = true;
	  fm.all("RiskCode").disabled = true;
	  fm.rateStartDate.disabled = true;
	  fm.MinPremLimit.disabled = true;
	  fm.MaxPremLimit.disabled = true;
	  fm.all("Rate").disabled = true;
	  fm.confirmRate.disabled = true;
	  fm.RiskName.disabled = true;
	}
	
}