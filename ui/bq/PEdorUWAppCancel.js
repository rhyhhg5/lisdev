//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass(); 
//var cEdorNo;
var cPolNo;
var alertflag = "0"; //10-29
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,0,0,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
	
	parent.fraMain.rows = "0,0,0,0,*";
}



// 查询按钮
function easyQueryClick()
{
	
//	alert("here");
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	
	strSQL = "select LPEdorMain.EdorNo,min(LPEdorMain.PolNo),min(LPEdorMain.EdorType),min(LCPol.RiskCode),min(LPEdorMain.InsuredNo),min(LPEdorMain.InsuredName),sum(LPEdorMain.GetMoney),decode(max(LPEdorMain.EdorState)||min(LPEdorMain.EdorState),'00','保全确认','11','正在申请','22','申请确认','21','部分申请确认','20','部分保全确认') from LPEdorMain,LCPol where  LPEdorMain.PolNo=LCPol.PolNo and LPEdorMain.UWState = '1' "			 
				 //+" and lpuwmaster.edorno = LPEdorMain.EdorNo "
				 //+" and lpuwmaster.edortype = LPEdorMain.EdorType"
				 + getWherePart( 'LPEdorMain.EdorNo','EdorNo' )
				 + getWherePart( 'LPEdorMain.PolNo','PolNo' )
				 + getWherePart( 'LPEdorMain.EdorType','EdorType' )
				 + getWherePart( 'LCPol.RiskCode','RiskCode' )
				 + getWherePart( 'LPEdorMain.InsuredNo','InsuredNo' )
				 + getWherePart( 'LPEdorMain.EdorAppDate','EdorAppDate' )
				 + getWherePart( 'LPEdorMain.ManageCom','ManageCom','like' )
				 + " group by LPEdorMain.EdorNo";			
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
//  alert(turnPage.strQueryResult);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
       if(alertflag!="1")     //
          alert("没有保全核保不通过的保全申请！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
//  alert("here");
}


//查询按钮
function QueryClick()
{
	initPolGrid();
	fm.all('Transact').value ="QUERY|EDOR";
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交 
}



// 项目明细查询
function ItemQuery()
{
	var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();
	
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击明细查询按钮。" );
	else
	{
	    var cEdorNo = PolGrid.getRowColData(tSel - 1,1);	
	    var cSumGetMoney = 	PolGrid.getRowColData(tSel - 1,6);			
//		parent.VD.gVSwitch.deleteVar("PayNo");				
//		parent.VD.gVSwitch.addVar("PayNo","",cPayNo);
		
		if (cEdorNo == "")
		    return;
		    
				window.open("../sys/AllPBqItemQuery.jsp?EdorNo=" + cEdorNo + "&SumGetMoney=" + cSumGetMoney);		
							
	}
}


	//撤销批单
function CancelEdor()
{
	var arrReturn = new Array();
				
		var tSel = PolGrid.getSelNo();
	
			
		if( tSel == 0 || tSel == null )
			alert( "请先选择一条记录，再点击申请撤销按钮。" );
		else
		{
			arrReturn = getQueryResult();
			fm.all('EdorNo').value = PolGrid.getRowColData(tSel-1,1);
			fm.all('PolNo').value = PolGrid.getRowColData(tSel-1,2);
			//---------------------------------------
			//cEdorNo = PolGrid.getRowColData(tSel-1,1);
			cPolNo = PolGrid.getRowColData(tSel-1,2);
			 alertflag = "1";//10-29
			//-----------------------------------
			//fm.all('EdorNo').value = arrReturn[0][0];
			//fm.all('PolNo').value=arrReturn[0][1];	
			fm.all("Transact").value = "DELETE||EDOR"
			// showSubmitFrame(mDebug);
		    var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  		    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  		    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			//easyQueryClick();   //10-22
			//alert("1234");
			fm.submit();  //10_22
		        //fm.all('PolNo').value = cPolNo;//
		        //easyQueryClick(); //
		}
}


function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrGrid == null )
		return arrSelected;
	arrSelected = new Array();
	
	//********dingzhong*************
	arrSelected[0] = PolGrid.getRowData(tRow-1);
	//*******************************
/*	
	//设置需要返回的数组
	arrSelected[0] = arrGrid[tRow-1];
	//alert(arrSelected[0][0]);
*/	
	return arrSelected;
}


function afterSubmit( FlagStr, content,Result )
{

  showInfo.close();
  if (FlagStr == "Fail" )
  {             
	fm.all('EdorNo').value ="";
	fm.all('PolNo').value="";	  	
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
	//#######################
		//var arrReturn = new Array();
	        //var tSel = PolGrid.getSelNo();
	        //fm.all('EdorNo').value = cEdorNo;
	        //fm.all('PolNo').value = cPolNo;
	//#######################
	//fm.all('EdorNo').value ="";
	//fm.all('PolNo').value="";	
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    fm.all('EdorNo').value ="";//
    fm.all('PolNo').value=cPolNo;	//
    easyQueryClick();
  }
  	
  
}
