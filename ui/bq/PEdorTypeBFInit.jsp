<script language="JavaScript">  

function initForm()
{
  try
  {
    initInpBox();
    initOldPolGrid();
    queryOldPol();
    initNewPolGrid();
    queryNewPol();
  }
  catch(e)
  {
    alert("初始化界面错误，请重新登录!");
  }
}

function initInpBox()
{   
  fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
  fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
  fm.all('ContNo').value = top.opener.fm.all('ContNoBak').value;
  //fm.all('AppntNo').value = top.opener.fm.all('AppntNo').value;
  fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
  //制定汉化
  showOneCodeName("EdorCode", "EdorTypeName");  
}

function initOldPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="80px";         		//列宽
    iArray[1][2]=100;          				//列最大值
    iArray[1][3]=3;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="险种号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="80px";         		//列宽
    iArray[2][2]=100;          				//列最大值
    iArray[2][3]=3;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="险种序号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="60px";         		//列宽
    iArray[3][2]=100;          				//列最大值
    iArray[3][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="险种代码";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="60px";         		//列宽
    iArray[4][2]=100;          				//列最大值
    iArray[4][3]=0;            				//是否允许输入,1表示允许，0表示不允许  
    
    iArray[5]=new Array();
    iArray[5][0]="险种名称";    			//列名
    iArray[5][1]="150px";            	//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="被保人客户号";    			//列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="被保人姓名";    	//列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="保额";    			//列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许  

    iArray[9]=new Array();
    iArray[9][0]="档次";    			//列名
    iArray[9][1]="80px";            	//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[10]=new Array();
    iArray[10][0]="保费";    			//列名
    iArray[10][1]="80px";            	//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="生效日期";    			//列名
    iArray[11][1]="80px";            	//列宽
    iArray[11][2]=100;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="交至日期";    			//列名
    iArray[12][1]="80px";            	//列宽
    iArray[12][2]=100;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[13]=new Array();
    iArray[13][0]="满期日期";    			//列名
    iArray[13][1]="80px";            	//列宽
    iArray[13][2]=100;            			//列最大值
    iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[14]=new Array();
    iArray[14][0]="终止时间";    			//列名
    iArray[14][1]="80px";            	//列宽
    iArray[14][2]=100;            			//列最大值
    iArray[14][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[15]=new Array();
    iArray[15][0]="终止原因";    			//列名
    iArray[15][1]="80px";            	//列宽
    iArray[15][2]=100;            			//列最大值
    iArray[15][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    
    PolGrid = new MulLineEnter("fm", "PolGrid"); 
    PolGrid.mulLineCount = 0;
    PolGrid.displayTitle = 1;
    PolGrid.canSel = 0;
    PolGrid.canChk = 1;
    PolGrid.hiddenSubtraction = 1;
    PolGrid.hiddenPlus = 1;
    PolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}

function initNewPolGrid()
{
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";         			//列宽
    iArray[0][2]=10;          				//列最大值
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="80px";         		//列宽
    iArray[1][2]=100;          				//列最大值
    iArray[1][3]=3;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[2]=new Array();
    iArray[2][0]="险种号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[2][1]="80px";         		//列宽
    iArray[2][2]=100;          				//列最大值
    iArray[2][3]=3;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="险种序号";			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[3][1]="60px";         		//列宽
    iArray[3][2]=100;          				//列最大值
    iArray[3][3]=0;            				//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="险种代码";					//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[4][1]="60px";         		//列宽
    iArray[4][2]=100;          				//列最大值
    iArray[4][3]=0;            				//是否允许输入,1表示允许，0表示不允许  
    
    iArray[5]=new Array();
    iArray[5][0]="险种名称";    			//列名
    iArray[5][1]="150px";            	//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="被保人客户号";    			//列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=100;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="被保人姓名";    	//列名
    iArray[7][1]="80px";            	//列宽
    iArray[7][2]=100;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="原保额";    			//列名
    iArray[8][1]="80px";            	//列宽
    iArray[8][2]=100;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[9]=new Array();
    iArray[9][0]="新保额";    			//列名
    iArray[9][1]="80px";            	//列宽
    iArray[9][2]=100;            			//列最大值
    iArray[9][3]=1;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[10]=new Array();
    iArray[10][0]="原档次";    			//列名
    iArray[10][1]="80px";            	//列宽
    iArray[10][2]=100;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[11]=new Array();
    iArray[11][0]="新档次";    			//列名
    iArray[11][1]="80px";            	//列宽
    iArray[11][2]=100;            			//列最大值
    iArray[11][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[12]=new Array();
    iArray[12][0]="生效时间选择";    			//列名
    iArray[12][1]="80px";            	//列宽
    iArray[12][2]=100;            			//列最大值
    iArray[12][3]=2;              			//是否允许输入,1表示允许，0表示不允许
    iArray[12][4]="ValidateTime"; 
 //   iArray[12][5]="12|13";              	    //引用代码对应第几列，'|'为分割符
 //   iArray[12][6]="1|0";         	 
 //   iArray[12][19]=1;
    
  //  iArray[13]=new Array();
  //  iArray[13][0]="生效时间";    			//列名
  //  iArray[13][1]="80px";            	//列宽
  //  iArray[13][2]=100;            			//列最大值
  //  iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[13]=new Array();
    iArray[13][0]="健康告知";    			//列名
    iArray[13][1]="80px";            	//列宽
    iArray[13][2]=100;            			//列最大值
    iArray[13][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    NewPolGrid = new MulLineEnter("fm", "NewPolGrid"); 
    NewPolGrid.mulLineCount = 0;
    NewPolGrid.displayTitle = 1;
    NewPolGrid.canSel = 0;
    NewPolGrid.canChk = 1;
    NewPolGrid.hiddenSubtraction = 1;
    NewPolGrid.hiddenPlus = 1;
    NewPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);        
  }
}
</script>