<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LDPersonSave.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
//      
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT language="javascript">
var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
</SCRIPT>
<%

  //接收信息，并作校验处理。
  //输入参数
  
  LPEdorMainSet mLPEdorMainSet   = new LPEdorMainSet();
  LPEdorItemSet mLPEdorItemSet =new LPEdorItemSet();
  
  LCInsuredSet tLCInsuredSet=new LCInsuredSet();
  LCPolSet tLCPolSet=new LCPolSet();
   
  TransferData tTransferData = new TransferData(); 
  PEdorAppItemUI tPEdorAppItemUI   = new PEdorAppItemUI();
  //输出参数
  String FlagStr = "";
  String Content = "";
 
  GlobalInput tGI = (GlobalInput) session.getValue("GI"); 

  if(tGI==null)
  { 
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆";  
  }
  else //页面有效
  {
    CErrors tError = null;
    String tBmCert = "";
    System.out.println("aaaa");
    //后面要执行的动作：添加，修改，删除
    String fmAction=request.getParameter("fmtransact");
    System.out.println("fmAction:"+fmAction); 
    String displayType=request.getParameter("DisplayType");
    tTransferData.setNameAndValue("DisplayType",displayType);
    System.out.println("displayType:"+displayType);
     try
    {
        if(fmAction.equals("INSERT||EDORITEM"))
        {
            if (displayType.equals("1"))
            {
                String tContNo[] = request.getParameterValues("LCContGrid1"); //保单号       
                String tGrpContNo[]= request.getParameterValues("LCContGrid10");
                //String tEdorNo[]= request.getParameterValues("LCContGrid8");
                String tRadio[] = request.getParameterValues("InpLCContGridSel");	//单选框（或复选框）的数组
                for(int index=0;index<tRadio.length;index++)
                {
                    if(tRadio[index].equals("1"))
                    {
                        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                        tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
                        //tLPEdorItemSchema.setEdorNo(tEdorNo[index]);
                        //System.out.println("tEdorNo[index]"+tEdorNo[index]);
                        tLPEdorItemSchema.setGrpContNo(request.getParameter("GrpContNo")); 
                        //tLPEdorItemSchema.setEdorAppNo(tEdorNo[index]); 
                        tLPEdorItemSchema.setDisplayType(displayType); 
                        tLPEdorItemSchema.setEdorType(request.getParameter("EdorType")); 
                        tLPEdorItemSchema.setContNo(tContNo[index]); 
                        tLPEdorItemSchema.setInsuredNo("000000");
                        tLPEdorItemSchema.setPolNo("000000");
//                        tLPEdorItemSchema.setManageCom(request.getParameter("ManageCom"));
                        tLPEdorItemSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
                        tLPEdorItemSchema.setEdorAppDate(request.getParameter("EdorAppDate"));
                        mLPEdorItemSet.add(tLPEdorItemSchema); 
                    }           
                }   
             }
             else if (displayType.equals("2"))
             {
                //String tEdorNo = request.getParameter("EdorNo");            //批单号
                String tInsuredNo[] = request.getParameterValues("InsuredGrid1");            //被保险人号码 
                String tContNo[] = request.getParameterValues("InsuredGrid6");            //保单号码                                                                                       
                String tGrpContNo[] = request.getParameterValues("InsuredGrid7");            //集体保单号码
                String tChk[] = request.getParameterValues("InpInsuredGridChk");
                
                for(int index=0;index<tChk.length;index++)
                {
                    if(tChk[index].equals("1"))
                    {
                        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                        tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
                        //tLPEdorItemSchema.setEdorNo(tEdorNo);
                        tLPEdorItemSchema.setGrpContNo(tGrpContNo[index]); 
                        //tLPEdorItemSchema.setEdorAppNo(tEdorNo); 
                        tLPEdorItemSchema.setDisplayType(displayType); 
                        tLPEdorItemSchema.setEdorType(request.getParameter("EdorType")); 
                        tLPEdorItemSchema.setContNo(tContNo[index]); 
                        tLPEdorItemSchema.setInsuredNo(tInsuredNo[index]);
                        tLPEdorItemSchema.setPolNo("000000");
//                        tLPEdorItemSchema.setManageCom(request.getParameter("ManageCom"));
                        tLPEdorItemSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
                        tLPEdorItemSchema.setEdorAppDate(request.getParameter("EdorAppDate"));
                        mLPEdorItemSet.add(tLPEdorItemSchema);
                    }           
                }            
                
             }  
             else if (displayType.equals("3"))
             {
                 String tEdorNo = request.getParameter("EdorNo");            //批单号
                 String tPolNo[] = request.getParameterValues("PolGrid2");                //险种保单号            
                 String tInsuredNo[] = request.getParameterValues("PolGrid1");            //被保险人号码 
                 String tContNo[] = request.getParameterValues("PolGrid7");            //被保险人号码
                 String tGrpContNo[] = request.getParameterValues("PolGrid8");            //集体保单号码
                 
           
                String tChk[] = request.getParameterValues("InpPolGridChk"); 
                for(int index=0;index<tChk.length;index++)
                {
                    if(tChk[index].equals("1"))
                    {
                        LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
                        tLPEdorItemSchema.setEdorAcceptNo(request.getParameter("EdorAcceptNo"));
                        tLPEdorItemSchema.setEdorNo(tEdorNo);
                        tLPEdorItemSchema.setGrpContNo(tGrpContNo[index]);
                        tLPEdorItemSchema.setEdorAppNo(request.getParameter("EdorNo")); 
                        tLPEdorItemSchema.setDisplayType(displayType);  
                        tLPEdorItemSchema.setEdorType(request.getParameter("EdorType")); 
                        tLPEdorItemSchema.setContNo(tContNo[index]); 
                        tLPEdorItemSchema.setInsuredNo(tInsuredNo[index]);
                        tLPEdorItemSchema.setPolNo(tPolNo[index]);
//                       tLPEdorItemSchema.setManageCom(request.getParameter("ManageCom"));
                        tLPEdorItemSchema.setEdorValiDate(request.getParameter("EdorValiDate"));
                        tLPEdorItemSchema.setEdorAppDate(request.getParameter("EdorAppDate"));
                        mLPEdorItemSet.add(tLPEdorItemSchema);
                    }           
                }
                  
             }
                
        }       
   
        // 准备传输数据 VData
         VData tVData = new VData();
         
         tVData.add(mLPEdorItemSet);
         tVData.add(tTransferData);
         tVData.add(tGI);
         
          
         //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
        if ( tPEdorAppItemUI.submitData(tVData,fmAction))
        {
            if (fmAction.equals("INSERT||EDORITEM"))
	        {
	    	    System.out.println("11111------return");
	            	
	    	    tVData.clear();
	    	    tVData = tPEdorAppItemUI.getResult();
	    	    //
	    	    LPEdorItemSet tLPEdorItemSet = new LPEdorItemSet(); 
	          tLPEdorItemSet=(LPEdorItemSet)tVData.getObjectByObjectName("LPEdorItemSet",0);
	        }
	        else if (fmAction.equals("DELETE||PEdorItem"))
	        {
	             %>
	            <SCRIPT language="javascript">
	            	if (mLoadFlag == "TASKFRAME")
					{
	                	parent.fraInterface.fraInterface.EdorItemGrid.delCheckTrueLine (); 
	                }
	                else
	                {
	                	parent.fraInterface.EdorItemGrid.delCheckTrueLine ();
	                }
	            </SCRIPT>
	            <%
	        }
	    }
	    else
	    {
  	    Content = "保存失败，原因是:" + tPEdorAppItemUI.mErrors.getFirstError();
        FlagStr = "Fail";
	    }
    }
    
    catch(Exception ex)
    {
      Content = "保存失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }
  

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tPEdorAppItemUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content ="保存成功！";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = "保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  Content = PubFun.changForHTML(Content);
}
%>                                       
<html>
<script language="javascript">
	if (mLoadFlag == "TASKFRAME")
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
	else
	{
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
</script>
</html>

