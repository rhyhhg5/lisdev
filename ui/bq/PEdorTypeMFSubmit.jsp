<%
	//程序名称：PEdorTypeMFSubmit.jsp
	//程序功能：
	//创建日期：2016-11-23 10:01
	//创建人  ：LC
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>

<%
	String FlagStr = "";
	String Content = "";

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	String transact = request.getParameter("fmtransact");
	GlobalInput tGlobalInput = (GlobalInput) session.getValue("GI");

	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String edorNo = request.getParameter("EdorNo");
	String edorType = request.getParameter("EdorType");
	String contNo = request.getParameter("ContNo");
	//复效生效日期
	String edorAppDate = request.getParameter("EdorAppDate");

	//利率
	String rates = request.getParameter("rates");
	// 	String tContNo[] = request.getParameterValues("PolGrid1");
	//  String tInsuredNo[]= request.getParameterValues("PolGrid1");
	String tPolNo[] = request.getParameterValues("PolGrid11");

	String tChk[] = request.getParameterValues("InpPolGridChk");

	//需要做保全的险种    
	LPPolSet mLPPolSet = new LPPolSet();
	DetailDataQuery tDetailDataQuery = new DetailDataQuery(
			edorAcceptNo, edorType);
	for (int i = 0; i < tChk.length; i++) {
		if (tChk[i].equals("1")) {
			System.out.println(tPolNo[i]);
			mLPPolSet.add((LPPolSchema) tDetailDataQuery.getDetailData(
					"LCPol", tPolNo[i]));
		}
	}
	System.out.print("BEFORE传入复效日期和利率");
	//传入复效日期和利率（利率为0）
	TransferData tTransferData = new TransferData();
	if (rates != null) {
		tTransferData.setNameAndValue("rates", rates);
		tTransferData.setNameAndValue("edorAppDate", edorAppDate);

	}
	System.out.print("\n before 传入保全项目");
	//传入保全项目
	LPEdorItemSchema tLPEdorItemSchema = new LPEdorItemSchema();
	tLPEdorItemSchema.setEdorAcceptNo(edorAcceptNo);
	tLPEdorItemSchema.setEdorNo(edorNo);
	tLPEdorItemSchema.setContNo(contNo);
	tLPEdorItemSchema.setEdorType(edorType);
	//tLPEdorItemSchema.setGetMoney(getMoney);
	//tLPEdorItemSchema.setReasonCode(reason);

	VData tVData = new VData();
	tVData.add(tGlobalInput);
	tVData.add(tLPEdorItemSchema);
	tVData.add(mLPPolSet);
	//	tVData.add(tLPPENoticeSet);
	tVData.add(tTransferData);

	PEdorMFDetailBL tPEdorMFDetailBL = new PEdorMFDetailBL();
	if (!tPEdorMFDetailBL.submitData(tVData, "")) {
		VData rVData = tPEdorMFDetailBL.getResult();
		System.out.println("Submit Failed! "
				+ tPEdorMFDetailBL.mErrors.getErrContent());
		Content = transact + "失败，原因是:"
				+ tPEdorMFDetailBL.mErrors.getFirstError();
		FlagStr = "Fail";
	} else {
		Content = "保存成功";
		FlagStr = "Success";
	}
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

