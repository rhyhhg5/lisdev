//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function edorTypeSCReturn()
{
		initForm();
}

function edorTypeSCSave()
{
  //associateAlert();
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
 	//showSubmitFrame(mDebug);
 
 
  fm.submit();
  //showSubmitFrame(mDebug);

}

function customerQuery()
{	
	window.open("./LCAppntGrpQuery.html");
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initLCAppntGrpGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}

//关联提示函数
function associateAlert()
{
  var strSql = "";
  var grpPolno = fm.all('GrpPolNo').value
  var Grpno = fm.all('GrpNo').value;
  strSql = "select Grppolno from lcgrppol where grppolno <> '" + grpPolno 
            + "' and grpno = '" + Grpno + "'"; 
//  alert(strSql);

  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
//  alert(turnPage.strQueryResult);
  
  //判断是否查询成功
  if (turnPage.strQueryResult) {
 		//查询成功则拆分字符串，返回二维数组
 	 turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
	
     var alertinfo = "相关联的集体保单如下：\n";
     for (var i = 0; i < turnPage.arrDataCacheSet.length; i++) {
     	alertinfo = alertinfo + turnPage.arrDataCacheSet[i][0] + "\n";
     }
     alert(alertinfo);
  }	
  	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
	//alert("ppp");
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
 //	alert(tTransact);
	if (tTransact=="QUERY||MAIN")
	{
	   var iArray;
	   //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  	   turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
	   //保存查询结果字符串
 	   turnPage.strQueryResult  = Result;
  	   //使用模拟数据源，必须写在拆分之前
  	   turnPage.useSimulation   = 1;  
           //查询成功则拆分字符串，返回二维数组
  	   var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
	   turnPage.arrDataCacheSet =chooseArray(tArr,[15,17,103]);
	   fm.all('GrpNo').value = turnPage.arrDataCacheSet[0][0];
	   fm.all('GrpName').value = turnPage.arrDataCacheSet[0][1];
	   var tGrpSpec = Conversion(turnPage.arrDataCacheSet[0][2]);
	   fm.all('GrpSpec').value = tGrpSpec;
        }
	else
	{
	   var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	   showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	   initForm();
	   //进行关联提示	
           //associateAlert();
	}
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];

		// 查询保单明细
		queryAppntGrpDetail();
	}
}
function queryAppntGrpDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tPolNo=fm.all('PolNo').value;
	//alert(tPolNo);
	tCustomerNo = fm.all('CustomerNo').value;
	//alert(tCustomerNo);
	//top.location.href = "./AppntGrpQueryDetail.jsp?EdorNo=" + tEdorNo+"&EdorType="+tEdorType+"&PolNo="+tPolNo+"&CustomerNo="+tCustomerNo;
	parent.fraInterface.fm.action = "./AppntGrpQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./GEdorTypeSCSubmit.jsp";
}
function returnParent()
{
	top.close();
}
