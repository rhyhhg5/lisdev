<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：LCGrpPolQueryDir.jsp
//程序功能：
//创建日期：2002-09-26 11:10:36
//创建人  ：Lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
  //输出参数
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
	
  //保单信息部分
  LCGrpPolSchema tLCGrpPolSchema = new LCGrpPolSchema();
  tLCGrpPolSchema.setGrpPolNo(request.getParameter("GrpPolNo1"));
  System.out.println("LCGrpPolQuery.GrpPolNo:"+tLCGrpPolSchema.getGrpPolNo());
  
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.addElement(tLCGrpPolSchema);
  tVData.addElement(tG);

  // 数据传输
  CGrpPolUI tCGrpPolUI   = new CGrpPolUI();
	if (!tCGrpPolUI.submitData(tVData,"QUERY||MAIN"))
	{
     		 Content = " 查询失败，原因是: " + tCGrpPolUI.mErrors.getError(0).errorMessage;
     		 FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tCGrpPolUI.getResult();
		// 显示
		LCGrpPolSet mLCGrpPolSet = new LCGrpPolSet(); 
		mLCGrpPolSet.set((LCGrpPolSet)tVData.getObjectByObjectName("LCGrpPolBLSet",0));
	
		LCGrpPolSchema mLCGrpPolSchema = mLCGrpPolSet.get(1);
		   	%>
		   	<script language="javascript">
		   	 	parent.fraInterface.fm1.GrpPolNo1.value="<%=mLCGrpPolSchema.getGrpPolNo()%>";
		   		parent.fraInterface.fm.GrpPolNo.value="<%=mLCGrpPolSchema.getGrpPolNo()%>";
		   		parent.fraInterface.fm.all("ContNo").value = "<%=mLCGrpPolSchema.getContNo()%>";
    	 		parent.fraInterface.fm.all("Name").value = "<%=mLCGrpPolSchema.getGrpName()%>";
    	 		parent.fraInterface.fm.all("LinkMan").value = "<%=mLCGrpPolSchema.getLinkMan1()%>";
    	 		parent.fraInterface.fm.all("SignDate").value = "<%=mLCGrpPolSchema.getSignDate()%>";
				parent.fraInterface.fm.all("PaytoDate").value = "<%=mLCGrpPolSchema.getPaytoDate()%>";
    	 		parent.fraInterface.fm.all("Prem").value = "<%=mLCGrpPolSchema.getPrem()%>";
    	 		parent.fraInterface.fm.all("Operator").value = "<%=mLCGrpPolSchema.getOperator()%>";
    	 		parent.fraInterface.fm.all("RiskCode").value = "<%=mLCGrpPolSchema.getRiskCode()%>";
                parent.fraInterface.fm.all("RiskCode1").value = "<%=mLCGrpPolSchema.getRiskCode()%>";
		   	</script>
		<%		
	}
 // end of if  
 
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tCGrpPolUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.emptyUndefined();
	parent.fraInterface.afterSubmit1("<%=FlagStr%>","<%=Content%>");
</script>
</html>