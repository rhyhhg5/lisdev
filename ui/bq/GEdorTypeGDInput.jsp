<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：
//创建日期：2005-11-28
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="GEdorTypeGD.js"></SCRIPT>
	<%@include file="GEdorTypeGDInit.jsp"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<title>管理式医疗保险金额分配 </title>
</head>
<body onload="initForm();" >
  <form action="./GEdorTypeGDSubmit.jsp" method=post name=fm target="fraSubmit">
  <table class="common">
    <tr class="common"> 
      <td class="title">受理号</TD>
      <td class="input"> 
        <input class="readonly" readonly name="EdorNo">
      </td>
      <td class="title">批改类型</TD>
      <td class="input">
      	<input class="readonly" readonly name="EdorType" type="hidden">
      	<input class="readonly" readonly name="EdorTypeName">
      </td>
      <td class="title">集体保单号</TD>
      <td class="input">
      	<input class="readonly" readonly name="GrpContNo">
      </td>
    </tr>
  </table>
  <table>
            <tr>
              <td><IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divSpecialInfo);"></td>
              <td class= titleImg>保单信息</td>
            </tr>
          </table>
          <div id="divSpecialInfo" style="display: '' ">
              </tr>
              <tr class="common">
	             <td class="title">生效日期</td>
	              <td class="input">
	                 <input class="readonly" readonly name="CValiDate" value="">
	              </td>
                <td class="title">团体公共保额</td>
                <td class="input">
                  <input class="readonly" readonly name="GrpAccMoney" value="">
                </td>
                <td class="title">个人总保额</td>
                <td class="input">
                  <input class="readonly" readonly name="AccMoney" value="">
                </td>
              </tr><br/>
              <tr class="common">
                <td class="title">特别约定</td>
                <td class="input" colspan="7">
                  <textarea class="readonly" readonly style="border: none; overflow: hidden; background-color: #F7F7F7;" name="Remark" value="" cols="90%" rows="3"></textarea> 
                </td>
              </tr>
            </table>
          </div>
  <table>
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divLPInsured);">
      </td>
      <td class= titleImg>导入清单</td>
    </tr>
   </table>
    <Div id= "divLPInsured" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanInsuredListGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
      </Div>	
  	</div>	 
   <table>
     <tr>
      <td>
        <IMG src= "../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFail);">
      </td>
      <td class= titleImg>无效数据</td>
    </tr>
   </table>
    <Div id= "divFail" style= "display: ''">
		<table class= common>
			<tr class= common>
				<td text-align: left colSpan=1>
					<span id="spanFailGrid" >
					</span> 
				</td>
			</tr>
		</table>
      <Div  id= "divPage2" align=center style="display: 'none' ">
      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
      </Div>	
  	</div>
    <br>
	<Div  id= "divSubmit" style= "display:''">
	  <table class = common>
			<TR class= common>
	   		 <input type=Button name="importButton" class=cssButton value="导  入"  onclick="importInsured();">
	   		 <input type=Button name="saveButton" class= cssButton value="保  存" onclick="save();">
	   		 <input type=Button name="returnButton" class= cssButton value="返  回" onclick="returnParent();">
			</TR>
	 	</table>
	</Div>
	<input type=hidden id="fmtransact" name="fmtransact"><p align="right"></p>
	<input type=hidden id="ContType" name="ContType">
	<input type=hidden id="ContNo" name="ContNo">
	<input type=hidden id="EdorValiDate" name="EdorValiDate">
	<input type=hidden id="InsuredId" name="InsuredId">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>