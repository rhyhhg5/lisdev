<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GrpNiInvalidSubmit.jsp
//程序功能：把导入清单中的被保人设为无效
//创建日期：2005-01-20
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%
	String FlagStr = "Succ";
	String Content = "";
	
	//从session中得到全局参数
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");	
	
	String grpContNo = request.getParameter("GrpContNo");
	String edorNo = request.getParameter("EdorNo");
	String insuredId = request.getParameter("InsuredId");
	
	LCInsuredListSchema tLCInsuredListSchema = new LCInsuredListSchema();
	tLCInsuredListSchema.setGrpContNo(grpContNo);
	tLCInsuredListSchema.setEdorNo(edorNo);
	tLCInsuredListSchema.setInsuredID(insuredId);
	LCInsuredListSet tLCInsuredListSet = new LCInsuredListSet();
	tLCInsuredListSet.add(tLCInsuredListSchema);
	
	//输入参数
	VData tVData = new VData();
	tVData.add(tGI);
	tVData.add(tLCInsuredListSet);
	
	DeleteInsuredListUI tDeleteInsuredListUI = new DeleteInsuredListUI();
	if (!tDeleteInsuredListUI.submitData(tVData, "INVALID"))
	{
		FlagStr = "Fail";
	}
%> 

<html>
<script language="javascript">
</script>
</html>

