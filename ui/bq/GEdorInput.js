//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var aEdorFlag='0';
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();
var queryFlag = "N";
var arrResult;

function initEdorType(cObj)
{
  if (GrpContGrid.getSelNo() == 0)
  {
    alert("请先选择一个保单！");
    return false;
  }
	//var tRiskCode = fm.all('RiskCode').value;
	var tGrpContNo = fm.all('GrpContNo').value;
	//mEdorType = " 1 ";
	//mEdorType=mEdorType+" and a.riskcode in(select riskcode from lcgrppol where grpcontno=#"+tGrpContNo+"# ";
	//mEdorType=mEdorType+" and SaleChnl in(select #01# from dual where #@#>=#D# union select #02# from dual union select #03# from dual)) ";
	//mEdorType=mEdorType + " and (a.EdorCode=b.EdorCode)"
  //mEdorType=mEdorType+" and (b.EdorPopedom is not null and b.EdorPopedom<=#@#)";
	
	showCodeList('EdorCode',[cObj, fm.EdorTypeName], [0,1], null, tGrpContNo, "1");
}

function actionKeyUp(cObj)
{
	//var tRiskCode = fm.all('RiskCode').value;
	var tGrpContNo = fm.all('GrpContNo').value;
	mEdorType = " 1 ";
	mEdorType=mEdorType+" and a.riskcode in(select riskcode from lcgrppol where grpcontno=#"+tGrpContNo+"# ";
	mEdorType=mEdorType+" and SaleChnl in(select #01# from dual where #@#>=#D# union select #02# from dual union select #03# from dual)) ";
	mEdorType=mEdorType + " and (a.EdorCode=b.EdorCode)"
    mEdorType=mEdorType+" and (b.EdorPopedom is not null and b.EdorPopedom<=#@#)";
	showCodeListKey('EdorCode',[cObj], null, null, mEdorType, "1");
}
/*********************************************************************
 *  校验批改类型输入
 *  参数  ：  无
 *  返回值：  boolean
 *********************************************************************/
 function checkEdorType()
 {
 	var tRiskCode = fm.all('RiskCode').value;
	var tPolNo = fm.all('PolNo').value;
	var tEdorType = fm.all('EdorType').value;
 	var arrCode;
 	var arrCount;
 	var bflag=false;
 	
	mEdorType = " 1 and a.RiskCode=#"+tRiskCode+"# and a.AppObj=#I#" ;
	mEdorType=mEdorType+" and a.riskcode in(select riskcode from lcpol where polno=#"+tPolNo+"# ";
	mEdorType=mEdorType+" and SaleChnl in(select #01# from dual where #@#>=#D# union select #02# from dual union select #03# from dual)) ";
	mEdorType=mEdorType + " and (a.EdorCode=b.EdorCode)"
	mEdorType=mEdorType+" and (b.EdorPopedom is not null and b.EdorPopedom<=#@#)";
        
 	arrCode = searchCode('EdorCode',mEdorType,"1");
 	arrCount=arrCode.length;
	for(i=0;i<arrCount;i++)
	{  
	   if(tEdorType==arrCode[i][0]){
	   	//alert("today_EdorType:" + tEdorType); 
	   	bflag=true;
	   	break;
	   	}
	}
	return bflag;
}

/*********************************************************************
 *  选择批改项目后的动作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterCodeSelect( cCodeName, Field )
{
	try	
	{
		if( cCodeName == "EdorCode" )	
		{
			//			添加关于生效日期字的显示
			if (Field.value == "TY" )
			{
				document.all("ZBdiv").style.display = "";
			}
			CtrlEdorType( Field.value );//loadFlag在页面出始化的时候声明
		}
		
		if (cCodeName == "EdorCode") 
		{
			var edorType = Field.value;
			if (edorType == "AC" || edorType == "BB" || edorType == "BC" || edorType == "IA" || edorType == "CC" || edorType == "SC") 
			{
				fm.all('EdorValiDate').value = fm.all('dayAfterCurrent').value;
			} 
			else if (edorType == "WT" || edorType == "ZT" || edorType == "GT") 
			{
				fm.all('EdorValiDate').value = fm.all('currentDay').value;
			} 
			else 
			{
				fm.all('EdorValiDate').value = fm.all('currentDay').value;
			}
		}
	}
	catch( ex ) {
	}
}
function CtrlEdorType(tEdorType)
{
	tEdorNo=fm.all('EdorNo').value;
			
	if (tEdorNo!=null&&tEdorNo!='')
		divedortype.style.display = '';
	else
		divedortype.style.display = 'none';
			
	easyQueryClick(tEdorType);
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  fm.fmAction.value = "INSERT||EDOR";
  
    if (fm.all('GrpContNo').value==null||fm.all('GrpContNo').value=='')
    {
        alert('请先查询团体保单，再进行申请！');
	    return;    
    }                                                         
    if (fm.all('EdorNo').value!='')
    {
        alert('该保全已经申请，不能重复申请！');
	    return;    
    }
    
   if (fm.all('EdorValiDate').value==null||fm.all('EdorValiDate').value=='') 
   {
        alert('请录入保全生效日期');
        return;
    }

   if (fm.all('EdorAppDate').value==null||fm.all('EdorAppDate').value=='') 
   {
	alert('请录入保全申请日期');
	return;
   }
    fm.all('OtherNo').value = fm.all('GrpContNo').value;
    fm.all('OtherNoType').value = "4"; //团体保单号

    
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(flag, content )
{
  if (showInfo != null)
  {
		showInfo.close();
		top.focus();
  }
  
  if (flag == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
    initGrpEdorItemGrid();
  	getGrpEdorItem();
    if (fm.all('fmAction').value == 'INSERT||GEDORAPPCONFIRM')//保全申请确认
    {
      //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
      //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    	//window.open("./GEdorAppConfirmResult.jsp?LoadFlag=GEDORAPPCONFIRM&EdorNo="+fm.all("EdorNo").value); 
    	var url = "../bq/GEdorAppConfirmResult.jsp?LoadFlag=GEDORAPPCONFIRM&EdorNo=" + fm.all("EdorNo").value;
	    top.document.all("fraInterface").src = url;
    }
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit2(FlagStr, content)
{
	try 
	{
		showInfo.close();
	} 
	catch(re) 
	{
	}
  window.focus();
  if (FlagStr == "Fail" )
  {           
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  {
		var getMoney=HaveFee(fm.EdorAcceptNo.value);
		if(getMoney=="")//查询出错
		{
			return;
		}
		
		//if(Number(getMoney)==0)
		//{
			edorAppAccConfirm();
			return;
		//}
		//else
		//{
		//	var ret1=IFISBalance(fm.EdorAcceptNo.value);//查询是否是定期结算的保单-1表示出错，1表示是定期结算，0非定期
		//	if(ret1==-1)
		//		return;
		//	if(ret1==1)
		//	{
		//		edorAppAccConfirm();
		//		return;
		//	}			
		//	
		//	var customerNo=getCustomerNo(fm.EdorAcceptNo.value);
		//	if(customerNo=="")
		//	{
		//		return;
		//	}
		//	var ret=AppAccExist(customerNo);
		//	if(ret=="")
		//	{
		//		return;
		//	}
		//	if(ret=="No")
		//	{
		//		edorAppAccConfirm();
		//		return;
		//	}
		//	if(ret=="Yes")
		//	{
		//		if(Number(getMoney)>0)
		//		{
		//			var accMoney=AppAccHaveGetFee(customerNo);
		//			if(accMoney=="")
		//			{
		//				return;
		//			}
		//			if(Number(accMoney)>0)
		//			{
		//				var urlStr="BqAppAccConfirmMain.jsp?ContType=1&AccType=0&EdorAcceptNo="+fm.EdorAcceptNo.value+"&CustomerNo="+customerNo+"&DestSource=01";
		//				showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:200px");	
		//			}
		//			edorAppAccConfirm();
		//			return;
		//		}
		//		if(Number(getMoney)<0)
		//		{
		//			var urlStr="BqAppAccConfirmMain.jsp?ContType=1&AccType=1&EdorAcceptNo="+fm.EdorAcceptNo.value+"&CustomerNo="+customerNo+"&DestSource=11";
		//			showInfo=showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:200px");	
		//			edorAppAccConfirm();
		//			return;
		//		}	
		//	}
		//}		
	}
}

function IFISBalance(EdorAcceptNo)
{
	var strSQL ="select coalesce(sum(balintv),0) from lcgrpbalplan where grpcontno=(" +
							"select grpcontno from LPGrpEdorMain where edoracceptno='"+EdorAcceptNo+
							"' and edorno='"+EdorAcceptNo+"')";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询定期结算计划表时出错，保全受理号为"+EdorAcceptNo+"！");
		return -1;
	}
	if(arrResult[0][0]=='0')
	{
		return 0;
	}else
	{
		return 1;
	}
}

//是否有补退费
function HaveFee(EdorAcceptNo)
{
	var strSQL ="select getmoney from LPEdorApp where EdorAcceptNo='"+EdorAcceptNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询保全申请主表时出错，保全受理号为"+EdorAcceptNo+"！");
		return "";
	}
	return arrResult[0][0];
}

//帐户余额是否有可领金额是否大于0
function AppAccHaveGetFee(customerNo)
{
	var strSQL ="select accgetmoney from lcappacc where customerNo='"+customerNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询投保人帐户表时出错，客户号为"+customerNo+"！");
		return "";
	}
	return arrResult[0][0];
}

//投保人帐户是否存在
function AppAccExist(customerNo)
{
	var strSQL ="select count(1) from lcappacc where customerNo='"+customerNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询投保人帐户表时出错！");
		return "";
	}
	if(Number(arrResult[0][0])==0)
	{
		return 'No';
	}
	else
	{
		return 'Yes';
	}
}

function getCustomerNo(edorNo)
{
	var strSQL ="select otherno from LPEdorApp where edoracceptno='"+edorNo+"'";
	arrResult = easyExecSql(strSQL);
	if (arrResult == null) 
  {
		alert("查询保全申请主表时出错！");
		return "";
	}
	return arrResult[0][0];
}

//保全理算后打印
function edorAppAccConfirm()
{
	
	fm.all("fmtransact").value = "INSERT||EDORAPPCONFIRM";	
	fm.action = "./BqGrpAccAfterConfirmSubmit.jsp";
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit1( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
		emptyUndefined();
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	var tTransact = fm.all('fmAction').value;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		divappconfirm.style.display='';
		if (tTransact=="QUERY||EDOR")
		{
			 var tEdorType = fm.all('EdorType').value;
			 CtrlEdorType(tEdorType);
		}
    //alert(FlagStr)
    //执行下一步操作
  }
}




//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在GEdorInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
	aEdorFlag='0';
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  aEdorFlag='1';
  fm.fmAction.value = "INSERT||EDOR";
  initForm();
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  fm.all('fmAction').value="UPDATE||GRPEDORITEM";
  var tActionOld=fm.action;
  fm.action = "../bq/GrpEdorItemSave.jsp"
  fm.submit();
  fm.action=tActionOld;
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  
  fm.all('EdorState').value ='1';

  fm.all('fmAction').value = "QUERY||EDOR";
  window.open("./GEdorQuery.html");
  //alert("query click");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的

}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  if (confirm("您确实想删除该记录吗?"))
  {
  	var i = 0;
   var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
   fm.fmAction.value = "DELETE||EDOR"
   fm.submit(); //提交
   resetForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//在原来申请的基础上增加保全项目
function insertEdorType()
{
	var tEdorNo ;
	tEdorNo = fm.all('EdorNo').value
	if (tEdorNo=='')
			alert("请重新申请!");
	else
	{
		if (window.confirm("是否添加该保全项目?"))
		{
			fm.fmAction.value="INSERT||EDORTYPE";
			if (fm.all('EdorValiDate').value==null||fm.all('EdorValiDate').value=='')
			{
				alert('请录入保全生效日期');
				return;
			}
			if (fm.all('EdorAppDate').value==null||fm.all('EdorAppDate').value=='')
			{
				alert('请录入保全申请日期');
				return;
			}
			if(!checkEdorType())
			{  
				alert("不能添加此项目：项目不存在或你没有权限！");
				return ;
			}
			var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
			fm.submit(); //提交
		}
	}
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	
	var arrResult = new Array();
    	
	if( arrQueryResult != null )
	{
	   
	   //alert("queryFlag"+queryFlag);
	   if(queryFlag==0)
	     { 
	       arrResult = arrQueryResult;
	       fm1.all( 'GrpContNo1' ).value = arrResult[0][0];
	       //var tGrpContNo1=fm1.all( 'GrpContNo1' ).value; 
	       //alert(fm1.all( 'GrpContNo1' ).value);
	       // 查询保单明细
	       queryGrpCont();
	       queryFlag="N";
	       //submitForm1();
             }	    
	   else 
	    {
	       arrResult = arrQueryResult;
	       fm.all( 'EdorAcceptNo' ).value = arrResult[0][0];
         //fm.all( 'EdorNo' ).value = arrResult[0][1];
	       fm.all( 'GrpContNo' ).value = arrResult[0][2];
	       fm1.all( 'GrpContNo1' ).value = arrResult[0][2];
	       // 查询保单明细
	       queryGrpEdorApp();
	       divappconfirm.style.display='';
	    }
	 }
}

/*********************************************************************
 *  根据查询返回的信息查询投保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryEdorDetail()
{
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   
	parent.fraInterface.fm.action = "./GEdorQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./GEdorSave.jsp";
}

function allConfirm()
{
	window.open("./GEdorConfirmQuery.html");
}


//确认整次申请
function edorAppConfirm()
{
	var tEdorNo ;
	tEdorNo = fm.all('EdorNo').value;
	fm.all('EdorState').value ='1';
	//add by chenlei 2008-4-24 未添加保全项目不能进行理算操作
	var strSQL=" select * from lpgrpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"'";
	var arrResult = easyExecSql(strSQL);
	if(!arrResult)
	{
	  alert("未添加保全项目，不能进行保全理算！");	
	  return false;
	}
	//add by chenlei 2008-4-24 本次保全申请下，若有保全项目未录入完毕，不能进行保全理算
	var strSQL=" select 'x' from lpgrpedoritem where edoracceptno='"+fm.all('EdorAcceptNo').value+"' and EdorState='3'";
	var arrResult = easyExecSql(strSQL);
	if(arrResult)
	{
	  alert("本次申请，有保全项目处于未录入状态，不能进行保全理算！");	
	  return false;
	}
	if (tEdorNo=='')
	{
			alert("请重新申请!");
	}
	else
	{		
      if (window.confirm("查看本次变更结果并理算?"))
      {
    	if(!checklLastBalaDate())
    		return false;
        fm.all('fmAction').value = "INSERT||GEDORAPPCONFIRM";	       
        var showStr="正在计算数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        var oldAction=fm.action;
        fm.action='GEdorAppConfirmSubmit.jsp';
        fm.submit();
      }
	}
}
	
	
//Click事件，当点击“查询”图片时触发该函数
function edorQuery()
{
  //下面增加相应的代码
  //window.showModelessDialog("./ReportQuery.html",window,"dialogWidth=15cm;dialogHeight=12cm");
  //var newWindow = window.open("./ReportQuery.html","ReportQuery",'width=700,height=450,top=150,left=190,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
  //newWindow.focus();
  fm.all('EdorState').value ='2';
  window.open("./GEdorQuery.html");
  //alert("query click");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的

}  
// 查询按钮
function easyQueryClick(tEdorType)
{
	//tRiskCode = fm.all('RiskCode').value;
	var tGrpContNo = fm.all('GrpContNo').value;
	//tEdorType = fm.all('EdorType').value;
	
	if (tEdorType==null||tEdorType=='')
	{
				alert("请选择批改项目!");
				return;
	}
	Else
	{
		var strSQL = "";
		strSQL = "select count(*) from lmriskedoritem where riskcode ='000000' and edorcode='"+tEdorType+"' and AppObj='G' and needdetail='1'";		 
		//查询SQL，返回结果字符串
	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
	  
	  //判断是否查询成功
	  if (!turnPage.strQueryResult) 
	  {
	    alert("查询失败！");
	  }
	  else
	  {
		  //查询成功则拆分字符串，返回二维数组
		  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);  	  
			if (turnPage.arrDataCacheSet[0][0]>0)
			{
				//alert(turnPage.arrDataCacheSet[0][0]);
				divdetail.style.display = '';
			}
			else
			{
				//alert(turnPage.arrDataCacheSet[0][0]);
				divdetail.style.display = 'none';
			}
		}
	}
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，显示团体保单明细
 *  参数  ：  parm1, parm2
 *  返回值：  无
 *********************************************************************
 */     
function getGrpContDetail(parm1, parm2)
{
	fm1.all("GrpContNo1").value = fm1.all(parm1).all('GrpContGrid1').value;   
  getMJState();	                	
	queryGrpCont();
	getGrpEdorItem();
}

/*********************************************************************
 *  查询团体保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryGrpCont()
 {
    var tGrpContNo1 = fm1.all('GrpContNo1').value;
    var showStr;
    var urlStr;
    	
    if (tGrpContNo1==null||tGrpContNo1 =='') 
    {
			resetForm();
			queryFlag="0";
			var newWindow = window.open("./GrpPolQueryMainBQ.jsp","GrpPolQueryMainBQ",'width='+screen.availWidth+',height='+screen.availHeight+',top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
			newWindow.focus(); 
    }
    else
    {
			var strSQL= "select a.grpcontno,a.grpname,b.LinkMan1,a.CValiDate,a.prem,a.amnt,a.Peoples2,a.Operator,a.Managecom,a.agentcode,a.AppntNo"
			           + " from lcgrpcont a,LCGrpAddress b where a.grpcontno='"+ tGrpContNo1 + "' and a.appflag='1' and a.AppntNo=b.CustomerNo and a.AddressNo = b.AddressNo";		
			arrResult = easyExecSql(strSQL);   

 	    if (arrResult == null) 
 	    {
 	    	strSQL= "select a.grpcontno,a.grpname,a.grpname,a.CValiDate,a.prem,a.amnt,a.Peoples2,a.Operator,a.Managecom,a.agentcode,a.AppntNo"
                  + " from lcgrpcont a where a.grpcontno='"+ tGrpContNo1 + "' and a.appflag='1'";		
 	    	arrResult = easyExecSql(strSQL); 
 	    	if (!arrResult)
 	    	{
 	    		alert("未查到团体保单号为"+tGrpContNo1+"的团体保单！");
      	}
	    } 
	    else
	    {
 	        try {fm.all('grpcontno').value= arrResult[0][0]; } catch(ex) { };
 	        try {fm.all('grpname').value= arrResult[0][1];fm.GrpName.title=arrResult[0][1]; } catch(ex) { };
 	        try {fm.all('LinkMan1').value= arrResult[0][2]; } catch(ex) { };
 	        try {fm.all('CValiDate').value= arrResult[0][3]; } catch(ex) { };
 	        try {fm.all('prem').value= arrResult[0][4]; } catch(ex) { };
 	        try {fm.all('amnt').value= arrResult[0][5]; } catch(ex) { };
 	        try {fm.all('Peoples2').value= arrResult[0][6]; } catch(ex) { };
 	        try {fm.all('Operator').value= arrResult[0][7]; } catch(ex) { };
 	        try {fm.all('ManageCom').value= arrResult[0][8]; } catch(ex) { };
 	        try {fm.all('AgentCode').value= arrResult[0][9]; } catch(ex) { };
 	        try {fm.all('AppntNo').value= arrResult[0][10]; } catch(ex) {}
 	    }
    }
}

/*********************************************************************
 *  根据客户号查询团体保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryGrpCont2(CustomerNo)
{
	var strSQL;
	strSQL = "select GrpContNo, PrtNo, GrpName, CValiDate, " +
	         "       Peoples2, Prem, Amnt, '' " +  
			     "from   LCGrpCont a " +              
	         "where  appflag = '1' and AppntNo = '" + CustomerNo + "' " +
	         "    and (StateFlag is null or StateFlag not in('0')) " +
	         "    and (state is null or state not in('03030002')) " + //非续保终止
//	         "    and not exists " +             
//	         "      (select 1 from LPEdorEspecialData a, LGWork b " +
//	         "      where a.EdorNo = b.WorkNo and b.ContNo = a.GrpContNo " + 
//	         "        and a.EdorType = 'MJ' " +  
//	         "        and a.DetailType = 'MJSTATE' " +
//	         "        and a.EdorValue != '1' and a.EdorValue != '0' )  "
	         "	order by GrpContNo ";              
	turnPage2.queryModal(strSQL, GrpContGrid);
}

/*********************************************************************
 *  查询团体保全申请信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryGrpEdorApp()
 {
    //查询险种信息
    queryGrpCont();
    //查询保全申请信息
    queryEdorApp();
    //查询批改主表信息
    queryGrpEdorMain();
    //查询保全项目
    getGrpEdorItem();
}

/*********************************************************************
 *  查询团体保全申请信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryEdorApp()
 {
    try
    {
        var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
        var tGrpContNo = fm.all('GrpContNo').value;
        var showStr;
        var urlStr;
        	
        
        //查询保全申请信息
        var strSQL= "select * from LPEdorApp where EdorAcceptNo='"
                    + tEdorAcceptNo+"'";		        
        
        arrResult = easyExecSql(strSQL);    
        if (arrResult == null) 
        {
            alert("未查到保全申请号为"+tEdorAcceptNo+"的保全申请信息！");
        } 
        else
        {
            try {fm.all('EdorAcceptNo').value= arrResult[0][0]; } catch(ex) { }; //保全受理号
            try {fm.all('OtherNo').value= arrResult[0][1]; } catch(ex) { }; //申请号码
            try {fm.all('OtherNoType').value= arrResult[0][2]; } catch(ex) { }; //申请号码类型
            try {fm.all('EdorAppName').value= arrResult[0][3]; } catch(ex) { }; //申请人名称
            try {fm.all('AppType').value= arrResult[0][4]; } catch(ex) { }; //申请方式
        }
    }
    catch(ex)
    {
        alert("在GEdorInput.js-->queryEdorApp函数中发生异常:"+ex);
    } 
}  


/*********************************************************************
 *  查询批改主表信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryGrpEdorMain()
 { 
    var i = 0;
    var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
    var tEdorNo = fm.all('EdorNo').value;
    var showStr;
    var urlStr;
    	
    //查询批改主表信息
    var strSQL= "select * from LPGrpEdorMain where EdorAcceptNo='"
                + tEdorAcceptNo + "' and EdorNo='"+tEdorNo+"'";		    
    
    arrResult = easyExecSql(strSQL);    
    if (arrResult == null) 
    {
        return false;  
    } 
    try {fm.all('ChgPrem').value= arrResult[0][6]; } catch(ex) { }; //变动的保费
    try {fm.all('ChgAmnt').value= arrResult[0][7]; } catch(ex) { }; //变动的保额
    try {fm.all('GetMoney').value= arrResult[0][8]; } catch(ex) { }; //补/退费金额
    try {fm.all('GetInterest').value= arrResult[0][9]; } catch(ex) { }; //补/退费利息
    try {fm.all('EdorAppDate').value= arrResult[0][10]; } catch(ex) { }; //批改申请日期
    try {fm.all('EdorValiDate').value= arrResult[0][11]; } catch(ex) { }; //批改生效日期
    try {fm.all('Operator').value= arrResult[0][11]; } catch(ex) { }; //操作员
       
    return true;
}

/*********************************************************************
 *  查询批改主表信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function queryGrpEdorMain2()
 { 
    var i = 0;
    var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
    	
    //查询批改主表信息
    var strSQL = "select EdorNo, GrpContNo from LPGrpEdorMain " +
                 "where EdorAcceptNo = '" + tEdorAcceptNo + "'";		     
    
    arrResult = easyExecSql(strSQL);    
    if (arrResult == null) 
    {
        return false;  
    }
    try {fm.all('EdorNo').value = arrResult[0][0]; } catch(ex) { };
    try {fm.all('GrpContNo').value = arrResult[0][1]; } catch(ex) { };
    try {fm1.all('GrpContNo1').value = arrResult[0][1]; } catch(ex) { };
    return true;
}

/*********************************************************************
 *  查询保全项目，写入MulLine
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getGrpEdorItem()
{
    var tEdorAcceptNo = fm.all('EdorAcceptNo').value;
    var tEdorNo = fm.all('EdorNo').value;
    //险种信息初始化
	if(tEdorAcceptNo!=null&&tEdorNo!="")
	{
      var strSQL = "select EdorAcceptNo, EdorNo, EdorType, GrpContNo, "
                  + "(select CustomerNo from LCGrpAppnt "
                  + " where GrpContNo = LPGrpEdorItem.GrpContNo), "
                  + " EdorValiDate, "
                  + "	case EdorState "
                  + "		when '1' then '录入完毕' "
                  + "		when '2' then '理算确认' "
                  + "		when '3' then '未录入' "
                  + "		when '4' then '试算成功' "
                  + "		when '0' then '保全确认' "
                  + "	end "
                  + "from LPGrpEdorItem "
                  + "where EdorAcceptNo='" + tEdorAcceptNo + "' "
                  + "	and EdorNo='" + tEdorNo + "' ";
      turnPage.pageLineNum = 100;
    	turnPage.queryModal(strSQL, GrpEdorItemGrid);
	}
	return true;
}

function checkGrpEdorType(){
	var checkSQL = "select riskcode from lcgrppol where grpcontno='"+fm.GrpContNo.value+"' and " +
	" exists( select 1 from lmriskapp where riskcode=lcgrppol.riskcode and startdate>='2013-11-06' ) and " +
	" not exists (select 1 from lmriskedoritem where riskcode=lcgrppol.riskcode and edorcode='"+fm.all('EdorType').value+"') ";
	
	var riskCode = easyExecSql(checkSQL);
	if(riskCode){
		var edorName = easyExecSql("select edorname from lmedoritem where edorcode='"+fm.all('EdorType').value+"' fetch first 1 rows only");
		alert("您的保单投保有险种：'"+riskCode[0][0]+"'，该险种的["+edorName[0][0]+"]保全功能暂未上线，无法添加保全项目！");
		return false;
	}
	return true;
}

/*********************************************************************
 *  添加保全项目
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function addRecord()
{
	
	   //判断保单是否有万能险种
	   if(hasULIRisk(fm.GrpContNo.value)){
		   //解约保全生效日期必须大于或者等于保单最后一次月结日期
		   var checkHaveEdor = "select edoracceptno from lpgrpedoritem where grpcontno='" + fm.GrpContNo.value + "' and exists ( " +
			" select 1 from lpedorapp where edoracceptno=lpgrpedoritem.edoracceptno and edorstate!='0' )  ";		   
			var result = easyExecSql(checkHaveEdor);
			if(result)
			{
				alert("该团险万能的保单下存在其他未结案的保全项目，不能继续操作！");
				return false;
			}
	   }
	   
		if(fm.all('EdorValiDate').value == null || fm.all('EdorValiDate').value == "")
		{
			 alert("保全生效日不能为空!");
			 return false ;
		}
		
    if (fm.all('EdorType').value==null||fm.all('EdorType').value=="")
    {
         alert("请选择需要添加的保全项目");
        return false;    
    }
    
    if(!checkGrpEdorType()){
    	return false;
    }
    
    var mySql = "select customgetpoldate from LCGRPCont where GrpContNo = '" + fm.GrpContNo.value + "'" ;
    var result = easyExecSql(mySql);
    if (result== null || result == "" || result=="null")
    {
	    	if(confirm("保单未进行回执回销，是否继续对保单继续做保全项目？")==false)
			{
				return false;
			}
    }
    //added by suyanlin at 2007-11-19 14:05 start
    //撤销犹豫期撤保处理增加限制提示
    var t1 = new Date(result[0][0].replace(/-/g,"\/")).getTime();   
    var t2 = new Date(fm.EdorValiDate.value.replace(/-/g,"\/")).getTime();
    var tMinus = (t2-t1)/(24*60*60*1000);   
    
		//选择保全生效日期已经过了保单生效日期的反悔期（>10天）
    if(fm.all('EdorType').value=="WT" && tMinus>10 )
    {
		if(confirm("已过十天反悔期，是否继续做撤保处理?")==false)
		{
			return false;
		}
		
		
    } 
    //做过无名单增减人的保单不能犹豫期退保
    if(fm.all('EdorType').value=="WT")
    {
	    var sql = "select 1 from lpgrpedoritem where grpcontno = '"+fm.all('GrpContNo').value+"' and edortype='WJ'";
	    arrResult = easyExecSql(sql);    
	    if (arrResult != null) 
	    {
	    	alert("该集体合同做过无名单减少被保人，不能进行犹豫期退保操作!");
	        return false;  
	    }
	    var sql3 = "select 1 from lmriskapp where riskcode in (select riskcode from lcgrppol where grpcontno = '"+fm.all('GrpContNo').value+"') and risktype4='4'";
	    arrResult3 = easyExecSql(sql3);    
	    if (arrResult3 != null) 
	    {
	    	var sql2 = "select 1 from ljagetendorse where  grpcontno = '"+fm.all('GrpContNo').value+"' and getmoney<>0 fetch first 1 rows only with ur";
		    arrResult2 = easyExecSql(sql2);    
		    if (arrResult2 != null) 
		    {
		    	alert("该团体万能的集体合同做过补退费的保全项目，不能进行犹豫期退保!");
		        return false;  
		    } 
          var sqlLP = "select 1 from llclaimdetail where grpcontno ='"+fm.all('GrpContNo').value+"' fetch first 1 rows only  with ur  ";
          arrResultWTLP = easyExecSql(sqlLP);
          if (!(arrResultWTLP== null || arrResultWTLP == "" || arrResultWTLP=="null"))
           {
           
           alert("团险万能满期保单有过理赔操作,不能做犹豫期退保!");
            
           return false;
           
           }
	    }
		
    } 
   //最后一次续期交费后，做过无名单增减人的保单不能退保
   if(fm.all('EdorType').value=="CT")
   {
	   //定期结算有白条  direct by lc
	   var checkDJSQL = "select 1 from ljagetendorse a where " +
	   		" a.grpcontno='"+fm.GrpContNo.value+"' " +
	   		" and exists (select 1 from ljaget where actugetno=a.actugetno and paymode='B') fetch first 1 rows only with ur";
	   var DJresult = easyExecSql(checkDJSQL);
	   if(DJresult != null){
		   alert("该保单存在未结算工单，不能解约");
		   return false;
	   }
	   
	   var checkDJSQL2 = "select 1 from ljagetendorse a where " +
  			" a.grpcontno='"+fm.GrpContNo.value+"' " +
  			" and exists (select 1 from ljapay where payno=a.actugetno and incometype='B') fetch first 1 rows only with ur";
	   	var DJresult2 = easyExecSql(checkDJSQL2);
	   	if(DJresult2 != null){
	   		alert("该保单存在未结算工单，不能解约");
	   		return false;
	   	}
	   
	   //补充团体医疗产品的解约生效日必须为申请日
	   if(hasTDBCRisk(fm.GrpContNo.value))
	   {
		   if(fm.all('EdorValiDate').value != fm.all('currentDay').value){
			   alert("补充团体医疗保单的解约生效日必须为当前日期");
			   return false;
		   }
	   }
	   
	   //判断保单是否有万能险种
	   if(hasULIRisk(fm.GrpContNo.value)){
		   //解约保全生效日期必须大于或者等于保单最后一次月结日期
		   var checkDateSQL = " select 1 from lcinsureaccclass where " 
		   +" grpcontno='"+fm.GrpContNo.value+"' "
		   +" and polno=(select polno from lcpol where grpcontno=lcinsureaccclass.grpcontno and poltypeflag='2') "
		   +" and baladate<='"+fm.all('EdorValiDate').value+"' with ur ";
		   
			var result = easyExecSql(checkDateSQL);
			if(!result)
			{
				alert("团险万能解约项目的保全生效日期必须大于或者等于保单最后一次月结日期！");
				return false;
			}
	   }
	   
	    var sql = "select 1 from lpgrpedoritem a where grpcontno = '"+fm.all('GrpContNo').value+"' and edortype='WJ'"
	            + " and exists( select 1 from lpedorapp where edoracceptno=a.edorno and  confdate>= (select max(confdate) from ljapaygrp where a.grpcontno=grpcontno )) "
	            ;
	    arrResult = easyExecSql(sql);    
	    if (arrResult != null) 
	    {
	    	alert("该集体合同最后一次续期缴费后做过无名单减少被保人，不能进行退保操作!");
	        return false;  
	    }
				//按照需求增加校验，对于约定交费的团单，只能申请协议退保，不能申请解约。
		
        sql = "select 1 from lcgrpcont where grpcontno='"+fm.all('GrpContNo').value+"'  and payintv = -1 "; 
        arrResult = easyExecSql(sql);
        if ((arrResult!= null && arrResult != "" &&  arrResult !="null"))
         {
         alert("该单缴费频次为约定缴费，不能申请解约保全项目");
         return false;
         
         }
   }    
   
   //定期结算有白条不能进行协议退保 direct by lc 
   if(fm.all('EdorType').value=="XT")
   {
	   //定期结算有白条  direct by lc
	   var checkDJSQL = "select 1 from ljagetendorse a where " +
	   		" a.grpcontno='"+fm.GrpContNo.value+"' " +
	   		" and exists (select 1 from ljaget where actugetno=a.actugetno and paymode='B') fetch first 1 rows only with ur";
	   var DJresult = easyExecSql(checkDJSQL);
	   if(DJresult != null){
		   alert("该保单存在未结算工单，不能协议退保");
		   return false;
	   }
	   
	   var checkDJSQL2 = "select 1 from ljagetendorse a where " +
			" a.grpcontno='"+fm.GrpContNo.value+"' " +
			" and exists (select 1 from ljapay where payno=a.actugetno and incometype='B') fetch first 1 rows only with ur";
	   var DJresult2 = easyExecSql(checkDJSQL2);
	   if(DJresult2 != null){
		   alert("该保单存在未结算工单，不能协议退保");
		   return false;
	   }
   }
    //added by suyanlin at 2007-11-19 14:05 end
    
    var sql = "select state from lcgrpbalplan where grpcontno = '"+fm.all('GrpContNo').value+"'";
    arrResult = easyExecSql(sql);    
    if (arrResult != null&&arrResult[0][0]!="0") 
    {
    	  alert("该集体合同目前正在进行定期结算!不能进行保全操作!");
        return false;  
    }
    
    var sql = "select edorno from lpgrpedoritem a where grpcontno = '"+fm.all('GrpContNo').value+"' and edortype='TZ' and " +
    		" exists ( select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0' )";
    arrResult = easyExecSql(sql);    
    if (arrResult!= null) 
    {
    	  alert("团体万能增人不能和其他保全项目一起操作!");
        return false;  
    }
    
    if (GrpEdorItemGrid.mulLineCount>0)
    {
    var Linecount=parseInt(GrpEdorItemGrid.mulLineCount);
    for(var h=0;h<Linecount;h++)
        {
          if(GrpEdorItemGrid.getRowColDataByName(h,"EdorType")=="JM")
          {
          alert("激活卡客户资料变更不能和其他保全项目一起操作!");
          return false;
          }
          if(GrpEdorItemGrid.getRowColDataByName(h,"EdorType")=="RS")
          {
          alert("保单暂停功能不能和其他保全项目一起操作!");
          return false;
          }
          if(GrpEdorItemGrid.getRowColDataByName(h,"EdorType")=="RR")
          {
          alert("保单恢复功能不能和其他保全项目一起操作!");
          return false;
          }
        }        
    }
          
          if(fm.all('EdorType').value=="RS")
          { 
                var flag=true;
                //是建工险种的不需要配置，可直接添加保单暂停的保全项目
                var checkJianGongXian="select riskcode from lcgrppol where grpcontno='"+fm.GrpContNo.value+"'";
                var arrResult = easyExecSql(checkJianGongXian);
                if(arrResult)
                {
                   
                    for(i=0;i<arrResult.length;i++)
                    {
                       if(arrResult[0][i]=="190106"||arrResult[0][i]=="590206"||arrResult[0][i]=="5901")
                       {
                          var checkJianGongXianQiTa="select 1 from lcgrppol where grpcontno='"+fm.GrpContNo.value
                                                   +"' and riskcode not in ('190106','590206','5901')";
                          var checkJianGongXianQiTaResult = easyExecSql(checkJianGongXianQiTa);
                          if(checkJianGongXianQiTaResult)
                          {
                             alert("该保单不只是含有建工险种，还含有其它险种,无法添加此项目!");
                             return false;
                          }
                       }
                    }
                    flag=false;
                }
	            if(flag)
	             {
	        	    var RSSql = "select 1 from LCGrpEdor where ValidFlag='1' and state='0' and GrpContNo = '" + fm.GrpContNo.value + "'" ;
	        	    var RSresult = easyExecSql(RSSql);
	        	    if (RSresult== null || RSresult == "" || RSresult=="null")
	        	    {
	        	    	alert("该保单未在配置表中配置,或者该保单已经暂停期满,无法添加此项目!");
	        			return false;
	        	    }
        	    }
          }
          var IsRSsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and ((enddate is null) or (startdate<=current date  and enddate>current date )) and grpcontno = '"+fm.all('GrpContNo').value+"'";
          var IsRSarrResult = easyExecSql(IsRSsql);    
          if (!(IsRSarrResult== null || IsRSarrResult == "" || IsRSarrResult=="null")) 
          {
        	  if(fm.all('EdorType').value!="RR")
        	  {
          	  alert("该集体合同目前正处于保单暂停状态!不能添加保单恢复外的任何保全操作!");
              return false;  
        	  }
          }
          else{
        	  if(fm.all('EdorType').value=="RR")
        	  {
          	  alert("该集体合同目前未处于保单暂停状态!不能添加保单恢复的保全操作!");
              return false;  
        	  }
          }
          
          
//	校验保全生效日是否在暂停期
          var IsRSRRsql = "select 1 from lcgrpcontstate where statetype='Stop' and statereason='01' and state='1' and enddate is not null and startdate<='"+fm.all('EdorValiDate').value+"'  and enddate>'"+fm.all('EdorValiDate').value+"'  and grpcontno = '"+fm.all('GrpContNo').value+"'";
          var IsRSRRResult = easyExecSql(IsRSRRsql);    
          if (!(IsRSRRResult== null || IsRSRRResult == "" || IsRSRRResult=="null")) 
          {
          	  alert("生效日期不可选在该集体合同暂停区间内!");
              return false;  
          }
          
          
          
    
    //add by xp 2009-2-4 特需医疗险种的团单增减人不可同一个工单来做
    sql = "select 1 from lcgrppol a where  grpcontno ='"+fm.all('GrpContNo').value+"' and exists (select 1 from lmriskapp where riskcode=a.riskcode and risktype3='7')";
    arrResult = easyExecSql(sql);
    if (arrResult&&GrpEdorItemGrid.mulLineCount>0)
    {
    var Linecount=parseInt(GrpEdorItemGrid.mulLineCount);
    for(var j=0;j<Linecount;j++)
        {        
          if(GrpEdorItemGrid.getRowColDataByName(j,"EdorType")=="NI"&&(fm.all('EdorType').value=="ZT"))
          {
          alert("特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行减人操作");
          return false;
          }
          if(GrpEdorItemGrid.getRowColDataByName(j,"EdorType")=="ZT"&&(fm.all('EdorType').value=="NI"))
          {
          alert("特需医疗险种的团单增人和减人项目不可在同一个工单进行,请新建工单进行增人操作");
          return false;
          }
        }        
    }
    
  if(fm.all('EdorType').value!="AC")
  {
    //add by xp 091201 添加对于卡折业务类型保单的保全校验
    sql = "select 1 from lcgrpcont where cardflag ='2' and grpcontno ='"+fm.all('GrpContNo').value+"'";
    arrResult = easyExecSql(sql);
    if (!(arrResult== null || arrResult == "" || arrResult=="null"))
    {
    	var wrapsql = "select riskwrapcode from lccontplanrisk where grpcontno = '"+fm.all('GrpContNo').value+"' and riskwrapcode is not null fetch first 1 rows only";
    	var wrapResult = easyExecSql(wrapsql);
    	if(!(wrapResult== null || wrapResult == "" || wrapResult=="null")){
    		var risksql = "select 1 from lmriskedoritem where edorcode = '"+fm.all('EdorType').value+"' and riskcode ='"+wrapResult+"'";
        	var riskResult = easyExecSql(risksql);
        	if((riskResult== null || riskResult == "" || riskResult=="null")){
        		sql = "select 1 from lccont where  grpcontno='"+fm.all('GrpContNo').value+"'  and exists (select 1 from licertify where prtno=lccont.prtno and activeflag='02')"; 
    	    	arrResult = easyExecSql(sql);
    	    	if(!(arrResult== null || arrResult == "" || arrResult=="null"))
    	    	{
    	    	  alert("该保单为激活卡保单,不允许操作任何保全项目");
    	       	  return false;
    	    	}
    	    	else
    	    	{
    	    		if(!((fm.all('EdorType').value=="AD")||(fm.all('EdorType').value=="CM")))
    	            {
    	              alert("卡折业务的保单只能操作客户资料变更或者联系方式变更的保全项目");
    	              return false;
    	            }
    	    	}
        	}
    	} else {
	    	sql = "select 1 from lccont where  grpcontno='"+fm.all('GrpContNo').value+"'  and exists (select 1 from licertify where prtno=lccont.prtno and activeflag='02')"; 
	    	arrResult = easyExecSql(sql);
	    	if(!(arrResult== null || arrResult == "" || arrResult=="null"))
	    	{
	    	  alert("该保单为激活卡保单,不允许操作任何保全项目");
	       	  return false;
	    	}
	    	else
	    	{
	    		if(!((fm.all('EdorType').value=="AD")||(fm.all('EdorType').value=="CM")))
	            {
	              alert("卡折业务的保单只能操作客户资料变更或者联系方式变更的保全项目");
	              return false;
	            }
	    	}
    	}
    }    
 }
  //添加对 AC不可和NI、ZT同时做的校验，杨天政 20110711 
  var tEdorTypeToUseSwitch_20110711 = fm.all('EdorType').value;
  if(tEdorTypeToUseSwitch_20110711=="AC")
    {
      sql = "select 1 from lpgrpedoritem where  edorno='"+fm.all('EdorNo').value+"'"; 
      arrResult_notAC_20110711 = easyExecSql(sql);
      if (!(arrResult_notAC_20110711== null || arrResult_notAC_20110711 == "" || arrResult_notAC_20110711=="null"))
       {
       
       alert("其他保全项目不可以和投保单位资料变更一起做。");
        
       return false;
       
       } 
    }
   else
    {
      sql = "select 1 from lpgrpedoritem where edortype='AC' and edorno='"+fm.all('EdorNo').value+"'"; 
      arrResult_notAC_20110711 = easyExecSql(sql);
      if (!(arrResult_notAC_20110711== null || arrResult_notAC_20110711 == "" || arrResult_notAC_20110711=="null"))
       {
       
       alert("投保单位资料变更不可以和其他保全一起做。");
        
       return false;
       
       } 
   
    }

 //添加对CM不可和NI、ZT同时做的校验，杨天政 20111222 
  var tEdorTypeToUseSwitch_20110711 = fm.all('EdorType').value;
  if(tEdorTypeToUseSwitch_20110711=="CM")
    {
      sql = "select 1 from lpgrpedoritem where  edorno='"+fm.all('EdorNo').value+"'"; 
      arrResult_notAC_20110711 = easyExecSql(sql);
      if (!(arrResult_notAC_20110711== null || arrResult_notAC_20110711 == "" || arrResult_notAC_20110711=="null"))
       {
       
       alert("其他保全项目不可以和团体客户资料变更一起做。");
        
       return false;
       
       } 
    }
   else
    {
      sql = "select 1 from lpgrpedoritem where edortype='CM' and edorno='"+fm.all('EdorNo').value+"'"; 
      arrResult_notAC_20110711 = easyExecSql(sql);
      if (!(arrResult_notAC_20110711== null || arrResult_notAC_20110711 == "" || arrResult_notAC_20110711=="null"))
       {
       
       alert("客户资料变更不可以和其他保全一起做。");
        
       return false;
       
       } 
   
    }

//    sql = "select 1 from lcgrpcont where cardflag ='0' and grpcontno ='"+fm.all('GrpContNo').value+"'";
//    arrResult = easyExecSql(sql);
//    if (arrResult)
//    {
//    	alert("该保单为简易保单");
//    }
        var sql = "select 1 from lpgrpedoritem a where grpcontno='"+fm.all('GrpContNo').value+"' and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and edortype='TY' "; 
        var arrResult = easyExecSql(sql);
        if (arrResult!= null && arrResult != "" && arrResult !="null")
         {
         alert("该保单下还有未结案的追加保费保全项目,请结案后再操作其他保全项目!");
         return false;
         } 
        
        //添加对 AC不可和NI、ZT同时做的校验，杨天政 20110711 
        var tEdorTypeUW = fm.all('EdorType').value;
        if(tEdorTypeUW=="UM")
          {
            sql = "select 1 from lpgrpedoritem where  edorno='"+fm.all('EdorNo').value+"'"; 
            arrResultUM = easyExecSql(sql);
            if (!(arrResultUM== null || arrResultUM == "" || arrResultUM=="null"))
             {
             
             alert("其他保全项目不可以和团险万能满期一起做。");
              
             return false;
             
             }
            sql = "select 1 from lpgrpedoritem a where 1=1 and exists (select 1 from lpedorapp where edoracceptno=a.edorno and edorstate!='0') and grpcontno='"+fm.all('GrpContNo').value+"' and edorno!='"+fm.all('EdorNo').value+"'"; 
            arrResultUMO = easyExecSql(sql);
            if (!(arrResultUMO== null || arrResultUMO == "" || arrResultUMO=="null"))
             {
             
             alert("其他保全项目不可以和团险万能满期一起做。");
              
             return false;
             
             }
            
//            sql = "select 1 from llcase where customerno in (select insuredno from lcinsured where grpcontno='"+fm.all('GrpContNo').value+"')" + "  and rgtstate not in('11','12','14') with ur  ";
//            arrResultUMIN = easyExecSql(sql);
//            if (!(arrResultUMIN== null || arrResultUMIN == "" || arrResultUMIN=="null"))
//             {
//             
//             alert("团险万能满期保单存在理赔!");
//              
//             return false;
//             
//             }
          }
         else
          {
            sql = "select 1 from lpgrpedoritem where edortype='UM' and edorno='"+fm.all('EdorNo').value+"'"; 
            arrResult_UM = easyExecSql(sql);
            if (!(arrResult_UM== null || arrResult_UM == "" || arrResult_UM=="null"))
             {
             alert("团险万能满期不可以和其他保全一起做。");
             return false;
             }
          }
        
  	if(fm.all('EdorType').value=="ZF"){

        var sql = "select 1 from lcgrpcont where grpcontno='"+fm.all('GrpContNo').value+"' and APPFLAG='1' and stateflag='1' and signdate is not null "; 
        var arrResult = easyExecSql(sql);
        if ((arrResult== null || arrResult == "" || arrResult =="null"))
         {
         
         alert("该保单为非承保有效保单，不能做终止缴费项目");
          
         return false;
         
         } 
        
        sql = "select 1 from lcgrpcont where grpcontno='"+fm.all('GrpContNo').value+"'  and payintv <> 0 "; 
        arrResult = easyExecSql(sql);
        if ((arrResult== null || arrResult == "" || arrResult =="null"))
         {
         
         alert("只有缴费频次为期缴或“约定缴费”的保单，才能申请终止缴费项目");
          
         return false;
         
         }
        
        sql = "select max(dealstate) from ljspayb where otherno='"+fm.all('GrpContNo').value+"' and dealstate not in ('1','2','6') "; 
        arrResult = easyExecSql(sql);
        if ((arrResult!= null && arrResult != "" && arrResult !="null"))
         {
        	if(arrResult[0][0]=="0")
        		alert("该保单续期状态为已抽档待收费，不能做终止缴费项目");
        	else if (arrResult[0][0]=="4")
        		alert("该保单续期状态为已收费待核销，不能做终止缴费项目");
        	else
        		alert("保单处在续期中间状态下，不能申请终止缴费项目");
        	return false;
         
         }
        
        sql = "select 1 from lcgrpcont where grpcontno='"+fm.all('GrpContNo').value+"'  and state = '03050002' "; 
        arrResult = easyExecSql(sql);
        if ((arrResult!= null && arrResult != "" && arrResult !="null"))
         {
         
         alert("满足上述条件的团单只要申请终止缴费结案后，不能再次申请终止缴费");
          
         return false;
         
         }
      
  		
  	}
  	//add by lzy20151118团体公共保额分配的生效日期必须为当天
  	if(fm.all('EdorType').value=="GD"){
  		if(fm.all('EdorValiDate').value != fm.all('currentDay').value){
  			alert("管理式医疗保险金额分配的保全生效日期必须为当前日期！");
  			return false;
  		}
  	}
  	
  	//团体医疗追加保费的生效日期必须为当天
  	if(fm.all('EdorType').value=="ZA" || fm.all('EdorType').value=="ZE"){
  		if(fm.all('EdorValiDate').value != fm.all('currentDay').value){
  			alert("管理式医疗追加保费和减少保额的保全生效日期必须为当前日期！");
  			return false;
  		}
  	}
    if(fm.all('EdorType').value=="ZT")
    {
 	   //管理式医疗产品减人生效日必须为申请日
 	   if(hasTDBCRisk(fm.GrpContNo.value))
 	   {
 		   if(fm.all('EdorValiDate').value != fm.all('currentDay').value){
 			   alert("管理式医疗保单的减少被保人保全生效日必须为当前日期");
 			   return false;
 		   }
 	   }
    }
  	
  		/*保单下有未结案理赔案件或者有未回销的预付赔款（保单预付赔款余额不为0）时
  	保全不能进行结余返还，待理赔案件处理完毕，没有未回销预付赔款时才能做结余返还。*/
  	if(fm.all('EdorType').value=="BJ"){
  		var sql_pay = "select sum(b.realpay) from " 
  				 +"llcase a,llclaimdetail b where "
  				 +" a.rgtno = b.rgtno "
  				 +" and a.caseno = b.caseno "
  				 +" and a.RgtState not in ('09','11','12','14') "
  				 +" and b.grpcontno = '"+fm.all('GrpContNo').value+"' "
  				 +" group by grpcontno ";
  		
  		var sql_bala = "select PrepaidBala from " 
  				+" LLPrepaidGrpCont where "
  				+" PrepaidBala > 0 "
  				+" and state = '1' "
  				+" and grpcontno = '"+fm.all('GrpContNo').value+"'";
  		
  		var pay_result = easyExecSql(sql_pay);
  		
  		var bala_result = easyExecSql(sql_bala);
  		
  		if(pay_result!=null||bala_result!=null){
		  	if(bala_result==null){
		  			var bala = 0.0;
		  		}else{
		  			bala = bala_result[0][0];
		  		}
		  		if(pay_result==null){
		  			var pay = 0.0; 
		  		}else{
		  			pay = pay_result[0][0];
		  		}
		  		alert("该团单未结案赔款"+pay+"元，未回销预付赔款"+bala+"元，无法添加此保全项目。因“未结案赔款”“未回销预付赔款”数据实时更新，请及时根据提示内容进行核实后进行操作。");
		  		return false;
	  		}
  		}
  	
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.all('fmAction').value="INSERT||GRPEDORITEM";
    var tActionOld=fm.action;
    fm.action = "../bq/GrpEdorItemSave.jsp"
    fm.submit();
    fm.action=tActionOld;
}

//判断团单是否有万能险种
function hasULIRisk(grpContNo)
{
//	var sql = "select * from LMRiskApp where RiskCode in (select RiskCode from LCPol where ContNo='"+ContNo+"') and RiskType4='4'";
	var sql = "select 1 from LMRiskApp as R,LCgrpPol as P where R.RiskCode=P.RiskCode and  grpContNo='"+grpContNo+"' and RiskType4='4' with ur ";
	var result = easyExecSql(sql);
	if(result)
	{
		return true;
	}
	return false;	
}

//补充团体医疗保单
function hasTDBCRisk(grpContNo)
{
	var sql = "select 1 from LCgrpPol where grpContNo='"+grpContNo+"' and riskcode in (select code from ldcode where codetype='tdbc') with ur ";
	var result = easyExecSql(sql);
	if(result)
	{
		return true;
	}
	return false;	
}


/*********************************************************************
 *  删除保全项目
 *  参数  ：  无
 *  返回值：  无
 *  add by Lanjun 2005-5-24 14:30 PICC v1.1
 *********************************************************************
 */
function deleteRecord()
{
	var tSelNo = GrpEdorItemGrid.getSelNo()-1;
	
	if (tSelNo==null||tSelNo < 0)
	{
	alert("请选择需要删除的项目！");
	return false;
	}
	
	//保全锁20110614
	var edorAcceptNo_1 = GrpEdorItemGrid.getRowColData(tSelNo,2);
   var findSql="select serialno from LCUrgeVerifyLog where serialno='"+edorAcceptNo_1+"' ";
   var arrResult_1 = easyExecSql(findSql);
   if( arrResult_1)
   {
   alert("正在保全确认或者重复理算操作，不能删除保全项目！");
   return false;
   }
	// 当时新人修改，检查的责任。 
	//added by suyanlin at 2007-11-16 17:12 start
	//犹豫期撤保处理撤销
 // if (fm.all('EdorType').value=="WT")//保单犹豫期退保
 // {
 // 	  var sql = "select cvalidate from lccont "
 // 			  + "where grpcontno = '" + fm.GrpContNo.value + "' "
 //			var result = easyExecSql(sql);
 // 
 //     alert("请选择需要添加的保全项目");
 //     return false;    
 // }
  
	
	//added by suyanlin at 2007-11-16 17:12 end
	
	var state= GrpEdorItemGrid.getRowColData(tSelNo,5);
	//alert(state);
	 
	fm.all('hEdorNo').value = GrpEdorItemGrid.getRowColData(tSelNo,2);
	fm.all('hGrpContNo').value=GrpEdorItemGrid.getRowColData(tSelNo,4);
	fm.all('hEdorType').value=GrpEdorItemGrid.getRowColData(tSelNo,3);
	//fm.all('hEdorState').value=GrpEdorItemGrid.getRowColData(tSelNo,6);
	//fm.all('MakeDate').value=GrpEdorItemGrid.getRowColData(tSelNo,7);
	//fm.all('MakeTime').value=GrpEdorItemGrid.getRowColData(tSelNo,8);			
	fm.all("Transact").value = "G&EDORITEM";
	fm.all("fmAction").value = "";
	fm.all("DelFlag").value="1";  //flag 为1 删除项目
	var tActionOld=fm.action;
	fm.action="./GEdorAppCancelSubmit.jsp";
	fm.submit();
	fm.action=tActionOld;
}

/*********************************************************************
 *  MulLine的RadioBox点击事件，显示项目明细按钮
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */       
function getEdorItemDetail(parm1,parm2)
{
    fm.EdorType.value = fm.all(parm1).all('GrpEdorItemGrid3').value;
    fm.EdorTypeName.value = getEdorTypeName(fm.EdorType.value);
    fm1.GrpContNo1.value = fm.all(parm1).all('GrpEdorItemGrid4').value;
    queryGrpCont();
}

//得到项目名
function getEdorTypeName(edorType)
{
	var sql = "  select edorName "
	          + "from LMEdorItem "
	          + "where appObj = 'G' "
	          + "   and edorCode = '" + edorType + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    return rs[0][0];
  }
  else
  {
    return "";
  }
}

function edorDetail()
{
  if(!checkNIState())
  {
    return false;
  }
  
  if (GrpEdorItemGrid.getSelNo() == 0)
  {
    alert("请选择一个保全项目！");
    return false;
  }
  detailEdorType();
}
// modify bu fuxin 2008-5-21 14:16:02 特需已经产生满期结算工单做保全项目时给出提示。    
function getMJState()
{

	var tGrpContNo1 = fm1.all('GrpContNo1').value;
	var strSQL ;
		strSQL = "select  'x'  " +
			     "from   LCGrpCont a " +              
	         "where  appflag = '1' and GrpContNo = '" + tGrpContNo1 + "' " +
	         "    and (StateFlag is null or StateFlag not in('0')) " +
	         "    and (state is null or state not in('03030002')) " + //非续保终止
	         "    and  exists " +             
	         "      (select 1 from LPEdorEspecialData a, LGWork b " +
	         "      where a.EdorNo = b.WorkNo and b.ContNo = a.GrpContNo " + 
	         "        and a.EdorType = 'MJ' " +  
	         "        and a.DetailType = 'MJSTATE' " +
	         "        and a.EdorValue != '1' and a.EdorValue != '0' )  "
	         ;    
	var arrResult = easyExecSql(strSQL);
	if(arrResult)
	{
		confirm("该保单已经有满期结算工单，是否继续操作？");
	}
}

//20090915 zhanggm 校验增人理算做到中途的情况，如果理算没有完成，则提示“本次增人已经进行理算操作，
//请点【保全理算】按钮完成本次操作。”。如果需要修改明细录入的内容或重新导入模板，可以理算完成后再理算回退，
//或者把本次增人的保全项目删除后重新添加。优点是不对业务操作的流程进行修改，缺点是操作比较繁琐。
function checkNIState()
{
  var tEdorType = fm.all('EdorType').value;
  var tEdorNo=fm.all('EdorNo').value;
  if(tEdorType != "NI")
  {
    return true;
  }
  var sql = "select 1 from lccont where appflag in ('0','2') "
          + "and contno in (select contno from lcinsuredlist where edorno = '" + tEdorNo + "' and state = '1') "; 
  var arrResult = easyExecSql(sql);
  if(arrResult)
  {
    alert("本次增人已经进行理算操作！\n请点【保全理算】按钮完成本次操作，或者把本次增人的保全项目删除后重新添加。");
    return false;
  }
  return true;
}
function checklLastBalaDate(){
	var edorType = GrpEdorItemGrid.getRowColDataByName(0,"EdorType");
	if(edorType == "CT"){
		var qGrpContNo = "select grpcontno from lpgrpedoritem where edorno = '"+fm.EdorAcceptNo.value+"'";
		var GrpContNo = easyExecSql(qGrpContNo);
		if(hasULIRisk(GrpContNo)){
			var sql = "select baladate from lcinsureaccclass where contno=(  select contno from lcpol where grpcontno = '"+ GrpContNo +"' and poltypeflag='2' fetch first 1 rows only )  fetch first 1 rows only with ur ";
			var baladate = easyExecSql(sql);
			var EdorValiDate = easyExecSql("select EdorValiDate from lpgrpedoritem where grpcontno = '"+GrpContNo+"' and edortype = 'CT'");
			var compareSql = " select 1 from lcinsureaccclass where " 
				   +" grpcontno='"+GrpContNo+"' "
				   +" and polno=(select polno from lcpol where grpcontno=lcinsureaccclass.grpcontno and poltypeflag='2') "
				   +" and baladate<to_date('"+EdorValiDate+"','yyyy-mm-dd') - 1 months with ur ";
			var result = easyExecSql(compareSql);
			if(result){
				if(confirm("本月及上月尚未公布结算利率，如果现在退保，本月及上月的利息只能按条款规定的最低保证利率结息，退保金额可能会低于客户预期。请确认是否仍然操作退保。")){
					return true;
				}else{
					return false;
				}
			}
		}
	}
	return true;
}
