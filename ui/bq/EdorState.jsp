<%
//程序名称：EdorAppQuery.jsp
//程序功能：保全受理申请报表
//创建日期：2010-02-02
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.db.*" %>
<%@page import="com.sinosoft.lis.schema.*" %>
<%@page import="com.sinosoft.lis.vschema.*" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
    GlobalInput tG1 = (GlobalInput)session.getValue("GI");
    String Branch =tG1.ComCode;
%>



<html>    
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
  

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>个险保全工作情况</title>
  
   <SCRIPT src="./EdorState.js"></SCRIPT>   
  <%@include file="./EdorStateInit.jsp"%>   
  
</head>      
 
<body  onload="initForm();" >
  <form method=post action=./EdorStatePrint.jsp name=fm>
   <Table class= common>
     <TR class= common> 
          <TD  class= title>管理机构</TD>
          <TD  class= input><Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >
          <TD  class= title>批单状态</TD>
         <TD  class= input><Input class="codeNo" name="EdorState" ondblclick="return showCodeList('appedorstate',[this,EdorStateName],[0,1]);" onkeyup="return showCodeListKey('appedorstate',[this,EdorStateName],[0,1]);"><Input class="codeName" name="EdorStateName"  elementtype="nacessary" readonly></TD>
     </TR>
     
     <TR  class= common>
        <TD  class= title width="25%">交费结束结案开始日期</TD>
       	<TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=modifydateBegin verify="交费结束结案开始日期|NOTNULL"></TD>
		<TD  class= title width="25%">交费结束结案结束日期</TD>
       	<TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=modifydateEnd verify="交费结束结案结束日期|NOTNULL"></TD>
     </TR>
   	</Table>  
   	<p>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="strsql" name="strsql">
    <!--数据区-->
    <INPUT VALUE="查  询" class= cssbutton TYPE=button onclick="easyQuery()"> 	
	<INPUT VALUE="打  印" class= cssbutton TYPE=button onclick="easyPrint()">
  	<Div  id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
					<span id="spanCodeGrid" ></span> 
		  		</td>
			</tr>
    	</table>
		<center>
      	<INPUT VALUE="首  页" class = cssbutton TYPE=button onclick="CodeGridturnPage.firstPage(); "> 
        <INPUT VALUE="上一页" class = cssbutton TYPE=button onclick="CodeGridturnPage.previousPage(); "> 				
        <INPUT VALUE="下一页" class = cssbutton TYPE=button onclick="CodeGridturnPage.nextPage(); "> 
        <INPUT VALUE="尾  页" class = cssbutton TYPE=button onclick="CodeGridturnPage.lastPage(); ">
		</center>
  	</div>  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 	
<script>
	<!--选择机构：只能查询本身和下级机构-->
	var codeSql = "1  and code like #"+<%=Branch%>+"%#";
</script>