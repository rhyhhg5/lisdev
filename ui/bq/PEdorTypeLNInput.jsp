<html>
	<%
		//程序名称：
		//程序功能：个人保全
		//创建日期：2011-10-31
		//创建人  ：XP
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="./PEdor.js"></SCRIPT>
		<SCRIPT src="./PEdorTypeLNInput.js"></SCRIPT>
		<%@include file="../common/jsp/UsrCheck.jsp"%>
		<%@include file="PEdorTypeLNInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form action="./PEdorTypeLNSubmit.jsp" method=post name=fm
			target="fraSubmit">
			<table class=common>
				<tr class=common>
					<td class="title">
						受理号
					</td>
					<td class=input>
						<input class="readonly" type="text" readonly name="EdorNo">
					</td>
					<td class="title">
						批改类型
					</td>
					<td class="input">
						<input class="readonly" type="hidden" readonly name="EdorType">
						<input class="readonly" readonly name="EdorTypeName">
					</td>
					<td class="title">
						保单号
					</td>
					<td class="input">
						<input class="readonly" readonly name="ContNo">
					</td>
				</tr>
				<tr class=common>
					<td class="title">
						当前贷款利率
					</td>
					<td class=input>
						<input class="readonly" type="text" readonly name="Rate">
					</td>
					<td class="title">
						贷款起始日期
					</td>
					<td class="input">
						<input class="readonly" type="text" readonly name="LoanDate">
					</td>
					<td class="title">
						规定还款日期
					</td>
					<td class="input">
						<input class="readonly" type="text" readonly name="PayOffDate">
					</td>
				</tr>
			</table>
			<Div id="divPolInfo" style="display: ''">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divPolGrid);">
						</td>
						<td class=titleImg>
							保单险种信息
						</td>
					</tr>
				</table>
				<Div id="divPolGrid" style="display: ''">
					<table class=common>
						<tr class=common>
							<td text-align: left colSpan=1>
								<span id="spanPolGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
			</DIV>
			<br>
			<div>
				<Input type=button name="cal" class=cssButton value="计算贷款信息"
					onclick="CalLoanInfo()">
				<br>
				<br>
				<Input type=button name="save" class=cssButton value="保  存"
					onclick="edorTypeLNSave()">
				<Input type=Button name="goBack" class=cssButton value="取  消"
					onclick="edorTypeLNReturn()">
				<Input type=Button name="goBack1" class=cssButton value="返  回"
					onclick="edorTypeLNReturn()">
			</div>
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="ContType" name="ContType">
			<input type=hidden id="XQFlag" name="XQFlag">
			<input type=hidden id="Prem" name="Prem">
			<input type=hidden id="PolNo" name="PolNo">
			<input type=hidden id="LoanMoney" name="LoanMoney">
			<input type=hidden name="EdorAcceptNo">
			<input type="hidden" name = "RiskCode"/>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
