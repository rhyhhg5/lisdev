//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var organcode="";


//校验录入数据的合法性
function checkData()
{
	if(fm.StartDate.value == null || fm.StartDate.value == "" 
		|| fm.EndDate.value == null||fm.EndDate.value=="")
	{
		alert("统计起止日期不能为空");
		return false;
	}
	
}


/*********************************************************************
 *  上下级机构判断
 *  参数  ：  pManageCom String : 管理机构
 *  返回值：  true 可查看该机构保单；false 不可查看
 *********************************************************************

 */
function IsValiCom(pManageCom)
{


	if (comCode =='86' || comCode =='86000000' ) return true;
	if ((pManageCom =='86' || pManageCom =='86000000') && (comCode =='86' || comCode =='86000000') ) return true;
	
	if (pManageCom.length >4 && comCode.substring(0,4) == pManageCom.substring(0,4) 
		&& comCode <= pManageCom  ) return true;
	if (pManageCom.length == 4 && comCode.substring(0,4) == pManageCom.substring(0,4))
	{
		var tManage = pManageCom+ "0000" ;
		if (comCode == tManage||comCode == pManageCom) return true;
		
	}
	return false;
	
}


function printTask()
{
	if (checkData() == false)
	return false;
	var newWindow = window.open("../f1print/bqWorkFinLis.jsp?StartDate="+fm.StartDate.value+"&EndDate="+fm.EndDate.value+"&organcode="+fm.organcode.value+"&contType="+fm.contType.value);
}


function clearData()
{
	
		fm.all('organname').value = "";
    fm.all('organcode').value = "";
    fm.all('StartDate').value = "";
    fm.all('EndDate').value = "";
    fm.all('contType').value = "0";
    showAllCodeName();
}