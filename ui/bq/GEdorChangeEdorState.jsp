<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GEdorChangeEdorState.jsp
//程序功能：工单管理转为待办数据保存页面
//创建日期：2005-01-19 20:14:18
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.bq.*"%>
  
<%
	String flag = "Succ";
	String content = "";
	String FeeFlag = request.getParameter("FeeFlag");
	String edorAcceptNo = request.getParameter("edorAcceptNo");
	GlobalInput tGI = (GlobalInput) session.getValue("GI");	
	System.out.println("VVVVVVVVVVVVVVVVVVVVv" + FeeFlag);
	
	//把补退费金额放入财务接口
	if ((FeeFlag != null) && (FeeFlag.equals("1")))
	{
    FinanceDataUI tFinanceDataUI = new FinanceDataUI(tGI, edorAcceptNo, BQ.NOTICETYPE_G);
    if (!tFinanceDataUI.submitData())
    {
    	flag = "Fail";
      content = "设置财务数据失败！原因是：" + tFinanceDataUI.getError();
    }
  }
  	
	TransferData td = new TransferData();
	td.setNameAndValue("tableName", request.getParameter("tableName"));
	td.setNameAndValue("edorState", request.getParameter("edorState"));
	td.setNameAndValue("edorAcceptNo", request.getParameter("edorAcceptNo"));
	
	VData tVData = new VData();
	tVData.add(td);
	tVData.add(tGI);
	
	GEdorChangeStatusBL tGEdorChangeStatusBL = new GEdorChangeStatusBL();
	if (tGEdorChangeStatusBL.submitData(tVData, "ok") == false)
	{
		flag = "Fail";
	}
	else 
	{
		content = "工单状态改变成功";
	}
%> 
<html>
parent.fraInterface.afterSubmit("<%=flag%>", "<%=content%>");
</html>

