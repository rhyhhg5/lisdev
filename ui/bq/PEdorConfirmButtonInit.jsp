<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorConfirmButtonInit.jsp
//程序功能：页面初始化
//创建日期：2005-04-19
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.SSRS"%>
<%@page import="com.sinosoft.utility.ExeSQL"%>

<%
	SSRS tSSRS = new SSRS();
	ExeSQL tExeSQL = new ExeSQL();
	String hasRedo = "0";
	
	String edorAcceptNo = request.getParameter("EdorAcceptNo");
	String sql = 	"select EdorState, OtherNo "
					+ "from LPEdorApp "
					+ "where EdorAcceptNo = '" + edorAcceptNo + "' ";
	
	tSSRS = tExeSQL.execSQL(sql);
	 if(tSSRS.getMaxRow() == 1 && tSSRS.GetText(1, 1).equals("2")) //理算完成状态
	 {
	 	hasRedo = "1";
	 }
	 String customerNo = tSSRS.GetText(1, 2);
%>
<SCRIPT>
var medorAcceptNo = "<%=edorAcceptNo%>";
var mGetMoney = 0;

//初始化表单
function initForm()
{
	fm.all("EdorAcceptNo").value = medorAcceptNo;
	if("<%=hasRedo%>" == "0")
	{
		document.all.redo.disabled=true;
	}
	mGetMoney = queryMoney(medorAcceptNo);
	//queryAccount(medorAcceptNo);
	getNoticeNo(medorAcceptNo);
	var getMoney = HaveFee(medorAcceptNo);
	if (getMoney != 0)
	{
		document.all("AccFee").style.display = "";
		getAppAccInfo();
		//useAcc();
		notUseAcc();
		if (getMoney > 0)
		{
		  document.all("trEndDate").style.display = "";
		  fm.EndDate.verify = "截止日期|notnull&date";
			document.all("AccTypeStr").innerText = "是否使用帐户余额抵扣?";
			fm.AccType.value = "0";
			fm.DestSource.value = "01";
		}
		else
		{
			document.all("AccTypeStr").innerText = "本次退费是否转到帐户余额?";
			fm.AccType.value = "1";
			fm.DestSource.value = "11";
		}
		showPayMode();
	}
	fm.CustomerNo.value = "<%=customerNo%>";
	initElementtype();
	setRemak();
	//控制保全确认和重复理算按钮的显示
//	var sqlConfirm = "  select edorState "
//	                 + "from LPEdorApp "
//	                 + "where edorAcceptNo = '" + medorAcceptNo + "' ";
//	var result = easyExecSql(sqlConfirm);
//	if(result)
//	{
//	    if(result[0][0] == "9")
//	    {
//	        fm.confirmButton.style.display = "none";
//	        document.all.redo.disabled=true;
//	    }
//	}
/*
  if(hasOmnipotenceWT())
  {
    fm.PayMode.value = "4";
    afterPayModeSelect();
  }
  */
}

//判断是否有万能险犹豫期退保
function hasOmnipotenceWT()
{
  var sql = "select 1 from LPEdorItem a where EdorNo = '" + medorAcceptNo + "' "
          + "   and exists (select 1 from LCCont "
          + "               where ContNo = a.ContNo and CardFlag = '8' and PayMode = '4') ";
  var rs = easyExecSql(sql);

  if(rs != null && rs[0][0] != null && rs[0][0] != "" && rs[0][0] != "null")
  {
    return true;
  }
  return false;
}
</SCRIPT>