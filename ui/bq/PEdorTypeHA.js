//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeCCReturn()
{  
	top.close();
}

function edorTypeHASave()
{
  if (!verifyInput2())
  {
    return false;
  }
	if(fm.all("PayType").value =="" || fm.all("PayType").value=="1")
	{
		alert("请选择给付对象.");
		return false ;	
	}
	

  fm.PayType1.value=fm.PayType.value;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
  fm.submit();
}

function checkBonusGetMode()
{
	if(fm.BonusGetMode.value == fm.BonusGetMode2.value)
	{
		alert("您没有修改“红利领取方式”，请修改后再保存");
		return false;
	}
	return true;
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
  try
  {
    showInfo.close();
    window.focus();
  }
  catch(ex) {}
  
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  {
  	var tTransact=fm.all('fmtransact').value;
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		returnParent();
  }
}

function returnParent()
{ 
	  try
	  {
		  top.opener.getEdorItem();
	  }
	  catch(ex) { }
	  
	  top.opener.focus();
		top.close();
}
//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function getContDetail()
{
	  var isModify = true;
	  var strSQL = "select appntno,appntname,insuredno,insuredname,cvalidate,paytodate,prem,amnt,BonusGetMode," +
	  			   " (select codename from ldcode where codetype='bonusgetmode' and code=lp.BonusGetMode) "+
	               " from  lppol lp where contNo='"+fm.ContNo.value  + "' and edortype='HA'" +
	               " and exists (select 1 from lmriskapp where riskcode=lp.riskcode and risktype4='2')" +
	               " and edorno='"+fm.EdorNo.value+"'";
    arrResult = easyExecSql(strSQL,1,0);
    if(arrResult == null)
    {
    	isModify = false;
		  var strSQL = "select appntno,appntname,insuredno,insuredname,cvalidate,paytodate,prem,amnt,BonusGetMode," +	
		  			   " (select codename from ldcode where codetype='bonusgetmode' and code=lc.BonusGetMode) "+
		               " from  lcpol lc where contNo='"+fm.ContNo.value  + "'" +
		               " and exists (select 1 from lmriskapp where riskcode=lc.riskcode and risktype4='2') ";
	    arrResult = easyExecSql(strSQL,1,0);
	}
	if( arrResult != null )
	{
		fm.AppntNo.value = arrResult[0][0];
		fm.AppntName.value = arrResult[0][1];
		fm.InsuredNo.value = arrResult[0][2];
		fm.InsuredName.value = arrResult[0][3];
		fm.CValidate.value = arrResult[0][4];
		fm.PayToDate.value = arrResult[0][5];
		fm.Prem.value = arrResult[0][6];
		fm.Amnt.value = arrResult[0][7];
		fm.BonusGetMode.value = arrResult[0][8];
		fm.BonusGetMode1.value = arrResult[0][9];
		
	}
	if(arrResult[0][8]=="2"){
		var strSQL =  " select insuaccbala  from  lcinsureacc  where contNo='"+fm.ContNo.value  + "' with ur" ;
		arrResult = easyExecSql(strSQL,1,0);
		if( arrResult != null )
		{
		  fm.InsuAccBala.value = arrResult[0][0];
		  fm.all('InsuAccBala').style.display = "";
		}
	}
		
		
}

function getInsurdNo()
{

	var sql = "select insuredno from lcpol  " +
    "where contno = '" +fm.ContNo.value + "' and insuredno=appntno with ur ";
	var arrResult = easyExecSql(sql, 1, 0);
	if (arrResult!=null)
	{
		document.all("getCustomer").style.display = "none";	
		fm.PayType.value = "3";
		fm.PayTypeName.value = "被保人";

	}else{
		document.all("getCustomer").style.display = "";
	}


}