<%
//GEdorTypeXTInit.jsp
//程序功能：
//创建日期：2005.3.31 20:00:00
//创建人  ：LHS
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">  
//单击时查询
function reportDetailClick(parm1,parm2)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divLPAppntGrpDetail.style.left=ex;
  	divLPAppntGrpDetail.style.top =ey;
   	detailQueryClick();
}
function initInpBox()
{ 
  try
  {    
  	//判断是否是在工单查看总查看项目明细，若是则没有保存按钮
  	var flag;
  	try
  	{
  		flag = top.opener.fm.all('loadFlagForItem').value;
  	}
  	catch(ex)
  	{
  		flag = "";	
  	}
  	
  	if(flag == "TASK")
  	{
  		fm.save.style.display = "none";
  		fm.goBack.style.display = "none";
  		fm.cancel.style.display = "none";
  	}
  	
    fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    fm.all('GrpContNo').value = top.opener.fm.all('GrpContNo').value;
    fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    fm.all('EdorAcceptNo').value = top.opener.fm.all('EdorAcceptNo').value;
    fm.all('ContType').value ='2';
    try {fm.all('AppntNo').value= ''; } catch(ex) { }; //客户号码
    try {fm.all('Peoples2').value= ''; } catch(ex) { }; //投保总人数
    try {fm.all('GrpName').value= ''; } catch(ex) { }; //单位名称
    try {fm.all('SignCom').value= ''; } catch(ex) { }; //签单机构
    try {fm.all('SignDate').value= ''; } catch(ex) { }; //签单日期
    try {fm.all('CValiDate').value= ''; } catch(ex) { }; //保单生效日期
    try {fm.all('Prem').value= ''; } catch(ex) { }; //总保费
    try {fm.all('Amnt').value= ''; } catch(ex) { }; //总保额
    try {fm.all('EndDate').value= getCurrentDate(); } catch(ex) { }; //总保额
    
    showOneCodeName("EdorCode", "EdorTypeName");
 }
  catch(ex)
  {
    alert("在GEdorTypeXTInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在GEdorTypeXTInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
   initInpBox();
   initSelBox(); 
   initQuery(); 
   
   initLCGrpPolGrid();
   queryLCGrpPolGrid();
   
   initElementtype();
  }
  catch(re)
  {
    alert("GEdorTypeXTInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initQuery()
{	
	try
  {
   getGrpCont();
   getEdorItemInfo();
  }
  catch(re)
  {
    alert("GEdorTypeXTInit.jsp-->getGrpCont函数中发生异常:初始化界面错误!");
  }
	
}

function initLCGrpPolGrid()
{
  var iArray = new Array();
  
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            		//列宽
    iArray[0][2]=30;            			//列最大值
    iArray[0][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[1]=new Array();
    iArray[1][0]="保障计划";         		//列名
    iArray[1][1]="50px";            		//列宽
    iArray[1][2]=100;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[1][21] = "ContPlanCode";
    
    iArray[2]=new Array();
    iArray[2][0]="险种序号";         		//列名
    iArray[2][1]="50px";            		//列宽
    iArray[2][2]=100;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[2][21] = "RiskSeqNo";
    
    iArray[3]=new Array();
    iArray[3][0]="险种代码";         		//列名
    iArray[3][1]="40px";            		//列宽
    iArray[3][2]=100;            			//列最大值
    iArray[3][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21] = "RiskCode";
    
    iArray[4]=new Array();
    iArray[4][0]="险种名称";         		//列名
    iArray[4][1]="150px";            		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21] = "RiskName";
    
    iArray[5]=new Array();
    iArray[5][0]="应交保费";         		//列名
    iArray[5][1]="50px";            		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21] = "SumDuePayMoney";
    
    iArray[6]=new Array();
    iArray[6][0]="期交保费";         		//列名
    iArray[6][1]="50px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21] = "Prem";
    
    iArray[7]=new Array();
    iArray[7][0]="实交保费";         		//列名
    iArray[7][1]="50px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[7][21] = "SumActuPayMoney";
    
    iArray[8]=new Array();
    iArray[8][0]="生效日期";         		//列名
    iArray[8][1]="60px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[8][21] = "CValiDate";
    
    iArray[9]=new Array();
    iArray[9][0]="保险满期";         		//列名
    iArray[9][1]="60px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[9][21] = "EndDate";
    
    iArray[10]=new Array();
    iArray[10][0]="交至日期";         		//列名
    iArray[10][1]="60px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[10][21] = "PayToDate";
    
    iArray[11]=new Array();
    iArray[11][0]="正常解约金额";         		//列名
    iArray[11][1]="70px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[11][21] = "GetMoney";
    
    iArray[12]=new Array();
    iArray[12][0]="协议解约金额";         		//列名
    iArray[12][1]="70px";            		//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[12][21] = "XTFee";
    
    iArray[13]=new Array();
    iArray[13][0]="协议解约比例";         		//列名
    iArray[13][1]="70px";            		//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    iArray[13][21] = "XTFeeRate";
      
    LCGrpPolGrid = new MulLineEnter( "fm" , "LCGrpPolGrid" ); 
    //这些属性必须在loadMulLine前
    LCGrpPolGrid.mulLineCount = 0;
    LCGrpPolGrid.displayTitle = 1;
    LCGrpPolGrid.locked = 1;
    LCGrpPolGrid.canSel = 0;
    LCGrpPolGrid.canChk = 1;
    LCGrpPolGrid.hiddenSubtraction = 1;
    LCGrpPolGrid.hiddenPlus = 1;
    LCGrpPolGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert(ex);
  }
}

</script>