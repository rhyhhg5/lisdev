//               该文件中包含客户端需要处理的函数和事件
var showInfo1;
var mDebug = "1";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
function edorTypeRFReturn() {
	try {
		top.opener.initEdorItemGrid();
		top.opener.getEdorItem();
		top.close();
		top.opener.focus();
	}
	catch (ex) {
	}
}
function getPolInfo(tContNo) {
	var strSQL = "select a.riskcode,(select riskname from lmrisk where riskcode=a.riskcode),amnt,prem, nvl((select summoney from lpreturnloan where edorno='" + fm.all("EdorNo").value + "'),(select summoney from loloan where polno=a.polno and payoffflag='0' and loantype='0'))   from LCPol a " + "  where ContNo='" + fm.all("ContNo").value + "' and riskcode in (select riskcode from lmloan) order by riskcode fetch first 1 row only with ur";
	turnPage.strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
    //判断是否查询成功
	if (!turnPage.strQueryResult) {
		return false;
	}
    //alert(turnPage.strQueryResult);
    //查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = PolGrid;    
    //保存SQL语句
	turnPage.strQuerySql = strSQL; 
    //设置查询起始位置
	turnPage.pageIndex = 0;  
    //在查询结果数组中取出符合页面显示大小设置的数组
	arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, 50);
    //调用MULTILINE对象显示查询结果
	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
}
function edorTypeRFSave() {
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo1 = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	try {
		showInfo1.close();
	}
	catch (ex) {
	}
	window.focus();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		edorTypeRFReturn();
	}
}

//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv, cShow) {
	if (cShow == "true") {
		cDiv.style.display = "";
	} else {
		cDiv.style.display = "none";
	}
}
/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug) {
	if (cDebug == "1") {
		parent.fraMain.rows = "0,0,50,82,*";
	} else {
		parent.fraMain.rows = "0,0,0,72,*";
	}
}

