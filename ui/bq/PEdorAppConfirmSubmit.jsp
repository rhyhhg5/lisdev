<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：PEdorAppConfirmSubmit.jsp
//程序功能：
//创建日期：2005-09-21
//创建人  ：QiuYang
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.bq.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%    
    String flag = "";
    String content = "";
    GlobalInput gi = (GlobalInput)session.getValue("GI");
    String edorAcceptNo = request.getParameter("EdorAcceptNo");

		//保全理算
    PEdorAppConfirmUI tPEdorAppConfirmUI = new PEdorAppConfirmUI(gi, edorAcceptNo);
    if (!tPEdorAppConfirmUI.submitData())
    {
    	flag = "Fail"; 
      content = "保全理算失败！原因是：" + tPEdorAppConfirmUI.getError();
    }
    else
		{
			flag = "Succ";
     		content = "保全理算成功。";
       
	    	//生成打印数据
	    	VData tVData = new VData();
			tVData.add(gi);
			PrtAppEndorsementBL tPrtAppEndorsementBL = new PrtAppEndorsementBL(
			        edorAcceptNo);
			if (!tPrtAppEndorsementBL.submitData(tVData, ""))
			{
				content = "保全理算成功。但生成理算结果预览失败！";
			}
		}
    content = PubFun.changForHTML(content);
%>                      
<html>
<script language="javascript">
	var mLoadFlag = "<%=request.getParameter("LoadFlag")%>";
	if (mLoadFlag == "TASKFRAME")
	{
		parent.fraInterface.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
	}
	else
	{
		parent.fraInterface.afterSubmit("<%=flag%>","<%=content%>");
	}
</script>
</html>