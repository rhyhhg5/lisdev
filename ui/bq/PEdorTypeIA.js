//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";

var turnPage = new turnPageClass();  

//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function edorTypeIAReturn()
{
		initForm();
}

function verify() {
  if (fm.all('AppntRelationToInsured').value == "") {
    alert("与被保人关系必须填写!");
    return false;	
  }
  
  if (fm.all('AppntCustomerNo').value == "") {
    alert("客户号不能为空!");
    //可能以后允许为空，为空表示增加新的投保人
    return false;
  }
  
  if (LCAppntIndGrid.getSelNo() == 0) {
  	alert("请选择需要变更的投保人！");
  	return false;
  }
  
  if (fm.all('AppntBirthday').value=="" || calAge(fm.all('AppntBirthday').value)<18) {
    alert("投保人的年龄为 " + calAge(fm.all('AppntBirthday').value) + "岁，必须大于等于18岁!");
    return false;	
  }
   
  return true;
}

function edorTypeIASave()
{
  if (!verify()) return false; 
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value = "INSERT||MAIN";
 	//showSubmitFrame(mDebug);
  fm.submit();
  //showSubmitFrame(mDebug);

}

function customerQuery()
{	
	window.open("./LCAppntIndQuery.html");
}

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  initLCAppntIndGrid();
 //  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,Result )
{
//	alert("ppp");
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  	var tTransact=fm.all('fmtransact').value;
//  	alert(tTransact);
		if (tTransact=="QUERY||MAIN")
		{
			var iArray;
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
			//保存查询结果字符串
 		 	turnPage.strQueryResult  = Result;
  		//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
			
			turnPage.arrDataCacheSet =chooseArray(tArr,[2,8,9,10,12,22,24]);
			//设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
		 	turnPage.pageDisplayGrid = LCAppntIndGrid;    
		  
		  //设置查询起始位置
		 	turnPage.pageIndex       = 0;
		 	//在查询结果数组中取出符合页面显示大小设置的数组
	  	var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
			//调用MULTILINE对象显示查询结果
	   	displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
		}
		else if (tTransact=="QUERY||DETAIL")
		{
			//清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  		turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  		//保存查询结果字符串
  	//	alert(Result);

 		 	turnPage.strQueryResult  = Result;
  		//使用模拟数据源，必须写在拆分之前
  		turnPage.useSimulation   = 1;  
    
  		//查询成功则拆分字符串，返回二维数组
  		var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,tTransact);
  		turnPage.arrDataCacheSet =chooseArray(tArr,[2,8,9,10,22,24,15,51,12,13,27,28,29,30,31,32,34,35]);

  		fm.all('CustomerNo').value = turnPage.arrDataCacheSet[0][0];
  		fm.all('Name').value = turnPage.arrDataCacheSet[0][1];
  		fm.all('Sex').value = turnPage.arrDataCacheSet[0][2];
  		fm.all('Birthday').value = turnPage.arrDataCacheSet[0][3];
  		fm.all('IDType').value = turnPage.arrDataCacheSet[0][4];
  		fm.all('IDNo').value = turnPage.arrDataCacheSet[0][5];
  		fm.all('OccupationType').value = turnPage.arrDataCacheSet[0][6];
  		fm.all('OccupationCode').value = turnPage.arrDataCacheSet[0][7];
  		fm.all('Nationality').value = turnPage.arrDataCacheSet[0][8];
  		fm.all('Marriage').value = turnPage.arrDataCacheSet[0][9];
  		
  		fm.all('ICNo').value = turnPage.arrDataCacheSet[0][10];
  		fm.all('HomeAddressCode').value = turnPage.arrDataCacheSet[0][11];
  		fm.all('HomeAddress').value = turnPage.arrDataCacheSet[0][12];
  		fm.all('PostalAddress').value = turnPage.arrDataCacheSet[0][13];
  		fm.all('ZipCode').value = turnPage.arrDataCacheSet[0][14];
  		fm.all('Phone').value = turnPage.arrDataCacheSet[0][15];
  		fm.all('Mobile').value = turnPage.arrDataCacheSet[0][16];
  		fm.all('EMail').value = turnPage.arrDataCacheSet[0][17];

  		divLPAppntIndDetail.style.display ='';
  		divDetail.style.display='';

		}
	else
		{
		  var strSql = "select paylocation from lcpol where polno='" + fm.PolNo.value + "'";
      var arrResult = easyExecSql(strSql);
      
      if (arrResult!=null && arrResult=='0') {
        alert("该保单的交费方式为银行转账，更换投保人后请变更账户！");
        //content = content + " 该保单的交费方式为银行转账，更换投保人后请变更账户！";
      }
      
      var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
      showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
      initForm(); 
  	}
  	
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
       
//---------------------------
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
	if( cDebug == "1" )
		parent.fraMain.rows = "0,0,50,82,*";
	else 
		parent.fraMain.rows = "0,0,0,72,*";
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;

		fm.all( 'CustomerNo' ).value = arrResult[0][1];
		fm.all( 'name').value = arrResult[0][2];
		/**
		alert("aa:"+arrResult[0][5]);
		fm.all('Nationality').value = arrResult[0][5];
		fm.all('Marriage').value=arrResult[0][6];
		fm.all('Stature').value=arrResult[0][7];
		fm.all('Avoirdupois').value=arrResult[0][8];
		fm.all('ICNo').value=arrResult[0][9];
		fm.all('HomeAddressCode').value=arrResult[0][10];
		fm.all('HomeAddress').value=arrResult[0][11];
		fm.all('PostalAddress').value=arrResult[0][12];
		fm.all('ZipCode').value=arrResult[0][13];
		fm.all('Phone').value=arrResult[0][14];
		fm.all('Mobile').value=arrResult[0][15];
		fm.all('EMail').value=arrResult[0][16];
		*/
		// 查询保单明细
		queryAppntIndDetail();
	}
}
function queryAppntIndDetail()
{
	var tEdorNO;
	var tEdorType;
	var tPolNo;
	var tCustomerNo;
	
	tEdorNo = fm.all('EdorNO').value;
	//alert(tEdorNo);
	tEdorType=fm.all('EdorType').value;
	//alert(tEdorType);
	tPolNo=fm.all('PolNo').value;
	//alert(tPolNo);
	tCustomerNo = fm.all('CustomerNo').value;
	//alert(tCustomerNo);
	//top.location.href = "./AppntIndQueryDetail.jsp?EdorNo=" + tEdorNo+"&EdorType="+tEdorType+"&PolNo="+tPolNo+"&CustomerNo="+tCustomerNo;
	parent.fraInterface.fm.action = "./AppntIndQueryDetail.jsp";
	fm.submit();
	parent.fraInterface.fm.action = "./PEdorTypeACSubmit.jsp";
}
function returnParent()
{
	top.close();
}

function personQuery()
{
    //window.open("./LCPolQuery.html");
    window.open("./LPTypeIAPersonQuery.html");
}

function personNew()
{
    //window.open("./LCPolQuery.html");
    window.open("./LPTypeIAPersonNew.html");
}


function afterPersonQuery(arrResult)
{
    //alert("here");
    if (arrResult == null ||arrResult[0] == null || arrResult[0][0] == "" )
        return;

    //选择了一个投保人,显示详细细节
    fm.all("QueryCustomerNo").value = arrResult[0][0];
    
    var strSql = "select * from ldperson where customerNo = " + arrResult[0][0];
    
	//查询SQL，返回结果字符串
    turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

 //   alert(turnPage.strQueryResult);
    //判断是否查询成功
    if (!turnPage.strQueryResult) {  
    	//清空MULTILINE，使用方法见MULTILINE使用说明 
      	VarGrid.clearData('VarGrid');  
      	alert("查询失败！");
      	return false;
  	}

  
 	 //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  	turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
    //查询成功则拆分字符串，返回二维数组
	turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
    fillPersonDetail();
    
  divLPAppntIndDetail.style.display = "";
    
    	
	
}

function fillPersonDetail()
{
	try {
		fm.all("AppntCustomerNo").value = turnPage.arrDataCacheSet[0][0];
		fm.all("AppntName").value = turnPage.arrDataCacheSet[0][2];
		fm.all("AppntSex").value = turnPage.arrDataCacheSet[0][3];
		fm.all("AppntBirthday").value = turnPage.arrDataCacheSet[0][4];
		
		fm.all("AppntIDType").value = turnPage.arrDataCacheSet[0][16];
		fm.all("AppntIDNo").value = turnPage.arrDataCacheSet[0][18];
		fm.all("AppntNativePlace").value = turnPage.arrDataCacheSet[0][5];
		
		fm.all("AppntPostalAddress").value = turnPage.arrDataCacheSet[0][24];
		fm.all("AppntZipCode").value = turnPage.arrDataCacheSet[0][25];
		fm.all("AppntHomeAddress").value = turnPage.arrDataCacheSet[0][23];
		fm.all("AppntHomeZipCode").value = turnPage.arrDataCacheSet[0][22];
		
		fm.all("AppntPhone").value = turnPage.arrDataCacheSet[0][26];
		fm.all("AppntPhone2").value = turnPage.arrDataCacheSet[0][56];
		fm.all("AppntMobile").value = turnPage.arrDataCacheSet[0][28];
		fm.all("AppntEMail").value = turnPage.arrDataCacheSet[0][29];
		fm.all("AppntGrpName").value = turnPage.arrDataCacheSet[0][38];
		
		fm.all("AppntWorkType").value = turnPage.arrDataCacheSet[0][48];
		fm.all("AppntPluralityType").value = turnPage.arrDataCacheSet[0][49];
		fm.all("AppntOccupationCode").value = turnPage.arrDataCacheSet[0][50];
		fm.all("AppntOccupationType").value = turnPage.arrDataCacheSet[0][9];
    } catch(ex) {
    	alert("在PEdorTypeIA.js-->fillPersonDetail函数中发生异常:初始化界面错误!");
  	}      
}