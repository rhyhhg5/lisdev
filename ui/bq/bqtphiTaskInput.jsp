<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：BonusPrintInput.jsp
//程序功能：分红查询打印页面
//创建日期：2012-09-03
//创建人  ：xp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.operfee.*"%>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
  <SCRIPT src="bqtphiTask.js"></SCRIPT>
  <%@include file="./bqtphiTaskInit.jsp"%>
</head>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
          	               	
%>

<SCRIPT>
  var managecom = <%=tGI.ManageCom%>;
  var operator = '<%=tGI.Operator%>';
</SCRIPT> 

<body  onload="initForm();" >
<form action="bqtphiTaskSave.jsp" method=post name="fm" target="fraSubmit">
  <table>
    <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAlivePayMulti1);">
      </td>
      <td class= titleImg>
        税优保全数据上报接口批处理：
      </td>
     </tr>
   </table>
   <Div  id= "divAlivePayMulti1" style= "display: ''">
   <table  class= common align=center>
      <tr  class= common>
            		<TD class= title>
   				接口类型
        </TD>
        <TD class= input>
        	<Input class="codeno" readOnly name= "queryFlag" CodeData="0|^00|请选择接口类型^END001|保全变更上传^END003|保单状态修改上传^END007|保单转出登记^PRM001|续期保费信息上传^RNW001|续保信息上传^ACT001|账户变更上传" 
          ondblClick="showCodeListEx('queryFlag',[this,queryFlagName],[0,1],null,null,null,1);"
          onkeyup="showCodeListKeyEx('queryFlag',[this,queryFlagName],[0,1],null,null,null,1);" verify="文件类型|NOTNULL" ><Input 
          class= codename name= 'queryFlagName' elementtype=nacessary ></TD>
      	<td class= title>上报业务号码</td>
      	<td class= input><Input class= common name=MsgContNo id="MsgContNo"></td>	
        <td><Input class = cssbutton VALUE="单独上报" TYPE=button onclick="queryOneRun(1);"> </td>
      </tr>
      <!-- <tr  class= common>
        <td ><Input class = cssbutton colspan="3" VALUE="上报全部数据" TYPE=button onclick="queryRun(1);"> </td>
      </tr> -->
    </table>
    <font style=color:red;valign:right>上报业务号码注释：<br>1、END001为保全工单号<br>2、END003为保单号<br>3、END007为保单号
    <br>4、PRM001为实收号PayNo<br>5、RNW001为实收号PayNo<br>6、ACT001为账户轨迹序号SerialNo<br>若上报业务号码为空，则将上报全部数据
    </font>
    </Div> 
    <br/>
    <br/><br/>
    <table>
    <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divEND006);">
      </td>
      <td class= titleImg>
        	END006保单转出信息查询接口
      </td>
     </tr>
   </table>
   <Div  id= "divEND006" style= "display: ''">
    <table  class= common align=center>
      <tr  class= common>
            		<TD class= title>
   				查询起期
        </TD>
        <TD class= input>
        	<Input class= common name=StartDate elementtype=nacessary  ></TD>
      	<td class= title>查询止期</td>
      	<td class= input>
      		<Input class= common name=EndDate elementtype=nacessary  >
      	</td>	
        <td><Input class = cssbutton VALUE="获取数据" TYPE=button onclick="queryOneRun(2);"> </td>
      </tr>
    </table>
    <font style=color:red;valign:right>中保信平台限制查询起止日期不能超过7天
    </font>
       </Div> 
       <input type="hidden" name="Flag">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
