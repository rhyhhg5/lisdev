
<%
  //程序名称：bqCTEdorSub.jsp
  //程序功能：保全退保统计
  //创建日期：2006-2-8
  //创建人  ：yanchao
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
ReportUI
</title>
<head></head>
<body>
<%
  String flag = "0";
  String FlagStr = "";
  String Content = "";
  String managecom = " ";
  String managecom1 = " ";
  String managecom2 = "";
  String managecomname = "";
  //设置模板名称
  String FileName = "bqCTEdorLis";
  String organcode = request.getParameter("organcode");
  System.out.println(organcode);
  String organname = request.getParameter("organname");
  System.out.println(organname);
  String StartDate = request.getParameter("StartDate");
  System.out.println(StartDate);
  String EndDate = request.getParameter("EndDate");
  System.out.println(EndDate);
  if (organcode.equals("86000000")) {
    managecom1 = " and p.managecom ='" + organcode + "'";
    managecom2 = " ";
  }
  else if (organcode.length() > 4 && organcode.substring(4).equals("0000")) {
    managecom1 = " and p.managecom = '" + organcode + "'";
    managecom2 = " ";
  }
  else if (organcode.length() == 4){
  	managecom1 = " and p.managecom like '" + organcode + "%'";
    managecom2 = " ";
  }
  else {
    managecom1 = " and p.managecom  like '" + organcode + "%'";
    managecom2 = "union select '86' x,t.riskcode r,m.riskname,t.q tq,t.w tw,m.riskprop o from ("
  	+"	select riskcode,sum(a) q,sum(b) w from ("
		+"	select riskcode,count(distinct prtno) a,sum(prem) b from  lcpol  p where appflag='1'"   
		+"	and makedate between '"+StartDate+"' and '"+EndDate+"'"
		+"		group by riskcode"
		+" 	union "
		+"	select riskcode,count(distinct prtno) a,sum(prem) b from  lbpol  p where appflag='1'   "
		+"		and makedate between '"+StartDate+"' and '"+EndDate+"'  "
		+"		group by riskcode"
		+"	) k group by riskcode"
		+") t,lmriskapp m "
		+"where t.riskcode=m.riskcode";  
  }
  String sql = "select name from ldcom where comcode = '"+organcode+"'";
  ExeSQL tExeSQL = new ExeSQL();
  managecomname = tExeSQL.getOneValue(sql);
  System.out.println(managecom1);
  System.out.println(managecom2);
  String strVFPathName = "";
  JRptList t_Rpt = new JRptList();
  String tOutFileName = "";
  if (flag.equals("0")) {
    t_Rpt.m_NeedProcess = false;
    t_Rpt.m_Need_Preview = false;
    t_Rpt.mNeedExcel = true;
    t_Rpt.AddVar("StartDate", StartDate);
    t_Rpt.AddVar("EndDate", EndDate);
    t_Rpt.AddVar("organcode", organcode);
    t_Rpt.AddVar("managecom1", managecom1);
    t_Rpt.AddVar("managecom2", managecom2);
    t_Rpt.AddVar("managecomname", managecomname);
    t_Rpt.Prt_RptList(pageContext, FileName);
    tOutFileName = t_Rpt.mOutWebReportURL;
    String strVFFileName = FileName + tOutFileName.substring(tOutFileName.indexOf("_"));
    String strRealPath = application.getRealPath("/web/Generated").replace('\\', '/');
    strVFPathName = strRealPath + "/" + strVFFileName;
    System.out.println(strVFPathName);
    System.out.println("=======Finshed in JSP===========");
    System.out.println(tOutFileName);
  }
%>
<form name="fm" method="post" action="../web/ShowF1Report.jsp">
<input name="FileName" type="hidden" value="<%=tOutFileName%>">
<input name="RealPath" type="hidden" value="<%=strVFPathName%>">
</form>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;

if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >