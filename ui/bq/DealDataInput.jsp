<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2006-10-29 20:08
//创建人  ：qulq
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	

	<SCRIPT src="DealDataInput.js"></SCRIPT>
	<%@include file="DealDataInputInit.jsp"%>
</head>
<body  onload="initForm();" >
<form action="./DealDataSave.jsp" method=post name=fm target="fraSubmit">

<table>
	<tr>
		<td class = common>
			<input class = cssbutton value="工单查询" type=button onclick="workQuery();">
		</td>
		<td class = common>
			<input class = cssbutton value="批单查询" type=button onclick="sementQuery();">
		</td>
	</tr>
</table>
<table>
	<tr>
		<td class = common>
			处理状态
		</td>
		<td class = common>
			<input class = "readonly" readonly name="dealState">
		</td>
		<td class = common>
			最后处理时间
		</td>
		<td class = common>
			<input class = "readonly" readonly name="lastDealDate">
		</td>
				<td class = common>
			批单号
		</td>
		<td class = common>
			<input class = "readonly" readonly name="edorNo">
		</td>
	</tr>
		<tr>
		<td class = common>
			确认日期
		</td>
		<td class = common>
			<input class = "readonly" readonly name="confimDate">
		</td>
		<td class = common>
			投保人
		</td>
		<td class = common>
			<input class = "readonly" readonly name="appntName">
		</td>
		<td class = common>
			投保人联系电话
		</td>
		<td class = common>
			<input class = "readonly" readonly name="appntPhone">
		</td>
	
	</tr>
	<tr>
		<td class = common>
			业务类型
		</td>
		<td class = common>
			<input class = "readonly" readonly name="bussKind">
		</td>
		<td class = common>
			转帐时间
		</td>
		<td class = common>
			<input class = "readonly" readonly name="transferDate">
		</td>
		<td class = common>
			转帐失败时间
		</td>
		<td class = common>
			<input class = "readonly" readonly name="failDate">
		</td>
	
	</tr>

</table>

<table class =common> 
	<tr>

		<td class = common>
			操作选择
		</td>
	</tr>
</table>
<table>
<tr>
		<td class = input>
		<input type="Radio" name="dealRadio" value="reset" onclick="init();" > 重设转帐退费事项
		</td>
	</tr>
	<tr class = common>
		<td class =common align="right">
			转帐时间
		</td>
		<td class =input>
			<input class="coolDatePicker"  dateFormat="short" name="tranDate" >	
		</td>
		<td class =common>
					转帐银行
		</td>
		<td>
					<input class = "readonly" readonly name="bank">
		</td>
				<td class =common>
					帐户名
		</td>
				<td>
					<input class = "readonly" readonly name="accontName">
		</td>
				<td class =common>
					帐号
		</td>
				<td>
					<input class = "readonly" readonly name="accontNo">
		</td>
</tr>
</table>
<table>
<tr>
	<td class = input>
		<input type="Radio" name="dealRadio" value="modify" align="right" onclick="unInit();"> 更改退费方式
	</td>
</tr>
<tr>

		<td class =input align="right">
			<input type="Radio" name="dealRadiotype" value="money"   > 现金：&nbsp;&nbsp;&nbsp;
	</td>


		<td class =common >
			领款人
	</td>
		<td>
			<input class =common name="getPerson" value="">
	</td>
		<td class =common>
			申请人性质
	</td>
		<td>
			<input class =common name="appKind" value="">
	</td>

</tr>
<tr>
	<td class =common >
	</td>
			<td class =common align="right">
			领款人证件号
	</td>
		<td>
			<input class =common name="no" value="">
	</td>
	<td>
	</td>
</tr>	
<tr>
	<td class =common align="center" colspan="2">
			<input type ="radio" name="dealRadiotype" value="accont"  > 投保人交费余额帐户
	</td>
</tr>
</table>

<table>
	<tr>
		<td class = common>
			<input class = cssbutton value="处理完毕" type=button onclick="save();">
		</td>
		<td class = common>
		<input class = cssbutton value="打印通知书" type=button onclick="printNotice();">
		</td>
		<td class = common>
		<input class = cssbutton value="重打批单" type=button onclick="rePrint();">	
		</td>
		<td class = common>
		<input class = cssbutton value="返回" type=button onclick="exit();">	
		</td>
	</tr>
</table>

    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
	  <input type=hidden name="LoadFlag">
	  <input type=hidden name="actuGetNo">
	  <input type=hidden name="gpflag">
	  <input type=hidden name="deal">
	  <input type=hidden name="dealtype">
	  <input type=hidden name="edorAcceptNo">
	  <input type=hidden name="busskind">
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
