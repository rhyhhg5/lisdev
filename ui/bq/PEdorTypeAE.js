var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量


function beforeSubmit()
{
  if (!verifyInput2())
  {
    return false;
  }
  if(fm.IDType.value == "0" || fm.IDType.value == "5"){
	  var strCheckIdNo = checkIdNo(fm.all("IDType").value,fm.all("IDNo").value, fm.all("Birthday").value, fm.all("Sex").value);
	  if ("" != strCheckIdNo && strCheckIdNo!=null)
	  {
		 alert(strCheckIdNo);
		 return false;
	  }
  }
  if(fm.PayMode.value == "4")
  {
    if(isNull(fm.BankCode.value) || isNull(fm.BankAccNo.value) || isNull(fm.AccName.value))
    {
      alert("交费方式为银行转账时，转帐银行、转账帐号、账户名都不能为空！");
      return false;
    }
  }
  return true;
}

//保存
function save()
{
   //20110712添加被保人和投保人的关系的校验
   var chkNum = 0;
   
   for (i = 0; i < RelationToAppntInsuredGrid.mulLineCount; i++)
   {
     if(RelationToAppntInsuredGrid.getChkNo(i))
     {
       var chkNum = chkNum+1;
     }
     if(chkNum==0)
     {
       alert("必须要选中被保人，并确认被保人和投保人的关系。");
       return false;
     }
   }
  

  if (!beforeSubmit())
  {
    return false;
  }
  
  var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.action = "./PEdorTypeAESubmit.jsp"
	fm.submit();
}

//查询P表保存的临时信息
function queryLPAppnt()
{
	var sql = "select AppntName, AppntSex, AppntBirthday, IDType, IDNo, " +
	          "       OccupationCode, OccupationType, BankCode, BankAccNo, AccName " +
	          "from LPAppnt " +
	          "where EdorNo = '" + fm.EdorNo.value + "' " +
	          "and EdorType = '" + fm.EdorType.value + "' " +
	          "and ContNo = '" + fm.ContNo.value + "'";
	var arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		return false;
	}
	fm.Name.value = arrResult[0][0];
	fm.Sex.value = arrResult[0][1];
	fm.Birthday.value = arrResult[0][2];
	fm.IDType.value = arrResult[0][3];
	fm.IDNo.value = arrResult[0][4];
	fm.OccupationCode.value = arrResult[0][5];
	fm.OccupationType.value = arrResult[0][6];
	fm.BankCode.value = arrResult[0][7];
	fm.BankAccNo.value = arrResult[0][8];
	fm.AccName.value = arrResult[0][9];

	sql = "select Phone, PostalAddress, ZipCode " +
	      "from LPAddress " +
	      "where EdorNo = '" + fm.EdorNo.value + "' " +
	      "and EdorType = '" + fm.EdorType.value + "' " +
	      "and AddressNo = (select AddressNo from LCAppnt " +
	      "    where ContNo = '" + fm.ContNo.value + "') "; 
	arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		return false;
	}
	fm.Phone.value = arrResult[0][0];
	fm.Address.value = arrResult[0][1];
	fm.ZipCode.value = arrResult[0][2];
	sql = "select PayMode, InsuredNo from LPCont " +
	      "where EdorNo = '" + fm.EdorNo.value + "' " +
	      "and EdorType = '" + fm.EdorType.value + "' " +
	      "and ContNo = '" + fm.ContNo.value + "'";
	arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		return false;
	}
	fm.PayMode.value = arrResult[0][0];
	//2017-0-05 yukun 删除,原因AE明细保存后再次点击明细mulLine不显示
//	var insuredNo = arrResult[0][1];
//	sql = "select RelationToMainInsured from LPInsured " +
//	      "where EdorNo = '" + fm.EdorNo.value + "' " +
//	      "and EdorType = '" + fm.EdorType.value + "' " +
//	      "and ContNo = '" + fm.ContNo.value + "' " +
//	      "and InsuredNo = '" + insuredNo + "'";
//	arrResult = easyExecSql(sql);
//	if (!arrResult)
//	{
//		return false;
//	}
//	fm.Relation.value = arrResult[0][0];
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(flag, content)
{
  showInfo.close();
  top.focus();
  if (flag == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  {
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		returnParent();
  }
}

//返回按钮
function returnParent()
{
	try
	{
		top.close();
		top.opener.getEdorItem();
		top.opener.focus();
	}
	catch (ex) {};
}

function afterCodeSelect(cName, Filed)
{
  if(cName == "PayMode1")
  {
    if(Filed.value == "4")
    {
      var sql = "select BankCode, (select max(bankname) from ldbank where bankcode = a.bankcode), " 
	          + "BankAccNo, AccName from LPAppnt a " 
	          + "where EdorNo = '" + fm.EdorNo.value + "' " 
	          + "and EdorType = '" + fm.EdorType.value + "' " 
	          + "and ContNo = '" + fm.ContNo.value + "'";
      var arrResult = easyExecSql(sql);
      if (!arrResult)
      {
      	return false;
      }
      fm.BankCode.value = arrResult[0][0];
      fm.BankCodeName.value = arrResult[0][1];
      fm.BankAccNo.value = arrResult[0][2];
      fm.AccName.value = arrResult[0][3];
    }
    else
    {
      fm.BankCode.value = "";
	  fm.BankCodeName.value = "";
	  fm.BankAccNo.value = "";
	  fm.AccName.value = "";
    }
  } 
}

function queryLogRTA() 
{
    //得到与投保人的关系
    //2011-03-29
    //【OoO?】杨天政
    var sql = "select insuredno,name,sex,birthday,relationtoappnt," +
    		" case when relationtomaininsured ='00' then '主被保险人'" +
    		" else '连带被保险人' end " +
    		" from LpInsured"
               //+ " where "
              + " where EdorNo = '" + fm.all('EdorNo').value + "' " 
              + " and EdorTYpe = '" + fm.all('EdorType').value + "' " 
			  + " and ContNo = '"+ fm.all('ContNo').value + "' ";
	var arrResult = easyExecSql(sql);
	if (!arrResult)
	{
		var sql = "select insuredno,name,sex,birthday,relationtoappnt," +
				"case when relationtomaininsured ='00' then '主被保险人'" +
    		    " else '连带被保险人' end " +
				" from LCInsured " +
            " where  ContNo = '"+ fm.ContNo.value + "' ";
            turnPage.pageDivName = "divPage";
            turnPage.queryModal(sql, RelationToAppntInsuredGrid);
	  //arrResult = easyExecSql(sql);
	}
	
	//fm.all('EdorNo').value = top.opener.fm.all('EdorNo').value;
    //fm.all('EdorType').value = top.opener.fm.all('EdorType').value;
    //fm.all('EdorTypeName').value = top.opener.fm.all('EdorType').value;
    //fm.all('ContNo').value = top.opener.fm.all('ContNoBak').value;
	
	//if (arrResult)
	//{
	//	//fm.all('RelationToAppnt').value = arrResult[0][0];
	//	//fm.all('RelationToAppntBak').value = arrResult[0][0];
	//}
 
  turnPage.pageDivName = "divPage";
  turnPage.queryModal(sql, RelationToAppntInsuredGrid);
  
}

function selectOne2() 
{
  var row1 = RelationToAppntInsuredGrid.getSelNo();
   
  //fm0.all('FeedId1').value = FeedGrid.getRowColDataByName(row1 - 1 , 'FeedId1');
  // fm0.all('FeedBackModule').value = FeedGrid.getRowColDataByName(row1 - 1, 'FeedBackModule');
    // fm0.all('Principal').value = FeedGrid.getRowColDataByName(row1 - 1, 'Principal');
  //fm0.all('State').value = FeedGrid.getRowColDataByName(row1 - 1, 'State');
 //fm.SeriNo.value = TempletGrid.getRowColDataByName(row - 1, "ColName");
  //fm0.all('Attitude').value = FeedGrid.getRowColDataByName(row1 - 1, 'Attitude');
  //fm.all('Reason').value = FeedGrid.getRowColDataByName(row - 1, 'Reason');
//fm.all('Route').value = FeedGrid.getRowColDataByName(row - 1, 'Route');
//fm.all('FeedBackId').value = FeedGrid.getRowColDataByName(row - 1, 'SerialNo');
}

//去左空格
function ltrim(s)
{
  return s.replace( /^\s*/, "");
}
//去右空格
function rtrim(s)
{
  return s.replace( /\s*$/, "");
}
//左右空格
function trim(s)
{
  return rtrim(ltrim(s));
}

function isNull(s)
{
  var t = trim(s);
  if(t==''||t==null)
  {
    return true;
  }
  else
  {
    return false;
  }
}