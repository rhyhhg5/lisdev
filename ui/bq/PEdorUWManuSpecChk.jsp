
<%@page contentType="text/html;charset=gb2312" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：PEdorAppUWManuSpecChk.jsp
//程序功能：人工核保特约承保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容
//modify by zhangxing Lanjun(2005-4-19 14:58)
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bq.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 
  <%@page import="com.sinosoft.workflowengine.*"%>
<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  
  boolean flag = true;
  GlobalInput tG = new GlobalInput();  
  tG=(GlobalInput)session.getValue("GI");  
  if(tG == null) {
		out.println("session has expired");
		return;
   } 
    //接收信息
  	LPSpecSchema tLPSpecSchema = new LPSpecSchema();
  	LPUWMasterSchema tLPUWMasterSchema = new LPUWMasterSchema();
  	TransferData tTransferData = new TransferData();
    String tContNo = request.getParameter("ContNo");
    String tEdorNo = request.getParameter("EdorNo");
    String tEdorType = request.getParameter("EdorType");
	
	String tRemark = request.getParameter("Remark");
	String tSpecReason = request.getParameter("SpecReason");
	String tPrtNo = request.getParameter("PrtNo");
	String tMissionID = request.getParameter("MissionID");
	String tSubMissionID = request.getParameter("SubMissionID");
	
	String tPolNo = request.getParameter("PolNo");
	
	System.out.println("PrtNo:"+tPrtNo);
	System.out.println("ContNo:"+tContNo);
	System.out.println("remark:"+tRemark);
	System.out.println("PolNo:"+tPolNo);
	if (tContNo == "" || (tRemark == "" ) )
	{
		Content = "请录入续保特别约定信息或续保备注信息!";
		FlagStr = "Fail";
		flag = false;
	}
	else
	{     
	      if(tContNo != null && tPrtNo != null && tMissionID != null && tSubMissionID != null)
	      {
		   //准备特约信息
		   
		   	tLPSpecSchema.setContNo(tContNo); 
		   	tLPSpecSchema.setEdorNo(tEdorNo);
		   	tLPSpecSchema.setEdorType(tEdorType);
		   	//tLPSpecSchema.setSpecType("1");
		   	
		   	tLPSpecSchema.setSpecContent(tRemark);
		   	tLPSpecSchema.setSpecType("1");
		   	tLPSpecSchema.setSpecCode("1");
		   //准备特约原因
		   tLPUWMasterSchema.setSpecReason(tSpecReason);
		   System.out.println(tSpecReason);
		   tLPUWMasterSchema.setEdorNo(tEdorNo);
		   tLPUWMasterSchema.setEdorType(tEdorType);
				   	
                
		   }// End of if
		  else
		  {
			Content = "传输数据失败!";
			flag = false;
		  }
	}
try
{
  	if (flag == true)
  	{
		// 准备传输数据 VData
		VData tVData = new VData();
		tVData.add(tLPSpecSchema);
		tVData.add( tLPUWMasterSchema );
		tVData.add(tPolNo);
		tVData.add( tG );
		
		// 数据传输
		PEdorUWSpecUI tPEdorUWSpecUI   = new PEdorUWSpecUI();
		if (!tPEdorUWSpecUI.submitData(tVData,""))
		  {     
		        
			int n = tPEdorUWSpecUI.mErrors.getErrorCount();
			Content = " 核保特约失败，原因是: " + tPEdorUWSpecUI.mErrors.getError(0).errorMessage;
			FlagStr = "Fail";
		}
		//如果在Catch中发现异常，则不从错误类中提取错误信息
		if (FlagStr == "Fail")
		{
		    tError = tPEdorUWSpecUI.mErrors;
		    if (!tError.needDealError())
		    {                          
		    	Content = " 续保核保特约成功! ";
		    	FlagStr = "Succ";
		    }
		    else                                                                           
		    {
		    	FlagStr = "Fail";
		    }
		}
	} 
}
catch(Exception e)
{
	e.printStackTrace();
	Content = Content.trim()+"提示：异常终止!";
}
%>                    
                 
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

