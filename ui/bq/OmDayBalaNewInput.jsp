<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：OmDayBalaNewInput.jsp
//程序功能：万能可转投资净额日结报表（新）
//创建日期：2010-10-25 
//创建人  ：zhanggm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="OmDayBalaNewInput.js"></SCRIPT>
  <%@include file="OmDayBalaNewInit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form  method=post name=fm target="fraSubmit" action="OmDayBalaNewDownload.jsp">
    <table  class= common>
      <tr> 
    	<td class= titleImg>查询条件</td>   		 
      </tr>
      <tr  class= common>
        <td  class= title>管理机构</td>
        <td  class= input><Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" readonly><Input class=codename readonly name=ManageComName></td>
        <td class= title>起始时间</td>
        <td  class= input><input class=coolDatePicker3 name="StartDate" verify="起始时间|date&notnull" elementtype="nacessary"  style="width:130"></td>
        <td class= title>终止时间</td>
        <td  class= input><input class=coolDatePicker3 name="EndDate" verify="终止时间|date&notnull"  elementtype="nacessary"  style="width:130"></td>
      </tr>
      <tr  class= common>
        <td  class= title>险种编码</td>
        <td  class= input colspan=3><Input class= "codeno"  name=RiskCode  ondblclick="return showCodeList('omrisk',[this,RiskName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('omrisk',[this,RiskName],[0,1],null,null,null,1);" ><Input class=codename readonly name=RiskName style="width:250">
        <input class=cssButton  VALUE="清空险种"  TYPE=button onclick="clearRisk()"></td>
      </tr>
    </table>
    
  <br>
  <input class=cssButton  VALUE="下载报表"  TYPE=button onclick="Download();">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>