
<%
  //程序名称：bqWorkFeeLis.jsp
  //程序功能：保单管理收付清单
  //创建日期：2005-10-10
  //创建人  ：yanchao
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.report.f1report.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<html>
<title>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
ReportUI
</title>
<head></head>
<body>
<%
  String flag = "0";
  String FlagStr = "";
  String Content = "";
  String state1 = " ";
  String state2 = " ";
  String feeType1 = " ";
  String feeType2 = " ";
  String com = " ";
  String group1 = " ";
  String group2 = " ";
  String usercode1 = " ";
  String usercode2 = " ";
  //设置模板名称
  String FileName = "bqWorkFeeLis";
  String StartDate = request.getParameter("StartDate");
  System.out.println(StartDate);
  String EndDate = request.getParameter("EndDate");
  System.out.println(EndDate);
  String feeState = request.getParameter("feeState");
  System.out.println(feeState);
  String feeType = request.getParameter("feeType");
  System.out.println(feeType);
  String feeTypeName = request.getParameter("feeTypeName");
  System.out.println(feeTypeName);
  String organcode = request.getParameter("organcode");
  System.out.println(organcode);
  String organname = request.getParameter("organname");
  System.out.println(organname);
  String Groupno = request.getParameter("Groupno");
  System.out.println(Groupno);
  String Groupname = request.getParameter("Groupname");
  System.out.println(Groupname);
  String usercode = request.getParameter("usercode");
  System.out.println(usercode);
  String username = request.getParameter("username");
  System.out.println(username);
  String sd = AgentPubFun.formatDate(StartDate, "yyyyMMdd");
  String ed = AgentPubFun.formatDate(EndDate, "yyyyMMdd");
  if (sd.compareTo(ed) > 0) {
    flag = "1";
    FlagStr = "Fail";
    Content = "操作失败，原因是:统计止期比统计统计起期早";
  }
  if (!(feeState == null || feeState.equals(""))) {
    state1 = "and '" + feeState + "'='1'";
    state2 = "and '" + feeState + "'='0'";
  }
  System.out.println(state1 + state2);
  if (!(feeType == null || feeType.equals(""))) {
    feeType1 = "and '" + feeType + "'='1'";
    feeType2 = "and '" + feeType + "'='0'";
  }
  System.out.println(feeType1 + feeType2);
  if (!(organcode == null || organcode.equals(""))) {
    String tManage = "";
    if (organcode.length() >= 4) {
      tManage = organcode.substring(0, 4) + "0000";
      System.out.println("tManage=" + tManage);
      System.out.println("organcode=" + organcode);
    }
    else {
      tManage = organcode + "0000";
    }
    if (organcode.equals(tManage)) {
      if (organcode.equals("86000000")) {
        com = "and lp.ManageCom =substr('" + organcode + "',1,2)";
      }
      else
        com = "and lp.ManageCom =substr('" + organcode + "',1,4)";
    }
    else {
      com = "and lp.ManageCom like '" + organcode + "%'";
    }
  }
  System.out.println(com);
  if (!(Groupno == null || Groupno.equals(""))) {
    group1 = "and (select acceptorno from lgwork where workno=sp.otherno) in (select memberno from lggroupmember where groupno='" + Groupno + "')";
    group2 = "and (select acceptorno from lgwork where workno=incomeno) in (select memberno from lggroupmember where groupno='" + Groupno + "')";
  }
  System.out.println(group1 + group2);
  if (!(usercode == null || usercode.equals(""))) {
    usercode1 = "and (select acceptorno from lgwork where workno=sp.otherno)= '" + usercode + "'";
    usercode2 = "and (select acceptorno from lgwork where workno=incomeno)='" + usercode + "'";
  }
  System.out.println(group1 + group2);
  JRptList t_Rpt = new JRptList();
  String tOutFileName = "";
  String strVFPathName = "";
  if (flag.equals("0")) {
    t_Rpt.m_NeedProcess = false;
    t_Rpt.m_Need_Preview = false;
    t_Rpt.mNeedExcel = true;
    StartDate = AgentPubFun.formatDate(StartDate, "yyyy-MM-dd");
    EndDate = AgentPubFun.formatDate(EndDate, "yyyy-MM-dd");
    String CurrentDate = PubFun.getCurrentDate();
    CurrentDate = AgentPubFun.formatDate(CurrentDate, "yyyy-MM-dd");
    t_Rpt.AddVar("StartDate", StartDate);
    t_Rpt.AddVar("EndDate", EndDate);
    t_Rpt.AddVar("state1", state1);
    t_Rpt.AddVar("state2", state2);
    t_Rpt.AddVar("feeType1", feeType1);
    t_Rpt.AddVar("feeType2", feeType2);
    t_Rpt.AddVar("feeTypeName", feeTypeName);
    t_Rpt.AddVar("organname", organname);
    t_Rpt.AddVar("com", com);
    t_Rpt.AddVar("Groupname", Groupname);
    t_Rpt.AddVar("group1", group1);
    t_Rpt.AddVar("group2", group2);
    t_Rpt.AddVar("username", username);
    t_Rpt.AddVar("usercode1", usercode1);
    t_Rpt.AddVar("usercode2", usercode2);
    String YYMMDD = "";
    YYMMDD = StartDate.substring(0, StartDate.indexOf("-")) + "年"
        + StartDate.substring(StartDate.indexOf("-") + 1, StartDate.lastIndexOf("-")) + "月"
        + StartDate.substring(StartDate.lastIndexOf("-") + 1) + "日";
    t_Rpt.AddVar("StartDateN", YYMMDD);
    YYMMDD = "";
    YYMMDD = EndDate.substring(0, EndDate.indexOf("-")) + "年"
        + EndDate.substring(EndDate.indexOf("-") + 1, EndDate.lastIndexOf("-")) + "月"
        + EndDate.substring(EndDate.lastIndexOf("-") + 1) + "日";
    t_Rpt.AddVar("EndDateN", YYMMDD);
    t_Rpt.Prt_RptList(pageContext, FileName);
    tOutFileName = t_Rpt.mOutWebReportURL;
    String strVFFileName = FileName + tOutFileName.substring(tOutFileName.indexOf("_"));
    String strRealPath = application.getRealPath("/web/Generated").replace('\\', '/');
    strVFPathName = strRealPath + "/" + strVFFileName;
    System.out.println(strVFPathName);
    System.out.println("=======Finshed in JSP===========");
    System.out.println(tOutFileName);
  }
%>
<form name="fm" method="post" action="../web/ShowF1Report.jsp">
<input name="FileName" type="hidden" value="<%=tOutFileName%>">
<input name="RealPath" type="hidden" value="<%=strVFPathName%>">
</form>
</body>
</html>
<script language="javascript">
var flag1 = <%=flag%>;

if (flag1 == '0')
{
  var rptError=" ";
  rptError = "<%=t_Rpt.m_ErrString%>";

  if (rptError ==" " || rptError =="" )
  {
    var ss = document.all("fm").FileName.value;
    fm.submit();
  }
  else
  {
    alert("rptError:"+rptError);
  }
}
else
{
	alert("<%=Content%>");
}
</script >