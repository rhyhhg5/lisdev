var turnPage = new turnPageClass();
var showInfo;

function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}

//查询
function quertData()
{
  	var comInfo=fm.MngCom.value;
  	var code = fm.MngCom.value ;
  	
  	if(fm.MngCom.value == "" || fm.MngCom.value == null)
  	{
  		alert("请选择管理机构！");
  		return ;
  	}
	var strSql=" select (case LevelCode1 when '1' then '全部' when '2' then '三级' when '3' then '二级' when '4' then '一级' end),"
		+"(select codename from ldcode where codetype = 'hospitalclass' and code = LevelCode2 ),AreaName,Year,AverageCharge,AverageDate ,FeeScale,makedate,maketime ,LevelCode1,LevelCode2,AreaCode, ManageCom ,HospitalCount,ClinicCount,DataSource from LDAreaHospital where ManageCom like '"
		+comInfo+"%' order by AreaCode,Year desc";
	fm.comInfo.value = 	comInfo ;

	turnPage.queryModal(strSql, AreaHospitalGrid);

}

//保存
function saveData()
{
	fm.Count.value = AreaHospitalGrid.mulLineCount;
	
	if(AreaHospitalGrid.checkValue()==false)return false;

	if(!checkHClass()) return false;

	if(fm.MngCom.value.length<8)			
	{
		alert('医院归属必须到三级机构，请修改管理机构');
		return false;	
	}			
	if(fm.Count.value == 0)
	{
		fm.fmtransact.value="DELETE||MAIN";
		
		if (!confirm("您确实想删除该机构下的地区医疗记录吗?"))
		{
			return ;
		}
	}
	else
	{
		fm.fmtransact.value="DELETE&&INSERT||MAIN";
	}
	
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action='./LDAreaHospitalSave.jsp';
	fm.submit(); //提交
}

//校验 医院级别1和2 只能选择一个
function checkHClass()
{
	var index = AreaHospitalGrid.mulLineCount;
	for ( var i = 0;i<index ;i++)
	{
		var hclass1 = AreaHospitalGrid.getRowColData(i,1);
		var hclass2 = AreaHospitalGrid.getRowColData(i,2);		
		
		if(hclass1.length==0)
		{
			AreaHospitalGrid.setRowColData(i,10,"")
		}
		if(hclass2.length==0)
		{
			AreaHospitalGrid.setRowColData(i,11,"")
		}
		var hclasscode1 = AreaHospitalGrid.getRowColData(i,10);
		var hclasscode2 = AreaHospitalGrid.getRowColData(i,11);	
			
		if( hclasscode1!="" && hclasscode2!="" )
		{
			alert('"医院级别1"和"医院级别2"不能兼选，只能选择一项!');
			return false;
		}		
	}
	return true;
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	AreaHospitalGrid.clearData();
}

