<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-01-15 14:25:18
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="LDHospitalInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LDHospitalInputInit.jsp"%>
	</head>

	<%
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
	%>
	<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
  var msql = "1 and char(length(trim(comcode)))=#8#"
</script>

	<body onload="initForm(); initElementtype();">
		<form action="./LDHospitalSave.jsp" method=post name=fm
			target="fraSubmit">
			<%@include file="../common/jsp/OperateButton.jsp"%>
			<%@include file="../common/jsp/InputButton.jsp"%>
			<Div id="divQueryButton" style="display: 'none'" align=right>
				<table>
					<tr>
						<td class=button width="10%" align=right>
							<INPUT class=cssButton name="querybutton1" VALUE="查  询"	TYPE=button onclick="return queryClick();">
						</td>
					</tr>
				</table>
				<hr>
			</Div>
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divLDHospital1);">
					</td>
					<td class=titleImg>名称设置</td>
				</tr>
			</table>
			<Div id="divLDHospital1" style="display: ''">
				<table class=common align='center'>
					<TR class=common>
						<TD class=title>
							医疗服务机构类型
						</TD>
						<TD class=input>
							<Input class=codeno name=HospitalType
								CodeData="0|^医疗机构|1^健康体检机构|2^健康管理公司|3^紧急救援公司|4^指定口腔医疗机构|5^其他合作机构|6" ondblClick="showCodeListEx('HospitalType',[this,HospitalType_ch],[1,0],null,null,null,1);"
								onkeyup="showCodeListKeyEx('HospitalType',[this,HospitalType_ch],[1,0],null,null,null,1);"><Input class=codename name=HospitalType_ch elementtype=nacessary
								verify="医疗服务机构类型|NOTNULL&len<=20">
						</TD>
						<TD class=title>
							医疗服务机构名称
						</TD>
						<TD class=input colSpan=3>
							<Input class='common' name=HospitName style="width:426px"
								elementtype=nacessary verify="医疗服务机构名称|NOTNULL&len<=50 ">
						</TD>

					</TR>
					<TR class=common>
						<TD class=title>
							医疗服务机构代码
						</TD>
						<TD class=input>
							<Input class='code' name=HospitCode elementtype=nacessary
								verify="机构代码|notnull&len<=20" ondblclick="getNoNew();">
						</TD>
						<TD class=title>
							机构简称或别名
						</TD>
						<TD class=input colSpan=3>
							<Input class='common' name=HospitShortName style="width:426px"
								verify="机构简称或别名|len<=50">
						</TD>
					</TR>
					<TR class=common>

						<TD class=title>
							管理机构
						</TD>
						<TD class=input>
							<Input class="codeno" name=MngCom
								ondblclick="return showCodeList('comcode',[this,MngCom_ch],[0,1],null,'','',1);"
								onkeyup="return showCodeListKey('comcode',[this,MngCom_ch],[0,1],null,'','',1);"
								readonly verify="管理机构|code:comcode&NOTNULL"><Input class=codename name=MngCom_ch>
						</TD>

						<TD class=title>
							地区名称
						</TD>
						<TD class=input>
							<Input class="codeno" name=AreaCode
								ondblclick="return showCodeList('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);"
								onkeyup="return showCodeListKey('hmareaname',[this,AreaName],[1,0],null,fm.AreaName.value,'codename',1);"
								verify="地区名称|notnull&len<=20"><Input class=codename name=AreaName elementtype=nacessary>
						</TD>


					</TR>
				</table>
			</Div>
			<Div id=CurrentlyInfo style="display: ''">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;">
						</td>
						<td class=titleImg>
							一般属性
						</td>
					</tr>
				</table>
				<TABLE class=common align='center'>
					<TR class=common>
						<TD class=title>
							详细地址
						</TD>
						<TD class=input>
							<Input class='common' name=Address verify="详细地址|len<=80">
						</TD>
						<TD class=title>
							联系电话
						</TD>
						<TD class=input>
							<Input class='common' name=Phone verify="联系电话|len<=18">
						</TD>
						<TD class=title>
							邮编
						</TD>
						<TD class=input>
							<Input class='common' name=ZipCode verify="邮编|len<=6&INT">
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							网址
						</TD>
						<TD class=input>
							<Input class='common' name=WebAddress verify="网址|len<=100">
						</TD>
						<TD class=title>
							乘车路线
						</TD>
						<TD class=input class=input colSpan=3>
							<Input class='common' name=Route verify="乘车路线|len<=1000"	style="width:419px">
						</TD>

					</TR>
				</table>
			</div>
			<Div id=servPart style="display: 'none'">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;">
						</td>
						<td class=titleImg>
							业务属性
						</td>
					</tr>
				</table>
				<table class=common align='center'>
					<TR class=common>
						<TD class=title>
							社保定点属性
						</TD>
						<TD class=input>
							<Input class='codeno' name=CommunFixFlag verify="社保定点属性|len<=20"
								CodeData="0|^0|否^1|是"
								ondblClick="showCodeListEx('CommunFixFlag',[this,CommunFixFlag_ch],[0,1]);"
								onkeyup="showCodeListKeyEx('CommunFixFlag',[this,CommunFixFlag_ch],[0,1]);"><Input class=codename name=CommunFixFlag_ch verify="社保定点属性|NOTNULL" elementtype=nacessary>
						</TD>
						<TD class=title>
							所属系统
						</TD>
						<TD class=input>
							<Input class='codeno' name=AdminiSortCode
								ondblClick="showCodeList('hmadminisortcode',[this,AdminiSortCode_ch],[0,1]);"><Input class=codename name=AdminiSortCode_ch verify="|len<=20">
						</TD>


						<TD class=title>
							经济类型
						</TD>
						<TD class=input>
							<Input class='codeno' name=EconomElemenCode
								ondblClick="showCodeList('hmeconomelemencode',[this,EconomElemenCode_ch],[0,1],null,null,null,1,200);"
								onkeyup="showCodeListKey('hmeconomelemencode',[this,EconomElemenCode_ch],[0,1]);"><Input class=codename name=EconomElemenCode_ch verify="|len<=20">
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							医院等级
						</TD>
						<TD class=input>
							<Input class='codeno' name=LevelCode
								ondblClick="showCodeList('hospitalclass',[this,LevelCode_ch],[0,1]);"
								onkeyup="showCodeListKeyEx('hospitalclass',[this,LevelCode_ch],[0,1]);"><Input class=codename name=LevelCode_ch verify="医院等级|len<=20&NOTNULL" elementtype=nacessary >
						</TD>
						<TD class=title>
							业务类型
						</TD>
						<TD class=input>
							<Input class='codeno' name=BusiTypeCode
								ondblClick="showCodeList('hmBusiType',[this,BusiTypeCode_ch],[0,1]);"
								onkeyup="showCodeListKey('hmBusiType',[this,BusiTypeCode_ch],[0,1]);"><Input class=codename name=BusiTypeCode_ch verify="业务类型|len<=20">
						</TD>
						<TD class=title>合作级别</TD>
						<TD class=input>
							<Input type=hidden name=AssociateClass verify="合作级别|notnull">
							<Input class='code' readonly name=AssociateClass_ch
								elementtype=nacessary verify="合作级别|len<=20&notnull"
								ondblClick="alert('请填写推荐医院记录便可自动生成合作级别');">
							<!--ondblClick= "showCodeList('llhospiflag',[this,AssociateClass],[1,0]);" -->
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							医院完全码
						</TD>
						<TD class=input>
							<Input class='common' name=HospitalStandardCode>
						</TD>
						<TD class=title>
							社保代码
						</TD>
						<TD class=input>
							<Input class='common' name=SecurityNo>
						</TD>
						<TD class=title id=UrbanName style="display: 'none'">城乡属性</TD>
						<TD class=input id=UrbanOpt style="display: 'none'">
							<Input class='codeno' name=UrbanOption CodeData="0|^0|非城区^1|城区"
								ondblClick="showCodeListEx('urbanOption',[this,UrbanOptionName],[0,1],null,null,null,1);"
								onkeyup="showCodeListKeyEx('urbanOption',[this,UrbanOptionName],[0,1],null,null,null,1);"><Input class=codename name=UrbanOptionName>
						</TD>					
					</TR>
					<TR class=common>
						<TD class=input>
							核保体检医院<input type="checkbox"  name="examination" value = '0' onclick="PayQuery()" />
						</TD>
						<TD class=title></TD>
						<TD class=input>
							<TD class=input />
						</TD>	
						<TD class=title></TD>
						<TD class=input>
							<TD class=input />
						</TD>						
											
					</TR>					
					
				</table>
			</div>
			<Div id=hospitalSrcInfo style="display: 'none'">
				<table>
					<tr>
						<td>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,divLDHospital2);">
						</td>
						<td class=titleImg>
							医疗资源
						</td>
					</tr>
				</table>
				<Div id="divLDHospital2" style="display: ''">
					<table class=common align='center'>
						<TR class=common>
							<TD class=title>
								床位数（张）
							</TD>
							<TD class=input>
								<Input class='common' name=BedAmount verify="床位数（张）|len<=200">
							</TD>
							<TD class=title>
								日均门诊量（人次）
							</TD>
							<TD class=input>
								<Input class='common' name=PatientPerDay
									verify="日均门诊量（人次）|len<=200">
							</TD>
							<TD class=title>
								年均出院人数（人次）
							</TD>
							<TD class=input>
								<Input class='common' name=OutHospital
									verify="年均出院人数（人次）|len<=200">
							</TD>
						</TR>
						<table>
							<tr>
								<td class=common>
									<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
										OnClick="showPage(this,specSecDia1);">
								</td>
								<td class=titleImg>
									诊疗特色
								</td>
							</tr>
						</table>
						<!-- 信息（列表） -->
						<Div id="specSecDia1" style="display:'' ">
							<div id="divSpecSecDia"
								style="display: none; position:absolute; slategray">
								<textarea name="SpecSecDia" cols="100%" rows="3" witdh=25%
									class="common" onkeydown="backSpecSecDia();"></textarea>
								<input type=button class=cssbutton value="返  回"
									onclick="backSpecSecDia2();">
							</div>
							<table class=common>
								<tr class=common>
									<td text-align:left colSpan=1>
										<span id="spanSpecSecDiaGrid"> </span>
									</td>
								</tr>
							</table>
						</div>
						<Div id="LHInstrumentInfo" style="display:'none' ">
							<table>
								<tr>
									<td class=common>
										<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
											OnClick="showPage(this,instrument1);">
									</td>
									<td class=titleImg>
										大型仪器设备
									</td>
								</tr>
							</table>
							<!-- 信息（列表） -->
							<Div id="instrument1" style="display:''">
								<div id="divInstrument"
									style="display: none; position:absolute; slategray">
									<textarea name="Instrument" cols="100%" rows="3" witdh=25%
										class="common" onkeydown="backInstrument();"></textarea>
									<input type=button class=cssbutton value="返  回"
										onclick="backInstrument2();">
								</div>
								<table class=common>
									<tr class=common>
										<td text-align:left colSpan=1>
											<span id="spanInstrumentGrid"> </span>
										</td>
									</tr>
								</table>
							</div>
						</div>						
					</table>
				</Div>
			</Div>
			<Div id="divcomHospital1" style="display:'none' ">
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
								OnClick="showPage(this,comHospital1);">
						</td>
						<td class=titleImg>
							合作级别
						</td>
					</tr>
				</table>
				<!-- 信息（列表） -->
				<Div id="comHospital1" style="display:'' ">
					<table class=common>
						<tr class=common>
							<td text-align:left colSpan=1>
								<span id="spanComHospitalGrid"> </span>
							</td>
						</tr>
					</table>
				</div>
			</div>			
			<Div id="div1" style="display: 'none'">
				<table class=common>
					<TD class=title>
						上级医疗机构
					</TD>
					<TD class=input>
						<Input class='code' name=SuperHospitCode
							ondblclick="getSuperHospitCode();return showCodeListEx('SuperHospitCode',[this],[0],'', '', '', true);"
							onkeyup="getSuperHospitCode();return showCodeListKeyEx('HospitName',[this],[0],'', '', '', true);">
					</TD>
					<TD class=title>
						最近修改日期
					</TD>
					<TD class=input>
						<Input class='coolDatePicker' name=LastModiDate dateFormat="short">
					</TD>
				</table>
			</Div>
			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="fmAction" name="fmAction">
			<input type=hidden name="SpecialClass">
			<input type=hidden name="Operator">
			<input type=hidden name="MakeDate">
			<input type=hidden name="MakeTime">
			<input type=hidden name="ModifyDate">
			<input type=hidden name="ModifyTime">
			<input type=hidden name="LoadFlag">
			<input type=hidden name=HiddenBtn>
			<Input type=hidden name=FlowNo ondblclick="generateFlowNo();">
		</form>
		<form action="./HospitalInfoSave.jsp" method=post name=fmlode
			target="fraSubmit" ENCTYPE="multipart/form-data">
			<table class=common>
				<TR>
					<TD width='8%' style="font:9pt">文件名：</TD>
					<TD width='80%'>
						<Input type="file" 　width="100%" name=FileName class=common>
						<INPUT VALUE="医院信息导入" class="cssbutton" TYPE=button
							onclick="HospitalListUpload();"><Input type="hidden" 　width="100%" name="insuredimport" value="1">
					</TD>
				</TR>

				<input type=hidden name=ImportFile>
			</table>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
