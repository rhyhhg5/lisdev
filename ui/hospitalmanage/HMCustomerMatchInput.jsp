<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2010-01-15 14:25:18
//创建人  ：caosg程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>

	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="HMCustomerMatchInput.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="HMCustomerMatchInputInit.jsp"%>
</head>

<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
%>
<script>
	var comCode = "<%=tG.ComCode%>";
	var manageCom = "<%=tG.ManageCom%>";
	var operator = "<%=tG.Operator%>";
</script>

<body onload="initForm(); initElementtype();">
	<form action="./HMCustomerMatchSave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data">		
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divImpFile);">
				</td>
				<td class=titleImg>医院数据导入识别</td>
			</tr>
		</table>
		<Div id="divImpFile" style="display: ''">	
			<table class=common>
				<TR class=common>
					<TD width='8%' style="font:9pt">选择导入文件</TD>
					<TD width='80%'>
						<Input type="file" 　width="100%" name=FileName class=common>
						<Input type="hidden" 　width="100%" name="insuredimport" value="1">
					</TD>					
				</TR>
			</table>			
		</Div>
		
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divCondition);">
				</td>
				<td class=titleImg>识别条件设定</td>
			</tr>
		</table>
		
		<Div id="divCondition" style="display: ''">
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>管理机构</TD>
					<TD class=input>
						<Input class="codeno" name=MngCom ondblclick="return showCodeList('comcodeall',[this,MngCom_ch],[0,1]);"
							onkeyup="return showCodeListKey('comcodeall',[this,MngCom_ch],[0,1]);"	
							readonly ><Input class=codename name=MngCom_ch>
					</TD>
					<TD class=title>业务渠道</TD>
					<TD class=input>
						<Input class=codeno name=TradeChannel 
							CodeData="0|^个险|1^团险|2^银行渠道|3" ondblClick="showCodeListEx('TradeChannel',[this,TradeChannel_ch],[1,0],null,null,null,1);"
							onkeyup="showCodeListKeyEx('HospitalType',[this,TradeChannel_ch],[1,0],null,null,null,1);"><Input class=codename name=TradeChannel_ch >										
					</TD> 
					<TD class=title></TD>
					<TD class=input></TD>										
				</TR>
				<TR class=common>
					<TD class=title>医疗险种类型</TD>
					<TD class=input>
						<Input class=codeno name=HMRiskType
							CodeData="0|^团险医疗|1^个险医疗|2" ondblClick="showCodeListEx('HMRiskType',[this,HMRiskType_ch],[1,0],null,null,null,1);"
							onkeyup="showCodeListKeyEx('HospitalType',[this,HMRiskType_ch],[1,0],null,null,null,1);"><Input class=codename name=HMRiskType_ch >					
					</TD>
					<TD class=title></TD>
					<TD class=input></TD>
					<TD class=title></TD>
					<TD class=input></TD>										
				</TR>								
			</table>
		</Div>								
		<br>
			<Input value="识别文件下载" type=button class=cssbutton onclick="fileDownload();">	
	    <Div id=DivFileDownload style="display:''">
	      <A id=fileUrl href=""></A>
	    </Div>				
		<br><br>	
		<INPUT VALUE="" TYPE=hidden name=downflag>
		<INPUT VALUE="" TYPE=hidden name=Url>
		<!-- 隐藏域 -->
		<input type=hidden name=ImportFile>
	</form>	
	
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>

</html>
