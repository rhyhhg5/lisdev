<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDHospitalSave.jsp
//程序功能：
//创建日期：2005-01-15 14:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.hospitalmanage.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="org.apache.commons.fileupload.*"%>  
  
<%
	//接收信息，并作校验处理。
	//输入参数

	System.out.println("---------接收信息，并作校验处理。------------");
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";		
	String FileName = "";
	String flag = "";
	int count = 0;
	
	//得到excel文件的保存路径
 	//String path = application.getRealPath(subPath);
 	String ImportPath = "/temp/hospitalmanage/";	
	String serverUrl = request.getParameter("Url");
	String MngCom = request.getParameter("MngCom");
	String TradeChannel = request.getParameter("TradeChannel");
	String HMRiskType = request.getParameter("HMRiskType");
		 		
 	String path = application.getRealPath("").replace('\\','/')+'/';			
	System.out.println("ImportPath:"+ImportPath);
	System.out.println("path:"+path);
	System.out.println("serverUrl:"+serverUrl);
	System.out.println("MngCom:"+MngCom);
	System.out.println("TradeChannel:"+TradeChannel);
	System.out.println("HMRiskType:"+HMRiskType);
		
	DiskFileUpload fu = new DiskFileUpload();
	// 设置允许用户上传文件大小,单位:字节
	fu.setSizeMax(10000000);
	// maximum size that will be stored in memory?
	// 设置最多只允许在内存中存储的数据,单位:字节
	fu.setSizeThreshold(4096);
	// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
	fu.setRepositoryPath(path+"temp");
	//开始读取上传信息

	List fileItems = null;
	try
	{	 
		fileItems = fu.parseRequest(request);	 
	}
	catch(Exception ex)
	{
		ex.printStackTrace();
	}

	// 依次处理每个上传的文件
	Iterator iter = fileItems.iterator();
	while (iter.hasNext()) 
	{
		FileItem item = (FileItem) iter.next();
		//忽略其他不是文件域的所有表单信息
		if (!item.isFormField()) 
		{
		    String name = item.getName();  
		    long size = item.getSize();
		    if((name==null||name.equals("")) && size==0)
				continue;
		    ImportPath= path + ImportPath;
		    FileName = name.substring(name.lastIndexOf("\\") + 1);
		    System.out.println("-----------importpath."+ImportPath + FileName);
		    
		    //设计 时 没有将导入与处理后的下载文件分成两步并进行管理，可以将上次导入的文件覆盖。
		    //File file = new File(ImportPath + FileName);
		    //if (file.exists()) 
		    //{
		    //    FlagStr = "Fail";
		    //    Content = "该文件已经导入过！";
		    //    break;
		    //}
		    
		    //保存上传的文件到指定的目录
		    try 
		    {
				item.write(new File(ImportPath + FileName));
				count = 1;
				System.out.println("count="+ count);
		    } 
		    catch(Exception e) 
		    {
				System.out.println("upload file error ...");
			}
		}
	}
	System.out.println("upload successfully");

	GlobalInput tG = new GlobalInput();
	TransferData tTransferData = new TransferData();	
	tG=(GlobalInput)session.getValue("GI");
	boolean res = true;
	HMCustomerMatch tHMCustomerMatch = new HMCustomerMatch();	
		
	if (count >0)
	{
		// 准备传输数据 VData
		System.out.println("------准备传输数据 VData--------");
	 	VData tVData = new VData();
	    tTransferData.setNameAndValue("FileName", FileName);
	    tTransferData.setNameAndValue("FilePath", ImportPath);
	    tTransferData.setNameAndValue("MngCom", MngCom);
	    tTransferData.setNameAndValue("TradeChannel", TradeChannel);	    
	    tTransferData.setNameAndValue("HMRiskType", HMRiskType);
	    	    	 	
		tVData.add(tTransferData);
	 	tVData.add(tG);
		try
		{	 	
		 	if(!tHMCustomerMatch.submitData(tVData, ""))
		 	{    		
		        Content = "保存失败，原因是:"+tHMCustomerMatch.mErrors.getFirstError() ;
		        FlagStr = "Fail";
		        res= false;
			}
			else
			{
				TransferData ssTransferData = (TransferData)tHMCustomerMatch.getResult().getObjectByObjectName("TransferData",0);			
				serverUrl = serverUrl+(String)ssTransferData.getValueByName("OutFileName");
				System.out.println("==outfile:"+serverUrl);
			}
		}
		catch(Exception ex)
		{
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		System.out.println("submitData Finished");
	}	
	else
	{
	    Content += "上载文件失败! ";
	    FlagStr = "Fail";
	}
	if (FlagStr.equals("Fail"))
	{
		res=false;
	}

	if (res)
	{
		Content = " 提交成功! ";
		FlagStr = "Succ";
		flag = "0";
	}
	else
	{
		Content = " 保存失败，原因是:" + Content;
		FlagStr = "Fail";
		flag = "1";
	}
  //添加各种预处理	
%>
<html>
<script language="javascript">

   	if ("<%=FlagStr%>" != "Fail")
   	{
   		parent.fraInterface.downAfterSubmit('<%=serverUrl%>',<%=flag%>);
   	}
   	if (<%=flag%> == 1)
   	{   	
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	}
	
</script>
</html>
