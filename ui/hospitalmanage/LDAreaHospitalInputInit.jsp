<%
//程序名称：LHTotalInfoQueryInit.jsp
//程序功能：客户综合信息查询(初始化页面)
//创建日期：2006-05-20 16:03:30
//创建人  ：hm
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。  
%> 
<Script language=javascript>
function initForm()
{
	try
	{  
		initInpBox();
		initAreaHospitalGrid();
	}
	catch(re)
	{
    	alert("LDAreaHospitalInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}  

function initInpBox()
{ 
	try
	{		
		fm.MngCom.value = manageCom;
		var codename = easyExecSql("select name from ldcom where comcode='"+manageCom+"' ");
		fm.all('MngCom_ch').value= codename==null?"":codename;
		fm.Count.value='';
	}
	catch(ex)
	{
		alert("在LDAreaHospitalInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}      
}	

function initAreaHospitalGrid() 
{ 
	var iArray = new Array();
    
	try 
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         		//列名
		iArray[0][1]="30px";         		//列名
		iArray[0][3]=0;         		//列名
		iArray[0][4]="station";         		//列名

		iArray[1]=new Array();            
		iArray[1][0]="医院级别1";              
		iArray[1][1]="120px";             
		iArray[1][2]=60;                  
		iArray[1][3]=2;                  
		iArray[1][10]="HClass";  //引用代码："CodeName"为传入数据的名称
		iArray[1][11]= "0|^1|全部|^2|三级|^3|二级|^4|一级" ; //"CodeContent" 是传入要下拉显示的代码
		iArray[1][12]="1|10";     //引用代码对应第几列，'|'为分割符
		iArray[1][13]="1|0";    //上面的列中放置引用代码中第几位值
		
		iArray[2]=new Array();            
		iArray[2][0]="医院级别2";              
		iArray[2][1]="120px";             
		iArray[2][2]=60;                  
		iArray[2][3]=2;                   
		iArray[2][4]="hospitalclass";
		iArray[2][5]="2|11";
		iArray[2][6]="1|0";				
			    
		iArray[3]=new Array(); 
		iArray[3][0]="地区名称";   
		iArray[3][1]="140px";   
		iArray[3][2]=60;        
		iArray[3][3]=2;
		iArray[3][4]="hmareaname";
		iArray[3][5]="3|12";
		iArray[3][6]="0|1";
		iArray[3][9]="地区名称|NOTNULL&len>0";
		iArray[3][15]="codename";
		iArray[3][17]="3";
		iArray[3][18]="200"; 
		iArray[3][19]="1" ;
		
		iArray[4]=new Array(); 
		iArray[4][0]="年度";   
		iArray[4][1]="80px";   
		iArray[4][2]=60;        
		iArray[4][3]=1;
		iArray[4][9]="年度|NOTNULL&len>0";
		
		iArray[5]=new Array(); 
		iArray[5][0]="地区年均住院费用";   
		iArray[5][1]="120px";   
		iArray[5][2]=20;        
		iArray[5][3]=1;
		
		iArray[6]=new Array();
		iArray[6][0]="地区年度平均住院日";
		iArray[6][1]="120px";         
		iArray[6][2]=20;            
		iArray[6][3]=1;             


		iArray[7]=new Array();
		iArray[7][0]="地区平均药品费占比";
		iArray[7][1]="120px";         
		iArray[7][2]=20;            
		iArray[7][3]=1; 
		
		iArray[8]=new Array();  //地区编码
		iArray[8][0]="makedate";
		iArray[8][1]="0px";         
		iArray[8][2]=20;            
		iArray[8][3]=3; 
		
		iArray[9]=new Array();  //地区编码
		iArray[9][0]="maketime";
		iArray[9][1]="0px";         
		iArray[9][2]=20;            
		iArray[9][3]=3; 
		
		iArray[10]=new Array();  //地区编码
		iArray[10][0]="HClass1";
		iArray[10][1]="0px";         
		iArray[10][2]=20;            
		iArray[10][3]=3; 		
		
		iArray[11]=new Array();  //地区编码
		iArray[11][0]="HClass2";
		iArray[11][1]="0px";         
		iArray[11][2]=20;            
		iArray[11][3]=3; 
				
		iArray[12]=new Array();  //地区编码
		iArray[12][0]="AreaCode";
		iArray[12][1]="0px";         
		iArray[12][2]=20;            
		iArray[12][3]=3;  		

		iArray[13]=new Array();  //地区编码
		iArray[13][0]="managecom";
		iArray[13][1]="0px";         
		iArray[13][2]=20;            
		iArray[13][3]=3; 

		iArray[14]=new Array();
		iArray[14][0]="地区住院总人次";
		iArray[14][1]="100px";         
		iArray[14][2]=20;            
		iArray[14][3]=1; 

		iArray[15]=new Array();
		iArray[15][0]="地区门诊总人次";
		iArray[15][1]="100px";         
		iArray[15][2]=20;            
		iArray[15][3]=1; 
		
		iArray[16]=new Array();
		iArray[16][0]="数据来源";
		iArray[16][1]="120px";         
		iArray[16][2]=20;            
		iArray[16][3]=1;  	
						
	    AreaHospitalGrid = new MulLineEnter( "fm" , "AreaHospitalGrid" ); 

	    AreaHospitalGrid.mulLineCount = 0;   
	    AreaHospitalGrid.displayTitle = 1;
	    AreaHospitalGrid.hiddenPlus = 0;
	    AreaHospitalGrid.hiddenSubtraction = 0;
	    AreaHospitalGrid.canSel = 0;
	    AreaHospitalGrid.canChk = 0;
	    //AreaHospitalGrid.selBoxEventFuncName = "showOne";	
	    AreaHospitalGrid.loadMulLine(iArray);  
	    //这些操作必须在loadMulLine后面
	    //AreaHospitalGrid.setRowColData(1,1,"asdf");
	}
	catch(ex) 
	{
    	alert(ex);
  	}
}

</script>
  