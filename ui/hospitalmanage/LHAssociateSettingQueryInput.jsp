<%
//程序名称：LHAssociateSettingQueryInput.jsp
//程序功能：功能描述
//创建日期：2006-03-15 13:04:48
//创建人  ：郭丽颖
//更新记录： 
// 更新人 : 
// 更新日期    : 
// 更新原因/内容: 
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHAssociateSettingQueryInput.js"></SCRIPT> 
  <%@include file="LHAssociateSettingQueryInputInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
  String no = request.getParameter("no");
  System.out.println("AAAAAAAAAAQQQQQQQQQQ "+no);
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
 
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "LHContraAssoSettingGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      团体合同编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNo verify="团体合同编号|len<=50">
    </TD>
    <TD  class= title>
      团体责任号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraItemNo verify="团体责任号|len<=50">
    </TD>
     <TD  class= title>
      个人合同编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=UnitContrano verify="个人合同编号|len<=50">
    </TD>
	</TR>
</table>
<table class = common style= "display: 'none'">
    <TD class= title>
    	个人合同编号）
    </TD>
    <TD  class= input>
    	<Input class= 'common' name=HospitCode >
    </TD>
    <TD  class= title>
      个人合同名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraType >
    </TD>
  </TR>
  <TR  class= common>
  	<TD  class= title>
      合作机构名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNowName >
    </TD>  
    <TD  class= title>
     签约人员名称
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=BalanceDate >
    </TD> 
    <TD  class= title>
      关联责任标识
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=BalanceRemindDate >
    </TD>
    </TR>
</table>
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT class =cssbutton VALUE="查询"   TYPE=button   class=common onclick="easyQueryClick();">
        <INPUT class =cssbutton VALUE="返回"   TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>  
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHContraAssoSettingGrid);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHContraAssoSettingGrid" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHContraAssoSettingGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
