/** 
 * 程序名称：LDHospitalInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-01-15 14:25:18
 * 创建人  ：ctrHTML
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() 
{
	if(showInfo!=null) 
	{
		try 
		{
			showInfo.focus();
		}
		catch(ex) 
		{
			showInfo=null;
		}
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) 
{
	showInfo.close();
	if (FlagStr == "Fail" ) 
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	else 
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		//parent.fraInterface.initForm();
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showDiv(operateButton,"true");
		showDiv(inputButton,"false");
		//执行下一步操作
	}
}
// 查询按钮
function easyQueryClick() 
{
	var LEN;
	if( manageCom.length == 8)  LEN = 4;
	else{LEN = manageCom.length;}
	var SpecialCode="";
	if(fm.all('SpecialCode').value != "")
	{
		SpecialCode =" and a.HospitCode in ( select distinct   c.HospitCode from  LDHospitalInfoIntro c where   c.SpecialName = '"+fm.all('SpecialCode').value+"')";	
	}
	var strSql = "select a.ManageCom, a.HospitCode, a.HospitName, (select codename  from ldcode where codetype = 'hospitalclass' and code = a.LevelCode), (case when a.CommunFixFlag = '1' then '是' else '否' end), (select codename from ldcode where codetype = 'llhospiflag' and code = a.AssociateClass), (case  when b.ContraState = '1' then '有效' when b.ContraState is null then '' else  '无效' end), b.IdiogrDate, b.ContraEndDate "
		+ "from ldhospital a left join LHGroupCont b on  (a.HospitCode = b.HospitCode " ;
	if(fm.ContFlag.value=='1' || fm.ContFlag.value=='2')
	{
		strSql = strSql + " and b.ContraState='"+fm.ContFlag.value+"'";
	}
		
	strSql = strSql + ") where  a.AreaCode like '"+fm.AreaCode.value+"%%' "	    
	    + getWherePart("a.HospitCode", "HospitCode")
	    + getWherePart("a.HospitName", "HospitName")
	    + getWherePart("a.Operator", "Operator")
	    + getWherePart("a.ModifyDate", "LastModifyDate")    
	    + getWherePart("a.LevelCode", "LevelCode") 
	    + getWherePart("a.AssociateClass","AssociateClass") 
	    + SpecialCode
	    + getWherePart("a.HospitalType","HospitalType") 
	if(fm.MngCom.value!=null && fm.MngCom.value!="")
	{
	    strSql = strSql+" and a.managecom like '"+fm.MngCom.value+"%%'"  ;
	}    
    //+" and a.managecom like '"+manageCom.substring(0,LEN)+"%%'"
  	strSql = strSql+" order by b.IdiogrDate "  ; 
  		  	
	turnPage.queryModal(strSql, LDHospitalGrid);
	
}

function showOne(parm1, parm2) 
{	
  //判断该行是否确实被选中
	alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
	var arrReturn = new Array();
	var tSel = LDHospitalGrid.getSelNo();
		
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录，再点击返回按钮。" );
		return false;
	}
	else
	{		
		try
		{	
			//alert(tSel);
			arrReturn = getQueryResult();
			top.opener.afterQuery0( arrReturn );
		}
		catch(ex)
		{
			alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		top.close();		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = LDHospitalGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LDHospitalGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
function getHospitCode()
{
    var strsql = "";
    var tCodeData = "0|";
    strsql = "select HospitCode,HospitName from LDHospital ";
    //alert("strsql :" + strsql);
    fm.all("SuperHospitCode").CodeData=tCodeData+easyQueryVer3(strsql, 1, 0, 1);
}