//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2011-06-02
//创建人  ：王洪亮
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------

var showInfo=null;
var turnPage=new turnPageClass();
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	  showInfo.close();
      var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	  window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	  if(FlagStr=="Fail")
      {
         return false
      }
      top.opener.initTextbox();
	  top.opener.queryClick(); 
      top.close();   
 
}

//-------------------添加时-------
function addClick()
{

   if( verifyInput() == false ) 
   {
      return false;
   }
   if(!checkData())
   {
	    return false;
   }
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
           var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
           showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
   fm.all("fmtransact").value="UPDATE||MAIN";
   fm.submit(); 
}   
function checkData()
{
   var Validate= fm.all('Validate').value;
   var CInvalidate=fm.all('CInvalidate').value;
   var getDate ="select current date from dual";
   var result = easyExecSql(getDate);
   if(result != "null" && result != "" && result != null)
   { 
      if(result[0][0]>Validate)
      {
         alert("医院配置属性生效日期不得早于当前日期！");
         return false;
      }
   }
   else
   {
      alert("没有找到当前日期！");
      return false;
   }
   if(trim(Validate)!=""&&trim(CInvalidate)!="")
   {
      if(CInvalidate<Validate)
      {
         alert("终止日期不能早于生效日期！");
         return false;
      }
   }
   //校验是否做出了修改
   var selHostCode=fm.all('HospitCode').value;
   var selManageCom=fm.all('ManageCom').value;
   var selRiskcode=fm.all('RiskCode').value;
   var sql="select Validate,CInvalidate from LDDingHospital  "
		     +" where 1=1 "
		     +" and HospitCode='"+selHostCode+"' "
		     +" and ManageCom='"+selManageCom+"' "
		     +" and Riskcode='"+selRiskcode+"' "
		     +" with ur "; 
   var arrResult = easyExecSql(sql); 
   if(arrResult==null)
   {
        alert("该医院不存在，不能修改！");
        return false;
   } 
   if((fm.all('Validate').value==arrResult[0][0])&&(fm.all('CInvalidate').value==arrResult[0][1]))    
   {
      alert("没有做出任何修改，不能保存！");
      return false;
   } 
   return true;
}        

