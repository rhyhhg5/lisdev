//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var xxxx;
window.onfocus=myonfocus;
var selectedDutyExolaiRow；
var selectedFeeExplaiRow；
var turnPage = new turnPageClass(); 
var logname;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	   var sqlContrano="select contrano from LHUnitContra where contrano='"+fm.all('ContraNo').value+"'";
     //alert(sqlContrano);
     // alert(easyExecSql(sqlContrano));
     contrano=easyExecSql(sqlContrano);
     if(fm.all('ContraNo').value==contrano)
     {
    	 alert("此合同编号的信息已经存在,不允许重复保存!");
     }
	    if(contrano==null||contrano==""||contrano=="null")
     {  
	       if(checkDate() == false)  return false;
         var i = 0;
         if( verifyInput2() == false ) return false;
         fm.fmtransact.value="INSERT||MAIN";
         var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
         var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
         showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
         fm.submit(); //提交
      }
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHContInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if(checkDate() == false)  return false;
  //下面增加相应的代码
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
  
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  //showInfo=window.open("./LHContQuery.html");
  showInfo=window.open("./LHContQuery.html","信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
	var rowNum=ContGrid.mulLineCount ; //行数 
	var i=0;
	for(i;i<rowNum;i++)
	{
			var k=(ContGrid.getRowColData(rowNum-1,4));
  }
           if(k=="是")
						{
							alert("此记录有关联记录，不允许删除!");
						}
						else
						{
								 if (confirm("您确实想删除该记录吗?"))
								 {
									  var i = 0;
									  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
									  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
									  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
									  
									  //showSubmitFrame(mDebug);
									  fm.fmtransact.value = "DELETE||MAIN";
									  fm.submit(); //提交
									  initForm();						  
								 }
								 else
								 {
								   alert("您取消了删除操作！");
								 }
            }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
//		arrResult = arrQueryResult;
//		alert(arrResult[0][0]);
//		var mulSql =  " select b.dutyitemcode,c.dutyitemname,b.dutystate,(select codename from ldcode where codetype='dutystate' and code ='"+b.dutystate+"' ),b.dutyexolai,b.feeexplai,a.contrano "
//								 +" from lhunitcontra a,lhcontitem b, LDContraItemDuty c  "
//								 +" where a.contrano = '"+arrResult[0][0]+"' and b.contrano= '"+arrResult[0][0]+"' and c.DutyItemCode = b.DutyItemCode";
//
//		turnPage.queryModal(mulSql,ContGrid);
//		
//        fm.all('ContraNo').value= arrResult[0][0];
//        fm.all('HospitCode').value= arrResult[0][1];
//        fm.all('ContraName').value= arrResult[0][2];
//        fm.all('IdiogrDate').value= arrResult[0][3];
//        fm.all('ContraBeginDate').value= arrResult[0][4];
//        fm.all('ContraEndDate').value= arrResult[0][5];
//        fm.all('ContraState').value= arrResult[0][6];
//        fm.all('FirstPerson').value= arrResult[0][7];
//        fm.all('SecondPerson').value= arrResult[0][8];
//        fm.all('linkman').value= arrResult[0][9];
//        fm.all('Phone').value= arrResult[0][10];
	}
}               
        
function inputDutyExolai(a)
{ 
	
	divDutyExolai.style.display='';
	var tempDutyExolai = fm.all(a).all('ContGrid5').value;
	selectedDutyExolaiRow = fm.all(a).all('ContGridNo').value-1;
	fm.all('textDutyExolai').value=tempDutyExolai;
	
}

function backDutyExolai()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		ContGrid.setRowColData(selectedDutyExolaiRow,5,fm.textDutyExolai.value);
		fm.textDutyExolai.value="";
		divDutyExolai.style.display='none';		
	}
}

function backDutyExolaiButton()
{ 
		ContGrid.setRowColData(selectedDutyExolaiRow,5,fm.textDutyExolai.value);
		fm.textDutyExolai.value="";
		divDutyExolai.style.display='none';		 
}

function inputFeeExplai(a)
{ 
	
	divFeeExplai.style.display='';
	var tempFeeExplai = fm.all(a).all('ContGrid6').value;
	selectedFeeExplaiRow = fm.all(a).all('ContGridNo').value-1;
	fm.all('textFeeExplai').value=tempFeeExplai;
	
}

function backFeeExplai()
{
	var keycode=event.keyCode;
	if ((keycode == "13")&&(event.ctrlKey == true)) {
		ContGrid.setRowColData(selectedFeeExplaiRow,6,fm.textFeeExplai.value);
		fm.textFeeExplai.value="";
		divFeeExplai.style.display='none';		
	}
}

function backFeeExplaiButton()
{ 
		ContGrid.setRowColData(selectedFeeExplaiRow,6,fm.textFeeExplai.value);
		fm.textFeeExplai.value="";
		divFeeExplai.style.display='none';		 
}

function afterQuery0( arrQueryResult )
{	
	 var sql="select  indivcontrano from LHContraAssoSetting "
	        +" where indivcontrano='"+arrQueryResult[0][0]+"'";
	 var item=easyExecSql(sql);
	// alert(item);
	  if(item==""||item=="null"||item==null) 
   	{

   		fm.all('HiddenBtn').value="";
   		initContGrid();
   	}
	  else
	  {
	    fm.all('HiddenBtn').value="1";
	    initContGrid();
	    fm.all('HiddenBtn').value="";
	  }
	 var arrResult = new Array();
	 if( arrQueryResult != null )
	 {
		 arrResult = arrQueryResult;
		 var strSql = "select ContraNo,ContraName,DoctNo,IdiogrDate, ContraBeginDate,ContraEndDate,ContraState,MakeDate,MakeTime from LHUnitContra where 1=1  and ContraNo='"+arrResult[0][0]+"'";  
     var arrContInput=easyExecSql(strSql);
        //alert(arrContInput[0][1]);   						  
        fm.all('ContraNo').value              = arrContInput[0][0];
        fm.all('ContraName').value            = arrContInput[0][1];      
        fm.all('DoctNo').value                = arrContInput[0][2];
        fm.all('DoctName').value              = easyExecSql("select DoctName from lddoctor where doctno = '"+arrContInput[0][2]+"'");
        fm.all('IdiogrDate').value            = arrContInput[0][3];
        fm.all('ContraBeginDate').value       = arrContInput[0][4];
        fm.all('ContraEndDate').value         = arrContInput[0][5];
        fm.all('ContraState').value           = arrContInput[0][6];
        if(arrContInput[0][6]=="1"){fm.all('ContraStateName').value="有效";}if(arrContInput[0][6]=="2"){fm.all('ContraStateName').value="无效";}
        fm.all('MakeDate').value              = arrContInput[0][7];
        fm.all('MakeTime').value              = arrContInput[0][8];
                                                           
           	var mulSql = "select (select Codename from ldcode where b.ContraType=ldcode.code  and ldcode.Codetype='hmdeftype'),"
                	+"(select c.DutyItemName from LDContraItemDuty c where c.DutyItemCode = b.dutyItemCode),"                                  
									+" (select case when b.dutystate = '1' then '有效' else '无效' end from dual),"
									+"( case when ( select distinct indivcontraitemno from LHContraAssoSetting where indivcontraitemno=b.ContraItemNo) is null then '否' else '是' end ),"
									//+"(select case when b.Contraflag = 'Y' then '是' else '否' end from ldcode where Codetype='yesno' and b.Contraflag=ldcode.code),"
									+"b.ContraType,b.DutyItemCode,b.dutystate,"
									+"b.Contraflag,b.ContraItemNo"
			  					+" from lhunitcontra a,lhcontitem b,LDContraItemDuty c "
        					+" where a.contrano ='"+arrResult[0][0]+"' and b.contrano= '"+arrResult[0][0]+"' and c.DutyItemCode = b.DutyItemCode"
        					;
        turnPage.pageLineNum = 200;
        turnPage.queryModal(mulSql,ContGrid);   
   	                                
	}
}      

function getCopy()
{
		showInfo=window.open("./ContMain.jsp?ContraNo="+fm.all('ContraNo').value);
}

function getService()//
{
	if(fm.all('ContraNo').value=="")
	{
		alert("你选择的编号为空，请重新选择!");
	}
	else
	{
			if (ContGrid.getSelNo() >= 1)
			{
				     //alert(ContGrid.getRowColData(ContGrid.getSelNo()-1,2));
				     if(ContGrid.getRowColData(ContGrid.getSelNo()-1,2)=="")
						 {
										alert("你选择了空的列，请重新选择!");
						  			return false;
							}
							else
							{
									 var ContraNo=fm.ContraNo.value;
								   var temp_no = ContGrid.getRowColData(ContGrid.getSelNo()-1,9);                                                                                                                                        
							     window.open("./LHServerPriceInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+temp_no+"&groupunit=2","服务价格表设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
		          }
		  } 
		  else
		  {
		  		alert("请选择一条要传输的记录！");    
		  }
	}
} 
function getCharge()//
{
	if(fm.all('ContraNo').value=="")
	{
		alert("你选择的编号为空，请重新选择!");
	}
	else
	{
			if (ContGrid.getSelNo() >= 1)               
			{    
				  if(ContGrid.getRowColData(ContGrid.getSelNo()-1,2)=="")
			  	{
								alert("你选择了空的列，请重新选择!");
						  	return false;
					}
					else
					{
						var ContraNo=fm.ContraNo.value;
					  var temp_no = ContGrid.getRowColData(ContGrid.getSelNo()-1,9);                                          
						window.open("./LHChargeBalanceInputMain.jsp?ContraNo="+ContraNo+"&ContraItemNo="+temp_no+"&groupunit=2","费用结算方式设置",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
			    }
			}
			else                             
		  {                                
				alert("请选择一条传输的记录！");   
		  } 
	}                               

}   

function getNewAssociate()
{
	//alert(fm.all('HiddenBtn').value);
	if(fm.all('HiddenBtn').value=="1")
	{
		returnParent2();
	}
	if(fm.all('HiddenBtn').value==null||fm.all('HiddenBtn').value=="null"||fm.all('HiddenBtn').value=="")
	{

		getAssociate();
			//returnParent2();
		//alert(22);
	}

}

function getAssociate()//关联设置
{
	
	if(fm.all('ContraNo').value=="")
	{
		alert("你选择的编号为空，请重新选择!");
	}
	else
	{
			if (ContGrid.getSelNo() >= 1)               
			{    
        
				  if(ContGrid.getRowColData(ContGrid.getSelNo()-1,2)=="")
			  	{
								alert("你选择了空的列，请重新选择!");
						  	return false;
					}
					else
					{
						var ContraNo=fm.ContraNo.value;
					  var temp_no = ContGrid.getRowColData(ContGrid.getSelNo()-1,9);       
			      var strSql = "select ContraNo,ContraItemNo from LHContraAssoSetting where 1=1  and IndivContraNo='"+ContraNo+"' and IndivContraItemNo='"+temp_no+"'";  
            //alert(strSql);
            var arrContInput=new Array();
            var arrContInput=easyExecSql(strSql);
            //alert(arrContInput);
            if(arrContInput!=""&&arrContInput!="null"&&arrContInput!=null)
            {
              var ContraNo2=arrContInput[0][0];
	            var ContraItemNo2=arrContInput[0][1];
	            window.open("./LHAssociateSettingInputMain.jsp?ContraNo="+ContraNo2+"&ContraItemNo="+ContraItemNo2+"&groupunit=4","返回至主责任",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');       
    	      }
    	    }
			}
			else                             
		  {                                
				alert("请选择一条传输的记录！");   
		  } 
	}           
} 

function compareDate(DateOne,DateTwo)
{
	var OneMonth = DateOne.substring(5,DateOne.lastIndexOf ("-"));
	var OneDay = DateOne.substring(DateOne.length,DateOne.lastIndexOf ("-")+1);
	var OneYear = DateOne.substring(0,DateOne.indexOf ("-"));
	
	var TwoMonth = DateTwo.substring(5,DateTwo.lastIndexOf ("-"));
	var TwoDay = DateTwo.substring(DateTwo.length,DateTwo.lastIndexOf ("-")+1);
	var TwoYear = DateTwo.substring(0,DateTwo.indexOf ("-"));
	
	if (Date.parse(OneMonth+"/"+OneDay+"/"+OneYear) >
	Date.parse(TwoMonth+"/"+TwoDay+"/"+TwoYear))
	{ 
		return true;
	}
	else
	{
		return false;
	}
}

function checkDate()
{
	if(fm.all('IdiogrDate').value != "" && fm.all('ContraBeginDate').value != "")
	{
			if(compareDate(fm.all('IdiogrDate').value,fm.all('ContraBeginDate').value) == true)
			{
				alert("合同签订时间应早于合同起始时间");
				return false;
			}
	}
	
	if(fm.all('ContraBeginDate').value != "" && fm.all('ContraEndDate').value != "")
	{
			if(compareDate(fm.all('ContraBeginDate').value,fm.all('ContraEndDate').value) == true)
			{
				alert("合同起始时间应早于合同终止时间");
				return false;
			}
	}
	if(fm.all('IdiogrDate').value != "" && fm.all('ContraEndDate').value != "")
	{
			if(compareDate(fm.all('IdiogrDate').value,fm.all('ContraEndDate').value) == true)
			{
				alert("合同签订时间应早于合同终止时间");
				return false;
			}
	}
}


//选择某一记录后,看是否有责任关联,从而控制按钮
function buttonFalse()
{
		var selNo = ContGrid.getSelNo();
//		alert(selNo);
	  var temp = ContGrid.getRowColData(selNo-1, 4);
  
		if(temp == "是")
		{
			     fm.getAssociateSetting.disabled = false;
		}
		else if(temp == "否")
		{
			     fm.getAssociateSetting.disabled = true;
		}

}
function getNo()
{
	  if(fm.all('DoctNo').value==""||fm.all('DoctNo').value==null|fm.all('DoctNo').value=="null")
    {
      alert("请您先选择签约人员代码！");
      return false;
    }
    var sql="select max(Contrano) from LHUnitContra where DoctNo='"+fm.all('DoctNo').value+"'";
    var result = easyExecSql(sql);
    var result2=parseInt(result)+1;
    var result3=result2.toString().substring(10);
    var result4;

     if(result3.length==4)
    {
      result4="2"+result3.toString();
    }
    else if(result3.length==3)
    {
    	result4="2"+"0"+result3.toString();
    }
    else if(result3.length==2)
    {
    	result4="2"+"00"+result3.toString();
    }
    else if(result3.length==1)
    {
    	result4="2"+"000"+result3.toString();
    }
    else
    {
    	result4="22"+result3.toString();
    }
    if(result==""||result==null|result=="null")
    {
       fm.ContraNo.value=fm.all('DoctNo').value+"2"+"000"+"1";
       return false;
    }
    else
    {
	   	fm.ContraNo.value=fm.all('DoctNo').value+result4;
	   
	  }
}

function returnParent2()
{
  var arrReturn = new Array();
	var tSel = ContGrid.getSelNo();
//	var tSel = fm.ContraNo.value;

	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
			try
			{	
			  //alert(fm.ContraNo.value);
				  arrReturn = getQueryResult();
			  //alert(arrReturn);
			  top.opener.focus(); 		
			  top.opener.afterQuery2(fm.ContraNo.value,arrReturn);
			  top.close();
       // parent.top.opener.afterQuery2(fm.ContraNo.value,arrReturn);
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口,返回关联页面!" + ex.toString() );
			}
			//top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ContGrid.getSelNo();

	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();

	//设置需要返回的数组
	arrSelected[0] = new Array();	
	arrSelected[0] = ContGrid.getRowData(tRow-1);
	arrSelected[0] = ContGrid.getRowColData(tRow-1,9);

	return arrSelected;
}