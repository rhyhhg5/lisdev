<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="./LDindHospitalInit.jsp"%>
<SCRIPT src="./LDindHospitalInput.js"></SCRIPT>

</head>
<script>
  var msql = "1 and #1# = #1# and length(trim(comcode))<=4"
</script>
<body  onload="initForm();initElementtype();" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./LDindHospitalSave.jsp" method="post" name="fm" target="fraSubmit">
  <!--div id="operateButton"-->
	  <table  class=common align=center>
	          <tr align=right>
	                  <td class=button>
	                      &nbsp;&nbsp;
	                  </td>
	                  <td class=button width="10%" align=right>
	                      <input class=cssButton name="saveButton" VALUE="添 加"  TYPE=button onclick="addClick();">
	                  </td>
	                  <td class=button width="10%" align=right>
	                      <input class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="updateClick();">
	                  </td>           
	                  <td class=button width="10%" align=right>
	                      <input class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="queryClick();">
	                  </td> 
	                  <td class=button width="10%" align=right>
	                  	  <input class=cssButton name="deletebutton" VALUE="删  除"  TYPE=hidden onclick="deleteClick(); ">
	                  </td>  
	          </tr>
	  </table>
  <!--/div--> 
  
    <table>
	   	  <tr>
	        <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHospital);"></td>
	    	<td class= titleImg>定点医院查询</td>
	      </tr>
    </table>
    
	<div  id= "divHospital" style= "display: ''">
	<table  class= common>
	    <tr  class= common> 
		    <td  class= title> 
					管理机构
			</td>
			<td  class=input>                                            
				<Input class="codeno" name=ManageCom  ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,msql,1,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,msql,1,1);" readonly><input class="codename" name="ComName"  readonly >
			</td> 
			<td  class= title> 
	        		医院名称
	        </td>  
	        <td  class= input>
					<input class="codeNo"  name="HospitCode" onclick="return showCodeList('llhospiquery',[this,HospitalName],[0,1],null,'#'+fm.ManageCom.value+'%# and HospitName like #%'+fm.HospitalName.value+'%','ManageCom');" onkeyup="return showCodeListKeyEx('llhospiquery',[this,HospitalName],[0,1],null,fm.ManageCom.value,'ManageCom',1,240);" verify="医院代码|notnull"  ><input class="codename" name="HospitalName" elementtype="nacessary" >
			</td>  
			 
			 <td  class= title> 
		         	险种代码
		     </td>
			 <td  class= input> 
			     <Input class="codeNo" name="RiskCode"  ondblclick="return showCodeList('riskcode',[this,RiskCodeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('riskcode',[this,RiskCodeName],[0,1],null,null,null,1);"><Input class="codeName" name="RiskCodeName"  > 
			 </td>          
			
	    </tr>
	    <tr  class= "common"> 
		    
	    
	         <td  class= title> 
	              医院配置属性生效日期
	         </td>
	         <td  class= input> 
	         	<input class= "coolDatePicker" dateFormat="short" name="ValiDate" >
	         </td>
	          <td  class= title> 
	              终止日期
	         </td>
	         <td  class= input> 
	         	<input class= "coolDatePicker" dateFormat="short" name="CInValiDate" >
	         </td>
	    </tr>

     </table>
     </div>
     <table>
	   	  <tr>
	        <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProblem);"></td>
	    	<td class= titleImg>定点医院清单</td>
	      </tr>
    </table>
      <center>
		 <div  id= "divProblem" style= "display: ''">
		 	<span id="spanDataGrid"></span>
		 	<input value="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
            <input value="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
            <input value="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
            <input value="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
	     </div>
     </center>
     
    <div  id= "divDataAfter" style= "display: 'none'"> 
    <table>
	   	  <tr>
	        <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProblem1);"></td>
	    	<td class= titleImg>历史清单</td>
	      </tr>
    </table>
      <center>
		 <div  id= "divProblem1" style= "display: ''">
		 	<span id="spanDataAfterGrid"></span>
		 	<input value="首  页" TYPE=button onclick="turnPage1.firstPage();" class="cssButton">
            <input value="上一页" TYPE=button onclick="turnPage1.previousPage();" class="cssButton">
            <input value="下一页" TYPE=button onclick="turnPage1.nextPage();" class="cssButton">
            <input value="尾  页" TYPE=button onclick="turnPage1.lastPage();" class="cssButton">
	     </div>
     </center>
     </div> 
      <input type="hidden" id="fmtransact" name="fmtransact" value="">
  
	  
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>