<%
//程序名称：LHHospitalInfoQuery.jsp
//程序功能：功能描述
//创建日期：2006-05-20 16:02:27
//创建人  ：hm
//更新人  ：  
%>

<%
 String HospitalCode = request.getParameter("HospitalCode");
%>

<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHHospitalInfoQuery.js"></SCRIPT> 
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHHospitalInfoQueryInit.jsp"%>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var manageCom = "<%=tG.ManageCom%>";
  var HospitalCode = "<%=HospitalCode%>";
  var msql = "1 and char(length(trim(comcode)))=#8#"   
</script>
<script>
function selected()
{
  	   var arrRiskCode=new Array;
  	   arrRiskCode=fm.txtRiskCode.value;
  	   fm.RelaNo.value=fm.txtRiskCode.value;
}
</script>
<body onload="initForm()">
<form method=post name=fm target=fraSubmit>	

<table class=common border=0 width=100%>
	<tr>
		<td class=titleImg align=center> 请输入查询条件： </td>
	</tr>
</table>
	
<Div  id= "divLDHospitalGrid1" style= "display: ''">    
	<table  class= common align='center' >
		<TR  class= common>
			<TD class=title>管理机构</TD>
			<TD class=input>
				<Input class="codeno" name=MngCom
					ondblclick="return showCodeList('comcode',[this,MngCom_ch],[0,1],null,'','',1);"
					onkeyup="return showCodeListKey('comcode',[this,MngCom_ch],[0,1],null,'','',1);" readonly verify="管理机构|code:comcode&NOTNULL"><Input class=codename name=MngCom_ch>
			</TD>						
			<TD  class= title>医疗服务机构类型</TD>
			<TD  class= input>
			  <Input class=codeno name=HospitalType  CodeData= "0|^医疗机构|1^健康体检机构|2^健康管理公司|3^紧急救援公司|4^指定口腔医疗机构|5^其他合作机构|6" 
			  ondblClick= "showCodeListEx('HospitalType',[this,HospitalType_ch],[1,0],null,null,null,1);" 
			  onkeyup= "showCodeListKeyEx('HospitalType',[this,HospitalType_ch],[1,0],null,null,null,1);"><Input class= codename name=HospitalType_ch  elementtype=nacessary  verify="医疗服务机构类型|NOTNULL&len<=20">
			</TD>
			<TD  class= title>医疗服务机构名称</TD>
			<TD  class= input>   	
			  <Input class= 'code' name= HospitName ondblclick="return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,300);" 
			  onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName',1,300); ">
			</TD>
		</TR>    
		<TR>    
			<TD  class= title>医疗服务机构代码</TD>
			<TD  class= input>
			     <Input class= 'common' name=HospitCode >
			</TD> 		    
		    <TD class= title>合作级别</TD>
			<TD  class= input>
		    	<Input class='codeno' name=AssociateClassQ 
		    	ondblclick="showCodeList('llhospiflag',[this,AssociateClassNameQ],[0,1],null,null,null,1,200);"><Input class= codename name=AssociateClassNameQ >  
		    </TD>
		    <TD  class= title>医院等级</TD>
		    <TD  class= input>
				<Input class= 'codeno' name=LevelCodeQ ondblClick= "showCodeList('hospitalclass',[LevelCodeQ,LevelNameQ],[0,1]);" 
				onkeyup= "showCodeListKeyEx('hospitalclass',[LevelCodeQ,LevelNameQ],[0,1]);"><Input class= codename name=LevelNameQ>  
		    </TD>
    	</TR>
	    <TR>
		    <TD  class= title>地区代码</TD>
		    <TD  class= input>
				<Input class= 'codeno' style="width:52%" name=AreaCodeNameQ 
				ondblClick= "showCodeList('hmareaname',[this,AreaCodeQ],[0,1],null,fm.AreaCodeNameQ.value,'codename',1,193);"><Input class=codeName style="width:25%" name=AreaCodeQ>
		    </TD>	    	
	    	<TD class=title>操作员</TD>
	    	<TD class=input>
	    		<input class=common name=Operator >
	    	</TD>
	    	<TD class=title>最后修改时间</TD>
	    	<TD class=input>
	    		<input class=coolDatePicker name=LastModifyDate>
	    	</TD>
	    </TR>
	    <TR>
	    	<TD  class= title>专科名称</TD>
			<TD  class= input>
		    	<Input class='codeno' name=SpecialCode 
		    	ondblclick="showCodeList('lhspecname',[this,SpecialName],[0,1],null,fm.SpecialCode.value,'code',1,200);"><Input class= codename name=SpecialName >  
		    </TD>	
	    	<TD  class= title>合同效力</TD>
			<TD  class= input>
				<Input class=codeno name=ContFlag  CodeData= "0|^有效|1^无效|2^全部|3"  value = '3'
					ondblClick= "showCodeListEx('LHContFlag',[this,LHContFlag_ch],[1,0],null,null,null,1);" 
			        onkeyup= "showCodeListKeyEx('LHContFlag',[this,LHContFlag_ch],[1,0],null,null,null,1);"><Input class= codename name=LHContFlag_ch  value = '全部'>		    		
		    </TD>			        
	    </TR>
	</table>
</Div>

<table class=common border=0 width=100%>
	<TR class=common>  
	<TD  class=title>
		<INPUT VALUE="查  询"  TYPE=button   class=cssbutton onclick="easyQueryClick();">				
	</TD>
	</TR>
</table> 
      
<hr>   
  
  <!-- 查询结果部分 -->      
<table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDHospital1);">
  		</td>
  		<td class=titleImg>信息</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLDHospital1" style="display:''">
		<table class=common>
    		<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLDHospitalGrid"></span> 
			    </td>
			</tr>
		</table>
	</div>
  	
	<Div id= "divPage5" align=center style= "display: '' ">
		<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage5.firstPage();"> 
		<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage5.previousPage();"> 					
		<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage5.nextPage();"> 
		<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage5.lastPage();">
	</Div>		
		
<table  class= common align='left' >
		<TR class= common>
			<TD class=title>  &nbsp;&nbsp&nbsp;&nbsp					
 	 		<Input value="医 院 信 息" type=button class=cssbutton onclick="HospitalInfoQuery();">&nbsp;&nbsp
 	 		<Input value="就 诊 情 况" type=button class=cssbutton onclick="InHospitalQuery();"><hr>
 			</TD>
 		</TR>
 </table>

<br><br><br><br>
<Div id= "PrintInfo" style= "display: 'none'">
	<table class= common border=0 width=100%>
		<TR  class= common>          
			<TD  class= title>统计起期</TD>
			<TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
			<TD  class= title>统计止期</TD>
			<TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
        	<TD class=title><INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()"></TD>
		</TR>	
		<input type="hidden" name=op value="">
	</table>
</Div>
	
<!--========================================基本信息=============================================================-->		
	<Div  id= "BasicInfo" style= "display: 'none'">  	
		<table>
	    	<tr>
	    		<td>
	    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,BasicInfo1);">
	    		</td>
	    		 <td class= titleImg>基本信息</td>   		 
	    	</tr>
	    </table>
		<Div  id= "BasicInfo1" style= "display: 'none'"> 
			<table  class= common align='center' >
				<TR  class= common>    
					<TD  class= title>机构属性标识</TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=ManageCom>
				    </TD>
				    <TD  class= title>详细地址</TD>
				    <TD  class= input class=input readOnly colSpan=3>
				      <Input class= 'common' readOnly name=Address style="width:421px">
				    </TD>
		 		</TR>
		 		<TR  class= common>    
				    <TD  class= title>联系电话</TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=Phone >
				    </TD>
				    <TD  class= title>邮编</TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=ZipCode>
				    </TD>
				    <TD  class= title>网址</TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=WebAddress >
				    </TD>
				</TR>
				<TR  class= common>
				    <TD  class= title> 乘车路线 </TD>
				    <TD  class= input class=input readOnly colSpan=5>
				    	<Input class= 'common' name=Route  style="width:682px">
				    </TD>
				</TR>
			</table>
		</div>	
		<table>
	    	<tr>
	    		<td>
	    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
	    		</td>
	    		<td class= titleImg>业务信息</td>   		 
	    	</tr>
	    </table>
		<table  class= common align='center' >	
			<TR  class= common>
				<TD  class= title>社保定点属性</TD>
				<TD  class= input>
					<Input class=common readOnly name=CommunFixFlag>
				</TD>
				<TD  class= title>所属系统</TD>
				<TD  class= input>
					<Input class=common readOnly name=AdminiSortCode>
				</TD>
				<TD  class= title>经济类型</TD>
				<TD  class= input>
					<Input class=common readOnly name=EconomElemenCode>
				</TD>
			</TR>
			<TR  class= common>
				<TD  class= title>医院等级</TD>
				<TD  class= input>
					<Input class=common readOnly name=LevelCode>
				</TD>
				<TD  class= title>业务类型</TD>
				<TD  class= input>
					<Input class=common readOnly name=BusiTypeCode>
				</TD>
				<TD  class= title>合 作 级 别</TD>
				<TD  class= input>
					<Input  class=common readOnly name=AssociateClass >
				</TD>
			</TR>
		</table>			    
	</Div>
<!--========================================医疗资源=============================================================-->
	<Div  id= "MedicalInstrument" style= "display: 'none'">  
		<table>
	    	<tr>
	    		<td>
	    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,MedicalInstrument1);">
	    		</td>
	    		 <td class= titleImg>医疗资源</td>   		 
	    	</tr>
	    </table>
		<Div  id= "MedicalInstrument1" style= "display: 'none'"> 
			<table  class= common align='center' >
				<TR  class= common>
					<TD  class= title> 床位数（张）</TD>
					<TD  class= input>
						<Input class= 'common' name=BedAmount >
					</TD>
					<TD  class= title>日均门诊量（人次）</TD>
					<TD  class= input>
						<Input class= 'common' name=PatientPerDay >
					</TD>
					<TD  class= title>年均出院人数（人次）</TD>
					<TD  class= input>
						<Input class= 'common' name=OutHospital >
					</TD>
				</TR>
			</table>
		</div>
		<table>				
			<tr>
				<td class=common>
					<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
				</td>
				<td class=titleImg>特色专科名称</td>
			</tr>
		</table>
		<table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanSpecSecDiaGrid">
	  				</span> 
			    </td>
			</tr>
		</table>
		<Div  id= "TechNameInfo" style= "display: 'none'"> 
			<table  class= common align='left' style= "width: '94%'" >
				<tr>
		        	<TD  class= title>特色介绍</TD>
					<TD  class= input colSpan=6>
					  <textarea class= 'common'  name=MainItem  style="width:713px;height:170px" ></textarea>    
					</TD>
				</tr>   
			</table>
		</div>
		<Div  id= "InstrumentGrid1" style= "display: 'none'"> 
	  		<table>
			  	<tr>
			      <td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
			  		</td>
			  		<td class=titleImg>
			  			 大型仪器设备
			  		</td>
			  	</tr>
			 </table>
			 <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanInstrumentGrid"></span> 
				    </td>
				</tr>
			</table>
		</div>				
		<Div id="ComHospital" style="display:'none' ">
			<table>
			  	<tr>
					<td class=common>
					    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,ComHospital1);">
			  		</td>
			  		<td class=titleImg>合作情况记录</td>
			  	</tr>
			</table>
			<Div id="ComHospital1" style="display:'none' ">
				<table class=common>
			    	<tr class=common>
				  		<td text-align:left colSpan=1>
			  				<span id="spanComHospitalGrid">
			  				</span> 
					    </td>
					</tr>
				</table>
			</div>
		</div>				 		
	</Div>
<!--========================================合同信息=============================================================-->	
	<Div id="LHGroupCont" style="display:'none'">	
		<table>
	  		<tr>
				<td class=common>
			    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,LHGroupCont1);">
	  			</td>
	  			<td class=titleImg>合同信息</td>
			</tr>
		</table>
		<Div id="LHGroupCont1" style="display:'none'">	
			<table class= common>
		  		<tr class=common>
				    <TD  class= title>合同当前状态</TD>
			        <TD  class= input>
			          <Input  type=hidden name=ContraState>
			          <Input class= 'code' name=ContraState_ch verify="合同当前状态|len<=4" CodeData= "0|^1|有效^2|无效" 
				          ondblClick= "showCodeListEx('',[this,ContraState],[1,0],null,null,null,'1',200);" 
				          codeClear(ContraState_ch,ContraState);">&nbsp;&nbsp;&nbsp;&nbsp;<Input value="查询" type=button class=cssbutton onclick="mainQuery();">&nbsp;&nbsp;&nbsp;&nbsp;<Input value="合同详细信息" type=button class=cssbutton onclick="contraspeinfo();">
			        </TD>
			        <TD class=title>
			        </TD>
			        <TD class=title>
			        </TD>
			        <TD class=title>
			        </TD>
	        	</tr>
			</table> 		
		    <table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHGroupContGrid"></span> 
				    </td>
				</tr>
			</table>		
		</div>
	</div>
	
<!--========================================责任信息=============================================================-->		
	<Div id="LHDutyInfo" style="display:'none'">
		<table>
	  		<tr>
				<td class=common>
			    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,LHDutyInfo1);">
	  			</td>
	  			<td class=titleImg>责任信息</td>
			</tr>
		</table>	
		<Div id="LHDutyInfo1" style="display:'none'">
			<table class= common>
		  		<tr class=common>
				    <TD  class= title>责任当前状态</TD>
			        <TD  class= input>
			          <Input  type=hidden name=DutyState>
			          <Input class= 'code' name=DutyState_ch verify="合同当前状态|len<=4" CodeData= "0|^1|有效^2|无效" ondblClick= "showCodeListEx('',[this,DutyState],[1,0],null,null,null,'1',200);" codeClear(DutyState_ch,DutyState);">&nbsp;&nbsp;&nbsp;&nbsp;<Input value="查询" type=button class=cssbutton onclick="dutyQuery();">&nbsp;&nbsp;<Input value="责任详细信息" type=button class=cssbutton onclick="dutyspecQuery();">
			        </TD>
			        <TD class=title>
			        </TD>
			        <TD class=title>
			        </TD>
			        <TD class=title>
			        </TD>
				</tr>
			</table> 
			<table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanLHDutySpecialInfo">
		  				</span> 
				    </td>
				</tr>
			</table>
			<Div id= "divPage3" align=center style= "display: '' ">
		        <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		        <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
			</Div>
			<table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanDutyDoneState">
		  				</span> 
				    </td>
				</tr>
			</table>			
			<Div id= "divPage" align=center style= "display: '' ">
		        <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage6.firstPage();"> 
		        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage6.previousPage();"> 					
		        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage6.nextPage();"> 
		        <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage6.lastPage();">
			</Div>
		</div>
	</Div>	
	<Div id="hiddenDiv" style="display:'none'">
		<TR  class= common> 
			<TD  class= title>
	       		赔付险种类别(代码)
			</TD>
			<TD  class= input>
				<Input class= 'code' name=Riskcode  ondblclick="showCodeList('riskcode1',[this,Riskname],[0,1],null,fm.Riskcode.value,'Riskcode','1',300);"  codeClear(Riskname,Riskcode);">
			</TD>
			<TD  class= title>
				赔付险种类别(名称)
			</TD>
			<TD  class= input>
				<Input class= 'code' name=Riskname ondblclick="showCodeList('riskcode1',[this,Riskcode],[1,0],null,fm.Riskcode.value,'Riskname','1',300);"  codeClear(Riskname,Riskcode);">
			</TD>   
		</tr>
	</Div>
<!--========================================就诊情况=============================================================-->			
	<Div id="LHInhospitalInfo" style="display:'none'">
		<table>
	  		<tr>
				<td class=common>
			    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,LHInhospitalInfo1);">
	  			</td>
	  			<td class=titleImg>就诊情况</td>
			</tr>
		</table>
		<Div id="LHInhospitalInfo1" style="display:'none'">
			<table  class= common >
				<tr>
			  		<TD  class= title >
						赔付险种代码
					</TD>
					<TD  class= input>
						<Input class= 'common'  style="width:160px" name=RiskcodeInfo>
			        </TD>
			      	<TD  class= title>
			        	赔付险种类型
					</TD>
			        <TD  class= input>
						<Input class= 'code' name=RisknameInfo  style="width:160px" ondblclick="queryRiskInfo();">
					</TD>
				</tr>						
				<TR  class= common> 
					<TD  class= title>
			        	起始时间
					</TD>
					<TD  class= input>
				       <Input class= 'coolDatePicker' style="width:160px" dateFromat="Short" name=InHospitStartDate verify="起始时间|DATE">
					</TD>
					<TD  class= title>
				     	  终止时间
					</TD>
					<TD  class= input>
				      <Input class= 'coolDatePicker' style="width:160px" dateFromat="Short" name=InHospitEndDate verify="终止时间|DATE">
					</TD>
					<TD>
				      <Input value="查 询" type=button style="width:60px" class=cssbutton onclick="InhospitQueryResu();">
				    </TD>
					<TD>
				      <Input value="导 出" type=button  style="width:60px" class=cssbutton onclick="LHExportHospital();">
				   </TD>
				</TR>
				<TR  class= common> <TD>　</TD></TR> 
				<TR  class= common>  
					<TD  class= title>
					      门诊总人次
					</TD>
					<TD  class= input>
					    <Input class= 'common' readOnly name=Timesinserv >
					</TD>
					<TD  class= title>
					    平均门诊费用
					</TD>
					<TD  class= input>
					    <Input class= 'common' readOnly name=OutpatientService >
					</TD>
					<TD  class= title>
					</TD>
					<TD  class= input>
					</TD>
				</TR> 
				<TR  class= common> <TD>　</TD></TR> 
				<TR  class= common>  
					<TD  class= title>
				      住院总人次
				  </TD>
				  <TD  class= input>
				    <Input class= 'common' readOnly name=Timesinhospit >
				  </TD>
				  <TD  class= title>
				    平均住院费用
				  </TD>
				  <TD  class= input>
				    <Input class= 'common' readOnly name=Inhospital >
				  </TD>
				  <TD  class= title>
				    平均住院日
				  </TD>
				  <TD  class= input>
				    <Input class= 'common' readOnly name=Averinhospit>
				  </TD>
				</TR> 		
				<TR  class= common>  
				    <TD  class= title>
				      药费占比
				    </TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=Medical>
				    </TD>
				   <TD  class= title>检查费占比</TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=Check >
				    </TD>
				    <TD  class= title>治疗费占比</TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=Cure >
				    </TD>
				</TR>
				<TR  class= common>  
	
				    <TD  class= title>手术费占比</TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=Operation>
				    </TD>
				    <TD  class= title>其他费用占比</TD>
				    <TD  class= input>
				      <Input class= 'common' readOnly name=Other >
				    </TD>
				    <TD  class= title>
				    </TD>
				    <TD  class= input>
				    </TD>
				</TR> 
			</table>				
			<table class=common>
		    	<tr class=common>
			  		<td text-align:left colSpan=1>
		  				<span id="spanHospitalInfoGrid">
		  				</span> 
				    </td>
				</tr>
			</table>
			<Div id= "divPage7" align=center style= "display: '' ">
		        <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage7.firstPage();"> 
		        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage7.previousPage();"> 					
		        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage7.nextPage();"> 
		        <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage7.lastPage();">
			</Div>				
		</div> 	
 	</Div>
 	
	<br><br><br><br><br><br><br><br><br><br>
   
	<Div id="LHDiseaseInfo1" style="display:'none'">
		<table  class= common align='left' >
			<tr class=common>
		    	<TD  class= title>
           			统计方式
       			</TD>
				<TD  class= input>
					<Input type=hidden name=DiseasTypeLevel_origin >
					<Input class= 'code' readOnly name=DiseasTypeLevel elementtype=nacessary verify="代码分类等级|notnull&len<=1" CodeData= "0|^1|1级代码^2|2级代码^3|3级代码^4|4级代码" ondblClick= "showCodeListEx('',[this,DiseasTypeLevel_origin],[1,0],null,null,null,1);"  onkeyup= "showCodeListKeyEx('DiseasTypeLevel',[this,DiseasTypeLevel],[0,1],null,null,null,1);">
		        </TD>   
		        <TD  class= title>
		          疾病代码
		        </TD>
		        <TD  class= input>
		          <Input name=ICDCode_origin style="width:100px">
		        </TD>
		        
		        <TD  class= title>
		          疾病名称
		        </TD>
		        <TD  class= input>
		          <Input class= 'code' name=ICDCode elementtype=nacessary ondblclick=" fm.all('union').value = fm.all('DiseasTypeLevel_origin').value + fm.all('ICDCode').value;return showCodeList('ldiseasecodeclassify',[this,ICDCode_origin],[1,0],null,fm.union.value,'aaa',1,350);" verify="疾病ICD代码|notnull&len<=18" >&nbsp;&nbsp;<Input value="查询" type=button class=cssbutton onclick="diseaseQuery();">
		        </TD>
		    </tr>
		</table>
		<br><br> <br> 					
		<table  class= common >
			<tr class=common align='left' >
		  		<td text-align:left colSpan=1>
	  				<span id="spanInhospitQuery">
	  				</span> 
			    </td>
			</tr> 
		</table>
		<table  class= common align='left' >
			<TR  class= common>  
				<TD  class= title>
			      住院费用比例-药费
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' readOnly name=Medical1 >
			    </TD>
			    <TD  class= title>
			      住院费用比例-检查费
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' readOnly name=Check1 >
			    </TD>
			    <TD  class= title>
			      住院费用比例-治疗费
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' readOnly name=Cure1>
			    </TD>
			</TR> 
			<TR  class= common>  
				<TD  class= title>住院费用比例-手术费</TD>
			    <TD  class= input>
			      <Input class= 'common' readOnly name=Operation1 >
			    </TD>
			</TR>
		</table>		
	</div>
							 			
	<input type=hidden name=union>
	<input type=hidden name=magcom>
	<input type=hidden id="RelaNo" name="RelaNo">
	<input type=hidden id="fmtransact" name="fmtransact">
	 
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
 
