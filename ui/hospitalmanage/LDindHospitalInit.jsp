<%
//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput m_gInput=(GlobalInput)session.getValue("GI");
	String strManageCom="";
	String strCurrentDate="";
	if(m_gInput==null)
	{
		strManageCom="Unknown";
		strCurrentDate="";
	}
	else
	{
		strManageCom=m_gInput.ManageCom;
		strCurrentDate=PubFun.getCurrentDate();
	}
%>
<script language="javascript" type="text/javascript">
	function initForm()
	{
		try
		{
		   initTextbox();
		   initDataGrid();
		   initDataAfterGrid();
		}
		catch(ex)
		{
			document.write("文本框初始化失败!");
		}
	}
	
	//初始化文本框
	function initTextbox()
	{
		document.all["ManageCom"].value="<%=strManageCom%>";
		document.all["ValiDate"].value="<%=strCurrentDate%>";
		document.all["CInValiDate"].value="<%=strCurrentDate%>";
		fm.all('HospitCode').value ="";
	    fm.all('HospitalName').value ="";
	    fm.all('Riskcode').value ="";	
		showAllCodeName();

	}
	//初始化DataGrid表格组件
   function initDataGrid()
	  {                               
	      var iArray = new Array(); 
	      try
	      {

	        iArray[0]=new Array();
	        iArray[0][0]="序号"
	        iArray[0][1]="30px";         //宽度
	        iArray[0][2]=100;         //最大长度
	        iArray[0][3]=0;  
	        
	        iArray[1]=new Array();
	        iArray[1][0]="医疗机构代码"
	        iArray[1][1]="100px";         //宽度
	        iArray[1][2]=100;         //最大长度
	        iArray[1][3]=0;  
	        
	        iArray[2]=new Array();
	        iArray[2][0]="医疗机构名称";         //列名
	        iArray[2][1]="100px";         //宽度
	        iArray[2][2]=100;         //最大长度
	        iArray[2][3]=0;         //是否允许录入,0--不能，1--允许
	   
	        iArray[3]=new Array();
	        iArray[3][0]="管理机构";         //列名
	        iArray[3][1]="100px";         //宽度
	        iArray[3][2]=100;         //最大长度
	        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[4]=new Array();
	        iArray[4][0]="险种代码";         //列名
	        iArray[4][1]="100px";         //宽度
	        iArray[4][2]=100;         //最大长度
	        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
	        

	        iArray[5]=new Array();
	        iArray[5][0]="生效日期";         //列名
	        iArray[5][1]="100px";         //宽度
	        iArray[5][2]=100;         //最大长度
	        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[6]=new Array();
	        iArray[6][0]="终止日期";         //列名
	        iArray[6][1]="60px";         //宽度
	        iArray[6][2]=100;         //最大长度
	        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[7]=new Array();
	        iArray[7][0]="操作员";         //列名
	        iArray[7][1]="60px";         //宽度
	        iArray[7][2]=100;         //最大长度
	        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[8]=new Array();
	        iArray[8][0]="最后一次修改时间";         //列名
	        iArray[8][1]="100px";         //宽度
	        iArray[8][2]=100;         //最大长度
	        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

	       
	        DataGrid = new MulLineEnter( "fm" , "DataGrid" ); 

	        //这些属性必须在loadMulLine前
	        DataGrid.mulLineCount = 0;//行属性，设置需要显示的行数   
	        DataGrid.displayTitle = 1;
	        DataGrid.hiddenPlus = 1;
	        DataGrid.hiddenSubtraction = 1;
	        DataGrid.locked=1;
	        DataGrid.canSel=1;
	        DataGrid.canChk=0;
	        DataGrid.selBoxEventFuncName ="afterSelect";
	        DataGrid.loadMulLine(iArray);  

	      }
	      catch(ex)
	      {
	        window.alert("初始化DataGrid时出错："+ ex);
	      }
	    }
	    
      function initDataAfterGrid()
	  {                               
	      var iArray = new Array(); 
	      try
	      {

	        iArray[0]=new Array();
	        iArray[0][0]="序号"
	        iArray[0][1]="30px";         //宽度
	        iArray[0][2]=100;         //最大长度
	        iArray[0][3]=0;  
	        
	        iArray[1]=new Array();
	        iArray[1][0]="医疗机构代码"
	        iArray[1][1]="100px";         //宽度
	        iArray[1][2]=100;         //最大长度
	        iArray[1][3]=0;  
	        
	        iArray[2]=new Array();
	        iArray[2][0]="医疗机构名称";         //列名
	        iArray[2][1]="100px";         //宽度
	        iArray[2][2]=100;         //最大长度
	        iArray[2][3]=0;         //是否允许录入,0--不能，1--允许
	   
	        iArray[3]=new Array();
	        iArray[3][0]="管理机构";         //列名
	        iArray[3][1]="100px";         //宽度
	        iArray[3][2]=100;         //最大长度
	        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[4]=new Array();
	        iArray[4][0]="险种代码";         //列名
	        iArray[4][1]="100px";         //宽度
	        iArray[4][2]=100;         //最大长度
	        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
	        

	        iArray[5]=new Array();
	        iArray[5][0]="生效日期";         //列名
	        iArray[5][1]="100px";         //宽度
	        iArray[5][2]=100;         //最大长度
	        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[6]=new Array();
	        iArray[6][0]="终止日期";         //列名
	        iArray[6][1]="60px";         //宽度
	        iArray[6][2]=100;         //最大长度
	        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[7]=new Array();
	        iArray[7][0]="操作员";         //列名
	        iArray[7][1]="60px";         //宽度
	        iArray[7][2]=100;         //最大长度
	        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[8]=new Array();
	        iArray[8][0]="最后一次修改时间";         //列名
	        iArray[8][1]="100px";         //宽度
	        iArray[8][2]=100;         //最大长度
	        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

	       
	        DataAfterGrid = new MulLineEnter( "fm" , "DataAfterGrid" ); 

	        //这些属性必须在loadMulLine前
	        DataAfterGrid.mulLineCount = 0;//行属性，设置需要显示的行数   
	        DataAfterGrid.displayTitle = 1;
	        DataAfterGrid.hiddenPlus = 1;
	        DataAfterGrid.hiddenSubtraction = 1;
	        DataAfterGrid.locked=1;
	        DataAfterGrid.canSel=1;
	        DataAfterGrid.canChk=0;
	        DataAfterGrid.loadMulLine(iArray);  

	      }
	      catch(ex)
	      {
	        window.alert("初始化DataGrid时出错："+ ex);
	      }
	    }
</script>