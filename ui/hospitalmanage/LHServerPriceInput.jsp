<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LHServerPriceInput.jsp
//程序功能：服务价格表设置
//创建日期：2006-02-28 17:30:48
//创建人  ：GuoLiying
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHServerPriceInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHServerPriceInputInit.jsp"%>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body  onload="initForm();initElementtype();" >
  <form action="./LHServerPriceSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHContServPrice1);">
    		</td>
    		<td class= titleImg>
        		 服务价格表设置
       	</td>   		 
    	</tr>
    </table>
    <Div  id= "divLHContServPrice1" style= "display: ''">
<table  class= common align='center' >
  <TR  class= common>
    <TD  class= title>
      合同编号
    </TD>
    <TD  class= input>

      <Input class= 'common' name=ContraNo verify="合同编号|NOTNULL&len<=50" readonly>
    </TD>
    <TD  class= title>
      合同名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraName verify="合同名称|NOTNULL&len<=50" readonly>
    </TD>
    <TD class= title>
    	合作机构名称/<br>
    	签约人名称
    </TD>
    <TD   class= input>
    	<Input  type=hidden   name= HospitCode>
    	<Input class= 'common' name=HospitName verify="合作机构名称"  readonly>
    </TD>   
  </TR>
  <TR  class= common>
  	 <TD  class= title>
      合同责任项目类型
    </TD>
    <TD  class= input>
      <Input  type=hidden name=ContraType>
      <Input class= 'common' name=ContraType_ch verify="合同责任项目类型" readonly>	
    </TD>
    <TD  class= title>
      合同责任项目名称
    </TD>
    <TD  class= input>
      <Input  type=hidden name=ContraNowName>
      <Input class= 'common' name=ContraNowName_ch verify="合同责任项目名称" readonly>	
    </TD>
    <TD  class= title>
      责任当前状态
    </TD>
    <TD   class= input>
      <Input type=hidden  name=ContraNowState>
      <Input class= 'common' name=ContraNowState_ch verify="责任当前状态|len<=4" readonly>	
    </TD>

		  	<tr>
		    <TD  class= title>
		      服务价格表类型
		    </TD>
		    <TD   class= input>
		      <Input  type=hidden  name=ServicePriceCode >
		      <Input class= 'code' name=ServicePriceName  verify=" 服务价格表类型|NOTNULL"  CodeData= "0|^1|按次计费价格(元/次)^2|核保体检价格^3|按人计费价格(元/人)^4|按人包干价格(元)"  ondblClick= "showCodeListEx('ServicePriceName',[this,ServicePriceCode],[1,0],null,null,null,'1',200);" readonly>	
		    </TD>
		    <TD  class= title>
		      费用类型
		    </TD>
		    <TD  class= input>
		      <Input type=hidden name=BalanceTypeCode>
		      <Input class= 'code' name=BalanceTypeName  verify="费用类型|NOTNULL"  CodeData= "0|^1|健康体检费用^2|健康管理技术费用^3|医师费用^4|其他费用^5|核保体检费用"  ondblClick= "showCodeListEx('',[this,BalanceTypeCode],[1,0],null,null,null,1,200);" readonly>	
		    </TD>
		     
		  </tr>
		  </table>
	    </Div>
		  <!-- 信息（列表） -->
		<Div id="divLHContServPrice" style="display:''">
	     <div id="divDutyExolai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textDutyExolai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backDutyExolai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backDutyExolaiButton()">
			 </div>
			 <div id="divFeeExplai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textFeeExplai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backFeeExplai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backFeeExplaiButton()">
			 </div>
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHContServPriceGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
		</div>

		</table>
    </Div>
		<div id="div1" style="display: 'none'">
			  <table class=common>
			  <TR  class= common>
			    <TD  class= title>
			      执行者
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=linkman >
			    </TD>
			    <TD  class= title>
			      责  任
			    </TD>
			    <TD  class= input>
			      <Input class= 'common' name=Phone >
			    </TD>
			    <TD >
			    </TD>
			  </TR>
			  </table>
		</div>
<Div  id= "divTestMoney" style= "display: 'none'">
			<table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 请输入价格信息
       	</td>   		 
    	</tr>
		  </table>
					<table class=common>
						<TR  class= common>
						 <TD   class= title >
				     	   服务价格
				     </TD>
				     <TD  class= input>
		          <Input class= 'common' name=MoneySum verify="服务价格|num&len<=10">
				     </TD>

				     <TD  type = hidden class= input>
				     	<Input  type = hidden class= 'common' name=MaxServNum verify="最大人数|num&len<=10">
				     </TD>
				     <TD   class= title >
				     </TD>
				     <TD  class= input>
				     </TD>
				    </tr>
				    <tr>
				  	<TD  class= title >
				     	   补充说明
				     </TD>
				     <TD  class= input colSpan=5>
             <textarea class= 'common'  name=ExplanOther verify="补充说明|len<=1500" style="width:883px;height:60px" ></textarea>    
             </TD>
		       
		       </tr>
				 </table>
</div>
<Div  id= "divTestMoney2" style= "display: 'none'">
			<table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 请输入价格信息
       	</td>   		 
    	</tr>
		  </table>
					<table class=common>
						<TR  class= common>
						 <TD   class= title >
				     	   服务价格
				     </TD>
				     <TD  class= input>
		          <Input class= 'common' name=MoneySum2 verify="服务价格|num&len<=10">
				     </TD>
				     <TD   class= title >
				     	   服务时程
				     </TD>
				     <TD  class= input>
				     	<Input class= 'common' name=ServPros2 verify="服务时程|len<=300">
				     </TD>

				     <TD type = hidden  class= input>
				     	<Input type = hidden  class= 'common' name=MaxServNum2 verify="最大人数|num&len<=10">
				     </TD>
				    </tr>
				    <tr>
				  	<TD  class= title >
				     	   补充说明
				     </TD>
				     <TD  class= input colSpan=5>
             <textarea class= 'common'  name=ExplanOther2 verify="补充说明|len<=1500" style="width:883px;height:60px" ></textarea>    
             </TD>
		       
		       </tr>
				 </table>
</div>
<Div  id= "divTestMoney3" style= "display: 'none'">
			<table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 请输入价格信息
       	</td>   		 
    	</tr>
		  </table>
					<table class=common>
						<TR  class= common>
						 <TD   class= title >
				     	   服务价格
				     </TD>
				     <TD  class= input>
		          <Input class= 'common' name=MoneySum3 verify="服务价格|num&len<=10">
				     </TD>
				     <TD   class= title >
				     	   服务时程
				     </TD>
				     <TD  class= input>
				     	<Input class= 'common' name=ServPros3 verify="服务时程|len<=300">
				     </TD>
				     <TD   class= title >
				     	   最大人数
				     </TD>
				     <TD  class= input>
				     	<Input class= 'common' name=MaxServNum3 verify="最大人数|num&len<=10">
				     </TD>
				    </tr>
				    <tr>
				  	<TD  class= title >
				     	   补充说明
				     </TD>
				     <TD  class= input colSpan=5>
             <textarea class= 'common'  name=ExplanOther3 verify="补充说明|len<=1500" style="width:883px;height:60px" ></textarea>    
             </TD>
		       
		       </tr>
				 </table>
</div>
<Div  id= "divTestServPrice" style= "display: 'none'">
			<table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" >
    		</td>
    		<td class= titleImg>
        		 体检价格信息
       	</td>   		 
    	</tr>
		  </table>
					<table class=common>
						<TR  class= common>
						 <TD  class= title >
				     	   体检套餐代码
				     </TD>
				     <TD  class= input>
		             <Input class= 'common' name=MedicaItemCode verify="体检套餐代码|len<=20">
				     </TD>
				     	<TD  class= title >
				     	   体检套餐名称
				     </TD>
				     <TD  class= input>
		           <Input class= 'code' name=MedicaItemName verify="体检套餐名称|len<=20" ondblclick="showCodeList('hmchoosetest',[this,MedicaItemCode],[0,1],null,fm.MedicaItemName.value,'MedicaItemName','',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('hmchoosetest',[this,MedicaItemCode],[0,1],null,fm.MedicaItemName.value,'MedicaItemName');" >
				     </TD>
				     <td style="width:900px;">
				  	 </td>
				     </TD>
				     <TD  class= input colSpan=5>
             </TD>
		       </tr>
				 </table>
</div>
<Div  id= "divShowInfo" style= "display: 'none'">	
			  <table class=common>
	    	<tr class=common>
	    		<table>
		    		<tr>
				      <td class=common>
						    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTestItemPriceGrid);">
				  		</td>
				  		<td class=titleImg>
				  			 检查项目单价
				  		</td>
				  	</tr>
			  	</table>	
<Div  id= "divTestItemPriceGrid" style= "display: 'none'">	
	 
	 <table class=common> 	
	 		<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanTestItemPriceGrid"></span> 
	  				<br>
	  			</td>
	  	</tr>
		</table>

</div>

	  		<table>
		      <td class=common>
				    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTestGrpPriceGrid);">
		  		</td>
		  		<td class=titleImg>
		  			 体检套餐价格
		  		</td>
		  	</table>
<Div  id= "divTestGrpPriceGrid" style= "display: 'none'">

		  	</tr>
            <span id="spanTestGrpPriceGrid"></span> 
			    </td>
				</tr>
			</table>

</div>

</div>
    <input type=hidden id="fmtransact" name="fmtransact">
    <input type=hidden id="fmAction" name="fmAction">
    <input type = hidden name = "Operator">
    <input type = hidden name = "MakeDate">
    <input type = hidden name = "MakeTime">
    <input type = hidden name = "ModifyDate">
    <input type = hidden name = "ModifyTime">
    <input type = hidden name = HiddenBtn>
    <Input  type=hidden name=ContraItemNo>
    <Input  type=hidden name=SerialNo>
  </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
