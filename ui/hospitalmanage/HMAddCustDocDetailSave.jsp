<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：HMAddCustDocDetailSave.jsp
//程序功能：添加客户档案明细
//创建日期：2010-3-8
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.config.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.hospitalmanage.*"%>

<%
 //接收信息，并作校验处理。
 
 //输入参数
 //××××Schema t××××Schema = new ××××Schema();
 HMAddCustDocDetailUI tHMAddCustDocDetailUI = new HMAddCustDocDetailUI();
 
 //输出参数
 CErrors tError = null;
 String tRela  = "";                
 String FlagStr = "";
 String Content = "";
 String transact = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getValue("GI");
 
 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 transact = request.getParameter("fmtransact");
 String  tCustNo = request.getParameter("CustNo");  //客户号码
 String tName = request.getParameter("Name");
 
 //客户档案信息
 HMCustDocInfoSchema tHMCustDocInfoSchema = new HMCustDocInfoSchema();
 //阳性指标信息
 HMCustExamInfoSet tHMCustExamInfoSet = new HMCustExamInfoSet();
 
 if(transact != null && transact.equals("INSERT||MAIN")) {
	//封装客户档案信息
	tHMCustDocInfoSchema.setCustomerNo(tCustNo);
	tHMCustDocInfoSchema.setCustomerName(tName);
	tHMCustDocInfoSchema.setPastDisHistory(request.getParameter("PastDisHistory"));
	tHMCustDocInfoSchema.setFamilyDisHistory(request.getParameter("FamilyDisHistory"));
	tHMCustDocInfoSchema.setBadHabit(request.getParameter("BadHabit"));
	
	//封装阳性指标信息
	String tTestMedicaItemCode[]=request.getParameterValues("ExamIndGrid2"); 
	String tTestResult[]=request.getParameterValues("ExamIndGrid4");         
	String tTestNo[]=request.getParameterValues("ExamIndGrid6");             
	String tUnit[]=request.getParameterValues("ExamIndGrid5");  
	String tIsNormal[] = request.getParameterValues("ExamIndGrid10");        
	String tTestDate[] = request.getParameterValues("ExamIndGrid9");
	String tNormalValue[] = request.getParameterValues("ExamIndGrid8");
	
	int HMCustDocInfoCount = 0;                                                     
	if(tTestMedicaItemCode != null )    
	{                                 
		HMCustDocInfoCount = tTestMedicaItemCode.length;
			                                                                           
			for(int i = 0; i < HMCustDocInfoCount; i++)                                 
			{                                                                          
				HMCustExamInfoSchema tHMCustExamInfoSchema = new HMCustExamInfoSchema();
				tHMCustExamInfoSchema.setCustomerNo(tCustNo); 
				tHMCustExamInfoSchema.setMedicaItemCode(tTestMedicaItemCode[i]);                  
				tHMCustExamInfoSchema.setTestResult(tTestResult[i]);                   
				tHMCustExamInfoSchema.setTestDate(request.getParameter("InHospitDate")); 
				tHMCustExamInfoSchema.setTestNo(String.valueOf(i+1));                           
				tHMCustExamInfoSchema.setStandardMeasureUnit(tUnit[i]);
				tHMCustExamInfoSchema.setIsNormal(tIsNormal[i]);
				tHMCustExamInfoSchema.setTestDate(tTestDate[i]);
				tHMCustExamInfoSchema.setNormalValue(tNormalValue[i]);
                                               
				tHMCustExamInfoSet.add(tHMCustExamInfoSchema);
			}   
		}
 }
 
 
 try
 {
  //准备传输数据VData
  VData tVData = new VData();
  
  //传输schema
  tVData.add(tG);
  tVData.addElement(tHMCustDocInfoSchema);
  tVData.addElement(tHMCustExamInfoSet);
  tHMAddCustDocDetailUI.submitData(tVData,transact);
 }
 catch(Exception ex)
 {
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr=="")
 {
  tError = tHMAddCustDocDetailUI.mErrors;
  if (!tError.needDealError())
  {                          
   Content = " 保存成功! ";
   FlagStr = "Success";
  }
  else                                                                           
  {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

