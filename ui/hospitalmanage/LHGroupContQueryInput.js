/** 
 * 程序名称：LHGroupContInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2005-03-19 15:05:48
 * 创建人  ：ctrHTML
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {
	//此处书写SQL语句			     
 			 
	var LEN = manageCom.length;
  if(LEN == 8){LEN = 4; }   
	var strSql = "select  distinct a.ContraNo,a.ContraName,"
	          +"(select HospitName from LDHospital where LDHospital.HospitCode=a.HospitCode),"
	          +"a.IdiogrDate,a.ContraBeginDate,a.ContraEndDate,"
	       //   +"(select b.ContraItemNo from LHContItem b where b.contrano=a.contrano),"
	          +"( case when a.ContraState = '1' then '有效' else '无效' end),a.MakeDate,a.MakeTime from LHGroupCont a "
				    +" where a.managecom like '"+manageCom.substring(0,LEN)+"%%'"
				    //+ getWherePart("a.ContraNo", "ContraNo", "like")
				    //+ getWherePart("a.ContraName", "ContraName", "like")
				    + getWherePart("a.HospitCode", "HospitCode", "like")
				    //+ getWherePart("a.IdiogrDate", "IdiogrDate", "like")
				    //+ getWherePart("a.ContraBeginDate", "ContraBeginDate", "like")
				    //+ getWherePart("a.ContraEndDate", "ContraEndDate", "like")
  					;
 // alert(strSql);
//  alert(easyExecSql(strSql));
	turnPage.queryModal(strSql, LHGroupContGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}
function returnParent()
{
  var arrReturn = new Array();
	var tSel = LHGroupContGrid.getSelNo();
	
	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterQuery0( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
function getQueryResult()
{
	var arrSelected = null;
	tRow = LHGroupContGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	//if( tRow == 0 || tRow == null || arrDataSet == null )
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	arrSelected[0] = LHGroupContGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}
