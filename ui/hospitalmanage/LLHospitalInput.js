//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------

var mAction="";
var mDebug=0;
var showInfo=null;
//----------------------------------------------------
//保存按键
function submitForm()
{
  	//if( verifyInput() == false ) return false;
  	//window.alert("------OK-----");  	
	if(mAction == "INSERT||MAIN")
	{
		document.all["fmtransact"].value = mAction;
		mAction = "";
		
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
	    document.forms[0].action="./LLHospitalSave.jsp";  
	    document.forms[0].submit(); //提交
	} 
	else 
	{
	    mAction="";
	    document.all["fmtransact"].value=mAction;
	}
}
//----------------------------------------------------


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,SerialNo)
{

	  showInfo.close();
	  if (FlagStr == "Fail" )
	  {             
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
	    window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	  }
	  else
	  { 
	
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	  	parent.fraInterface.initForm();
	    var win=window.showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	    showDiv(operateButton,"true"); 
	    document.all["SerialNo"].value=SerialNo;
	    //执行下一步操作
	    
	  }
	  //document.all["ProblemDetail"].readOnly=true;
	  //document.all["Action"].readOnly=true;
	 
}

function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	  window.alert("在LLHospitalInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
}



function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//-------------------保存时-------
//
function addClick()
{	
	if(fm.ManageCom.value.length<8)			
	{
		alert('医院归属必须到三级机构，请修改管理机构');
		return false;	
	}
	if(document.all["hiddenSerialNo"].value!=""||document.all["hiddenSerialNo"].value==null)
	{
		window.alert("该条记录已经存在!");
		return false;
	}
	if(document.all["ManageCom"].value==""||document.all["ManageCom"].value==null)
	{
		window.alert("机构名称不能为空!");
		return false;
	}
	if(document.all["HospitalCode"].value==""||document.all["HospitalCode"].value==null)
	{
		window.alert("医院名称不能为空");
		return false;
	}
	if(document.all["Charger"].value==""||document.all["Charger"].value==null)
	{
		window.alert("责任人名称不能为空");
		return false;
	}
	else
	{	
		//var strSql="select * from LLMedicalManagement where HospitalCode='"+document.all["HospitalCode"].value+"'"
				   //+" and Charger='"+document.all["Charger"].value+"'";
				 
		//var strQueryResult=easyQueryVar3(strSql,1,1,1);
		//var strQueryResult=easyExecSql(strSql);
		//window.alert(strQueryResult);
		/*if(strQueryResult)
		{
			window.alert("该责任人已经在医院中存在!");
			mAction="";
			document.all["fmtransact"].value=mAction;
			return false;
		}
		else
		{*/
			mAction="INSERT||MAIN";
			submitForm();
		//}
		
		document.all["hiddenSerialNo"].value="";
		document.all["hiddenFlag"].value="hiddenFlag";
	}			   
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
      parent.fraMain.rows = "0,0,0,0,*";
  }
   else
   {
      parent.fraMain.rows = "0,0,0,0,*";
   }
}

//-----------------------------------------------------
//修改按键
function updateClick()
{	
	if(fm.ManageCom.value.length<8)			
	{
		alert('医院归属必须到三级机构，请修改管理机构');
		return false;	
	}
	if(document.all["hiddenSerialNo"].value==""||document.all["hiddenSerialNo"].value==null)
	{
		window.alert("查询后才能修改!");
		return false;
	}
	if(document.all["ManageCom"].value==""||document.all["ManageCom"].value==null)
	{
		window.alert("机构名称不能为空!");
		return false;
	}
	if(document.all["HospitalCode"].value==""||document.all["HospitalCode"].value==null)
	{
		window.alert("医院名称不能为空");
		return false;
	}
	if(document.all["Charger"].value==""||document.all["Charger"].value==null)
	{
		window.alert("责任人名称不能为空");
		return false;
	}
    else
    {
    	var strSql="select HospitalCode,Charger from LLMedicalManagement where SerialNo='"+document.all["hiddenSerialNo"].value+"'";
				  
		var strQueryResult=easyExecSql(strSql);
		if(!strQueryResult)
		{
			window.alert("该责任人在医院中不存在!");
			mAction="";
			document.all["fmtransact"].value=mAction;
			return false;
		}
    	//下面增加相应的代码
	    if (window.confirm("您确实想修改该记录吗?"))
	    {
	    	//document.all["ProblemDetail"].readOnly=false;
	    	//document.all["Action"].reaOnly=false;
	        mAction="UPDATE||MAIN";
	        document.all["fmtransact"].value=mAction;
	        var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
            showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
            mDebug=1;
            showSubmitFrame(mDebug);
            mDebug=0;
	        mAction="";
	        document.forms[0].action="./LLHospitalSave.jsp";  
	    	document.forms[0].submit(); //提交
	    }
	    else
	    {
	        mAction="";
	        document.all["fmtransact"].value=mAction;
	        window.alert("您取消了修改操作！");
	    }
   }
   document.all["hiddenSerialNo"].value=="";
   document.all["hiddenFlag"].value="hiddenFlag";
}
//-----------------------------------------------------
//删除按键
function deleteClick()
{
	if(document.all["hiddenSerialNo"].value==""||document.all["hiddenSerialNo"].value==null)
	{
		window.alert("查询后才能修改!");
		return false;
	}
	if(document.all["ManageCom"].value==""||document.all["ManageCom"].value==null)
	{
		window.alert("机构名称不能为空!");
		return false;
	}
	if(document.all["HospitalCode"].value==""||document.all["HospitalCode"].value==null)
	{
		window.alert("医院名称不能为空");
		return false;
	}
	if(document.all["Charger"].value==""||document.all["Charger"].value==null)
	{
		window.alert("责任人名称不能为空");
		return false;
	}
    else
    {
    	var strSql="select HospitalCode,Charger from LLMedicalManagement where HospitalCode='"+document.all["HospitalCode"].value+"'"
				   +" and Charger='"+document.all["Charger"].value+"'"+" and SerialNo='"+document.all["hiddenSerialNo"].value+"'";
		//var strQueryResult=easyQueryVar3(strSql,1,0,1);
		//window.alert(strSql);
		var strQueryResult=easyExecSql(strSql);
		if(!strQueryResult)
		{
			window.alert("该责任人在医院中不存在!");
			mAction="";
			document.all["fmtransact"].value=mAction;
			return false;
		}
    	//下面增加相应的代码
	    if (window.confirm("您确实想删除该记录吗?"))
	    {
	        mAction="DELETE||MAIN";
	        document.all["fmtransact"].value=mAction;
	        var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
       	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
            showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
            mDebug=1;
            showSubmitFrame(mDebug);
            mDebug=0;
	        mAction="";
	        document.forms[0].action="./LLHospitalSave.jsp";  
	    	document.forms[0].submit(); //提交
	    }
	    else
	    {
	        mAction="";
	        document.all["fmtransact"].value=mAction;
	        window.alert("您取消了删除操作！");
	    }
   }
}
//------------------------------------------------------



//------------------------------------------------------
//查询按键
function queryClick()
{
	window.open("./LLHospitalDetailQueryMain.jsp");
    //window.open("./LLHospitalDetailQuery.jsp?Interface=LLHospitalDetailQueryMain.jsp",'width=700,height=250,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=0,status=0');
}
//------------------------------------------------------



//-------------------------------------------------------
//添加扫描件
/*function addScan()
{
	window.alert("addScan");
}*/
//--------------------------------------------------------



//-------------------------------------------------------
//查询扫描件
//function queryScan()
//{
	//window.alert("queryScan");
//}
//--------------------------------------------------------

function queryScan()
{
    var arrResult = new Array();
    if(document.all["hiddenSerialNo"].value!=""&&document.all["hiddenSerialNo"].value!=null)
    {
   		var strSerialNo=document.all["hiddenSerialNo"].value;
   		
   		//-----------------------------查看扫描件---------------------------
   		var docId = "";
		var sql = " select docid from es_doc_main where Busstype='HM' and subtype='HM23' and DocCode='" 
								+ strSerialNo + "' ";
		var arr = easyExecSql(sql);
		if(!arr) 
		{
			window.alert("数据库中无该扫描件!");
			return false;
		} 
		else 
		{
			docId = arr[0][0];
		}
	
		var url="./LLHospitalScanMain.jsp?DocID="+docId+"&DocCode=" + strSerialNo ;
		window.open(url);
    }
    else
    {
        window.alert("请先查询医疗管理信息!");
    }
}

//-------------------------------------------------------
//医院问题查询
function hospitalProblemQuery()
{
	window.open("./LLHospitalProblemInputMain.jsp");
}
//--------------------------------------------------------



//-------------------------------------------------------
//医护问题查询
function doctorProblemQuery()
{
	window.open("./LLHospitalDoctorInputMain.jsp");
}
//--------------------------------------------------------




//-------------------------------------------------------
//评价数据提取
function evaluationData()
{
	window.alert("evaluationData");
}
//--------------------------------------------------------

function afterQuery(arrDataSet)
{
	if(arrDataSet!=null)
	{
		try
		{
			//管理机构名称利用隐藏控件
			document.all["hiddenSerialNo"].value=arrDataSet[0];
			document.all["SerialNo"].value=document.all["hiddenSerialNo"].value;
			document.all["ManageCom"].value=arrDataSet[1];
			document.all["HospitalName"].value=arrDataSet[2];
			document.all["MsgChannel"].value=arrDataSet[3];
			document.all["Charger"].value=arrDataSet[4];
			document.all["Professional"].value=arrDataSet[5];
			document.all["BussinessDate"].value=arrDataSet[6];
			document.all["ProblemDetail"].value=arrDataSet[7];
			document.all["Action"].value=arrDataSet[8];
			//document.getElementById("SerialNo").setAttribute("readOnly","true");
			//document.getElementById("Charger").setAttribute("readOnly","true");
			document.getElementById("ProblemDetail").setAttribute("readOnly",false);
			//document.getElementById("BussinessDate").setAttribute("readOnly","true");
			document.getElementById("Action").setAttribute("readOnly",false);
			
			//window.alert("-----OK-----");
			var strSql="select HospitalCode,MsgChannelCode,ProfessionalCode from LLMedicalManagement where SerialNo='"+document.all["hiddenSerialNo"].value+"'";
			var strQueryResult=easyExecSql(strSql);
			document.all["HospitalCode"].value=strQueryResult[0][0];
			document.all["MsgChannelCode"].value=strQueryResult[0][1];
			document.all["ProfessionalCode"].value=strQueryResult[0][2];
			document.all["SerialNo"].disabled=false;
			document.all["SerialNo"].readOnly=true;
			//document.all["SerialNo"].value=arrDataSet[0];
		}
		catch(ex)
		{
			document.writeln("页面赋值失败!");
		}
	}
}