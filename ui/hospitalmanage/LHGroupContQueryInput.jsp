<%
//程序名称：LHGroupContInput.jsp
//程序功能：功能描述
//创建日期：2005-03-19 15:05:48
//创建人  ：ctrHTML
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LHGroupContQueryInput.js"></SCRIPT> 
  <%@include file="LHGroupContQueryInit.jsp"%>
  
  <title></title>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>
  
  <!-- 导入按钮界面 -->
 
    
  <!-- 查询条件部分 -->
  <!-- 查询条件Title -->
  <table class=common border=0 width=100%>
    <tr>
			<td class=titleImg align=center> 请输入查询条件： </td>
		</tr>
	</table>
	
  <Div  id= "divLHGroupContGrid1" style= "display: ''">    
<table  class= common align='center' >
  <TR  class= common>
    <!--<TD  class= title>
      合同编号
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraNo verify="合同编号|len<=50">
    </TD>
    <TD  class= title>
      合同名称
    </TD>
    <TD  class= input>
      <Input class= 'common' name=ContraName verify="合同名称|len<=50">
    </TD>-->
     <TD class= title>
    	医疗机构代码
    </TD>
    <TD  class= input>
    	<Input class= 'common'  name= HospitCode>
    </TD>
    <TD class= title>
    	医疗机构名称
    </TD>
    <TD  class= input>
    	  <Input class= 'code' name=HospitName  ondblclick="showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName','',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName');"   onkeydown="return showCodeListKey('lhhospitname',[this,HospitCode],[0,1],null,fm.HospitName.value,'HospitName'); codeClear(HospitName,HospitCode);">
    </TD>
     <TD class= title>
    </td>
    <TD  class= input>
    </td>
	</TR>
	 <!--<TR  class= common>
  	<TD  class= title>
      签订时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=IdiogrDate style="width:140px">
    </TD>
    <TD  class= title>
      合同起始时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=ContraBeginDate   style="width:140px">
    </TD>
    <TD  class= title>
      合同终止时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=ContraEndDate  style="width:140px" >
    </TD>
  </TR>-->
</table>
<!--table class = common style= "display: 'none'">
    <TD class= title>
    	合作机构名称
    </TD>
    <TD  class= input>
    	<Input class= 'common' name=HospitCode >
    </TD>
    <TD  class= title>
      合作机构代码
    </TD>
    <TD  class= input>
      <Input class= 'common' name=HospitCode >
    </TD>
    
  </TR>
  <TR  class= common>
  	<TD  class= title>
      签订时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=IdiogrDate >
    </TD>
    <TD  class= title>
      合同起始时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=ContraBeginDate >
    </TD>
    <TD  class= title>
      合同终止时间
    </TD>
    <TD  class= input>
      <Input class= 'coolDatePicker' name=ContraEndDate >
    </TD>
  </TR>
  <TR  class= common>
    <TD  class= title>
      合同当前状态
    </TD>
    <TD  class= input>
      <Input class= 'code' name=ContraState verify="|len<=2" CodeData= "0|^1|a^2|b" ondblClick= "showCodeListEx('',[this,''],[0,1]);" onkeyup= "showCodeListKeyEx('',[this,''],[0,1]);">	
    </TD>
		<TD  class= title>
      合同原本备份
    </TD>
    <TD class = input>
    	<Input class= 'common' name=ContraCopy >
    </TD>
   </TR>
</table-->
  </Div>
    
  <table class=common border=0 width=100%>
    <TR class=common>  
      <TD  class=title>
        <INPUT class =cssbutton VALUE="查询"   TYPE=button   class=common onclick="easyQueryClick();">
        <INPUT class =cssbutton VALUE="返回"   TYPE=button   class=common onclick="returnParent();"> 					
      </TD>
    </TR>
  </table>
        
  <hr>   
  
  <!-- 查询结果部分 -->      
  <table>
  	<tr>
      <td class=common>
		    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHGroupCont1);">
  		</td>
  		<td class=titleImg>
  			 信息
  		</td>
  	</tr>
  </table>
  <!-- 信息（列表） -->
	<Div id="divLHGroupCont1" style="display:''">
    <table class=common>
    	<tr class=common>
	  		<td text-align:left colSpan=1>
  				<span id="spanLHGroupContGrid">
  				</span> 
		    </td>
			</tr>
		</table>
	</div>
  	
  <Div id= "divPage" align=center style= "display: '' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
  </Div>
    
<!-- 录入提交信息部分     
  <table class=common border=0 width=100%>
    <tr>
  		<td class=titleImg align= center>录入信息：</td>
  	</tr>
  </table> 
     		  								
  <table  class=common align=center>
    <TR CLASS=common>
      <TD  class=title>
        通知单号码
      </TD>
      <TD  class=input>
        <Input NAME=GetNoticeNo class=common verify="通知单号码|notnull">
      </TD>
      <TD  class=title>
        <INPUT VALUE="打 印" class=common TYPE=button onclick="PPrint()">
      </TD>
      
    </TR>
  </table>
  
  <br>           
-->    
 
  <INPUT VALUE="" TYPE=hidden name=serialNo>
  <input type=hidden id="fmtransact" name="fmtransact">
 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
 
