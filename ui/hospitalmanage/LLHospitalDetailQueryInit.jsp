<%
//---------------------------------------------------
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
//----------------------------------------------------
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	GlobalInput m_gInput=(GlobalInput)session.getValue("GI");
	String strManageCom="";
	String strCurrentDate="";
	if(m_gInput==null)
	{
		strManageCom="Unknown";
		strCurrentDate="";
	}
	else
	{
		strManageCom=m_gInput.ManageCom;
		strCurrentDate=PubFun.getCurrentDate();
	}
%>

<script language="javascript" type="text/javascript">
	function initForm()
	{
		try
		{
			initTextbox();
			initDataGrid();
		}
		catch(ex)
		{
			document.write("文本框初始化失败!");
		}
	}
	
	//初始化文本框
	function initTextbox()
	{
		
		document.all["ManageCom"].value="<%=strManageCom%>";
		document.all["ComName"].value="";
		document.all["HospitalCode"].value="";
		document.all["HospitalName"].value="";
		document.all["MsgChannelCode"].value="";
		document.all["MsgChannel"].value="";
		document.all["Charger"].value="";
		document.all["ProfessionalCode"].value="";
		document.all["Professional"].value="";
		
		document.all["StartDate"].value="";
		document.all["EndDate"].value="<%=strCurrentDate%>";
	}
	
	  //初始化DataGrid表格组件
	  function initDataGrid()
	  {                               
	      var iArray = new Array(); 
	      
	      try
	      {

	        iArray[0]=new Array();
	        iArray[0][0]="序号"
	        iArray[0][1]="30px";         //宽度
	        iArray[0][2]=100;         //最大长度
	        iArray[0][3]=0;  
	        
	        iArray[1]=new Array();
	        iArray[1][0]="流水线号"
	        iArray[1][1]="100px";         //宽度
	        iArray[1][2]=100;         //最大长度
	        iArray[1][3]=0;  
	        
	        iArray[2]=new Array();
	        iArray[2][0]="管理机构";         //列名
	        iArray[2][1]="60px";         //宽度
	        iArray[2][2]=100;         //最大长度
	        iArray[2][3]=0;         //是否允许录入,0--不能，1--允许
	   
	        iArray[3]=new Array();
	        iArray[3][0]="医院名称";         //列名
	        iArray[3][1]="100px";         //宽度
	        iArray[3][2]=100;         //最大长度
	        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[4]=new Array();
	        iArray[4][0]="信息渠道";         //列名
	        iArray[4][1]="100px";         //宽度
	        iArray[4][2]=100;         //最大长度
	        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
	        

	        iArray[5]=new Array();
	        iArray[5][0]="责任人姓名";         //列名
	        iArray[5][1]="100px";         //宽度
	        iArray[5][2]=100;         //最大长度
	        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[6]=new Array();
	        iArray[6][0]="职业";         //列名
	        iArray[6][1]="60px";         //宽度
	        iArray[6][2]=100;         //最大长度
	        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

	        iArray[7]=new Array();
	        iArray[7][0]="发生日期";         //列名
	        iArray[7][1]="100px";         //宽度
	        iArray[7][2]=100;         //最大长度
	        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[8]=new Array();
	        iArray[8][0]="问题描述";         //列名
	        iArray[8][1]="100px";         //宽度
	        iArray[8][2]=100;         //最大长度
	        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

	        iArray[9]=new Array();
	        iArray[9][0]="采取动作及结果";         //列名
	        iArray[9][1]="100px";         //宽度
	        iArray[9][2]=100;         //最大长度
	        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
	        
	        iArray[10]=new Array();
	        iArray[10][0]="医院编码";         //列名
	        iArray[10][1]="100px";         //宽度
	        iArray[10][2]=100;         //最大长度
	        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
	        DataGrid = new MulLineEnter( "fm" , "DataGrid" ); 

	        //这些属性必须在loadMulLine前
	        DataGrid.mulLineCount = 0;//行属性，设置需要显示的行数   
	        DataGrid.displayTitle = 1;
	        DataGrid.hiddenPlus = 1;
	        DataGrid.hiddenSubtraction = 1;
	        DataGrid.locked=1;
	        DataGrid.canSel=1;
	        DataGrid.canChk=0;
	        DataGrid.loadMulLine(iArray);  
	      }
	      catch(ex)
	      {
	        window.alert("初始化DataGrid时出错："+ ex);
	      }
	    }
</script>