<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2005-04-19 15:05:48
//创建人  ：hm
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LHContInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LHContInputInit.jsp"%>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
<SCRIPT>	
	
		
	var initWidth = 0;
	//图片的队列数组
	var pic_name = new Array();
	var pic_place = 0;
	var b_img	= 0;  //放大图片的次数
	var s_img = 0;	//缩小图片的次数
	
	function queryScanType() 
	{
    	var strSql = "select code1, codename, codealias from ldcode1 where codetype='scaninput'";
    	var a = easyExecSql(strSql);

    	return a;
  	} 
</SCRIPT>
<SCRIPT language=javascript1.2>
	//查看扫描件按钮对应事件
function showsubmenu()
{
    whichEl = eval('submenu' );
    if (whichEl.style.display == 'none')
    {
         //submenu1.style.display='';
    	   eval("submenu1.style.display='';");
         submenu.style.display='';
    }
    else
    {
    	  submenu.style.display='none';
        eval("submenu1.style.display='';");
    }
    var tWorkNo = fm.all('ContraNo').value;
        //prtNo = easyExecSql(strSql);
    var url="../common/EasyScanQuery/EasyScanQuery.jsp?prtNo="+tWorkNo+"&BussNoType=61&BussType=HM&SubType=HM02";
    document.getElementById("iframe1").src=url;
}
</SCRIPT>
<body  onload="initForm();initElementtype();" >
  <form action="./LHContSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
     <table>
    	<tr>
			<td class=button width="100%" align=left>
				<INPUT class=cssButton VALUE="查看合同扫描件"  TYPE=button onclick=" showsubmenu();">
			</td>
		</tr>	
		<tr>
        <td style='display:none' id='submenu'>
			<div>
				<table class=common cellpadding=0 cellspacing=0 >   
					<tr class=common>
						<td class=input>
	  		       			<iframe scrolling="auto" width="900" height="300" id=iframe1 src=''>
	  		        		</iframe>
						</td>
					</tr>
	  	       </table>
			</div>
			  </td>
		 </tr>
		</table>  
		<div style="display:''" id='submenu1'>
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHCont1);">
    		</td>
    		 <td class= titleImg>
        		 个人合同一般信息
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divLHCont1" style= "display: ''">
    <table  class= common align='center' >
       <TR  class= common>
         <TD  class= title>
           合同编号
         </TD>
         <TD  class= input>
         	 <Input type=hidden name=ContraNoHidden >
           <Input class= 'common' name=ContraNo style="width:152px"  verify="合同编号|notnull&len<=24" ondblclick="getNo();" readonly><font color=red> *</font>
         </TD>
         <TD  class= title>
           合同名称
         </TD>
         <TD  class= input>
           <Input class= 'common' name=ContraName style="width:152px"  verify="合同名称|NOTNULL&len<=50"><font color=red> *</font>
         </TD>
         <TD  class= title>
           签约人员代码
         </TD>
         <TD  class= input>
         	 <Input class=codeno name=DoctNo ondblclick="showCodeList('lhdoctname',[this,DoctName],[1,0],null,fm.DoctName.value,'DoctName','1',200);" onkeyup=" if(event.keyCode ==13) return showCodeList('lhdoctname',[this,DoctName],[1,0],null,fm.DoctName.value,'DoctName',null,'160');" onkeydown="return showCodeListKey('lhdoctname',[this,DoctName],[1,0],null,fm.DoctName.value,'DoctName'); codeClear(DoctName,DoctNo);" verify="签约人员代码|NOTNULL&len<=20" ><Input class= 'codename' name=DoctName style="width:152px" ><font color=red> *</font>                                             
         </TD>                                              
       </TR>
       <TR  class= common>
       	<TD  class= title>
           签订时间
         </TD>
         <TD  class= input>
           <Input class= 'coolDatePicker' dateFormat="Short" style="width:152px" name=IdiogrDate verify="签订时间|DATE">
         </TD>
         <TD  class= title>
           合同起始时间
         </TD>
         <TD  class= input>
           <Input class= 'coolDatePicker' dateFormat="Short" style="width:152px" name=ContraBeginDate verify="合同起始时间|DATE">
         </TD>
         <TD  class= title>
           合同终止时间
         </TD>
         <TD  class= input>
           <Input class= 'coolDatePicker' dateFormat="Short" style="width:152px" name=ContraEndDate verify="合同终止时间|DATE">
         </TD>
       </TR>
       <TR  class= common>
         <TD  class= title>
           合同当前状态
         </TD>
         <TD  class= input>  
         	 <input class=codeno name=ContraState ondblClick= "showCodeList('lhcontrastate',[this,ContraStateName],[0,1],null,null,null,'',200);" onkeyup=" if(event.keyCode ==13)  return showCodeList('',[this,ContraStateName],[1,0],null,fm.ContraStateName.value,'ContraStateName');"   onkeydown="return showCodeListKey('',[this,ContraStateName],[1,0],null,fm.ContraStateName.value,'ContraStateName'); codeClear(ContraStateName,ContraState);"><Input class= 'codename' name=ContraStateName verify="|len<=2" >	
         </TD>
         
         
         <TD  class= title>
           
         </TD>
         <TD  class= input>
         </TD>
       </tr>
    </table>
    </Div>
    
    <table>
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLHContGrid2);">
    		</td>
    		 <td class= titleImg>
        		 责任项目信息
       		 </td>  
       	<td style="width:350px;">
		  		</td>
					<TD align ="right">
							<Input type=button class=cssbutton style="width:110px;" name=getServicePrice value="服务价格表设置" onclick=" getService();">
					</TD>
					<TD align ="right">
							<Input type=button class=cssbutton style="width:110px;" name=getChargeBalance value="费用结算方式设置" onclick=" getCharge();">
					</TD> 
					<TD align ="right">
							<Input type=button class=cssbutton style="width:110px;" name=getAssociateSetting value="返回至主责任"  disabled=true onclick="getNewAssociate();">
					</TD>		
				  
    	</tr>
    </table>
   	<Div id="divLHContGrid2" style="display:''">
   		<div id="divDutyExolai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textDutyExolai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backDutyExolai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backDutyExolaiButton()">
			 </div>
			 <div id="divFeeExplai"  style="display: none; position:absolute; slategray">
    	     <textarea name="textFeeExplai" cols="100%" rows="3" witdh=25% class="common" onkeydown="backFeeExplai();">
    	     </textarea>
    	     <input type=button class=cssButton value="返回" OnClick="backFeeExplaiButton()">
			 </div>
	    <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanContGrid">
	  				</span> 
			    </td>
				</tr>
			</table>
		</div>
    <div id="div1" style="display: 'none'">
      <table>
        <TR  class= common>
            <TD  class= title>
              操作员代码
            </TD>
            <TD  class= input>
              <Input class= 'common' name=Operator >
            </TD>
            <TD  class= title>
              入机日期
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeDate >
            </TD>
            <TD  class= title>
              入机时间
            </TD>
            <TD  class= input>
              <Input class= 'common' name=MakeTime >
            </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最后一次修改日期
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyDate >
          </TD>
          <TD  class= title>
            最后一次修改时间
          </TD>
          <TD  class= input>
            <Input class= 'common' name=ModifyTime >
          </TD>
        </TR>
      </table>
    </div>
      <input type=hidden id="fmtransact" name="fmtransact">
      <input type=hidden id="fmAction" name="fmAction">
      <input type=hidden name = HiddenBtn>
      <input type=hidden name = Flag>
      <input type=hidden name = HiddenContraNo>
      <input type=hidden name = HiddenContraItemNo>
      <input type=hidden name = ContraNo2>
      <input type=hidden name = ContraItemNo2>
      <input type=hidden id="iscomefromquery" name="iscomefromquery">
 </form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</div>
</body>
</html>
