<%
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LLHospitalDetailQueryInit.jsp"%>
<SCRIPT src="./LLHospitalDetailQueryInput.js"></SCRIPT>

</head>
<body  onload="initForm();initElementtype();" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="#" method="post" name="fm" target="fraSubmit">
    <table>
	   	  <tr>
	        <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHospital);"></td>
	    	<td class= titleImg>医疗问题查询</td>
	      </tr>
    </table>
    
	<div  id= "divHospital" style= "display: ''">
	<table  class= common>
		<tr  class= common> 
		    <td  class= title> 
		            医院行为号
		    </td>
		    <td>
		    	<input class="common" name="SerialNo" readonly=true >
		    </td>
		    <td  class= title> 
					管理机构
			</td>
			<td  class=input> 
					<Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName>
			</td>         
		    <td  class= title> 
		            医院名称
		    </td>
		    <td  class= input>
		    		<input class="codeNo" name="HospitalCode" verify="医院编码|notnull" ondblclick="return showCodeList('llhospiquery',[this,HospitalName],[0,1],null,fm.HospitalName.value,'Hospitname',1);" onkeyup="return showCodeListKey('llhospiquery',[this,HospitalName],[0,1],null,fm.HospitalName.value,'Hospitname',1);"><input class="codename" name="HospitalName" elementtype="nacessary">
		    </td>     
	    </tr>
	    <tr  class= common> 
              <td  class= title> 
                 	信息渠道    
              </td>
              <td class= input> 
              	<input class="codeNo" name="MsgChannelCode" verify="信息渠道|code:MsgChannelCode&notnull" ondblclick="return showCodeList('MsgChannelCode',[this,MsgChannel],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('MsgChannelCode',[this,MsgChannel],[0,1],null,null,null,1);"><input class="codename" name="MsgChannel">
              </td>
              <td  class= title> 
		         责任人姓名
		     </td>
			 <td  class= input> 
			     <input class="common" name="Charger" elementtype="nacessary"> 
			 </td>
		
			 <td class="title">
             		责任人职业
             </td>
	         <td class="input">
	         		<Input class="codeNo" name="ProfessionalCode" CodeData="0|^0|医生^1|护士^2|不详" ondblclick="return showCodeListEx('ProfessionalCode',[this,Professional],[0,1]);" onkeyup="return showCodeListEx('ProfessionalCode',[this,Professional],[0,1]);"><Input class="codeName" name="Professional">
	         </td> 
	    </tr>
	    <tr class="common">
		    <td  class= title> 
	        	 发生起期
	        </td>
	        <td  class= input> 
	        	 <input class= "coolDatePicker" dateFormat="short" name="StartDate" verify="受理时间止期|NOTNULL" >
	        </td>
	        <td  class= title> 
       	 		发生止期
       	 	</td>
	        <td  class= input> 
	       	 	<input class= "coolDatePicker" dateFormat="short" name="EndDate" verify="受理时间止期|NOTNULL" >
	        </td>
	        <td></td>
	    </tr>
	    
     </table>
     </div>
    
 	<center>
		<table  class= "common">
		    <tr class= "common" align="left"> 
			    <td class=button>
            	  <input class=cssButton name="querybutton" VALUE="查  询"  TYPE="button" onclick="queryClick();">
            	  <input class=cssButton name="returnbutton" VALUE="返  回"  TYPE="button" onclick="returnClick();">
            	</td>  
		    </tr>    
	    </table>
    </center>
	 
    <table>
     <tr class="common">
         <td class="common">
               <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProblem);">
         </td> 
         <td class=titleImg>
               查询结果如下所示:
         </td>
     </tr>
    </table>
     <center>
		 <div  id= "divProblem" style= "display: ''">
		 	<span id="spanDataGrid"></span>
		 	<input value="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
            <input value="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
            <input value="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
            <input value="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
	     </div>
     </center>
	  <!--隐藏-->
	  <input type="hidden" name="FixFlag"/>
	  <input type="hidden" name="HosGrade"/>
	  <input type="hidden" name="UrbanFlag"/>
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
