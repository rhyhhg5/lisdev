//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass(); 
var turnPage2 = new turnPageClass(); 
var turnPage3 = new turnPageClass(); 
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提交，保存按钮对应操作
function submitForm()
{
	  var sqlContraItemNo="select ContraNo, ContraItemNo from LHContServPrice"
	                +" where  ContraNo='"+fm.all('ContraNo').value+"'"
	                +" and  ContraItemNo='"+fm.all('ContraItemNo').value+"'";
     //alert(sqlContraItemNo);
      //alert(easyExecSql(sqlContraItemNo));
     var arrResult = new Array(); 
     arrResult=easyExecSql(sqlContraItemNo);
     if(arrResult!=""&&arrResult!="null"&&arrResult!=null)
     {
       var  ContraNo=arrResult[0][0];
       var  ContraItemNo=arrResult[0][1];
     }
     //alert(ContraItemNo);
     if(fm.all('ContraNo').value==ContraNo&&fm.all('ContraItemNo').value==ContraItemNo)
     {
    	 alert("此合同编号的价格信息已经存在,不允许重复保存!");
    	 return false;
     }
	   if((ContraNo==""&&ContraItemNo=="")||(ContraNo=="null"&&ContraItemNo=="null")||(ContraNo==null&&ContraItemNo)==null)
     {  
        if( verifyInput2() == false ) return false;
        fm.fmtransact.value="INSERT||MAIN";
        var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
        showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
        fm.submit(); //提交
      }
} 
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LHServerPriceInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  //mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.fmtransact.value = "INSERT||MAIN" ;
}           
//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	
  //下面增加相应的代码
  if( verifyInput2() == false ) return false;
  if (confirm("您确实想修改该记录吗?"))
  {
  
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "UPDATE||MAIN";
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
 // alert(fm.HiddenBtn.value);
  var no;
  var xx=fm.HiddenBtn.value;
  showInfo=window.open("./LHServerPriceQueryInput.html?no="+xx,"信息查询",'width=1024,height=748,top=0,left=0,status=yes,menubar=yes,location=yes,directories=yes,resizable=yes,scrollbars=auto,toolbar=yes');
}           
//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.fmtransact.value = "DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}           
//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function afterQuery0( arrQueryResult )
{
  //alert(arrQueryResult[0][0]);
	//alert(arrQueryResult[0][1]);
	var sqlType="select ServPriceType from LHContServPrice where contrano='"+arrQueryResult[0][1]+"'";

	fm.ServicePriceCode.value=easyExecSql(sqlType);
 // alert(fm.ServicePriceCode.value);
	if(fm.ServicePriceCode.value=="2")
	{
		  divTestMoney.style.display='none';
		  divShowInfo.style.display='';
			divTestItemPriceGrid.style.display='';
			divTestGrpPriceGrid.style.display='';
		
					 var itemPriceSql= "select a.MedicaItemCode,"
					                 +"  CASE  WHEN "
 	                         +"(select b.MedicaItemCode from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) not like 'ZH%%' "
     											 +" THEN (select b.MedicaItemname from lhcountrmedicaitem b where b.MedicaItemCode = a.MedicaItemCode ) "
                           +" ELSE (select distinct c.Testgrpname from ldtestgrpmgt c where c.testgrpcode = a.MedicaItemCode ) END ,"
					                 +" MedicaItemPrice ,Explain "
					                 +" from LDTestPriceMgt a "
		                       +" where a.contrano='"+arrQueryResult[0][1]+"'"
		                       +" and a.PriceClass='JCXM'";
		                     //  +" and a.MedicaItemCode='"+arrQueryResult[0][12]+"'";
		                     //  +" and SerialNo='"+arrQueryResult[0][11]+"'";
		           // alert(itemPriceSql);
				      //  alert( easyExecSql(itemPriceSql));
				      turnPage.pageLineNum = 100;
				        turnPage.queryModal(itemPriceSql, TestItemPriceGrid);
		   
				    var testGrpPriceSql= "select a.MedicaItemCode,"
					                 +" (select distinct b.Testgrpname from ldtestgrpmgt b where b.Testgrpcode = a.MedicaItemCode ) ,"
					                 +" a.MedicaItemPrice ,a.Explain "
					                 +" from LDTestPriceMgt  a"
		                       +" where a.contrano='"+arrQueryResult[0][1]+"'"
		                       +" and a.PriceClass='TJTC'";
		                     //  +" and a.MedicaItemCode='"+arrQueryResult[0][12]+"'";
		                      // +" and ContraItemNo='"+arrQueryResult[0][10]+"'"
		                     //  +" and SerialNo='"+arrQueryResult[0][11]+"'";
		          //  alert(testGrpPriceSql);
		          //  alert( easyExecSql(testGrpPriceSql));
		          turnPage.pageLineNum = 50;
		         turnPage.queryModal(testGrpPriceSql, TestGrpPriceGrid);
		    
  }
  if(fm.ServicePriceCode.value=="1")
  {
  	  divTestMoney.style.display='';
			divTestItemPriceGrid.style.display='none';
			divTestGrpPriceGrid.style.display='none';
  }
						if(fm.HiddenBtn.value=="1")
						{
								var arrResult = new Array();
								if( arrQueryResult != null )
								{
							    		arrResult = arrQueryResult;
							    		//alert(arrResult[0][10]);	//+"(select case when a.ExpType = '1' then '支票' else '转帐' end from dual),"
											var mulSql = "select a.ContraNo,a.ContraName,"
											            +"(select HospitName from LDHospital where LDHospital.HospitCode=a.Contraobj)," 
													        +"(select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=a.ContraType),"
							                    +"(select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= a.DutyItemCode),"
								                  +"(select  case when a.ServPriceType = '1' then '服务单价' else '体检价格' end from dual),"
								                  +"( case a.ExpType when '1' then '健康体检费用' when '2' then '健康管理技术费用' when '3' then '医师费用' when '4' then '其他费用'when '5' then '核保体检' else '未知费用' end ),"
								                  +"(select LDHospital.HospitCode from LDHospital  where LDHospital.HospitCode=a.Contraobj),"
								                  +"(select Code from ldcode where  codetype = 'hmdeftype' and ldcode.Code=a.ContraType),"
								                  +"(select DutyItemCode from LDContraItemDuty where LDContraItemDuty.DutyItemCode= a.DutyItemCode),"
								                  +"a.ServPriceType,a.ExpType ,a.ContraItemNo ,"
								                  +"(select (select case when n.dutystate = '1' then '有效' else '无效' end from dual) from lhgroupcont m ,lhcontitem n where m.contrano=n.contrano and  n.ContraItemNo=a.ContraItemNo and m.contrano=a.contrano and m.contrano='"+arrResult[0][0]+"') ,"
								                  //+" b.MedicaItemPrice "
								                  +"a.MoneySum "
								                  +" from LHContServPrice a ,LDTestPriceMgt b "
							                    +" where a.contrano ='"+arrResult[0][0]+"' and a.contrano=b.contrano"
							                    +" and a.ContraItemNo=b.ContraItemNo and a.ContraItemNo ='"+arrResult[0][10]+"'";
							        //alert(mulSql);
									 		arrResult = easyExecSql(mulSql);		
									  	//alert(arrResult);    
									  	//alert(arrResult[0][14]);    
							        fm.all('ContraNo').value							= arrResult[0][0];
							        fm.all('ContraName').value						= arrResult[0][1];
							        fm.all('HospitName').value     	 			= arrResult[0][2];//合作机构名称
							        fm.all('ContraType_ch').value					= arrResult[0][3]; //合同责任项目类型名称
							        fm.all('ContraNowName_ch').value			= arrResult[0][4];// 合同责任项目名称
											fm.all('ServicePriceName').value			= arrResult[0][5];
											fm.all('BalanceTypeName').value 			= arrResult[0][6];
											fm.all('HospitCode').value						= arrResult[0][7];//合作机构编号
							        fm.all('ContraType').value						= arrResult[0][8];//合同责任项目类型编号
							        fm.all('ContraNowName').value					= arrResult[0][9]; // 合同责任项目编号
							        fm.all('ServicePriceCode').value			= arrResult[0][10];
							        fm.all('BalanceTypeCode').value 			= arrResult[0][11];
							        fm.all('ContraItemNo').value 			    = arrResult[0][12];		
							        fm.all('ContraNowState_ch').value 	  = arrResult[0][13];		
							        fm.all('MoneySum').value              = arrResult[0][14];	
								} 
							}
							if(fm.HiddenBtn.value=="2")
							{
								var arrResult = new Array();
								if( arrQueryResult != null )
								{
							    		arrResult = arrQueryResult;
							    	//	alert(arrQueryResult);
							    	//	alert(arrQueryResult[0][1]);	
											var mulSql = "select a.ContraNo,a.ContraName,"
											            +"(select DoctName from LDDoctor where LDDoctor.Doctno=a.Contraobj)," 
													        +"(select CodeName from ldcode where  codetype = 'hmdeftype' and ldcode.Code=a.ContraType),"
							                    +"(select DutyItemName from LDContraItemDuty where LDContraItemDuty.DutyItemCode= a.DutyItemCode),"
								                  +"(select  case when a.ServPriceType = '1' then '服务单价' else '体检价格' end from dual),"
								                  //+"(select case when a.ExpType = '1' then '支票' else '转帐' end from dual),"
								                  +"( case a.ExpType when '1' then '健康体检费用' when '2' then '健康管理技术费用' when '3' then '医师费用' when '4' then '其他费用' when '5' then '核保体检' else '未知费用' end ),"
								                  +"(select LDDoctor.Doctno from LDDoctor  where LDDoctor.Doctno=a.Contraobj),"
								                  +"(select Code from ldcode where  codetype = 'hmdeftype' and ldcode.Code=a.ContraType),"
								                  +"(select DutyItemCode from LDContraItemDuty where LDContraItemDuty.DutyItemCode= a.DutyItemCode),"
								                  +"a.ServPriceType,a.ExpType ,a.ContraItemNo,"
								                  +"(select (select case when n.dutystate = '1' then '有效' else '无效' end from dual) from LHUnitContra m ,lhcontitem n where m.contrano=n.contrano and  n.ContraItemNo=a.ContraItemNo and m.contrano=a.contrano and m.contrano='"+arrResult[0][1]+"'),"
								                  +" a.MoneySum "
								                  +" from LHContServPrice a ,LDTestPriceMgt b "
							                    +" where a.contrano ='"+arrResult[0][1]+"' and a.contrano=b.contrano"
							                    +" and a.ContraItemNo=b.ContraItemNo and a.ContraItemNo ='"+arrResult[0][10]+"'";
									 	//	alert(mulSql);
									 		arrResult = easyExecSql(mulSql);		
									 // 	alert(arrResult);    
							        fm.all('ContraNo').value							= arrResult[0][0];
							        fm.all('ContraName').value						= arrResult[0][1];
							        fm.all('HospitName').value     	 			= arrResult[0][2];//合作机构名称
							        fm.all('ContraType_ch').value					= arrResult[0][3]; //合同责任项目类型名称
							        fm.all('ContraNowName_ch').value			= arrResult[0][4];// 合同责任项目名称
											fm.all('ServicePriceName').value			= arrResult[0][5];
											fm.all('BalanceTypeName').value 			= arrResult[0][6];
											fm.all('HospitCode').value						= arrResult[0][7];//合作机构编号
							        fm.all('ContraType').value						= arrResult[0][8];//合同责任项目类型编号
							        fm.all('ContraNowName').value					= arrResult[0][9]; // 合同责任项目编号
							        fm.all('ServicePriceCode').value			= arrResult[0][10];
							        fm.all('BalanceTypeCode').value 			= arrResult[0][11];
							        fm.all('ContraItemNo').value 			    = arrResult[0][12];		
							        fm.all('ContraNowState_ch').value 	  = arrResult[0][13];	
							        fm.all('MoneySum').value              = arrResult[0][14];			
								} 
							}    
}  

function getNo()
{
    var sql="select max(ContraNo) from LHContServPrice where managecom='"+manageCom+"'";
    var result = easyExecSql(sql);
    var result2=parseInt(result)+1;
    var result3=result2.toString().substring(2);
    var result4;
    if(result3.length==4)
    {
      result4=result3.toString();
    }
    else if(result3.length==3)
    {
    	result4="0"+result3.toString();
    }
    else if(result3.length==2)
    {
    	result4="00"+result3.toString();
    }
    else if(result3.length==1)
    {
    	result4="000"+result3.toString();
    }
  // alert(result4);
		fm.ContraNo.value=manageCom+result4;
		//	fm.ContraNo.value=manageCom+(parseInt(result)+1);
}       

function afterCodeSelect(codeName,Field)
{
	//alert(codeName);
	if(codeName == "ServicePriceName")
	{
		//alert(fm.all('ServicePriceCode').value);
	  if(fm.all('ServicePriceCode').value == "1")//体检套餐价格
		{
			//alert("1");
			divTestMoney.style.display='';
			divTestMoney3.style.display='none';
			divTestItemPriceGrid.style.display='none';
			divTestGrpPriceGrid.style.display='none';
			divTestMoney2.style.display='none';
			divShowInfo.style.display='none';
		}
		if(fm.all('ServicePriceCode').value== "2")//核保体检类型
		{
			divShowInfo.style.display='';
			divTestMoney3.style.display='none';
			divTestItemPriceGrid.style.display='';
			divTestGrpPriceGrid.style.display='';
			divTestMoney.style.display='none';
			divTestMoney2.style.display='none';
		}

		if(fm.all('ServicePriceCode').value == "3")//服务单价
		{
			//alert("1");
			divTestMoney2.style.display='';
			divTestMoney.style.display='none';
			divTestMoney3.style.display='none';
			divTestItemPriceGrid.style.display='none';
			divTestGrpPriceGrid.style.display='none';
			divShowInfo.style.display='none';
		}
	  if(fm.all('ServicePriceCode').value == "4")//服务单价
		{
			 divTestMoney3.style.display='';
			 divTestMoney2.style.display='none';
		   divTestMoney.style.display='none';
		   divTestItemPriceGrid.style.display='none';
		   divTestGrpPriceGrid.style.display='none';
		   divShowInfo.style.display='none';
		}
		if(fm.all('ServicePriceCode').value== "2"&& fm.all('ContraType').value!= "02")//核保体检
		{
			divShowInfo.style.display='none';
			alert("只有合同责任项目类型为核保体检时才有体检价格信息!");
			divShowInfo.style.display='none';
			fm.all('ServicePriceCode').value = "";
			fm.all('ServicePriceName').value = "";
			
		}
	  //if(fm.all('ServicePriceCode').value== "2"&& fm.all('ContraType').value== "01")//体检项目价格
		//{
		//	//alert("2");
		//	divTestMoney.style.display='none';
		//	divTestItemPriceGrid.style.display='';
		//	divTestGrpPriceGrid.style.display='';
		//	divTestMoney2.style.display='none';
		//	divShowInfo.style.display='';
		//}
	}
}