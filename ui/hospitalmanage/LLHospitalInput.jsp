<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：就医管理
//程序功能：
//创建日期：2010-03-02
//创建人  ：丁剑伟
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="./LLHospitalInit.jsp"%>
<SCRIPT src="./LLHospitalInput.js"></SCRIPT>

</head>
<body  onload="initForm();initElementtype();" style="behavior:url(#default#clientCaps)" id="oClientCaps">
  <form action="./LLHospitalSave.jsp" method="post" name="fm" target="fraSubmit">
  <!--div id="operateButton"-->
	  <table  class=common align=center>
	          <tr align=right>
	                  <td class=button>
	                      &nbsp;&nbsp;
	                  </td>
	                  <td class=button width="10%" align=right>
	                      <input class=cssButton name="saveButton" VALUE="保  存"  TYPE=button onclick="addClick();">
	                  </td>
	                  <td class=button width="10%" align=right>
	                      <input class=cssButton name="modifyButton" VALUE="修  改"  TYPE=button onclick="updateClick();">
	                  </td>           
	                  <td class=button width="10%" align=right>
	                      <input class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="queryClick();">
	                  </td> 
	                  <td class=button width="10%" align=right>
	                  	  <input class=cssButton name="deletebutton" VALUE="删  除"  TYPE=button onclick="deleteClick();">
	                  </td>  
	          </tr>
	  </table>
  <!--/div--> 
  
    <table>
	   	  <tr>
	        <td class=common><img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHospital);"></td>
	    	<td class= titleImg>医疗行为管理</td>
	      </tr>
    </table>
    
	<div  id= "divHospital" style= "display: ''">
	<table  class= common>
	    <tr  class= common> 
		    <td  class= title> 
					管理机构
			</td>
			<td  class=input> 
				<Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&INT" ondblclick="return showCodeList('comcode',[this,ComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ComName],[0,1],null,null,null,1);"><input class=codename name=ComName>
			</td>         
			<td  class= title> 
	        		医院名称
	        </td>
			<td  class= input>
					<!--input class="codeNo" name="HospitalCode" verify="医院编码|code:comcode&notnull" ondblclick="return showCodeList('HospitalCode',[this,HospitalName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('HospitalCode',[this,HospitalName],[0,1],null,null,null,1);"-->
					<input class="codeNo"  name="HospitalCode" onclick="return showCodeList('llhospiquery',[this,HospitalName,FixFlag,HosGrade,UrbanFlag],[0,1,2,3,4],null,fm.HospitalName.value,'Hospitname',1,240);" onkeyup="return showCodeListKeyEx('llhospiquery',[this,HospitalName,FixFlag,HosGrade,UrbanFlag],[0,1,2,3,4],null,fm.HospitalName.value,'Hospitname',1,240);" verify="医院代码|notnull"><input class="codename" name="HospitalName" elementtype="nacessary" >
			</td>       
			<td  class= title> 
					信息渠道    
			</td>
			<td class= input> 
			    <input class="codeNo" name="MsgChannelCode" verify="信息渠道|code:MsgChannelCode&notnull" ondblclick="return showCodeList('MsgChannelCode',[this,MsgChannel],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('MsgChannelCode',[this,MsgChannel],[0,1],null,null,null,1);"><input class="codename" name="MsgChannel" readonly=true >
			</td>
	    </tr>
	    <tr  class= "common"> 
		     <td  class= title> 
		         	责任人姓名
		     </td>
			 <td  class= input> 
			     <input class="common" name="Charger" elementtype=nacessary> 
			 </td>
		
	         <td class="title">
	             责任人职业
	         </td>
	         <td class="input">
	         		<Input class="codeNo" name="ProfessionalCode" CodeData="0|^0|医生^1|护士^2|不详" ondblclick="return showCodeListEx('ProfessionalCode',[this,Professional],[0,1]);" onkeyup="return showCodeListEx('ProfessionalCode',[this,Professional],[0,1]);"><Input class="codeName" name="Professional" readOnly=true>
	         </td> 
	    
	         <td  class= title> 
	              发生日期
	         </td>
	         <td  class= input> 
	         	<input class= "coolDatePicker" dateFormat="short" name="BussinessDate" verify="受理时间止期|NOTNULL" >
	         </td>
	    </tr>
		    <tr>
			    <td  class= title> 
		        	医疗行为号
		        </td>
			   <td  class= input> 
			   		<input class= "common" name="SerialNo" readOnly=true>
			   </td>
		    </tr>
     </table>
     </div>
    
    
	   
     <table>
	     <tr class="common">
	         <td class="common">
	               <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divProblem);">
	         </td> 
	         <td class=titleImg>
	               问题描述
	         </td>
	     </tr>
     </table>
     <div  id= "divProblem" style= "display: ''">
     	<center>
			<table  class= "common">
			    <tr class= "common"> 
			    	<td class="common" align="left" width="100%">
		             	<textarea class="common" name="ProblemDetail" cols="120" rows="4">
		             	</textarea>
		            </td> 
			    </tr>    
		    </table>
	    </center>
     </div>
     
     <table>
	     <tr class="common">
	         <td class="common">
	               <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAction);">
	         </td> 
	         <td class=titleImg>
	               采取动作及结果
	         </td>
	     </tr>
     </table>
	 <div  id= "divAction" style= "display: ''">
	 	<center>
			<table  class= "common">
			    <tr class= "common"> 
			    	<td class="common" align="left" width="100%">
		             	<textarea class="common" name="Action" cols="120" rows="4" value="">
		             	</textarea>
		            </td> 
			    </tr>    
		    </table>
	    </center>
	 </div>
	 
	  <div id="operateButton" >
		  <table  class="common" align="center">
		          <tr align="left">
		                  <!--td class=button width="10%">
		                      <input class=cssButton name="AddScan" VALUE="添加扫描件"  TYPE=button onclick="addScan();">
		                  </td-->
		                  <td class=button width="10%">
		                      <input class=cssButton name="QueryScan" VALUE="查询扫描件"  TYPE=button onclick="queryScan();">
		                  </td>           
		                  <td class=button width="10%">
		                      <!--input class=cssButton name="HospitalProblemQuery" VALUE="医院问题查询"  TYPE=button onclick="hospitalProblemQuery();"-->
		                      <input class=cssButton name="DoctorProblemQuery" VALUE="医护问题查询"  TYPE=button onclick="doctorProblemQuery();">
		                  </td> 
		                  <td class=button width="10%">
		                  	  <!--input class=cssButton name="DoctorProblemQuery" VALUE="医护问题查询"  TYPE=button onclick="doctorProblemQuery();"-->
		                  	<input class=cssButton name="HospitalProblemQuery" VALUE="医院问题查询"  TYPE=button onclick="hospitalProblemQuery();">
		                  </td>  
		                  <!--td class=button width="10%">
	                  	  	  <input class=cssButton name="EvaluationData" VALUE="评价数据提取"  TYPE=button onclick="evaluationData();">
	                  	  </td--> 
		                  <td class=button>
	                      	&nbsp;
	                      </td>
		          </tr>
		  </table>
	  </div> 
	  <!--隐藏-->
	  <input type="hidden" name="FixFlag" value=""/>
	  <input type="hidden" name="HosGrade" value=""/>
	  <input type="hidden" name="UrbanFlag" value=""/>

	  <input type="hidden" id="fmtransact" name="fmtransact" value="">
	  <input type="hidden" id="hiddenSerialNo" name="hiddenSerialNo" value="">
	  <input type="hidden" id="hiddenFlag" name="hiddenFlag" value="">
	  
	  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>