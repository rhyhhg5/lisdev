<%
  //程序名称：HMSuggestInit.jsp
  //程序功能：专项健康建立录入
  //创建日期：2010-3-17
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initForm()
{
	try{
		initDisSugGrid();
		initNotSendCustGrid();
		initAlSendGrid();
		
		//
		initCustNotSugGrid();
		initCustAlSugGrid();
		initCustAlSendGrid();
	}
	catch(re){
		alert("HMSuggestInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initDisSugGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="疾病代码";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="疾病名称";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="录入时间";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;


		DisSugGrid = new MulLineEnter( "fm" , "DisSugGrid" ); 

		DisSugGrid.mulLineCount=1;
		DisSugGrid.displayTitle=1;
		DisSugGrid.canSel=1;
		DisSugGrid.canChk=0;
		DisSugGrid.hiddenPlus=1;
		DisSugGrid.hiddenSubtraction=1;
		DisSugGrid.selBoxEventFuncName = "selectInfoGrid";
		DisSugGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initNotSendCustGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="客户号";
		iArray[1][1]="100px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="客户姓名";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="手机号码";
		iArray[3][1]="120px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="邮件地址";
		iArray[4][1]="120px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="住址";
		iArray[5][1]="150px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="疾病代码";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="疾病名称";
		iArray[7][1]="120px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="最近一次就诊结束时间";
		iArray[8][1]="120px";
		iArray[8][2]=100;
		iArray[8][3]=0;

		iArray[9]=new Array();
		iArray[9][0]="最近一年就诊次数";
		iArray[9][1]="100px";
		iArray[9][2]=100;
		iArray[9][3]=0;
		
		iArray[10]=new Array();
		iArray[10][0]="最近一年医疗费用理赔额度";
		iArray[10][1]="0px";
		iArray[10][2]=100;
		iArray[10][3]=3;
		
		iArray[11]=new Array();
		iArray[11][0]="就诊方式";
		iArray[11][1]="0px";
		iArray[11][2]=100;
		iArray[11][3]=3;


		NotSendCustGrid = new MulLineEnter( "fm" , "NotSendCustGrid" ); 

		NotSendCustGrid.mulLineCount=1;
		NotSendCustGrid.displayTitle=1;
		NotSendCustGrid.canSel=0;
		NotSendCustGrid.canChk=1;
		NotSendCustGrid.hiddenPlus=1;
		NotSendCustGrid.hiddenSubtraction=1;

		NotSendCustGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initAlSendGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=1;

		iArray[1]=new Array();
		iArray[1][0]="客户号";
		iArray[1][1]="120px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="客户姓名";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=1;

		iArray[3]=new Array();
		iArray[3][0]="手机号码";
		iArray[3][1]="120px";
		iArray[3][2]=100;
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="邮件地址";
		iArray[4][1]="120px";
		iArray[4][2]=100;
		iArray[4][3]=1;

		iArray[5]=new Array();
		iArray[5][0]="住址";
		iArray[5][1]="200px";
		iArray[5][2]=100;
		iArray[5][3]=1;
		
		iArray[6]=new Array();
		iArray[6][0]="疾病代码";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=1;
		
		iArray[7]=new Array();
		iArray[7][0]="疾病名称";
		iArray[7][1]="120px";
		iArray[7][2]=100;
		iArray[7][3]=1;

		iArray[8]=new Array();
		iArray[8][0]="最近一次就诊结束时间";
		iArray[8][1]="120px";
		iArray[8][2]=100;
		iArray[8][3]=1;

		iArray[9]=new Array();
		iArray[9][0]="最近一年就诊次数";
		iArray[9][1]="120px";
		iArray[9][2]=100;
		iArray[9][3]=1;

		iArray[10]=new Array();
		iArray[10][0]="理赔额度";
		iArray[10][1]="60px";
		iArray[10][2]=100;
		iArray[10][3]=1;

		iArray[11]=new Array();
		iArray[11][0]="健康建议发送时间";
		iArray[11][1]="120px";
		iArray[11][2]=100;
		iArray[11][3]=1;

		iArray[12]=new Array();
		iArray[12][0]="发送方式";
		iArray[12][1]="60px";
		iArray[12][2]=100;
		iArray[12][3]=1;
		
		iArray[13]=new Array();
		iArray[13][0]="发送结果";
		iArray[13][1]="100px";
		iArray[13][2]=100;
		iArray[13][3]=1;
		
		iArray[14]=new Array();
		iArray[14][0]="发送未成功原因";
		iArray[14][1]="120px";
		iArray[14][2]=100;
		iArray[14][3]=1;
		
		iArray[15]=new Array();
		iArray[15][0]="流水号";
		iArray[15][1]="0px";
		iArray[15][2]=100;
		iArray[15][3]=3;


		AlSendGrid = new MulLineEnter( "fm" , "AlSendGrid" ); 

		AlSendGrid.mulLineCount=1;
		AlSendGrid.displayTitle=1;
		AlSendGrid.canSel=1;
		AlSendGrid.canChk=0;
		AlSendGrid.hiddenPlus=1;
		AlSendGrid.hiddenSubtraction=1;
		AlSendGrid.selBoxEventFuncName = "selectAlSendGrid";
		AlSendGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

//******************************************************************************************
function initCustNotSugGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=1;

		iArray[1]=new Array();
		iArray[1][0]="客户号";
		iArray[1][1]="100px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="客户姓名";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=1;

		iArray[3]=new Array();
		iArray[3][0]="手机号码";
		iArray[3][1]="120px";
		iArray[3][2]=100;
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="邮件地址";
		iArray[4][1]="120px";
		iArray[4][2]=100;
		iArray[4][3]=1;

		iArray[5]=new Array();
		iArray[5][0]="住址";
		iArray[5][1]="200px";
		iArray[5][2]=100;
		iArray[5][3]=1;

		iArray[6]=new Array();
		iArray[6][0]="最近一次就诊结束时间";
		iArray[6][1]="150px";
		iArray[6][2]=100;
		iArray[6][3]=1;

		iArray[7]=new Array();
		iArray[7][0]="最近一年就诊次数";
		iArray[7][1]="120px";
		iArray[7][2]=100;
		iArray[7][3]=1;

		iArray[8]=new Array();
		iArray[8][0]="就诊方式编码";
		iArray[8][1]="0px";
		iArray[8][2]=100;
		iArray[8][3]=3;
		
		iArray[9]=new Array();
		iArray[9][0]="就诊方式";
		iArray[9][1]="0px";
		iArray[9][2]=100;
		iArray[9][3]=3;

		iArray[10]=new Array();
		iArray[10][0]="最近一年医疗费用理赔金额";
		iArray[10][1]="150px";
		iArray[10][2]=100;
		iArray[10][3]=1;


		CustNotSugGrid = new MulLineEnter( "fmsave" , "CustNotSugGrid" ); 

		CustNotSugGrid.mulLineCount=1;
		CustNotSugGrid.displayTitle=1;
		CustNotSugGrid.canSel=1;
		CustNotSugGrid.canChk=0;
		CustNotSugGrid.hiddenPlus=1;
		CustNotSugGrid.hiddenSubtraction=1;

		CustNotSugGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initCustAlSugGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="50px";
		iArray[0][2]=200;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="客户姓名";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="客户号";
		iArray[2][1]="100px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="手机号";
		iArray[3][1]="120px";
		iArray[3][2]=100;
		iArray[3][3]=0;
		
		iArray[4]=new Array();
		iArray[4][0]="邮件地址";
		iArray[4][1]="120px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="住址";
		iArray[5][1]="120px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="健康建议录入时间";
		iArray[6][1]="120px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		
		iArray[7]=new Array();
		iArray[7][0]="最近一次就诊结束时间";
		iArray[7][1]="0px";
		iArray[7][2]=100;
		iArray[7][3]=3;

		iArray[8]=new Array();
		iArray[8][0]="最近一年就诊次数";
		iArray[8][1]="0px";
		iArray[8][2]=100;
		iArray[8][3]=3;

		iArray[9]=new Array();
		iArray[9][0]="就诊方式编码";
		iArray[9][1]="0px";
		iArray[9][2]=100;
		iArray[9][3]=3;
		
		iArray[10]=new Array();
		iArray[10][0]="就诊方式";
		iArray[10][1]="0px";
		iArray[10][2]=100;
		iArray[10][3]=3;

		iArray[11]=new Array();
		iArray[11][0]="最近一年医疗费用理赔金额";
		iArray[11][1]="0px";
		iArray[11][2]=100;
		iArray[11][3]=3;
		

		CustAlSugGrid = new MulLineEnter( "fmsave" , "CustAlSugGrid" ); 

		CustAlSugGrid.mulLineCount=1;
		CustAlSugGrid.displayTitle=1;
		CustAlSugGrid.canChk=1;
		CustAlSugGrid.canSel=1;
		CustAlSugGrid.hiddenPlus=1;
		CustAlSugGrid.hiddenSubtraction=1;
		CustAlSugGrid.selBoxEventFuncName = "selectAlSuggestGrid";
		CustAlSugGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initCustAlSendGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=1;

		iArray[1]=new Array();
		iArray[1][0]="客户号";
		iArray[1][1]="100px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="客户姓名";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=1;

		iArray[3]=new Array();
		iArray[3][0]="手机号码";
		iArray[3][1]="120px";
		iArray[3][2]=100;
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="邮件地址";
		iArray[4][1]="150px";
		iArray[4][2]=100;
		iArray[4][3]=1;

		iArray[5]=new Array();
		iArray[5][0]="住址";
		iArray[5][1]="200px";
		iArray[5][2]=100;
		iArray[5][3]=1;

		iArray[6]=new Array();
		iArray[6][0]="最近一次就诊结束时间";
		iArray[6][1]="130px";
		iArray[6][2]=100;
		iArray[6][3]=1;

		iArray[7]=new Array();
		iArray[7][0]="最近一年就诊次数";
		iArray[7][1]="120px";
		iArray[7][2]=100;
		iArray[7][3]=1;

		iArray[8]=new Array();
		iArray[8][0]="理赔额度";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=1;

		iArray[9]=new Array();
		iArray[9][0]="健康建议发送时间";
		iArray[9][1]="120px";
		iArray[9][2]=100;
		iArray[9][3]=1;

		iArray[10]=new Array();
		iArray[10][0]="发送方式";
		iArray[10][1]="60px";
		iArray[10][2]=100;
		iArray[10][3]=1;
		
		iArray[11]=new Array();
		iArray[11][0]="发送结果";
		iArray[11][1]="100px";
		iArray[11][2]=100;
		iArray[11][3]=1;
		
		iArray[12]=new Array();
		iArray[12][0]="发送未成功原因";
		iArray[12][1]="120px";
		iArray[12][2]=100;
		iArray[12][3]=1;
		
		iArray[13]=new Array();
		iArray[13][0]="流水号";
		iArray[13][1]="0px";
		iArray[13][2]=100;
		iArray[13][3]=3;


		CustAlSendGrid = new MulLineEnter( "fmsave" , "CustAlSendGrid" ); 

		CustAlSendGrid.mulLineCount=1;
		CustAlSendGrid.displayTitle=1;
		CustAlSendGrid.canSel=1;
		CustAlSendGrid.canChk=0;
		CustAlSendGrid.hiddenPlus=1;
		CustAlSendGrid.hiddenSubtraction=1;
		CustAlSendGrid.selBoxEventFuncName = "selectCustAlSendGrid";
		CustAlSendGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
