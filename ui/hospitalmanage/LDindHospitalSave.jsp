<%
//程序名称：LLHospitalSave.jsp
//程序功能：
//创建日期：2010-03-04 15:39:06
//创建人  ：DINGJW程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>

<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.hospitalmanage.*"%>   
  
<%  //接收信息，并作校验处理。
  //输入参数
  System.out.println("------------------------DingDianYiYuanPeiZhi------------------------");
  LDDingHospitalSchema tLDDingHospitalSchema = new LDDingHospitalSchema();
  LDindHospitalUI tLDindHospitalUI = new LDindHospitalUI();
  VData mmResult = new VData();
  
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");              //获得用户信息
  CErrors tError = null;                                 //错误类的获得
  String tOperate=request.getParameter("fmtransact");   //获得操作符号
  tOperate=tOperate.trim();
                                                         
  VData tVData = new VData();                            // 准备传输数据 VData
  String FlagStr="";
  String Content="";
  	tLDDingHospitalSchema.setManageCom(request.getParameter("ManageCom"));
  	tLDDingHospitalSchema.setHospitName(request.getParameter("HospitalName"));
  	tLDDingHospitalSchema.setHospitCode(request.getParameter("HospitCode"));
  	tLDDingHospitalSchema.setRiskcode(request.getParameter("RiskCode"));
  	tLDDingHospitalSchema.setValidate(request.getParameter("ValiDate"));
  	tLDDingHospitalSchema.setCInvalidate(request.getParameter("CInValiDate"));

    tVData.add(tG);					//准备向后方传输的数据	
    tVData.addElement(tLDDingHospitalSchema);
    
    try{
    	tLDindHospitalUI.submitData(tVData,tOperate); 
    	System.out.println("------------LDindHospitalSave.jsp--------------");
    }catch(Exception ex)
  	{
    		Content = "保存失败，原因是:" + ex.toString();
    		System.out.println(Content);
    		FlagStr = "Fail";
  	}
    if (!FlagStr.equals("Fail"))
  	{
   	    tError = tLDindHospitalUI.mErrors;
	    if (!tError.needDealError())
	    {
	    		if (tOperate.equals("INSERT||MAIN"))
	            { 
	                mmResult = tLDindHospitalUI.getResult();
	                Content="添加成功";
	             }
	             if (tOperate.equals("UPDATE||MAIN"))
	             {
	                 mmResult = tLDindHospitalUI.getResult();
	                 Content="修改成功";
	             }   
	                  
	             if(tOperate.equals("DELETE||MAIN"))
	             {
	                 mmResult = tLDindHospitalUI.getResult();
	                 Content="删除成功";
	             }
	    		 FlagStr = "Succ";
	     }
	   	 else
	     {
	    		Content = " 保存失败，原因是:" + tError.getFirstError();
	    		FlagStr = "Fail";
	      }
  	}
    System.out.println(Content);
    System.out.println(FlagStr);
  	
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>  	