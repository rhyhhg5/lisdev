<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHContSave.jsp
//程序功能：
//创建日期：2005-03-19 15:05:48
//创建人  ：CrtHtml程序创建
//更新记录： 对新字段内容的操作
// 更新人 : 郭丽颖
// 更新日期    : 2006-03-03 10:38:48
// 更新原因/内容: 插入新的字段
%>
//用户校验类
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.hospitalmanage.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数     
  System.out.println("***********begin save page**************");
  String contrano="";
  LHUnitContraSchema tLHUnitContraSchema   = new LHUnitContraSchema();
  LHContItemSet tLHContItemSet  =new  LHContItemSet();
  LHContraAssoSettingSet tLHContraAssoSettingSet = new LHContraAssoSettingSet();		//关联合同责任信息
  LHUnitContraUI tLHUnitContraUI   = new LHUnitContraUI();
  System.out.println("***********begin init**************");
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
    transact = request.getParameter("fmtransact");
    System.out.println("**AABBBBBBBBBBBBAAAAA**" +transact);
    tLHUnitContraSchema.setContraNo(request.getParameter("ContraNo"));
    tLHUnitContraSchema.setContraName(request.getParameter("ContraName"));
    tLHUnitContraSchema.setDoctNo(request.getParameter("DoctNo"));
    tLHUnitContraSchema.setIdiogrDate(request.getParameter("IdiogrDate"));
    tLHUnitContraSchema.setContraBeginDate(request.getParameter("ContraBeginDate"));
    tLHUnitContraSchema.setContraEndDate(request.getParameter("ContraEndDate"));
    tLHUnitContraSchema.setContraState(request.getParameter("ContraState"));
    
    tLHUnitContraSchema.setOperator(request.getParameter("Operator"));
    tLHUnitContraSchema.setMakeDate(request.getParameter("MakeDate"));
    tLHUnitContraSchema.setMakeTime(request.getParameter("MakeTime"));
    tLHUnitContraSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLHUnitContraSchema.setModifyTime(request.getParameter("ModifyTime"));
    
       String tContraItemNo[]=request.getParameterValues("ContGrid9");
       String tContraType[]=request.getParameterValues("ContGrid5");
       String tDutyItemCode[]=request.getParameterValues("ContGrid6");
       String tDutyState[]=request.getParameterValues("ContGrid7");
    //   String tDutyExolai[]=request.getParameterValues("ContGrid5");
    //   String tFeeExplai[]=request.getParameterValues("ContGrid6");
       String tContraFlag[]=request.getParameterValues("ContGrid8");
       
    
    int LHUnitContraCount = 0;
			if (tContraItemNo!= null) LHUnitContraCount = tContraItemNo.length;
	        
			for (int i = 0; i < LHUnitContraCount; i++)	
			{
				LHContItemSchema tLHContItemSchema=new LHContItemSchema();
				tLHContItemSchema.setContraNo(request.getParameter("ContraNo"));
				tLHContItemSchema.setContraType(tContraType[i]);
				tLHContItemSchema.setContraItemNo(tContraItemNo[i]);
				 System.out.println("PPPAAAAAA"+tLHContItemSchema.getContraItemNo());
				tLHContItemSchema.setDutyItemCode(tDutyItemCode[i]);
				tLHContItemSchema.setDutyState(tDutyState[i]);
			//	tLHContItemSchema.setDutyExolai(tDutyExolai[i]);
			//	tLHContItemSchema.setFeeExplai(tFeeExplai[i]);
		   	tLHContItemSchema.setContraFlag(tContraFlag[i]);
				
				tLHContItemSchema.setOperator(request.getParameter("Operator"));
        tLHContItemSchema.setMakeDate(request.getParameter("MakeDate"));
        tLHContItemSchema.setMakeTime(request.getParameter("MakeTime"));
        tLHContItemSchema.setModifyDate(request.getParameter("ModifyDate"));
        tLHContItemSchema.setModifyTime(request.getParameter("ModifyTime"));
        
         tLHContItemSet.add(tLHContItemSchema);
         
         System.out.println("****^^^^^^^^^^***"+tLHContItemSchema.getContraFlag());

           LHContraAssoSettingSchema tLHContraAssoSettingSchema = new LHContraAssoSettingSchema();
           tLHContraAssoSettingSchema.setContraNo(request.getParameter("ContraNo2"));
		       tLHContraAssoSettingSchema.setContraItemNo(request.getParameter("ContraItemNo2")); //合同责任项目编号(系统流水号)
  
  			   System.out.println("QQQQQQQQQQQQQQ"+tLHContraAssoSettingSchema.getContraNo());
      	   
      	   tLHContraAssoSettingSchema.setOperator(request.getParameter("Operator"));
				   tLHContraAssoSettingSchema.setMakeDate(request.getParameter("MakeDate"));
				   tLHContraAssoSettingSchema.setMakeTime(request.getParameter("MakeTime"));                             
			                           
				   tLHContraAssoSettingSet.add(tLHContraAssoSettingSchema);
        
        
        
       
      }
      System.out.println("***********end get data **************");
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	  tVData.add(tLHUnitContraSchema);
	  tVData.add(tLHContraAssoSettingSet);
	  tVData.add(tLHContItemSet);  
  	tVData.add(tG);  	
  	System.out.println("***********after submit data**************");
  	
    tLHUnitContraUI.submitData(tVData,transact);
   
    
    if ( FlagStr.equals("Success") && "INSERT||MAIN".equals(transact))
     {
      VData res = tLHUnitContraUI.getResult();
      System.out.println("res================"+res);
      LHUnitContraSchema mLHUnitContraSchema = new LHUnitContraSchema();
      
      mLHUnitContraSchema.setSchema((LHUnitContraSchema) res.
                                          getObjectByObjectName("LHUnitContraSchema",
                  0));
      
      
    	contrano =(String)mLHUnitContraSchema.getContraNo();
     }
     
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLHUnitContraUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    	  
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
      //	parent.fraInterface.afterQuery0(fm.all("ContraNo").value);
      //  alert(parent.fraInterface.fm.all("ContraNo").value);
    	 // parent.fraInterface.fm.all("ContraNo").value=""	
    	var FlagStr="<%=FlagStr%>";
    	//alert(FlagStr);
    	if(FlagStr!="Fail")
    	{
           var transact = "<%=transact%>";
          // alert("AAAAAAAAAAAAAA"+transact);
	         if(transact!="DELETE||MAIN")
	          {
	        	    var arrResult = new Array();
	             arrResult[0] =new Array();
	             arrResult[0][0] = parent.fraInterface.fm.all("ContraNo").value;
	             //alert(arrResult[0][0]);
	             parent.fraInterface.afterQuery0(arrResult); 
	            // top.opener.focus();
	            // top.close();
	          }
	     }
	     //else
	     //{
	     // 	//alert("请输入完整的个人合同信息");
	     //}
</script>
</html>
