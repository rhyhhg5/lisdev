<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<html>

<%
 //程序名称：HMAddCustDocDetailInput.jsp
 //程序功能：添加客户档案明细
 //创建日期：2010-3-8
 //创建人  ：chenxw
 //更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tCustomerNo = (String)request.getParameter("CustomerNo");
  String tIDType = (String)request.getParameter("IDType");
  String tIDNo = (String)request.getParameter("IDNo");
%>
<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
  
  var tCustomerNo = "<%=tCustomerNo%>";
  var tIDType = "<%=tIDType%>";
  var tIDNo = "<%=tIDNo%>";
</script>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="HMAddCustDocDetail.js"></SCRIPT>
  <%@include file="HMAddCustDocDetailInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./HMAddCustDocDetailSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >客户信息</td>
  </tr>
</table>
<table  class="common" >
  <tr class="common">
    <td class="title">姓名</td><td class="input"><input class="readonly" name="Name" readonly ></td>  
    <td class="title">性别</td><td class="input"><input class="readonly" name="Sex" readonly ></td>  
    <td class="title">出生日期</td><td class="input"><input class="readonly" name="Birthday" readonly ></td>  
  </tr>
	<tr class="common">
    <td class="title">客户号</td><td class="input"><input class="readonly" name="CustNo" value="<%=tCustomerNo%>" readonly ></td>  
    <td class="title">手机号</td><td class="input"><input class="readonly" name="MobileNo" readonly ></td>  
    <td class="title">电话</td><td class="input"><input class="readonly" name="TelNo" readonly ></td>  
  </tr>
	<tr class="common">
    <td class="title">电子邮箱</td><td class="input"><input class="readonly" name="EMail" readonly ></td>  
    <td class="title">联系地址</td><td class="input"><input class="readonly" name="Addr" readonly ></td>  
    <td class="title">邮编</td><td class="input"><input class="readonly" name="Zip" readonly ></td>  
  </tr>
	<tr class="common">
    <!-- <td class="title">投保企业名称</td><td class="input"><input class="readonly" name="AppEnterprise" readonly ></td>-->  
   <!-- <td class="title">建档时间</td><td class="input"><input class="readonly" name="BuildTime" readonly ></td>-->
  </tr>
</table>
<table>
  <tr>
    <td class="titleImg" >有效保单信息</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanPolGrid" >
     </span> 
      </td>
   </tr>
</table>
<table>
  <tr>
    <td class="titleImg" >检查阳性指标</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanExamIndGrid" >
     </span> 
      </td>
   </tr>
</table>
<table>
  <tr>
    <td class="titleImg" >既往病史</td>
  </tr>
</table>
<textarea class="common" name="PastDisHistory" cols="100%" rows="2" ></textarea>
<br><BR>
<table>
  <tr>
    <td class="titleImg" >家族病史</td>
  </tr>
</table>
<textarea class="common" name="FamilyDisHistory" cols="100%" rows="2" ></textarea>
<br><BR>
<table>
  <tr>
    <td class="titleImg" >不良生活习惯</td>
  </tr>
</table>
<textarea class="common" name="BadHabit" cols="100%" rows="2" ></textarea>
<br><BR>
<input value="保存客户档案"  onclick="return saveCustDoc();" class="cssButton" type="button" >
<br>
<input type="hidden" name="fmtransact">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
