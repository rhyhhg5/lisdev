<%
  //程序名称：HMDisCaseManageInit.jsp
  //程序功能：疾病案例管理
  //创建日期：2010-3-4
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

function initForm()
{
	try{
		initCustomerInfoGrid();
		showOneCodeNametoAfter("comcode","ManageCom2","ManageComName2");
	}
	catch(re){
		alert("HMDisCaseManageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initCustomerInfoGrid()
{
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="客户号";
		iArray[1][1]="45px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="客户姓名";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="证件类型编码";
		iArray[3][1]="90px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="证件类型";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="证件号";
		iArray[5][1]="130px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="一年内累计住院天数";
		iArray[6][1]="150px";
		iArray[6][2]=100;
		iArray[6][3]=0;
		
		iArray[7]=new Array();
		iArray[7][0]="一年内累计医疗费用发生额";
		iArray[7][1]="150px";
		iArray[7][2]=100;
		iArray[7][3]=0;
		
		iArray[8]=new Array();
		iArray[8][0]="一年内累计医疗费用理赔额";
		iArray[8][1]="150px";
		iArray[8][2]=100;
		iArray[8][3]=0;
		
		iArray[9]=new Array();
		iArray[9][0]="疾病身故险有效保额";
		iArray[9][1]="150px";
		iArray[9][2]=100;
		iArray[9][3]=0;
		
		iArray[10]=new Array();
		iArray[10][0]="疾病定额给付险有效保额";
		iArray[10][1]="150px";
		iArray[10][2]=100;
		iArray[10][3]=0;

		CustomerInfoGrid = new MulLineEnter( "fm" , "CustomerInfoGrid" ); 

		CustomerInfoGrid.mulLineCount=1;
		CustomerInfoGrid.displayTitle=1;
		CustomerInfoGrid.canSel=1;
		CustomerInfoGrid.canChk=0;
		CustomerInfoGrid.hiddenPlus=1;
		CustomerInfoGrid.hiddenSubtraction=1;

		CustomerInfoGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
