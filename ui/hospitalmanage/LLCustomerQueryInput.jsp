<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LHCustomerQueryInput.jsp
//程序功能：功能描述
//创建日期：2010-03-01 15:18
//创建人  ：hm
//更新人  ：  
//更新日期：2010-03-01 15:18
%>

<%@page contentType="text/html; charset=GBK"%>
<html> 
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <SCRIPT src="LLCustomerQueryInput.js"></SCRIPT> 
  <%@include file="LLCustomerQueryInit.jsp"%>
</head>
<%
  GlobalInput tG = new GlobalInput(); 
  tG = (GlobalInput)session.getValue("GI");
%>
<script>
  var manageCom = "<%=tG.ManageCom%>";
</script>
<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>   
<SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
<body onload="initForm();">
  <form method=post name=fm target=fraSubmit>	
<Div  id= "CustomHealthQuery" style= "display: ''">    
	<table  class= common  >
		<TR  class= common>
			    <TD  class= title> 管理机构</TD><TD  class= input>
						<Input class= "codeno"  name=MngCom  ondblclick="return showCodeList('comcode',[this,MngComName],[0,1]);" onkeyup="return showCodeListKey('comcode',[this,MngComName],[0,1]);" readonly verify="管理机构|code:comcode&NOTNULL" ><Input class=codename  name=MngComName>
				</TD>
				<TD  class= title>业务员代码</TD>
    	   		<TD  class= input>
    	     	<Input class= 'code' name=AgentCode elementtype=nacessary verify="业务员代码|notnull&len<=24" ondblclick="return queryAgent();" onkeyup="return queryAgent2();">
    	  		</TD>
    	    	<TD  class= title>业务员姓名</TD>
    	   	 	<TD  class= input><Input class= 'common' name=AgentName  ></TD>
    	 </TR> 
    	 <TR  class= common>
     			<TD  class= title>理赔案件号</TD>
    	    	<TD  class= input><Input class= 'common' name=Caseno  ></TD>
				<TD  class= title>客户号码</TD>
    	    	<TD  class= input>
    	      	<Input class= 'code' name=CustomerNo elementtype=nacessary verify="客户号码|notnull&len<=24" ondblclick="return queryCustomerNo('Customer');" onkeyup="return queryCustomerNo2();">
    	    	</TD>
    	    	<TD  class= title>客户姓名</TD>
    	    	<TD  class= input><Input class= 'common' name=CustomerName ></TD>
      	</TR>
      	<TR  class= common>
    	    <TD  class= title>客户身份证号</TD>
    	    <TD  class= input><Input class= 'common' name=IDNo ></TD>  	
    	    <TD  class= title></TD>
    	    <TD  class= input></TD>
    	    <TD  class= title></TD>
    	    <TD  class= input></TD>    
	  </TR> 
	</table>
  
  <BR >       
  <table class= common border=0 width=100%>
   	<TR class= common>
  		<TD class=title>  				
  	 		<Input value="保单信息" type=button class=cssbutton style="width:158px;"  onclick="ContQuery();">&nbsp;&nbsp;
  	 		<Input value="个人就诊及理赔信息" type=button class=cssbutton   style="width:158px;" onclick="InHospitalQuery();">&nbsp;&nbsp;
  	 		
  	    </TD>
  	 </tr>
  	 <!-- Input value="个人服务计划" type=button class=cssbutton style="width:158px;" onclick="ServeQuery();">&nbsp;&nbsp;
  	 		<Input value="服务执行情况" type=button class=cssbutton style="width:158px;"  onclick="ServerStatusQuery();"> 
  	 	
  	 <tr>
  	 	<TD class=title>  					

  	 		<Input value="个人体检信息" type=button class=cssbutton   style="width:158px;" onclick="HealthCheckQuery();">&nbsp;&nbsp;
  	 		<Input value="健康档案打印" type=button class=cssbutton   style="width:158px;" onclick="FileInfoPrint();">
  	 		<Input value="当前健康状况" type=hidden class=cssbutton   style="width:158px;"  onclick="HealthStatusQuery();">&nbsp;&nbsp;
  	 	</TD>
  	 </TR>
  	 <TR class= common>
  		<TD class=title>  				
  	 		<Input value="健康通讯信息" type=button class=cssbutton style="width:158px;"  onclick="LHMessSendQuery();">&nbsp;&nbsp; 				
  	 		<Input value="个人体检报告" type=hidden class=cssbutton style="width:158px;"  onclick="LHHealthCheck();">&nbsp;&nbsp;
  	 	</TD>
  	 </tr> -->
  </table>
  <BR > 
  <Div  id= "ContStyle" style= "display: 'none'">  
	<hr>
	<table> 
		<tr>
			<td>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,ContStyle);">
    		</td>
    		<td class= titleImg>
        		 保单信息
			</td>   		 
    	</tr>
	</table>
	<table  class= common align='left' >
		<table class= common> 
			<TR  class= common>
			  <TD  class= title>
			  	投保人号码
			  </TD>
    	   <TD  class= input>
    	     <Input class= 'code' name=ACustomerNo elementtype=nacessary ondblclick="return getCustomerNo('ACustomer');">
    	   </TD>
    	   <TD  class= title>
			    投保人姓名
    	   </TD>
    	   <TD  class= input>
    	     <Input class= 'common' name=ACustomerName  >
    	   </TD>
    	   <TD  class= title>
    	   </TD>
    	   <TD  class= input>
    	  </TD>
		</TR> 
    	<tr class= common>
    		<TD class= title>
        		 保单类型
       		</TD>   		 
			<TD  class= input>
				<Input class= 'codename' style="width:40px" name=ContType value='1' readonly><Input class= 'codeno'  style="width:120px" name=ContTypeName value='个人保单' verify="个人服务计划名称|NOTNULL&len<=200" CodeData= "0|^1|个人保单^2|健管服务保单^3|团体保单"   ondblclick="showCodeListEx('',[this,ContType],[1,0],null,null,null,'1',160);" codeClear(ContTypeName,ContType);">&nbsp;&nbsp;<Input value="查询"   style="width:75px" type=button class=cssbutton onclick="ContCheckQuery();">&nbsp;&nbsp;&nbsp;&nbsp;
			</TD>
       <td style="width:400px;">
			</td>
			<TD  class= input>              
			</TD> 
			<TD  class= title>
			</TD>
			<TD  class= input align='right'>    
        		<Input value="保单详细信息" type=button class=cssbutton onclick="ContDetailInfo();">               
        	</TD>  
		</tr> 
		</table>
		<table>
        	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHHealthServPlanGrid">
	  				</span> 
			    </td>
			</tr> 
		</table> 
		<Div id= "divPage" align=center style= "display: '' ">
	        <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	        <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	        <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	        <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	    </Div>

		<table> 
	<tr class=common>
		<td text-align:left colSpan=1>
			<span id="spanLHContSpecialInfo">
			</span> 
		</td>
	</tr>
</table>
	<Div id= "divPage" align=center style= "display: '' ">
	     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
	     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
	     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
	     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</Div>
  </table>	
    <BR > 
</div>
 
    
 <Div  id= "ServeStyle" style= "display: 'none'">   
	<hr> 
	<table> 
		<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,ServeStyle);">
    		</td>
    		<td class= titleImg>
        		 个人服务计划信息
       	</td>   		 
    	</tr>
    </table>
	<table  class= common align='left' >
		<table  class= common >
			<TR  class= common>  
				<TD  class= title>
					服务计划类型
				</TD>
				<TD  class= input>
				        <Input class= 'codename' style="width:40px" name=Riskcode VALUE="6201"><Input class= 'code' style="width:165px" name=Riskname value="个人健康维护计划" verify="个人服务计划名称|NOTNULL&len<=200" ondblclick="showCodeList('hmriskcode',[this,Riskcode],[1,0],null,fm.Riskname.value,'Riskcode','1',300);" codeClear(Riskname,Riskcode);">&nbsp;<Input value="查询" style="width:75px" type=button class=cssbutton onclick="ServePlanQuery();">  
				</TD>
				<td style="width:500px;">
			  </td>
        <TD  class= input>       
        	<Input value="计划详细信息" type=button class=cssbutton onclick="ServPlanDetail();">        
        </TD>
        	</TR>  
     	</table>	
		<table class=common>
			<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHServerPlanType">
	  				</span> 
			    </td>
			</tr>
		</table>
		<Div id= "divPage" align=center style= "display: '' ">
	      <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage3.firstPage();"> 
	      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage3.previousPage();"> 					
	      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage3.nextPage();"> 
	      <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage3.lastPage();">
		</Div>  
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
	  				<span id="spanLHPlanSpecialInfo">
	  				</span> 
			</td>
		</tr>
	</table>
	<Div id= "divPage" align=center style= "display: '' ">
	     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage4.firstPage();"> 
	     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage4.previousPage();"> 					
	     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage4.nextPage();"> 
	     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage4.lastPage();">
	 </Div>
	 
 </table>

	<table class= common border=0 width=100%>
   	<TR class= common>
  		<TD class=title>  					
  	 		<Input type=hidden value="服务执行详情" type=button class=cssbutton onclick="ServRunDetail();">
  	 	</TD>
  	 	 <input type = hidden   name = ContTypeIn>
  	 </TR>
  </table>
    <BR >
</Div>


<Div  id= "ServerStatus" style= "display: 'none'">   
	<table> 
		<tr>
			<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,ServerStatus);">
    		</td>
    		<td class= titleImg>
        		 服务执行情况信息
       		</td>   		 
		</tr>
    </table>    
	<table  class= common align='center' >
		<TR  class= common>
			<TD  class= title>
				保单号
			</TD>
			<TD  class= input>
				<input class="common" name=ContNo>
			</TD>
			<TD  class= title>
				服务项目代码
			</TD>
			<TD  class= input>
				<input class="code" name=ServItemCode ondblclick="showCodeList('hmservitemcode',[this,ServItemName],[0,1],null,null,'ServItemCode',1,200);">
			</TD>
			<TD  class= title>
				服务项目名称
			</TD>
			<TD  class= input>
				<input class="code" name=ServItemName ondblclick="showCodeList('hmservitemcode',[this,ServItemCode],[1,0],null,null,'ServItemName',1,200);">
			</TD>
		</TR>
		<TR  class= common align='left'> 
			<TD  class= title>
				服务状态
			</TD>
			<TD  class= input>
				<Input class= 'codename' style="width:50px" name=ServePlanStatusType value="1" readonly><Input class= 'codeno'  style="width:110px" name=ContTypeName value="已结束" ondblclick="return showCodeList('caseassociate',[this,ServePlanStatusType],[1,0],null,null,null,1,160);" >
			</TD>
			<TD  class= title  align='left'>
				<Input value="查询" style="width:50px" type=button class=cssbutton onclick="StatusQuery();">
			</TD>
			<TD  class= input>
			</TD> 
			<TD  class= title>
				<Input value="评估问卷查看" type=button style="width:100px" class=cssbutton onclick="ShowQuesInfo();">
			</TD>
			<TD  class= input align='left'>    
				<Input value="评估报告查看" type=button style="width:100px" class=cssbutton onclick="ShowReportInfo();">
			</TD>
		</TR>  
	</table>
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
	  				<span id="spanLHServerDoStatus">
	  				</span> 
			</td>
		</tr>
	</table>
	<Div id= "divPage" align=center style= "display: '' ">
	     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage5.firstPage();"> 
	     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage5.previousPage();"> 					
	     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage5.nextPage();"> 
	     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage5.lastPage();">
	</Div>
	
	<table class=common>
		<tr class=common>
			<td text-align:left colSpan=1>
	  				<span id="spanServItemCaseGrid">
	  				</span> 
			</td>
		</tr>
	</table>
	<Div id= "divPage" align=center style= "display: '' ">
	     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage8.firstPage();"> 
	     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage8.previousPage();"> 					
	     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage8.nextPage();"> 
	     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage8.lastPage();">
	</Div>
	<table class= common border=0 width=100%>
	   	<TR class= common>
	  		<TD class=title>  					
	  	 		<Input value="服务执行详情" type=button class=cssbutton onclick="ServRunDetail2();">
	  	 	</TD>
		</TR>
	</table>
	<BR >
</Div>
  

 <Div  id= "InHospitalStyle" style= "display: 'none'">    
 	  	<hr>     
 	  <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,InHospitalStyle);">
    		</td>
    		<td class= titleImg>
        		 个人就诊及理赔信息
       	</td>   		 
    	</tr>
    </table> 
  <table  class= common align='left' >
  <table  class= common>
  			<TR  class= common>  
  			<TD  class= title>
				就诊方式
				</TD>
				<TD  class= input>
				 <Input class= 'codename' style="width:60px" name=InHospitalType><Input class= 'code' style="width:105px" name=InHospitMode ondblclick=" return showCodeList('hminhospitalmode',[this,InHospitalType],[1,0]);" onkeydown=" return showCodeListKey('hminhospitalmode',[this,InHospitModeCode],[1,0]);codeClear(InHospitMode,InHospitModeCode);"  verify="就诊方式代码|len<=10">&nbsp;&nbsp;<Input value="查询" style="width:85px" type=button class=cssbutton onclick="HospitalQuery();">  
				</TD>
				<td style="width:400px;">
		    </td>
  		 <TD class=title>  					
  	 		<Input value="就诊详细信息" type=button class=cssbutton onclick="DealCheck();">  	 	
  	</tr>
   </table>	
   <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHInHospitalMode">
	  				</span> 
			    </td>
				</tr>
	</table>
	<Div id= "divPage" align=center style= "display: '' ">
	     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage6.firstPage();"> 
	     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage6.previousPage();"> 					
	     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage6.nextPage();"> 
	     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage6.lastPage();">
	</Div>
	</table>
</Div> 
<Div  id= "HealthCheck" style= "display: 'none'">     
	  	<hr>  
	  <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,HealthCheck);">
    		</td>
    		<td class= titleImg>
        		 个人体检信息
       	</td>   		 
    	</tr>
    </table>  
  <table  class= common align='left' >
  <table  class= common>
  			<TR  class= common>  
  			 <TD  class= title>
				   体检类别
				 </TD>
				 <TD  class= input>
				 <Input class=codeno name=TestModeCode style="width:60px" readonly style="background-color:#F7F7F7;" verify="体检方式|notnull" ><Input class=codeName style="width:108px; background-color:#D7E1F6;" name=TestMode ondblclick="return showCodeList('lhtestmode',[this,TestModeCode],[0,1]);" onkeydown="return showCodeListKey('lhtestmode',[this,TestModeCode],[0,1]);" verify="体检方式代码|len<=10">&nbsp;&nbsp;<Input value="查询" style="width:81px" type=button class=cssbutton onclick="healthQuery();">
				 </TD>
        <td style="width:630px;">
		  	</td> 
		  	<TD class=title>  					
  	 		<Input value="体检详细信息" style="width:90px;" type=button class=cssbutton ;" onclick="HealthCheckDetail();">
  	 	</TD> 
  	 		<TD class=title>  					
  	 		<Input value="查看扫描件" style="width:90px;" type=button class=cssbutton onclick="LookScanTestInfo();">
  	 	</TD>   
  	 	<TD  class= title>
         <INPUT VALUE="个人体检报告" style="width:90px;" class="cssButton" TYPE="button" onclick="ContHealthCheck()">	
      </TD>                        
      </TR>  
  </table>	
  <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHHealthCheckupStyle">
	  				</span> 
			    </td>
				</tr>
	</table>
	<Div id= "divPage" align=center style= "display: '' ">
	     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage7.firstPage();"> 
	     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage7.previousPage();"> 					
	     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage7.nextPage();"> 
	     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage7.lastPage();">
	</Div>
	<input type=hidden  id=ManageCom name="CustomerNoH">
	<input type=hidden  id=ManageCom name="InHospitNoH">
  </table>	
</Div>
<Div  id= "CustomHealthStatus" style= "display: 'none'">  
	<hr>  
	  <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,CustomHealthStatus);">
    		</td>
    		<td class= titleImg>
        		 当前健康状况
       	</td>   		 
    	</tr>
    </table>   
  <table  class= common align='left' >
  	 	<TR  class= common>
  	 			<TD  class= title>
                    血压舒张压（mmHg）
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' readonly name=BloodPressLow verify="血压舒张|INT&len<=5"> 
                  </TD>
                  <TD  class= title>
                    血压收缩压（mmHg）
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' readonly name=BloodPressHigh verify="血压收缩压|INT&len<=5"> 
                  </TD>
                  <TD  class= title>
                    脉压差（mmHg)
                  </TD>
                  <TD  class= input>
                    <Input class= readonly readonly name=BloodPress verify="脉压差（mmHg)|num&len<=10" > 
                  </TD>
                </TR>
                <TR  class= common>
                  <TD  class= title>
                    身高（m）
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' readonly name=Stature verify="身高|num&len<=5" >
                  </TD>
                  <TD  class= title>
                    体重（kg）
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' readonly name=Avoirdupois verify="体重|num&len<=6">
                  </TD>
                  <TD  class= title>
                    体重指数（kg/m2）
                  </TD>
                  <TD  class= input>
                    <Input class=readonly readonly name=AvoirdIndex MAXLENGTH="5" verify="体重指数|num&len<=10">
                  </TD>
                </TR>
                 <TR  class= common>
                  <TD  class= title>
                    腰围
                  </TD>
                  <TD  class= input>
                    <Input class= 'common' readonly name=Waistline verify="身高|num&len<=5" >
                  </TD>
                 </TR>
                
  </table>
<!--
<br><br><br><br><br>
 
      	   <table  class= common align='center' >
      	      <tr class=common>
      	      	<TD  >
                   <span id="spanLHCustomGymGrid" ></span>
                 </TD>
                 
      	      </tr>
      	   </table>
          
          <table  class= common align='center' >
               <TR  class= common>
               	<TD  class= title>
                   吸烟（支/天）
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=Smoke verify="吸烟|INT&len<=5">
                 </TD>
                 <TD  class= title>
                   饮酒（两/周）
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=KissCup verify="饮酒|num&len<=5">
                 </TD>
                 <TD  class= title>
                   熬夜（次/月）
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=SitUp verify="熬夜|INT&len<=5">
                 </TD>
                 
               </TR>
               <TR  class= common>
                 <TD  class= title>
                   饮食不规律（餐/周）
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=DiningNoRule verify="饮食不规律|INT&len<=5">
                 </TD>
                 <TD  class= title>
                   其它不良嗜好
                 </TD>
                 <TD  class= input>
                   <Input class= 'common' name=BadHobby verify="不良嗜好|len<=200">
                 </TD>
                 <TD  class=title>
                 </TD>
                 <TD   class=input>
                 </TD>
               </tr>
           </table>
           
         	<table  class= common align='center'>
      	      <tr class=common>
      	      	<TD  >
                 <span id="spanLHCustomFamilyDiseasGrid"></span>
                 </TD>
                 
      	      </tr>
      	   </table>
       </Div>

<hr>
<Div  id= "DateCheck" style= "display: ''">      
  <table class= common border=0 width=100%>
     <TR  class= common>          
          <TD  class= title>
          	起始时间
          </TD>
          <TD  class= input>
          	<Input name=StartDate class='coolDatePicker'  style="width:132px" dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> 
          </TD>
          <TD  class= title>
          	终止时间
          </TD>
          <TD  class= input> 
          	<Input name=EndDate class='coolDatePicker'  style="width:132px" dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> 
          </TD> 
        	<TD class=title>
           <Input value="客户健康档案打印"  style="width:130px" type=button class=cssbutton onclick="submitFormA();">
          </TD>
     </TR>
  </table>
  -->
</div>
<Div  id= "HealthFilePrint" style= "display: 'none'">  
	<hr>  
	  <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,HealthFilePrintInfo);">
    		</td>
    		<td class= titleImg>
        		 健康档案打印
       	</td>   		 
    	</tr>
    </table> 
    <Div  id= "HealthFilePrintInfo" style= "display: ''">  
    <table class= common border=0 width=100%>
      	<TR  class= common>          
          <TD  class= title>统计起期</TD>
          <TD  class= input> <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="统计起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>统计止期</TD>
          <TD  class= input> <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="统计止期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title></TD>
          <TD  class= input></td>
        </TR>
    </table>
    <br>
		<INPUT VALUE="就诊档案打印" style="width:110px;" class="cssButton" TYPE="button" onclick="submitFormRpt()">
		<INPUT VALUE="病例档案打印" class="cssButton" style="width:110px;" TYPE="button" onclick="TestPrint()"> 
		<INPUT VALUE="服务档案打印" class="cssButton" style="width:110px;" TYPE="button" onclick="ServPtint()"> 	
	</div>
</div> 
 <Div  id= "divLHMessSendInfoGrid" style= "display: 'none'">
 	<hr>  
	  <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;">
    		</td>
    		<td class= titleImg>
        		 健康通讯短信信息
       	</td>   		 
    	</tr>
    </table> 
    <Div  id= "LHMessSendInfo" style= "display: ''">  
		 <table  class= common align='center' >
		  <TR  class= common>   
			   <TD  class= title>
            健康通讯代码
         </TD>
         <TD  class= input>
           <Input class= 'code' name=HmMessCode  ondblclick="showCodeList('lhmessagecode',[this,HmMessName],[0,1],null,fm.HmMessCode.value,'HmMessCode','1',300);"  codeClear(HmMessName,HmMessCode);">
         </TD>
         <TD  class= title>
            健康通讯名称
         </TD>
             <TD  class= input>
             <Input class= 'code' name=HmMessName   ondblclick="showCodeList('lhmessagecode',[this,HmMessCode],[1,0],null,fm.HmMessName.value,'HmMessName','1',300);"  codeClear(HmMessName,HmMessCode);">
         </TD>
				 <TD  class= title>
       	 </TD>
        <TD  class= input>
        </TD>
		  </TR>
		  <TR class=common>
	        <TD class=title>
            	起始事件实施时间
          </TD>
          <TD class=input>
          	<Input class= 'CoolDatePicker' dateFormat="Short" style="width:142px;"  name=LHMStartDate >
          </TD>
           <TD class=title>
          	  终止事件实施时间
          </TD>
          <TD class=input>
          	<Input class= 'CoolDatePicker' dateFormat="Short" style="width:142px;"  name=LHMEndDate >
          </TD>
          <TD  class= title>
          	<Input value="查 询"  type=button class=cssbutton style="width:60px;" onclick="LHMessSendInfo();">
       	 </TD>
         <TD  class= input>				
  	 	   </TD>  
       <TR>
	</table>
	 <table class=common>
	    	<tr class=common>
		  		<td text-align:left colSpan=1>
	  				<span id="spanLHMessSendMgGrid">
	  				</span> 
			    </td>
				</tr>
	</table>
	<Div id= "divPage" align=center style= "display: '' ">
	     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage9.firstPage();"> 
	     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage9.previousPage();"> 					
	     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage9.nextPage();"> 
	     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage9.lastPage();">
	</Div>
  </div>
</div> 
  <Div  id= "HealthCheckPrint" style= "display: 'none'">  
	<hr>  
	  <table> 
    	<tr>
    		<td>
    		     <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,HealthCheckPrintInfo);">
    		</td>
    		<td class= titleImg>
        		 个人体检报告打印
       	</td>   		 
    	</tr>
    </table> 
    <Div  id= "HealthCheckPrintInfo" style= "display: ''">  
    <table class= common border=0 width=100%>
      	<TR  class= common>          
          <TD  class= title>体检起期</TD>
          <TD  class= input> <Input name=cStartDate class='coolDatePicker' dateFormat='short' verify="体检起期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>体检止期</TD>
          <TD  class= input> <Input name=cEndDate class='coolDatePicker' dateFormat='short' verify="体检止期|notnull&Date" elementtype=nacessary> </TD> 
          <TD  class= title>
          	<INPUT VALUE="打 印" style="width:80px;" class="cssButton" TYPE="button" onclick="ContHealthCheck()">	
          </TD>
          <TD  class= input>
          </td>
        </TR>
    </table>
    <br>
	</div>
</div> 
<input type = hidden name = "fmtransact">
<input type = hidden id=ManageCom name="ComID">
<!-- 业务员查询临时参数变量 -->
<input type = hidden id=SaleChnl name="SaleChnl">
<input type = hidden id=AgentCom name="AgentCom">
<input type = hidden id=branchtype name="branchtype">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
 
