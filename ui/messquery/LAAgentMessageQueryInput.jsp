<%
//程序名称：LAAgentMessageQueryInput.jsp
//程序功能：action="./LAAgentMessageQuerySubmit.jsp"
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAAgentMessageQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAgentMessageQueryInit.jsp"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <title>代理人信息查询</title>
</head>
<body  onload="initForm();" >
  <form  method=post name=fm target="fraSubmit">
  
  <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgentMessage1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAAgentMessage1" style= "display: ''">
    <table  class= common>
        <TR  class= common>
          <TD  class= title>
            代理人编码
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=AgentCode >
          </TD>
          <TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Name >
          </TD>
        </TR>
    </table>
    </div>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="重置" TYPE=button onclick="resetForm();"> 					
    <Div  id= "divLAAgentMessage2" style= "display: ''">
    <table>
        <TR  class= common>
          <TD  class= title>
            代理人组别
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=AgentGroup >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=ManageCom >
          </TD>
        </TR>  
        
        <TR  class= common>
          <!--TD  class= title>
            姓名
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Name >
          </TD-->
          <TD  class= title>
            性别
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Sex >
          </TD>
        </TR>  
        
        <TR  class= common>
          <TD  class= title>
            出生日期
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Birthday >
          </TD>
          <TD  class= title>
            籍贯
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=NativePlace >
          </TD>
        </TR>  
         
        <TR  class= common>
          <TD  class= title>
            民族
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Nationality >
          </TD>
          <TD  class= title>
            婚姻状况
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Marriage >
          </TD>
        </TR>  
         
        <TR  class= common>
          <TD  class= title>
            信用等级
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=CreditGrade >
          </TD>
          <TD  class= title>
            家庭地址
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=HomeAddress >
          </TD>
        </TR>  
         
        <TR  class= common>
          <TD  class= title>
            通讯地址
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=PostalAddress >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=ZipCode >
          </TD>
        </TR>  
         
        <TR  class= common>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Phone >
          </TD>
          <TD  class= title>
            传呼
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=BP >
          </TD>
        </TR>  
         
        <TR  class= common>
          <TD  class= title>
            手机
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Mobile >
          </TD>
          <TD  class= title>
            e_mail
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Email >
          </TD>
        </TR>  
        
        <TR  class= common>
          <TD  class= title>
            身份证号码
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=IDNo >
          </TD>
          <TD  class= title>
            来源地
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Source >
          </TD>
        </TR>  
        
        <TR  class= common>
          <TD  class= title>
            血型
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=BloodType >
          </TD>
          <TD  class= title>
            政治面貌
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=PolityVisage >
          </TD>
        </TR>  
        
        <TR  class= common>
          <TD  class= title>
            学历
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Degree >
          </TD>
          <TD  class= title>
            毕业院校
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=GraduateSchool >
          </TD>
        </TR>
        
        <TR  class= common>
          <TD  class= title>
            专业
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Speciality >
          </TD>
          <TD  class= title>
            职称
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=PostTitle > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            外语水平
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=ForeignLevel >
          </TD>
          <TD  class= title>
            从业年限
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=WorkAge > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            原工作单位
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=OldCom >
          </TD>
          <TD  class= title>
            原职业
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=OldOccupation > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            工作职务
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=HeadShip >
          </TD>
          <TD  class= title>
            推荐代理人
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=RecommendAgent > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            工种/行业
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=Business >
          </TD>
          <TD  class= title>
            销售资格
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=SaleQuaf > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            代理人资格证号码
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=QuafNo >
          </TD>
          <TD  class= title>
            证书开始日期
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=QuafStartDate > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            证书结束日期
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=QuafEndDate >
          </TD>
          <TD  class= title>
            展业证号码1
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=DevNo1 > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            展业证号码2
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=DevNo2  >
          </TD>
          <TD  class= title>
            聘用合同号码
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=RetainContNo > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            代理人类别
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=AgentKind  >
          </TD>
          <TD  class= title>
            业务拓展级别
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=DevGrade > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            内勤标志
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=InsideFlag  >
          </TD>
          <TD  class= title>
            是否专职标志
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=FullTimeFlag > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            是否有待业证标志
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=NoWorkFlag  >
          </TD>
          <TD  class= title>
            培训日期
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=TrainDate > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            录用日期
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=EmployDate  >
          </TD>
          <TD  class= title>
            转正日期
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=InDueFormDate > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            代理人状态
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=AgentState  >
          </TD>
          <TD  class= title>
            所在营业组代码
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=BranchCode > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            是否吸烟标志
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=SmokeFlag  >
          </TD>
          <TD  class= title>
            户口所在地
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=RgtAddress > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            银行编码
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=BankCode  >
          </TD>
          <TD  class= title>
            银行帐户
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=BankAccNo > 
          </TD>
        </TR> 
        
        <TR  class= common>
          <TD  class= title>
            展业类型
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=BranchType  >
          </TD>
          <TD  class= title>
            培训期数
          </TD>
          <TD  class= input>
            <Input class='readonly' readonly name=TrainPeriods > 
          </TD>
        </TR>
      </table>
     </div>
  </form>	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>
