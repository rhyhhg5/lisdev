var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass(); 
var showInfo;

//提交，保存按钮对应操作
function submitForm()
{
	var Count=CustomerInfo.mulLineCount;
  if(Count==0){
  	alert("没有客户信息!");
  	return false;
  }
  
  var chkFlag=false;
  for (i=0;i<Count;i++){
    if(CustomerInfo.getChkNo(i)==true){
    	if(CustomerInfo.getRowColData(i,13)==""||CustomerInfo.getRowColData(i,13)==null){
		    alert("第"+(i+1)+"行客户风险等级为空!");	
				return false;
			}
			if(CustomerInfo.getRowColData(i,15)==""||CustomerInfo.getRowColData(i,15)==null){
		    alert("第"+(i+1)+"行分类依据为空!");	
				return false;
			}
			if(CustomerInfo.getRowColData(i,16)==""||CustomerInfo.getRowColData(i,16)==null){
		    alert("第"+(i+1)+"行分类依据为空!");	
				return false;
			}
      chkFlag=true;
    }
  }
  
  if (chkFlag==false){
    alert("请选择要分类的客户!");
    return false;
  }
  
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.target = "fraSubmit";
	fm.action="./LCCustomerRCConfirmSave.jsp"
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LCCustomerRCConfirm.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

// 事件响应函数，当用户改变CodeSelect的值时触发
function afterCodeSelect(cCodeName, Field)
{
	try {
		if( cCodeName == "ClassState"){
			if (fm.ClassState.value=="0"){
				divConfirm.style.display='none';
			} else {
				divConfirm.style.display='';
			}
		}
		if (cCodeName == "CustomerType") {
			initCustomerInfo();
			if (fm.CustomerType.value=="0") {
				divIDNo.style.display="none";
				divIDNot.style.display="none";
			} else {
				divIDNo.style.display="";
				divIDNot.style.display="";
			}
		}
	} catch(ex) {
		alert("在afterCodeSelect中发生异常");
	}
}


function searchCustomer()
{
	divRiskClassTrace.style.display="none";
	
	if (fm.SignDateS.value==""||fm.SignDateS.value==null
	    ||fm.SignDateE.value==""||fm.SignDateE.value==null) {
	    	alert("请输入承保日期");
	    	return false;
	}
	
	if (fm.ClassState.value!="0"&&(fm.ConfirmDateS.value==""||fm.ConfirmDateS.value==null
	    ||fm.ConfirmDateE.value==""||fm.ConfirmDateE.value==null)) {
	    	alert("请输入确认日期");
	    	return false;
	}
	
	strSQLHead = " SELECT a.customerno,a.customername,"
	   	       + " codename('nativeplace',a.nativeplace),"
	   	       + " codename('sex',a.customersex),a.customerbirthday,a.idno,"
	   	       + " a.occupationtype,a.occupationcode,"
	   	       + " (select occupationname from ldoccupation where occupationcode=a.occupationcode),"
	   	       + " a.signdate,a.prem,codename('paymode',a.paymode),"
	   	       + " a.customergrade,codename('riskclass',a.customergrade),"
	   	       + " a.classreason,codename((CodeAlias('riskclass',a.customergrade)),a.classreason),"
	   	       + " a.confirmdate,a.remark ";
	strSQLSum = " select count(a.customerno) ";
	strSQL = " from lccustomerriskclass a where a.managecom like '"+fm.ManageCom.value+"%' "
		     + getWherePart('a.customertype','CustomerType')
		     + getWherePart('a.classstate','ClassState')
		     + getWherePart('a.signdate','SignDateS','>=')
		     + getWherePart('a.signdate','SignDateE','<=')
		     + getWherePart('a.customerno','CustomerNo')
		     + getWherePart('a.customername','CustomerName')
		     + getWherePart('a.idno','IDNo');
  
  if (fm.ClassState.value!="0") {
  	strSQL += getWherePart('a.confirmdate','ConfirmDateS','>=')
		       + getWherePart('a.confirmdate','ConfirmDateE','<=')
		       + getWherePart('a.customergrade','CustomerGrade');
	}
	
	if (fm.SaleChnl.value=="0") {
		strSQL += " and a.salechnl in ('01','06','10') ";
	} else if (fm.SaleChnl.value=="1") {
		strSQL += " and a.salechnl in ('02','03','07','08','11','12') ";
	} else if (fm.SaleChnl.value=="2") {
		strSQL += " and a.salechnl in ('04','13') ";
	}
	strSQLHead += strSQL;
	strSQLHead += " order by a.signdate desc with ur";
	strSQLSum += strSQL;
	strSQLSum += " with ur";
	
	arr = easyExecSql(strSQLSum);
	if (arr) {
		fm.SumCount.value = arr[0][0];
	}
	turnPage.queryModal(strSQLHead,CustomerInfo);

	fm.SearchSQL.value= strSQL;
}

function printList()
{
	if (fm.SearchSQL.value=="") {
		alert("请先进行查询");
		return false;
	}
	
	 if(dateDiff(fm.SignDateS.value,fm.SignDateE.value,"D")>90) {
	    alert("如需下载清单，请将统计时间限制在三个月内。");
	    return false;
	}

	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.action = "./LCCustomerRCPrintSave.jsp";
	fm.submit();
}

function SelectCustomerInfo()
{
	var vRow = CustomerInfo.getSelNo();

	if( vRow == null || vRow == 0 ) {
		return;
	}
	
	divRiskClassTrace.style.display="";
	initCustomerTrace();
	var CustomerNo = CustomerInfo.getRowColData(vRow-1,1);
	if (CustomerNo!=null&&CustomerNo!=""){
		strSQL = " SELECT a.customerno,a.customername,"
		       + " a.signdate,"
		       + " codename('riskclass',a.customergrade),"
		       + " codename((CodeAlias('riskclass',a.customergrade)),a.classreason),"
		       + " case when a.classstate = '1' then '初步分类' else '最终分类' end,"
		       + " a.confirmoperator,a.confirmdate,a.remark "
		       + " from lccustomerriskclasstrace a where "
		       + " a.customerno = '"+CustomerNo+"' and classstate in ('1','2') order by a.confirmdate desc with ur"
		turnPage1.queryModal(strSQL,CustomerTrace);
	}
}

function closePage()
{
  top.close();
}

function afterPrint(outname,outpathname) {
	showInfo.close();
	fileUrl.href = "../f1print/download.jsp?filename="+outname+"&filenamepath="+outpathname;
  fileUrl.click();
}

function CustomerQuery() {
	var vRow = CustomerInfo.getSelNo();

	if( vRow == null || vRow == 0 ) {
		alert("请选择一条客户信息");
		return;
	}
	
	var varSrc ="";
  pathStr="../sys/GrpPolQueryInput.jsp?CustomerNo=" + CustomerInfo.getRowColData(vRow-1,1);
  showInfo = OpenWindowNew(pathStr,"","left");
	
}