<%
//程序名称：LCCustomerRCConfirmSave.jsp
//程序功能：
//创建日期：2009-08-04
//创建人  ：MN
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.riskclass.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  
  LCCustomerRiskClassSet tLCCustomerRiskClassSet = new LCCustomerRiskClassSet();
	
  LCCustomerRiskClassUI tLCCustomerRiskClassUI = new LCCustomerRiskClassUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("Operate");
  tOperate=tOperate.trim();
  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String LoadFlag = request.getParameter("LoadFlag");
  
  String tChk[] = request.getParameterValues("InpCustomerInfoChk");
  String tCustomerNo[] = request.getParameterValues("CustomerInfo1");
  String tCustomerGrade[] = request.getParameterValues("CustomerInfo13");
  String tClassReason[] = request.getParameterValues("CustomerInfo15");
  String tClassReasonDesc[] = request.getParameterValues("CustomerInfo16");
  String tReMark[] = request.getParameterValues("CustomerInfo18");
  System.out.println(tChk.length);
  for (int index=0 ; index<tChk.length ; index++) {
    System.out.println(tChk[index]);
    if(tChk[index].equals("1")) {
        System.out.println(tCustomerNo[index]);
        LCCustomerRiskClassSchema tLCCustomerRiskClassSchema = new LCCustomerRiskClassSchema();
        tLCCustomerRiskClassSchema.setCustomerNo(tCustomerNo[index]);
        tLCCustomerRiskClassSchema.setCustomerGrade(tCustomerGrade[index]);
        tLCCustomerRiskClassSchema.setClassReason(tClassReason[index]);
        tLCCustomerRiskClassSchema.setClassReasonDesc(tClassReasonDesc[index]);
        tLCCustomerRiskClassSchema.setRemark(tReMark[index]);
        tLCCustomerRiskClassSet.add(tLCCustomerRiskClassSchema);
    }
  }
 
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("LoadFlag",LoadFlag);
  
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLCCustomerRiskClassSet);
	tVData.addElement(tTransferData);
	tVData.addElement(tG);
  try
  {
    tLCCustomerRiskClassUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLCCustomerRiskClassUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	System.out.println(Content);
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	//Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
%>     
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

