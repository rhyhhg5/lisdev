<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LABroComInput.jsp
//程序功能：
//创建日期：2003-09-16
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
</script>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LABroComInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LABroComInit.jsp"%>
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./LASpecComSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABroCom1);"></td>
        <td class=titleImg>经纪公司机构信息</td> 
      </tr>
    </table>
  <Div  id= "divLABroCom1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 经纪公司机构编码 </td>
        <td  class= input> <input class=common name=AgentCom> </td>
        <td  class= title> 经纪公司机构名称 </td>
        <td  class= input> <input class=common  name=Name > </td>
      </tr>
      
      <tr  class= common> 
        <td  class= title> 工商执照编码 </td>
        <td  class= input> <input  class= common name=BusiLicenseCode> </td>
        <td  class= title> 组织机构代码 </td>
        <td  class= input> <input  class= common name=AppAgentCom> </td>
      </tr>
      
      <tr class = common >
         <td  class= title> 经纪公司单位负责人 </td>
         <td  class= input> <input class=common name=Corporation> </td>
         <td  class= title> 经纪公司业务负责人 </td>
         <td  class= input> <input class= common name=LinkMan> </td>
      </tr>
      
      <tr  class= common> 
        <td  class= title> 经纪公司机构地址 </td>
        <td  class= input> <input  class= common name=Address> </td>
	<td  class= title> 经纪公司机构电话 </td>
        <td  class= input> <input  class=common name=Phone> </td>
      </tr>
      
      <tr  class= common> 
        <td  class= title> 上级代理机构 </td>
        <td  class= input> <input class=common name=UpAgentCom onchange="getComName(UpComName)"> </td>
	<td  class= title> 上级机构名称 </td>
        <td  class= input> <input class='readonly' readonly name=UpComName > </td>   
      </tr>   
    </table>
  </Div>
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABroCom2);"></td>
        <td class=titleImg>保险公司对应信息</td> 
      </tr>
    </table>
  <Div  id= "divLABroCom2" style= "display: ''"> 
    <table  class= common>
    
      <tr  class= common>
        <td  class= title> 客户经理代码 </td>
        <td  class= input> <Input class="code" name=AgentCode ondblclick="return showCodeList('AgentCode',[this], [0,1],null,acodeSql,'1',null);" onkeyup="return showCodeListKey('AgentCode', [this], [0,1],null,acodeSql,'1',null);" onchange="getManagerName(AgentName)"> </td>
	<td  class= title> 客户经理 </td>
        <td  class= input> <input class='readonly' readonly name=AgentName ></td>
      </tr>     
      
      <tr  class= common>
	<td  class= title> 对应渠道组 </td>
        <td  class= input> <Input class="code" name=AgentGroup ondblclick="return showCodeList('BranchAttr',[this], [0,1],null,bcodeSql,'1',null);" onkeyup="return showCodeListKey('BranchAttr', [this], [0,1],null,bcodeSql,'1',null);"> </td> 
	<td  class= title> 负责管理机构 </td>
        <td  class= input> <input class='code' name=ManageCom 
        verify="负责机构|notnull&code:comcode" 
        ondblclick="return showCodeList('comcode',[this]);">  </td>
      </tr>
      
      <tr  class= common>
        <td  class= title> 代理机构类别 </td>
        <td  class= input> <input class= readonly readonly name=ACType > </td>
	<td  class= title> 销售资格 </td>
        <td  class= input> <input  name=SellFlag class= 'code'
		           ondblclick="return showCodeList('YesNo',[this]);" 
		           onkeyup="return showCodeListKey('YesNo',[this]);"> </td>
      </tr>
    </table>
  </Div>
  <input type=hidden name=ChannelType value='E'>
  <input type=hidden name=hideOperate value=''>
  <input type=hidden name=HiddenAgentGroup value=''>
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>


<script>
  var acodeSql = "1 and BranchType=#3# and ManageCom like #" + manageCom + "%# and (AgentState = #01# or AgentState = #02#) ";
  var bcodeSql = "1 and BranchType=#3# and EndFlag != #Y# and ManageCom like #" + manageCom + "%#"
</script>