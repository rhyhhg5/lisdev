<%
//程序名称：LABranchToComInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">

	function initForm()
	{
		try
		{
		 
			initInpBox();
			//initBranchGrid();
		}
		catch(re)
		{
			alert("LABranchToComInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	
	function initInpBox()
	{
		try
		{
		  // alert("123456".substr(0,4));
			// fm.all('AdjustBranchCode').value = '';
			// fm.all('AdjustAfterBranchCode').value = '';
			// fm.all('NewBranchAttr').value = '';
		  	fm.MakeTime.value="<%=CurrentDate%>";
   			fm.Operator.value="<%=tOperator%>";
				fm.all('BranchType').value ='<%=BranchType%>';
				fm.all('BranchType2').value='<%=BranchType2%>'
		}
		catch(ex)
		{
			alert("在LABranchToComInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
  function initDate()
  {
   			
   			
   		 
   }	
	// 险种授权的初始化
	function initBranchGrid()
	{
		var iArray = new Array();
		try
		{
			iArray[0]=new Array();
			iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1]="30px"; 	           		//列宽
			iArray[0][2]=1;            			//列最大值
			iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[1]=new Array();
			iArray[1][0]="展业机构代码";
			iArray[1][1]="100px";
			iArray[1][2]=20;
			iArray[1][3]=0;
			//iArray[1][4]="RiskCode";
			//iArray[1][5]="1|2";
			//iArray[1][6]="0|1";


			iArray[2]=new Array();
			iArray[2][0]="展业机构名称";
			iArray[2][1]="160px";
			iArray[2][2]=20;
			iArray[2][3]=0;

			iArray[3]=new Array();
			iArray[3][0]="管理机构";
			iArray[3][1]="80px";
			iArray[3][2]=10;
			iArray[3][3]=0;

			iArray[4]=new Array();
			iArray[4][0]="上级机构代码";
			iArray[4][1]="80px";
			iArray[4][2]=10;
			iArray[4][3]=0;

			iArray[5]=new Array();
			iArray[5][0]="展业机构级别";
			iArray[5][1]="70px";
			iArray[5][2]=10;
			iArray[5][3]=0;

			iArray[6]=new Array();
			iArray[6][0]="展业机构管理人员";
			iArray[6][1]="80px";
			iArray[6][2]=10;
			iArray[6][3]=0;

			iArray[7]=new Array();
			iArray[7][0]="展业机构内部代码";
			iArray[7][1]="0px";
			iArray[7][2]=0;
			iArray[7][3]=0;

			BranchGrid = new MulLineEnter( "fm" , "BranchGrid" );
			//这些属性必须在loadMulLine前
			BranchGrid.mulLineCount = 0;
			BranchGrid.displayTitle = 1;
			BranchGrid.locked=1;
			BranchGrid.canSel=1;
			BranchGrid.canChk=0;
			BranchGrid.loadMulLine(iArray);
		}
		catch(ex)
		{
			alert(ex);
		}
	}

</script>
