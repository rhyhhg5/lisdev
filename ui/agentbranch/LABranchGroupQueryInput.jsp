<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchGroupQueryInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String Flag=request.getParameter("Flag");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType22222:"+BranchType2);
  System.out.println("flag:"+Flag);
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and branchlevelcode=#03# ";
</script>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./LABranchGroupQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./LABranchGroupQueryInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
<%@include file="../common/jsp/ManageComLimit.jsp"%>
<title>销售团队</title>
</head>
<body onload="initForm();initElementtype();">
  <form action="./LABranchGroupQuerySubmit.jsp" method=post name=fm target="fraSubmit">
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLABranchGroup1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            销售团队代码
          </TD>
          <TD  class= input>
            <Input class=common name=BranchAttr >
          </TD>
          <TD  class= title>
            销售团队名称
          </TD>
          <TD  class= input>
            <Input class= common name=Name >
          </TD>
        </TR>
        <TR  class= common>
           <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly >
          </TD> 
          <!--TD  class= title>
            类型
          </TD>
          <TD  class= input>
            <Input class='code' name=BranchType maxlength=2 ondblclick="return showCodeList('BranchType',[this]);"
                                                            onkeyup="return showCodeListKey('BranchType',[this]);">
          </TD-->
     <TD  class= title>
            级别
          </TD>
          <TD  class= input>
            <Input class= 'codeno' name=BranchLevel verify="展业机构级别|notnull&code:BranchLevel" 
                                                  ondblclick="return showCodeList('branchlevel',[this,BranchLevelName],[0,1],null,msql,1);" 
                                                  onkeyup="return showCodeListKey('branchlevel',[this,BranchLevelName],[0,1],null,msql,1);" 
            ><Input class=codename name=BranchLevelName readOnly >
          </TD>
          </TR>
        <!--TR  class= common>
          <TD  class= title>
            地址编码
          </TD>
          <TD  class= input>
            <Input class= 'code' name=BranchAddressCode ondblclick="return showCodeList('station',[this]);" onkeyup="return showCodeListKey('station',[this]);">
          </TD>
          <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAddress >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=BranchPhone >
          </TD>
          <TD  class= title>
            传真
          </TD>
          <TD  class= input>
            <Input class= common name=BranchFax >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            邮编
          </TD>
          <TD  class= input>
            <Input class= common name=BranchZipcode maxlength=6 >
          </TD>
        </TR-->
        <TR class=common>
          <TD  class= title>
            团队主管代码
          </TD>
          <TD  class= input>
            <Input class= common name=BranchManager >
          </TD>
        <TD  class= title>
            团队主管名称
          </TD>
          <TD  class= input>
            <Input class= common name=BranchManagerName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            成立标志日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' name=FoundDate format='short'>
          </TD>
          <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate format='short'>
          </TD>
          
          	<!--TD class=title>
         		团队托管状态
         	</TD>
         	<TD  class= input>
            <Input class='codeno' name=TrusteeShip  ondblclick="return showCodeList('yesno',[this,TrusteeShipName],[0,1]);"
                                                         onkeyup="return showCodeListKey('yesno',[this]),[0,1];"
            ><Input class='codename' name=TrusteeShipName readOnly>
          </TD-->
         </TR>
         <!--TR class=common>
         	<TD class=title>
         		团队托管状态
         	</TD>
         	<TD  class= input>
            <Input class='code' name=TrusteeShip  ondblclick="return showCodeList('yesno',[this]);"
                                                         onkeyup="return showCodeListKey('yesno',[this]);">
          </TD>
        </TR-->
          <!--TD  class= title>
            停业
          </TD>
          <TD  class= input>
            <Input class='code' name=EndFlag MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);"
                                                         onkeyup="return showCodeListKey('yesno',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            是否单证交接
         </TD>
          <TD  class= input>
            <Input class= 'code' name=CertifyFlag MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);"
                                                              onkeyup="return showCodeListKey('yesno',[this]);">
          </TD>
          <TD  class= title>
            是否有独立的营销职场
          </TD>
          <TD  class= input>
            <Input class= 'code' name=FieldFlag MAXLENGTH=1 ondblclick="return showCodeList('yesno',[this]);"
                                                            onkeyup="return showCodeListKey('yesno',[this]);" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            标志
          </TD>
          <TD  class= input>
            <Input class=common name=State MaxLength=10 >
          </TD>
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class=common name=Operator >
          </TD>
        </TR-->
      </table>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <% if(Flag==null) {%>
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
          <%} %>
          <INPUT VALUE="下  载" TYPE=button onclick="downLoad();" class="cssButton">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAGroupGrid);">
    		</td>
    		<td class= titleImg>
    			 销售团队结果
    		</td>
    	</tr>
    </table>
    <Input type=hidden name=TrusteeShip>
    <Input type=hidden name=Flag >
    <Input type=hidden name=BranchType >
     <Input type=hidden name=BranchType2 >
    <input type=hidden name=AgentGroup value=''>  <!--后台操作的隐式机构编码，不随机构的调整而改变 -->
    <input type=hidden name=UpBranch value=''>  <!--上级机构代码，存储隐式机构代码 -->
    <input type=hidden name=querySQL value=""> 
  	<Div  id= "divAGroupGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBranchGroupGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
