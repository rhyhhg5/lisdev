<%
//程序名称：LASpecComInit.jsp
//程序功能：
//创建日期：2003-09-16
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('AgentCom').value = '';
    fm.all('Name').value = '';
    fm.all('Address').value = '';
    fm.all('ACType').value = '';
    fm.all('ACTypeName').value = '';
    fm.all('ACTypeCode').value = '';
    fm.all('Asset').value = '';
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';
    fm.all('Profit').value = '';
    fm.all('ChiefBusiness').value = '';
    fm.all('BankAccNo').value='';
    fm.all('BankAccNoInsure').value='';
    fm.all('Corporation').value='';
    fm.all('SellFlag').value = '';
    fm.all('SellFlagName').value = '';
    fm.all('LicenseNo').value = '';
    fm.all('BankCode').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('hideOperate').value = '';
    fm.all('EndFlag').value = '';
    fm.all('EndFlagName').value = '';
    fm.all('EndDate').value = '';
    fm.all('Licensestart').value = '';
    fm.all('Licenseend').value = '';
    fm.all('BankAccName').value = '';
    fm.all('BankAccOpen').value = '';
    fm.all('SignDate').value='';
    fm.all('ProEndDate').value='';
    fm.all('ProtocolNo').value='';
    fm.all('BankAccOpen').value='';
    fm.all('BankAccName').value='';
    fm.all('AgentOrganCode').value='';
    
  }
  catch(ex)
  {
    alert("在LASpecComInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LASpecComInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
var ComToAgentGrid;
function initComToAgentGrid() { 
  var acodeSql =getSql();                              
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="互动专员编码";          		        //列名
    iArray[1][1]="50px";      	      		//列宽
    iArray[1][2]=20;            			//列最大值
    iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[1][4]="comtoagent";
    iArray[1][5]="1|2|3|4";     //引用代码对应第几列，'|'为分割符
    iArray[1][6]="0|1|2|3";    //上面的列中放置引用代码中第几位值
    iArray[1][15]='LABranchGroup.BranchType';
    iArray[1][16]=acodeSql;

    
    iArray[2]=new Array();
    iArray[2][0]="互动专员姓名";          		        //列名
    iArray[2][1]="50px";      	      		//列宽
    iArray[2][2]=20;            			//列最大值
    iArray[2][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    
    iArray[3]=new Array();
    iArray[3][0]="所在团队代码";          		        //列名
    iArray[3][1]="100px";      	      		//列宽
    iArray[3][2]=20;            			//列最大值
    iArray[3][3]=0;             //是否允许输入,       
    
   
    iArray[4]=new Array();
    iArray[4][0]="所在团队名称";          		        //列名
    iArray[4][1]="100px";      	      		//列宽
    iArray[4][2]=100;            			//列最大值
    iArray[4][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
    
    iArray[5]=new Array();
    iArray[5][0]="主键";          		        //列名
    iArray[5][1]="0px";      	      		//列宽
    iArray[5][2]=100;            			//列最大值
    iArray[5][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的 
 

    ComToAgentGrid = new MulLineEnter( "fm" , "ComToAgentGrid" ); 
    //这些属性必须在loadMulLine前

    ComToAgentGrid.mulLineCount = 1;   
    ComToAgentGrid.displayTitle = 1;
  //ComToAgentGrid.selBoxEventFuncName = "showOne";
  //ComToAgentGrid.hiddenPlus=1;
  //ComToAgentGrid.hiddenSubtraction=1;
    ComToAgentGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ComToAgentGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();  
    initComToAgentGrid();
      
  }
  catch(re)
  {
    alert("LASpecComInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>