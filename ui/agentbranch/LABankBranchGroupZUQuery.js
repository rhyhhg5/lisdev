//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var arrDataSet; 
var turnPage = new turnPageClass();


//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LABranchGroupQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
  var arrReturn = new Array();
	var tSel = BranchGroupGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				//alert(1);
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = BranchGroupGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected;
	//}
	arrSelected = new Array();
	//tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select AgentGroup,Name,ManageCom,UpBranch,BranchAttr,BranchType,BranchLevel,";
	strSQL = strSQL + "getunitecode(BranchManager),BranchAddress,BranchPhone,BranchZipcode,FoundDate,EndDate,EndFlag,FieldFlag,";
	strSQL = strSQL + "Operator,BranchManagerName,BranchFax,UpBranchAttr";
	strSQL = strSQL + " from LABranchGroup where AgentGroup ='"+BranchGroupGrid.getRowColData(tRow-1,8)+"' and (state<>'1' or state is null)";
	strSQL = strSQL + " and BranchType = '"+document.fm.BranchType.value+"' and BranchType2 = '"+document.fm.BranchType2.value+"'";
	//alert(strSQL);
	
	//测试修改********************************************
	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
}


// 查询按钮
function easyQueryClick()
{	 
	if(!verifyInput()) 
  { return; }
	// 初始化表格
	initBranchGroupGrid();
	//fm.all('BranchType').value = top.opener.fm.BranchType.value;
	// 书写SQL语句
	var strSQL = "";
	var tSQL = "";
	var tSelect = "";
	var tReturn = parseManageComLimitlike();
	var tMark = 0;
	var tReturn = parseManageComLimitlike();
	tSQL = "select distinct(a.agentgroup) from latree a,laagent b where a.agentcode = b.agentcode and a.AgentKind = 'E'"
	if(document.fm.BranchManager.value != "" && document.fm.BranchManager.value != null)
  {
  	tSQL += " and b.AgentCode like '"+document.fm.BranchManager.value+"%25'";
  	tMark = 1;
  }
  if(document.fm.BranchManagerName.value != "" && document.fm.BranchManagerName.value != null)
  {
  	tSQL += "  and b.Name like '%25"+document.fm.BranchManagerName.value+"%25'";
  	tMark = 1;
  }
  if(tMark == 1)
  {
	  var arrValue = getArrValueBySQL(tSQL);
	  if(arrValue != null)
	  {
	  	var i;
	  	tSelect = "'"
	  	for(i=0;i<arrValue.length;i++)
	  	{
	  		tSelect += arrValue[i][0] + "','";
	  	}
	  	tSelect += "'";
	  }
  }
	var strAgent = "";
	if(fm.BranchManager.value!=null&&fm.BranchManager.value!=''){
		strAgent = " and a.BranchManager=getAgentCode('"+fm.BranchManager.value+"') ";
	}
  strSQL  = "SELECT";
  strSQL += " a.BranchAttr,";
  strSQL += " a.Name,";
  strSQL += " a.ManageCom,";
  strSQL += " getunitecode(a.branchmanager),";
  strSQL += " a.branchmanagername,";
  strSQL += " a.FoundDate,";
  strSQL += " a.EndFlag,";
  strSQL += " a.AgentGroup";
  strSQL += " FROM";
  strSQL += " LABranchGroup a";
  strSQL += " WHERE";
  strSQL += "    (a.State<>'1' or a.State is null)   and branchlevel='41'  ";
  
  //strSQL += getWherePart('a.InsideFlag','InsideFlag')
  strSQL += getWherePart('a.EndFlag','EndFlag')
  strSQL += getWherePart('a.Name','Name','like');
  strSQL += getWherePart('a.ManageCom','ManageCom','like');
  strSQL += getWherePart('a.BranchAttr','BranchAttr','like');
 // strSQL += getWherePart('a.BranchLevel','BranchLevel','like');
  strSQL += getWherePart('a.FoundDate','FoundDate');
  strSQL += getWherePart('a.EndDate','EndDate');
  strSQL += getWherePart('a.BranchType','BranchType');
  strSQL += getWherePart('a.BranchType2','BranchType2');
  strSQL += getWherePart('a.BranchManagerName','BranchManagerName');
  strSQL += strAgent;//+= getWherePart('a.BranchManager','BranchManager');
//  if("" != tSelect)
//  {
//  	strSQL += " and a.agentgroup in ("+tSelect+")";
//  }
  strSQL += tReturn + " order by BranchAttr";
  
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.arrDataCacheSet = chooseArray(tArr,[0,1,2,3,4,5]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BranchGroupGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

/***************************************
 * 根据参数查询结果
 *     参数：pmSQL  查询用的SQL文
 *     返回：查询结果
 ***************************************/
function getArrValueBySQL(pmSQL)
{
	//alert(pmSQL);
	var strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
	
	if (!strQueryResult)
		return null;
		
	var arr = decodeEasyQueryResult(strQueryResult);
	
	return arr;
}


function DoNewDownload()
{
  var tManageCom  = fm.all('ManageCom').value;
	if ( tManageCom == null || tManageCom == "" )
	{
		alert("请输入管理机构！");
		return;
	}
 if(BranchGroupGrid.mulLineCount==0)
 {
    alert("列表中没有数据可下载");
    return;
 }
 else {
 // 书写SQL语句
		var strSQL = "";

  	strSQL  =" SELECT  BranchAttr, Name,ManageCom,"
               +" branchmanager,branchmanagername, FoundDate, EndFlag "
               +" FROM  LABranchGroup "
               +" WHERE  (State<>'1' or State is null)   and branchlevel='41' "
               +" and ManageCom  like '"+fm.all("ManageCom").value+"%' "
            // + getWherePart('ManageCom','ManageCom','like')
               + getWherePart('BranchAttr','BranchAttr')
               + getWherePart('Name','Name')
               + getWherePart('BranchManager','BranchManager')
               + getWherePart('BranchManagerName','BranchManagerName')  
               + getWherePart('EndFlag','EndFlag')
               + getWherePart('FoundDate','FoundDate')
               + getWherePart('EndDate','EndDate')
               + getWherePart('BranchType','BranchType')
               + getWherePart('BranchType2','BranchType2');
    strSQL+=" order by BranchAttr ";
    fm.querySql.value = strSQL;
    var oldAction = fm.action;
    fm.action = "LABankBranchGroupQueryXLS.jsp";
    fm.submit();
    fm.action = oldAction;
  }
}					