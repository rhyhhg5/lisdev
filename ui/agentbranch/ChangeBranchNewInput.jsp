<%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<html>
	<%
	//程序名称：ChangeBranchInput.jsp
	//程序功能：
	//创建日期：2002-08-16 16:25:40
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		String tOperator = tG.Operator;
		String BranchType=request.getParameter("BranchType");
		String BranchType2=request.getParameter("BranchType2");
 		String CurrentDate= PubFun.getCurrentDate();   
	%>
	<%@page contentType="text/html;charset=GBK" %>
   <script language="javascript">
   		function initDate(){
   			fm.MakeTime.value="<%=CurrentDate%>";
   			fm.Operator.value="<%=tOperator%>";
				fm.all('BranchType').value ='<%=BranchType%>';
				fm.all('BranchType2').value='<%=BranchType2%>'
   			
   		//	var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
   		//	var arrResult = easyExecSql(strSQL);
		//		if(arrResult != null)
		//		{
   		//    fm.optname.value= arrResult[0][0];
 		//		}
   		}
   </script>
	<head >
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<!--  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>-->
		<SCRIPT src="ChangeBranchNewInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="ChangeBranchNewInit.jsp"%>
		<%@include file="../agent/SetBranchType.jsp"%>
		<%@include file="../common/jsp/ManageComLimit.jsp"%>
		<title></title>
	</head>
	<body  onload="initDate();initForm();initElementtype();" >
		<form action="./ChangeBranchNewSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divChangeBranch);">
					</td>
					<td class=titleImg>被调整单位信息</td>
				</tr>
			</table>
			<Div  id= "divChangeBranch" style= "display: ''">
				<table  class= common>
			<tr  class= common> 
                <td  class= title> 管理机构 </td>
                <td  class= input><Input class="codeno" name=AddManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
                     ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
                     onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
                ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
                </td> 
            </tr>
					<TR  class= common>
						<TD  class= title>调整机构代码</TD>
						<TD  class= input>
							<Input class=code name=AdjustBranchCode readonly 
							ondblclick="return getChangeComName(this,AdjustBranchName);"
							onkeyup="return getChangeComName(this,AdjustBranchName);"
							verify="调整机构代码|notnull"  >
						</TD>
						<TD  class= title>机构名称</TD>
						<TD  class= input>
							<Input class=readonly readonly name=AdjustBranchName>
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>机构管理人员</TD>
						<TD  class= input>
							<Input class=readonly readonly name=BranchManager>
						</TD>
						<TD  class= title>机构管理人员姓名</TD>
						<TD  class= input>
							<Input class=readonly readonly name=BranchManagerName>
						</TD>
					</TR>
					<TR>
						<TD  class= title >
							调动类型
						</TD>
						<TD  class= Input >
							<input class=codeno CodeData="0|2^0|断裂原有关系^1|带走原有关系"  name=IsSingle ondblclick="return showCodeListEx('IsSingle',[this,IsSingleName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('IsSingle',[this,IsSingleName],[0,1],null,null,null,1);" verify="调动类型|NotNull"><input class=codename name=IsSingleName elementtype=nacessary>
						</TD>
					</TR>
				</table>
			</div>

			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAimChangeBranch);">
						<td class=titleImg>
							目标单位信息
						</td>
					</td>
				</tr>
			</table>
			<Div  id= "divAimChangeBranch" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD class= title>目标机构代码</TD>
						<TD  class= input>
							<Input class=code name=AdjustAfterBranchCode  readonly 
							ondblclick="return getChangeComName(this,AdjustAfterBranchName);" 
							onkeyup="return getChangeComName(this,AdjustAfterBranchName);" 
							verify="调整调入的目标机构代码|notnull"   >
						</TD>
						<TD class= title>目标机构名称</TD>
						<TD  class= input>
							<Input class=readonly readonly  name=AdjustAfterBranchName verify="调整调入的目标机构名称|notnull">
						</TD>
					</tr>
					<TR  class= common>
						<TD  class= title>调整日期</TD>
						<TD  class= input >
							<Input class='coolDatePicker' dateformat= 'short' name=AdjustDate verify="调整日期|notnull&Date">
						</TD>
						<TD  class= title>调动后机构代码</TD>
						<TD  class= input>
							<Input class=readonly  name=NewBranchAttr readonly>
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>操作日期</TD>
						<TD  class= input>
							<Input class=readonly readonly name=MakeTime >
						</TD>
						<TD  class= title>操作员代码</TD>
						<TD  class= input>
							<Input class="readonly" readonly name=Operator><Input type=hidden name=optname>
						</TD>
					</TR>
				</table>
				<br>
				<input type=button class=cssButton name=saveb value='保存' onclick="return submitSave()">
				<input type=button class=cssButton value='重置' onclick="clearGrid();">
			</div>
			<input type=hidden name=UpBranchAttr value=''>
			<input type=hidden name=hideAdjustAgentGroup value=''>
			<input type=hidden name=hideAimAgentGroup value=''>
			<input type=hidden name=hideLevel value=''>
			<input type=hidden name=hideCom value=''>
			<input type=hidden name=hideUpBranch value=''>
			<input type=hidden name=AdjustAfterAgentGroup value=''>
			<input type=hidden name=AdjustAfterLevel value=''>
			<input type=hidden name=AdjustAfterAttr value=''>
			<input type=hidden name=BranchType value=''>
			<input type=hidden name=BranchType2 value=''>
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
