//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	if ((trim(fm.all('BranchAttr').value)=='')||(fm.all('BranchAttr').value==null))
  {
  	alert("请查询出机构信息！");
  	return false;
  }
   if ((trim(fm.all('BranchManager').value)=='')||(fm.all('BranchManager').value==null))
  {
  	alert("请录入管理人员代码！");
  	return false;
  }
  if ((trim(fm.all('BranchManagerName').value)=='')||(fm.all('BranchManagerName').value==null))
  {
  	alert("请录入正确的管理人员代码！");
  	return false;
  }
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  if(tBranchType=='1' && tBranchType2=='01')             
	{
	var rSQL=""; 
	if ((trim(fm.all('BranchLevel').value)=='')||(fm.all('BranchLevel').value==null))
  {
  	alert("请先查询销售团队代码相关信息 ！");
  	return false;
  }
  else
  { 
  		if(fm.all('BranchLevel').value=='02'){
  		rSQL=" select getunitecode(agentcode) from latree where agentseries='2' and branchtype='1' and branchtype2='01' and agentgrade>'B01' "	
  		+" and agentcode=getagentcode('"+fm.all('BranchManager').value+"')";
  		} 
  		else if(fm.all('BranchLevel').value=='01'){
  		rSQL=" select getunitecode(agentcode) from latree where agentseries='1' and branchtype='1' and branchtype2='01' and agentgrade>'B01' "	
  		+" and agentcode=getagentcode('"+fm.all('BranchManager').value+"')";
  			}
  		else if(fm.all('BranchLevel').value=='03'){
  		rSQL=" select getunitecode(agentcode) from latree where agentseries='3' and branchtype='1' and branchtype2='01' and agentgrade>'B01' "	
  		+" and agentcode=getagentcode('"+fm.all('BranchManager').value+"')";
  			}
  		var strQueryResult  = easyQueryVer3(rSQL, 1, 0, 1);
		  if(!strQueryResult ){
			alert("管理人员代码 与销售团队级别不符！");
			return false;
		  }
		}
		}
	
  if (!beforeSubmit())
    return false;
   if(!checkBranchManager()){
		return false;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
   //showSubmitFrame(mDebug);
  fm.submit(); //提交
}
//核查团队有无主管
function checkBranchManager(){

	var tBranchAttr;
	var tBranchManager=fm.all('BranchManager').value;
	var tBranchType = fm.all('BranchType').value;
    var tBranchType2 = fm.all('BranchType2').value;
	if(fm.all('BranchAttr').value==null || fm.all('BranchAttr').value==''){
		alert('销售团队代码不能为空！');
		return false;
	}else{
		if(tBranchType=='3' && tBranchType2=='01'){
			tBranchAttr=fm.all('BranchAttr').value;
			var tSql="select gradeproperty2 from laagentgrade where gradecode=(select agentgrade from latree where agentcode=getagentcode('"+tBranchManager+"'))";
			var arr=easyExecSql(tSql);
		//alert(arr[0][0]);
			var tGradeProperty2=arr[0][0];
		
			tSql="select * from laagent where agentcode in (select agentcode from latree where agentgroup=(select agentgroup from labranchgroup "
			+"where branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and branchattr='"+tBranchAttr+"' and Endflag='N')"
			+"and agentgrade in (select gradecode from laagentgrade where branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and gradeproperty2='"+tGradeProperty2+"')"
			+"and branchtype='"+tBranchType+"' and branchtype2='"+tBranchType2+"' and agentcode<>getagentcode('"+tBranchManager+"')) and agentstate<'06'";
		var strQueryResult  = easyQueryVer3(tSql, 1, 0, 1);
		if(strQueryResult && tGradeProperty2==1){
			alert("该团队已存在渠道经理级别的主管！");
			return false;
		}else if(strQueryResult && tGradeProperty2==2){
			alert("该团队已存在营业部负责人级别的主管！");
			return false;
		}
		}
		
		
		return true;	
	}
	
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  initForm();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LABranchGroup.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
	if (!verifyInput())
  return false;
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
	var tAgentGrade = fm.all('AgentGrade').value;//这里存储管理人员的职级。
	if(tBranchType=='3' && tBranchType2=='01')             
	{
    if(fm.all('BranchLevel').value=='42'&&tAgentGrade==1){
    	alert('营业部的主管只能是营业部负责人');
    	return false;
    }
    if(fm.all('BranchLevel').value=='41'&&tAgentGrade==2){
    	alert('营业组的主管只能是渠道经理!');
    	return false;
    }
  }
  //添加操作
  if (fm.BranchManager.disabled == true)
  {
    alert("该展业机构内已存在主管级的代理人,不能再任命主管!");
    return false;
  }
  

  var sql_manger="select getUniteCode(branchmanager) from labranchgroup a where branchattr='"+fm.all('BranchAttr').value+"' "
        +getWherePart('a.BranchType','BranchType')
        +getWherePart('a.BranchType2','BranchType2');
  var strQueryResult  = easyQueryVer3(sql_manger, 1, 0, 1);
  if (strQueryResult)
  {     
  	var arr = decodeEasyQueryResult(strQueryResult);
    var tmanager = arr[0][0];
    if(tmanager!=null && tmanager!='')
    {
    	alert("该机构已有主管,不能对其任命主管!");
    	initForm();
    	return false;   	
    }  
  }
 	
      
  return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }else
  {
  	parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//Click事件，当点击“查询”图片时触发该函数
function BranchQuery()
{
  //下面增加相应的代码
  if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!=''){
  	var arrReturn = getQueryResult();
  	if(!arrReturn) return ;
  	afterQuery( arrReturn );
  	return;
  }
  mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  //alert(tBranchType2);
  if(tBranchType=='2' && tBranchType2=='01')
  {
  showInfo=window.open("./LABranchManagerQueryHtml.jsp?BranchType="+tBranchType+"&BranchType2="+tBranchType2);	
  }else
  {
  showInfo=window.open("./LABranchManagerQueryHtml.jsp?BranchType="+tBranchType+"&BranchType2="+tBranchType2);
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function afterQuery(arrQueryResult)
{
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('AgentGroup').value = arrResult[0][0];  
		fm.all('Name').value = arrResult[0][1];                                              
		fm.all('ManageCom').value = arrResult[0][2];                                              
		fm.all('UpBranch').value = arrResult[0][3];                         
    	fm.all('BranchAttr').value = arrResult[0][4];   
		fm.all('BranchType').value = arrResult[0][5];                                             
		fm.all('BranchLevel').value = arrResult[0][6];				                                           
		fm.all('BranchManager').value = arrResult[0][7];                                           
		fm.all('BranchAddress').value = arrResult[0][8];                                           
		fm.all('BranchPhone').value = arrResult[0][9];                                               
		fm.all('BranchZipcode').value = arrResult[0][10];                                               
		fm.all('FoundDate').value = arrResult[0][11];                                               
		fm.all('EndDate').value = arrResult[0][12];                                               
		fm.all('EndFlag').value = arrResult[0][13];                                               
		fm.all('FieldFlag').value = arrResult[0][14];                                               
		fm.all('Operator').value = arrResult[0][15];
		fm.all('BranchManagerName').value = arrResult[0][16];                                               
		fm.all('BranchFax').value = arrResult[0][17];                                                  
		fm.all('UpBranchAttr').value = arrResult[0][18]; 
		fm.all('BranchType2').value = arrResult[0][19];	
 	}
}

function QueryBranchManagerName()
{
	if (fm.all('BranchManager').value==null||fm.all('BranchManager').value=='')
 	{
 	alert("请录入管理人员代码!");
 		return;
 	}
	var tReturn = getManageComLimitlike("a.ManageCom");
 	var strSQL=""; 
 	var strAgent = "";
	if(fm.BranchManager.value!=null&&fm.BranchManager.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.BranchManager.value+"') ";
	}
 	strSQL = "select getunitecode(a.AgentCode),a.name,(Select AgentSeries From LATree Where AgentCode = a.AgentCode) "
         +"from LAAgent a where 1=1 "
         + " and  a.managecom ='"+fm.all('ManageCom').value+"'"
         +" and (a.AgentState is null or a.AgentState < '03') "
         //+getWherePart('a.AgentCode','BranchManager')
         + strAgent
         +getWherePart('a.BranchType','BranchType')
         +getWherePart('a.BranchType2','BranchType2');
   var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;       
 
 
  if(tBranchType=='1' && tBranchType2=='01')             
	{
	if ((trim(fm.all('BranchLevel').value)=='')||(fm.all('BranchLevel').value==null))
  {
  	alert("请先查询销售团队代码相关信息 ！");
  	return false;
  }
  else
  	{
  		if(fm.all('BranchLevel').value=='02'){
  		strSQL=strSQL+" and a.agentcode in (select agentcode from latree where agentseries='2' and branchtype='1' and branchtype2='01') "	;
  		}
  		else if(fm.all('BranchLevel').value=='01'){
  		strSQL=strSQL+" and a.agentcode in (select agentcode from latree where agentseries='1' and branchtype='1' and branchtype2='01') "	;
  			}
  		else if(fm.all('BranchLevel').value=='03'){
  		strSQL=strSQL+" and a.agentcode in (select agentcode from latree where agentseries='3' and branchtype='1' and branchtype2='01') "	;
  			}
	}
}
  
  
  
  var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!strQueryResult)
  {
  	alert('所输管理人员代码无效！');
  	fm.all('BranchManager').value='';
  	fm.all('BranchManagerName').value = '';
  	fm.all('AgentGrade').value = '';
  	return false;
  }
  else
  {
    var arr = decodeEasyQueryResult(strQueryResult);
    fm.all('BranchManagerName').value = arr[0][1];
    fm.all('AgentGrade').value = arr[0][2];
    if(fm.all('AgentGrade').value == '0')
    {
      alert('管理人员代码输入有误，必须是管理系列的人员!');
      fm.all('BranchManager').value='';
      fm.all('BranchManagerName').value = '';
      fm.all('AgentGrade').value = '';
      return false;	
    }
  }
}
function getQueryResult()
{
	var strSQL = "";
	strSQL = "select AgentGroup,Name,ManageCom,UpBranch,BranchAttr,BranchType,BranchLevel,"
	+" getUniteCode(BranchManager),BranchAddress,BranchPhone,BranchZipcode,FoundDate,EndDate,EndFlag,FieldFlag,"
	+" Operator,BranchManagerName,BranchFax,UpBranchAttr,BranchType2,TrusteeShip,ModifyDate"
	+" from LABranchGroup "
	+" where BranchAttr ='"+fm.all('BranchAttr').value+"' and (state<>'1' or state is null) "
	+" and (upbranchattr='0' or upbranchattr is null)  " 
	+ getWherePart('BranchType','BranchType')
  + getWherePart('BranchType2','BranchType2');
	     
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("所查团队不符合任命规则或不存在！");
    return false;
  }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  if(arrSelected[0][7]!=null&&arrSelected[0][7]!=''){
  	alert("该销售团队已经存在主管,不需要再次任命!");
  	return false;
  }
	return arrSelected;
}