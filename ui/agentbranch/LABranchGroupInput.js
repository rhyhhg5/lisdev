//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  { 
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	fm.all('TrusteeShip').value='N';
	if(mOperate=="")
	{
		addClick();
	}
	
  if (!beforeSubmit())
    return false;
   // alert(1);
	if (!changeGroup())
		return false;
  if(mOperate=="INSERT||MAIN")
  {
  	if(!CheckBranchValid())
  		return false;
  }
  if(fm.BranchType2.value=='01'&&fm.BranchType.value=='1'){
  	if(!CheckDirect())
  		return false;
  }
  
  fm.all('Save').disabled = true ;	
  fm.all('Modify').disabled = true ;	
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
   if(mOperate=="INSERT||MAIN")
  {
  	fm.all('Save').disabled = true ;	
  }  
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	
  fm.all('UpBranchAttr').disabled = true ;
	mOperate="";
	//alert(mOperate);
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    //alert(content);
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }  
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LABranchGroup.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	 
//  if ((trim(fm.all('BranchAttr').value)=='')||(fm.all('BranchAttr').value==null))
//  {
//  	alert("请输入展业机构代码！");
//  	fm.all('BranchAttr').focus();
//  	return false;
//  }
//  if ((trim(fm.all('Name').value)=='')||(fm.all('Name').value==null))
//  {
//  	alert("请输入展业机构名称！");
//  	fm.all('BranchName').focus();
//  	return false;
//  }
//  if ((trim(fm.all('ManageCom').value)=='')||(fm.all('ManageCom').value==null))
//  {
//  	alert("请输入管理机构！");
//  	fm.all('ManageCom').focus();
//  	return false;
//  }
//  if ((trim(fm.all('BranchLevel').value)=='')||(fm.all('BranchLevel').value==null))
//  {
//  	alert("请输入展业机构级别！");
//  	fm.all('BranchLevel').focus();
//  	return false;
//  }
//  if ((trim(fm.all('EndFlag').value)=='')||(fm.all('EndFlag').value==null))
//  {
//  	alert("请输入停业标记！");
//  	fm.all('EndFlag').focus();
//  	return false;
//  }
  if (!verifyInput()) return false;
  //alert(mOperate);
  if (trim(mOperate) == 'INSERT||MAIN')
  {
    //校验显式展业机构代码  
    var strSQL = "";
    strSQL = "select AgentGroup from LABranchGroup where 1=1 and (state<>'1' or state is null)"
	    + getWherePart('BranchAttr')
	    + getWherePart('BranchType')
	    + getWherePart('BranchType2')
	    +" Union "
	    +"Select AgentGroup From LABranchGroupB Where 1=1 and (state<>'1' or state is null)"
	    + getWherePart('BranchAttr')
	    + getWherePart('BranchType')
	    + getWherePart('BranchType2');	 
    var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    //alert(strSQL);
    if (strQueryResult) {  	
  	alert('该展业机构代码已存在！');
  	fm.all('BranchAttr').value = '';
  	return false;
    }
    //xjh Modify for PICCH,2005/2/18,修改使其一致
    var cManageCom = fm.all('ManageCom').value;
	var cBranchAttr = fm.all('BranchAttr').value;
	if (cManageCom.substring(0,cManageCom.length) != cBranchAttr.substring(0,cManageCom.length))
	{
		alert("销售团队代码录入错误，请查询！");
		fm.all('BranchAttr').focus();
		return false;
	}
//		if(cManageCom.length < 8)
//	{
//		var a = "99999999999999999";
//		if(cBranchAttr.substring(cManageCom.length,8) != a.substring(cManageCom.length,8))
//		{
//		alert("销售团队代码录入错误，请查询！");
//		fm.all('BranchAttr').focus();
//		return false;
//		}
//	}
  }
  var tbranchattr = trim(fm.all('BranchAttr').value);
  if (trim(mOperate) == 'UPDATE||MAIN')
  {
  var cManageCom = fm.all('ManageCom').value;
	  var cBranchAttr = fm.all('BranchAttr').value;
	 
	 	if (cManageCom.substring(0,cManageCom.length) != cBranchAttr.substring(0,cManageCom.length))
	 	{
			alert("销售团队代码录入错误，请查询！");
		fm.all('BranchAttr').focus();
		return false;
	}
}

 if(tbranchattr.length == 14)
  	{
  		if(fm.all('BranchLevel').value!='01')
  		{
  		alert("级别与机构代码长度不对应,应该为01");	
  		return false;
  		}
  		
  	}
   if(tbranchattr.length == 10)
  	{
  		if(fm.all('BranchLevel').value!='03')
  		{
  		alert("级别与机构代码长度不对应,应该为03");
  		return false;	
  		}
  		
  	}
   if(tbranchattr.length == 12)
  	{
  		
  		if(fm.all('BranchLevel').value!='02')
  		{
  			
  		alert("级别与机构代码长度不对应,应该为02");	
  		return false;
  		}
  		
  	}
  	
  	//对成本中心编码进行校验
 	if(fm.ManageCom.value=="")
 	{
 		alert("所属管理机构不能为空！");
 		return false;
 	}
 	if(!checkSap(fm.CostCenter.value,fm.ManageCom.value))
 	{
 		return false;
 	}
  return true;
} 

//对成本中心编码进行校验
function checkSap(CostCenter,tManageCom)
{
 var erroInfo="成本中心代码有误，请根据发文《人保健康计财[2008] 212号》申请成本中心代码！";
 var tt=CostCenter.substr(4,1);
 if(tt!="9")
 {
 	alert(erroInfo);
 	return false;
 } 

//modify by zhuxt 20141028 #2251 取消个险渠道团队创建和修改时针对sap管理机构对应关系的校验
/*
 var tSQL="select codealias from licodetrans  where codetype='ManageCom' and code='"+tManageCom+"'";
 var strQueryResult1 = easyQueryVer3(tSQL, 1, 1, 1); 	  
 var arr1 = decodeEasyQueryResult(strQueryResult1);
 if(!arr1)
 {
 	alert("SAP没有定义公司编码，请联系SAP定义公司编码！"); 
 	return false;
 }else{
 	var ss=arr1[0][0]
 	if(ss!=CostCenter.substr(0,4))
 	{
 	alert(erroInfo);
 	return false;
	 }
 }
 */
 return 1;
}          


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
  fm.all('BranchManager').value ='';
  fm.all('BranchManagerName').value ='';
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
    if ((fm.all("AgentGroup").value==null)||(trim(fm.all("AgentGroup").value)==''))
    {
    	alert("请先查询出要修改的销售单位！");
    	return false;
    }
    
  if ((fm.all("BranchAttr").value==null)||(trim(fm.all("BranchAttr").value)==''))
    alert("请确定要修改的销售单位！");
  else
  { 
    if (!queryBranchCode())
      return false;
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //mOperate="QUERY||MAIN";
     var tBranchType = "1";
    var tBranchType2 = "01";
  showInfo=window.open("./LABranchGroupQueryHtml.jsp?BranchType="+tBranchType+"&BranchType2="+tBranchType2);
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("BranchAttr").value==null)||(trim(fm.all("BranchAttr").value)==''))
    alert("请确定要删除的销售单位！");
  else
  {
    if (!queryBranchCode())
      return false;
    if (!queryAgent())
      return false;
    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";  
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('AgentGroup').value = arrResult[0][0];  
		fm.all('Name').value = arrResult[0][1];                                              
		fm.all('ManageCom').value = arrResult[0][2]; 
		//UpBranch
		fm.all('UpBranch').value = arrResult[0][3]; 
		if(trim(arrResult[0][3]) != "" || arrResult[0][3] != null)
		{
			var sql="select BranchAttr,name from LABranchGroup where AgentGroup = '" + trim(arrResult[0][3]) +"'";
			var strQueryResult  = easyQueryVer3(sql, 1, 0, 1);  
    	if (strQueryResult) 
    	{
    		var arrSelected = decodeEasyQueryResult(strQueryResult);
    		fm.all('UpBranchA').value = arrSelected[0][0];
    		//alert(1);
    		fm.all('UpBranchAName').value = arrSelected[0][1];
    	}
		}                         
    fm.all('BranchAttr').value = arrResult[0][4];   
		fm.all('BranchType').value = arrResult[0][5];                                             
		fm.all('BranchLevel').value = arrResult[0][6];				                                           
		fm.all('BranchManager').value = arrResult[0][7]; 
		//2014-11-06 张飞飞 加的新工号
		fm.all('groupagentcode').value = arrResult[0][27];
		fm.all('BranchAddress').value = arrResult[0][8];                                           
		fm.all('BranchPhone').value = arrResult[0][9];                                               
		fm.all('BranchZipcode').value = arrResult[0][10];                                               
		fm.all('FoundDate').value = arrResult[0][11];                                               
		fm.all('EndDate').value = arrResult[0][12];                                               
		fm.all('EndFlag').value = arrResult[0][13];                                               
		fm.all('FieldFlag').value = arrResult[0][14];                                               
		fm.all('Operator').value = arrResult[0][15];
		fm.all('BranchManagerName').value = arrResult[0][16];                                               
		fm.all('BranchFax').value = arrResult[0][17]; 
		fm.all('TrusteeShip').value = arrResult[0][20];
		fm.all('ModifyDate').value = arrResult[0][21];
		fm.all('CostCenter').value = arrResult[0][22];
		fm.all('tCostCenter').value = arrResult[0][22];
		fm.all('UpBranchAttr').value = arrResult[0][18]==''?0:arrResult[0][18];
		fm.all('UpBranchAttrName').value = fm.all('UpBranchAttr').value=='0'?'非直辖':'直辖';
		fm.all('UpBranchAttr').disabled = true;
     if(fm.all('ManageCom').value!="" && fm.all('ManageCom').value!=null)
		  {
		  var Sql_Name="select Name from ldcom where comcode='"+fm.all('ManageCom').value+"' ";
     
      var strQueryResult_Name  = easyQueryVer3(Sql_Name, 1, 1, 1);
		  if (strQueryResult_Name)
			{
				var arr = decodeEasyQueryResult(strQueryResult_Name);
			  fm.all('ManageComName').value= trim(arr[0][0]) ;
		  }
		 }                                                      
           
    showOneCodeNametoAfter('branchlevel','BranchLevel');
		 
		 if(fm.all('FieldFlag').value!="" && fm.all('FieldFlag').value!=null)
		  {
		  var Sql_Name="select codename from ldcode where codetype='yesno' and code='"+fm.all('FieldFlag').value+"' ";
     
      var strQueryResult_Name  = easyQueryVer3(Sql_Name, 1, 1, 1);
      //alert(strQueryResult_Name);
		  if (strQueryResult_Name)
			{
				var arr = decodeEasyQueryResult(strQueryResult_Name);
			  fm.all('FieldFlagName').value= trim(arr[0][0]) ;
		  }
		 }                                                      
            
      if(fm.all('EndFlag').value!="" && fm.all('EndFlag').value!=null)
		  {
		  var Sql_Name="select codename from ldcode where codetype='yesno' and code='"+fm.all('EndFlag').value+"' ";
     
      var strQueryResult_Name  = easyQueryVer3(Sql_Name, 1, 1, 1);
      //alert(strQueryResult_Name);
		  if (strQueryResult_Name)
			{
				var arr = decodeEasyQueryResult(strQueryResult_Name);
			  fm.all('EndFlagName').value= trim(arr[0][0]) ;
		  }
		 }
		 
	 // add new 
		 fm.all('ApplyGbFlag').value=arrResult[0][23];
		 fm.all('ApplyGbStartDate').value=arrResult[0][24];
		 fm.all('GbuildFlag').value=arrResult[0][25];
		 fm.all('GbuildStartDate').value=arrResult[0][26];
 	}   
}

function queryBranchCode()
{  
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select AgentGroup,UpBranch from LABranchGroup where 1=1  and (state<>'1' or state is null)"//and (EndFlag is null or EndFlag <> 'Y')
	  + getWherePart('BranchAttr')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');	 
	        
  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	
  if (!strQueryResult) {
    alert("不存在所要操作的销售单位！");
    fm.all("BranchAttr").value = '';
    fm.all('AgentGroup').value = '';
    fm.all('UpBranch').value = '';
    return false;
  }
  var arr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentGroup').value = arr[0][0];
  fm.all('UpBranch').value = arr[0][1];
  return true;
}

function queryAgent()
{
    var strSQL="";	
    strSQL="select AgentCode from LAAgent where 1=1 and (AgentState is null or AgentState < '03') "
            +getWherePart('AgentGroup','AgentGroup');	 
	        
    var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	
    if (strQueryResult) {
    	alert('该销售单位下还有业务员！');
    	return false;
    }
    strQueryResult = null;
    strSQL = "select AgentGroup from LABranchGroup where 1=1 and (EndFlag is null or EndFlag <> 'Y') and (state<>'1' or state is null)"
	  + getWherePart('UpBranch','AgentGroup')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');	 
	        
    strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
    if (strQueryResult) {
    	alert('该销售单位下还有下属机构！');
    	return false;
    }
    return true;
} 

function CheckBranchValid()
{
	if((fm.all('BranchAttr').value==null)||(fm.all('BranchAttr').value==""))
	{
		alert("机构代码不能为空！");
		return false;
	}
	var sql="select AgentGroup from LABranchGroup where 1 = 1 and (state<>'1' or state is null)" 
	        + getWherePart('BranchAttr','BranchAttr')
	        + getWherePart('BranchType')
	        + getWherePart('BranchType2') ;
	strQueryResult  = easyQueryVer3(sql, 1, 0, 1);  
    	if (strQueryResult) 
    	{
    		alert('该机构已经存在，只能更新或请先删除再录入！');
    		return false;
    	}
    return true;
}

function CheckDirect(){
	 fm.all('UpBranchAttr').disabled = false;
   if(fm.UpBranchAttr.value=='1'){
    		var sql = "select agentgroup,BranchLevel from LABranchGroup where BranchType='1' and BranchType2='01' and branchattr='"
    		+fm.UpBranchA.value+"'";
    		var urr = easyExecSql(sql);
    		if(!urr){
    			alert("上级机构信息查询失败！");
    			return false;
    		}
    		if(urr[0][1]!='02'){
    			alert("上级不是营业区，不能指定直辖处！");
    			return false;
    		}
    		sql="select agentgroup,branchattr,name from LABranchGroup where UpBranch='"
    		+urr[0][0]+"' and UpBranchAttr='1' and (EndFlag is null or EndFlag <> 'Y')";
    		zrr = easyExecSql(sql);
    		if(zrr){
    			if(zrr[0][0]!=fm.AgentGroup.value){
    				alert("上级营业区已有直辖处"+zrr[0][1]+zrr[0][2]+"，不能将该单位指定成直辖处！");
    				return false;
    			}
    		}
    	}
    	return true;
}
/*
function changeManager()
{
    if (getWherePart('AgentCode','BranchManager')=='')
      return false;
      
    var strSQL="";	
    strSQL="select AgentCode from LAAgent where 1=1 and (AgentState is null or AgentState < '03') "
            +getWherePart('AgentCode','BranchManager');	 
    //alert(strSQL);
    var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
	
    if (!strQueryResult) {
    	alert('所输代码无效！');
    	fm.all('BranchManager').value='';
    	return false;
    }
}*/

function changeGroup()
{
	if(fm.all('EndFlag').value == null || fm.all('EndFlag').value == "")
	{
		alert("停业属性为空，请置位！");
		return false;
	}
	if(fm.all('EndFlag').value == 'Y' && (fm.all('EndDate').value == null || fm.all('EndDate').value == ""))
  {
  	alert("停业属性为Y，请置停业日期！");
    return false;
  }
	if(fm.all('EndFlag').value == 'N' && fm.all('EndDate').value != "")
  {  
  	alert("停业属性为N，不能置停业日期！");
  	fm.all('EndDate').value = "";
    return false;
  }
  return true;
}

//根据管理机构级别代码取得级别名称
function getLevelName(pmBranchLevelNo)
{
	var tSQL = "";
	
	tSQL  = "select BranchLevelCode,BranchLevelName from LABranchLevel ";
  tSQL += "where 1=1 and branchtype='"+document.fm.BranchType.value;
  tSQL += "' and branchtype2='"+document.fm.BranchType2.value;
  tSQL += "' and branchlevelcode='"+pmBranchLevelNo+"'";
  
  var strQueryResult  = easyQueryVer3(tSQL, 1, 1, 1);
  if (!strQueryResult) 
  {
  	//alert('机构级别检索失败！');
		//fm.all('BranchAttr').value = '';
		//fm.all('UpBranchA').value = '';
		return false;
  }
  //把结果拆成数组
  var arr = decodeEasyQueryResult(strQueryResult);
  
  //设置机构级别名称
  document.fm.BranchLevelName.value = arr[0][1];
}

function getUpBranch()
{
	var tUpBranch = null;
	var tbranchattr = trim(fm.all('BranchAttr').value)
  	if(tbranchattr.length != 10 )
  	{
  		alert("机构编码长度不符合规定");
  		fm.all('BranchAttr').value = '';
  		fm.all('UpBranchA').value = '';
  		fm.all('UpBranch').value = '';
  		fm.all('UpBranchAName').value = '';
  		return false;
  	}
  	if(tbranchattr.length == 10)
  	{
  		fm.all('BranchLevel').value='03';
  		getLevelName('03');
  	}
  	
}

