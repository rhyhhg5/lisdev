<%
//程序名称：LAArchieveInput.jsp
//程序功能：
//创建日期：2005-03-20 18:05:58
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>

<SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>     
<script language="JavaScript">
var StrSql=" 1 and branchtype=#3# and branchtype2=#01#  AND GRADECODE<>#F00#";
</script>
                                      
<script language="JavaScript">

function initInpBox()
{ 
  try
  {         
      fm.all('ManageCom').value = "";
      fm.all('ManageComName').value = "";
      fm.all('AdjustDate').value ='2012-01-01';
  }
  catch(ex)
  {
    alert("LABankBasicWageInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initArchieveGrid();
  }
  catch(re)
  {
    alert("LAContFYCRateInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var ArchieveGrid;
function initArchieveGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();                                                 
    iArray[1][0]="agentgroup";         		//列名                         
    iArray[1][1]="0px";            		//列宽                             
    iArray[1][2]=200;            			//列最大值                           
    iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    
    iArray[2]=new Array();                                                 
    iArray[2][0]="2010营业组编码";         		//列名                             
    iArray[2][1]="90px";            		//列宽                             
    iArray[2][2]=200;            			//列最大值                           
    iArray[2][3]=0;                                                        
                                              
                                                                           
    iArray[3]=new Array();                                                 
    iArray[3][0]="2010营业组名称";         		//列名                         
    iArray[3][1]="200px";            		//列宽                             
    iArray[3][2]=200;            			//列最大值                           
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="迁移";          		        //列名
    iArray[4][1]="30px";      	      		//列宽         			//列最大值
    iArray[4][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[4][3]=1;   
    
    
    iArray[5]=new Array();
    iArray[5][0]="2011营业部编码";          		        //列名
    iArray[5][1]="90px";      	      		//列宽         			//列最大值
    iArray[5][2]=200;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    iArray[5][3]=1; 
    
    iArray[6]=new Array();                                                 
    iArray[6][0]="2011营业部名称";         		//列名                         
    iArray[6][1]="200px";            		//列宽                             
    iArray[6][2]=200;            			//列最大值                           
    iArray[6][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();                                                 
    iArray[7][0]="新成本中心编码";         		//列名                         
    iArray[7][1]="90px";            		//列宽                             
    iArray[7][2]=200;            			//列最大值                           
    iArray[7][3]=1;              			//是否允许输入,1表示允许，0表示不允许
    
  
    
  
    ArchieveGrid = new MulLineEnter( "fm" , "ArchieveGrid" ); 
    //这些属性必须在loadMulLine前

    ArchieveGrid.mulLineCount = 0;  
    ArchieveGrid.canChk = 0; 
    ArchieveGrid.displayTitle = 1;
    ArchieveGrid.hiddenPlus = 1;
    ArchieveGrid.hiddenSubtraction = 1;    

    ArchieveGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ArchieveGrid.setRowColData(1,1,"asdf");
    
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
