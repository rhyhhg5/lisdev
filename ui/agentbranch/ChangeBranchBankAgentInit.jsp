<%
//程序名称：ChangeBranchBankAgentInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
function initForm()
{
	try
	{
		initInpBox();
		initBankAgentGrid();
	}
	catch(re)
	{
		alert("在ChangeBranchBankAgentInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initInpBox()
{
	try
	{
		// fm.all('AdjustBranchCode').value = '';
		// fm.all('AdjustAfterBranchCode').value = '';
		// fm.all('NewBranchAttr').value = '';
		fm.all('AdjustDate').value = '';
	}
	catch(ex)
	{
		alert("在ChangeBranchBankAgentInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
	}
}
// 险种授权的初始化
function initBankAgentGrid()
{
	var iArray = new Array();
	try
	{
		iArray[0]=new Array();
		iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1]="30px"; 	           		//列宽
		iArray[0][2]=1;            			//列最大值
		iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

		iArray[1]=new Array();
		iArray[1][0]="业务员代码";
		iArray[1][1]="100px";
		iArray[1][2]=20;
		iArray[1][3]=0;
		//iArray[1][4]="RiskCode";
		//iArray[1][5]="1|2";
		//iArray[1][6]="0|1";

		iArray[2]=new Array();
		iArray[2][0]="业务员姓名";
		iArray[2][1]="160px";
		iArray[2][2]=20;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="业务职级";
		iArray[3][1]="80px";
		iArray[3][2]=10;
		iArray[3][3]=0;

		BankAgentGrid = new MulLineEnter( "fm" , "BankAgentGrid" );
		//这些属性必须在loadMulLine前
		BankAgentGrid.mulLineCount = 0;
		BankAgentGrid.displayTitle = 1;
		BankAgentGrid.hiddenPlus = 1;
    BankAgentGrid.hiddenSubtraction = 1;
		BankAgentGrid.locked=1;
		BankAgentGrid.canSel=0;
		BankAgentGrid.canChk=0;
		BankAgentGrid.loadMulLine(iArray);
	
	
	
	
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>
