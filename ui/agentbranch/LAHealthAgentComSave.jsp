<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LASpecComSave.jsp
//程序功能：
//创建日期：2003-09-17 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAInteractionSpecComBL tLAInteractionSpecComBL = new LAInteractionSpecComBL();
  LAComSchema tLAComSchema   = new LAComSchema(); 
  LAComToAgentSet tLAComToAgentSet=new LAComToAgentSet();
  LAContSchema tLAContSchema   = new LAContSchema();
  
  //输出参数
  CErrors mErrors = new CErrors();
  String tOperate = request.getParameter("hideOperate");
  tOperate = tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
   
  tG=(GlobalInput)session.getValue("GI");

  //代理机构,将数据存储在tLAComSchema中.
  tLAComSchema.setAgentCom(request.getParameter("AgentCom"));
  tLAComSchema.setName(request.getParameter("Name"));
  tLAComSchema.setACType(request.getParameter("ACTypeCode"));
  tLAComSchema.setChiefBusiness(request.getParameter("ChiefBusiness"));
  tLAComSchema.setProfits(request.getParameter("Profit"));
  tLAComSchema.setAssets(request.getParameter("Asset"));
  tLAComSchema.setProtocalNo(request.getParameter("ProtocalNo"));
  tLAComSchema.setCorporation(request.getParameter("Corporation"));
  tLAComSchema.setManageCom(request.getParameter("ManageCom"));
  System.out.println("@@@@@@@@@@@@:"+tLAComSchema.getManageCom());
  tLAComSchema.setACType(request.getParameter("ACType"));
  tLAComSchema.setSellFlag(request.getParameter("SellFlag"));
  tLAComSchema.setAddress(request.getParameter("Address"));
  tLAComSchema.setAgentOrganCode(request.getParameter("AgentOrganCode"));
  System.out.println("****:"+tLAComSchema.getAgentOrganCode());
  tLAComSchema.setOperator(tG.Operator);
  tLAComSchema.setAreaType("A");
  String tProtocolNo = request.getParameter("ProtocolNo");
  
  tLAComSchema.setAreaType(" ");
  if(!tOperate.equals("UPDATE||MAIN")){
    	tLAComSchema.setCrs_Check_Status("99"); //更新标志
    	tLAContSchema.setProtocolNo(tProtocolNo);
    	
    }else{
    	tLAComSchema.setCrs_Check_Status("00");//新增标志
    	tLAContSchema.setProtocolNo(tProtocolNo);	
  }
  tLAComSchema.setChannelType(request.getParameter("ChannelType"));
  tLAComSchema.setLicenseNo(request.getParameter("LicenseNo"));
  tLAComSchema.setBankAccNo(request.getParameter("BankAccNo"));

  tLAComSchema.setBankCode(request.getParameter("BankCode"));
  // 合作终止状态 Y无效 N有效
  tLAComSchema.setEndFlag(request.getParameter("EndFlag"));
  tLAComSchema.setEndDate(request.getParameter("EndDate"));
  
  tLAComSchema.setBranchType(request.getParameter("BranchType"));
  tLAComSchema.setBranchType2(request.getParameter("BranchType2"));
  tLAComSchema.setLicenseStartDate(request.getParameter("Licensestart"));
  tLAComSchema.setLicenseEndDate(request.getParameter("Licenseend"));
  tLAComSchema.setBankAccName(request.getParameter("BankAccName"));
  tLAComSchema.setBankAccOpen(request.getParameter("BankAccOpen"));

  tLAContSchema.setSignDate(request.getParameter("SignDate"));
  tLAContSchema.setManageCom(request.getParameter("ManageCom"));
  tLAContSchema.setAgentCom(request.getParameter("AgentCom"));
  tLAContSchema.setStartDate(request.getParameter("SignDate"));
  tLAContSchema.setEndDate(request.getParameter("ProEndDate"));
  tLAContSchema.setProtocolNo(request.getParameter("ProtocolNo"));
  tLAContSchema.setProtocolType("0");

  String SignDate1= request.getParameter("SignDate1");

  String mAgentCom="";
  String tAgentCode[] = request.getParameterValues("ComToAgentGrid1");
  String tAgentGroup[] = request.getParameterValues("ArchieveGrid3");
  String tAgentCode1[] = request.getParameterValues("ComToAgentGrid5");
  
  int length=0;
  if(null != tAgentCode)
  {
  	length=tAgentCode.length;
  }
  for (int i=0;i<length;i++)
  {
     LAComToAgentSchema tLAComToAgentSchema=new LAComToAgentSchema();
     LAComToAgentSchema temp=new LAComToAgentSchema();
     tLAComToAgentSchema.setAgentCom(request.getParameter("AgentCom"));
     tLAComToAgentSchema.setRelaType("1");
     System.out.println("tAgentCode[i]"+tAgentCode[i]);
  	 String tempsql = "select agentcode from laagent where groupagentcode = '"+tAgentCode[i]+"'";
	 String  tempAgentCode= new ExeSQL().getOneValue(tempsql);
     tLAComToAgentSchema.setAgentCode(tempAgentCode);
     tLAComToAgentSchema.setOperator(tAgentCode1[i]);
     tLAComToAgentSet.add(tLAComToAgentSchema);
  }
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tLAComSchema);
  tVData.add(tLAComToAgentSet);
  tVData.add(tLAContSchema);
  tVData.add(tG);
  
  //将数据传输到bl层.
  tVData.addElement(SignDate1);

  try
  { 
    if(!tLAInteractionSpecComBL.submitData(tVData,tOperate))
    {
          // @@错误处理
      mErrors.copyAllErrors(tLAInteractionSpecComBL.mErrors);
      CError tError = new CError();
      tError.moduleName = "LAInteractionSpecComSave";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      mErrors.addOneError(tError) ;
    }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    mErrors = tLAInteractionSpecComBL.mErrors;
    if (!mErrors.needDealError())
    {
      Content = " 保存成功! ";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 保存失败，原因是:" + mErrors.getFirstError();
      FlagStr = "Fail";
    }
  }
    //添加各种预处理

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mAgentCom%>");
</script>
</html>

