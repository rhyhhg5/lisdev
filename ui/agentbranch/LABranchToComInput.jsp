<%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="com.sinosoft.lis.pubfun.PubFun"%>
 
 <html>
 <%
 //团队机构调动
 //创建人：xiangchun
 //创建日期：2007-11-15
 %>

	<%
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		String tOperator = tG.Operator;
		String BranchType=request.getParameter("BranchType");
		String BranchType2=request.getParameter("BranchType2");
 		String CurrentDate= PubFun.getCurrentDate();   
	%>
   <script language="javascript">
      
 			 
   		 var manageCom = <%=tG.ManageCom%>;
       var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and length(trim(Branchattr))=10 " ;
   
    
   </script>	
  <%@page contentType="text/html;charset=GBK" %>
 <head >
    
     
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		
		<!--  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>-->
 
		<SCRIPT src="./LABranchToComInput.js"></SCRIPT>
		 
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="LABranchToComInit.jsp"%>
	 
		<%@include file="../common/jsp/ManageComLimit.jsp"%>
		<title></title>
	</head>
	
	<body  onload=" initForm();initElementtype();" >
	<form action="LABranchToComSave.jsp" method=post name=fm target="fraSubmit">
			<table >
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divChangeBranch);">
					</td>
					<td class=titleImg>被调整营业部信息</td>
				</tr>
			</table>
			<Div  id= "divChangeBranch" style= "display: ''">
	 <table  class= common>
	     <TR  class= common >
						<TD  class= title>被调整营业部代码</TD>
						<TD  class= input>
							<Input class=code name=AdjustBranchCode readonly 
							ondblclick="return showCodeList('branchattr',[this,AdjustBranchName],[0,1],null,msql,1);"  
							onkeyup   ="return showCodeListKey('branchattr',[this,AdjustBranchName],[0,1],null,msql,1);"  
             verify="调整机构代码|notnull"  elementtype=nacessary>
						</TD>
						<TD  class= title>营业部名称</TD>
						<TD  class= input>
							<Input class=readonly readonly name=AdjustBranchName>
						</TD>
	     </TR>
	     
       <TR  class= common>
						<TD  class= title>营业部管理人员</TD>
						<TD  class= input>
							<Input class=readonly readonly name=BranchManager>
						</TD>
						<TD  class= title>营业部管理人员姓名</TD>
						<TD  class= input>
							<Input class=readonly readonly name=BranchManagerName>
						</TD>
	      </TR>     
	       <TR  class= common>
          <TD  class= title>
           所属管理机构
          </TD>
          <TD  class= input>
            <Input class=readonly name=ManageCom   readOnly
             
             > 
          </TD> 		
        </tr>
      
			<table  >
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAimChangeBranch);">
						<td class=titleImg>
							目标机构信息
						</td>
					</td>
				</tr>
			</table>	
			<Div  id= "divAimChangeBranch" style= "display: ''">
			<table class= common>
			 <TR  class= common>
          <TD  class= title>
           新管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=NewManageCom verify="新管理机构|code:comcode&NOTNULL&len=8" 
            ondblclick="return showCodeList('comcode',[this,NewManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,NewManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class=codename name=NewManageComName readOnly elementtype=nacessary> 
          </TD> 		
           
			  	<TD  class= title>调动后机构代码</TD>
						<TD  class= input>
							<Input class=readonly  name=NewBranchAttr readonly>
						</TD>
        </tr>
        <TR  class= common>
        	<TD  class= title>调整日期</TD>
					<TD  class= input >
							<Input class='coolDatePicker' dateformat= 'short' name=AdjustDate verify="调整日期|notnull&Date" elementtype=nacessary>
					</TD>
        </tr>
				<TR  class= common>
						<TD  class= title>操作日期</TD>
						<TD  class= input>
							<Input class=readonly readonly name=MakeTime >
						</TD>
						<TD  class= title>操作员代码</TD>
						<TD  class= input>
							<Input class="readonly" readonly name=Operator><Input type=hidden name=optname>
						</TD>
				</TR>        
      <table>	   
      	<br>
				<!--input type=button class=common name=savea value='确认' onclick="return BranchConfirm()"-->
				<input type=button class=cssButton name=saveb value='保存' onclick="return submitSave()">
				<input type=button class=cssButton value='重置' onclick="clearGrid();">
			</div>
			<input type=hidden name=UpBranchAttr value=''>
			<input type=hidden name=hideAdjustAgentGroup value=''>
			<input type=hidden name=hideAimAgentGroup value=''>
			<input type=hidden name=hideLevel value=''>
			<input type=hidden name=hideCom value=''>
			<input type=hidden name=hideUpBranch value=''>
			<input type=hidden name=AdjustAfterAgentGroup value=''>
			<input type=hidden name=AdjustAfterLevel value=''>
			<input type=hidden name=AdjustAfterAttr value=''>
			<input type=hidden name=BranchType value=''>
			<input type=hidden name=BranchType2 value=''>
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
       