 <%
//程序名称：AdjustAgentInit.jsp
//程序功能：
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">                                

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox(); 
    initAgentGrid();    
  }
  catch(re)
  {
    alert("AdjustAgentInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initInpBox()
{ 

  try
  {                                   
    fm.all('BranchCode').value = '';
    fm.all('BranchName').value = '';
    fm.all('BranchManager').value = '';
    fm.all('ManagerName').value = '';
    fm.all('AgentGroup').value = '';
    fm.all('AgentGroupName').value = '';
    fm.all('AgentGroupManager').value = '';
    fm.all('AgentGroupManagerName').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('hideAgentCode').value = '';
    fm.all('hideAgentGroup').value = '';
    fm.all('AdjustDate').value = '';
    fm.all('BranchType').value='<%=BranchType%>';
    fm.all('BranchType2').value='<%=BranchType2%>';
  }
  catch(ex)
  {
    alert("在AdjustAgentInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 险种授权的初始化
function initAgentGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="业务员代码" ;          		//列名
      iArray[1][1]="100px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
      //iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
      //iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
      //iArray[1][6]="0|1";              	        //上面的列中放置引用代码中第几位值


      iArray[2]=new Array();
      iArray[2][0]="业务员名称";         			//列名
      iArray[2][1]="80px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="展业机构代码";      	   		//列名
      iArray[3][1]="150px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
    
      
      iArray[4]=new Array();
      iArray[4][0]="业务员职级";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=10;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="本方业务员代码";      	   		//列名
      iArray[5][1]="0px";            			//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      AgentGrid = new MulLineEnter( "fm" , "AgentGrid" ); 
      //这些属性必须在loadMulLine前
      AgentGrid.mulLineCount = 0;   
      AgentGrid.displayTitle = 1;
      AgentGrid.locked=1;   
      AgentGrid.canSel=0;
      AgentGrid.canChk=1;  
      AgentGrid.hiddenPlus = 1;
      AgentGrid.hiddenSubtraction = 1;
      AgentGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>
