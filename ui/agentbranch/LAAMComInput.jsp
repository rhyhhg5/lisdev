<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LASpecComInput.jsp
//程序功能：
//创建日期：2003-09-16
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println(BranchType+" "+BranchType2);
%>
<script> 
   var BranchType=<%=BranchType%>;
   var BranchType2=<%=BranchType2%>;
   var msql='1 and code <> #01# ';
   var strsql="1 and codealias= #"+'2'+"#";
</script>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LAAMComInput.js"></SCRIPT>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LAAMComInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAMComSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLASpecCom1);"></td>
        <td class=titleImg>中介机构信息</td> 
      </tr>
    </table>
  <Div  id= "divLASpecCom1" style= "display: ''"> 
    <table  class= common>
     <tr> 
     <TD class = title>
             渠道类型
          </TD>
          <TD  class= input>
            <Input class='codeno' name=BranchType2 
             ondblclick="return showCodeList('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);"
             onkeyup="return showCodeListKey('BranchType2',[this,BranchType2Name],[0,1],null,strsql,1);" 
              verify="渠道类型|notnull&code:BranchType2" 
             ><Input class=codename name=BranchType2Name readOnly elementtype=nacessary>
          </TD>
           </tr>  
      <tr  class= common> 
        <td  class= title> 中介机构编码 </td>
        <td  class= input> <input class= 'readonly' readonly  name=AgentCom> </td>
        <td  class= title> 中介机构名称 </td>
        <td  class= input> 
        	<input class=common name=Name elementtype=nacessary verify="中介机构名称|NOTNULL" > 
        </td>
      </tr>
      
      <tr  class= common> 
        <TD  class= title>
            所属管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>=4" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
            ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
          </TD> 
        <td  class= title> 中介机构类型 </td>  
        <td  class= code> <input class="codeno" name="ACType" verify="中介机构类型|NOTNULL&code:ACType"
        	   ondblclick="return showCodeList('ACType',[this,ACTypeName],[0,1],null,msql,1);"
        	   onkeyup="return showCodeListKey('ACType',[this,ACTypeName],[0,1],null,msql,1);"
        	   ><Input class=codename name=ACTypeName readOnly elementtype=nacessary>
        </td>
       
      </tr>
      
      <tr class = common >
       <td  class= title> 注册资金 </td>
        <td  class= input> <input  class= common name=Asset verify="注册资金|num"> 单位:万元 </td>
         <td  class= title> 开业时间 </td>
         <td  class= input> 
           <input class=coolDatePicker name=ChiefBusiness elementtype=nacessary verify="开业时间|NOTNULL&Date" format='short'>
         </td>
      </tr>
      
      <tr  class= common> 
       <td  class= title> 去年手续费收入 </td>
         <td  class= input> <input class= common name=Profit verify="去年手续费收入|num"> 单位:万元</td>
        <td  class= title> 法人代表 </td>
        <td  class= input> <input  class= common name=Corporation elementtype=nacessary verify="法人代表|NOTNULL"> </td>
	      
      </tr>  
      
      <tr class=common>
      	<td  class= title> 销售资格 </td>
        <td  class= input> <input  name=SellFlag class= 'codeno'   verify="销售资格|NOTNULL&code:YesNo"
		           ondblclick="return showCodeList('YesNo',[this,SellFlagName],[0,1]);" 
		           onkeyup="return showCodeListKey('YesNo',[this,SellFlagName],[0,1]);"
		           ><Input class=codename name=SellFlagName readOnly elementtype=nacessary> 
		    </td>
        <td  class= title> 业务许可证号 </td>
        <td  class= input> <input class=common name=LicenseNo elementtype=nacessary verify="业务许可证号|notnull"> </td>
      </tr>
      <tr  class= common>
      <td  class= title> 业务许可证有效起期 </td>
         <td  class= input> 
           <input class=coolDatePicker name=Licensestart elementtype=nacessary verify="业务许可证有效起期|NOTNULL&Date" format='short'>
         </td>
      <td  class= title> 业务许可证有效止期 </td>
         <td  class= input> 
           <input class=coolDatePicker name=Licenseend elementtype=nacessary verify="业务许可证有效止期|NOTNULL&Date" format='short'>
         </td>
      </tr>
      <tr class=common>
        <td class=title>开户行</td>
        <td  class= input> 
        	<input class=common name=BankAccOpen > 
        </td>
        <td class=title>机构账户名称</td>
        <td  class= input> 
        	<input class=common name=BankAccName > 
        </td>
        </tr>
      <tr  class= common>
        <td  class= title> 机构帐户 </td>
        <td  class= input> <input class=common name=BankAccNo elementtype=nacessary verify="机构帐户|notnull&num"> </td>
	      <td  class= title> 机构帐户确认 </td>
        <td  class= input> <input class=common name=BankAccNoInsure verify="机构帐户确认|notnull&NUM" elementtype=nacessary> </td> 
      </tr>
      <tr>
         <td  class= title> SAP供应商编号 </td>
         <td  class= input> 
           <input  class= common name=BankCode verify="SAP供应商编号|len=10&notnull&num" elementtype=nacessary> </td>
         </td>
          <TD  class= title>
            合作状态
          </TD>
          <TD  class= input>
            <Input class='codeno' name=CalFlag verify="状态|code:yesno&NOTNULL" CodeData = "0|^Y|有效|^N|无效"
             ondblclick="return showCodeListEx('CalFlag',[this,CalFlagName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('CalFlag',[this,CalFlagName],[0,1]);"
             ><Input class=codename name=CalFlagName readOnly elementtype=nacessary>
          </TD>
      </tr> 
     <tr class=common>
        <td  class= title> 中介机构营业场所地址 </td>
        <td  class= input> <input class=common name=Address > </td>
       <td class=title>中介组织机构编码</td>
        <td  class= input> 
        	<input class=common name=AgentOrganCode > 
        </td>
      </tr>
    </table>
  </Div>
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLASpecCom2);"></td>
        <td class=titleImg>中介业务专员信息</td> 
      </tr>
    </table>
    
    <Div  id= "divComToAgent" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanComToAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    </Div>
  <input type=hidden name=ChannelType value='E'>
  <input type=hidden name=hideOperate value=''>
  <input type=hidden name=ACTypeCode value=''>
  <input type=hidden name=HiddenAgentGroup value=''>
  <input type=hidden name=HiddenBankCode value=''>
  <input type=hidden name=BranchType value=''>
  </form>

  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>

</body>
</html>


<script>
   var bcodeSql = "1 and LABranchGroup.BranchType=#"+BranchType+"# and LABranchGroup.BranchType2=#"+BranchType2+"# and EndFlag != #Y# and ManageCom like #" + fm.all('ManageCom').value + "%#"
</script>
