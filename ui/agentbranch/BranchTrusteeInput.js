 //               该文件中包含客户端需要处理的函数和事件
   
var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var mOperate="";
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
	
	if (mOperate=="")
  {
  	addClick();
  }

	//alert(11);
	if(!beforeSubmit())
 {
 	return false;
 	}

 	fm.all("hideOperate").value=mOperate;
 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
 
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{       
	
	
	fm.reset();
	 fm.all('BranchAttr').readOnly=false;
	
	// document.all.AgentOld.readOnly=false;
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

  }
  initForm();
  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  
} 

//取消按钮对应操作
function cancelForm()
{

}
 
 
 	

	

 	
//提交前的校验、计算  
function beforeSubmit()
{
	
  //添加操作
  if(!getTimeDistance())
 {
 	return false;
 	}
 

 if(!verifyInput())
 {
 	return false;
 }	
 if(fm.all('StartDate').value!=null  && fm.all('StartDate').value!="")
{ 
 if(!CheckStartDate())
 {
    return false;
 	}
}
 
//   var arrReturn = new Array();
//	var tSel = TrusteeGrid.getSelNo();
	
	
//		
//       if( tSel == 0 || tSel == null )
		//top.close();
			
   if((fm.all('AgentGroup').value=='') || (fm.all('AgentGroup').value==null ) ) 		
       {
          alert( "请先查询并选择一条记录，再进行此操作。" );
	
            return false ;
        }
if(fm.all('EndDate').value!=null  && fm.all('EndDate').value!="")
{        
    if(!CheckEndDate())
 {
    return false;
 	}    
}

  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  //showDiv(operateButton,"false"); 
  //showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	
   if(!beforeSubmit())
 {
 	return false;
 	}
  else
  {
  if (confirm("您确实想修改该记录吗?"))
  {
    mOperate="UPDATE||MAIN";
    
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了修改操作！");
  }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
	 //下面增加相应的代码
  //mOperate="QUERY||MAIN";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  showInfo=window.open("./BranchTrusteeQueryHtml.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
 
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function easyQueryClick()
{
	
 if ((fm.all('StartDate1').value != '')&&(fm.all('StartDate1').value!= null)&&(fm.all('EndDate1').value != '')&&(fm.all('EndDate1').value!= null))	
{
	if(fm.all('StartDate1').value>=fm.all('EndDate1').value)
	{
	 alert("查询止期应大于查询起期");
	 return false;
         }
        
}	

  initTrusteeGrid();	    
	var strSql = "select b.BranchAttr,b.name,b.branchmanager,b.branchmanagername,(select agentgrade from latree c where c.agentcode=b.branchmanager),a.Idx,a.leavedate,a.ShouldEndDate,a.enddate,a.agentgroup from lahols a,labranchgroup b where 1=1 "
	+" and a.agentgroup=b.agentgroup  and a.agentcode=b.branchManager"
    + getWherePart("b.ManageCom", "ManageCom1")
    + getWherePart("b.BranchAttr", "BranchAttr1")
    + getWherePart("b.name", "BranchName1")
    + getWherePart("BranchManager", "BranchManager1")
    + getWherePart("BranchManagerName", "BranchManagerName1")
      + getWherePart("a.BranchType", "BranchType")
    + getWherePart("a.BranchType2", "BranchType2")
  ;
  
  //alert(strSql);
 //turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1); 	
 
 
   if ((fm.all('StartDate1').value != '')&&(fm.all('StartDate1').value != null))
   {
   	
    strSql+=" and a.leavedate>=	'"+fm.all('StartDate1').value+"'  ";
   }	
   if ((fm.all('EndDate1').value != '')&&(fm.all('EndDate1').value != null))
   {
    strSql+="  AND a.ShouldEndDate<='"+fm.all('EndDate1').value+"' ";
   }
  
	turnPage.queryModal(strSql, TrusteeGrid);
	 //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }		
}

function getTimeDistance()
{
	
 if ((fm.all('StartDate').value != '')&&(fm.all('StartDate').value!= null)&& (fm.all('EndDate').value != '')&&(fm.all('EndDate').value!= null))
 {
    //fm.all('TimeDistance').value= fm.all('EndDate').value-fm.all('StartDate').value;	
 }  	
 
  return true;
}


function showOne()
{
 var arrReturn = new Array();
 var tRow = TrusteeGrid.getSelNo();
 if( tRow == 0 || tRow == null )
	    return arrSelected;		
fm.all('BranchAttr').value=TrusteeGrid.getRowColData(tRow-1,1);	
fm.all('BranchManager').value=TrusteeGrid.getRowColData(tRow-1,3);	
fm.all('BranchManagerName').value=TrusteeGrid.getRowColData(tRow-1,4);	
fm.all('Idx').value=TrusteeGrid.getRowColData(tRow-1,6);	
fm.all('AgentGroup').value=TrusteeGrid.getRowColData(tRow-1,10);	
//alert(fm.all('AgentGroup').value);
	
}

function checkBranchGroup()
{
    // alert(tArr1[0][1]);  
 if(fm.all('BranchAttr').value!=null && fm.all('BranchAttr').value!="")
 {	
  var strsql="select name,Branchmanager,(select name from laagent where agentcode=labranchgroup.Branchmanager)"
   +" from labranchgroup  where 1=1  "
   + getWherePart("BranchAttr", "BranchAttr")
    + getWherePart("BranchType", "BranchType")
    + getWherePart("BranchType2", "BranchType2");
   var strQueryResult = easyQueryVer3(strsql, 1, 0, 1);
  //判断是否查询成功
  //alert(strQueryResult);
   if (!strQueryResult) 
    {
    alert("无此机构！");
    //document.fm.AgentCode.value="";
    document.fm.BranchAttr.value = "";
    document.fm.BranchManager.value  = "";  
     document.fm.BranchName.value  = ""; 
    document.fm.BranchManagerName.value  = "";
     document.fm.Idx.value  = "";
    return;
     } 	
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('BranchName').value=tArr[0][0];
    fm.all('BranchManager').value=tArr[0][1];
    fm.all('BranchManagerName').value=tArr[0][2];
		
  var sql_1="select max(idx) from lahols a,labranchgroup b  where  a.leavestate='0' and a.agentcode=b.branchmanager "
  + getWherePart("b.BranchAttr", "BranchAttr")
   + getWherePart("a.BranchType", "BranchType")
    + getWherePart("a.BranchType2", "BranchType2");
   
   var strQueryResult2 = easyQueryVer3(sql_1, 1, 0, 1);
   var tArr2 = new Array();
   tArr2 = decodeEasyQueryResult(strQueryResult2);
  // alert(tArr2[0][0]);
   if (tArr2[0][0]=='0') 
    {
    alert("此机构的主管没有请假");  
      document.fm.BranchAttr.value = "";
    document.fm.BranchManager.value  = "";  
    document.fm.BranchName.value  = ""; 
    document.fm.BranchManagerName.value  = "";
    document.fm.Idx.value  = "";
    return;
     } 	 	
	
  var sql="select a.idx,a.LeaveDate,b.agentgroup from lahols a,labranchgroup b  where  a.leavestate='0' and a.agentcode=b.branchmanager "
        +" and a.idx="+tArr2[0][0]+" "
  + getWherePart("b.BranchAttr", "BranchAttr")
   + getWherePart("a.BranchType", "BranchType")
    + getWherePart("a.BranchType2", "BranchType2");
   
   var strQueryResult1 = easyQueryVer3(sql, 1, 0, 1);
 // alert(sql);
   
  //判断是否查询成功
   if (!strQueryResult1) 
    {
    alert("此机构的主管没有请假");  
      document.fm.BranchAttr.value = "";
    document.fm.BranchManager.value  = "";  
    document.fm.BranchManagerName.value  = "";
    document.fm.BranchName.value  = ""; 
    document.fm.Idx.value  = "";
    fm.all('AgentGroup').value="";
    fm.all('StartDate').value="";
    return;
     } 	 
      var tArr1 = new Array();
   tArr1 = decodeEasyQueryResult(strQueryResult1);
   fm.all('Idx').value=tArr1[0][0];    
   fm.all('StartDate').value=tArr1[0][1];   
    fm.all('AgentGroup').value=tArr1[0][2];   

    
 }   	
}	

function checkAgentCode()
{
  var sql="select * from lahols a where  leavestate='0'"
  + getWherePart("a.AgentCode", "AgentCode")
   + getWherePart("a.BranchType", "BranchType")
   + getWherePart("a.BranchType2", "BranchType2");
   var strQueryResult1 = easyQueryVer3(sql, 1, 0, 1);
  //alert(sql)
    if (strQueryResult1) 
    {
    alert("该主管已经请假!");  
    fm.all('AgentCode').value="";
    return  false;
     } 	 
	
var strsql="select a.name,b.agentgrade from laagent a,latree b where a.agentcode=b.agentcode  and a.agentstate<'03'"
   + getWherePart("a.AgentCode", "AgentCode")
    + getWherePart("a.BranchType", "BranchType")
    + getWherePart("a.BranchType2", "BranchType2");
   
  var strQueryResult = easyQueryVer3(strsql, 1, 0, 1);
  //判断是否查询成功
  if (!strQueryResult) {
    alert("无此业务员！");
    //document.fm.AgentCode.value="";
    document.fm.AgentCode.value = "";
    document.fm.AgentName.value  = "";
    return;
  }
 
  //查询成功则拆分字符串，返回二维数组
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
   if(tArr[0][1]<'B01')
  {
    alert("托管人职级应该是主管级别！");
    //document.fm.AgentCode.value="";
    document.fm.AgentCode.value = "";
    document.fm.AgentName.value  = ""; 
    fm.all('AgentGrade').value= ""; 
    return;
  }
  fm.all('AgentName').value=tArr[0][0];
   fm.all('AgentGrade').value=tArr[0][1];
  
  //设置姓名	
}	
function CheckStartDate()
{
	
  var sql="select leavedate from lahols a where agentcode='"+fm.all('BranchManager').value+"'  and idx="+fm.all('Idx').value+"  "
   + getWherePart("a.BranchType", "BranchType")
   + getWherePart("a.BranchType2", "BranchType2");
   var strQueryResult1 = easyQueryVer3(sql, 1, 0, 1);
  //alert(sql)
    if (!strQueryResult1) 
    {
    alert("查询请假信息出错");  
    fm.all('StartDate').value="";
    return  false;
     } 	 
     var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult1);	
  if(fm.all('StartDate').value<tArr[0][0])
  {
   alert("托管起期必须大于请假假日期");  
    fm.all('StartDate').value="";
    return  false;
  }	
  return  true;
}
function CheckEndDate()
{
   
  var sql="select enddate from lahols a  where agentcode='"+fm.all('BranchManager').value+"'  and idx="+fm.all('Idx').value+" and leavestate='1' "
   + getWherePart("a.BranchType", "BranchType")
    + getWherePart("a.BranchType2", "BranchType2");
   
   var strQueryResult1 = easyQueryVer3(sql, 1, 0, 1);
 // alert(sql);
   
  //判断是否查询成功
   if (!strQueryResult1) 
    {
    alert("此机构的主管请假还没有销假,不能录入托管止期");  
    fm.all('EndDate').value="";
    return  false;
     } 	 
     var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult1);	
  if(fm.all('EndDate').value>tArr[0][0])
  {
   alert("托管止期必须小于销假日期");  
    fm.all('EndDate').value="";
    return  false;
  }	
   return  true;
}		
function getQueryResult()
{
	
	var arrSelected = null;
	tRow = LGAppealGrid.getSelNo();
	//alert("111" + tRow);
	//edit by guo xiang at 2004-9-13 17:54
	
	if( tRow == 0 || tRow == null )
	    return arrSelected;
	
	arrSelected = new Array();
	
	//设置需要返回的数组
	//edit by guo xiang at 2004-9-13 17:54
	arrSelected[0] = new Array();
	
	arrSelected[0] = LGAppealGrid.getRowData(tRow-1);
	//arrSelected[0] = arrDataSet[tRow-1];
	
	return arrSelected;
}  




function afterQuery(arrQueryResult)
{	
	
	
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
	 arrResult=arrQueryResult;
	 fm.all('ManageCom').value=arrResult[0][0];
	 fm.all('BranchAttr').value=arrResult[0][1];
	 fm.all('BranchManager').value=arrResult[0][3];
	 fm.all('BranchManagerName').value=arrResult[0][4];
	 fm.all('Idx').value=arrResult[0][5];
	 fm.all('AgentCode').value=arrResult[0][6];
	  fm.all('UpAgentCode').value=arrResult[0][6]; //为了修改主键
	 fm.all('AgentName').value=arrResult[0][7];
	 fm.all('StartDate').value=arrResult[0][8];
	 fm.all('EndDate').value=arrResult[0][9];
	 fm.all('TimeDistance').value=arrResult[0][10];
	 fm.all('Operator').value=arrResult[0][12];
	 fm.all('AgentGroup').value=arrResult[0][11]; 
	 
	 fm.all('IdxNo').value=arrResult[0][13];
	  fm.all('UpIdxNo').value=arrResult[0][13];//为了修改主键
	 fm.all('BranchAttr').readOnly=true;	
	 		
			
       var strSql = "select agentgrade from latree where agentcode='"+fm.all('AgentCode').value+"' ";
       var strQueryResult_Name  = easyQueryVer3(strSql, 1, 1, 1);
     //  alert(strQueryResult_Name);
		  if (strQueryResult_Name)
			{
			
				var arr = decodeEasyQueryResult(strQueryResult_Name);
			  fm.all('AgentGrade').value= trim(arr[0][0]) ;
		  }
       var strSql = "select name from ldcom where comcode='"+fm.all('ManageCom').value+"' ";
       var strQueryResult_Name  = easyQueryVer3(strSql, 1, 1, 1);
     //  alert(strQueryResult_Name);
		  if (strQueryResult_Name)
			{
			
				var arr = decodeEasyQueryResult(strQueryResult_Name);
			  fm.all('ManageComName').value= trim(arr[0][0]) ;
		  }	  
		  
		  
		 }                                                
   
    }	
 	




