<%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<html>
	<%
	//程序名称：ChangeBranchInput.jsp
	//程序功能：
	//创建日期：
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
		GlobalInput tG = new GlobalInput();
		tG=(GlobalInput)session.getValue("GI");
		String tOperator = tG.Operator;
		System.out.println("tOperator"+tOperator);
		String BranchType=request.getParameter("BranchType");
		String BranchType2=request.getParameter("BranchType2");
 		String CurrentDate=  PubFun.getCurrentDate();  
	%>
		<%@page contentType="text/html;charset=GBK" %>
   <script language="javascript">
   		function initDate(){
//   			fm.AdjustDate.value="<%=CurrentDate%>";
   			fm.Operator.value="<%=tOperator%>";
   			
				fm.all('BranchType').value ='<%=BranchType%>';
				fm.all('BranchType2').value='<%=BranchType2%>'
   			
   		}
   </script>
 
	<head >
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<!--  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>-->
		<SCRIPT src="BankChangeComInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="BankChangeComInit.jsp"%>
		<%@include file="../agent/SetBranchType.jsp"%>
		<%@include file="../common/jsp/ManageComLimit.jsp"%>
		<title></title>
	</head>
	<body  onload="initDate();initForm();initElementtype();" >
		<form action="./BankChangeComSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divChangeBranch);">
					</td>
					<td class=titleImg>网点原有信息</td>
				</tr>
			</table>
			<Div  id= "divChangeBranch" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD  class= title>银行机构代码</TD>
						<TD  class= input>
						<Input class=common name=AgentOldCom   verify="银行机构代码|NOTNULL" 
               onchange="return getAgentComMs();" elementtype=nacessary>
     
						</TD>
						<TD  class= title>银行机构名称</TD>
						<TD  class= input>
							<Input class=readonly readonly name=AgentComName>
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>级        别</TD>
						<TD  class= input>
							<Input class=readonly readonly name=BankType>
						</TD>
						<TD  class= title>销  售 资 格</TD>
						<TD  class= input>
							<Input class=readonly readonly name=SellFlag>
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>上级机构代码</TD>
						<TD  class= input>
							<Input class=readonly readonly name=UpAgentCom>
						</TD>
						<TD  class= title>上级机构名称</TD>
						<TD  class= input>
							<Input class=readonly readonly name=UpAgentComName>
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title>原负责人代码</TD>
						<TD  class= input>
							<Input class=readonly readonly name=OldAgentCode>
						</TD>
						<TD  class= title>原负责人姓名</TD>
						<TD  class= input>
							<Input class=readonly readonly name=OldAgentName>
						</TD>
					</TR>
						</TR>
					<TR  class= common>
						<TD  class= title>原销售团队代码</TD>
						<TD  class= input>
							<Input class=readonly readonly name=OldAgentGroup>
						</TD>
						<TD  class= title>原销售团队名称</TD>
						<TD  class= input>
							<Input class=readonly readonly name=OldAgentGroupName>
						</TD>
					</TR>
			   <TR>
						<TD  class= title >
							原管理机构
						</TD>
					<TD  class= input>
							<Input class=readonly readonly name=OldManageCom>
								</TD>
					</TR>
				</table>
			</div>

			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAimChangeBranch);">
						<td class=titleImg>
							网点新信息
						</td>
					</td>
				</tr>
			</table>
			<Div  id= "divAimChangeBranch" style= "display: ''">
				<table  class= common>
					<TR  class= common>
						<TD class= title>新负责人代码</TD>
						<TD  class= input>
							<Input class=common name=NewAgentCode  
							 onchange="return getAgentMs();" 
							  verify="新负责人代码|notnull"   elementtype=nacessary>
						</TD>
						<TD class= title>新负责人名称</TD>
						<TD  class= input>
							<Input class=readonly readonly name=NewAgentName>
						</TD>
					</tr>
					<TR  class= common>
						<TD class= title>新销售团队代码</TD>
						<TD  class= input>
							<Input class=readonly readonly name=NewAgentGroup>
						</TD>
						<TD class= title>新销售团队名称</TD>
						<TD  class= input>
							<Input class=readonly readonly name=NewAgentGroupName>
						</TD>
					</tr>
						<TR  class= common>
						<TD  class= title>新管理机构</TD>
						<TD  class= input>
							<Input class=readonly readonly name=NewManageCom >
						</TD>
						<TD></TD>
						
					</TR>
					
					<TR  class= common>
						<TD  class= title>调整日期</TD>
						<TD  class= input >
							<Input class='coolDatePicker' dateformat= 'short' name=AdjustDate verify="调整日期|notnull&Date"   elementtype=nacessary>


						</TD>
						<TD  class= title>操作员代码</TD>
						<TD  class= input>
							<Input class="readonly" readonly name=Operator>
						</TD>
					</TR>
					
					
				</table>
				<br>
       <input type=button class=cssButton name=saveb value='保存' onclick="return submitSave()">
				<input type=button class=cssButton value='重置' onclick="clearGrid();">
			</div>
      <input type=hidden name=RelaType value=''>
      <input type=hidden name=MakeDate value=''>
      <input type=hidden name=MakeTime value=''>
      <input type=hidden name=ModifyDate value=''>
      <input type=hidden name=ModifyTime value=''>
			<input type=hidden name=StartDate value=''>
			<input type=hidden name=EndDate value=''>
			<input type=hidden name=OldOperator value=''>
			<input type=hidden name=BranchType value=''>
			<input type=hidden name=BranchType2 value=''>
			
		</form>
		<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
