 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var agentError = "";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
   if (verifyInput() == false)
    return false;
 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//取消按钮对应操作
function cancelForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
   var rowNum = AgentGrid.mulLineCount;
   var iCount = 0;
   var selFlag = true;
   var newBranchAttr = fm.all('BranchCode').value ;
   for (i=0;i<rowNum;i++)
   {
   	 if( AgentGrid.getSelNo(i))
   	 {
   	   iCount++;
   	   var oldBranchAttr = AgentGrid.getRowColData(i,3) ;
   	   var strSql = " select * from labranchgroup where branchtype='"
   	                +fm.all('BranchType').value
   	                +"' and branchtype2='"
   	                +fm.all('BranchType2').value   	                
   	                +"' and branchattr='"
   	                +oldBranchAttr
   	                +"' and managecom in (select managecom from labranchgroup where branchattr='"
   	                +newBranchAttr
   	                +"' and branchtype='"
   	                +fm.all('BranchType').value
   	                +"' and branchtype2='"
   	                +fm.all('BranchType2').value
   	                +"') with ur " ;
       var arrResult = easyExecSql(strSql);
       if(!arrResult)
       {
         selFlag = false;
         agentError = "不能在不同管理机构之间调整代理人!" ;
       }
   	   break;
   	 }
   }
   return selFlag;

}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{

}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//验证字段的值
function BranchConfirm()
{
	var tReturn = parseManageComLimitlike();
  if((trim(fm.all('BranchCode').value)=="")||(fm.all('BranchCode').value==null))
  {
    alert("请输入目标展业机构代码！");
    fm.all('BranchCode').focus();
    return false;
  }
  var strSQL = "";

  //查询出非停业且展业级别在营业组以上的
  strSQL = "select BranchAttr,Name,BranchType,getUniteCode(BranchManager),BranchLevel,UpBranch,AgentGroup,Branchmanagername,BranchType2 from LABranchGroup where 1=1 "//xijh增加Branchmanagername
         + tReturn
	     +" and BranchAttr = '"+trim(fm.all('BranchCode').value)+"' and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null)"
	     +getWherePart('BranchType','BranchType')
	     +getWherePart('BranchType2','BranchType2');

  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!strQueryResult) {
  	//查询失败
    alert("该展业机构无效！");
    fm.all('BranchCode').value="";
    fm.all('BranchName').value = "";
    fm.all('BranchManager').value = "";
    fm.all('ManagerName').value ="";
    fm.all('hideAgentCode').value = "";
    fm.all('hideBranchLevel').value = "";
    fm.all('hideUpBranch').value = "";
    fm.all('hideAgentGroup').value = "";
    return false;
  }

  var arr = decodeEasyQueryResult(strQueryResult);
  
  if ((trim(arr[0][2])=='1')&&(trim(arr[0][8])=='01') && (trim(arr[0][4])!='01'))//xjh修改，保留原有银代部分，去掉个险部分，因为个险部分已经将调整范围扩大
  {
  	//判定展业机构类型
    alert('业务员只能在营业组间调动！');
    fm.all('BranchCode').value="";
    fm.all('BranchName').value = "";
    fm.all('BranchManager').value = "";
    fm.all('ManagerName').value ="";
    fm.all('hideAgentCode').value = "";
    fm.all('hideBranchLevel').value = "";
    fm.all('hideUpBranch').value = "";
    fm.all('hideAgentGroup').value = "";
    return false;

  }
  fm.all('BranchName').value = arr[0][1];
  fm.all('BranchType').value = arr[0][2];
  fm.all('BranchManager').value = arr[0][3];//xjh增加，显示机构管理人员代码 
  fm.all('ManagerName').value = arr[0][7];//xjh增加，显示机构管理人名称
  fm.all('hideBranchLevel').value = arr[0][4]; 
  fm.all('hideUpBranch').value = arr[0][5];
  fm.all('hideAgentGroup').value = arr[0][6];
 
  if(arr[0][2]=='2'){
  	return true;
  }
  
  if((arr[0][3]==null)||(trim(arr[0][3])==''))
  {
    alert('请先确定目标机构的管理人员！');
  	fm.all('BranchManager').value = '';
  	fm.all('hideAgentCode').value = '';
  	fm.all('BranchManager').focus();
  	return false;
  }
  else
  {
  	strSQL = "select Name,groupAgentCode from LAAgent where groupAgentCode = '"+trim(arr[0][3])+"'";
  	strQueryResult = easyQueryVer3(strSQL,1,0,1);
  	if (!strQueryResult)
  	{
  	   fm.all('BranchManager').value = '';
  	   fm.all('hideAgentCode').value = '';
  	   alert('请先确定目标机构的管理人员！');
  	   fm.all('BranchManager').focus();
  	   return false;
  	}
  	else
  	{
  	   arr = decodeEasyQueryResult(strQueryResult);
       fm.all('ManagerName').value = arr[0][0];
  	   fm.all('hideAgentCode').value = arr[0][1];
  	   fm.all('BranchManager').value = arr[0][1];
  	}
  }
  return true;

}
function changeManager()
{
   if (getWherePart('BranchManager')=='')
     return false;
   var strSQL = "select groupAgentCode,Name from LAAgent where 1=1 and (AgentState is null or AgentState < '03')"
                + getWherePart('groupAgentCode','BranchManager')
                + getWherePart('AgentGroup','BranchCode','<>');

   var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('该代理人无效！');
   	fm.all('BranchManager').value = '';
   	fm.all('ManagerName').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   fm.all('ManagerName').value = arr[0][1];
}

function queryAgent()
{
      if (verifyInput() == false) return false;
   var tReturn = getManageComLimitlike("c.ManageCom");
   var  tBranchCode=fm.all('BranchCode').value;
   var  tManageCom=tBranchCode.substring(0,8);
   var  ttManageCom=tBranchCode.substring(0,4);
   if((fm.all('BranchCode').value==null)||(trim(fm.all('BranchCode').value)==""))
   {
      alert("请先确定目标展业机构！");
      fm.all('BranchCode').focus();
      return false;
   }
  
   if(trim(fm.all('BranchCode').value) == trim(fm.all('AgentGroup').value))
   {
   	alert('调动人员所属展业机构不能和目标机构相同！');
   	fm.all('AgentGroup').focus();
   	return false;
   }
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  if(tBranchType==1 && tBranchType2=='01')

  strSQL = "select getUniteCode(a.AgentCode),b.Name,c.BranchAttr,a.AgentGrade from LATree a,LAAgent b,LABranchGroup c where 1=1 and (AgentGrade <= 'B01') "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('b.groupAgentCode','AgentCode')
	     + getWherePart('a.AgentGrade','AgentGrade')	     
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup  "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"' and  c.managecom ='"+tManageCom+"' " 
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"' ";

	else if(tBranchType==3)

		strSQL = "select getUniteCode(a.AgentCode),b.Name,c.BranchAttr,a.AgentGrade from LATree a,LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('b.groupAgentCode','AgentCode')
	     + getWherePart('a.AgentGrade','AgentGrade')
	     + getWherePart('b.groupAgentCode','BranchManager','<>')
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"' and b.agentkind>'03' and  c.managecom ='"+tManageCom+"' "
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"'";

	     else if(tBranchType==2 && tBranchType2=='01')

		strSQL = "select getUniteCode(a.AgentCode),b.Name,c.BranchAttr,a.AgentGrade,a.AgentGroup from LATree a,LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('b.groupAgentCode','AgentCode')	    
	     + getWherePart('a.AgentGrade','AgentGrade')
	     + getWherePart('b.groupAgentCode','BranchManager','<>')
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup and a.agentgrade<'E01'  "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"'"
	     + " and  c.managecom  like '"+ttManageCom+"%' "
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"'";;

  // alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
  	alert('查询失败,不存在有效纪录！');
  	return false;
  }
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,3,4]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentGrid;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function submitSave()
{
   var mCount = AgentGrid.mulLineCount;

   if ((mCount == null)||(mCount == 0))
   {
   	alert("请查询出要调动的人员！");
   	return false;
   }

   var isChk=false;
   for (i=0;i<mCount;i++)
   {
   	if( AgentGrid.getSelNo( i ))
   	{
   	   isChk=true;

   	   break;
   	}
   }
   if (isChk==false)
   {
     alert("请选出要调动的人员！");
     return false;
   }
   if(!beforeSubmit())
   {
     alert(agentError);
     return false;
   }
     submitForm();

}

function clearAll()
{
   fm.all('BranchCode').value = '';
   fm.all('BranchName').value = '';
   fm.all('BranchManager').value = '';
   fm.all('ManagerName').value = '';
   fm.all('hideAgentCode').value = '';
   fm.all('hideBranchLevel').value = '';
   fm.all('hideUpBranch').value='';
   fm.all('hideAgentGroup').value='';
   clearGrid();
}
function clearGrid()
{
   fm.all('AgentGroup').value = '';
   fm.all('AgentCode').value = '';
   fm.all('AgentGrade').value = '';
   AgentGrid.clearData("AgentGrid");
}
/*********************************************************************
 *  通过业务员查询保单信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function setContValue()
{
   initContGrid() ;                     
   var agentNo = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 1);
	 //保单性质为1：已签单； 2：撤保或退保
	 //查询保单信息
	 var strSQL = "select ContNo,AppntNo,AppntName,CValiDate,CInValiDate,SumPrem ,Signdate,'未签单', '个单' "
      + " from LCCont where GrpContNo='00000000000000000000' and Conttype='1' "
      + " and AgentCode=getAgentCode('" + agentNo + "') and Signdate is null " 
      + " union all "  //这里用union all比用union效率高
      + " select a.GrpContNo,a.AppntNo,a.GrpName,a.CValiDate,a.CInValiDate,a.SumPrem,Signdate,'未签单','团单'"
      + " from LCGrpCont a "
      + " where  a.Signdate is null "
      + " and a.AgentCode = '" + agentNo + "' "
      + " union all "  
      + " select ContNo,AppntNo,AppntName,CValiDate,CInValiDate,SumPrem ,Signdate,'已签单', '个单' "
      + " from LCCont where GrpContNo='00000000000000000000' and Conttype='1'"
      + " and AgentCode=getAgentCode('" + agentNo + "') and Signdate is  not null " 
      + " union all "  
      + " select a.GrpContNo,a.AppntNo,a.GrpName,a.CValiDate,a.CInValiDate,a.SumPrem,Signdate,'已签单','团单'"
      + " from LCGrpCont a "
      + " where  a.Signdate is   not null "
      + " and a.AgentCode = getAgentCode('" + agentNo + "') "
      + " union all " 
      + " select a.ContNo, a.AppntNo, a.AppntName,a.CValiDate,b.ModifyDate, a.SumPrem,a.Signdate,'已终止', '个单' "
      + " from LBCont a, LPEdorItem b "
      + " where a.EdorNo = b.EdorNo "
      + " and a.GrpContNo = '00000000000000000000' "
      + " and a.AgentCode =getAgentCode( '" + agentNo + "') "
      + " union all "                                                                                                                                          
      + " select a.GrpContNo, a.AppntNo, a.GrpName, a.CValiDate,b.ModifyDate,a.SumPrem,a.Signdate, '已终止','团单' "
      + " from LBGrpCont a, LPGrpEdorItem b "                                                                                                                       
      + " where a.EdorNo = b.EdorNo "   
      + " and a.AgentCode =getAgentCode( '" + agentNo + "') order by signdate desc ";
    turnPage1.queryModal(strSQL, ContGrid);    
    showCodeName();
    
    // add new 
    var strSQL1 = "select value(max(distinct IndexCalNo),'C') from LAWage where 1=1  and state='1' and agentcode=getAgentCode('"+agentNo+"') "
             +getWherePart('BranchType')
             +getWherePart('BranchType2');
   var tResult1 = easyQueryVer3(strSQL1,1,1,1);
   var  mArr1 = decodeEasyQueryResult(tResult1);
   var maxWMonth= mArr1[0][0]; 
   if(maxWMonth!='C')
   {
  	 var maxwday=maxWMonth.substring(0,4)+"-"+maxWMonth.substring(4,6)+"-01"; 
  	 var strSQL2 = "select date('"+maxwday+"') + 1 month from dual where 1=1 "
  	 var strResult = easyQueryVer3(strSQL2, 1, 1, 1);
  	 var  mArr2 = decodeEasyQueryResult(strResult);
     var result= mArr2[0][0];
     fm.all('AdjustDate').value=result;
     fm.all('Flag').value = '2';
     
  var agentNo = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 1);
  var agentName = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 2);
  var agentGroup = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 5);  

  var sql=" select distinct  grpcontno from  lacommision "
  +" where agentcode=getAgentCode('"+agentNo+"') and agentgroup='"+agentGroup+"' "
  +" and (wageno>'"+maxWMonth+"' or wageno is null) and grpcontno<>'00000000000000000000' "
  +" union all "
  +" select distinct  contno  from  lacommision "
  +" where agentcode=getAgentCode('"+agentNo+"')  and agentgroup='"+agentGroup+"' "
  +" and (wageno>'"+maxWMonth+"' or wageno is null) and grpcontno<>'00000000000000000000' ";
   var strResult1 = easyQueryVer3(sql, 1, 1, 1); 
  if(strResult1)
   {
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strResult1);	
    alert("由于团险业务员"+agentName+"(工号"+agentNo+")：薪资月"+maxWMonth+"后至今已收费未计算薪资保单："+tArr+"，没有进行保单归属，故无法进行调整。请先归属保单！");
   // fm.all('saveb').disable=true;
    document.getElementById('saveb').disabled = true;
    return false;     
   }
   }else{
    var strSQL2 = "select employdate from laagent where 1=1   and agentcode=getAgentCode('"+agentNo+"') "
             +getWherePart('BranchType')
             +getWherePart('BranchType2');
     var tResult2 = easyQueryVer3(strSQL2,1,1,1);
     var mArr3 = decodeEasyQueryResult(tResult2);
     var tEmploydate = mArr3[0][0];
     fm.all('AdjustDate').value=tEmploydate;
      
  	 fm.all('Flag').value="3";
	 var agentGroup = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 5);   
     var agentNo = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 1);
     var agentName = AgentGrid.getRowColData(AgentGrid.getSelNo() - 1, 2);
     var sql=" select distinct  grpcontno from  lacommision "
     +" where agentcode=getAgentCode('"+agentNo+"') and agentgroup='"+agentGroup+"' "
     +" and grpcontno<>'00000000000000000000' "
     +" union all "
     +" select distinct  contno  from  lacommision "
     +" where agentcode=getAgentCode('"+agentNo+"')  and agentgroup='"+agentGroup+"' "
     +" and grpcontno<>'00000000000000000000' "
  var strResult1 = easyQueryVer3(sql, 1, 1, 1); 
  if(strResult1)
   {
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strResult1);	
    alert("由于团险业务员"+agentName+"(工号"+agentNo+")：至今已收费未计算薪资保单："+tArr+"，没有进行保单归属，故无法进行调整。请先归属保单！");
    document.getElementById('saveb').disabled = true;
    return false;     
   }
  }  	
}

 