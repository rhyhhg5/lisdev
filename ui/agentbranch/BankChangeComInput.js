 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,NewBranchAttr )
{
  //fm.reset();	
  initForm();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//    fm.all('NewBranchAttr').value=NewBranchAttr;
    //执行下一步操作
    //fm.all('AdjustBranchCode').value = '';
    //fm.all('AdjustAfterBranchCode').value = '';
   // BranchChange();
  }
}


//***************************************************
//* 验证对应业务员代码的名字和销售机构
//***************************************************
function getAgentComMs()
{
 if (fm.all('AgentOldCom').value==null || fm.all('AgentOldCom').value=='')
 { 
 	alert("请先录入银行机构代码");
 	fm.all('NewAgentCode').value="";
 	return;
}
 var strSQL = "";
 var tAgentCode=fm.all('AgentOldCom').value;
 
   if (tAgentCode!='' )
  {	
     strSQL = "select a.agentcom,b.name,b.BankType,b.SellFlag,b.UpAgentCom"
     +" ,(select name from lacom where agentcom =b.UpAgentCom )"
     +" , getunitecode(a.agentcode),(select name from laagent where agentcode= a.agentcode )"
    +" , a.agentgroup,(select name  from labranchgroup  where agentgroup=a.agentgroup )"
     +" ,(select managecom from laagent where  agentcode= a.agentcode)"
     +" ,a.relatype,a.makedate,a.maketime,a.modifydate,a.modifytime,a.startdate,a.enddate,a.operator"
     
     +" from lacomtoagent a,lacom b  where 1=1  and a.agentcom=b.agentcom  and a.relatype='1'  "
     + getWherePart('a.AgentCom','AgentOldCom') ;   
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
     
  //判断是否查询成功
     if (!strQueryResult) 
    {
      alert("没有此机构");
      fm.all('AgentOldCom').value="";
      fm.all('AgentComName').value = "";
      fm.all('BankType').value = "";
      fm.all('SellFlag').value = "";
      fm.all('UpAgentCom').value="";
      fm.all('UpAgentComName').value = "";
      fm.all('OldAgentCode').value = "";
      fm.all('OldAgentName').value="";
      fm.all('OldAgentGroup').value = "";
      fm.all('OldAgentGroupName').value = "";
      fm.all('OldManageCom').value="";
      return;
    }
  }
 
else
  {
     fm.all('AgentOldCom').value="";
      fm.all('AgentComName').value = "";
      fm.all('BankType').value = "";
      fm.all('SellFlag').value = "";
      fm.all('UpAgentCom').value="";
      fm.all('UpAgentComName').value = "";
      fm.all('OldAgentCode').value = "";
      fm.all('OldAgentName').value="";
      fm.all('OldAgentGroup').value = "";
      fm.all('OldAgentGroupName').value = "";
      fm.all('OldManageCom').value="";
     return;
  }
 //alert("4");
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
 
 
//	    fm.all('AgentOldCom').value=tArr[0][0];
      fm.all('AgentComName').value = tArr[0][1];
      fm.all('BankType').value = tArr[0][2];
      fm.all('SellFlag').value = tArr[0][3];
      fm.all('UpAgentCom').value=tArr[0][4];
      fm.all('UpAgentComName').value = tArr[0][5];
      fm.all('OldAgentCode').value = tArr[0][6];
      fm.all('OldAgentName').value=tArr[0][7];
      fm.all('OldAgentGroup').value = tArr[0][8];
      fm.all('OldAgentGroupName').value = tArr[0][9];
      fm.all('OldManageCom').value=tArr[0][10];
      fm.all('RelaType').value=tArr[0][11];
      fm.all('MakeDate').value=tArr[0][12];
      fm.all('MakeTime').value=tArr[0][13];
      fm.all('ModifyDate').value=tArr[0][14];
      fm.all('ModifyTime').value=tArr[0][15];
      fm.all('StartDate').value=tArr[0][16];
      fm.all('EndDate').value=tArr[0][17];
      fm.all('OldOperator').value=tArr[0][18];

	
	
}
//***************************************************
//* 验证对应业务员代码的名字和销售机构
//***************************************************
function getAgentMs()
{
 if (fm.all('AgentOldCom').value==null || fm.all('AgentOldCom').value=="")
 { 
 	alert("请先录入银行机构代码");
 	fm.all('NewAgentCode').value="";
 	return;
}
 var strSQL = "";
 var tAgentCode=fm.all('NewAgentCode').value;
 
   if (tAgentCode!='' )
   {	var strAgent = "";
	if(fm.NewAgentCode.value!=null&&fm.NewAgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.NewAgentCode.value+"') ";
	}
     strSQL = "select a.name,a.agentgroup,b.name,a.managecom  from laagent a,labranchgroup b where a.agentstate<='02' and a.agentgroup=b.agentgroup  and a.branchtype='3'  and a.branchtype2='01' "
     //+ getWherePart('a.AgentCode','NewAgentCode') ; 
     + strAgent;
     var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
     
  //判断是否查询成功
     if (!strQueryResult) 
    {
      alert("不存在此业务员");
      fm.all('NewAgentCode').value="";
      fm.all('NewAgentName').value = "";
      fm.all('NewAgentGroup').value = "";
      fm.all('NewAgentGroupName').value = "";
      fm.all('NewManageCom').value="";
      return;
    }
  }
 
else
  {
      fm.all('NewAgentCode').value="";
      fm.all('NewAgentName').value = "";
      fm.all('NewAgentGroup').value = "";
      fm.all('NewAgentGroupName').value = "";
      fm.all('NewManageCom').value="";
     return;
  }
 //alert("4");
  //查询成功则拆分字符串，返回二维数组
  //var arrDataSet = decodeEasyQueryResult(strQueryResult);
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
  
  fm.all('NewAgentGroup').value  =tArr[0][1];
  fm.all('NewAgentName').value  =tArr[0][0];	
 fm.all('NewAgentGroupName').value =tArr[0][2];
 fm.all('NewManageCom').value = tArr[0][3];
	
	
	
}


//提交前的校验、计算  
function beforeSubmit()
{
	
//	var strSQL = "select a.sellflag from lacom  a  where 1=1 "
//	     + getWherePart('a.AgentCom','AgentOldCom') ;  
// 
//  var arr = easyExecSql(strSQL);
//	var tsellflag=arr[0][0] ;
//	//alert(tsellflag);
//	if(tsellflag=="N" )
//	{
//		alert("此银行机构无营销资格无需指定业务员！");	
//    return false;
//	}
	//	alert("1111111111111");
		var strSQL1 = "select a.agentgroup,(select managecom  from labranchgroup where agentgroup=a.agentgroup) ,getunitecode(a.agentcode)  from lacomtoagent  a   where 1=1  and relatype='1'"
	     + getWherePart('a.AgentCom','AgentOldCom') ;  
 
  var arr = easyExecSql(strSQL1);
	var tmanagecom=arr[0][1] ;
	var tagentcode=arr[0][2] ;
	var strAgent = "";
	if(fm.NewAgentCode.value!=null&&fm.NewAgentCode.value!=''){
		strAgent = " and agentcode=getAgentCode('"+fm.NewAgentCode.value+"') ";
	}
	strSQL2 = "select getunitecode(agentcode),managecom  from laagent  where agentstate<='02' "
     //+ getWherePart('AgentCode','NewAgentCode') ; 
	+ strAgent;
	
  arr = easyExecSql(strSQL2);
  var tupManageCom=arr[0][1] ;
  var tupagentcode=arr[0][0] ;
	if(tupManageCom != tmanagecom )
	{
		alert("不能在不同的管理机构间更改业务员！");	
		 fm.all('NewAgentCode').value="";
      fm.all('NewAgentName').value = "";
      fm.all('NewAgentGroup').value = "";
      fm.all('NewAgentGroupName').value = "";
      fm.all('NewManageCom').value="";
    return false;
	}
	return false;
	if(tupagentcode == tagentcode )
	{
		alert("业务员没有更改，请重新输入业务员代码");	
		 fm.all('NewAgentCode').value="";
      fm.all('NewAgentName').value = "";
      fm.all('NewAgentGroup').value = "";
      fm.all('NewAgentGroupName').value = "";
      fm.all('NewManageCom').value="";
    return false;
	}
	return true ;
	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}




function submitSave()
{   
	
      if((trim(fm.all('AgentOldCom').value)=="")
     ||(fm.all('AgentOldCom').value==null)) 
   {
      alert("请输入机构代码！");	
      return false;
   }  
  
   if((fm.all('NewAgentCode').value==null)
     ||(trim(fm.all('NewAgentCode').value)=="")) 
   {
      alert("请输入新负责人代码！");	
      return false;
   } 
     if((fm.all('AdjustDate').value==null)
     ||(trim(fm.all('AdjustDate').value)=="")) 
   {
      alert("请输入调整日期！");	
      return false;
   }   
   
    if (!verifyInput())
     return false;
      
     
	  if(beforeSubmit()==false)
         return false;
  
  
     
     submitForm();
     
}




function clearGrid()
{
   fm.all('AgentOldCom').value = '';
   fm.all('AgentComName').value = '';
   fm.all('BankType').value = '';
   fm.all('SellFlag').value = '';
   fm.all('UpAgentCom').value = '';
   fm.all('UpAgentComName').value = '';
   fm.all('OldAgentCode').value = '';
   fm.all('OldAgentName').value = '';
   fm.all('OldAgentGroup').value = '';
   fm.all('OldAgentGroupName').value = '';
   fm.all('OldManageCom').value = '';
   fm.all('NewAgentCode').value = '';
   fm.all('NewAgentName').value = '';
   fm.all('NewAgentGroup').value = '';
   fm.all('NewAgentGroupName').value = '';
   fm.all('NewManageCom').value = '';
   fm.all('AdjustDate').value = '';
   
   
   
   //fm.all('AdjustAfterBranchName').value = '';
   //fm.all('NewBranchAttr').value='';
   //BranchGrid.clearData("BranchGrid");
   //saveClick=false;
}







