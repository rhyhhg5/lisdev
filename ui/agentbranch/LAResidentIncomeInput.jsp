<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：DataIntoLACommision.jsp
	//程序功能：
	//创建日期：2003-06-24
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="LAResidentIncomeInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="LAResidentIncomeInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form action="LAResidentIncomeSave.jsp" method=post name=fm
	target="fraSubmit"><span id="operateButton">
<table class="common" align=center>
	<tr align=right>
		<td class=button>&nbsp;&nbsp;</td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="保  存"
			TYPE=button onclick="return submitForm();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="修  改"
			TYPE=button onclick="return updateClick();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="删  除"
			TYPE=button onclick="return deleteClick();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="查  询"
			TYPE=button onclick="return queryClick();"></td>
	</tr>
</table>
</span>
<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif"
			style="cursor:hand;" OnClick="showPage(this,divAgent1);"></td>
		<td class=titleImg>居民收入录入</td>
	</tr>
</table>
<Div id="divAgent1" style="display: ''">
<table class=common>
	<TR class=common>
		<TD class=title>居民类型</TD>
		<TD class=input><Input class='codeno' name=ResidentType
			verify="居民类型|NOTNULL" CodeData="0|^1|城镇|^2|农村"
			ondblclick="return showCodeListEx('ResidentType',[this,ResidentTypeName],[0,1],null,null,null,1);"
			onkeyup="return showCodeListKey('ResidentType',[this,ResidentTypeName],[0,1],null,null,null,1);"><Input
			class=codename name=ResidentTypeName  elementtype=nacessary></TD>
		<TD class=title>管理机构</TD>
		<TD class=input><Input class="codeno" name=ManageCom
			ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,4,'to_char(length(trim(comcode)))',1);"
			onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,4,'char(length(trim(comcode)))',1);"
			verify="管理机构|notnull&code:comcode"><Input class=codename
			name=ManageComName  elementtype=nacessary></TD>
	</TR>
	<TR class=common>
		<TD class=title>居民收入起始日期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short"
			name=StartDate verify="居民收入起始日期|NOTNULL&Date" elementtype=nacessary>
		</TD>
		<TD class=title>居民收入终止日期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short"
			name=EndDate verify="居民收入终止日期|NOTNULL&Date" elementtype=nacessary>
		</TD>
	</TR>
	<tr id='rpcdi' class=common style="display: '' ">
		<TD class=title>最近年度居民人均可支配收入</TD>
		<TD class=input><Input class='common' name=RecentPCDI
			elementtype=nacessary><font color="#ff0000">(单位:元)</font></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</tr>
	<tr id='rpcni' class=common style="display: 'none' ">
		<TD class=title>最近年度居民人均纯收入</TD>
		<TD class=input><Input class='common' name=RecentPCNI
			elementtype=nacessary><font color="#ff0000">(单位:元)</font></TD>
	</tr>
</table>
<input type="hidden" class=input name=fmAction>
<input type="hidden" class=input name=hideOperate>
<input type="hidden" class=input name=idx>
</Div>
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
