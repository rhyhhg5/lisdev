var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

// 页面查询功能
function easyQueryClick()
{
   if (!verifyInput()) return false; // 页面简单校验
   var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   initBranchGroupGrid();
   var strSQL = "";
	//strSQL = "select BranchAttr,name,BranchManager,BranchManagerName,ManageCom,ApplyGbFlag,ApplyGbStartDate,GbuildFlag,GbuildStartDate,GbuildEndDate,AgentGroup from LaBranchgroup where 1=1 and Branchlevel = '03' "
	strSQL = "select BranchAttr,name,getUniteCode(BranchManager),BranchManagerName,ManageCom,ApplyGbFlag,ApplyGbStartDate,GbuildFlag,GbuildStartDate,GbuildEndDate,AgentGroup from LaBranchgroup where 1=1 and Branchlevel = '03' "    
			+ getWherePart('Name','Name','like')
	       //  + getWherePart('ManageCom','ManageCom')
	         //+ getWherePart('BranchManager','BranchManager','like')
			 //+ getWherePart('getAgentCode(laa.groupagentcode)','BranchManager','like')
	         + getWherePart('BranchManagerName','BranchManagerName','like')
	         + getWherePart('BranchLevel')
	         + getWherePart('FoundDate')
	         + getWherePart('EndDate')
	         + getWherePart('BranchType')
	         + getWherePart('BranchType2')
	         + getWherePart('BranchAttr');
	       if(fm.all('ManageCom').value!=null&&fm.all('ManageCom').value!='')
	       {
	       	strSQL =strSQL+ " and managecom like '"+fm.all('ManageCom').value+"%' ";
	       }
	       if(fm.all('BranchManager').value!=null&&fm.all('BranchManager').value!='')
	       {
	       	strSQL =strSQL+ " and exists(select 1 from laagent a where a.agentcode = LaBranchgroup.branchmanager and a.groupagentcode like '%"+fm.all('BranchManager').value+"%') ";
	       }
	       strSQL =strSQL + "order by branchattr";
	       
	turnPage.queryModal(strSQL, BranchGroupGrid);   		
}

//返回操作
function returnParent()
{
    var arrReturn = new Array();
	var tSel = BranchGroupGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
			try
			{	
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}
// 查询返回数据信息
function getQueryResult()
{
   	var arrSelected = null;
	tRow = BranchGroupGrid.getSelNo();
	if( tRow == 0 || tRow == null) return arrSelected;
	arrSelected = new Array();
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select BranchAttr,Name,ManageCom,BranchLevel,BranchManager,BranchManagerName,ApplyGbFlag,ApplyGbStartDate,GbuildFlag,GbuildStartDate,GbuildEndDate,";
	strSQL = strSQL + "BranchType,BranchType2,AgentGroup,FoundDate,(select a.groupagentcode from laagent a where a.agentcode = branchmanager) ";
	strSQL = strSQL + " from LABranchGroup where Agentgroup ='"+BranchGroupGrid.getRowColData(tRow-1,11)+"' "; 
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
	
}
