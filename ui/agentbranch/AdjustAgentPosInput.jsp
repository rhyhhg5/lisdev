<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
	//程序名称：AdjustAgentInput.jsp
	//程序功能：
	//创建日期：2002-08-16 16:25:40
	//创建人  ：CrtHtml程序创建
	//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	String BranchType=request.getParameter("BranchType");
	String BranchType2=request.getParameter("BranchType2");
	System.out.println("BranchType:"+BranchType);
	System.out.println("BranchType2:"+BranchType2);
	String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
	%>
	<script>
		var manageCom = <%=tG.ManageCom%>;
		var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and gradecode <> #B21#";
	</script>
	<%@page contentType="text/html;charset=GBK" %>
	<head >
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="AdjustAgentPosInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="AdjustAgentPosInit.jsp"%>
		<%@include file="../agent/SetBranchType.jsp"%>
		<%@include file="../common/jsp/ManageComLimit.jsp"%>
		<title></title>
	</head>
	<%
	String tTitleAgent="";
	if("1".equals(BranchType))
	{
	tTitleAgent = "营销员";
}else if("2".equals(BranchType))
	{
	tTitleAgent = "业务员";
	}
	%>
	<body  onload="initForm();initElementtype();" >
		<form action="./AdjustAgentPosSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr class=common>
					<td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent);">
					</td>
							<td class=titleImg>人员调岗信息</td>
				</tr>
			</table>
			<Div  id= "divAdjustAgent" style= "display: ''">
				<table  class= common>
					<TR  class= common>					
						<TD  class= title><%=tTitleAgent%>代码</TD>
						<TD  class= input>
							<Input class=common name=AgentCode
							 verify="营销员编码|notnull" elementtype=nacessary onchange="return changeAgent();">
						</TD>
						<TD  class= title><%=tTitleAgent%>姓名</TD>
						<TD  class= input><Input class=common name=Name ></TD>
					</TR>
					<TR  class= common>
						<TD  class= title>销售单位</TD>
						<TD  class= input>
							<Input class='readonly'readonly name=OldAgentGroup >
						</TD>
						<TD  class= title>销售单位名称</TD>
						<TD  class= input>
							<Input class='readonly'readonly name=OldAgentGroupName >
						</TD>
					</TR>
					<TR  class= common>
						<TD  class= title><%=tTitleAgent%>当前职级</TD>
						<TD  class= input>
							<Input class='readonly'readonly name=OldAgentGrade >
						</TD>
						<TD class= title><%=tTitleAgent%>目标职级</TD>
						<TD class= input>
							<Input name=NewAgentGrade class="codeno"
							ondblclick="return showCodeList('AgentGrade',[this,NewAgentGradeName],[0,1],null,msql,1);"
							onkeyup="return showCodeListKey('AgentGrade',[this,NewAgentGradeName],[0,1],null,msql,1);" verify="营销员目标职级|notnull"
							><Input name=NewAgentGradeName class="codename" elementtype=nacessary >
						</TD>
					</TR>

					<TR>
						<TD  class= title>调整年月</TD>
						<TD  class= input>
							<Input class='common' name=IndexCalNo verify="调整年月|notnull&len=6" elementtype=nacessary>
						</TD>
					</TR>
				</table>

				<table class='common'>
					<input type=button class=cssButton name="save"  value='保存' onclick="submitForm();">
				 <p><font color="red">注：目标职级的开始日期为“调整年月”下个月一号（如：调整年月为200807，则目标职级开始日期为“2008-08-01”）！</font></p>
				</table>
				<input type=hidden name=BranchType value=''>
				<input type=hidden name=BranchType2 value=''>
				<input type=hidden name=ManageCom value=''>
				<input type=hidden name=AgentGroup value=''>
				<input type=hidden name=TransferType value=''>
				<input type=hidden name=OldSeries value=''>
				<input type=hidden name=NewSeries value=''>
			</form>
			<span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
		</body>
	</html>
