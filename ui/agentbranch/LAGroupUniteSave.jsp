<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAGroupUniteSave.jsp
//程序功能：
//创建日期：2005-7-18 17:18
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  LAGroupUniteUI tLAGroupUniteUI = new LAGroupUniteUI();
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema tLABranchGroupSchemaA = new LABranchGroupSchema();
  LABranchGroupSchema tLABranchGroupSchemaU = new LABranchGroupSchema();
  LABranchGroupSet tLABranchGroupSet = new LABranchGroupSet();
  LATreeSchema tLATreeSchema = new LATreeSchema();
  //输出参数
  CErrors tError = null;

  String tOperate="";

  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  //目标团队代码
  tLABranchGroupSchemaA.setBranchAttr(request.getParameter("BranchAttr"));
  //被合并团队代码
  tLABranchGroupSchemaU.setBranchAttr(request.getParameter("BranchAttrU"));
  //机构信息
  tLABranchGroupSet.add(tLABranchGroupSchemaA);
  tLABranchGroupSet.add(tLABranchGroupSchemaU);
  //被合并团队管理员信息
  tLATreeSchema.setAgentCode(request.getParameter("GroupManager"));
  tLATreeSchema.setAgentGrade(request.getParameter("AgentGradeNew"));
  tLATreeSchema.setAgentLastGrade(request.getParameter("AgentGradeOld"));
  tLATreeSchema.setStartDate(request.getParameter("AdjustDate"));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tG);
	tVData.add(tLABranchGroupSet);
	tVData.add(tLATreeSchema);
  try
  {
    tLAGroupUniteUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLAGroupUniteUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>