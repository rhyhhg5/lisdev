<%
//程序名称：LANewBankInit.jsp
//程序功能：
//创建日期：2012-10-11 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {  
                   
  //$initfield$
  }
  catch(ex)
  {
    alert("在LANewBankInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LANewBankInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	initLANewBankGrid();
  }
  catch(re)
  {
    alert("LANewBankInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化WageGrid
 ************************************************************
 */
function initLANewBankGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="银行编码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="银行名称";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
 
  
        LANewBankGrid = new MulLineEnter( "fm" , "LANewBank" ); 

        //这些属性必须在loadMulLine前
        LANewBankGrid.mulLineCount = 0;   
        LANewBankGrid.displayTitle = 1;
        LANewBankGrid.locked=1;
        LANewBankGrid.canSel=0;
        LANewBankGrid.canChk=0;
       	LANewBankGrid.hiddenPlus = 1;
      	LANewBankGrid.hiddenSubtraction = 1;
        LANewBankGrid.loadMulLine(iArray);   

      }
      catch(ex)
      {
        alert("初始化LANewBankGrid时出错："+ ex);
      }
    }


</script>