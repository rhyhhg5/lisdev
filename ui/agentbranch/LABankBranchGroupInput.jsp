<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LABankBranchGroupInput.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
%>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"# and branchlevelcode=#43#";
</script>
<head >
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="./LABankBranchGroupInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="./LABankBranchGroupInit.jsp"%>
<%@include file="../agent/SetBranchType.jsp"%>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LABankBranchGroupSave.jsp" method=post name=fm target="fraSubmit" >
    <%@include file="../common/jsp/OperateAgentButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      团队基本信息
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLABranchGroup1" style= "display: ''">
      <table  class= common>
      	 <TR  class= common>        
         	<TD class= title>销售机构代码</TD>
          <TD  class= input>
          	<Input class="readonly" name=BranchAttr verify="销售机构代码|NOTNULL" elementtype=nacessary readOnly>
          </TD>
          <TD  class= title>销售机构名称</TD>
          <TD  class= input>
            <Input  name=BranchName elementtype=nacessary verify="销售机构名称|NOTNULL" >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            所属管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom verify="所属管理机构|code:comcode&NOTNULL&len>=4"   
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"  
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
             readonly ><Input class=codename name=ManageComName readOnly elementtype=nacessary> 
          </TD> 
          <TD  class= title>级别</TD>
          <TD  class= input>
            <Input class= 'codeno' name=BranchLevel verify="级别|notnull&code:BranchLevel" 
            ondblclick="return showCodeList('branchlevel',[this,BranchLevelName],[0,1],null,msql,1);" 
            onkeyup="return showCodeListKey('branchlevel',[this,BranchLevelName],[0,1],null,msql,1);" 
             readonly ><Input class=codename name=BranchLevelName readOnly elementtype=nacessary>
          </TD>        
        <Input type=hidden name=InsideFlag >
        <Input type=hidden name=FieldFlag value=''>
        </TR>
        <TR  class= common>
	      <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAddress >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name=BranchZipcode verify="展业机构邮编|len<=6" >
          </TD>        
        </TR>
        <TR  class= common>
	      <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=BranchPhone >
          </TD>
          
          <TD  class= title>
            成立日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' verify="成立日期|NOTNULL&Date" name=FoundDate format='short' elementtype=nacessary>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='codeno' name=EndFlag verify="停业属性|code:yesno&NOTNULL" 
             ondblclick="return showCodeList('yesno',[this,EndFlagName],[0,1]);" 
             onkeyup="return showCodeListKey('yesno',[this,EndFlagName],[0,1]);"
              readonly ><Input class=codename name=EndFlagName readOnly elementtype=nacessary>
          </TD>
	        <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate verify="停业日期|Date" format='short' onfocusout="return changeGroup();">
          </TD>
        </TR>       

        <tr class=common>         
          <TD  class= title>
            操作员代码
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=Operator >
          </TD>
           <TD  class= title>
            成本中心编码
          </TD>
          <TD  class= input>
            <Input class= common name=CostCenter verify="成本中心编码|notnull&len=10"  elementtype=nacessary>
          </TD>
        </tr>
        
      </table>
      <tr><td><p> <font color="#ff0000">注：在录入、电话、地址等信息时，请不要输入Tab、空格或是其他特殊字符，这些特殊字符会导致系统查询该机构时失败。</font></p></td></tr>
      
    </Div>
   
    <Input type=hidden name=Operator_name >
    <Input type=hidden name=hideOperate value=''>
    <Input type=hidden name=BranchType>   
    <Input type=hidden name=BranchType2>
    <Input type=hidden name=hmanagecom>
     <Input type=hidden name=tCostCenter value=''>
    <Input type=hidden name=hupbranchattr>
    <Input type=hidden name=hbranchlevel>
    <Input type=hidden name=AgentGroup value='' >  <!--后台操作的隐式机构编码，不随机构的调整而改变 -->
    <Input type=hidden name=UpBranch value='' >  <!--上级机构代码，存储隐式机构代码 -->
    <Input type=hidden class='common' name=ManageComCode value='<%=tG.ManageCom%>'>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>

