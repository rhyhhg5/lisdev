 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
 
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,NewBranchAttr )
{
  //fm.reset();	
  initForm();
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    fm.all('NewBranchAttr').value=NewBranchAttr;
    //执行下一步操作
    //fm.all('AdjustBranchCode').value = '';
    //fm.all('AdjustAfterBranchCode').value = '';
  
  }
}

//提交前的校验、计算  
function beforeSubmit()
{
	 
	
}     
    
function submitSave()
{   
	 
	  if(beforeSubmit()==false)
         return false;
   /*  
   */
   if( fm.all('ManageCom').value.substr(0,4)!=fm.all('NewManageCom').value.substr(0,4) )  
   {
      alert("营业部只能在同一个分公司中调整！");	
      return false;
   }     
   
   if (!verifyInput())
     return false;
     
     submitForm();
     
}
 
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//验证字段的值
function BranchConfirm()
{
   
}


  
function clearGrid()
{
	 
   fm.all('AdjustBranchCode').value = '';
   fm.all('AdjustBranchName').value = '';
   fm.all('ManageCom').value = '';   
   fm.all('NewManageCom').value = '';
   fm.all('NewManageComName').value = '';
   fm.all('AdjustDate').value='';
   
   //BranchGrid.clearData("BranchGrid");
   //saveClick=false;
}

 
function getBranchManager(tBranchCode){
	var tBranchManager='';
	var tBranchType = fm.BranchType.value;
	var tBranchType2 = fm.BranchType2.value;
	var strSQL = "select BranchManager,BranchManagerName,managecom from LABranchGroup where 1=1 and branchattr='"+tBranchCode
             +"' and  branchtype='"+tBranchType+"' and  branchtype2='"+tBranchType2+"' ";
  var arr = easyExecSql(strSQL);
 
  return arr;
}

function afterCodeSelect( cCodeName, Field ) 
{
  if( cCodeName == "branchattr")
  {
  	var adminrr = getBranchManager(fm.AdjustBranchCode.value);
  	if(adminrr)
  	{
  		fm.BranchManager.value=adminrr[0][0];
  		fm.BranchManagerName.value=adminrr[0][1];
  		fm.ManageCom.value=adminrr[0][2];
  	}
  }
}

