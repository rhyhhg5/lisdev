<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：LATelSpecComSave.jsp
//程序功能：
//创建日期：2003-09-17 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LASpecComUI tLASpecComUI = new LASpecComUI();
  
  LAComSchema tLAComSchema   = new LAComSchema(); 
  LAComToAgentSet tLAComToAgentSet=new LAComToAgentSet();

  //输出参数
  CErrors mErrors = new CErrors();
  String tOperate = request.getParameter("hideOperate");
  tOperate = tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
   
  tG=(GlobalInput)session.getValue("GI");

System.out.println("[[[[[[[[[[[[[[((((-0-))))]]]]]]]]]]]]]]");
  //代理机构
  tLAComSchema.setAgentCom(request.getParameter("AgentCom"));
  tLAComSchema.setName(request.getParameter("Name"));
  tLAComSchema.setACType(request.getParameter("ACTypeCode"));
  //tLAComSchema.setIncome(request.getParameter("Income"));
  tLAComSchema.setChiefBusiness(request.getParameter("ChiefBusiness"));
  tLAComSchema.setProfits(request.getParameter("Profit"));
  tLAComSchema.setAssets(request.getParameter("Asset"));
  tLAComSchema.setProtocalNo(request.getParameter("ProtocalNo"));
  //tLAComSchema.setPersonnalSum(request.getParameter("PersonnalSum"));
  tLAComSchema.setCorporation(request.getParameter("Corporation"));
  //tLAComSchema.setUpAgentCom(request.getParameter("UpAgentCom"));
  tLAComSchema.setManageCom(request.getParameter("ManageCom"));
  tLAComSchema.setACType(request.getParameter("ACType"));
  tLAComSchema.setSellFlag(request.getParameter("SellFlag"));
  tLAComSchema.setOperator(tG.Operator);
  tLAComSchema.setEndFlag("N");
  tLAComSchema.setAreaType(" ");
  tLAComSchema.setChannelType(request.getParameter("ChannelType"));
  tLAComSchema.setLicenseNo(request.getParameter("LicenseNo"));
  tLAComSchema.setBankAccNo(request.getParameter("BankAccNo"));
  tLAComSchema.setBranchType(request.getParameter("BranchType"));
  tLAComSchema.setBranchType2(request.getParameter("BranchType2"));
  tLAComSchema.setLicenseStartDate(request.getParameter("Licensestart"));
  tLAComSchema.setLicenseEndDate(request.getParameter("Licenseend"));
System.out.println(request.getParameter("Licensestart"));
System.out.println(request.getParameter("Licenseend")); 
System.out.println("起期："+tLAComSchema.getLicenseStartDate())  ;
System.out.println("止期："+tLAComSchema.getLicenseEndDate());
  String mAgentCom="";
  String tAgentCode[] = request.getParameterValues("ComToAgentGrid1");
  String tAgentGroup[] = request.getParameterValues("ArchieveGrid3");
System.out.println("[[[[[[[[[[[[[[((((-2-))))]]]]]]]]]]]]]]");
  //System.out.prinln(tAgentCode);
  
  int length=0;
  if(null != tAgentCode)
  {
  	length=tAgentCode.length;
  }
  for (int i=0;i<length;i++)
  {
     LAComToAgentSchema tLAComToAgentSchema=new LAComToAgentSchema();
     tLAComToAgentSchema.setAgentCom(request.getParameter("AgentCom"));
     tLAComToAgentSchema.setRelaType("1");
     tLAComToAgentSchema.setAgentCode(tAgentCode[i]);
     tLAComToAgentSet.add(tLAComToAgentSchema);
  }
  System.out.println("注册资金：Asset="+tLAComSchema.getAssets()+" / 去年手续费收入：Profit="+tLAComSchema.getProfits()
     + " / 中介机构类型编码" +tLAComSchema.getACType());
  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tLAComSchema);
  tVData.add(tLAComToAgentSet);
  tVData.add(tG);
System.out.println(tOperate);
System.out.println("[[[[[[[[[[[[[[((((-3-))))]]]]]]]]]]]]]]");
  try
  {
    if(!tLASpecComUI.submitData(tVData,tOperate))
    {
          // @@错误处理
      mErrors.copyAllErrors(tLASpecComUI.mErrors);
      CError tError = new CError();
      tError.moduleName = "LASpecComSave";
      tError.functionName = "submitData";
      tError.errorMessage = "数据提交失败!";
      mErrors.addOneError(tError) ;
    }
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    mErrors = tLASpecComUI.mErrors;
    if (!mErrors.needDealError())
    {
      //mAgentCom=(String)(tLASpecComUI.getResult().get(0));
      Content = " 保存成功! ";
      FlagStr = "Succ";
    }
    else
    {
      Content = " 保存失败，原因是:" + mErrors.getFirstError();
      FlagStr = "Fail";
    }
  }
    //添加各种预处理

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mAgentCom%>");
</script>
</html>

