<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：AdjustBankInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
  String msql=" 1 and branchtype='"+BranchType+"' and branchtype2='"+BranchType2+"'";
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="AdjustAgentInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AdjustBankInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();">
  <form action="./AdjustAgentSave.jsp" method=post name=fm target="fraSubmit">
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent);">
    <td class=titleImg>
      目标机构信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent" style= "display: ''">
    <table  class= common>
       <TR  class= common>
          <TD  class= title>
            目标机构
          </TD>
          <TD  class= input>
            <Input class=common name=BranchCode elementtype=nacessary>
          </TD>
           <!--确定按钮 -->
          <TD  class= common>
            <Input type=button class="cssButton" value="确  认" onclick="BranchConfirm();">
          </TD>
       </TR>
      </table>
      <table class=common>
       <TR  class= common>
          <TD  class= title>
            目标机构名称
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=BranchName>
          </TD>
          <TD  class= title>
            目标机构类型
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=BranchType>
          </TD>
        </TR>
       <TR  class= common>
          <TD  class= title>
            机构管理人员代码
          </TD>
          <TD  class= input>
            <!--Input class=common name=BranchManager onchange="return changeManager();"-->
            <Input class='readonly'readonly name=BranchManager>
          </TD>
          <TD  class= title>
            机构管理人名称
          </TD>
          <TD  class= input>
            <Input class='readonly'readonly name=ManagerName>
          </TD>
        </TR>
    </table>

    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAdjustAgent2);">
    <td class=titleImg>
      查询调动员工信息
    </td>
    </td>
    </tr>
    </table>
  <Div  id= "divAdjustAgent2" style= "display: ''">
    <table  class= common>
        <TR  class= common>
          <TD  class= title>
           所属机构代码
          </TD>
          <TD  class= input>
            <Input class=common name=AgentGroup elementtype=nacessary>
          </TD>
          <TD  class= title>
           员工代码
          </TD>
          <TD  class= input>
            <Input class=common name=AgentCode>
          </TD>
        </TR>
        <TR  class= common>
         
          <TD  class= title>
            职级
          </TD>
          <TD  class= input>
            <Input class='code' name=AgentGrade ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');"
                                                onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');">
          </TD>
        </TR>
    </table>
    <br>
	<Input type=button class="cssButton" name=queryb value="查  询" onclick="queryAgent();">
	<input type=button class="cssButton" value='重  置' onclick="clearGrid();">
    <table class=common>
      <TR  class= common>
          <TD  class= title>
           调动日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=AdjustDate dateFormat='short' verify="调动日期|notnull&DATE" elementtype=nacessary>
          </TD>
          <TD  class= title>
          </TD>
          <TD  class= input>
          </TD>
        </TR>
    </table>
    <table  class= common>
         <tr  class= common>
             <td text-align: left colSpan=1>
		  <span id="spanAgentGrid">
		  </span>
             </td>
         </tr>
    </table>
    </div>
    <br>
	<INPUT VALUE="首  页" TYPE=button class="cssButton" onclick="turnPage.firstPage();">
	<INPUT VALUE="上一页" TYPE=button class="cssButton" onclick="turnPage.previousPage();">
	<INPUT VALUE="下一页" TYPE=button class="cssButton" onclick="turnPage.nextPage();">
	<INPUT VALUE="尾  页" TYPE=button class="cssButton" onclick="turnPage.lastPage();">
	<br><br><br>
	<input type=button class="cssButton" name=saveb value='保  存' onclick="return submitSave()">
	<input type=button class="cssButton" value='重  置' onclick="clearAll();">
   </div>
    <input type=hidden name=hideAgentCode value=''>
    <input type=hidden name=hideBranchLevel value=''>
    <input type=hidden name=hideUpBranch value=''>
    <input type=hidden name=hideAgentGroup value=''>
    <input type=hidden name=BranchType2 value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"> </span>
</body>
</html>
