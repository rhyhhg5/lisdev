//该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="1";
var arrDataSet; 
var turnPage = new turnPageClass();


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LASocialBranchGroupQueryInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")	
  {
		parent.fraMain.rows = "0,0,50,82,*";
  }else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

// 查询按钮
function easyQueryClick()
{	
	if( verifyInput() == false ) return false;
	// 初始化表格
	initBranchGroupGrid();
	// 书写SQL语句
	var strSQL = "";
	var tReturn = parseManageComLimitlike();

	strSQL = "select BranchAttr,name,getUniteCode(branchmanager),(select a.name from laagent a where agentcode=labranchgroup.branchmanager),managecom,(select name from ldcom where comcode = labranchgroup.managecom),founddate,endflag,agentgroup,TrusteeShip from LABranchGroup where 1=1   "
	         + tReturn
	         + getWherePart('Name','Name','like')
	         + getWherePart('ManageCom','ManageCom','like')
	         + getWherePart('BranchAttr','BranchAttr','like')
	   	  if(fm.all("BranchManager").value!=""){
	   		strSQL +=" and branchmanager = getAgentCode('"+fm.BranchManager.value+"')";
	         }
	         + " order by BranchAttr";;
  if(document.fm.BranchManagerName.value != "" && document.fm.BranchManagerName.value != null)
  {// 加入主管姓名查询
  	strSQL += " and BranchManager in (select AgentCode from LAAgent where Name like '%25"+document.fm.BranchManagerName.value+"%25' and AgentState in ('01','02'))";
  }
	strSQL +=  getWherePart('BranchLevel')
	         + getWherePart('BranchType')
	         + getWherePart('BranchType2')
	         +" order by BranchAttr";
  //alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据！");
    return false;
    }
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BranchGroupGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

// 返回主界面
function returnParent()
{
    var arrReturn = new Array();
	var tSel = BranchGroupGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
	  try
	  {	
		arrReturn = getQueryResult();
		top.opener.afterQuery( arrReturn );
	  }catch(ex)
	  {
		alert( "没有发现父窗口的afterQuery接口。" + ex );
	  }
		top.close();
		
	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = BranchGroupGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	  return arrSelected;
	  
	arrSelected = new Array();
	var strSQL = "";
	strSQL = "select AgentGroup,Name,ManageCom,(Select name From ldcom where comcode =LABranchGroup.managecom ),BranchAttr,BranchType,BranchLevel,";
	strSQL = strSQL + "getUniteCode(BranchManager),BranchManagerName,BranchAddress,BranchZipcode,BranchPhone,FoundDate,EndFlag,(Select codename from ldcode where codetype='yesno' and code=LABranchGroup.endflag),EndDate,ComStyle,(select codename from ldcode where codetype='comstyle' and  code=LABranchGroup.comstyle),Operator,State,CostCenter";
	strSQL = strSQL + " from LABranchGroup where Agentgroup ='"+BranchGroupGrid.getRowColData(tRow-1,9)+"'  "; 
	     
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
    //判断是否查询成功
    if (!turnPage.strQueryResult) 
    {
     alert("没有符合条件的数据！");
     return false;
    }
    //查询成功则拆分字符串，返回二维数组
    arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	return arrSelected;
}



