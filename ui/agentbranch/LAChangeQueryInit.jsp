<%
//程序名称：LAChangeQueryInit.jsp
//程序功能：
//创建日期：2003-07-08 14:14:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{
  try
  {
//    fm.all('AgentCode').value = '';
    fm.all('AgentCom').value = '';   
    fm.all('RelaType').value = ''; 
  }
  catch(ex)
  {
    alert("在LAChangeQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();    
    initPlanGrid();
  }
  catch(re)
  {
    alert("LAChangeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化PlanGrid
 ************************************************************
 */
function initPlanGrid()
{                               
  var iArray = new Array();
   try
   {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=50;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="网点代码";         //列名
    iArray[1][1]="100px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=2;         //是否允许录入，0--不能，1--允许    

    iArray[2]=new Array();
    iArray[2][0]="网点名称";         //列名
    iArray[2][1]="180px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="变动日期";         //列名
    iArray[3][1]="100px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
    iArray[4]=new Array();
    iArray[4][0]="负责人";         //列名
    iArray[4][1]="100px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="负责渠道组";         //列名
    iArray[5][1]="100px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();
    iArray[6][0]="关联类型";         //列名
    iArray[6][1]="60px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="agentgroup";         //列名
    iArray[7][1]="0px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="MakeDate";         //列名
    iArray[8][1]="0px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="MakeTime";         //列名
    iArray[9][1]="0px";         //宽度
    iArray[9][2]=100;         //最大长度
    iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
  
        PlanGrid = new MulLineEnter( "fm" , "PlanGrid" ); 

        //这些属性必须在loadMulLine前
        PlanGrid.mulLineCount = 0;   
        PlanGrid.displayTitle = 1;
        PlanGrid.locked=1;
        PlanGrid.canSel=1;
        PlanGrid.canChk=0;
        PlanGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化PlanGrid时出错："+ ex);
      }
    }


</script>