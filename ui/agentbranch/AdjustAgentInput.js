 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

//  showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
    //fm.all('AdjustBranchCode').value = '';
    //BranchChange();
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//取消按钮对应操作
function cancelForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作


}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{

}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

//验证字段的值
function BranchConfirm()
{
	var tReturn = parseManageComLimitlike();
  if((trim(fm.all('BranchCode').value)=="")||(fm.all('BranchCode').value==null))
  {
    alert("请输入目标展业机构代码！");
    fm.all('BranchCode').focus();
    return false;
  }
  var strSQL = "";

  //查询出非停业且展业级别在营业组以上的
  strSQL = "select BranchAttr,Name,BranchType,BranchManager,BranchLevel,UpBranch,AgentGroup,Branchmanagername,BranchType2,(select groupagentcode from laagent where agentcode=LABranchGroup.BranchManager) from LABranchGroup where 1=1 "//xijh增加Branchmanagername
         + tReturn
	     +" and BranchAttr = '"+trim(fm.all('BranchCode').value)+"' and (EndFlag <> 'Y' or EndFlag is null) and (state<>'1' or state is null)"
	     +getWherePart('BranchType','BranchType')
	     +getWherePart('BranchType2','BranchType2');

  var strQueryResult = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!strQueryResult) {
  	//查询失败
    alert("该展业机构无效！");
    fm.all('BranchCode').value="";
    fm.all('BranchName').value = "";
    //fm.all('BranchType').value = "";
    fm.all('BranchManager').value = "";
    fm.all('hideBranchManager').value = '';
    fm.all('ManagerName').value ="";
    fm.all('hideAgentCode').value = "";
    fm.all('hideBranchLevel').value = "";
    fm.all('hideUpBranch').value = "";
    fm.all('hideAgentGroup').value = "";
    return false;
  }
  var arr = decodeEasyQueryResult(strQueryResult);
  //if (((trim(arr[0][2])=='3')&&(trim(arr[0][4])!='3'))||((trim(arr[0][2])=='1')&&(trim(arr[0][4])!='01')))//jiangcx add for BK
  if ((trim(arr[0][2])=='1')&&(trim(arr[0][8])=='01') && (trim(arr[0][4])!='01'))//xjh修改，保留原有银代部分，去掉个险部分，因为个险部分已经将调整范围扩大
  {
  	//判定展业机构类型
    alert('业务员只能在营业组间调动！');
    fm.all('BranchCode').value="";
    fm.all('BranchName').value = "";
    //fm.all('BranchType').value = "";
    fm.all('BranchManager').value = "";
    fm.all('hideBranchManager').value = '';
    fm.all('ManagerName').value ="";
    fm.all('hideAgentCode').value = "";
    fm.all('hideBranchLevel').value = "";
    fm.all('hideUpBranch').value = "";
    fm.all('hideAgentGroup').value = "";
    return false;

  }
  fm.all('BranchName').value = arr[0][1];
  fm.all('BranchType').value = arr[0][2];
  fm.all('BranchManager').value = arr[0][9];//集团工号
  fm.all('hideBranchManager').value = arr[0][3];//内部工号
	fm.all('ManagerName').value = arr[0][7];//xjh增加，显示机构管理人名称
  fm.all('hideBranchLevel').value = arr[0][4]; 
  fm.all('hideUpBranch').value = arr[0][5];
  fm.all('hideAgentGroup').value = arr[0][6];

  if(arr[0][2]=='2'){
  	//如果是法人则不作目标管理人员检测。
  	//fm.BranchManager.style.readonly='true';此句无效
  	return true;
  }

  if((arr[0][3]==null)||(trim(arr[0][3])==''))
  {
  	alert('请先确定目标机构的管理人员！');
    fm.all('BranchManager').value = '';
  	fm.all('hideBranchManager').value = '';
  	fm.all('hideAgentCode').value = '';
  	fm.all('BranchManager').focus();
  	return false;
  }
  else
  {
  	strSQL = "select Name,AgentCode,groupagentcode from LAAgent where AgentCode = '"+trim(arr[0][3])+"'";
  	strQueryResult = easyQueryVer3(strSQL,1,0,1);
  	if (!strQueryResult)
  	{
  	   fm.all('BranchManager').value = '';
  	   fm.all('hideBranchManager').value = '';
  	   fm.all('hideAgentCode').value = '';
  	   //fm.BranchManager.disabled = false;
  	   alert('请先确定目标机构的管理人员！');
  	   fm.all('BranchManager').focus();
  	   return false;
  	}
  	else
  	{
  	   arr = decodeEasyQueryResult(strQueryResult);
           fm.all('ManagerName').value = arr[0][0];
  	   fm.all('hideAgentCode').value = arr[0][1];
  	   fm.all('BranchManager').value = arr[0][2];//集团工号
       fm.all('hideBranchManager').value = arr[0][1];//内部工号
  	   //fm.BranchManager.disabled = true;
  	   //alert('ssds');
  	}
  }
  return true;

}
function changeManager()
{
   if (getWherePart('BranchManager')=='')
     return false;
   var strSQL = "select AgentCode,Name from LAAgent where 1=1 and (AgentState is null or AgentState < '03')"
                + getWherePart('AgentCode','BranchManager')
                + getWherePart('AgentGroup','BranchCode','<>');
//        alert(strSQL);
   var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
   if (!strQueryResult)
   {
   	alert('该代理人无效！');
   	fm.all('BranchManager').value = '';
   	fm.all('ManagerName').value = '';
   	return false;
   }
   var arr = decodeEasyQueryResult(strQueryResult);
   fm.all('ManagerName').value = arr[0][1];
}

function queryAgent()
{
   var tReturn = getManageComLimitlike("c.ManageCom");
   var  tBranchCode=fm.all('BranchCode').value;
   var  tManageCom=tBranchCode.substring(0,8);
  // alert(tManageCom);
   if((fm.all('BranchCode').value==null)||(trim(fm.all('BranchCode').value)==""))
   {
      alert("请先确定目标展业机构！");
      fm.all('BranchCode').focus();
      return false;
   }
  
   if(trim(fm.all('BranchCode').value) == trim(fm.all('AgentGroup').value))
   {
   	alert('调动人员所属展业机构不能和目标机构相同！');
   	fm.all('AgentGroup').focus();
   	return false;
   }
  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;
  var tBranchType2 = fm.all('BranchType2').value;
  if(tBranchType==1 && tBranchType2=='01')

  strSQL = "select b.groupAgentCode,b.Name,c.BranchAttr,a.AgentGrade,a.agentcode from LATree a,LAAgent b,LABranchGroup c where 1=1 and (AgentGrade <= 'B01') "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('b.groupAgentCode','AgentCode')
	     + getWherePart('a.AgentGrade','AgentGrade')	     
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup  "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"' and  c.managecom ='"+tManageCom+"' " 
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"' ";

	else if(tBranchType==3)

		strSQL = "select b.groupAgentCode,b.Name,c.BranchAttr,a.AgentGrade,a.agentcode from LATree a,LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('b.groupAgentCode','AgentCode')
	     + getWherePart('a.AgentGrade','AgentGrade')
	     + getWherePart('b.groupAgentCode','BranchManager','<>')
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"' and b.agentkind>'03' and  c.managecom ='"+tManageCom+"' "
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"'";

	     else if(tBranchType==2 && tBranchType2=='01')

		strSQL = "select b.groupAgentCode,b.Name,c.BranchAttr,a.AgentGrade,a.agentcode from LATree a,LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','AgentGroup')
	     + getWherePart('b.groupAgentCode','AgentCode')	    
	     + getWherePart('a.AgentGrade','AgentGrade')
	     + getWherePart('b.groupAgentCode','BranchManager','<>')
	     +tReturn
	     + " and c.AgentGroup = a.AgentGroup and a.agentgrade<'E01'  "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and c.branchtype='"+tBranchType+"' and  c.managecom ='"+tManageCom+"' "
	     + " and c.BranchType2='"+fm.all('BranchType2').value+"' and c.BranchAttr!='"+fm.all('BranchCode').value+"'";;

  // alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
  	alert('查询失败,不存在有效纪录！');
  	return false;
  }
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,3,4]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentGrid;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

function submitSave()
{
   var mCount = AgentGrid.mulLineCount;
   if ((mCount == null)||(mCount == 0))
   {
   	alert("请查询出要调动的人员！");
   	return false;
   }
   //submitForm();

   var isChk=false;
   for (i=0;i<mCount;i++)
   {
   	if( AgentGrid.getChkNo( i ))
   	{
   	   isChk=true;
   	   break;
   	}
   }
   if (isChk==false)
   {
     alert("请选出要调动的人员！");
     return false;
   }
   else
     submitForm();

}

function clearAll()
{
   fm.all('BranchCode').value = '';
   //fm.all('BranchType').value = '';
   fm.all('BranchName').value = '';
   fm.all('BranchManager').value = '';
   fm.all('ManagerName').value = '';
   fm.all('hideAgentCode').value = '';
   fm.all('hideBranchLevel').value = '';
   fm.all('hideUpBranch').value='';
   fm.all('hideAgentGroup').value='';
   clearGrid();
}
function clearGrid()
{
   fm.all('AgentGroup').value = '';
   fm.all('AgentCode').value = '';
  // fm.all('AgentSeries').value = '';
   fm.all('AgentGrade').value = '';
   AgentGrid.clearData("AgentGrid");
}
