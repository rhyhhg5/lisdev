//该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}
//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  if (fm.all('hideOperate').value =="" && fm.all('idx').value=="" ){
    if(fm.all('idx').value!=null && fm.all('idx').value!=''){
       alert('查询出的数据，不能进行保存操作！');    
       return false;	
    }
	fm.all('hideOperate').value="INSERT||MAIN";
  }
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    if(fm.all('hideOperate').value!=null && fm.all('hideOperate').value !='' && fm.all('hideOperate').value =="UPDATE||MAIN"){
    	fm.all('ResidentType').disabled = 'true';
		fm.all('ResidentTypeName').disabled = 'true';
    }
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px"); 
    initInpBox();
  }
 
}

function afterCodeSelect(codeName,Field)
{
	if(codeName == 'ResidentType')
	{
	   if(fm.all('ResidentType').value=='1'){
		   fm.all('rpcdi').style.display='';
		   fm.all('rpcni').style.display='none';
		   fm.all('RecentPCDI').value = '';
		   fm.all('RecentPCNI').value = '';
	   }else if(fm.all('ResidentType').value=='2'){
	   	   fm.all('rpcdi').style.display='none';
	   	   fm.all('rpcni').style.display='';
	   	   fm.all('RecentPCDI').value = '';
		   fm.all('RecentPCNI').value = '';
	   }
	}
}

         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	  parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   


function queryClick()
{	 
	showInfo=window.open("./LAResidentIncomeQueryHtml.jsp");	
}

function updateClick()
{
    
	if( verifyInput() == false) return false; 
	if(!beforeSubmit()) return false; 
	if(fm.all('idx').value==null || fm.all('idx').value==''){
       alert('请先查询数据，再进行修改！');    
       return false;	
    }
    if (confirm("您确实想修改该记录吗?"))
    {
      fm.all('hideOperate').value="UPDATE||MAIN";
      fm.all('ResidentType').disabled = '';
      fm.all('ResidentTypeName').disabled = '';
      submitForm();
    }
    else
    {
      fm.all('hideOperate').value="";
      alert("您取消了修改操作！");
    }
}
function deleteClick()
{
	if( verifyInput() == false ) return false; 
	if(!beforeSubmit()) return false;
	if(fm.all('idx').value==null || fm.all('idx').value==''){
       alert('请先查询数据，再进行删除！');    
       return false;	
    }
    if (confirm("您确实想删除该记录吗?"))
    {
      fm.all('hideOperate').value="DELETE||MAIN";
      submitForm();
    }
    else
    {
     fm.all('hideOperate').value="";
      alert("您取消了删除操作！");
    }
}


function beforeSubmit()
{
   if(fm.all('ResidentType').value=='1'){
   	   if(fm.all('RecentPCDI').value==null || fm.all('RecentPCDI').value==''){
   	   	  alert('最近年度居民人均可支配收入不能为空！');
   	   	  return false;
   	   }
   }else if(fm.all('ResidentType').value=='2'){
   	   if(fm.all('RecentPCNI').value==null || fm.all('RecentPCNI').value==''){
   	   	  alert('最近年度居民人均纯收入不能为空！');
   	   	  return false;
   	   }
   }
   
   var startDate = fm.all('StartDate').value;
	var endDate = fm.all('EndDate').value;
	if(startDate>endDate){
	   alert("终止日期要大于或等于起始日期！");
	   return false;
	}
   return true;
}


function afterQuery(arrQueryResult)
{
    var arrResult = new Array();
	if( arrQueryResult!= null)
	{
	    initInpBox();
		arrResult = arrQueryResult;
		fm.all('idx').value = arrResult[0][0];
		fm.all('ResidentType').value = arrResult[0][2];
		fm.all('ManageCom').value = arrResult[0][1];
		fm.all('StartDate').value = arrResult[0][3];
		fm.all('EndDate').value = arrResult[0][4];		
		fm.all('RecentPCDI').value = arrResult[0][5];
		fm.all('RecentPCNI').value = arrResult[0][6];
		
		fm.all('ResidentType').disabled = 'true';
		fm.all('ResidentTypeName').disabled = 'true';
		if(fm.all('ResidentType').value=='1'){
		   fm.all('rpcdi').style.display='';
		   fm.all('rpcni').style.display='none';
	   }else if(fm.all('ResidentType').value=='2'){
	   	   fm.all('rpcdi').style.display='none';
	   	   fm.all('rpcni').style.display='';
	   }
   }
	showOneCodeNametoAfter('comcode','ManageCom','ManagecomName');
	showOneCodeNametoAfter('ResidentType','ResidentType','ResidentTypeName');
}



