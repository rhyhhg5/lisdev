<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAChangeComAgentSave.jsp
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  
  LAComToAgentSchema tLAComToAgentSchema ;  
  ALAComToAgentUI tALAComToAgentUI   = new ALAComToAgentUI();

  //输出参数
  CErrors tError = null;
  String tOperate = request.getParameter("hideOperate");
  tOperate = tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String AgentComB = request.getParameter("AgentComB");
  tLAComToAgentSchema=new LAComToAgentSchema();
  tLAComToAgentSchema.setAgentCom(request.getParameter("AgentCom"));//多行表格
  tLAComToAgentSchema.setRelaType(request.getParameter("RelaType"));
  tLAComToAgentSchema.setAgentCode(request.getParameter("AgentCode"));
  tLAComToAgentSchema.setOperator(tG.Operator);

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.addElement(tLAComToAgentSchema);
  tVData.add(tG);
  tVData.add(AgentComB);
  try
  {
    System.out.println("start to UI");
    tALAComToAgentUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tALAComToAgentUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功! ";
      FlagStr = "Succ";
    }
    else                                                                           
    {
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">  
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>


