//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm(){

  /*中英的系统不需要检测上级机构。
  if (!queryBranchAttr())
    return false;
  */

//  showSubmitFrame(1);

	if (mOperate!="UPDATE||MAIN")
		mOperate="INSERT||MAIN";
	//如果是新增操作，则需要将主管编码和主管姓名清空，否则会传入后台，如果是更新操作，则不需要如此处理
	if (mOperate=="INSERT||MAIN"){
		fm.all('BranchManager').value = "";
		fm.all('BranchManagerName').value = "";
	}

	fm.hideOperate.value=mOperate;
	if (fm.hideOperate.value==""){
		alert("操作控制数据丢失！");
	}
	if (!beforeSubmit())
		return false;
	if (!changeGroup())
		return false;
    fm.all('State').disabled = false;//此标记不能修改		
    fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  fm.all('State').disabled = true;//此标记不能修改	
  //showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
	mOperate = "";
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LABranchGroup.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit(){
	if( verifyInput() == false ) return false;
	//var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	//var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	//fm.submit(); //提交
	//showInfo.close();
	var cManageCom = fm.all('ManageCom').value;
	var cBranchAttr = fm.all('BranchAttr').value;
	//xjh Modify for PICCH,2005/2/17,修改使其一致
	if (cManageCom.substring(0,cManageCom.length) != cBranchAttr.substring(0,cManageCom.length)){
	//if (cManageCom.substring(0,4) != cBranchAttr.substring(0,4)){
		alert("销售团队代码录入错误，请查询！");
		fm.all('BranchAttr').focus();
		return false;
	}
	if(cManageCom.length < 8)
	{
		var a = "0000000000000000";
		if(cBranchAttr.substring(cManageCom.length,8) != a.substring(cManageCom.length,8))
		{
		alert("销售团队代码录入错误，请查询！");
		fm.all('BranchAttr').focus();
		return false;
		}
	}
	if(mOperate=="INSERT||MAIN"){
		if(fm.all("State").value=="1")
 		{
 	 	//查询管理机构下是否已经建立公司业务团队了
 	 		var SQL="select branchattr from labranchgroup where managecom like '"+fm.all('ManageCom').value+"%' and state='1' and endflag='N' ";
 	   		var strQueryResult = easyQueryVer3(SQL, 1, 1, 1); 	  
 	    	if(strQueryResult)
 	    	{
 	     		var arr1 = decodeEasyQueryResult(strQueryResult); 
 	     		alert("该机构下已经建立公司业务团队'"+arr1[0][0]+"'!,一个机构只能建立一个公司业务团队！");
 	     		return false;
 	    	}	            
 		}
 		var tempSQL="select 1 from labranchgroup where branchtype='2' and endflag<>'Y' and name='"
 		+fm.all('BranchName').value+"'";
 		var tempQueryResult = easyQueryVer3(tempSQL, 1, 1, 1); 	  
 	    	if(tempQueryResult)
 	    	{
 	     		alert("销售团队名称重复！该销售团队名称在数据库中已经存在，请录入新的销售团队名称。");
 	     		return false;
 	    	}	            
	}else if(mOperate=="UPDATE||MAIN" && fm.all('BranchName').value!=fm.all('hiddenBranchName').value){
		var checkUpdateSQL="select 1 from labranchgroup where branchtype='2' and endflag<>'Y' and name='"
 		+fm.all('BranchName').value+"'";
 		var checkUpdateQueryResult = easyQueryVer3(checkUpdateSQL, 1, 1, 1); 	  
 	    	if(checkUpdateQueryResult)
 	    	{
 	     		alert("销售团队名称重复！该销售团队名称在数据库中已经存在，请录入新的销售团队名称。");
 	     		fm.all('BranchName').value=fm.all('hiddenBranchName').value;
 	     		return false;
 	    	}	            
	}
	
	if(fm.all("BranchStyle").value=="01" )
	{
		 if(fm.all("ComStyle").value!=null && fm.all("ComStyle").value!="")
		 {
		 	  alert("二级机构营业部不能录入机构地区类别!");
		 	  return false ;
		 }
	}
 else if(fm.all("BranchStyle").value=="02" ) 
  {
     
		 if(fm.all("ComStyle").value==null || fm.all("ComStyle").value=="")
		 {
		 	  alert("三级机构营业部必需录入机构地区类别!");
		 	  return false ;
		 }  		
  		
 	}
 	//公司业务属性的判断，一个分公司下只有建立一个公司业务团队，一个支公司下也只能建立一个公司业务团队
 	
 	
 	
 	//对成本中心编码进行校验
 	if(fm.ManageCom.value=="")
 	{
 		alert("所属管理机构不能为空！");
 		return false;
 	}
 	if(!checkSap(fm.CostCenter.value,fm.ManageCom.value))
 	{
 		return false;
 	}
	return true;

}

//对成本中心编码进行校验
function checkSap(CostCenter,tManageCom)
{
 var erroInfo="成本中心代码有误，请根据发文《人保健康计财[2008] 212号》申请成本中心代码！";
 var tt=CostCenter.substr(4,2);
 if(tt!="90"&&tt!="91"&&tt!="92")
 {
 	alert(erroInfo);
 	return false;
 } 

 var tSQL="select codealias from licodetrans  where codetype='ManageCom' and code='"+tManageCom+"'";
 var strQueryResult1 = easyQueryVer3(tSQL, 1, 1, 1); 	  
 var arr1 = decodeEasyQueryResult(strQueryResult1);
 if(!arr1)
 {
 	alert("SAP没有定义公司编码，请联系SAP定义公司编码！"); 
 	return false;
 }else{
	var ss=arr1[0][0]
 	if(ss!=CostCenter.substr(0,4))
 	{
 	alert(erroInfo);
 	return false;
	 }
 }
 return 1;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,500,82,*";
  }
  else
  {
 	parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
  fm.all('BranchManager').value ='';
  fm.all('BranchManagerName').value ='';
}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (!((fm.all("EndDate").value==null)||(trim(fm.all("EndDate").value)==''))&&(trim(fm.all("EndFlag").value)=='N'))
    {alert("停业属性为N，不能置停业日期！");
    return false;}

  //下面增加相应的代码

  if ((fm.all("BranchAttr").value==null)||(trim(fm.all("BranchAttr").value)==''))
    alert("请确定要修改的销售团队！");
  else
  {
    if (!queryBranchCode())
      return false;

    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }

}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
//  mOperate="QUERY||MAIN";

    var tBranchType = "2";
    var tBranchType2 = "01";
  showInfo=window.open("./LABGGrpQuery.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function afterQuery(arrQueryResult)
{
var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		 document.all.BranchAttr.readOnly="true";
		fm.all('AgentGroup').value = arrResult[0][0];
		fm.all('BranchName').value = arrResult[0][1];
		//fm.all('hiddenBranchName').value = arrResult[0][1];
		fm.all('ManageCom').value = arrResult[0][2];
    if(fm.all('ManageCom').value!="" && fm.all('ManageCom').value!=null)
		{
		  var Sql_Name="select Name from ldcom where comcode='"+fm.all('ManageCom').value+"' ";
      var strQueryResult_Name  = easyQueryVer3(Sql_Name, 1, 1, 1);
		  if (strQueryResult_Name)
	     {
		    var arr = decodeEasyQueryResult(strQueryResult_Name);
	      fm.all('ManageComName').value= trim(arr[0][0]) ;
		   }
		 }      		
		fm.all('UpBranch').value = arrResult[0][3];
    fm.all('BranchAttr').value = arrResult[0][4];
		fm.all('BranchType').value = arrResult[0][5];
		fm.all('BranchLevel').value = arrResult[0][6];
		if(fm.all('BranchLevel').value!="" && fm.all('BranchLevel').value!=null)
		  {
		  var Sql_Name="select branchlevelname from labranchlevel where branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"' and  branchlevelcode='"+fm.all('BranchLevel').value+"' ";
      var strQueryResult_Name  = easyQueryVer3(Sql_Name, 1, 1, 1);
      //alert(strQueryResult_Name);
		  if (strQueryResult_Name)
	    {
	      var arr = decodeEasyQueryResult(strQueryResult_Name);
	      fm.all('BranchLevelName').value= trim(arr[0][0]) ;
		  }
		 }       
		fm.all('BranchManager').value = arrResult[0][7];
		fm.all('BranchAddress').value = arrResult[0][8];
		fm.all('BranchPhone').value = arrResult[0][9];
		fm.all('BranchZipcode').value = arrResult[0][10];
		fm.all('FoundDate').value = arrResult[0][11];
		fm.all('EndDate').value = arrResult[0][12];
		fm.all('EndFlag').value = arrResult[0][13];
		fm.all('CostCenter').value = arrResult[0][25];
		fm.all('tCostCenter').value = arrResult[0][25];
		if(fm.all('EndFlag').value!="" && fm.all('EndFlag').value!=null)
		  {
		  var Sql_Name="select codename from ldcode where codetype='yesno' and code='"+fm.all('EndFlag').value+"' ";
      var strQueryResult_Name  = easyQueryVer3(Sql_Name, 1, 1, 1);
      //alert(strQueryResult_Name);
		  if (strQueryResult_Name)
		  {
		    var arr = decodeEasyQueryResult(strQueryResult_Name);
		    fm.all('EndFlagName').value= trim(arr[0][0]) ;
		  }
		 }    
		fm.all('FieldFlag').value = (arrResult[0][14]=="1"?"Y":"N");
	}
	if (arrResult[0][6]!="1")
	{
		var strSQL = "";
  		strSQL = "select BranchAttr from LABranchGroup where agentgroup='"+arrResult[0][3]+"'   ";
	    var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
	    if (strQueryResult)
	    {
	      var arr = decodeEasyQueryResult(strQueryResult);
	      fm.all('UpBranchAttr').value=arr[0][0];
	    }
	}
	else
  {
    fm.all('UpBranchAttr').value="";
  }
	fm.all('Operator').value = arrResult[0][15];
	fm.all('BranchManagerName').value = arrResult[0][16];
	
	fm.all('State').value=arrResult[0][22];
	showOneCodeNametoAfter('branchstate','State');
	fm.all('State').disabled = true;//此标记不能修改
	
	fm.all('BranchStyle').value = arrResult[0][23];
	if(fm.all('BranchStyle').value!="" && fm.all('BranchStyle').value!=null)
		{
		  var Sql_Name="select codename from ldcode where  codetype='branchstyle'  and code='"+fm.all('BranchStyle').value+"' ";
      var strQueryResult_Name  = easyQueryVer3(Sql_Name, 1, 1, 1);
		  if (strQueryResult_Name)
	     {
		    var arr = decodeEasyQueryResult(strQueryResult_Name);
	      fm.all('BranchStyleName').value= trim(arr[0][0]) ;
		   }
		 }  
	
	fm.all('ComStyle').value = arrResult[0][24];
	if(fm.all('ComStyle').value!="" && fm.all('ComStyle').value!=null)
		{
		  var Sql_Name="select codename from ldcode where codetype='comstyle' and  code='"+fm.all('ComStyle').value+"' ";
      var strQueryResult_Name  = easyQueryVer3(Sql_Name, 1, 1, 1);
		  if (strQueryResult_Name)
	     {
		    var arr = decodeEasyQueryResult(strQueryResult_Name);
	      fm.all('ComStyleName').value= trim(arr[0][0]) ;
		   }
		 }     
	
	
}

function queryBranchCode()
{
  // 书写SQL语句
  var strSQL = "";
 // var tReturn = getManageComLimitlike("managecom");
  strSQL = "select AgentGroup,UpBranch from LABranchGroup where 1=1 and (EndFlag is null or EndFlag <> 'Y')   "
	  	 //+tReturn
	  	 + getWherePart('BranchAttr')
	         + getWherePart('BranchType')
	         + getWherePart('BranchType2');
  var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);
//alert(strSQL);
  if (!strQueryResult) {
    alert("不存在所要操作的销售单位！");
    fm.all("BranchAttr").value = '';
    fm.all('AgentGroup').value = '';
    fm.all('UpBranch').value = '';
    return false;
  }

  var arr = decodeEasyQueryResult(strQueryResult);
  fm.all('AgentGroup').value = arr[0][0];
  fm.all('UpBranch').value = arr[0][1];
  return true;
}

function queryBranchAttr()
{
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select AgentGroup from LABranchGroup where 1=1 and (EndFlag is null or EndFlag <> 'Y')   "
	  + getWherePart('BranchAttr','UpBranchAttr')
	  + getWherePart('BranchType')
	  + getWherePart('BranchType2');

  var strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  if (!strQueryResult) {
    alert("不存在上级销售团队！");
    fm.all("BranchAttr").value = '';
    fm.all('AgentGroup').value = '';
    fm.all('UpBranch').value = '';
    return false;
  }

  if (((fm.all("EndDate").value==null)||(trim(fm.all("EndDate").value)==''))&&(trim(fm.all("EndFlag").value)=='Y'))
    {  alert("停业属性为Y，请置停业日期！");
    return false;  }


  var arr = decodeEasyQueryResult(strQueryResult);
  fm.all('UpBranch').value = arr[0][0];
  return true;
}

function changeGroup()
{
	if(fm.all('EndFlag').value == null || fm.all('EndFlag').value == "")
	{
		//alert("停业属性为空，请置位！");
		return false;
	}
	if(fm.all('EndFlag').value == 'Y' && (fm.all('EndDate').value == null || fm.all('EndDate').value == ""))
  {
  	alert("停业属性为Y，请置停业日期！");
    return false;
  }
	if(fm.all('EndFlag').value == 'N' && fm.all('EndDate').value != "")
  {  
  	alert("停业属性为N，不能置停业日期！");
  	fm.all('EndDate').value = "";
    return false;
  }
  return true;
}
