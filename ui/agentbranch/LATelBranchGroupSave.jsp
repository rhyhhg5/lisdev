<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LATelBranchGroupSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentbranch.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
  ALABranchGroupUI tLABranchGroup = new ALABranchGroupUI();
  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  System.out.println("hideOperate:----"+request.getParameter("hideOperate"));
  System.out.println("operater:--"+tOperate);
  tOperate=tOperate.trim();
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

    tLABranchGroupSchema.setAgentGroup(request.getParameter("AgentGroup"));
    tLABranchGroupSchema.setName(request.getParameter("BranchName"));
    tLABranchGroupSchema.setManageCom(request.getParameter("ManageCom"));
    System.out.println("UpBranchAttr:"+request.getParameter("UpBranchAttr"));
    if(request.getParameter("UpBranchAttr")==null||request.getParameter("UpBranchAttr").equals(""))
      tLABranchGroupSchema.setUpBranch("");
    else
      tLABranchGroupSchema.setUpBranch(request.getParameter("UpBranch"));
    System.out.println("UpBranch:"+tLABranchGroupSchema.getUpBranch().trim());
    tLABranchGroupSchema.setBranchAttr(request.getParameter("BranchAttr"));
    tLABranchGroupSchema.setBranchType(request.getParameter("BranchType"));
    tLABranchGroupSchema.setBranchType2(request.getParameter("BranchType2"));
    tLABranchGroupSchema.setInsideFlag(request.getParameter("InsideFlag"));
    tLABranchGroupSchema.setBranchLevel("31");
    tLABranchGroupSchema.setBranchManager(request.getParameter("BranchManager"));
    tLABranchGroupSchema.setBranchManagerName(request.getParameter("BranchManagerName"));
    tLABranchGroupSchema.setBranchAddressCode(request.getParameter("BranchAddressCode"));
    tLABranchGroupSchema.setBranchAddress(request.getParameter("BranchAddress"));
    tLABranchGroupSchema.setBranchPhone(request.getParameter("BranchPhone"));
    tLABranchGroupSchema.setBranchFax(request.getParameter("BranchFax"));
    tLABranchGroupSchema.setBranchZipcode(request.getParameter("BranchZipcode"));
    tLABranchGroupSchema.setFoundDate(request.getParameter("FoundDate"));
    tLABranchGroupSchema.setAStartDate(request.getParameter("FoundDate"));
    tLABranchGroupSchema.setEndDate(request.getParameter("EndDate"));
    tLABranchGroupSchema.setEndFlag(request.getParameter("EndFlag"));
    tLABranchGroupSchema.setCostCenter(request.getParameter("CostCenter"));
    //tLABranchGroupSchema.setCertifyFlag(request.getParameter("CertifyFlag"));
    //String BranchJobType=(request.getParameter("FieldFlag").equals("Y"))?"1":"0";
    //tLABranchGroupSchema.setBranchJobType(BranchJobType);/*jiangcx 是否是外勤字段*/
    tLABranchGroupSchema.setState(request.getParameter("State"));
    tLABranchGroupSchema.setOperator(request.getParameter("Operator"));
    //tLABranchGroupSchema.setUpBranchAttr("");/*jiangcx 上级机构代码*/
    System.out.println("operater:--"+tOperate);
    System.out.println("BranchType:--"+request.getParameter("BranchType"));
    System.out.println("BranchType2:--"+request.getParameter("BranchType2"));


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLABranchGroupSchema);
	tVData.add(tG);
  try
  {
    tLABranchGroup.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABranchGroup.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
parent.fraInterface.fm.all('Operator').value = "<%=tG.Operator%>";
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>