//客户端程序

var turnPage = new turnPageClass();

/***************************************************
 * 获得被合并机构相关信息
 ***************************************************
 */
function getGroupU()
{
	var tBranchAttrU = document.fm.BranchAttrU.value;
	var tGroupArr;
	var tSQL;
	//机构代码没有填写内容
	if (trim(tBranchAttrU) == "")
	{
		initInpBox2();
		return false;
	}
	
	tGroupArr = getQueryResult(tBranchAttrU);
	//查询数据无结果
	if(tGroupArr == false)
	{
		initInpBox2();
		return false;
	}
 tSQL = "select count(*) from LAAgent b,LABranchGroup c where 1=1 "
   + getWherePart('c.BranchAttr','BranchAttrU')
   + getWherePart('b.AgentCode','AgentCodeU','<>')
   + getWherePart('c.ManageCom','ManageCommU')
   + " and c.AgentGroup = b.AgentGroup "
   + " and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
   + " and b.branchtype='2' "
   + " and b.BranchType2='01'";
	//document.fm.BranchAttrU.value="";
	document.fm.AgentGroupU.value=tGroupArr[0][0];
	document.fm.BranchNameU.value=tGroupArr[0][1];
	document.fm.ManageCommU.value=tGroupArr[0][2];
	document.fm.BranchLevelU.value=tGroupArr[0][6];
	document.fm.AgentCodeU.value=tGroupArr[0][7];
	document.fm.NameU.value=tGroupArr[0][16];
	document.fm.FoundDateU.value=tGroupArr[0][11];
	document.fm.AgentCountU.value=getAgentCount(tGroupArr[0][0],tSQL);
	
	//设置被合并团队管理员信息
	if(trim(tGroupArr[0][7])=="" || !setGroupManagerU(tGroupArr[0][7]))
	{
		//initInpBox3();
	}
}

/***************************************************
 * 设置被合并团队管理员信息
 ***************************************************
 */
function setGroupManagerU(pmGroupManager)
{
	 var arrSelected;
 	 var tSQL ="";
 	 tSQL  ="select a.agentcode,a.name,b.agentgrade";
 	 tSQL +="  from laagent a,latree b";
 	 tSQL +=" where a.agentcode='"+pmGroupManager+"'";
 	 tSQL +="   and a.agentcode=b.agentcode";
 	 
 	 turnPage.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);  
   //判断是否查询成功
   if (!turnPage.strQueryResult) {
     alert("查询被合并团体管理员失败！");
     return false;
   }
   
   //查询成功则拆分字符串，返回二维数组
   arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
   
   document.fm.GroupManager.value = arrSelected[0][0];
   document.fm.GroupManagerName.value = arrSelected[0][1];
   document.fm.AgentGradeOld.value = arrSelected[0][2];
   
   return true;
}

/***************************************************
 * 获得目标机构相关信息
 ***************************************************
 */
function getGroupArm()
{
	var tBranchAttr = document.fm.BranchAttr.value;
	var tGroupArr;
	var tSQL;
	//机构代码没有填写内容
	if (trim(tBranchAttr) == "")
	{
		initInpBox1();
		return false;
	}
	
	tGroupArr = getQueryResult(tBranchAttr);
	//查询数据无结果
	if(tGroupArr == false)
	{
		initInpBox1();
		return false;
	}
	
	 tSQL = "select count(*) from LAAgent b,LABranchGroup c where 1=1 "
   + getWherePart('c.BranchAttr','BranchAttr')
   + getWherePart('b.AgentCode','AgentCode','<>')
   + getWherePart('c.ManageCom','ManageComm')
   + " and c.AgentGroup = b.AgentGroup "
   + " and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
   + " and b.branchtype='2' "
   + " and b.BranchType2='01'";
	
	//document.fm.BranchAttr.value="";
	//document.fm.BranchAttrC.value=tBranchAttr;
	document.fm.AgentGroupA.value=tGroupArr[0][0];
	document.fm.BranchName.value=tGroupArr[0][1];
	//document.fm.BranchNameC.value=tGroupArr[0][1];
	document.fm.ManageComm.value=tGroupArr[0][2];
	document.fm.BranchLevel.value=tGroupArr[0][6];
	document.fm.AgentCode.value=tGroupArr[0][7];
	document.fm.Name.value=tGroupArr[0][16];
	document.fm.FoundDate.value=tGroupArr[0][11];
	document.fm.AgentCount.value=getAgentCount(tGroupArr[0][0],tSQL);
}

/***************************************************
 * 对于目标机构主管的调整，指定机构的查询
 ***************************************************
 */
 function getGroupChange()
 {
 	 var tBranchAttr = document.fm.BranchAttrC.value;
 	 
 	 var tGroupArr = getQueryResult(tBranchAttr);
 	 
	 document.fm.BranchNameC.value=tGroupArr[0][1];
 }

/***************************************************
 * 查询指定团队信息
 ***************************************************
 */
 function getAgentCount(pmAgentGroup,pmSQL)
 {
 	 var tSQL ="";
 	 var tBranchType = document.fm.BranchType.value;
 	 var tBranchType2 = document.fm.BranchType2.value;
 	 
 	 tSQL = "select count(*) from LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','BranchAttr')
	     + getWherePart('b.AgentCode','AgentCode','<>')
	     + getWherePart('c.ManageCom','ManageComm')
	     + " and c.AgentGroup = b.AgentGroup "
	     + " and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and b.branchtype='2' "
	     + " and b.BranchType2='01'";
 	 
 	 turnPage.strQueryResult  = easyQueryVer3(pmSQL, 1, 0, 1);
   //判断是否查询成功
   if (!turnPage.strQueryResult) {
     alert("查询机构内人数失败！");
     return 0;
   }
   
   //查询成功则拆分字符串，返回二维数组
   arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	 return arrSelected[0][0];
 }

/***************************************************
 * 查询指定团队信息
 ***************************************************
 */
 function getQueryResult(pmBranchAttr)
 {
	 var arrSelected = new Array();

   // 书写SQL语句
	 var strSQL = "";
	 strSQL  = "SELECT";
	 strSQL += "    AgentGroup,";
	 strSQL += "    Name,";
	 strSQL += "    ManageCom,";
	 strSQL += "    UpBranch,";
	 strSQL += "    BranchAttr,";
	 strSQL += "    BranchType,";
	 //strSQL += "    BranchLevel,";
	 strSQL += "    (SELECT";
	 strSQL += "         trim(b.BranchLevelCode) || ' - ' || b.BranchLevelName";
	 strSQL += "       FROM";
	 strSQL += "         LABranchLevel b";
	 strSQL += "      WHERE";
	 strSQL += "         b.BranchType='2' AND";
	 strSQL += "         b.BranchType2='01' AND";
	 strSQL += "         b.BranchLevelCode = BranchLevel) as BranchLevel,";
	 strSQL += "    BranchManager,";
	 strSQL += "    BranchAddress,";
	 strSQL += "    BranchPhone,";
	 strSQL += "    BranchZipcode,";
	 strSQL += "    FoundDate,";
	 strSQL += "    EndDate,";
	 strSQL += "    EndFlag,";
	 strSQL += "    FieldFlag,";
	 strSQL += "    Operator,";
	 strSQL += "    BranchManagerName,";
	 strSQL += "    BranchFax,";
	 strSQL += "    UpBranchAttr,";
	 strSQL += "    BranchType2,";
	 strSQL += "    TrusteeShip";
	 strSQL += " FROM";
	 strSQL += "    LABranchGroup";
	 strSQL += " WHERE";
	 strSQL += "    BranchAttr ='" + pmBranchAttr + "' and branchtype='2' and  branchtype2='01'";
	 strSQL += "   AND (state<>'1' or state is null)";
	     
	 turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
   //判断是否查询成功
   if (!turnPage.strQueryResult) {
     alert("没有此编码的单位！");
     return false;
   }
   
   //查询成功则拆分字符串，返回二维数组
   arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	 return arrSelected;
 }
 
/***************************************************
 * 验证目标单位和被合并单位是否未填入
 ***************************************************
 */
 function checkNullGroup()
 {
 	 var tBranchAttrAim = document.fm.BranchAttr.value;
 	 var tBranchAttrU = document.fm.BranchAttrU.value;
 	 var tAdjustDate = document.fm.AdjustDate.value;
 	 
 	 if (trim(tBranchAttrAim) == "")
 	 {
 	 	 alert("请指定目标管理机构！");
 	 	 return false;
 	 }
 	 
 	 if (trim(tBranchAttrU) == "")
 	 {
 	 	 alert("请指定被合并管理机构！");
 	 	 return false;
 	 }
 	 
 	 if(trim(tAdjustDate) == "")
 	 {
 	 	 alert("请填写处理日期！");
 	 	 return false;
 	 }
 	 
 	 return true;
 }
 
/***************************************************
 * 验证目标单位和被合并单位是否是同一机构
 ***************************************************
 */
 function checkSameGroup()
 {
 	 var tBranchAttrAim = document.fm.BranchAttr.value;
 	 var tBranchAttrU = document.fm.BranchAttrU.value;
 	 
 	 if (tBranchAttrAim == tBranchAttrU)
 	 {
 	 	 alert("目标单位和被合并单位不能为同一单位！");
 	 	 return false;
 	 }
 	 
 	 return true;
 }
 
/***************************************************
 *验证被合并机构有主管的是否确定了主管的调整动作
 ***************************************************
 */
 function checkGroupManager()
 {
 	 var tGroupManager = document.fm.GroupManager.value;
 	 var tAgentGradeNew = document.fm.AgentGradeNew.value;
 	 //alert("/"+tGroupManager+"/");
 	 //如果被合并机构有主管，则做如下验证
 	 if(tGroupManager!=""&&tGroupManager!="null"&&tGroupManager!=null)
 	 {
 	 	 //如果被合并机构存在主管，必须要进行新职级指定
 	 	 if(tAgentGradeNew == "")
 	 	 {
 	 	 	 alert("请指定被合并机构主管的调整职级！");
 	 	 	 return false;
 	 	 }
 	 }

 	 return true;
 }

/***************************************************
 * 执行合并操作
 ***************************************************
 */
 function submitForm()
 {
 	 //验证目标机构和被合并机构是否没填
 	 if (!checkNullGroup())
 	 {
 	 	 return false;
 	 }
 	 //验证目标机构和被合并机构是否为同一机构
 	 if(!checkSameGroup())
 	 {
 	 	 return false;
 	 }
 	 //验证被合并机构主管处理与否
 	 if(!checkGroupManager())
 	 {
 	 	 return false;
 	 }
 	 //验证合并时间是当月1号
 	 if(!checkAdjustDate())
 	 {
 	 	 return false;
 	 }
 	 
 	 //验证通过提交后台处理
 	 var i = 0;
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
   fm.submit(); //提交
 }
 
/***************************************************
 * 验证合并时间是否是指定月的1号
 ***************************************************
 */
 function checkAdjustDate()
 {
 	 var tAdjustDate = document.fm.AdjustDate.value;
 	 var tAdjustDay = tAdjustDate.substring((tAdjustDate.length - 2),tAdjustDate.length);
 	 //alert(tAdjustDay)
 	 if(tAdjustDay != "01")
 	 {
 	 	 alert("合并日期必须是一号");
 	 	 return false;
 	 }
 	 
 	 return true;
 }
 
/***************************************************
 * 查询操作
 ***************************************************
 */
 function queryGroup()
 {
 	 var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   showInfo=window.open("./LABranchGroupGrpQuery.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
 }
 
/***************************************************
 * 执行查询目标单位操作
 ***************************************************
 */
 function queryAimClick()
 {
 	 document.fm.GroupPart.value="0";
 	 queryGroup();
 }
 
/***************************************************
 * 执行查询被合并单位操作
 ***************************************************
 */
 function queryUniteClick()
 {
 	 document.fm.GroupPart.value="1";
 	 queryGroup();
 }
 
/***************************************************
 * 查询返回后的操作
 ***************************************************
 */
 function afterQuery(pmArrGroup)
 {
 	 //alert(pmArrGroup[0][0]);
 	 var tGroupPart = document.fm.GroupPart.value;
 	 
 	 if(tGroupPart == "0")
 	 {
 	   //设置机构代码
 	   document.fm.BranchAttr.value = pmArrGroup[0][4];
 	   //查询并显示目标管理机构
 	   //alert("目标管理机构");
 	   getGroupArm();
 	 }else if(tGroupPart == "1")
 	 {
 	   //设置机构代码
 	   document.fm.BranchAttrU.value = pmArrGroup[0][4];
 	   //查询并显示被合并管理机构
 	   //alert("被合并管理机构");
 	   initInpBox3();
 	   getGroupU();
 	 }
 }
 
 //提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}