<%
//程序名称：AdjustAgentInit.jsp
//程序功能：添加页面控件的初始化。
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

	function initForm(){
		try{
			initInpBox();
			initAgentGrid();
		}
		catch(re){
			alert("AdjustAgentInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	
	function initInpBox(){
		try{
			fm.all('AgentCode').value = '';
			fm.all('AgentName').value = '';
			fm.all('AgentGrade').value = '';
			fm.all('AgentGradeName').value = '';
			fm.all('RecomAgentCode').value = '';
			fm.all('RecomAgentName').value = '';
			fm.all('RecomAgentGrade').value = '';
			fm.all('RecomAgentGradeName').value = '';
			fm.all('OldBranchCode').value = '';
			fm.all('OldBranchName').value = '';
			fm.all('OldAgentGroup').value = '';
			fm.all('OldManageCom').value = '';
			fm.all('OldBranchManager').value = '';
			fm.all('OldManagerName').value = '';
			fm.all('BranchCode').value = '';
			fm.all('BranchName').value = '';
			fm.all('AgentGroup').value = '';
			fm.all('ManageCom').value = '';
			fm.all('BranchManager').value = '';
			fm.all('ManagerName').value = '';
			//fm.all('IsSingle').value = '';
			//fm.all('IsSingleName').value = '';
			fm.all('AdjustDate').value = '';
			fm.all('BranchType').value='<%=BranchType%>';
			fm.all('BranchType2').value='<%=BranchType2%>';
			fm.all('BranchLevel').value = '';
			fm.all('UpBranch').value = '';
			fm.all('AgentSeries').value = '';
		}
		catch(ex){
			alert("在AdjustAgentInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	
	// 险种授权的初始化
	function initAgentGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array();
			iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1]="30px"; 	           	//列宽
			iArray[0][2]=1;            				//列最大值
			iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[1]=new Array();
			iArray[1][0]="<%=tTitleAgent%>代码";
			iArray[1][1]="100px";
			iArray[1][2]=20;
			iArray[1][3]=0;


			iArray[2]=new Array();
			iArray[2][0]="<%=tTitleAgent%>名称";
			iArray[2][1]="80px";
			iArray[2][2]=20;
			iArray[2][3]=0;

			iArray[3]=new Array();
			iArray[3][0]="展业机构代码";
			iArray[3][1]="150px";
			iArray[3][2]=10;
			iArray[3][3]=0;



			iArray[4]=new Array();
			iArray[4][0]="<%=tTitleAgent%>职级";
			iArray[4][1]="80px";
			iArray[4][2]=10;
			iArray[4][3]=0;


			AgentGrid = new MulLineEnter( "fm" , "AgentGrid" );
			//这些属性必须在loadMulLine前
			AgentGrid.mulLineCount = 0;
			AgentGrid.displayTitle = 1;
			AgentGrid.locked=1;
			AgentGrid.canSel=0;
			AgentGrid.canChk=1;
			AgentGrid.hiddenPlus = 1;
			AgentGrid.hiddenSubtraction = 1;
			AgentGrid.loadMulLine(iArray);
		}
		catch(ex){
			alert(ex);
		}
	}


</script>
