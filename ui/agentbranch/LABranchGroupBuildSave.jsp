<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LABranchGroupSave.jsp
//程序功能：
//创建日期：2002-08-16 15:12:33
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LABranchGroupSchema tLABranchGroupSchema = new LABranchGroupSchema();
  LABranchGroupBuildBL tLABranchGroupBuildBL = new LABranchGroupBuildBL();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();

  String FlagStr = "Fail";
  String Content = "";

  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  tLABranchGroupSchema.setAgentGroup(request.getParameter("AgentGroup"));
  tLABranchGroupSchema.setName(request.getParameter("Name"));
  tLABranchGroupSchema.setManageCom(request.getParameter("ManageCom"));
  tLABranchGroupSchema.setBranchAttr(request.getParameter("BranchAttr"));
  tLABranchGroupSchema.setBranchType(request.getParameter("BranchType"));
  tLABranchGroupSchema.setBranchType2(request.getParameter("BranchType2"));
  tLABranchGroupSchema.setFoundDate(request.getParameter("FoundDate"));
    
    // 申报标志、申报标志起始日期
  tLABranchGroupSchema.setApplyGBFlag(request.getParameter("ApplyGBFlag"));
  tLABranchGroupSchema.setApplyGBStartDate(request.getParameter("ApplyGBStartDate"));
    // 团队建设标记、团队建设标记起始日期
  tLABranchGroupSchema.setGBuildFlag(request.getParameter("GbuildFlag"));
  tLABranchGroupSchema.setGBuildStartDate(request.getParameter("GbuildStartDate"));
  

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLABranchGroupSchema);
	tVData.add(tG);
  try
  {
	  tLABranchGroupBuildBL.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    System.out.println(Content);
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tLABranchGroupBuildBL.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {

    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    	System.out.println(Content);
    }
  }
  System.out.println(Content);
  //添加各种预处理
%>
<html>

<script language="javascript" type="">
	var FlagStr='<%=FlagStr%>';
	var Content1='<%=Content%>';
parent.fraInterface.afterSubmit(FlagStr,Content1);
</script>
</html>

