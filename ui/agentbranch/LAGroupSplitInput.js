var turnPage = new turnPageClass();

/***************************************************
 * 执行查询目标单位操作
 ***************************************************
 */
 function queryAimClick()
 {
 	 queryGroup();
 }
 
/***************************************************
 * 查询操作
 ***************************************************
 */
 function queryGroup()
 {
 	 var tBranchType = fm.all('BranchType').value;
   var tBranchType2 = fm.all('BranchType2').value;
   showInfo=window.open("./LABranchGroupGrpQuery.jsp?BranchType=" + tBranchType +"&BranchType2=" + tBranchType2);
 }
 
/***************************************************
 * 查询返回后的操作
 ***************************************************
 */
 function afterQuery(pmArrGroup)
 { 
   //设置机构代码
   document.fm.BranchAttr.value = pmArrGroup[0][4];
   //查询并显示被合并管理机构
   //alert("被合并管理机构");
   getGroupArm();
 }
 
/***************************************************
 * 获得目标机构相关信息
 ***************************************************
 */
 function getGroupArm()
 {
	 var tBranchAttr = document.fm.BranchAttr.value;
	 var tGroupArr;
	 //机构代码没有填写内容
	 if (trim(tBranchAttr) == "")
	 {
	 	 initInpBox1();
	 	 return false;
	 }

 	 tGroupArr = getQueryResult(tBranchAttr);
 	 //查询数据无结果
 	 if(tGroupArr == false)
	 {
		 initInpBox1();
		 return false;
	 }

	 //document.fm.AgentGroupA.value=tGroupArr[0][0];
	 document.fm.BranchName.value=tGroupArr[0][1];
	 document.fm.ManageComm.value=tGroupArr[0][2];
	 document.fm.BranchLevel.value=tGroupArr[0][6];
	 document.fm.AgentCode.value=tGroupArr[0][7];
	 document.fm.Name.value=tGroupArr[0][16];
	 document.fm.FoundDate.value=tGroupArr[0][11];
	 document.fm.AgentCount.value=getAgentCount(tGroupArr[0][0]);
	 
	 //查询机构下业务员
	 queryAgent();
 }
 
/***************************************************
 * 查询指定团队信息
 ***************************************************
 */
 function getQueryResult(pmBranchAttr)
 {
	 var arrSelected = new Array();

   // 书写SQL语句
	 var strSQL = "";
	 strSQL  = "SELECT";
	 strSQL += "    AgentGroup,";
	 strSQL += "    Name,";
	 strSQL += "    ManageCom,";
	 strSQL += "    UpBranch,";
	 strSQL += "    BranchAttr,";
	 strSQL += "    BranchType,";
	 //strSQL += "    BranchLevel,";
	 strSQL += "    (SELECT";
	 strSQL += "         trim(b.BranchLevelCode) || ' - ' || b.BranchLevelName";
	 strSQL += "       FROM";
	 strSQL += "         LABranchLevel b";
	 strSQL += "      WHERE";
	 strSQL += "         b.BranchType='2' AND";
	 strSQL += "         b.BranchType2='01' AND";
	 strSQL += "         b.BranchLevelCode = BranchLevel) as BranchLevel,";
	 strSQL += "    getUniteCode(BranchManager),";
	 strSQL += "    BranchAddress,";
	 strSQL += "    BranchPhone,";
	 strSQL += "    BranchZipcode,";
	 strSQL += "    FoundDate,";
	 strSQL += "    EndDate,";
	 strSQL += "    EndFlag,";
	 strSQL += "    FieldFlag,";
	 strSQL += "    Operator,";
	 strSQL += "    BranchManagerName,";
	 strSQL += "    BranchFax,";
	 strSQL += "    UpBranchAttr,";
	 strSQL += "    BranchType2,";
	 strSQL += "    TrusteeShip";
	 strSQL += " FROM";
	 strSQL += "    LABranchGroup";
	 strSQL += " WHERE";
	 strSQL += "    BranchAttr ='" + pmBranchAttr + "'";
	 strSQL += "   AND (state<>'1' or state is null)";
	 strSQL += "   AND BranchType = '2'";
	 strSQL += "   AND BranchType2 = '01'";
	     
	 turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
   //判断是否查询成功
   if (!turnPage.strQueryResult) {
     alert("没有此编码的单位！");
     return false;
   }
   
   //查询成功则拆分字符串，返回二维数组
   arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	 return arrSelected;
 }

/***************************************************
 * 查询指定团队信息
 ***************************************************
 */
 function getAgentCount(pmAgentGroup)
 {
 	 var tSQL ="";
 	 var tBranchType = document.fm.BranchType.value;
 	 var tBranchType2 = document.fm.BranchType2.value;
 	 tSQL  ="SELECT COUNT(*) FROM LAAGENT WHERE AGENTGROUP='"+pmAgentGroup+"' AND BRANCHTYPE='"+tBranchType
 	       +"' AND BRANCHTYPE2='"+tBranchType2+"'";
 	 
 	 tSQL = "select count(*) from LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','BranchAttr')
	     + getWherePart('b.groupAgentCode','AgentCode','<>')
	     + getWherePart('c.ManageCom','ManageComm')
	     + " and c.AgentGroup = b.AgentGroup "
	     + " and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and b.branchtype='2' "
	     + " and b.BranchType2='01'";
 	 
 	 turnPage.strQueryResult  = easyQueryVer3(tSQL, 1, 0, 1);
   //判断是否查询成功
   if (!turnPage.strQueryResult) {
     alert("查询机构内人数失败！");
     return 0;
   }
   
   //查询成功则拆分字符串，返回二维数组
   arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	 return arrSelected[0][0];
 }
 
function queryAgent()
{
   var tReturn = getManageComLimitlike("c.ManageCom");
   if((fm.all('BranchAttr').value==null)||(trim(fm.all('BranchAttr').value)==""))
   {
      alert("请先确定目标展业机构！");
      fm.all('BranchAttr').focus();
      return false;
   }

  var strSQL = "";
  var tBranchType = fm.all('BranchType').value;

  strSQL = "select getUniteCode(a.AgentCode),b.Name,c.BranchAttr,a.AgentGrade from LATree a,LAAgent b,LABranchGroup c where 1=1 "
	     + getWherePart('c.BranchAttr','BranchAttr')
	     + getWherePart('b.groupAgentCode','AgentCode','<>')
	     + getWherePart('c.ManageCom','ManageComm')
	     + " and c.AgentGroup = a.AgentGroup "
	     + " and a.AgentCode = b.AgentCode and (b.AgentState is null or b.AgentState < '03') and (c.state<>'1' or c.state is null)"
	     + " and b.branchtype='2' "
	     + " and b.BranchType2='01'";

  // alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  if (!turnPage.strQueryResult)
  {
  	//alert('查询失败,不存在有效纪录！');
  	return false;
  }
  //查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = chooseArray(arrDataSet,[0,1,2,3]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentGrid;
  //保存SQL语句
  turnPage.strQuerySql     = strSQL;
  //设置查询起始位置
  turnPage.pageIndex       = 0;
  //在查询结果数组中取出符合页面显示大小设置的数组
  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

/***************************************************
 * 执行合并操作
 ***************************************************
 */
 function submitForm()
 {
 	 //对非空进行验证
 	 if( verifyInput() == false ) return false;
 	 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  
   fm.submit(); //提交
 }
 
/***************************************************
 * 提交后操作,服务器数据返回后执行的操作
 ***************************************************
 */
 function afterSubmit( FlagStr, content )
 {
 	showInfo.close();
 	
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
    initForm();
  }
 }