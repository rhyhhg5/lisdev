  <%
//程序名称：GrpAgentComInit.jsp
//程序功能：
//创建日期：2004-5-24
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {    
    fm.all('GrpPolNo').value = '';            
    fm.all('ManageCom').value = '';
    fm.all('AgentCom').value = '';            
  }
  catch(ex)
  {
    alert("在GrpAgentComInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
 var AgentQueryGrid ;
 
// 保单信息列表的初始化
function initAgentQueryGrid()
  {                               
    var iArray = new Array();
    try
	{
	      iArray[0]=new Array();
	      iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
	      iArray[0][1]="30px";            		//列宽
	      iArray[0][2]=10;            		//列最大值
	      iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	 
	      iArray[1]=new Array();
	      iArray[1][0]="集体保单号码";         	//列名
	      iArray[1][1]="150px";              	//列宽
	      iArray[1][2]=200;            	        //列最大值
	      iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		
	      iArray[2]=new Array();
	      iArray[2][0]="管理机构";         		//列名
	      iArray[2][1]="100px";            		//列宽
	      iArray[2][2]=200;            	        //列最大值
	      iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	  
	      iArray[3]=new Array();
	      iArray[3][0]="单位名称";         		//列名
	      iArray[3][1]="150px";            		//列宽
	      iArray[3][2]=200;            	        //列最大值
	      iArray[3][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
      AgentQueryGrid = new MulLineEnter( "fm" , "AgentQueryGrid" ); 
      //这些属性必须在loadMulLine前
      AgentQueryGrid.mulLineCount = 10;   
      AgentQueryGrid.displayTitle = 1;
      AgentQueryGrid.hiddenPlus = 1;
      AgentQueryGrid.hiddenSubtraction = 1;
      AgentQueryGrid.loadMulLine(iArray);
      AgentQueryGrid.canChk =1;  
      }
      catch(ex)
      {
        alert(ex);
      }
 }
function initForm()
{
  try
  {
    initInpBox();
//    initAgentQueryGrid();
  }
  catch(re)
  {
    alert("GrpAgentComInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
