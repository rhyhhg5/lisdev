   //               该文件中包含客户端需要处理的函数和事件
var arrDataSet;
var showInfo;
var mDebug="0";
var tFlag = "0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//提数操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  //showSubmitFrame(mDebug);
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在ChFinInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//执行重算         
function ChFinSave()
{
  if (tFlag == "0")
  {
  	alert("请先点击查询按钮，选择需要审核的手续费！");
  	return;	
  }
  tFlag = "0";
  //首先检验录入框
  if(!verifyInput()) return false;
//  divAgentQuery.style.display='none'; 
//  fm.action="./ChFinSave.jsp";
  submitForm();	
}  
                  

//执行查询
function ChFinQuery()
{
	    //首先检验录入框
  //if(!verifyInput()) return false;

// if(fm.all('WageYear').value==''||fm.all('WageMonth').value==''||fm.all('BranchType').value=='')
//  {
//    alert("请填写查询条件");
//    return ;	
//  }
 divAgentQuery.style.display='';

// initAgentQueryGrid();
 showRecord();
 showRecord1();
 tFlag = "1";
}



//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord()
{            
var tReturn = getManageComLimitlike("a.ManageCom");		
  var strSql = "select a.AgentCom,a.ChargeType,a.CalDate,a.GrpPolNo,a.PolNo,a.ManageCom,a.RiskCode,a.ReceiptNo,a.TransType,a.PayCount,a.TransMoney,a.ChargeRate,a.Charge from LACharge a where  a.ChargeState='1' "
  		+ "and a.BranchType='2' "
  		+ tReturn
		+ getWherePart('a.AgentCom','AgentCom')
		+ getWherePart('a.ChargeType','ChargeType')
		+ getWherePart('a.CalDate','StartDate','>=')
		+ getWherePart('a.CalDate','EndDate','<=');		
		
		
//  alert(strSql);	    
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    AgentQueryGrid.clearData('AgentQueryGrid');  
    alert("查询失败！");
    return false;
  }
  
//  alert("111");
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = AgentQueryGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
//  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
//    try { window.divPage.style.display = ""; } catch(ex) { }
//  } else {
//    try { window.divPage.style.display = "none"; } catch(ex) { }
//  }	
}



//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord1()
{            
	var tReturn = getManageComLimitlike("a.ManageCom");
	var strSQL = "select a.AgentCom,b.Name,a.ChargeType,a.CurrMoney,a.SumMoney from LAComCharge a,LACom b " 
							+ "where a.AgentCom=b.AgentCom and a.state='1' "
							+ "and a.BranchType='2' "
							+ tReturn
							+ getWherePart( 'a.AgentCom','AgentCom' )
							+ getWherePart( 'a.ChargeType','ChargeType' )
							+ getWherePart( 'a.StartDate','StartDate' )
							+ getWherePart( 'a.StartEnd','EndDate' );


		
		
	//alert(strSQL);
	turnPage2.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage2.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    //LCPolGrid.clearData();  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage2.arrDataCacheSet = clearArrayElements(turnPage2.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage2.arrDataCacheSet = decodeEasyQueryResult(turnPage2.strQueryResult, 0, 0, turnPage2);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage2.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage2.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage2.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage2.getData(turnPage2.arrDataCacheSet, turnPage2.pageIndex, turnPage2.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage2.pageDisplayGrid, turnPage2);
  
  //控制是否显示翻页按钮
//  if (turnPage2.queryAllRecordCount > turnPage2.pageLineNum) {
//    try { window.divPage2.style.display = ""; } catch(ex) { }
//  } else {
//    try { window.divPage2.style.display = "none"; } catch(ex) { }
//  }
}
