<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ChAudGrantSave.jsp
//程序功能：中介手续费  手续费审核发放
//创建日期：2005-12-7 16:58
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentcharge.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  String tAgentCom = request.getParameter("AgentCom");
	String tChargeType = request.getParameter("ChargeType");
	String tACType = request.getParameter("ACType");
	String tStartDate = request.getParameter("StartDate");
	String tEndDate = request.getParameter("EndDate");
	String tManageCom = request.getParameter("ManageCom");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	LACommisionSet tLACommisionSet = new LACommisionSet();
	ChAutGrantUI tChAutGrantUI = new ChAutGrantUI();

  //输出参数
  CErrors tError = null;

  String tOperate="";

  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
  tVData.add(tAgentCom);
  tVData.add(tManageCom);
  tVData.add(tACType);
  tVData.add(tStartDate);
  tVData.add(tEndDate);
  tVData.add(tChargeType);
  tVData.add(tBranchType);
  tVData.add(tBranchType2);
	tVData.add(tG);
	System.out.println("数据准备完毕，开始后台操作.");
  try
  {
    tChAutGrantUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tChAutGrantUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>