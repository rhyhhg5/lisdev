<%
//程序名称：ChAudInit.jsp
//程序功能：
//创建日期：2003-11-06
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
 
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {    
    fm.all('GrpContNo').value = '';
    fm.all('ManageCom').value = '';
    fm.all('StartDate').value = '';
    fm.all('EndDate').value = '';
    fm.all('AgentCom').value = '';
    fm.all('ChargeType').value = '';
    fm.all('ChargeTypeName').value = '';
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';
  }
  catch(ex)
  {
    alert("在ChAudInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      
 var AgentQueryGrid ;
 
// 保单信息列表的初始化
function initAgentQueryGrid()
{
		
    var iArray = new Array();
    try
  	{
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         		//列名（此列为顺序号，列名无意义，而且不显示）
	    iArray[0][1]="30px";            		//列宽
	    iArray[0][2]=10;            		//列最大值
	    iArray[0][3]=0;              		//是否允许输入,1表示允许，0表示不允许
	 
	    iArray[1]=new Array();
	    iArray[1][0]="集体保单合同号";         	//列名
	    iArray[1][1]="90px";              	//列宽
	    iArray[1][2]=200;            	        //列最大值
	    iArray[1][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		
	    iArray[2]=new Array();
	    iArray[2][0]="管理机构";         		//列名
	    iArray[2][1]="60px";            		//列宽
	    iArray[2][2]=200;            	        //列最大值
	    iArray[2][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		
	    iArray[3]=new Array();
	    iArray[3][0]="管理机构名称";         		//列名
	    iArray[3][1]="0px";            		//列宽
	    iArray[3][2]=200;            	        //列最大值
	    iArray[3][3]=3;                   	//是否允许输入,1表示允许，0表示不允许
		
	    iArray[4]=new Array();
	    iArray[4][0]="中介机构编码";         		//列名
	    iArray[4][1]="80px";            		//列宽
	    iArray[4][2]=200;            	        //列最大值
	    iArray[4][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	  
	    iArray[5]=new Array();
	    iArray[5][0]="中介机构名称";         		//列名
	    iArray[5][1]="120px";            		//列宽
	    iArray[5][2]=200;            	        //列最大值
	    iArray[5][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
		
	    iArray[6]=new Array();
	    iArray[6][0]="交费日期";         		//列名
	    iArray[6][1]="70px";            		//列宽
	    iArray[6][2]=200;            	        //列最大值
	    iArray[6][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	    iArray[7]=new Array();
	    iArray[7][0]="中介专员代码";         		//列名
	    iArray[7][1]="70px";            		//列宽
	    iArray[7][2]=200;            	        //列最大值
	    iArray[7][3]=0;                   	//是否允许输入,1表示允许，0表示不允许

	    iArray[8]=new Array();
	    iArray[8][0]="中介专员姓名";         		//列名
	    iArray[8][1]="70px";            		//列宽
	    iArray[8][2]=200;            	        //列最大值
	    iArray[8][3]=0;                   	//是否允许输入,1表示允许，0表示不允许
	
	    iArray[9]=new Array();
	    iArray[9][0]="结算状态";         		//列名
	    iArray[9][1]="50px";            		//列宽
	    iArray[9][2]=200;            	        //列最大值
	    iArray[9][3]=0;                   	//是否允许输入,1表示允许，0表示不允许    
	
	    iArray[10]=new Array();
	    iArray[10][0]="新保全状态";         		//列名
	    iArray[10][1]="60px";            		//列宽
	    iArray[10][2]=200;            	        //列最大值
	    iArray[10][3]=0;                   	//是否允许输入,1表示允许，0表示不允许   
	
	    iArray[11]=new Array();
	    iArray[11][0]="CommiaionSn";         		//列名
	    iArray[11][1]="0px";            		//列宽
	    iArray[11][2]=200;            	        //列最大值
	    iArray[11][3]=3;                   	//是否允许输入,1表示允许，0表示不允许   

      AgentQueryGrid = new MulLineEnter( "fm" , "AgentQueryGrid" ); 
      //这些属性必须在loadMulLine前
      AgentQueryGrid.displayTitle = 1;
      AgentQueryGrid.hiddenPlus = 1;
      AgentQueryGrid.hiddenSubtraction = 1;
      AgentQueryGrid.canSel = 0;
      AgentQueryGrid.canChk = 0;
      AgentQueryGrid.loadMulLine(iArray);
    }
    catch(ex)
    {
      alert(ex);
    }
 }
function initForm()
{
  try
  {
    initInpBox();
    initAgentQueryGrid();
  }
  catch(re)
  {
    alert("ChAudInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>
