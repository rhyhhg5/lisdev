<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：LARateCharge.jsp
//程序功能：机构手续费录入
//创建人  ：销售管理
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LARateCharge.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <%@include file="LARateChargeInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
<title></title>
</head>
<body  onload="initForm();" >
  <form action="./LACommChargeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
   <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAPlan1);">
     <td class=titleImg>
      机构代理手续费维护信息
     </td>
    </td>
    </table>
  <Div  id= "divLAPlan1" style= "display: ''"> 
    <table  class= common>
      <tr  class= common> 
        <td  class= title> 代理机构</td>
        <td  class= input> <Input class="code" name=AgentCom  verify="险种|code:agentcom" ondblclick="initEdorType(this);" onkeyup="actionKeyUp(this);">  </td>        
        <td  class= title> 险种	</td>
        <td  class= input> <input name=RiskCode class="code" verify="险种|code:Riskcode" 
		         ondblclick="return showCodeList('riskcode',[this]);" onkeyup="return showCodeListKey('riskcode',[this]);"> </td>
      </tr>
      <TR>
          <TD  class= title width="25%">起始日期</TD>
          <TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="起始日期|NOTNULL"> </TD>      
          <TD  class= title width="25%">终止日期</TD>
          <TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=EndDate  verify="结束日期|NOTNULL"></TD>  
      </TR> 
      <tr  class= common>  
        <td  class= title>投保年龄</td>
        <td  class= input> <input class=common name=AppAge verify="投保年龄|notnull"> </td>
        <td  class= title>保险年期</td>
        <td  class= input><input class=common name=Years verify="保险年期|notnull"> 	</td>
      </tr>
      <tr  class= common> 
        <td  class= title>交费间隔</td>
        <td  class= input><input class="code" name=PayIntv  verify="交费间隔|notnull" CodeData="0|^0|趸交^1|月交^12|年交^-1|不定期交" ondblclick="showCodeListEx('PayIntv',[this],[0]);"  onkeyup="showCodeListKeyEx('PayIntv',[this],[0]);"> 	</td>
        <td  class= title>手续费比例</td>
        <td  class= input><input name=Rate class= common verify = "手续费比率|notnull"> </td>
      </tr>
      <tr  class= common> 
        <td  class= title>计算类型</td>
        <td  class= input> <input name=CalType class="code" verify="计算类型|notnull" 
		         CodeData="0|^51|机构手续费^52|机构劳务费^53|机构业务费^54|机构节余费" ondblclick="showCodeListEx('Cal',[this],[0]);"  onkeyup="showCodeListKeyEx('Cal',[this],[0]);"> </td>
        <td  class= title>保单类型</td>
        <td  class= input><input class='code' name=PolType verify="计划类型|notnull"
		         CodeData="0|^0|优惠业务^1|正常业务" 
		         ondblclick="showCodeListEx('Type',[this],[0]);"  onkeyup="showCodeListKeyEx('Type',[this],[0]);"</td>
      </tr>
      <tr  class= common>         
        <td  class= title> 操作员代码</td>
        <td  class= input> <input name=Operator class= 'readonly'readonly > </td>		    
      </tr>
    </table>
  </Div>    
    <input name=hideOperate type=hidden value = ''>
    <input name=AgentComB type=hidden value = ''>
    <input name=RiskCodeB type=hidden value = ''>
    <input name=StartDateB type=hidden value = ''>
    <input name=EndDateB type=hidden value = ''>
    <input name=AppAgeB type=hidden value = ''>
    <input name=YearsB type=hidden value = ''>
    <input name=PayIntvB type=hidden value = ''>
    <input name=RateB type=hidden value = ''>
    <input name=CalTypeB type=hidden value = ''>
    <input name=PolTypeB type=hidden value = ''>
    <input name=OperatorB type=hidden value = ''>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
<script>
  var sql = "";
  var sqlField = "";
</script>