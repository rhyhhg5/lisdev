<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ChAudRuleSave.jsp
//程序功能：中介手续费  特殊业务结算
//创建日期：2005-11-29 10:00
//创建人  ：LiuHao
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.agentcharge.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	String tChargeType = request.getParameter("ChargeType");
	String tChargeType2 = "FEW";
	String tStringDate = request.getParameter("ReckonStartDate");
	String tEndDate = request.getParameter("ReckonEndDate");
	String tMamageCom = request.getParameter("ReckonManageCom");
	String tBranchType = request.getParameter("BranchType");
	String tBranchType2 = request.getParameter("BranchType2");
	LACommisionSet tLACommisionSet = new LACommisionSet();
	ChAudRuleUI tChAudRuleUI = new ChAudRuleUI();

  //输出参数
  CErrors tError = null;

  String tOperate="";

  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.add(tStringDate);
	tVData.add(tEndDate);
	tVData.add(tMamageCom);
	tVData.add(tChargeType);
	tVData.add(tBranchType);
	tVData.add(tBranchType2);
	tVData.add(tChargeType2);
	tVData.add(tG);
	System.out.println("数据准备完毕，开始后台操作.");
  try
  {
    tChAudRuleUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = tChAudRuleUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

  //添加各种预处理

%>
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>