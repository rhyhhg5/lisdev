<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：ChDstInput.jsp
//程序功能：
//创建日期：2004-03-11
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>    
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="LACrossDstInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LACrossDstInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form method=post name=fm target="fraSubmit" action="./ChDstSave1.jsp">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		手续费提取
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            中介机构编码
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCom verify="代理机构|code:agentcom&notnull"  ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null,'1 and actype=#05# ','1',1);" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null,'1 and actype=#05# ','1',1);">
          </TD>
          <TD  class= title>
            中介机构名称
          </TD>
          <TD>
            <Input class=common  name=AgentComName readonly >
          </TD>  
	  
        </TR>
        <TR  class= common>
          <TD  class= title>
            统计起期
          </TD>
          <TD  class= input>
            <Input name=StartDate class='coolDatePicker' verify="统计起期|NOTNULL"  dateFormat='short'>
          </TD>
	  <TD  class= title>
            统计止期
          </TD>
          <TD  class= input>
            <Input name=EndDate class='coolDatePicker' verify="统计止期|NOTNULL"  dateFormat='short' >
          </TD> 
        </TR>
        <TR>
          <TD  class= title>
            手续费类型
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ChargeType verify="手续费类型|NOTNULL"  ondblclick="return showCodeList('ChargeType',[this,ChargeTypeName],[0,1],null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('ChargeType',[this,ChargeTypeName],[0,1],null,mcodeSql,'1',null);" 
            ><Input class=codename name=ChargeTypeName readonly >
          </TD> 
         </TR>

        <TR class=input>              
         <TD class=common>
          <input type =button class=common value="查询" onclick="ChDstQuery();">    
        </TD>
        <!--TD class=common>
          <input type =button class=common value="重算" onclick="ChDstRepeat();">    
        </TD>
       </TR>   
       <TR class=input-->              
         <TD class=common>
          <input type =button class=common value="提取" onclick="ChDstSave();">    
         </TD>
       </TR>   
              
      </table>
    </Div>  
    <!--input type=hidden name=BranchType value=""-->
    <Div  id= "divAgentQuery" style= "display: 'none'">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  			</TD>
      </TR>
     </Table>	
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
	<input type=hidden name=Operate value=''>   
     </Table>
   </Div>	     
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var mcodeSql = "1 and code in (#61#,#62#)";
</script>