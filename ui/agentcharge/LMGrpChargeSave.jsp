<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LMGrpChargeSave.jsp
//程序功能：
//创建日期：2003-11-06
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.agentcharge.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%@page contentType="text/html;charset=gb2312" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  GlobalInput tG = new GlobalInput();
  tG = (GlobalInput)session.getValue("GI");
  
  LMGrpChargeSchema tLMGrpChargeSchema   = new LMGrpChargeSchema();

  GrpChargeUI tGrpChargeUI   = new GrpChargeUI();

  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("------transact111111111111111111111111111111111111111111111111111111111111:"+transact);


    tLMGrpChargeSchema.setGrpPolNo(request.getParameter("GrpPolNo"));
    tLMGrpChargeSchema.setPayCountFrom(request.getParameter("PayCountFrom"));
    tLMGrpChargeSchema.setPayCountTo(request.getParameter("PayCountTo"));
    tLMGrpChargeSchema.setChargeType(request.getParameter("ChargeType"));
    tLMGrpChargeSchema.setChargeRate(request.getParameter("ChargeRate"));
    tLMGrpChargeSchema.setChargeAccount(request.getParameter("ChargeAccount"));
    tLMGrpChargeSchema.setParm1(request.getParameter("Parm1"));
    tLMGrpChargeSchema.setParm2(request.getParameter("Parm2"));
    tLMGrpChargeSchema.setNoti(request.getParameter("Noti"));
    
 System.out.println("------transact111111111111111111111111111111111111111111111111111111111111:"+transact);
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
	tVData.addElement(tLMGrpChargeSchema);
	tVData.addElement(tG);
   System.out.println("------transact111111111111111111111111111111111111111111111111111111111111:"+transact);
    tGrpChargeUI.submitData(tVData,transact);
    System.out.println("------transact111111111111111111111111111111111111111111111111111111111111:"+transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tGrpChargeUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

