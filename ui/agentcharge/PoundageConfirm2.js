//               该文件中包含客户端需要处理的函数和事件
var arrDataSet 
var showInfo;
var mDebug="1";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var flag=false;


function EdorType(cObj)
{
	mEdorType = " #1# and actype <>#01# ";
	showCodeList('AgentCom',[cObj], null, null, mEdorType, "1");
}

function KeyUp(cObj)
{	
	mEdorType = " #1# and actype <>#01#";
	showCodeListKey('AgentCom',[cObj], null, null, mEdorType, "1");
}

function submitForm()
{
	flag=false
  var i = 0;
  var showStr="正在处理数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
  flag=true;
}

//提交，保存按钮对应操作
function printFee()
{
  var i = 0;
  var arrReturn = new Array();
  var tSel = PolGrid.getSelNo();
  if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		fm.AgentCom.value = PolGrid.getRowColData(tSel-1,1);
		fm.AgentName.value = PolGrid.getRowColData(tSel-1,2);
		fm.CurrMoney.value = PolGrid.getRowColData(tSel-1,4);
		fm.SumMoney.value = PolGrid.getRowColData(tSel-1,5);
		mOperate="PRINT";
		fm.hideOperate.value = "PRINT";
		fm.action="./PoundPrtSave.jsp";
		fm.target = "f1print";  				
		fm.submit();		
	}
	//showInfo.close();
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	if( tRow == 0 || tRow == null || arrDataSet == null )
	return arrSelected;
	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = arrDataSet[tRow-1];
	return arrSelected;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  //showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
     easyQueryClick();
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function DetailClick3()
{
	if(!verifyInput()) return false;
	initDetailGrid();
	// 书写SQL语句
	var tReturn = getManageComLimitlike("b.managecom");	
	if(fm.ChargeType.value=='55')
	{
	var strSQL = "select a.AgentCode,a.Name,b.PolNo,b.P11,b.RiskCode,e.Transmoney,b.PayYears,b.paycount,c.agentcom,c.name,e.Charge,d.codename"
	             +" from LAAgent a,lacommision b,lacom c,lacharge e,ldcode d  where a.AgentCode=b.AgentCode and b.branchtype='3' "
	             +" and b.agentcom=c.agentcom and e.agentcom=b.agentcom and b.CommisionSN=e.CommisionSN and e.ChargeType like '5%%'"
	             +" and d.code=e.chargetype and d.codetype = 'bankcharge' "
	             + getWherePart( 'e.AgentCom','AgentCom')
							 + getWherePart( 'e.caldate','BeginDate','>=' )
							 + getWherePart( 'e.caldate','EndDate','<=' )
							 +tReturn
							 +" order by e.agentcom,b.polno,e.chargetype";
	}
	else 
	{
		var strSQL = "select a.AgentCode,a.Name,b.PolNo,b.P11,b.RiskCode,e.Transmoney,b.PayYears,b.paycount,c.agentcom,c.name,e.Charge,d.codename"
	             +" from LAAgent a,lacommision b,lacom c,lacharge e,ldcode d  where a.AgentCode=b.AgentCode and b.branchtype='3' "
	             +" and b.agentcom=c.agentcom and e.agentcom=b.agentcom and b.CommisionSN=e.CommisionSN "  
							 +" and d.code=e.chargetype and d.codetype = 'bankcharge' "
							 + getWherePart( 'e.AgentCom','AgentCom')
							 + getWherePart( 'e.ChargeType','ChargeType' )
							 + getWherePart( 'e.caldate','BeginDate','>=' )
							 + getWherePart( 'e.caldate','EndDate','<=' )
							 + tReturn
							 +" order by e.agentcom,b.polno,e.chargetype";
	}			
	
	//alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  

  //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    //清空MULTILINE，使用方法见MULTILINE使用说明 
    //VarGrid.clearData();  
    alert("查询失败！");
    return false;
  }
  
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = DetailGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  } 
  
}



// 查询按钮
function easyQueryClick()
{
	//alert(111);
	//alert(fm.ChargeType.value);
	// 书写SQL语句
	if(fm.ChargeType.value=='55')
	   var strSQL = "select a.AgentCom,b.Name,a.ChargeType,a.CurrMoney,a.SumMoney from LAComCharge a,LACom b " 
							+ "where a.AgentCom=b.AgentCom "
							+ "and a.BranchType='" + tBranchType + "' and a.ChargeType in ('1','2','3','0') and a.state='2' "
							+ getWherePart( 'a.AgentCom','AgentCom' )							
							+ getWherePart( 'a.StartDate','BeginDate' )
							+ getWherePart( 'a.StartEnd','EndDate' );
	else 
		{var tChargeType;
			if(fm.ChargeType.value=='51')
			 tChargeType='0';
			else if(fm.ChargeType.value=='52')
			 tChargeType='1';
			else if(fm.ChargeType.value=='53')
			 tChargeType='2';
			else if(fm.ChargeType.value=='54')
			 tChargeType='3';
			
	  var strSQL = "select a.AgentCom,b.Name,a.ChargeType,a.CurrMoney,a.SumMoney from LAComCharge a,LACom b " 
							+ "where a.AgentCom=b.AgentCom and a.state='2' and a.ChargeType='"+tChargeType
							+ "' and a.BranchType='" + tBranchType + "' "
							+ getWherePart( 'a.AgentCom','AgentCom' )							
							+ getWherePart( 'a.StartDate','BeginDate' )
							+ getWherePart( 'a.StartEnd','EndDate' );
			}
	
	//alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1); 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    //alert("查询失败！");
    return false;
    }
//查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
 arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
 //tArr=chooseArray(arrDataSet,[0]) 
  //调用MULTILINE对象显示查询结果
  
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //displayMultiline(tArr, turnPage.pageDisplayGrid);
  
}


function calClick()
{
	if(!verifyInput()) return false;
	fm.all('Operate').value='CONFIRM';
	fm.action="./PoundCalSave.jsp";
	fm.target="fraSubmit";
	mOperate="CAL";
	fm.hideOperate.value=mOperate;
	initPolGrid();
	fm.submit();
	//showInfo.close();
}

function DetailPrint()
{
	fm.action="./PoundDetailPrint.jsp";
	fm.target="f1print";
	if(!verifyInput()) return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
  showInfo.close();
}