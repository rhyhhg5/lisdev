<%@include file="../common/jsp/UsrCheck.jsp"%>
 <html>  
<%
//程序名称：ChFinInput.jsp
//程序功能：
//创建日期：2004-03-11
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!-- %@include file="../common/jsp/UsrCheck.jsp"%-->
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>       
  <SCRIPT src="ChFinInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="ChFinInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
</head>
<body  onload="initForm();" >    
  <form method=post name=fm target="fraSubmit" action="./ChFinSave.jsp">
    <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		财务审核
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCom verify="代理机构|NOTNULL" ondblclick="return showCodeList('AgentCom',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('AgentCom',[this],null,null,null,null,1);">
          </TD>
	  <TD  class= title>
            手续费类型
          </TD>
          <TD  class= input>
            <Input class="code" name=ChargeType verify="手续费类型|NOTNULL" ondblclick="return showCodeList('ChargeType',[this],null,null,mcodeSql,'1',null);" onkeyup="return showCodeListKey('ChargeType',[this],null,null,mcodeSql,'1',null);" >
          </TD> 
        </TR>
        <TR  class= common>
          <TD  class= title>
            统计起期
          </TD>
          <TD  class= input>
            <Input name=StartDate class='coolDatePicker' verify="统计起期|NOTNULL" dateFormat='short'>
          </TD>
	  <TD  class= title>
            统计止期
          </TD>
          <TD  class= input>
            <Input name=EndDate class='coolDatePicker' verify="统计止期|NOTNULL" dateFormat='short' >
          </TD> 
        </TR>

        <TR class=input>              
         <TD class=common>
          <input type =button class=common value="查询" onclick="ChFinQuery();">    
        </TD>
        <TD class=common>
          <input type =button class=common value="审核" onclick="ChFinSave();">    
        </TD>
       </TR>   
              
      </table>
    </Div>  
    <!--input type=hidden name=BranchType value=""-->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentQuery);">
    		</td>
    		<td class= titleImg>
    			 明细信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divAgentQuery" style= "display: ''">
     <Table  class= common>
       <TR  class= common>
        <TD text-align: left colSpan=1>
            <span id="spanAgentQueryGrid" ></span> 
  	</TD>
      </TR>
     </Table>	
     <Div  id= "divPage" align=center style= "display: '' ">
     <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
     </Div>
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 手续费信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage2" align=center style= "display: '' ">
    <Table>
	<TR  class= common>  
	  <TD class=common>   					
      	     	<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 
          </TD>
	  <TD class=common>					
      		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
          </TD>
	  <TD class=common>
      		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
          </TD>
	</TR>
     </Table>
  	</div>

   </Div>	     
  </form>   
   
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

<script>
  var mcodeSql = "#1# and code in (#11#,#12#)";
</script>