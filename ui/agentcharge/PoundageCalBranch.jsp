<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-10-29 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tBranchType = "";
	try
	{
		tBranchType = request.getParameter("BranchType");
	}
	catch( Exception e )
	{
		tBranchType = "";
	}
%>
<Script>
var tBranchType = "<%=tBranchType%>";
</Script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="PoundageCalBranch.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
  <%@include file="PoundageCalInit.jsp"%>
  	<%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>手续费计算 </title>
</head>
<body  onload="initForm();">
  <form  method=post name=fm >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入计算条件：
			</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=AgentCom  verify="代理机构|NOTNULL"  ondblclick="EdorType(this);" onkeyup="KeyUp(this);">
          </TD>
          <TD  class= title>
            手续费类型 
          </TD>
          <TD  class= input>
            <Input class="code" name=ChargeType  verify="手续费类型|NOTNULL"  CodeData="0|^51|手续费^52|劳务费用^53|业务费用^54|节余费用^55|所有费用" ondblclick="showCodeListEx('ttType',[this],[0]);"  onkeyup="showCodeListKeyEx('ttType',[this],[0]);">
          </TD>          
        </TR>
        <TR  class= common>
          <TD  class= title>
            开始日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=BeginDate verify="开始日期|NOTNULL">
          </TD>
          <TD  class= title>
            结束日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="结束日期|NOTNULL">
          </TD>
        </TR>
    </table>
         <INPUT VALUE="明细查询" TYPE=button onclick="DetailClick2();">
         <INPUT VALUE="明细打印" TYPE=button onclick="DetailPrint();">     	
         <!-- 明细信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 明细信息
    		</td>
    	</tr>
    </table>
    <Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanDetailGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: '' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>			
  	</div>    
          <INPUT VALUE="审核确认" TYPE=button onclick="calClick();"> 
          <!-- 手续费信息部分（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 手续费信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "divPage2" align=center style= "display: '' ">
      <INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
  	</div>
  	<input type=hidden name=hideOperate value=''>  	
  	<input type=hidden name=AgentName value=''>
  	<input type=hidden name=CurrMoney value=''>
  	<input type=hidden name=SumMoney value=''>
  	<table>     
  	 		<Input type=hidden name=BranchType >
  	 		<input type=hidden id="Operate" name="Operate">
  	</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
