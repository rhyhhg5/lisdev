//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAActiveChargeQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

//查询下载之前基本校验
function beforeSubmit()
{
	var startDate=new Date(fm.all('StartDate').value);
	var endDate=new Date(fm.all('EndDate').value);
	var diff=startDate.getTime()-endDate.getTime();
	var day=Math.floor(diff/(1000*60*60*24));
   if (day>0)
   {
   	alert("财务结算起期必须小于等于财务结算止期！");
        fm.all('StartDate').value = '';
        fm.all('EndDate').value = '';
   	return false;
   }
   return true;
}


//双击得到agentcom
function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#"+ fm.all('BranchType').value +"# and BranchType2=#"+ fm.all('BranchType2').value +"# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}

//页面查询
function easyQueryClick()
{	 
    //首先检验录入框
    if(!verifyInput()) return false;   
	 var sql= "select a.managecom,(select name from ldcom where a.managecom = comcode),a.agentcom,(select name from lacom where a.agentcom = agentcom )" +
	 		",sum(charge),a.batchno ," +
	 		"(select (case when payresponsecode ='1' then '成功'  when payresponsecode ='0' then '失败' else '未进行支付状态查询' end) from LAChargePayBatchLog b where a.batchno = b.batchno) " +
	 		"from lacharge a where   a.tmakedate>='"+fm.all('StartDate').value+"'and a.tmakedate<='"+fm.all('EndDate').value+"' " +
			" and a.managecom like '"+fm.all('ManageCom').value+"%'"
     	  + getWherePart('a.BranchType', 'BranchType')
		  + getWherePart('a.BranchType2', 'BranchType2')
		  + getWherePart('a.AgentCom', 'AgentCom')
//		  + getWherePart('a.batchno', 'BatchNo')
		  +" group  by a.managecom,a.agentcom,a.batchno " 
	    turnPage.queryModal(sql, ContChargeQueryGrid);   
}


function contChargeQueryState()
{
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  
  var End = fm.EndDate.value;
  var cur = fm.CurrentDate.value;	
  
	if (End > cur)
	{
		alert("错误，计算止期不能大于今天");
		return false;
	}	
  var selno = ContChargeQueryGrid.getSelNo();
  if(0>=selno)
  {
	  alert("请选择一行进行查询");
	  return false;
  }
//  var i = 0;
//  var showStr="正在 查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
//  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
//  showInfo.close();
//	 
//  if (FlagStr == "Fail" )
//  {             
//    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//  }
//  else
//  { 
//    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
//  }
	var sql ="";
}

/*
** 注释：现在这个查询条件被去除了
*/
function getSiteManager(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输 入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#5# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%# and (AgentState = #01# or AgentState = #02#) ";
  
	showCodeList('AgentCode',[cObj,cName],[0,1],null,strsql,'1',1);
}
function afterCodeSelect( cCodeName, Field )
{
  if( cCodeName == "crstype")
  {
    if(fm.all('CrsType').value=='N')
    {
    	document.getElementById("divSpanLACommisionGrid").style.display='';
    	document.getElementById("divSpanLACommisionGrid2").style.display='none';
    	initLACommisionGrid(); 
        initLACommisionGrid2();
    }
    else
    {
    	document.getElementById("divSpanLACommisionGrid").style.display='none';
    	document.getElementById("divSpanLACommisionGrid2").style.display='';
    	initLACommisionGrid(); 
        initLACommisionGrid2();
    }
  }
}
