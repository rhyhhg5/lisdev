<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GrpAgentComSave.jsp
//程序功能：
//创建日期：2004-5-24
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT src="GrpAgentComInput.js"></SCRIPT>
<!--用户校验类-->
  <%@page import="java.lang.*"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentcharge.*"%>  

<%
// 输出参数
  CErrors tError = null;          
  String FlagStr = "";
  String Content = "";
  GlobalInput tGI = new GlobalInput(); //repair:
  tGI=(GlobalInput)session.getValue("GI");  //参见loginSubmit.jsp
  if(tGI==null)
  {
    System.out.println("页面失效,请重新登陆");   
    FlagStr = "Fail";        
    Content = "页面失效,请重新登陆"; 
  }
  else //页面有效
  {
	  int lineCount = 0;
	  String tChk[] = request.getParameterValues("InpAgentQueryGridChk");
	  int number = 0; 
	  for(int index=0;index<tChk.length;index++)
          {
            if(tChk[index].equals("1"))           
              number++;
//            if(tChk[index].equals("0"))      
//              System.out.println("该行未被选中");
          }               
	if(number==0)
	{
       		Content = " 失败，原因:没有选择要调整的员工！";
       		FlagStr = "Fail";		
	}
	else
	{	  
		String[] tGrpPolNo = request.getParameterValues("AgentQueryGrid1");   
	  	String tAgentCom = request.getParameter("AgentCom");   
	  
	  lineCount = tChk.length; //行数
	  System.out.println("length= "+String.valueOf(lineCount));

   VData tVData = new VData();
   tVData.addElement(tGrpPolNo);
   tVData.addElement(tAgentCom);
   tVData.addElement(tGI);

    	
   //如果在Catch中发现异常，则不从错误类中提取错误信息
   try
   {
     GrpAgentComUI mGrpAgentComUI = new GrpAgentComUI();
     mGrpAgentComUI.submitData(tVData,"INSERT||CHARGE");
     
     tError = mGrpAgentComUI.mErrors;
     if (!tError.needDealError())
     {                          
       Content = " 保存成功! ";
       FlagStr = "Succ";
     }
     else                                                                           
     {
       Content = " 失败，原因:" + tError.getFirstError();
       FlagStr = "Fail";
     }
   } 
   catch(Exception ex)
   {
     Content = " 失败，原因:" + ex.toString();
     FlagStr = "Fail";    
   }    
   }
   System.out.println(Content);                  
}//页面有效区

%>                      
<html>
<script language="javascript">
        parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

