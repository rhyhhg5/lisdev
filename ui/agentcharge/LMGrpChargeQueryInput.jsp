<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LMGrpChargeQueryInput.jsp
//程序功能：
//创建日期：2003-11-05 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LMGrpChargeQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LMGrpChargeQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>集体手续费信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./LMGrpChargeQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMGrpChargeGrid1);">
      </td>
      <td class= titleImg>
        请您输入查询条件： 
      </td>
    	</tr>   
    </table>
  <Div  id= "divLMGrpChargeGrid1" style= "display: ''">    
  <table  class= common>
  <TR  class= common>
    <TD  class= title>
      集体保单号码
    </TD>
    <TD  class= input>
      <Input class= common name=GrpPolNo >
    </TD>
    <TD  class= title>
      手续费类型
    </TD>
    <TD  class= input>
      <Input class= common name=ChargeType >
    </TD>
  </TR>
   <tr class=common>
    <TD  class= title>
        管理机构
    </TD>
    <TD  class= input>
        <Input class="code" name=Managecom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
    </TD>    
    <td class=title nowrap>投保人客户号</td>
    <td class=input > <input class=common name=GrpNo></td>
   </tr>  
  
  
  
</table>
 </Div>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();" class="cssButton">					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMGrpChargeGrid);">
    		</td>
    		<td class= titleImg>
    			 集体手续费查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLMGrpChargeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLMGrpChargeGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class="cssButton">				
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class="cssButton">
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();" class="cssButton">					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
