<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LLMainAskSave.jsp
//程序功能：
//创建日期：2005-01-12 16:10:37
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
//接收信息，并作校验处理。
//输入参数
TransferData tTransferData = new TransferData();
ClaimCheckUI tClaimCheckUI = new ClaimCheckUI();
//输出参数
CErrors tError = null;
String tRela  = "";
String FlagStr = "";
String Content = "";
String transact = "";
GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
String Path = application.getRealPath("config//Conversion.config");
//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
transact = request.getParameter("fmtransact");
//添加至咨询登记表中
String tMngCom = request.getParameter("Station");
String tClaimer = request.getParameter("Claimer");
String tAgentCode = request.getParameter("AgentCode");
String tDealWith = request.getParameter("DealWith");
String tGiveType = request.getParameter("GiveType");
String tRate = request.getParameter("Rate");
String tRgtDateS = request.getParameter("RgtDateS");
String tRgtDateE = request.getParameter("RgtDateE");
String tClaimPopedom = request.getParameter("ClaimPopedom");
String tOpTime = request.getParameter("OpTime");
String tAccTimes = request.getParameter("AccTimes");
String tAccDateS = request.getParameter("AccDateS");
String tAccDateE = request.getParameter("AccDateE");
String tQuery = request.getParameter("Query");

tTransferData.setNameAndValue("MngCom",tMngCom);
tTransferData.setNameAndValue("Claimer",tClaimer);
tTransferData.setNameAndValue("AgentCode",tAgentCode);
tTransferData.setNameAndValue("DealWith",tDealWith);
tTransferData.setNameAndValue("GiveType",tGiveType);
tTransferData.setNameAndValue("Rate",tRate);
tTransferData.setNameAndValue("RgtDateS",tRgtDateS);
tTransferData.setNameAndValue("RgtDateE",tRgtDateE);
tTransferData.setNameAndValue("ClaimPopedom",tClaimPopedom);
tTransferData.setNameAndValue("OpTime",tOpTime);
tTransferData.setNameAndValue("AccTimeS",tAccDateS);
tTransferData.setNameAndValue("AccDateE",tAccDateE);
tTransferData.setNameAndValue("Query",tQuery);

try
{
  // 准备传输数据 VData
  VData tVData = new VData();
  tVData.add(tTransferData);
  tVData.add(tG);
  if(!tClaimCheckUI.submitData(tVData,transact))    //提取案件信息
  {
    Content = "保存失败，原因是" ;
    FlagStr = "Fail";
  }
}
catch(Exception ex)
{
  Content = "保存失败，原因是:" + ex.toString();
  FlagStr = "Fail";
}
    System.out.println("submitData Finished"+FlagStr);
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr.equals(""))
{
  tError = tClaimCheckUI.mErrors;
  if (!tError.needDealError())
  {
    Content = " 保存成功! ";
    FlagStr = "Success";
  }
  else
  {
    Content = " 保存失败，原因是:" + tError.getFirstError();
    FlagStr = "Fail";
  }
}

//添加各种预处理

%>

<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
