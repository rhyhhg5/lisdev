<%
//程序名称：BlackListInput.jsp
//程序功能：
//创建日期：2003-01-10
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LDBlackListSchema tLDBlackListSchema   = new LDBlackListSchema();
	LDBlackListReasonDetailSet tLDBlackListReasonDetailSet = new LDBlackListReasonDetailSet();
  LDBlackListUI tLDBlackListUI = new LDBlackListUI();

  //输出参数
  CErrors tError = null;
  String tOperate=request.getParameter("hideOperate");
  tOperate=tOperate.trim();
  String tRela  = "";                
  String FlagStr = "Fail";
  String Content = "";

	GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

    tLDBlackListSchema.setBlackListNo(request.getParameter("BlackListNo"));
    tLDBlackListSchema.setBlackName(request.getParameter("BlackName"));
   	tLDBlackListSchema.setBlackListType(request.getParameter("BlackListType"));	
    tLDBlackListSchema.setBlackListOperator(request.getParameter("BlackListOperator"));
    tLDBlackListSchema.setBlackListMakeDate(request.getParameter("BlackListMakeDate"));
		tLDBlackListSchema.setBlackListSource(request.getParameter("Origin"));
		tLDBlackListSchema.setRelaContNo(request.getParameter("ContNo"));
		tLDBlackListSchema.setRelaPolNo(request.getParameter("ContNo"));
		if(tOperate.equals("INSERT")) {
			tLDBlackListSchema.setRelaOtherNo(request.getParameter("CaseNo1"));
		} else {
			tLDBlackListSchema.setRelaOtherNo(request.getParameter("CaseNo"));
		}
		tLDBlackListSchema.setRelaOtherNoType(request.getParameter("Origin"));
		tLDBlackListSchema.setIDNo(request.getParameter("IDNo"));
    String Path = application.getRealPath("config//Conversion.config");
		tLDBlackListSchema.setBlackListReason(StrTool.Conversion(request.getParameter("BlackListReason"),Path));
		String tChk[] = request.getParameterValues("InpBlackListReasonGridChk"); 
		String treasoncode[] = request.getParameterValues("BlackListReasonGrid1"); 
		String treason[] = request.getParameterValues("BlackListReasonGrid2"); 
		for(int index=0;index<tChk.length;index++)
	 	{
	  	if(tChk[index].equals("1"))
	   	{
	     LDBlackListReasonDetailSchema tLDBlackListReasonDetailSchema = new LDBlackListReasonDetailSchema();
	     tLDBlackListReasonDetailSchema.setBlackListReasonKind(treasoncode[index]);
	     tLDBlackListReasonDetailSchema.setBlackListReasonDetail(treason[index]);
	     tLDBlackListReasonDetailSet.add(tLDBlackListReasonDetailSchema);
	    }
	  }


  // 准备传输数据 VData
  VData tVData = new VData();
  FlagStr="";
	tVData.addElement(tLDBlackListSchema);
	tVData.addElement(tLDBlackListReasonDetailSet);
	tVData.addElement(tG);
  try
  {
    tLDBlackListUI.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  if (!FlagStr.equals("Fail"))
  {
    tError = tLDBlackListUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	//Content = " 保存失败，请检查数据是否正确";
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

