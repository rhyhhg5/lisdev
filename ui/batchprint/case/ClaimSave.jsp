<%
//程序名称：ClaimSave.jsp
//程序功能：
//创建日期：2002-07-21 20:09:16
//创建人  ：wujs
//更新记录：  更新人    更新日期     更新原因/内容

%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<!--%@include file="./CaseCommonSave.jsp"%-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import=" java.text.*"%>
<%@page import= "java.util.*"%>


<%
//接收信息，并作校验处理。
//输入参数

LLClaimSchema tLLClaimSchema   = new LLClaimSchema();
LLClaimSet tLLClaimSet=new LLClaimSet();
LLClaimDetailSet tLLClaimDetailSet=new LLClaimDetailSet();

LLClaimPolicySchema tLLClaimPolicySchema = null;
LLCaseSchema tLLCaseSchema = new LLCaseSchema();
LLClaimPolicySet tLLClaimPolicySet = new LLClaimPolicySet();


GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
TransferData RecalFlag = new TransferData();

String transact="";
String FlagStr = "";
String Content = "";
CErrors tError = null;
String tOpt=request.getParameter("Opt");
System.out.println("opt=="+tOpt);
String  CaseNo = request.getParameter("CaseNo");
System.out.println("CaseNo"+ CaseNo );
String RgtNo = request.getParameter("RgtNo");

// ==  变更的地方为ClaimDetailGrid 后台涉及到这个地方需要处理
if (tOpt.equals("cal") ) {
  transact = "Cal";
  if(tOpt.equals("recal")){
    RecalFlag.setNameAndValue("RecalFlag","Y");
  }
  else{
    RecalFlag.setNameAndValue("RecalFlag","N");
  }
  tLLCaseSchema.setCaseNo(CaseNo);

  String[] strContNo=request.getParameterValues("ClaimPolGrid1");
  System.out.println("显示修改后的保单号码==========="+strContNo);
  String[] strRiskCode=request.getParameterValues("ClaimPolGrid11");
  String[] strGetDutyKind=request.getParameterValues("ClaimPolGrid3");
  String[] strPolNo=request.getParameterValues("ClaimPolGrid7");// 

  int ClaimPolCount = strContNo.length;
  int tSelNo =0 ;
  for (int i = 0; i < ClaimPolCount; i++){
    if(strContNo[i]!=null&&strPolNo[i]!=null&&!strContNo[i].equals("")&&!strPolNo[i].equals("")){
      tLLClaimPolicySchema= new LLClaimPolicySchema();
      tLLClaimPolicySchema.setRgtNo(request.getParameter("RgtNo"));
      tLLClaimPolicySchema.setContNo( strContNo[i]);
      tLLClaimPolicySchema.setRiskCode(strRiskCode[i]);
      tLLClaimPolicySchema.setCaseNo(CaseNo);
      tLLClaimPolicySchema.setGetDutyKind(strGetDutyKind[i]);
      tLLClaimPolicySchema.setPolNo(strPolNo[i]);
      tLLClaimPolicySet.add(tLLClaimPolicySchema);
    }
  }
  ClaimCalUI tClaimCalUI   = new ClaimCalUI();

  // 准备传输数据 VData
  VData tVData = new VData();
  try{
    tVData.addElement(tG);
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tLLClaimPolicySet);
    tVData.addElement(RecalFlag);
    tClaimCalUI.submitData(tVData,transact);
  }
  catch(Exception ex){
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  if (FlagStr==""){
    tError = tClaimCalUI.mErrors;
    if (!tError.needDealError()){
      Content = " 计算成功!"+tClaimCalUI.getBackMsg();
      FlagStr = "Succ";
    }
    else{
      Content = " 计算失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
}

if (tOpt.equals("save")) {
  transact = "SAVE";

  String[] tChk = request.getParameterValues("InpClaimDetailGridChk");
  String[] strContNo1=request.getParameterValues("ClaimDetailGrid1");
  String[] strGetDutyCode=request.getParameterValues("ClaimDetailGrid18");// 往后移一位 原先为17
  String[] strGetDutyKind1=request.getParameterValues("ClaimDetailGrid19");// 往后移一位 原先为18
  String[] StandPay=request.getParameterValues("ClaimDetailGrid14");// 往后移一位 原先为13
  String[] strRealPay1=request.getParameterValues("ClaimDetailGrid15");// 往后移一位 原先为14
  String[] strPolNo1=request.getParameterValues("ClaimDetailGrid16");// 往后移一位 原先为15
  String[] strClmNo1=request.getParameterValues("ClaimDetailGrid17");// 往后移一位 原先为16
  String[] strDutyCode1=request.getParameterValues("ClaimDetailGrid20");// 往后移一位 原先为19
  String[] GiveType1=request.getParameterValues("ClaimDetailGrid21");// 往后移一位 原先为20
  String[] GiveTypeDesc1=request.getParameterValues("ClaimDetailGrid10");
  String[] GiveReason1=request.getParameterValues("ClaimDetailGrid22");//往后移一位 原先为21
  String[] GiveReasonDesc1=request.getParameterValues("ClaimDetailGrid11");
  String[] caserelano=request.getParameterValues("ClaimDetailGrid23");//往后移一位 原先为22
  String[] IsRate=request.getParameterValues("ClaimDetailGrid24");//往后移一位 原先为23
  
  


  //====== ADD ===== zhangtao ====== 2005-03-08 ================= BGN ============
  String[] DeclineAmnts = request.getParameterValues("ClaimDetailGrid5");
  String[] ApproveAmnts = request.getParameterValues("ClaimDetailGrid7");
  //====== ADD ===== zhangtao ====== 2005-03-08 ================= END ============
	  
  //====== ADD ===== fant ====== 2012-10-31 ================= BGN ============
  String[] Remark = request.getParameterValues("ClaimDetailGrid12");// 获取前台备注框信息
  //====== ADD ===== fant ====== 2012-10-31 ================= BGN ============
  int ClaimPolCount = strContNo1.length;
  
  tLLCaseSchema.setCaseNo(CaseNo);
  if (request.getParameterValues("EasyCase") != null) {
     tLLCaseSchema.setCaseProp("09");
	} else{
	   tLLCaseSchema.setCaseProp("08");
	}

  for (int i = 0; i < ClaimPolCount; i++){
    if(tChk[i].equals("0")) //未选
    continue;
    System.out.println("...........");
    LLClaimDetailSchema tLLClaimDetailSchema= new LLClaimDetailSchema();
    tLLClaimDetailSchema.setRgtNo(request.getParameter("RgtNo"));

    tLLClaimDetailSchema.setContNo( strContNo1[i]);
    tLLClaimDetailSchema.setGetDutyCode(strGetDutyCode[i]);
    tLLClaimDetailSchema.setGetDutyKind(strGetDutyKind1[i]);
    tLLClaimDetailSchema.setStandPay(StandPay[i]);
    tLLClaimDetailSchema.setRealPay(strRealPay1[i]);
    tLLClaimDetailSchema.setPolNo(strPolNo1[i]);
    tLLClaimDetailSchema.setClmNo(strClmNo1[i]);
    tLLClaimDetailSchema.setDutyCode( strDutyCode1[i]);
    tLLClaimDetailSchema.setGiveType(GiveType1[i]);
    tLLClaimDetailSchema.setGiveTypeDesc(GiveTypeDesc1[i]);
    tLLClaimDetailSchema.setGiveReason(GiveReason1[i]);
    tLLClaimDetailSchema.setGiveReasonDesc(GiveReasonDesc1[i]);
    tLLClaimDetailSchema.setCaseRelaNo(caserelano[i]);
    tLLClaimDetailSchema.setCaseNo(CaseNo);
    tLLClaimDetailSchema.setDeclineNo(IsRate[i]);

    //====== ADD ===== zhangtao ====== 2005-03-08 ================= BGN ============
    tLLClaimDetailSchema.setDeclineAmnt(DeclineAmnts[i]);
    tLLClaimDetailSchema.setApproveAmnt(ApproveAmnts[i]);
    System.out.println("DeclineAmnts[i]********:" + DeclineAmnts[i]);
    System.out.println("ApproveAmnts[i]********:" + ApproveAmnts[i]);
    //====== ADD ===== zhangtao ====== 2005-03-08 ================= END ============

    //====== ADD ===== fant ====== 2012-10-31 ================= BGN ============
     tLLClaimDetailSchema.setREMARK(Remark[i]);
    System.out.println("Remark[i]********------->>:" + Remark[i]);
//  ====== ADD ===== fant ====== 2012-10-31 ================= BGN ============
    
    tLLClaimDetailSet.add(tLLClaimDetailSchema);
    
  }

  ClaimSaveUI tClaimSaveUI   = new ClaimSaveUI();
  boolean cs= true;
  // 准备传输数据 VData
  VData tVData = new VData();
  try{
    tVData.addElement(tG);
    tVData.addElement(tLLClaimDetailSet);
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tLLClaimSet);
    cs= tClaimSaveUI.submitData(tVData,transact);
  }
  catch(Exception ex){
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (FlagStr==""){
    tError = tClaimSaveUI.mErrors;
    if (cs || !tError.needDealError()){
      Content = "保存成功"+tClaimSaveUI.getBackMsg();
      FlagStr = "Succ";
    }
    else{
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
}
if (tOpt.equals("recal")) {
  transact = "SAVE";
System.out.println("in recal aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
  String[] tChk = request.getParameterValues("InpClaimResultGridChk");
  String[] strContNo=request.getParameterValues("ClaimResultGrid1");
  String[] strTabFee=request.getParameterValues("ClaimResultGrid4");
  String[] strRefuseAmnt=request.getParameterValues("ClaimResultGrid5");
  String[] OutDutyRate=request.getParameterValues("ClaimResultGrid6");
  String[] OutDutyAmnt=request.getParameterValues("ClaimResultGrid7");
  String[] MaxAmnt=request.getParameterValues("ClaimResultGrid8");
  String[] strClmMoney=request.getParameterValues("ClaimResultGrid9");
  String[] StandPay=request.getParameterValues("ClaimResultGrid10");
  String[] strRealPay=request.getParameterValues("ClaimResultGrid11");
  String[] strPolNo=request.getParameterValues("ClaimResultGrid12");
  String[] strClmNo=request.getParameterValues("ClaimResultGrid13");
  String[] strGetDutyCode=request.getParameterValues("ClaimResultGrid14");
  String[] strGetDutyKind=request.getParameterValues("ClaimResultGrid15");
  String[] strDutyCode=request.getParameterValues("ClaimResultGrid16");
  String[] caserelano=request.getParameterValues("ClaimResultGrid17");
  String[] strapproveamnt=request.getParameterValues("ClaimResultGrid20");

  int ClaimDutyCount = strContNo.length;

  for (int i = 0; i < ClaimDutyCount; i++){
    if(tChk[i].equals("0")) //未选
    continue;
    System.out.println("...........");
    LLClaimDetailSchema tLLClaimDetailSchema= new LLClaimDetailSchema();
    tLLClaimDetailSchema.setRgtNo(RgtNo);

    tLLClaimDetailSchema.setContNo( strContNo[i]);
    tLLClaimDetailSchema.setPolNo(strPolNo[i]);
    tLLClaimDetailSchema.setClmNo(strClmNo[i]);
    tLLClaimDetailSchema.setDutyCode( strDutyCode[i]);
    tLLClaimDetailSchema.setGetDutyCode(strGetDutyCode[i]);
    tLLClaimDetailSchema.setGetDutyKind(strGetDutyKind[i]);
    tLLClaimDetailSchema.setTabFeeMoney(strTabFee[i]);
    tLLClaimDetailSchema.setClaimMoney(strClmMoney[i]);
    tLLClaimDetailSchema.setOutDutyAmnt(OutDutyAmnt[i]);
    tLLClaimDetailSchema.setOutDutyRate(OutDutyRate[i]);
    tLLClaimDetailSchema.setDeclineAmnt(strRefuseAmnt[i]);
    tLLClaimDetailSchema.setStandPay(StandPay[i]);
    tLLClaimDetailSchema.setRealPay(strRealPay[i]);
    tLLClaimDetailSchema.setCaseRelaNo(caserelano[i]);
    tLLClaimDetailSchema.setApproveAmnt(strapproveamnt[i]);
    tLLClaimDetailSchema.setOverAmnt(MaxAmnt[i]);
    tLLClaimDetailSchema.setCaseNo(CaseNo);
    System.out.println("*********************************************************:" + CaseNo);
    System.out.println("*********************************************************:" + tLLClaimDetailSchema.getApproveAmnt());
     System.out.println("*********************************************************"+tLLClaimDetailSchema.getOutDutyAmnt());
     System.out.println("*********************************************************"+tLLClaimDetailSchema.getOutDutyRate());
    tLLClaimDetailSet.add(tLLClaimDetailSchema);
  }

  ClaimReCalUI tClaimReCalUI   = new ClaimReCalUI();
  boolean cs= true;
  // 准备传输数据 VData
  VData tVData = new VData();
  try{
    tVData.addElement(tG);
    tVData.addElement(tLLClaimDetailSet);
    cs= tClaimReCalUI.submitData(tVData,transact);
  }
  catch(Exception ex){
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (FlagStr==""){
    tError = tClaimReCalUI.mErrors;
    if (cs || !tError.needDealError()){
      Content = "保存成功"+tClaimReCalUI.getBackMsg();
      FlagStr = "Succ";
    }
    else{
      Content = " 保存失败，原因是:" + tError.getFirstError();
      FlagStr = "Fail";
    }
  }
}

%>
<html>
<script language="javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
