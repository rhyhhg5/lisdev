<html>
  <%
  //Name：LLMainAskInput.jsp
  //Function：登记界面的初始化
  //Date：2005-4-23 16:49:22
  //Author：Xx
  %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import = "com.sinosoft.utility.*"%>
  <%@page import = "com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
  <%@page import = "com.sinosoft.lis.vschema.*"%>
  <%@page import = "com.sinosoft.lis.llcase.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="java.util.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String Operator=tG.Operator;
  String Comcode=tG.ManageCom;

  String CurrentDate= PubFun.getCurrentDate();
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);
  String AheadDays="-90";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  FDate fdate = new FDate();
  String afterdate = fdate.getString( AfterDate );
  %>

  <head>
    <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
    <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
    <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
    <SCRIPT src="ClaimCheckList.js"></SCRIPT>
    <%@include file="ClaimCheckListInit.jsp"%>
    <script language="javascript">
      function initDate(){
        fm.RgtDateS.value="<%=afterdate%>";
        fm.RgtDateE.value="<%=CurrentDate%>";
        var usercode="<%=Operator%>";
        var comcode="<%=Comcode%>";
        fm.Operator.value=usercode;
        fm.Station.value=comcode;
        showAllCodeName();

        var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
        var arrResult = easyExecSql(strSQL);
        if(arrResult != null)
          fm.optname.value= arrResult[0][0];
      }
    </script>
  </head>
  <body  onload="initDate();initForm();">
    <form action="./ClaimCheckSave.jsp" method=post name=fm target="fraSubmit">
      <table>
        <TR>
          <TD>
            <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
          </TD>
          <TD class= titleImg>
            稽查指定范围
          </TD>
        </TR>
      </table>

      <Div  id= "divLLLLMainAskInput1" style= "display: ''">
        <table  class= common>
          <TR  class= common8>
          </tr>
          <TR  class= common8>
            <TD  class= title>稽查机构</TD>
            <TD  class= input><Input class="codeno" name=Station verify="机构代码|notnull&code:ComCode" ondblclick="return showCodeList('ComCode',[this, StationName], [0, 1]);" onkeyup="return showCodeListKey('ComCode', [this, StationName], [0, 1]);"><Input class= "codename" readonly name= StationName  ></TD>
            <TD  class= title >处理人</TD>
            <TD  class= input><Input class= "codeno"  name=Claimer ondblclick="return showCodeList('optnameunclass',[this, ClaimerName], [0, 1]);" onkeyup="return showCodeListKey('optnameunclass', [this, ClaimerName], [0, 1]);" ><Input class= "codename" name=ClaimerName ></TD>
            <TD  class= title8>业务员</TD>
            <TD  class= input8><Input class= "codeno"  name=AgentCode ondblclick="return showCodeList('agentcode',[this, AgentName], [0, 1]);" onkeyup="return showCodeListKey('agentcode', [this, AgentName], [0,1]);" ><Input  class='codename'  name=AgentName></TD>
          </TR>
          <TR  class= common>
            <TD  class= title8>案件类型</TD>
            <TD  class= input8> <input class=codeno CodeData="0|3^0|申请类^1|申诉类^2|错误处理类 "  name=DealWith ondblclick="return showCodeListEx('DealWith',[this,DealWithName],[0,1]);" onkeyup="return showCodeListKeyEx('DealWith',[this,DealWithNam],[0,1]);"><input class=codename name=DealWithName></TD>
            <TD  class= title8>给付类型</TD>
            <TD  class= input8> <input class=codeno name=GiveType ondblclick="return showCodeList('llclaimdecision',[this,GiveTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('llclaimdecision',[this,GiveTypeName],[0,1],null,null,null,1);"><input class=codename name=GiveTypeName></TD>
            <TD  class= title8>给付金额</TD>
            <TD  class= input8> <input class=codeno CodeData="0|2^1|1000^2|20000"  name=PayMoney ondblclick="return showCodeListEx('PayMoney',[this,PayMoneyN],[0,1]);" onkeyup="return showCodeListKeyEx('PayMoney',[this,PayMoneyN],[0,1]);"><input class=codename name=PayMoneyN></TD>
          </TR>
          <TR  class= common>
            <TD  class= title8>稽查比例</TD>
            <TD  class= input8> <input class=common name=Rate onblur="checkNumber(ClaimSpotRate);"></TD>
            <TD  class= title8>受理日期从</TD>
            <TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
            <TD  class= title8>到</TD>
            <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
          </TR>
        </table>
        <table style='display:none'>
          <tr>
            <TD class= title>用户核赔权限</TD>
            <TD class=input><Input class="codeno" name=ClaimPopedom ondblClick="showCodeList('llclaimpopedom',[this,ClaimPopedomName],[0,1]);"  onkeyup="showCodeListKey('llclaimpopedom',[this,ClaimPopedomName],[0,1]);"><Input class="codename" name=ClaimPopedomName></TD>
            <TD  class= title8>结案时效</TD>
            <TD  class= input8> <input class=codeno CodeData="0|7^1|1日^3|3日^7|7日^10|10日^20|20日 "  name=OpTime ondblclick="return showCodeListEx('OpTime',[this,OpTimeName],[0,1]);" onkeyup="return showCodeListKeyEx('OpTime',[this,OpTimeName],[0,1]);"><input class=codename name=OpTimeName></TD>
            <TD  class= title8>就诊医院</TD>
            <TD  class= input8> <input class=codeno  name=HospitalCode onclick="return showCodeList('llhospiquery',[this,HospitalName],[0,1],null,fm.HospitalName.value,'Hospitname',0,240);" onkeyup="return showCodeListKeyEx('llhospiquery',[this,HospitalName],[0,1],null,fm.HospitalName.value,'Hospitname',1,240);" ><input class=codename  name=HospitalName elementtype=nacessary verify="医院名称|notnull"></TD>
          </tr>
          <TR  class= common>
            <TD  class= title8>出险日期从</TD>
            <TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=AccDateS onkeydown="QueryOnKeyDown()"></TD>
            <TD  class= title8>到</TD>
            <TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=AccDateE onkeydown="QueryOnKeyDown()"></TD>
            <TD  class= title8>出险次数</TD>
            <TD  class= input8> <input class=codeno CodeData="0|^1|1次^2|2次^3|3次^4|3次以上 "  name=AccTimes ondblclick="return showCodeListEx('AccTimes',[this,AccTimesName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('AccTimes',[this,AccTimesName],[0,1],null,null,null,1);"><input class=codename name=AccTimesName></TD>
          </TR>
          <TR>
            <TD  class= title8>调查类型</TD>
            <TD  class= input8> <input class=codeno CodeData="0|^1|调查件^0|非调查件"  name=Query ondblclick="return showCodeListEx('Query',[this,QueryName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('Query',[this,QueryName],[0,1],null,null,null,1);"><input class=codename name=QueryName></TD>
          </TR>
        </table>
          <table>
            <TR  class= common8 >
              <TD align=left colSpan=1><input  class=cssButton type=button value="稽   查" onclick="submitForm()"></TD>
            </tr>
          </table>
        <br>
        <!--  <input style="display:''"  class=cssButton type=button value="查 询" onclick="easyQuery()"> -->
        <Div  id= "divCustomerInfo" style= "display: ''">
          <table>
            <TR>
              <TD>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLLLLMainAskInput1);">
              </TD>
              <TD class= titleImg>
                稽查案件列表
              </TD>
            </TR>
          </table>
          <table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanCheckGrid" >
                </span>
              </TD>
            </TR>
          </table>
        </Div>
        
        <Div  align=center style= "display: '' ">
          <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
          <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
          <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
          <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
        </Div>
        <hr>
        <Div>
          <table  class= common>
            <TR  class= common8>
              <TD><input  class=cssButton type=button value="确   认" onclick="submitForm()"></TD>
              <TD  class= title>稽查人姓名</TD><TD  class= input>
                <Input class= "readonly" readonly  name=optname onkeydown="QueryOnKeyDown();" ondblclick="getRights();return showCodeList('optname',[this, Operator], [0, 1],null,Str,'1');" onkeyup="getRights();return showCodeListKey('optname', [this, Operator], [0, 1],null,Str,'1');" >
              </TD>
              <TD  class= input><Input type=hidden readonly name=Operator></TD>
            </TR>
          </table>
        </div>
            <!--隐藏域-->
            <Input type="hidden" class= common name="fmtransact" >
            <Input type=hidden name=AllOperator>
            <input type=hidden name=sql>
          </form>
          <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
        </body>
      </html>
