<html>
	<%
	//Name:RegisterInput.jsp
	//Function：立案界面的初始化程序
	//Date：2002-07-21 17:44:28
	//Author ：LiuYansong
	%>
	<%@page contentType="text/html;charset=GBK" %>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%
    GlobalInput GI = new GlobalInput();
    GI = (GlobalInput)session.getValue("GI");
  %>
	<script>
  var manageCom = "<%=GI.ManageCom%>"; //记录管理机构
  var ComCode = "<%=GI.ComCode%>";//记录登陆机构
  </script>
	<head >
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="ClaimUserInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="ClaimUserInit.jsp"%>
	  <script language="javascript">
	    function initDate(){
	    	fm.Station.value=manageCom;
      }
    </script>
	</head>
	<body  onload="initForm();initDate();">
		<form action="" method=post name=fm target="fraSubmit">
			<%@include file="../common/jsp/OperateButton.jsp"%>
			<%@include file="../common/jsp/InputButton.jsp"%>

			<table class=common>
				<tr class= titleImg>
					<TD class= titleImg > 理赔岗位用户维护信息 </TD>
				</tr>
			</table>

			<Div id= "divRegisterInfo" style= "display: ''">
				<table class=common>
					<TR>
						<TD  class= title>
							用户机构代码
						</TD>
						<TD  class= input>
							<Input class="code" name=Station verify="机构代码|notnull&code:ComCode" ondblclick="return showCodeList('ComCode',[this, StationName], [0, 1]);" onkeyup="return showCodeListKey('ComCode', [this, StationName], [0, 1]);">
						</TD>
						<TD  class= title>
							用户机构名称
						</TD>
						<TD  class= input>
							<Input class= "readonly" readonly name= StationName  >
						</TD>
					</TR>

					<TR class=common >
						<TD  class= title >
							用户编码
						</TD>
						<TD  class= input>
							<Input class= "code"  name=UserCode ondblclick="return showCodeList('UserCode',[this, UserName], [0, 1],null,fm.Station.value,'ComCode');" onkeyup="return showCodeListKey('UserCode', [this, UserName], [0, 1],null,fm.Station.value,'ComCode');" >
						</TD>
						<TD  class= title >
							用户名称
						</TD>
						<TD  class= input>
							<Input class="readonly" readonly name=UserName >
						</TD>
					</TR>


					<TR class= common>

						<TD class= title>
							用户核赔权限
						</TD>
						<TD class=input>
							<Input class="codeno" name=ClaimPopedom ondblClick="showCodeList('llclaimpopedom',[this,ClaimPopedomName],[0,1]);"  onkeyup="showCodeListKey('llclaimpopedom',[this,ClaimPopedomName],[0,1]);"><Input class="codename" name=ClaimPopedomName>
						</TD>
						<TD class= title>
							上级用户代码
						</TD>
						<TD class = input>
						  <Input class= "codeno"  name=UpUserCode ondblclick="return showCodeList('llusercode',[this,UpUserName],[0,1],null,'1 and StateFlag not in (#0#,#3#,#4#,#9#) and usercode<>#claim# ','1');" onkeyup="return showCodeListKey('llusercode',[this,UpUserName],[0,1],null,'1 and StateFlag not in (#0#,#3#,#4#,#9#) and usercode<>#claim# ','1');" ><Input class= "codename" name=UpUserName >
						</TD>
					</TR>

					<!--以上隐藏-->
					<TR  class= common style="display:'none'">
						<TD  class= title>
							立案权限
						</TD>
						<TD  class= input>
							<Input class="code"  value="1" name=RgtFlag 
							CodeData="0|^0|无立案权限^1|有立案权限"
							ondblClick="showCodeListEx('RgtFlag_1',[this],[0,1]);"
							onkeyup="showCodeListKeyEx('RgtFlag_1',[this],[0,1]);">
						</TD>


					</TR>

					<TR  class= common style="display:'none'">

						<TD  class= title>
							理算审核权限
						</TD>
						<TD  class= input>
							<Input class="code"  value="1" name=CheckFlag 
							CodeData="0|^0|无理算审核权限1|有理算审核权限"
							ondblClick="showCodeListEx('CheckFlag_1',[this],[0,1]);"
							onkeyup="showCodeListKeyEx('CheckFlag_1',[this],[0,1]);">
						</TD>
					</tr>
					<!--以上隐藏-->


					<TR  class= common>
						<TD  class= title>
							理赔员权限
						</TD>
						<TD  class= input>
							<Input class="codeno" name=ClaimDeal verify="理赔员权限|NOTNULL"
							CodeData="0|^0|无权限^1|有理赔权限"
							ondblClick="showCodeListEx('RgtFlag_1',[this,ClaimDealName],[0,1]);"
							onkeyup="showCodeListKeyEx('RgtFlag_1',[this,ClaimDealName],[0,1]);"><input class="codename" name=ClaimDealName>
						</TD>
						<TD  class= title>
							调查权限
						</TD>
						<TD  class= input>
							<Input class="codeno" name=ServeyFlag verify="调查权限|NOTNULL"
							CodeData="0|^0|无调查权限^1|调查主管^2|普通调查员"
							ondblClick="showCodeListEx('ServeyFlag_1',[this,ServeyFlagName],[0,1]);"
							onkeyup="showCodeListKeyEx('ServeyFlag_1',[this,ServeyFlagName],[0,1]);"><Input class=codename name=ServeyFlagName>
						</TD>
					<TR>
						<TD  class= title>
							参加案件分配
						</TD>
						<TD  class= input>

							<Input class=codeno name=HandleFlag verify="参加案件分配|NOTNULL"
							CodeData="0|^0|否^1|是"
							ondblClick="showCodeListEx('HandleFlag_1',[this,HandleFlagName],[0,1]);"
							onkeyup="showCodeListKeyEx('HandleFlag_1',[this,HandleFlagName],[0,1]);"><Input class=codename name=HandleFlagName>
						</TD>
						<TD  class= title>
							案件分配上限
						</TD>
						<TD  class= input>
							<Input class=common name="DispatchRate"  onblur="checkNumber(DispatchRate);">
						</TD>
					</TR>
					<TR>
						<TD  class= title>
							用户有效状态
						</TD>
						<TD  class= input>
							<Input class="codeno" name=StateFlag verify="用户有效状态|NOTNULL"
							CodeData="0|^1|有效^2|休假^3|离职^4|离岗^9|其他无效情况"
							ondblClick="showCodeListEx('StateFlag_1',[this,StateFlagName],[0,1]);"
							onkeyup="showCodeListKeyEx('StateFlag_1',[this,StateFlagName],[0,1]);"><Input class=codename name=StateFlagName>

						</TD>
						<TD  class= title>
							理赔抽检比例
						</TD>
						<TD  class= input>
							<Input class=common name="ClaimSpotRate"  onblur="checkNumber(ClaimSpotRate);">
						</TD>

					</TR>
<!--					<TR  class= common>
						<TD  class= title>
							参加账单分配
						</TD>
						<TD  class= input>
							<Input class=codeno name=InputFlag 
							CodeData="0|^0|否^1|是"
							ondblClick="showCodeListEx('HandleFlag_1',[this,InputFlagName],[0,1]);"
							onkeyup="showCodeListKeyEx('HandleFlag_1',[this,InputFlagName],[0,1]);"><Input class=codename name=InputFlagName>
						</TD>
					</TR> -->
					<TR>
						<TD  class= title>
							预付赔款审批权限
						</TD>
						<TD  class= input>
							<Input class="codeno" name=PrepaidFlag 
							CodeData="0|^0|否^1|是"
							ondblClick="showCodeListEx('PrepaidFlag',[this,PrepaidFlagName],[0,1]);"
							onkeyup="showCodeListKeyEx('PrepaidFlag',[this,PrepaidFlagName],[0,1]);"><Input class= "codename" name=PrepaidFlagName >
						</TD>
						<TD  class= title>
							预付赔款审批额度
						</TD>
						<TD  class= input>
							<Input class=common name="PrepaidLimit"  onblur="checkNumber(PrepaidLimit);">
						</TD>
					</TR>
					<TR>
						<TD  class= title>
							预付赔款审批上级用户
						</TD>
						<TD class = input>
						  <Input class= "codeno"  name=PrepaidUpUserCode ondblclick="return showCodeList('optnameunclass',[this, PrepaidUpUserCodeName], [0, 1]);" onkeyup="return showCodeListKey('optnameunclass', [this, PrepaidUpUserCodeName], [0, 1]);" ><Input class= "codename" name=PrepaidUpUserCodeName >
						</TD>

					</TR>
				</table>
				
				
			<table class=common>
				<tr class= titleImg>
					<TD class= titleImg >特需业务相关</TD>
				</tr>
			</table>
			
				<table class=common>
					<TR>
						<TD  class= title>
							特需业务审批权限
						</TD>
						<TD  class= input> 
							<input class="codeno" CodeData="0|2^0|否^1|是"  name=SpecialNeedFlag ondblclick="return showCodeListEx('SpecialNeedFlag',[this,SpecialNeedName],[0,1],null,null,null,1);" onkeydown="QueryOnKeyDown();" onkeyup="return showCodeListKeyEx('SpecialNeedFlag',[this,SpecialNeedName],[0,1]);" value="0"><input class=codename name=SpecialNeedName value="否">
						 </TD>
						<TD  class= title>
							特需业务审批额度
						</TD>
						<TD  class= input>
						<Input class=common name="SpecialNeedLimit"  onblur="checkNumber(SpecialNeedLimit);">
						</TD>
					</TR>

					<TR class=common >
						<TD  class= title >
							特需业务审批上级用户
						</TD>
						<TD  class= input>
							<Input class= "codeno"  name=SpecialNeedUpUserCode ondblclick="return showCodeList('optnameunclass',[this, SpecialNeedUpUserCodeName], [0, 1]);" onkeyup="return showCodeListKey('optnameunclass', [this, SpecialNeedUpUserCodeName], [0, 1]);" ><Input class= "codename" name=SpecialNeedUpUserCodeName >
						</TD>
					</TR>
					</table>

			<table class= common>
				<TR  class= common>
					<TD  class= title>
						备注
					</TD>
				</TR>
				<TR class= common>
					<TD  class= input>
					<textarea name=Remark cols="110%" rows="3" witdh=25% class="common"  ></textarea></TD>
				</TR>
			</table>
			
			<Div  id= "divUnclose" style= "display: ''">
        <table>
          <tr>
            <td class=common>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCaseDis);">
            </td>
            <td class= titleImg>
              案件分配配置
            </td>
          </tr>
        </table>
        <Div  id= "divCaseDis" align=center style= "display: ''">
          <table  class= common>
            <TR  class= common>
              <TD text-align: left colSpan=1>
                <span id="spanCaseDisGrid" >
                </span>
              </TD>
            </TR>
          </table>
        </Div>
      </Div>
      <HR>
      <table >
		<input type=hidden id="fmtransact" name="fmtransact">
	</form>

	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>