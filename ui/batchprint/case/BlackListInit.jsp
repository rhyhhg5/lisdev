<%
//程序名称：BlackListInput.jsp
//程序功能：
//创建日期：2003-1-10
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT >
	var tOperator = '<%=tG.Operator%>';
</SCRIPT>
<%

  String CustomerNo= request.getParameter("CustomerNo");
  CustomerNo= CustomerNo==null?"":CustomerNo;
  String CustomerName=new String(request.getParameter("CustomerName").getBytes("ISO8859_1"),"GBK"); ;
  CustomerName=CustomerName== null ?"":CustomerName;
  String CaseNo = "";
  if(!"null".equals(request.getParameter("CaseNo"))){
     CaseNo=request.getParameter("CaseNo");}
  String LoadC = request.getParameter("LoadC");
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    var LoadC = '<%=LoadC%>';
    if(LoadC=='2'){
      divConfirm.style.display = 'none';
      divOper.style.display = 'none';
    }
    fm.all('BlackListNo').value = '<%=CustomerNo%>';
    //fm.all('BlackListType').value = '0';
    //fm.all('BlackListTypeName').value = '被保险人';
    //fm.all('BlackName').value = '<%=CustomerName%>';
   // fm.all('OriginName').value = "理赔";
    //fm.all('Origin').value = "1";
    //var tsql = "select BlacklistType,BlackName,BlackListSource,BlackListReason from LDBlacklist where 1=1  AND BlackListNo='"+fm.all('BlackListNo').value+"'";
    //brr = easyExecSql(tsql);
    //if (brr){
    	//if( brr[0][2]=='0'){
      //fm.all('OriginName').value = "核保";
      //fm.all('Origin').value = "0";
			//	}
			//if( brr[0][2]=='1'){
      //fm.all('OriginName').value = "理赔";
      //fm.all('Origin').value = "1";
			//	}
      //fm.all('BlackName').value = brr[0][1];
    //}
    //fm.all('BlackListOperator').value = '';
    fm.all('BlackListMakeDate').value = getCurrentDate();
    fm.all('BlackListOperator').value = '<%=tG.Operator%>';
    fm.all('BlackListReason').value = '';
    fm.CaseNo.value = '<%=CaseNo%>';
    fm.CaseNo1.value = '<%=CaseNo%>';
    
		fm.all('addBlack').disabled=false;
		//if(fm.all('BlackListNo').value!="" && fm.all('BlackName').value!="")
  }
  catch(ex)
  {
    alert("在BlackListInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSelBox()
{
  try
  {
//    setOption("t_sex","0=男&1=女&2=不详");
//    setOption("sex","0=男&1=女&2=不详");
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");
  }
  catch(ex)
  {
    alert("在BlackListInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    queryClient();
    initSelBox();
    initBlackListReasonGrid();
    initBlackListGrid();
    initList();
    queryBlack();
  }
  catch(re)
  {
    alert("BlackListInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initBlackListReasonGrid()
{
    var iArray = new Array();
      try
      {
          iArray[0]=new Array("序号","0px","0",1);

		      iArray[1]=new Array();
      	  iArray[1][0]="原因代码";         //列名（此列为顺序号，列名无意义，而且不显示）
          iArray[1][1]="10px";         			//列宽
          iArray[1][2]=10;          			//列最大值
          iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许

	      	iArray[2]=new Array("黑名单原因","50px","20",0);
	      	iArray[3]=new Array("原因类型","50px","20",0);

      BlackListReasonGrid = new MulLineEnter( "fm" , "BlackListReasonGrid" );
      //这些属性必须在loadMulLine前
      BlackListReasonGrid.mulLineCount = 3;
      BlackListReasonGrid.displayTitle = 1;
      BlackListReasonGrid.canChk = 1;
      BlackListReasonGrid.hiddenPlus=1;
      BlackListReasonGrid.hiddenSubtraction=1;
      BlackListReasonGrid.loadMulLine(iArray);

      //这些操作必须在loadMulLine后面
      //AffixGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }
  
function initBlackListGrid()
{
    var iArray = new Array();
      try
      {
          iArray[0]=new Array("序号","0px","0",1);

		      iArray[1]=new Array();
      	  iArray[1][0]="黑名单号";         //列名（此列为顺序号，列名无意义，而且不显示）
          iArray[1][1]="50px";         			//列宽
          iArray[1][2]=20;          			//列最大值
          iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[2]=new Array("客户类型","10px","20",3);
          iArray[3]=new Array("录入来源","10px","20",3);
	      	iArray[4]=new Array("相关案件","50px","20",0);
	      	iArray[5]=new Array("相关保单","50px","20",0);

      BlackListGrid = new MulLineEnter( "fm" , "BlackListGrid" );
      //这些属性必须在loadMulLine前
      BlackListGrid.mulLineCount = 0;
      BlackListGrid.displayTitle = 1;
      BlackListGrid.canSel =1;
      BlackListGrid.hiddenPlus=1;
      BlackListGrid.hiddenSubtraction=1;
      BlackListGrid.selBoxEventFuncName ="queryReason";
      BlackListGrid.loadMulLine(iArray);
      
      //这些操作必须在loadMulLine后面
      //AffixGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>