<html>
<%
//Name：LLMainAskInput.jsp
//Function：登记界面的初始化
//Date：2004-12-23 16:49:22
//Author：
%>
 <%@page contentType="text/html;charset=GBK" %>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import = "com.sinosoft.lis.schema.*"%>
 <%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
 <%@page import = "com.sinosoft.lis.vschema.*"%>
 <%@page import = "com.sinosoft.lis.llcase.*"%>
 <%@include file="../common/jsp/UsrCheck.jsp"%>
 <%@page import="java.util.*"%> 
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.operfee.*"%>
 <%
 	GlobalInput tG = new GlobalInput(); 
  	tG=(GlobalInput)session.getValue("GI");
  	String Operator=tG.Operator;
  	String Comcode=tG.ManageCom;
  
 	String CurrentDate= PubFun.getCurrentDate();   
        String tCurrentYear=StrTool.getVisaYear(CurrentDate);
        String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
        String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
        String AheadDays="-30";
        FDate tD=new FDate();
        Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
        FDate fdate = new FDate();
        String afterdate = fdate.getString( AfterDate );
 %>

 <head>
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
   <SCRIPT src="ClaimQuery.js"></SCRIPT>
   <%@include file="ClaimQueryInit.jsp"%>
   <script language="javascript">
   function initDate(){
   		fm.RgtDateS.value="<%=afterdate%>";
   		fm.RgtDateE.value="<%=CurrentDate%>";
   		var usercode="<%=Operator%>";
   		var comcode="<%=Comcode%>";
//   		fm.Operator.value=usercode;
//   		fm.OrganCode.value=comcode;
//   		
//   		
//   		var strSQL="select username from llclaimuser where usercode='"+usercode+"'";
//   		var arrResult = easyExecSql(strSQL);
//			if(arrResult != null)
//			{
//			         fm.optname.value= arrResult[0][0];
// 			}
   }
   </script>
 </head>
<body  onload="initDate();initForm();">
   <form action="./LLMainAskSave.jsp" method=post name=fm target="fraSubmit">

<table>
        <TR>
	         <TD class= titleImg>
	         		请录入查询条件
	         </TD>
       </TR>
      </table>

     <Div  id= "divLLLLMainAskInput2" style= "display: ''">
       <table  class= common>
				<TR>
					<TD  class= title>客户姓名</TD>
					<TD  class= input><Input class=common8 name=CustomerName onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>客户号</TD>
					<TD  class= input><Input class=common name=CustomerNo onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>身份证号</TD>
					<TD  class= input><Input class=common name=IDNo onkeydown="QueryOnKeyDown()"></TD>
				</TR>
				<TR  class= common>	
					<TD  class= title>理赔号</TD>
					<TD  class= input> <Input class=common8 name=CaseNo  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>类型</TD> 
	        <TD  class= input8><Input  CodeData="0|2^0|未结案^1|已结案" onClick="return showCodeListEx('casestate',[this,CaseStateName],[0,1]);" onkeyup="return showCodeListKeyEx('casestate',[this,CaseStateName],[0,1]);" class=codeno name="CaseState" onkeydown="QueryOnKeyDown()"><Input class= codename name=CaseStateName onkeydown="QueryOnKeyDown()"></TD>
	        <TD  class= title></TD> 
	        <TD  class= input8></TD>
				</TR>
				<TR style="display:'none'">
					<TD  class= title>团体批次号</TD>
					<TD  class= input> <Input class=common8 name=RgtNo  onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title>操作员姓名</TD>
					<TD  class= input><Input class= "code8"  name=optname onkeydown="QueryOnKeyDown();"onclick="return showCodeList('optname',[this, Operator], [0, 1],null,fm.optname.value,'username','1');">
					<TD  class= title>操作员代码</TD>
					<TD  class= input><Input class="readonly" readonly name=Operator></TD>
					<TD  class= title>管理机构</TD>
					<TD  class= input><Input class= "code8"  name=OrganCode onkeydown="QueryOnKeyDown();" onclick="return showCodeList('stati',[this], [0],null,null,null,'1');" onkeyup="getstr();return showCodeListKey('stati', [this], [0],null,null,null,'1');" >
				</TR>
				<TR>
					<TD  class= title8>受理日期从</TD>
					<TD  class= input8> <input class="coolDatePicker"  dateFormat="short"  name=RgtDateS onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8>到</TD>
					<TD  class= input8 > <input class="coolDatePicker"  dateFormat="short"  name=RgtDateE onkeydown="QueryOnKeyDown()"></TD>
					<TD  class= title8></TD><TD  class= input8></TD>
				</TR>
       </table>
     </Div>
     <br>
     <hr>
 <!--  <input style="display:''"  class=cssButton type=button value="查 询" onclick="easyQuery()"> -->
    <Div  id= "divCustomerInfo" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD text-align: left colSpan=1 align=center>
            <span id="spanCheckGrid" >
            </span>
          </TD>
        </TR>
      </table>
       
      
    </Div>
  </br>
   <Div  align=center style= "display: '' ">  
	    <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
	    <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
	    <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
	    <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
	</Div>
	<hr>
	
		<table style="display:''">
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divPolGrid);">
        </td>
        <td class= titleImg> 保单责任明细</td>
      </tr>
    </table>
  <Div  id= "divPolGrid" style= "display: ''">
    	<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanClaimPolGrid" >
					</span>
				</td>
			</tr>
		</table>
	</Div>	
	
	<table style="display:'none'">
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divDetailGrid);">
        </td>
        <td class= titleImg> 保单责任明细</td>
      </tr>
    </table>
    <Div  id= "divDetailGrid" style= "display: 'none'">
		<table  class= common>
        	<tr  class= common>
    	  		<td text-align: left colSpan=1>
					<span id="spanClaimDetailGrid" >
					</span>
				</td>
			</tr>
		</table>
   </Div>
	
	<table>
      <tr>
        <td class=common> <img  src= "../common/images/butExpand.gif" style= "cursor:hand;" onClick= "showPage(this,divClaimTotal);">
        </td>
        <td class= titleImg> 计算赔付汇总</td>
      </tr>
    </table>
    <div  id= "divClaimTotal" style= "display: ''">
    <table  class= common>
				<TR  class= common8>
					<TD  class= title8>先期给付</TD><TD  class= input8><Input class= readonly name="PreGiveAmnt" readonly></TD>
					<TD  class= title8>自付金额</TD><TD  class= input8><Input class= readonly name="SelfGiveAmnt" readonly></TD>
					<TD  class= title8>不合理费用</TD><TD  class= input8><Input class= readonly name="RefuseAmnt" readonly></TD>
				</TR>
				<TR  class= common8>
					<TD  class= title8>理算金额</TD><TD  class= input8><Input class= readonly name="StandPay" readonly></TD>
					<TD  class= title8>实赔金额</TD><TD  class= input8><Input class= readonly name="RealPay" readonly></TD>
					<TD  class= title8></TD><TD  class= input8><Input type= hidden name="RealHospDate" readonly></TD>
				</TR> 
				
				<TR  class= common8 style="display:'none'">
					<TD  class= title8>赔付金额</TD>
					<TD  class= input8><Input class= readonly name="AllGiveAmnt" readonly></TD>		
					<TD  class= title8>赔付结论</TD>
					<TD  class= input8><Input class= readonly name="AllGiveType" readonly></TD>
					<td class=title style="width:15%">个人历史赔付率                 
	  			</td>                                          
	  			<td class=input style="width:70px"><input readonly class=common name=PersonPayR style="width:80px">
	  			</td>
				</TR> 
		</table>
		<br>
  	<table class= common>
  		<tr class= common>
  			<td class=title style="width:10%">个单赔付占比
  			</td>
  			<td class=input style="width:70px"><input readonly class=common name=ContPayR >
  			</td>                                          
  			<td class=title style="width:15%">个人占团单赔付比例                  
  			</td>                                          
  			<td class=input style="width:70px"><input readonly class=common name=GrpContPayR >
  			</td>                                                                                              
  			                                                                                                   
  			<td class=title  >保单赔付率                                                                       
  			</td>                                                                                              
  			<td class=input style="width:70px"><input readonly class=common name=AGrpContPayR>
  			</td>
  		</tr>
  	</table>
</div>
	    <hr>
		<input type=button class=cssButton style="align:right" value="案件理算查询" onClick="ShowInfoPage();">
		<input type=button class=cssButton value="保单查询" OnClick="ContInfoPage();">
<!--<input name="AskIn" style="display:''"  class=cssButton type=button value="结案确认" onclick="DealOk()">-->
     <!--隐藏域-->
     <Input type="hidden" class= common name="fmtransact" >
     <Input type="hidden" class= common name="ComCode"  value="<%=Comcode%>">
  	</form>
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	</body>
</html>
