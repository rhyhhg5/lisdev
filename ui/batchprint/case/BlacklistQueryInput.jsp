<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：BlacklistQueryInput.jsp
//程序功能：
//创建日期：2003-01-10
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./BlacklistQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./BlacklistQueryInit.jsp"%>
<title>黑名单信息 </title>
</head>
<body  onload="initForm();" >
  <form action="./BlacklistQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  <table  class= common>
  <TR  class= common>
    <TD  class= title>
      黑名单客户号码
    </TD>
    <TD  class= input>
      <Input class= common name=BlacklistNo >
    </TD>
    <TD  class= title>
      黑名单客户姓名
    </TD>
    <TD  class= input>
      <Input class= common name=BlackName >
    </TD>  
  </TR>
</table>
    </table>
          <INPUT class=cssbutton VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBlacklistGrid);">
    		</td>
    		<td class= titleImg>
    			 黑名单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBlacklistGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBlacklistGrid" >
  					</span>
  			  	</td>
  			</tr>
    	</table >
    	<table align=center>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class=cssButton>
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class=cssButton>
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class=cssButton>
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class=cssButton>
    </table>
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
