<%
//Name：CaseCancel.jsp
//Function：为"立案"模块的基本功能-撤销
//Date：2005-9-21
//Author  ：Xx
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.llcase.*"%>
  <%@page contentType="text/html;charset=GBK" %>
<%
  RegisterBL tRegisterBL   = new RegisterBL();
  LLCaseSchema tLLCaseSchema = new LLCaseSchema();
  VData tVData = new VData();
  //输出参数
  CErrors tError = null;
  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  String strOperate = request.getParameter("fmtransact");
  System.out.println("strOperate::"+strOperate);
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getValue("GI");
	String LoadFlag = request.getParameter("LoadFlag");
  String CaseNo = request.getParameter("CaseNo");
  String RgtNo = request.getParameter("RgtNo");
  String CancleReason = request.getParameter("CancleReason");
  String CancleRemark = request.getParameter("CancleRemark");
  tLLCaseSchema.setCaseNo(CaseNo);
  tLLCaseSchema.setRgtNo(RgtNo);
  tLLCaseSchema.setCancleReason(CancleReason);
  tLLCaseSchema.setCancleRemark(CancleRemark);
  
  try
  {
    tVData.addElement(tLLCaseSchema);
    tVData.addElement(tG);
    if(tRegisterBL.submitData(tVData,strOperate))
    {
      Content = "案件撤销成功";
      FlagStr = "Succ";
    }
    else
    {
      FlagStr = "Fail";
    }
  }
  catch(Exception ex)
  {
    Content = "案件撤销失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  System.out.println("返回的标志是"+FlagStr);
  if (FlagStr=="Fail")
  {
    tError = tRegisterBL.mErrors;
    if (tError.needDealError())
    {
      Content = " 撤销失败,原因是"+tError.getFirstError();
      FlagStr = "Fail";
      tVData.clear();
    }
  }
  else
  {
    Content = "撤销成功！";
    FlagStr = "Succ";
  }
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>