<%
//程序名称：ClaimRecQueryInit.jsp
//程序功能：既往理赔信息查询
//创建日期：2006-11-13 11:10:36
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
%>

<script language="JavaScript">


		// 下拉框的初始化
		function initSelBox(){
			try{
			}
			catch(ex){
				alert("在ClaimRecQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
			}
		}

		function initForm(){
			try{
			//	initInpBox();
				initSelBox();
				initCaseGrid();
				ClaimQuery();
				initClaimPayGrid();
				initQuery();
				CalInfo();
				
			}
			catch(re){
				alert("在ClaimRecQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
			}
		}

		// 保单信息列表的初始化
		function initCaseGrid(){
			var iArray = new Array();
			try{
				iArray[0]=new Array("序号","30px","10",0);
				iArray[1]=new Array("理赔号","100px","200",0);
				iArray[2]=new Array("受理日期","50px","20",0);
				iArray[3]=new Array("结案日期","50px","200",0);
				iArray[4]=new Array("帐单金额","50px","10",0);
				iArray[5]=new Array("赔付金额","50px","10",0);
				iArray[6]=new Array("出险事件数","50px","100",0);
				iArray[7]=new Array("赔付险种数","50px","10",0);
				iArray[8]=new Array("理赔人","50px","100",0);
				iArray[9]=new Array("案件状态","50px","100",0);

				CaseGrid = new MulLineEnter( "fm" , "CaseGrid" );
				CaseGrid.mulLineCount = 3;
				CaseGrid.displayTitle = 1;
				CaseGrid.locked = 1;
				CaseGrid.canSel = 1;
				CaseGrid.hiddenPlus = 1;
				CaseGrid.hiddenSubtraction = 1;
				CaseGrid.loadMulLine(iArray);
				CaseGrid.selBoxEventFuncName = "ShowCaseInfo";
			}
			catch(ex){
				alert(ex);
			}
		}

		// 保单信息列表的初始化
		function initClaimPayGrid(){
			var iArray = new Array();
			try{
				iArray[0]=new Array("序号","30px","10",0);
				iArray[1]=new Array("保单号","100px","200",0);
				iArray[2]=new Array("险种名称","160px","20",0);
				iArray[3]=new Array("保额/限额","80px","200",0);
				iArray[4]=new Array("帐单金额","80px","10",0);
				iArray[5]=new Array("赔付金额","80px","10",0);
				iArray[6]=new Array("险种代码","80px","10",3);
				iArray[7]=new Array("险种号","80px","10",3);
				ClaimPayGrid = new MulLineEnter( "fm" , "ClaimPayGrid" );
				//这些属性必须在loadMulLine前
				ClaimPayGrid.mulLineCount = 2;
				ClaimPayGrid.displayTitle = 1;
				ClaimPayGrid.locked = 1;
				ClaimPayGrid.canSel = 1;
				ClaimPayGrid.hiddenPlus = 1;
				ClaimPayGrid.hiddenSubtraction = 1;
				ClaimPayGrid.loadMulLine(iArray);
				ClaimPayGrid.selBoxEventFuncName = "ShowPayInfo";
			}
			catch(ex){
				alert(ex);
			}
		}

		function initQuery()
		{
		}

	</script>