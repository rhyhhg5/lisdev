<%
//程序名称：BlacklistQuery.js
//程序功能：
//创建日期：2003-01-10
//创建人  ：Xx
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{

  try
  {
  	fm.all('BlacklistNo').value='';
  	fm.all('BlackName').value='';
  }
  catch(ex)
  {
    alert("在BlacklistQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("在BlacklistQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
	  initBlacklistGrid();
  }
  catch(re)
  {
    alert("BlacklistQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var BlacklistGrid;          //定义为全局变量，提供给displayMultiline使用
function initBlacklistGrid()
  {
    var iArray = new Array();
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=0;

        iArray[1]=new Array();
		    iArray[1][0]="客户号码";
		    iArray[1][1]="80px";
		    iArray[1][2]=100;
		    iArray[1][3]=0;

		    iArray[2]=new Array();
		    iArray[2][0]="客户类型";
		    iArray[2][1]="60px";
		    iArray[2][2]=100;
		    iArray[2][3]=3;

		    iArray[3]=new Array();
		    iArray[3][0]="名称";
		    iArray[3][1]="100px";
		    iArray[3][2]=100;
		    iArray[3][3]=0;

		    iArray[4]=new Array();
		    iArray[4][0]="操作员";
		    iArray[4][1]="60px";
		    iArray[4][2]=100;
		    iArray[4][3]=0;

		    iArray[5]=new Array();
		    iArray[5][0]="操作日期";
		    iArray[5][1]="80px";
		    iArray[5][2]=100;
		    iArray[5][3]=0;

		    iArray[6]=new Array();
		    iArray[6][0]="原因描述";
		    iArray[6][1]="300px";
		    iArray[6][2]=100;
		    iArray[6][3]=0;

        BlacklistGrid = new MulLineEnter( "fm" , "BlacklistGrid" );

        //这些属性必须在loadMulLine前
        BlacklistGrid.mulLineCount = 10;
        BlacklistGrid.displayTitle = 1;
        BlacklistGrid.canSel=1;
      	BlacklistGrid.hiddenPlus = 1;
      	BlacklistGrid.hiddenSubtraction = 1;
        BlacklistGrid.loadMulLine(iArray);
    		BlacklistGrid. selBoxEventFuncName = "DealBlacklist";

      }
      catch(ex)
      {
        alert("初始化BlacklistGrid时出错："+ ex);
      }
    }


</script>