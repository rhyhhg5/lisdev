<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-1-7
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
//            陈海强    2004-12-31
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="BlackListInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="BlackListInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./BlackListSave.jsp" method=post name=fm target="fraSubmit">
    <!--%@include file="../common/jsp/OperateButton.jsp"%-->
    <!--%@include file="../common/jsp/InputButton.jsp"%-->
    <table>
      <tr>
      <td>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBlackList1);">
      </td>
      <td class= titleImg>
        特殊客户信息
      </td>
    	</tr>
    </table>
    <Div  id= "divBlackList1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>客户姓名</TD>
          <TD  class= input>
            <Input class=common  name=BlackName onkeydown="QueryOnKeyDown()" readonly >
          </TD>
          <TD  class= title>客户号码</TD>
          <TD  class= input>
            <Input class=common  name=BlackListNo onkeydown="QueryOnKeyDown()" readonly >
          </TD>
          <TD  class= title>身份证号</TD>
          <TD  class= input>
            <Input class=common name=IDNo readonly >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>联系地址</TD>
          <TD  class= input>
            <Input class=readonly  readonly name=Address>
          </TD>
          <TD  class= title>联系电话</TD>
          <TD  class= input>
            <Input class=readonly  readonly name=Phone>
          </TD>
          <TD  class= title>邮政编码</TD>
          <TD  class= input>
            <Input class=readonly readonly name=Zipcode>
          </TD>
        </TR>
        <TR class=common>
          <TD  class= title>
            黑名单记录清单
          </TD>
        </tr>            
        <tr class=common align= 'top'>
    	  	<td text-align: left colSpan=6 align= 'top'>
						<span id="spanBlackListGrid" align= 'top'>
						</span>
					</td>
        </TR>
        <TR class=common>
          <TD  class= title>客户类型</TD>
          <!--<TD  class= input><input class="codeno" name="BlackListType" CodeData="0|4^0|被保险人^1|投保人^2|受益人^3|团体客户^4|其他" onclick="return showCodeListEx('BlackListType',[this,BlackListTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('BlackListType',[this,BlackListTypeName],[0,1]);"><Input class=codename   name=BlackListTypeName >
          -->
          <TD  class= input><input class="codeno" name="BlackListType" value="0"><Input class=codename   name=BlackListTypeName value="被保险人">
          </TD>
          <TD  class= title></TD><TD  class= input></TD>
          <TD  class= title></TD><TD  class= input></TD>
        </TR>
        <TR class=common>
          <TD  class= title>录入来源</TD>
          <!--<TD  class= input><input class="codeno" name="Origin"  CodeData="0|2^0|核保^1|理赔" onclick="return showCodeListEx('Origin',[this,OriginName],[0,1]);" onkeyup="return showCodeListKeyEx('Origin',[this,OriginName],[0,1]);"><Input class=codename name=OriginName  >
          -->
          <TD  class= input><input class="codeno" name="Origin"  value="1" ><Input class=codename name=OriginName value="理赔" >
          </TD>
          <TD  class= title>
            相关保单
          </TD>
          <TD  class= input>
            <Input class=code name=ContNo onclick="return showCodeList('llinsucont',[this],[0],null,fm.BlackListNo.value,'insuredno',1);" onkeyup="return showCodeListKey('llinsucont',[this],[0],null,fm.BlackListNo.value,'insuredno',1);">
          </TD>
          <TD  class= title>
            相关案件
          </TD>
          <TD  class= input>
            <Input class=common name=CaseNo readonly >
          </TD>
        </TR>
        <TR class=common>
          <TD  class= title>
            录入原因
          </TD>
        </tr>            
        <tr class=common align= 'top'>
    	  	<td text-align: left colSpan=6 align= 'top'>
						<span id="spanBlackListReasonGrid" align= 'top'>
						</span>
					</td>
        </TR>
      </table>
      <table class= common style=display:'none'>
        <TR class= common>
			<TD class= title>黑名单原因描述</TD>
		</TR>					
		<TR  class= common>
		  	<TD  class= input>
	    		<textarea name=BlackListReason cols="85%" rows="3"  class="common"></textarea>
		    </TD>
  		</TR>
      </table>
      <div id=divOper>
      <table class=common>
  		<TR class= common>
          <TD  class= title>
            经办人
          </TD>
          <TD  class= input>
            <Input class="readonly" readonly name=BlackListOperator >
          </TD>
          <TD  class= title>
            录入日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=BlackListMakeDate >
          </TD>
          <TD  class= title></TD><TD  class= input></TD>
  		</TR>
      </table>
    </div>
      <br>
      <div id=divConfirm>
		<Input class=cssbutton type=button value="增加黑名单"  onclick="addClick()" name="addBlack">
		<Input class=cssbutton type=hidden value="黑名单查询"  onclick="EspQuery()" disabled=true >
		<Input class=cssbutton type=button value="修改黑名单"  onclick="updateClick()" name="uptBlack">
		<Input class=cssbutton type=button value="删除黑名单"  onclick="deleteClick()" name="uptBlack">
	</div>
    </Div>
    <input type=hidden name=hideOperate value=''>
    <input type=hidden name=strsqlinit1 value=''>
    <input type=hidden name="CaseNo1" value="">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
