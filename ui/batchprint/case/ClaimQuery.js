var showInfo;
var mDebug="1";
var tSaveFlag = "0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var tSaveType="";

function QueryOnKeyDown()
{
	var keycode = event.keyCode;
	//回车的ascii码是13
	if(keycode=="13")
	{
		easyQuery();
	}
}

function easyQuery()
{
	var comCode = fm.ComCode.value;
	initClaimPolGrid();  
	fm.PreGiveAmnt.value  ="";
	fm.SelfGiveAmnt.value ="";
	fm.RefuseAmnt.value   ="";
	fm.StandPay.value     ="";
	fm.RealPay.value      ="";
  fm.ContPayR.value ="";    
  fm.GrpContPayR.value =""; 
  fm.PersonPayR.value ="";  
  fm.AGrpContPayR.value ="";
  
	
	var sqlpart="";
	if (fm.CaseState.value=="0")
		sqlpart = " and a.endcasedate is null";
	if (fm.CaseState.value=="1")
		sqlpart = " and a.endcasedate is not null";
	//咨询类3
	var strSQL ="";
		strSQL ="select distinct case when a.rgtno=a.caseno then '' else a.rgtno end, "
		+ " a.CaseNo,a.CustomerName,a.endcasedate,a.CustomerNo,a.rgtdate,a.handler,c.codename ,"
		+ " (select c.codename from ldcode c, llclaim h where h.CaseNo = a.CaseNo and c.codetype = 'llclaimdecision' and c.code = h.givetype), e.idno, "
		+ "(select k.name from ldcom k where a.Mngcom = k.comcode ),(select h.username from lduser h where h.usercode = a.handler),a.handler "
		+ " from LLCase a, LLregister b, ldcode c ,llclaimpolicy d, ldperson e "
		+ " where 1=1 and a.rgtno=b.rgtno and d.caseno=a.caseno and c.code=a.rgtstate and c.codetype='llrgtstate' "
		+ " and a.customerno = e.customerno "
		+ getWherePart("a.rgtno","RgtNo")
		+ getWherePart("a.CaseNo","CaseNo") 
		+ getWherePart("a.CustomerNo","CustomerNo")
		+ "and a.CustomerName like '%%" + fm.CustomerName.value +"%%' "
//		+ getWherePart("a.MngCom","OrganCode")
		+ getWherePart("e.IDNo","IDNo")
//		+ getWherePart("a.handler","Operator") 
		+ getWherePart("a.rgtdate","RgtDateE","<=")
		+ getWherePart("a.rgtdate","RgtDateS",">=")
  	+" and a.mngcom like '"+comCode+"%%'" 
		+ sqlpart+" order by a.rgtdate DESC ";
		turnPage.pageLineNum = 5;
		turnPage.queryModal(strSQL, CheckGrid);
}

function DealClaim()
{ 
	 fm.ContPayR.value ="";
	 fm.GrpContPayR.value ="";
	 fm.PersonPayR.value ="";
	 fm.AGrpContPayR.value ="";
	 
	 var selno = CheckGrid.getSelNo();
	 var CaseNo = CheckGrid.getRowColData(selno-1,2);
	
 	 var strSQL = " select distinct c.GetDutyName,b.riskname,a.ContNo,''," 
		          + " a.StandPay,a.RealPay,a.polno,a.clmno," 
		          + " a.GiveTypeDesc,a.GiveType,a.riskcode"
		          + " from LLClaimdetail a, lmrisk b, LMDutyGetClm c " 
		          + " where a.caseno='"+ CaseNo +"'" 
		          + " and a.riskcode = b.riskcode " 
		          + " and a.GetDutycode = c.GetDutycode "
		          + " order by a.polno "
						  ;     
	turnPage1.queryModal(strSQL,ClaimPolGrid);
	
	strSQL =" select sum(PreGiveAmnt),sum(SelfGiveAmnt),sum(RefuseAmnt),sum(claimmoney),sum(RealPay)  "
				 +" from llclaimdetail where clmno in (select clmno from LLClaimPolicy where  caseno='"+ CaseNo +"')";
	var brr = easyExecSql(strSQL );
	if ( brr )
	{
		brr[0][0]==null||brr[0][0]=='null'?'0':fm.PreGiveAmnt.value  =brr[0][0];
		brr[0][1]==null||brr[0][1]=='null'?'0':fm.SelfGiveAmnt.value =brr[0][1];
		brr[0][2]==null||brr[0][2]=='null'?'0':fm.RefuseAmnt.value   =brr[0][2];
		brr[0][3]==null||brr[0][3]=='null'?'0':fm.StandPay.value     =brr[0][3];
		brr[0][4]==null||brr[0][4]=='null'?'0':fm.RealPay.value      =brr[0][4];
	}	
	else{return;}   
		     
}   
 
//赔付率计算
function CalRatio()
{
		//个人保单赔付率
		var contno = ClaimPolGrid.getRowColData(ClaimPolGrid.getSelNo()-1,3);
		var CustomerNo = CheckGrid.getRowColData(CheckGrid.getSelNo()-1,5);
		var sql = " select sum(RealPay) from llclaimpolicy where insuredno = '"+CustomerNo+"'";
		var sqlPol = " select nvl(sum(Prem),0) from lcpol where insuredno = '"+CustomerNo+"' ";
		var Pay = easyExecSql(sql);  
		var Prem = easyExecSql(sqlPol); 
		
		if(Prem==0) {
		
		fm.ContPayR.value = '0'+" %";
		} else {
			var a = Pay*100/Prem;
			fm.ContPayR.value = a.toString().substring(0,6)+" %";
		}
		
		
		//对应团单赔付率 
		var GrpContNo = easyExecSql(" select distinct grpcontno from lcpol where contno = '"+contno+"' union select distinct grpcontno from lbpol where contno = '"+contno+"'" );
		if(GrpContNo == "00000000000000000000") {
		} else {
			Pay = easyExecSql(" select sum(realpay) from llclaimpolicy where grpcontno = '"+GrpContNo+"' and insuredno = '"+CustomerNo+"'"); 
		    var AllPay = easyExecSql(" select sum(a.realpay) from llclaim a, (select distinct caseno, grpcontno from llclaimpolicy) as b where  a.caseno = b.caseno and b.grpcontno = '"+GrpContNo+"' ");
//		    Prem = easyExecSql(" select sum(prem) from lcpol where grpcontno = '"+GrpContNo+"' ");
			if(AllPay==0) {
			    
				fm.GrpContPayR.value = "0" +" %";
			} else {
			    var a = Pay*100/AllPay; 
		        fm.GrpContPayR.value = a.toString().substring(0,6)+" %";
		    }
	       
		    //保单赔付率
		    Pay = easyExecSql(" select sum(realpay) from llclaimpolicy where grpcontno = '"+GrpContNo+"'"); 
			Prem = easyExecSql(" select nvl(sum(prem),0) from lcpol where grpcontno = '"+GrpContNo+"' ");     
			if(Prem==0) {
				fm.AGrpContPayR.value = "0"+" %";
			} else {
				var a = Pay*100/Prem;                 
				fm.AGrpContPayR.value = a.toString().substring(0,6)+" %"; 
		   }
			  
		}
		
		
		//个人历史赔付率
//		Pay = easyExecSql("select sum(realpay) from llclaimpolicy where insuredno = '"+CustomerNo+"'"); 
//		Prem = easyExecSql(" select sum(prem) from lcpol where insuredno = '"+CustomerNo+"' ");
//		a = Pay*100/Prem; //alert(Pay);alert(Prem);
//		fm.PersonPayR.value = a.toString().substring(0,6)+" %";
		
		
		
}
    
function DealClaim_origin()
{   
	var LoadC="2";
	var caseno = '';
	var selno = CheckGrid.getSelNo()-1;
	if (selno <0)
	{
		//alert("请选择要理算的个人受理");
		//return ;
	}
else
	{
		caseno = CheckGrid.getRowColData(selno,2);
	}
	//window.open("ClientQueryMain.html","客户查询",'width=800,height=600,top=0,left=0,toolbar=0,location=0,directories=0,menubar=0,scrollbars=1,resizable=1,status=0');
	var varSrc="&CaseNo=" + caseno ;
	varSrc += "&LoadC="+LoadC;
	var newWindow = OpenWindowNew("./FrameMainCaseClaim.jsp?Interface=LLClaimInput2.jsp"+
	varSrc,"理算","left");
}

//原始信息查询页面
function ShowInfoPage()
{
	var selno = CheckGrid.getSelNo();//alert(CheckGrid.getSelNo());
	if(selno == "" || selno == null)
	{alert("请先选择一条信息");return;}
	var CaseNo = CheckGrid.getRowColData(selno-1,2);
	OpenWindowNew("./LLClaimInput.jsp?CaseNo="+CaseNo+"&LoadC=2","RgtSurveyInput","left");
}

//保单查询
function ContInfoPage()
{
		var selno = CheckGrid.getSelNo();//alert(CheckGrid.getSelNo());
		if(selno == "" || selno == null)
		{alert("请先选择一条信息");return;}
		var CaseNo = CheckGrid.getRowColData(selno-1,2);
		var ContNo = easyExecSql("select distinct grpcontno from llclaimpolicy where caseno = '"+CaseNo+"'");
		if(ContNo=='00000000000000000000')
		{
			ContNo = easyExecSql("select distinct contno from llclaimpolicy where caseno = '"+CaseNo+"'");
			window.open("../sys/PolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
			}else
				{
					window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo="+ContNo+"&ContType=1" );  
	      }
}