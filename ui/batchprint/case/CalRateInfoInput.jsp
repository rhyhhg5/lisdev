<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
  <%
  //程序名称：LLImportCaseInput.jsp
  //程序功能：理赔批量导入
  //创建日期：2006-01-27 17:39:01
  //创建人  ：Xx
  //更新记录：  更新人    更新日期     更新原因/内容
  %>
  <%@page contentType="text/html;charset=GBK" %>

  <head >
    <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
    <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
    <SCRIPT src="CalRateInfoInput.js"></SCRIPT>
    <script>
    </script>
    <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
    <%@include file="ImportRateInfoInit.jsp"%>
  </head>
  <body  onload="initForm();Scroll();" >
    <form method=post name=fm target="fraSubmit" >
      <div id="divDiskInput" style="display:''">
        <table>
          <tr>
            <td>
              <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInuredList);">
            </td>
            <td class= titleImg>
              团体批次信息
            </td>
          </tr>
        </table>
        <table class = common >
          <TR class = common>
            <TD class=title>团体批次号</TD><TD><input class=readonly readonly name=RgtNo></TD>
            <TD class=title>待计算人数</TD><TD><input class=readonly readonly name=AppPeople></TD>
          </TR>
        </table>
        <br>
        <div id="divInsuredInfo" style="display: ''">
          <table class=common>
            <tr class=common >
              <TD  class= title>计算成功数</TD>
              <TD  class= input><Input class=readonly readonly name=SuccInsured ></TD>
              <TD  class= title>计算失败数</TD>
              <TD  class= input><Input class=readonly readonly name=FailInsured ></TD>
            </TR>
          </table>
        </div>

        <input type=hidden id="" name="fmtransact">
        <input type=hidden id="" name="fmAction">
        <input type=hidden name="BatchCaseNo">
        <input type=hidden id="GrpContNo" name="GrpContNo">
        <input type=hidden id="LoadFlag" name="LoadFlag" value="1">
        <input name ="RiskCode" type="hidden">
        <input name ="realpeoples" type="hidden">
        <input name ="LoadC" type="hidden">
      </form>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    </body>
  </html>
