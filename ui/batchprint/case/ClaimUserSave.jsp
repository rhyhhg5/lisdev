<%
//Name    ：ClaimUserSave.jsp
//Function：对理赔岗位人员权限的分配执行后台调用程序
//Author   :LiuYansong
//Date：2004-10-26@民生
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.sys.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.llcase.*"%>
<%@page import="com.sinosoft.lis.vbl.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%

  LLClaimUserSchema mLLClaimUserSchema = new LLClaimUserSchema();
  LDSpotUWRateSchema mLDSpotUWRateSchema = new LDSpotUWRateSchema();
  LDCode1Set mLDCode1Set = new LDCode1Set();

  CErrors tError = null;
  String transact = request.getParameter("fmtransact");
  System.out.println("ClaimUserSave.jsp本次执行的操作是"+transact);

  String tRela  = "";
  String FlagStr = "";
  String Content = "";
  
  
    String Path = application.getRealPath("config//Conversion.config");
    mLLClaimUserSchema.setRemark(StrTool.Conversion(request.getParameter("Remark"),Path));
    mLLClaimUserSchema.setUserCode(request.getParameter("UserCode"));
    String mUserName = StrTool.unicodeToGBK(request.getParameter("UserName")); //名称
    mLLClaimUserSchema.setUserName(mUserName);
    mLLClaimUserSchema.setComCode(request.getParameter("Station"));
    mLLClaimUserSchema.setClaimPopedom(request.getParameter("ClaimPopedom"));
    mLLClaimUserSchema.setUpUserCode(request.getParameter("UpUserCode"));
    mLLClaimUserSchema.setRgtFlag(request.getParameter("RgtFlag"));
    mLLClaimUserSchema.setSurveyFlag(request.getParameter("ServeyFlag"));
    mLLClaimUserSchema.setClaimDeal(request.getParameter("ClaimDeal"));
    mLLClaimUserSchema.setCheckFlag(request.getParameter("CheckFlag"));
    mLLClaimUserSchema.setHandleFlag(request.getParameter("HandleFlag"));
    mLLClaimUserSchema.setStateFlag(request.getParameter("StateFlag"));
    mLLClaimUserSchema.setDispatchRate(request.getParameter("DispatchRate"));
    mLLClaimUserSchema.setInputFlag(request.getParameter("InputFlag"));
    mLLClaimUserSchema.setPrepaidFlag(request.getParameter("PrepaidFlag"));
    mLLClaimUserSchema.setPrepaidUpUserCode(request.getParameter("PrepaidUpUserCode"));
    mLLClaimUserSchema.setPrepaidLimit(request.getParameter("PrepaidLimit"));
    
    //Add By Houyd 2014-02-21
    mLLClaimUserSchema.setSpecialNeedFlag(request.getParameter("SpecialNeedFlag"));
    mLLClaimUserSchema.setSpecialNeedUpUserCode(request.getParameter("SpecialNeedUpUserCode"));
    mLLClaimUserSchema.setSpecialNeedLimit(request.getParameter("SpecialNeedLimit"));
    
    
    mLDSpotUWRateSchema.setUserCode(request.getParameter("UserCode"));
    mLDSpotUWRateSchema.setUserName(mUserName);
    mLDSpotUWRateSchema.setUWGrade(request.getParameter("ClaimPopedom"));
    mLDSpotUWRateSchema.setUWType("30000");
    mLDSpotUWRateSchema.setUWRate(request.getParameter("ClaimSpotRate"));
    
    //案件配置信息
   // String tRNum[] = request.getParameterValues("InpCaseDisGrid");
    //System.out.println("length:"+tRNum.length);
	String mngcom[] = request.getParameterValues("CaseDisGrid1");
	String mngname[] = request.getParameterValues("CaseDisGrid2");
	String risktype[] = request.getParameterValues("CaseDisGrid3");
	//System.out.println("<--submit mulline-->");
	
	if (mngcom != null) 
	{
		//System.out.println("<-tRNum-->"+ tRNum.length);
  	for (int i = 0; i < mngcom.length; i++)	
  	{           
  	            String t = Integer.toString(i);
	  			LDCode1Schema tLDCode1Schema = new LDCode1Schema();
		        tLDCode1Schema.setCodeType("LLCaseAssign");
			    tLDCode1Schema.setCode1(t);
			    tLDCode1Schema.setCodeName(StrTool.unicodeToGBK(mngname[i]));
			    tLDCode1Schema.setComCode(mngcom[i]);
			    tLDCode1Schema.setCode(request.getParameter("UserCode"));
			    tLDCode1Schema.setOtherSign(risktype[i]);
			    System.out.println("kkkk "+tLDCode1Schema.getCode());
				mLDCode1Set.add(tLDCode1Schema);
			
	}
	}

    GlobalInput tG = new GlobalInput();
    tG=(GlobalInput)session.getValue("GI");

    VData tVData = new VData();
    ClaimUserOperateUI mClaimUserOperateUI = new ClaimUserOperateUI();
    try
    {
      tVData.addElement(mLLClaimUserSchema);
      tVData.addElement(mLDSpotUWRateSchema);
      tVData.addElement(tG);
      tVData.addElement(mLDCode1Set);
      mClaimUserOperateUI.submitData(tVData,transact);
    }
    catch(Exception ex)
    {
      Content = transact+"失败，原因是:" + ex.toString();
      FlagStr = "Fail";
    }

    if (FlagStr=="")
    {
      tError = mClaimUserOperateUI.mErrors;
      if (!tError.needDealError())
      {
        Content = " 操作成功！";
        FlagStr = "Succ";
      }
      else
      {
        Content = " 操作失败，原因是:" + tError.getFirstError();
        FlagStr = "Fail";
      }
    }
  
  //处理转意字符
%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>