<table>
	<tr>
		<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divClientInfoTitle);">
		</td>
		<td class= titleImg>
			案件基本信息
		</td>
	</tr>
</table>
<div id= "divClientInfoTitle" style= "display: ''" >
	<table  class= common>
		<TR class= common8>
			<TD class=title8 id='titleGrp' style="display:'none'">团体批次号</TD>
			<TD class=input8 id='inputGrp' style="display:'none'"><Input class=readonly readonly name="RgtNo"></TD>
			<TD class=title8 id='titleCaseNo'>理赔号</TD>
			<TD class=input8><Input class=common8 name="CaseNo" onkeydown="CaseOnKeyDown(1)"></TD>
			<TD class=title8>案件状态</TD>
			<TD class=input8><Input class=readonly readonly name="RgtState"></TD>
		</TR>
		<TR class= common8>
			<TD class= title8>处理人</TD>
			<TD class= input8><Input class=readonly readonly name="Handler"></TD>
			<TD class= title8>处理日期</TD>
			<TD class= input8><Input class=readonly readonly name="ModifyDate"></TD>
			<TD class= title8 id='titleAppNum' style="display:'none'">申请人数</TD>
			<TD class= input8 id='inputAppNum' "  style="display:'none'"><Input class=readonly readonly name="AppNum"></TD>
		</TR>
		<TR class= common8 id=idAppealNO style='display:none'>
			<TD class=title8>原理赔号</TD>
			<TD class= input8><Input class=common8 name="Orig_CaseNo" id='inputOCaseNo'></TD>
		</TR>
	</table>
</div>
<script language='javascript'>

	function initTitle(flag){
		if ( fm.CaseNo.value!=""){
			var sql = "select a.RgtNo,a.CustomerNo,a.CustomerName,(case a.CustomerSex when '0' then '男' when '1' then '女' when '2' then '不详' end),a.AccdentDesc,"
			+"a.handler,b.codename,c.apppeoples from llcase a,ldcode b,llregister c where a.CaseNo='" 
			+ fm.CaseNo.value +"' and c.rgtno=a.rgtno and b.codetype='llrgtstate' and b.code=a.rgtstate";
			var arr = easyExecSql(sql);
			if (arr){
				fm.RgtNo.value = arr[0][0];
				fm.CustomerNo.value = arr[0][1];
				fm.CustomerName.value = arr[0][2];
				fm.ContRemark.value = arr[0][4];
				fm.Handler.value = arr[0][5];
				fm.RgtState.value = arr[0][6];
				fm.AppNum.value = arr[0][7];
		    	fm.Sex.value = arr[0][3];
			}
			else{
				fm.RgtNo.value  = "";
				fm.CustomerNo.value = "";
				fm.CustomerName.value = "";
		        fm.Sex.value = "";
				fm.ContRemark.value = "";
				fm.RgtState.value  = "";
				fm.Handler.value  = "";
				fm.AppNum.value = "";
				return;
			}
			if (fm.RgtNo.value ==fm.CaseNo.value ){  //个人
				titleGrp.style.display ="none";
				inputGrp.style.display ="none";
				titleAppNum.style.display="none";
				inputAppNum.style.display="none";
			}
			else{
				titleGrp.style.display ="";
				inputGrp.style.display ="";
				titleAppNum.style.display="";
				inputAppNum.style.display="";
			}
			<%
				GlobalInput tG = new GlobalInput();
				tG=(GlobalInput)session.getValue("GI");
			%>
		}

		if (flag && flag=='grp' &&  titleGrp.style.display =="none"){
			titleGrp.style.display ="";
			inputGrp.style.display ="";
		}
		if (fm.CaseNo.value.substring(0,1)=="S"||fm.CaseNo.value.substring(0,1)=="R"){
			idAppealNO.style.display="";
			var sql3="select CaseNo,appealtype,AppeanRCode,appealreason,appealrdesc,case appealtype when '0' then '申诉类' when '1' then '纠错类' end  from llappeal where appealno='"+fm.CaseNo.value+"'";
			var brr = easyExecSql(sql3 );
			fm.Orig_CaseNo.value=brr[0][0];
			if(fm.AppealType!=null&&fm.AppeanRCode!=null&&fm.AppealReason!=null&&fm.AppealRDesc!=null&&fm.AppealTypeName!=null)//只有在申诉纠错页面时符合
			{
			fm.AppealType.value=brr[0][1];
			fm.AppeanRCode.value=brr[0][2];
			fm.AppealReason.value=brr[0][3];
			fm.AppealRDesc.value=brr[0][4];
			fm.AppealTypeName.value=brr[0][5];
			}
		}
		queryBlackList();
	}

	function CaseOnKeyDown(flag){
		var keycode = event.keyCode;
		//回车的ascii码是13
		if ( flag=='1'){
			if(keycode=="13"){
				CaseChange();
			}
		}
		else{
			RgtChange();
		}
	}

function queryBlackList(){
  var	strSQLx = "select blacklistno,blackname from LDBlacklist where blacklistno ='"
  +fm.CustomerNo.value+"'";
  var	crrResult1 = easyExecSql(strSQLx);
  if(crrResult1){
    alert("请务必查询相关客户信息！");
  }
}

</script>
