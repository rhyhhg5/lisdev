<%
//程序名称：ClaimUnderwriteInput.js
//程序功能：审批审定页面函数
//创建日期：2002-06-19 11:10:36
//创建人  ：Wujs
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
     //添加页面控件的初始化。
     String LoadC = "";
		if(request.getParameter("LoadC")!=null){
			LoadC = request.getParameter("LoadC");
		}
		String LoadD = "";
		if(request.getParameter("LoadD")!=null){
			LoadD = request.getParameter("LoadD");
		}
		
		String tShowCaseRemarkFlag = "";//
		tShowCaseRemarkFlag = (String)session.getAttribute("ShowCaseRemarkFlag");
		System.out.println("tShowCaseRemarkFlag:"+tShowCaseRemarkFlag);
%>

<script language="JavaScript">

//输入框的初始化（单记录部分）
function initInpBox(){
  try{
    var tCaseNo= '<%=request.getParameter("CaseNo")%>';
    fm.all('CaseNo').value = tCaseNo=='null'?'':tCaseNo;
    fm.all('Handler').value = '';
    fm.all('ModifyDate').value = '';
    fm.all('DecisionSD').value = '';
    fm.all('RemarkSP').value = '';
    fm.all('RemarkSD').value = '';
  }
  catch(ex){
    alert("ClaimUnderwriteInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

// 下拉框的初始化
function initSelBox(){
  try{

  }
  catch(ex){
    alert("ClaimUnderwriteInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm(){
  try{
  	    initInpBox();
		initTitle();
		initClaimPolGrid();
		initClaimPayGrid();
		fm.ShowCaseRemarkFlag.value = "<%=tShowCaseRemarkFlag%>";
		var LoadFlag = "<%=LoadC%>";
     if (LoadFlag =='2'){
		    divconfirm.style.display='none';
		    divcontral.style.display='none';
		 	}
       fm.LoadD.value="<%=LoadD%>";
       if(fm.LoadD.value=='1')
     {

       document.getElementById('sub1').disabled = true;
       document.getElementById('sub2').disabled = true;
       document.getElementById('sub3').disabled = true;
       document.getElementById('sub4').disabled = true;
       document.getElementById('sub5').disabled = true;
       document.getElementById('sub6').disabled = true;
       document.getElementById('sub7').disabled = true;
       document.getElementById('sub8').disabled = true;
       
     }
 	  <%GlobalInput mG = new GlobalInput();
  		mG=(GlobalInput)session.getValue("GI");
  	%>
     fm.Handler.value = "<%=mG.Operator%>";
     fm.ModifyDate.value = getCurrentDate();
     var strSQL2="select endcasedate,handler from llcase where rgtstate in ('09','11','12') and caseno='"+fm.CaseNo.value+"'";
		 var arrResult2 = easyExecSql(strSQL2);
		 if(arrResult2 != null && arrResult2 !="" ){
		 fm.ModifyDate.value = arrResult2[0][0];
		 fm.Handler.value = arrResult2[0][1];
		}else{
     var strSQL3="select enddate,operator from llcaseoptime where rgtstate='06' and caseno='"+fm.CaseNo.value+"' order by sequance desc fetch first 1 rows only";
		 var arrResult3 = easyExecSql(strSQL3);
		 if(arrResult3 != null && arrResult3 !="" ){
		 fm.ModifyDate.value = arrResult3[0][0];
		 fm.Handler.value = arrResult3[0][1];
		}else{
		var strSQL4="select enddate,operator from llcaseoptime where rgtstate='05' and caseno='"+fm.CaseNo.value+"' order by sequance desc fetch first 1 rows only";
		 var arrResult4 = easyExecSql(strSQL4);
		 if(arrResult4 != null && arrResult4 !="" ){
		 fm.ModifyDate.value = arrResult4[0][0];
		 fm.Handler.value = arrResult4[0][1];
		}
	}
}
	
	initQuery();	
	showCaseRemark();//#1769 案件备注信息的录入和查看功能 add by Houyd 初始化页面弹出
  }
  catch(re)
  {
    alert("ClaimUnderwriteInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

function initClaimPolGrid()
{
    var iArray = new Array();

      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	//列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号码";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[1][1]="120px";         			//列宽
      iArray[1][2]=10;          			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许




      iArray[2]=new Array();
      iArray[2][0]="险种代码";         //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[2][1]="50px";         			//列宽
      iArray[2][2]=10;          			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
       iArray[2][4]="llclaimrisk"
      iArray[2][15]="保单号码"
      iArray[2][17]="1"

      iArray[3]= new Array("给付责任","120px","100","0");

      iArray[4]= new Array("账单金额","50px","100","0");
      iArray[5]= new Array("拒付金额","50px","100","0");
      iArray[6]= new Array("先期给付","50px","100","0");
      iArray[7]= new Array("通融/协议给付比例","100px","100","0");
      iArray[8]= new Array("免赔额","50px","100","3");
      iArray[9]= new Array("溢额","50px","100","0");

      iArray[10]= new Array("赔付结论","60px","100","0");

      iArray[11]= new Array("赔付结论依据","80px","100","0");

      iArray[12]= new Array("理算金额","60px","100","0");
      iArray[13]= new Array("核算赔付金额","80px","100","3");
      iArray[14]= new Array("实赔金额","60px","100","0");

      iArray[15]= new Array("险种保单号","80px","100","3");
      iArray[16]= new Array("赔案号","80px","100","3");
      iArray[17]= new Array("给付责任代码","80px","100","3");
      iArray[18]= new Array("给付责任类型","80px","100","3");
      iArray[19]= new Array("责任代码","80px","100","3");
      iArray[20]= new Array("赔付结论代码","80px","100","3");
      iArray[21]= new Array("赔付结论依据代码","80px","100","3");

      ClaimPolGrid = new MulLineEnter( "fm" , "ClaimPolGrid" );
      //这些属性必须在loadMulLine前
      ClaimPolGrid.mulLineCount = 1;
      ClaimPolGrid.displayTitle = 1;

      ClaimPolGrid.canSel=0;
      ClaimPolGrid.canChk=0;
      ClaimPolGrid.hiddenPlus=1;
      ClaimPolGrid.hiddenSubtraction=1;

      ClaimPolGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面

      }
      catch(ex)
      {
        alert(ex);
      }
}
function initClaimPayGrid()
{
    var iArray = new Array();

      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";    	           //列名
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=100;            			//列最大值
      iArray[0][3]=0;              			  //是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         			//列名
      iArray[1][1]="120px";         			//列宽
      iArray[1][2]=10;          			    //列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]= new Array("险种名称","150px","100","0");

      iArray[3]= new Array("给付责任类型","80px","100","0");

      iArray[4]= new Array("原理算赔付金额","80px","100","3");
      iArray[5]= new Array("核算赔付金额","80px","100","3");
      iArray[6]= new Array("实赔额","80px","100","3");
       iArray[7]= new Array("险种保单号","80px","100","3");
      iArray[8]= new Array("赔案号","80px","100","3");
      iArray[9]= new Array("赔付结论","80px","100","3");
      iArray[10]= new Array("赔付结论代码","80px","100","3");
      iArray[11]= new Array("险种代码","80px","100","3");

      ClaimPayGrid = new MulLineEnter( "fm" , "ClaimPayGrid" );
      //这些属性必须在loadMulLine前
      ClaimPayGrid.mulLineCount = 1;
      ClaimPayGrid.displayTitle = 1;
      ClaimPayGrid.canSel=0;
      ClaimPayGrid.canChk=0;
      ClaimPayGrid.hiddenPlus=1;
      ClaimPayGrid.hiddenSubtraction=1;

      ClaimPayGrid.loadMulLine(iArray);

      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>