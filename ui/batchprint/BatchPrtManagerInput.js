//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();
var mDebug="0";
var mOperate="";
var showInfo;
window.onfocus=myonfocus;
var strSQL = "";
var strCountSQL=""; 

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
  }
}

//（批量）选择打印
function BatchPPrint()
{
	fm.action="NBBPrintSave.jsp";
	fm.fmtransact.value="CONFIRM";
	var recordRecord=10;
	var aTime=1;
		if (fm.BillName.value == "bq009" || fm.BillName.value == "bq014") 
	{
		fm.action="BQBatchPrintSave.jsp"
	  fm.fmtransact.value="PRINT";
	}
	if (verifyInput() == false) return false; 
	var showStr="正在生成打印数据文件，请您稍候... ...";
	//var showStr="正在生成打印数据文件，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../barBean/barBeanStatus.jsp?content="+showStr+"&countNum="+recordRecord+"&oneTime="+aTime;    
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();
}
//（批量）全部打印
function BatchPrintWH()
{
	fm.action="NBBPrintSaveWH.jsp";
	fm.fmtransact.value="CONFIRM";
	if (verifyInput() == false) return false;  
	var showStr="正在生成打印数据文件，请您稍候... ...";
	var recordRecord=10;
	var aTime=1;
	if (fm.BillName.value == "bq009" || fm.BillName.value == "bq014") 
	{
		fm.action="BQBatchPrintSaveWH.jsp"
	  fm.fmtransact.value="PRINT";
	}
	//var Count = 1;
	//opener.intCount=1;
	var urlStr="../barBean/barBeanStatus.jsp?content="+showStr+"&countNum="+recordRecord+"&oneTime="+aTime;  
	//alert(urlStr);
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	fm.submit();

}

function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}   

// 查询按钮
function easyQueryClick() {
  strSQL = "select case department when 'LP' then '理赔' when 'QY' then '契约' "
         + "when 'BQ' then '保全' when 'XQ' then '续期' else '其他' end,batchtimescode,"
         + "case billtype when '01' then'批单' when '02' then '保全客户通知书' "
         + "when '03' then '缴费提醒'when '04' then '缴费对账单'when '05' then '满期终止通知书' "
         + "when '06' then '团险缴费提醒' when '07' then '缴费对账单'when '08' then '满期终止通知书' "
         + "when '09' then '理赔通知书' when '10' then '契约客户通知书' else '其他' end,copy,makedate,operator "
         + "from LOBATCHPRTLOG where prtflag = '0' "
         + getWherePart('ManageCom', 'ManageCom', 'like')
         + "with ur";
  turnPage.queryModal(strSQL,TaskGrid);
  //turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //SetQueryBL();
} 

//用于当单证名称变化时，打印信息列表项名称的变化
function updatefield()
{
	alert("as");
	var iArray=new Array();
		iArray[0]="投保单号";
		iArray[1]="140px";
		iArray[2]=100;
		iArray[3]=0;

	TaskGrid.updateField(3,iArray);
}    

//明细查询
function ListQuery()
{
	var tBillType ="";
	var tBatch = "";
	try
	{
	  fm.all("PrtTypeName").style.display='';
	  fm.all("divTask").style.display='none';
  }
  catch(ex){}
    var tSel = TaskGrid.getSelNo();
    //var tSel = 1;
  if( tSel == 0 || tSel == null )
  {
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
	}
	else{
		tBillType ='理赔通知书'; //spanTaskGrid.getRowColData(tSel-1,3);
		tBatch ='86000000000316'; //spanTaskGrid.getRowColData(tSel-1,2);
	}
	if (tBillType == "理赔通知书"){
		initClaimNoticeMutline();
		var sql = "select distinct a.otherno,c.customername,e.name,b.codename,c.rgtdate,c.handler,a.prtseq "
            + "from lobatchprtmanager a,ldbatchprint b,llcase c,llclaimdetail d,lcinsured e "
            + "where a.batchtimescode = '"+tBatch+"' and c.caseno = a.otherno "
            + "and a.code = b.code and d.caseno = a.otherno and e.contno = d.contno "
            + "union select all a.otherno,c.customername,e.name,b.codename,c.rgtdate,c.handler,a.prtseq "
            + "from lobatchprtmanager a,ldbatchprint b,llcase c,llclaimdetail d,lbinsured e "
            + "where a.batchtimescode = '"+tBatch+"' and c.caseno = a.otherno "
            + "and a.code = b.code and d.caseno = a.otherno and e.contno = d.contno "
            + "order by otherno with ur ";
    //fm.all("ManageComName").value = sql;
    turnPage1.queryModal(sql,ListGrid);
		}
}


function CancelListQuery()
{
	try{
		 fm.all("PrtTypeName").style.display='none';
		 fm.all("divTask").style.display='';
		}
	catch(ex){}	
}
function PrnListQuery()
{
	initPrnMutline();
}
function ClientNoticeListQuery()
{
	initClientNoticeMutline();
}
function FeeNoticeListQuery()
{
	initFeeNoticeMutline();
}
function BillPrnListQuery()
{
	initBillPrnMutline();
}
function DisableNoticeListQuery()
{
	initDisableNoticeMutline();
}
function initPrnMutline()
{
	  ID="序号";
    ListCols1="批单号";
    ListCols2="客户号";	
    ListCols3="投递人姓名";
    ListCols4="生成时间";
    ListCols5="经办人";
    ListCols6="NO";
    ListCols7="NO";
    ListCols61="0px";
    ListCols71="0px";
    
    initListGrid();
	} 
//理赔通知书明细初始化
function initClientNoticeMutline()
{
		ID="序号";
    ListCols1="受理号";	
    ListCols2="客户号";
    ListCols3="投递人姓名";
    ListCols4="通知书类型";
    ListCols5="生成时间";
    ListCols6="经办人";
    ListCols61="100px";
    ListCols7="NO";
    ListCols71="0px";
    initListGrid();
	}
function initClaimNoticeMutline()
{
		ID="序号";
    ListCols1="案件号";	
    ListCols2="客户号";
    ListCols3="投递人姓名";
    ListCols4="通知书类型";
    ListCols5="生成时间";
    ListCols6="经办人";
    ListCols61="100px";
    ListCols7="NO";
    ListCols71="0px";
    initListGrid();
	
}	   
function initFeeNoticeMutline()
{
		ID="序号";
    ListCols1="通知书号";	
    ListCols2="投保人姓名";
    ListCols3="保单号";
    ListCols4="应收日期";
    ListCols5="应收记录号";
    ListCols6="生成时间";
    ListCols7="经办人";
    ListCols61="100px";
    ListCols71="100px";
    initListGrid(); 
	}	
function initBillPrnMutline()
{
		ID="序号";
    ListCols1="收据号";	
    ListCols2="投保人姓名";
    ListCols3="保单号";
    ListCols4="保费金额";
    ListCols5="生成时间";
    ListCols6="经办人";
    ListCols7="NO";
    ListCols61="100px";
    ListCols71="0px";
    initListGrid(); 
	}	
function initDisableNoticeMutline()
{
		ID="序号";
    ListCols1="失效通知书号";	
    ListCols2="投保人姓名";
    ListCols3="保单号";
    ListCols4="应收日期";
    ListCols5="失效日期";
    ListCols6="生成时间";
    ListCols7="经办人";
    ListCols61="100px";
    ListCols71="100px";
    initListGrid();
	}	
