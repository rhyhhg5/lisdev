<!--
 * <p>FileName: \Risk111398.jsp </p>
 * <p>Description: 险种界面文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Author：Minim's ProposalInterfaceMaker
 * @CreateDate：2003-12-30
-->

<DIV id=DivPageHead STYLE="display:''">
<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
</head>

<body>
<form action="./ProposalSave.jsp" method=post name=fm target="fraTitle">

</DIV>

<DIV id=DivRiskCode STYLE="display:''">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      责任编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RiskCode1 VALUE="" MAXLENGTH=6 CLASS=code ondblclick="if(LoadFlag=='1')  showCodeList('RiskCode1',[this]);" onkeyup="return showCodeListKey('RiskCode1',[this]);"  >
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivRiskHidden STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      主险保单号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=MainPolNo>
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCPolHidden STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      险种版本 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RiskVersion>
    </TD>
    <TD CLASS=title>
      合同号
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=ContNo111>
    </TD>
    <TD CLASS=title>
      集体合同号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpContNo>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      首期交费日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=FirstPayDate>
    </TD>
    <TD CLASS=title>
      语种 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Lang>
    </TD>
    <TD CLASS=title>
      货币种类
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Currency>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      合同争议处理方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=DisputedFlag>
    </TD>
    <TD CLASS=title>
      银行代收标记
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentPayFlag>
    </TD>
    <TD CLASS=title>
      银行代付标记
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentGetFlag>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      经办人 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Handler>
    </TD>
    <TD CLASS=title>
      联合代理人编码
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentCode1>
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCPolButton STYLE="display:'none'">
<!-- 保单信息部分 -->
<table>
<tr>
<td>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCPol);">
</td>
<td class= titleImg>
保单信息
<INPUT VALUE="查询责任信息" TYPE=button onclick="showDuty();">
<INPUT VALUE="关联暂交费信息" TYPE=button onclick="showFee();">
<!--<INPUT id="butChooseDuty" VALUE="选择责任" TYPE=button onclick="chooseDuty();" disabled >
<INPUT id="butBack" VALUE="返回" TYPE=button disabled >-->
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCPol STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      投保单号码</TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=ProposalNo VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=11 >
    </TD>
    <TD CLASS=title>
      印刷号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PrtNo VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=11  >
    </TD>
    <TD CLASS=title>
      管理机构 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=ManageCom VALUE="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('comcode',[this],null,null,'#1# and Length(trim(comcode))=8','1',1);" onkeyup="return showCodeListKey('comcode',[this],null,null,'#1# and Length(trim(comcode))=8','1',1);"  >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      销售渠道 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=SaleChnl VALUE="" CLASS=common MAXLENGTH=2 >
    </TD>
    <TD CLASS=title>
      代理人编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentCode VALUE="" MAXLENGTH=10 CLASS=code ondblclick="return queryAgent();"onkeyup="return queryAgent2();" >
    </TD>
    <TD CLASS=title>
      代理人组别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentGroup VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=12 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      备注
    </TD>
    <TD CLASS=input>
      <Input NAME=Remark VALUE="" CLASS=common5 MAXLENGTH=255 >
    </TD>
    <TD CLASS=title>
      合同号码
    </TD>
    <TD CLASS=input COLSPAN=1>
     <Input NAME=ContNo VALUE="" CLASS=readonly readonly TABINDEX=-1 MAXLENGTH=14  >
    </TD>
 
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      代理机构 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentCom VALUE="" CLASS=code ondblclick="showCodeList('AgentCom',[this],null,null,'#1# and ManageCom like #' + fm.all('ManageCom').value.substring(0,4) + '%# and #' + fm.all('ManageCom').value + '# is not null  ','1');" onkeyup="return showCodeListKey('AgentCom',[this],null,null,'#1# and ManageCom like #' + fm.all('ManageCom').value.substring(0,4) + '%# and #' + fm.all('ManageCom').value + '# is not null  ','1');"  >
    </TD>
    <TD CLASS=title>
      银行营业网点
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AgentType VALUE="" CLASS=common >
    </TD>
  </TR>
          <TR CLASS=common>
	    <TD CLASS=title  >
	      缴费频次 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayIntv VALUE="" CLASS="code" CodeData="0|^1|月缴 ^3|季缴 ^6|半年缴 ^12|年缴" MAXLENGTH=20  ondblclick="return showCodeListEx('PayIntv',[this]);" onkeyup="return showCodeListKeyEx('PayIntv',[this]);" >
	    </TD>
	    <TD CLASS=title  >
	      缴费方式 
	    </TD>
	    <TD CLASS=input COLSPAN=1  >
	      <Input NAME=PayMode VALUE="" CLASS="code" CodeData="0|^0|银行转账 ^1|自缴" MAXLENGTH=20  ondblclick="return showCodeListEx('PayMode',[this]);" onkeyup="return showCodeListKeyEx('PayMode',[this]);" >
	    </TD>	    
	  </TR> 
</TABLE>
</DIV>

<DIV id=DivLCAppntIndButton STYLE="display:'none'">
<!-- 投保人信息部分 -->
<table>
<tr>
<td>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntInd);">
</td>
<td class= titleImg>
投保人信息（客户号：<Input class= common  name=AppntCustomerNo >
<INPUT id="butBack" VALUE="查询" TYPE=button onclick="queryAppntNo();">
首次投保客户无需填写客户号）
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCAppntInd STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntName VALUE="" CLASS=common MAXLENGTH=20  >
    </TD>
    <TD CLASS=title>
      性别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntSex VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('Sex', [this]);" onkeyup="return showCodeListKey('Sex', [this]);"  >
    </TD>
    <TD CLASS=title>
      出生日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntBirthday VALUE="" CLASS=common  >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      与被保人关系 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntRelationToInsured VALUE="" MAXLENGTH=2 CLASS=code ondblclick="return showCodeList('Relation', [this]);" onkeyup="return showCodeListKey('Relation', [this]);" >
    </TD>
    <TD CLASS=title>
      证件类型 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntIDType VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('IDType', [this]);" onkeyup="return showCodeListKey('IDType', [this]);"  >
    </TD>
    <TD CLASS=title>
      证件号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntIDNo VALUE="" CLASS=common MAXLENGTH=20 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      国籍
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntNativePlace VALUE="" CLASS=code ondblclick="return showCodeList('NativePlace', [this]);" onkeyup="return showCodeListKey('NativePlace', [this]);" >
    </TD>
    <TD CLASS=title>
      户籍所在地 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntRgtAddress VALUE="" CLASS=common MAXLENGTH=80 >
    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      通讯地址
    </TD>
    <TD CLASS=input COLSPAN=3>
      <Input NAME=AppntPostalAddress VALUE="" CLASS=common3 MAXLENGTH=80 >
    </TD>
    <TD CLASS=title>
      通讯地址邮政编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntZipCode VALUE="" CLASS=common MAXLENGTH=6  >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      住址
    </TD>
    <TD CLASS=input COLSPAN=3>
      <Input NAME=AppntHomeAddress VALUE="" CLASS=common3 MAXLENGTH=80 >
    </TD>
    <TD CLASS=title>
      住址邮政编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntHomeZipCode VALUE="" CLASS=common MAXLENGTH=6 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      联系电话（1）
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntPhone VALUE="" CLASS=common MAXLENGTH=18 >
    </TD>
    <TD CLASS=title>
      联系电话（2）
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntPhone2 VALUE="" CLASS=common MAXLENGTH=18 >
    </TD>
    <TD CLASS=title>
      移动电话
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntMobile VALUE="" CLASS=common MAXLENGTH=15 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      电子邮箱
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntEMail VALUE="" CLASS=common MAXLENGTH=60 >
    </TD>
    <TD CLASS=title>
      工作单位
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntGrpName VALUE="" CLASS=common MAXLENGTH=60 >
    </TD>
    <TD CLASS=title>
      职业（工种） 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntWorkType VALUE="" CLASS=common >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      兼职（工种） 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntPluralityType VALUE="" CLASS=common >
    </TD>
    <TD CLASS=title>
      职业代码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntOccupationCode VALUE="" CLASS=code ondblclick="return showCodeList('OccupationCode', [this,AppntOccupationType],[0,2]);" onkeyup="return showCodeListKey('OccupationCode', [this,AppntOccupationType],[0,2]);"  >
    </TD>
    <TD CLASS=title>
      职业类别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppntOccupationType VALUE="" CLASS=readonly readonly TABINDEX=-1  >
    </TD>
  </TR>

  <TR CLASS=common>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCAppntGrpButton STYLE="display:'none'">
<!-- 集体投保人信息部分 -->
<table>
<tr>
<td>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCAppntGrp);">
</td>
<td class= titleImg>
投保单位资料
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCAppntGrp STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      单位客户号
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppGrpNo>
    </TD>
    <TD CLASS=title>
      单位名称 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppGrpName>
    </TD>
    <TD CLASS=title>
      单位地址 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppGrpAddress>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      邮政编码
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AppGrpZipCode>
    </TD>
    <TD CLASS=title>
      单位性质 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpNature>
    </TD>
    <TD CLASS=title>
      行业类别
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=BusinessType>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      单位总人数
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Peoples>
    </TD>
    <TD CLASS=title>
      主营业务 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=MainBussiness>
    </TD>
    <TD CLASS=title>
      单位法人代表
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Corporation>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD>
      <B>保险联系人一</B>
    </TD>
    <TD COLSPAN=1>

    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=LinkMan1>
    </TD>
    <TD CLASS=title>
      部门
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Department1>
    </TD>
    <TD CLASS=title>
      联系电话
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpPhone1>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      联系电话
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpPhone1>
    </TD>
    <TD CLASS=title>
      E_mail
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=E_Mail1>
    </TD>
    <TD CLASS=title>
      传真
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Fax1>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD>
      <B>保险联系人二</B>
    </TD>
    <TD COLSPAN=1>

    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=LinkMan2>
    </TD>
    <TD CLASS=title>
      部门
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Department2>
    </TD>
    <TD CLASS=title>
      联系电话
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpPhone2>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      E_mail
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=E_Mail2>
    </TD>
    <TD CLASS=title>
      传真
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Fax2>
    </TD>
    <TD>
      <B></B>
    </TD>
    <TD COLSPAN=1>

    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      付款方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetFlag>
    </TD>
    <TD CLASS=title>
      开户银行
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpBankCode>
    </TD>
    <TD CLASS=title>
      银行帐号 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpBankAccNo>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      货币种类
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Currency>
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCInsuredButton STYLE="display:'none'">
<!-- 被保人信息部分 -->
<table>
<tr>
<td class=common>
<!--<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCInsured);">-->
</td>
<td class= titleImg>
被保人信息（客户号：<Input class= common name=CustomerNo >
<INPUT id="butBack" VALUE="查询" TYPE=button onclick="queryInsuredNo();"> 首次投保客户无需填写客户号）
<Div  id= "divSamePerson" style= "display: ''">
<font color=red>
如投保人为被保险人本人，可免填本栏，请选择
<INPUT TYPE="checkbox" NAME="SamePersonFlag" onclick="isSamePerson();">
</font>
</div>
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCInsured STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      姓名 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Name VALUE="" CLASS=common MAXLENGTH=20 >
    </TD>
    <TD CLASS=title>
      性别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Sex VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('Sex', [this]);" onkeyup="return showCodeListKey('Sex', [this]);"  >
    </TD>
    <TD CLASS=title>
      出生日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Birthday VALUE="" CLASS=common  >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      证件类型 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=IDType VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('IDType', [this]);" onkeyup="return showCodeListKey('IDType', [this]);" >
    </TD>
    <TD CLASS=title>
      证件号码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=IDNo VALUE="" CLASS=common MAXLENGTH=20 >
    </TD>
    <TD CLASS=title>
      国籍
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=NativePlace VALUE="" CLASS=code ondblclick="return showCodeList('NativePlace', [this]);" onkeyup="return showCodeListKey('NativePlace', [this]);"  >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      户口所在地 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RgtAddress VALUE="" CLASS=common MAXLENGTH=80 >
    </TD>
    <TD CLASS=title>
      住址 
    </TD>
    <TD CLASS=input COLSPAN=3>
      <Input NAME=HomeAddress VALUE="" CLASS=common3 MAXLENGTH=80 >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      邮政编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=HomeZipCode VALUE="" CLASS=common MAXLENGTH=6 >
    </TD>
    <TD CLASS=title>
      联系电话（1）
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Phone VALUE="" CLASS=common MAXLENGTH=18 >
    </TD>
    <TD CLASS=title>
      联系电话（2）
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Phone2 VALUE="" CLASS=common >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      工作单位
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GrpName VALUE="" CLASS=common MAXLENGTH=60 >
    </TD>
    <TD CLASS=title>
      职业（工种） 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=WorkType VALUE="" CLASS=common >
    </TD>
    <TD CLASS=title>
      兼职（工种） 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PluralityType VALUE="" CLASS=common >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      职业代码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=OccupationCode VALUE="" CLASS=code ondblclick="return showCodeList('OccupationCode', [this,OccupationType],[0,2]);" onkeyup="return showCodeListKey('OccupationCode', [this,OccupationType],[0,2]);"  >
    </TD>
    <TD CLASS=title>
      职业类别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=OccupationType VALUE="" CLASS=readonly readonly TABINDEX=-1  >
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCInsuredHidden STYLE="display:'none'">
<TABLE class=common>

  <TR CLASS=common>
    <TD CLASS=title>
      健康状况 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Health>
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCBnf STYLE="display:'none'">
<!-- 受益人信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCBnf1);">
</td>
<td class= titleImg>
受益人信息
</td>
</tr>
</table>

<Div  id= "divLCBnf1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanBnfGrid" >
</span>
</td>
</tr>
</table>
</div>

</DIV>

<DIV id=DivLCKindButton STYLE="display:''">
<!-- 险种信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCKind);">
</td>
<td class= titleImg>
险种信息
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCKind STYLE="display:''">
<TABLE class=common> 
 
 <TR CLASS=common>
    <TD CLASS=title>
     交通意外责任类别
    </TD>
    <TD CLASS=input COLSPAN=1>
    	<Input class=code  name="traffic" value ="飞机" CodeData="0|^1|飞机^2|火车^3|轮船^4|客运汽车^5|自驾车"  ondblclick=" showCodeListEx('traffic',[this],[0,1],null,null,null,1);" onkeyup=" showCodeListKeyEx('traffic',[this],[0,1],null,null,null,1);">
    </TD>
     <TD CLASS=title>
      保额 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Amnt VALUE="" elementtype=nacessary CLASS=common MAXLENGTH=12 >单位:万元
    </TD>
 </TR>
 
 <tr class=common>
    
    <TD CLASS=title>
      各方案投保人数
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InsurePeople value=""  elementtype=nacessary class=common>
    </TD>
    <TD CLASS=title>
     责任保费理算 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PayEndYearFlag value="" class=common>
    </TD>
  </TR>
  
</TABLE>
</DIV>

<DIV id=DivLCKindHidden STYLE="display:'none'">
<TABLE class=common>
 <TR CLASS=common>
	  <TD CLASS=title>
      保单生效日期
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=CValiDate VALUE="" CLASS=common >
    </TD>
     <TD CLASS=title>
      投保申请日期
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PolApplyDate VALUE="" CLASS=common verify="保单申请日期|date" >
    </TD>
    <TD CLASS=title>
      是否指定生效日期 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=SpecifyValiDate VALUE="Y" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('YesNo', [this]);" onkeyup="return showCodeListKey('YesNo', [this]);"  >
    </TD>
    <TD CLASS=title>
      收费方式
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=PayLocation VALUE="" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('PayLocation', [this]);" onkeyup="return showCodeListKey('PayLocation', [this]);"  >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      开户行 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=BankCode VALUE="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('bank', [this]);" onkeyup="return showCodeListKey('bank', [this]);" >
    </TD>
    <TD CLASS=title>
      户名 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AccName VALUE="" CLASS=common MAXLENGTH=20 >
    </TD>
    <TD CLASS=title>
      银行帐号 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=BankAccNo VALUE="" CLASS=common MAXLENGTH=40 >
    </TD>
  </TR>
<!--
  <TR CLASS=common>
    <TD CLASS=title>
      保险期间
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InsuYear>
    </TD>
    <TD CLASS=title>
      保险期间单位 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InsuYearFlag>
    </TD>
    
-->


  </TR>
  <TR CLASS=common>
    <TD CLASS=title>
      溢交保费方式
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=OutPayFlag VALUE="1" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('OutPayFlag', [this]);" onkeyup="return showCodeListKey('OutPayFlag', [this]);"  >
    </TD>
    <TD CLASS=title>
      浮动费率
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=FloatRate VALUE="1"  CLASS=common  TABINDEX=-1 >
    </TD>
	   <TD CLASS=title>
      是否体检件
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=HealthCheckFlag VALUE="N" MAXLENGTH=1 CLASS=code ondblclick="return showCodeList('YesNo', [this]);" onkeyup="return showCodeListKey('YesNo', [this]);" verify="是否体检件|code:YesNo" >
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      给付方法
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetDutyKind>
    </TD>
    <TD CLASS=title>
      领取方式
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=getIntv>
    </TD>
    <TD CLASS=title>
      起领期间
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetYear>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      起领期间单位 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetYearFlag>
    </TD>
    <TD CLASS=title>
      起领日期计算类型 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=GetStartType>
    </TD>
    <TD CLASS=title>
      自动垫交标志 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=AutoPayFlag>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      利差返还方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InterestDifFlag>
    </TD>
    <TD CLASS=title>
      减额交清标志 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=SubFlag>
    </TD>
    <TD CLASS=title>
      红利领取方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=BonusGetMode>
    </TD>
  </TR>

  <TR CLASS=common>
    <TD CLASS=title>
      生存金领取方式 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=LiveGetMode>
    </TD>
    <TD CLASS=title>
      是否自动续保
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RnewFlag>
    </TD>
  </TR>

</TABLE>
</DIV>

<DIV id=DivLCSubInsured STYLE="display:'none'">
<!-- 连带被保人信息部分（列表） -->
<Div  id= "divLCInsured0" style= "display: 'none'">
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
</td>
<td class= titleImg>
连带被保人信息
</td>
</tr>
</table>

<Div  id= "divLCInsured2" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanSubInsuredGrid" >
</span>
</td>
</tr>
</table>
</div>

</div>

</DIV>

<DIV id=DivLCImpart STYLE="display:'none'">
<!-- 告知信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCImpart1);">
</td>
<td class= titleImg>
告知信息
</td>
</tr>
</table>

<Div  id= "divLCImpart1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartGrid" >
</span>
</td>
</tr>
</table>
</div>

</DIV>

<DIV id=DivLCSpec STYLE="display:''">
<!-- 特约信息部分（列表） -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCSpec1);">
</td>
<td class= titleImg>
特约信息
</td>
</tr>
</table>

<Div  id= "divLCSpec1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanSpecGrid">
</span>
</td>
</tr>
</table>
</div>

</DIV>

<DIV id=DivChooseDuty STYLE="display:'none'">
<!--可以选择的责任部分，该部分始终隐藏-->
<Div  id= "divChooseDuty0" style= "display: 'none'">
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divDutyGrid1);">
</td>
<td class= titleImg>
可选责任信息
</td>
</tr>
</table>

<Div  id= "divDutyGrid1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanDutyGrid" >
</span>
</td>
</tr>
</table>
</div>
</div>

</DIV>

<DIV id=DivPageEnd STYLE="display:''">
<input type=hidden id="inpNeedDutyGrid" name="inpNeedDutyGrid" value="0">
<input type=hidden id="fmAction" name="fmAction">
<input type=hidden id="ContType" name="ContType" value="">
<input  type= "hidden" class= Common name= SelPolNo value= "">
<input type=hidden id="inpNeedPremGrid" name="inpNeedPremGrid" value="0">

<Div  id= "divButton" style= "display: ''">
<br>
<%@include file="../common/jsp/ProposalOperateButton.jsp"%>
</DIV>

</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
<span id="spanApprove"  style="display: none; position:relative; slategray"></span>

</body>
</html>

<script>
function returnParent() {
var isDia=0;
var haveMenu=0;
var callerWindowObj;

try	{
callerWindowObj = dialogArguments;
isDia=1;
}
catch(ex1) {
isDia=0;
}

try	{
if(isDia==0) { //如果是打开一个新的窗口，则执行下面的代码
top.opener.parent.document.body.innerHTML=window.document.body.innerHTML;
}
else { //如果打开一个模态对话框，则调用下面的代码
callerWindowObj.document.body.innerHTML=window.document.body.innerHTML;
haveMenu = 1;
callerWindowObj.parent.frames("fraMenu").Ldd = 0;
callerWindowObj.parent.frames("fraMenu").Go();
}
}
catch(ex)	{
if( haveMenu != 1 ) {
alert("Riskxxx.jsp:发生错误："+ex.name);
}
}

top.close();
}

returnParent();
</script>

</DIV>



