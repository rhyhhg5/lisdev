<!--
 * <p>FileName: \Risk111111.jsp </p>
 * <p>Description: 险种界面文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Author：Minim's ProposalInterfaceMaker
 * @CreateDate：2003-12-30
-->

<DIV id=DivPageHead STYLE="display:''">
<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>

<body>
<form action="./ProposalSave.jsp" method=post name=fm target="fraTitle">

</DIV>


<DIV id=DivRiskCode STYLE="display:''">
<TABLE class=common>


</TABLE>
</DIV>

<DIV id=DivLCKindButton STYLE="display:''">
<!-- 险种信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCKind);">
</td>
<td class= titleImg>
责任信息
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCKind STYLE="display:''">
<TABLE class=common> 
 
  <TR CLASS=common>
    <TD CLASS=title>
      责任编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RiskCode1 VALUE="111111" MAXLENGTH=6 CLASS=code readonly=true  ondblclick="return showCodeList('RiskCode1',[this]);" onkeyup="return showCodeListKey('RiskCode1',[this]);" >
    </TD>
      <TD CLASS=title>
    险种编码 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=RiskCode VALUE="" MAXLENGTH=6 CLASS=code ondblclick="showCodeList('RiskCode',[this]);" onkeyup="showCodeListKey('RiskCode',[this]);" >
    </TD>
  </TR>
 <TR CLASS=common>
    <TD CLASS=title>
     年龄组
    </TD>
    <TD CLASS=input COLSPAN=1>
    	<Input class=code  name=Age value ="18-65周岁" CodeData="0|^1|18-65周岁^2|0-17周岁^3|66-80周岁"  ondblclick="showCodeListEx('Age',[this],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('Age',[this],[0,1],null,null,null,1);">说明:高于80岁,不支持报价
    </TD>
    <TD CLASS=title>
     职业类别 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input class=code NAME=Professional value="一类" CodeData="0|^1|一类^2|二类^3|三类^4|四类^5|五类^6|六类" ondblclick=" showCodeListEx('Professional',[this],[0,1],null,null,null,1);" onkeyup=" showCodeListKeyEx('Professional',[this],[0,1],null,null,null,1);">
    </TD>

 </TR>
 
 <tr class=common>
    <TD CLASS=title>
      保额 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Amnt VALUE="" elementtype=nacessary CLASS=common MAXLENGTH=12 >单位:万元
    </TD>
    <TD CLASS=title>
      各方案投保人数
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InsurePeople elementtype=nacessary value="" class=common>
    </TD>
    
  </TR>
  <TR CLASS=common>
  	<TD CLASS=title>
      录入保费 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InputPrem VALUE="" elementtype=nacessary CLASS=common MAXLENGTH=12 >
    </TD>
  </TR>
  
</TABLE>
</DIV>

<Div  id= "divLCImpart1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartGrid" >
</span>
</td>
</tr>
</table>
</div>



<Div  id= "divDutyGrid1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanDutyGrid" >
</span>
</td>
</tr>
</table>


</DIV>

<DIV id=DivPageEnd STYLE="display:''">
<input type=hidden id="inpNeedDutyGrid" name="inpNeedDutyGrid" value="0">
<input type=hidden id="fmAction" name="fmAction">
<input type=hidden id="ContType" name="ContType" value="">
<input  type= "hidden" class= Common name= SelPolNo value= "">
<input type=hidden id="inpNeedPremGrid" name="inpNeedPremGrid" value="0">

<div id="inputQuest" style="display: 'black'">
	        <input type="button" class=cssButton name="Input" value="上一步" onClick="returnparent()" class=cssButton>
     
		<!-- <INPUT class=cssButton VALUE=问题件查询 TYPE=button onclick="QuestQuery();"> -->
		<!--input type="button" class=cssButton name="Input" value="强制解除锁定" onClick="unLockTable();" class=cssButton-->
</div>
<div id="modifyButton" style="display: 'black'">
	  	<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();" style="float: right">
			<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return deleteClick();" style="float: right">
        <!-- 
        <INPUT class=cssButton id="riskbutton2" VALUE="录入完毕"  TYPE=button onclick="inputConfirm(1);" style="float: right"> 					
         -->
		<!--input type="button" class=cssButton name="Input" value="强制解除锁定" onClick="unLockTable();" class=cssButton-->
</div>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
<span id="spanApprove"  style="display: none; position:relative; slategray"></span>

</body>
</html>

<script>
function returnParent() {
var isDia=0;
var haveMenu=0;
var callerWindowObj;

try	{
callerWindowObj = dialogArguments;
isDia=1;
}
catch(ex1) {
isDia=0;
}

try	{
if(isDia==0) { //如果是打开一个新的窗口，则执行下面的代码
top.opener.parent.document.body.innerHTML=window.document.body.innerHTML;
}
else { //如果打开一个模态对话框，则调用下面的代码
callerWindowObj.document.body.innerHTML=window.document.body.innerHTML;
haveMenu = 1;
callerWindowObj.parent.frames("fraMenu").Ldd = 0;
callerWindowObj.parent.frames("fraMenu").Go();
}
}
catch(ex)	{
if( haveMenu != 1 ) {
alert("Riskxxx.jsp:发生错误："+ex.name);
}
}

top.close();
}

returnParent();
</script>

</DIV>



