<!--
 * <p>FileName: \Dutyai001.jsp </p>
 * <p>Description: 险种界面文件 </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: sinosoft </p>
 * @Author：Minim's ProposalInterfaceMaker
 * @CreateDate：2003-12-30
-->

<DIV id=DivPageHead STYLE="display:''">
<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>

<body>
<form action="./ProposalSave.jsp" method=post name=fm target="fraTitle">
</DIV>


<DIV id=DivRiskCode STYLE="display:''">
<TABLE class=common>


</TABLE>
</DIV>

<DIV id=DivLCKindButton STYLE="display:''">
<!-- 险种信息部分 -->
<table>
<tr>
<td class=common>
<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,DivLCKind);">
</td>
<td class= titleImg>
责任信息
</td>
</tr>
</table>

</DIV>

<DIV id=DivLCKind STYLE="display:''">
<TABLE class=common> 
 
  <TR CLASS=common>
    <TD CLASS=title>
      责任编码 
    </TD>
    <TD CLASS=input8 COLSPAN=1>
      <Input class="codeNo" NAME=DutyCode VALUE="ht012" MAXLENGTH=6 readonly=true  ondblclick="return showCodeList('DutyCode,[this,DutyCode1Name],[0,1]);" onkeyup="return showCodeListKey('DutyCode',[this,DutyCode1Name],[0,1]);" ><input class=codename name=DutyCode1Name value="住院医疗" readonly=true >
    </TD>
    <TD CLASS=title>
    险种编码 
    </TD>
    <TD CLASS=input8 COLSPAN=1>
      <Input class="codeNo" NAME=RCode ondblclick="showCodeList('RiskCode',[this,RName],[0,1]);" onkeyup="showCodeListKey('RCode',[this,RName],[0,1]);" ><input class=codename name=RName readonly=true >      
    </TD>
  </TR>
  <TR CLASS=common>
   <TD CLASS=title>
     平均年龄
    </TD>
    <TD CLASS=input COLSPAN=1>
    	<Input class=common8 name=Age  >
    </TD>
  <TD CLASS=title>
     免赔额/免责日
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input class=codeNo NAME=Deductible value="" CodeData="0|^1|0^2|50^3|100^4|200^5|300^6|500" ondblclick=" showCodeListEx('Deductible',[this,DeductibleName],[0,1],null,null,null,1);" onkeyup=" showCodeListKeyEx('Deductible',[this,DeductibleName],[0,1],null,null,null,1);"><Input class=codeName name=DeductibleName readonly=true elementtype=nacessary>单位：元
    </TD>
  </TR>
  <TR CLASS=common>
  <TD CLASS=title>
  医疗状况
  </TD>
  <TD CLASS=input COLSPAN=1>
  <input class=codeNo NAME=MedicalState value="" CodeData="0|^1|有^2|无" ondblclick=" showCodeListEx('MedicalState',[this,MedicalStateName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('MedicalState',[this,MedicalStateName],[0,1],null,null,null,1);"><Input class=codeName name=MedicalStateName readonly=true elementtype=nacessary>
  </TD>
  <TD CLASS=title>
  医保所在地
  </TD>
  <TD CLASS=input COLSPAN=1>
  <input class=codeNO NAME=Medicallocal verify="投保人证件类型|code:Medicallocal" value="" ondblclick=" showCodeList('Medicallocal',[this,MedicallocalName],[0,1]);" onkeyup="showCodeListKey('Medicallocal',[this,MedicallocalName],[0,1]);"><Input class=codeName name=MedicallocalName  elementtype=nacessary>
  </TD>
  </TR>
 
 <TR CLASS=common>
    <TD CLASS=title>
     给付比例
    </TD>
    <TD CLASS=input COLSPAN=1>
    	<Input class=codeNo  name=Payment value ="" CodeData="0|^1|100%^2|90%^3|80%^4|70%^5|60%"  ondblclick="showCodeListEx('Payment',[this,PaymentName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('Payment',[this,PaymentName],[0,1],null,null,null,1);"><input class=codeName name=PaymentName readonly=true elementtype=nacessary>
    </TD>
    <TD CLASS=title>
     自费药范围
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input class=codeNo name=MedicalOwn value="" CodeData="0|^1|全部为社保范围内^2|开放乙类药品^3|开放乙类药品+诊疗^4|开放乙类+自费" ondblclick=" showCodeListEx('MedicalOwn',[this,MedicalOwnName],[0,1],null,null,null,1);" onkeyup=" showCodeListKeyEx('MedicalOwn',[this,MedicalOwnName],[0,1],null,null,null,1);"><input class=codeName name=MedicalOwnName readonly=true elementtype=nacessary>
    </TD>

 </TR>
 <tr class=common>
    <TD CLASS=title>
      人数
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=People elementtype=nacessary value="" class=common>
    </TD>
    <TD CLASS=title>
     免赔方式
    </TD>
    <TD CLASS=input COLSPAN=1>
    	<Input class=codeNo  name=AoadMode value ="" CodeData="0|^1|年免赔^2|次免赔"  ondblclick="showCodeListEx('AoadMode',[this,AoadModeName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('AoadMode',[this,AoadModeName],[0,1],null,null,null,1);"><input class=codename name=AoadModeName readonly=true elementtype=nacessary>
    </TD>
  </TR>
  <TR CLASS=common>
  <TD CLASS=title>
      保额 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=Amnt verify="保额|notnull" VALUE="" elementtype=nacessary CLASS=common MAXLENGTH=12 >单位:万元
    </TD>
  	<TD CLASS=title>
      录入保费 
    </TD>
    <TD CLASS=input COLSPAN=1>
      <Input NAME=InputPrem verify="录入保费|notnull" VALUE="" elementtype=nacessary CLASS=common MAXLENGTH=12 >
    </TD>
  </TR>
  
</TABLE>
</DIV>

<Div  id= "divLCImpart1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanImpartGrid" >
</span>
</td>
</tr>
</table>
</div>



<Div  id= "divDutyGrid1" style= "display: ''">
<table  class= common>
<tr  class= common>
<td text-align: left colSpan=1>
<span id="spanDutyGrid" >
</span>
</td>
</tr>
</table>


</DIV>

<DIV id=DivPageEnd STYLE="display:''">
<input type=hidden id="inpNeedDutyGrid" name="inpNeedDutyGrid" value="0">
<input type=hidden id="fmAction" name="fmAction">
<input type=hidden id="ContType" name="ContType" value="">
<input  type= "hidden" class= Common name= SelPolNo value= "">
<input type=hidden id="inpNeedPremGrid" name="inpNeedPremGrid" value="0">

<div id="inputQuest" style="display: 'black'">
	        <input type="button" class=cssButton name="Input" value="上一步" onClick="returnparent()" class=cssButton>
     
		<!-- <INPUT class=cssButton VALUE=问题件查询 TYPE=button onclick="QuestQuery();"> -->
		<!--input type="button" class=cssButton name="Input" value="强制解除锁定" onClick="unLockTable();" class=cssButton-->
</div>
<div id="modifyButton" style="display: 'black'">
	  	<INPUT class=cssButton VALUE="修  改"  TYPE=button onclick="return updateClick();" style="float: right">
			<INPUT class=cssButton VALUE="保  存"  TYPE=button onclick="return deleteClick();" style="float: right">
        <!-- 
        <INPUT class=cssButton id="riskbutton2" VALUE="录入完毕"  TYPE=button onclick="inputConfirm(1);" style="float: right"> 					
         -->
		<!--input type="button" class=cssButton name="Input" value="强制解除锁定" onClick="unLockTable();" class=cssButton-->
</div>
</form>

<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
<span id="spanApprove"  style="display: none; position:relative; slategray"></span>

</body>
</html>

<script>
function returnParent() {
var isDia=0;
var haveMenu=0;
var callerWindowObj;

try	{
callerWindowObj = dialogArguments;
isDia=1;
}
catch(ex1) {
isDia=0;
}

try	{
if(isDia==0) { //如果是打开一个新的窗口，则执行下面的代码
top.opener.parent.document.body.innerHTML=window.document.body.innerHTML;
}
else { //如果打开一个模态对话框，则调用下面的代码
callerWindowObj.document.body.innerHTML=window.document.body.innerHTML;
haveMenu = 1;
callerWindowObj.parent.frames("fraMenu").Ldd = 0;
callerWindowObj.parent.frames("fraMenu").Go();
}
}
catch(ex)	{
if( haveMenu != 1 ) {
alert("Dutyxxx.jsp:发生错误："+ex.name);
}
}

top.close();
}

returnParent();
</script>

</DIV>



