var turnPage = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
  	//parent.fraInterface.initForm();
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showDiv(operateButton, "true");
		showDiv(inputButton, "false");
    //执行下一步操作
	}
}
// 查询按钮
function easyQueryClick() {
	if (!verifyInput2()) {
		return false;
	}
	fm.prtno.value = prtNo;
	
	//此处书写SQL语句			     			    
	var strSql = "select grpcontno, name, "
	            + " (select codename from ldcode where codetype = 'sex' and code = sex), birthday, "
	            + " (select codename from ldcode where codetype = 'idtype' and code = IDtype), IDNo, "
	            + " persontype, SocialSecurityNo, ContactInformation, phone, mobile,seqno "
				+ "from LCIllnessInsuredList where 1=1 " 
				+  getWherePart('GrpContNo','prtno','=') 
				+  getWherePart('name','Name','=')
				+  getWherePart('sex','Sex','=')
				+  getWherePart('birthday','Birthday','=')
				+  getWherePart('IDtype','IDType','=')
				+  getWherePart('IDNo','IDNo','=')
				+ "with ur";
	turnPage.queryModal(strSql, LCInuredListGrid,'1');
	fm.querySql.value = strSql;
}
function getQueryResult() {
	var arrSelected = null;
	tRow = LCInuredListGrid.getSelNo();
	if (tRow == 0 || tRow == null) {
		return arrSelected;
	}
	arrSelected = new Array();
	arrSelected[0] = new Array();
	arrSelected[0] = LCInuredListGrid.getRowData(tRow - 1);
	return arrSelected;
}
function returnParent() {
		top.close();
}

function goToInsert(){

 

  var i = 0;
  var checkFlag = 0;
  var state = "0";

  

    var strReturn="1";
    //打开扫描件录入界面
    sFeatures = "";
    
     

    if (strReturn == "1"){
    
        window.open("./SIUpdateInsuredMain.jsp?saveid=1&prtNo="+ prtNo +"&ManageCom="+ manageCom +"&MissionID="+MissionID, "", "");
        
		
	}

}

function goToUpdate(){

  if (LCInuredListGrid.mulLineCount == 0)
		{
			alert("对不起，列表中无数据！");
			return false;
		}
	var count = 0;
	var tChked = new Array();
	var b = "";
	for(var i = 0; i < LCInuredListGrid.mulLineCount; i++)
	{
		if(LCInuredListGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
			b = i;
		}
		
	}
	
	if(count == 0){
		alert("请选择一条数据！");
		return false;
	}
	
	if(count > 1){
		alert("修改只能选择一条数据进行修改！");
		return false;
	}
	
	

	if(count == 1)
  	{
  		var seqno = LCInuredListGrid.getRowColData(b,12,LCInuredListGrid);//seqno
  		var grpcontno = LCInuredListGrid.getRowColData(b,1,LCInuredListGrid);
  		window.open("./SIUpdateInsuredMain.jsp?saveid=2&prtNo="+grpcontno+"&seqno=" +seqno , "", "");
	}
  
  
  
  
  
	
}
function deleteAllClick(){
	
		if (LCInuredListGrid.mulLineCount == 0)
		{
			alert("对不起，列表中无数据！");
			return false;
		}
	
	    fm.seqno.value = prtNo;
    	fm.fmtransact.value="DELETEALL";
		var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
		fm.action="./SIUpdateInsuredSave.jsp?prtNo="+prtNo;
		fm.submit(); //提交
}



function deleteClick()
{

	if (LCInuredListGrid.mulLineCount == 0)
		{
			alert("对不起，列表中无数据！");
			return false;
		}
	var count = 0;
	var tChked = new Array();
	for(var i = 0; i < LCInuredListGrid.mulLineCount; i++)
	{
		if(LCInuredListGrid.getChkNo(i) == true)
		{
			tChked[count++] = i;
		}
		
	}
	
	fm.target = "fraSubmit";
	fm.fmtransact.value = "DELETE||MAIN";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./SIUpdateInsuredSave.jsp";
	fm.submit();
}

function afterSubmit(FlagStr, content)
{	
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		window.focus();
		window.location.reload();
	}
}

//下载
function downLoad() {
	if(fm.querySql.value != null && fm.querySql.value != "" && LCInuredListGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "SIInsuredQueryDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}