<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<% 
String LoadFlag = request.getParameter("LoadFlag");
String Resource = request.getParameter("Resource");
//String PrtNo = request.getParameter("prtNo");
String SaveId = request.getParameter("saveid");
String grpcontno = request.getParameter("grpcontno");
String name = request.getParameter("name");
String sex = request.getParameter("sex");
String birthday = request.getParameter("birthday");
String IDNo = request.getParameter("IDNo");
String seqno = request.getParameter("seqno");
String prtNo = request.getParameter("prtNo");
String ManageCom = request.getParameter("ManageCom");
String MissionID = request.getParameter("MissionID");

String mobile = request.getParameter("mobile");
String phone = request.getParameter("phone");
String contactinformation = request.getParameter("contactinformation");
String socialsecurityno = request.getParameter("socialsecurityno");
String persontype = request.getParameter("persontype");
String idtype = request.getParameter("idtype");


%>
	<head>
		<script>
			
			var mobile = "<%=mobile%>";
			var phone = "<%=phone%>";
			var contactinformation = "<%=contactinformation%>";
			var socialsecurityno = "<%=socialsecurityno%>";
			var persontype = "<%=persontype%>";
			var idtype = "<%=idtype%>";
		
			var prtNo="<%=prtNo%>";
			var LoadFlag="<%=LoadFlag%>";
 			var Resource="<%=Resource%>";
		
			var grpcontno="<%=grpcontno%>";
			
 			var SaveId = "<%=SaveId%>";
 			var name = "<%=name%>";
 			var sex = "<%=sex%>";
 			var birthday = "<%=birthday%>";
 			var IDNo = "<%=IDNo%>";
 			var seqno = "<%=seqno%>";
 			
	 		 var MissionID = "<%=MissionID%>";
	 		  var ManageCom = "<%=ManageCom%>";
 			
 		</script>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="SIUpdateInsuredInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<%@include file="SIUpdateInsuredInit.jsp"%>

	</head>
	<body onload="initForm();initElementtype();">
		<form action="./SIUpdateInsuredSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;">
					</td>
					<td class=titleImg>
						合同信息
					</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						保单号
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=PrtNo>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=ManageCom>
					</TD>
					<TD class=title>
						投保单位名称
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=GrpName>
					</TD>
				</TR>
			</table>
			<br>
			<hr>
			<Div id="firstSetting" style="display: ''">
				
				<table class=common>
				
					<TR class=common>
						<TD class=title8>
							合同ID
						</TD>
						<TD class=input8>
							<Input class=common name="PaperID"  elementtype="nacessary" verify="合同ID|notnull" >
						</TD>
						<TD class=title8>
							被保人ID
						</TD>
						<TD class=input>
							<input class=common name=ProtectID  elementtype=nacessary verify="被保人ID|notnull" >
						</TD>
						
						<TD class=title>
							与主被保人关系
						</TD>
						<TD class=input>
							<Input class="codeNo" name="Relation"  
							CodeData="0|^00|本人^01|丈夫^02|儿女^03|父亲^04|母亲^05|岳父^06|岳母^07|女婿^08|公公^09|媳妇^10|兄弟姐妹^11|表兄弟姐妹^12|叔伯^13|姑姑^14|舅舅^15|姨^16|侄子/女^17|爷爷^18|奶奶"
							  verify="职业类别|code:Relation"  ondblclick="return showCodeListEx('', [this,RelationName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('', [this,RelationName],[0,1],null,null,null,1);"><input class=codename name="RelationName" readonly="true" elementtype="nacessary">
						</TD>
						
					</TR>
				
					<TR class=common>
						<TD class=title8>
							被保人姓名
						</TD>
						<TD class=input8>
							<Input class=common name="Name" verify="被保人姓名|notnull" elementtype="nacessary">
						</TD>
						<TD class=title8>
							被保人性别
						</TD>
						<TD class=input>
							<Input class=codeNo name=Sex  CodeData="0|^0|男^1|女^2|其他" verify="投保人性别|code:Sex" ondblclick="return showCodeListEx('',[this,SexName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('',[this,SexName],[0,1],null,null,null,1);"><input class=codename name=SexName readonly=true elementtype=nacessary>
						</TD>
						<TD class=title>
							出生日期
						</TD>
						<TD class=input>
							<Input class="coolDatePicker" verify="出生日期|notnull"  name="Birthday" verify="出生日期|date|notnull" elementtype="nacessary">
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							证件类型
						</TD>
						<TD class=input>
							<Input class="codeNo" name="IDType"  CodeData="0|^0|身份证^1|护照^2|军官证^3|工作证^4|其他^5|户口本"  verify="投保人证件类型|code:IDType" ondblclick="return showCodeListEx('',[this,IDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('',[this,IDTypeName],[0,1],null,null,null,1);"><input class="codename" name="IDTypeName" readonly="true"	elementtype="nacessary">
						</TD>
						<TD class=title8>
							证件号码
						</TD>
						<TD class=input8>
							<Input class=common name="IDNo" verify="投保人证件号码|len<=20|notnull" elementtype="nacessary">
						</TD>
						<TD class=title>
							证件有效期起
						</TD>
						<TD class=input colspan=1>
							<Input class=common name="IDStartDate" verify="Idstartdate">
						</TD>
					</TR>
					<TR class=common>
						<TD class=title>
							证件有效期止
						</TD>
						<TD class=input colspan=1>
							<Input class="common" name="IDEndDate" verify="Idstartdate">
						</TD>
						<TD class=title>
							其他证件类型
						</TD>
						<TD class=input>
							<Input class="codeNo" name="OthIDType"  CodeData="0|^0|身份证^1|护照^2|军官证^3|工作证^4|其他^5|户口本"  verify="投保人证件类型|code:IDType" ondblclick="return showCodeListEx('',[this,OthIDTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('',[this,OthIDTypeName],[0,1],null,null,null,1);"><input class="codename" name="OthIDTypeName" readonly="true">
						</TD>
						<TD class=title>
							其他证件号码
						</TD>
						<TD class=input colspan=1>
							<Input class="common" name="OthIDNo" verify="投保人证件号码|len<=20">
						</TD>
					</TR>
					
					<TR class=common>
						<TD class=title8>
							人员类别
						</TD>
						<TD class=input8>
							<Input class="common" name="PersonType"  >
						</TD>
						<TD class=title>
							社保卡号
						</TD>
						<TD class=input colspan=1>
							<Input class="common" name="SocialSecurityNo" verify="OccupationCode">
						</TD>

						<TD class=title>
							联系方式
						</TD>
						<TD class=input>
							<input class="common" name="ContactInformation" >
						</TD>
					</TR>
					
					<TR class=common>
						<TD class=title8>
							联系电话
						</TD>
						<TD class=input8>
							<Input class="common" name="Phone" >
						</TD>
						<TD class=title>
							移动电话
						</TD>
						<TD class=input colspan=1>
							<Input class="common" name="Mobile" verify="OccupationCode">
						</TD>

						<TD class=title>
							保障计划
						</TD>
						<TD class=input>
							<Input class="common" name="ContPlanCode" verify="保障计划|notnull" elementtype="nacessary" />
						</TD>
					</TR>
					
					
					<TR class=common>
						
						<TD class=title>
							职业代码
						</TD>
						<TD class=input colspan=1>
							<Input class="common" name="OccupationCode" verify="OccupationCode">
						</TD>

						<TD class=title>
							职业类别
						</TD>
						<TD class=input>
							<Input class="codeno" name="OccupationType"  CodeData="0|^1|一类^2|二类^3|三类^4|四类^5|五类"   verify="职业类别|code:OccupationType"  ondblclick="return showCodeListEx('',[this,OccupationTypeName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('',[this,OccupationTypeName],[0,1],null,null,null,1);"><input class="codename" name="OccupationTypeName" readonly="true">
						</TD>
						<TD class=title>
							在职/退休
						</TD>
						<TD class=input>
							<Input class="codeno" name="Retire" verify="在职/退休|int" CodeData="0|^1|在职^2|退休" ondblClick="showCodeListEx('',[this,RetireName],[0,1],null,null,null,1);" onkeyup="showCodeListKeyEx('',[this,RetireName],[0,1],null,null,null,1);"><input class="codename" name="RetireName" readonly="true">
						</TD>
					</TR>
					<TR class=common>
					<TD class=title>
							帐号
						</TD>
						<TD class=input>
							<Input class="common" name="BankAccNo">
						</TD>

						<TD class=title>
							户名
						</TD>
						<TD class=input>
							<Input class="common" name="AccName">
						</TD>
						
						
						<TD class=title>
							理赔金转帐银行
						</TD>
						<TD class=input>
							<Input class="common" name="BankCode">
						</TD>
					</TR>
					<TR class=common>
						
						
						<TD class=title>
							
						</TD>
						<TD class=input>
							<Input class="common" type = hidden name="EmployeeName">
						</TD>

						<TD class=title>
							
						</TD>
						<TD class=input>
							<Input class=common type=hidden name="Relation1"   >
						</TD>
					</TR>
					<TR style="display:none">
						<TD class=title>
							客户唯一标识
						</TD>
						<TD class=input>
							<Input class="common" name="SeqNo" readonly="true">
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>
							<Input class="common" name="GrpContNo" readonly="true">
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>
						</TD>
					</TR>
				</table>
				<table class=common>
					<tr align=left>
						<td class=button>
							<INPUT class=cssButton VALUE="保  存" TYPE=button name="save" onclick="return addClick();">
							<INPUT class=cssButton VALUE="修  改" TYPE=button name="update" onclick="return updateClick();">
						</td>
					</tr>
				</table>

			</Div>
			<input type=hidden id="fmtransact" name="fmtransact" value="INSERT||MAIN">
			<input type=hidden id="fmAction" name="fmAction">
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
