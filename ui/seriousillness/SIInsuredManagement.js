//程序名称：ScanContInput.js
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容

var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{
  if(!verifyInput2())
    return false;
  var ApplyDate = fm.InputDate.value;
  if(strCurDay!==ApplyDate)
  {
    alert("申请日期不是当天！");
    return false;
  }
  if( verifyInput2() == false )
    return false;
  cPrtNo = fm.PrtNo.value;
  cManageCom = fm.ManageCom.value;
  if(cPrtNo == "")
  {
    alert("请录入印刷号！");
    return;
  }
  if(cManageCom == "")
  {
    alert("请录入管理机构！");
    return;
  }
  //if(isNumeric(cPrtNo)==false)
  //{
  //  alert("印刷号应为数字");
  //  return false;
  //}
  if(type=='2')//对于集体保单的申请
  {
    if (GrpbeforeSubmit() == false)
    {
      alert("已存在该印刷号，请选择其他值!");
      return false;
    }

  }
  else //对于个人保单的申请
  {
    if (beforeSubmit() == false)
    {
      alert("已存在该印刷号，请选择其他值!");
      return false;
    }
  }
  fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{

  easyQueryClick();
}

/*********************************************************************
 *  执行新契约扫描的EasyQuery
 *  描述:查询显示对象是扫描件.显示条件:扫描件已上载成功
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
  // 初始化表格
  
  	var bdNo = fm.PrtNo.value;
	var startDate = fm.StartDate.value;
	var endDate = fm.EndDate.value;
	if(bdNo == null || bdNo == ""){
			if((startDate == null || startDate == "")&&(endDate==null || endDate == "")){
			alert("保单号码或生效日期范围必须录入一项!");
			return false;
		}
	}
	
	if((startDate != null && startDate != "") && (endDate == null || endDate == ""))
	{
		alert("请录入投保终止日期！");
		return false;
	}
	
	if((startDate == null || startDate == "") && (endDate != null && endDate != ""))
	{
		alert("请录入投保起始日期！");
		return false;
	}
	
	if((startDate != null && startDate != "") && (endDate != null && endDate != ""))
	{
		if(dateDiff(startDate, endDate, "M") > 3)
		{
			alert("投保日期区间不能大于3个月！")
			return false;
		}
	}
	
   
  // 书写SQL语句
  		var strSQL = "";
 
		strSQL = "select grpcontno,managecom,cvalidate "
				+ "from lcgrpcont "
				+ "where appflag = '1' "
				+ getWherePart('managecom','ManageCom','like')
				+ getWherePart('grpcontno','PrtNo')
				+ getWherePart('cvalidate','StartDate','>=')
				+ getWherePart('cvalidate','EndDate','<=')
				+"with ur";
       turnPage.queryModal(strSQL,GrpGrid); 
       return true; 
   
  //turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //
  ////判断是否查询成功
  //if (!turnPage.strQueryResult)
  //{
  //  alert("没有已申请的投保单，请录入印刷号开始录入！");
  //  return "";
  //}
  //
  ////查询成功则拆分字符串，返回二维数组
  //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //
  ////设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  //turnPage.pageDisplayGrid = GrpGrid;
  //
  ////保存SQL语句
  //turnPage.strQuerySql     = strSQL;
  //
  ////设置查询起始位置
  //turnPage.pageIndex       = 0;
  //
  ////在查询结果数组中取出符合页面显示大小设置的数组
  //var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  //
  ////调用MULTILINE对象显示查询结果
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);

}
function GoToInput()
{

  var i = 0;
  var checkFlag = 0;
  var state = "0";

  for (i=0; i<GrpGrid.mulLineCount; i++)
  {
    if (GrpGrid.getSelNo(i))
    {
      checkFlag = GrpGrid.getSelNo();
      break;
    }
  }

  if (checkFlag)
  {
    var	prtNo = GrpGrid.getRowColData(checkFlag - 1, 1);
    var ManageCom = GrpGrid.getRowColData(checkFlag - 1, 2);
    var PolApplyDate = GrpGrid.getRowColData(checkFlag - 1, 3);

    var strReturn="1";
    //打开扫描件录入界面
    sFeatures = "";
    
    var urlStr = "../common/jsp/UnLockTable.jsp?PrtNo=" + prtNo + "&CreatePos=承保录单&PolState=1002&Action=INSERT";
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:0px;dialogHeight:0px;resizable:1"); 

    if (strReturn == "1"){
    
        window.open("./SIInputInsuredMain.jsp?ScanFlag=0&LoadFlag=1&prtNo="+prtNo+"&ManageCom="+ManageCom+"&MissionID="+PolApplyDate , "", sFeatures);
				
	}
  }
  else
  {
    alert("请先选择一条保单信息！");
  }	
}
function beforeSubmit()
{
  var strSQL = "";
  strSQL = "select missionprop1 from lwmission where 1=1 "
           + " and activityid = '0000001098' "
           + " and processid = '0000000003'"
           + " and missionprop1='"+fm.PrtNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }

  strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
           "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);

  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }
}
function GrpbeforeSubmit()
{
  var strSQL = "";
  strSQL = "select missionprop1 from lwmission where 1=1 "
           + " and activityid = '0000002098' "
           + " and processid = '0000000004'"
           + " and missionprop1='"+fm.PrtNo.value+"'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (turnPage.strQueryResult)
  {

    return false;
  }

  strSQL = "select prtno from lccont where prtno = '" + fm.PrtNo.value + "'"
           "union select prtno from lbcont where prtno = '" + fm.PrtNo.value + "'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (turnPage.strQueryResult)
  {
    return false;
  }
}






