<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		String GrpContNo = request.getParameter("GrpContNo");
		String LoadFlag = request.getParameter("LoadFlag");
		String Resource = request.getParameter("Resource");
		String prtNo = request.getParameter("prtNo");
		String ManageCom = request.getParameter("ManageCom");
		String MissionID = request.getParameter("MissionID");
	%>
	<head>
		<script>
			var GrpContNo="<%=GrpContNo%>";
			var prtNo="<%=prtNo%>"
			var LoadFlag="<%=LoadFlag%>";
 			var Resource="<%=Resource%>";
 			var prtNo ="<%=prtNo%>";
 			var ManageCom ="<%=ManageCom%>";
 			var MissionID ="<%=MissionID%>";
 		</script>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

		<SCRIPT src="SIInputInsuredInput.js"></SCRIPT>
		<%@include file="SIInputInsuredInit.jsp"%>
		<title>大团单被保人导入</title>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit" enctype="multipart/form-data">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;">
					</td>
					<td class=titleImg>
						合同信息
					</td>
				</tr>
			</table>
			<table class=common align=center>
				<TR class=common>
					<TD class=title>
						保单号
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=PrtNo>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=ManageCom>
					</TD>
					<TD class=title>
						投保单位名称
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=GrpName>
					</TD>
				</TR>
				<TR  class=common>
					<TD class=title>
						已导入总人数
					</TD>
					<TD class=input>
						<Input class=readonly readonly name=InsuredCount>
					</TD>
				</TR>
			</table>
			<br>
			<hr>
			<table>
				<tr class=common style="height:30px">
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"">
					</td>
					<td class=titleImg>
						导入被保险人
					</td>
				</tr>
			</table>
			<table class=common>
				<TR class=common>
					<TD width='8%' style="font:9pt">
						导入文件名：
					</TD>
					<TD width='80%'>
						<input type="file" width="100%" name=FileName class=common>
						<input VALUE="上载被保险人" class="cssbutton" TYPE=button
							onclick="InsuredListUpload();">
					</TD>
				</TR>
			</table>
			<br>
			<hr>
			<table>
				<tr class=common style="height:30px">
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"">
					</td>
					<td class=titleImg>
						导入被保险人情况
					</td>
					<td style="width:100px">
						<center>
							<INPUT VALUE="刷  新" class=cssbutton TYPE=button
								onclick="refreshInsured();">
						</center>
					</td>
				</tr>
			</table>
			<table class=common>
				<tr class=common>
					<td class=title>
						导入文件名
					</td>
					<td class=input>
						<input class=readonly name=BatchNo readonly>
					</td>
					<TD class=title>
						已导入人数
					</TD>
					<TD class=input>
						<Input class='common' name=InputInsured value="0" readonly>
					</TD>
					<TD class=title>
						导入失败人数
					</TD>
					<TD class=input>
						<Input class='common' name=InputErrorInsured value="0" readonly>
					</TD>
				</tr>
			</table>
			<br>
			<hr>
			<br>
			<table>
       			<tr>
            		<td class=common>
                		<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;">
            	</td>
            		<td class= titleImg>被保险人导入信息</td>
        		</tr>
			</table>
    		<Div  id= "divDiskErr" style= "display: ''">
        		<Table  class= common>
            		<TR  class= common>
                		<TD text-align: left colSpan=1>
                    		<span id="spanDiskErrQueryGrid" ></span>
                		</TD>
            		</TR>
        		</Table>
        		<div align="center">
            		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();">
            		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();">
            		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();">
            		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();">
        		</div>
    		</Div>
    		<br>
    		<hr>
    		<font color="red"><strong>在导入被保人后，该页面可以关闭，导入处理会在后台进行。</strong></font>
			<table>
				<tr>
					<td>
						<input class="button" type="button" value="已导入被保人管理"
							onclick="GoToInput();">
					</td>
					<td>
						<input class="button" type="button" value="关  闭"
							onclick="goBack();">
					</td>
				</tr>
			</table>
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
