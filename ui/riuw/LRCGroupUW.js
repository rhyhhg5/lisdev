//程序名称：GroupUW.js
//程序功能：集体人工核保
//创建日期：2002-06-19 11:10:36
//创建人  ：WHN
//更新记录：  更新人    更新日期     更新原因/内容


/*********************************************************************
 *  //该文件中包含客户端需要处理的函数和事件
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();
var turnPage3 = new turnPageClass();
var k = 0;
var cflag = "1";  //问题件操作位置 1.核保
var mSwitch = parent.VD.gVSwitch;
 var Resource;

/*********************************************************************
 *  提交对应操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //showSubmitFrame(mDebug);
  fmQuery.submit(); //提交
  alert("submit");
}



/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

  }
  else
  {
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
}



function afterSubmit1( FlagStr, content )
{
  showInfo.close();
  window.focus();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

  }
  else
  {
    //showDiv(operateButton,"true");
    //showDiv(inputButton,"false");
    //执行下一步操作
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showFeeRate();
  }
}

function showFeeRate()
{
	
	
  var strSql = "select a.RiskCode,b.RiskName,div(a.prem,(select sum(standprem) from lcpol c where a.RiskCode=c.RiskCode and c.GrpContNo in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"'))),a.ComFeeRate,a.BranchFeeRate from LCGrpPol a,LMRisk b"+
               " where a.RiskCode=b.RiskCode and a.GrpContNo in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"') order by a.riskcode";
  turnPage3.queryModal(strSql,FeeRateGrid); 
  //如果该险种有一责任的标准保费为0,则折扣比例显示为空,并且总公司,分公司费用率分别为'N/A'
  var grppolSql = "select grppolno,standbyflag3,riskcode from lcgrppol where grpcontno in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"') order by riskcode";
  var arr1 = easyExecSql(grppolSql); 
  if(arr1)
  {
  	  for(var i = 0;i<arr1.length; i++)
  	  {   	  	
  	  	  if(arr1[i][1] == null || arr1[i][1] == "")
  	  	  {  	  	  	
							 var dutySql = "select sum(standprem),sum(prem) from lcduty where polno in (select polno from lcpol where grppolno='"+arr1[i][0]+"') and trim(dutycode) in (select dutycode from lmriskduty where riskcode='"+arr1[i][2]+"')";
							 var arr = easyExecSql(dutySql);
							 if(arr) 
							 {						
								    for(var a=0;a<arr.length;a++) 
								    {   							    	
								    	  if(arr[a][0] == "0")
								     	  { 				     	  
								     	  	   FeeRateGrid.setRowColData(i,3,'N/A') ;								     	  	   
								     	  	   FeeRateGrid.setRowColData(i,5,'N/A') ;
								     	  }
								     	  else
								     	  	{
                            var b=arr[a][1]/arr[a][0];
								     	      var b = ''+b;		
								     	  	   FeeRateGrid.setRowColData(i,3,b.substring(0,4)); 								     	      
								     	  		}
								    }
						   }   
				  }   
				  else
				  {		
				  	var tSql = " select a.riskcode,b.riskname,a.standbyflag3,a.ComFeeRate,a.BranchFeeRate from lcgrppol a,lmrisk b where a.grpcontno = '"+GrpContNo+"'"
				  	         + " and a.riskcode = b.riskcode";			  	        
				  	turnPage3.queryModal(tSql,FeeRateGrid);          
         }				  
			}
	  }
}
//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}


function showRIExamInfo(){
	window.open("./RIExamInfo.jsp?PrtNo="+fmQuery.all("PrtNo").value);
} 
/*********************************************************************
 *  查询团体单核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGNewUWSub()
{
  var cGrpContNo=fmQuery.GrpContNo.value;
  if(cGrpContNo==""||cGrpContNo==null)
  {
    alert("请先选择一个团体投保单!");
    return ;
  }
  window.open("../uw/UWGErrMain.jsp?GrpContNo="+cGrpContNo,"window1");
}

//取消按钮对应操作
function cancelForm()
{
  showDiv(operateButton,"true");
  showDiv(inputButton,"false");
}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作
}



/*********************************************************************
 *  显示frmSubmit框架，用来调试
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false");
  showDiv(inputButton,"true");
}


/*********************************************************************
 *  查询团体单下个人单既往投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  var cInsureNo = fmQuery.InsuredNo.value;
  if(cProposalNo==""||cProposalNo==null|| cInsureNo==""||cInsureNo==null)
  {
    showInfo.close();
    alert("请选择个人保单,后查看其信息!");
    return ;
  }
  //showModalDialog("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./UWAppMain.jsp?ProposalNo1="+cProposalNo+"&InsureNo1="+cInsureNo,"window1");
  showInfo.close();
}


/*********************************************************************
 *  查询团体单下个人单以往核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showOldUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==""||cProposalNo==null)
  {
    showInfo.close();
    alert("请选择个人保单,后查看其信息!");
    return ;
  }
  //showModalDialog("./UWSubMain.jsp?ProposalNo1="+cProposalNo,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./UWSubMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}

function showLRCContMsg()
{
	 window.open("./LRCManuUW.jsp?PrtNo="+fmQuery.all("PrtNo").value);
}
/*********************************************************************
 *  查询团体单下个人单核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showNewUWSub()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==""||cProposalNo==null)
  {
    showInfo.close();
    alert("请先选择个人保单,后查看其信息!");
    return ;
  }
  window.open("./UWErrMain.jsp?ProposalNo1="+cProposalNo,"window1");
  showInfo.close();
}


/*********************************************************************
 *  查询团体单核保记录
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */




/*********************************************************************
 *  查询团体下的附属于各团单的个人保单明细信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showPolDetail()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fmQuery.ProposalNo.value;
  if(cProposalNo==null||cProposalNo=="")
  {
    showInfo.close();
    alert("请先选择个人保单,后查看其信息!");

  }
  else
  {
    mSwitch.deleteVar( "PolNo" );
    mSwitch.addVar( "PolNo", "", cProposalNo );
    window.open("../app/ProposalMain.jsp?LoadFlag=4");
    showInfo.close();
  }
}


/*********************************************************************
 *  查询团体下个人保单体检资料
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHealth()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  if (cProposalNo != "")
  {
    window.open("./UWManuHealthMain.jsp?ProposalNo1="+cProposalNo,"window1");
    showInfo.close();
  }
  else
  {
    showInfo.close();
    alert("请先选择个人保单,后查看其信息!");
  }
}


/*********************************************************************
 *  对团体单下的个人加费承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.ProposalNo.value;
  tUWIdea = fmQuery.all('UWIdea').value;
  if (cProposalNo != "")
  {
    window.open("./UWGSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag=2&UWIdea="+tUWIdea,"window1");
    showInfo.close();
  }
  else
  {
    showInfo.close();
    alert("请先选择一个个人投保单!");
  }
}


/*********************************************************************
 *  对团体单特约承保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showGSpec()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  var cProposalNo=fmQuery.GrpProposalContNo.value;
  var tUWIdea = fmQuery.GUWIdea.value;
  if (cProposalNo != "")
  {
    window.open("./UWGrpSpecMain.jsp?ProposalNo1="+cProposalNo+"&Flag=1&UWIdea="+tUWIdea,"window1");
    showInfo.close();
  }
  else
  {
    showInfo.close();
    alert("请先选择一个团体保单!");
  }
}


/*********************************************************************
 *  对团体主险投保单的个人体检资料查询(团体单体检件只容许录入到主险个单上)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHealthQ()
{
  var cProposalNo=fmQuery.ProposalNo.value;
  var cGrpProposalContNo=fmQuery.GrpProposalContNo.value;
  var cGrpMainProposalNo=fmQuery.GrpMainProposalNo.value;

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;

  if (cProposalNo != "" && cGrpProposalContNo == cGrpMainProposalNo)
  {
    window.open("./UWManuHealthQMain.jsp?ProposalNo1="+cProposalNo,"window1");

  }
  else
  {

    alert("请先选择一个团体主险投保单下的个人投保单!");
  }
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  //initPolGrid();
  fmQuery.submit(); //提交
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//Click事件，当点击“责任信息”按钮时触发该函数
function showDuty()
{
  //下面增加相应的代码
  showModalDialog("./ProposalDuty.jsp",window,"status:no;help:0;close:0;dialogWidth=18cm;dialogHeight=13cm");

}

//Click事件，当点击“暂交费信息”按钮时触发该函数
function showFee()
{
  //下面增加相应的代码
  showModalDialog("./ProposalFee.jsp",window,"status:no;help:0;close:0;dialogWidth=16cm;dialogHeight=8cm");

}


/*********************************************************************
 *  显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


/*********************************************************************
 *  查询团体单下主附团体投保单单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function querygrp()
{
  
  // 初始化表格
  initPolBox();
  initGrpGrid();

  // 书写SQL语句
  var str = "";
  var PrtNo = fmQuery.all('PrtNoHide').value;
  var mOperate = fmQuery.all('Operator').value;
  var strsql = "";
  //	strsql = "select LCGrpPol.GrpProposalContNo,LCGrpPol.prtNo,LCGrpPol.GrpName,LCGrpPol.RiskCode,LCGrpPol.RiskVersion,LCGrpPol.ManageCom from LCGrpPol,LCGUWMaster where 1=1 "
  //			 	 + " and LCGrpPol.AppFlag='0'"
  //				 + " and LCGrpPol.ApproveFlag in('2','9') "
  //				 + " and LCGrpPol.UWFlag in ('2','3','5','6','8','7')"
  //				 + " and LCGrpPol.contno = '00000000000000000000'"				          //自动核保待人工核保
  //				 + " and LCGrpPol.PrtNo = '"+PrtNo+"'"
  //				 + " and LCGrpPol.GrpPolNo = LCGUWMaster.GrpPolNo"
  //				 + " and (LCGUWMaster.appgrade <= (select UWPopedom from LDUser where usercode = '"+mOperate+"') or LCGUWMaster.appgrade is null)"
  //				 + " order by LCGrpPol.makedate ,LCGrpPol.maketime" ;
  strsql = "select a.ContPlanCode,a.contplanname,a.peoples2,"
           +" '',"//男女比例
           +" ((select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2' and b.ContPlanCode=a.ContPlanCode and b.ContPlanCode not in ('00','11'))/(select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.ContPlanCode=a.ContPlanCode and b.ContPlanCode not in ('00','11')))*100,"
           +" (select varchar(round(sum((to_date(current date)-to_date(b.Birthday))/365)/(select count(1) from lcinsured b where a.grpcontno=b.grpcontno),2)) from lcinsured b where b.grpcontno=a.grpcontno and b.ContPlanCode=a.ContPlanCode) ,"
           +" d.RiskCode,e.RiskName ,'',"
           +" (select sum(standPrem) from lcpol where lcpol.grpcontno=a.grpcontno and lcpol.riskcode=d.riskcode and lcpol.insuredno in (select insuredno from lcinsured where lcinsured.grpcontno=d.grpcontno and lcinsured.contplancode=d.contplancode)) ,"
           +" (select sum(Prem) from lcpol where lcpol.grpcontno=a.grpcontno and lcpol.riskcode=d.riskcode and lcpol.insuredno in (select insuredno from lcinsured where lcinsured.grpcontno=d.grpcontno and lcinsured.contplancode=d.contplancode)) , "
           //+" varchar(((select sum(Prem) from lcpol where lcpol.grpcontno=a.grpcontno and lcpol.riskcode=d.riskcode and lcpol.insuredno in (select insuredno from lcinsured where lcinsured.grpcontno=d.grpcontno and lcinsured.contplancode=d.contplancode)))/(select count(1) from lcinsured where lcinsured.contplancode=d.contplancode and lcinsured.grpcontno=d.grpcontno)), "
           +" '' "
           +" from LCContPlan a,LCContPlanRisk d,LMRisk e where 1=1 and e.RiskCode=d.RiskCode and a.grpcontno=d.grpcontno and a.ContPlanCode=d.ContPlanCode and a.GrpContNo='"+GrpContNo+"' and a.contplancode not in ('00','11')";

  //execEasyQuery( strSQL );
  //查询SQL，返回结果字符串
  //turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 0, 1);
  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    //alert("没有符合条件集体单！");
    //return "";
  }
  else
  {
    ///查询成功则拆分字符串，返回二维数组
    //turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //
    ////设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
    //turnPage.pageDisplayGrid = GrpGrid;
    //
    ////保存SQL语句
    //turnPage.strQuerySql = strsql;
    //
    ////设置查询起始位置
    //turnPage.pageIndex = 0;
    //
    ////在查询结果数组中取出符合页面显示大小设置的数组
    //var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    //
    ////调用MULTILINE对象显示查询结果
    //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  }



  var arrSelected = new Array();
  var strSQL = " select GrpContNo,PrtNo,ManageCom,SaleChnl,AgentCom,AgentCode,AgentGroup,AppntNo,VIPValue, "  
							+" BlacklistFlag,PolApplyDate,CValiDate from lcgrpcont,LDGrp where proposalgrpcontno='"+GrpContNo+"' " 
							+" and lcgrpcont.AppntNo = LDGrp.CustomerNo "                                                  
							+" union "                                                                                     
							+" select GrpContNo,PrtNo,ManageCom,SaleChnl,AgentCom,AgentCode,AgentGroup,AppntNo,VIPValue, " 
							+" BlacklistFlag,PolApplyDate,CValiDate from lbgrpcont,LDGrp where proposalgrpcontno='"+GrpContNo+"' " 
							+" and lbgrpcont.AppntNo = LDGrp.CustomerNo "                                                  
							+" union "                                                                                     
							+" select GrpContNo,PrtNo,ManageCom,SaleChnl,AgentCom,AgentCode,AgentGroup,AppntNo,VIPValue, " 
							+" BlacklistFlag,PolApplyDate,CValiDate from lobgrpcont,LDGrp where proposalgrpcontno='"+GrpContNo+"' " 
							+" and lobgrpcont.AppntNo = LDGrp.CustomerNo  "           
 									;
  //alert(strSQL);
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  if (turnPage.strQueryResult)
  {
    fmQuery.all('GrpContNo').value = arrSelected[0][0];
    fmQuery.all('PrtNo').value = arrSelected[0][1];
    fmQuery.all('ManageCom').value = arrSelected[0][2];
	fmQuery.all('ManageCom_ch').value = arrSelected[0][2];
    //fmQuery.all('ManageCom_ch').value = easyExecSql("select codename from ldcode where codetype='station' and code='"+arrSelected[0][2]+"'");
    fmQuery.all('SaleChnl').value = arrSelected[0][3];
    fmQuery.all('SaleChnl_ch').value = easyExecSql("select codename from ldcode where codetype='salechnl' and code='"+arrSelected[0][3]+"'");

    fmQuery.all('AgentCom').value = arrSelected[0][4];
    if(arrSelected[0][4]=="")
    {
      fmQuery.all('AgentCom_ch').value="";
    }
    else
    {
      fmQuery.all('AgentCom_ch').value = easyExecSql("select name from lacom where AgentCom='"+arrSelected[0][4]+"'");
    }

    fmQuery.all('AgentCode').value = arrSelected[0][5];
    fmQuery.all('AgentCode_ch').value = easyExecSql("select name from laagent where agentcode='"+arrSelected[0][5]+"'");
    fmQuery.all('AgentGroup').value = arrSelected[0][6];
    fmQuery.all('AgentGroup_ch').value = easyExecSql("select name from labranchgroup where  AgentGroup='"+arrSelected[0][6]+"'");
    fmQuery.all('AppntNo').value = arrSelected[0][7];
    fmQuery.all('VIPValue').value = arrSelected[0][8];
    fmQuery.all('BlacklistFlag').value = arrSelected[0][9];
    fmQuery.all('PolApplyDate').value = arrSelected[0][10];
    fmQuery.all('CValiDate').value = arrSelected[0][11];
  }
  showGrpAppntInfo();
  return true;
}


/*********************************************************************
 *  查询团体单下自动核保未通过的个人单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function queryPol()
{
  var GrpProposalContNo = fmQuery.all('GrpProposalContNo').value;
  var mOperate = fmQuery.all('Operator').value;
  var strsql = "";

  // 初始化表格
  showDiv(divPerson,"false");
  initPolBox();
  //initPolGrid();
  // 书写SQL语句

  strsql = "select LCPol.ProposalNo,LCPol.AppntName,LCPol.RiskCode,LCPol.RiskVersion,LCPol.InsuredName,LCPol.ManageCom from LCPol where 1=1 "
           + "and LCPol.GrpPolNo = '"+GrpProposalContNo+"'"
           + " and LCPol.AppFlag='0'"
           + " and LCPol.UWFlag in ('3','5','6','8','7')"         //自动核保待人工核保
           + " and LCPol.contno = '00000000000000000000'"
           + " order by LCPol.makedate ";

  //alert(strsql);
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strsql, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    alert("此单下个人单核保均已全部通过！");
    return "";
  }

  showDiv(divPerson,"true");
  //个人体检资料录入只针对团体主险投保单下的个人单
  if(GrpProposalContNo == fmQuery.all('GrpMainProposalNo').value)
  {
    showDiv(divBoth,"true");
    showDiv(divAddFee,"false");
  }
  else
  {
    showDiv(divBoth,"false");
    showDiv(divAddFee,"true");
  }
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  //设置初始化过的MULTILINE对象，HealthGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = PolGrid;

  //保存SQL语句
  turnPage.strQuerySql     = strsql;

  //设置查询起始位置
  turnPage.pageIndex       = 0;

  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  return true;
}


/*********************************************************************
 *  查询集体下未过个人单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getGrpPolGridCho()
{
  fmQuery.PolTypeHide.value = '2';
  fmQuery.submit();
}



/*********************************************************************
 *  选择要人工核保保单
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function getPolGridCho()
{
  fmQuery.PolTypeHide.value = '1';
  fmQuery.submit();
}

/*********************************************************************
 *  团体整单核保确认
 *  (0 未核保 1拒保 2延期 3条件承保 4通融 5自动 6待上级 7问题件 8核保通知书 9正常 a撤单 b保险计划变更)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
 function gmanuchk()
{

    var tRICUWState = fmQuery.RICUWState.value;
    if(tRICUWState == null || tRICUWState ==""){
    	alert("请选择总公司核保审核结论！");
    	return false;
    }
    var tRICUWIdea = trim(fmQuery.RICUWIdea.value);
    if(tRICUWState!="0" && (tRICUWIdea ==null || tRICUWIdea =="")){
    	alert("请填写总公司核保审核意见！");
    	return false;
    }
    fmQuery.WorkFlowFlag.value = "0000002012";
    fmQuery.MissionID.value = MissionID;
    fmQuery.SubMissionID.value = SubMissionID;
    //showModalDialog("./UWGrpManuNormMain.jsp?GrpContNo="+cGrpContNo+"&Flag="+flag+"&UWIdea="+tUWIdea,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fmQuery.action = "./LRCUWConfirm.jsp";
    fmQuery.submit(); //提交
}

/**
1、符合再保审核条件，但没有发送给再保模块，需要给出提示
2、若有未结案的再保审核任务，需要给出提示
*/
function checkReInsure()
{
  var sql = "select 1 from LCUWError where GrpContNo = '" + GrpContNo + "' and "
          + "uwrulecode in (select uwcode from lmuw where riskcode "
          + "in (select riskcode from lcgrppol where grpcontno = '"+GrpContNo+"') and passflag='R') "
          + "and exists (select 1 from LCGrpPol where GrpContNo = '"+GrpContNo+"' and GrpPolNo not in (select PolNo from LCReinsurTask)) ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    return confirm("保单还有险种符合再保审核条件，但没有发送给再保模块，继续？");
  }
  
  var sql2 = "select 1 from LCReinsurTask "
      + "where PolNo in(select GrpPolNo from LCPol where GrpContNo = '" + GrpContNo + "') "
      + " and (select GrpContNo from lcgrpcont where GrpContNo = '"+GrpContNo+"') ||'BJ' not in (select polno from LCReinsurTask) "
      + " and State != '02' ";
  var rs2 = easyExecSql(sql2);
  if(rs2)
  {
    return confirm("保单还有未办结的再保审核任务，继续？");
  }
  
  return true;
}

//记事本
function showNotePade()
{
    
    window.open("../uw/UWGroupNotepadMain.jsp?GrpContNo="+GrpContNo+"&PrtNo="+PrtNo, "window1");
}

function showRReport()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");


  var cContNo = fmQuery.GrpContNo.value;


  var cPrtNo = fmQuery.PrtNo.value;
  var mOperate = fmQuery.all('Operator').value;
 var Resource=fmQuery.Resource.value;
 //alert(Resource);
 if(Resource==1|Resource==2|Resource==3){
  if (cContNo != "")
  {
//alert(12);
    window.open("../uw/GrpUWRReportMain.jsp?ContNo1="+cContNo+"&Resource="+Resource+"&PrtNo="+cPrtNo,"window1");
    showInfo.close();
  	}
	}else{
  if (cContNo != "")
  {

    window.open("../uw/GrpUWRReportMain.jsp?ContNo1="+cContNo+"&PrtNo="+cPrtNo,"window1");
    showInfo.close();
  	}	
	}
}

/*********************************************************************
 *  团体问题件录入(团体下的问题件录入原则是:个人单问题件全部发到该团体单下主险团体单上,不发到相应的个单上(考虑到团体单自身业务特点而设计))
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestInput()
{
  var cGrpProposalContNo = fmQuery.GrpContNo.value;  //团体保单号码
  if(cGrpProposalContNo==""||cGrpProposalContNo==null)
  {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  //  if(cGrpProposalContNo != fmQuery.GrpMainProposalNo.value)
  //{
  //		alert("请先选择一个团体主险投保单!");
  //		return ;
  //  }
  //showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
 var Resource=fmQuery.Resource.value;
 if(Resource==1|Resource==2|Resource==3){
window.open("../uw/GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+cflag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&loadflag=5","window2");
}else{
  window.open("../uw/GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+cflag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&loadflag=1","window2");
}
	}

/*********************************************************************
 *  个单问题件录入(目前只是预留,功能已实现,只是未在用户界面上未显示录入按钮)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestInput()
{
  var cProposalNo = fmQuery.ProposalNo.value;

  if(cProposalNo == "")
  {
    alert("请先选择一条个人单记录！");
  }
  else
  {
    window.open("./GrpQuestInputMain.jsp?GrpProposalContNo="+cProposalNo+"&ProposalNo="+cProposalNo+"&Flag="+cflag,"window1");
  }

}


/*********************************************************************
 *  团体问题件录入(团体下的问题件录入原则是:个人单问题件全部发到该团体单下主险团体单上,不发到相应的个单上(考虑到团体单自身业务特点而设计))
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************


function GrpQuestInput()
{
  var cGrpProposalContNo = fmQuery.GrpContNo.value;  //团体保单号码
  if(cGrpProposalContNo==""||cGrpProposalContNo==null)
  {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  //  if(cGrpProposalContNo != fmQuery.GrpMainProposalNo.value)
  //{
  //		alert("请先选择一个团体主险投保单!");
  //		return ;
  //  }
  //showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
 var Resource=fmQuery.Resource.value;
 if(Resource==1|Resource==2|Resource==3){
window.open("./GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+cflag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&loadflag=5","window2");
}else{
  window.open("./GrpQuestInputMain.jsp?GrpProposalContNo="+cGrpProposalContNo+"&ProposalNo="+cGrpProposalContNo+"&Flag="+cflag+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&loadflag=1","window2");
}
	}


/*********************************************************************
 *  个单问题件查询(目前只是预留,功能已实现,只是未在用户界面上未显示查询按钮)
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function QuestQuery()
{
  var cProposalNo = fmQuery.ProposalNo.value;//个单投保单号
  if(cProposalNo==""||cProposalNo==null)
  {
    alert("请先选择一个个人投保单!");
    return ;
  }
  //showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./QuestQueryMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cflag,"window1");
}


/*********************************************************************
 *  团单问题查询(团体下的问题件发放原则是:个人单问题件全部发到该团体单下主险团体单上,不发到相应的个单上(考虑到团体单自身业务特点而设计))
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GrpQuestQuery()
{
  var cGrpProposalContNo = fmQuery.GrpProposalContNo.value;  //团单投保单号码
  if(cGrpProposalContNo==""||cGrpProposalContNo==null)
  {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  if(cGrpProposalContNo!=fmQuery.GrpMainProposalNo.value)
  {
    alert("请先选择一个团体主险投保单!");
    return ;
  }
  //showModalDialog("./QuestInputMain.jsp?ProposalNo1="+cProposalNo+"&Flag="+cFlag,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
  window.open("./QuestQueryMain.jsp?ProposalNo1="+cGrpProposalContNo+"&Flag="+cflag,"window1");
}

//发团体问题件通知书
function SendIssueNotice()
{
  var tGrpContNo = fmQuery.GrpContNo.value ;
  if(tGrpContNo == "")
  {
    alert("页面错误，未查询到团体合同信息！");
    return ;
  }
  var strSql = "select * from LCGrpIssuePol where GrpContNo='"+tGrpContNo+"'";
  var arr = easyExecSql(strSql);
  if(!arr)
  {
    alert("页面错误，未查询到团体问题件信息！");
    return ;
  }
  else
  {
  }
  var strSql = "select 1 from loprtmanager where otherno='"
               +tGrpContNo+"' and OtherNoType = '01' and code='54' and StandbyFlag1 is null";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    alert("已发团体问题件通知书。\n补发功能待作！");
    return ;
  }

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  //alert();
  fmQuery.action = "./GrpSendIssueSave.jsp?GrpContNo="+tGrpContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
  //alert(fmQuery.action);
  fmQuery.submit();
}

function showMarkPrice()
{
  window.open("../uw/MakePriceMain.jsp?GrpContNo="+fmQuery.GrpContNo.value+"&RiskFlag=UW");
}

//发核保通知书
function SendNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fmQuery.GrpContNo.value;


  if (cProposalNo != "")
  {
    fmQuery.action = "./UWSendNoticeChk.jsp";
    fmQuery.submit();
  }
  else
  {
    showInfo.close();
    alert("请先选择保单!");
  }

}

/*********************************************************************
 *  点击取消按钮,初始化界面各隐藏变量
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function cancelchk()
{
  fmQuery.all('RICUWState').value = "";
  fmQuery.all('RICUWIdea').value = "";
}


/*********************************************************************
 *  返回到自动核保查询主界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function GoBack()
{
  location.href  = "./LRCManuUWAll.jsp?type=2";

}
function temp()
{
  alert("此功能尚缺！");
}
/*********************************************************************
 *  点击扫描件查询
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ScanQuery2()
{
  var arrReturn = new Array();

  var prtNo=fmQuery.all("PrtNo").value;
  if(prtNo==""||prtNo==null)
  {
    alert("请先选择一个团体投保单!");
    return ;
  }
  var SQLDocID=" select DocID from es_doc_main where doccode='"+prtNo+"' " ;
  var cDocID=easyExecSql(SQLDocID);
//  alert(cDocID[0][0]);
  window.open("../easyscan/QCManageInputMainShow.jsp?EASYWAY=1&DocID="+cDocID+"&DocCode="+prtNo+"&BussTpye=TB&SubTpye=TB02");
 }


/*********************************************************************
 *  进入团体合同
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function showGrpCont()
{
  var cGrpContNo=fmQuery.GrpContNo.value;
  var Resource=fmQuery.Resource.value;
  var sql=" select proposalgrpcontno from lcgrpcont where grpcontno='"+cGrpContNo+"' "
					+" union "
					+" select proposalgrpcontno from lbgrpcont where grpcontno='"+cGrpContNo+"'"  
					+" union "
					+" select proposalgrpcontno from lobgrpcont where grpcontno='"+cGrpContNo+"'" 
  ;
  var arr=easyExecSql(sql);
  cGrpContNo=arr[0][0];
  //alert(arr[0][0]);
   var strSql = "SELECT * from lcgrppol l where prtno='"+PrtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
   	var arrResult = easyExecSql(strSql);
   	if (arrResult == null) {

  //var newWindow=window.open("../app/GroupPolApproveInfo.jsp?LoadFlag=16&polNo="+cGrpContNo, "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
  window.open("../app/GroupPolApproveInfo.jsp?ScanFlag=1&scantype=scan&prtNo="+PrtNo+"&scantype=scan&prtNo="+PrtNo+"&Resource="+Resource+"&LoadFlag=16&polNo="+cGrpContNo,"window1");
}else{
 window.open("../uliapp/GroupPolApproveInfo.jsp?ScanFlag=1&scantype=scan&prtNo="+PrtNo+"&scantype=scan&prtNo="+PrtNo+"&Resource="+Resource+"&LoadFlag=16&polNo="+cGrpContNo,"window1");
}
}


//团单既往询价信息
function showAskApp()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cGrpContNo=fmQuery.GrpContNo.value;
  cAppntNo = fmQuery.AppntNo.value;

  if (cGrpContNo != "")
  {

    window.open("../askapp/AskUWAppMain.jsp?GrpContNo1="+cGrpContNo+"&AppntNo1="+cAppntNo,"window2");
    showInfo.close();
  }

}

/*********************************************************************
 *  集体体检通知书
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function checkBody()
{
  cGrpContNo=fmQuery.GrpContNo.value;


  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  if (cGrpContNo != "" )
  {
    fmQuery.action = "../uw/UWManuSendBodyCheckChk.jsp";

    fmQuery.submit();

  }
  else
  {
    showInfo.close();
    alert("请先选择保单!");
  }
}

/*********************************************************************
 *  契调通知书
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
/*
function RReport()
{
 cGrpContNo=fmQuery.GrpContNo.value;
 
  
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
 	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 
  if (cGrpContNo != "" )
  {
  	fmQuery.action = "./UWManuSendRReportChk.jsp";
  	
    fmQuery.submit();
   
  }
  else
  {
 	   showInfo.close();
 	   alert("请先选择保单!");
     }
 	
}
*/





/*********************************************************************
 *  既往保障信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function showHistoryCont()
{
  cGrpContNo=fmQuery.GrpContNo.value;
  window.open("../uw/GrpUWHistoryContMain.jsp?GrpContNo="+cGrpContNo,"window2");
}

function GrpContQuery(cGrpContNo)
{
	var tloadFlag=fmQuery.LoadFlag.value;
	var Resource=fmQuery.Resource.value;
	//alert(tloadFlag);
  window.open("../uw/GroupContMain.jsp?GrpContNo="+ cGrpContNo+"&Resource="+Resource+"&LoadFlag="+tloadFlag,"GroupContQuery");
}

/*********************************************************************
 *  既往信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showHistoryContI()
{
  cGrpContNo=fmQuery.GrpContNo.value;
  cAppntNo  = fmQuery.AppntNo.value;
  window.open("../uw/GrpUWHistoryCont.jsp?pmGrpContNo="+cGrpContNo+"&pmAppntNo=" +cAppntNo+ "","window2");
}
/*********************************************************************
 *  承保计划变更
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function showChangePlan()
{
  //	alert("into showChangePlan");
  var cpolNo = fmQuery.GrpContNo.value;
  //  alert("cpolNo:"+cpolNo);
  var prtNo=fmQuery.PrtNo.value;
   //  alert("prtNo"+prtNo);
  //var cType = "ChangePlan";
//alert(cpolNo);
//alert(prtNo);
var msql="select MissionID,SubMissionID from lwmission where activityid in ('0000002004') and missionprop1='"+cpolNo+"' ";
var arr=easyExecSql(msql);
  // alert("arr:"+arr);
//alert(arr[0][0]);
//alert(arr[0][1]);
cMissionID=arr[0][0];
cSubMissionID=arr[0][1];
 // window.open("../app/GroupPolApproveInfo.jsp?ScanFlag=1&scantype=scan&LoadFlag=23&polNo="+cpolNo, "","status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
//window.open("../sys/ProposalEasyScan.jsp?prtNo="+prtNo+"&Type=1&BussNoType=12&BussType=TB&SubType=TB1002", "", "status:no;help:0;close:0;dialogTop:-800;dialogLeft:-800;fullscreen=1");
   var strSql = "SELECT * from lcgrppol l where prtno='"+PrtNo+"'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
   	var arrResult = easyExecSql(strSql);
   	if (arrResult == null) {
 window.open("../app/GroupPolApproveInfo.jsp?ScanFlag=1&LoadFlag=23&polNo="+cpolNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&scantype=scan&prtNo="+prtNo,"");
}else{
window.open("../uliapp/GroupPolApproveInfo.jsp?ScanFlag=1&LoadFlag=23&polNo="+cpolNo+"&MissionID="+cMissionID+"&SubMissionID="+cSubMissionID+"&scantype=scan&prtNo="+prtNo,"");
}
}

/*********************************************************************
 *  保险计划变更结论录入显示
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */

function showChangeResultView()
{
  fmQuery.ChangeIdea.value = "";
  //fm.PolNoHide.value = fm.ProposalNo.value;  //保单号码
  //divUWResult.style.display= "none";
  divChangeResult.style.display= "";
}


//显示保险计划变更结论
function showChangeResult()
{
  //fmQuery.uwstate.value = "b";
  //fmQuery.UWIdea.value = fm.ChangeIdea.value;
  var cContNo = fmQuery.GrpContNo.value;

  cChangeResult = fmQuery.ChangeIdea.value;

  if (cChangeResult == "")
  {
    alert("没有录入结论!");
  }
  else
  {
    var strSql = "select * from LCIssuepol where ContNo='" + cContNo + "' and (( backobjtype in('1','4') and replyresult is null) or ( backobjtype in('2','3') and needprint = 'Y' and replyresult is null))";
    var arrResult = easyExecSql(strSql);
    if (arrResult != null)
    {
      var tInfo = "有未回复的问题件,确认要进行承保计划变更?";
      if(!window.confirm( tInfo ))
      {
        HideChangeResult()
        return;
      }
    }

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    fmQuery.action = "./GrpUWChangPlanChk.jsp";
    fmQuery.submit(); //提交
  }
  //divUWResult.style.display= "";
  //divChangeResult.style.display= "none";
  //fmQuery.uwstate.value = "";
  //fmQuery.UWIdea.value = "";
  //fmQuery.UWPopedomCode.value = "";
}


//隐藏保险计划变更结论
function HideChangeResult()
{
  //divUWResult.style.display= "";
  divChangeResult.style.display= "none";
  //fmQuery.uwstate.value = "";
  //fmQuery.UWIdea.value = "";
  //fmQuery.UWPopedomCode.value = "";
}


//发首期交费通知书
function SendFirstNotice()
{
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  cProposalNo=fmQuery.GrpContNo.value;

  cOtherNoType="01"; //其他号码类型
  cCode="57";        //单据类型

  if (cProposalNo != "")
  {
    showModalDialog("./GrpUWSendPrintMain.jsp?ProposalNo1="+cProposalNo+"&OtherNoType="+cOtherNoType+"&Code="+cCode,window,"status:no;help:0;close:0;dialogWidth=20cm;dialogHeight=10cm");
    showInfo.close();
  }
  else
  {
    showInfo.close();
    alert("请先选择保单!");
  }
}

//发团体契调通知书
function SendRReport()
{
  var tGrpContNo = fmQuery.GrpContNo.value ;
  // alert("tGrpContNo:"+tGrpContNo);
  if(tGrpContNo == "")
  {
     alert("页面错误，未查询到团体合同信息！");
    return ;
  }
  var strSql = "select 1 from LCGrpRReportItem where GrpContNo='"+tGrpContNo+"'";
  var arr = easyExecSql(strSql);
 // alert("arr:"+arr);
  if(!arr)
  {
    alert("页面错误，未查询到团体契调信息！");
    return ;
  }
  var strSql = "select 1 from loprtmanager where otherno='"
               +tGrpContNo+"' and OtherNoType = '01' and code='74'";
  var arr = easyExecSql(strSql);
  if(arr)
  {
    alert("已发团体契调通知书。\n补发功能待作！");
    return ;
  }

  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fmQuery.action = "./GrpSendRReportSave.jsp?GrpContNo="+tGrpContNo;
  //alert(fmQuery.action);
  fmQuery.submit();
}




//问题件录入
function QuestInput()
{
  cContNo = fmQuery.ContNo.value;  //保单号码

  var strSql = "select * from LCcUWMaster where ContNo='" + cContNo + "' and ChangePolFlag ='1'";
  //alert(strSql);
  var arrResult = easyExecSql(strSql);
  //alert(arrResult);
  if (arrResult != null)
  {
    var tInfo = "承保计划变更未回复,确认要录入新的问题件?";
    if(!window.confirm( tInfo ))
    {
      return;
    }
  }
  window.open("./QuestInputMain.jsp?ContNo="+cContNo+"&Flag="+cflag,"window1");

}

function getfee(parm1,parm2)
{
  var grppolno=fmQuery.all(parm1).all('GrpGrid7').value;
  var strSQL="select sum(StandPrem),sum(Prem) from lcpol where grppolno='"+grppolno+"'";
  turnPage.queryModal(strSQL,GrpPolFeeGrid);
  var a = GrpPolFeeGrid.getRowColData(0,1);
  var b = GrpPolFeeGrid.getRowColData(0,2);
  var c = GrpPolFeeGrid.getRowColData(0,3);
  var a = a/1
          //var a = Math.round(a);
          var a = ''+a;
  //var c = b/a;
  //var c = ''+c;
  if(a=="0")
  {
    c =0;
    //alert(c);
  }
  else
  {
    var c = b/a;
    var c = ''+c;
    c = c.substring(0,5);
  }
  //c = c.substring(0,5);
  GrpPolFeeGrid.setRowColData(0,1,a);
  GrpPolFeeGrid.setRowColData(0,3,c);
  var row = GrpGrid.getSelNo()-1
            //alert();
            fmQuery.GrpProposalContNo.value = GrpGrid.getRowColData(row,1);
  //alert(fmQuery.GrpProposalContNo.value);
}

//显示团体投保资料
function showGrpAppntInfo()
{
	var tloadFlag=fmQuery.LoadFlag.value;
	var tResource=fmQuery.Resource.value;
	//alert(GrpContNo);
	if(tloadFlag==1&tResource==2){
  var strSql = " select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lcpol where grpcontno=a.grpcontno and poltypeflag='0'), "
               +" (select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lcgrpcont a where proposalgrpcontno='"+GrpContNo+"' and  a.uwflag in ('1','a') "
               +" union "
               +" select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lobpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lobpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lobinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lobpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lobpol where grpcontno=a.grpcontno and poltypeflag='0'), "
               +" (select count(1) from lobinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lobgrpcont a where proposalgrpcontno='"+GrpContNo+"' "
               ;
               //alert(1);
              }else if(tloadFlag==1&tResource==1){
  var strSql =  " select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lbpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lbpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lbinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lbpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lbpol where grpcontno=a.grpcontno and poltypeflag='0'), "
               +" (select count(1) from lbinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lbgrpcont a where proposalgrpcontno='"+GrpContNo+"' " 
               ;
                //alert(2);           
              }else if(tloadFlag==1&tResource==3){
  var strSql =  " select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lcpol where grpcontno=a.grpcontno and poltypeflag='0'), "
               +" (select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lcgrpcont a where proposalgrpcontno='"+GrpContNo+"' " 
               ;
               //alert(3);               
              }else{
   var strSql =  " select a.GrpName,a.BusinessType,a.BusinessBigType,a.GrpNature,a.peoples2"
               +",(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),"
               +" DECIMAL(div((select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0'),DECIMAL(a.peoples2,10,2)),10,2)*100,"
               +" div((select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.InsuredStat='2'),(select count(distinct insuredno) from lcpol b where a.grpcontno=b.grpcontno and poltypeflag='0')),"
               +" (select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from lcpol where grpcontno=a.grpcontno and poltypeflag='0'), "
               +" (select count(1) from lcinsured b where a.grpcontno=b.grpcontno and b.RelationToMainInsured<>'00') "
               +" from lcgrpcont a where proposalgrpcontno='"+GrpContNo+"' " 
               ;
               //alert(4);             
              }
  turnPage2.queryModal(strSql,GrpAppntGrid);
  //填充被保险人退休比例，被保险人平均年龄
  //var strSql = "select count(1) from lcinsured where GrpContNo='"+GrpContNo+"'";
  //var arr = easyExecSql(strSql);
  //var insuredCount = 0;
  //if(arr){
  //	insuredCount = arr[0][0];
  //}
  //var strSql = "select count(1) from lcinsured where GrpContNo='"+GrpContNo+"' and InsuredStat='2'";
  //arr = easyExecSql(strSql);
  //var offWorkPepoles=0;
  //if(arr){
  //	offWorkPepoles = arr[0][0];
  //}
  //var rate = 0;
  //if(offWorkPepoles>0){
  //	rate =
  //}
  //alert(GrpAppntGrid.mulLineCount);
  if(GrpAppntGrid.mulLineCount==0)
  {
    var strSql="select a.GrpName,a.GrpNature,a.BusinessBigType,a.BusinessType,a.peoples2 "
               +" from lcgrpcont a where grpcontno='"+GrpContNo+"'";
    turnPage2.queryModal(strSql,GrpAppntGrid);
  }
  //L 保障计划
  //M 人员类别
  //A 参保人数
  //B 计划中应保人数
  //C1 男
  //C2 女
  //D1 在职
  //D2 退休
  //E 平均年龄
  //F 投保险种代码
  //G 投保险种名称
  //H 标准保费
  //I 录入保费
  //I/A 人均保费
  //K 折扣比例
  	if(tloadFlag==1&tResource==2){
  var strSql = "select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +",(select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LCPol where RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode) and GrpContNo=Z.ProposalGrpContNo) E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode)) H"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='0') H2"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode)) I"
               +",(select distinct 1 from LCDuty m where m.standprem = 0 and PolNo in (select PolNo from LCPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode))) k"
               +" from LCContPlanRisk Z,LCContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X "
               +" union "
               +" select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +",(select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LobPol where RiskCode=Z.RiskCode and insuredno in (select insuredno from LobInsured where GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode) and GrpContNo=Z.ProposalGrpContNo) E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LobInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode)) H"
               +",(select sum(StandPrem) from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LobInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='0') H2"
               +",(select sum(StandPrem) from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LobInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LobInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode)) I"
               +",(select distinct 1 from LobDuty m where m.standprem = 0 and PolNo in (select PolNo from LobPol n where n.GrpContNo=Z.ProposalGrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LobInsured j where j.GrpContNo=Z.ProposalGrpContNo and ContPlanCode=Z.ContPlanCode))) k"
               +" from LobContPlanRisk Z,LobContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X with ur "
               ;
              }else if(tloadFlag==1&tResource==1){
  var strSql = "select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +",(select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LbPol where RiskCode=Z.RiskCode and insuredno in (select insuredno from LbInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and GrpContNo=Z.GrpContNo) E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LbInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode)) H"
               +",(select sum(StandPrem) from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LbInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='0') H2"
               +",(select sum(StandPrem) from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LbInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LbInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode)) I"
               +",(select distinct 1 from LbDuty m where m.standprem = 0 and PolNo in (select PolNo from LbPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LbInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode))) k"
               +" from LbContPlanRisk Z,LbContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X with ur"
               ;              
              }else if(tloadFlag==1&tResource==3){
  var strSql = "select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +",(select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LCPol where RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and GrpContNo=Z.GrpContNo) E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode)) H"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='0') H2"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode)) I"
               +",(select distinct 1 from LCDuty m where m.standprem = 0 and PolNo in (select PolNo from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode))) k"
               +" from LCContPlanRisk Z,LCContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X with ur "
               ;              
              }else{
  var strSql = "select L,M,A,div(A,B),div(C1,C2),div(D2,A),E,F,G,'',case when k=1 and (H4 is null or H4<>'3') then 'N/A'  when H4='3' then varchar(H3) else varchar(H) end,I,div(i,a),case when k=1 then 'N/A' else varchar(div(I,H)) end ,K from ("
               +" select "
               +"Z.ContPlanCode L"
               +",Y.ContPlanName M"
               +",Y.Peoples3 A"
               +",Y.Peoples2 B"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='0') C1"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and Sex='1') C2"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='1') D1"
               +",(select count(1) from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode and InsuredStat='2') D2"
               +",(select varchar(round(avg((to_date(CValidate) - to_date(InsuredBirthday)) / 365), 2)) from LCPol where RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured where GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and GrpContNo=Z.GrpContNo) E"
               +",Z.RiskCode F"
               +",(select RiskName from LMRisk where RiskCode=Z.RiskCode) G"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode)) H"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='0') H2"
               +",(select sum(StandPrem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode) and insuredsex='1') H3"
               +",(select risktype8 from lmriskapp where RiskCode=z.RiskCode) H4"
               +",(select sum(Prem) from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode)) I"
               +",(select distinct 1 from LCDuty m where m.standprem = 0 and PolNo in (select PolNo from LCPol n where n.GrpContNo=Z.GrpContNo and RiskCode=Z.RiskCode and insuredno in (select insuredno from LCInsured j where j.GrpContNo=Z.GrpContNo and ContPlanCode=Z.ContPlanCode))) k"
               +" from LCContPlanRisk Z,LCContPlan Y where Z.ProposalGrpContNo='"+GrpContNo+"' and Z.ProposalGrpContNo=Y.ProposalGrpContNo and Z.contplancode not in ('00','11') and Y.contplancode not in ('00','11') and Z.contplancode=Y.contplancode "
               +") as X with ur "
               ;                            
              }
  turnPage2.queryModal(strSql,GrpGrid,0,0,1);
  
}

function showFeeRate()
{
	
	
  var strSql = "select a.RiskCode,b.RiskName,div(a.prem,(select sum(standprem) from lcpol c where a.RiskCode=c.RiskCode and c.GrpContNo in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"'))),a.ComFeeRate,a.BranchFeeRate from LCGrpPol a,LMRisk b"+
               " where a.RiskCode=b.RiskCode and a.GrpContNo in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"') order by a.riskcode";
  turnPage3.queryModal(strSql,FeeRateGrid); 
  //如果该险种有一责任的标准保费为0,则折扣比例显示为空,并且总公司,分公司费用率分别为'N/A'
  var grppolSql = "select grppolno,standbyflag3,riskcode from lcgrppol where grpcontno in (select grpcontno from lcgrpcont where proposalgrpcontno='"+GrpContNo+"') order by riskcode";
  var arr1 = easyExecSql(grppolSql); 
  if(arr1)
  {
  	  for(var i = 0;i<arr1.length; i++)
  	  {   	  	
  	  	  if(arr1[i][1] == null || arr1[i][1] == "")
  	  	  {  	  	  	
							 var dutySql = "select sum(standprem),sum(prem) from lcduty where polno in (select polno from lcpol where grppolno='"+arr1[i][0]+"') and trim(dutycode) in (select dutycode from lmriskduty where riskcode='"+arr1[i][2]+"')";
							 var arr = easyExecSql(dutySql);
							 if(arr) 
							 {						
								    for(var a=0;a<arr.length;a++) 
								    {   							    	
								    	  if(arr[a][0] == "0")
								     	  { 				     	  
								     	  	   FeeRateGrid.setRowColData(i,3,'N/A') ;								     	  	   
								     	  	   FeeRateGrid.setRowColData(i,5,'N/A') ;
								     	  }
								     	  else
								     	  	{
                            var b=arr[a][1]/arr[a][0];
								     	      var b = ''+b;		
								     	  	   FeeRateGrid.setRowColData(i,3,b.substring(0,4)); 								     	      
								     	  		}
								    }
						   }   
				  }   
				  else
				  {		
				  	var tSql = " select a.riskcode,b.riskname,a.standbyflag3,a.ComFeeRate,a.BranchFeeRate from lcgrppol a,lmrisk b where a.grpcontno = '"+GrpContNo+"'"
				  	         + " and a.riskcode = b.riskcode";			  	        
				  	turnPage3.queryModal(tSql,FeeRateGrid);          
         }				  
			}
	  }
}

//初始化核保既望核保信息
function initQueryUWidea(){
	var strSql="SELECT A.OPERATOR,B.USERNAME,'',CHAR(A.MAKEDATE)||' '||CHAR(A.MAKETIME),(SELECT CODENAME FROM LDCODE WHERE CODE=A.SENDTYPE AND CODETYPE='sendtype'),CODENAME,UWIDEA FROM LCUWSENDTRACE A,LDUSER B,LDCODE WHERE CODETYPE='uwflag' AND CODE=A.uwflag AND A.uwflag NOT IN ('0','5','z') AND A.OPERATOR=B.USERCODE AND otherno='"+GrpContNo+"' and sendtype not in ('4','5') order by uwno";
	var arr = easyExecSql(strSql);
	if(arr){
	//	displayMultiline(arr,HisUWInfoGrid);
	}
}	

function checkStandPrem(){
	if(GrpGrid.mulLineCount > 0)
  {
    for (var i = 0 ;i < GrpGrid.mulLineCount; i++)
    {
      var tStandPrem = GrpGrid.getRowColData(i,15);
      //alert(tStandPrem);
      if(tStandPrem == "1")
      {
        GrpGrid.setRowColData(i,11,'N/A') ;
        GrpGrid.setRowColData(i,14,'N/A') ;
      }
    }
  }
}

/**
 * 查询中介手续费录入比例
 */
function queryAgentChargeRate()
{
    var tPrtNo = fmQuery.PrtNo.value;
    
    var tStrSql = " select varchar(double(Rate)) "
        + " from LARateCharge "
        + " where 1 = 1 "
        + " and OtherNo = '" + tPrtNo + "' "
        + " and OtherNoType = '0' "
        + " order by MakeDate desc, MakeTime desc "
        + " fetch first rows only "
        ;
        
    var tRate = easyExecSql(tStrSql);
    if(tRate)
    {
        fmQuery.AgentcyRate.value = tRate;
        showAgentDiv();
    }
}