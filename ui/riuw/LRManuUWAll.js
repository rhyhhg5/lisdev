//程序名称：LRManuUWAll.js
//程序功能：再保审核
//创建日期：2011-11-14 
//创建人  ：Gaoyx
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mDebug="0";
var flag;
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnConfirmPage = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行再保审核的EasyQuery
 *  描述:查询显示对象是合同保单.显示条件:合同未进行人工核保，或状态为待核保
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryClick()
{
	// 书写SQL语句	
var SQL = "select lgc.prtno,varchar(lgc.grpname),char(lgc.cvalidate),varchar(lgc.prem), char(lgc.makedate),lwn.activityid,lgc.grpcontno,lwn.MissionID,lwn.SubMissionID "
          + " from lcgrpcont lgc, lwmission lwn "
          + " where lwn.MissionProp2 = lgc.prtno and lwn.activityid = '0000002011'"
          + getWherePart('PrtNo', 'PrtNo')
          + getWherePart('GrpName', 'GrpName')
          + getWherePart('CValiDate', 'CValiDate')
          + getWherePart('ManageCom', 'ManageCom','like');
//prompt('',SQL);
	turnPage.queryModal(SQL, PolGrid);
    return true;
}

/*********************************************************************
 *  执行新契约人工核保的EasyQueryAddClick
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function easyQueryAddClick()
{
	// alert("easyQueryAddClick");
	var tSel = PolGrid.getSelNo();
	var activityid = PolGrid.getRowColData(tSel - 1,6);
	var ContNo = PolGrid.getRowColData(tSel - 1,7);
	var PrtNo = PolGrid.getRowColData(tSel - 1,1);
	var MissionID = PolGrid.getRowColData(tSel - 1,8);
	var SubMissionID = PolGrid.getRowColData(tSel - 1,9);
	// alert("activityid:"+activityid);
	window.location="./LRGroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
	
}

/*********************************************************************
 *  申请核保
 *  描述:进入核保界面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyUW()
{
	if(!verifyInput2())
	return false;
	var cApplyNo = fm.ApplyNo.value;
	
	if(cApplyNo >10)
	{
		alert("每次申请最大数为10件");
		return;
	}
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	//fm.ApplyNo.value ="5";     //每次申请为5条
	fm.action = "./ManuUWAllChk.jsp";
  fm.submit(); //提交
}

/*********************************************************************
 *  提交后操作,服务器数据返回后执行的操作
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	window.focus();
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  

  if (FlagStr == "Fail" )
  {                 
    alert(content);
  }
  else
  { 
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");     
    //执行下一步操作
  }
  easyQueryClick();

}

function dealUpReport()
{
	var strSql = "select t.missionprop1,t.missionprop2,t.missionprop7,"
								+"case t.activitystatus "
		 						+"when '1' then '未人工核保'  "
		 						+"when '3' then '核保已回复' "
		 						+"when '2' then '核保未回复' end,"
		 						+"case t.activityid when '0000001100' "
		 						+"then '新契约' end,t.missionid,t.submissionid,t.activityid ,''"
		 						+"from lwmission t where 1=1 "
								+ " and t.activityid in ('0000001160')"
								+ " and t.defaultoperator ='" +operator+ "'"
								+ " order by t.modifydate asc,t.modifytime asc ";
turnPage2.queryModal(strSql, UpReportPolGrid);

}

function easyQueryAddClick1()
{

	var tSel = RePolGrid.getSelNo();
	var activityid = RePolGrid.getRowColData(tSel - 1,9);
	var ContNo = RePolGrid.getRowColData(tSel - 1,7);
	var PrtNo = RePolGrid.getRowColData(tSel - 1,1);
	var MissionID = RePolGrid.getRowColData(tSel - 1,10);
	var SubMissionID = RePolGrid.getRowColData(tSel - 1,8);

	if(activityid == "0000001160")
	{
		window.location="./UWManuInputMain.jsp?ContNo="+ContNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&PrtNo="+PrtNo;
	}
	if(activityid == "0000002004")
	{
		window.location="./GroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID;
	}
	if(activityid == "0000006004")
	{
		window.location="../askapp/AskGroupUW.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&MissionID="+MissionID+"&SubMissionID="+SubMissionID+"&ActivityID="+activityid; 
	}
}
function HistoryeasyQueryClick()
{  
    var hisSql = " select lgc.prtno,case when lgc.appflag = '1' then lgc.grpcontno else '' end,lgc.grpname,lgc.cvalidate,lgc.prem, "
    			 + "case when lru.conclusion = '0' then '同意' when lru.conclusion = '1' then '不同意' when lru.conclusion = '2' then '需核保审核' end, "
    			 + "lru.auditopinion,lru.operator,"
    			 + "varchar(lru.MakeDate) || ' '|| lru.MakeTime "
                 + " from lcgrpcont lgc "
                 + " inner join LRUWMaster lru " 
                 + " on lgc.prtno = lru.prtno "
                 + " where AuditFlag = '0' "
                 + getWherePart('lru.PrtNo', 'PrtNo')
          		 + getWherePart('lgc.GrpName', 'GrpName')
                 + getWherePart('lgc.CValiDate', 'CValiDate')
                 + getWherePart('lru.ManageCom', 'ManageCom','like')
                 + " order by varchar(lru.MakeDate) || ' '|| lru.MakeTime desc";
                 //prompt('',hisSql);
   fm.historySql.value = hisSql;
   turnPage1.queryModal(hisSql, HistoryPolGrid);
}
function easyQueryHistory()
{
		var tSel = HistoryPolGrid.getSelNo();
		var ContNo = HistoryPolGrid.getRowColData(tSel - 1,7);
		var PrtNo = HistoryPolGrid.getRowColData(tSel - 1,1);
		var Resource= HistoryPolGrid.getRowColData(tSel - 1,5);
		if(Resource=="退保"){
			Resource=1;
		}else if(Resource=="撤保"){
			Resource=2;
		}else{
			Resource=3;
		}
		//alert(Resource);
		//alert(ContNo);
		//alert(PrtNo);
		window.location="./GroupUWMain.jsp?GrpContNo="+ContNo+"&PrtNo="+PrtNo+"&Resource="+Resource+"&LoadFlag=1";
}

//下载已完成信箱的下载
function downloadHistoryList()
{
    if(HistoryPolGrid.mulLineCount == 0)
    {
      alert("没有需要下载的数据");
      return false;
    }
    //fm.target='_blank';
    fm.action = "LRManuUWHistoryDown.jsp";
    fm.submit();
}
