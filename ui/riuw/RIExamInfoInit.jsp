<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：RIExamInfoInit.jsp
//程序功能：再保审核信息
//创建日期：2011-11-15
//创建人  ：Gaoyx
//更新记录：  更新人    更新日期     更新原因/内容
%> 
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
 try{
 
          
  }
  catch(ex)
  {
    alert("在UWInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
  	
        initInpBox();
    	initGrpRIUWGrid();
    	queryGrpRIUW();
    	initGrpHisUWGrid();
    	queryGrpHisUW();
    	
  }
  catch(re)
  {
    alert("在RIExamInfoInit.jsp-->InitForm函数中发生异常:初始化界面错误!"+re.message);
  }
}

// 再保自核信息列表的初始化
function initGrpRIUWGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="险种编码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="再报审核未通过原因";         		//列名
      iArray[2][1]="240px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="记录日期";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

           
      GrpRIUWGrid = new MulLineEnter( "fm" , "GrpRIUWGrid" );      // 生成对象域
      //这些属性必须在loadMulLine前
      GrpRIUWGrid.mulLineCount = 0;   
      GrpRIUWGrid.displayTitle = 1;
      GrpRIUWGrid.locked = 1;
      GrpRIUWGrid.canSel = 0;       
      GrpRIUWGrid.hiddenPlus = 1;
      GrpRIUWGrid.hiddenSubtraction = 1;
      
      GrpRIUWGrid.loadMulLine(iArray);     
      
      }
      catch(ex)
      {
        alert(ex);
      }
}


// 再保审核历史信息列表的初始化
function initGrpHisUWGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=30;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="审核结论";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="再保审核意见";         		//列名
      iArray[2][1]="240px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="审核人";         		//列名
      iArray[3][1]="60px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="记录日期";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

           
      GrpHisUWGrid = new MulLineEnter( "fm" , "GrpHisUWGrid" );      // 生成对象域
      //这些属性必须在loadMulLine前
      GrpHisUWGrid.mulLineCount = 0;   
      GrpHisUWGrid.displayTitle = 1;
      GrpHisUWGrid.locked = 1;
      GrpHisUWGrid.canSel = 0;       
      GrpHisUWGrid.hiddenPlus = 1;
      GrpHisUWGrid.hiddenSubtraction = 1;
      
      GrpHisUWGrid.loadMulLine(iArray);     
     
      }
      catch(ex)
      {
        alert(ex);
      }
}



</script>