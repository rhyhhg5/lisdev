<A HREF=""></A> <html> 
<%
//程序名称：IndiDueFeeListQuery.jsp
//程序功能：个险应收清单查询、打印
//创建日期：2006-11-06 
//创建人  ：
//更新记录：  更新人 huxl    更新日期     更新原因/内容
%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@include file="../common/jsp/CurrentTime.jsp"%>
<html>
<%
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
  String CurrentDate= PubFun.getCurrentDate();   
  String tCurrentYear=StrTool.getVisaYear(CurrentDate);
  String tCurrentMonth=StrTool.getVisaMonth(CurrentDate);
  String tCurrentDate=StrTool.getVisaDay(CurrentDate);                	               	
  String AheadDays="30";
  FDate tD=new FDate();
  Date AfterDate = PubFun.calDate(tD.getDate(CurrentDate),Integer.parseInt(AheadDays),"D",null);
  String SubDate=tD.getString(AfterDate);  
  String tSubYear=StrTool.getVisaYear(SubDate);
  String tSubMonth=StrTool.getVisaMonth(SubDate);
  String tSubDate=StrTool.getVisaDay(SubDate);     
    	               
%>
<head >
  <SCRIPT>
var CurrentYear=<%=tCurrentYear%>;  
var CurrentMonth=<%=tCurrentMonth%>;  
var CurrentDate=<%=tCurrentDate%>;
var CurrentTime=CurrentYear+"-"+CurrentMonth+"-"+CurrentDate;
var SubYear=<%=tSubYear%>;  
var SubMonth=<%=tSubMonth%>;  
var SubDate=<%=tSubDate%>;
var SubTime=SubYear+"-"+SubMonth+"-"+SubDate;
var managecom = <%=tGI.ManageCom%>;

var contCurrentTime; //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的起始时间
var contSubTime;    //杨红于2005-07-19添加该全局变量，目的是保存 查询保单信息 的终止时间
  </SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="UliDueFeeListQuery.js"></SCRIPT>
  <SCRIPT src="ChildInsureOper.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="UliDueFeeListQueryInit.jsp"%>
 
</head>
<body onload="initForm();">
<form name=fm action=./IndiDueFeeListQuery2.jsp target=fraSubmit method=post>
  <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
  <table  class= common align=center>
	   <tr class= common align=LEFT>

		   <TD  class= title>
        抽档开始日期
        </TD>
        <TD  class= input>
          <Input class="coolDatePicker" dateFormat="short" name=StartDate >
        </TD>
        <TD  class= title>
        抽档终止日期
        </TD>
        <TD  class= input>
          <Input class="coolDatePicker" dateFormat="short" name=EndDate >
        </TD> 		  
		 </tr> 
		 <TR class= common align=LEFT>        
		  <TD  class= title> 操作人 </TD>
          <TD  class= input> <Input class=common name=Operator > </TD>

      <TD  class= title> 操作机构</TD><TD  class= input>
      <Input class= "codeno"  name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName></TD>		  
	    </tr>
	  </table>
	  <INPUT VALUE="查询批次应收纪录" class = cssbutton TYPE=button onclick="easyQueryClick('1');">  
	    	<br>
	<br>
	<table class= common >
    <TR  class= common>		
      <TD  class= title> 保单号 </TD>
      <TD  class= input> <Input class= common name=ProposalGrpContNo >  </TD>
      <TD  class= title>  客户号 </TD>
      <TD  class= input>  <Input class= common name=AppNo ></TD>
		  <TD class= title>催收状态</TD> 
		  <TD  class= input>
		   <Input class="codeNo" name="DealState" CodeData="0|^0|待收费^1|催收成功^2|应收作废^3|已撤销^4|待核销^5|全部" verify="交付费方式|&code:DealState"  ondblclick="return showCodeListEx('DealState',[this,PayModeName],[0,1]);" onkeyup="return showCodeListEx('DealState',[this,PayModeName],[0,1]);"><Input class="codeName" name="PayModeName"  elementtype="nacessary" readonly>
		 </TD>
    </TR>

   <TR class= common>  
   		 		  <TD class= title>
				产品类型
			</TD>
			<TD class= input>
				<input class="codeNo" name="queryType" id="queryType" value="1" CodeData="0|^1|万能险" readOnly
    	          ><Input class="codeName" name="queryTypeName" readonly>
			</TD>
	   <TD  class= title>
      应交开始日期
      </TD>
      <TD  class= input>
        <Input class="coolDatePicker" dateFormat="short" name=SingleStartDate >
      </TD>
      <TD  class= title>
      应交终止日期
      </TD>
      <TD  class= input>
        <Input class="coolDatePicker" dateFormat="short" name=SingleEndDate >
      </TD>

   </TR>

	   <TR class= common>  
		
    	<TD  class= title>代理人部门</TD>
    	<TD  class= input>
      	<Input class= "codeno"  name=AgentGroup  ondblclick="return showCodeList('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" ><Input class=codename  name=AgentGroupName>
      </TD> 
      <TD  class= title>代理人</TD>
      <TD  class= input>
      	<Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCodet" ondblclick="return showCodeList('agentcodetbq',[this,AgentCodeName,null,null],[0,1,3,4],null,fm.AgentGroup.value,'AgentGroup',1);" onkeyup="return showCodeListKey('agentcodetbq',[this,AgentCodeName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
      </TD>
    </TR>
	</table>
</table>
<BR>
	
	<INPUT VALUE="查询个案应收纪录" class = cssbutton TYPE=button onclick="singleQueryClick('1');">
	<!--
	<TABLE class= common align=center>
    <TR  class= common >		
    	<TD  class= title>代理人部门</TD>
    	<TD  class= input>
      	<Input class= "codeno"  name=AgentGroup  ondblclick="return showCodeList('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" onkeyup="return showCodeListKey('agentgroupbq',[this,AgentGroupName],[0,1],null,'03','BranchLevel',1);" ><Input class=codename  name=AgentGroupName>
      </TD>      
      <TD  class= title>代理人</TD>
      <TD  class= input>
      	<Input class=codeNo name=AgentCode verify="业务员代码|code:AgentCodet" ondblclick="return showCodeList('agentcodetbq',[this,AgentCodeName,null,null],[0,1,3,4],null,fm.AgentGroup.value,'AgentGroup',1);" onkeyup="return showCodeListKey('agentcodetbq',[this,AgentCodeName],[0,1,3,4]);showAllCodeName();"><input class=codename name=AgentCodeName readonly=true >
      </TD>
		  <TD class= title>催收状态</TD> 
		  <TD  class= input>
		   <Input class="codeNo" name="DealState1" CodeData="0|^0|待收费^1|催收成功^2|应收作废^3|已撤销^4|待核销^5|全部" verify="交付费方式|&code:DealState"  ondblclick="return showCodeListEx('DealState1',[this,DealName],[0,1]);" onkeyup="return showCodeListEx('DealState1',[this,DealName],[0,1]);"><Input class="codeName" name="DealName"  elementtype="nacessary" readonly>
		 </TD>			 
    </TR>
  </TABLE>
  <table class= common align=center>
   <TR class= common>  
     <TD  class= title>
        应交日期范围
      </TD>
	   <TD  class= title>
      开始日期
      </TD>
      <TD  class= input>
        <Input class="coolDatePicker" dateFormat="short" name=MultStartDate >
      </TD>
      <TD  class= title>
      终止日期
      </TD>
      <TD  class= input>
        <Input class="coolDatePicker" dateFormat="short" name=MultEndDate >
      </TD> 		  
   </TR>   				  
	  <TR class = common >
   	<TD  class= title>保单管理机构</TD>
   	<TD  class= input>
      <Input class= "codeno"  name=ManageCom2  ondblclick="return showCodeList('comcode',[this,ManageComName2],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName2],[0,1],null,null,null,1);" ><Input class=codename  name=ManageComName2>
		</TD>

		<TD class= title>
				选择类型:
		</TD>
		<TD class= input>
				<input class="codeNo" name="queryType2"  value="1" CodeData="0|^1|请选择类型^2|普通单^3|少儿单" readOnly
    	          ondblclick="return showCodeListEx('queryType2',[this,queryType2Name],[0,1]);" 
    	          onkeyup="return showCodeListEx('queryType2',[this,queryType2Name],[0,1]);"><Input class="codeName" name="queryType2Name" readonly>
		</TD>

	 </TR> 
	</table>
	
	<INPUT VALUE="查询个案应收纪录" class = cssbutton TYPE=button onclick="singleQueryClick('2');">  
	-->
	<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 应收记录：
    		</td>
			<td class= titleImg><INPUT VALUE="查询应收保单" class = cssbutton TYPE=button
			onclick="contQueryClick();">    </td>
    	</tr>
    </table>
  	<Div  id= "divJisPay" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanJisPayGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		  <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">			
		</center>  	
  	</div>          
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 保单信息
    		</td>
			<td class= titleImg><td>			
			<td class= titleImg>    <INPUT VALUE="保单明细查询" class = cssbutton style="width:100px" TYPE=button onclick="returnParent();">
            </td>
            <td class= titleImg><INPUT VALUE="打印通知书" class = hidden style="width:100px" TYPE=hidden onclick="printNotice();"> </td>
            <td class= titleImg><INPUT VALUE="打印PDF通知书" class = cssbutton style="width:100px" TYPE=hidden onclick="printInsManage();"> </td>
			<td class= titleImg><INPUT VALUE="打印PDF通知书" class = cssbutton style="width:100px" TYPE=button onclick="newprintInsManage();"> </td>
			<td class= titleImg><INPUT VALUE="批打PDF通知书" class = cssbutton style="width:100px" TYPE=hidden onclick="printInsManageBat();">  </td>	
	        <td class= titleImg><INPUT VALUE="批打PDF通知书" class = cssbutton style="width:100px" TYPE=button onclick="newprintInsManageBat();">  </td>	
	        <td class= titleImg><INPUT VALUE="打印应收清单" class = cssbutton style="width:100px" TYPE=button onclick="printList();">  </td>	
    	    <td class= titleImg><INPUT VALUE="打印应收清单-险种" class = cssbutton style="width:120px" TYPE=button onclick="printPolList();">  </td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanContGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<center>    	
		  <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage1.firstPage();"> 
		  <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 			<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
		  <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage1.lastPage();">			
		</center>  	
  	</div>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrpDue1);">
    		</td>
    		<td class= titleImg>
    			 险种信息
    		</td>
    	</tr>
  </table> 	
  <Div  id= "divGrpDue1" style= "display: ''">
    <Table  class= common>
       <TR  class= common>
         <TD text-align: left colSpan=1>
            <span id="spanPolGrid" ></span> 
  	     </TD>
       </TR>
       <input type=hidden name=strsql />
    </Table>	
    <center>      				
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
      </center>  
  </Div>
  <Div id= "divChildInsureOper" style= "display: 'none'">
    <Table  class= common id = "divButton1">
    	<TR class= common>
    		<TD class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divButton);">
    		</TD>
    		<TD class= titleImg>
    			 少儿险回销操作：
    		</TD>
    	</TR>
    </Table>
    <Div id = "divButton">
    	<Table>
       <TR  class= common id = "divButton2">

		   <TD  class= title>
        客户答复日期：
        </TD>
        <TD  class= input>
          <Input class="coolDatePicker" dateFormat="short" name=CustomerBackDate >
        </TD>

       	<TD  class= title>
        客户选择操作：
        </TD>
        <TD class="input">
          <input class="codeNo" name="dealType" value="" CodeData="0|^1|续保^2|满期终止" readOnly
    	          ondblclick="return showCodeListEx('dealType',[this,dealTypeName],[0,1]);" 
    	          onkeyup="return showCodeListEx('dealType',[this,dealTypeName],[0,1]);"><Input class="codeName" name="dealTypeName" readonly>
        </TD>
       </TR>
       <TR class="common">
        <TD class="common" colspan=6>
          <input type=Button class=cssButton id="dealButton" value="满期处理" onclick="endDateDeal();">
	        <input type=Button class=cssButton id="XBDealButton" style= "display: 'none'" value="打印续保批单" onclick="printEndDateList('XBDeal');">
          <input type=Button class=cssButton id="MJDealButton" style= "display: 'none'" value="打印满期终止结算单" onclick="printEndDateList('MJDeal');">
          <input type=Button class=cssButton id="Paymode" style= "display: 'none'" value="给付方式变更" onclick="ChangePaymode()">	
        </TD>
       </TR>
    </Table>
   </Div>
	</Div>
	<!-- 给付方式变更-->
		  <div id="PayModeChangeID" style="display: 'none'">
	    <Table  class= common>
	      <tr>
  			  <td class=titleImg colspan="10">给付信息变更：</td>
  			</tr>
      	<tr>
      		<td class= title>原给付方式</td>
      		<td class= input><Input type="hidden" name=PayModeOld><Input name=PayModeOldName style="width: 130" class=readonly readonly></td>
      		<td class= title>变更为</td>
      		<td class= input><Input class= "codeno" name=PayModeNew CodeData="0|^1|现金^2|现金支票^3|转帐支票^4|银行转帐" ondblclick="return showCodeListEx('PayMode2',[this,PayModeNewName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PayMode2',[this,PayModeNewName],[0,1],null,null,null,1);" style="width: 50"><Input class=codename name=PayModeNewName readonly style="width: 80"></td>
      	</tr>
      	<tr>
    			<td class= titleImg colspan="2">账户信息由:</td>
      	</tr>
      	<tr>
    			<td class= title>开户银行</td>
    			<td class= input><Input class=readonly name=BankCodeOld style="width: 50" readonly ><input class=readonly name=BankNameOld readonly style="width: 80"></td>
    			<td class= title>账户名</td>
    			<td class= input><Input class=readonly name=AccNameOld style="width: 130" readonly></td>
    			<td class= title>账号</td>
    			<td class= input><Input class=readonly name=BankAccNoOld style="width: 130" readonly></td>
    		</tr>
      	<tr>
    			<td class=titleImg colspan="2">变更为:</td>
    		</tr>
      	<tr>
    			<td class= title>开户银行</td>
    			<td class= input><Input class=codeNo name=BankCode style="width: 50" ondblclick="return showCodeList('Bank',[this,BankName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('Bank',[this,BankName],[0,1],null,null,null,1);"><input class=codename name=BankName readonly style="width: 80"></td>
    			<td class= title>账户名</td>
    			<td class= input><Input class=common1 name=AccName></td>
    			<td class= title>账号</td>
    			<td class= input><Input class=common1 name=BankAccNo></td>
    			<td class= title>账号确认</td>
    			<td class= input><Input class=common1 name=BankAccNo2></td>
      	</tr>
      	<tr>
    			<td class= title>
    			  <INPUT VALUE="保全确认" TYPE=button class = cssbutton onclick="changePayModeSubmit()">
          </td>
      	</tr>
      </Table>  
	  </div>
	
      <Input type=hidden name=GetNoticeNo> 
      
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  		<iframe name="printfrm" src="" width=0 height=0></iframe>
  		<form method=post id=printform target="printfrm" action="">
      		<input type=hidden name=filename value=""/>
  		</form>  
</body>
</html>
