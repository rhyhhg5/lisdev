//程序名称：ShieldBorUnevenInput.js 
//程序功能：接口表数据维护
//创建日期：2015-06-03 
//创建人  ：m
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();//日结试算相关数据
var turnPage2 = new turnPageClass();//日结确认相关数据
var mFlag = "0";
var showInfo;
var mOperate="";

/****************************************************
*导出相关数据财务接口报表分明细和期间的统计
*********************************************************/ 
function EmptyData()
{ 	 
 if(!beforeSubmit())
  {
    return false;
  }	
	if(fm.all('ReadState').value == '2'){
		alert("该读取状态下数据不允许进行任何操作！！！");
		return false;
	}
 else if(fm.ReadState.value == null||fm.ReadState.value==''){
		alert("请选择数据的读取状态，谢谢！");
		return false;
	}
    else if (confirm("您确实想置空数据吗?"))
  {
    fm.mOperate.value="UPDATEE";
    submitForm();
  }
  else
  {
    fm.mOperate.value="";
    alert("您取消了置空操作！");
  }

}

function ShieldData()
{ 	 
 if(!beforeSubmit())
  {
    return false;
  }		
if(fm.all('ReadState').value == '2'){
		alert("该读取状态下数据不允许进行任何操作！！！");
		return false;
	}
  else if(fm.ReadState.value == null||fm.ReadState.value==''){
		alert("请选择数据的读取状态，谢谢！");
		return false;
	}
   else if (confirm("您确实想屏蔽数据吗?"))
  {
    fm.mOperate.value="UPDATES";
    submitForm();
  }
  else
  {
    fm.mOperate.value="";
    alert("您取消了屏蔽操作！");
  }
}

function Query()
{ 	 
	 if(!beforeSubmit())
  {
    return false;
  }
	 var i = 0;
	 var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	 var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	 showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px"); 
	 
    initIntFaceTabDataGrid();
    var sql="";
	sql = "select SerialNo,batchno,VoucherType ,AccountCode,DCFlag,SumMoney,ChargeDate,DepCode ,RiskCode,CostCenter,ReadState, VoucherID from InterfaceTable where batchno = '"+fm.all('BatchNo').value+"'"
	       +" and ChargeDate = '"+fm.all('ChargeDate').value+"'"
	      +getWherePart('DepCode','DepCode')
	      +getWherePart('VoucherType','VoucherType')
	      +getWherePart('SerialNo','SerialNo')
	      if (fm.all('ReadState').value == '4'){
	      sql += "and ReadState is null";
	      }else {
	      sql +=getWherePart('ReadState','ReadState');
	      }

     turnPage.queryModal(sql,IntFaceTabDataGrid); 
    	 
    	 afterSubmitl();
 
    return false;
  }


function beforeSubmit()
{
	if(fm.BatchNo.value == null||fm.BatchNo.value==''){
		alert("请输入您想要维护数据的批次号，谢谢！");
		return false;
	}
	
	if(fm.ChargeDate.value == null||fm.ChargeDate.value==''){
		alert("请选择您想要维护数据的记账日期，谢谢！");
		return false;
	}
	return true;
}
/************************
*PDF打印功能实现 
***************************/
function submitForm()
{
 
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
      
	}

/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content,cOperate )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    
  }
}
function afterSubmitl( FlagStr, content )
{
  showInfo.close();
  if (!turnPage.strQueryResult )
  {        
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有查询到数据" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
  }
  else
  { 
  	if(turnPage.strQueryResult){
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + "查询成功" ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  } 
  }
}

// add 2015-06-03 
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function BankSelect() {
//       alert('oooo');
		var tSel = IntFaceTabDataGrid.getSelNo();
		try
		{
			fm.all('DepCode').value = IntFaceTabDataGrid.getRowColData(tSel-1, 8);
//			alert (fm.all('Codea').value);
			fm.VoucherType.value = IntFaceTabDataGrid.getRowColData(tSel-1,3);// 业务名称
		    fm.all('SerialNo').value= IntFaceTabDataGrid.getRowColData(tSel-1,1);// 编码
		    if (IntFaceTabDataGrid.getRowColData(tSel-1,11) == null || IntFaceTabDataGrid.getRowColData(tSel-1,11) == ""){
		    fm.all('ReadState').value = '4';
		    }else {
			fm.all('ReadState').value= IntFaceTabDataGrid.getRowColData(tSel-1,11);// 编码
			}

		}
		catch(ex)
		{
			alert( "读取数据错误,请刷新页面再使用。" + ex );
		}
		}