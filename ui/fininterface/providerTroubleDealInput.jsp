<html>
<%
//程序名称 :IntFaceTabDataInput.jsp 
//程序功能 :维护接口表数据
//创建人 :
//创建日期 :2015-06-03 
//
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。  
 %>
<script>
  var comcode = "<%=tGI1.ComCode%>";
 

</script>
<head >
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
<SCRIPT src = "providerTroubleDeal.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="providerTroubleDealInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form method=post name=fm target="fraSubmit">
<Div style= "display: ''">
<strong>供应商数据维护</strong>
  	<table>
    	<tr>
    		 <td class= titleImg>供应商为空数据维护</td>   		 
    	</tr>
    </table>

 <table class= common border=0 width=100%>
        <TR  class= common>
		<TD class=title>中介机构</TD>
		<TD class=input><Input class="common" name=agentcom type="text"></TD>
		<TD class=title>记账日期</TD>
		<TD class=input><Input class="coolDatePicker"
			verify="记账日期|notnull&date"  dateFormat="short" name=Accountdate elementtype=nacessary></TD>	
        </TR>  
        <TR  class= common>
        <TD class=title>修改后记账日期</TD>
		<TD class=input><Input class="coolDatePicker"  dateFormat="short" name=NewAccountdate></TD>	
        </TR>           
 </table> 
 <br>
 <INPUT VALUE="查询数据" class=cssButton TYPE=button onclick="DataQuery();">
 <INPUT VALUE="维护数据" class=cssButton TYPE=button onclick="DataEmpty();">
 <br><br/>
 <INPUT class=common name=mOperate TYPE=hidden >
    </TR>
<br>

	<Div  id= "divDataGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanDataGrid" >
  					</span> 
  			  	</td>
  			</tr>					
</table>	
      <Div id= "divPage" align="center" style= "display: '' ">
      <input class=cssButton value="首页"    type=button  onclick="turnPage.firstPage();"></input>
      <input class=cssButton value="上一页"  type=button  onclick="turnPage.previousPage();"></input>
      <input class=cssButton value="下一页"  type=button  onclick="turnPage.nextPage();"></input>
      <input class=cssButton value="尾页"    type=button  onclick="turnPage.lastPage();"></input>
      </Div>
      </Div>
      <hr width=98%>
 <table class= common border=0 width=100%>
		<tr>
    		 <td class= titleImg>供应商错误数据维护</td>   		 
    	</tr>
</table> 
 <table class= common border=0 width=100%>
        <TR  class= common>
		<TD class=title>批次号码</TD>
		<TD class=input><Input class="common" verify="批次号|notnull" name=batchno  elementtype=nacessary  type="text"></TD>
		<TD class=title>过账日期</TD>
		<TD class=input><Input class="coolDatePicker"
			verify="过账日期|notnull&date" dateFormat="short" name=chargedate elementtype=nacessary ></TD>	
        </TR> 
        <tr>
        <TD class=title>流水号</TD>
		<TD class=input><Input class="common" name=serialno type="text"></TD>
        <TD class=title>凭证类型</TD>
		<TD class=input><Input class="common" name=vouchertype verify="凭证类型|notnull" type="text" elementtype=nacessary ></TD>

        </tr>
        <tr>
        <TD class=title>错误供应商号码</TD>
		<TD class=input><Input class="common" name=agentno type="text"></TD>
		<TD class=title>正确供应商号码</TD>
		<TD class=input><Input class="common" name=newagentno type="text"></TD>
        </tr>
        <tr>
        <TD class=title>四位财务机构</TD>
		<TD class=input><Input class="common" name=depcode type="text"></TD>
				<TD class=title>修改后过账日期</TD>
		<TD class=input><Input class="coolDatePicker" dateFormat="short" name=Newchargedate ></TD>	
        </tr>
                <tr>
        <TD class=title>维护类型</TD>
				<TD class=input><Input class=codeno name=Style
			CodeData="0|^1|维护供应商|M^2|放开数据^3|修改记账日期并放开数据" verify="校检标记"
			ondblclick="showCodeListEx('Style',[this,StyleName],[0,1]);"
			onkeyup="showCodeListKeyEx('Style',[this,StyleName],[0,1]);"><input
			class=codename name=StyleName readonly=true elementtype=nacessary></TD>
        </tr>
        
</table>
 <br>
 <INPUT VALUE="查询数据" class=cssButton TYPE=button onclick="InterQuery();">
 <INPUT VALUE="维护数据" class=cssButton TYPE=button onclick="InterEmpty();">
 <br><br/> 
 <Div  id= "divInterfaceDataGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanInterfaceDataGrid" >
  					</span> 
  			  	</td>
  			</tr>	
  			</table>					


      <Div id= "divPage2" align="center" style= "display: '' ">
      <input class=cssButton value="首页"    type=button  onclick="turnPage2.firstPage();"></input>
      <input class=cssButton value="上一页"  type=button  onclick="turnPage2.previousPage();"></input>
      <input class=cssButton value="下一页"  type=button  onclick="turnPage2.nextPage();"></input>
      <input class=cssButton value="尾页"    type=button  onclick="turnPage2.lastPage();"></input> 
      </Div>
      </Div>     
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>