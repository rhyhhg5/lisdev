// 该文件中包含客户端需要处理的函数和事件
var showInfo;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

function initQuery() {
//	alert("111");
	if(fm1.all("Bdate").value==""||fm1.all("Edate").value==""){
		alert("请输入日期范围！");
		return false;
		}
	else{
	BatchNoGrid.clearData("BatchNoGrid");
/*  var tSQL = "select batchno,classtype,(select classtypename from liclasstypekeydef where classtype = aa.classtype),to_char(sum(dside),'FM999999999990.00'),to_char(sum(cside),'FM999999999990.00'),"+
       " (select makedate||' '||maketime from lidistilllog where batchno = aa.batchno),(select managecom from lidistilllog where batchno = aa.batchno)"+
  	   " from (select batchno as batchno, classtype as classtype,"+
       " (case finitemtype when 'C' then 0 when 'D' then sum(summoney) end) as cside,"+
       " (case finitemtype when 'D' then 0 when 'C' then sum(summoney) end) as dside"+
       " from lidatatransresult "+
       " where accountdate between '" + fm1.all("Bdate").value + "' and '" + fm1.all("Edate").value + "'" +
       " group by batchno, classtype, finitemtype) aa"+
	   " having sum(cside) <> sum(dside) group by batchno, classtype";
*/
		var tSQL = "select classtype,"+
							" (select classtypename from liclasstypekeydef where classtype = aa.classtype),(sum(dside)),(sum(cside)),managecom,accountdate "+
							" from (select classtype as classtype,"+
							" (case finitemtype when 'C' then 0 when 'D' then sum(summoney) end) as cside,"+
							" (case finitemtype when 'D' then 0 when 'C' then sum(summoney) end) as dside,managecom as managecom ,accountdate as accountdate"+
							" from lidatatransresult "+
							" where accountdate between '" + fm1.all("Bdate").value + "' and '" + fm1.all("Edate").value + "'" +
							" group by batchno, classtype, finitemtype,managecom,accountdate) aa"+
 							"  group by classtype,ManageCom,accountdate  having sum(cside) <> sum(dside)";
//  alert(strSql);
  turnPage.queryModal(tSQL, BatchNoGrid);
	if(BatchNoGrid.mulLineCount=="0"){
		alert("没有借贷不平的数据！");
		return false;
	}
	}
} 

function ClearData3(){
	DetailDataGrid.clearData("DetailDataGrid");
	}

function checkData()
{
  if(BatchNoGrid.getSelNo()) {
//  	alert("111");
//    fm2.all("mBatchNo").value = BatchNoGrid.getRowColData(BatchNoGrid.getSelNo()-1, 1);
    fm2.all("cClassType").value = BatchNoGrid.getRowColData(BatchNoGrid.getSelNo()-1, 1);
//    alert(fm.all("mBatchNo").value + "   " + fm.all("cClassType").value);
    return true;
  }
  else {
    alert("请先选择一条对帐信息！"); 
    return false;
  }
}

function submitForm(){
	if(checkData()){
     var i = 0;
     var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
//     fm.target = "_blank";
//		 alert("00");
		 ClassTypeGrid.clearData("ClassTypeGrid");
		 fm2.action="../fininterface/DataDetailCheckQuery.jsp";
//		 alert(fm2.all("mBatchNo").value);
//		 alert(fm2.all("cClassType").value);
		 try{
     fm2.submit();
    }catch(ex){
//    		alert(fm2);
    		alert(ex.message);
    	}
    }
}

function detailCheck(){
		if(checkDetailData()){
			DetailDataGrid.clearData("DetailDataGrid");
			var strSql = "select a.batchno,(select classtypename from liclasstypekeydef where classtype = b.classtype),(select classname from LIOperationDataClassDef where classid = a.classid),a." + fm2.all("cKeyID").value + "," + 
									" b.accountcode,b.finitemtype, (b.summoney),b.accountdate,b.managecom from liaboriginaldata a,lidatatransresult b " +
									" where a.serialno = b.standbystring3" +
									" and b.summoney<>0  and b.accountdate between '" + fm1.all("Bdate").value + "' and '" + fm1.all("Edate").value + "'" +
									" and a." + fm2.all("cKeyID").value + " = '" + fm2.all("cContNo").value + "' and b.classtype = '" + fm2.all("cClassType").value + "'";
  //alert(strSql);
  		turnPage2.queryModal(strSql, DetailDataGrid);
		}
}

function checkDetailData(){
  if(ClassTypeGrid.getSelNo()) {
  	fm2.all("cKeyID").value = ClassTypeGrid.getRowColData(ClassTypeGrid.getSelNo()-1, 2);
    fm2.all("cContNo").value = ClassTypeGrid.getRowColData(ClassTypeGrid.getSelNo()-1, 4);
    return true;
  }
  else {
    alert("请先选择一条信息！"); 
    return false;
  }	
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord(strRecord)
{
  turnPage1.queryModal(strRecord, ClassTypeGrid);
}
