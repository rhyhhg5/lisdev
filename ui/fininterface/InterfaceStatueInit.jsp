<% 
//程序名称：FinInterfaceCheckInit.jsp
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initInpBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("TestInterfaceNotRunInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                            

function initForm()
{
  try
  {
    initInpBox();
    initInterfaceStatueGrid();  //初始化共享工作池
    getRefresh();
  }
  catch(re)
  {
    alert("TestInterfaceNotRunInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initInterfaceStatueGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="业务类型编码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="业务类型";         		//列名
      iArray[2][1]="115px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="批次号";         		//列名
      iArray[3][1]="75px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="已提取数据量";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="当前提取日期";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[6]=new Array();
      iArray[6][0]="当前提取金额";         		//列名
      iArray[6][1]="50px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
          
      InterfaceStatueGrid = new MulLineEnter( "fm" , "InterfaceStatueGrid" ); 
      //这些属性必须在loadMulLine前
      InterfaceStatueGrid.mulLineCount = 10;   
      InterfaceStatueGrid.displayTitle = 1;
      InterfaceStatueGrid.locked = 1;
      InterfaceStatueGrid.canSel = 1;
      InterfaceStatueGrid.canChk = 0;
      //FinDayTestGrid.hiddenPlus = 1;
      //FinDayTestGrid.hiddenSubtraction = 1;        
      InterfaceStatueGrid.loadMulLine(iArray);  
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>
