<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>

<html>
<head>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

<SCRIPT src="BankDetailSet.js"></SCRIPT>

<%@include file="BankDetailSetInit.jsp"%>

</head>

<body onload="initForm();">
<form method=post name=fm target="fraSubmit">
<!-- 流水号码 --> 
	<Input type=hidden name=curManageCom>
  <table>
    	<tr> 
    		<td class= titleImg>定义银行明细</td>   		 
    	</tr>
  </table> 
  
  <table class=common>
		<TR class=common>
			<TD class=title>机构代码</TD>
			<TD class=input><Input class=codeno name=cManageCom
				ondblclick="showCodeListEx('ManageCom',[this,ManageComName],[0,1],null,null,null,1,null);"
				onkeyup="showCodeListKeyEx('ManageCom',[this,ManageComName],[0,1],null,null,null,1,null);"><input
				class=codename name=ManageComName readonly=true elementtype=nacessary></TD>				
			<TD class=title>银行代码</TD>
			<TD class=input><Input class=codeno name=cBankCode
				ondblclick="showCodeList('comtobankcw',[this,BankCodeName],[0,1],null,[fm.cManageCom.value],['comcode'],1,null);"
				onkeyup="showCodeListKey('comtobankcw',[this,BankCodeName],[0,1],null,[fm.cManageCom.value],['comcode'],1,null);"><input
				class=codename name=BankCodeName readonly=true elementtype=nacessary></TD>	
		<TD class=title>帐户类型</TD>
		<TD class=input><Input class=codeno name=cAccountType readonly=true
		  CodeData="0|^S|收入户|M^F|支出户|M" verify=帐户类型|notnull"
			ondblclick="showCodeListEx('AccountType',[this,AccountTypeName],[0,1]);"
			onkeyup="showCodeListKeyEx('AccountType',[this,AccountTypeName],[0,1]);"><input
			class=codename name=AccountTypeName readonly=true elementtype=nacessary></TD>		
	</TR>
	
	<TR>
		<TD class=common>请输入帐户明细</TD>
		<TD class=input><Input class= common name=cBankDetail></TD>
		<TD class=common></TD>
		<TD class=input></TD>
		<TD class=common></TD>
		<TD class=input></TD>
	</TR>
	</table>	
	
	<br>
	<INPUT VALUE=" 查 询 " class=cssButton TYPE=button onclick="queryData();">&nbsp;
  <INPUT VALUE=" 添 加 " class=cssButton TYPE=button onclick="submitForm();">&nbsp;
  <INPUT VALUE=" 删 除 " class=cssButton TYPE=button onclick="delData();">&nbsp;
  <INPUT VALUE=" 修 改 " class=cssButton TYPE=button onclick="updateData();">&nbsp;
	<br>
	<br>
<Div id="divAllGrid" style="display: ''" align=center>
<table class=common>
	<tr class=common>
		<td text-align: left colSpan=1><span id="spanBankDetailGrid"> </span></td>
	</tr>
</table>
<INPUT CLASS=cssbutton VALUE="首  页" TYPE=button
	onclick="turnPage.firstPage();"> <INPUT CLASS=cssbutton
	VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> <INPUT
	CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
<INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button
	onclick="turnPage.lastPage();">
</div>
	
</form>

<span id="spanCode" style="display: none; position:absolute; slategray"></span>

</body>
</html>