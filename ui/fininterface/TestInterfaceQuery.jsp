<html>
<%
//程序名称：TestInterfaceQuery.jsp
//程序功能：财务接口批次查询信息处理 
//创建日期：2006-12-08
//创建人  ：lijs
//更新记录：  更新人    更新日期     更新原因/内容
//         
%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.finfee.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
  <%@page import="com.sinosoft.lis.fininterface.uphold.*" %>
  <%@page contentType="text/html;charset=GBK" %>
  <%@page import="javax.servlet.http.HttpServletRequest"%>
<%
  
    CErrors tError = null;  
    String Content = "";
    String FlagStr = "";
    GlobalInput tG = new GlobalInput();
    tG = (GlobalInput)session.getValue("GI");
    VData tVData = new VData();

    LIDistillLogSet oQUERYLIDistillLogSet = new LIDistillLogSet();//查询结果集
    //初始化数据
    String sOperator = "query";//操作类型　添加　删除　修改　
    System.out.println(sOperator);
    String sOperateName = "Batch";//操作类型的编码()
    String sStartDate = request.getParameter("sStartDate");//起始日期
    String sEndDate = request.getParameter("sEndDate");//结束日期
    String QueryType = request.getParameter("QueryType");//查询类型:1 批处理 2 接口报表查询
    tVData.add(sOperateName);
    tVData.add(sStartDate);
    tVData.add(sEndDate);
    tVData.add(QueryType);
    System.out.println("sOperateName == " + sOperateName);
    System.out.println("QueryType == " + QueryType);
    	 
    //提交数据到后台处理
    FinInterfaceTransCodeUI tFinInterfaceTransCodeUI = new FinInterfaceTransCodeUI(); 
    if (!tFinInterfaceTransCodeUI.submitData(tVData,sOperator)){
		  System.out.println("没有查到相关的数据!");
		  Content = " 查询失败，原因是: " + tFinInterfaceTransCodeUI.mErrors.getError(0).errorMessage;
		  FlagStr = "Fail";
	  }else{   
	        tVData.clear();
	  	  //返回时按照对应的方式来存放值对象
	  	  //收费
	  	  if(sOperator.equals("query")){
	  		 tVData = tFinInterfaceTransCodeUI.getResult();
	  		 oQUERYLIDistillLogSet.set((LIDistillLogSet)tVData.getObjectByObjectName("LIDistillLogSet",0));
	  		 int recordCount = oQUERYLIDistillLogSet.size();
	  	     System.out.println("recordCount====" + recordCount);
	  		 if(recordCount>0){ 	  	  			
	  	    	  String strRecord="0|"+recordCount+"^";
	  		      strRecord = strRecord + oQUERYLIDistillLogSet.encode();  
	  		  	if(QueryType.equals("1") || QueryType.equals("2")){	  		  	
	  		  	%>
		  	        <script language="javascript">
		  	        //调用js文件中显示数据的函数 
		  	        parent.fraInterface.showRecord("<%=strRecord%>");                       
		  		    </script>  	                 
	  	<% 
		  	    	}
	  	    	} 
	  	  }else{
	  		  Content = "操作成功!";
	  		  FlagStr = "Succ";
	  	  }
    	}
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tFinInterfaceTransCodeUI.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 查询成功";
      FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  System.out.println("数据查询结束!");
%>                               
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
