<%
  //程序名称：ligdcardviewInit.jsp
  //程序功能：元数据定义
  //创建日期：2011/11/10
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">

function initForm(){
	try{
		cardGrid();
		//fm.ManageCom.value = comcode;
		//ManagecomName();
	}
	catch(re){
		alert("ligdcardviewInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function cardGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="SAP机构";
		iArray[1][1]="30px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="核心系统机构";
		iArray[2][1]="50px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		
		
		iArray[3]=new Array();
		iArray[3][0]="机构名称";
		iArray[3][1]="80px";
		iArray[3][2]=100;
		iArray[3][3]=0;



		managecomGrid = new MulLineEnter( "fm" , "managecomGrid" ); 

		managecomGrid.mulLineCount=1;
		managecomGrid.displayTitle=1;
		managecomGrid.canSel=0;
		managecomGrid.canChk=0;
		managecomGrid.hiddenPlus=1;
		managecomGrid.hiddenSubtraction=1;

		managecomGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
