 <%@ page contentType="text/html; charset=GBK" %>
 <%@page import="com.sinosoft.lis.fininterface_v3.core.gather.*"%>
 <%@page import="com.sinosoft.lis.fininterface_v3.tools.datalog.FIOperationLog"%>
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.utility.*"%>
 <%@page import="java.net.*"%>
 <%@page import="java.util.*"%>
 <%@page import="java.io.*"%>
 <%@page import="com.sinosoft.lis.f1print.provideTroubleDealUI"%>

<%
	CErrors tError = new CErrors();
	String FlagStr = "";
	String Content = "";
	String flag = "true";
	//接收信息前台信息
	String sAccountDate = request.getParameter("sAccountDate");//记账日期
	String sProvideNo = request.getParameter("sProvideNo");//错误的供应商号码
	String sManageCom = request.getParameter("sManageCom");//管理机构
	String sNewNo = request.getParameter("newno");//正确的供应商号码
	String sBatNo = request.getParameter("batno");//批次号码
	String sVouType = request.getParameter("voutype");//凭证类型
	String sSerNo = request.getParameter("serno");//流水号码
	
	System.out.println("批次号:"+sBatNo);
	
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	//导入数据
	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("AccountDate",sAccountDate);
	tTransferData.setNameAndValue("ProvideNo",sProvideNo);
	tTransferData.setNameAndValue("ManageCom",sManageCom);
	tTransferData.setNameAndValue("NewNo",sNewNo);
	tTransferData.setNameAndValue("BatchNo",sBatNo);
	tTransferData.setNameAndValue("VoucherType",sVouType);
	tTransferData.setNameAndValue("SerialNo",sSerNo);
	
	//准备向后台传输数据
	
	if(flag.equals("true"))
	{
		System.out.println("开始传输数据");
		try 
		{  
			VData tInputData = new VData();
			tInputData.add(tGI);
			tInputData.add(tTransferData);
			provideTroubleDealUI ui=new provideTroubleDealUI();

			if (!ui.submitData(tInputData, ""))
			{
		       FlagStr = "Fail";
		       Content = "修改失败失败，原因是：";
		       System.out.println("操作失败");               
			}
			else
			{
		        FlagStr = "Succ";
		        Content = "修改成功"; 
		        System.out.println("操作成功");
			}   	                     
		}
		catch(Exception ex) 
		{
		
		      FlagStr = "Fail";
		      Content="修改失败，原因是: " + ex.getMessage();   
		      System.out.println(Content);             
		}  	   
		System.out.println("修改结束"); 	  
  }
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
