<% 
//程序名称：ShieldBorUnevenInit.jsp
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initInpBox()
{
  try
  {
	//查询条件置空
	fm.all('BatchNo').value = '';
	fm.all('ContNo').value = '';
	fm.all('ManageCom').value = '';
	fm.all('VoucherType').value = '';
	fm.all('AccountDate').value = '';
	fm.all('CheckFlag').value = '';
  }
  catch(ex)
  {
    alert("ShieldBorUnevenInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                            

function initForm()
{
  try
  {
    initShieldBorUnevenGrid();  //初始化共享工作池
  }
  catch(re)
  {
    alert("ShieldBorUnevenInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initShieldBorUnevenGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="38px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";         		//列名
      iArray[1][1]="140px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="凭证类型";         		//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;             			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="科目信息";         		//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;                       //是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="借贷标志";         		//列名
      iArray[4][1]="50px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
            
      iArray[5]=new Array();
      iArray[5][0]="金额";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      
      iArray[6]=new Array();
      iArray[6][0]="记账日期";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[7]=new Array();
      iArray[7][0]="机构";         		//列名
      iArray[7][1]="65px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 
      
              
      iArray[8]=new Array();
      iArray[8][0]="险种";         		//列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
      
      iArray[9]=new Array();
      iArray[9][0]="成本中心";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="保单号码";         		//列名
      iArray[10][1]="120px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许 

      iArray[11]=new Array();
      iArray[11][0]="校验标记";         		//列名
      iArray[11][1]="120px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
    
         
         
          
      ShieldBorUnevenGrid = new MulLineEnter( "fm" , "ShieldBorUnevenGrid" ); 
      //这些属性必须在loadMulLine前
      ShieldBorUnevenGrid.mulLineCount = 5;   
      ShieldBorUnevenGrid.displayTitle = 1;
      ShieldBorUnevenGrid.locked = 1;
      ShieldBorUnevenGrid.canSel = 1;
      //ShieldBorUnevenGrid.canChk = 0;
      ShieldBorUnevenGrid.hiddenPlus = 1;
      ShieldBorUnevenGrid.hiddenSubtraction = 1;        
      ShieldBorUnevenGrid.loadMulLine(iArray); 
      ShieldBorUnevenGrid.selBoxEventFuncName = "BankSelect";  
	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>
