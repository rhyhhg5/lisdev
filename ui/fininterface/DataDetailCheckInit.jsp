<%
//程序名称：DataDetailCheckInit.jsp
//程序功能：
//创建日期：2007-10-29
//创建人  ：linyun
//更新记录：  更新人    更新日期     更新原因/内容
%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initForm() {
  try {	
  	initBatchNoGrid();  
  	initClassTypeGrid();
    initDetailDataGrid();
  }
  catch(re) {
    alert("InitForm 函数中发生异常:初始化界面错误!");
  }
}

// 领取项信息列表的初始化
var BatchNoGrid;
function initBatchNoGrid() {                               
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            	//列宽
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="凭证编号";      	   		//列名
    iArray[1][1]="45px";            			//列宽
    iArray[1][2]=10;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="凭证类型";      	   		//列名
    iArray[2][1]="60px";            			//列宽
    iArray[2][2]=10;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[3]=new Array();
    iArray[3][0]="借方金额";      	   		//列名
    iArray[3][1]="70px";            			//列宽
    iArray[3][2]=10;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="贷方金额";      	   		//列名
    iArray[4][1]="50px";            			//列宽
    iArray[4][2]=10;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

 	iArray[5]=new Array();
    iArray[5][0]="管理机构";      	   		//列名
    iArray[5][1]="40px";            			//列宽
    iArray[5][2]=10;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="账务日期";      	   		//列名
    iArray[6][1]="40px";            			//列宽
    iArray[6][2]=10;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    
    BatchNoGrid = new MulLineEnter( "fm1" , "BatchNoGrid" ); 
    //这些属性必须在loadMulLine前
    BatchNoGrid.mulLineCount = 0;   
    BatchNoGrid.displayTitle = 1;
    BatchNoGrid.hiddenPlus = 1;
    BatchNoGrid.hiddenSubtraction = 1;
    BatchNoGrid.canSel = 1;
    BatchNoGrid.canChk = 0;
    BatchNoGrid.selBoxEventFuncName = "ClearData3";
    BatchNoGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("initBatchNoGrid:" + ex);
  }
  }
  
function initClassTypeGrid() {                               
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            	//列宽
    iArray[0][2]=10;
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="凭证类型";         		//列名
    iArray[1][1]="65px";            	//列宽
    iArray[1][2]=10;
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="业务号码类型";      	   		//列名
    iArray[2][1]="0px";            			//列宽
    iArray[2][2]=10;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="业务号码类型";      	   		//列名
    iArray[3][1]="40px";            			//列宽
    iArray[3][2]=10;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="业务号码";      	   		//列名
    iArray[4][1]="65px";            			//列宽
    iArray[4][2]=10;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="借方金额";      	   		//列名
    iArray[5][1]="60px";            			//列宽
    iArray[5][2]=10;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="贷方金额";      	   		//列名
    iArray[6][1]="60px";            			//列宽
    iArray[6][2]=10;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="业务日期";      	   		//列名
    iArray[7][1]="40px";            			//列宽
    iArray[7][2]=10;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[8]=new Array();
    iArray[8][0]="所属机构";      	   		//列名
    iArray[8][1]="45px";            			//列宽
    iArray[8][2]=10;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    ClassTypeGrid = new MulLineEnter( "fm2" , "ClassTypeGrid" ); 
    //这些属性必须在loadMulLine前
    ClassTypeGrid.mulLineCount = 0;   
    ClassTypeGrid.displayTitle = 1;
    ClassTypeGrid.hiddenPlus = 1;
    ClassTypeGrid.hiddenSubtraction = 1;
    ClassTypeGrid.canSel = 1;
    ClassTypeGrid.canChk = 0;
    //BatchNoGrid.selBoxEventFuncName = "showStatistics";
    ClassTypeGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("initClassTypeGrid:" + ex);
  }
}
 
function initDetailDataGrid() {
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            	//列宽
    iArray[0][2]=10;
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="批次号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[1][1]="90px";            	//列宽
    iArray[1][2]=10;
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="凭证类型";         		//列名
    iArray[2][1]="40px";            	//列宽
    iArray[2][2]=10;
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="业务号码类型";      	   		//列名
    iArray[3][1]="120px";            			//列宽
    iArray[3][2]=10;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="业务号码";      	   		//列名
    iArray[4][1]="65px";            			//列宽
    iArray[4][2]=10;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[5]=new Array();
    iArray[5][0]="科目代码";      	   		//列名
    iArray[5][1]="40px";            			//列宽
    iArray[5][2]=10;            			//列最大值
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[6]=new Array();
    iArray[6][0]="借贷标志";      	   		//列名
    iArray[6][1]="40px";            			//列宽
    iArray[6][2]=10;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[7]=new Array();
    iArray[7][0]="金额";      	   		//列名
    iArray[7][1]="60px";            			//列宽
    iArray[7][2]=10;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="业务日期";      	   		//列名
    iArray[8][1]="40px";            			//列宽
    iArray[8][2]=10;            			//列最大值
    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[9]=new Array();
    iArray[9][0]="机构";      	   		//列名
    iArray[9][1]="45px";            			//列宽
    iArray[9][2]=10;            			//列最大值
    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    DetailDataGrid = new MulLineEnter( "fm3" , "DetailDataGrid" ); 
    //这些属性必须在loadMulLine前
    DetailDataGrid.mulLineCount = 0;   
    DetailDataGrid.displayTitle = 1;
    DetailDataGrid.hiddenPlus = 1;
    DetailDataGrid.hiddenSubtraction = 1;
    DetailDataGrid.canSel = 1;
    DetailDataGrid.canChk = 0;
    //BatchNoGrid.selBoxEventFuncName = "showStatistics";
    DetailDataGrid.loadMulLine(iArray);  
  }
  catch(ex) {
    alert("DetailDataGrid:" + ex);
  }
}
 
</script>

	
