<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI"); 
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initForm() {
  try {	
        fm.curManageCom.value = <%=tGI.ManageCom%>;
    	fm.ManageComName.value = "";
		fm.BankCodeName.value = "";
		fm.BankName.value = "";
		fm.FinFlag.value = "";
	 	fm.BankAccNo.value = "";
 		fm.FinFlag.value = "";
 		fm.Operater.value = "";
	 	fm.Code.value = "";
    	fm.CodeName.value = "";
//		alert("ok");
	//	initCodeQuery();
		initBankDetailGrid();
		
  }
  catch(re) {
    alert("InitForm 函数中发生异常:初始化界面错误!");
  }
}

 
function initBankDetailGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="管理机构";         		//列名
      iArray[1][1]="58px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="银行编码";         		//列名
      iArray[2][1]="48px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="银行名称";         		//列名
      iArray[3][1]="98px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="银行帐号";         		//列名
      iArray[4][1]="128px";            		//列宽
      iArray[4][2]=130;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="收付标记";         		//列名
      iArray[5][1]="48px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许 


      iArray[6]=new Array();
      iArray[6][0]="银保标记";         		//列名
      iArray[6][1]="48px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      
      iArray[7]=new Array();
      iArray[7][0]="科目代码";         		//列名
      iArray[7][1]="70px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      
      
      iArray[8]=new Array();
      iArray[8][0]="科目代码名称";         		//列名
      iArray[8][1]="226px";            		//列宽
      iArray[8][2]=250;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许     

      iArray[9]=new Array();
      iArray[9][0]="税优转移收费账户标识";         		//列名
      iArray[9][1]="70px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许  


      iArray[10]=new Array();
      iArray[10][0]="账户户名";         		//列名
      iArray[10][1]="226px";            		//列宽
      iArray[10][2]=250;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许          
      
 /**/
            
    BankDetailGrid = new MulLineEnter( "fm" , "BankDetailGrid" ); 
      
    BankDetailGrid.mulLineCount = 0;   
    BankDetailGrid.displayTitle = 1;
    BankDetailGrid.hiddenPlus = 1;
    BankDetailGrid.hiddenSubtraction = 1;
    BankDetailGrid.canSel = 1;
    BankDetailGrid.canChk = 0;
    BankDetailGrid.selBoxEventFuncName = "BankSelect"; 
    BankDetailGrid.loadMulLine(iArray);  

	}
	catch(ex)
	{
		alert(ex);
	}
}
</script>