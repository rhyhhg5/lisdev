var turnPage = new turnPageClass();
var mFlag = "0";
var showInfo;

function beforeSubmit(){
	if(fm.cBankCode.value==""){
		alert("请填写银行代码！");
		return false;
	}else if(fm.cManageCom.value==""){
		alert("请填写机构代码！");
		return false;
	}else if(fm.cAccountType.value==""){
		alert("请填写帐户类型！");
		return false;
	}else{
		return true;
	}
}

function submitForm(){
	beforeSubmit();
  if(fm.cBankDetail.value==""){
  	alert("请填写帐户明细信息！");
		return false;	
	}
	else{
     var i = 0;
     var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
//		 fm.target="_blank";
//		 	alert("222");
		 fm.action="../fininterface/BankDetailSetSave.jsp?dealType=add";
		 fm.submit();
  }
}

function updateData(){
	beforeSubmit();
	if(fm.cBankDetail.value==""){
  	alert("请填写帐户明细信息！");
		return false;	
	}else{
     var i = 0;
     var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
//		 fm.target="_blank";
//		 	alert("222");
		 fm.action="../fininterface/BankDetailSetSave.jsp?dealType=update";
		 fm.submit();
    }
}

function delData(){
	if(beforeSubmit()){
     var i = 0;
     var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
//		 fm.target="_blank";
//		 	alert("222");
		 fm.action="../fininterface/BankDetailSetSave.jsp?dealType=del";
		 fm.submit();
	}
}

function queryData(){
	if(fm.cManageCom.value==""){
		alert("请至少输入机构代码！");
		return false;
	}
	var sCom = "";

	if((trim(fm.cManageCom.value)).length==4){
		sCom = trim(fm.cManageCom.value) + "01";
	}
	else{
	  sCom = trim(fm.cManageCom.value);
	}

	var qSQL = "select code1,code,case codetype when 'bankdetailS' then '收入户'  when 'bankdetailF' then '支出户' end as accounttype,codename from ldcode1 where 1=1";
	if(fm.cAccountType.value=="S"){
		qSQL = qSQL + " and codetype = 'bankdetailS'";
	}
	else if(fm.cAccountType.value=="F"){
		qSQL = qSQL + " and codetype = 'bankdetailF'";
	}
	else if(fm.cAccountType.value=="" || fm.cAccountType.value == null){
		qSQL = qSQL + " and codetype like 'bankdetail%%'";
	}
	if(fm.cBankCode.value!=""){
		qSQL = qSQL + " and code = '" + trim(fm.cBankCode.value) + "'";
	}
	if(fm.cManageCom.value!=""){
		qSQL = qSQL + " and code1 like '" + sCom + "%%'";
	}
	qSQL = qSQL + " order by code1,code"
//	alert(qSQL);
//var	cArray = decodeEasyQueryResult(qSQL);
	turnPage.queryModal(qSQL,BankDetailGrid);
	if(BankDetailGrid.mulLineCount=="0"){
		alert("没有 查询到数据！");
		return false;
	}
}

function afterCodeSelect(cCodeName, Field) 
{
	if(cCodeName == "cManageCom") 
	{
		if(fm.cManageCom.value!=""){
			var strQueryResultB = easyQueryVer3("select distinct bankcode,max(bankname) from ldbank where comcode like '" + fm.cManageCom.value + "%%' group by bankcode order by bankcode",1,1,1);
			var	tArrayB = decodeEasyQueryResult(strQueryResultB);
			var tDS = "0|^";
			for(i=0;i<tArrayB.length;i++){
				tDS =tDS + tArrayB[i][0] + "|" + tArrayB[i][1] + "|M";
				if((i+1)!=tArrayB.length){
					tDS = tDS + "^";
				}
			}
			alert(tDS);
			fm.all("cBankCode").CodeData = tDS;
		}
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}