<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
 
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<title>添加银行信息</title>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="AddBankInfo.js"></SCRIPT>
		<%@include file="AddBankInfoInit.jsp"%>
	</head>
	<body onload="initForm();initElementtype();">
		<div>
			<strong>定义银行明细</strong>
		</div>

		<form method="post" name="fm" target="fraSubmit" action="AddBankInfoSave.jsp">
			<Input type="hidden" name="curManageCom" />
			<INPUT type= "hidden" name= "hManageCom" value=""/>
       		<INPUT type= "hidden" name= "hBankCode" value=""/>
       		<input type="hidden" name="originalLDFinBankSchema" value=""/>
     		<input type="hidden" name="originalLICodeTransSchema" value=""/>
			<input type="hidden" name="originalLIDetailFinItemCodeSchema" value=""/>
			<input type="hidden" name="YBT" value="YBT"/>			
			
			<table class="common">
				<tr class="common">
					<td class="title">
						管理机构
					</td>
					<td class="input">
						<Input name=GlobalManageCom type="hidden">
						<Input class="codeNo" name="ManageCom"
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,' comcode <> char(86000000) and char(length(trim(comcode))) ',1);" verify="管理机构|notnull"
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,' comcode <> char(86000000) and char(length(trim(comcode))) ',1);" 
							/><input class="codename" name="ManageComName" readonly="true"
							elementtype="nacessary" />
					</td>

					<td class="title">
						银行编码
					</td>
					<td class="input">
						<Input name="BankCode" value="" maxlength="10" class="codeNo"
							readonly="true" verify="银行编码|notnull"
							ondblclick="return showCodeList('banknum', [this,BankCodeName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('banknum', [this,BankCodeName],[0,1]);"
							verify="code:banknum" /><input class="codename" name="BankCodeName" readonly="false"
							elementtype="nacessary" />
						<!-- 
			        <input name="BankCode" maxlength="10" class="codeno" ondblclick="return showCodeList('banknum',[this,BankCodeName],[0,1]);" 
			        onkeyup="return showCodeListKey('banknum',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:banknum" 
			        /><input class="codename" name="BankCodeName" readonly="true" elementtype="nacessary"/>
			    -->
					</td>

					<td class="title">
						银行名称
					</td>
					<td class="input">
						<Input class="common" type="text" name="BankName" elementtype="nacessary" verify="银行名称|notnull"/>
					</td>
				</tr>

				<tr>
					<td class="title">
						收付标记
					</td>
					<td class="input">
						<Input style='width=26' class='code' name="FinFlag_sp"
							CodeData='0|^S|收帐号^F|支帐号'
							onclick="return showCodeListEx('FinFlag_sp',[this,FinFlag],[0,1],null,null,null,1,75);" 
							/><Input style='width=134' class='common' name="FinFlag"
							elementtype=nacessary verify="收付标记|notnull"/>
					</td>

					<td class="title">
						银行帐号
					</td>
					<td class="input">
						<Input class="common" type="text" name="BankAccNo"
							elementtype="nacessary" verify="银行帐号|notnull"/>
					</td>

					<td class="title">
						银保通标记
					</td>
					<td class="input">
						<Input style='width=26' class='code' name="Operater_sp"
							CodeData='0|^Y|银保通^N|非银保通'
							onclick="return showCodeListEx('Operater_sp',[this,Operater],[0,1],null,null,null,1,75);" 
							/><Input style='width=134' class='common' name="Operater"/>
					</td>
				</tr>


				<tr>
					<td class="title">
						科目代码
					</td>
					<td class="input">
						<Input elementtype="nacessary" class="common" type="text" name="Code" verify="科目代码|notnull"/>
					</td>

					<td class="title">
						科目代码名称
					</td>
					<td class="input">
						<Input elementtype="nacessary" class="common" type="text" name="CodeName" verify="科目代码名称|notnull"/>
					</td>

					<td class="title"></td>
					<td class="input"></td>
				</tr>
				
				<tr>
					<td class="title">
						税优转移收费账户标识
					</td>
					<td class="input">
						<Input style='width=26' class='code' name="RuFlag"
							CodeData='0|^1|是^0|否'
							ondblclick="showCodeListEx('RuFlag',[this,RuFlagName],[0,1]);"
			                onkeyup="showCodeListKeyEx('RuFlag',[this,RuFlagName],[0,1]);" 
							/><Input style='width=134' class='common' name="RuFlagName" verify="税优转移收费账户标识"/>
					</td>

					<td class="title">
						账户户名
					</td>
					<td class="input">
						<Input class="common" type="text" name="RuZhName" verify="账户户名"/>
					</td>
				</tr>

			</table>
			<p>
			<div>
				<INPUT VALUE=" 查 询 " class="cssButton" TYPE="button"
					onclick="queryData();" />
				&nbsp;&nbsp;
				<INPUT VALUE=" 添 加 " class="cssButton" TYPE="button"
					onclick="insertData();" />
				&nbsp;&nbsp;
				<INPUT VALUE=" 删 除 " class="cssButton" TYPE="button"
					onclick="delData();" />
				&nbsp;&nbsp;
				<!-- 
				<INPUT VALUE=" 修 改 " class="cssButton" TYPE="button"
					onclick="updateData();" />
				 -->					
				&nbsp;&nbsp;
 
			</div>
			</p>


			<div>
				<span id="spanBankDetailGrid"> </span>
				<span id="spanCode"
					style="display: none; position:absolute; slategray"></span>
				<Div id="divPage" align=center style="display: 'none' ">
					<input VALUE="首  页" class="cssbutton" TYPE="button"
						onclick="turnPage.firstPage();"/>
					<input VALUE="上一页" class="cssbutton" TYPE="button"
						onclick="turnPage.previousPage();"/>
					<input VALUE="下一页" class="cssbutton" TYPE="button"
						onclick="turnPage.nextPage();"/>
					<input VALUE="尾  页" class="cssbutton" TYPE="button"
						onclick="turnPage.lastPage();"/>
				</div>
			</div>

		</form>
	</body>
</html>
