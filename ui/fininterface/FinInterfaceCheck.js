//程序名称：FinInterfaceCheck.js
//程序功能：凭证核对
//创建日期：2007-10-23 
//创建人  ：m
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();//日结试算相关数据
var turnPage2 = new turnPageClass();//日结确认相关数据
var mFlag = "0";
var showInfo;

/****************************************************
*导出相关数据财务接口报表分明细和期间的统计
*********************************************************/ 
function ToExcel()
{
	if(CheckQueryDataGrid.mulLineCount=="0"){
		alert("没有查询到数据");
		return false;
	} 	 	
	fm.action="./FinInterfaceCheckExcel.jsp";
	fm.target="fraSubmit";
	fm.submit(); //提交
}
/************************
*PDF打印功能实现 
***************************/
function printFinInterface(){
	
	if(CheckQueryDataGrid.getSelNo()){	  	
		    var i = 0;
			var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		 	fm.action = "./TestInterfacePrintPDF.jsp?operator=printPDF";//测试PDF打印相关数据
		 	fm.target=".";	 
		 	fm.all("serialNo").value = CheckQueryDataGrid.getRowColData(CheckQueryDataGrid.getSelNo()-1, 1);
		 	fm.submit();
		 	showInfo.close();  
      }else{
        alert("请选择一条记录!");
      }

}
/********************
*对页面的条件限制
************************/
function beforeSubmit(){
	if(fm.checkType.value==''||fm.checkType.value==null){
		alert("请选择核对类型");
		fm.checkType.focus();
		return false;
	}
	var myDate = new Date();
	var hours = myDate.getHours();   	
   if(fm.checkType.value=="1"){
	  if(fm.accountCode.value==''||fm.accountCode.value==null){
	  			alert("请选择科目类型及科目");
	  			fm.accountCodeType.focus();
	  			return false;
	  	}	  	
	  if(fm.FinItemType.value==''||fm.FinItemType.value==null){
	  			alert("请选择借贷标志");
	  			fm.FinItemType.focus();
	  			return false;
	  	}
	  if(fm.ManageCom.value==''||fm.ManageCom.value==null){
	  			alert("请选择管理机构");
	  			fm.ManageCom.focus();
	  			return false;
	  	}		  	  	
	  if(fm.StartDay.value==''||fm.StartDay.value==null){
	  			alert("请选择起始日期");
	  			fm.StartDay.focus();
	  			return false;
	  	}
	  if(fm.EndDay.value==''||fm.EndDay.value==null){
	  			alert("请选择终止日期");
	  			fm.EndDay.focus();
	  			return false;
	  	}
	  if(fm.EndDay.value<fm.StartDay.value){
					alert("起始时间大于终止时间,请重新输入!");
					fm.EndDay.focus();
					return false;
	　    }	
		//alert(hours);
		if(hours>="19"||hours<"5")
		{
		  if(fm.EndDay.value>fm.StartDay.value){
		 		var days = parseInt((new Date(fm.EndDay.value.replace(/-/g, "/")).getTime() - new Date(fm.StartDay.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
		 				//alert(days);
		 		if(days>368) {
					alert("起始时间与结束时间相差不能大于一年,请重新输入按年统计，非常感谢!");
					fm.EndDay.focus();
					return false;
				}
		　    }	  		
		} else {
			if(fm.EndDay.value>fm.StartDay.value){
		 		var days = parseInt((new Date(fm.EndDay.value.replace(/-/g, "/")).getTime() - new Date(fm.StartDay.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
		 				//alert(days);
		 		if(days>31) {
					alert("起始时间与结束时间相差不能大于一个月,请重新输入按月统计，如需跨月查询，请在晚7点至早5点，非常感谢!");
					fm.EndDay.focus();
					return false;
				}
		　    }
		}		  		  	
	 }
   else if(fm.checkType.value=="2"){
 	  if(fm.ContType.value==''||fm.ContType.value==null){
	  			alert("请输入业务号码类型！");
	  			fm.ContType.focus();
	  			return false;
	  	}    
	  if(fm.tNo.value==''||fm.tNo.value==null){
	  			alert("请输入业务号码！");
	  			fm.tNo.focus();
	  			return false;
	  	}
	  if(fm.StartDate.value==''||fm.StartDate.value==null){
			alert("请输入起始日期！");
			fm.StartDate.focus();
			return false;
	    }
      if(fm.EndDate.value==''||fm.EndDate.value==null){
			alert("请输入结束日期！");
			fm.EndDate.focus();
			return false;
	    }
	 if(fm.EndDate.value<fm.StartDate.value){
				alert("起始时间大于终止时间,请重新输入!");
				fm.EndDate.focus();
				return false;
        }
	 }	  
   else if(fm.checkType.value=="3"){
 	  if(fm.SClassType.value==''||fm.SClassType.value==null){
	  			alert("请输入凭证类型！");
	  			fm.SClassType.focus();
	  			return false;
	  	}    
	  if(fm.SDate.value==''||fm.SDate.value==null){
	  			alert("请输入起始日期！");
	  			fm.SDate.focus();
	  			return false;
	  	}
	  if(fm.EDate.value==''||fm.EDate.value==null){
	  			alert("请输入结束日期！");
	  			fm.EDate.focus();
	  			return false;
	  	}
	  	 if(fm.EDate.value<fm.SDate.value){
					alert("起始时间大于终止时间,请重新输入!");
					fm.EDate.focus();
					return false;
	　    }
	  	 
		if(hours>="19"||hours<"5")
			{
			  if(fm.EDate.value>fm.SDate.value){
			 		var days = parseInt((new Date(fm.EDate.value.replace(/-/g, "/")).getTime() - new Date(fm.SDate.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
			 				//alert(days);
			 		if(days>368) {
						alert("起始时间与结束时间相差不能大于一年,请重新输入按年统计，非常感谢!");
						fm.EDate.focus();
						return false;
					}
			　    }	  		
			} else {
				if(fm.EDate.value>fm.SDate.value){
			 		var days = parseInt((new Date(fm.EDate.value.replace(/-/g, "/")).getTime() - new Date(fm.SDate.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
			 				//alert(days);
			 		if(days>31) {
						alert("起始时间与结束时间相差不能大于一个月,请重新输入按月统计，如需跨月查询，请在晚7点至早5点，非常感谢!");
						fm.EDate.focus();
						return false;
					}
			　    }
			}	
	 }	
	return true;
}

/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}
// add 2006-9-30 11:49
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function CheckQueryData()
{
  try
  {
	if(beforeSubmit()){
	
		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   			 	
	
	CheckQueryDataGrid.clearData("CheckQueryDataGrid");   
	var strSQL = "";


			var strQueryResultC = easyQueryVer3("select classtype,keyid from liclasstypekeydef order by classtype",1,1,1);
			var	tArrayC = decodeEasyQueryResult(strQueryResultC);

    if(fm.checkType.value=="2"){//核对类型^1|科目 ^2|业务代码
/*       	if(fm.ContType.value=="1"){//个单
       		if(fm.ContNo.value!="" && fm.ContNo.value!=null)
       		{
       			strSQL= strSQL +" and a.ContNo='" + fm.ContNo.value + "'";
       		}
       		else if(fm.PrtNo.value!="" && fm.PrtNo.value!=null)
       		{
       			strSQL= strSQL +" and a.ContNo=(select contno from lccont where ContType='1' and prtno='" + fm.PrtNo.value + "')";      		
       		}
       }
       	if(fm.ContType.value=="2"){//团单
       		if(fm.ContNo.value!="" && fm.ContNo.value!=null)
       		{
       			strSQL= strSQL +" and a.ContNo='" + fm.ContNo.value + "'";
       		}
       		else if(fm.PrtNo.value!="" && fm.PrtNo.value!=null)
       		{
				strSQL= strSQL +" and a.ContNo=(select distinct GrpContNo from lccont where ContType='2' and prtno='" + fm.PrtNo.value + "')";      		
       		}
      }
*/		
			
		var tNoType = "";
		if(fm.ContType.value=="1"||fm.ContType.value=="3")
			tNoType = "a.contno";
		else if(fm.ContType.value=="2"||fm.ContType.value=="4")
			tNoType = "a.grpcontno";
		else if(fm.ContType.value=="5")
			tNoType = "a.tempfeeno";
		else if(fm.ContType.value=="6")
			tNoType = "a.actugetno";
		else if(fm.ContType.value=="7")
			tNoType = "a.payno";
		else if(fm.ContType.value=="8")
			tNoType = "a.edoracceptno";
		else if(fm.ContType.value=="9")
			tNoType = "a.endorsementno";
		else if(fm.ContType.value=="10")
			tNoType = "a.caseno";
			
//		strSQL = "select * from (select a.batchno as batchno,"
		strSQL = "select a.batchno as batchno,"
	        +"a.managecom as managecom,"
	        +"b.finitemtype as listflag,"
	        +"b.accountcode||'-'||(select codename from ldcode where trim(code) = trim(b.accountcode) and codetype = 'accountcode') as finiteminfo,"
	        +"(select classname from lioperationdataclassdef where classid = a.classid  union select businessname from FIBnTypeDef where businessid = a.classid) as classinfo,"
	        +"b.riskcode as riskcode,"
	        +"b.salechnl as salechnl,"
	        +"b.accountdate as accountdate,"
	        //+"b.contno as contno,"
	        //+"(select (case conttype when '1' then contno else grpcontno end) from lccont where contno=b.contno or grpcontno=b.contno or prtno=b.contno union select (case conttype when '1' then contno else grpcontno end) from lbcont where contno=b.contno or grpcontno=b.contno or prtno=b.contno fetch first 1 rows only) as contno,"
	        +"LF_getContNoForCheck(b.classtype,b.contno) as contno,"
	        +"b.bankaccno as bankdetail,"
	        +"b.budget as budget,"
	        +"b.costcenter as costcenter,"
	       // +"to_char(b.summoney,'FM999999999990.00') as money,"
	        +" (b.summoney) as money,"
	        +"b.classtype as classtype,"
	        +"(select keyname from liclasstypekeydef where classtype = b.classtype) as keyvaluename,"
	        +"case b.classtype ";
/*	        +"case b.classtype when 'B-01' then a.tempfeeno when 'B-02' then a.endorsementno when 'B-03' then a.edoracceptno "
					+"when 'B-04' then a.actugetno when 'C-01' then a.actugetno when 'C-02' then a.actugetno when 'N-01' then a.tempfeeno "
					+"when 'N-02' then a.payno when 'N-03' then a.actugetno when 'N-04' then a.grpcontno when 'N-05' then a.tempfeeno "
					+"when 'X-01' then a.tempfeeno when 'X-02' then a.tempfeeno when 'X-03' then a.contno when 'X-04' then a.actugetno when 'X-05' then a.payno "
					+"when 'T-01' then a.endorsementno when 'C-03' then a.caseno end as orzno"*/
		for(i=0;i<tArrayC.length;i++){
				strSQL = strSQL + "when '" + tArrayC[i][0] + "' then a." + tArrayC[i][1] + " ";
			}
					
	  strSQL = strSQL + "end as orzno,(select voucherid from interfacetable where importtype=b.classtype and batchno=b.batchno and depcode=b.managecom and accountcode=b.accountcode and chargedate=b.accountdate  fetch first 1 rows only),"
			       	+" (case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select grpname from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select grpname from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select appntname from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select appntname from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select appntname from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select appntname from lccont where contno=a.contno fetch first 1 rows only) "
					+" 	end ) appntname, "
					+" 	(case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select cvalidate from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select cvalidate from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select cvalidate from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select cvalidate from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select cvalidate from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select cvalidate from lccont where contno=a.contno fetch first 1 rows only) "
					+" 	end ) cvalidate, "
					+" 	(case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select cinvalidate from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select cinvalidate from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select cinvalidate from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select cinvalidate from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select cinvalidate from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select cinvalidate from lccont where contno=a.contno fetch first 1 rows only) "
					+" 	end ) cinvalidate "
	       
	       
//	        +"(select appntname from lcpol where grpcontno=b.contno or contno=b.contno or prtno=b.contno union select appntname from lbpol where grpcontno=b.contno or contno=b.contno or prtno=b.contno fetch first 1 rows only),"
//	        +"(select cvalidate from lccont where grpcontno=b.contno or contno=b.contno or prtno=b.contno union select cvalidate from lbcont where grpcontno=b.contno or contno=b.contno or prtno=b.contno fetch first 1 rows only),"
//	        +"(select cinvalidate from lccont where grpcontno=b.contno or contno=b.contno or prtno=b.contno union select cinvalidate from lbcont where grpcontno=b.contno or contno=b.contno or prtno=b.contno fetch first 1 rows only)"
	        +" from liaboriginaldata a,lidatatransresult b"
	        +" where a.serialno = b.standbystring3 and b.summoney<>0 "
	        +" and b.accountdate >= '" +fm.StartDate.value + "'"
       		+" and b.accountdate <= '" +fm.EndDate.value + "'"
	        + getWherePart('a.ManageCom','ManCom' )	   

	        //+" and "+ tNoType + " = '" + trim(fm.tNo.value) + "'"
		if(fm.ContType.value=="8"){
			strSQL = strSQL + " and a.endorsementno = '" + trim(fm.tNo.value) + "' and a.listflag='1' ";
			}
		else if(fm.ContType.value=="9"){
			strSQL = strSQL + "and (a.endorsementno = '" + trim(fm.tNo.value) + "' or a.endorsementno in (select trim(edorappno) from lpgrpedormain where edorno = '" + trim(fm.tNo.value) + "') ) and a.listflag='2'  ";
			}
		else if(fm.ContType.value=="1"){
			strSQL = strSQL +" and (" + tNoType + " = '" + trim(fm.tNo.value) + "'  )"; 
			}
		else if(fm.ContType.value=="2"){
			strSQL = strSQL +" and (" + tNoType + " = '" + trim(fm.tNo.value) + "'  )"; 
			}
	  else if(fm.ContType.value=="3"){
			strSQL = strSQL +" and (" + tNoType + " = '" + trim(fm.tNo.value) + "'  )" ;
			}
	  else if(fm.ContType.value=="4"){
			strSQL = strSQL +" and  (" + tNoType + " = '" + trim(fm.tNo.value) + "'  )" ;			 
			}
		else{
			strSQL = strSQL + " and "+ tNoType +" = '" + trim(fm.tNo.value) + "'";
		}
			strSQL = strSQL +" with ur" ;
//			strSQL = strSQL +")  as tab1 order by accountdate,orzno with ur" ;

    }
    else if(fm.checkType.value=="1"){//核对类型^1|科目 ^2|业务代码
    var ttNoType = "";
		if(fm.PayType.value=="1"||fm.PayType.value=="3")
			ttNoType = "b.contno";
		else if(fm.PayType.value=="2"||fm.PayType.value=="4")
			ttNoType = "b.grpcontno";
		else if(fm.PayType.value=="5")
			ttNoType = "b.tempfeeno";
		else if(fm.PayType.value=="6")
			ttNoType = "b.actugetno";
		else if(fm.PayType.value=="7")
			ttNoType = "b.payno";
		else if(fm.PayType.value=="8")
			ttNoType = "b.edoracceptno";
		else if(fm.PayType.value=="9")
			ttNoType = "b.endorsementno";
		else if(fm.PayType.value=="10")
			ttNoType = "b.caseno";
		else if(fm.PayType.value=="11")
			ttNoType = "c.voucherid";
  //     	strSQL = "select * from (select a.batchno as batchno,"
       		strSQL = "select a.batchno as batchno,"
	        +"a.managecom as managecom,"
	        +"a.finitemtype as listflag,"
	        +"a.accountcode||'-'||(select codename from ldcode where trim(code) = trim(a.accountcode) and codetype = 'accountcode') as finiteminfo,"
	        +"(select classname from lioperationdataclassdef where classid = a.classid union select businessname from FIBnTypeDef where businessid = a.classid) as classinfo,"
	        +"a.riskcode as riskcode,"
	        +"a.salechnl as salechnl,"
	        +"a.accountdate as accountdate,"
	        //+"a.contno as contno,"
	         //+"(select (case conttype when '1' then contno else grpcontno end) from lccont where contno=a.contno or grpcontno=a.contno or prtno=a.contno union select (case conttype when '1' then contno else grpcontno end) from lbcont where contno=a.contno or grpcontno=a.contno or prtno=a.contno fetch first 1 rows only) as contno,"
	        +"LF_getContNoForCheck(a.classtype,a.contno) as contno,"
	        +"a.bankaccno as bankdetail,"
	        +"a.budget as budget,"
	        +"a.costcenter as costcenter,"	        
	       //  +"to_char(a.summoney,'FM999999999990.00') as money,"
	        +" (a.summoney) as money,"
	        +"a.classtype as classtype,"
	        +"(select keyname from liclasstypekeydef where classtype = a.classtype) as keyvaluename,"
	        +" case a.classtype ";
/*	        +"case a.classtype when 'B-01' then b.tempfeeno when 'B-02' then b.endorsementno when 'B-03' then b.edoracceptno "
					+"when 'B-04' then b.actugetno when 'C-01' then b.actugetno when 'C-02' then b.actugetno when 'N-01' then b.tempfeeno "
					+"when 'N-02' then b.payno when 'N-03' then b.actugetno when 'N-04' then b.grpcontno when 'N-05' then b.tempfeeno "
					+"when 'X-01' then b.tempfeeno when 'X-02' then b.tempfeeno when 'X-03' then b.contno when 'X-04' then b.actugetno "
					+"when 'X-05' then b.payno when 'T-01' then b.endorsementno when 'C-03' then b.caseno end as orzno"
*/
		for(i=0;i<tArrayC.length;i++){
				strSQL = strSQL + "when '" + tArrayC[i][0] + "' then b." + tArrayC[i][1] + " ";
			}
			
	  	strSQL = strSQL +"end as orzno,(select voucherid from interfacetable where importtype=a.classtype and batchno=a.batchno and depcode=a.managecom and accountcode=a.accountcode and chargedate=a.accountdate  fetch first 1 rows only), "
	  	   			+" (case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select grpname from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select grpname from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select appntname from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select appntname from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select appntname from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select appntname from lccont where contno=a.contno fetch first 1 rows only) "
					+" 	end ) appntname, "
					+" 	(case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select cvalidate from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select cvalidate from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select cvalidate from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select cvalidate from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select cvalidate from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select cvalidate from lccont where contno=a.contno fetch first 1 rows only) "
					+" 	end ) cvalidate, "
					+" 	(case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select cinvalidate from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select cinvalidate from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select cinvalidate from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select cinvalidate from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select cinvalidate from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select cinvalidate from lccont where contno=a.contno fetch first 1 rows only) "
					+" 	end ) cinvalidate "
	 // 	    +"(select appntname from lcpol where grpcontno=a.contno or contno=a.contno or prtno=a.contno union select appntname from lbpol where grpcontno=a.contno or contno=a.contno or prtno=a.contno fetch first 1 rows only),"
	//        +"(select cvalidate from lccont where grpcontno=a.contno or contno=a.contno or prtno=a.contno union select cvalidate from lbcont where grpcontno=a.contno or contno=a.contno or prtno=a.contno fetch first 1 rows only),"
	//        +"(select cinvalidate from lccont where grpcontno=a.contno or contno=a.contno or prtno=a.contno union select cinvalidate from lbcont where grpcontno=a.contno or contno=a.contno or prtno=a.contno fetch first 1 rows only)"
	  	    +" from LIDataTransResult a, liaboriginaldata b"
	        +" where b.serialno = a.standbystring3 "
	        +" and a.summoney<>0"
       		+" and a.accountdate >= '" +fm.StartDay.value + "'"
       		+" and a.accountdate <= '" +fm.EndDay.value + "'"
       		+" and a.accountcode = '" + fm.accountCode.value + "'"
       		+" and a.FinItemType = '"  +fm.FinItemType.value + "'"
       		+" and b.managecom like '" +fm.ManageCom.value + "%'"
       		+ getWherePart('a.riskcode','riskcode' )
 //      		+" and a.accountdate <= '" +fm.EndDay.value + "') "
 //      		+" as tab1 order by accountdate,orzno with ur";
    if(fm.PayType.value=="8"){
			strSQL = strSQL + " and b.endorsementno = '" + trim(fm.AllNo.value) + "' and b.listflag='1' ";
			}
		else if(fm.PayType.value=="9"){
			strSQL = strSQL + "and (b.endorsementno = '" + trim(fm.AllNo.value) + "' or b.endorsementno in (select trim(edorappno) from lpgrpedormain where edorno = '" + trim(fm.AllNo.value) + "') ) and b.listflag='2'  ";
			}
		else if(fm.PayType.value=="1"){
			strSQL = strSQL +" and (" + ttNoType + " = '" + trim(fm.AllNo.value) + "'  )"; 
			}
		else if(fm.PayType.value=="2"){
			strSQL = strSQL +" and (" + ttNoType + " = '" + trim(fm.AllNo.value) + "'  )"; 
			}
	  else if(fm.PayType.value=="3"){
			strSQL = strSQL +" and (" + ttNoType + " = '" + trim(fm.AllNo.value) + "'  )" ;
			}
	  else if(fm.PayType.value=="4"){
			strSQL = strSQL +" and  (" + ttNoType + " = '" + trim(fm.AllNo.value) + "'  )" ;
			}
     else if(fm.PayType.value=="11"){
	 	strSQL = "select a.batchno as batchno,"
	        +"a.managecom as managecom,"
	        +"a.finitemtype as listflag,"
	        +"a.accountcode||'-'||(select codename from ldcode where trim(code) = trim(a.accountcode) and codetype = 'accountcode') as finiteminfo,"
	        +"(select classname from lioperationdataclassdef where classid = a.classid union select businessname from FIBnTypeDef where businessid = a.classid) as classinfo,"
	        +"a.riskcode as riskcode,"
	        +"a.salechnl as salechnl,"
	        +"a.accountdate as accountdate,"
	        //+"a.contno as contno,"
	         //+"(select (case conttype when '1' then contno else grpcontno end) from lccont where contno=a.contno or grpcontno=a.contno or prtno=a.contno union select (case conttype when '1' then contno else grpcontno end) from lbcont where contno=a.contno or grpcontno=a.contno or prtno=a.contno fetch first 1 rows only) as contno,"
	        +"LF_getContNoForCheck(a.classtype,a.contno) as contno,"
	        +"a.bankaccno as bankdetail,"
	        +"a.budget as budget,"
	        +"a.costcenter as costcenter,"	        
	       //  +"to_char(a.summoney,'FM999999999990.00') as money,"
	        +" (a.summoney) as money,"
	        +"a.classtype as classtype,"
	        +"(select keyname from liclasstypekeydef where classtype = a.classtype) as keyvaluename,"
	        +" case a.classtype ";
		for(i=0;i<tArrayC.length;i++){
				strSQL = strSQL + "when '" + tArrayC[i][0] + "' then b." + tArrayC[i][1] + " ";
			}
			
	  	strSQL = strSQL +"end as orzno,c.voucherid , "
	  	   			+" (case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select grpname from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select grpname from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select appntname from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select appntname from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select appntname from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select appntname from lccont where contno=a.contno fetch first 1 rows only) "
					+" 	end ) appntname, "
					+" 	(case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select cvalidate from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select cvalidate from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select cvalidate from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select cvalidate from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select cvalidate from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select cvalidate from lccont where contno=a.contno fetch first 1 rows only) "
					+" 	end ) cvalidate, "
					+" 	(case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
					+" 	then (select cinvalidate from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
					+" 	then (select cinvalidate from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lccont where prtno=a.contno) "
					+" 	then (select cinvalidate from lccont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where prtno=a.contno) "
					+" 	then (select cinvalidate from lbcont where prtno=a.contno fetch first 1 rows only) "
					+" 	when exists (select 1 from lbcont where contno=a.contno) "
					+" 	then (select cinvalidate from lbcont where contno=a.contno fetch first 1 rows only) "
					+" 	else "
					+" 	(select cinvalidate from lccont where contno=a.contno fetch first 1 rows only) "   
					+" 	end ) cinvalidate "
	  	   +" from LIDataTransResult a, liaboriginaldata b,interfacetable c"
	        +" where b.serialno = a.standbystring3 "
	        +" and c.readstate = '2'"
	        +" and a.classtype = c.ImportType"
	        +" and a.managecom = c.depcode"
	        +" and a.batchno = c.batchno"
	        +" and a.accountcode=c.accountcode"
	        +" and a.accountdate = c.chargedate"
	        +" and a.FinItemType = c.DCFlag"
	        +" and a.summoney<>0"
       		+" and a.accountdate >= '" +fm.StartDay.value + "'"
       		+" and a.accountdate <= '" +fm.EndDay.value + "'"
       		+" and a.accountcode = '" + fm.accountCode.value + "'"
       		+" and a.FinItemType = '"  +fm.FinItemType.value + "'"
       		+" and b.managecom like '" +fm.ManageCom.value + "%'"
       		+getWherePart('a.riskcode','riskcode' ) 
       		+ " and "+ ttNoType +" = '" + trim(fm.AllNo.value) + "'";
			}
			else if(fm.PayType.value=="5"){
			strSQL = strSQL + " and "+ ttNoType +" = '" + trim(fm.AllNo.value) + "'";
			}
			else if(fm.PayType.value=="6"){
			strSQL = strSQL + " and "+ ttNoType +" = '" + trim(fm.AllNo.value) + "'";
			}
			else if(fm.PayType.value=="7"){
			strSQL = strSQL + " and "+ ttNoType +" = '" + trim(fm.AllNo.value) + "'";
			}
			else if(fm.PayType.value=="10"){
			strSQL = strSQL + " and "+ ttNoType +" = '" + trim(fm.AllNo.value) + "'";
			}
			
		else{
//			strSQL = strSQL + " with ur" ;
//			strSQL = strSQL + " and "+ ttNoType +" = '" + trim(fm.AllNo.value) + "'";
//			strSQL = strSQL +getWherePart('ttNoType','" + trim(fm.AllNo.value) + "');
		}
	
			strSQL = strSQL +" with ur" ;       		

    }
    else if(fm.checkType.value=="3"){

			var cSQL = easyQueryVer3("select keyid from liclasstypekeydef where classtype = '"+fm.all("SClassType").value+"'",1,1,1);
			var	cArray = decodeEasyQueryResult(cSQL);

			strSQL = "select a.batchno as batchno,"
		        +"a.managecom as managecom,"
		        +"b.finitemtype as listflag,"
		        +"b.accountcode||'-'||(select codename from ldcode where trim(code) = trim(b.accountcode) and codetype = 'accountcode') as finiteminfo,"
		        +"(select classname from lioperationdataclassdef where classid = a.classid union select businessname from FIBnTypeDef where businessid = a.classid) as classinfo,"
		        +"b.riskcode as riskcode,"
		        +"b.salechnl as salechnl,"
		        +"b.accountdate as accountdate,"
		        //+"b.contno as contno,"
		        //+"(select (case conttype when '1' then contno else grpcontno end) from lccont where contno=b.contno or grpcontno=b.contno or prtno=b.contno union select (case conttype when '1' then contno else grpcontno end) from lbcont where contno=b.contno or grpcontno=b.contno or prtno=b.contno fetch first 1 rows only) as contno,"
		        +"LF_getContNoForCheck(b.classtype,b.contno) as contno,"
		        +"b.bankaccno as bankdetail,"
		        +"b.budget as budget,"
		        +"b.costcenter as costcenter,"
		        // +"to_char(b.summoney,'FM999999999990.00') as money,"
		        +" (b.summoney) as money,"
		        +"b.classtype as classtype,"
		        +"(select keyname from liclasstypekeydef where classtype = b.classtype) as keyvaluename,"
						+" a."+cArray[0][0]+" as orzno  ,(select voucherid from interfacetable where importtype=b.classtype and batchno=b.batchno and depcode=b.managecom and accountcode=b.accountcode and chargedate=b.accountdate  fetch first 1 rows only),"
						+" (case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
						+" 	then (select grpname from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
						+" 	then (select grpname from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lccont where prtno=a.contno) "
						+" 	then (select appntname from lccont where prtno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbcont where prtno=a.contno) "
						+" 	then (select appntname from lbcont where prtno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbcont where contno=a.contno) "
						+" 	then (select appntname from lbcont where contno=a.contno fetch first 1 rows only) "
						+" 	else "
						+" 	(select appntname from lccont where contno=a.contno fetch first 1 rows only) "
						+" 	end ) appntname, "
						+" 	(case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
						+" 	then (select cvalidate from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
						+" 	then (select cvalidate from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lccont where prtno=a.contno) "
						+" 	then (select cvalidate from lccont where prtno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbcont where prtno=a.contno) "
						+" 	then (select cvalidate from lbcont where prtno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbcont where contno=a.contno) "
						+" 	then (select cvalidate from lbcont where contno=a.contno fetch first 1 rows only) "
						+" 	else "
						+" 	(select cvalidate from lccont where contno=a.contno fetch first 1 rows only) "
						+" 	end ) cvalidate, "
						+" 	(case when exists (select 1 from lcgrpcont where grpcontno=a.contno) "
						+" 	then (select cinvalidate from lcgrpcont where grpcontno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbgrpcont where grpcontno=a.contno) "
						+" 	then (select cinvalidate from lbgrpcont where grpcontno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lccont where prtno=a.contno) "
						+" 	then (select cinvalidate from lccont where prtno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbcont where prtno=a.contno) "
						+" 	then (select cinvalidate from lbcont where prtno=a.contno fetch first 1 rows only) "
						+" 	when exists (select 1 from lbcont where contno=a.contno) "
						+" 	then (select cinvalidate from lbcont where contno=a.contno fetch first 1 rows only) "
						+" 	else "
						+" 	(select cinvalidate from lccont where contno=a.contno fetch first 1 rows only) "
						+" 	end ) cinvalidate "
//						+"(select appntname from lcpol where grpcontno=b.contno or contno=b.contno or prtno=b.contno union select appntname from lbpol where grpcontno=b.contno or contno=b.contno or prtno=b.contno fetch first 1 rows only),"
//	        			+"(select cvalidate from lccont where grpcontno=b.contno or contno=b.contno or prtno=b.contno union select cvalidate from lbcont where grpcontno=b.contno or contno=b.contno or prtno=b.contno fetch first 1 rows only),"
//	        			+"(select cinvalidate from lccont where grpcontno=b.contno or contno=b.contno or prtno=b.contno union select cinvalidate from lbcont where grpcontno=b.contno or contno=b.contno or prtno=b.contno fetch first 1 rows only)"
						+"  from  liaboriginaldata a,LIDataTransResult b"
	       		+" where a.serialno = b.standbystring3 "
	       		+" and b.summoney<>0"
	       		//+" and a.managecom like '" +fm.ManageCom.value + "%'"
	       		+getWherePart('a.ManageCom','operateCom' )
       			+" and b.accountdate >= '" +fm.SDate.value + "'"
       			+" and b.accountdate <= '" +fm.EDate.value + "'"
       			+" and b.classtype = '" +fm.all("SClassType").value+"' with ur";
//       			+" and b.accountdate <= '" +fm.EDate.value + "'"
//       		  +" order by accountdate,a."+cArray[0][0]+" with ur";
    	}
	fm.ExportExcelSQL.value=strSQL;//保存sql，导出excel时用到
	turnPage.queryModal(strSQL,CheckQueryDataGrid);

/*var strSQL = "select '1' as batchno,"
       +"'1' as managecom,"
       +"'1' as listflag,"
       +"'1' as accountcode,"
       +"'1' as bankdetail,"
       +"'1' as riskcode,"
       +"'1' as salechnl,"
       +"'1' as classid,"
       +"'1' as classname,"
       +"'1' as accountdate,"
       +"'1' as contno,"
       +"'1' as money"
  	   +" from lidatatransresult a"
       +" where 1=1";

    if(fm.checkType.value=="2"){//核对类型^1|科目 ^2|业务代码
       	if(fm.ContType.value=="1"){//个单
       		if(fm.ContNo.value!="" && fm.ContNo.value!=null)
       		{
       			strSQL= strSQL +" and ContNo='" + fm.ContNo.value + "'";
       		}
       		else if(fm.PrtNo.value!="" && fm.PrtNo.value!=null)
       		{
       			strSQL= strSQL +" and ContNo=(select contno from lccont where ContType='1' and prtno='" + fm.PrtNo.value + "')";      		
       		}
       }
       	if(fm.ContType.value=="2"){//团单
       		if(fm.ContNo.value!="" && fm.ContNo.value!=null)
       		{
       			strSQL= strSQL +" and ContNo='" + fm.ContNo.value + "'";
       		}
       		else if(fm.PrtNo.value!="" && fm.PrtNo.value!=null)
       		{
				strSQL= strSQL +" and ContNo=(select distinct GrpContNo from lccont where ContType='2' and prtno='" + fm.PrtNo.value + "')";      		
       		}
      }
    }
    else if(fm.checkType.value=="1"){//核对类型^1|科目 ^2|业务代码
       	strSQL= strSQL + " and 2=2";
    }
    else{}
	fm.ExportExcelSQL.value=strSQL;//保存sql，导出excel时用到
	fm.temptext.value=strSQL;
//	alert(fm.ExportExcelSQL.value);
	turnPage.queryModal(strSQL,CheckQueryDataGrid);
*/
	showInfo.close();
	
	if(CheckQueryDataGrid.mulLineCount=="0"){
		alert("没有查询到数据");
	}		 
	return true;		
	}
  }
  catch(ex)
  {
     alert(ex);
   }
}

function afterCodeSelect(cCodeName, Field) 
{
	if(cCodeName == "checkType") 
	{  
		if(fm.checkType.value=="1"){
			fm.all("divSearch1").style.display = '';
			fm.all("divSearch2").style.display = 'none';
			fm.all("divSearch3").style.display = 'none';
		}else if(fm.checkType.value=="2"){
			fm.all("divSearch2").style.display = '';
			fm.all("divSearch1").style.display = 'none';	
			fm.all("divSearch3").style.display = 'none';		
		}else if(fm.checkType.value=="3"){
			fm.all("divSearch3").style.display = '';
			fm.all("divSearch2").style.display = 'none';
			fm.all("divSearch1").style.display = 'none';			
		}
	}
}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord(strRecord)
{

  //保存查询结果字符串
  turnPage.strQueryResult  = strRecord;

//alert(strRecord);
  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray = new Array(0,9,4,1,7);
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  //初始化的对象
  	 turnPage.pageDisplayGrid = CheckQueryDataGrid;       
              
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
	
}
