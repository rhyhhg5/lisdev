
<%@ page contentType="text/html; charset=GBK"%>
<%@page import="com.sinosoft.lis.fininterface.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.net.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>

<%
	CErrors tError = new CErrors();
	String FlagStr = "";
	String Content = "";
	String flag = "true";
	String bdate = request.getParameter("StartDate");
	String edate = request.getParameter("EndDate");
	String cManageCom = request.getParameter("sComCode");
	String strReturnRecord = "";
	
	FDate chgdate = new FDate();
	Date dbdate = chgdate.getDate(bdate);
	Date dedate = chgdate.getDate(edate);
	if (dbdate.compareTo(dedate) > 0) {
		flag = "false";
		FlagStr = "Fail";
		Content = "起始日期不能晚于终止日期!";
	}

	if (flag.equals("true")) {
		try {
			CheckCountMoneyBL oCheckCountMoneyBL = new CheckCountMoneyBL();
			if (!oCheckCountMoneyBL.CheckData(bdate, edate, cManageCom)) {
		              FlagStr = "Fail";
		              Content = oCheckCountMoneyBL.mErrors.getFirstError();
		              System.out.println("操作失败！");
			}
			else{
                VData oData = new VData();
                oData = oCheckCountMoneyBL.getResult();
				LICodeTransSet oCodeTransSet = new LICodeTransSet();
			    oCodeTransSet.set((LICodeTransSet)oData.getObjectByObjectName("LICodeTransSet",0));
			    
			    if(oCodeTransSet != null && oCodeTransSet.size() > 0){
			    	strReturnRecord = "0|" + oCodeTransSet.size() + "^" + oCodeTransSet.encode();	
				    FlagStr = "Succ";
					Content = "校验完毕";
			    }else{			    	
				    FlagStr = "Succ";
					Content = "未发现不等的数据";
			    }
			}
		} catch (Exception ex) {
			FlagStr = "Fail";
			Content = "校验失败，原因是: " + ex.getMessage();
			System.out.println(Content);
		}

	}
%>
<%@page import="com.sinosoft.lis.vschema.LICodeTransSet"%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=strReturnRecord%>");
</script>
</html>

