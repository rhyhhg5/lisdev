<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    

<%@page contentType="text/html;charset=GBK" %>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
%>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="GetFeePrintInput.js"></SCRIPT>   
<%@include file="GetFeePrintInit.jsp"%>
 
   
</head>
<body onload="initForm()">
<form action="GetFeePrintSave.jsp" method=post name=fm target="fraSubmit">
  <table>
    <tr>
        <td class="titleImg">应收保费查询报表</td>
    </tr>
  </table>
  <TABLE class="common">
    <tr class="common">
        <td class="title">开始日期</td>
        <td class="input">
          <input class="coolDatePicker" dateFormat="short" name="StartDate">
        </td>
        <td class="title">结束日期</td>
        <td class="input">
          <input class="coolDatePicker" dateFormat="short" name="EndDate" verify="结束日期|notnull&date">
          <font color="#FF0000" align="Left">*</font>
        </td>
    </tr>
  </TABLE>
  <br>
  <input value="应收保费统计表" type=button onclick="Print()" class="cssButton" type="button"> 
  <input type="hidden" name="fmtransact" value="">
</form>
<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
