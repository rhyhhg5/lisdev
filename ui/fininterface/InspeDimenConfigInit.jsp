
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	String strOperator = globalInput.Operator;
	String strManageCom = globalInput.ComCode;
%>

<script language="JavaScript">
                                   
function initForm()
{
  try
  {
    initConfigFicodetransGrid();   
  }
  catch(re)
  {
    alert("InitForm函数中发生异常:初始化界面错误!");
  }
}
var ConfigFicodetransGrid;
function initConfigFicodetransGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="险类-新考核维度";
		iArray[1][1]="30px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="险种编码";
		iArray[2][1]="50px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		

		ConfigFicodetransGrid = new MulLineEnter( "fm" , "ConfigFicodetransGrid" ); 
		ConfigFicodetransGrid.mulLineCount=0;
		ConfigFicodetransGrid.displayTitle=1;
		ConfigFicodetransGrid.canSel=0;
		ConfigFicodetransGrid.canChk=0;
		ConfigFicodetransGrid.hiddenPlus=1;
		ConfigFicodetransGrid.hiddenSubtraction=1;

		ConfigFicodetransGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
