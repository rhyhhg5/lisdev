<%@page contentType="text/html;charset=GBK"%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
 String CurrentDate = PubFun.getCurrentDate();
%>  
<html>
<head>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

  <SCRIPT src="DataDetailCheck.js"></SCRIPT>
  <%@include file="DataDetailCheckInit.jsp"%>
  <title>数据明细核对</title>
</head>

<body  onload="initForm();" >
  <form name=fm1 target="fraSubmit">

  <table class= common border=0 width=100%>
     <tr>
			  <td class= titleImg align= center>请选择提数日期范围：</td>
		 </tr>
	</table>
   <table  class= common align=center>

      	<TR  class= common>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Bdate value = '<%=CurrentDate%>'>
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Edate value = '<%=CurrentDate%>'>
          </TD>
        </TR>

	      </TR>
	        <TR  class= common>
            <TD>
             <INPUT  class=cssButton VALUE=" 查 询 " TYPE=Button onclick="initQuery();">
            </TD>
        </TR>
    </table>
        
    <!-- 批次号信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divData1);">
    		</td>
    		<td class= titleImg>
    			 批次号信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divData1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBatchNoGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage1" align=center style= "display: '' ">
    	<center>
    		<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    		<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
		</center>
    </Div>  
    <br>    
    </form>
      <form name=fm2 target="fraSubmit" action="../fininterface/DataDetailCheckQuery.jsp">
       <!--INPUT VALUE="test" class= cssButton TYPE=button onclick="testtt()"-->
    <INPUT VALUE= "核对数据" class= cssButton TYPE=button onclick="submitForm();">  &nbsp;&nbsp;&nbsp;<font color=red>*</font>点击查询借贷不平的业务号码
    <br>
    <hr width = 98%>
    <br>

  	<Div  id= "divData2" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanClassTypeGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage2" align=center style= "display: '' ">
    	<center>
    		<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();"> 
    		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
    		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
    		<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
		</center>
    </Div>
    
    <input type=hidden value='' name = mBatchNo>
    <input type=hidden value='' name = cClassType>
    <input type=hidden value='' name = cContNo>
    <input type=hidden value='' name = cKeyID>
    
    <br>    
    </form>
    
      <form name=fm3 target="fraSubmit">
       <!--INPUT VALUE="test" class= cssButton TYPE=button onclick="testtt()"-->
    <INPUT VALUE= "核对明细数据" class= cssButton TYPE=button onclick="detailCheck();">  &nbsp;&nbsp;&nbsp;<font color=red>*</font>点击查询选定的业务号码在该凭证内的所有记录
    <br>
    <hr width = 98%>
    <br>   

  	<Div  id= "divData3" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanDetailDataGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage3" align=center style= "display: '' ">
    	<center>
    		<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
    		<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
    		<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
    		<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
		</center>
    </Div>
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>