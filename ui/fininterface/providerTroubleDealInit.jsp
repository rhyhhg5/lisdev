<% 
//程序名称：IntFaceTabDataInit.jsp
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

function initInpBox()
{
  try
  {
	//查询条件置空
	fm.all('agentcom').value = '';
	fm.all('Accountdate').value = '';
	fm.all('batchno').value = '';
	fm.all('serialno').value = '';
	fm.all('vouchertype').value = '';
	fm.all('agentno').value = '';
	fm.all('newagentno').value = '';
  }
  catch(ex)
  {
    alert("IntFaceTabDataInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                            

function initForm()
{
  try
  {
	initInpBox();
    initDataGrid();  //初始化共享工作池
    initInterfaceDataGrid();
  }
  catch(re)
  {
    alert("providerTroubleDealInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initDataGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="38px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="业务号码";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[2]=new Array();                                                            
      iArray[2][0]="记账日期";         		//列名                                    
      iArray[2][1]="140px";            		//列宽                                  
      iArray[2][2]=170;            			//列最大值                                
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
                                                                                  
      iArray[3]=new Array();                                                      
      iArray[3][0]="金额";         		//列名                                  
      iArray[3][1]="50px";            		//列宽                                  
      iArray[3][2]=100;            			//列最大值                                
      iArray[3][3]=0;             			//是否允许输入,1表示允许，0表示不允许     
                                                                                  
      iArray[4]=new Array();                                                      
      iArray[4][0]="管理机构";         		//列名                                  
      iArray[4][1]="120px";            		//列宽                                  
      iArray[4][2]=100;            			//列最大值                                
      iArray[4][3]=0;                       //是否允许输入,1表示允许，0表示不允许 


      iArray[5]=new Array();                                                            
      iArray[5][0]="中介机构";         		//列名                                    
      iArray[5][1]="140px";            		//列宽                                  
      iArray[5][2]=170;            			//列最大值                                
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
                                                                                                                                                            
          
      DataGrid = new MulLineEnter( "fm" , "DataGrid" ); 
      //这些属性必须在loadMulLine前
      DataGrid.mulLineCount = 0;   
      DataGrid.displayTitle = 1;
      DataGrid.locked = 1;
      DataGrid.canSel = 1;
      //IntFaceTabDataGrid.canChk = 0;
      DataGrid.hiddenPlus = 1;
      DataGrid.hiddenSubtraction = 1;        
      DataGrid.loadMulLine(iArray); 
      DataGrid.selBoxEventFuncName = "BankSelect"; 
      
	}
	catch(ex)
	{
		alert("IntDataGrid-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initInterfaceDataGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="38px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="流水号";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[2]=new Array();                                                            
      iArray[2][0]="记账日期";         		//列名                                    
      iArray[2][1]="140px";            		//列宽                                  
      iArray[2][2]=170;            			//列最大值                                
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许     
                                                                                  
      iArray[3]=new Array();                                                      
      iArray[3][0]="管理机构";         		//列名                                  
      iArray[3][1]="50px";            		//列宽                                  
      iArray[3][2]=100;            			//列最大值                                
      iArray[3][3]=0;             			//是否允许输入,1表示允许，0表示不允许     
                                                                                  
      iArray[4]=new Array();                                                      
      iArray[4][0]="凭证类型";         		//列名                                  
      iArray[4][1]="120px";            		//列宽                                  
      iArray[4][2]=100;            			//列最大值                                
      iArray[4][3]=0;                       //是否允许输入,1表示允许，0表示不允许 

      iArray[5]=new Array();                                                      
      iArray[5][0]="批次号码";         		//列名                                  
      iArray[5][1]="120px";            		//列宽                                  
      iArray[5][2]=100;            			//列最大值                                
      iArray[5][3]=0;                       //是否允许输入,1表示允许，0表示不允许 

      iArray[6]=new Array();                                                      
      iArray[6][0]="金额";         		//列名                                  
      iArray[6][1]="120px";            		//列宽                                  
      iArray[6][2]=100;            			//列最大值                                
      iArray[6][3]=0;                       //是否允许输入,1表示允许，0表示不允许 



      iArray[7]=new Array();                                                      
      iArray[7][0]="错误供应商号码";         		//列名                                  
      iArray[7][1]="120px";            		//列宽                                  
      iArray[7][2]=100;            			//列最大值                                
      iArray[7][3]=0;                       //是否允许输入,1表示允许，0表示不允许 
                                                                                 
                                                                                 
          
      InterfaceDataGrid = new MulLineEnter( "fm" , "InterfaceDataGrid" ); 
      //这些属性必须在loadMulLine前
      InterfaceDataGrid.mulLineCount = 0;   
      InterfaceDataGrid.displayTitle = 1;
      InterfaceDataGrid.locked = 1;
      InterfaceDataGrid.canSel = 1;
      //IntFaceTabDataGrid.canChk = 0;
      InterfaceDataGrid.hiddenPlus = 1;
      InterfaceDataGrid.hiddenSubtraction = 1;        
      InterfaceDataGrid.loadMulLine(iArray); 
      InterfaceDataGrid.selBoxEventFuncName = "AgentnoSelect"; 
      
	}
	catch(ex)
	{
		alert("InterfaceDataGrid-->InitForm函数中发生异常:初始化界面错误!");
	}
}

</script>
