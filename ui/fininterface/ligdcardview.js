//该文件中包含客户端需要处理的函数和事件

//程序名称：ligdcardview.js
//程序功能：元数据定义
//创建日期：2011/11/10
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

function ManagecomName()
{
	var checksql = "select name from ldcom where comcode='"+comcode+"' " ;	
	var result=easyExecSql(checksql);
	fm.ManageComName.value = result ;
}

//查询按钮
function CardQuery(){
	if(fm.ManageCom.value==''||fm.ManageCom.value==null){
	  			alert("请选择管理机构");
	  			fm.ManageCom.focus();
	  			return false;
	  	}		  	  	
	  if(fm.StartDay.value==''||fm.StartDay.value==null){
	  			alert("请选择起始日期");
	  			fm.StartDay.focus();
	  			return false;
	  	}
	  if(fm.EndDay.value==''||fm.EndDay.value==null){
	  			alert("请选择终止日期");
	  			fm.EndDay.focus();
	  			return false;
	  	}
	  if(fm.EndDay.value<fm.StartDay.value){
					alert("起始时间大于终止时间,请重新输入!");
					fm.EndDay.focus();
					return false;
	　    }	
	  if(fm.EndDay.value>fm.StartDay.value){
	 		var days = parseInt((new Date(fm.EndDay.value.replace(/-/g, "/")).getTime() - new Date(fm.StartDay.value.replace(/-/g, "/")).getTime())/1000/60/60/24);
	 			//	alert(days);
	 		if(days>31) {
				alert("因将近年底各分公司均在做凭证核对，所以起始时间与结束时间相差不能大于一个月,请重新输入按月统计，非常感谢!");
				fm.EndDay.focus();
				return false;
			}
	　    }	 
	var strSQL = "";
	
	var len = comcode.length;
	if(len==2&&comcode!="86")
	{
		alert('机构不合法，请刷新页面重新获取！');
		return false;
	}
	
	if(len==4&&comcode!="8644")
	{
		alert('机构不合法，广东卡单只允许8644下的机构可进行查询！');
		return false;
	}
	if(len>4&&comcode.substr(0,4)!='8644')
	{
		alert('机构不合法，广东卡单只允许8644下的机构可进行查询！');
		return false;
	}
	strSQL = "  select "
			+"	a.batchno,"
			+"	a.managecom as macom,"
			+"	b.managecom as excom,"
			+"	a.classtype||' 广东卡单' type,"
			+"	a.contno,"
			+"	(case a.classtype when 'N-05' then substr(a.keyunionvalue,length(a.keyunionvalue)-10,11) else '' end) cardno,"
			+"	a.accountdate,"
			+"	a.riskcode,"
			+"	a.summoney,"
			+"	(select c.voucherid"
			+"	              from interfacetable c"
			+"	              where c.importtype = a.classtype"
			+"	              and c.batchno = a.batchno"
			+"	              and c.depcode = a.managecom"
			+"	              and c.accountcode = a.accountcode"
			+"	              and c.chargedate = a.accountdate fetch first 1 rows only)  voucherid ,"
			+"  a.accountcode,"
			+"  a.finitemtype,"
			+"  (select activedate from licertify where cardno = substr(a.keyunionvalue,length(a.keyunionvalue)-10,11)),"
			+"  (select cvalidate from licertify where cardno = substr(a.keyunionvalue,length(a.keyunionvalue)-10,11))"
			+"	from lidatatransresult a, liaboriginaldata b"
			+"	where 1=1"//"a.classid in ('00000149','00000150','00000157')"
			+"	and b.managecom like '8644%'"
			+"  and a.classtype in ('N-01','N-05')"	
			+"  and exists (select 1 from lcgrpcont where cardflag = '2' and grpcontno = a.contno and managecom like '8644%')"					
			+"  and b.managecom like '"+comcode+"%'"
			+"	and a.standbystring3=b.serialno"
			+"	and a.accountdate between '"+fm.StartDay.value+"' and '"+fm.EndDay.value+"' and a.accountcode='6031000000'  "
			+"	with ur";
	turnPage.queryModal(strSQL, cardGrid);
	if(cardGrid.mulLineCount=="0"){
		alert("没有查询到数据");
	}
	
	fm.querysql.value=strSQL;
	
}

//导出Excel按钮
function upload(){

if(cardGrid.mulLineCount=="0"){
		alert("没有查询到数据");
		return false;
	} 	 	
	fm.action="./ligdcardtoexcel.jsp";
	fm.target="fraSubmit";
	fm.submit(); //提交
}


//提交前的校验、计算  
function beforeSubmit(){
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}
