//程序名称：FinInterfaceReporting.js
//程序功能：财务接口报表
//创建日期：2006-11-20 
//创建人  ：lijs
//更新记录：  更新人    更新日期     更新原因/内容
//

var turnPage = new turnPageClass();//日结试算相关数据
var mFlag = "0";
var showInfo;
/****************************************************
*导出相关数据财务接口报表分明细和期间的统计
*********************************************************/ 
function ToExcel()
{
  if(FinInterfaceGrid.getSelNo()){  	
	   fm.all("serialNo").value = FinInterfaceGrid.getRowColData(FinInterfaceGrid.getSelNo()-1, 1); 
	   //fm.all("othernotype").value = FinInterfaceGrid.getRowColData(FinInterfaceGrid.getSelNo()-1,6); 	 	
	   fm.action="./TestInterfaceDataToExcel.jsp";
	   fm.target="fraSubmit";
	   fm.submit(); //提交
  }
  else
  {  	
   	  alert("请先选择一条批次号信息");   	
  }
}
/************************
*PDF打印功能实现 
***************************/
function printFinInterface(){
	
	if(FinInterfaceGrid.getSelNo()){	  	
		    var i = 0;
			var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		 	fm.action = "./TestInterfacePrintPDF.jsp?operator=printPDF";//测试PDF打印相关数据
		 	fm.target=".";
		 	//fm.fmtransact.value="PRINT";	 
		 	fm.all("serialNo").value = FinInterfaceGrid.getRowColData(FinInterfaceGrid.getSelNo()-1, 1);
		 	//fm.all("m_sOperator").value = FinDayConfGrid.getRowColData(FinDayConfGrid.getSelNo()-1, 2);
		 	//fm.all("othernotype").value = FinDayConfGrid.getRowColData(FinDayConfGrid.getSelNo()-1,4); 
		 	fm.submit();
		 	showInfo.close();  
      }else{
        alert("请选择一条记录!");
      }
}
/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr,content)
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}
/*******************
**财务接口报表查询
**********************/
function queryFinInterface(){

	
	if(beforeQueryFin()){
		var i = 0;
		var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   		 		
		fm.sStartDate.value = fm.StartDay1.value;
		fm.sEndDate.value = fm.EndDay1.value;
		fm.QueryType.value = '2';//财务接口报表查询
		//fm.sInterfaceInfo.value = 'FinInterfaceInfo';	//财务接口明细
		fm.action = "./TestInterfaceQuery.jsp";
		fm.target="fraSubmit";
		fm.submit();
	}
}

function beforeQueryFin(){
  if(fm.StartDay1.value==''||fm.StartDay1.value==null)
  {
  			alert("请选择起始日期");
  			fm.StartDay1.focus();
  			return false;
  			}
  if(fm.EndDay1.value==''||fm.EndDay1.value==null)
  {
  			alert("请选择终止日期");
  			fm.EndDay1.focus();
  			return false;
  			}
  if(fm.EndDay1.value<fm.StartDay1.value)
	{
				alert("起始时间大于终止时间,请重新输入!");
				fm.StartDay1.focus();
				return false;
	}
	
	return true;
}
//测试财务接口报表查询数据
//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord(strRecord)
{

  //保存查询结果字符串
  turnPage.strQueryResult  = strRecord;  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  	var filterArray = new Array(0,7,9,1,4); 
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  //初始化的对象
  	 turnPage.pageDisplayGrid = FinInterfaceGrid;            
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }  
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;	
}

