<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：CompetiReportSave.jsp
//程序功能：2012全年业务竞赛报表
//创建日期：2012/08/09
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.fininterface.*"%>

<%
 //接收信息，并作校验处理。
 CompetiReportNewBL tCompetiReportNewBL = new CompetiReportNewBL();
	
	//得到excel文件的保存路径
	String Bdate=request.getParameter("Bdate");
	String Edate=request.getParameter("Edate");
	String YBdate=request.getParameter("YBdate");
	String YEdate=request.getParameter("YEdate");	
 	String path = application.getRealPath("").replace('\\','/')+'/';
 	String outputPath=path+"fininterface/";
	System.out.println("保费起始日期:"+Bdate);
	System.out.println("保费终止日期:"+Edate);
	System.out.println("犹豫期退保起始日期:"+YBdate);
	System.out.println("犹豫期退保终止日期:"+YEdate);
	System.out.println(outputPath);

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("Bdate",Bdate);
	tTransferData.setNameAndValue("Edate",Edate);
	tTransferData.setNameAndValue("YBdate",YBdate);
	tTransferData.setNameAndValue("YEdate",YEdate);
	VData tVData = new VData();
	tVData.add(tTransferData);

 //输出参数
 CErrors tError = null;        
 String FlagStr = "True";
 String Content = "生成报表成功！";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getAttribute("GI");
 tVData.add(tG); 
 
 try{
 	if(!tCompetiReportNewBL.submitData(tVData,outputPath)){
 	  Content = "生成报表失败，原因是：" + tCompetiReportNewBL.mErrors.getFirstError();
	  FlagStr = "Fail";
 	}   
 } 
catch(Exception ex){
	  Content = "生成报表失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr==""){
  tError = tCompetiReportNewBL.mErrors;
 if (!tError.needDealError()){                          
   Content = " 保存成功! ";
   FlagStr = "Success";
  }else  {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 //添加各种预处理
 //window.location.href="../fininterface/000001.xls";
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
