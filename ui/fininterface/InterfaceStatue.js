//程序名称：FinInterfaceCheck.js
//程序功能：凭证核对
//创建日期：2007-10-26 
//创建人  ：l
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();

var mFlag = "0";
var showInfo;

/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}
// add 2006-9-30 11:49
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


function getRefresh()
{
  try
  {		InterfaceStatueGrid.clearData("InterfaceStatueGrid");   
		var strSQL = "select * from lidistilllog where batchno = 'FinInterFaceService' and flag = '1'";
	
		strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
		if (!strQueryResult) 
		{
			fm.all("NotRunning").style.display='';
			fm.all("Running").style.display='none';
			fm.all("divInterfaceStatue").style.display='none';
			return false;
		}
		else{
			fm.all("NotRunning").style.display='none';
			fm.all("Running").style.display='';
			fm.all("divInterfaceStatue").style.display='';	

//			strQueryResult = easyQueryVer3("select max(batchno) from lidatatransresult");
		  
			strQueryResult = easyQueryVer3("select managecom,startdate,enddate,makedate,maketime,operator from lidistilllog where batchno = 'FinInterFaceService'",1,1,1);
			var	tArray = decodeEasyQueryResult(strQueryResult);
//			alert(tArray2[0][0]);
			var curManageCom = tArray[0][0];
			var curTimeArea = tArray[0][1] + " 至 " + tArray[0][2];
			var curMakeTime = tArray[0][3] + " " + tArray[0][4];
			var curOperator = tArray[0][5];
			fm.all("tManageCom").innerText = curManageCom;
			fm.all("tTimeArea").innerText = curTimeArea;
			fm.all("tStartTime").innerText = curMakeTime;
			fm.all("tOperator").innerText = curOperator;
			
			strQueryResult = easyQueryVer3("select max(batchno),char(sum(sumactumoney)),count(1) from liaboriginaldata group by batchno order by batchno desc");
			if(strQueryResult){
			tArray = decodeEasyQueryResult(strQueryResult);
//			alert(tArray[0][0]);
			var curBatchNo = tArray[0][0];
//			alert(tArray[0][1]);
			fm.all("tSumMoney").innerText = tArray[0][1];
			fm.all("tDataNum").innerText = tArray[0][2];
			
			strSQL = "select distinct classid,(select classname from lioperationdataclassdef " +
			         "where classid = a.classid) as classname,batchno,count(1),max(accountdate),char(sum(summoney)) " +
			         "from lidatatransresult a where batchno = '" + curBatchNo + "'  group by classid,batchno";

		turnPage.queryModal(strSQL,InterfaceStatueGrid);
		}

		if(InterfaceStatueGrid.mulLineCount=="0"){
			alert("没有查询到数据");
		}		 
		return true;
		}
  }
  catch(ex)
  {
     alert(ex);
   }
}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord(strRecord)
{

  //保存查询结果字符串
  turnPage.strQueryResult  = strRecord;

//alert(strRecord);
  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray = new Array(0,9,4,1,7);
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  //初始化的对象
  	 turnPage.pageDisplayGrid = CheckQueryDataGrid;       
              
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
	
}
