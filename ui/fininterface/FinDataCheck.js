//程序名称：AuditDataCheck.js
//程序功能：保监会稽核数据校验js
//创建日期：2009-06-18
//创建人  : Qisl
//更新记录：  更新人    更新日期     更新原因/内容

//该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();

function checkInput()
{
	var vStartDate = fm.all('StartDate').value;
	var vManageCom = fm.all('ManageCom').value;
	
	if(vStartDate == null || vStartDate == "")
	{
		alert("请输入校验时间段起期！");
		return false;
	}
	
	if(vManageCom == null || vManageCom == "")
	{
		alert("请输入管理机构！");
		return false;
	}
	
	 return true;
}

//提交，保存按钮对应操作
function submitForm()
{
	if(!checkInput())
	{
		return false;
	}
	
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //initPolGrid();
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	queryClick();
}

//查询并列出所有校验结果
function queryClick() {
	var sql = "select codename,othersign from LIFinDataCheck where codetype='FinDataCheck'";
	
	turnPage.pageLineNum = 35;			  
    turnPage.queryModal(sql, CheckResultGrid);
}  	
