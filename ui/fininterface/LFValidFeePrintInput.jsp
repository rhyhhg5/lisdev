<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：LFValidFeePrintInput.jsp
		//程序功能：标准保费报表打印
		//创建日期：2008-05-30 16:41:06
		//创建人  ：yanghao
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="LFValidFeePrint.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<%@include file="LFValidFeePrintInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">
			<div>
				<strong>请输入查询的时间范围</strong>
			</div>
			<br />
			<Div id="divFCDay" style="display: ''">
				<table class="common">
					<TR class="common">
						<TD class="title">
							起始时间
						</TD>
						<TD class="input">
							<Input class="coolDatePicker" dateFormat="short" name=StartDay
								verify="起始时间|NOTNULL">
						</TD>
						<TD class="title">
							结束时间
						</TD>
						<TD class="input">
							<Input class="coolDatePicker" dateFormat="short" name=EndDay
								verify="结束时间|NOTNULL">
						</TD>
					</TR>
				</table>
			</Div>

			<br />
			<div style="right:50px;position:absolute;">
				<input class=cssButton type=Button value="可用保费报表打印"
					onclick="printPay()">
			</div>

			<input type=hidden id="fmtransact" name="fmtransact">
		</form>
 
	</body>
</html>
