
<jsp:directive.page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"/>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.fininterface.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.LDFinBankDB"%>
<%@page import="com.sinosoft.lis.fininterface.addbankinfo.AddBankInfoBL"%>

<%
	request.setCharacterEncoding("GBK");
	CErrors tError = new CErrors();
	String FlagStr = "";
	String Content = "";
	String sOperation = "";
	VData tVData = new VData();
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
	
	// 获得前台页面传来的值
	String transact = request.getParameter("dealType");
	String sManageCom = request.getParameter("ManageCom");
	String sBankCode = request.getParameter("BankCode");
	if(sBankCode.indexOf("05") == 0){
		sBankCode = sBankCode.substring(0,2);
		System.out.println("sBankCode:"+sBankCode);
	}
	String sBankName = request.getParameter("BankName");
	String sFinFlag_sp = request.getParameter("FinFlag_sp");
	String sBankAccNo = request.getParameter("BankAccNo");
	String sOperater_sp = request.getParameter("Operater_sp");
	String sCode = request.getParameter("Code");// 科目代码
	String sCodeName = request.getParameter("CodeName");// 科目代码名称
	String sRuFlag = request.getParameter("RuFlag");
	String sRuZhName = request.getParameter("RuZhName");

	
	String Operater = "";// 1是否银保通

	if(sOperater_sp.equals("") || sOperater_sp.equals("N")  ){
		Operater = tGI.Operator;// 如果是N 赋值 操作人员代码
	}else if(sOperater_sp.equals("Y") ){
		Operater = "YBT";// 如果是Y 赋值 "YBT" 
	}
 	// 设置tLDFinBankSchema的值
	LDFinBankSchema tLDFinBankSchema = new LDFinBankSchema();
	tLDFinBankSchema.setManageCom(sManageCom);
	tLDFinBankSchema.setBankCode(sBankCode);
	tLDFinBankSchema.setBankName(sBankName);
	tLDFinBankSchema.setFinFlag(sFinFlag_sp);
	tLDFinBankSchema.setBankAccNo(sBankAccNo);
	tLDFinBankSchema.setOperater(Operater);
	tLDFinBankSchema.setRuFlag(sRuFlag);
	tLDFinBankSchema.setRuZhName(sRuZhName);
	
	// 设置tLDCodeSchema的值
	LDCodeSchema tLDCodeSchema = new LDCodeSchema();
	tLDCodeSchema.setCodeType("accountcode");
	tLDCodeSchema.setOtherSign("1");
	tLDCodeSchema.setCode(sCode);
	tLDCodeSchema.setCodeName(sCodeName);
	
	// 设置tLICodeTransSchema的值
	LICodeTransSchema tLICodeTransSchema = new LICodeTransSchema();
	tLICodeTransSchema.setCodeType("accountcode");
	tLICodeTransSchema.setOtherSign("1");
	tLICodeTransSchema.setCode(sCode);
	tLICodeTransSchema.setCodeName(sCodeName);
	
	// 设置tLIDetailFinItemCodeSchema的值
	LIDetailFinItemCodeSchema tLIDetailFinItemCodeSchema = new LIDetailFinItemCodeSchema();
	String realsCode = sCode.substring(4,4+6);
	tLIDetailFinItemCodeSchema.setFinItemID("0000003"); 
	tLIDetailFinItemCodeSchema.setFinItemlevel("2");
	tLIDetailFinItemCodeSchema.setJudgementNo("02");
	tLIDetailFinItemCodeSchema.setLevelCode(realsCode);
	tLIDetailFinItemCodeSchema.setLevelCondition(sBankAccNo);
	
	//FICodeTransSchema tFICodeTransSchema = new FICodeTransSchema();
	//tFICodeTransSchema.setCodeType("accountcode");
	//tFICodeTransSchema.setOtherSign("1");
	//tFICodeTransSchema.setCode(sCode);
	//tFICodeTransSchema.setCodeName(sCodeName);
	
	//FIDetailFinItemCodeSchema tFIDetailFinItemCodeSchema = new FIDetailFinItemCodeSchema();
	//tFIDetailFinItemCodeSchema.setVersionNo("00000000000000000002");
	//tFIDetailFinItemCodeSchema.setFinItemID("0000003"); 
	//tFIDetailFinItemCodeSchema.setJudgementNo("02");
	//tFIDetailFinItemCodeSchema.setLevelCode(realsCode);
	//tFIDetailFinItemCodeSchema.setLevelConditionValue(sBankAccNo);
	//tFIDetailFinItemCodeSchema.setNextJudgementNo("END");
	//tFIDetailFinItemCodeSchema.setReMark("银行账号");

	
	//System.out.println(sManageCom);
	//System.out.println(sBankCode);
	//System.out.println(sBankName);
	//System.out.println(sFinFlag_sp);
	//System.out.println(sBankAccNo);
	//System.out.println(transact);
	//System.out.println(" 银保通:"+sOperater_sp + " Operater:"+ Operater + " tGI.Operator"+tGI.Operator);

	// 把要处理的对象都存入 VData里去。
	tVData.add(tGI);
	tVData.add(tLDFinBankSchema);
	tVData.add(tLDCodeSchema);
	tVData.add(tLICodeTransSchema);
	tVData.add(tLIDetailFinItemCodeSchema);
	tVData.add(sBankAccNo);
	tVData.add(sCode);
	//tVData.add(tFICodeTransSchema);
	//tVData.add(tFIDetailFinItemCodeSchema);
	
	
	if(transact.equals("insert")){
		sOperation = "插入";
	}	
	else if(transact.equals("delete")){
		sOperation = "删除";
	}
		
	try {
 		System.out.println("开始进行操作：" + transact);
		AddBankInfoBL tAddBankInfoBL = new AddBankInfoBL();// 调用BL处理类进行处理
		if ( !tAddBankInfoBL.submitData(tVData, transact) ) {
			FlagStr = "Fail";
			Content = sOperation + "信息失败，原因是：" + tAddBankInfoBL.mErrors.getFirstError();
			System.out.println("操作失败");
		} else {
			FlagStr = "Succ";
			Content = sOperation + "信息成功";
			System.out.println("操作成功");
		}
	} catch (Exception ex) {
		FlagStr = "Fail";
		Content = "操作失败，原因是: " + ex.getMessage();
		System.out.println(Content);
	}
		
	System.out.println("操作结束");
%>
<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
		parent.fraInterface.initForm();
	</script>
</html>

