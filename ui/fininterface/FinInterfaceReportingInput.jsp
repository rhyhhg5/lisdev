<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：FinInterfaceReportingInput.jsp
//程序功能：财务接口报表查询
//创建日期：2007-01-11 09:30
//创建人  ：lijs
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>  
 
<%
	//添加页面控件的初始化。
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");  
%>   
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script> 
<head>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>  
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="FinInterfaceReporting.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>  
  <%@include file="FinInterfaceReportingInit.jsp"%>
</head>

<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
  <!-- 操作标志 -->
  <input type=hidden name=opt>
  <!-- 类型查询 -->
  <input type=hidden name=QueryType>
　<!-- 流水号码 -->
  <input type=hidden name=serialNo>
  <!-- 日结类型 -->
  <input type=hidden name=othernotype>
  <!-- 日结操作员 -->
  <input type=hidden name=m_sOperator>
  <!-- 开始时间 -->
  <input type=hidden name=sStartDate>
  <!-- 结束时间 -->
  <input type=hidden name=sEndDate>
  <!-- 明细 -->
  <input type=hidden name=sInterfaceInfo>   
	    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSearch);">
    		</td>
    		<td class= titleImg>
    			财务接口报表查询
    		</td>
    	</tr>
    </table>
  	 <Div  id= "divSearch" style= "display: ''"> 
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            起始时间
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=StartDay1 >
          </TD>
          <TD  class= title>
            结束时间
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" verify="结束时间|notnull&date"  dateFormat="short" name=EndDay1 >
          </TD>
        </TR>
      </table> 
      <INPUT VALUE="财务接口报表查询" class = cssButton TYPE=button onclick="queryFinInterface();" >      
    </div> 
  	<Div  id= "divFinInterfaceGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanFinInterfaceGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 					
  </div>
  <br>
  <div id="divButtonInfo" style="display:''" >
	  <table>
		  <tr>	
			<!-- 打印 和到导出 -->
			  <td>
			    <INPUT VALUE="导出Excel" class = cssButton TYPE=button onclick="ToExcel();"> 
				<!--INPUT VALUE="打    印" class = cssButton TYPE=button onclick="printFinInterface();"--> 
			  </td>
		  </tr>
	  </table>
  </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>