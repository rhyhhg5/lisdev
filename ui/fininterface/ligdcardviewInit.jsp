<%
  //程序名称：ligdcardviewInit.jsp
  //程序功能：元数据定义
  //创建日期：2011/11/10
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">

function initForm(){
	try{
		cardGrid();
		fm.ManageCom.value = comcode;
		ManagecomName();
	}
	catch(re){
		alert("ligdcardviewInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function cardGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="流转SAP批次号";
		iArray[1][1]="80px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="SAP机构";
		iArray[2][1]="30px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="核心系统机构";
		iArray[3][1]="50px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="业务类型";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;

		iArray[5]=new Array();
		iArray[5][0]="保单号";
		iArray[5][1]="80px";
		iArray[5][2]=100;
		iArray[5][3]=0;

		iArray[6]=new Array();
		iArray[6][0]="卡单号";
		iArray[6][1]="50px";
		iArray[6][2]=100;
		iArray[6][3]=0;

		iArray[7]=new Array();
		iArray[7][0]="过账日期";
		iArray[7][1]="50px";
		iArray[7][2]=100;
		iArray[7][3]=0;

		iArray[8]=new Array();
		iArray[8][0]="险种";
		iArray[8][1]="30px";
		iArray[8][2]=100;
		iArray[8][3]=0;

		iArray[9]=new Array();
		iArray[9][0]="金额";
		iArray[9][1]="30px";
		iArray[9][2]=100;
		iArray[9][3]=0;

		iArray[10]=new Array();
		iArray[10][0]="凭证号";
		iArray[10][1]="45px";
		iArray[10][2]=100;
		iArray[10][3]=0;
		
		iArray[11]=new Array();
		iArray[11][0]="科目信息";
		iArray[11][1]="45px";
		iArray[11][2]=100;
		iArray[11][3]=0;
		
		iArray[12]=new Array();
		iArray[12][0]="借贷标志";
		iArray[12][1]="45px";
		iArray[12][2]=100;
		iArray[12][3]=0;
		
		iArray[13]=new Array();
		iArray[13][0]="卡单激活日期";
		iArray[13][1]="45px";
		iArray[13][2]=100;
		iArray[13][3]=0;
		
		iArray[14]=new Array();
		iArray[14][0]="卡单生效日期";
		iArray[14][1]="45px";
		iArray[14][2]=100;
		iArray[14][3]=0;


		cardGrid = new MulLineEnter( "fm" , "cardGrid" ); 

		cardGrid.mulLineCount=1;
		cardGrid.displayTitle=1;
		cardGrid.canSel=0;
		cardGrid.canChk=0;
		cardGrid.hiddenPlus=1;
		cardGrid.hiddenSubtraction=1;

		cardGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
