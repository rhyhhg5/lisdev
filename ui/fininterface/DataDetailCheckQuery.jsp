 <%@ page contentType="text/html; charset=GBK" %>
 <%@page import="com.sinosoft.lis.fininterface.*"%>
 <%@page import="com.sinosoft.lis.db.*"%>
 <%@page import="com.sinosoft.lis.vdb.*"%>
 <%@page import = "com.sinosoft.lis.pubfun.*"%>
 <%@page import = "com.sinosoft.utility.*"%>
 <%@page import="java.net.*"%>
 <%@page import="java.util.*"%>
 <%@page import="java.io.*"%>

<%
  System.out.println("------- DataDetailCheckQuery.jsp -------");
  CErrors tError = new CErrors();
  String FlagStr = "";
  String Content = "";
//  String sBatchNo = request.getParameter("mBatchNo");
  String sClassType = request.getParameter("cClassType");
  String strReturnRecord = "";

  GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");

       System.out.println("开始提数");

       try 
       {  
           DataDetailCheckQuery tDataDetailCheckQuery = new DataDetailCheckQuery();
           if(!tDataDetailCheckQuery.CheckDetailData(sClassType))
           {
             FlagStr = "Fail";
             Content = "查询数据失败！"; 
             System.out.println("操作失败！");
           }
          else
          {
          	 strReturnRecord = tDataDetailCheckQuery.getResult();
             FlagStr = "Succ";
             Content = "查询成功！"; 
             System.out.println("操作成功！");
          }
          	                     
       }
       catch(Exception ex) 
       {
       
             FlagStr = "Fail";
             Content="查询失败，原因是: " + ex.getMessage();   
             System.out.println(Content);             
       }  	   
  	   System.out.println("提数结束");

%>
<html>
<script language="javascript">
	parent.fraInterface.showRecord("<%= strReturnRecord %>");
	parent.fraInterface.afterSubmit("<%= FlagStr%>","<%= Content%>");
</script>
</html>

