<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：BusCompetiReportInput.jsp
 //程序功能：业务竞赛报表
 //创建日期：2011/12/19
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>
	<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
  String operator=tGI1.Operator.trim();
  System.out.println("operator-"+operator);
 %>
<script>
  var comcode = "<%=tGI1.ComCode%>";
  var operator="<%=operator%>";
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="BusCompetiReport.js"></SCRIPT>

</head>
<body  onload="initForm();initElementtype();">
<form action="BusCompetiReportSave.jsp" method=post name=fm target="fraSubmit">
<br>
   <table>
    	<tr>
    		 <td class= titleImg>查询条件</td>   		 
    	</tr>
   </table>
	<table  class= common>
       <TR  class= common>
          <TD  class= title>
            保费起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Bdate >
          </TD>
          <TD  class= title>
            保费终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=Edate >
          </TD>
  </tr>
    <TR  class= common>
          <TD  class= title>
            犹豫期退保起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=YBdate >
          </TD>
          <TD  class= title>
            犹豫期退保终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=YEdate >
          </TD>
  </tr>
	</table> 
	<br>
	<table  class= common>
    	<TR  class= common>
			<TD class= title>
				<INPUT class = cssButton name=uploadbutton VALUE="生成报表Excel"  TYPE=button onclick="build();" >  
			</TD>			
			<TD class= title>
			    <INPUT class = cssButton name=uploadbutton VALUE="下载报表Excel"  TYPE=button onclick="download();" >  
			</TD>
			<TD class= input></TD>
			<TD class= title></TD>
			<TD class= input></TD>
		</TR>
    </table>
	<hr></hr>
	<br>
	<h5>说明：(1)请先生成报表Excel，然后再下载报表Excel，若直接点击"下载报表Excel"按钮,下载的报表为上次生成的Excel报表。</h5>
	<input type=hidden name=querysql>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
