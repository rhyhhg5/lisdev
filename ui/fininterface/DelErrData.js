//               该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();         //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";
var Action;
var tRowNo=0;

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
}

function BatchNoQuery() {

  var strSQL = "select distinct b.batchno,count(1),b.managecom,b.operator,varchar(b.startdate)||'至'||varchar(b.enddate) as getinfo,varchar(b.makedate)||' '||varchar(b.maketime) as timeinfo," +
							" case flag when 'SUCC' then '提数成功' else b.filepath end as succflag" +
							" from lidatatransresult a,lidistilllog b" +
							" where a.batchno = b.batchno" +
							 " and   exists (select 1 from interfacetable c where b.batchno = c.batchno and  c.readstate='3' )" +
							" and a.accountdate between '" + fm.all("Bdate").value + "' and '" + fm.all("Edate").value + "'" +
							" and b.managecom like '" + fm.all("cManageCom").value + "%%'" +
							" group by b.batchno,b.filepath,b.managecom,b.operator,b.makedate,b.maketime,b.flag,b.startdate,b.enddate"+
							" having count(1)>0";

  turnPage.queryModal(strSQL, MoveDataGrid);
	if(MoveDataGrid.mulLineCount=="0"){
		alert("没有符合要求的数据！");
	}
} 
 
function SubmitForm()
{
  if(MoveDataGrid.getSelNo()) { 
    fm.all("sBatchNo").value = MoveDataGrid.getRowColData(MoveDataGrid.getSelNo()-1, 1);
    //fm.all("fmtransact").value = "create"    
     var i = 0;
     var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");  
//     fm.target = "_blank";
     fm.submit(); //提交
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}
