<%
	 /************************
	 /*个人日结数据导出保存功能*/
	/*页面:TestInterfaceDataToExcel.jsp*/
	/*create:lijs*/
	/*创建时间:2006 version 1.0*/
	/*******************************/
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
	//在此设置导出Excel的列名，应与sql语句取出的域相对应
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");

	ExportExcel.Format format = new ExportExcel.Format();
	ArrayList listCell = new ArrayList();
	ArrayList listLB = new ArrayList();
	ArrayList listColWidth = new ArrayList();
	format.mListCell = listCell;
	format.mListBL = listLB;
	format.mListColWidth = listColWidth;

	ExportExcel.Cell tCell = null;
	ExportExcel.ListBlock tLB = null;

	String tManageCom = tGlobalInput.ComCode;
	String tSerialNo = request.getParameter("serialNo");
	//String tOtherNoType = request.getParameter("othernotype");
	//System.out.println("tSerialNo===" + tSerialNo + "===tOtherNoType==" + tOtherNoType);

	listColWidth.add(new String[] { "0", "5000" });
	String sql = "";
	if (tSerialNo != null && !tSerialNo.equals("")) {
		//if(tOtherNoType.equals("")){  //收费操作
		//	sql = "select a.tempfeeno, "
		//		+ " case (select distinct tempfeetype from ljtempfee where tempfeeno = a.tempfeeno) "
		//		+ " when '1' then '新单' when '4' then '保全' end as tempfee,"
		//		+ " case a.paymode when '1' then '现金' when '2' then '现金支票' when '3' then '转帐支票' when '6' then '进账单' end,"
		//		+ " a.otherno,a.paymoney,a.EnterAccDate,a.paydate"
		//		+ " from ljtempfeeclass a "
		//		+ " where a.tempfeeno in(select otherno from ljfindaylog where FINDAYID = '"
		//		+ tSerialNo + "') order by tempfee,a.paymode";
		//}else{   //付费操作
			sql = " Select AccountDate,(select b.classname from lioperationdataclassdef b where a.classid=b.classid),managecom,AccountCode,RiskCode," +
				  " SaleChnl,to_char(SumMoney,'FM999999999990.00')," +
      			  " keyunionvalue" +
				  " From lidatatransresult a Where batchno = '" + tSerialNo + "' order by classid";
		//}
	} else{
		System.out.println("没有得到相关的号码");
	}
    /***排序通过前台传过来的OTHERNOTYPE　来判断用什么排序***/
	tLB = new ExportExcel.ListBlock("001");
	//if(tOtherNoType.equals("f")){
    tLB.colName = new String[] { "业务日期", "业务类型", "机构", "科目代码",
			//"科目明细代码", 
			"险种","渠道","金额","业务线索信息(收据号,收费方式,保单号等)" };
	//}else{
	//    tLB.colName = new String[] { "实付号码", "付费方式", "付费类型", "关联号码", "金额",
	//			"到账日期", "付费日期" };		
	//}
	tLB.sql = sql;
	tLB.row1 = 0;
	tLB.col1 = 0;
	tLB.InitData();
	listLB.add(tLB);

	try {

		response.reset();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition",
		"attachment; filename=TestInterfaceBatchNo_List.xls");

		OutputStream outOS = response.getOutputStream();
		BufferedOutputStream bos = new BufferedOutputStream(outOS);

		ExportExcel excel = new ExportExcel();
		excel.write(format, bos);
		bos.flush();
		bos.close();
	} catch (Exception e) {
		System.out.println("导出Excel失败！");
	}
%>
