<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2015-05-19 
		//创建人  ：
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="JYReturnDetailInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel="stylesheet" type="text/css">
		<%@include file="JYReturnDetailInit.jsp"%>
	</head>
	<body onload="initForm();">
		<form method=post name=fm target="fraSubmit">

				<strong><IMG id="a1" src="../common/images/butExpand.gif"
						style="cursor:hand;" OnClick="showPage(this,divOperator);" />结余返还统计明细表</strong>

			<Div id="divOperator" style="display: ''">
			   <font color="#FF0000" size="3">
    		   注意：因数据量较大原因，可能导致报表打印时间较长，请耐心等待，不要重复点击打印按钮！
    		   </font>
    		    <p>
				<strong>输入查询的时间范围</strong>
				<Div id="divFCDay" style="display: ''">
					<table class="common">
						<TR class=common>
							<TD class=title>
								起始时间
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="StartDate"
									name="StartDate" verify="起始时间|NOTNULL">
							</TD>
							<TD class=title>
								结束时间
							</TD>
							<TD class=input>
								<Input class="coolDatePicker" dateFormat="short" id="EndDate"
									name="EndDate" verify="结束时间|NOTNULL">
							</TD>
    		
						</TR>
					</table>
				</Div>
<p>
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="SSPrint" value="预估结余返还统计明细表" onclick="FMPrintX()" />
				<br />
				<br />
				
				<input class="cssButton" style="margin-left:100px;width:200px"
					type="Button" name="SFPrint" value="实际结余返还统计明细表" onclick="printPayX()" />
				<br />
				<br />
				
			</Div>

          <input type="hidden" name="fmtransact" value="">
		</form>

		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>
