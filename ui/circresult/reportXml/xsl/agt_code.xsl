<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 中介机构代码 -->  
  <xsl:value-of select="AGT_CODE"/>
  <xsl:value-of select="'|'"/>  
  <!-- 中介机构名称 -->
  <xsl:value-of select="AGT_NAME"/>
  <xsl:value-of select="'|'"/>  
  <!-- 机构性质 -->
  <xsl:value-of select="AGT_ORG_TYPE"/>
  <xsl:value-of select="'|'"/>  
  <!-- 经营保险中介业务许可证号 -->
  <xsl:value-of select="AGT_BUSI_NUM"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>