<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 业务员代码 -->  
  <xsl:value-of select="AGENTNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 业务员姓名 -->
  <xsl:value-of select="AGENT_NAME"/>
  <xsl:value-of select="'|'"/>  
  <!-- 保险代理从业资格证号 -->
  <xsl:value-of select="QUANO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 展业证书号 -->
  <xsl:value-of select="PROFNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 身份证号 --> 
  <xsl:value-of select="IDNO"/> 
  <xsl:value-of select="'|'"/>
  
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>