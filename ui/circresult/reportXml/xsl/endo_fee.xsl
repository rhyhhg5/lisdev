<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 所属省份 -->
  <xsl:value-of select="DEPT_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 批单号 -->
  <xsl:value-of select="ENTNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 个团标志 -->
  <xsl:value-of select="GP_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 保单号 -->  
  <xsl:value-of select="POLNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 分单号 -->
  <xsl:value-of select="CERTNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 险种序号 --> 
  <xsl:value-of select="BRNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 险种代码 --> 
  <xsl:value-of select="PLAN_CODE"/>
  <xsl:value-of select="'|'"/>
  <!-- 保单年度 -->
  <xsl:value-of select="POL_YR"/>
  <xsl:value-of select="'|'"/>
  <!-- 保险期限（月） -->
  <xsl:value-of select="PERIOD"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴别 -->
  <xsl:value-of select="PREM_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 缴费期限（月） -->
  <xsl:value-of select="PREM_TERM"/>
  <xsl:value-of select="'|'"/>
  <!-- 业务来源类别 --> 
  <xsl:value-of select="BUSI_SRC_TYPE"/> 
  <xsl:value-of select="'|'"/>
   <!-- 中介机构代码 -->
  <xsl:value-of select="AGT_CODE"/>
  <xsl:value-of select="'|'"/>
  <!-- 业务员代码 -->
  <xsl:value-of select="AGENTNO"/>
  <xsl:value-of select="'|'"/>
  <!-- 批改类型 -->
  <xsl:value-of select="POS_TYPE"/>
  <xsl:value-of select="'|'"/>
  <!-- 收付类型 -->
  <xsl:value-of select="AMT_TYPE"/>
  <xsl:value-of select="'|'"/>  
  <!-- 币种 -->
  <xsl:value-of select="CURNO"/>
  <xsl:value-of select="'|'"/>  
  <!-- 实发生金额折合人民币 -->
  <xsl:value-of select="AMT_INCURED_CNVT"/>
  <xsl:value-of select="'|'"/>
  <!-- 批改后份数 -->
  <xsl:value-of select="UNITS"/>
  <xsl:value-of select="'|'"/>
  <!-- 批改后保额 -->
  <xsl:value-of select="SUM_INS"/>
  <xsl:value-of select="'|'"/>
  <!-- 发生日期 -->
  <xsl:value-of select="PROC_DATE"/>
  <xsl:value-of select="'|'"/> 
  <!-- 到帐日期 -->
  <xsl:value-of select="GAINED_DATE"/>
  <xsl:value-of select="'|'"/>

  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>