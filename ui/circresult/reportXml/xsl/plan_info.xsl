<?xml version="1.0" encoding="GBK"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="GBK"/>

<xsl:template match="/">

<xsl:for-each select="POLDATA/ROW">

  <!-- 险种类别 -->  
  <xsl:value-of select="PLAN_TYPE"/>
  <xsl:value-of select="'|'"/>  
  <!-- 代码 -->
  <xsl:value-of select="PLAN_CODE"/>
  <xsl:value-of select="'|'"/>  
  <!-- 全称 -->
  <xsl:value-of select="FULL_NAME"/>
  <xsl:value-of select="'|'"/>  
  <!-- 简称 -->
  <xsl:value-of select="ABBR_NAME"/>
  <xsl:value-of select="'|'"/>
  <!-- 期限类别 --> 
  <xsl:value-of select="PLAN_PERIOD"/> 
  <xsl:value-of select="'|'"/>
  <!-- 是否分红保险 -->
  <xsl:value-of select="IS_DIVIDEND"/>
  <xsl:value-of select="'|'"/>
  <!-- 是否储金险种 -->
  <xsl:value-of select="IS_DEPO"/>
  <xsl:value-of select="'|'"/>
  <!-- 是否法定 -->
  <xsl:value-of select="ISLEGAL"/>
  <xsl:value-of select="'|'"/>
  <!-- 是否年金型 -->
  <xsl:value-of select="IS_ANNU"/>
  <xsl:value-of select="'|'"/>
  <!-- 是否有效 -->
  <xsl:value-of select="ISVALID"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>