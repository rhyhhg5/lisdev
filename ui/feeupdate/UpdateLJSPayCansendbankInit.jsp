<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：UpdateFanChongTempFeeTypeInit.jsp
//程序功能：暂收查询
//创建日期：20170112
//创建人  ：ys
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）


  function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('Otherno').value = '';
  }
  catch(ex)
  {
    alert("在UpdateFanChongTempFeeTypeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在UpdateFanChongTempFeeTypeInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                         

function initForm()
{
  try
  {
    initInpBox();   
    initSelBox();   
    initLJSPayCansendbankGrid();
   /*  initPolStatuGrid() */
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initLJSPayCansendbankGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="通知书号码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="投保人客户号码";         		//列名
      iArray[2][1]="40px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="其他号";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      

      iArray[4]=new Array();
      iArray[4][0]="其他号类型";         		//列名
      iArray[4][1]="40px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     

      iArray[5]=new Array();
      iArray[5][0]="允许发送银行标记";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="银行帐号";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="管理机构";         		//列名
      iArray[7][1]="60px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      LJSPayCansendbankGrid = new MulLineEnter( "fm" , "LJSPayCansendbankGrid" ); 
      //这些属性必须在loadMulLine前
      LJSPayCansendbankGrid.mulLineCount =1;   
      LJSPayCansendbankGrid.displayTitle = 1;
      LJSPayCansendbankGrid.locked = 1;
      LJSPayCansendbankGrid.canSel = 1;
      LJSPayCansendbankGrid.hiddenPlus = 1;
      LJSPayCansendbankGrid.hiddenSubtraction = 1;
      LJSPayCansendbankGrid.canChk = 0;
      LJSPayCansendbankGrid.selBoxEventFuncName = "LJSPayCansendbankinit";
      LJSPayCansendbankGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>