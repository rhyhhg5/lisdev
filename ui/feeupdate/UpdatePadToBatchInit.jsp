<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--用户校验类-->
<%
	GlobalInput globalInput = (GlobalInput) session.getValue("GI");
	String strManageCom = globalInput.ManageCom;
	String strOperator = globalInput.Operator;
%>

<script language="JavaScript">
	function initForm() {
		try {
			initInpBox();
			initPadGrid();
			//showAllCodeName();
		} catch (re) {
			alert("updatePadToBatchInit.jsp-->InitForm 函数中发生异常:初始化界面错误!");
		}
	}
	
	function initInpBox() {
		try {
			// 初始化
			fm.all('GetnodiceNo').value = '';
		} catch (ex) {
			alert("updatePadToBatchInit.jsp-->InitInputBox 函数中发生异常:初始化界面错误!");
		}
	}

	var BankGrid; //定义为全局变量，Multiline使用
	// 列表的初始化
	function initPadGrid() {
		var iArray = new Array();

		try {
			iArray[0] = new Array();
			iArray[0][0] = "序号"; //列名
			iArray[0][1] = "30px"; //列宽
			iArray[0][2] = 10; //列最大值
			iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[1] = new Array();
			iArray[1][0] = "应收号"; //列名
			iArray[1][1] = "60px"; //列宽
			iArray[1][2] = 10; //列最大值
			iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[2] = new Array();
			iArray[2][0] = "业务号"; //列名
			iArray[2][1] = "100px"; //列宽
			iArray[2][2] = 100; //列最大值
			iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[3] = new Array();
			iArray[3][0] = "是否在途"; //列名
			iArray[3][1] = "60px"; //列宽
			iArray[3][2] = 100; //列最大值
			iArray[3][3] = 0; //是否允许输入,1表示允许，0表示不允许

			iArray[4] = new Array();
			iArray[4][0] = "能否发送银行"; //列名
			iArray[4][1] = "100px"; //列宽
			iArray[4][2] = 100; //列最大值
			iArray[4][3] = 0; //是否允许输入,1表示允许，0表示不允许

			PadGrid = new MulLineEnter("fm", "PadGrid");

			
			//这些属性必须在loadMulLine前
			PadGrid.mulLineCount = 0;
			PadGrid.displayTitle = 1;// 显示标题
			PadGrid.locked = 1;
			PadGrid.canSel = 1;// 显示单选框
			PadGrid.hiddenPlus = 1;
			PadGrid.selBoxEventFuncName = "GetnodiceNoInit";// 单击单选框时,响应 js 函数
			PadGrid.hiddenSubtraction = 1;// 不显示 - 号按钮

			PadGrid.loadMulLine(iArray);

		} catch (ex) {
			alert(ex);
		}
	}
</script>