var arrDataSet
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();
//var PolNo;

window.onfocus = myonfocus;

// 点击查询按钮触发
function easyQueryClick() {
	initBankGrid();
	if (!verifyInput2())// 检验之后将光标置于错误出，并且颜色变黄
		return false;
	if (fm.BankCodeNo.value == "") {
		alert("请输入银行编码！");
		return false;
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL="select BankUniteCode,BankUniteName,BankCode,UniteBankCode,UniteBankName from ldbankunite where bankunitecode in ('7706','7705') " 
				+ getWherePart('bankcode', 'BankCodeNo');
	turnPage.queryModal(strSQL,BankGrid); 
	return true;
}

// 初始化查询数据
function BankCodeInit() {
	var i = 0;
	var checkFlag = 0;

	for (i = 0; i < BankGrid.mulLineCount; i++) {
		if (BankGrid.getSelNo(i)) {
			checkFlag = BankGrid.getSelNo();//选中一条信息
			break;
		}
	}
	if (checkFlag) {
		
		var beforebankUniteCode = BankGrid.getRowColData(checkFlag - 1, 1);
		var beforeBankUniteName = BankGrid.getRowColData(checkFlag - 1, 2);
		fm.all('afterBankUniteCode').value = beforebankUniteCode;
		fm.all('afterBankUniteName').value = beforeBankUniteName;
	} else {
		alert("请先选择一条信息！");
	}
}

function updateBankUniteCode() {
	var mSelNo = BankGrid.getSelNo();// 判断该行的 Radio 单选框被选中,行号是从1开始,如果没有选中行,返回值是0
	if (mSelNo == 0) {
		alert("请选择一条数据！");
		return false;
	}
	var afterBankUniteCode = "";
	afterBankUniteCode = fm.afterBankUniteCode.value;
	
	if (afterBankUniteCode == "") {
		alert("请选择要修改银联编号！");
		return false;
	}
	fm.fmtransact.value = "UPDATE";
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function afterSubmit(FlagStr, content) {
	easyQueryClick();
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showContInfo();
	}
}
