<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ScanContInit.jsp
//程序功能：个单新契约扫描件保单录入
//创建日期：2004-12-22 11:10:36
//创建人  ：HYQ
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
<%-- function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
    fm.all('PrtNo').value = '';
		fm.all('InputDate').value = "<%=strCurDay%>";    
  }
  catch(ex)
  {
    alert("在GroupUWAutoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
} --%>            

function initForm()
{
  try
  {
	  initSpayGrid();
	  initSendbankGrid();
	  initContGrid();
	  initTempGrid();
	  initAgetGrid();
	  initBanklogGrid();
	  initSendtobankGrid();
  }
  catch(re)
  {
    alert("ProposalQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 总应收信息列表的初始化
function initSpayGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="通知书号";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="其他号码";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="账户名";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="总应收金额";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="60px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      SpayGrid = new MulLineEnter( "fm" , "SpayGrid" ); 

      //这些属性必须在loadMulLine前
      SpayGrid.mulLineCount = 0;   
      SpayGrid.displayTitle = 1;
      SpayGrid.locked = 1;
      SpayGrid.canSel = 1;
      SpayGrid.canChk = 0;
      SpayGrid.hiddenSubtraction = 1;
      SpayGrid.hiddenPlus = 1;
      SpayGrid.loadMulLine(iArray);

      
      
      //这些操作必须在loadMulLine后面
      //GrpGrid1.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }
// 保单信息列表的初始化
function initContGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="印刷号";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="银行账户名";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="保费";         		//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="管理机构";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 


      ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 

      //这些属性必须在loadMulLine前
      ContGrid.mulLineCount = 0;   
      ContGrid.displayTitle = 1;
      ContGrid.locked = 1;
      ContGrid.canSel = 0;
      ContGrid.canChk = 0;
      ContGrid.hiddenSubtraction = 1;
      ContGrid.hiddenPlus = 1;
      ContGrid.loadMulLine(iArray);

      
      
      //这些操作必须在loadMulLine后面
      //GrpGrid1.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
//保单信息列表的初始化
function initSendbankGrid()
  {     
                             
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";         		//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="缴退费号";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="账户名";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="管理机构";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 


      SendbankGrid = new MulLineEnter( "fm" , "SendbankGrid" ); 

      //这些属性必须在loadMulLine前
      SendbankGrid.mulLineCount = 0;   
      SendbankGrid.displayTitle = 1;
      SendbankGrid.locked = 1;
      SendbankGrid.canSel = 1;
      SendbankGrid.canChk = 0;
      SendbankGrid.hiddenSubtraction = 1;
      SendbankGrid.hiddenPlus = 1;
      SendbankGrid.loadMulLine(iArray);

      
      
      //这些操作必须在loadMulLine后面
      //GrpGrid1.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
  }
      function initTempGrid()
      {     
                                 
        var iArray = new Array();
          
          try
          {
          iArray[0]=new Array();
          iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
          iArray[0][1]="30px";            		//列宽
          iArray[0][2]=10;            			//列最大值
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[1]=new Array();
          iArray[1][0]="暂交费收据号";         		//列名
          iArray[1][1]="120px";            		//列宽
          iArray[1][2]=170;            			//列最大值
          iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[2]=new Array();
          iArray[2][0]="交费金额";         		//列名
          iArray[2][1]="120px";            		//列宽
          iArray[2][2]=100;            			//列最大值
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[3]=new Array();
          iArray[3][0]="账户名";         		//列名
          iArray[3][1]="100px";            		//列宽
          iArray[3][2]=200;            			//列最大值
          iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[4]=new Array();
          iArray[4][0]="管理机构";         		//列名
          iArray[4][1]="80px";            		//列宽
          iArray[4][2]=100;            			//列最大值
          iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
          

          TempGrid = new MulLineEnter( "fm" , "TempGrid" ); 
          //这些属性必须在loadMulLine前
          TempGrid.mulLineCount = 0;   
          TempGrid.displayTitle = 1;
          TempGrid.locked = 1;
          TempGrid.canSel = 1;
          TempGrid.canChk = 0;
          TempGrid.hiddenSubtraction = 1;
          TempGrid.hiddenPlus = 1;
          TempGrid.loadMulLine(iArray);  
          
          
          //这些操作必须在loadMulLine后面
          //GrpGrid.setRowColData(1,1,"asdf");
          }
          catch(ex)
          {
            alert(ex);        
          }
      }
      function initAgetGrid()
      {     
                                 
        var iArray = new Array();
          
          try
          {
          iArray[0]=new Array();
          iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
          iArray[0][1]="30px";            		//列宽
          iArray[0][2]=10;            			//列最大值
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[1]=new Array();
          iArray[1][0]="实付号码";         		//列名
          iArray[1][1]="120px";            		//列宽
          iArray[1][2]=170;            			//列最大值
          iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[2]=new Array();
          iArray[2][0]="其它号码";         		//列名
          iArray[2][1]="120px";            		//列宽
          iArray[2][2]=100;            			//列最大值
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[3]=new Array();
          iArray[3][0]="总给付金额";         		//列名
          iArray[3][1]="80px";            		//列宽
          iArray[3][2]=200;            			//列最大值
          iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[4]=new Array();
          iArray[4][0]="管理机构";         		//列名
          iArray[4][1]="100px";            		//列宽
          iArray[4][2]=100;            			//列最大值
          iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 


          AgetGrid = new MulLineEnter( "fm" , "AgetGrid" ); 

          //这些属性必须在loadMulLine前
          AgetGrid.mulLineCount = 0;   
          AgetGrid.displayTitle = 1;
          AgetGrid.locked = 1;
          AgetGrid.canSel = 0;
          AgetGrid.canChk = 0;
          AgetGrid.hiddenSubtraction = 1;
          AgetGrid.hiddenPlus = 1;
          AgetGrid.loadMulLine(iArray);

          
          
          //这些操作必须在loadMulLine后面
          //GrpGrid1.setRowColData(1,1,"asdf");
          }
          catch(ex)
          {
            alert(ex);
          }
    }
      function initBanklogGrid()
      {     
                                 
        var iArray = new Array();
          
          try
          {
          iArray[0]=new Array();
          iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
          iArray[0][1]="30px";            		//列宽
          iArray[0][2]=10;            			//列最大值
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[1]=new Array();
          iArray[1][0]="批次号";         		//列名
          iArray[1][1]="120px";            		//列宽
          iArray[1][2]=170;            			//列最大值
          iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[2]=new Array();
          iArray[2][0]="总数量";         		//列名
          iArray[2][1]="120px";            		//列宽
          iArray[2][2]=100;            			//列最大值
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[3]=new Array();
          iArray[3][0]="总金额";         		//列名
          iArray[3][1]="80px";            		//列宽
          iArray[3][2]=200;            			//列最大值
          iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[4]=new Array();
          iArray[4][0]="机构编码";         		//列名
          iArray[4][1]="100px";            		//列宽
          iArray[4][2]=100;            			//列最大值
          iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 


          BanklogGrid = new MulLineEnter( "fm" , "BanklogGrid" ); 

          //这些属性必须在loadMulLine前
          BanklogGrid.mulLineCount = 0;   
          BanklogGrid.displayTitle = 1;
          BanklogGrid.locked = 1;
          BanklogGrid.canSel = 0;
          BanklogGrid.canChk = 0;
          BanklogGrid.hiddenSubtraction = 1;
          BanklogGrid.hiddenPlus = 1;
          BanklogGrid.loadMulLine(iArray);

          
          
          //这些操作必须在loadMulLine后面
          //GrpGrid1.setRowColData(1,1,"asdf");
          }
          catch(ex)
          {
            alert(ex);
          }
    }
      function initSendtobankGrid()
      {     
                                 
        var iArray = new Array();
          
          try
          {
          iArray[0]=new Array();
          iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
          iArray[0][1]="30px";            		//列宽
          iArray[0][2]=10;            			//列最大值
          iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[1]=new Array();
          iArray[1][0]="批次号";         		//列名
          iArray[1][1]="120px";            		//列宽
          iArray[1][2]=170;            			//列最大值
          iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[2]=new Array();
          iArray[2][0]="缴退费号";         		//列名
          iArray[2][1]="120px";            		//列宽
          iArray[2][2]=100;            			//列最大值
          iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[3]=new Array();
          iArray[3][0]="缴退金额";         		//列名
          iArray[3][1]="80px";            		//列宽
          iArray[3][2]=200;            			//列最大值
          iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

          iArray[4]=new Array();
          iArray[4][0]="管理机构";         		//列名
          iArray[4][1]="100px";            		//列宽
          iArray[4][2]=100;            			//列最大值
          iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 


          SendtobankGrid = new MulLineEnter( "fm" , "SendtobankGrid" ); 

          //这些属性必须在loadMulLine前
          SendtobankGrid.mulLineCount = 0;   
          SendtobankGrid.displayTitle = 1;
          SendtobankGrid.locked = 1;
          SendtobankGrid.canSel = 0;
          SendtobankGrid.canChk = 0;
          SendtobankGrid.hiddenSubtraction = 1;
          SendtobankGrid.hiddenPlus = 1;
          SendtobankGrid.loadMulLine(iArray);

          
          
          //这些操作必须在loadMulLine后面
          //GrpGrid1.setRowColData(1,1,"asdf");
          }
          catch(ex)
          {
            alert(ex);
          }
      }
</script>