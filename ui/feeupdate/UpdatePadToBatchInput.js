var arrDataSet
var showInfo;
var mDebug = "0";
var turnPage = new turnPageClass();

window.onfocus = myonfocus;

// 点击查询按钮触发
function easyQueryClick() {
	initPadGrid();
	if (fm.GetnodiceNo.value == "") {
		alert("请输入应收号！");
		return false;
	}
	// 书写SQL语句
	var strSQL = "";
	strSQL="select getnoticeno,otherno,bankonthewayflag,cansendbank  from ljspay where 1=1 " 
				+ getWherePart('getnoticeno', 'GetnodiceNo');
	turnPage.queryModal(strSQL,PadGrid); 
	return true;
}

// 初始化查询数据
function GetnodiceNoInit() {
	var i = 0;
	var checkFlag = 0;

	for (i = 0; i < PadGrid.mulLineCount; i++) {
		if (PadGrid.getSelNo(i)) {
			checkFlag = PadGrid.getSelNo();//选中一条信息
			break;
		}
	}
	if (checkFlag) {
		
		var beforeNo = PadGrid.getRowColData(checkFlag - 1, 4);
		fm.all('afterNo').value = beforeNo;
	} else {
		alert("请先选择一条信息！");
	}
}

function update() {
	
	    if(fm.afterNo1.value == "")
	    {
	        alert("修改后数据不能为空");
	        return false;
	    }
	    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	    
	    fm.action = "./UpdatePadToBatchSave.jsp";
	    fm.submit();
	   // alert("12212121");

}


function afterImportSubmit(FlagStr, Content)
{
	//alert("afterImportSubmit");
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("修改失败，请尝试重新进行修改。");
    }
}


