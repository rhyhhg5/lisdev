<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>
    var manageCom = "<%=tGI.ManageCom%>";// 记录管理机构
    var comcode = "<%=tGI.ComCode%>";// 记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<!-- 通用录入校验要是用的 js 文件 -->
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<!-- 使用双击下拉需要引入的 JS 文件 -->
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<title>财务续费修改收费类型</title>
<!-- 自己的输入验证 js -->
<script src="UpdateTempFeeTypeInput.js"></script>
<!-- 初始化 mulline -->
<%@include file="UpdateTempFeeTypeInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="UpdateTempFeeTypeSave.jsp" method="post" name="fm" target="fraSubmit">
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>请输入查询条件：</td>
			</tr>
		</table>

		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>暂 交 费 收 据 号 码 :</td>
				<td class=input><input class=common name=TempFeeNo /></td>
			</tr>
		</table>
		<INPUT VALUE="查询暂缴费信息" class="cssButton" TYPE=button onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common><IMG src="../common/images/butExpand.gif" style="cursor: hand;" OnClick="showPage(this,divLCPol1);"></td>
				<td class=titleImg>暂缴费信息</td>
			</tr>
		</table>
		<div id="divLCPol1" style="display: ''">
			<table class=common>
				<TR class=common>
					<td text-align: left colSpan=1>
						<span id="spanPolGrid"></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
		<br />
		<table class=common border=0 width=100%>
			<tr class=common>
				<td class=title>修改前暂交费收据类型</td>
				<td class=input>
					<Input class=readonly readOnly=true name=beforeTempFeeType>
				</td>
				<td class=title>修改前其它号码类型</td>
				<td class=input>
					<Input class=readonly readOnly=true name=beforeOtherNoType>
				</td>
			</tr>
			<tr class=common>
				<td class=title>修改后暂交费收据类型</td>
				<td class=input>
					<Input class=codeNo readonly=true name=afterTempFeeType verify="暂交费类型|code:TempFeeType"
					ondblclick="return showCodeList('TempFeeType',[this,afterTempFeeTypeName],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('TempFeeType',[this,afterTempFeeTypeName],[0,1]);"><input
					class=codename name=afterTempFeeTypeName readonly=true>
				</td>
				<td class=title>修改后其它号码类型</td>
				<td class=input>
					<Input class=codeNo readonly=true name=afterOtherNoType verify="暂交费类型|code:OtherNoType"
					ondblclick="return showCodeList('OtherNoType',[this,afterOtherNoTypeName],[0,1],null,null,null,1);"
					onkeyup="return showCodeListKey('OtherNoType',[this,afterOtherNoTypeName],[0,1]);"><input
					class=codename name=afterOtherNoTypeName readonly=true>
				</td>
			</tr>
		</table>
		<INPUT VALUE="修改暂交费类型" class="cssButton" TYPE=button onclick="updateTempFeeType();">
		<input type=hidden id="fmtransact" name="fmtransact">
	</form>
	<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
