var showInfo;
/**
 * 集中代收付发盘异常处理
 */
var turnPage = new turnPageClass();
function inquiry(){
	if(fm.serialno.value == ""){
		alert("批次号不能为空！！！");
		return false;
	}
	if(fm.paycode.value == ""){
		alert("付费号不能为空！！！");
		return false;
	}
	initSpayGrid();
	initContGrid();
	initSendbankGrid();
	initTempGrid();
	var sqls = "select getnoticeno,otherno,accname,sumduepaymoney,managecom from ljspay where getnoticeno='"+fm.paycode.value+"'";
	var arrResult = easyExecSql(sqls);
	if(arrResult==null){
		alert("找不到该信息，请检查收费号是否正确！！");
		return false;
	}
	var sqlc = "select prtno,appntname,prem,managecom from lccont where prtno ='"+arrResult[0][1]+"'";
	var sqlb = "select serialno,paycode,accname,comcode from lysendtobank where serialno ='"+fm.serialno.value+"' and paycode='"+fm.paycode.value+"'";
	var sqlt = "select tempfeeno,paymoney,accname,managecom from ljtempfeeclass where tempfeeno='"+fm.paycode.value+"'";
	turnPage.queryModal(sqlb,SendbankGrid);
	turnPage.queryModal(sqls,SpayGrid);
	turnPage.queryModal(sqlc,ContGrid);
	turnPage.queryModal(sqlt,TempGrid);
	return true;
}
function update(){
	var i = 0;
	var checks = 0;
	var checkb = 0;
	var checkt = 0;
	  for (i=0; i<SpayGrid.mulLineCount; i++)
	  {
	    if (SpayGrid.getSelNo(i))
	    {
	      checks = SpayGrid.getSelNo();
	      break;
	    }
	  }
	  for (i=0; i<SendbankGrid.mulLineCount; i++)
	  {
	    if (SendbankGrid.getSelNo(i))
	    {
	      checkb = SendbankGrid.getSelNo();
	      break;
	    }
	  }
	  for (i=0; i<TempGrid.mulLineCount; i++)
	  {
	    if (TempGrid.getSelNo(i))
	    {
	      checkt = TempGrid.getSelNo();
	      break;
	    }
	  }
	  if (checks || checkb)
	  {
		  if(checks){
			  if(checkb){
				  fm.tFlag.value = 3;//应收和发盘表都选中
				 /* var getnoticeno = SpayGrid.getRowColData(checks - 1, 1);
				  var serialno = SendbankGrid.getRowColData(checkb - 1, 1);
				  var paycode = SendbankGrid.getRowColData(checkb - 1, 2);*/
			  }
			  fm.tFlag.value = 2;//只选中应收
			  /*var getnoticeno = SpayGrid.getRowColData(checks - 1, 1); */
		  }else{
			  fm.tFlag.value = 1;//只选中发盘表
			  /*var serialno = SendbankGrid.getRowColData(checkb - 1, 1);
			  var paycode = SendbankGrid.getRowColData(checkb - 1, 2);*/
		  }
	  }else{
		  if(checkt){//只选中暂收表flag不赋值
			  
		  }else{
			  alert("最少选择一条信息修改！！！")
			  return false;   
		  }
		  
	  }
	  if (checkt)
	  {
		  var tempfeeno = TempGrid.getRowColData(checkt - 1, 1);
		  fm.temp.value = 1;//选中暂收表temp赋值
	  } 
   if(ContGrid.getRowColData(0, 1)==null || ContGrid.getRowColData(0, 1)==""){
	   alert("查询不到信息，请检查收费号是否正确！！");
	   return false;
   }
   var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		showContInfo();
	}	
}
function inquiry1(){
	
	if(fm.serialno1.value == ""){
		alert("批次号不能为空！！！");
		return false;
	}
	if(fm.paycode1.value == ""){
		alert("付费号不能为空！！！");
		return false;
	}
	initAgetGrid();
	initBanklogGrid();
	initSendtobankGrid();
	var sqla = "select actugetno,otherno,sumgetmoney,managecom from ljaget where actugetno='"+fm.paycode1.value+"'";
	var sqlb = "select serialno,totalnum,totalmoney,comcode from lybanklog where serialno='"+fm.serialno1.value+"'";
	var sqls = "select serialno,paycode,paymoney,comcode from lysendtobank where serialno='"+fm.serialno1.value+"' and paycode='"+fm.paycode1.value+"'";
	turnPage.queryModal(sqla,AgetGrid);
	turnPage.queryModal(sqlb,BanklogGrid);
	turnPage.queryModal(sqls,SendtobankGrid);
	return true;
	
}
function update1(){
	inquiry1();
	if(AgetGrid.getRowColData(0, 1) =="" ){
		alert("没有查询到数据，请检查付款号是否正确！！");
		return false;
	}
	if(BanklogGrid.getRowColData(0, 1) ==""){
		alert("没有查询到数据，请检查批次号是否正确！！");
		return false;
	}
	if(SendtobankGrid.getRowColData(0, 1) ==""){
		alert("没有查询到数据，请检查批次号或付款号是否正确！！");
		return false;
	}
	fm.tFlag.value = 0;
	var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}