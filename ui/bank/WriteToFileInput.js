//WriteToFileInput.js该文件中包含客户端需要处理的函数和事件

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;
var mDebug = "0";
var tSelNo = "";
var filePath = "";
var strBankCode ="";
//提交，保存按钮对应操作
function submitForm() {
  //if(verifyInput() == false) return false;  
  
    var sql = ""
    
  	if(dealType == "S"){
  		sql = "select 1 from ldbank where bankcode = (select bankcode from lybanklog where serialno='" 
    		+ BankGrid.getRowColData(BankGrid.getSelNo()-1, 2) 
    		+ "') and agentpaysendf like '%.xml'"
  	} else {
  		sql = "select 1 from ldbank where bankcode = (select bankcode from lybanklog where serialno='" 
    		+ BankGrid.getRowColData(BankGrid.getSelNo()-1, 2) 
    		+ "') and agentgetsendf like '%.xml'"
  	}
    
    var arrResult = easyExecSql(sql);
  
	if(arrResult){
  		fm.action = "WriteToExcelSave.jsp";
	} else {
  		fm.action = "WriteToFileSave.jsp";
	}
  
  if (BankGrid.getSelNo()) { 
    fm.all("serialNo").value = BankGrid.getRowColData(BankGrid.getSelNo()-1, 2);
    fm.all("fmtransact").value = "create";
    //fm.all('downfilesum').value = BankGrid.getRowColData(BankGrid.getSelNo()-1, 3);

    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    //showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.submit(); //提交
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  //try { showInfo.close(); } catch(e) {}
  
  //var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  //showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

//初始化查询
function initQuery() {
  var strSql = "select BankCode, SerialNo, OutFile,senddate,makedate from LYBankLog where InFile is null and ComCode like '" + comCode + "%%'"
             + " and LogType='" + dealType + "'" + " order by SerialNo desc";
  
  turnPage.queryModal(strSql, BankGrid);
} 

//选择进行文件下载
function fileDownload() {
   
  if (BankGrid.getSelNo()) { 
  	
  	 strBankCode = BankGrid.getRowColData(BankGrid.getSelNo()-1, 1);

    if (strBankCode=="9995" || strBankCode=="9595")
    {
    	alert("银联下载请点击“汇总文件下载”和“明细文件下载”!");
    	return;
    }
  	
    fm.all('Url').value = filePath;
    fm.all("serialNo").value = BankGrid.getRowColData(BankGrid.getSelNo()-1, 2);
    fm.all('fmtransact').value = "download";
    fm.all('downflag').value = "";
    fm.submit();
  }
  else {
    alert("请先选择一条批次号信息，将下载对应的文件！"); 
  } 
}

//选择进行总汇文件下载
function fileDownload1() {
    if(BankGrid.getSelNo()=='0'){
        alert("请选择发盘批次");
        return false;
    }
	strBankCode = BankGrid.getRowColData(BankGrid.getSelNo()-1, 1);
    if (strBankCode!="9995" & strBankCode!="9595")
    {
    	alert("非银联下载请点击“文件下载”!");
    	return;
    }
  if (BankGrid.getSelNo()) { 
    fm.all('Url').value = filePath;
    fm.all('fmtransact').value = "download";
    fm.all('downflag').value = "sum";
    fm.submit();
  }
  else {
    alert("请先选择一条批次号信息，将下载对应的文件！"); 
  } 
}

//选择进行明细文件下载
function fileDownload2() {
    if(BankGrid.getSelNo()=='0'){
        alert("请选择发盘批次");
        return false;
    }
	strBankCode = BankGrid.getRowColData(BankGrid.getSelNo()-1, 1);
    if (strBankCode !="9995" & strBankCode!="9595")
    {
    	alert("非银联下载请点击“文件下载”!");
    	return;
    }
  if (BankGrid.getSelNo()) { 
    fm.all('Url').value = filePath;
    fm.all('downfilesum').value = "";
    fm.all('fmtransact').value = "download";
    fm.all('downflag').value = "list";
    fm.submit();
  }
  else {
    alert("请先选择一条批次号信息，将下载对应的文件！"); 
  } 
}

//提交后自动弹出文件下载
function downAfterSubmit(cfilePath,cflag) {
	try { showInfo.close(); } catch(e) {}
	
	filePath = cfilePath;
	var aflag = fm.all('downflag').value;
	if (cflag == 0)
	{
		  var strfile = BankGrid.getRowColData(BankGrid.getSelNo()-1, 3);
		  
		  if(aflag == "sum" )
		  {
		     fileUrl.href = filePath + strfile.substring(0,strfile.lastIndexOf(","));
		  }
		  else if (aflag == "list")
		  {
		 	 	 fileUrl.href = filePath + strfile.substring(strfile.lastIndexOf(",")+1);
		  }
		  else	
		  {    
		     fileUrl.href = filePath + BankGrid.getRowColData(BankGrid.getSelNo()-1, 3);
		  }
  }
	else
	{
	var fileSql = "select outfile from lybanklog where SerialNo = '" + turnPage.arrDataCacheSet[tSelNo][1]+ "'";     
    var fileResult = easyExecSql(fileSql);
  	fileUrl.href = filePath + fileResult[0][0]; 
	}
	//alert(fileUrl.href);
	
  fileUrl.click();
  initQuery();
  fm.all('downflag').value = "";
  
}

//获取文件下载路径
function getFilePath() {
  var strSql = "select SysVarValue from LDSysVar where SysVar = 'DisplayBankFilePath'";
  
  filePath = easyExecSql(strSql);
  fm.all('Url').value = filePath;
  //alert(filePath);
}

//初始化银行代码
function initBankCode() {
  var strSql = "select BankCode, BankName from LDBank where ComCode = '" + comCode + "'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("bankCode").CodeData = strResult;
  }
  
  easyQueryClick();
}   

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句
	var strSql = "select BankCode, SerialNo, OutFile, SendDate, MakeDate from LYBankLog where InFile is null and ComCode like '" + comCode + "%%'"
	           + " and LogType='" + dealType + "'"
				     + getWherePart('BankCode')
				     + getWherePart('MakeDate')
				     + " order by SerialNo desc";
				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
}

//显示银行批次数据的统计信息，金额、件数
function showStatistics(parm1, parm2) {
  var strSql = "select Totalmoney, Totalnum from lybanklog where SerialNo = '" 
             + BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2) 
             + "'";
             
  //alert(easyExecSql(strSql));      
  var arrResult = easyExecSql(strSql);
  
  fm.all("TotalMoney").value = arrResult[0][0];
  fm.all("TotalNum").value = arrResult[0][1];
  tSelNo = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
}
