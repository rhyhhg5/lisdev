//ReadFromFileInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var filePath;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage2 = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function HZButton(){
	
	var serialno = fm.serialNo.value;
	var querySQL = "select serialno,count(1),sum(paymoney),'','','','','','','','','',''  from lysendtosettle a where a.SerialNo = '" 
                  + serialno 
                  + "' and dealType='" + dealType + "' and succflag = (select substr(agentpaysuccflag, 1, length(agentpaysuccflag) - 1) from ldmedicalcom where a.medicalcode = medicalcomcode)  "
                  + " group by serialno ";
    var arrResult = easyExecSql(querySQL);
    if(arrResult==null){
        querySQL = "select '"+serialno+"' ,0,0,'','','','','','','','','','' from dual ";
    }              
	fm.querySql.value = querySQL;
	fm.filename.value = 'HZList_';
	//提交，下载
	DownLoad(serialno);
}
function MXButton(){
	
	var serialno = fm.serialNo.value;
	var querySQL = " select  getUniteCode(a.agentcode) ,c.name, a.medicalcode,a.accno,a.accname,a.paycode,a.polno,"
                 +" case a.paytype when '1' then '首期' else '续期' end, "
                 +" a.paymoney,getpoldate,a.comcode,case a.succflag when (select substr(agentpaysuccflag, 1, length(agentpaysuccflag) - 1) from ldmedicalcom where a.medicalcode = medicalcomcode) then '是' else '否' end , "
                 +" a.failreason  "
                 +" from lysendtosettle a,lybanklog b,laagent c "
                 +" where a.serialno= b.serialno "
                 +" and  a.agentcode =c.agentcode "
                 +" and  b.logtype = '"+ dealType +"'"
                 +" and  a.serialno = '"+ serialno +"' "
                 +" with ur ";
	fm.querySql.value = querySQL;
	fm.filename.value = 'MXList_';
	//提交，下载
	DownLoad(serialno);
}

//下载
function DownLoad(serialno) {
	
	if(serialno != null && serialno != "" )
    {
        var formAction = fm.action;
        fm.action = "MedicalListOfSettleDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先选择批次，再下载清单！");
        return ;
    }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

//获取文件上传保存路径
function getUploadPath() {
  var strSql = "select SysVarValue from LDSysVar where SysVar = 'ReturnFromBankPath'";
  
  filePath = easyExecSql(strSql);
}

//查询出本机构已返回处理的银行批次
function initQuery() {
  var strSql = "select BankCode, Serialno, MakeDate,ReturnDate from LYBankLog where ReturnDate >= current date - 2 day and ComCode like '" + comCode + "%'"
             + " and LogType='" + dealType + "' order by returndate desc";
  
  turnPage.queryModal(strSql, BankGrid);
  
}

//初始化银行代码
function initMedicalCode() {
  var strSql = "select MedicalComCode, MedicalComName from LDMedicalCom where  ComCode = '" + comCode + "'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("MedicalCode").CodeData = strResult;
  }
}   

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句
	var strSql = "select BankCode, Serialno,MakeDate,ReturnDate from LYBankLog where ReturnDate is not null and ComCode like '" + comCode + "%'"
	           + " and LogType='" + dealType + "'"
				     + getWherePart('BankCode','MedicalCode')
				     + getWherePart('ReturnDate')
				     + " order by returndate desc ";
				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
}
//显示银行批次数据的统计信息，金额、件数
function showStatistics(parm1, parm2) {
  var strSql = "select Totalmoney, Totalnum from lybanklog where SerialNo = '" 
             + BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2) 
             + "' and LogType='" + dealType + "'";
  fm.serialNo.value = BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2);   
  var arrResult = easyExecSql(strSql);
  fm.all("TotalMoney").value = arrResult[0][0];
  fm.all("TotalNum").value = arrResult[0][1];
 
}

function QueryGrid1Data(){
	
	var strsql = "select distinct getUniteCode(a.agentcode) ,c.name, a.medicalcode,a.accno,a.accname,a.paycode,a.polno,"
                    +" case a.paytype when '1' then '首期' else '续期' end, "
                    +" a.paymoney,a.getpoldate,a.comcode	"
					+"  from lysendtoconfirm a,lybanklog b,laagent c "
					+"  where a.serialno = b.serialno "
					+"   and a.agentcode =c.agentcode "
					+"   and a.comcode like '"+comCode+"%' "
					+getWherePart('a.medicalcode','MedicalCode2')
					+"   and b.LogType = 'C'  "
					+"   and a.ConfirmState = '1'  "
					+"   and a.succflag = '0'"
					+"   and not exists (select 1 from lysendtosettle where paycode = a.paycode and  succflag = '0')"
					+"   and exists (select 1 from lccont where prtno=a.polno ) "
					+" with ur";					
   fm.querySql1.value = strsql;
   turnPage2.queryModal(strsql, QueryGrid1);
}

function download1(){
	var querySQL = fm.querySql1.value;
	if(querySQL != null && querySQL != "" )
    {
        var formAction = fm.action;
        fm.action = "ListOfUnSettleDownload.jsp";
        //fm.target = "_blank";
        fm.filename.value = 'UnSettleList_';
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先查询，再下载！");
        return ;
    }
}

