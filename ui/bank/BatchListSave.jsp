<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>


<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bank.*"%>

<%
  String FlagStr = "";
  String Content = "";
    System.out.println("---print--batchlist---");
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  Calendar cal = new GregorianCalendar();
  String year = String.valueOf(cal.get(Calendar.YEAR));
  String month=String.valueOf(cal.get(Calendar.MONTH)+1);
  String date=String.valueOf(cal.get(Calendar.DATE));
  String now = year + month + date + "_" ;
  
  String  millis = String.valueOf(System.currentTimeMillis());  
  String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
  String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
  TransferData tTransferData = new TransferData();
  tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
  tTransferData.setNameAndValue("StartDate",request.getParameter("StartDate"));
  tTransferData.setNameAndValue("EndDate",request.getParameter("EndDate"));
  tTransferData.setNameAndValue("PrtFlag",request.getParameter("PrtFlag"));
  tTransferData.setNameAndValue("BankType",request.getParameter("BankType"));

  try
  {
      VData vData = new VData();
      vData.add(tG);
      vData.add(tTransferData);
      BatchListUI tBatchListUI = new BatchListUI();
      if (!tBatchListUI.submitData(vData, ""))
      {
          Content = "报表下载失败，原因是:" + tBatchListUI.mErrors.getFirstError();
          FlagStr = "Fail";
      }
      else
      {
          String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
          File file = new File(tOutXmlPath);
          
          response.reset();
          response.setContentType("application/octet-stream"); 
          response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
          response.setContentLength((int) file.length());
      
          byte[] buffer = new byte[10000];
          BufferedOutputStream output = null;
          BufferedInputStream input = null;    
          //写缓冲区
          try 
          {
              output = new BufferedOutputStream(response.getOutputStream());
              input = new BufferedInputStream(new FileInputStream(file));
        
          int len = 0;
          while((len = input.read(buffer)) >0)
          {
              output.write(buffer,0,len);
          }
          input.close();
          output.close();
          }
          catch (Exception e) 
          {
            e.printStackTrace();
           } // maybe user cancelled download
          finally 
          {
              if (input != null) input.close();
              if (output != null) output.close();
              file.delete();
          }
       }
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
    }
  
  if (!FlagStr.equals("Fail"))
  {
    Content = "";
    FlagStr = "Succ";
  }
	out.clear();
    out = pageContext.pushBody();
%>
<html>
<script language="javascript">  
    alert("<%=Content%>");
    top.close();
</script>
</html>