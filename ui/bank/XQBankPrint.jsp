<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：XQBankPrint.jsp
//程序功能：实现打印的功能
//创建人  ：刘岩松
//创建日期：2004-4-29
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
 <%@page import="com.sinosoft.lis.f1print.*"%>
<%
  System.out.println("开始执行打印操作");
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  //初始化全局变量，从前台承接数据
   String strStartDate=request.getParameter("StartDate");                //开始日期
   String strEndDate=request.getParameter("EndDate");                  //结束日期
   String strAgentState = request.getParameter("AgentState");             //业务员的状态(1为在职单，0为孤儿单)
   String strPremType = request.getParameter("PremType");               //首续期的标志
   String strFlag = request.getParameter("Flag");                   //S or F(S为银行代收，F为银行代付)
	 String strStation = request.getParameter("Station");                  //界面上录入的管理机构
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  XQPremBankErrUI tXQPremBankErrUI = new XQPremBankErrUI();

  VData tVData = new VData();
  VData mResult = new VData();
  try
  {
    tVData.clear();
    tVData.addElement(strStartDate);
    tVData.addElement(strEndDate);
    tVData.addElement(strAgentState);
    tVData.addElement(strPremType);
    tVData.addElement(strFlag);
    tVData.addElement(strStation);
    tVData.addElement(tG);
    tXQPremBankErrUI.submitData(tVData,"PRINT");
  }
  catch(Exception ex)
  {
    Content = "PRINT"+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  mResult = tXQPremBankErrUI.getResult();
  XmlExport txmlExport = new XmlExport();
  txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
  if (txmlExport==null)
  {
    System.out.println("null");
     tError = tXQPremBankErrUI.mErrors;
    Content = "打印失败,原因是没有需要打印的数据信息！";

    FlagStr = "Fail";

  }
  else
  {
  	session.putValue("PrintStream", txmlExport.getInputStream());
  	System.out.println("put session value");
  	response.sendRedirect("../f1print/GetF1Print.jsp");
  }
  %>
  <html>
  <script language="javascript">
	alert("<%=Content%>");
	top.opener.focus();
	top.close();
</script>
</html>