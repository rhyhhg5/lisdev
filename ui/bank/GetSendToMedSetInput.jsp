<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：GetSendToMedicalComInput.jsp
//程序功能：
//创建日期：2013-6-6 10:50:08
//创建人  ：王硕
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

  <SCRIPT src="GetSendToMedSetcy.js"></SCRIPT>
  <%@include file="GetSendToMedSetInit.jsp"%>
  
  <title>医保代收 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  var comCode = "<%=tGlobalInput.ComCode%>";
  var cDate = "<%=PubFun.getCurrentDate()%>";
</script>

<body  onload="initForm();" >
  <form action="./GetSendToMedSetcySave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <table class= common border=0>
    	<tr>
			<td class= titleImg align= left width = '10%'>请选择医保机构 </td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        医保机构编码
      </TD>
      <TD  class= input>
        <Input NAME=MedicalCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeListEx('MedicalCode',[this,MedicalName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('MedicalCode',[this,MedicalName],[0,1],null,null,null,1);" verify="医保机构编码|notnull&code:MedicalCode" ><input class=codename name=MedicalName readonly=true >
      </TD>
      <TD  class= input>
        <INPUT VALUE="&nbsp;生成结算数据&nbsp;"  
    	class=cssButton TYPE=button name=sub onclick="submitForm()">
      </TD>
      <TD  class= input>
        &nbsp;
      </TD>  
    </TR>
    </table>
    
    
    <Input type="hidden" name=typeFlag >
    <hr>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 生成数据清单：
    		</td>
    	</tr>
    </table>
   <Div  id= "divBank1" style= "display: ''" align=center>
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  		<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
  	</div> 
  	<br/>

<!--  <table  class= common align=left>
    	<TR  class= common>
        <TD  class= title>
          批次总金额
        </TD>
        <TD  class= input>
          <Input NAME=TotalMoney class=common >
        </TD>
        <TD  class= title>
          批次总笔数
        </TD>
        <TD  class= input>
          <Input name=TotalNum class=common >
        </TD>
      </TR>
     
      <!--  <tr>
      <TD>
        <INPUT VALUE="汇总清单下载"  
    	class=cssButton TYPE=button name=sub onclick="1212()">
      </TD>
      <TD>
        <INPUT VALUE="明细清单下载"  
    	class=cssButton TYPE=button name=sub onclick="MXButton()">
      </TD>
      </tr>-->

<!--  <br>
<br>
<br>
<br>


<br>
          <hr>
   <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>超过20天未确认的保单清单</td>
  		</tr>
  	</table> -->
  	
    <!-- <table  class= common>
    <TR  class= common>
      <TD  class= title>
        医保机构编码
      </TD>
      <TD  class= input>
        <Input NAME=QMedicalCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('MedicalCode',[this,QMedicalName],[0,1]);" onkeyup="return showCodeListKey('MedicalCode',[this,QMedicalName],[0,1]);" verify="医保机构编码|notnull&code:MedicalCode" ><input class=codename name=QMedicalName readonly=true >
      </TD>
      <TD  class= input>
        <INPUT VALUE="&nbsp;查询&nbsp;"  
    	class=cssButton TYPE=button name=sub onclick="submitForm()">
      </TD>
      <TD  class= input>
        &nbsp;
      </TD>  
    </TR>
    </table> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrid);">
    		</td>
    		<td class= titleImg>
    			超过20天未做回执回销的保单清单：
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrid" style= "display: ''" align=center>
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanQueryGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  		<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
  	</div> 
    <br>-->
    
    <INPUT VALUE="" TYPE=hidden name=serialNo>
    <INPUT VALUE="" TYPE=hidden name=Url>
    <INPUT VALUE="" TYPE=hidden name=downfilesum>
    <INPUT VALUE="" TYPE=hidden name=downflag>
    <INPUT VALUE="" TYPE=hidden  name=DealType>
    <INPUT VALUE="" TYPE=hidden  name=querySql>
    <INPUT VALUE="" TYPE=hidden  name=filename>
  </form>
  
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
