<?xml version="1.0" encoding="gbk"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xalan="http://xml.apache.org/xalan"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java" version="1.0">
	<xsl:include href="return_function.xsl" />
	<xsl:template match="/">
		<BANKDATA>
			<xsl:for-each select="BANKDATA/ROW[position()>13]">
				<ROW>
					<!-- 帐号 -->
					<AccNo type="text">
						<xsl:value-of
							select="xalan:tokenize(COLUMN, '|')[2]" />
					</AccNo>
					<!-- 金额 -->
					<PayMoney type="number">
						<xsl:value-of
							select="xalan:tokenize(COLUMN, '|')[3]" />
					</PayMoney>
					<!-- 账户名 -->
					<AccName type="text">
						<xsl:value-of
							select="xalan:tokenize(COLUMN, '|')[4]" />
					</AccName>					
					<!-- 付费号 -->
					<PayCode>
						<xsl:value-of
							select="xalan:tokenize(COLUMN, '|')[5]" />
					</PayCode>
					<!-- 成功标识 -->
					<BankSuccFlag type="text">
						<xsl:value-of
							select="xalan:tokenize(COLUMN, '|')[6]" />
					</BankSuccFlag>
					<!-- 回盘时间 -->
					<BankDealDate type="text">
						<xsl:value-of
							select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()" />
					</BankDealDate>
				</ROW>

			</xsl:for-each>

		</BANKDATA>

	</xsl:template>

</xsl:stylesheet>

