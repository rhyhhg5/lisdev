<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 --> 
    
    <!-- 账号，18位 -->    
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 11, 18)"/>
    </AccNo>   
    <!-- 户名，用证件号来代替，18位 -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 29, 18)"/>
    </AccName> 
    
    
    <!-- 交易金额，12位， -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 47, 12) div 100"/>
    </PayMoney>
   
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN, 59, 8),1,4), '-', substring(substring(COLUMN, 59, 8),5,2), '-', substring(substring(COLUMN, 59, 8),7,2))"/>
    </BankDealDate>
    <!-- 单据号 -->
    <PayCode type="text">
      <xsl:value-of select="concat('86',substring(COLUMN, 71, 18) )"/>
    </PayCode>
    <!-- 交易状态（成功标志） -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 89, 1)"/>
    </BankSuccFlag>
    <!-- 以上的代码块根据不同的银行而不同 -->
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

