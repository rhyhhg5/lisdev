<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>
<xsl:template match="/">   
<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 	
		<row>
		       
		<AccName type="text"> 
		  <xsl:value-of select="xalan:tokenize(COLUMN, ',')[5]"/> 
		</AccName>
		<AccNo type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, ',')[4]"/>
		</AccNo> 
      <xsl:variable name="payMoneyStr" select="xalan:tokenize(COLUMN, ',')[7]"/>
		<PayMoney type="number">
		   <xsl:value-of select="format-number($payMoneyStr*0.01,'#.00')"/> 
		</PayMoney>
		 <PayCode>
			<xsl:value-of select="xalan:tokenize(COLUMN, ',')[9]"/>
		 </PayCode>
		<BankSuccFlag type="text">
					<xsl:value-of select="xalan:tokenize(COLUMN, ',')[10]"/>
		</BankSuccFlag> 
   
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>  
    </row>  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

