<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 --> 
    
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN, 5,10)"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '10')"/>    
    <!-- 账户名称（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 5, 10-$chinaLen)"/>
    </AccName>
    
    <!-- 账号，16位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 15-$chinaLen, 19)"/>
    </AccNo>
    
    <!-- 交易金额，10位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 34-$chinaLen, 10) div 100"/>
    </PayMoney>
    
    <!-- 交易日期 -->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>
    
    <!-- 交易状态 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="'S'"/>
    </BankSuccFlag>
    
    <!-- 以上的代码块根据不同的银行而不同 -->
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

