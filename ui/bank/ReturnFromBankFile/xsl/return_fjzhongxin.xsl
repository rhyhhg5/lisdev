<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>1]"> 
  <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>  
		<PayCode>
			<xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
		</PayCode>       
		<AccName type="text"> 
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/> 
		</AccName>
		<AccNo type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[4]"/>
		</AccNo> 

		<PayMoney type="number">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[5]"/>
		</PayMoney>
		
		<BankDealDate type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[6]"/>
    </BankDealDate> 
 
		<BankSuccFlag type="text">
					<xsl:value-of select="xalan:tokenize(COLUMN, '|')[7]"/>
		</BankSuccFlag> 
   
	</ROW>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

