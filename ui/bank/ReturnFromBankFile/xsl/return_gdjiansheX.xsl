<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW[position()>1]">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 --> 
    <!-- 收据号 -->   
    <PayCode type="text"> 
      <xsl:value-of select="substring(COLUMN, 1, 12)"/>
    </PayCode>  
    <!-- 收据号 -->   
    <PolNo type="text"> 
      <xsl:value-of select="substring(COLUMN, 1, 12)"/>
    </PolNo> 
    <!-- 账号，27位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 13, 27)"/>
    </AccNo>
    
    <!-- 交易金额，12位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 84, 18)"/>
    </PayMoney>  
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>
        
    <!-- 以上的代码块根据不同的银行而不同 -->
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>


