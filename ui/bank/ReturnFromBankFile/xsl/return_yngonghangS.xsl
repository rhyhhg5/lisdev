<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>1]"> 
  <!-- ¨°???|ì???¨2???¨|?¨′?Y2?¨a?|ì?¨°?DD??2?¨a? -->
  <ROW>  
    <!-- 成功标记  -->  
    <xsl:variable name="flagStr" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK(xalan:tokenize(COLUMN, '&#9;')[15]))"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('处理成功'))"/> 
    <xsl:if test="$flagStr=$flagStr1"> 
    <BankSuccFlag type="text">
      <xsl:value-of select="'Y'"/>
    </BankSuccFlag>
    </xsl:if>
        
        <AccName type="text"> 
          <xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[12]"/> 
        </AccName>
        <AccNo type="text">
          <xsl:variable name="acc" select="xalan:tokenize(COLUMN, '&quot;')[6]"/>
          <xsl:variable name="newAcc" select="string(java:com.sinosoft.lis.pubfun.PubFun.RePlaceString($acc,'&quot;',''))"/>  
          <xsl:value-of select="$newAcc"/>
        </AccNo> 

        <PayMoney type="number">
          <xsl:variable name="money" select="xalan:tokenize(COLUMN, '&#9;')[13]"/>
          <xsl:variable name="newMoney" select="string(java:com.sinosoft.lis.pubfun.PubFun.RePlaceString($money,'元',''))"/>  
          <xsl:value-of select="$newMoney"/>
        </PayMoney>
        
        <BankDealDate type="text">
          <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()" />
        </BankDealDate>
    </ROW>  
  <!-- ¨°?¨|?|ì???¨2???¨|?¨′?Y2?¨a?|ì?¨°?DD??2?¨a? -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

