<?xml version="1.0" encoding="gbk"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
  
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 公司编号 -->    
     <ComCode type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN,'|')[1]"/>
    </ComCode>    
    <!-- 缴费通知书号 -->    
     <PayCode type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN,'|')[2]"/>
    </PayCode>  
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring-after(COLUMN, substring(COLUMN,1,32))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '10')"/>
    <!-- 账户名 -->
    <AccName type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN,'|')[3]"/>
    </AccName>    
    <!-- 账号 -->
     <AccNo type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN,'|')[4]"/>
    </AccNo> 
    <!-- 金额，10位，包括小数点后两位，注意，工行返回数据中有小数点 -->
    <PayMoney type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN,'|')[5]"/>
    </PayMoney>    
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN,'|')[6]"/>
    </BankDealDate>
    <!-- 回盘标识 -->
      <BankSuccFlag type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN,'|')[7]"/>
    </BankSuccFlag>
    </ROW>
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

