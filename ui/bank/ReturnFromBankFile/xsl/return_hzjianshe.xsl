<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 -->       
    <!-- 保单号，截掉前两位，用收据号代替 
    <PayCode type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
    </PayCode>
    -->
    <!-- 账户名称，8位 -->
    <AccName type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
    </AccName>
    <!-- 账号，20位 -->
    <AccNo type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/>
    </AccNo>
    
    <!-- 交易金额，18位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[4]"/>
    </PayMoney>
    
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(xalan:tokenize(COLUMN, '|')[5],1,4), '-', substring(xalan:tokenize(COLUMN, '|')[5],5,2), '-', substring(xalan:tokenize(COLUMN, '|')[5],7,2))"/>    
    </BankDealDate>
    
    <!-- 交易状态 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[6]"/>
    </BankSuccFlag>
    <!-- 以上的代码块根据不同的银行而不同 -->
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

