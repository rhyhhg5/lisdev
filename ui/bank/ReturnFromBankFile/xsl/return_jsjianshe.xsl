<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN, 21, 10)"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '5')"/>
    <!-- 账户名称，最大20位 -->    
    <AccName type="text"> 
      <xsl:value-of select="substring(COLUMN, 21, 10-$chinaLen)"/>
    </AccName>
        
    <!-- 账号，最大16位 -->        
    <AccNo type="text"> 
      <xsl:value-of select="substring(COLUMN, 31-$chinaLen, 20)"/>
    </AccNo>
    
    <!-- 交易金额，整数部分最多13位，小数部分为2位 -->
    <xsl:variable name="payMoneyStr" select="substring(COLUMN, 51-$chinaLen, 10)"/>
    <PayMoney type="number">
      <xsl:value-of select="$payMoneyStr"/>
    </PayMoney> 
    
    
    <!--交易时间-->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(COLUMN, 61-$sumChinaLen, 4), '-', substring(COLUMN, 65-$sumChinaLen, 2), '-', substring(COLUMN, 67-$sumChinaLen, 2))"/>
    </BankDealDate>
    
    <!-- 交易状态 -->
    <xsl:variable name="flagStr" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK(substring(COLUMN, 69-$sumChinaLen, 20)))"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('成功'))"/> 
    <xsl:if test="$flagStr!=$flagStr1"> 
    	<BankSuccFlag type="text">
    		<xsl:value-of select="N"/>
    	</BankSuccFlag>
    </xsl:if>
    <xsl:if test="$flagStr=$flagStr1"> 
      <BankSuccFlag type="text">
        <xsl:value-of select="Y"/>
      </BankSuccFlag> 
    </xsl:if>
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

