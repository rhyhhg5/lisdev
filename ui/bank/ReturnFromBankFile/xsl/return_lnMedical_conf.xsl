<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                xmlns:medical="com.sinosoft.lis.bank"
                exclude-result-prefixes="java"
                version="1.0">


<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <xsl:variable name="succflag" select="xalan:tokenize(COLUMN, '|')[5]"/>
    <!-- 缴费通知书号 -->        
    <PayCode type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
    </PayCode>
    <!-- 个人编号 -->        
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
    </AccNo>
    <!--险种编码-->
    <RiskCode type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/> 
    </RiskCode>  
    <!--业务类型-->
   	<PayType type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[4]"/>
    </PayType>
    <!--成功标志-->
    <SuccFlag type="text">
      <xsl:value-of select="$succflag"/>
    </SuccFlag>
    
    <!--失败原因-->
    <FailReason type="text">
	    <!-- 银行账号为18位及以下，卡折标志为0，银行账号为19位，卡折标志为3 -->
	  	<xsl:choose>
		<xsl:when test="$succflag='0'">
			<xsl:value-of select="''"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="substring(xalan:tokenize(COLUMN, '|')[6],1,100)"/>
		</xsl:otherwise>
   		</xsl:choose>
    </FailReason>
    <!--确认时间-->
	<xsl:choose>
		<xsl:when test="$succflag='0'">
			<DealDate type="text">
				<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
			</DealDate>
			<DealTime type="text">
				<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentTime()"/>
			</DealTime>
		</xsl:when>
   </xsl:choose>
	
    <!-- 实际生效日期
    <RealDate type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[7]"/>
    </RealDate>-->
    <!-- 社保经办日期 -->
    <!--<BankDealDate type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[8]"/>
    </BankDealDate> -->
    
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>
 
</BANKDATA>

</xsl:template>

</xsl:stylesheet>

