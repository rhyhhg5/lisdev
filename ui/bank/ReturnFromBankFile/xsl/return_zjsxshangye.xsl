<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">
  <ROW>  
   <!-- 以下的代码块根据不同的银行而不同 -->   
    <!-- 账号，不够20位后面补空格 --> 
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 12, 20)"/>
    </AccNo>   
 
  <!-- 交易金额，10位， -->
   <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 32, 10)"/>
   </PayMoney>
   
   <xsl:variable name="accNameValue" select="substring-after(COLUMN,substring(COLUMN,1,41))"/>
   <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue,'2')"/>
   <BankSuccFlag type="text">
   		<xsl:choose>
		    <xsl:when test="$chinaLen>0">
		      <xsl:value-of select="substring(COLUMN, 42, 1)"/>
		    </xsl:when>
		   	<xsl:otherwise>
		     	<xsl:value-of select="substring(COLUMN, 42, 2)"/>
		   	</xsl:otherwise>
		  </xsl:choose> 
	 </BankSuccFlag>
   
   <PayCode type="text">
     <xsl:value-of select="substring(COLUMN, 55-$chinaLen, 17)"/>
   </PayCode>
   
   <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
   </BankDealDate>  
    
  <!-- 以上的代码块根据不同的银行而不同 -->
     
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>




