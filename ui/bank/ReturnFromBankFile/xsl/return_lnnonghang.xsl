<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">
  <ROW>  
   <!-- 以下的代码块根据不同的银行而不同 -->   
   	<xsl:variable name="accNameValue" select="substring(COLUMN, 7,10)"/>
		<xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '10')"/> 
		  	 <!-- 账户名称（姓名）8位 -->
		<AccName type="text">
		   <xsl:value-of select="substring(COLUMN, 7,10-$chinaLen)" />
		</AccName>   
  
    <!-- 账号 --> 
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 17-$chinaLen, 19)"/>
    </AccNo>   
    
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 36-$chinaLen, 10)"/>
    </PayMoney>
  
   <PayCode type="text">
     <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
   </PayCode> 
  <!-- 交易状态（成功标志） -->
   <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 64-$chinaLen, 1)"/>
   </BankSuccFlag>

     <!-- 交易日期 -->
   <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
   </BankDealDate>
   
  <!-- 以上的代码块根据不同的银行而不同 -->
     
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>




