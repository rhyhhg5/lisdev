<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 --> 
    <!-- 收据号 -->   
    <PayCode type="text"> 
      <xsl:value-of select="substring(COLUMN, 2, 18)"/>
    </PayCode>  

    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring-after(COLUMN, substring(COLUMN, 2, 18))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '10')"/>    
    <!-- 账户名称（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 20, 10-$chinaLen)"/>
    </AccName>
    
    <!-- 账号，16位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 30-$chinaLen, 20)"/>
    </AccNo>
    
    <!-- 交易金额，10位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 50-$chinaLen, 10) div 100"/>
    </PayMoney>
    
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN, 60-$chinaLen, 8),1,4), '-', substring(substring(COLUMN, 56-$chinaLen, 8),5,2), '-', substring(substring(COLUMN, 56-$chinaLen, 8),7,2))"/>
    </BankDealDate>
    
    <!-- 交易状态 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 68-$chinaLen, 1)"/>
    </BankSuccFlag>
    <!-- 以上的代码块根据不同的银行而不同 -->
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

