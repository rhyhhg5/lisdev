<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 账号，最19位 -->        
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 61, 30)"/>
    </AccNo>

    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN, 92, 101)"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>    
    <DealType type="test">
    	<xsl:value-of select="$chinaLen"/>
    </DealType>
    <!-- 账户名称（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 92, 10-$chinaLen)"/>
    </AccName>
    <!-- 交易金额，12位，包括小数点后两位 -->
    <PayMoney type="number">
    	<xsl:variable name="tMoney" select="substring(COLUMN, 102-$chinaLen, 11)"/>
      <xsl:value-of select="$tMoney div 100"/>
    </PayMoney>
    <!-- 交易状态 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 113-$chinaLen, 4)"/>
    </BankSuccFlag>

    <!-- 收据号 -->   

    <!--交易时间-->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>  
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

