<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW[position()>1]"> 
  <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>         
      <!-- 账号 -->        
   		<AccNo type="text"> 
		    <xsl:value-of select="xalan:tokenize(COLUMN, '|')[6]"/>
		  </AccNo>
		  
		  <!-- 交易金额 -->
		  <xsl:variable name="payMoneyStr" select="xalan:tokenize(COLUMN, '|')[5] div 100"/>
		  <PayMoney type="number">
		    <xsl:value-of select="number($payMoneyStr)"/> 
		  </PayMoney>    
		  
		  <!-- 费用记录号 -->  
		  <xsl:variable name="PayCodeNo" select="xalan:tokenize(COLUMN, '|')[2]"/>
		  <PayCode type="text">
		    <xsl:value-of select="$PayCodeNo"/> 
		  </PayCode>   
		  
		  <!--成功标志-->	     
      <BankSuccFlag type="text">
        <xsl:value-of select="xalan:tokenize(COLUMN, '|')[8]"/>
      </BankSuccFlag>   
      
		  <!--交易时间-->
		  <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
		  <BankDealDate type="text">
		    <xsl:value-of select="concat(substring(xalan:tokenize(COLUMN, '|')[4],1,4),'-',substring(xalan:tokenize(COLUMN, '|')[4],5,2),'-',substring(xalan:tokenize(COLUMN, '|')[4],7,2))"/>
		  </BankDealDate> 
		     
		  <!-- 帐户名  -->        
		  <AccName type="text"> 
		    <xsl:value-of select="xalan:tokenize(COLUMN, '|')[7]"/>
		  </AccName>		
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

