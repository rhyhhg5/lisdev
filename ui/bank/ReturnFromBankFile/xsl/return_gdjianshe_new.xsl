<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 账号，最大16位 -->        
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
    </AccNo>
    <!-- 账户名称，最大20位 -->    
    <AccName type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/> 
    </AccName>

    <PayMoney type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/> 
    </PayMoney> 
    <xsl:variable name="flagStr" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK(substring(xalan:tokenize(COLUMN, '|')[5],1,4)))"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('交易成功'))"/> 
    <xsl:if test="$flagStr!=$flagStr1"> 
    	<BankSuccFlag type="text">
    		<xsl:value-of select="substring(xalan:tokenize(COLUMN, '|')[5],1,2)"/>
    	</BankSuccFlag>
    </xsl:if>
    <xsl:if test="$flagStr=$flagStr1"> 
      <BankSuccFlag type="text">
        <xsl:value-of select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('成功'))"/>
      </BankSuccFlag> 
    </xsl:if>
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>
    
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>
 
</BANKDATA>

</xsl:template>

</xsl:stylesheet>

