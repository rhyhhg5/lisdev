<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>
  <xsl:for-each select="BANKDATA/ROW[position()>0]"> 
  <ROW>
  
    <PayCode type="text"> 
      <xsl:value-of select="substring(COLUMN, 18, 17)"/>
    </PayCode>
    

    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 54, 20)"/>
    </AccNo>  
    
    <xsl:variable name="accNameValue" select="substring(COLUMN, 74, 15)"/>   
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>    
 
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 74, 15-$chinaLen)"/>
    </AccName>
   
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 89-$chinaLen , 10)"/>
    </PayMoney>
  
    <BankDealDate type="text">
       <xsl:value-of select="substring(COLUMN,100-$chinaLen , 8)"/>
    </BankDealDate>
    
    
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN,108-$chinaLen , 100)"/>
    </BankSuccFlag>

  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

