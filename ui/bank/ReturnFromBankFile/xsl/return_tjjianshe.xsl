<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>  
    <!-- 账号，最大32位 -->        
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
    </AccNo>
    <!-- 账户名称，最大20位 -->    
    <AccName type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/> 
    </AccName>
    <!-- 交易金额，整数部分最多8位 -->
    <PayMoney type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/>
    </PayMoney>
    <xsl:variable name="flag"  select ="string-length(xalan:tokenize(COLUMN, '|')[5])"/>
    <xsl:choose>
    			<xsl:when test="$flag=0">
    					<BankSuccFlag type="text">
				      	<xsl:value-of select="'A'"/>
				    	</BankSuccFlag>
    			</xsl:when>
   				<xsl:otherwise>
     				 <!-- 成功标志 -->
				    <BankSuccFlag type="text">
				      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[5]"/>
				    </BankSuccFlag>
   				</xsl:otherwise>
  	</xsl:choose>
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

