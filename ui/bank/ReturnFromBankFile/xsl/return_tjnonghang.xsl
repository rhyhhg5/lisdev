<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW">
    <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>
    <!-- 账号，最19位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,1,19)"/>
    </AccNo>
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring-after(COLUMN,substring(COLUMN,1,19))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue,'10')"/>
    <!-- 账户名称（姓名）10位 -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN,20,10-$chinaLen)"/>
    </AccName>
    <!-- 收据号 20位 -->
    <PayCode type="text">
      <xsl:value-of select="substring(COLUMN,34-$chinaLen,20)"/>
    </PayCode>
    <!-- 交易日期 8位 -->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN,62-$chinaLen,8),1,4),'-',substring(substring(COLUMN,62-$chinaLen,8),5,2),'-',substring(substring(COLUMN,62-$chinaLen,8),7,2))"/>
    </BankDealDate>
    <!-- 证件号码 30位 -->
    <IDNo type="text">
      <xsl:value-of select="substring(COLUMN,76-$chinaLen,30)"/>
    </IDNo>
    <!-- 交易金额，12位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN,106-$chinaLen,12)"/>
    </PayMoney>
    <!-- 交易状态 4位 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN,130-$chinaLen,4)"/>
    </BankSuccFlag>

    </ROW>
    <!-- 以上的代码块根据不同的银行而不同 -->
 </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

