<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>5]"> 
  <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>  		
		<!-- 账号 -->        
		<AccNo type="text">
			 <xsl:value-of select="xalan:tokenize(COLUMN, '|')[6]"/>
		</AccNo>				
		<!-- 交易金额 -->
		<xsl:variable name="money"  select ="xalan:tokenize(COLUMN, '|')[10]"/>
		<PayMoney type="number">
		  <xsl:value-of select="number(substring-before(money,'元'))"/>
		</PayMoney>

     <!--公司编号-->
    <PayCode type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
		</PayCode>

		<!--  姓名，32位 -->    
		<AccName type="text"> 
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[9]"/> 
		</AccName>
    <!-- 成功标记  -->    
    <BankSuccFlag type="text">
		   <xsl:value-of select="xalan:tokenize(COLUMN, '|')[12]"/>
		</BankSuccFlag> 
		<!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate> 

	</ROW>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

