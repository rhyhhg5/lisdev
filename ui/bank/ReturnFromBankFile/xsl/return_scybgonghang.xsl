<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>5]"> 
  <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>  
  	<!-- 保单号 -->
		<PayCode type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
		</PayCode>
		<!-- 账号 -->        
		<AccNo type="text">
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[8]"/>
		</AccNo>
  	<!-- 账户名称 -->    
		<AccName type="text"> 
		  <xsl:value-of select="xalan:tokenize(COLUMN, '|')[9]"/> 
		</AccName>	
		<!-- 交易金额 -->
		<PayMoney type="number">
			<xsl:variable name="money" select="string(java:com.sinosoft.lis.pubfun.PubFun.RePlaceString(xalan:tokenize(COLUMN, '|')[10],',',''))"/>
   		<xsl:variable name="newMoney" select="string(java:com.sinosoft.lis.pubfun.PubFun.RePlaceString($money,'元',''))"/>  
		  <xsl:value-of select="$newMoney"/>
		</PayMoney>
		
		<BankSuccFlag type="text">
      <xsl:variable name="tFlag" select="substring(xalan:tokenize(COLUMN, '|')[12],3,2)"/>
	    <xsl:choose>
		    <xsl:when test="$tFlag=''">
		      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[12]"/>
		    </xsl:when>
		    <!-- 多于四个汉字的处理 -->
		    <xsl:otherwise>
		      <xsl:value-of select="$tFlag"/>
		    </xsl:otherwise>
		  </xsl:choose>
    </BankSuccFlag>  

		<!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>    
   
	</ROW>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

