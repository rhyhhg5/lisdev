<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 银行返回标记 -->
    <xsl:variable select="xalan:tokenize(COLUMN, ',')[1]" name="SuccFlag"/>
    <!-- 银行成功标记00代表成功 -->
    <xsl:variable select="00" name="SuccFlag00"/>

    <BankSuccFlag type="text">
      <xsl:value-of select="$SuccFlag"/>
    </BankSuccFlag>
    <xsl:choose>
      <xsl:when test="$SuccFlag00=$SuccFlag">
       <!-- 账户名称，最大20位 -->    
       <AccName type="text"> 
         <xsl:value-of select="xalan:tokenize(COLUMN, ',')[3]"/> 
       </AccName>
       <!-- 账号，最大16位 -->        
       <AccNo type="text"> 
         <xsl:value-of select="xalan:tokenize(COLUMN, ',')[4]"/>
       </AccNo>
       <PayMoney type="number">
         <xsl:value-of select="xalan:tokenize(COLUMN, ',')[6]"/> 
       </PayMoney>  
      </xsl:when>
    <xsl:otherwise>
       <!-- 账户名称，最大20位 -->    
       <AccName type="text"> 
         <xsl:value-of select="xalan:tokenize(COLUMN, ',')[4]"/> 
       </AccName>
       <!-- 账号，最大16位 -->        
       <AccNo type="text"> 
         <xsl:value-of select="xalan:tokenize(COLUMN, ',')[5]"/>
       </AccNo>
       <PayMoney type="number">
         <xsl:value-of select="xalan:tokenize(COLUMN, ',')[7]"/> 
       </PayMoney>  
     </xsl:otherwise>
  </xsl:choose>
     	
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
    </BankDealDate>
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>
 
</BANKDATA>

</xsl:template>

</xsl:stylesheet>

