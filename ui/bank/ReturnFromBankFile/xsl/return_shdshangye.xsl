<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
  
 <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
 	<!-- 最后一行为汇总行 -->
  <xsl:choose>
  <xsl:when test="substring(COLUMN,10,12) = '003900000097'">
  </xsl:when>
  <xsl:otherwise>
  
  <row>    
			  <!-- 业务号 -->    
			    <PayCode type="text">
			      <xsl:value-of select="substring(COLUMN,10,12)"/>
			    </PayCode>    
			     <!-- 交易金额，10位，包括小数点后两位，注意，工行返回数据中有小数点 -->
			    <PayMoney type="text">
			      <xsl:value-of select="substring(COLUMN,22,10)"/>
			    </PayMoney>    
			    <!-- 交易日期 -->
			    <BankDealDate type="text">
			      <xsl:value-of select="concat(substring(substring(COLUMN,37,8),1,4),'-',substring(substring(COLUMN,37,8),5,2),'-',substring(substring(COLUMN,37,8),7,2))"/>
			    </BankDealDate>
			    <!-- 交易日期 -->
			    <PolNo type="text">
			       <xsl:value-of select="substring(COLUMN,45,12)"/>
			    </PolNo>
  			      
	</row>  
		 <!-- 以上的代码块根据不同的银行而不同 -->    
  
  </xsl:otherwise>
  </xsl:choose>   
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

