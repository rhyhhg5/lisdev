<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">
<xsl:include href="return_function.xsl"/>
<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>1]">
  <ROW> 
   <!-- 1 处理顺序号 --> 
  	<xsl:variable name = "SubStr1" select ="substring-after(COLUMN,',')" />
  	
		<!-- 2 通联支付用户编号 --> 
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!-- 3 银行代码 --> 
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!-- 4 帐号类型 --> 
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!-- 5 账号 --> 
		<AccNo type="text"> 
      <xsl:value-of select="substring-before($SubStr1,',')"/>
    </AccNo>
    
    <xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		<!-- 6 账户名 --> 
		<AccName type="text"> 
			<xsl:value-of select="substring-before($SubStr1,',')"/>
		</AccName> 
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!-- 7 开户行所在省 --> 
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!-- 8 开户行所在市 --> 
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!-- 9 开户行名称 --> 
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!-- 10 账户类型 -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--11 金额 -->
		<xsl:variable name = "SubStr2" select ="substring-before($SubStr1,',')" />
		
		<xsl:variable name="paymode1" select="format-number($SubStr2,'#.00')"/>
		<PayMoney type="number">
			<xsl:value-of select="$paymode1 div 100"/>
    </PayMoney>
    <xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--12 货币类型 -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--13 协议号 -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--14 协议用户编号 -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--15 开户证件类型 -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--16 证件号 -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--17 手机号小灵通 -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--18 自定义用户号 -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--19 备注	CHAR(50) -->
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!-- 20 反馈码 -->
		<BankSuccFlag type="text">
			<xsl:value-of select="substring-before($SubStr1,',')"/>
		</BankSuccFlag>
		<xsl:variable name = "SubStr1" select ="substring-after($SubStr1,',')" />
		
		<!--21原因 -->
		<BankUnSuccReason type="text">
			<xsl:value-of select="$SubStr1"/>
		</BankUnSuccReason>
		
		<BankDealDate type="text">
      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
   </BankDealDate> 
		
	</ROW>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>