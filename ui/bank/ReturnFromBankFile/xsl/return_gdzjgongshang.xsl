<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
		<xsl:variable name="flagStr"  select="substring(COLUMN, 5, 1)"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('2'))"/> 
		<xsl:if test="$flagStr=$flagStr1" >
    <ROW>
        <!-- 交易状态 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 1, 4)"/>
    </BankSuccFlag>    
    <!-- 账号，最19位 -->        
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,18, 19)"/>
    </AccNo>

    <!-- 交易金额，17位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 45, 17) div 100"/>
    </PayMoney>

    <!--交易时间-->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>

    <!-- 收据号 -->   
    <PayCode type="text"> 
      <xsl:value-of select="substring(COLUMN, 122, 22)"/>
    </PayCode>
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:if>
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

