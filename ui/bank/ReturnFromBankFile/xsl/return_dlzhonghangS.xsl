<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xalan="http://xml.apache.org/xalan"
    xmlns:java="http://xml.apache.org/xslt/java"
    exclude-result-prefixes="java" version="1.0">

    <xsl:include href="return_function.xsl" />

    <xsl:template match="/">
        <BANKDATA>
            <xsl:for-each select="BANKDATA/ROW[position()>1]">
                <ROW>
                    <PayCode type="text">
                        <xsl:value-of select="substring(COLUMN,1,8)" />
                    </PayCode>
                    <PayMoney>
                        <xsl:variable name="paymoney"
                            select="substring(COLUMN,69,15)" />
                        <xsl:value-of select="0.01*$paymoney" />
                    </PayMoney>
                    <BankSuccFlag type="text">
                        <xsl:value-of select="substring(COLUMN,84,2)" />
                    </BankSuccFlag>
                    <BankDealDate type="text">
                        <xsl:value-of
                            select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()" />
                    </BankDealDate>
                </ROW>
            </xsl:for-each>

        </BANKDATA>

    </xsl:template>

</xsl:stylesheet>

