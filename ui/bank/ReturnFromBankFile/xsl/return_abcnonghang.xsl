<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">
<xsl:include href="return_function.xsl"/>
	<xsl:template match="/">
	<BANKDATA>
	<xsl:for-each select="BANKDATA/ROW">
	<ROW>
	<!-- 序列  -->
	<XuLie type="text">
	<xsl:value-of select="xalan:tokenize(COLUMN, '|_|')[1]"/>
	</XuLie>
	<!-- 账户名 -->
	<AccName type="text">
	<xsl:value-of select="xalan:tokenize(COLUMN, '|_|')[2]"/>
	</AccName>
	<!-- 账号 -->
	<AccNo type="text">
	<xsl:value-of select="xalan:tokenize(COLUMN, '|_|')[3]"/>
	</AccNo>
	<!-- 银行实际收、缴金额 -->
	<PayMoney type="text">
	<xsl:value-of select="xalan:tokenize(COLUMN, '|_|')[5]"/>
	</PayMoney>
	<!-- 交退费编码 -->
	<PayCode type="text">
	<xsl:value-of select="xalan:tokenize(COLUMN, '|_|')[6]"/>
	</PayCode>
	<BankSuccFlag type="text">
	<xsl:value-of select="xalan:tokenize(COLUMN, '|_|')[11]"/>
	</BankSuccFlag>
	<ERR_MSG type="text">
	<xsl:value-of select="xalan:tokenize(COLUMN, '|_|')[12]"/>
	</ERR_MSG>
	
	</ROW>
	</xsl:for-each>
	</BANKDATA>
	</xsl:template>
</xsl:stylesheet>