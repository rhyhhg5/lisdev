<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW[position()>0]"> 
  <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>  
		<PayCode>
			<xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[3]"/>
		</PayCode>  
		<PolNo>
			<xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[3]"/>
		</PolNo>      
		<xsl:variable name="tPayMoney" select="xalan:tokenize(COLUMN, '&#9;')[4]"/>
		<PayMoney type="number">
		  <xsl:value-of select="$tPayMoney"/>
		</PayMoney>
     
      <xsl:variable name="dealDate" select="xalan:tokenize(COLUMN, '&#9;')[2]"/>
   <BankDealDate type="text">
      <xsl:value-of select="concat(substring($dealDate,1,4), '-', substring($dealDate,5,2), '-', substring($dealDate,7,2))"/>
    </BankDealDate> 
	</ROW>  
  <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

