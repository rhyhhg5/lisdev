<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 账号，最22位 -->        
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
    </AccNo>
    <!-- 账户名称，最大20位 -->    
    <PayCode type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/> 
    </PayCode>    
    <!-- 交易金额 -->
    <xsl:variable name="payMoneyStr" select="xalan:tokenize(COLUMN, '||')[3]"/>
    <PayMoney type="number">
      <xsl:value-of select="number($payMoneyStr)"/> 
    </PayMoney>
    <xsl:variable name="flagStrCount" select="sum(xalan:tokenize(COLUMN, '|')[4])"/>
    <Pay type="text">
    <xsl:value-of select="$flagStrCount"/>
    </Pay>    
    <!-- 交易状态 -->
    <xsl:variable name="flagStr" select="xalan:tokenize(COLUMN, '|')[4]"/>   
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('F'))"/> 
    <xsl:variable name="flagStr2" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('0'))"/> 
    <xsl:if test="$flagStrCount=$flagStr2"> 
    <BankSuccFlag type="text">
      <xsl:value-of select="'S'"/>
    </BankSuccFlag>
    </xsl:if>
    <xsl:if test="$flagStr=$flagStr1"> 
    <BankSuccFlag type="text">
      <xsl:value-of select="'F'"/>
    </BankSuccFlag>
    </xsl:if>

        <!--交易时间-->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>  
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

