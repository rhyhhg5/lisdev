<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <xsl:choose>
    <xsl:when test="substring(COLUMN, 1, 1) = '#'">
    </xsl:when>
    <xsl:otherwise>
    
  <ROW>      
    <!-- 以下的代码块根据不同的银行而不同 -->   
    <!-- 账号，最大16位 -->        
    <xsl:variable name="accNoStr" select="xalan:tokenize(COLUMN, ' ;')[1]"/>     
    <AccNo type="text">
      <xsl:value-of select="xalan:tokenize($accNoStr, '=')[2]"/>
    </AccNo>
    
    <!-- 账户名称，最大20位 -->    
    <xsl:variable name="accNameStr" select="xalan:tokenize(COLUMN, ' ;')[2]"/> 
    <AccName type="text">
      <xsl:value-of select="xalan:tokenize($accNameStr, '=')[2]"/>
    </AccName>    
   
    <!-- 交易状态 -->
    <xsl:variable name="flagStr" select="xalan:tokenize(COLUMN, ' ;')[3]"/>  
    
    <xsl:choose>
    <xsl:when test="xalan:tokenize($flagStr, '=')[1] = 'ERRDSP'">
    <xsl:variable name="flagStrTwo" select="xalan:tokenize(COLUMN, ' ;')[4]"/>     
    <BankSuccFlag type="text">
      <xsl:value-of select="xalan:tokenize($flagStrTwo, '=')[2]"/>
    </BankSuccFlag>
        
    <!-- 交易金额，整数部分最多13位，小数部分为2位 -->
    <xsl:variable name="payMoneyStr" select="xalan:tokenize(COLUMN, ' ;')[5]"/>        
    <PayMoney type="number">
      <xsl:value-of select="number(xalan:tokenize($payMoneyStr, '=')[2])"/>
    </PayMoney>  
    
    <!-- 备注，最大16位，这里为15位收据号 -->          
    <xsl:variable name="payCodeStr" select="xalan:tokenize(COLUMN, ' ;')[6]"/>     
    <PayCode type="text">
      <xsl:value-of select="xalan:tokenize($payCodeStr, '=')[2]"/>
    </PayCode> 
        
    </xsl:when>
    
    <xsl:otherwise>    
               
    <BankSuccFlag type="text">
      <xsl:value-of select="xalan:tokenize($flagStr, '=')[2]"/>
    </BankSuccFlag>
        
    <!-- 交易金额，整数部分最多13位，小数部分为2位 -->
    <xsl:variable name="payMoneyStr" select="xalan:tokenize(COLUMN, ' ;')[4]"/>        
    <PayMoney type="number">
      <xsl:value-of select="number(xalan:tokenize($payMoneyStr, '=')[2])"/>
    </PayMoney>  
    
    <!-- 备注，最大16位，这里为15位收据号 -->          
    <xsl:variable name="payCodeStr" select="xalan:tokenize(COLUMN, ' ;')[5]"/>     
    <PayCode type="text">
      <xsl:value-of select="xalan:tokenize($payCodeStr, '=')[2]"/>
    </PayCode> 
    </xsl:otherwise>
    </xsl:choose> 
    <!-- 以上的代码块根据不同的银行而不同 -->
     
  </ROW>
    </xsl:otherwise>
  </xsl:choose>     
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

