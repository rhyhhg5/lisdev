<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>   
    <!-- 以下的代码块根据不同的银行而不同 --> 
    <!-- 收据号 -->   
    <PayCode type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
    </PayCode>  
    <!-- 账户名称（姓名）8位 -->
    <AccName type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
    </AccName>
    <!-- 账号，最20位 -->
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[3]"/>
    </AccNo>
    <!-- 交易金额，18位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[4]"/>
    </PayMoney> 
    <!-- 交易日期 8位 -->
    <BankDealDate type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[5]"/>
    </BankDealDate>
    <!-- 标志 4位 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[6]"/>
    </BankSuccFlag>
    <!-- 备注 30位 -->
    <Remark type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[7]"/>
    </Remark>
 
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>


