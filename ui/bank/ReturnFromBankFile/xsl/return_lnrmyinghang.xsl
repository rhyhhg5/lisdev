<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>
    
<xsl:for-each select="BANKDATA/ROW"> 
	<ROW>
  				<!-- 明细序号-->
					
					<!-- 付款人开户行号 -->
					<BankCode type="text">
			      <xsl:value-of select="xalan:tokenize(COLUMN, ',')[2]"/>
			    </BankCode>					
					<!-- 付款人账号-->
					<AccNo type="text">
			      <xsl:value-of select="xalan:tokenize(COLUMN, ',')[3]"/>
			    </AccNo>				
					<!-- 付款人名称 -->
					<AccName type="text">
			      <xsl:value-of select="xalan:tokenize(COLUMN, ',')[4]"/>
			    </AccName>					
					<!-- 缴费方式 -->
					<PayType type="text">
			      <xsl:value-of select="xalan:tokenize(COLUMN, ',')[5]"/>
			    </PayType>					
					<!-- 交易金额，不超过18位-->
					<PayMoney type="text">
			      <xsl:value-of select="xalan:tokenize(COLUMN, ',')[6]"/>
			    </PayMoney>					
				  <!-- 合同协议号-->
					  
				  <!-- 缴费通知书号 -->
					<PayCode type="text">
			      <xsl:value-of select="xalan:tokenize(COLUMN, ',')[8]"/>
			    </PayCode>				  
				  <!-- 各区分号 -->
					<AgentCode type="text">
			      <xsl:value-of select="xalan:tokenize(COLUMN, ',')[9]"/>
			    </AgentCode>		
			    <!-- 标志 2位 -->
			    <BankSuccFlag type="text">
			      <xsl:value-of select="xalan:tokenize(COLUMN, ',')[10]"/>
			    </BankSuccFlag>		
					<!-- 交易日期 ,模板中没有给，自行生成-->
					<BankDealDate type="text">
			  		<xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()" /> 
			  	</BankDealDate>
    </ROW>
</xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>
