<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>15]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>         
    <!-- 账号，最22位 -->        
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/>
    </AccNo>
    <!-- 交易金额 -->
    <PayMoney type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/>
    </PayMoney>
    <!-- 缴费号码 -->
    <PayCode type="text">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[5]"/>
    </PayCode>          
    <!-- 交易状态 -->
    <xsl:variable name="flagDetailStr" select="sum(xalan:tokenize(COLUMN, '|')[6])"/>
    <Pay type="text">
      <xsl:value-of select="$flagDetailStr"/>
    </Pay>     
    <xsl:variable name="flagDetailStr111" select="substring(xalan:tokenize(COLUMN, '|')[6],1,7)"/>          
    <xsl:variable name="flagDetailStr0" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('0'))"/> 
 		<xsl:variable name="flagDetailStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0111'))"/>
 		<xsl:variable name="flagDetailStr2" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0346'))"/>
 		<xsl:variable name="flagDetailStr3" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0657'))"/>
 		<xsl:variable name="flagDetailStr4" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0664'))"/>
 		<xsl:variable name="flagDetailStr5" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('ETS0400'))"/>
 		<xsl:variable name="flagDetailStr6" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('ETS0401'))"/>
 		<xsl:variable name="flagDetailStr7" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0845'))"/>
 		<xsl:variable name="flagDetailStr8" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0866'))"/>
 		<xsl:variable name="flagDetailStr9" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0864'))"/>
 		<xsl:variable name="flagDetailStr10" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0867'))"/>
 		<xsl:variable name="flagDetailStr11" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('ETS0141'))"/>
 		<xsl:variable name="flagDetailStr12" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0766'))"/>
 		<xsl:variable name="flagDetailStr13" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('ETS0402'))"/>
 		<xsl:variable name="flagDetailStr14" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0029'))"/>
 		<xsl:variable name="flagDetailStr15" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0044'))"/>
 		<xsl:variable name="flagDetailStr16" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0861'))"/>
 		<xsl:variable name="flagDetailStr17" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0014'))"/>
 		<xsl:variable name="flagDetailStr18" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('ETS0504'))"/>
 		<xsl:variable name="flagDetailStr19" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('EGG0834'))"/>    
    <xsl:if test="$flagDetailStr=$flagDetailStr0"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'Y'"/>
    </BankSuccFlag>    
    </xsl:if> 
    <xsl:if test="$flagDetailStr111=$flagDetailStr1"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0111'"/>
    </BankSuccFlag>    
    </xsl:if>        
    <xsl:if test="$flagDetailStr111=$flagDetailStr2"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0346'"/>
    </BankSuccFlag>    
    </xsl:if>      
    <xsl:if test="$flagDetailStr111=$flagDetailStr3"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0657'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr4"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0664'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr5"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'ETS0400'"/>
    </BankSuccFlag>    
    </xsl:if>      
    <xsl:if test="$flagDetailStr111=$flagDetailStr6"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'ETS0401'"/>
    </BankSuccFlag>    
    </xsl:if>      
    <xsl:if test="$flagDetailStr111=$flagDetailStr7"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0845'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr8"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0866'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr9"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0864'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr10"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0867'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr11"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'ETS0141'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr12"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0766'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr13"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'ETS0402'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr111=$flagDetailStr14"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0029'"/>
    </BankSuccFlag>    
    </xsl:if>     
    <xsl:if test="$flagDetailStr=$flagDetailStr15"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0044'"/>
    </BankSuccFlag>    
    </xsl:if> 
    <xsl:if test="$flagDetailStr111=$flagDetailStr16"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0861'"/>
    </BankSuccFlag>    
    </xsl:if>    
    <xsl:if test="$flagDetailStr111=$flagDetailStr17"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0014'"/>
    </BankSuccFlag>    
    </xsl:if>    
    <xsl:if test="$flagDetailStr=$flagDetailStr18"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'ETS0504'"/>
    </BankSuccFlag>    
    </xsl:if>    
    <xsl:if test="$flagDetailStr111=$flagDetailStr19"> 		
    <BankSuccFlag type="text">
      <xsl:value-of select="'EGG0834'"/>
    </BankSuccFlag>    
    </xsl:if>     
        <!--交易时间-->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>  
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

