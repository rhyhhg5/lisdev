<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW[position()>1]">
  <ROW>  
  	
   <!-- 以下的代码块根据不同的银行而不同 -->   
   	<xsl:variable name="accNameValue" select="substring(COLUMN, 21,35)"/>
		<xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '35')"/> 
		  	 <!-- 账户名称（姓名）8位 -->
		<AccName type="text">
		   <xsl:value-of select="substring(COLUMN, 21,35-$chinaLen)" />
		</AccName>   
  
    <!-- 账号 --> 
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 56-$chinaLen, 30)"/>
    </AccNo>   
    
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 86-$chinaLen, 18)"/>
    </PayMoney>
  
  	<xsl:variable name="accNameValue1" select="substring(COLUMN, 1)"/>
 		<xsl:variable name="chinaLen1" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue1)"/> 
		<BankDealDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN, 136-$chinaLen1, 8),1,4), '-', substring(substring(COLUMN, 136-$chinaLen1, 8),5,2), '-', substring(substring(COLUMN, 136-$chinaLen1, 8),7,2))"/>
   	</BankDealDate>
   
   	<BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 144-$chinaLen1, 4)"/>
   	</BankSuccFlag>
   
  <!-- 以上的代码块根据不同的银行而不同 -->
     
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>




