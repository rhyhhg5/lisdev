﻿<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>    
  <xsl:for-each select="BANKDATA/ROW[position()>0]"> 
    <ROW>        
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>
     <BankSuccFlag type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[1]"/> 
    </BankSuccFlag>
    <AccNo type="text"> 
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[2]"/> 
    </AccNo>
    <PayMoney type="number">
      <xsl:value-of select="xalan:tokenize(COLUMN, '|')[4]"/>
    </PayMoney> 
    </ROW>  
  </xsl:for-each>
</BANKDATA>
</xsl:template>
</xsl:stylesheet>

