<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <ROW>
   <xsl:for-each select="BANKDATA/ROW[position()=1]"> 	
    <!-- 以下的代码块根据不同的银行而不同 --> 

    <!-- 总笔数，8位 -->
    <TotalNum type="number">
      <xsl:value-of select="number(substring(COLUMN, 48, 8))"/>
    </TotalNum>
    
    <!-- 总金额，16位，包括小数点后两位 -->
    <TotalMoney type="number">
      <xsl:value-of select="number(substring(COLUMN, 56, 16))"/>
    </TotalMoney>
    
    <!-- 银行成功总数量，8位 -->
    <BankSuccNum type="number">
      <xsl:value-of select="number(substring(COLUMN, 72, 8))"/>
    </BankSuccNum>
    
    <!-- 银行成功总金额，14位，包括小数点后两位 -->
    <BankSuccMoney type="number">
      <xsl:value-of select="number(substring(COLUMN, 80, 16))"/>
    </BankSuccMoney>
    
    <!-- 财务确认总金额，14位，包括小数点后两位 -->
    <AccTotalMoney type="number">
      <xsl:value-of select="number(substring(COLUMN, 80, 16))"/>
    </AccTotalMoney>
    
    <!-- 划款日期 -->
    <TransDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN, 117, 8),1,4), '-', substring(substring(COLUMN, 117, 8),5,2), '-', substring(substring(COLUMN, 117, 8),7,2))"/>
    </TransDate>
    <!-- 以上的代码块根据不同的银行而不同 -->
    </xsl:for-each> 	
  </ROW>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

