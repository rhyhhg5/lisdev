<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>=1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
  <ROW>         
	<!-- 账号 -->        
   			<AccNo type="text"> 
		    	<xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[1]"/>
		    </AccNo>
		    <!-- 帐户名  -->        
		    <AccName type="text"> 
		      <xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[2]"/>
		    </AccName>
		    <!-- 交易金额 -->
		    <xsl:variable name="payMoneyStr" select="xalan:tokenize(COLUMN, '&#9;')[3]"/>
		    <PayMoney type="number">
		      <xsl:value-of select="number($payMoneyStr)"/> 
		    </PayMoney> 
		    <PayCode type="text"> 
		      <xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[4]"/>
		    </PayCode> 
		    <BankSuccFlag type="text"> 
		      <xsl:value-of select="xalan:tokenize(COLUMN, '&#9;')[5]"/>
		    </BankSuccFlag> 
		    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    		<BankDealDate type="text">
      		<xsl:value-of select="$Today"/>
    		</BankDealDate>  
	</ROW>   
    <!-- 以上的代码块根据不同的银行而不同 -->   
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

