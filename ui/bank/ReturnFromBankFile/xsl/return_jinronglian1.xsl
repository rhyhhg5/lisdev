<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">
<BANKDATA>    
 <xsl:for-each select="BANKDATA/ROW"> 
  <row> 
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN,25,32)"/>
    </AccNo>
    <PayMoney type="number">
        <xsl:value-of select="substring(COLUMN,57,12)"/>
    </PayMoney> 
    <BankAccFlag type="number">
        <xsl:value-of select="substring(COLUMN,69,1)"/>
    </BankAccFlag> 
   <!-- �������� -->
		<BankDealDate type="text">
			  <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
		</BankDealDate>
    </row>
 </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

