<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>

    <!-- 交易金额，10位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 43, 10) div 100 "/>
    </PayMoney>
     <!--交易时间-->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>  
    <xsl:variable name="SucFlag" select="substring(COLUMN, 61, 4)"/>
    
    <xsl:choose>
    	<xsl:when test="$SucFlag='    '">
      	<BankSuccFlag type="text">
       	<xsl:value-of select="'1'"/>
    		</BankSuccFlag>
    	</xsl:when>
    	<!-- 多于五个汉字的处理 -->
    	<xsl:otherwise>
      	<BankSuccFlag type="text">
       <xsl:value-of select="$SucFlag"/>
    		</BankSuccFlag>
    	</xsl:otherwise>
  	</xsl:choose>
    
    <!-- 收据号 -->   
    <PayCode type="text"> 
     <xsl:value-of select="substring(COLUMN, 65, 15)"/>
    </PayCode>
       
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

