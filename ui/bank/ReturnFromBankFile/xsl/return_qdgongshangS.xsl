<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
<xsl:for-each select="BANKDATA/ROW"> 
  <ROW>  
    <AccName type="text"> 
       <xsl:value-of select="xalan:tokenize(COLUMN, ',')[1]"/> 
    </AccName>    
    <AccNo type="text">
       <xsl:value-of select="xalan:tokenize(COLUMN, ',')[2]"/>
    </AccNo>
    <PayMoney type="number">
       <xsl:value-of select="xalan:tokenize(COLUMN, ',')[3]"/>
    </PayMoney>
    <BankDealDate type="text">
       <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()" />
    </BankDealDate>   
   
		<xsl:variable name="flagStr" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK(xalan:tokenize(COLUMN, ',')[5]))"/> 
    <xsl:variable name="flagStr1" select="string(java:com.sinosoft.utility.StrTool.unicodeToGBK('ȫ���ɹ�'))"/> 
    <xsl:if test="$flagStr=$flagStr1"> 
    <BankSuccFlag type="text">
      <xsl:value-of select="'1'"/>
    </BankSuccFlag>
    </xsl:if>
   
	</ROW>  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

