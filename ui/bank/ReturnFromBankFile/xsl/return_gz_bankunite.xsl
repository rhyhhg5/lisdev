<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>
<xsl:template match="/">   
<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
  	<xsl:choose>
			<xsl:when test="position()=last()">
			</xsl:when>
			<xsl:otherwise>		
		    <!-- 以下的代码块根据不同的银行而不同 -->
		    <row>
		    	<AccNo type="text">
			      <xsl:value-of select="substring(COLUMN,15,30)"/>
			    </AccNo> 
			    <!-- 中文字符处理 -->
			    <xsl:variable name="accNameValue" select="substring-after(COLUMN, substring(COLUMN,1,45))"/>
			    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '10')"/>
			    
			     <!-- 账户名称（姓名） -->
			    <AccName type="text">
			      <xsl:value-of select="substring(COLUMN,46,10-$chinaLen)"/>
			    </AccName>   
			    <!-- 交易金额，12位 -->
			    <PayMoney type="text">
			      <xsl:value-of select="substring(COLUMN,75-$chinaLen,12) div 100"/>
			    </PayMoney>
			    <!-- 成功标记  -->    
			    <BankSuccFlag type="text">
						<xsl:value-of select="substring(COLUMN,87-$chinaLen,2)"/>
					</BankSuccFlag>     
			    <PayCode type="text">
			      <xsl:value-of select="substring(COLUMN,89-$chinaLen,20)"/>
			    </PayCode>            
			    <!-- 交易日期 -->
			    <BankDealDate type="text">
			      <xsl:value-of select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()"/>
			    </BankDealDate>
    		</row>  
    	</xsl:otherwise>
    </xsl:choose>	
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

