<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">
  <xsl:variable name="tDetailFlag"  select ="substring(COLUMN, 4, 1)"/> 
  	<xsl:choose>
  	<xsl:when test="$tDetailFlag='2'">
  				<ROW>  
				   <!-- 以下的代码块根据不同的银行而不同 -->  
				   	<BankSuccFlag type="text">
								<xsl:value-of select="substring(COLUMN, 1, 3)"/>
						</BankSuccFlag>
				   
				    
				    <!-- 账号，不够19位后面补空格 --> 
				    <AccNo type="text">
				      <xsl:value-of select="substring(COLUMN, 17, 19)"/>
				    </AccNo>   
				 
				  <!-- 交易金额，12位， -->
				   <PayMoney type="number">
				      <xsl:value-of select="substring(COLUMN, 43, 17) div 100"/>
				   </PayMoney>
				   
				   <xsl:variable name="tBankDealDate"  select ="substring(COLUMN, 112, 8)"/> 
				   <BankDealDate type="text">
							<xsl:value-of select="concat(substring($tBankDealDate, 1, 4),'-',substring($tBankDealDate, 5, 2),'-',substring($tBankDealDate, 7, 2))"/>
					 </BankDealDate>
				
				   
				  <PayCode type="text">
				     <xsl:value-of select="substring(COLUMN, 141, 61)"/>
				  </PayCode>

  <!-- 以上的代码块根据不同的银行而不同 -->
     
  			</ROW>
  			</xsl:when>
	</xsl:choose>
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>




