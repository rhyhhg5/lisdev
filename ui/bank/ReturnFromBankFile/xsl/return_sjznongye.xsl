<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 -->   
    <!-- 保单号，使用收据号代替 -->
    <PayCode type="text">
      <xsl:value-of select="substring(COLUMN, 1, 20)"/>
    </PayCode>
    
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring-after(COLUMN, substring(COLUMN, 1, 20))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '30')"/>
    <!-- 账户名称（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 21, 30-$chinaLen)"/>
    </AccName>
    
    <!-- 账号，19位 -->
    <!-- 因为真实账号中最后为*号，而银行返回数据中没有，返回处理会出错，故暂不处理 
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 51-$chinaLen, 41)"/>
    </AccNo>   
    -->
    
    <!-- 交易金额，10位，包括小数点后两位，注意，工行返回数据中有小数点 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 92-$chinaLen, 37)"/>
    </PayMoney>
   
    <!-- 交易日期 -->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(substring(COLUMN, 129-$chinaLen, 8),1,4), '-', substring(substring(COLUMN, 129-$chinaLen, 8),5,2), '-', substring(substring(COLUMN, 129-$chinaLen, 8),7,2))"/>
    </BankDealDate>
    
    <!-- 交易状态（成功标志） -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 137-$chinaLen, 5)"/>
    </BankSuccFlag>
    <!-- 以上的代码块根据不同的银行而不同 -->
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

