<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xalan="http://xml.apache.org/xalan"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      
<BANKDATA>
    
  <xsl:for-each select="BANKDATA/ROW[position()>1]"> 
    <!-- 以下的代码块根据不同的银行而不同 -->
    <ROW>
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN, 21, 8)"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '4')"/>
    <!-- 账户名称，最大20位 -->    
    <AccName type="text"> 
      <xsl:value-of select="substring(COLUMN, 21, 8-$chinaLen)"/>
    </AccName>
        
    <!-- 账号，最大16位 -->        
    <AccNo type="text"> 
      <xsl:value-of select="substring(COLUMN, 53-$chinaLen, 30)"/>
    </AccNo>
    
    <!-- 交易金额，整数部分最多13位，小数部分为2位 -->
    <xsl:variable name="payMoneyStr" select="substring(COLUMN, 83-$chinaLen, 18)"/>
    <PayMoney type="number">
      <xsl:value-of select="$payMoneyStr"/>
    </PayMoney> 
    
    <!-- 中文字符处理 -->
    <xsl:variable name="strDescValue" select="substring(COLUMN, 101-$chinaLen)"/>
    <xsl:variable name="descLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($strDescValue)"/>
    <xsl:variable name="sumChinaLen" select="$chinaLen+$descLen"/>
        
    <!--交易时间-->
    <BankDealDate type="text">
      <xsl:value-of select="concat(substring(COLUMN, 131-$sumChinaLen, 4), '-', substring(COLUMN, 135-$sumChinaLen, 2), '-', substring(COLUMN, 137-$sumChinaLen, 2))"/>
    </BankDealDate>
    
    <!-- 交易状态 -->
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN, 139-$sumChinaLen, 4)"/>
    </BankSuccFlag>
    
    </ROW>  
    <!-- 以上的代码块根据不同的银行而不同 -->    
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

