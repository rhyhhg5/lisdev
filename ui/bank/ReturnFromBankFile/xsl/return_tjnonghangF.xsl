<?xml version="1.0" encoding="iso-8859-1"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:include href="return_function.xsl"/>

<xsl:template match="/">      

<BANKDATA>

  <xsl:for-each select="BANKDATA/ROW">

  <ROW>
    
    <!-- 以下的代码块根据不同的银行而不同 --> 
    <!-- 中文字符处理 -->
    <xsl:variable name="accNameValue" select="substring(COLUMN, substring(COLUMN, 0, 0))"/>
    <xsl:variable name="chinaLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue, '20')"/>    
    <!-- 账户名称（姓名） -->
    <AccName type="text">
      <xsl:value-of select="substring(COLUMN, 1, 20-$chinaLen)"/>
    </AccName>
    <!-- 交易日期 -->
    <xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
    <BankDealDate type="text">
      <xsl:value-of select="$Today"/>
    </BankDealDate>
    <!-- 交易金额，15位，包括小数点后两位 -->
    <PayMoney type="number">
      <xsl:value-of select="substring(COLUMN, 22-$chinaLen, 15)"/>
    </PayMoney>
    <!-- 账号，30位 -->
    <AccNo type="text">
      <xsl:value-of select="substring(COLUMN, 38-$chinaLen, 30)"/>
    </AccNo>
    <!-- 收据号，30位 -->
    <PayCode type="text">
      <xsl:value-of select="substring(COLUMN, 90-$chinaLen, 21)"/>
    </PayCode>

    <!-- 交易状态 -->    
    <xsl:variable name="LongStr"  select="string-length(COLUMN)+$chinaLen"/>
    <BankSucc type="text">
      <xsl:value-of select="$LongStr"/>
    </BankSucc>     
    <xsl:variable name="LongStr1" select="111"/> 
    <xsl:variable name="LongStr2" select="112"/>      
    <xsl:if test="$LongStr=$LongStr1" >
    <BankSuccFlag type="text">
      <xsl:value-of select="'Y'"/>
    </BankSuccFlag>    
    </xsl:if>
    <xsl:if test="$LongStr=$LongStr2" >
    <BankSuccFlag type="text">
      <xsl:value-of select="substring(COLUMN,112-$chinaLen, 1)"/>
    </BankSuccFlag>   
    </xsl:if>      
    
  </ROW>
  
  </xsl:for-each>

</BANKDATA>

</xsl:template>

</xsl:stylesheet>

