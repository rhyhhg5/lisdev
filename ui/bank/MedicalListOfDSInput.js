//ReadFromFileInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var filePath;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

function succButton(){
	
	var serialno = fm.serialNo.value;
	var querySQL = "select getUniteCode(rfb.Agentcode),"
	    +"   lag.name, "
	    +"   rfb.bankcode, "
	    +"   rfb.accno, "
	    +"  rfb.accname, "
	    +"   rfb.paycode, "
	    +"   rfb.polno, "
	    +"   case rfb.paytype "
	    +"     when '1' then "
	    +"     '首期' "
	    +"     else "
	    +"      '续期' "
	    +"   end, "
	    +"   paymoney, "
	    +"   rfb.comcode,'' "
	    +" from lyreturnfrombank rfb "
	    +" left join laagent lag "
	    +" on rfb.agentcode = lag.agentcode "
	    +" where serialno = '"+ serialno +"' "
	    +" and banksuccflag = "
	    +"   (select substr(agentpaysuccflag, 1, length(agentpaysuccflag) - 1) "
	    +"     from ldmedicalcom "
	    +"     where rfb.bankcode = medicalcomcode) and dealtype = '"+ dealType +"' with ur";
	fm.querySql.value = querySQL;
	fm.filename.value = 'SuccessList_';
	//提交，下载
	DownLoad(serialno);
}
function failButton(){
	
	var serialno = fm.serialNo.value;
	var querySQL = "select getUniteCode(rfb.Agentcode),"
	    +"   lag.name, "
	    +"   rfb.bankcode, "
	    +"   rfb.accno, "
	    +"  rfb.accname, "
	    +"   rfb.paycode, "
	    +"   rfb.polno, "
	    +"   case rfb.paytype "
	    +"     when '1' then "
	    +"     '首期' "
	    +"     else "
	    +"      '续期' "
	    +"   end, "
	    +"   paymoney, "
	    +"   rfb.comcode ,"
	    +" (select codename from ldcode1 where codetype='bankerror' and code=rfb.bankcode and code1=rfb.banksuccflag)"
	    +" from lyreturnfrombank rfb "
	    +" left join laagent lag "
	    +" on rfb.agentcode = lag.agentcode "
	    +" where serialno = '"+ serialno +"' "
	    +" and banksuccflag <> "
	    +"   (select substr(agentgetsuccflag, 1, length(agentgetsuccflag) - 1) "
	    +"     from ldmedicalcom "
	    +"     where rfb.bankcode = medicalcomcode) and dealtype = '"+ dealType +"' with ur";
	fm.querySql.value = querySQL;
	fm.filename.value = 'FailedList_';
	//提交，下载
	DownLoad(serialno);
}

//下载
function DownLoad(serialno) {
	
	if(serialno != null && serialno != "" )
    {
        var formAction = fm.action;
        fm.action = "MedicalListOfDSDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先选择批次，再生成清单！");
        return ;
    }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

//获取文件上传保存路径
function getUploadPath() {
  var strSql = "select SysVarValue from LDSysVar where SysVar = 'ReturnFromBankPath'";
  
  filePath = easyExecSql(strSql);
}

//查询出本机构未返回处理的银行批次
function initQuery() {
  var strSql = "select BankCode, Serialno, MakeDate,ReturnDate from LYBankLog where ReturnDate is not null and ComCode like '" + comCode + "%'"
             + " and LogType='" + dealType + "'" + " order by returndate desc";
  
  turnPage.queryModal(strSql, BankGrid);
}

//初始化银行代码
function initMedicalCode() {
  var strSql = "select MedicalComCode, MedicalComName from LDMedicalCom where  ComCode = '" + comCode + "'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("MedicalCode").CodeData = strResult;
  }
}   

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句
	var strSql = "select BankCode, Serialno,MakeDate,ReturnDate from LYBankLog where ReturnDate is not null and ComCode like '" + comCode + "%'"
	           + " and LogType='" + dealType + "'"
				     + getWherePart('BankCode','MedicalCode')
				     + getWherePart('ReturnDate')
				     + " order by returndate desc";
				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
}
//显示银行批次数据的统计信息，金额、件数
function showStatistics(parm1, parm2) {
  var strSql = "select Totalmoney, Totalnum from lybanklog where SerialNo = '" 
             + BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2) 
             + "'";
  fm.serialNo.value = BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2);
//  alert(fm.serialNo.value);
  //alert(easyExecSql(strSql));      
  var arrResult = easyExecSql(strSql);
  
  fm.all("TotalMoney").value = arrResult[0][0];
  fm.all("TotalNum").value = arrResult[0][1];
  tSelNo = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
}
