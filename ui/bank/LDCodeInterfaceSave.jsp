<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDCodeInterfaceEdit.jsp
//程序功能：修改代码定义
//创建日期：2006-10-17 19:56
//创建人  ：Wulg
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.tb.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDCodeInterfaceSet tLDCodeInterfaceSet = new LDCodeInterfaceSet();
  LDCodeInterfaceUI tLDCodeInterfaceUI = new LDCodeInterfaceUI();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  
  String SysName = request.getParameter("SysName");
  String CodeType = request.getParameter("CodeType");
  String CodeTypeNmae = request.getParameter("CodeTypeNmae");
  String[] chk = request.getParameterValues("CodeGridNo");
  String[] interfaceCode = request.getParameterValues("CodeGrid1");
  String[] codeName = request.getParameterValues("CodeGrid2");
  String[] code = request.getParameterValues("CodeGrid3");
  String[] MakeDate = request.getParameterValues("CodeGrid4");
  String[] MakeTime = request.getParameterValues("CodeGrid5");
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
  System.out.println("开始设置Schema:" + transact);
  for ( int i = 0; i < chk.length; i++) {
  LDCodeInterfaceSchema tLDCodeInterfaceSchema = new LDCodeInterfaceSchema();
    tLDCodeInterfaceSchema.setSysName(SysName);
    tLDCodeInterfaceSchema.setCodeType(CodeType);
    tLDCodeInterfaceSchema.setCodeTypeNmae(CodeTypeNmae);
    tLDCodeInterfaceSchema.setInterfaceCode(interfaceCode[i]);
    tLDCodeInterfaceSchema.setCode(code[i]);
    tLDCodeInterfaceSchema.setCodeName(codeName[i]);
    tLDCodeInterfaceSet.add(tLDCodeInterfaceSchema);
    if("UPDATE||MAIN".equals(transact)) {
      tLDCodeInterfaceSchema.setMakeDate(MakeDate[i]);
      tLDCodeInterfaceSchema.setMakeTime(MakeTime[i]);
    }
  }
  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
    System.out.println(tLDCodeInterfaceSet.encode());
	  tVData.add(tLDCodeInterfaceSet);
  	tVData.add(tG);
    tLDCodeInterfaceUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDCodeInterfaceUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 保存成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
