<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：WriteToFileInput.jsp
//程序功能：
//创建日期：2013-8-3 11:10:36
//创建人  ：王 硕
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head > 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="MedicalListOfSettleInput.js"></SCRIPT> 
  <%@include file="MedicalListOfSettleInit.jsp"%>
  
  <title>生成发送医保文件 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  comCode = "<%=tGlobalInput.ComCode%>";
  dealType = "<%=request.getParameter("DealType")%>";
</script>

<body  onload="initForm();" >
  <form name=fm target="fraTitle"  method = "post">
    <%@include file="../common/jsp/InputButton.jsp"%>
    
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
	
    <table  class= common align=center>
      	<TR  class= common>
 <TD  class= title>
        医保机构代码
      </TD>
      <TD  class= input>
        <Input NAME=MedicalCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('MedicalCode',[this,MedicalCodeName],[0,1]);" onkeyup="return showCodeListKey('MedicalCode',[this,MedicalCodeName],[0,1]);" verify="医保机构代码|notnull&code:MedicalCode" ><input class=codename name=MedicalCodeName readonly=true >
      </TD>
          <TD  class= title>
          回盘时间
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=ReturnDate >
          </TD>         
	          <TD  class= input>
	            <INPUT VALUE="查  询" TYPE=button class=CssButton onclick="easyQueryClick();">
	          </TD>
        </TR>
    </table>
    
    <hr>  
           
    <!-- 批次号信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 请选择批次号：
    		</td>
    		<td>
				<font color="#FF0000" size=2>
		        	默认显示近三天的结算数据
		        </font>
		   </td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=CssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=CssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=CssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=CssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
    </Div>

    
      <br>
    
    
      
    <INPUT VALUE="" TYPE=hidden name=serialNo>
    <INPUT VALUE="" TYPE=hidden name=Url>
    <INPUT VALUE="" TYPE=hidden name=downfilesum>
    <INPUT VALUE="" TYPE=hidden name=downflag>
    <INPUT VALUE="" TYPE=hidden  name=DealType>
    <INPUT VALUE="" TYPE=hidden  name=querySql>
    <INPUT VALUE="" TYPE=hidden  name=filename>
    <input type=hidden id="fmtransact" name="fmtransact">
    <hr>
    <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>批次数据统计：</td>
		</tr>
	  </table> 
	     		  								
    <table  class= common align=center>
    	<TR  class= common>
        <TD  class= title>
        批次总金额
        </TD>
        <TD  class= input>
          <Input NAME=TotalMoney class=common >
        </TD>
        <TD  class= title>
          批次总笔数
        </TD>
        <TD  class= input>
          <Input name=TotalNum class=common >
        </TD>

      </TR>
    </table>
    <br>
   <INPUT VALUE="汇总清单下载" class= CssButton TYPE=button onclick="HZButton()">
   <INPUT VALUE="明细清单下载" class= CssButton TYPE=button onclick="MXButton()">
 
   <font color="red">结算批次内不成功的可再做结算</font>
   <br><br><br>
    <hr> 
	    <table class= common border=0 width=100%>
	    	<tr>
				<td class= titleImg align= center>已确认未做结算的保单清单</td>
	  		</tr>
	  	</table>
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        医保机构编码
      </TD>
      <TD  class= input>
        <Input NAME=MedicalCode2 CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('MedicalCode',[this,MedicalName2],[0,1]);" onkeyup="return showCodeListKey('MedicalCode',[this,MedicalName2],[0,1]);" ><input class=codename name=MedicalName2 readonly=true >
      </TD>
      <TD  class= input>
        <INPUT VALUE="&nbsp;查询&nbsp;"  
    	class=cssButton TYPE=button name=sub onclick="QueryGrid1Data()">
    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    	<INPUT VALUE="&nbsp;下载&nbsp;"  
    	class=cssButton TYPE=button name=sub onclick="download1()">
      </TD>
      <TD  class= input>
        &nbsp;
      </TD>  
    </TR>
    </table>
    
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGrid);">
    		</td>
    		<td class= titleImg>
    			查询结果清单：
    		</td>
    	</tr>
    </table>
    <Div  id= "divGrid" style= "display: ''" align=center>
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanQuery1Grid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	  <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage2.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage2.lastPage();">
  	</div> 
    <br>		
     <input type = 'hidden' name = 'querySql1' >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>

