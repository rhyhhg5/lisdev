<%@page contentType="text/html;charset=GBK" %>

 
<html> 
<%
//程序名称：WriteToFileInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head > 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <%@page import="com.sinosoft.lis.pubfun.*" %>
  <%@include file="SendBankConfInit.jsp"%>
  <SCRIPT src="SendBankConfInput.js"></SCRIPT> 
  <title>生成送银行文件 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>
<script>
  comCode = "<%=tGlobalInput.ComCode%>";
  bankcode= "<%=request.getParameter("BankCode")%>";
  dealType = "<%=request.getParameter("DealType")%>";
</script>


<body onload="initForm();initElementtype();"   >
  <form action="./SendBankConfSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
	
    <table  class= common>
      	<TR  class= common>
          <TD  class= title>
            银行代码
         </TD>
         <TD  class= input>
            <Input NAME=BankCode CLASS=codeno ondblclick="return showCodeList('sendbank4',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank4',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank" ><input class=codename name=BankCodeName readonly=true >
         </TD>
         <TD  class= title>
            银行类型
          </TD>
          <TD  class= input>
             <Input class=codeno name=BankType verify="NOTNULL"CodeData="0|^1|非中国银行^2|中国银行" ondblClick="showCodeListEx('BankType',[this,BankTypeName],[0,1]);"onkeyup="showCodeListKeyEx('BankType',[this,BankTypeName],[0,1]);"><input class=codename name=BankTypeName readonly=true >
          </TD>
        </TR>
     </table>
        <br>
	    <INPUT VALUE="代  收  查  询" TYPE=button class=CssButton onclick="easyQueryClick();">
        <INPUT VALUE="代  付  查  询" TYPE=button class=CssButton onclick="easyQueryClick1();">
       
    <!-- 批次号信息（列表） -->
    
    <hr>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 清单列表：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	<input type = hidden name = sub>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=CssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=CssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=CssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=CssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
    </Div>

    
    <br>
    <INPUT VALUE="审核不确认" class= CssButton TYPE=button onclick="submitForm1()">
    <br><br><hr>
     <div style="color:red">点击审核确认会对列表中全部数据进行确认</div>
    <INPUT VALUE="审核确认" class= CssButton TYPE=button onclick="submitForm()">
    
    <INPUT VALUE="" TYPE=hidden name=serialNo>
    <INPUT VALUE="" TYPE=hidden name=Url>
    
    <INPUT VALUE="" TYPE=hidden  name=DealType>
    <input type=hidden id="fmtransact" name="fmtransact">
    <INPUT VALUE="" TYPE=hidden  name=flag>
    <INPUT VALUE="" TYPE=hidden  name=buttonflag>
    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>