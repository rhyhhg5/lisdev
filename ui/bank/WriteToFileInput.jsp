<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：WriteToFileInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head > 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="WriteToFileInput.js"></SCRIPT> 
  <%@include file="WriteToFileInit.jsp"%>
  
  <title>生成送银行文件 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  comCode = "<%=tGlobalInput.ComCode%>";
  dealType = "<%=request.getParameter("DealType")%>";
</script>

<body  onload="initForm();" >
  <form action="./WriteToFileSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
	
    <table  class= common align=center>
      	<TR  class= common>
 <TD  class= title>
        银行代码
      </TD>
      <TD  class= input>
        <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank" ><input class=codename name=BankCodeName readonly=true >
      </TD>
          <TD  class= title>
            文件生成日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=MakeDate >
          </TD>
	          <TD  class= input>
	            <INPUT VALUE="查  询" TYPE=button class=CssButton onclick="easyQueryClick();">
	          </TD>
        </TR>
    </table>
    
    <hr>  
           
    <!-- 批次号信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 请选择批次号：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=CssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=CssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=CssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=CssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
    </Div>

    
    <br>
    
    <!--
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        请输入要生成的文件名称:
      </TD>
      <TD  class= input>
        <Input class= common5 TYPE=file name=FileName verify="文件名称|notnull" > 
      </TD>           
    </TR>
    </table>
    -->
    
    <Div id=DivFileDownload style="display:'none'">
      <A id=fileUrl href=""></A>
    </Div>
      
    <INPUT VALUE="生成文件" class= CssButton TYPE=button onclick="submitForm()">
    <INPUT VALUE="文件下载" class= CssButton TYPE=button onclick="fileDownload()">
    <br><hr>
    <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>银联银行文件下载：</td>
		</tr>`
	  </table> 
    <INPUT VALUE="总汇文件下载" class= CssButton TYPE=button onclick="fileDownload1()">
    <INPUT VALUE="明细文件下载" class= CssButton TYPE=button onclick="fileDownload2()">
    <INPUT VALUE="" TYPE=hidden name=serialNo>
    <INPUT VALUE="" TYPE=hidden name=Url>
    <INPUT VALUE="" TYPE=hidden name=downfilesum>
    <INPUT VALUE="" TYPE=hidden name=downflag>
    <INPUT VALUE="" TYPE=hidden  name=DealType>
    <input type=hidden id="fmtransact" name="fmtransact">
    
    <br><br><hr>
    <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>银行批次数据统计：</td>
		</tr>
	  </table> 
	     		  								
    <table  class= common align=center>
    	<TR  class= common>
        <TD  class= title>
          批次总金额
        </TD>
        <TD  class= input>
          <Input NAME=TotalMoney class=common >
        </TD>
        <TD  class= title>
          批次总笔数
        </TD>
        <TD  class= input>
          <Input name=TotalNum class=common >
        </TD>

      </TR>
    </table>
    <br>
  
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>

