//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  initPersonGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
// 查询按钮
function easyQueryClick()
{	
	var strSQL = "";
	var bankNo = fm.all('BankNo').value;
	var bankName = fm.all('BankName').value;
	
	if(bankNo != null && bankNo != "")
	{
		bankNo = "and bankcode like '%" + bankNo + "%' ";
	}
	if(bankName != null && bankName != "")
	{
		bankName = "and bankName like '%" + bankName + "%' ";
	}
		
	// 初始化表格
	initBankGrid();
	var strSQL = "select bankcode,bankname from ldbank "
	           + " where (BankUniteFlag is null or BankUniteFlag<>'1') and cansendflag='1' and"
             + " Comcode like '" + fm.all('comcode').value + "%' "
             + bankNo 
             + bankName
             + " order by BankCode";
  
  turnPage.pageDivName = "divPage";
	turnPage.queryModal(strSQL, BankGrid); 
}

// 数据返回父窗口
function returnParent()
{
	var arrReturn = new Array();
	var tSel = BankGrid.getSelNo();
	if( tSel == 0 || tSel == null )
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		try
		{
			arrReturn = getQueryResult();
			top.opener.afterSendBankQuery( arrReturn );
			top.close();
		}
		catch(ex)
		{
			alert( "请先选择一条非空记录，再点击返回按钮。");
			//alert( "没有发现父窗口的afterQuery接口。" + ex );
		}
		
	}
}

function getQueryResult()
{
	//获取正确的行号
	tRow = BankGrid.getSelNo() - 1;

	arrSelected = new Array();
	//设置需要返回的数组
	arrSelected[0] = BankGrid.getRowColData(tRow, 1); 
	arrSelected[1] = BankGrid.getRowColData(tRow, 2);
	return arrSelected;
}





