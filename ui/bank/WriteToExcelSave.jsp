<%@page contentType="text/html;charset=GBK"%>
<%
	//程序名称：WriteToExcelSave.jsp
	//程序功能：
	//创建日期：2013-2-7
	//创建人  ：张成轩
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.bank.*"%>
<%@page import="com.sinosoft.lis.easyscan.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>


<%
	System.out.println("\n\n---WriteToFileSave Start---");

	String serverUrl = request.getParameter("Url");
	
	String DownloadPath = application.getRealPath("bank/SendToBankFile/") + "/";
	System.out.println(DownloadPath);

	String DealType = request.getParameter("DealType");
	System.out.println("DealType : " + DealType);

	LYBankLogSchema tLYBankLogSchema = new LYBankLogSchema();
	tLYBankLogSchema.setSerialNo(request.getParameter("serialNo"));
	
	System.out
			.println("serialNo : " + request.getParameter("serialNo"));

	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");
	VData inVData = new VData();

	String Content = "";
	String FlagStr = "";
	int flag = -1;

	String action = request.getParameter("fmtransact");/*下载或生成文件标记*/
	System.out.println(action);

	if (action.equals("download")) {
		flag = 0;
	} else {
		flag = 1;

		inVData.add(tLYBankLogSchema);
		inVData.add(DownloadPath);
		inVData.add(tGlobalInput);
		//生成文件
		WriteToExcelBL tWriteToExcelUI = new WriteToExcelBL();
		if (!tWriteToExcelUI.submitData(inVData, "WRITE")) {
			CErrors errors = tWriteToExcelUI.getErrors();
			Content = " 处理失败，原因是:" + (String) errors.getFirstError();
			FlagStr = "Fail";
		} else {
			Content = " 处理成功! ";
			FlagStr = "Success";
		}

		System.out.println(Content + "\n" + FlagStr + "\n---WriteToFileSave End---\n\n");
	}
%>
<html>
	<script language="javascript">
    	if ("<%=FlagStr%>" != "Fail")
    	{
    		parent.fraInterface.downAfterSubmit('<%=serverUrl%>',<%=flag%>);
    	}
    	if (<%=flag%> == 1)
    	{
    		parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
    	}
    	
    </script>
</html>
