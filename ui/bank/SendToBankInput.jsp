<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：SendToBankInput.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

  <SCRIPT src="SendToBankInput.js"></SCRIPT>
  <%@include file="SendToBankInit.jsp"%>
  
  <title>银行代收 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  var comCode = "<%=tGlobalInput.ComCode%>";
  var cDate = "<%=PubFun.getCurrentDate()%>";
</script>

<body  onload="initForm();" >
  <form action="./SendToBankSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 银行代收 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请选择银行，并输入代收日期区间</td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        银行代码
      </TD>
      <TD  class= input>
        <Input NAME=BankCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('sendbank2',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKey('sendbank2',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank2" ><input class=codename name=BankCodeName readonly=true >
      </TD>
      <TD  class= title>
        起始日期
      </TD>
      <TD  class= input>
        <Input class="coolDatePicker" dateFormat="short" name=StartDate verify="起始日期|date" >
      </TD>
      <TD  class= title>
        终止日期
      </TD>
      <TD  class= input>
        <Input class="coolDatePicker" dateFormat="short" name=EndDate verify="终止日期|notnull&date" >
      </TD>
    </TR>
    </table>
    
    <br>
		<br>
    
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>包括首期和续期续保所有数据：</td>
  		</tr>
  	</table>
    <INPUT VALUE="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;银行代收&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"  
    	class=cssButton TYPE=button name=sub onclick="submitForm()">
    <INPUT VALUE="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;银行代付&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"  
    	class=cssButton TYPE=button name=sub onclick="submitForm1()">	 
    
    <br><br><!--hr--><br>
    <INPUT  TYPE=hidden  name=flag>
    <!--table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>包括首期和续期续保所有数据（应根据业务部门需求进行）：</td>
  		</tr>
  	</table-->
  	
    <INPUT VALUE="银行代收（续期续保）"  class=cssButton TYPE=hidden onclick="submitFormForXQ()">
    
    <Input type="hidden" name=typeFlag >
           										
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
