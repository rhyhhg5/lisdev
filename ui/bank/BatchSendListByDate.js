var showInfo;
var turnPage = new turnPageClass();

function downloadList(succFlag)
{
    if(!verifyInput()){
    	return false;
    }
    if(fm.Flag.value!= "S" && fm.Flag.value!= "F"&& fm.Flag.value!= "R"){
    	alert("收付费标记填写有误，请重新填写！");
    	return false;
    }
    if(fm.BankCode.value!= "7703" && fm.BankCode.value!= "7705" && fm.BankCode.value!= "7706" && fm.BankCode.value!= "7707" && fm.BankCode.value!= "7709"){
    	alert("银行代码填写有误，请重新填写！");
    	return false;
    }
    fm.SuccFlag.value= succFlag;
    fm.target="PRINT";
    fm.submit();
}

function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}


