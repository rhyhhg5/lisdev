<html>
  <%@page contentType="text/html;charset=GBK"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="BatchSendListByDate.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BatchSendListByDateInit.jsp"%>
</head>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<SCRIPT>
  var managecom = <%=tGI.ManageCom%>;
</SCRIPT> 

<body onload="initForm();initElementtype();">
  <form action="./BatchSendListByDateSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/InputButton.jsp"%>
    <table class=common>
      <tr class=titleImg>
		 <td class=titleImg>总公司集中代收代付清单</td>
	  </tr>
	</table>
	<table class=common>
	  <tr class=common>
		 <td class=title>开始日期</td>
		 <td class=input><input class="coolDatePicker" verify="开始日期|NOTNULL" dateFormat="short" name="StartDate" elementtype="nacessary"></td>
		 <td class=title>结束日期</td>
		 <td class=input><input class="coolDatePicker" verify="结束|NOTNULL" dateFormat="short" name="EndDate" elementtype="nacessary"></td>
	  </tr>
	  <tr class=common>
		 <td class=title>收付费标记</td>
 		 <td class=input><Input class=codeno name=Flag verify="收付费标志|NOTNULL" CodeData="0|^S|代收^F|代付^R|实时代收" ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);" onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);"><input class=codename name=SXFlagName readonly=true elementtype="nacessary"></td>
		 <td class=title>银行代码</td>
		 <td class=input><Input name=BankCode class=codeNo MAXLENGTH=10 verify="银行代码|NOTNULL" CodeData="0|^7705|银联^7706|通联^7703|邮储联^7707|金联^7709|银联高费率渠道" ondblclick="return showCodeListEx('BankCode',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKeyEx('BankCode',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank4ytj"><input class=codename name=BankCodeName readonly=true elementtype="nacessary"></td>
      </tr>
      <tr class=common>
		 <td class=title>管理机构</td>
		 <td class=input colspan='3'><Input class=codeNo name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true elementtype="nacessary"></td>
	  </tr>
   </table>
   <br>
	<table>
      <tr class=common>
           <td><input class=cssButton type=button value="下载成功清单"onclick="downloadList('S');">&nbsp;&nbsp;</td>
           <td><input class=cssButton type=button value="下载失败清单"onclick="downloadList('F');"></td>
      </tr>
	</table>
	<input type="hidden" name=SuccFlag>
 </form>
 <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
