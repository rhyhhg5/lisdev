<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：PayReturnFromBankInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="PayReturnFromBankInput.js"></SCRIPT>
  <%@include file="PayReturnFromBankInit.jsp"%>
  
  <title>银行代付 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  String com = tGlobalInput.ComCode;
  if(com.length()==4) {
      com=com+"0000";
  } else if (com.length()==2) {
      com=com+"000000";
  }
%>

<script>
  comCode = "<%=tGlobalInput.ComCode%>";
  
  function zdpp(autobankaccno){
  if(autobankaccno.checked){
  document.getElementById('bankcode').style.display="none";
  document.getElementById('accno').style.display="none";
   fm.getbankcode.verify="银行代码";
   fm.getbankaccno.verify="银行账号";
  fm.autobankaccno.value="00";
  fm.autobankaccno2.checked=""
   }else{
   document.getElementById('bankcode').style.display="";
   document.getElementById('accno').style.display="";
   fm.getbankcode.verify="银行代码|notnull";
   fm.getbankaccno.verify="银行账号|notnull";
   }
  }
  
  function zdpp2(autobankaccno2){
  if(autobankaccno2.checked){
  document.getElementById('bankcode').style.display="";
  document.getElementById('accno').style.display="";
  fm.getbankcode.verify="银行代码|notnull";
  fm.getbankaccno.verify="银行账号|notnull";
  fm.autobankaccno2.value="00";
  fm.autobankaccno.checked=""
   }
  }
</script>

<body  onload="initForm();" >
  <form action="./PayReturnFromBankSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 银行代付 fraSubmit-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请选择批次号：</td>
  		</tr>
  	</table>   
        
    <!-- 批次号信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 批次号信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div>

    
    <br>
    
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        请输入银行返回的总金额：
      </TD>
      <TD  class= input>
        <Input class= common name=TotalMoney verify="总金额|notnull&num" > 
      </TD>
      <TD  class= title colspan=2>
      <INPUT TYPE="checkBox" NAME="autobankaccno" id="autobankaccno" value="01" onclick="zdpp(autobankaccno)">是否自动匹配本方银行&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
     <div style= "display: 'none'"> <INPUT TYPE="checkBox" NAME="autobankaccno2" id="autobankaccno2" value="01" onclick="zdpp2(autobankaccno2)">使用银行帐号匹配到各机构下</TD></div>
    </TR>
	<TR class= common>
      <TD  class= title>
        银行代码
      </TD>
      <TD  class= input>
      <div id=bankcode>
        <Input NAME=BankCodeName CodeData="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('getbankcode2',[this,getbankcode],[0,1],null,null,null,null,'50%');" onkeyup="return showCodeListKey('getbankcode2',[this,getbankcode],[0,1]);"  ><input class=codename name=getbankcode readonly=true TYPE=hidden verify="银行账号|notnull">
     </div>
      </TD>
       <TD  class= title>
        银行账号
      </TD>
      <TD  class= input>
      <div id=accno>
        <Input NAME="getbankaccno"  CodeData="" MAXLENGTH=20 CLASS=code ondblclick="return showCodeList('paybankaccno',[this],[0],null,'#'+fm.getbankcode.value+'# and managecom=#' + <%=com %> + '#','BankCode');" onkeyup="return showCodeListKey('paybankaccno',[this],[0],null,'#'+fm.getbankcode.value+'# and managecom=#' + <%=com %> + '#','BankCode');" verify="银行账号|notnull" >
     </div>
      </TD>
     </TR>
    </table>
    <table width="95%">
    	<tr>
    		<td><INPUT VALUE="代付返回处理" class= cssbutton TYPE=button onclick="submitForm()"></td>
    		<td align=right><INPUT VALUE="文件返回回退" class= cssbutton TYPE=button onclick="backForm()"></td>
    	</tr>
    </table>
    <INPUT VALUE="" TYPE=hidden name=serialNo>
           										
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
 
</html>
