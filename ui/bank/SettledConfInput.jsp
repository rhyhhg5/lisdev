<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html> 
<%
//程序名称：WriteToFileInput.jsp
//程序功能：
//创建日期：2013-8-3 11:10:36
//创建人  ：王 硕
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head > 
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="SettledConfInput.js"></SCRIPT> 
  <%@include file="SettleConfInit.jsp"%>
  
  <title>生成发送医保文件 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  comCode = "<%=tGlobalInput.ComCode%>";
  dealType = "<%=request.getParameter("DealType")%>";
</script>

<body  onload="initForm();" >
  <form name=fm action = './SettledConfSave.jsp' target="fraTitle" method = "post">
    <%@include file="../common/jsp/InputButton.jsp"%>
    
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
	
    <table  class= common align=center>
      	<TR  class= common>
 <TD  class= title>
        医保机构代码
      </TD>
      <TD  class= input>
        <Input NAME=MedicalCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('MedicalCode',[this,MedicalCodeName],[0,1]);" onkeyup="return showCodeListKey('MedicalCode',[this,MedicalCodeName],[0,1]);" verify="医保机构代码|notnull&code:MedicalCode" ><input class=codename name=MedicalCodeName readonly=true >
      </TD>
          <TD  class= title>
           结算日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short" name=dealdate >
          </TD>
	          <TD  class= input>
	            <INPUT VALUE="查  询" TYPE=button class=CssButton onclick="easyQueryClick();">
	          </TD>
        </TR>
    </table>
    
    <hr>  
           
    <!-- 批次号信息（列表） -->
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 请选择批次号：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>
  	
    <Div id= "divPage" align=center style= "display: 'none' ">
    <INPUT CLASS=CssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
    <INPUT CLASS=CssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
    <INPUT CLASS=CssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
    <INPUT CLASS=CssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
    </Div>

    
      <br>
    
    
    <INPUT VALUE="" TYPE=hidden name=bankCode> 
    <INPUT VALUE="" TYPE=hidden name=serialNo>
    <INPUT VALUE="" TYPE=hidden name=Url>
    <INPUT VALUE="" TYPE=hidden name=downfilesum>
    <INPUT VALUE="" TYPE=hidden name=downflag>
    <INPUT VALUE="" TYPE=hidden  name=DealType>
    <INPUT VALUE="" TYPE=hidden  name=querySql>
    <INPUT VALUE="" TYPE=hidden  name=filename>
    <input type=hidden id="fmtransact" name="fmtransact">
    <hr>
    <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>批次数据统计：</td>
		</tr>
	  </table> 
	     		  								
    <table  class= common>
    <TR  class= common>
      <TD  class= title>
        请输入银行返回的总金额：
      </TD>
      <TD  class= input>
        <Input class= common name=TotalMoney verify="总金额|notnull&num" > 
      </TD> 
	  <TD  class= title>
      </TD>
      <TD  class= input>
       </TD>
    </TR>
	<TR class= common>
      <TD  class= title>
        银行代码
      </TD>
      <TD  class= input>
        <Input NAME=BankCodeName CodeData="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('getbankcode1',[this,getbankcode],[0,1],null,null,null,null,'50%');" onkeyup="return showCodeListKey('getbankcode1',[this,getbankcode],[0,1]);" verify="银行代码|notnull" ><input class=codename name=getbankcode readonly=true TYPE=hidden>
      </TD>
       <TD  class= title>
        银行账号
      </TD>
	  <td CLASS="input" COLSPAN="1">
			<input NAME="getbankaccno" VALUE MAXLENGTH="20" CLASS="code" elementtype=nacessary ondblclick="return showCodeList('getbankaccno',[this],[0],null,fm.getbankcode.value,'BankCode');" onkeyup="return showCodeListKey('getbankaccno',[this],[0],null,fm.getbankcode.value,'BankCode');" verify="银行账号|notnull" >
	  </td>
    </TR>
    </table>
    <br><br>
   <INPUT VALUE="结算确认" class= CssButton TYPE=button onclick="submitForm()">
  <!--  <INPUT VALUE="汇总清单下载" class= CssButton TYPE=button onclick="HZButton()">
   <INPUT VALUE="明细清单下载" class= CssButton TYPE=button onclick="MXButton()"> -->
  
  
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>

