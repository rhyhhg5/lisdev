<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GetSendToMedicalComSave.jsp
//程序功能：
//创建日期：2013-6-6 11:02:02
//创建人  ：王硕
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="java.text.*"%>
  <%@page import="java.util.*"%>
  
<% 
  String Content = "";
  String FlagStr = "";
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");

  
  //生成银行数据
  System.out.println("\n\n---GetSendToBankSave Start---");
  GetSendToMedcyUI tGetSendToMedcyUI = new GetSendToMedcyUI();

  TransferData transferData1 = new TransferData();
  transferData1.setNameAndValue("MedicalCode", request.getParameter("MedicalCode"));
  transferData1.setNameAndValue("typeFlag", request.getParameter("typeFlag"));

  VData tVData = new VData();
  tVData.add(transferData1);
  tVData.add(tGlobalInput);
  String Manage = "";
  String SerialNo = "";
//更新LJSPay向LYSendToBank和LYBankLog插入数据
  if (!tGetSendToMedcyUI.submitData(tVData, "GETMONEY")) {
    VData rVData = tGetSendToMedcyUI.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
  	FlagStr = "Fail";
  }
  else {
  	VData rVData = tGetSendToMedcyUI.getResult();
    Content = "送保结算处理成功! ";
  	FlagStr = "Succ";
  	Manage = tGetSendToMedcyUI.getManage();
  	SerialNo = tGetSendToMedcyUI.getSerialNo();
  }  

	System.out.println(Content + "\n" + FlagStr + "\n---GetSendToBankSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>','<%=Manage%>','<%=SerialNo%>');
	parent.fraInterface.fm.all("sub").disabled=false;
</script>
</html>
