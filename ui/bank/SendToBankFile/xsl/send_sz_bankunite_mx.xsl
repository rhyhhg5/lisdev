<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<!-- 设置第一行开始 -->
<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>

<xsl:for-each select="BANKDATA/ROW">

	<!-- 回车换行，第一行不回车 -->
  <xsl:choose>
    <xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
      <xsl:text>&#13;&#10;</xsl:text>
    </xsl:when>
  </xsl:choose>

  <!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 明细序号 -->
  <xsl:value-of select="substring(XuLie, 1, 8)"/>
  <xsl:call-template name="supplement">
  <xsl:with-param name="valueLen" select="string-length(XuLie)"/>
  <xsl:with-param name="length" select="8"/>
  <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 协议编码 -->
  <xsl:value-of select="'                                '"/>
  
   <!-- 收款金额，16位，9999999999999.99 -->
  <xsl:value-of select="format-number(PayMoney, '0000000000000.00')"/> 
  
  <!-- 明细本地行别，分行代码 -->
  <xsl:value-of select="UniteBankCode"/>
  
  <!-- 明细行号， -->
  <xsl:value-of select="'              '"/>
  
  <!-- 账号，不够32位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1, 32)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="32"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  
  <!-- 账户名称，不够120位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="5 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 120-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 5)"/>
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="120"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 付款附言 -->
  <xsl:value-of select="'                                                                                                                        '"/>
  
  <!-- 银行返回码 -->
  <xsl:value-of select="'--------'"/>
  
  <!-- 附件信息 不够100位右补空格-->
  <xsl:value-of select="substring(PayCode, 1, 100)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="100"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 银行返回附言 -->
  <xsl:value-of select="'                                                                                                                        '"/>

  <!-- 以上的代码块根据不同的银行而不同 -->

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>