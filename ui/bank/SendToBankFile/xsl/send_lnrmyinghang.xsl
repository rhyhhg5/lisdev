<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
					<!-- 明细序号，整个批次内唯一：批次号+明细序号，8n长度不足左补0-->
          <xsl:variable name="date"  select ="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2(),3,6)"/> 
            <xsl:variable name="time"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentTime())"/>
            <xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,$time),6))"/>
         <xsl:value-of select="concat('00',$no)"/>
           
					<xsl:value-of select="','"/>
					
					<!-- 付款人开户行号，12n，长度不足左补0 -->
					
        	<xsl:call-template name="supplement">
            <xsl:with-param name="key" select="'0'"/>
            <xsl:with-param name="valueLen" select="string-length(BankCode)"/>
            <xsl:with-param name="length" select="12"/>
           </xsl:call-template>
           <xsl:value-of select="substring(BankCode, 1, 12)"/>
           <xsl:value-of select="','"/>
           
					
					<!-- 付款人账号，32x，长度不足右补空格-->
					<xsl:value-of select="substring(AccNo, 1, 32)"/>
        	<xsl:call-template name="supplement">
            <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
            <xsl:with-param name="length" select="32"/>
            <xsl:with-param name="key" select="' '"/>
           </xsl:call-template>
           <xsl:value-of select="','"/>
           
					<!-- 付款人名称，60g，长度不足右补空格 ,汉字是否应该使用下面这种格式-->
		  		<xsl:variable name="accNameValue" select="string(AccName)"/>
		  		<xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
		  		
		  		<xsl:value-of select="substring(AccName, 1, 60-$accNameLen)"/>
		      <xsl:call-template name="supplement">
		        <xsl:with-param name="valueLen" select="string-length(AccName)+$accNameLen"/>
		        <xsl:with-param name="length" select="60"/>
		        <xsl:with-param name="key" select="' '"/>
		      </xsl:call-template>
		      
		      <xsl:value-of select="','"/>
		      
			
					<!-- 缴费方式 ，60g，长度不足右补空格 -->
					<xsl:variable name = "payTypeValue" select = "string(PayTyep)" />
					<xsl:variable name = "payTypeLen" select = "java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($payTypeValue)"/>
					
					<xsl:value-of select="substring(PayType, 1, 60-$payTypeLen)"/>
		      <xsl:call-template name="supplement">
		        <xsl:with-param name="valueLen" select="string-length(PayType)+$payTypeLen"/>
		        <xsl:with-param name="length" select="60"/>
		        <xsl:with-param name="key" select="' '"/>
		      </xsl:call-template>
					<xsl:value-of select="','"/>
					
					<!-- 交易金额，15n,以分为单位，长度不足左补空格-->
					<xsl:variable name="payMoneyValue" select="format-number(PayMoney, '0.00')"/>
					<xsl:call-template name="supplement">
			        <xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
			        <xsl:with-param name="length" select="15"/>
			        <xsl:with-param name="key" select="' '"/>
      		</xsl:call-template>
      		<xsl:value-of select = "string($payMoneyValue)"/>
				  <xsl:value-of select="','"/> 
				  
				  <!-- 合同协议号,60x,长度不足右补空格-->
				  <xsl:value-of select="'NULL'"/>
					<xsl:value-of select="','"/>
					
				  <!-- 缴费通知书号,60g,长度不足右补空格 -->	

          <xsl:variable name = "payCodeValue" select = "string(PayCode)" />
					<xsl:variable name = "payCodeLen" select = "java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($payCodeValue)"/>
					
					<xsl:value-of select="substring(PayCode, 1, 60-$payCodeLen)"/>
		      <xsl:call-template name="supplement">
		        <xsl:with-param name="valueLen" select="string-length(PayCode)+$payCodeLen"/>
		        <xsl:with-param name="length" select="60"/>
		        <xsl:with-param name="key" select="' '"/>
		      </xsl:call-template>
					<xsl:value-of select="','"/>
					
				  <!-- 各区分号 -->
				  <xsl:value-of select="string(AgentCode)"/>
				  <xsl:value-of select="' '"/>

					<!-- 回车换行 -->
					<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>
