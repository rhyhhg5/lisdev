<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
              <!--汇总记录标志-->
              <xsl:value-of select="'F'"/>
              <!--公司代码-->
              <xsl:value-of select="'00000'"/>
              <!--批号-->
              <xsl:value-of select="'00000'"/>
						  <!--入帐日期，转换成yyyymmdd -->
						  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>                
              <xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
  						<!-- 收付标志 -->
  						<xsl:choose>
    						<xsl:when test="DealType='S'">
              <!--代发笔数 7位-->
              <xsl:value-of select="'0000000'"/>
              <!--代发金额 12位-->
              <xsl:value-of select="'            '"/>
				      <!-- 代扣笔数 7位-->
						  <xsl:value-of select="format-number($totalpic, '0000000')"/>
						  <!-- 代扣金额 12位 -->
						  <xsl:value-of select="number($totalpay*100)"/>  							
  							<xsl:call-template name="supplement">
    								<xsl:with-param name="valueLen" select="string-length(number($totalpay*100))"/>
    								<xsl:with-param name="length" select="12"/>
    								<xsl:with-param name="key" select="' '"/>
  						</xsl:call-template> 
    					</xsl:when>
    					<xsl:otherwise>
				      <!-- 代发笔数 7位-->
						  <xsl:value-of select="format-number($totalpic, '0000000')"/>
						  <!-- 代发金额 12位 -->
						  <xsl:value-of select="number($totalpay*100)"/> 							
  							<xsl:call-template name="supplement">
    								<xsl:with-param name="valueLen" select="string-length(number($totalpay*100))"/>
    								<xsl:with-param name="length" select="12"/>
    								<xsl:with-param name="key" select="' '"/>
  						</xsl:call-template> 
              <!--代扣笔数 7位-->
              <xsl:value-of select="'0000000'"/>
              <!--代扣金额 12位-->
              <xsl:value-of select="'            '"/>
    						</xsl:otherwise>
  						</xsl:choose>
              <!--备用-->
              <xsl:value-of select="'         '"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>

					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
	<!-- 以下的代码块根据不同的银行而不同 -->
  <!--汇总记录标志-->
  <xsl:value-of select="'1'"/>
 	<!--公司代码-->
  <xsl:value-of select="'00000'"/>  
  <!-- 明细序号 5位-->
	<xsl:variable name="detailnumber" select="position()"/>
	<xsl:value-of select="format-number($detailnumber, '00000')"/>	
  <!-- 账号，不够22位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1, 22)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="22"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template> 		
  <!-- 交易金额，12位 -->
  <xsl:value-of select="format-number(PayMoney*100, '000000000000')"/>
	<!--入帐日期，转换成yyyymmdd -->
	<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>                
  <!-- 收付标志 -->
  <xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>1</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>0</xsl:text>
    </xsl:otherwise>
  </xsl:choose>			
  <!-- 帐号编号，不够17位后面补空格 -->
  <xsl:value-of select="PayCode"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="17"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	
  <!--出错代码 3位-->
  <xsl:value-of select="'   '"/>	
	
  <!--备用-->
  <xsl:value-of select="'             '"/>			
	
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
	
</xsl:template>

</xsl:stylesheet>