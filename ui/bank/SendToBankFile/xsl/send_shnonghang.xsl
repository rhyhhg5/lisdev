<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							<!-- 省市代号，3位--> 
							<xsl:value-of select="'009'"/>  
							<!-- 行部号，4位--> 
							<xsl:value-of select="'3202'"/>	
  						<!-- 代理种类,3位 -->
  				<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>070</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>069</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose>
							<!-- 代理序号，4位--> 
							<xsl:value-of select="'0005'"/>								
							<!-- 明细序号，6位--> 
							<xsl:value-of select="'000000'"/>									
							<!-- 记帐状态，1位--> 
							<xsl:value-of select="'0'"/>								
							<!-- 检查状态，1位--> 
							<xsl:value-of select="'0'"/>	
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				         	<!-- 总金额 13位 -->
						      <xsl:value-of select="format-number($totalpay*100, '0000000000000')"/> 
				          <!-- 总笔数 6位-->
						      <xsl:value-of select="format-number($totalpic, '000000')"/>
				
							<!-- 实际入帐金额，13位--> 
							<xsl:value-of select="'0000000000000'"/>								
							<!-- 实际入帐笔数，6位--> 
							<xsl:value-of select="'000000'"/>									
							<!-- 公司全称，40位--> 
							<xsl:value-of select="'人保健康                                '"/>
						<!-- 公司帐号，20位-->			
  					<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>00003332200040030337</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>00003332200040030345</xsl:text>
   					 </xsl:otherwise>
  					</xsl:choose>												
						  <!-- 入帐日期，转换成yyyymmdd -->
						  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>  
						  <!-- 上传日期，转换成yyyymmdd -->						  
						  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/> 
							<!-- 扣帐剩余天数，3位--> 
							<xsl:value-of select="'000'"/>							  
							<!-- 公司简称，10位--> 
							<xsl:value-of select="'人保健康  '"/>
							<!-- 公司编号，5位--> 
  					<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>20061</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>20060</xsl:text>
   					 </xsl:otherwise>
  					</xsl:choose>
							<!-- filler,46位--> 
							<xsl:value-of select="'                                              '"/>  					  																		        						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
							<!-- 省市代号，3位--> 
							<xsl:value-of select="'009'"/>  
							<!-- 行部号，4位--> 
							<xsl:value-of select="'3202'"/>	
  						<!-- 代理种类,3位 -->
  				<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>070</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>069</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose>
							<!-- 代理序号，4位--> 
							<xsl:value-of select="'0005'"/>			
				      <!-- 明细序号 6位-->
				     <xsl:variable name="detailnumber" select="position()"/>
						 <xsl:value-of select="format-number($detailnumber, '000000')"/>
							<!-- 记帐状态，1位--> 
							<xsl:value-of select="'0'"/>	
							<!-- 客户姓名，40位--> 
							<xsl:value-of select="'                                        '"/>	
							<!-- 以下的代码块根据不同的银行而不同 -->
  						<!-- 账号，不超过19位-->
  						<!-- 账号，不够19位右补空格 -->
  					<xsl:value-of select="substring(AccNo, 1, 19)"/>
  					<xsl:call-template name="supplement">
    						<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    						<xsl:with-param name="length" select="19"/>
    						<xsl:with-param name="key" select="' '"/>
  					</xsl:call-template>
							<!-- 借贷标志，1位--> 
							<xsl:value-of select="'0'"/>	
  						<!-- 入帐金额，13位 -->
  						<xsl:value-of select="format-number(PayMoney*100, '0000000000000')"/>
							<!-- 币种，3位--> 
							<xsl:value-of select="'000'"/>	
							<!-- 传票号，8位--> 
							<xsl:value-of select="'00000000'"/>
							<!-- 入账返回码，4位--> 
							<xsl:value-of select="'    '"/>								
							<!-- 入帐日期，8位--> 
							<xsl:value-of select="'00000000'"/>	
							<!-- 主机流水号，9位--> 
							<xsl:value-of select="'000000000'"/>
							<!-- 开户行号，7位--> 
							<xsl:value-of select="'0000000'"/>								
							<!-- 公司编号，5位--> 
  					<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>20061</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>20060</xsl:text>
   					 </xsl:otherwise>
  					</xsl:choose>
							  <!-- 证件号，18位  -->
  						<xsl:value-of select="substring(IDNo, 1, 18)"/>
  					<xsl:call-template name="supplement">
    						<xsl:with-param name="valueLen" select="string-length(IDNo)"/>
    						<xsl:with-param name="length" select="18"/>
    						<xsl:with-param name="key" select="' '"/>
  					</xsl:call-template>														
							<!-- filler(收据号),46位--> 
  				<xsl:value-of select="PayCode"/>
 					 <xsl:call-template name="supplement">
   				 <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    			 <xsl:with-param name="length" select="44"/>
    			<xsl:with-param name="key" select="' '"/>
  			</xsl:call-template>	

	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>