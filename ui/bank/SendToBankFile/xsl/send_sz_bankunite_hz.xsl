<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW[position()=1]">

  <!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 业务类型 代收304代付303 -->
  <xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>304</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>303</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  
  <!-- 企业代码 -->
  <xsl:value-of select="'C00000000C18  '"/>
  
  <!-- 代办银行-->
  <xsl:value-of select="'308584001016  '"/>
  
  <!-- 费项代码 -->
  <xsl:value-of select="'90620'"/>
  
  <!-- 交易文件名 -->
  <!-- 交易文件名待做！！！！！！！！！！！！！！！-->
  <xsl:value-of select="concat('C1800', format-number(number(SeqNo), '000'))"/>
  
  <!-- 币种 -->
  <xsl:value-of select="'CNY'"/>
  
  <!-- 总笔数 6位-->
  <xsl:variable name="serialno"  select ="SerialNo"/> 
	<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
  <xsl:value-of select="format-number($totalpic, '00000000')"/>
  
  <!-- 总金额 12位 -->
  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
  <xsl:value-of select="format-number($totalpay, '0000000000000.00')"/>
  
  <!-- 成功笔数 -->
  <xsl:value-of select="'        '"/>
  
  <!-- 成功金额 -->
  <xsl:value-of select="'                '"/>
  
  <!-- 业务编码 代收30401代付30301 -->
  <xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>30401</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>30301</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  
  <!-- 当前委托日期，转换成yyyymmdd -->
  <xsl:variable name="curDate" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
  <xsl:value-of select="concat(substring($curDate,1,4), substring($curDate,6,2), substring($curDate,9,2))"/>
  
  <!-- 应划款日期，转换成yyyymmdd -->
  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
  
  <!-- 回执日期 -->
  <xsl:value-of select="'        '"/>
  
  <!-- 代办账号 -->
  <xsl:value-of select="'                                '"/>
  
  <!-- 代办账户名 -->
  <xsl:value-of select="'                                                                                                                        '"/>
  <!-- 以上的代码块根据不同的银行而不同 -->
  
  <!-- 回车换行 
  <xsl:text>&#13;&#10;</xsl:text>-->
  
</xsl:for-each>

</xsl:template>

</xsl:stylesheet>