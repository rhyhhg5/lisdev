﻿<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">			
			<xsl:value-of select="'2'"/>
			<xsl:value-of select="'220311001'"/>
			<xsl:value-of select="'439'"/>
			<xsl:value-of select="substring(AccNo, 1,19)"/>
  		<xsl:call-template name="supplement">
				 <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
				 <xsl:with-param name="length" select="19"/>
				 <xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="'3'"/>
			<xsl:value-of select="format-number(position(), '000000')"/>	
			<xsl:value-of select="format-number(PayMoney*100, '00000000000000000')"/>	
			<xsl:value-of select="'00000000000000001'"/>
			<xsl:if test="DealType='S'"> 		
          <xsl:value-of select="'1'"/>
      </xsl:if> 
      <xsl:if test="DealType='F'"> 		
          <xsl:value-of select="'2'"/>   
      </xsl:if> 				
  		<xsl:value-of select="substring(PayCode, 1,30)"/>
	  		<xsl:call-template name="supplement">
			    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
			    <xsl:with-param name="length" select="70"/>
			    <xsl:with-param name="key" select="'0'"/>
			  </xsl:call-template>
			<xsl:value-of select="'000000000000'"/> 

			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
 <xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>

	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
		<xsl:otherwise>
			<xsl:variable name="serialno"  select ="SerialNo"/> 
      <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
			<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>         
      <xsl:value-of select="'1'"/>
			<xsl:value-of select="'220311001'"/>
			<xsl:value-of select="'439'"/>
			<xsl:value-of select="substring(AccNo, 1,19)"/>
  		<xsl:call-template name="supplement">
				 <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
				 <xsl:with-param name="length" select="19"/>
				 <xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="'3'"/>
			<xsl:value-of select="format-number(position(), '000000')"/>	
			<xsl:value-of select="format-number($totalpay*100, '00000000000000000')"/>	
			<xsl:value-of select="format-number($totalpic, '00000000000000000')"/>
			<xsl:if test="DealType='S'"> 		
          <xsl:value-of select="'1'"/>
      </xsl:if> 
      <xsl:if test="DealType='F'"> 		
          <xsl:value-of select="'2'"/>   
      </xsl:if> 				
  		<xsl:value-of select="'0000000000000000000000000000000000000000000000000000000000000000000000'"/>
			  <xsl:value-of select="'000000000000'"/> 
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
</xsl:template>

</xsl:stylesheet>