<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">
 <xsl:variable name="tposition" select="position()"/>
 <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length($tposition)"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="'0'"/>
  </xsl:call-template>
  <xsl:value-of select="$tposition"/>
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  <xsl:choose>
    <xsl:when test="5 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 10-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 5)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>


  <xsl:value-of select="substring(AccNo, 1,19)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="19"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
 
	
	<!-- 交易金额，18位 -->
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(string(PayMoney))"/>
    <xsl:with-param name="length" select="13"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
  <xsl:value-of select="PayMoney"/>

  
  <!-- 以上的代码块根据不同的银行而不同 -->
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>