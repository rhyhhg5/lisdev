<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
    <xsl:for-each select="BANKDATA/ROW">
            
            <xsl:value-of select="substring(PolNo,1,10)"/>
            <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(substring(PolNo,1,10))"/>
        <xsl:with-param name="length" select="10"/>
        <xsl:with-param name="key" select="' '"/>
        </xsl:call-template>
        <!-- ���д��� -->
        <xsl:value-of select="UniteBankCode"/>
                    
            <xsl:value-of select="AccNo"/>
            <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
        <xsl:with-param name="length" select="21"/>
        <xsl:with-param name="key" select="' '"/>
        </xsl:call-template>
        
        <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
        <xsl:with-param name="length" select="12"/>
        <xsl:with-param name="key" select="' '"/>
            </xsl:call-template>
            <xsl:value-of select="PayMoney"/>
            
        <xsl:value-of select="'X'"/>    
            <xsl:text>&#13;&#10;</xsl:text>
    </xsl:for-each>

</xsl:template>

</xsl:stylesheet>