<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	
	
	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
	
	<!-- 序号，6位--> 
	<xsl:value-of select="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2),'ZJ'),6))"/>
  <!-- 公司编号，20位-->
  <xsl:value-of select="substring(PayCode, 1, 20)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 姓名，10位-->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>  

  <xsl:choose>
    <xsl:when test="5> $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 10-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 5)"/>
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>

	
  <!-- 身份证号，18位-->
  <xsl:value-of select="substring(Idno, 1, 18)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(Idno)"/>
    <xsl:with-param name="length" select="18"/>
    <xsl:with-param name="key" select="'0'"/>
  </xsl:call-template>
  
  <!-- 账号，不够19位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1, 19)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="19"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <!-- 空格，6位-->
  <xsl:value-of select="'      '"/>

	<!-- 交易金额，10位， -->
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
		<xsl:value-of select="substring(PayMoney, 1, 10)"/> 
	<!-- 摘要，4位-->
  <xsl:value-of select="'人保'"/>	
  <!-- 日期，6位-->
	<xsl:value-of select="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2(),3,6)"/>
  
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>