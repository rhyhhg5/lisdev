<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							<!-- 币种--> 
							<xsl:value-of select="'RMB|'"/>  
						  <!-- 日期，转换成yyyymmdd -->
						  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>  
              <xsl:value-of select="'|'"/>
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getPercentPay($totalpay))"/> 
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 

              <!--总计标记-->
              <xsl:value-of select="'1|'"/>
						  <!-- 总金额 18位 -->
						  <xsl:value-of select="$totalpay"/> 
						  <xsl:value-of select="'|'"/>
				      <!-- 总笔数 5位-->
						  <xsl:value-of select="number($totalpic)"/>
						  <xsl:value-of select="'|'"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
	<!--币种-->		
	<xsl:value-of select="'RMB|'"/>  
  <!-- 日期，转换成yyyymmdd -->
  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>  
  <xsl:value-of select="'|'"/>
  <!--明细标记-->
  <xsl:value-of select="'2|'"/>
  <!--顺序号，5位-->
  <xsl:value-of select="position()"/>
  <xsl:value-of select="'|'"/>
  <!--付方开户行名，60位-->
  <xsl:value-of select="''"/>
  <xsl:value-of select="'|'"/>
  <!-- 账号，不超过30位-->
  <xsl:value-of select="substring(AccNo, 1, 30)"/>
  <xsl:value-of select="'|'"/>
  <!--缴费编号，不超过15位-->
  <xsl:value-of select="substring(PolNo, 1, 15)"/>
  <xsl:value-of select="'|'"/>  
  <!--付方户名，60位-->
  <xsl:value-of select="substring(AccName, 1, 60)"/> 
  <xsl:value-of select="'|'"/>
  <!--收款帐号开户行名称，不超过60位-->
  <xsl:value-of select="'工行园通支行'"/>  
  <xsl:value-of select="'|'"/>
  <!--收款帐号省份名，不超过20位-->
  <xsl:value-of select="'云南'"/>
  <xsl:value-of select="'|'"/>  
  <!--收款帐号地市名，不超过20位-->
  <xsl:value-of select="'昆明'"/>
  <xsl:value-of select="'|'"/>
  <!--收款帐号地区码，不超过5位-->
  <xsl:value-of select="'2502'"/>
  <xsl:value-of select="'|'"/>
  <!--收款人帐号，不超过30位-->
  <xsl:value-of select="'2502010829023102449'"/>
  <xsl:value-of select="'|'"/>
  <!--协议编号-->
  <xsl:value-of select="'BDP2253'"/>
  <xsl:value-of select="'|'"/>
	<!--收款人名称，不超过60位-->
	<xsl:value-of select="'中国人民健康保险股份有限公司云南分公司'"/>  
	<xsl:value-of select="'|'"/>
	<!-- 交易金额，不超过18位， -->
		<xsl:value-of select="number(PayMoney*100)"/> 
		<xsl:value-of select="'|'"/>
	<!--汇款用途，不超过50位-->
	<xsl:value-of select="'交保费'"/>  
	<xsl:value-of select="'|'"/>			
	<!--备注,不超过70位-->	
	<!--xsl:value-of select="PolNo"/-->
  <!--xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PolNo)"/>
    <xsl:with-param name="length" select="70"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template-->
  <xsl:value-of select="'|'"/>
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
  <!--结束标记-->
  <xsl:value-of select="'*'"/>  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>
</xsl:template>

</xsl:stylesheet>