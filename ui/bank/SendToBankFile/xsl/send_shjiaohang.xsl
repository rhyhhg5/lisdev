﻿<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>

	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
				      <xsl:value-of select="'3100000812'"/> 
							<xsl:value-of select="'|'"/>  
						   <xsl:variable name="serialno"  select ="SerialNo"/> 
						  <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
					
						  <xsl:value-of select="number($totalpic)"/>
						  <xsl:value-of select="'|'"/>
						 
              <xsl:value-of select="format-number($totalpay, '0.00')"/> 
              <xsl:value-of select="'|'"/>
						
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
	 <xsl:value-of select="'3100000812'"/>
  <xsl:value-of select="'|'"/> 
    <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/> 
    <xsl:value-of select="string(PayCode)"/>
  <xsl:value-of select="'|'"/>
    <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>
	 <xsl:value-of select="'15'"/>
  <xsl:value-of select="'|'"/> 
    <xsl:value-of select="string(IDNo)"/>
  <xsl:value-of select="'|'"/>
	
  <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/>
   <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>
   <xsl:value-of select=" string(PayMoney)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(PayCode)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>

		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>