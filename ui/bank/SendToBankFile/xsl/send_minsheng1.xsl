<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
				      <xsl:value-of select="'商户号'"/> 
							<xsl:value-of select="'|'"/>  
							<xsl:value-of select="'01'"/> 
							<xsl:value-of select="'|'"/>  
							<xsl:choose>
    						<xsl:when test="DealType='S'">
      							<xsl:text>0</xsl:text>
    						</xsl:when>
    						<xsl:otherwise>
      						<xsl:text>1</xsl:text>
    						</xsl:otherwise>
  						</xsl:choose>
  						<xsl:value-of select="'|'"/> 
  						<xsl:value-of select="'商户处理卡号'"/>
  						<xsl:value-of select="'|'"/>
  						<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
  						<xsl:value-of select="'|'"/> 
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
						  <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						  <!-- 总笔数 5位-->
						  <xsl:value-of select="number($totalpic)"/>
						  <xsl:value-of select="'|'"/>
						  <!-- 总金额 18位 -->
              <xsl:value-of select="format-number($totalpay, '0.00')"/> 
              <xsl:value-of select="'|'"/>
              <xsl:value-of select="'预留头字段1'"/> 
              <xsl:value-of select="'|'"/>
              <xsl:value-of select="'预留头字段2'"/> 
              <xsl:value-of select="'|'"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
	<xsl:value-of select="'明细序号'"/>
	<xsl:value-of select="format-number(position(),'00000000')"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'156'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select=" string(PayMoney)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'客户开户行号'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'客户开户行名称'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>		
  <xsl:value-of select="'备注'"/>
  <xsl:value-of select="'|'"/>
   <xsl:value-of select="'99'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(IDNo)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'客户联系手机号'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'交易手续费'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'预留备用1'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'预留备用2'"/>
  <xsl:value-of select="'|'"/>
	
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>