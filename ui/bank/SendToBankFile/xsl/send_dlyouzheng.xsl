<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">


	<!-- 费用记录号，15位--> 
	<xsl:value-of select="substring(PayCode, 1, 15)"/>
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="15"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
	<!-- 分隔符-->
	<xsl:value-of select="' '"/>
	<!-- 收付费类型，1位-->
	<xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>1</xsl:text>
    </xsl:when>
   	<xsl:otherwise>
     	<xsl:text>2</xsl:text>
   	</xsl:otherwise>
  </xsl:choose>
	<!-- 分隔符-->
	<xsl:value-of select="' '"/>
	<!-- 申请日期，转换成yyyy/mm/dd -->
  <xsl:value-of select="concat(substring(SendDate,1,4),'/', substring(SendDate,6,2),'/', substring(SendDate,9,2))"/>
  <!-- 分隔符-->
	<xsl:value-of select="' '"/>
	<!-- 银行代码-->
	<xsl:value-of select="'LNYZSY'"/>
	<!-- 分隔符-->
	<xsl:value-of select="' '"/>
	<!-- 客户身份证号码-->
  <xsl:value-of select="IDNo"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(IDNo)"/>
    <xsl:with-param name="length" select="18"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <!-- 分隔符-->
	<xsl:value-of select="' '"/>
	<!-- 账户名称，不够10位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="5 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 10-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 5)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	<!-- 分隔符-->
	<xsl:value-of select="' '"/>
	<!-- 交易金额，10位 -->
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(string(PayMoney))"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
  <xsl:value-of select="PayMoney"/>
  <!-- 分隔符-->
	<xsl:value-of select="' '"/>
	<!-- 账号，不够19位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1,19)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="19"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 以上的代码块根据不同的银行而不同 -->
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>