<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
							<xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						  <!-- 总笔数 8位-->
						  <xsl:call-template name="supplement">
						    <xsl:with-param name="valueLen" select="string-length($totalpic)"/>
						    <xsl:with-param name="length" select="8"/>
						    <xsl:with-param name="key" select="'0'"/>
						  </xsl:call-template>
						  <xsl:value-of select="substring($totalpic, 1,8)"/>
				      <!-- 总金额 18位 -->
				      <xsl:call-template name="supplement">
						    <xsl:with-param name="valueLen" select="string-length($totalpay*100)"/>
						    <xsl:with-param name="length" select="18"/>
						    <xsl:with-param name="key" select="'0'"/>
						  </xsl:call-template>
						  <xsl:value-of select="substring($totalpay*100, 1,18)"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			<!-- 明细标志--> 
			<!-- 保单号，30位 -->
  		<xsl:value-of select="substring(PayCode, 1, 15)"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(PayCode)"/>
				<xsl:with-param name="length" select="15"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			
			<xsl:value-of select="substring(AccNo, 1, 25)"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
				<xsl:with-param name="length" select="25"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>

			<xsl:variable name="accNameValue" select="string(AccName)"/>
		  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
		  
		  <xsl:choose>
		    <xsl:when test="10 > $accNameLen">
		      <xsl:value-of select="substring(AccName, 1,20-$accNameLen)"/>
		      <xsl:call-template name="supplement">
		        <xsl:with-param name="valueLen" select="string-length(AccName)+$accNameLen"/>
		        <xsl:with-param name="length" select="20"/>
		        <xsl:with-param name="key" select="' '"/>
		      </xsl:call-template>
		    </xsl:when>
		    <!-- 多于四个汉字的处理 -->
		    <xsl:otherwise>
		      <xsl:value-of select="substring(AccName, 1, 10)"/> 
		    </xsl:otherwise>
		  </xsl:choose>
			
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(PayMoney*100)"/>
				<xsl:with-param name="length" select="18"/>
				<xsl:with-param name="key" select="'0'"/>
			</xsl:call-template>
			<xsl:value-of select="substring(PayMoney*100, 1,18)"/>
			
			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>