<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java" version="1.0">

	<xsl:output method="text" encoding="gb2312" />

	<xsl:include href="send_function.xsl" />

	<xsl:template match="/">
		<xsl:variable name="MaxRow"  select ="0"/>
		<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()" />
		<!-- 汇总记录 -->
		<xsl:for-each select="BANKDATA/ROW">
			<xsl:variable name="MaxRow"  select ="position()"/>
			<xsl:choose>
				<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'保险费              '"/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
		<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
		<xsl:for-each select="BANKDATA/ROW">
			<!-- 流水号，6位 -->
			<xsl:variable name="date"  select ="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2(),3,6)"/> 
			<xsl:variable name="time"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentTime())"/>
			<xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,$time),6))"/>
			<xsl:value-of select="$no"  />
			<!-- 银行代码，8位  -->
			<xsl:value-of select="substring(BankCode,1,8)"  />
			<xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(BankCode)"/>
        <xsl:with-param name="length" select="8"/>
        <xsl:with-param name="key" select="' '"/>
      </xsl:call-template>
			<!-- 账号，不够30位右补空格 -->
      <xsl:value-of select="substring(AccNo, 1,30)"/>
      <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
        <xsl:with-param name="length" select="30"/>
        <xsl:with-param name="key" select="' '"/>
      </xsl:call-template>
      
			<!-- 帐号类型-->
	    <xsl:value-of select="'0'"/>
			
			<!-- 用户姓名10位-->
      <xsl:variable name="accNameValue" select="string(AccName)"/>
		  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
		  
		  <xsl:choose>
		    <xsl:when test="5 > $accNameLen">
		      <xsl:value-of select="substring(AccName, 1,10-$accNameLen)"/>
		      <xsl:call-template name="supplement">
		        <xsl:with-param name="valueLen" select="string-length(AccName)+$accNameLen"/>
		        <xsl:with-param name="length" select="10"/>
		        <xsl:with-param name="key" select="' '"/>
		      </xsl:call-template>
		    </xsl:when>
		    <!-- 多于四个汉字的处理 -->
		    <xsl:otherwise>
		      <xsl:value-of select="substring(AccName, 1, 5)"/> 
		    </xsl:otherwise>
		  </xsl:choose>		  
			<!--身份证号 -->
			<xsl:value-of select="substring(IDNo,1,19)" />
			<xsl:call-template name="supplement">
		        <xsl:with-param name="valueLen" select="string-length(IDNo)"/>
		        <xsl:with-param name="length" select="19"/>
		        <xsl:with-param name="key" select="' '"/>
		  </xsl:call-template>
		  <!--扣款金额-->				
      <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(string(PayMoney*100))"/>
        <xsl:with-param name="length" select="12"/>
        <xsl:with-param name="key" select="'0'"/>
      </xsl:call-template>  
      <xsl:value-of select="substring(PayMoney*100,1,12)"/>
			<!--响应码-->
			<xsl:value-of select="'  '"/>
			<!--保留域-->
			<xsl:value-of select="substring(PayCode,1,20)"/>
			<xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
        <xsl:with-param name="length" select="100"/>
        <xsl:with-param name="key" select="' '"/>
      </xsl:call-template> 
			<!-- 回车换行 -->
			
			<xsl:text>&#13;&#10;</xsl:text>
		</xsl:for-each>
		<xsl:for-each select="BANKDATA/ROW[position()=$MaxRow+1]">					
      <xsl:variable name="serialno"  select ="SerialNo"/>
			<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
			<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
			<!-- 总金额 12位 -->
			<xsl:value-of select="format-number($totalpay*100, '000000000000')"/>
			<!-- 总笔数 12位-->
			<xsl:value-of select="format-number($totalpic, '000000000000')"/>

			<xsl:value-of select="'000000000000'"/>
			<xsl:value-of select="'000000000000'"/>
			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	 </xsl:for-each>

	</xsl:template>

</xsl:stylesheet>
