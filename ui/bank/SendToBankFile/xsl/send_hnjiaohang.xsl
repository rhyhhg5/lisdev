<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
							<xsl:value-of select="'BXGS'"/>
							<xsl:value-of select="'              '"/>
							<xsl:value-of select="'8010'"/>
							<xsl:value-of select="'                                                                                                                                                                                                                            '"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 序号 -->
  		<xsl:value-of select="position()"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(position())"/>
				<xsl:with-param name="length" select="18"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			
			<xsl:value-of select="substring(IDNo, 1, 18)"/>
			
			<xsl:variable name="accNameValue" select="substring(AccName, 1, 18)"/>
		  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
		  <xsl:value-of select="$accNameValue"/>
		  <xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length($accNameValue)+$accNameLen"/>
				<xsl:with-param name="length" select="60"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
		  
			<xsl:value-of select="'BXGS'"/>
			<xsl:value-of select="'     '"/>
			<xsl:value-of select="AccNo"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
				<xsl:with-param name="length" select="30"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="'      '"/>
			<xsl:variable name="totalMoney" select="format-number(PayMoney, '0.00')"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length($totalMoney)"/>
				<xsl:with-param name="length" select="12"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="$totalMoney"/>
			<xsl:value-of select="'                                '"/>
			
			<xsl:variable name="PolPolNo" select="PolNo"/>
			<xsl:value-of select="$PolPolNo"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length($PolPolNo)"/>
				<xsl:with-param name="length" select="16"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="'                                                                                            '"/>
			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>