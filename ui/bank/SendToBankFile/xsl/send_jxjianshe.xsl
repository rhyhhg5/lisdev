<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 保单号，用收据号代替 -->  
  <xsl:value-of select="PayCode"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 账户名称，8位 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="4> $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 8-$accNameLen)"/>
    </xsl:when>
    <!-- 多于8个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 4)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="'|'"/>  
  <!-- 账号 -->
  <xsl:value-of select="substring(AccNo, 1, 20)"/>
  <xsl:value-of select="'|'"/>  
  <!-- 交易金额，有小数点 -->
  <xsl:value-of select="PayMoney"/>
  <xsl:value-of select="'|'"/>  
  <!-- 交易日期，转换成yyyymmdd -->
  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
  <xsl:value-of select="'|'"/>
  <!-- 处理标志，4位--> 
  <xsl:value-of select="'9999'"/> 
  <xsl:value-of select="'|'"/>
  <!-- 备注，30位 -->  
  <xsl:value-of select="'|'"/>
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>