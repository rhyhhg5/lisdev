<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">
  <!-- 机构编码  -->
  <xsl:value-of select="ComCode"/>
  <xsl:value-of select="'|'"/>
  
  <xsl:value-of select="PayCode"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select=" string(PayMoney)"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select=" string(SendDate)"/>

  <!-- 以上的代码块根据不同的银行而不同 -->
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>