<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">


	<!-- 处理标识-->
	<xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>1</xsl:text>
    </xsl:when>
   	<xsl:otherwise>
     	<xsl:text>2</xsl:text>
   	</xsl:otherwise>
  </xsl:choose>
  
  <!-- 保险公司自添项-->
	<xsl:value-of select="substring(PayCode, 1, 40)"/>
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="40"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	
	<!-- 账号，不够19位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1,20)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 交易金额，10位 -->
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(string(PayMoney))"/>
    <xsl:with-param name="length" select="12"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
  <xsl:value-of select="PayMoney"/>
  
  <!-- 申请日期，转换成yyyymmdd -->
  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>

  
  <!-- 以上的代码块根据不同的银行而不同 -->
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>