<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
	
			<xsl:variable name="flag" select="AccNo"/>
			
			<!-- 以下的代码块根据不同的银行而不同 -->		
			<xsl:variable name="no"  select ="position()"/> 
			<xsl:value-of select="$no"/>	
			<xsl:value-of select="','"/>
			<xsl:value-of select="AccName"/>
			<xsl:value-of select="','"/>
			<xsl:value-of select="AccNo"/>
  			<xsl:value-of select="','"/>
  			<!-- 银行账号为18位及以下，卡折标志为5，银行账号为19位，卡折标志为4 -->
  			<xsl:choose>
			    <xsl:when test="string-length($flag)=19">
			      <xsl:value-of select="'4'"/>
			    </xsl:when>
			    <xsl:otherwise>
			      <xsl:value-of select="'5'"/>
			    </xsl:otherwise>
   			</xsl:choose>
  			<xsl:value-of select="','"/>
  			<xsl:value-of select="PayMoney"/>
  			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
	

</xsl:template>

</xsl:stylesheet>