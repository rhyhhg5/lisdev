<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->
  
  <!-- 保单号，填充位，使用收据号代替，不够20位后面补空格 -->
  <xsl:value-of select="PayCode"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 账户名称（姓名），不够12位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="6 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 12-$accNameLen)"/>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 6)"/>
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="12"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 账号，不够22位后面补空格 --> 
  <xsl:value-of select="AccNo"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="22"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 交易金额，12位，保留小数点，左边补零 -->  
 
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(format-number(PayMoney, '#########.00'))"/>
    <xsl:with-param name="length" select="12"/>
    <xsl:with-param name="key" select="'0'"/>
  </xsl:call-template>   
  <xsl:value-of select="format-number(PayMoney, '#########.00')"/>
  
  <!-- 以上的代码块根据不同的银行而不同 -->
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>