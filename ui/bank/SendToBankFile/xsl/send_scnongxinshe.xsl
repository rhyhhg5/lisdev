<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
						<xsl:text>3300000121</xsl:text>
						<xsl:text>|</xsl:text>
						<xsl:text>7336</xsl:text>
						<xsl:text>|</xsl:text>
						<xsl:text>88140120003397726</xsl:text>
						<xsl:text>|</xsl:text>
						<xsl:text>中国人民健康保险股份有限公司四川分公司</xsl:text>
						<xsl:text>|</xsl:text>
						<xsl:variable name="serialno"  select ="SerialNo"/> 
					  <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						<!-- 总笔数 -->
						<xsl:value-of select="$totalpic"/>
						<!-- 分隔符--> 
						<xsl:text>|</xsl:text>
				    <!-- 总金额  -->
						<xsl:value-of select="format-number($totalpay, '0.00')"/>		
						<xsl:text>|</xsl:text>								        						                    
						<!-- 回车换行 -->
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW"> 
			<xsl:value-of select="AccNo"/>
			<xsl:value-of select="'|'"/>
			<xsl:value-of select="AccName"/>
			<xsl:value-of select="'|'"/>
			<xsl:value-of select="PayMoney"/>
			<xsl:value-of select="'|'"/>
			<xsl:value-of select="PayCode"/>
  		<xsl:value-of select="'|'"/>
  		<xsl:value-of select="IDNo" />
  		<xsl:value-of select="'|'"/>
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>
</xsl:stylesheet>