<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							<!-- 汇总标志，1位--> 
							<xsl:value-of select="'F'"/>
							<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>	
							<!-- 商户代码，4位--> 
							<xsl:value-of select="'0004'"/>
							<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>	
							<xsl:variable name="date"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2())"/> 
						  <xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,'tjyz'),2))"/>
							<!-- 批次号，20位当天唯一--> 
							<xsl:value-of select="concat('0004','370101',$date,$no)"/>
							<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
							<!-- 收付标志，1位--> 
							<xsl:choose>
    			      <xsl:when test="DealType='S'">
      				   <xsl:value-of select="'0'"/>
    			      </xsl:when>
   				      <xsl:otherwise>
     				      <xsl:value-of select="'1'"/>
   				      </xsl:otherwise>
  		        </xsl:choose>	
							<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
							<xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						  <!-- 总笔数 4位-->
						  <xsl:value-of select="substring($totalpic, 1,4)"/>
						  <!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
				      <!-- 总金额 12位 -->
						  <xsl:value-of select="substring($totalpay, 1,12)"/>
						  <!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
											        						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			<!-- 明细标志--> 
			<xsl:value-of select="'1'"/>
			<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
			
			<!-- 收款人卡号，30位 -->
  		<xsl:value-of select="substring(AccNo, 1, 20)"/>
  		<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
  		<!-- 金额，12位 -->
  		<xsl:value-of select="substring(PayMoney, 1,12)"/>
  		<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
  		<!-- 保单号，30位 -->
  		<xsl:value-of select="substring(PayCode, 1, 30)"/>
  		<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
  		<!-- 收款人开户行名称，30位 -->
  		<xsl:choose>
    			<xsl:when test="AccName='null'">
      				<xsl:text>#</xsl:text>
    			</xsl:when>
   				<xsl:otherwise>
     				 <xsl:value-of select="substring(AccName, 1, 30)"/>
   				</xsl:otherwise>
  		</xsl:choose>
  		<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
  		<!-- 批量交易类型--> 
			<xsl:value-of select="'01'"/>
			<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
			<!-- 备用 10--> 
			<xsl:value-of select="'#'"/>
			<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
			<!-- 备用 30--> 
			<xsl:value-of select="'#'"/>
			<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
			<!-- 备用 30--> 
			<xsl:value-of select="'#'"/>
			<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
			<!-- 备用 30--> 
			<xsl:value-of select="'#'"/>
			<!-- 分隔符--> 
							<xsl:text>&#9;</xsl:text>
			
			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>