<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
					<xsl:value-of select="'序号'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'币种'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'金额'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'付方开户行名'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'付方卡号'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'缴费编号'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'付方户名'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'收款单位开户行名'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'省份'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'收款方地区名'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'地区代码'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'收款账号'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'协议编号'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'收款单位名称'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'用途'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'备注'"/>
					<xsl:value-of select="'|'"/>						        						                    
						<!-- 回车换行 -->
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>	
<xsl:for-each select="BANKDATA/ROW">
         <xsl:value-of select="position()"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'RMB'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select=" string(PayMoney)"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="''"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="string(AccNo)"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="string(PayCode)"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="string(AccName)"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'中国工商银行楚雄经济技术开发区支行'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'云南'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'楚雄市'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'2516'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'2516032629200050987'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'BDT6902'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'中国人民健康保险股份有限公司楚雄中心支公司'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="'交保费'"/>
					<xsl:value-of select="'|'"/>
					<xsl:value-of select="''"/>
					<xsl:value-of select="'|'"/>		
  
  <!-- 以上的代码块根据不同的银行而不同 -->
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>