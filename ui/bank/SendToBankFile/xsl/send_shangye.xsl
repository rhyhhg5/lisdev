<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 账号，16位 -->
  <xsl:value-of select="substring(AccNo, 1, 16)"/>
  <xsl:value-of select="','"/>
  
  <!-- 交易日期，转换成yyyymmdd -->
  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
  <xsl:value-of select="','"/>
  
  <!-- 库标（首续标志），6表示个人投保单号（首期），2、3表示个人保单号（续期），其它归为特殊划帐-->
  <xsl:choose>
    <xsl:when test="NoType='6'">
      <xsl:text>0</xsl:text>
    </xsl:when>
    <xsl:when test="NoType='2'">
      <xsl:text>1</xsl:text>
    </xsl:when>
    <xsl:when test="NoType='3'">
      <xsl:text>1</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>0</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="','"/>
  
  <!-- 账户名称，16位 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="8 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 16-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 8)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="','"/>
  
  <!-- 交易金额，16位，有小数点 -->
  <xsl:value-of select="substring(PayMoney, 1, 16)"/>
  <xsl:value-of select="','"/>
  
  <!-- 备注，50位，收据号 -->
  <xsl:value-of select="PayCode"/>
  <xsl:value-of select="','"/>
  
  <!-- 险种代码，24位 -->
  <!-- 获取险种、交费年期和交费间隔类型 -->
  <xsl:variable name="polNoValue" select="PolNo"/>
  <xsl:value-of select="java:com.sinosoft.lis.pubfun.XmlFun.getRiskInfo($polNoValue)"/>
  <xsl:value-of select="','"/>
  
  <!-- 预留字段，6位，号码类型 -->
  <xsl:value-of select="NoType"/>
  <xsl:value-of select="','"/>
  
  <!-- 保单号，50位 -->
  <xsl:value-of select="PolNo"/>
  <xsl:value-of select="','"/>
  
  <!-- 收付标志 -->
  <xsl:value-of select="'0'"/>
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>