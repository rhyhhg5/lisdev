<?xml version="1.0" encoding="gb2312"?>
<!-- yingxl 2008-04-11 sinosoft 公司编号|帐户名|帐号|金额|日期 -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java" version="1.0">

	<xsl:output method="text" encoding="gb2312" />

	<xsl:include href="send_function.xsl" />

	<xsl:template match="/">

		<xsl:for-each select="BANKDATA/ROW">
			<!-- 机构编码  -->
			<xsl:value-of select="ComCode"/>
      <xsl:value-of select="'|'"/>
			
			<!-- 公司编号  -->
      <xsl:value-of select="PayCode"/>
      <xsl:value-of select="'|'"/>
      
			<!-- 帐户名-->
      <xsl:value-of select="AccName"/>
		  <xsl:value-of select="'|'"/> 
		  
			<!-- 账号-->
      <xsl:value-of select="AccNo"/>
      <xsl:value-of select="'|'"/>
      
			<!-- 金额 -->
	    <xsl:value-of select="PayMoney"/>		
      <xsl:value-of select="'|'"/>
      
			<!-- 交易日期 -->
		
				<xsl:value-of
					select="java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()" />
			
			<!-- 回车换行 -->
			<xsl:text>&#13;&#10;</xsl:text>

		</xsl:for-each>

	</xsl:template>

</xsl:stylesheet>
