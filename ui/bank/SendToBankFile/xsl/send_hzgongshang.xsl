<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->  
  <!-- 账户名称（姓名），不够8位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="4 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 8-$accNameLen)"/>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 4)"/>
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="8"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  
  <!-- 账号，不够19位后面补空格 -->
  <xsl:value-of select="substring(AccNo, 1, 19)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="19"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>  

  <!-- 交易金额，10位，乘100用来去掉小数点 -->
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(format-number(PayMoney*100, '##########'))"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template> 
  <xsl:value-of select="format-number(PayMoney*100, '##########')"/> 
  <!--<xsl:value-of select="format-number(PayMoney*100, '#####.##')"/> -->

  <!-- 单据号，填充位，使用收据号代替，不够20位前边补空格 -->  
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template> 
  <xsl:value-of select="PayCode"/>
  <!-- 交易日期，转换成yyyymmdd，8位 -->
  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>  
  <!-- 处理标志，2位--> 
  <xsl:value-of select="'00'"/> 
  <!-- 备注，20位，右边补空格 -->  
  <xsl:value-of select="'                    '"/>                                               
  <!-- 以上的代码块根据不同的银行而不同 -->
   
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>