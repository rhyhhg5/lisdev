<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
	<xsl:choose>
		<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
			</xsl:when>
				<xsl:otherwise>
					<!-- 收款标志 -->
  				<xsl:value-of select="'F'"/>
  				<xsl:value-of select="','"/> 
  				
  				<!-- 商户ID -->
  				<xsl:value-of select="'1342000006'"/>
  				<xsl:value-of select="','"/> 
  				
  				<!-- 提交日期 -->
  				<xsl:variable name="tDate"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/> 
					<xsl:value-of select="concat(substring($tDate,1,4),substring($tDate,6,2),substring($tDate,9,2))"/> 		
					<xsl:value-of select="','"/> 
					
  				<!-- 总记录数 -->
  				<xsl:variable name="serialno"  select ="SerialNo"/> 
					<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
					<xsl:value-of select="number($totalpic)"/>
  				<xsl:value-of select="','"/> 
  				
  				<!-- 总金额 -->
  				<xsl:value-of select="format-number($totalpay*100, '####')"/> 
  				<xsl:value-of select="','"/> 
  				
  				<!-- 业务类型 -->
  				<xsl:value-of select="'10600'"/> 
					
					<!-- 回车换行 -->
					<xsl:text>&#13;&#10;</xsl:text>
				</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	
	<xsl:for-each select="BANKDATA/ROW">
		<!-- 1处理顺序号，6位，是 --> 
		<xsl:value-of select="format-number(position(), '000000')"/>
		<xsl:value-of select="','"/> 
		
		<!-- 2通联支付用户编号，20位,否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 3银行代码，3位，是--> 
		<xsl:value-of select="substring(UniteBankCode, 1,3)"/>
		<xsl:value-of select="','"/> 
		
		<!-- 4帐号类型，2位，否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 账号，32位，是 --> 
		<xsl:value-of select="substring(AccNo, 1,32)"/>
		<xsl:value-of select="','"/> 
		
		<!-- 账户名，60，是 --> 
		<xsl:value-of select="substring(AccName, 1,60)"/>
		<xsl:value-of select="','"/> 
		
		<!-- 开户行所在省	CHAR(20)	否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 开户行所在市	CHAR(20)	否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 开户行名称	CHAR(60)	否 --> 
		<xsl:value-of select="','"/> 
		
		<!-- 账户类型	CHAR(1)	否 -->
		<xsl:value-of select="','"/> 
		
		<!--金额	CHAR(12)	是 -->
		<xsl:value-of select="format-number(PayMoney*100, '####')"/> 
		<xsl:value-of select="','"/> 
		
		<!--货币类型	CHAR(3)	否 -->
		<xsl:value-of select="','"/> 
		
		<!--协议号	CHAR(60)	否 -->
		<xsl:value-of select="','"/> 
		
		<!--协议用户编号	CHAR(30) 否 -->
		<xsl:value-of select="','"/> 
		<!--开户证件类型	CHAR(1)	Null-->
		<xsl:value-of select="','"/> 
		<!--证件号	CHAR(22)	否      -->
		<xsl:value-of select="','"/> 
		<!--手机号小灵通	CHAR(13)	否-->
		<xsl:value-of select="','"/> 
		<!--自定义用户号	CHAR(20)	否-->
		<xsl:value-of select="','"/> 
		<!--备注	CHAR(50)	否        -->
		<xsl:value-of select="','"/> 
		<!--反馈码	CHAR(4)	否        -->
		<xsl:value-of select="','"/> 
		<!--原因	CHAR(100)	否        -->
		
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>
	
</xsl:template>

</xsl:stylesheet>