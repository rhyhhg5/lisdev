<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">


<xsl:for-each select="BANKDATA/ROW">
 
  <xsl:variable name="positions" select="position()"/>
    <xsl:value-of select="$positions"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length($positions)"/>
    <xsl:with-param name="length" select="25"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <!-- 账户名称，10	右对齐，不足补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="10 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 20-$accNameLen)"/>
      <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length($accNameValue)+$accNameLen"/>
        <xsl:with-param name="length" select="20"/>
        <xsl:with-param name="key" select="' '"/>
      </xsl:call-template>

    </xsl:when>
    <!-- 多于5个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 10)"/>
    </xsl:otherwise>
  </xsl:choose>

    <!-- 账号，不够19位右补空格 -->
  <xsl:value-of select="substring(AccNo, 1, 30)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
   <!-- 交易金额，11位 -->
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
    <xsl:with-param name="length" select="15"/>
    <xsl:with-param name="key" select="'0'"/>
  </xsl:call-template>
  <xsl:value-of select="substring(PayMoney, 1, 15)"/>
  
   <xsl:value-of select="substring(PayCode, 1, 20)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <!-- 以上的代码块根据不同的银行而不同 -->
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>