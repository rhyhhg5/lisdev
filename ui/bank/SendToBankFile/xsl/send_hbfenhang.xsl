<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
					    <!-- 商户号 -->
				      <xsl:value-of select="'12345000000001'"/>
		          <xsl:value-of select="'|'"/>  
					    
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
						  <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
					    
					    <!-- 明细序号 6位-->
				      <xsl:variable name="date"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2())"/> 
						  <xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,'tjyz'),2))"/>
							<!-- 批次号，20位当天唯一--> 
							<xsl:value-of select="concat('0000',$no)"/>
						  <xsl:value-of select="'|'"/>
						  
						  <!-- 总笔数 4位-->
						  <xsl:value-of select="substring($totalpic, 1,4)"/>
						  <xsl:value-of select="'|'"/>
						  
						  <!-- 总金额 12位 -->
						  <xsl:value-of select="substring($totalpay*100, 1,12)"/> 
              
              
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			
	<!-- 日期，转换成yyyymmdd -->
	<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>		
	<xsl:value-of select="'|'"/>
	
	<!-- 保单号，30位 -->
  <xsl:value-of select="substring(PayCode, 1, 30)"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 银行编码，4位 -->
  <xsl:value-of select="string(BankCode)"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 银行标识，4位 -->
  <xsl:value-of select="'1'"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 银行账号，4位 -->
  <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 银行账号名称，4位 -->
  <xsl:value-of select="string(AccName)"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 身份类型，2位 -->
  <xsl:value-of select="'01'"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 身份号码，2位 -->
  <xsl:value-of select="string(IDNo)"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 金额，变长12位 -->
  <xsl:value-of select="string(PayMoney*100)"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 扣款用途，2位 -->
  <xsl:value-of select="'代扣'"/>
  <xsl:value-of select="'|'"/>
  
  <!-- 私有域，2位 -->
  <xsl:value-of select="'空'"/>
 
  
	
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>