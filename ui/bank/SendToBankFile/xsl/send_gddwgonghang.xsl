<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">
	<xsl:variable name="date"  select ="substring(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate2(),3,6)"/> 
	<xsl:variable name="time"  select ="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentTime())"/> 
	<xsl:variable name="no"  select ="string(java:com.sinosoft.lis.pubfun.SysMaxNoPicch.CreateMaxNoByClass(concat($date,$time),1))"/> 
  <!-- 1位序列号 -->
  <xsl:value-of select="$no"/>  
  <xsl:value-of select="','"/> 
  <!-- 账户名称（姓名） -->
  <xsl:value-of select="AccName"/> 
  <xsl:value-of select="','"/> 
  <!-- 账号 -->
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="','"/>
  <!-- 交易金额 -->
  <xsl:value-of select="PayMoney*100"/> 

  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>