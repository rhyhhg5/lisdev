<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							<!-- 业务类型号:40501 付费标识--> 
							<xsl:value-of select="'40501'"/>
              <xsl:value-of select="'|'"/>
							<!-- 分隔符--> 
							<xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalnum" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				      <!-- 总笔数 6位-->
						  <xsl:value-of select="$totalnum"/>
						  <!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
				      <!-- 总金额 15位：注意这里的总金额位数 -->
				      <xsl:value-of select="$totalpay"/>	
						  <!-- 分隔符--> 
							<xsl:value-of select="'|'"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
			
			<!-- 明细序号，16位当天唯一--> 
			<xsl:value-of select="position()"/>
			<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>

			<xsl:variable name="paycode"  select ="PayCode"/> 
			<xsl:variable name="Bankname" select="string(java:com.sinosoft.lis.pubfun.BankFileInfo.getBankName($paycode))"/>
			<!-- 行名 -->
  		<xsl:value-of select="$Bankname"/>
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>			
			<!-- 账号 -->
  		<xsl:value-of select="AccNo"/>	
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>			
			<!-- 户名 -->
  	  <xsl:value-of select="AccName"/>	
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>	
  		<!-- 金额-->
  		<xsl:value-of select="PayMoney"/>			
  		<!-- 分隔符--> 
			<xsl:value-of select="'|'"/>	
  		   			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>
</xsl:stylesheet>
