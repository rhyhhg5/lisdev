<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							
							<!-- 表头信息-->
							<xsl:value-of select="'序号'"/>
							<xsl:value-of select="'&#9;'"/>
							
							<xsl:value-of select="'账号'"/>
							<xsl:value-of select="'&#9;'"/>
							
							<xsl:value-of select="'户名'"/>
							<xsl:value-of select="'&#9;'"/>
							
							<xsl:value-of select="'金额'"/>
							<xsl:value-of select="'&#9;'"/>
							
							<xsl:value-of select="'摘要'"/>
							<xsl:value-of select="'&#9;'"/>
							
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
	<!--序号-->		
	<xsl:value-of select="position()"/>  
	<xsl:value-of select="'&#9;'"/>
	<!-- 账号，不超过30位-->
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="'&#9;'"/>
  <!--付方户名，60位-->
  <xsl:value-of select="AccName"/> 
  <xsl:value-of select="'&#9;'"/>
  <!--金额-->
  <xsl:value-of select="PayMoney"/>
  <xsl:value-of select="'&#9;'"/>
  <!-- 摘要 -->
  <xsl:value-of select="'    '"/>
  <xsl:value-of select="'&#9;'"/>
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>
</xsl:template>

</xsl:stylesheet>