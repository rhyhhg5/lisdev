<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<!-- 汇总记录 -->
	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							<xsl:value-of select="'0012'"/>
						<xsl:value-of select="'|'"/>
						<xsl:value-of select="'0100'"/>
						<xsl:value-of select="'|'"/>
						<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
						<xsl:value-of select="'|'"/>
						<xsl:value-of select="DealType"/>
						<xsl:value-of select="'|'"/>
						<xsl:variable name="serialno" select="SerialNo"/> 
			      <xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>         
						<xsl:value-of select="format-number(substring($totalpay,1,15), '0.00')"/> 
						<xsl:value-of select="'|'"/>
						<xsl:value-of select="$totalpic"/>
											        						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
<xsl:for-each select="BANKDATA/ROW">
  <!-- 以下的代码块根据不同的银行而不同 -->
  <xsl:value-of select="SerialNo"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="position()"/>	
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="PayMoney"/>
  <xsl:value-of select="'|'"/> 
  <xsl:value-of select="PayCode"/> 
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="AccName"/> 
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'|'"/>
  <xsl:value-of select="'1'"/>
  <xsl:value-of select="'|'"/>
  
  <xsl:variable name="serialno" select="SerialNo"/> 
	<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
  <xsl:choose>
		    <xsl:when test="position() = $totalpic">
		    </xsl:when>
		    <!-- 多于四个汉字的处理 -->
		    <xsl:otherwise>
		        <!-- 回车换行 -->
  					<xsl:text>&#13;&#10;</xsl:text>
		    </xsl:otherwise>
	</xsl:choose>	


</xsl:for-each>

</xsl:template>

</xsl:stylesheet>