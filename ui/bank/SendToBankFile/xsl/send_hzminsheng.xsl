<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 账号 -->
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="'|'"/> 
  <!-- 交易金额，有小数点 -->
  <xsl:value-of select="PayMoney"/>
  <xsl:value-of select="'|'"/>   
  <!-- 账户名称，32位 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="16> $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 32-$accNameLen)"/>
    </xsl:when>
    <!-- 多于8个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 16)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="'|'"/>  
  <!-- 保单号，用收据号代替 -->  
  <xsl:value-of select="PayCode"/>
  <xsl:value-of select="'|'"/> 
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>