<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
<xsl:variable name="MaxRow"  select ="0"/>
<xsl:for-each select="BANKDATA/ROW">
	<xsl:variable name="MaxRow"  select ="position()"/>
	<!-- 记录标志，1位  --> 
	<xsl:value-of select="'2'"/>
	<!-- 代理业务编号，9位  --> 
	<xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>340000895</xsl:text>
    </xsl:when>
   	<xsl:otherwise>
     	<xsl:text>340000894</xsl:text>
   	</xsl:otherwise>
  </xsl:choose>
	<!-- 代理业务种，3位  --> 
	<xsl:value-of select="'291'"/>
	<!-- 账号，19位 不足后补空格 --> 
  <xsl:value-of select="substring(AccNo,1,19)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="19"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <!-- 帐号使用标志，1位 --> 
  <xsl:value-of select="'0'"/>
  <!-- 处理顺序号，6位 --> 
	<xsl:value-of select="format-number(position(), '000000')"/> 
	<!-- 应处理金额，17位 --> 
	<xsl:value-of select="format-number(PayMoney*100, '00000000000000000')"/> 
	<!-- 应处理笔数，17位 --> 
  <xsl:value-of select="'00000000000000001'"/> 
  <!-- 借贷标志，1位 --> 
  <xsl:choose>
		<xsl:when test="DealType='S'">
				<xsl:value-of select="'1'"/> 
		</xsl:when>
		<xsl:otherwise>
				<xsl:value-of select="'2'"/>
		</xsl:otherwise>
	</xsl:choose>
	<!-- 摘要，21位 -->
	<xsl:value-of select="'                     '"/>
	<!-- 备注，61位 -->
	<xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="61"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	<xsl:value-of select="substring(PayCode,1,61)"/>
  
   
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW[position()=$MaxRow+1]">
							<!-- 记录标志，1位  --> 
							<xsl:value-of select="'1'"/>
							<!-- 代理业务编号，9位  --> 
							<xsl:choose>
						    <xsl:when test="DealType='S'">
						      <xsl:text>340000895</xsl:text>
						    </xsl:when>
						   	<xsl:otherwise>
						     	<xsl:text>340000894</xsl:text>
						   	</xsl:otherwise>
						  </xsl:choose>
						  <!-- 代理业务种，3位  --> 
							<xsl:value-of select="'291'"/>
							<!-- 账号，19位 不足后补空格 --> 
  						
  						<xsl:choose>
						    <xsl:when test="DealType='S'">
						      <xsl:value-of select="'3400200129023109827'"/>
						    </xsl:when>
						   	<xsl:otherwise>
						     	<xsl:value-of select="'3400200129023109703'"/>
						   	</xsl:otherwise>
						  </xsl:choose>	
							<!-- 帐号使用标志，1位 --> 
  						<xsl:value-of select="'0'"/>  						
              <xsl:variable name="serialno"  select ="SerialNo"/>
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				      <!-- 处理顺序号，6位 --> 
  						<xsl:value-of select="format-number($totalpic+1, '000000')"/>
				      <!-- 总金额 15位 -->
						  <xsl:value-of select="format-number($totalpay*100, '00000000000000000')"/>
				      <!-- 总笔数 6位-->
						  <xsl:value-of select="format-number($totalpic, '00000000000000000')"/>
						  <!-- 借贷标志，1位 -->
						  <xsl:choose>
						    <xsl:when test="DealType='S'">
						      <xsl:value-of select="'2'"/>
						    </xsl:when>
						   	<xsl:otherwise>
						     	<xsl:value-of select="'1'"/>
						   	</xsl:otherwise>
						  </xsl:choose>						
						  <!-- 摘要，21位 -->
							<xsl:value-of select="'                     '"/>
							<!-- 备注，61位 -->
							<xsl:value-of select="'                                                             '"/>
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>


</xsl:template>

</xsl:stylesheet>