﻿<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
  		<xsl:value-of select="AccNo"/>
			<xsl:text>|</xsl:text>
			<xsl:value-of select="AccName"/>
			<xsl:text>|</xsl:text>
  		<xsl:value-of select="format-number(PayMoney, '0.00')"/>	
			<xsl:text>|</xsl:text>
	<xsl:variable name="IDType" select="string(IDType)"/>
  <xsl:choose>
    <xsl:when test="$IDType='0'">
      <xsl:value-of select="'身份证'"/>
    </xsl:when>
    <xsl:when test="$IDType='1'">
      <xsl:value-of select="'护照'"/>
    </xsl:when>
    <xsl:when test="$IDType='2'">
      <xsl:value-of select="'军官证'"/>
    </xsl:when>
    <xsl:when test="$IDType='3'">
      <xsl:value-of select="'驾照'"/>
    </xsl:when>
    <xsl:when test="$IDType='4'">
      <xsl:value-of select="'户口本'"/>
    </xsl:when>
    <xsl:when test="$IDType='5'">
      <xsl:value-of select="'学生证'"/>
    </xsl:when>
    <xsl:when test="$IDType='6'">
      <xsl:value-of select="'工作证'"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="'无证件'"/>
    </xsl:otherwise>
  </xsl:choose> 
  	  <xsl:text>|</xsl:text>
  		<xsl:value-of select="IDNo"/>	
			<xsl:text>|</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>