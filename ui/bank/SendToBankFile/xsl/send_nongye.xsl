<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 授权书号 -->
  <xsl:value-of select="'000000000'"/>
  
  <!-- 收付标志（借贷标志） -->
  <xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:text>0</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>1</xsl:text>
    </xsl:otherwise>
  </xsl:choose>  
  
  <!-- 账号，不够18位后面补空格 -->
  <xsl:value-of select="substring(AccNo, 1, 18)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="18"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <!-- 证件号 ,用户名代替，不够18位右补空格-->
  
  <xsl:value-of select="substring(AccName, 1, 18)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName)"/>
    <xsl:with-param name="length" select="18"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
   
  <!-- 交易金额，12位，乘100用来去掉小数点 -->
  <xsl:value-of select="format-number(PayMoney*100, '000000000000')"/> 
  
  <!-- 交易日期，转换成yyyymmdd -->
  <xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>
  
  <!-- 险种，4位，左补零 -->
  <xsl:value-of select="'0000'"/>
  
  <!-- 保单号，18位，全为空格 -->
  <xsl:value-of select="substring(PayCode,3,20)"/>
  
  
  <!-- 以上的代码块根据不同的银行而不同 -->
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>