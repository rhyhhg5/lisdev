<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:for-each select="BANKDATA/ROW">
			<!-- 以下的代码块根据不同的银行而不同 -->
							<!-- 记录标志,1位 --> 
							<xsl:value-of select="'2'"/>
							<!-- 代理业务编号，9位--> 
					<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>602289008</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>536001021</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose>
							<!-- 代理业务种类，3位-->   				
					<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>602</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>536</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose>  						
							<!-- 以下的代码块根据不同的银行而不同 -->
  						<!-- 账号，不超过19位-->
  						<!-- 账号，不够19位右补空格 -->
  					<xsl:value-of select="substring(AccNo, 1, 19)"/>
  					<xsl:call-template name="supplement">
    						<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    						<xsl:with-param name="length" select="19"/>
    						<xsl:with-param name="key" select="' '"/>
  					</xsl:call-template>			
							<!-- 帐号使用标志，2位--> 
							<xsl:value-of select="'00'"/>			
				      <!-- 明细序号 6位-->
				     <xsl:variable name="detailnumber" select="position()"/>
						 <xsl:value-of select="format-number($detailnumber, '000000')"/>			
  						<!-- 应收(付)金额 17位 -->
  						<xsl:value-of select="format-number(PayMoney*100, '00000000000000000')"/>			
							<!-- 应收(付)笔数 17位-->
							<xsl:value-of select="'00000000000000001'"/>
  						<!-- 借贷标志,1位 -->
  				<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>1</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>2</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose>							
  				<xsl:value-of select="PayCode"/>
 					 <xsl:call-template name="supplement">
   				 <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    			 <xsl:with-param name="length" select="22"/>
    			<xsl:with-param name="key" select="' '"/>
  				</xsl:call-template>
  				  <!-- 账户名称，不够48位右补空格 -->
  					<xsl:variable name="accNameValue" select="string(AccName)"/>
  					<xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>

  					<xsl:choose>
    			<xsl:when test="24 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 48-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 24)"/>
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="48"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  				
  												
  						<!-- 备用，12位--> 
							<xsl:value-of select="'            '"/>	  				
							<!-- 填充字段，100位--> 
							<xsl:value-of select="'                                                                                                    '"/>	  								  																		        						                    

	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
							<!-- 记录标志,1位 --> 
							<xsl:value-of select="'1'"/>
					<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>602289008</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>536001021</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose>
							<!-- 代理业务种类，3位-->   				
					<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>602</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>536</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose> 							
							<!-- 帐号/卡号，19位--> 
							<xsl:value-of select="'0000000000000000000'"/>		
							<!-- 帐号使用标志，2位--> 
							<xsl:value-of select="'00'"/>
							<!-- 处理顺序号，6位--> 
							<xsl:value-of select="'000000'"/>
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				         	<!-- 应收(付)总金额 17位 -->
						      <xsl:value-of select="format-number($totalpay*100, '00000000000000000')"/> 
				          <!-- 应收(付)总笔数 17位-->
						      <xsl:value-of select="format-number($totalpic, '00000000000000000')"/>							 	
  						<!-- 借贷标志,1位 -->
  				<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>1</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>2</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose>
  				  	<!-- 70位--> 
							<xsl:value-of select="'                                                                      '"/>	  				
  				  	
  				  	<!-- 备用，12位--> 
							<xsl:value-of select="'            '"/>	  				
							<!-- 填充字段，100位--> 
							<xsl:value-of select="'                                                                                                    '"/>	  								  																		        						                    

  				<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>