﻿<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
  <xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>

	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>
				      <xsl:value-of select="'付款帐号开户行'"/> 
							<xsl:value-of select="'&#9;'"/>  
						  <xsl:value-of select="'付款帐号'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'付款帐号名称'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'收款帐号开户行'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'省份名'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'地市名'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'收款帐号'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'收款帐号名称'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'金额(分)'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'汇款用途'"/> 
							<xsl:value-of select="'&#9;'"/>   
							<xsl:value-of select="'备注'"/> 
							<xsl:value-of select="'&#9;'"/>  
						 <xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
<xsl:for-each select="BANKDATA/ROW">
	<xsl:value-of select="'工行广州粤电支行'"/>
  <xsl:value-of select="'&#9;'"/> 
   <xsl:value-of select="'3602202329100015176'"/>
  <xsl:value-of select="'&#9;'"/>  
   <xsl:value-of select="'中国人民健康保险股份有限公司广东分公司'"/>
  <xsl:value-of select="'&#9;'"/>  
     <xsl:value-of select="BankCode"/>
  <xsl:value-of select="'&#9;'"/>
        <xsl:value-of select="'广东省'"/>
  <xsl:value-of select="'&#9;'"/> 
        <xsl:value-of select="'地市名'"/>
  <xsl:value-of select="'&#9;'"/>
   <xsl:value-of select="substring(AccNo, 1, 21)"/>
  <xsl:value-of select="'&#9;'"/> 
   <xsl:value-of select="substring(AccName, 1, 40)"/>
  <xsl:value-of select="'&#9;'"/>
     <xsl:value-of select="concat(PayMoney,'元')"/>
  <xsl:value-of select="'&#9;'"/>
  <xsl:value-of select="'保险付费'"/>
  <xsl:value-of select="'&#9;'"/>  
	<xsl:value-of select="PayCode"/>
  <xsl:value-of select="'&#9;'"/> 

  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>