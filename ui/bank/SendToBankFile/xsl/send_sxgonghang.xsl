<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>
<xsl:template match="/">
	<!-- 汇总记录 -->
	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
						<xsl:value-of select="'缴费编号'"/>
						<xsl:value-of select="'|'"/>
						<xsl:value-of select="'缴费通知书号码'"/>
						<xsl:value-of select="'|'"/>
						<xsl:value-of select="'付方户名'"/>
						<xsl:value-of select="'|'"/>
						<xsl:value-of select="'付方卡/帐号'"/> 
			      <xsl:value-of select="'|'"/>
			      <xsl:value-of select="'收费帐号'"/> 
			      <xsl:value-of select="'|'"/>
						<xsl:value-of select="'协议编号'"/>         
					  <xsl:value-of select="'|'"/>
						<xsl:value-of select="'收款单位名称'"/>
						<xsl:value-of select="'|'"/>
						<xsl:value-of select="'付款金额'"/>
						<xsl:value-of select="'|'"/>    	
						<xsl:value-of select="'用途'"/>
						<xsl:value-of select="'|'"/>   
						<xsl:value-of select="'备注'"/>
						<xsl:value-of select="'|'"/>    						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:value-of select="PolNo"/>
			<xsl:value-of select="'|'"/>
			<xsl:value-of select="PayCode"/>
			<xsl:value-of select="'|'"/>
  		        <xsl:value-of select="string(AccName)"/>
  		        <xsl:value-of select="'|'"/>
  		        <xsl:value-of select="AccNo"/>
			        <xsl:value-of select="'|'"/>
              <xsl:value-of select="'3700024629200360194'"/>
              <xsl:value-of select="'|'"/>
              <xsl:value-of select="'BDP6157'"/>
              <xsl:value-of select="'|'"/>
              <xsl:value-of select="'中国人民健康保险股份有限公司陕西分公司'"/>
              <xsl:value-of select="'|'"/>
			<xsl:value-of select="PayMoney"/>
			<xsl:value-of select="'|'"/>
			<xsl:value-of select="'|'"/>
      <xsl:value-of select="'|'"/>
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>