<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>
              <xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 

							<!-- 保险公司名称 32位-->
							<xsl:value-of select="'人民健康天津分公司              '"/>
							
							<!-- 传盘批次编号 3位-->
							<xsl:value-of select="'000'"/>
							
							<!-- 代收标识 -->
							<xsl:value-of select="'D'"/>				
										
				      <!-- 总笔数 9位 右对齐 左补空格
						  <xsl:value-of select="format-number($totalpic, '        ')"/>
						   -->
						  <xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length($totalpic)"/>
              <xsl:with-param name="length" select="9"/>
              <xsl:with-param name="key" select="' '"/>
              </xsl:call-template> 
              <xsl:value-of select="$totalpic"/>
  			     
						  
						  <!-- 总金额 13位  右对齐 左补空格
						  
              <xsl:value-of select="format-number($totalpay*100, '       ')"/>
						  -->
						  <xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length($totalpay*100)"/>
              <xsl:with-param name="length" select="13"/>
              <xsl:with-param name="key" select="' '"/>
              </xsl:call-template> 
              <xsl:value-of select="$totalpay*100"/>
							
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>

	<xsl:for-each select="BANKDATA/ROW">
	
	<!-- 保险公司名称，60位-->
	<xsl:value-of select="substring(PayCode, 1, 60)"/>
  <xsl:call-template name="supplement">
  <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
  <xsl:with-param name="length" select="60"/>
  <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
	
  <!-- 账号，不够30位右对齐 左补空格 
  	<xsl:value-of select="format-number(AccNo, '                        ')"/>
  	-->
  	
              <xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
              <xsl:with-param name="length" select="30"/>
              <xsl:with-param name="key" select="' '"/>
              </xsl:call-template> 
              <xsl:value-of select="AccNo"/>
  
  
  
  
  
  <!-- 帐号使用标志，1位 --> 
		<xsl:value-of select="'1'"/>
 
    <!-- 多于五个汉字的处理 -->
   <xsl:variable name="accNameValue" select="string(AccName)"/>
   <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
   <xsl:choose>
    <xsl:when test="5 > $accNameLen">
    <xsl:value-of select="substring(AccName, 1, 10-$accNameLen)"/>
      <xsl:call-template name="supplement">
        <xsl:with-param name="valueLen" select="string-length($accNameValue)+$accNameLen"/>
        <xsl:with-param name="length" select="10"/>
        <xsl:with-param name="key" select="' '"/>
      </xsl:call-template>     
    </xsl:when>
     <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 5)"/>
    </xsl:otherwise>
  </xsl:choose>
   
    
  <!-- 交易金额，右对齐，左补空格  11位 -->
   
	<xsl:call-template name="supplement">
              <xsl:with-param name="valueLen" select="string-length(PayMoney*100)"/>
              <xsl:with-param name="length" select="11"/>
              <xsl:with-param name="key" select="' '"/>
              </xsl:call-template> 
              <xsl:value-of select="PayMoney*100"/>		
	
			
  <!--入帐结果-->		
	<xsl:value-of select="'0000'"/> 				
			
  <!--费用记录号-->		
	<xsl:value-of select="'               '"/> 

  <!--授权性质-->		
	<xsl:value-of select="'0'"/> 	
	
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
	<xsl:text>&#13;&#10;</xsl:text>
</xsl:template>

</xsl:stylesheet>