<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>
					<xsl:otherwise>	
						<!--保险代码-->	
						<xsl:value-of select="'0090'"/>
						<xsl:value-of select="'|'"/>
						
		
						<xsl:variable name="serialno"  select ="SerialNo"/> 
						<xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
						<xsl:variable name="totalpic" select="number(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
						
						<!--交易日期-->
						 <!--交易时间-->
						<xsl:variable name="Today" select="string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate())"/>
						 <xsl:value-of select="$Today"/>
						 <xsl:value-of select="'|'"/>
						
						<!-- 总笔数 8位-->
						<xsl:call-template name="supplement">
							<!--xsl:with-param name="key" select="' '"/-->
							<xsl:with-param name="valueLen" select="string-length($totalpic)"/>
							<!--xsl:with-param name="length" select="8"/-->
						</xsl:call-template>  
						<xsl:value-of select="$totalpic"/>	
						<xsl:value-of select="'|'"/>
						
						
						<!-- 回车换行 -->
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW">	
		 <xsl:value-of select="position()" />
		 <xsl:value-of select="'|'"/>
		 
		 <!--投保单号-->
		 <xsl:value-of select="substring(SerialNo, 1, 21)"/>
		 <xsl:value-of select="'|'"/>		
		 
		 <!--交易日期-->
		 <xsl:value-of select="SendDate"/>
		 <xsl:value-of select="'|'"/>
		 
		 
		 <!-- 交易金额，整数Max13位，2位小数点 -->
		 <xsl:variable name="PayMoney" select="format-number(PayMoney,'0.00')"/>
		 <xsl:value-of select="substring($PayMoney,1,16)"/>		  
		 <xsl:value-of select="'|'"/>
		 
		 <!-- 账户名称（姓名），不够30位右补空格 -->
		 <xsl:variable name="accNameValue" select="string(AccName)"/>
		 <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
		 <xsl:choose>
		   <xsl:when test="6 > $accNameLen">
		     <xsl:value-of select="substring(AccName, 1, 30-$accNameLen)"/>
		   </xsl:when>
		   <!-- 多于四个汉字的处理 -->
		   <xsl:otherwise>
		     <xsl:value-of select="substring(AccName, 1, 6)"/>
		   </xsl:otherwise>
		 </xsl:choose>
		 <xsl:value-of select="'|'"/>
		 
		 <!--账号，Max:21位-->
		 <xsl:value-of select="substring(AccNo, 1, 21)"/>
		 <xsl:value-of select="'|'"/>	
		 
		 
		 <!-- 回车换行 -->
		 <xsl:text>&#13;&#10;</xsl:text>	
 </xsl:for-each>		

</xsl:template>

</xsl:stylesheet>