<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gb2312"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">
  
  <!-- 序列号 -->
  <xsl:value-of select="position()"/>
  <xsl:value-of select="'|'"/>
  <!-- 账号 -->
  <xsl:value-of select="string(AccNo)"/>
  <xsl:value-of select="'|'"/>
  <!-- 金额 -->
  <xsl:value-of select=" string(PayMoney)"/>
  <xsl:value-of select="'|'"/>
  <!-- 账户名 -->
  <xsl:value-of select=" string(AccName)"/>
  <xsl:value-of select="'|'"/>
  <!-- 证件类型 -->
  <xsl:value-of select=" string(IDType)"/>
  <xsl:value-of select="'|'"/>
  <!-- 证件号码 -->
  <xsl:value-of select=" string(IDNo)"/>
  <xsl:value-of select="'|'"/>
  <!-- 备注 -->
  <xsl:value-of select="' '"/>
  <xsl:value-of select="'|'"/>
 
  


  <!-- 以上的代码块根据不同的银行而不同 -->
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>