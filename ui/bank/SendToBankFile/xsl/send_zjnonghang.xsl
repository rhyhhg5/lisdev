<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

<xsl:for-each select="BANKDATA/ROW">

<!-- 委托书部分开始 -->  
  <!-- 顺序号 12位 左补0 -->
 	<xsl:variable name="detailnumber" select="position()"/>
		<xsl:value-of select="format-number($detailnumber, '000000000000')"/>			
  <xsl:value-of select="','"/>  
  <!-- 管理机构 -->
  <xsl:value-of select="substring(ComCode, 1, 6)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(ComCode)"/>
    <xsl:with-param name="length" select="6"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="','"/>
	<!-- 授权年度 -->
	<xsl:variable name="Today" select="substring(string(java:com.sinosoft.lis.pubfun.PubFun.getCurrentDate()),1,4)"/>
	<xsl:value-of select="number($Today)"/>
	<xsl:value-of select="','"/>  
	<!-- 委托书印刷号 15字符 -->  	
	<xsl:value-of select="'               '"/>
  <xsl:value-of select="','"/>
  <!-- 账号，25位 不足后补空格 --> 
  <xsl:value-of select="AccNo"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="25"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="','"/>
  <!-- 账户名称（姓名），不够60位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  <xsl:choose>
    <xsl:when test="30 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 60-$accNameLen)"/>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 30)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="60"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="','"/>
  <!-- 保险合同号 36字符 续期才有保险合同号 -->
  <xsl:value-of select="'                                    '"/>
  <xsl:value-of select="','"/>
  <!-- 保单号 28字符 只有首期有投保单号 -->
  <xsl:value-of select="PolNo"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PolNo)"/>
    <xsl:with-param name="length" select="28"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="','"/>
  <!-- 授权性质 -->
  <xsl:value-of select="'2'"/>
  <xsl:value-of select="','"/>
  <!-- 身份证号码 20字符 -->
  <xsl:value-of select="IDNo"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(IDNo)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="','"/>
  <!-- 委托书部分结束 -->  

  
  <!-- 转帐文件部分开始 -->  
  <!-- 账号类型，1位 --> 
  <xsl:value-of select="''"/>
  <xsl:value-of select="','"/>
  <!-- 账号，25位 不足后补空格 --> 
  <xsl:value-of select="AccNo"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    <xsl:with-param name="length" select="25"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="','"/>
  <!-- 转账顺序  -->
  <xsl:value-of select="'    '"/>
  <xsl:value-of select="','"/>
  <!-- 交易金额，实际长度输出，乘100用来去掉小数点 -->
  <xsl:value-of select="PayMoney*100"/>  
  <xsl:value-of select="','"/>
  <!-- 收付标志（借贷标志）1位 -->
  <xsl:variable name="tDealType" select="DealType"/>
  <xsl:choose>
    <xsl:when test=" $tDealType='S'">
      <xsl:value-of select="'0'"/>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="'1'"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="','"/> 
    <!-- 顺序号 12位 左补0 -->
 	<xsl:value-of select="string(PayCode)"/>		
  <xsl:value-of select="','"/>  
     
  <!-- 转帐开始日期 可为空 YYYYMMDD格式 -->
  <xsl:value-of select="''"/>  
  <xsl:value-of select="','"/>  
  <!-- 转帐结束时间 可为空 YYYYMMDD格式 -->
  <xsl:value-of select="''"/>  
  <xsl:value-of select="','"/>  
  <!-- 账户名称（姓名），不够60位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  <xsl:choose>
    <xsl:when test="30 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 60-$accNameLen)"/>
    </xsl:when>
    <!-- 多于四个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 30)"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="60"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="','"/>
  <!-- 合同号 续期才有保险合同号 36位 必添 -->
  <xsl:value-of select="'000000000000000000000000000000000000'"/>
  <xsl:value-of select="','"/>
  <!-- 保单号 28位 首期只有保单号 必添 -->
  <xsl:value-of select="PayCode"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    <xsl:with-param name="length" select="28"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <!-- 授权性质 -->
  <xsl:value-of select="','"/>
  <xsl:value-of select="'2'"/>
  <xsl:value-of select="','"/>
  <!-- 销售方式 0对应为银行代理 -->
  <xsl:value-of select="'0'"/>
  <xsl:value-of select="','"/>
   
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>