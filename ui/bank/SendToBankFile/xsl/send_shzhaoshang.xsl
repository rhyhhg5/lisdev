<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">


<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>		
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
              <!-- 总笔数 6位-->
						  <xsl:value-of select="format-number($totalpic, '00000000')"/>
						  <!-- 总金额 12位 -->
						  <xsl:value-of select="format-number($totalpay*100, '000000000000000')"/> 
						  <xsl:value-of select="'00000000'"/> 		
						  <xsl:value-of select="'000000000000000'"/> 	
						  <xsl:value-of select="'00000000'"/> 		
						  <xsl:value-of select="'000000000000000'"/>         						                    
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
  
<xsl:for-each select="BANKDATA/ROW">
	<!-- 日期 -->
	<xsl:value-of select="concat(substring(SendDate,1,4), substring(SendDate,6,2), substring(SendDate,9,2))"/>  
	<!-- paycode -->
	<xsl:value-of select="substring(PayCode, 1, 30)"/> 
	<xsl:call-template name="supplement"> 
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/> 
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>

  <!-- 账户名称，不够60位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
  
  <xsl:choose>
    <xsl:when test="30 > $accNameLen"> 
      <xsl:value-of select="substring(AccName, 1, 60-$accNameLen)"/> 
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 30)"/> 
    </xsl:otherwise>
  </xsl:choose>
  <xsl:call-template name="supplement"> 
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/> <!--string-length()返回参数长度-->
    <xsl:with-param name="length" select="60"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <!-- 金额 -->
  <xsl:value-of select="format-number(PayMoney*100, '000000000000000')"/>
  
  <xsl:value-of select="'                                                                                                    '"/>
  <xsl:value-of select="'                                                                                                    '"/>

  <xsl:value-of select="substring(AccNo, 1, 50)"/> 
  <xsl:call-template name="supplement"> 
    <xsl:with-param name="valueLen" select="string-length(AccNo)"/> <!--string-length()返回参数长度-->
    <xsl:with-param name="length" select="50"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <xsl:value-of select="substring(IDNo, 1, 20)"/>
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(IDNo)"/>
    <xsl:with-param name="length" select="20"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  
  <xsl:value-of select="substring(PayCode, 1, 30)"/> 
	<xsl:call-template name="supplement"> 
    <xsl:with-param name="valueLen" select="string-length(PayCode)"/> 
    <xsl:with-param name="length" select="30"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>
  <xsl:value-of select="'                                                                                                    '"/>

	<!-- 出错代码和解释 -->
  <xsl:value-of select="'                                        '"/>
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>
 

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>