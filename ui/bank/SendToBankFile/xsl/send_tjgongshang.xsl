<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

	<xsl:variable name="setFirstLine" select="java:com.sinosoft.lis.pubfun.XmlFun.setFirstLine()"/>
	<!-- 汇总记录 -->
	<xsl:for-each select="BANKDATA/ROW">
			<xsl:choose>
					<xsl:when test="java:com.sinosoft.lis.pubfun.XmlFun.isFirstLine()=false">
					</xsl:when>

					<xsl:otherwise>			
			<!-- 单位名称,32位 -->
			<xsl:value-of select="'中国人民健康保险股份有限公司天津'"/>
			<!-- 业务种类代码,3位 --> 				
			<xsl:value-of select="'001'"/>
			<!--入账标志,1位 --> 				
					<xsl:choose>
    					<xsl:when test="DealType='S'">
      				<xsl:text>2</xsl:text>
    					</xsl:when>
   					 <xsl:otherwise>
     				 <xsl:text>1</xsl:text>
   					 </xsl:otherwise>
  				</xsl:choose>							
						  <xsl:variable name="serialno"  select ="SerialNo"/> 
					    <xsl:variable name="totalpay" select="string(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPay($serialno))"/>
							<xsl:variable name="totalpic" select="number(java:com.sinosoft.lis.pubfun.GetTotalSum.getTotalPic($serialno))"/> 
				          <!-- 总笔数 9位-->
  					<xsl:call-template name="supplement">
  					    <xsl:with-param name="key" select="' '"/>
  					    <xsl:with-param name="valueLen" select="string-length($totalpic)"/>
    						<xsl:with-param name="length" select="9"/>
  					</xsl:call-template>   	
  					<xsl:value-of select="$totalpic"/>	
				         	<!-- 总金额 13位 -->  					
  					<xsl:call-template name="supplement">
  					    <xsl:with-param name="key" select="' '"/>
  					    <xsl:with-param name="valueLen" select="string-length(format-number($totalpay*100,'00'))"/>
    						<xsl:with-param name="length" select="13"/>
  					</xsl:call-template>   	
  					<xsl:value-of select="format-number($totalpay*100,'00')"/>	  					
  					<!-- 日期，转换成yyyymmdd -->
						  		<xsl:value-of select="concat(substring(SendDate,1,4),'.',substring(SendDate,6,2),'.',substring(SendDate,9,2))"/>   	
  					<xsl:value-of select="format-number($totalpay*100,'00')"/>	
							<!-- 回车换行 -->
							<xsl:text>&#13;&#10;</xsl:text>
					</xsl:otherwise>
			</xsl:choose> 
	</xsl:for-each>
	<xsl:for-each select="BANKDATA/ROW">							
  				<!-- 收据号,20位--> 
  				<xsl:value-of select="PayCode"/>
 					 <xsl:call-template name="supplement">
   				 <xsl:with-param name="valueLen" select="string-length(PayCode)"/>
    			 <xsl:with-param name="length" select="20"/>
    			<xsl:with-param name="key" select="' '"/>
  			</xsl:call-template>
  			<!-- 帐号,19位--> 
  					<xsl:value-of select="substring(AccNo, 1, 19)"/>
  					<xsl:call-template name="supplement">
    						<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
    						<xsl:with-param name="length" select="19"/>
    						<xsl:with-param name="key" select="' '"/>
  					</xsl:call-template>  										
  <!-- 账户名称，不够10位右补空格 -->
  <xsl:variable name="accNameValue" select="string(AccName)"/>
  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>

  <xsl:choose>
    <xsl:when test="10 > $accNameLen">
      <xsl:value-of select="substring(AccName, 1, 10-$accNameLen)"/>
    </xsl:when>
    <!-- 多于五个汉字的处理 -->
    <xsl:otherwise>
      <xsl:value-of select="substring(AccName, 1, 10)"/>
    </xsl:otherwise>
  </xsl:choose>
  
  <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length(AccName) + $accNameLen"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="' '"/>
  </xsl:call-template>							
  				<!-- 入帐金额，11位 --> 					
  					<xsl:call-template name="supplement">
  					    <xsl:with-param name="key" select="' '"/>
  					    <xsl:with-param name="valueLen" select="string-length(format-number(PayMoney*100,'00'))"/>
    						<xsl:with-param name="length" select="11"/>
  					</xsl:call-template>   	
  					<xsl:value-of select="format-number(PayMoney*100, '00')"/>	
  							
	<!-- 处理标志	，3位 -->					
	<xsl:value-of select="'000'"/>						
	<!--保留字段，40位-->		
	<xsl:value-of select="'                                        '"/> 						
	<!-- 回车换行 -->
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>