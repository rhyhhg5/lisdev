<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">

  <xsl:value-of select="'#'"/>
  <xsl:value-of select="'SYSCODE=000 ;'"/>  
  
  <xsl:choose>
    <xsl:when test="DealType='S'">
      <xsl:value-of select="'TYPE=AGENTC ;'"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="'TYPE=AGENTPRS ;'"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:value-of select="'VERSION=2.0'"/>  
  
  <xsl:text>&#13;&#10;</xsl:text>  
  
<xsl:for-each select="BANKDATA/ROW">

  <!-- 以下的代码块根据不同的银行而不同 -->
  <!-- 账号，最大16位 -->
  <xsl:value-of select="'ACCNBR='"/>  
  <xsl:value-of select="AccNo"/>
  <xsl:value-of select="' ;'"/>
    
  <!-- 账户名称，最大20位 -->
  <xsl:value-of select="'CLTNAM='"/>  
  <xsl:value-of select="AccName"/>
  <xsl:value-of select="' ;'"/>
  
  <!-- 交易金额，整数部分最多13位，小数部分为2位 -->
  <xsl:value-of select="'TRSAMT='"/>  
  <xsl:value-of select="PayMoney"/>
  <xsl:value-of select="' ;'"/>   
  
  <!-- 备注，最大16位，这里为15位收据号 -->
  <xsl:value-of select="'TRSDSP='"/>    
  <xsl:value-of select="PayCode"/>
  
  <!-- 回车换行 -->
  <xsl:text>&#13;&#10;</xsl:text>

</xsl:for-each>

</xsl:template>

</xsl:stylesheet>