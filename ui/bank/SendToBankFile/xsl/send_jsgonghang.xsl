<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

 <xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">
	<xsl:for-each select="BANKDATA/ROW">
	  <xsl:variable name="tposition" select="position()"/>
    <xsl:call-template name="supplement">
    <xsl:with-param name="valueLen" select="string-length($tposition)"/>
    <xsl:with-param name="length" select="10"/>
    <xsl:with-param name="key" select="'0'"/>
    </xsl:call-template>
    <xsl:value-of select="$tposition"/>
			<xsl:value-of select="'|'"/>
			<xsl:value-of select="string(AccName)"/>
			<xsl:value-of select="'|'"/>
  		<xsl:value-of select="AccNo"/>
			<xsl:value-of select="'|'"/>
			<xsl:value-of select="'011'"/>
			<xsl:value-of select="'|'"/>
			<xsl:value-of select="1102"/> 
			<xsl:value-of select="'|'"/>
			
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>