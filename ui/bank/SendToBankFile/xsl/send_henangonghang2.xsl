<?xml version="1.0" encoding="gb2312"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:java="http://xml.apache.org/xslt/java"
                exclude-result-prefixes="java"
                version="1.0">

<xsl:output method="text" encoding="gbk"/>

<xsl:include href="send_function.xsl"/>

<xsl:template match="/">


	<xsl:for-each select="BANKDATA/ROW">
	     <xsl:variable name="tposition" select="position()"/>
				<xsl:value-of select="substring($tposition,1,16)"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length($tposition)"/>
				<xsl:with-param name="length" select="16"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="' '"/>
			
  		<xsl:value-of select="substring(PayCode,1, 16)"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(PayCode)"/>
				<xsl:with-param name="length" select="16"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="' '"/>
			
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(IDNo)"/>
				<xsl:with-param name="length" select="18"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
      <xsl:value-of select="substring(IDNo, 1, 18)"/>
      <xsl:value-of select="' '"/>
			 
        <xsl:value-of select="substring(AccNo, 1, 19)"/>
      	<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(AccNo)"/>
				<xsl:with-param name="length" select="19"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
      <xsl:value-of select="' '"/>
      
			<xsl:variable name="accNameValue" select="string(AccName)"/>
		  <xsl:variable name="accNameLen" select="java:com.sinosoft.lis.pubfun.XmlFun.getChinaLen($accNameValue)"/>
		  
		   <xsl:choose>
		    <xsl:when test="7 > $accNameLen">
		      <xsl:value-of select="substring(AccName, 1,14-$accNameLen)"/>
		    </xsl:when>
		    <xsl:otherwise>
		      <xsl:value-of select="substring(AccName, 1, 7)"/> 
		    </xsl:otherwise>
		  </xsl:choose>
		     <xsl:call-template name="supplement">
		        <xsl:with-param name="valueLen" select="string-length(AccName)+$accNameLen"/>
		        <xsl:with-param name="length" select="14"/>
		        <xsl:with-param name="key" select="' '"/>
		    </xsl:call-template>
			    <xsl:value-of select="' '"/>
			<xsl:call-template name="supplement">
				<xsl:with-param name="valueLen" select="string-length(PayMoney)"/>
				<xsl:with-param name="length" select="10"/>
				<xsl:with-param name="key" select="' '"/>
			</xsl:call-template>
			<xsl:value-of select="substring(PayMoney, 1,10)"/>	
			<!-- �س����� -->
			<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>

</xsl:template>

</xsl:stylesheet>