//ReadFromFileInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var filePath;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量


//提交，保存按钮对应操作
function submitForm() {
  if(verifyInput() == false) return false;
  fm.action = "./SettledConfSave.jsp";
  var accsql = "select 1 from ldfinbank where bankcode ='" + fm.getbankcode.value + "' and bankaccno='" + fm.getbankaccno.value +"'";
      var arrBankResult = easyExecSql(accsql);
      if(arrBankResult==null){
        alert("归集账户不存在，请确认录入的归集账户");
        return false ;
      }
      
  if (BankGrid.getSelNo()) { 
    fm.all("serialNo").value = BankGrid.getRowColData(BankGrid.getSelNo()-1 , 2);
    fm.all("bankCode").value = BankGrid.getRowColData(BankGrid.getSelNo()-1 , 1);
    
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}

function HZButton(){
	
	var serialno = fm.serialNo.value;
	var querySQL = "select rfb.Agentcode,"
	    +"   lag.name, "
	    +"   rfb.bankcode, "
	    +"   rfb.accno, "
	    +"  rfb.accname, "
	    +"   rfb.paycode, "
	    +"   rfb.polno, "
	    +"   case rfb.paytype "
	    +"     when '1' then "
	    +"     '首期' "
	    +"     else "
	    +"      '续期' "
	    +"   end, "
	    +"   paymoney, "
	    +"   rfb.comcode "
	    +" from lyreturnfrombank rfb "
	    +" left join laagent lag "
	    +" on rfb.agentcode = lag.agentcode "
	    +" where serialno = '"+ serialno +"' "
	    +" and banksuccflag = "
	    +"   (select substr(agentpaysuccflag, 1, length(agentpaysuccflag) - 1) "
	    +"     from ldmedicalcom "
	    +"     where rfb.bankcode = medicalcomcode) and dealtype = '"+ dealType +"' with ur";
	fm.querySql.value = querySQL;
	fm.filename.value = 'SettledHZList_';
	//提交，下载
	DownLoad(serialno);
}
function MXButton(){
	
	var serialno = fm.serialNo.value;
	var querySQL = "select lss.agentcode,"
				    +"   laa.name,"
				    +"   lss.medicalcode,"
				    +"  lss.accno,"
				    +" lss.accname,"
				    +" lss.paycode,"
				    +" lss.polno,"
				    +" case lss.paytype"
				    +"   when '1' then"
				    +"    '首期'"
				    +"   else"
				    +"    '续期'"
				    +" end,"
				    +" lss.paymoney,"
				    +" lss.comcode"
				    +" from lysendtosettle lss"
				    +" left join laagent laa"
				    +" on lss.agentcode = laa.agentcode"
				    +" where serialno = '"+serialno+"'"
				    +" and dealstate is not null with ur";
	fm.querySql.value = querySQL;
	fm.filename.value = 'SettledMX_';
	//提交，下载
	DownLoad(serialno);
}

//下载
function DownLoad(serialno) {
	
	if(serialno != null && serialno != "" )
    {
        var formAction = fm.action;
        fm.action = "MedicalListOfDSDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先选择批次，再生成清单！");
        return ;
    }
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

//获取文件上传保存路径
function getUploadPath() {
  var strSql = "select SysVarValue from LDSysVar where SysVar = 'ReturnFromBankPath'";
  
  filePath = easyExecSql(strSql);
}

//查询出本机构未返回处理的银行批次
function initQuery() {
  var strSql = "select distinct log.bankcode, log.serialno, log.makedate, log.returndate"
				+"  from lybanklog log"
				+"  left join lysendtosettle set"
				+"    on log.serialno = set.serialno"
				+" where set.dealtype = 'D'"
				+"  and set.dealstate = '1' order by log.returndate desc with ur";
  
  turnPage.queryModal(strSql, BankGrid);
}

//初始化银行代码
function initMedicalCode() {
  var strSql = "select MedicalComCode, MedicalComName from LDMedicalCom where  ComCode like '" + comCode + "%'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("MedicalCode").CodeData = strResult;
  }
}   

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句
	var strSql = "select distinct log.bankcode, log.serialno, log.makedate, log.returndate"
		+"  from lybanklog log"
		+"  left join lysendtosettle set"
		+"    on log.serialno = set.serialno"
		+" where set.dealtype = 'D'"
		+"  and set.dealstate = '1'"
		+ getWherePart('log.BankCode','MedicalCode')
		+ getWherePart('log.returndate','dealdate')
				+" order by log.returndate desc with ur";
				     

				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
}
//显示银行批次数据的统计信息，金额、件数
function showStatistics(parm1, parm2) {
//  var strSql = "select Totalmoney, Totalnum from lybanklog where SerialNo = '" 
//             + BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2) 
//             + "'";
  fm.serialNo.value = BankGrid.getRowColData(BankGrid.getSelNo() - 1, 2);
//  alert(fm.serialNo.value);
  //alert(easyExecSql(strSql));      
//  var arrResult = easyExecSql(strSql);
  
//  fm.all("TotalMoney").value = arrResult[0][0];
//  fm.all("TotalNum").value = arrResult[0][1];
  tSelNo = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
}
