<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>

	<%@page contentType="text/html;charset=GBK"%>
	<%@page import="java.util.*"%>

	<%
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
	%>
	<script>
 		 var managecom = <%=tG.ManageCom%>;
	</script> 	
	<head>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

		<SCRIPT src="BankListPrintInput.js"></SCRIPT>

		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>

		<%@include file="BankListPrintInit.jsp"%>


	</head>
	<body onload="initForm();initElementtype();">
		<form action="BankListPrintSave.jsp" method=post name=fm
			target="PRINT">

			<table>
				<tr>
					<td class="titleImg">
						系统代收付统计表
					</td>
				</tr>
			</table>
			<TABLE class="common">
				<tr class=common>
					<td class=title>
						管理机构
					</td>
					<td class=input colspan='3'>
						<Input class=codeNo name=ManageCom verify="管理机构|notnull"
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly
							elementtype="nacessary">
					</td>
				</tr>
				<tr class="common">
					<td class="title">
						开始日期
					</td>
					<td class="input">
						<input class="coolDatePicker" verify="开始日期|notnull&date"
							dateFormat="short" name="StartDate">
							<font color="#FF0000" align="Left">*</font>
					<td class="title">
						结束日期
					</td>
					<td class="input">
						<input class="coolDatePicker" dateFormat="short" name="EndDate"
							verify="结束日期|notnull&date">
						<font color="#FF0000" align="Left">*</font>
					</td>
				</tr>
			</TABLE>
			<br>
			<input value="下 载" type=button onclick="Print()" class="cssButton"
				type="button">
			<br>
			<input type="hidden" name="fmtransact" value="">
		</form>
		<span id="spanCode" style="display: none; position: absolute;"></span>
	</body>
</html>
