<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：PayReturnFromBankSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  
<%
  System.out.println("\n\n---PayReturnFromBankSave Start---");
  
  PayReturnFromBankUI payReturnFromBankUI1 = new PayReturnFromBankUI();
 String autobankaccno=request.getParameter("autobankaccno")==null?"22":request.getParameter("autobankaccno");
  String autobankaccno2=request.getParameter("autobankaccno2")==null?"22":request.getParameter("autobankaccno2");
  TransferData transferData1 = new TransferData();
  transferData1.setNameAndValue("totalMoney", request.getParameter("TotalMoney"));
  transferData1.setNameAndValue("getbankcode", request.getParameter("getbankcode"));
  transferData1.setNameAndValue("getbankaccno", request.getParameter("getbankaccno"));
    transferData1.setNameAndValue("autobankaccno", autobankaccno);//22表示没有选中，00表示选中；
   transferData1.setNameAndValue("autobankaccno2", autobankaccno2);//22表示没有选中，00表示选中；
   
    System.out.println("autobankaccno:"+autobankaccno);
     System.out.println("autobankaccno2:"+autobankaccno2);
    
  LYReturnFromBankSchema tLYReturnFromBankSchema = new LYReturnFromBankSchema();
  tLYReturnFromBankSchema.setSerialNo(request.getParameter("serialNo"));

  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
    
  VData tVData = new VData();
  tVData.add(transferData1);
  tVData.add(tLYReturnFromBankSchema);
  tVData.add(tGlobalInput);
  
  String Content = "处理成功";
  String FlagStr = "Succ";

  if (!payReturnFromBankUI1.submitData(tVData, "PAYMONEY")) {
    VData rVData = payReturnFromBankUI1.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
  	FlagStr = "Fail";
  }
 else {
  	payReturnFromBankUI1.submitData(tVData, "CLAIM");
    Content = " 处理成功! ";
  	FlagStr = "Succ";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---PayReturnFromBankSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.initQuery();
</script>
</html>
