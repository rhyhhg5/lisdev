<%
//程序名称：LDBankQueryInit.jsp
//程序功能：
//创建日期：2007-4-6 14:52
//创建人  ：qulq程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
     GlobalInput tG1 = (GlobalInput)session.getValue("GI");
     String comcode = tG1.ComCode;
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
	// 保单查询条件
		fm.all('comcode').value = '<%=comcode%>';
    fm.all('BankNo').value = '';
    fm.all('BankName').value = '';
    
  }
  catch(ex)
  {
    alert("在LDBankQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                      

function initForm()
{
  try
  {
    initInpBox();  
    initBankGrid();
  }
  catch(re)
  {
    alert("在LDBankQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 保单信息列表的初始化
function initBankGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="银行代码";         		//列名
      iArray[1][1]="40px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="银行名";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


         			//是否允许输入,1表示允许，0表示不允许      

      BankGrid = new MulLineEnter( "fm" , "BankGrid" ); 
      //这些属性必须在loadMulLine前
      BankGrid.mulLineCount = 10;   
      BankGrid.displayTitle = 1;
      BankGrid.locked = 1;
      BankGrid.canSel = 1;
      BankGrid.hiddenPlus=1;
      BankGrid.hiddenSubtraction=1;
      BankGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>