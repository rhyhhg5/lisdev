/*
  //创建人：亓莹莹
  //修改人：
  //修改内容：
  //该文件中包含客户端需要处理的函数和事件
*/
var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


//根据起始日期进行查询出要该日期范围内的批次号码
function showSerialNo()
{
  if((fm.StartDate.value=="")||(fm.EndDate.value=="")||(fm.StartDate.value==null)||(fm.EndDate.value==null))
  {
    alert("请您输入起始日期和结束日期！");
    return false;
  }
  if((fm.Flag.value=="")||(fm.Flag.value==null))
  {
    alert("请您输入收付费标志!");
    return false;
  }

  fm.TFFlag.value="T";
  //initYCSuccessListGrid();
  if(fm.Flag.value=="YS")
  {
    strFlag = 'S';
  }
  if(fm.Flag.value=="YF")
  {
    strFlag = 'F';
  }
  
  var main_sql1 = "select a.serialno,b.bankcode, sum(paymoney), count(*) from lyreturnfrombankb a,lybanklog b"
           +" where a.serialno in (select serialno from lybanklog where 1 = 1";
           
  var main_sql2 = " union all select c.serialno,b.bankcode,sum(paymoney), count(*) from lyreturnfrombank c,lybanklog b"
           +" where c.serialno in (select serialno from lybanklog where 1 = 1";
           
  main_sql1 = main_sql1+ " and StartDate >= '"+fm.StartDate.value+"'"
           +" and StartDate <= '"+fm.EndDate.value+"'"
   		   +" and LogType = '"+strFlag+"'"
   		   +" and ReturnDate is not null";
   		   
  main_sql2 = main_sql2 + " and StartDate >= '"+fm.StartDate.value+"'"
           +" and StartDate <= '"+fm.EndDate.value+"'"
           +" and LogType = '"+strFlag+"'"
           +" and ReturnDate is not null";
 
  
  if((fm.BankCode.value!="")&&(fm.BankCode.value!=null)){
  	main_sql1 = main_sql1 + " and BankCode = '"+fm.BankCode.value+"'";
  	main_sql2 = main_sql2 + " and BankCode = '"+fm.BankCode.value+"'";
  }
  main_sql1 = main_sql1 + ") and banksuccflag='0000'"
  main_sql2 = main_sql2 + ") and banksuccflag='0000'"
  if((fm.ManageCom.value!="")&&(fm.ManageCom.value!=null)){
      main_sql1 = main_sql1 + " and a.ComCode like '"+fm.ManageCom.value+"%'";
      main_sql2 = main_sql2 + " and c.ComCode like '"+fm.ManageCom.value+"%'";
  }
  main_sql1 = main_sql1 +" and a.serialno = b.serialno group by a.serialno,b.bankcode";
  main_sql2 = main_sql2	+" and c.serialno = b.serialno group by c.serialno,b.bankcode";
  var main_sql = main_sql1 + main_sql2;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  var tempQuery = turnPage2.queryModal(main_sql,YCSuccessListGrid);
  if(tempQuery==false){
  	showInfo.close();
  	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有符合条件的数据" ;
	showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  	return false;
  }
  showInfo.close();
}

//根据选中的批次号、银行编码、管理机构进行查询并且执行打印功能；
function printBill()
{
  var tRow=YCSuccessListGrid.getSelNo();
  if (tRow==0)
  {
    alert("请您先进行选择");
    return;
  }
  else
  {
    tSerialno = YCSuccessListGrid.getRowColData(tRow-1,1);
    tBankCode = YCSuccessListGrid.getRowColData(tRow-1,2);
    tSumDuePayMoney = YCSuccessListGrid.getRowColData(tRow-1,3);
    tSumCount = YCSuccessListGrid.getRowColData(tRow-1,4);
    fm.BillNo.value=tSerialno;
    fm.BankCode1.value=tBankCode;
    fm.SumDuePayMoney.value=tSumDuePayMoney;
    fm.SumCount.value=tSumCount;
    fm.action = "./YCSuccessListSave.jsp";
    fm.target="f1print";
    fm.submit();
   }
}

