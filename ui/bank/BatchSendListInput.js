/*
  //创建人：
  //修改人：
  //修改内容：
  //该文件中包含客户端需要处理的函数和事件
*/
var showInfo;
var turnPage = new turnPageClass();
var turnPage2 = new turnPageClass();

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
  else
  {
    parent.fraMain.rows = "0,0,0,0,*";
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


//根据起始日期进行查询出要该日期范围内的批次号码
function showSerialNo()
{
  if((fm.StartDate.value=="")||(fm.EndDate.value=="")||(fm.StartDate.value==null)||(fm.EndDate.value==null))
  {
    alert("请您输入起始日期和结束日期！");
    return false;
  }
  if((fm.Flag.value=="")||(fm.Flag.value==null))
  {
    alert("请您输入收付费标志!");
    return false;
  }

  if(fm.Flag.value=="S")
  {
    strFlag = 'S';
  }
  if(fm.Flag.value=="F")
  {
    strFlag = 'F';
  }
  if(fm.Flag.value=="R")
  {
    strFlag = 'R';
  }
          
  var main_sql = "select c.serialno,b.bankcode,sum(paymoney), count(*) from lyreturnfrombank c,lybanklog b"
           +" where c.serialno in (select serialno from lybanklog where 1 = 1 ";
           
  main_sql = main_sql + " and StartDate >= '"+fm.StartDate.value+"'"
           +" and StartDate <= '"+fm.EndDate.value+"'"
           +" and LogType = '"+strFlag+"'"
           +" and ReturnDate is not null and SendDate is not null ";
 
  
  if((fm.BankCode.value!="")&&(fm.BankCode.value!=null)){
    main_sql = main_sql + " and BankCode = '"+fm.BankCode.value+"')";
  } else {
    main_sql = main_sql + " and BankCode in ('7705','7706','7703','7707','7709'))";
  }
  if((fm.ManageCom.value!="")&&(fm.ManageCom.value!=null)){
      main_sql = main_sql + " and c.ComCode like '"+fm.ManageCom.value+"%'";
  }
  main_sql = main_sql +" and c.serialno = b.serialno group by c.serialno,b.bankcode";
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  var tempQuery = turnPage2.queryModal(main_sql,BatchSendListGrid);
  if(tempQuery==false){
    showInfo.close();
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + "没有符合条件的数据" ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    return false;
  }
  showInfo.close();
}

//根据选中的批次号、银行编码、管理机构进行查询并且执行打印功能；
function printBill(succflag)
{
  if(succflag == 'S'){
     fm.TFFlag.value="T";
  }else {
     fm.TFFlag.value="F";
  }
  
  var tRow=BatchSendListGrid.getSelNo();
  if (tRow==0)
  {
    alert("请您先进行选择");
    return;
  }
  else
  {
    tSerialno = BatchSendListGrid.getRowColData(tRow-1,1);
    tBankCode = BatchSendListGrid.getRowColData(tRow-1,2);
    tSumDuePayMoney = BatchSendListGrid.getRowColData(tRow-1,3);
    tSumCount = BatchSendListGrid.getRowColData(tRow-1,4);
    fm.BillNo.value=tSerialno;
    fm.BankCode1.value=tBankCode;
    
    fm.action = "./BatchSendListSave.jsp";
    fm.target="f1print";
    var sql = "select count(*),sum(paymoney) from lyreturnfrombank where serialno='"+ tSerialno +"' and comcode like '" + fm.ManageCom.value + "%'"
    if(succflag == 'S'){
    	if(tBankCode == '7707'){
    		sql = sql + " and banksuccflag='00'";
   	} else {
   		sql = sql + " and banksuccflag='0000'";
   	}
    	
       
       var arrS = easyExecSql(sql);
       tSumCount = arrS[0][0];
       tSumDuePayMoney = arrS[0][1];
       if(tSumCount == 0){
          alert("该批次不存在成功扣款");
          return;
       }
    }else {
    	
    	if(tBankCode == '7707'){
    		sql = sql + " and banksuccflag<>'00'";
   	} else {
   		sql = sql + " and banksuccflag<>'0000'";
   	}
       
       var arrF = easyExecSql(sql);
       tSumCount = arrF[0][0];
       tSumDuePayMoney = arrF[0][1];
       if(tSumCount == 0){
          alert("该批次不存在失败扣款");
          return;
       }
    }
    fm.SumDuePayMoney.value=tSumDuePayMoney;
    fm.SumCount.value=tSumCount;

    fm.submit();
   }
}

function printAll()
{
  var tRow=BatchSendListGrid.getSelNo();
  if (tRow==0)
  {
    alert("请您先进行选择");
    return;
  }
  else
  {
    tSerialno = BatchSendListGrid.getRowColData(tRow-1,1);
    tBankCode = BatchSendListGrid.getRowColData(tRow-1,2);
    tSumDuePayMoney = BatchSendListGrid.getRowColData(tRow-1,3);
    tSumCount = BatchSendListGrid.getRowColData(tRow-1,4);
    fm.BillNo.value=tSerialno;
    fm.BankCode1.value=tBankCode;
    fm.SumDuePayMoney.value=tSumDuePayMoney;
    fm.SumCount.value=tSumCount;
    fm.action = "./BatchAllListSave.jsp";
    fm.target="f1print";
    fm.submit();
   }
}


function downloadBill(succflag)
{
  if(succflag == 'S'){
     fm.TFFlag.value="T";
  }else {
     fm.TFFlag.value="F";
  }
  
  var tRow=BatchSendListGrid.getSelNo();
  if (tRow==0)
  {
    alert("请您先进行选择");
    return;
  }
  else
  {
    tSerialno = BatchSendListGrid.getRowColData(tRow-1,1);
    tBankCode = BatchSendListGrid.getRowColData(tRow-1,2);
    tSumDuePayMoney = BatchSendListGrid.getRowColData(tRow-1,3);
    tSumCount = BatchSendListGrid.getRowColData(tRow-1,4);
    fm.BillNo.value=tSerialno;
    fm.BankCode1.value=tBankCode;
    
    fm.action = "./BatchSendDownloadListSave.jsp";
    fm.target="f1print";
    var sql = "select count(*),sum(paymoney) from lyreturnfrombank where serialno='"+ tSerialno +"' and comcode like '" + fm.ManageCom.value + "%'"
    if(succflag == 'S'){
    	if(tBankCode == '7707'){
    		 sql = sql + " and  banksuccflag = '00' ";
    	} else {
    		 sql = sql + " and banksuccflag='0000' ";
    	}
    	
       var arrS = easyExecSql(sql);
       tSumCount = arrS[0][0];
       tSumDuePayMoney = arrS[0][1];
       if(tSumCount == 0){
          alert("该批次不存在成功扣款");
          return;
       }
    }else {
    	
    	if(tBankCode == '7707'){
    		sql = sql + " and banksuccflag<>'00' ";
   	} else {
   		 sql = sql + " and banksuccflag<>'0000' ";
   	}
       
       var arrF = easyExecSql(sql);
       tSumCount = arrF[0][0];
       tSumDuePayMoney = arrF[0][1];
       if(tSumCount == 0){
          alert("该批次不存在失败扣款");
          return;
       }
    }
    fm.SumDuePayMoney.value=tSumDuePayMoney;
    fm.SumCount.value=tSumCount;

    fm.submit();
   }
}

function downloadAll()
{
  var tRow=BatchSendListGrid.getSelNo();
  if (tRow==0)
  {
    alert("请您先进行选择");
    return;
  }
  else
  {
    tSerialno = BatchSendListGrid.getRowColData(tRow-1,1);
    tBankCode = BatchSendListGrid.getRowColData(tRow-1,2);
    tSumDuePayMoney = BatchSendListGrid.getRowColData(tRow-1,3);
    tSumCount = BatchSendListGrid.getRowColData(tRow-1,4);
    fm.BillNo.value=tSerialno;
    fm.BankCode1.value=tBankCode;
    fm.SumDuePayMoney.value=tSumDuePayMoney;
    fm.SumCount.value=tSumCount;
    fm.action = "./BatchAllListDownloadSave.jsp";
    fm.target="f1print";
    fm.submit();
   }
}

