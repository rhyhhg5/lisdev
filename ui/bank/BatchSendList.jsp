<html>
<%
  //程序名称：邮储代收代付成功清单
  //程序功能：
  //创建日期：2011-02-28
  //创建人  ：亓莹莹
  //更新记录：  更新人    更新日期     更新原因/内容
 %>
  <%@page contentType="text/html;charset=GBK"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="java.util.*"%> 
  <%@page import="com.sinosoft.utility.*"%>
  <%@include file="../common/jsp/UsrCheck.jsp"%>
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="BatchSendListInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BatchSendListInit.jsp"%>
</head>
<%
	
  GlobalInput tGI = new GlobalInput();
  tGI = (GlobalInput)session.getValue("GI");
%>

<SCRIPT>
  var managecom = <%=tGI.ManageCom%>;
</SCRIPT> 

<body onload="initForm();">
  <form action="./BatchSendListSave.jsp" method=post name=fm target="fraSubmit">
  <%@include file="../common/jsp/InputButton.jsp"%>
  <!-- 显示或隐藏LLReport1的信息 -->
  <Div id="divLLReport1" style="display: ''">
    <table class=common>
      <tr class=titleImg>
		 <td class=titleImg>总公司批量代收代付清单</td>
	  </tr>
	</table>
	<table class=common>
	  <tr class=common>
		 <td class=title>开始日期</td>
		 <td class=input><input class="coolDatePicker" dateFormat="short" name="StartDate"></td>
		 <td class=title>结束日期</td>
		 <td class=input><input class="coolDatePicker" dateFormat="short" name="EndDate"></td>
	  </tr>
	  <tr class=common>
		 <td class=title>收付费标记</td>
 		 <td class=input><Input class=codeno name=Flag verify="收付费标志|NOTNULL" CodeData="0|^S|代收^F|代付^R|实时代收" ondblClick="showCodeListEx('SXFlag1',[this,SXFlagName],[0,1]);" onkeyup="showCodeListKeyEx('SXFlag1',[this,SXFlagName],[0,1]);"><input class=codename name=SXFlagName readonly=true></td>
		 <td class=title>银行代码</td>
		 <td class=input><Input name=BankCode class=codeNo MAXLENGTH=10 CodeData="0|^7705|银联^7706|通联^7703|邮储联^7707|金联^7709|银联高费率渠道" ondblclick="return showCodeListEx('BankCode',[this,BankCodeName],[0,1]);" onkeyup="return showCodeListKeyEx('BankCode',[this,BankCodeName],[0,1]);" verify="银行代码|notnull&code:sendbank4ytj"><input class=codename name=BankCodeName readonly=true></td>
      </tr>
      <tr class=common>
		 <td class=title>管理机构</td>
		 <td class=input colspan='3'><Input class=codeNo name=ManageCom ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1]);"><input class=codename name=ManageComName readonly=true></td>
	  </tr>
      <tr class=common>
		 <td><input class=cssButton type=button value="查询批次号" onclick="showSerialNo()"></td>
      </tr>
      <tr></tr>
      </table>
      <div id="divButton" style="display: 'none'">
      <table >
      <tr class=common>
		 <td><input class=cssButton type=button value="打印扣款成功清单"onclick="printBill('S');"></td>
           <td><input class=cssButton type=button value="打印扣款失败清单"onclick="printBill('F');"></td>
          <td><input class=cssButton type=button value="打印汇总清单"onclick="printAll();"></td>
		 <td><input type=hidden name=BillNo><input type=hidden name=TFFlag><input type=hidden name=SumDuePayMoney><input type=hidden name=SumCount></td>
	  </tr>
	  </table>
	  </div>
    <table>
	  <tr>
		 <td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" onClick="showPage(this,divLCPol1);"></td>
		 <td class=titleImg>清单列表</td>
	 </tr>
	</table>
	<Div id="divLLReport2" style="display: ''">
		<table class=common>
		  <tr class=common>
			 <td text-align: left colSpan=1><span id="spanBatchSendListGrid"></span></td>
		  </tr>
		</table>
	<Div id="divPage" align=center style="display: 'none' ">
		<INPUT CLASS=common VALUE="首页" TYPE=button onclick="turnPage2.firstPage();">
		<INPUT CLASS=common VALUE="上一页" TYPE=button onclick="turnPage2.previousPage();">
		<INPUT CLASS=common VALUE="下一页" TYPE=button onclick="turnPage2.nextPage();">
		<INPUT CLASS=common VALUE="尾页" TYPE=button onclick="turnPage2.lastPage();">
	</Div>
	<br><br>
	<table>
      <tr class=common>
           <td><input class=cssButton type=button value="下载成功清单"onclick="downloadBill('S');">&nbsp;&nbsp;</td>
           <td><input class=cssButton type=button value="下载失败清单"onclick="downloadBill('F');">&nbsp;&nbsp;</td>
           <td><input class=cssButton type=button value="下载汇总清单"onclick="downloadAll();"></td>
      </tr>
	</table>
</Div>
		 <input type="hidden" name=SerialNo value="">
		 <input type="hidden" name=BankCode1 value="">
		 <input type="hidden" name=ToltalMoney value="">
		 <input type="hidden" name=ToltalNum value="">
 </form>
 <span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>
