<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GetReturnFromBankBackSave.jsp
//程序功能：文件返回错误回退页面
//创建日期：	2012-7-23 10:26:59
//创建人  ：zhangjl
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="java.io.*"%>
  <%@page import="java.util.*"%>
	<%@page import="org.apache.commons.fileupload.*"%>
 
<%
CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
	
  System.out.println("\n\n---GetReturnFromBankSave Start---");
  
  GetReturnFromBankBackUI tGetReturnFromBankBackUI = new GetReturnFromBankBackUI();

  TransferData transferData1 = new TransferData();
  transferData1.setNameAndValue("serialNo", request.getParameter("serialNo"));

  System.out.print("serialNo:"+transferData1.getValueByName("serialNo"));


  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
    
  VData backVData = new VData();
  backVData.add(transferData1);
  backVData.add(tGlobalInput);
  
  if (!tGetReturnFromBankBackUI.submitData(backVData, "READ")) {
    VData rVData = tGetReturnFromBankBackUI.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
  	FlagStr = "Fail";
  }
  else {
    Content = " 处理成功! ";
  	FlagStr = "Succ";
  }
  
  Content = PubFun.changForHTML(Content);
  System.out.println(Content + "\n" + FlagStr + "\n---GetReturnFromBankSave End---\n\n");
%>

<html>
<script language="javascript">	
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");	
</script>