<%
//程序名称：BankPauseFeeInput.jsp
//程序功能：暂停收费输入
//创建日期：2005-08-01 
//创建人  ：张君
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT> 
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
<SCRIPT src="BankPauseFeeInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="BankPauseFeeInit.jsp"%>
</head>
<body  onload="initForm();" >
<Form action="BankPauseFeeSave.jsp" method=post name=fm target="fraSubmit">
	<!--<%@include file="../common/jsp/OperateButton.jsp"%> -->
    <!--<%@include file="../common/jsp/InputButton.jsp"%> -->
<!--框架一：公有信息 --> 
  <Div  id= "Frame1" style= "display: ''">          
     <Table class= common border=0>
       <TR  class= common> 
          <TD  class= title>
            管理机构
          </TD>          
          <TD  class= input>
	         <Input class=codeNo name=ManageCom  ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true >  
	        </TD>             
          <TD class= title>
            交费日期
          </TD>
          <TD class= input >
            <Input class="coolDatePicker" verify="交费日期|DATE&NOTNULL" name=PayDate >
          </TD>                                  
       </TR>         
       <TR  class= common>
          <TD  class= title>
            业务员
          </TD>
          <TD class="input" width="25%">
           <!--Input class="code" name=AgentCode verify="代理人编码|code:AgentCode" ondblclick="return showCodeList('AgentCode',[this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');" onkeyup="return showCodeListKey('AgentCode', [this, AgentGroup], [0, 2], null, ManageCom, 'ManageCom');"-->     
            <Input class=common name=AgentCode >
            <input class=cssButton type="button" value="查  询" style="width:60" onclick="queryAgent()">   
          </TD> 
          <TD class=title>
          	银行代码
          </TD>
          <TD class= input>
            <Input name= BankCode CodeData="" MAXLENGTH=10 CLASS=code ondblclick="return showCodeList('bank',[this]);" onkeyup="return showCodeListKey('bank',[this]);" verify="银行代码|notnull&code:bank"><Input class="readonly" readonly tabindex=-1 name=AgentGroup  type=hidden>
          </TD>
       </TR>           	                 	 
       <TR  class= common>
          <TD  class= title>
            交费类型
          </TD>          
          <TD  class= input>
            <Input class=codeNo name=PauseFeeType verify="交费类型|code:PauseFeeType" ondblclick="return showCodeList('pausefeetype',[this,PauseFeeTypeName],[0,1]);" onkeyup="return showCodeListKey('pausefeetype',[this,PauseFeeTypeName],[0,1]);"><input class=codename name=PauseFeeTypeName readonly=true >
          </TD>        
          <TD class= title>
            是否有暂停
          </TD>
          <TD class= input>
            <Input class=codeNo name=PauseFlag  ondblclick="return showCodeList('pauseflag',[this,PauseType],[0,1]);"onkeyup="return showCodeListKey('pauseflag',[this,PauseType],[0,1]);"><Input class=codename name=PauseType readonly=true>
          </TD>
       </TR>	
    </Table>          
 </Div>  
<!--/Form-->
<!--框架一  -->     

<!--框架三：保存提交的数据 -->
<!--Form action="../finfee/TempFeeSave.jsp" method=post name=fmSave target="fraSubmit"-->

     <Table>
    	<TR>
    		<TD>
    			<input type=button class=cssButton value="查询" onclick="PauseDataQuery();">
    			<input type=button class=cssButton value="暂停收费" onclick="PausePayFee();">     
    		  <input type=button class=cssButton value="恢复收费" onclick="CancelPauseFee();">     
    		</TD>
    	</TR>
     </Table>         
	
  <!--收费分类表部分 -->
    <Table>
    	<tr>
         <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divTempFeeSave);">    	 
          <td class= titleImg>
            暂停收费信息
          </td>    	 
    	   </td>
    	</tr>
     </Table> 
  	 <Div  id= "divBill" style= "display: ''">
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					<span id="spanBillGrid" >
  					</span> 
  			  	</td>
  		  	</tr>
     </Div>
    <Div id= "divPage" align=center style= "display: '' ">
     <INPUT CLASS=cssButton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
     <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
     <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
     <INPUT CLASS=cssButton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
    </Div>     
      <!--input type =button class=cssButton value="撤销全部数据" onclick="clearFormData();"-->    
     <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
     <td class= input><input type= hidden name=fmtransact></td>
</Form>   
</body>
</html>