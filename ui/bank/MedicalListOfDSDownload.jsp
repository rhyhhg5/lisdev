<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：ProjectReportQueryDownLoad.jsp
//程序功能：项目制管理-->项目综合-->项目信息查询报表
//创建日期：2012-06-27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    String finame = request.getParameter("filename");
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = finame+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql = request.getParameter("querySql");
    querySql = querySql.replaceAll("%25","%");
    //设置表头
    String[][] tTitle = new String[1][];
    if(finame.equals("SuccessList_")){
         tTitle[0] =new String[] {"业务员编码", "业务员姓名", "医保机构编码", "医保账号", "个人帐户名", "缴费通知书号","业务编码","类别","金额","管理机构",""};
    }else{
         tTitle[0] =new String[] {"业务员编码", "业务员姓名", "医保机构编码", "医保账号", "个人帐户名", "缴费通知书号","业务编码","类别","金额","管理机构","失败原因"};
    }
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
        createexcellist.setRowColOffset(row,0);//设置偏移
    if(createexcellist.setData(querySql,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>
