<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<html> 
<%
//程序名称：PaySendToBankInput.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>   
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <SCRIPT src="SendToMedicalSInput.js"></SCRIPT>
  <%@include file="SendToMedicalSInit.jsp"%>
  
  <title>医保代收 </title>
</head>

<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
%>

<script>
  var comCode = "<%=tGlobalInput.ComCode%>";
  var cDate = "<%=PubFun.getCurrentDate()%>";
</script>

<body  onload="initForm();" >
  <form action="./SendToMedicalSSave.jsp" method=post name=fm target="fraTitle">
    <%@include file="../common/jsp/InputButton.jsp"%>
    <!-- 医保代收 fraSubmit-->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请选择医保机构，并输入代收日期区间：</td>
  		</tr>
  	</table>
  	
    <table  class= common>
    <TR  class= common>
 <TD  class= title>
       	医保机构代码
      </TD>
      <TD  class= input>
        <Input NAME=MedicalCode CodeData="" MAXLENGTH=10 CLASS=codeno ondblclick="return showCodeList('MedicalCode',[this,MedicalName],[0,1]);" onkeyup="return showCodeListKey('MedicalCode',[this,MedicalName],[0,1]);" verify="医保机构代码|notnull&code:MedicalCode" ><input class=codename name=MedicalName readonly=true >
      </TD> 
      <TD  class= title>
        起始日期
      </TD>
      <TD  class= input>
        <Input class="coolDatePicker" dateFormat="short" name=StartDate verify="起始日期|date" >
      </TD>
      <TD  class= title>
        终止日期
      </TD>
      <TD  class= input>
        <Input class="coolDatePicker" dateFormat="short" name=EndDate verify="终止日期|notnull&date" >
      </TD>
    </TR>
    </table>
	<br/>
	<Input type="hidden" name=typeFlag >
    <INPUT VALUE="提取数据" class= cssButton  TYPE=button name=sub onclick="submitForm()">
    <br><br>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBank1);">
    		</td>
    		<td class= titleImg>
    			 生成数据清单：
    		</td>
    	</tr>
    </table>
  	<Div  id= "divBank1" style= "display: ''">
      	<table  class= common>
          	<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanBankGrid" >
  					</span> 
  				</td>
  			</tr>
  		</table>
  	</div>							
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
</html>
