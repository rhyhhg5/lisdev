var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//点击查询按钮启动该方法
function easyQueryClick() {
	var CodeType = fm.CodeType.value;
	var InterfaceCode = fm.InterfaceCode.value;
	var CodeName = fm.CodeName.value;
	var Code = fm.Code.value;
	var SysName = fm.SysName.value;
	var strSql = "select CodeType, CodeTypeNmae,InterfaceCode, CodeName, Code, MakeDate, MakeTime "
  						 + "from LDCodeInterface "
  						 + "where 1=1 "
  						 //+ "and SysName = '" + SysName + "' "
  						 + getWherePart("CodeType", "CodeType")
  						 + getWherePart("Code", "Code")
  						 + getWherePart("CodeName", "CodeName")
  						 + getWherePart("InterfaceCode", "InterfaceCode");
  turnPage.queryModal(strSql, SearchGrid);
}

//返回所选类型
function returnType() {
	var arrReturn = new Array();
  var tSel = SearchGrid.getSelNo();
	
  if(tSel == 0 || tSel == null) {
  	alert("请先选择一条记录，再点击返回按钮。");
  	//top.close();	
  } else {
  	try {
  		arrReturn = getQueryResult();
  		var CodeType = arrReturn[0][0];
  		var strSql = "select CodeType, CodeTypeNmae,InterfaceCode, CodeName, Code , MakeDate, MakeTime "
  						   + "from LDCodeInterface "
  						   + "where CodeType= '"+ CodeType + "'";
  		var RS = easyExecSql(strSql);
  		if(RS == null) {
  			alert("为空的返回值。");
  			top.close();
  		}
  		top.opener.afterQuery(RS);
  	} catch(ex) {
  		alert("没有发现父窗口的返回接口，无法返回相关的数据。" + ex.message);
  	}
  	top.close();
  }
}

//返回所选结果
function returnParent() {
  var arrReturn = new Array();
	var tSel = SearchGrid.getSelNo();
	
	if( tSel == 0 || tSel == null ) {
		alert( "请先选择一条记录，再点击返回按钮。");
		//top.close();
	}
	else {
    try {	
		  arrReturn = getQueryResult();
		  if(arrReturn == null) {
  			alert("为空的返回值。");
  			top.close();
  		}
			top.opener.afterQuery( arrReturn );
		} catch(ex) {
		  alert( "没有发现父窗口的afterQuery接口。" + ex.message );
		}
		top.close();		
	}
}

//得到选择数据
function getQueryResult() {
	var arrSelected = null;
	tRow =SearchGrid.getSelNo();

	if( tRow == 0 || tRow == null ) {
	    return arrSelected;
	}
	arrSelected = new Array();	
	
	arrSelected[0] = new Array();
	arrSelected[0] = SearchGrid.getRowData(tRow-1);
	
	return arrSelected;
}