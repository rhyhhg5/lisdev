<%
//程序名称：BankPauseFeeInit.jsp
//程序功能：
//创建日期：2005-8-1
//创建人  ：张君
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   

  }
  catch(ex)
  {
    alert("在BankYSBillInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在BankYSBillInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  { 
    initInpBox();
    initSelBox();
		initBillGrid();
  }
  catch(re)
  {
    alert("BankYSBillInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 报案信息列表的初始化
var BillGrid;
function initBillGrid()
{
    var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			  //列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="通知书号码";	      //列名
      iArray[1][1]="100px";            	//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="银行代码";         	//列名
      iArray[2][1]="100px";            	//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

　　　iArray[3]=new Array();
      iArray[3][0]="银行账号";       		//列名
      iArray[3][1]="150px";            	//列宽
      iArray[3][2]=40;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="总应收金额";     		//列名
      iArray[4][1]="100px";            	//列宽
      iArray[4][2]=20;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="交费日期";     		//列名
      iArray[5][1]="100px";            	//列宽
      iArray[5][2]=20;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="暂停标记";     		//列名
      iArray[6][1]="0px";            	//列宽
      iArray[6][2]=20;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      BillGrid = new MulLineEnter( "fm" , "BillGrid" ); 
      //这些属性必须在loadMulLine前
      BillGrid.mulLineCount = 10;   
      BillGrid.displayTitle = 1;
      BillGrid.hiddenPlus=1;
      BillGrid.hiddenSubtraction=1;
      BillGrid.canSel=0;
      BillGrid.canChk=1;
      BillGrid.loadMulLine(iArray);  
      //BillGrid.detailInfo="单击显示详细信息";
      //BillGrid.detailClick=RegisterDetailClick;
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
