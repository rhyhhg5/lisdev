//ReturnFileInput.js该文件中包含客户端需要处理的函数和事件

var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;
var mDebug = "0";
var tSelNo = "";
var filePath = "";
var strBankCode ="";
//提交，保存按钮对应操作
function submitForm() {
 fm.buttonflag.value = "Yes";
 for (i=0; i<BankGrid.mulLineCount; i++) {
 if (BankGrid.getChkNo(i)) { 
    
 }
 }
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.submit(); //提交
}

function submitForm1() {
 fm.buttonflag.value = "No";
 for (i=0; i<BankGrid.mulLineCount; i++) {
 if (BankGrid.getChkNo(i)) { 
    
 }
 }
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.submit(); //提交
}

//初始化银行代码
function initBankCode() {
  var strSql = "select BankCode, BankName from LDBank where operator = 'sys' "; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("bankCode").CodeData = strResult;
  }
  
  easyQueryClick();
  easyQueryClick1();
  
}   

//代收查询按钮
function easyQueryClick() {
    // 书写SQL语句
    if(fm.all("BankCode").value == null || fm.all("BankCode").value == "") {
        alert("请录入银行代码！");
        return false;
    }
    
    var bankSQL = "";//用于区分中行、非中行
    if(fm.all("BankType").value == null || fm.all("BankType").value == ""){
        alert("请选择银行类型！");
        return false;
    } else if (fm.all("BankType").value == "1"){
        bankSQL=" and unitebankcode <> '104' ";
    } else if (fm.all("BankType").value == "2"){
        bankSQL=" and unitebankcode = '104' ";
    } else {
       alert("请选择银行错误,请选择正确的银行类型！");
       return false;
    }
    
    fm.flag.value = "1";
        var strSql = "select getnoticeno,BankCode,SumDuePayMoney,BankAccNo,AccName,ManageCom  from ljspay a where ManageCom like '" + comCode + "%%' "
                     + " and cansendbank = '8' "
                     +"  and exists ( select 1 from ldbankunite where bankunitecode='"
                     + fm.BankCode.value
                     +"' and bankcode=a.bankcode " + bankSQL + ")"
                     + " order by SerialNo desc";        
    //查询SQL，返回结果字符串
   turnPage.strQueryResult  = easyQueryVer3(strSql, 1, 1, 1);  
    //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    BankGrid.clearData('BankGrid');  
    alert("没有查询到数据！");
    return false;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BankGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  turnPage.pageLineNum=30;
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
    
}
//代付查询按钮
function easyQueryClick1()
{
    if(fm.all("BankCode").value == null || fm.all("BankCode").value == "") {
        alert("请录入银行代码！");
        return false;
    }
    fm.flag.value = "2";

    var bankSQL = "";//用于区分中行、非中行
    if(fm.all("BankType").value == null || fm.all("BankType").value == ""){
        alert("请选择银行类型！");
        return false;
    } else if (fm.all("BankType").value == "1"){
        bankSQL=" and unitebankcode <> '104' ";
    } else if (fm.all("BankType").value == "2"){
        bankSQL=" and unitebankcode = '104' ";
    } else {
       alert("请选择银行错误,请选择正确的银行类型！");
       return false;
    }

    var strSql1 = "select OtherNo,BankCode,SumGetMoney,BankAccNo,AccName,ManageCom from ljaget a where ManageCom like '"+comCode+"%%' "
          + " and cansendbank = '8' "
          +   "  and exists ( select 1 from ldbankunite where bankunitecode='"
          + fm.BankCode.value
          + "' and bankcode=a.bankcode " + bankSQL + ")"
          + " order by SerialNo desc";
    //查询SQL，返回结果字符串
   turnPage.strQueryResult  = easyQueryVer3(strSql1, 1, 1, 1);  
    //判断是否查询成功
  if (!turnPage.strQueryResult) {  
    BankGrid.clearData('BankGrid');  
    alert("没有查询到数据！");
    return false;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = BankGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSql1; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  turnPage.pageLineNum=30;
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  //turnPage.queryModal(turnPage.strQuerySql, turnPage.pageDisplayGrid);
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
}
function afterSubmit( FlagStr, content  )
{    
      showInfo.close();
      if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,
                    "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    easyQueryClick();
  }
 
}
//得到选择数据
function getQueryResult()
{
    var arrSelected = null;
    tRow = BankGrid.getSelNo();
    
    if(tRow == 0 || tRow == null)
    {
        return arrSelected;
    }
    arrSelected = new Array();
    
    arrSelected[0] = new Array();
    arrSelected[0] = BankGrid.getRowColData(tRow - 1);
    
    return arrSelected;
}
