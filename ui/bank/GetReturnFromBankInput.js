//GetReturnFromBankInput.js该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;
var mDebug="0";
var tFees;
//提交，保存按钮对应操作
function submitForm() {
  if(verifyInput() == false) return false;
  fm.action = "./GetReturnFromBankSave.jsp";
  var accsql = "select 1 from ldfinbank where bankcode ='" + fm.getbankcode.value + "' and bankaccno='" + fm.getbankaccno.value +"'";
      var arrBankResult = easyExecSql(accsql);
      if(arrBankResult==null){
        alert("归集账户不存在，请确认录入的归集账户");
        return false ;
      }
      
  if (BankGrid.getSelNo()) { 
    fm.all("serialNo").value = BankGrid.getRowColData(BankGrid.getSelNo()-1 , 1);
    fm.all("bankCode").value = BankGrid.getRowColData(BankGrid.getSelNo()-1 , 2);
    fm.all("bankUniteFlag").value = trim(BankGrid.getRowColData(BankGrid.getSelNo()-1 , 3));
    
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}

function initQuery() {
  var strSql = " (select a.SerialNo as z, a.BankCode, ' ' from LYBankLog a, LDBank b where "
             + " a.DealState is null and a.ComCode like '" + comCode + "%%' and a.LogType='S' "
             + " and a.bankcode=b.bankcode " 
             + " and (b.bankuniteflag='0' or b.bankuniteflag is null) "
             
             +" union "
             +" select a.SerialNo as z, a.BankCode, ' ' from LYBankLog a, LDBank b where "
             + " a.DealState is null and a.ComCode like '" + comCode + "%%' and a.LogType='S' "
             + " and a.bankcode=b.bankcode " 
             + " and b.bankuniteflag='1' and b.bankcode in (select code from ldcode where codetype='bankunitecode') "
             
  					 + " union " 
  					 + " select c.SerialNo as z, d.BankCode, c.bankcode from lybanklog c, lybankunitelog d where "
  					 + " c.dealstate is null and c.comcode like '" + comCode + "%%' and c.LogType='S' "
  					 + " and c.serialno=d.serialno and c.bankcode=d.bankunitecode "
  					 + " and d.dealstate is null ) order by z desc";
  
  turnPage.queryModal(strSql, BankGrid);
} 
function afterCodeSelect(cCodeName, Field) {
	if(cCodeName == "getbankcode") {
	  fm.getbankaccno.value='';
	}
	
}


//退回文件返回 按钮
function backForm() {
	
	if (BankGrid.getSelNo()) { 
		functionback();
		var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	    fm.action = "./GetReturnFromBankBackSave.jsp";
		fm.submit();
	} else {
		alert("请先选择一条批次号信息！");  
	}
}

/* 保存完成后的操作，由框架自动调用 */
function afterSubmit(FlagStr, content)
{
	showInfo.close();
	window.focus();
	if (FlagStr == "Fail")
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		
		window.focus();
		window.location.reload();
	}
}

//选中MulLine行
function functionback () //参数名可以是任意的，不限于parm1和parm2 
{ 
	var i=BankGrid.getSelNo();
	var value=BankGrid.getRowColData(i-1,1);
	fm.serialNo.value=value;
}



