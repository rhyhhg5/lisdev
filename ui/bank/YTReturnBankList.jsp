<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：BatchPayQueryList.jsp
	//程序功能：清单下载
	//创建日期：2009-06-18
	//创建人  ：yanjing
	//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
	boolean errorFlag = false;
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	
	
	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));
	
    //取得前台传入数据
	String startDate = request.getParameter("StartDate");
	String endDate = request.getParameter("EndDate");
	String flag = request.getParameter("Flag");
	String bankcode = request.getParameter("BankCode");
	System.out.println("startDate:" + startDate);
	System.out.println("endDate:" + endDate);
	System.out.println("flag:" + flag);
	System.out.println("bankcode:" + bankcode);
	
	String name="";
	String tab="";
	
	if("7705".equals(bankcode))
	{
		name="银联";
	}
	else if("7706".equals(bankcode))
	{
		name="通联";
		
	} 
	else if("7709".equals(bankcode))
	{
		name="银联高费率渠道";
		
	}
	else {
		
		name="金联";
	}
	if("S".equals(flag))
	{
		tab="代收";
	}
	else if("R".equals(flag))
	{
		tab="实时代收";
		
	}else{
	    tab="代付";
	}

	//生成文件
	CreateExcelListEx createexcellist = new CreateExcelListEx("");//指定文件名
	createexcellist.createExcelFile();
	String[] sheetName = { "list" };
	createexcellist.addSheet(sheetName);
	
	String downLoadFileName = "";
	String filePath = "" ;
	String tOutXmlPath = "";
	String querySql = "";
	
		//文件名
		downLoadFileName = ""+name + tab +"_" + tG.Operator + "_" + min + sec
				+ ".xls";
		filePath = application.getRealPath("temp");
		tOutXmlPath = filePath + File.separator + downLoadFileName;
		System.out.println("OutXmlPath:" + tOutXmlPath);

		//隐藏字段提供查询sql
		querySql = "select a.Serialno, a.Returndate, b.Comcode ,"
			      +" (Select Name From Ldcom Where Comcode = b.Comcode),"
			      +"  b.Paycode, b.Paymoney , b.Polno, b.Accno"
			  	  +" from Lybanklog a"
			  	  +" Inner Join Lyreturnfrombank b On a.Serialno = b.Serialno"
			 	  +" where ( b.Banksuccflag = '0000' or b.Banksuccflag = '00' ) "
			      +" and a.Bankcode ='"+bankcode+"' "
			      +" and a.returndate between '"+startDate+"' and '"+endDate+"' "
			      +" And a.Logtype = '"+flag+"' with ur ";
		
		System.out.println(querySql);
		
		//设置表头
		String[] tTitle = { "批次号", "回盘日期", "机构编码", "机构名称", "收付费号", "金额","业务号码","客户账号"};

		//数据的显示属性(指定对应列是否显示在清单中)
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7,8};
		if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
			errorFlag = true;
		}	
	
	if (!errorFlag) {
		//写文件到磁盘
		try {
			createexcellist.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	}
	//返回客户端
	if (!errorFlag) {
		downLoadFile(response, filePath, downLoadFileName);
	}

	out.clear();
	out = pageContext.pushBody();
%>

