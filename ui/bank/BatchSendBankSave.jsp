<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：GetSendToBankSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="com.sinosoft.lis.operfee.*"%>
  <%@page import="java.text.*"%>
  <%@page import="java.util.*"%>
  
<% 
  String Content = "";
  String FlagStr = "";
  String flag = request.getParameter("flag");
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
	if("1".equals(flag))
	{
		  //自动催收
		  System.out.println("-----------    代收执行了     -----------");
		  LCPolSchema  tLCPolSchema = new LCPolSchema();  // 个人保单表
		  String startDate = request.getParameter("StartDate");
		  System.out.println("StartDate:"+startDate);
		  if (startDate.equals("")) {
		    startDate = "1990-1-1";
		  }
		  System.out.println("StartDate:"+startDate);
		  tLCPolSchema.setGetStartDate(startDate);                        //将判断条件设置在起领日期字段中
		  tLCPolSchema.setPayEndDate(request.getParameter("EndDate"));    //将判断条件设置在终交日期字段中

	
		 //用TransferData来向后台传送数据  zhanghui 2005.2.18
		 TransferData transferData2 = new TransferData();
		 transferData2.setNameAndValue("bankCode", request.getParameter("BankCode"));

		  VData tVData2 = new VData();
		  tVData2.add(tLCPolSchema);
		  tVData2.add(tGlobalInput);
		  tVData2.add(transferData2);//by zhanghui
		  NewIndiDueFeeMultiUI tNewIndiDueFeeMultiUI = new NewIndiDueFeeMultiUI();
		  tNewIndiDueFeeMultiUI.submitData(tVData2, "INSERT");
		  
		  if (tNewIndiDueFeeMultiUI.mErrors.needDealError()) {
		    Content="催收处理失败，不能生成银行发送数据，原因是:";
		    
		    for(int n=0;n<tNewIndiDueFeeMultiUI.mErrors.getErrorCount();n++) {       
		     Content=Content+tNewIndiDueFeeMultiUI.mErrors.getError(n).errorMessage;
		     Content=Content+"|";
		     System.out.println(Content);
		    }
		    FlagStr="Fail";
		  }
		  else { 
		    System.out.println("催收处理成功！");
		  }
		  
		  //生成银行数据
		  System.out.println("\n\n---BatchSendBankSave Start---");
		  BatchSendBankUI tBatchSendBankUI = new BatchSendBankUI();

		  TransferData transferData1 = new TransferData();
		  transferData1.setNameAndValue("startDate", startDate);
		  transferData1.setNameAndValue("endDate", request.getParameter("EndDate"));
		  transferData1.setNameAndValue("bankCode", request.getParameter("BankCode"));
		  transferData1.setNameAndValue("typeFlag", request.getParameter("typeFlag"));

		  VData tVData = new VData();
		  tVData.add(transferData1);
		  tVData.add(tGlobalInput);

		  if (!tBatchSendBankUI.submitData(tVData, "GETMONEY")) {
		    VData rVData = tBatchSendBankUI.getResult();
		    Content = " 处理失败，原因是:" + (String)rVData.get(0);
		  	FlagStr = "Fail";
		  }
		  else {
		  	VData rVData = tBatchSendBankUI.getResult();
		    Content = "送银行数据处理成功! ";
		  	FlagStr = "Succ";
		  }  

			System.out.println(Content + "\n" + FlagStr + "\n---SendToBankSave End---\n\n");
	}
	else if("2".equals(flag))
	{
		
		System.out.println("-----------    代付执行了     -----------");
		  
		  //生成银行数据
		  System.out.println("\n\n---BatchSendBankSave Start---");
		  BatchSendBankFUI tBatchSendBankFUI = new BatchSendBankFUI();

		  TransferData transferData1 = new TransferData();
		  transferData1.setNameAndValue("startDate", request.getParameter("StartDate"));
		  transferData1.setNameAndValue("endDate", request.getParameter("EndDate"));
		  transferData1.setNameAndValue("bankCode", request.getParameter("BankCode"));

		  VData tVData = new VData();
		  tVData.add(transferData1);
		  tVData.add(tGlobalInput);

		  if (!tBatchSendBankFUI.submitData(tVData, "PAYMONEY")) {
		    VData rVData = tBatchSendBankFUI.getResult();
		    Content = " 处理失败，原因是:" + (String)rVData.get(0);
		  	FlagStr = "Fail";
		  }
		  else {
		  	VData rVData = tBatchSendBankFUI.getResult();
		    Content = "送银行数据处理成功! "+ (String)rVData.get(0);
		  	FlagStr = "Succ";
		  }  

			System.out.println(Content + "\n" + FlagStr + "\n---BatchSendBankSave End---\n\n");
	}
  
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.fm.all("sub").disabled=false;
</script>
</html>
