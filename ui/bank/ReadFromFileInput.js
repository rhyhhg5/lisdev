//ReadFromFileInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var filePath;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

//提交，保存按钮对应操作
function submitForm() {
  //if(verifyInput() == false) return false;  

  if (BankGrid.getSelNo()) {     
   if (!(fm.all('FileName').value==""||fm.all('FileName').value==null))
   {
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
    var sql = ""
    
  	if(dealType == "S"){
  		sql = "select 1 from ldbank where bankcode = (select bankcode from lybanklog where serialno='" 
    		+ BankGrid.getRowColData(BankGrid.getSelNo()-1, 2) 
    		+ "') and agentpaysendf like '%.xml'"
  	} else {
  		sql = "select 1 from ldbank where bankcode = (select bankcode from lybanklog where serialno='" 
    		+ BankGrid.getRowColData(BankGrid.getSelNo()-1, 2) 
    		+ "') and agentgetsendf like '%.xml'"
  	}
  	
  	var arrResult = easyExecSql(sql);
    
    if(arrResult){
    	fm.action = "./ReadFromExcelSave.jsp" + "?filePath=" + filePath
    					+ "&serialno=" + BankGrid.getRowColData(BankGrid.getSelNo()-1, 2);
    } else {
    
    fm.action = "./ReadFromFileSave.jsp" + "?filePath=" + filePath
    					+ "&serialno=" + BankGrid.getRowColData(BankGrid.getSelNo()-1, 2)
              + "&bankCode=" + BankGrid.getRowColData(BankGrid.getSelNo()-1, 1)
              + "&bankUniteFlag=";
    }
    
              
    fm.submit(); //提交
   }
   else
   {
   	alert("请输入要读取的文件名称！");
    }
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}

//总汇文件提交，保存按钮对应操作
function submitFormHZ() {
  //if(verifyInput() == false) return false;  
  
  if (BankGrid.getSelNo()) {     
   if (!(fm.all('FileName').value==""||fm.all('FileName').value==null))
   {
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

    fm.action = "./ReadFromFileSave.jsp" + "?filePath=" + filePath 
    					+ "&serialno=" + BankGrid.getRowColData(BankGrid.getSelNo()-1, 2)
              + "&bankCode=" + BankGrid.getRowColData(BankGrid.getSelNo()-1, 1)
              + "&bankInUniteCode=" + fm.all("BankInUniteCode").value
              + "&bankUniteFlag=HZ";
  
    fm.submit(); //提交
   }
   else
   {
   	alert("请输入要读取的文件名称！");
    }
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}

//明细文件提交，保存按钮对应操作
function submitFormMX() {
  //if(verifyInput() == false) return false;  
  
  if (BankGrid.getSelNo()) {     
   if (!(fm.all('FileName').value==""||fm.all('FileName').value==null))
   {
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    
    fm.action = "./ReadFromFileSave.jsp" + "?filePath=" + filePath 
    					+ "&serialno=" + BankGrid.getRowColData(BankGrid.getSelNo()-1, 2)
              + "&bankCode=" + BankGrid.getRowColData(BankGrid.getSelNo()-1, 1)
              + "&bankInUniteCode=" + fm.all("BankInUniteCode").value
              + "&bankUniteFlag=MX";;
    
    fm.submit(); //提交

   }
   else
   {
   	alert("请输入要读取的文件名称！");
    }
  }
  else {
    alert("请先选择一条批次号信息！"); 
  }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

//获取文件上传保存路径
function getUploadPath() {
  var strSql = "select SysVarValue from LDSysVar where SysVar = 'ReturnFromBankPath'";
  
  filePath = easyExecSql(strSql);
}

//查询出本机构未返回处理的银行批次
function initQuery() {
  var strSql = "select BankCode, Serialno, Outfile, StartDate from LYBankLog where Infile is null and ComCode like '" + comCode + "%%'"
             + " and LogType='" + dealType + "'" + " order by SerialNo desc";
  
  turnPage.queryModal(strSql, BankGrid);
}

//初始化银行代码
function initBankCode() {
  var strSql = "select BankCode, BankName from LDBank where ComCode = '" + comCode + "'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("bankCode").CodeData = strResult;
  }
}   

// 查询按钮
function easyQueryClick() {
	// 书写SQL语句
	var strSql = "select BankCode, Serialno, Outfile, StartDate from LYBankLog where InFile is null and ComCode like '" + comCode + "%%'"
	           + " and LogType='" + dealType + "'"
				     + getWherePart('BankCode')
				     + getWherePart('SendDate')
				     + " order by SerialNo desc";
				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
}

