//该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo = window;
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}

//添加代码对应关系定义
function submitForm() {
	
	if(!verifyInput2()) {
    return false;
  }
  
  if(!verifyData()) {
  	alert("至少要输入一行LAS中代码而且每一行的“LAS中代码”和“对应代码”都不能为空。");
  	return false;
  }
  
  if(!checkData()) {
  	alert("该数据已经存在，请点击 修改 按钮保存修改的数据信息。");
  	return false;
  }
  
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.fmtransact.value = "INSERT||MAIN";
  fm.submit();
}

//删除代码对应关系定义
function deleteClick() {
	if (confirm("您确实想删除该记录吗?")) {
	  if(!verifyInput2()) {
      return false;
    }
  
    if(!verifyData()) {
  	  alert("至少要输入一行LAS中代码,而且每一行的“LAS中代码”和“对应代码”都不能为空。");
  	  return false;
    }
  
    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
    fm.fmtransact.value = "DELETE||MAIN";
    fm.submit();
  }
}

//更改代码对应关系
function updateClick() {
	if (confirm("您确实想更改该记录吗?")) {
    if(!verifyData()) {
  	  alert("至少要输入一行LAS中代码而且每一行的“LAS中代码”和“对应代码”都不能为空。");
  	  return false;
    }
    if (checkData()) {
  	  alert("这条数据尚未在数据库中，不能进行修改，请检查代码类型与代码类型名称。");
  	  return false;
    }
    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    fm.fmtransact.value = "UPDATE||MAIN";
    fm.submit();
  }
}

//查询代码对应关系
function queryClick() {
	showInfo=window.open("./LDCodeInterfaceQueryFrame.jsp");
}

//得到查询界面返回的查询结果
function afterQuery( arrQueryResult ) {	
	resetForm();
	if( arrQueryResult != null ) {
    fm.all('CodeType').value = arrQueryResult[0][0];
    fm.all('CodeTypeNmae').value = arrQueryResult[0][1];
    var rowNum = arrQueryResult.length;
	  var row = 0;
	  while(row < rowNum) {
		  CodeGrid.addOne();
		  CodeGrid.setRowColData(row,1,arrQueryResult[row][2]);
		  CodeGrid.setRowColData(row,2,arrQueryResult[row][3]);
		  CodeGrid.setRowColData(row,3,arrQueryResult[row][4]);
		  CodeGrid.setRowColData(row,4,arrQueryResult[row][5]);
		  CodeGrid.setRowColData(row,5,arrQueryResult[row][6]);
		  row++;
	  }
	} else {
		//alert("返回数据错误！");
	}
}

//重置表单
function resetForm() {
	fm.CodeType.value = "";
  fm.CodeTypeNmae.value = "";
  CodeGrid.clearData(); 
}      

//检察信息是否存在
function checkData() {
	
	var rowNum=CodeGrid.mulLineCount;	
	var CodeType = fm.CodeType.value;
	var SysName = fm.SysName.value;
	var row = 0;
	
	while (row < rowNum) {
		var result;
		var Code = CodeGrid.getRowColData(row, 3);
		var strSql = "select * " + 
               "from LDCodeInterface " +
               "where CodeType = '" + CodeType + 
               "' and Code = '" + Code + 
               "' and SysName = '" + SysName + "'";
    var arr = easyExecSql(strSql);
  
    if (arr) {
  	  return false;
    } else {
    	row++;
    	result = true;
    }
	}
  return result;
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  showInfo.close();
  resetForm();
  if (FlagStr == "Fail" ) {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//效验两个数据是否为空
function verifyData() {
	
	var rowNum=CodeGrid.mulLineCount;
	if(rowNum<1) {
		return false;
	}
	while (rowNum > 0) {
		//alert("总行数"+rowNum);
		var row = rowNum - 1;
		var interfaceCode = CodeGrid.getRowColData(row, 1);
		var code = CodeGrid.getRowColData(row, 3);
		
    if(interfaceCode == null || interfaceCode == "") {
    	//alert(interfaceCode + " 1a");
  	  return false;
    }
    if(code == null || code == "") {
    	//alert(code + " 1a");
  	  return false;
    }
    rowNum --;
  }
  return true;
}