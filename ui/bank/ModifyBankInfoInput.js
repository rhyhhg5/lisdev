//ModifyBankInfoInput.js该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

var showInfo;

//提交，保存按钮对应操作
function submitForm() {
  if (verifyInput() == false) return false;  
  if (fm.BankCode.value == "0101") {
    if (trim(fm.AccNo.value).length!=19 || !isInteger(trim(fm.AccNo.value))) {
      alert("工商银行的账号必须是19位的数字，最后一个星号（*）不要！");
      return false;
    }
  }
     
  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ) {
  try { showInfo.close(); } catch(e) {}
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");   
}   

// 查询按钮
function easyQueryClick() {
  if (fm.PrtNo.value == "") {
    alert("请先输入印刷号");
    return; 
  }
  
	// 书写SQL语句			     
	var strSql = "select tempfeeno, paymoney, bankcode, accname, bankaccno, case (select bankonthewayflag from ljspay where getnoticeno=tempfeeno) when '1' then '在途' else '不在途' end from ljtempfeeclass where EnterAccDate is null and tempfeeno in "
          	 + " (select tempfeeno from ljtempfee where otherno='" + fm.PrtNo.value + "')" ;
				     
  //alert(strSql);
	turnPage.queryModal(strSql, BankGrid);
	fm.PrtNo2.value = fm.PrtNo.value;
}

