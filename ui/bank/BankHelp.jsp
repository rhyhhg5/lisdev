<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：GetAdvanceDateInput.jsp
//程序功能：清单下载
//创建日期：2011-11-24
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
  boolean errorFlag = false;
  
  //获得session中的人员信息
  GlobalInput tG = (GlobalInput)session.getValue("GI");
  
  //生成文件名
  Calendar cal = new GregorianCalendar();
  String downLoadFileName = "支持银行清单.xls";
  String filePath = application.getRealPath("temp");
  String tOutXmlPath = filePath +File.separator+ downLoadFileName;
  System.out.println("OutXmlPath:" + tOutXmlPath);
  
  ExeSQL tExeSQL = new ExeSQL();  
  String ySql = "select code1,codename,case othersign when '1' then '支持代收' when '2' then '支持代付' when '3' then '支持代收及代付' end from LDCode1 where CodeType='BatchBank' and code = '7705' order by code1";
  SSRS ySSRS = tExeSQL.execSQL(ySql);
  
  String tSql = "select code1,codename,case othersign when '1' then '支持代收' when '2' then '支持代付' when '3' then '支持代收及代付' end from LDCode1 where CodeType='BatchBank' and code = '7706' order by code1";
  SSRS tSSRS = tExeSQL.execSQL(tSql);
  
  String jSql = "select code1,codename,case othersign when '1' then '支持代收' when '2' then '支持代付' when '3' then '支持代收及代付' end from LDCode1 where CodeType='BatchBank' and code = '7707' order by code1";
  SSRS jSSRS = tExeSQL.execSQL(jSql);
  
  String[][] mToExcel = new String[1000][20];
        mToExcel[0][0] = "集中代收付支持银行清单";
        mToExcel[2][0] = "银联(7705)";
        mToExcel[3][0] = "银行代码";
        mToExcel[3][1] = "银行名称";
        mToExcel[3][2] = "收付费支持";
        
        for(int row = 1; row <= ySSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= ySSRS.getMaxCol(); col++)
            {
                mToExcel[row + 3][col -1] = ySSRS.GetText(row, col);
            }
        }
        
        mToExcel[2][4] = "通联(7706)";
        mToExcel[3][4] = "银行代码";
        mToExcel[3][5] = "银行名称";
        mToExcel[3][6] = "收付费支持";
        
        for(int row = 1; row <= tSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= tSSRS.getMaxCol(); col++)
            {
                mToExcel[row + 3][col + 3] = tSSRS.GetText(row, col);
            }
        }
        
        mToExcel[2][8] = "金联(7707)";
        mToExcel[3][8] = "银行代码";
        mToExcel[3][9] = "银行名称";
        mToExcel[3][10] = "收付费支持";
        
        for(int row = 1; row <= jSSRS.getMaxRow(); row++)
        {
            for(int col = 1; col <= jSSRS.getMaxCol(); col++)
            {
                mToExcel[row + 3][col + 7] = jSSRS.GetText(row, col);
            }
        }
        
        
 
        try
        {
            WriteToExcel t = new WriteToExcel("");
            t.createExcelFile();
            String[] sheetName ={PubFun.getCurrentDate()};
            t.addSheet(sheetName);
            t.setData(0, mToExcel);
            t.write(tOutXmlPath);
        }
        catch(Exception ex)
        {
            ex.toString();
            ex.printStackTrace();
        }
  //返回客户端
  if(!errorFlag)
    downLoadFile(response,filePath,downLoadFileName);
  out.clear();
    out = pageContext.pushBody();
%>