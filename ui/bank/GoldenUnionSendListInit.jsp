<%
//程序名称：BatchSendListInit.jsp
//程序功能：
//创建日期：2011-3-1
//创建人  ：亓莹莹
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
    
 %>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<script language="JavaScript">
//单击时查询
function RegisterDetailClick(cObj)
{
    var ex,ey;
    ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
    ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
    divDetailInfo.style.left=ex;
    divDetailInfo.style.top =ey;
    divDetailInfo.style.display ='';
}


// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
    fm.all('ManageCom').value = managecom;
    var sql = "select current date - 10 days,current date from dual ";
    var rs = easyExecSql(sql);
    fm.FStartDate.value = rs[0][0];
    fm.FEndDate.value = rs[0][1];
    fm.HStartDate.value = rs[0][0];
    fm.HEndDate.value = rs[0][1];
    showAllCodeName();
  }
  catch(ex)
  {
    alert("在GoldenUnionSendList.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initSelBox();    
    initBatchSendListGrid();
  }
  catch(re)
  {
    alert("GoldenUnionSendList.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 报案信息列表的初始化
function initBatchSendListGrid()
{
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";                    //列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";                  //列宽
      iArray[0][2]=10;                      //列最大值
      iArray[0][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="批次号";                 //列名
      iArray[1][1]="80px";                 //列宽
      iArray[1][2]=40;                     //列最大值
      iArray[1][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="付费号";                 //列名
      iArray[2][1]="60px";                 //列宽
      iArray[2][2]=30;                     //列最大值
      iArray[2][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="机构";                  //列名
      iArray[3][1]="50px";                 //列宽
      iArray[3][2]=150;                     //列最大值
      iArray[3][3]=0;                       //是否允许输入,1表示允许，0表示不允许

　　　 iArray[4]=new Array();
      iArray[4][0]="发盘时间";                   //列名
      iArray[4][1]="50px";                 //列宽
      iArray[4][2]=100;                     //列最大值
      iArray[4][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="户名";                   //列名
      iArray[5][1]="40px";                 //列宽
      iArray[5][2]=60;                      //列最大值
      iArray[5][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="账号";                   //列名
      iArray[6][1]="80px";                 //列宽
      iArray[6][2]=60;                      //列最大值
      iArray[6][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="金额";                   //列名
      iArray[7][1]="40px";                 //列宽
      iArray[7][2]=60;                      //列最大值
      iArray[7][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="处理状态";                   //列名
      iArray[8][1]="40px";                 //列宽
      iArray[8][2]=40;                      //列最大值
      iArray[8][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="回盘次数";                   //列名
      iArray[9][1]="40px";                 //列宽
      iArray[9][2]=10;                      //列最大值
      iArray[9][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="回盘时间";                   //列名
      iArray[10][1]="50px";                 //列宽
      iArray[10][2]=60;                      //列最大值
      iArray[10][3]=0;                       //是否允许输入,1表示允许，0表示不允许
      
      iArray[11]=new Array();
      iArray[11][0]="失败原因";                   //列名
      iArray[11][1]="40px";                 //列宽
      iArray[11][2]=80;                      //列最大值
      iArray[11][3]=0;                       //是否允许输入,1表示允许，0表示不允许

      BatchSendListGrid = new MulLineEnter( "fm" , "BatchSendListGrid" ); 
      //这些属性必须在loadMulLine前
      BatchSendListGrid.mulLineCount = 0;   
      BatchSendListGrid.displayTitle = 1;
      BatchSendListGrid.locked = 1;
      BatchSendListGrid.loadMulLine(iArray);  
      BatchSendListGrid.detailInfo="单击显示详细信息";
      BatchSendListGrid.detailClick=RegisterDetailClick;
      
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>