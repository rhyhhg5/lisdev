<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：BatchPayQueryList.jsp
	//程序功能：清单下载
	//创建日期：2009-06-18
	//创建人  ：yanjing
	//更新记录：更新人    更新日期     更新原因/内容
%>

<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
	boolean errorFlag = false;
	//获得session中的人员信息
	GlobalInput tG = (GlobalInput) session.getValue("GI");
	
	
	Calendar cal = new GregorianCalendar();
	String min = String.valueOf(cal.get(Calendar.MINUTE));
	String sec = String.valueOf(cal.get(Calendar.SECOND));
	

	String startDate = request.getParameter("StartDate");
	String endDate = request.getParameter("EndDate");
	
	
	//生成文件
	CreateExcelListEx createexcellist = new CreateExcelListEx("");//指定文件名
	createexcellist.createExcelFile();
	String[] sheetName = { "list" };
	createexcellist.addSheet(sheetName);
	String downLoadFileName = "";
	String filePath = "" ;
	String tOutXmlPath = "";
	String querySql = "";
	
	//根据不同type生成相应的报表
	String typename = request.getParameter("type");
	if("DS".equals(typename)){
		//邮储代收
		downLoadFileName = "邮储代收_" + tG.Operator + "_" + min + sec
				+ ".xls";
		filePath = application.getRealPath("temp");
		tOutXmlPath = filePath + File.separator + downLoadFileName;
		System.out.println("OutXmlPath:" + tOutXmlPath);

		//隐藏字段提供查询sql
		querySql = "select Trim(Char(Year(d.returndate))),"
			      +" Trim(Char(Month(d.returndate))),"
			      +" Trim(Char(day(d.returndate))),"
			      +" c.comcode,"
			      +" (select name from ldcom where comcode = c.comcode),"
			      +" count(1),sum(paymoney) "
			  	  +" from lyreturnfrombank c, lybanklog d "
			 	  +" where d.bankcode = '7703' "
			      +" and d.logtype = 'S' "
			      +" and c.banksuccflag = '0000' "
			      +" and c.serialno = d.serialno "
			      +" and d.returndate between '"+startDate+"' and '"+endDate+"' "
			      +" group by d.returndate, c.comcode "
			      +" order by d.returndate with ur ";
		
		System.out.println(querySql);
		//这个在获取js中查询的sql的时候使用
		//querySql = querySql.replaceAll("%25", "%");
		
		//设置表头
		String[] tTitle = { "年", "月", "日", "机构代码", "机构名称", "成功笔数","成功金额"};

		//数据的显示属性(指定对应列是否显示在清单中)
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7};
		if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
			errorFlag = true;
		}
		
	}else if("DF".equals(typename)){
		//邮储代付
		downLoadFileName = "邮储代付_" + tG.Operator + "_" + min + sec
				+ ".xls";
		filePath = application.getRealPath("temp");
		tOutXmlPath = filePath + File.separator + downLoadFileName;
		System.out.println("OutXmlPath:" + tOutXmlPath);

		//隐藏字段提供查询sql
		querySql = "select Trim(Char(Year(d.returndate))),"
			      +" Trim(Char(Month(d.returndate))),"
			      +" Trim(Char(day(d.returndate))),"
			      +" c.comcode,"
			      +" (select name from ldcom where comcode = c.comcode),"
			      +" count(1),sum(paymoney) "
			  	  +" from lyreturnfrombank c, lybanklog d "
			 	  +" where d.bankcode = '7703' "
			      +" and d.logtype = 'F' "
			      +" and c.banksuccflag = '0000' "
			      +" and c.serialno = d.serialno "
			      +" and d.returndate between '"+startDate+"' and '"+endDate+"' "
			      +" group by d.returndate, c.comcode "
			      +" order by d.returndate with ur ";
		
		System.out.println(querySql);
		String[] tTitle = { "年", "月", "日", "机构代码", "机构名称", "成功笔数","成功金额"};
		int[] displayData = { 1, 2, 3, 4, 5, 6, 7};
		if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
			errorFlag = true;
		}
		
	}else if("DSC".equals(typename)){
		//邮储代收合计
		downLoadFileName = "邮储代收合计_" + tG.Operator + "_" + min + sec
				+ ".xls";
		filePath = application.getRealPath("temp");
		tOutXmlPath = filePath + File.separator + downLoadFileName;
		System.out.println("OutXmlPath:" + tOutXmlPath);

		//隐藏字段提供查询sql
		querySql = "select c.comcode,"
			      +" (select name from ldcom where comcode = c.comcode),"
			      +" count(1),sum(paymoney) "
			  	  +" from lyreturnfrombank c, lybanklog d "
			 	  +" where d.bankcode = '7703' "
			      +" and d.logtype = 'S' "
			      +" and c.banksuccflag = '0000' "
			      +" and c.serialno = d.serialno "
			      +" and d.returndate between '"+startDate+"' and '"+endDate+"' "
			      +" group by c.comcode with ur ";
		
		System.out.println(querySql);


		String[] tTitle = {"机构代码", "机构名称", "成功笔数","成功金额"};
		int[] displayData = { 1, 2, 3, 4};
		if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
			errorFlag = true;
		}
		
	}else if("DFC".equals(typename)){
		//邮储代付合计
		downLoadFileName = "邮储代付合计_" + tG.Operator + "_" + min + sec
				+ ".xls";
		filePath = application.getRealPath("temp");
		tOutXmlPath = filePath + File.separator + downLoadFileName;
		System.out.println("OutXmlPath:" + tOutXmlPath);

		//隐藏字段提供查询sql
		querySql = "select c.comcode,"
			      +" (select name from ldcom where comcode = c.comcode),"
			      +" count(1),sum(paymoney) "
			  	  +" from lyreturnfrombank c, lybanklog d "
			 	  +" where d.bankcode = '7703' "
			      +" and d.logtype = 'F' "
			      +" and c.banksuccflag = '0000' "
			      +" and c.serialno = d.serialno "
			      +" and d.returndate between '"+startDate+"' and '"+endDate+"' "
			      +" group by c.comcode with ur ";
		
		System.out.println(querySql);
	
		String[] tTitle = { "机构代码", "机构名称", "成功笔数","成功金额"};
		int[] displayData = { 1, 2, 3, 4};
		if (createexcellist.setDataAddNo(tTitle, querySql, displayData) == -1) {
			errorFlag = true;
		}
	}
	
	if (!errorFlag) {
		//写文件到磁盘
		try {
			createexcellist.write(tOutXmlPath);
		} catch (Exception e) {
			errorFlag = true;
			System.out.println(e);
		}
	}
	//返回客户端
	if (!errorFlag) {
		downLoadFile(response, filePath, downLoadFileName);
	}

	out.clear();
	out = pageContext.pushBody();
%>

