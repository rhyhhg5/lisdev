<%
//程序名称：WriteToFileInit.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox() {
  fm.all('fmtransact').value = "";
  fm.all('DealType').value=dealType;
} 

function initForm() {
  try {

  	initInpBox();
    
  	initBankGrid();
  	
  	initQuery();
  	
  	initMedicalCode();
  	
  	initQuery1Grid();
  }
  catch(re) {
    alert("InitForm 函数中发生异常:初始化界面错误!");
  }
}

// 领取项信息列表的初始化
var BankGrid;
function initBankGrid() {                               
  var iArray = new Array();
    
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
    iArray[0][1]="30px";            	//列宽
    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="医保机构编码";         		//列名
    iArray[1][1]="60px";            	//列宽
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[2]=new Array();
    iArray[2][0]="批次号";         		//列名
    iArray[2][1]="110px";            	//列宽
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    iArray[3]=new Array();
    iArray[3][0]="批次生成日期";         		//列名
    iArray[3][1]="60px";            	//列宽
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

    
    iArray[4]=new Array();
    iArray[4][0]="回盘日期";         		//列名
    iArray[4][1]="60px";            	//列宽
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

   /*  iArray[5]=new Array();
    iArray[5][0]="处理结果";         		//列名
    iArray[5][1]="60px";            	//列宽
    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="失败原因";         		//列名
    iArray[6][1]="100px";            	//列宽
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许 */
    
    BankGrid = new MulLineEnter( "fm" , "BankGrid" ); 
    //这些属性必须在loadMulLine前
    BankGrid.mulLineCount = 0;   
    BankGrid.displayTitle = 1;
    BankGrid.hiddenPlus = 1;
    BankGrid.hiddenSubtraction = 1;
    BankGrid.canSel = 1;
    BankGrid.canChk = 0;
    BankGrid.selBoxEventFuncName = "showStatistics";
    BankGrid.loadMulLine(iArray);  
    
    //这些操作必须在loadMulLine后面
    //BankGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex.message);
  }
}

function initQuery1Grid() {                               
	  var iArray = new Array();
	  try {
	    iArray[0]=new Array();
	    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
	    iArray[0][1]="30px";                   	//列宽
	    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

	    iArray[1]=new Array();
	    iArray[1][0]="业务员编码";         		//列名
	    iArray[1][1]="0px";            	//列宽
	    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[2]=new Array();
	    iArray[2][0]="业务员姓名";         		//列名
	    iArray[2][1]="0px";            	//列宽
	    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    iArray[3]=new Array();
	    iArray[3][0]="医保机构编码";         		//列名
	    iArray[3][1]="60px";            	//列宽
	    iArray[3][3]=0; 

	    iArray[4]=new Array();
	    iArray[4][0]="医保账户";         		//列名
	    iArray[4][1]="40px";            	//列宽
	    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	    
	    
	    iArray[5]=new Array();
	    iArray[5][0]="个人账户名";         		
	    iArray[5][1]="40px";            	
	    iArray[5][3]=0;
	    
	    iArray[6]=new Array();
	    iArray[6][0]="缴费通知书号";         		
	    iArray[6][1]="40px";            	
	    iArray[6][3]=0;
	    
	    iArray[7]=new Array();
	    iArray[7][0]="业务编码";         		
	    iArray[7][1]="40px";            	
	    iArray[7][3]=0;
	    
	    iArray[8]=new Array();
	    iArray[8][0]="类别";         		
	    iArray[8][1]="40px";            	
	    iArray[8][3]=0;
	    
	    iArray[9]=new Array();
	    iArray[9][0]="金额";         		
	    iArray[9][1]="40px";            	
	    iArray[9][3]=0;
	    
	    iArray[10]=new Array();
	    iArray[10][0]="回执回销日期";         		
	    iArray[10][1]="40px";            	
	    iArray[10][3]=0;
	    
	    iArray[11]=new Array();
	    iArray[11][0]="管理机构";         		
	    iArray[11][1]="40px";            	
	    iArray[11][3]=0;
	   
	    
	    QueryGrid1 = new MulLineEnter( "fm" , "Query1Grid" ); 
	    
	    QueryGrid1.mulLineCount = 0;   
	    QueryGrid1.displayTitle = 1;
	    QueryGrid1.hiddenPlus = 1;
	    QueryGrid1.hiddenSubtraction = 1;
	    QueryGrid1.canSel = 0;
	    QueryGrid1.locked = 1;
	    QueryGrid1.loadMulLine(iArray);
	    
	  }
	  catch(ex) {
	    alert(ex.message);
	  }
	}	

</script>

	
