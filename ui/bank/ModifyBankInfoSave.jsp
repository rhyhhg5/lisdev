<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ModifyBankInfoSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  
<%
  System.out.println("\n\n---ModifyBankInfoSave Start---");
  System.out.println("PrtNo2:" + request.getParameter("PrtNo2"));
  System.out.println("BankCode:" + request.getParameter("BankCode"));
  System.out.println("AccName:" + request.getParameter("AccName"));
  System.out.println("AccNo:" + request.getParameter("AccNo"));

  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  
  LCContSchema tLCContSchema = new LCContSchema();
  tLCContSchema.setPrtNo(request.getParameter("PrtNo2"));
  tLCContSchema.setBankCode(request.getParameter("BankCode"));
  tLCContSchema.setBankAccNo(request.getParameter("AccNo"));
  tLCContSchema.setAccName(request.getParameter("AccName"));
  LCContSet inLCContSet = new LCContSet();
  inLCContSet.add(tLCContSchema);

  VData inVData = new VData();
  inVData.add(inLCContSet);
  inVData.add(tGlobalInput);
  
  String Content = "";
  String FlagStr = "";
  
  ModifyBankInfoUI ModifyBankInfoUI1 = new ModifyBankInfoUI();

  if (!ModifyBankInfoUI1.submitData(inVData, "INSERT")) {
    VData rVData = ModifyBankInfoUI1.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
  	FlagStr = "Fail";
  }
  else {
    Content = " 处理成功! ";
  	FlagStr = "Succ";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---ModifyBankInfoSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
	parent.fraInterface.easyQueryClick();
</script>
</html>
