//GetSendToBankInput.js该文件中包含客户端需要处理的函数和事件

var showInfo;
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var turnPage1 = new turnPageClass();          //使用翻页功能，必须建立为全局变量
//提交，保存按钮对应操作
function submitForm() {
  
  if(checkMedicalCode() == false) return false;
  if(verifyInput() == false) return false;
  fm.all("typeFlag").value = "";
  fm.all("sub").disabled=true;
  fm.submit();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,Manage ,SerialNo ) {
  try { showInfo.close(); } catch(e) {}
  
  if(Manage != ""){
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Manage ;  
	  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px");  
  }
  
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:300px"); 
  
  //显示提取的清单
  if (SerialNo != ""){
	  showList(SerialNo);
  } else {
	  BankGrid.clearData("BankGrid");
  }
} 

//初始化银行代码
function initMedicalCode() {
  var strSql = "select MedicalComCode, MedicalComName from LDMedicalCom where ComCode = '" + comCode + "'"; 
  
  var strResult = easyQueryVer3(strSql);
  //alert(strResult);

  if (strResult) {
    fm.all("MedicalCode").CodeData = strResult;
  }
}   


//显示提取的清单
function showList(serialno) {
	// 书写SQL语句
	var strSql = "select paycode, polno,getpoldate, medicalcode, paymoney,accno,accname,comcode,serialno from lysendtoconfirm where serialno='" + serialno + "'";
				     
	turnPage.queryModal(strSql, BankGrid);
}

function checkMedicalCode(){
 var MedicalCode = fm.all("MedicalCode").value;
 var strSql ="select MedicalComCode,MedicalComName from LDMedicalCom where (AgentPaySendF is  null or AgentPayReceiveF is null or AgentPaySuccFlag is  null) and MedicalComCode = '"+ MedicalCode + "'"; 
 var strResult = easyQueryVer3(strSql);
  if (strResult) {
    alert("您所选择的医保机构不支持收款！");
    return false;
  }
}

function QueryGridData(){
	
	var MedicalCode = fm.all("MedicalCode1").value;
	var strSQL = " select distinct ltf.tempfeeno,"
				 +"               lcc.prtno,"
				 +"               lcc.customgetpoldate,"
				 +"               lcc.bankcode,"
				 +"               (select paymoney "
				 +"                   from ljtempfeeclass "
				 +"                  where tempfeeno = ltf.tempfeeno),"
				 +"               (select bankaccno"
				 +"                  from ljtempfeeclass"
				 +"                 where tempfeeno = ltf.tempfeeno),"
				 +"               (select accname"
				 +"                  from ljtempfeeclass"
				 +"                 where tempfeeno = ltf.tempfeeno),"
				 +"               lcc.managecom,"
				 +" case  when lcc.customgetpoldate is null then '未回执回销' "
				 +" else  '未确认' end"
				 +" from lccont lcc"
				 +" left join ljtempfee ltf"
				 +"   on lcc.contno = ltf.otherno"
				 +" where  lcc.managecom like '" + comCode + "%' "
				 +"	and lcc.paymode = '8'"
				 +"	and lcc.bankcode like '%"+MedicalCode+"%'"
				 +"   and not exists (select 1"
				 +"         from lysendtoconfirm stc"
				 +"        where stc.paycode = ltf.tempfeeno"
				 +"          and stc.confirmstate in ('1', '2')) "
				 +"	and ltf.tempfeeno is not null"
				 +" with ur";
	fm.querySql.value = strSQL;
	turnPage1.queryModal(strSQL, QueryGrid);
}
function download(){
	var querySQL = fm.querySql.value;
	if(querySQL != null && querySQL != "" )
    {
        var formAction = fm.action;
        fm.action = "ListOfUnComfirmDownload.jsp";
        //fm.target = "_blank";
        fm.filename.value = 'UnConfList_';
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先查询，再下载！");
        return ;
    }
}
