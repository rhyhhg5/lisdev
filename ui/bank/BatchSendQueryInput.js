var turnPage = new turnPageClass();
// 查询按钮
function easyQueryClick()
{
    if (!verifyInput()){
        return false;
    }
    
    if(fm.all("StartDate").value == null || fm.all("StartDate").value == ""
    	|| fm.all("EndDate").value == null || fm.all("EndDate").value == ""){
    	if(fm.all("ReturnStartDate").value == null || fm.all("ReturnStartDate").value == "" 
    		|| fm.all("ReturnEndDate").value == null || fm.all("ReturnEndDate").value == ""){
    		alert("起始日期/终止日期、回盘起期/回盘止期，两组日期至少录入一组。");
    		return false;
    	}
    }
    
    // 书写SQL语句
    var strSQL = "";
    var sSql1 = " and bl.startdate>='" + fm.all("StartDate").value + "' ";
    var eSql1 = " and bl.startdate<='" + fm.all("EndDate").value + "' ";
    var sReSql1 = " and bl.returndate>='" + fm.all("ReturnStartDate").value + "' ";
    var eReSql1 = " and bl.returndate<='" + fm.all("ReturnEndDate").value + "' ";
    
    if (fm.all("StartDate").value == null || fm.all("StartDate").value == "") {
         sSql1 = "";
    }
    
    if (fm.all("EndDate").value == null || fm.all("EndDate").value == "") {
         eSql1 = "";
    }
    
     if (fm.all("ReturnStartDate").value == null || fm.all("ReturnStartDate").value == "") {
         sReSql1 = "";
    }
    
    if (fm.all("ReturnEndDate").value == null || fm.all("ReturnEndDate").value == "") {
         eReSql1 = "";
    }
    
    if (fm.all("UniteBankCode").value == null || fm.all("UniteBankCode").value == "") {
        bankSql = "'7705','7706','7707','7709'";
    } else {
        bankSql = "'" + fm.all("UniteBankCode").value + "'";
    }
    
    if(fm.all("ManageCom").value == null || fm.all("ManageCom").value == ""){
        comSql = "";
    } else {
        comSql = " and comcode like '" + fm.all("ManageCom").value + "%'"
    }
    
    if(fm.all("Flag").value == null || fm.all("Flag").value == ""){
        flagSql = "";
    } else {
        flagSql = " and bl.logtype='" + fm.Flag.value + "'";
    }
    
    strSQL = "select bl.serialno,case bl.bankcode when '7705' then '银联' when '7706' then '通联' when '7707' then '金联' when '7709' then '银联高费率渠道' end, "
              + "case bl.logtype when 'S' then '银行代收' when 'F' then '银行代付' when 'R' then '银行实时代收' end, "
              + "case when bl.senddate is null and bl.outfile is null then '未发盘' "
              + "when bl.senddate is null and bl.outfile is not null then '发盘失败' "
              + "when bl.senddate is not null then '发盘成功' end, "
              + "bl.senddate, "
              + "case when bl.senddate is null then null  "
              + "when bl.dealstate is null then '未回盘' "
              + "when bl.dealstate is not null then '回盘成功' end, "
              + "bl.returndate, "
              + "(select nvl(sum(paymoney),0) from lyreturnfrombank where serialno=bl.serialno " + comSql + ") + (select nvl(sum(paymoney),0) from lysendtobank where serialno=bl.serialno " + comSql + "),"
              + "(select count(*) from lyreturnfrombank where serialno=bl.serialno " + comSql + ") + (select count(*) from lysendtobank where serialno=bl.serialno " + comSql+ "),"
              + "case when bl.returndate is null then 0 else (select nvl(sum(paymoney),0) from lyreturnfrombank where serialno=bl.serialno and (banksuccflag='0000' or banksuccflag='00' ) " + comSql + ") end, "
              + "case when bl.returndate is null then 0 else (select count(*) from lyreturnfrombank where serialno=bl.serialno and  (banksuccflag='0000' or banksuccflag='00') " + comSql + ") end, "
              + "case when bl.returndate is null then 0 else (select nvl(sum(paymoney),0) from lyreturnfrombank where serialno=bl.serialno and (banksuccflag<>'0000' and banksuccflag<>'00') " + comSql + ") end, "
              + "case when bl.returndate is null then 0 else (select count(*) from lyreturnfrombank where serialno=bl.serialno and (banksuccflag<>'0000' and banksuccflag<>'00') " + comSql + ") end "
              + ",bl.outfile,bl.infile "
              + "from lybanklog bl "
              + "where  bl.bankcode in (" + bankSql + ") " + sSql1 + eSql1 + sReSql1 + eReSql1 + flagSql + " order by senddate,returndate";
             

     //查询SQL，返回结果字符串
     var strSqlTemp=easyQueryVer3(strSQL, 1, 0, 1); 
     turnPage.strQueryResult=strSqlTemp;
     turnPage.pageLineNum = 20;
     
     if(!turnPage.strQueryResult)
     {
        window.alert("没有查询记录!");
        BankLogGrid.clearData();
        return false;
     }
     else
     {
        fm.sql.value = strSQL;
        turnPage.queryModal(strSQL,BankLogGrid);
     }
  
  return true;
}

function download(){
    if(BankLogGrid.mulLineCount == 0 || BankLogGrid.mulLineCount == null)

  {
    alert("没有需要下载的数据，请先查询！");
    return false;
  }
  submitForm();
}

function submitForm() {
  //检验录入框
    if (!verifyInput()){
        return false;
    }
    fm.submit(); //提交
}