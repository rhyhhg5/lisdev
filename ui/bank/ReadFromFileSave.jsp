<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：ReadFromFileSave.jsp
//程序功能：
//创建日期：2002-11-18 11:10:36
//创建人  ：胡 博
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.bank.*"%>
  <%@page import="java.io.*"%>
  <%@page import="java.util.*"%>
	<%@page import="org.apache.commons.fileupload.*"%>
  
<%
  System.out.println("\n\n---ReadFromFileSave Start---");
  
  //上载文件
	int count = 0;        
	//文件名，后面通过上载类获取
	String fileName = "";
	String objectPath = request.getParameter("filePath");
	System.out.println("objectPath:" + objectPath);
	
	String serialNo = request.getParameter("serialno");
	String bankCode = request.getParameter("bankCode");
	String bankUniteFlag = request.getParameter("bankUniteFlag");
	String bankInUniteCode = request.getParameter("bankInUniteCode");
	String DealType=request.getParameter("DealType");
	System.out.println("serialNo:" + serialNo);
	System.out.println("bankCode:" + bankCode);
	System.out.println("bankUniteFlag:" + bankUniteFlag);
	System.out.println("bankInUniteCode:" + bankInUniteCode);
	
	DiskFileUpload fu = new DiskFileUpload();
	// 设置允许用户上传文件大小,单位:字节
	fu.setSizeMax(10000000);
	// maximum size that will be stored in memory
	// 设置最多只允许在内存中存储的数据,单位:字节
	fu.setSizeThreshold(4096);
	// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
	fu.setRepositoryPath(objectPath);
	//开始读取上传信息
	
	List fileItems = null;
	try{
	 fileItems = fu.parseRequest(request);
	}
	catch(Exception ex)
	{
		ex.printStackTrace();
	}
	
	// 依次处理每个上传的文件
	Iterator iter = fileItems.iterator();
	while (iter.hasNext()) {
	  FileItem item = (FileItem) iter.next();
	  //忽略其他不是文件域的所有表单信息
	  if (!item.isFormField()) {
	    String name = item.getName();
	    System.out.println("name:" + name);
	    long size = item.getSize();
	    if((name==null||name.equals("")) && size==0)
	      continue;
	     
	    fileName = objectPath + name.substring(name.lastIndexOf("\\") + 1);
	    System.out.println("fileName:" + fileName);
	    //保存上传的文件到指定的目录
	    try {
	      item.write(new File(fileName));
	      count = 1;
	    } catch(Exception e) {
	      System.out.println("upload file error ...");
	    }
	  }
	}
	
	//// Initialization
	//mySmartUpload.initialize(pageContext);
	//mySmartUpload.setTotalMaxFileSize(1000000);
  //
	//System.out.println("...开始上载文件");
	//try	{
	//	mySmartUpload.upload();
	//}
	//catch(Exception ex)	{
	//	ex.printStackTrace();
	//}
  //
	//try {
	//	//get the file Name from http stream
	//	fileName = mySmartUpload.getFiles().getFile(0).getFileName();
	//	count = mySmartUpload.save(objectPath);
	//	
	//	System.out.println("bankUniteFlag:" + bankUniteFlag);
	//	System.out.println("objectPath:" + objectPath);
	//	System.out.println("fileName:" + fileName);
	//	System.out.println("serialNo:" + serialNo);
	//	System.out.println("bankCode:" + bankCode);
	//	System.out.println(count + " file(s) uploaded.");
	//} 
	//catch (Exception e) { 
	//	e.printStackTrace();
	//}

  
  ReadFromFileUI readFromFileUI1 = new ReadFromFileUI();

  TransferData transferData1 = new TransferData();
  transferData1.setNameAndValue("fileName", fileName);
  transferData1.setNameAndValue("serialNo", serialNo);
  transferData1.setNameAndValue("bankCode", bankCode);
  transferData1.setNameAndValue("bankUniteFlag", bankUniteFlag);
  transferData1.setNameAndValue("bankInUniteCode", bankInUniteCode);
  transferData1.setNameAndValue("DealType", DealType);
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");

  VData inVData = new VData();
  inVData.add(transferData1);
  inVData.add(tGlobalInput);
  
  String Content = "";
  String FlagStr = "";

  if (!readFromFileUI1.submitData(inVData, "READ")) {
    VData rVData = readFromFileUI1.getResult();
    Content = " 处理失败，原因是:" + (String)rVData.get(0);
  	FlagStr = "Fail";
  }
  else {
    Content = " 处理成功! ";
  	FlagStr = "Succ";
  }

	System.out.println(Content + "\n" + FlagStr + "\n---ReadFromFileSave End---\n\n");
%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>', '<%=Content%>');
</script>
</html>
