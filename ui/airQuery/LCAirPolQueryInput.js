/** 
 * 程序名称：LCAirPolInit.jsp
 * 程序功能：功能描述 该文件中包含客户端需要处理的函数和事件
 * 创建日期：2004-04-16 13:56:09
 * 创建人  ：yangtao
 * 更新人  ：
 */
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if(showInfo!=null) {
	  try {
	    showInfo.focus();
	  }
	  catch(ex) {
	    showInfo=null;
	  }
	}
}
//提交后操作,服务器数据返回后执行的操作
function afterSubmit(FlagStr, content) {
	
  showInfo.close();
  if (FlagStr == "Fail" ) {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}
// 查询按钮
function easyQueryClick() {

var strSql = "select PolNo,InsuredName,MakeDate from LCAirPol where 1=1 "
    + " and CValiDate>='"+fm.Bdate.value+"' and CValiDate<='"+fm.Edate.value
    + "'  ";
	if (operator=="")
	{
		
	}
	else
	{
		strSql=strSql+"and operator like '"+operator+"%%'";
	}
	if (managecom=="")
	{
		
	}
	else
	{
		strSql=strSql+" and agentcom = '"+managecom+"'";
	}
    
 
	turnPage.queryModal(strSql, LCAirPolGrid);
}
function showOne(parm1, parm2) {	
  //判断该行是否确实被选中
  alert("此处选择某一行的代码");
//	if(fm.all(parm1).all('InpBankGridSel').value == '1' ) {
//	  var index = (fm.all(parm1).all('BankGridNo').value - 1) % (turnPage.blockPageNum * turnPage.pageLineNum);
//	  fm.GetNoticeNo.value = turnPage.arrDataCacheSet[index][0];
 // }
}

function getMultiLineHead() {
  var arrHead = new Array();
  for (var i=0; i<LCAirPolGrid.arraySave.length; i++) {
    arrHead.push(LCAirPolGrid.arraySave[i][0]);
  }
  return arrHead;
}

function getMultiLineOrderNo() {
  var arrOrderNo = new Array();
  for (var i=0; i<LCAirPolGrid.mulLineCount; i++) {
    arrOrderNo.push(LCAirPolGrid.recordNo + i + 1);
  }
  
  //alert(arrOrderNo);
  return arrOrderNo;
}



function getEasyQueryOrderNO(){
  var arrOrderNo = new Array();
  for (var i=0; i<turnPage.arrDataCacheSet.length; i++) {
     arrOrderNo.push(i + 1);
  }
  return arrOrderNo;
}

function easyQueryPrint() {

	fm.action = "../f1print/EasyF1Print.jsp";
	fm.target = "_blank";
	fm.submit();
}