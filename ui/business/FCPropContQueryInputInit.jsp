 <%
//程序名称：FCGrpContQueryInputInit.jsp
//程序功能：
//创建日期：2003-06-12 08:49:52
//创建人  ：hou zhi wei
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
      fm.reset();
  }
  catch(ex)
  {
    alert("在FCPropContQueryInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                  

function initForm()
{
	try
	{
	  	initInpBox();  
	  	initPolicyGrid();
	  	initPolicy2Grid();
	  	initButton();
		if(tPageFrom=="Comple1")
    	{
    		inputComple.style.display = "";
    		butComple.style.display = "";
			mulComple.style.display = "";
			mulNotComple.style.display = "none";
			var sql="select SysvarValue from FDSysvar where Sysvar='dt"+tCorporate+"'";
    		var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			//是否有保单
			if(resultArr!=null)
			{
				fm.StartDate.value=resultArr[0][0];
			}
			//easyQueryClick();
		}
		else if(tPageFrom=="Comple2")
		{
			inputComple.style.display = "";
			butComple.style.display = "";
			mulComple.style.display = "";
			mulNotComple.style.display = "none";
			var sql="select SysvarValue from FDSysvar where Sysvar='dt"+tCorporate+"'";
    		var result=easyQueryVer3(sql,1,0);
			var resultArr=new Array();
			resultArr=decodeEasyQueryResult(result);
			//是否有保单
			if(resultArr!=null)
			{
				fm.StartDate.value=resultArr[0][0];
			}
			//easyQueryClick();
		}
		else if(tPageFrom=="fast"||tPageFrom=="full"||tPageFrom=="NotCar")
		{
			inputComple.style.display = "none";
			butComple.style.display = "none";
			mulComple.style.display = "none";
			mulNotComple.style.display = "";
			
			if(tPageFrom=="fast"||tPageFrom=="full")
			{
				fm.CarFlag.value = "Y";
				fm.CarFlagName.value = "是";
			}
			if(tPageFrom=="NotCar")
			{
				fm.CarFlag.value = "N";
				fm.CarFlagName.value = "否";
			}
		}
		else if(tPageFrom=="copycar"||tPageFrom=="copynotcar")
		{
			inputComple.style.display = "none";
			butComple.style.display = "none";
			mulComple.style.display = "none";
			mulNotComple.style.display = "";
			if(tPageFrom=="copycar")
			{
				fm.CarFlag.value = "Y";
				fm.CarFlagName.value = "是";
			}
			if(tPageFrom=="copynotcar")
			{
				fm.CarFlag.value = "N";
				fm.CarFlagName.value = "否";
			}
		}
		else
		{
			inputComple.style.display = "none";
			butComple.style.display = "none";
			mulComple.style.display = "none";
			mulNotComple.style.display = "";
		}
	}
	catch(re)
	{
	  alert("在FCPropContQueryInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initButton()
{
	
}

// 保单信息列表的初始化
function initPolicyGrid()
{                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[1]=new Array();
      iArray[1][0]="录单员";         		//列名
      iArray[1][1]="42px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="出单日期";         		//列名
      iArray[2][1]="52px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="代理人";         		//列名
      iArray[3][1]="52px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="保险公司";         		//列名
      iArray[4][1]="72px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[5]=new Array();
      iArray[5][0]="被保人";         		//列名
      iArray[5][1]="52px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="车牌号";         		//列名
      iArray[6][1]="52px";            		//列宽
      iArray[6][2]=60;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      iArray[7]=new Array();
      iArray[7][0]="保单号";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=60;            			//列最大值
      iArray[7][3]=0;
      
      
      iArray[8]=new Array();
      iArray[8][0]="险种";         		//列名
      iArray[8][1]="62px";            		//列宽
      iArray[8][2]=60;            			//列最大值
      iArray[8][3]=0;
      
      iArray[9]=new Array();
      iArray[9][0]="保费";         		//列名
      iArray[9][1]="52px";            		//列宽
      iArray[9][2]=60;            			//列最大值
      iArray[9][3]=0;
      
      iArray[10]=new Array();
      iArray[10][0]="保单状态";         		//列名
      iArray[10][1]="62px";            		//列宽
      iArray[10][2]=60;            			//列最大值
      iArray[10][3]=0;
      
      iArray[11]=new Array();
      iArray[11][0]="生效日期";         		//列名
      iArray[11][1]="52px";            		//列宽
      iArray[11][2]=60;            			//列最大值
      iArray[11][3]=0;
      
      iArray[12]=new Array();
      iArray[12][0]="内部集体合同号码(主键)";         		//列名
      iArray[12][1]="150px";            		//列宽
      iArray[12][2]=60;            			//列最大值
      iArray[12][3]=3;
      
      iArray[13]=new Array();
      iArray[13][0]="备注";         		//列名
      iArray[13][1]="60px";            		//列宽
      iArray[13][2]=60;            			//列最大值
      iArray[13][3]=0;

      PolicyGrid = new MulLineEnter( "fm" , "PolicyGrid" ); 
      //这些属性必须在loadMulLine前
      PolicyGrid.mulLineCount = 0;   
      PolicyGrid.displayTitle = 1;
      PolicyGrid.locked = 1;
      PolicyGrid.canSel = 1;
      PolicyGrid.hiddenPlus=1;
      PolicyGrid.hiddenSubtraction=1;
      PolicyGrid.selBoxEventFuncName ="showExam";
      PolicyGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert(ex);
      }
}

// 保单信息列表的初始化
function initPolicy2Grid()
{                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[1]=new Array();
      iArray[1][0]="保单号";         		//列名
      iArray[1][1]="135px";            		//列宽
      iArray[1][2]=60;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[2]=new Array();
      iArray[2][0]="保险公司";         		//列名
      iArray[2][1]="120px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="险种";         		//列名
      iArray[3][1]="65px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[4]=new Array();
      iArray[4][0]="被保人";         		//列名
      iArray[4][1]="65px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[5]=new Array();
      iArray[5][0]="代理人";         		//列名
      iArray[5][1]="70px";            		//列宽
      iArray[5][2]=60;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[6]=new Array();
      iArray[6][0]="经办人";         		//列名
      iArray[6][1]="65px";            		//列宽
      iArray[6][2]=60;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      iArray[7]=new Array();
      iArray[7][0]="车牌号";         		//列名
      iArray[7][1]="65px";            		//列宽
      iArray[7][2]=60;            			//列最大值
      iArray[7][3]=0;
      
      
      iArray[8]=new Array();
      iArray[8][0]="录单员";         		//列名
      iArray[8][1]="70px";            		//列宽
      iArray[8][2]=60;            			//列最大值
      iArray[8][3]=0;
      
      iArray[9]=new Array();
      iArray[9][0]="出单日期";         		//列名
      iArray[9][1]="65px";            		//列宽
      iArray[9][2]=60;            			//列最大值
      iArray[9][3]=0;
      
      iArray[10]=new Array();
      iArray[10][0]="生效日期";         		//列名
      iArray[10][1]="65px";            		//列宽
      iArray[10][2]=60;            			//列最大值
      iArray[10][3]=0;
      
      iArray[11]=new Array();
      iArray[11][0]="保单状态";         		//列名
      iArray[11][1]="75px";            		//列宽
      iArray[11][2]=60;            			//列最大值
      iArray[11][3]=0;
      
      iArray[12]=new Array();
      iArray[12][0]="内部集体合同号码(主键)";         		//列名
      iArray[12][1]="13";            		//列宽
      iArray[12][2]=60;            			//列最大值
      iArray[12][3]=3;

      Policy2Grid = new MulLineEnter( "fm" , "Policy2Grid" ); 
      //这些属性必须在loadMulLine前
      Policy2Grid.mulLineCount = 0;   
      Policy2Grid.displayTitle = 1;
      Policy2Grid.locked = 1;
      Policy2Grid.canSel = 0;
      Policy2Grid.canChk = 1;
      Policy2Grid.hiddenPlus=1;
      Policy2Grid.hiddenSubtraction=1;
      Policy2Grid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>