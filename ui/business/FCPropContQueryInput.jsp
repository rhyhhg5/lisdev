<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：FCGrpContQueryInput.jsp
	//程序功能：
	//创建日期：2003-06-12 08:49:52
	//创建人  ：zx
	//更新记录：更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312"%>
<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String operator = tG.Operator;
	// String tComType = tG.ComType;
	//String tCorporate = tG.Corporate;
%>

<%
	String tPageFrom = request.getParameter("tPageFrom");
	if (tPageFrom == null || tPageFrom.equals("null"))
		tPageFrom = "";
%>
<html>
<title>查询</title>
<head>
<script>
var tPageFrom="<%=tPageFrom%>";
var tOperator="<%=operator%>";
var tComType="0001";
var tCorporate="0001";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<script src="./FCPropContQueryInput.js"></script>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./FCPropContQueryInputInit.jsp"%>
<%@include file="../common/jsp/FHManageComLimit.jsp"%> 
</head>
<body onload="initForm();initElementtype();">
<form name=fm target=fraSubmit method=post>

<Table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divQueryCondition);">
		查询条件</TD>
	</TR>
<TR>
		<TD class=common><font style="color: #FF6600;">注：请用星号（*）来控制保单号、被保人、经办人、代理人姓名、车牌号是否需要模糊查询。（其他查询条件不支持模糊查询）</font></TD>
	</TR>
	<TR>
		<TD class=common><font style="color: #FF6600;">如：张* 代表所有姓张的客户；*小明 代表所有名字叫小明的客户；北京*支行 代表所有名字以北京开头，支行结束的客户。</font></TD>
	</TR>
</Table>

<Div id="divQueryCondition" style="display: ''">
<table class=common>
	<tr class=common style="display: ''">
		<Input name=SupplierCode type=hidden>
			 <td class="title">保险公司</td>
		 <td class="input">
		 <input class="codeno" name="SupplierShowCode" 
		 CodeData="0|^001|中国人民健康保险股份有限公司^002|中国人民人寿保险股份有限公司^003|中国人民财产保险股份有限公司^004|长城人寿保险股份有限公司^005|中英人寿保险有限公司"  
		 ondblclick="return showCodeListEx('SupplierShowCode',[this,SupplierShowCode],[0,1],null,null,null,1);" 
		 onkeyup="return showCodeListEx('SupplierShowCode',[this,SupplierShowCode],[0,1],null,null,null,1);" 
		  nextcasing=><input class="codename"  elementtype=nacessary name="SupplierName"  ></td> 
		<td class=title>保单号&nbsp;</td>
		<td class=input><input class=common name=ContNo></td>
		<td class=title>车牌号&nbsp;</td>
		<td class=input><input class=common name=LicenseNo></td>
	</tr>
	<TR class=common>
		<TD class=title>被保人&nbsp;</TD>
		<TD class=input><Input class=common name=InsuredName></TD>
		<TD class=title>经办人&nbsp;</TD>
		<TD class=input><Input class=common name=HandlingName></TD>
		<TD class=title>手机&nbsp;</TD>
		<TD class=input><Input class=common name=Phone></TD>
	</TR>
	<TR>
		<TD class=title>出单机构&nbsp;</TD>
		<TD class=input><input class=codeno name=OutManageCom
			onDblClick="return showCodeList('ManageCom2',[this,OutManageComName],[0,1],null,null,null,null,300);"
			onKeyUp="return showCodeListKey('ManageCom2',[this,OutManageComName],[0,1],null,null,null,null,300);"
			><input class=codename
			name=OutManageComName readonly onmouseover="showtitle(this);">
		</TD>
		<TD class=title>接单机构</TD>
		<TD class=input><input class=codeno name=GetManageCom
			onDblClick="return showCodeList('GetManageCom',[this,GetManageComName],[0,1],null,null,null,null,300);"
			onKeyUp="return showCodeListKey('GetManageCom',[this,GetManageComName],[0,1],null,null,null,null,300);"
			verify="接单机构|notnull&code:GetManageCom"><input class=codename
			name=GetManageComName readonly elementtype=nacessary
			onmouseover="showtitle(this);"></TD>
	</TR>
	<TR class=common>
		<TD class=title>代理人所属团队</TD>
		<TD class=input><input class=codeno name=AgentGroup
			ondblClick="showCodeList('agentgroup',[this,AgentGroupName],[0,1],null,null,null,null,300);"
			onkeyup="showCodeListKey('agentgroup',[this,AgentGroupName],[0,1],null,null,null,null,300);"><input
			class=codename name=AgentGroupName readonly onmouseover="showtitle(this);"></td>
		<TD class=title>代理人编码&nbsp;</TD>
		<TD class=input><Input class=common name=AgentCode></TD>
		<TD class=title>代理人姓名&nbsp;</TD>
		<TD class=input><Input class=common name=AgentName></TD>
	</TR>
	<TR>
		<TD class=title>是否车险&nbsp;</TD>
		<TD class=input><input class=codeno name=CarFlag
			onDblClick="return showCodeList('yesorno',[this,CarFlagName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,CarFlagName],[0,1]);"
			><input class=codename name=CarFlagName readonly></TD>
		<TD class=title>出单日期起期</TD>
		<TD class=input><input name="StartDate" class=coolDatePicker
			dateFormat="short"></td>
		<TD class=title>出单日期止期</TD>
		<TD class=input><input name="EndDate" class=coolDatePicker
			dateFormat="short"></td>
	</TR>
	
	<TR>
		<TD class=title>保单状态&nbsp;</TD>
		<TD class=input><input class=codeno name=Qstate
			CodeData="0|^-1|录入中^0|审核不通过^1|审核通过"
			ondblClick="showCodeListEx('Qstate',[this,QstateName],[0,1]);"
			onkeyup="showCodeListKeyEx('Qstate',[this,QstateName],[0,1]);"
			><input class=codename name=QstateName readonly></td>
		<TD class=title>录单员编码</TD>
		<TD class=input><input name="ReceiveOperator" class=common></td>
		<TD class=title>录单员姓名</TD>
		<TD class=input><input name="ReceiveOperatorName" class=common></td>
	</TR>
	<TR class=common id="inputComple" style="display: 'none'">
		<TD class=title>是否已做补充录入&nbsp;</TD>
		<TD class=input><input class=codeno name=Comple value="N"
			onDblClick="return showCodeList('yesorno',[this,CompleName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,CompleName],[0,1]);"
			verify="是否已做补充录入|NOTNULL&code:yesorno"
			><input class=codename name=CompleName readonly value="否" elementtype=nacessary></TD>
		<TD class=title></TD>  
		<TD class=input></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</TR>
</Table>

<table>
<tr>
<td><INPUT VALUE="查  询" onclick="easyQueryClick();" TYPE=button class=cssButton></td>
<td><INPUT VALUE="重  置" onclick="initForm();" name="button" TYPE=button class=cssButton></td>
<td><INPUT VALUE="返  回" onclick="returnParent();" name="button" TYPE=button class=cssButton></td>
<td id="butComple" style="display:''"><INPUT VALUE="补充录入完成" onclick="compleClick();" TYPE=button class=cssButton></td>
</tr>
</table>
<font color=red>该页面已经实现通过【回车键】代替点击查询按钮</font>
</Div>


<Div id="mulNotComple" style="display:''">
<Table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divQueryResult);">
		查询结果</TD>
	</TR>
</Table>

<Div id="divQueryResult" style="display: ''">
<table class=common>
	<tr>
		<td text-align: left colSpan=1><span id="spanPolicyGrid">
		</span></td>
	</tr>
</Table>
<%@include file="../common/jsp/Page.jsp"%> </Div>
</Div>
<Div id="divExamInfo" style="display: 'none'">
<table>
	<tr>
		<td class="titleImg"><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divExamInfo2);">
		审核不通过信息 </td>
	</tr>
</table>
<table class="common" id="divExamInfo2">
	<TR class=common>
		<TD class=title>审核人</TD>
		<TD class=input><input class=readonly name=ExamManName readonly></TD>
		<TD class=title>审核日期</TD>
		<TD class=input><input name="ExamDate" class=readonly readonly></TD>
	</TR>
	<TR class=common>
		<TD class=title>审核意见</TD>
		<TD colspan="3" class=input><textarea name="ExamIdea"
			class="readonly" readonly></textarea>
		</TD>
	</TR>
</table>
</Div>

<Div id="mulComple" style="display:''">
<Table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divQueryResult2);">
		查询结果</TD>
	</TR>
</Table>

<Div id="divQueryResult2" style="display: ''">
<table class=common>
	<tr>
		<td text-align: left colSpan=1><span id="spanPolicy2Grid">
		</span></td>
	</tr>
</Table>
<%@include file="../common/jsp/Page.jsp"%>  </Div>
	<table class=common>
	<tr  class= common>
        <td style="color:#FF6600;">注: 1.请先选中做补充录入完成的保单(在序号前打对钩),然后点击“补充录入完成”按钮.(动作只对当前页有效)</td>
    </tr>
    </table>
</Div>

<input type="hidden" name="hideOperate" value="">
<span id="spanCode" style="display: none; position: absolute;"></span></Form>
</body>
</html>
<script LANGUAGE="JavaScript">
function document.onkeydown()
{
	if(event.keyCode==13)
	{
		fm.ContNo.focus();
		easyQueryClick();
	}
}
fm.ContNo.focus();
</script>