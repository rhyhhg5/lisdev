<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：FCPropContInput.jsp
	//程序功能：
	//创建日期：2008-7-3 16:41:13
	//创建人  ：陈强
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.PubFun"%>
<%
//	GlobalInput tG = new GlobalInput();
//	tG = (GlobalInput) session.getValue("GI");
	String tComType = "1201198308";
	String tOperator = "001";

	String tCurrentDate = PubFun.getCurrentDate();
	String tCurrentTime = PubFun.getCurrentTime();
	String tCurrentHour = tCurrentTime.substring(0, 2);
	String tCurrentMinute = tCurrentTime.substring(3, 5);
	String tCurrentSecond = tCurrentTime.substring(6, 8);
%>
<html>
<head>
<script language="javascript">
	var tComType="<%=tComType%>";
	var tCurrentDate="<%=tCurrentDate%>";
	var state = "<%=request.getParameter("state")%>";
</script>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer4.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="FCCarContInput.js"></SCRIPT>
<SCRIPT src="ScanCarContInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FCCarContInputInit.jsp"%>
<%@include file="../common/jsp/FHManageComLimit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
<form action="./FCCarContSave.jsp" name=fm target="fraSubmit"
	method=post>
<table class="common" align=right>
	<tr align=right>
		<td class=button><INPUT id=butProj class=cssButton VALUE="项目查询"
			TYPE=hidden onclick="return projQueryClick();"></td>
		<td class=button width="10%"><INPUT type=hidden VALUE="完整录入"
			id="butFull" onclick="return fullClick();" class=cssButton
			TYPE=button></td>
		<td class=button width="10%"><INPUT id=butFresh class=cssButton
			VALUE="刷  新" name=butFresh TYPE=hidden onclick="return initForm();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="查  询"
			TYPE=hidden name=butQuery onclick="return queryCont();"></td>
		<td class=button width="10%"><INPUT class=cssButton VALUE="删  除"
			id="butDel" name=butDele TYPE=hidden
			onclick="return deleteContClick();"></td>
		<td class=button width="10%"><input value="保  存" id=butSave
			name=butSave onClick="saveClick();" type="button" class=cssButton>
		</td>
		<td class=button width="10%"><input value="保单录入完毕" id=butEnd
			name=butEnd onClick="confirmClick();" type="hidden"
			class=cssButton></td>
		<td width="4%"></td>
	</tr>
</table>
<br>

<!-- 这里是新增影像随动图片的信息 -->
<!-- div id="win"
	style="border: 1px red solid; width: 1024px; height: 80px; position: absolute; display: 'none';"
	oncontextmenu="this.style.display='none';return false;"><img
	id="imgpart" width="100%" height="100%"></div>
<div id="imgDivCtrl" style="display: 'none'">
<!-- table class="common">
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPageSpe(this,imgDiv);"> 影像信息
		</TD>
	</TR>
</table>
</div>
<div id="imgDiv" style="display: 'none'"><iframe name="fraImg"
	id="fraImg" width=1024 height=256 src=""></iframe></div -->
<!-- 这里是新增影像随动图片的信息 end -->

<Div id="contDiv" style="display: ''">
<table class="common">
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divPrtNo);"> 单证信息
		</TD>
	</TR>
</TABLE>

<Div id="divPrtNo" style="display: ''">
<TABLE class="common">
	<TR class="common">
		<TD class=title>保单印刷号&nbsp;</TD>
		<TD class=input><Input class=common name=PolicyPrtNo
			verify="保单印刷号|len<=100"></TD>
		<TD class=title>发票印刷号&nbsp;</TD>
		<TD class=input><Input class=common name=InvoicePrtNo
			verify="发票印刷号|len<=100"></TD>
		<TD class=title>保险卡印刷号&nbsp;</TD>
		<TD class=input><Input class=common name=InsCardPrtNo
			verify="保险卡印刷号|len<=100"></TD>
	</TR>
	<TR class="common">
		<TD class=title>交强险标志印刷号&nbsp;</TD>
		<TD class=input><Input class=common name=CInsFlagPrtNo
			verify="交强险标志印刷号|len<=100"></TD>
		<TD class=title></TD>
		<TD class=input></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</TR>
</TABLE>
</Div>
<table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divContInfo);">
		保单信息&nbsp;&nbsp;&nbsp;</TD>
	</TR>
</TABLE>
<Div id="divContInfo" style="display: ''">
<TABLE class="common">
	<TR>
		 <TD  class= title>出单机构</TD><TD  class= input><Input class="codeno" name=OutManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
		 ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
		 onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"   
		  readonly ><Input class=codename name=ManageComName readOnly elementtype=nacessary >  
		<TD class=title>接单机构</TD>
		<TD class=input><input class=codeno name=GetManageCom
			onDblClick="return showCodeList('GetManageCom',[this,GetManageComName],[0,1],null,null,null,null,300);"
			onKeyUp="return showCodeListKey('GetManageCom',[this,GetManageComName],[0,1],null,null,null,null,300);"
			verify="接单机构|notnull&code:GetManageCom"><input class=codename
			name=GetManageComName readonly elementtype=nacessary
			onmouseover="showtitle(this);"></TD>
		<TD class=title>车牌号<font color=red size=2.5>(新车录0)</TD>
		<TD class=input><Input class=common name=LicenseNo
			verify="车牌号|notnull&len<=10" elementtype=nacessary></TD>
	</TR>
	<TR>
		 <td class="title">保险公司</td>
		 <td class="input">
		 <input class="codeno" name="SupplierShowCode" 
		 CodeData="0|^001|中国人民健康保险股份有限公司^002|中国人民人寿保险股份有限公司^003|中国人民财产保险股份有限公司^004|长城人寿保险股份有限公司^005|中英人寿保险有限公司"  
		 ondblclick="return showCodeListEx('SupplierShowCode',[this,SupplierName],[0,1],null,null,null,1);" 
		 onkeyup="return showCodeListEx('SupplierShowCode',[this,SupplierName],[0,1],null,null,null,1);" 
		  nextcasing=><input class="codename"  elementtype=nacessary name="SupplierName"  ></td> 
		<td class=title>保单号&nbsp;</td>
		<td class=input><input class=common name=ContNo
			verify="保单号|notnull&len<=40" elementtype=nacessary></td>
		<TD class=title>业务性质</TD>
		<TD class=input><input class=codeno name=BussNature
			onDblClick="return showCodeList('BussNature2',[this,BussNatureName],[0,1]);"
			onKeyUp="return showCodeListKey('BussNature2',[this,BussNatureName],[0,1]);"
			verify="业务性质|code:BussNature2&notnull"><input class=codename
			name=BussNatureName readonly elementtype=nacessary></TD>
	</TR>
	<TR>
		<TD class=title>代理人</TD>
		<TD class=input><Input class=common name=AgentCode
			onchange="getAgent();"></TD>
		<TD class=title>代理人所在团队&nbsp;</TD>
		<TD class=input><input type='hidden' name=AgentGroup><input
			class=readonly name=AgentGroupName readonly
			onmouseover="showtitle(this);"></TD>

		<TD class=title>是否参与内部考核积分</TD>
		<TD class=input><input class=codeno name=RightSendFlag
			onDblClick="return showCodeList('yesorno',[this,RightSendFlagName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,RightSendFlagName],[0,1]);"
			verify="是否参与内部考核积分|code:yesorno&notnull"><input
			class=codename name=RightSendFlagName readonly elementtype=nacessary>
		</TD>
	</TR>
	<TR id="channel" style="display: 'none'">
		<TD class=title>渠道编码</TD>
		<TD class=input><Input class=common name=ChannelCode
			onblur="showChannelName()" verify="渠道编码|len<=20"> <input
			type="button" class=cssButtonLong value="查  询"
			onClick="queryChannel();"></TD>
		<TD class=title>渠道名称</TD>
		<TD class=input><Input class=readonly name=ChannelName readonly></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</TR>
	<TR class=common>
		<TD class=title>签单日期</TD>
		<TD class=input><input name=UWDate value="<%=tCurrentDate%>"
			class=coolDatePicker dateFormat="short" verify="签单日期|notnull&date"
			elementtype=nacessary></TD>
		<TD class=title>出单日期</TD>
		<TD class=input><input name=SignDate value="<%=tCurrentDate%>"
			class=coolDatePicker dateFormat="short" verify="出单日期|notnull&date"
			elementtype=nacessary></TD>
		<TD class=title>出单时间</TD>
		<TD class=input><input size=2 name=hour value="<%=tCurrentHour%>"
			verify="小时|notnull&NUM&len=2" elementtype=nacessary>&nbsp;时&nbsp;<input
			maxlength=2 size=2 name=minute value="<%=tCurrentMinute%>"
			verify="分钟|notnull&NUM&len=2" elementtype=nacessary>&nbsp;分<Input
			name=Second type=hidden value="00"></TD>
	</TR>
	<TR class=common>
		<td class=title>保单生效日期&nbsp;</td>
		<td class=input><input name="CValiDate"
			onblur="showCInValiDate();" class=coolDatePicker dateFormat="short"
			verify="保单生效日期|notnull&date" elementtype=nacessary></td>
		<td class=title>保单有效日期至&nbsp;</td>
		<td class=input><input name="CInValiDate" class=coolDatePicker
			dateFormat="short" verify="保单有效日期至|notnull&date"
			elementtype=nacessary></td>
		<TD class=title>POS机交易号</TD>
		<TD class=input><Input class=common name=POSNo
			verify="POS机交易号|len<=20"></TD>
	</TR>
	<TR class=common>
		<TD class=title>被保人类型&nbsp;</TD>
		<TD class=input><input class=codeno name=InsuredCustType
			onDblClick="return showCodeList('CustType',[this,InsuredCustTypeName],[0,1]);"
			onKeyUp="return showCodeListKey('CustType',[this,InsuredCustTypeName],[0,1]);"
			verify="被保人类型|notnull&code:CustType"><input class=codename
			name=InsuredCustTypeName readonly elementtype=nacessary>
		<TD class=title>被保人姓名&nbsp;</TD>
		<TD class=input><Input class=common name=InsuredName
			onblur="showCarOwnerName();" verify="被保人姓名|notnull&len<=60"
			elementtype=nacessary></TD>
		<td class=title>被保人证件类型</td>
		<TD class=input><Input class=codeno name=InsuredIDType
			ondblClick="showCodeList('IDType',[this,InsuredIDTypeName],[0,1],null,fm.InsuredCustType.value,fm.InsuredCustType.value,'1');"
			onkeyup="showCodeListKey('IDType',[this,InsuredIDTypeName],[0,1],null,fm.InsuredCustType.value,fm.InsuredCustType.value,'1');"
			verify="被保人证件类型|code:AllIDType"><input class=codename
			name=InsuredIDTypeName readonly></TD>
	</TR>
	<TR class=common>
		<td class=title>被保人证件号码</td>
		<td class=input><input class=common name=InsuredIDNo
			verify="被保人证件号码|len<=20"></td>
		<TD class=title>是否见费出单</TD>
		<TD class=input><input class=codeno name=ComFirstFeeFlag
			onDblClick="return showCodeList('yesorno',[this,ComFirstFeeFlagName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,ComFirstFeeFlagName],[0,1]);"
			verify="是否见费出单|code:yesorno&notnull"><input class=codename
			name=ComFirstFeeFlagName readonly elementtype=nacessary></TD>
		<TD class=title>是否放弃并授权</TD>
		<TD class=input><input class=codeno name=AccreditFlag
			onDblClick="return showCodeList('yesorno',[this,AccreditFlagName],[0,1]);"
			onKeyUp="return showCodeListKey('yesorno',[this,AccreditFlagName],[0,1]);"
			verify="是否放弃并授权|code:yesorno&notnull"><input class=codename
			name=AccreditFlagName readonly elementtype=nacessary></TD>
	</TR>
	<TR>
		<TD class=title>车主</TD>
		<TD class=input><Input class=common name=CarOwner
			verify="车主|len<=30"></TD>
		<TD class=title>新车购置价格(万元)</TD>
		<TD class=input><Input class=common name=PurchasePrice
			verify="新车购置价格(万元)|notnull&Money:8-4&value>=0&value<=1000"
			elementtype=nacessary></TD>
		<TD class=title>使用性质</TD>
		<TD class=input><Input class=codeno name=UseNatureCode
			verify="使用性质|notnull&code:carusages"
			ondblClick="showCodeList('carusages',[this,UseNatureName],[0,1],null,'1','OtherSign');"
			onkeyup="showCodeListKey('carusages',[this,UseNatureName],[0,1],null,'1','OtherSign');"><input
			class=codename name=UseNatureName readonly elementtype=nacessary>
		</TD>
	</TR>
	<TR class=common>
		<TD class=title>备注</TD>
		<TD colspan="13" class=input><textarea name="Remark"
			class="common"></textarea> <input type=hidden name=hideRemark>
		</TD>
	</TR>
</TABLE>
</Div>
<%@include file="FBProjRiskIns.jsp"%>
<Table>
	<TR>
		<TD class=titleImg><IMG src="../common/images/butExpand.gif"
			style="cursor: hand;" OnClick="showPage(this,divRiskDetail);">
		险种信息</TD>
	</TR>
</Table>
<Div id="divRiskDetail" style="display: ''">
<TABLE class="common">
	<TR class=common>
		<TD class=title>险种编码</TD>  
        <td class="input">
        <input class="codeno" name=OutRiskCode
        CodeData="0|^130101|普通车险^130102|意外车险"  
        ondblclick="return showCodeListEx('OutRiskCode',[this,RiskName],[0,1],null,null,null,1);" 
        onkeyup="return showCodeListEx('OutRiskCode',[this,RiskName],[0,1],null,null,null,1);"  
        nextcasing=><input class="codename" elementtype=nacessary name=RiskName  ></td> 
		<TD class=title>保费&nbsp;</TD>
		<TD class=input><Input class=common name=SumPrem
			verify="保费|NOTNULL&NUM&value>=0&Money:15-2" elementtype=nacessary></TD>
		<TD class=title>车船税</TD>
		<TD class=input><input class=common name=tax1
			verify="车船税|NUM&NOTNULL&value>=0&Money:15-2"></TD>
	</TR>
	<TR id="tax" style="display: ''">
		<TD class=title>费率等级</TD>
		 <TD class=input><input class="codeno" name=FYCType
			verify="费率等级|NOTNULL"
			CodeData="0|^000212|A等级^000213|B等级"
			ondblclick="return showCodeListEx('FYCType',[this,FYCTypeName],[0,1],null,null,null,1);" 
            onkeyup="return showCodeListEx('FYCType',[this,FYCTypeName],[0,1],null,null,null,1);"><input class=codename name=FYCTypeName readonly
            onmouseover= "return getFYCTypeReMark()" ></TD>
		<TD class=title>费率等级描述</TD>
		<TD class=input><input class=readonly name=FYCTypeReMark></TD>
		<TD class=title>印花税</TD>
		<TD class=input><input class=common name=tax2
			verify="印花税|NUM&NOTNULL&value>=0&Money:15-2"></TD>
	</TR>
</table>
</Div>
<!--div id="divKindInfo" style="display: ''">
<TABLE class="common">
	<TR class=common>
		<TD height="23" colspan="4" class=titleImg><IMG
			src="../common/images/butExpand.gif" style="cursor: hand;"
			OnClick="showPage(this,divKindGrid);"> 险别信息</TD>
	</TR>
</TABLE>

<Div id="divKindGrid" style="display: ''">
<table id=mytable1 class=common>
	<tr>
		<td text-align:left colSpan=1><span id="spanKindGrid"></span></td>
	</tr>
</table>
</Div>
</Div -->
<!--Div id="divCheckBox" style="display: ''">
< TABLE class="common">
	<TR class=common align="left">
		<TD><INPUT TYPE=checkBox NAME=CopyFlag id=CopyFlag>复制保单</input></TD>
		<TD>&nbsp;</TD>
		<TD><INPUT TYPE=checkBox NAME=ISSingle id=ISSingle   onClick="isSingle()">是否单出</input></TD>
		<TD width="70%">&nbsp;</TD>
	</TR>
</TABLE>
< /Div-->
</Div>
<Div id="riskDiv" style="display: ''">
<table>
	<TR>
		<TD class=titleImg OnClick="showPage2(this,divCarInfo);"
			style="CURSOR: hand;"><IMG
			src="../common/images/butCollapse.gif" style="cursor: hand;">
		车辆信息</TD>
	</TR>
</TABLE>
<Div id="divCarInfo" style="display: ''">
<table class=common>
	<TR>
		<TD class=title>出单员</TD>
		<TD class=input><Input class=common name=SignMan ></TD>
		<TD class=title>录单员</TD>
		<TD class=input><Input class=common name=ReceiveOperator></TD>
		<TD class=title>录单日期</TD>
		<TD class=input><input class=readonlyhalf name=ReceiveDate
			value="<%=tCurrentDate%>" readonly></TD>
	</TR>
	<TR class=common>
		<TD class=title>车架号</TD>
		<TD class=input><Input class=common name=FrameNo
			verify="车架号|len<=30"></TD>
		<TD class=title>发动机号</TD>
		<TD class=input><Input class=common name=EngineNo
			verify="发动机号|len<=30"></TD>
		<TD class=title>车辆种类</TD>
		<TD class=input><Input class=codeno name=CarKindCode
			verify="车辆种类|code:carkind"
			ondblClick="showCodeList('carkind',[this,CarKindName],[0,1]);"
			onkeyup="showCodeListKey('carkind',[this,CarKindName],[0,1]);"><input
			class=codename name=CarKindName readonly></TD>
	</TR>
	<TR class=common>
		<TD class=title>厂牌车型(车系+车型)</TD>
		<TD class=input><Input class=common name=ModelCode
			verify="厂牌车型|len<=50"></TD>
		<TD class=title>初次登记日期</TD>
		<TD class=input><input name="EnrollDate" class=coolDatePicker
			dateFormat="short" verify="初次登记日期|Date"></TD>
		<TD class=title>年审到期日</TD>
		<TD class=input><input name="YearExamDate" class=coolDatePicker
			dateFormat="short" verify="年审到期日|Date"></TD>
	</TR>
	<TR>
		<TD class=title>座位数</TD>
		<TD class=input><Input class=common name=SeatCount1
			verify="座位数|len<=8&int"></TD>
		<TD class=title>吨位数</TD>
		<TD class=input><Input class=common name=TonCount
			verify="吨位数|len<=8&num"></TD>
		<TD class=title>国别性质</TD>
		<TD class=input><Input class=codeno name=CountryNature
			verify="国别性质|code:countrynature"
			ondblClick="showCodeList('countrynature',[this,CountryNatureName],[0,1]);"
			onkeyup="showCodeListKey('countrynature',[this,CountryNatureName],[0,1]);"><input
			class=codename name=CountryNatureName readonly></TD>
	</TR>
	<TR>
		<TD class=title>车牌底色</TD>
		<TD class=input><Input class=codeno name=LicenseColorCode
			verify="车牌底色|code:licensecolor"
			ondblClick="showCodeList('licensecolor',[this,LicenseColorName],[0,1]);"
			onkeyup="showCodeListKey('licensecolor',[this,LicenseColorName],[0,1]);"><input
			class=codename name=LicenseColorName readonly></TD>
		<TD class=title>使用年限</TD>
		<TD class=input><Input class=common name=UseYears
			verify="使用年限|len<=12&num"></TD>
		<TD class=title></TD>
		<TD class=input></TD>
	</TR>
	<TR class=common>
		<TD class=title>保单特约</TD>
		<TD colspan="13" class=input><textarea name="PolicyPli"
			class="common"></textarea> <input type=hidden name=hidePolicyPli>
		</TD>
	</TR>
</table>
</DIV>
</Div>
<!--从这里开始是车险保单随动录入需要的隐藏帧  begin  --> <input name="Policy" type=hidden>
<input name="isscanflag" type=hidden><!-- 判断该保单是否有影响件 1：有影响件 0：没有影响件 -->
<input name="PolicyType" type=hidden> <input name="ScanFlag"
	type=hidden value="Fast"> <input name="InsuredSex1" type=hidden>
<input name="InsuredSexName1" type=hidden> <input
	name="InsuredPostalAddress1" type=hidden> <input
	name="InsuredBirthday1" type=hidden> <input
	name="InsuredEMail1" type=hidden> <input name="InsuredMobile1"
	type=hidden> <input name="InsuredHomePhone1" type=hidden>
<input name="InsuredPhone1" type=hidden> <!-- 从这里开始是车险保单随动录入需要的隐藏帧  end   -->
<input name="url" type=hidden> <input name="hideOperate"
	type=hidden> <input name="FullFlag" type=hidden value="false">
<Input name="InnerContNo" type=hidden> <Input
	name="SupplierCode" type=hidden> <Input name="ProtocolNo"
	type=hidden> <Input name="Corporation" type=hidden> <input
	name="RiskCode" type=hidden> <input name="KindCode" type=hidden>
<input name="RiskCons" type=hidden> <input name="FYCCons"
	type=hidden> <input name="SignTime" type=hidden> <input
	name="PolNo" type=hidden> <input name="ItemType" type=hidden>
<input name="ItemNo" type=hidden> <input name="ItemCode"
	type=hidden value="06"> <input name="ItemName" type=hidden
	value="车辆"> <input name="InsuredNo" type=hidden> <input
	name="AgentGrade" type=hidden value="#"> <input
	name="PKProtocolNo" type=hidden> <input name="hideCInValiDate"
	type=hidden> <input name="FirstFeeFlag" type=hidden> <input
	name="FirstFeeFlagName" type=hidden><input name="AgentFlag"
	type=hidden> <input name="PayIntv" type=hidden value="0">
<input name="AgentScore" type=hidden> <input name="RuleID"
	type=hidden> <input name="IsSingle1" type=hidden><br>
	<input name="State" type=hidden >
</form>
<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
<script LANGUAGE="JavaScript">
function document.onkeydown()
{
	if(event.keyCode==13)
	{
		document.getElementById("win").style.display="none";
		if(window.document.activeElement.name=="LicenseNo")
		{
			getCont();
		}
		if(window.document.activeElement.name=="ContNo")
		{
			getPrintCont();
		}
		if(window.document.activeElement.name=="AgentCode")
		{
			getAgent();
		}
	}
}

fm.PolicyPrtNo.focus();
</script>
