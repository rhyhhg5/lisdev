//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	if(tPageFrom=="Comple1"||tPageFrom=="Comple2")
	{
		initPolicy2Grid();
	}
	else
	{
		initPolicyGrid();
	}	 
	
	var sqlPart = "";
	
	if(tComType=="00"||tComType=="01")
	{
		//集团和事业部不允许在代理业务中查询
		sqlPart = " and 1=0";
	}
	
	//保单录入，保单补充录入，按录单机构来控制机构查询权限.
	else if(tPageFrom=="fast"||tPageFrom=="full"||tPageFrom=="NotCar"||tPageFrom=="Comple1"||tPageFrom=="Comple2")
	{	
		 sqlPart =getManageComLimitlike4("a.AgentGroup",UserComCode);//代理人机构 能查询
		 sqlPart+= " or a.ReceiveCom='"+UserComCode+"')";
	}
	//保全能查询整个法人的保单
	else if(tPageFrom=="PropEdor"||tPageFrom=="PropEdorInput")
	{
	    if(tComType=="02")//财险平台公司查询 所有法人
	    {
	    	sqlPart = getManageComLimitlike3("a.Corporation",UserComCode," and comtype ='03'");
	    }else
	    {
	    	sqlPart = " and a.Corporation='"+tCorporate+"'";
	    }
		
	}
	else
	{
		sqlPart = getManageComLimitlike3("a.AgentGroup",UserComCode);
	}
		
	var strSQL = "";
	if(tPageFrom=="copycar")
	{
		if(NullOrBlank(fm.SupplierShowCode.value)&&NullOrBlank(fm.ContNo.value)
	 	  &&NullOrBlank(fm.ReceiveCom.value)&&NullOrBlank(fm.OutManageCom.value)
	 	  &&NullOrBlank(fm.GetManageCom.value)&&NullOrBlank(fm.AgentGroup.value)
	 	  &&NullOrBlank(fm.InsuredName.value)&&NullOrBlank(fm.AgentCode.value)
	 	  &&NullOrBlank(fm.AgentName.value)&&NullOrBlank(fm.HandlingName.value)
	 	  &&NullOrBlank(fm.CarFlag.value)&&NullOrBlank(fm.LicenseNo.value)
	 	  &&NullOrBlank(fm.StartDate.value)&&NullOrBlank(fm.EndDate.value)&&NullOrBlank(fm.Phone.value)
	 	  &&NullOrBlank(fm.Qstate.value)&&NullOrBlank(fm.ReceiveOperator.value)&&NullOrBlank(fm.ReceiveOperatorName.value)){
	 	  		alert("所有查询条件不能都为空！");
				return false;
	 	}
		
		strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgenttree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where "
	        +" a.State<>'05' and a.BizType='02' and a.CarFlag='Y'"
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom' )
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode')
			//+ getWherePart( 'd.name','AgentName','like2')
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')"
		}
		
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		//modify by fanxj
		var AgentNameSql = " and exists(select 1 from FAAgenttree where a.AgentCode = AgentCode and a.Corporation = Corporation and name like '%"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
		 
		strSQL +=" order by a.CValiDate desc";
	}
	else if(tPageFrom=="copynotcar")
	{
		if(NullOrBlank(fm.SupplierShowCode.value)&&NullOrBlank(fm.ContNo.value)
	 	  &&NullOrBlank(fm.ReceiveCom.value)&&NullOrBlank(fm.OutManageCom.value)
	 	  &&NullOrBlank(fm.GetManageCom.value)&&NullOrBlank(fm.AgentGroup.value)
	 	  &&NullOrBlank(fm.InsuredName.value)&&NullOrBlank(fm.AgentCode.value)
	 	  &&NullOrBlank(fm.AgentName.value)&&NullOrBlank(fm.HandlingName.value)
	 	  &&NullOrBlank(fm.CarFlag.value)&&NullOrBlank(fm.LicenseNo.value)
	 	  &&NullOrBlank(fm.StartDate.value)&&NullOrBlank(fm.EndDate.value)&&NullOrBlank(fm.Phone.value)
	 	  &&NullOrBlank(fm.Qstate.value)&&NullOrBlank(fm.ReceiveOperator.value)&&NullOrBlank(fm.ReceiveOperatorName.value)){
	 	  		alert("所有查询条件不能都为空！");
				return false;
	 	}
	 	
	 	strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgentTree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where a.State<>'05' and a.BizType='02' and a.CarFlag='N'"
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom' )
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode')
			//+ getWherePart( 'd.name','AgentName','like2')
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')"
		}
		
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		
		//modify by fanxj
		var AgentNameSql = " and exists(select 1 from FAAgenttree where a.AgentCode = AgentCode and a.Corporation = Corporation and name like '%"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}

		strSQL +=" order by a.CValiDate desc";
	}

	// 借单申请保单查询
	 else if(tPageFrom=="Lent")
	 {
		strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgentTree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where a.State='01' and a.BizType='02' "
	        +" and a.FirstFeeFlag='Y' "
            +" and (substr(a.PolState,1,2)='05' or substr(a.PolState,1,2)='07' or substr(a.PolState,1,2)='08') "
            +" and a.innercontno not in (select innercontno from FCContLend where (ExamStat is null  or ExamStat='1') and innercontno is not null)"
            +" and a.innercontno in(select innercontno from FJSPayCustomer where innercontno = a.innercontno) "
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom' )
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode')
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')"
		}
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		
		var AgentNameSql = " and exists(select 1 from FAAgenttree where a.AgentCode = AgentCode and a.Corporation = Corporation and name like '%"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
		 
		strSQL +=" order by a.CValiDate desc";
	}
	//财险保全批单录入保单查询
	//此处保单按照前后模糊查询效率很低，建议如果输入6位则采用模糊匹配，否则采用“=”的方式。 add by xyw/2010-06-21(与客户商量一下吧~)
	else if(tPageFrom=="PropEdorInput" || tPageFrom=="PropEdor")
	      {
	      	if(NullOrBlank(fm.SupplierShowCode.value)&&NullOrBlank(fm.ContNo.value)
		 	  &&NullOrBlank(fm.ReceiveCom.value)&&NullOrBlank(fm.OutManageCom.value)
		 	  &&NullOrBlank(fm.GetManageCom.value)&&NullOrBlank(fm.AgentGroup.value)
		 	  &&NullOrBlank(fm.InsuredName.value)&&NullOrBlank(fm.AgentCode.value)
		 	  &&NullOrBlank(fm.AgentName.value)&&NullOrBlank(fm.HandlingName.value)
		 	  &&NullOrBlank(fm.CarFlag.value)&&NullOrBlank(fm.LicenseNo.value)
		 	  &&NullOrBlank(fm.StartDate.value)&&NullOrBlank(fm.EndDate.value)&&NullOrBlank(fm.Phone.value)
		 	  &&NullOrBlank(fm.Qstate.value)&&NullOrBlank(fm.ReceiveOperator.value)&&NullOrBlank(fm.ReceiveOperatorName.value)){
		 	  		alert("所有查询条件不能都为空！");
					return false;
		 	}
	      
			strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgenttree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where a.State='01' and a.BizType='02'"
            //+" and (substr(a.PolState,1,2)='05' or substr(a.PolState,1,2)='07' or substr(a.PolState,1,2)='08' or substr(a.PolState,1,2)='09') "
            //+" and substr(a.PolState, 1, 2) in ('05', '07', '08', '09')"
            +" and ( a.PolState like '05%' or a.PolState like '07%' or a.PolState like '08%' or a.PolState like '09%')" //使之不走X索引，走机构索引比较快。摒弃cost modify by xyw 2010-06-21
            +" and not exists(select 1 from fpedormain where innercontno = a.innercontno and edorstate in ('01', '03'))"
            //+" and a.innercontno not in (select c.InnerContNo from FPEdorMain c  where c.edorstate in ('01','03') )"
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom')
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode')
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')"
		}
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		var AgentNameSql = " and exists(select 1 from FAAgentTree where a.AgentCode = AgentCode and a.Corporation= Corporation and name like '"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
		 
		//strSQL +=" order by a.CValiDate desc";
	}
	
	else if(tPageFrom=="fast")
	    {
		   strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgentTree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where a.State='01' and a.BizType='02' "
	        +" and a.PolState like '01%' "
	        +"  and a.CarFlag='Y' "
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom' )
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode')
			//+ getWherePart( 'd.name','AgentName','like2')
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')";
		}
		
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		
		//modify by fanxj
		var AgentNameSql = " and exists(select 1 from FAAgenttree where a.AgentCode = AgentCode and a.Corporation = Corporation and name like '"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
		 
		strSQL +=" order by a.CValiDate desc";
	}
	else if(tPageFrom=="full")
	    {
	    strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgenttree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where a.State='01' and a.BizType='02' "
	        +" and a.PolState like '03%' "
	        +"  and a.CarFlag='Y' "
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom' )
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode')
			//+ getWherePart( 'd.name','AgentName','like2')
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')";
		}
		
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		//modify by fanxj
		var AgentNameSql = " and exists(select 1 from FAAgentTree where a.AgentCode = AgentCode and a.Corporation= Corporation and name like '"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
		 
		strSQL +=" order by a.CValiDate desc";
	}
	else if(tPageFrom=="Comple1")//车险保单补充录入查询
    {
    if(verifyInput2() == false)
  	{
	    return false;
	}
	  strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
		+" a.SignDate,"
		+" (select d.Name from FAAgentTree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
		+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
		+" a.InsuredName,"
		+" a.LicenseNo,"
		+" a.ContNo,"
		+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
  		+" a.SumPrem,"
  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
		+" a.CValiDate, "
		+" a.InnerContNo,"
		+" a.Remark"
		+" from FCCont a"
        +" where a.State='01' and a.BizType='02' "
        +" and a.CarFlag='Y' "
        +sqlPart
	    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
	    + getWherePart2( 'a.ContNo','ContNo')
		+ getWherePart2( 'a.ReceiveCom','ReceiveCom' )
	    + getWherePart2( 'a.OutManageCom','OutManageCom' )
	    + getWherePart2( 'a.GetManageCom','GetManageCom' )
		+ getWherePart2( 'a.AgentGroup','AgentGroup')
	    + getWherePart2( 'a.InsuredName','InsuredName')
	    + getWherePart2( 'a.AgentCode','AgentCode')
	    + getWherePart2( 'a.HandlingName','HandlingName')
	    + getWherePart2( 'a.CarFlag','CarFlag')
		+ getWherePart2( 'a.LicenseNo','LicenseNo')
		+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
	    + getWherePart( 'a.SignDate','StartDate','>=')
	    + getWherePart( 'a.SignDate','EndDate','<=');
	    
	    if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')"
		}
		if(fm.Comple.value=="Y")
		{
			strSQL += " and substr(a.PolState,1,2) in ('05','07','08','09') "
			+" and exists (select 1 from FCContFull where  VisitOperator is null and innercontno=a.innercontno)";
			//strSQL += " and substr(a.PolState,1,2)='08' and a.innercontno in (select innercontno from fccontfull where innercontno=a.innercontno)";//条件1只确定了做过非车险补充录入
			//strSQL += "  and not  exists(select 1 from fccontfull where innercontno = a.innercontno and  VisitOperator is not null )";//条件2 确定 没有做过回访的
		}
		else if(fm.Comple.value=="N")
		{
			strSQL += " and substr(a.PolState,1,2) in ('05','07','08','09') "
			+" and not exists (select 1 from FCContFull where  VisitOperator is null and innercontno=a.innercontno)";
			//strSQL += " and substr(a.PolState,1,2) in ('05','09','07')";
		}
		else if(fm.Comple.value=="")
		{
			strSQL += " and substr(a.PolState,1,2) in ('05','07','08','09') ";
			//strSQL += " and (substr(a.PolState,1,2) in ('05','09','07') or (substr(a.PolState,1,2)='08' and a.innercontno in (select innercontno from fccontfull where innercontno=a.innercontno)))";
		}
		else
		{
			strSQL += " and a.PolState='AAAAAAAA'";
		}
		
		var AgentNameSql = " and exists(select 1 from FAAgentTree where a.AgentCode = AgentCode and a.Corporation = Corporation and name like '"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
	    
	    strSQL += " order by a.CValiDate desc";
	}
	
	else if(tPageFrom=="Comple2")//非车险保单补充录入查询
    {
    if(verifyInput2() == false)
  	{
	    return false;
	}
	  strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgentTree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where a.State='01' and a.BizType='02' "
	        +" and a.CarFlag='N' "
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom' )
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode')
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')"
		}
		if(fm.Comple.value=="Y")
		{
			strSQL += " and substr(a.PolState,1,2)='08' and a.innercontno in (select innercontno from fccontfull where innercontno=a.innercontno)";//条件1只确定了做过非车险补充录入
			//strSQL += "  and not  exists(select 1 from fccontfull where innercontno = a.innercontno and  VisitOperator is not null )";//条件2 确定 没有做过回访的
		}
		else if(fm.Comple.value=="N")
		{
			strSQL += " and substr(a.PolState,1,2) in ('09','07')";
		}
		else if(fm.Comple.value=="")
		{
			strSQL += " and (substr(a.PolState,1,2) in ('09','07') or (substr(a.PolState,1,2)='08' and a.innercontno in (select innercontno from fccontfull where innercontno=a.innercontno)))";
		}
		else
		{
			strSQL += " and a.PolState='AAAAAAAA'";
		}
		
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		var AgentNameSql = " and exists(select 1 from FAAgentTree where a.AgentCode = AgentCode and a.Corporation = Corporation and name like '"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
	    
	    strSQL += " order by a.CValiDate desc";
	}
	
	
	
	else if(tPageFrom=="NotCar")
	{
		strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgenttree d where a.AgentCode=d.AgentCode and a.Corporation=d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where a.State='01' and a.BizType='02' "
	        +" and a.PolState like '03%' "
	        +"  and a.CarFlag='N' "
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode')
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom' )
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode')
			//+ getWherePart( 'd.name','AgentName','like2')
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')"
		}
		
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		
		//modify by fanxj
		var AgentNameSql = " and exists(select 1 from FAAgentTree where a.AgentCode = AgentCode and a.Corporation = Corporation and name like '"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
		 
		strSQL +=" order by a.CValiDate desc";
	}
	 else if(tPageFrom=="exam")
	   {
		strSQL = " select (select e.UserName from FDUser e where e.UserCode=a.ReceiveOperator),"
			+" a.SignDate,"
			+" (select d.Name from FAAgentTree d where a.AgentCode=d.AgentCode and a.Corporation =d.Corporation),"
			+" (select b.ShortName from FDInsCom b where a.SupplierCode=b.SupplierCode),"
			+" a.InsuredName,"
			+" a.LicenseNo,"
			+" a.ContNo,"
			+" (select g.RiskShortName from FCPol f,FMRisk g where f.InnerContNo=a.InnerContNo and g.RiskCode=f.RiskCode and rownum=1),"
	  		+" a.SumPrem,"
	  		+" decode(a.examstate,'0','审核不通过',(select CodeName from FDCode where CodeType = 'ppolstate' and Code = concat(a.State, substr(a.PolState, 1, 2)))),"
			+" a.CValiDate, "
			+" a.InnerContNo,"
			+" a.Remark"
			+" from FCCont a"
	        +" where a.State='01' and a.BizType='02' "
	       	+" and (substr(a.PolState,1,2)='02' or substr(a.PolState,1,2)='04') "
	        +sqlPart
		    + getWherePart2( 'a.SupplierCode','SupplierShowCode' )
		    + getWherePart2( 'a.ContNo','ContNo')
		    + getWherePart2( 'a.ReceiveCom','ReceiveCom' )
		    + getWherePart2( 'a.OutManageCom','OutManageCom' )
		    + getWherePart2( 'a.GetManageCom','GetManageCom' )
			+ getWherePart2( 'a.AgentGroup','AgentGroup')
		    + getWherePart2( 'a.InsuredName','InsuredName')
		    + getWherePart2( 'a.AgentCode','AgentCode' )
		    + getWherePart2( 'a.HandlingName','HandlingName')
		    + getWherePart2( 'a.CarFlag','CarFlag')
			+ getWherePart2( 'a.LicenseNo','LicenseNo')
			+ getWherePart2( 'a.ReceiveOperator','ReceiveOperator')
		    + getWherePart( 'a.SignDate','StartDate','>=')
		    + getWherePart( 'a.SignDate','EndDate','<=');
		
		if(!NullOrBlank(fm.Phone.value))
		{
			strSQL +=" and (a.InsuredMobile='"+fm.Phone.value+"' or a.AppntMobile='"+fm.Phone.value+"' or a.HandlingMobile='"+fm.Phone.value+"')"
		}
		if(!NullOrBlank(fm.Qstate.value))
		{
			if(fm.Qstate.value=="-1")
				{
					strSQL += " and a.ExamState is null";
				}
				else if(fm.Qstate.value=="0")
				{
					strSQL += " and a.ExamState='0'";
				}
				else if(fm.Qstate.value=="1")
				{
					strSQL += " and a.ExamState='1'";
				}else
				{
					strSQL += " and 1=0";
				}
		}
		if(!NullOrBlank(fm.ReceiveOperatorName.value))
		{
			strSQL +=" and exists(select 1 from fduser where usercode = a.ReceiveOperator and username like '"+ fm.ReceiveOperatorName.value +"%') ";
		}
		
		var AgentNameSql = " and exists(select 1 from FAAgentTree where a.AgentCode = AgentCode and a.Corporation= Corporationand name like '"+ fm.AgentName.value +"%')";
		if(!NullOrBlank(fm.AgentName.value))
		{
			strSQL += AgentNameSql;
		}
		 
		strSQL +=" order by a.CValiDate desc";
	 }
	
	if(tPageFrom=="Comple1"||tPageFrom=="Comple2")
	{
		turnPage.queryModal(strSQL,Policy2Grid,0,1); 
	}
	else
	{
		turnPage.queryModal(strSQL,PolicyGrid,0,1); 
	}
	
	//判断是否查询成功
	if (!turnPage.strQueryResult) 
	{
		alert("没有查到相关信息！");
		return false;
	}
}


function returnParent()
{
	var tInnerContNo = "";
	var arrReturn = new Array();
	
	if(tPageFrom=="Comple1"||tPageFrom=="Comple2")
	{
		Policy2Grid.delBlankLine("Policy2Grid");
		lineCount = Policy2Grid.mulLineCount;
		
		if (lineCount == 0)
	    {
	        alert("请先选择一条记录，再点击返回按钮。");
	        return false;
	    }
	    else
	    {
	    	var j = 0;
	    	for (var i = 0; i < lineCount; i++)
	        {
	         	//判断是否选中
	         	var chk = Policy2Grid.getChkNo(i);
	        	if(chk == true)
	        	{
	        		j = j +1;
	        		tInnerContNo = Policy2Grid.getRowColData(i, 12);
	          	}
	        }
	        if(j != 1)
	        {
	        	alert("请先选择一条记录，再点击返回按钮。");
	        	return false;
	        }
	    }
	}
	else
	{
		var tSel = PolicyGrid.getSelNo();
		if( tSel == 0 || tSel == null ){
			alert( "请先选择一条记录，再点击返回按钮。" );
			return false;
		}
		
		tInnerContNo = PolicyGrid.getRowColData(tSel-1,12);
	}		
	try
	{
		if(tPageFrom=="copycar"||tPageFrom=="copynotcar") //保单复制返回
		{
			top.opener.afterQueryCopy( tInnerContNo );
		}
		//借单保单申请返回	
	   	else if(tPageFrom=="Lent") 
		{
			top.opener.afterQueryCont(tInnerContNo);
		} 
     	//财险保全申请返回			 
	   	else if(tPageFrom=="PropEdor")
	   	{
	     	top.opener.afterQueryPropEdor( tInnerContNo );
	   	}
	   //	财险保全批单录入保单查询	
	   	else if(tPageFrom=="PropEdorInput")
	   	{
	     	top.opener.afterContQuery(tInnerContNo);	
	   	}
	   	//保单查询返回
		else 
		{
			top.opener.afterQuery( tInnerContNo );
		}
	}
	catch(ex)
	{
		alert( "没有发现父窗口的afterQuery接口。" + ex );
	}
	top.close();
}

//提交前的校验、计算  
function beforeSubmit(){
    var lineCount = 0;
    var tPolState = "";
    
    Policy2Grid.delBlankLine("Policy2Grid");
    //得到行数
    lineCount = Policy2Grid.mulLineCount;
    if (lineCount == 0)
    {
        alert("请先查询出相关信息！");
        return false;
    }
    else
    {
    	var j = 0;
    	for (var i = 0; i < lineCount; i++)
        {
         	//判断是否选中
         	var chk = Policy2Grid.getChkNo(i);
        	if(chk == true)
        	{
        		j = j +1;
        		
        		tPolState = Policy2Grid.getRowColData(i, 10);
        		if(tPolState=="补充录入完成")
        		{
        			alert("选择的保单【"+Policy2Grid.getRowColData(i, 7)+"】已经是补充录入完成状态！");
        			return false;
        		}
          	}
        }
        if(j == 0)
        {
        	alert("请先选择！");
        	return false;
        }
    }
    
	return true;
}

function compleClick()
{
	if(!beforeSubmit())
 	{
    	return false;
  	}
  	
	var i = 0;
	var showStr="正在查询数据，请您稍候";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;  
	
	showInfo=window.showModelessDialog(urlStr,window,WINDOWSSIZE);   
	fm.hideOperate.value=tPageFrom;
	
	if (fm.hideOperate.value=="")
	{
		alert("操作控制数据丢失！");
	}
	
	fm.action="FCPropContQuerySave.jsp";
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
 	showInfo.close();
 	if (FlagStr == "Fail" )
 	{             
   		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
   		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
 	}
 	else
 	{ 
   		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
   		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		
		easyQueryClick();
 	}
}

function showExam()
{
	divExamInfo.style.display="none";
	var tSel = PolicyGrid.getSelNo();
	if(PolicyGrid.getRowColData(tSel-1,10)=="审核不通过")
	{
		divExamInfo.style.display="";
		var sql="select b.UserName,a.ExamDate,a.ExamIdea from FCExam a,FDUser b"
		+" where a.BussNo='"+PolicyGrid.getRowColData(tSel-1,12)+"'"
		+" and a.BussNoType='1' and a.ExamType='02'"
		+" and a.ExamMan=b.UserCode"
		+" order by SerialNo desc";
		
		var result=easyQueryVer3(sql,1,0,1);
		var resultArr=new Array();
		resultArr=decodeEasyQueryResult(result,0,0);
		if(resultArr!=null)
		{
			fm.ExamManName.value = resultArr[0][0];
			fm.ExamDate.value = resultArr[0][1];
			fm.ExamIdea.value = unescapes(resultArr[0][2]);
		}
	}
}