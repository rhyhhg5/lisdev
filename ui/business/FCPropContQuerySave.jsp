<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.CErrors"%>
<%@page import="com.sinosoft.utility.VData"%>
<%@page import="com.sinosoft.jbs.schema.FCContSchema"%>
<%@page import="com.sinosoft.jbs.vschema.FCContSet"%>
<%@page import="com.sinosoft.jbs.business.FCPropContComple"%>
<%@page import="com.sinosoft.jbs.pubfun.GlobalInput"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
String FlagStr = "";
String Content = "";
CErrors tError = null;

String tOperate = request.getParameter("hideOperate");

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");

if(tOperate.equals("Comple1"))
{
	FCPropContComple tFCPropContComple = new FCPropContComple();
	FCContSchema tFCContSchema = null;
	FCContSet tFCContSet = new FCContSet();
	
	int lineCount = 0;
	String tChk[] = request.getParameterValues("InpPolicy2GridChk");
	String tInnerContNo[] = request.getParameterValues("Policy2Grid12");
	String tPolState[] = request.getParameterValues("Policy2Grid13");

	lineCount = tChk.length;
	for(int i=0;i<lineCount;i++)
	{ 	
	   	if(tChk[i].equals("on")||tChk[i].equals("1"))
	  	{	
	   		tFCContSchema = new FCContSchema();
	   		tFCContSchema.setInnerContNo(tInnerContNo[i]);
	   		tFCContSchema.setPolState(tPolState[i]);
	   		
	   		tFCContSet.add(tFCContSchema);
	    }
	}
	
	VData tVData = new VData();
	tVData.add(tG);
	tVData.add(tFCContSet);

  	try
  	{
    	System.out.println("this will save the data!!!");
    	tFCPropContComple.submitData(tVData, tOperate);
  	}
  	catch(Exception ex)
  	{
     	Content = "保存失败，原因是:" + ex.toString();
     	FlagStr = "Fail";
  	}

	if (!FlagStr.equals("Fail"))
	{
  		tError = tFCPropContComple.mErrors;
  		if (!tError.needDealError())
  		{                          
  			Content = " 保存成功! ";
  			FlagStr = "Succ";
  		}
  		else                                                                           
  		{
  			Content = " 保存失败，原因是:" + tError.getFirstError();
  			FlagStr = "Fail";
  		}
	}
}

if(tOperate.equals("Comple2"))
{
	FCPropContComple tFCPropContComple = new FCPropContComple();
	FCContSchema tFCContSchema = null;
	FCContSet tFCContSet = new FCContSet();
	
	int lineCount = 0;
	String tChk[] = request.getParameterValues("Policy2GridChk");
	String tInnerContNo[] = request.getParameterValues("Policy2Grid12");
	String tPolState[] = request.getParameterValues("Policy2Grid13");

	lineCount = tChk.length;
	for(int i=0;i<lineCount;i++)
	{ 	
	   	if(tChk[i].equals("on")||tChk[i].equals("1"))
	  	{	
	   		tFCContSchema = new FCContSchema();
	   		tFCContSchema.setInnerContNo(tInnerContNo[i]);
	   		tFCContSchema.setPolState(tPolState[i]);
	   		
	   		tFCContSet.add(tFCContSchema);
	    }
	}
	
	VData tVData = new VData();
	tVData.add(tG);
	tVData.add(tFCContSet);

  	try
  	{
    	System.out.println("this will save the data!!!");
    	tFCPropContComple.submitData(tVData, tOperate);
  	}
  	catch(Exception ex)
  	{
     	Content = "保存失败，原因是:" + ex.toString();
     	FlagStr = "Fail";
  	}

	if (!FlagStr.equals("Fail"))
	{
  		tError = tFCPropContComple.mErrors;
  		if (!tError.needDealError())
  		{                          
  			Content = " 保存成功! ";
  			FlagStr = "Succ";
  		}
  		else                                                                           
  		{
  			Content = " 保存失败，原因是:" + tError.getFirstError();
  			FlagStr = "Fail";
  		}
	}
}
if(tOperate.equals("CusComple"))//客户批量回访录入完成
{
	FCPropContComple tFCPropContComple = new FCPropContComple();
	FCContSchema tFCContSchema = null;
	FCContSet tFCContSet = new FCContSet();
	
	int lineCount = 0;
	String tChk[] = request.getParameterValues("Policy2GridChk");
	String tInnerContNo[] = request.getParameterValues("Policy2Grid12");
	String tPolState[] = request.getParameterValues("Policy2Grid13");
	
	lineCount = tChk.length;
	for(int i=0;i<lineCount;i++)
	{ 	
	   	if(tChk[i].equals("on")||tChk[i].equals("1"))
	  	{	
	   		tFCContSchema = new FCContSchema();
	   		tFCContSchema.setInnerContNo(tInnerContNo[i]);
	   		tFCContSchema.setPolState(tPolState[i]);
	   		
	   		tFCContSet.add(tFCContSchema);
	    }
	}
	
	VData tVData = new VData();
	tVData.add(tG);
	tVData.add(tFCContSet);

  	try
  	{
    	System.out.println("this will save the data!!!");
    	tFCPropContComple.submitData(tVData, tOperate);
  	}
  	catch(Exception ex)
  	{
     	Content = "保存失败，原因是:" + ex.toString();
     	FlagStr = "Fail";
  	}

	if (!FlagStr.equals("Fail"))
	{
  		tError = tFCPropContComple.mErrors;
  		if (!tError.needDealError())
  		{                          
  			Content = " 保存成功! ";
  			FlagStr = "Succ";
  		}
  		else                                                                           
  		{
  			Content = " 保存失败，原因是:" + tError.getFirstError();
  			FlagStr = "Fail";
  		}
	}
	
	
}

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>