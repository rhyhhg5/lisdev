<%
//程序名称：LAAChargeWrapRateInit.jsp
//程序功能：
//创建时间：2008-01-24
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
 //var code1 = 1;
 var  mSQL=" 1 and  char(length(trim(comcode))) = #8# and sign=#1#" ;
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  { 
	fm.all('F05Name').value=''; 
	fm.all('F05Code').value='';  
	fm.all('CurYear').value='';
	//fm.all('Year').value='';
	fm.all('ManageComName').value='';
	fm.all('RiskCodeName').value='';
    fm.all('RiskCode').value='';
    fm.all('AgentCom').value='';
    fm.all('Name').value='';
    fm.all('Rate').value='';  
    fm.all('ManageCom').value='';
    fm.all('BranchType').value=getBranchType();  
    fm.all('LogManagecom').value = '<%=ManageCom%>';
    fm2.diskimporttype.value='LARateCommision';  
    fm.BranchType.value=getBranchType();
    fm2.BranchType.value=getBranchType();
    fm.diskimporttype.value='LARateCommision';
    fm.branchtype2.value="0"+<%= BranchType2%>;
    fm2.branchtype2.value="0"+<%= BranchType2%>;
  }
  catch(ex)
  {
    alert("在InterProtocolPoundageInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许  
        
    iArray[1]=new Array();
	iArray[1][0]="险种编码";          		//列名
	iArray[1][1]="60px";      	      		//列宽
	iArray[1][2]=20;            			//列最大值
	iArray[1][3]=2;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	iArray[1][4]="riskcode";              	        //是否引用代码:null||""为不引用
	iArray[1][5]="1|2";              	                //引用代码对应第几列，'|'为分割符
	iArray[1][6]="0|1";
    iArray[1][9]="险种编码|NotNull";
    
   
    
    iArray[2]=new Array();
    iArray[2][0]="险种名称"; //列名
    iArray[2][1]="80px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=1;              //是否允许输入,1表示允许,0表示不允许
                                          

    
    
    iArray[3]=new Array();
    iArray[3][0]="管理机构"; //列名
    iArray[3][1]="80px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[3][4]="ComCode";
    iArray[3][5]="3|4";
    iArray[3][6]="0|1";
    iArray[3][9]="管理机构|NotNull&NUM&len=8";  
    iArray[3][15]="1";
    iArray[3][16]=mSQL; 
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构名称"; //列名
    iArray[4][1]="100px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=1;              //是否允许输入,1表示允许,0表示不允许     
       
    iArray[5]=new Array();
    iArray[5][0]="代理机构"; //列名
    iArray[5][1]="80px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=2;              //是否允许输入,1表示允许,0表示不允许
    iArray[5][4]="agentcom4";
    iArray[5][5]="5|6";
    iArray[5][6]="0|1"
    iArray[5][9]="代理机构|NotNull";
    iArray[5][15]="managecom";
    //iArray[6][16]=tSQL; 
    iArray[5][17]= "3"; 
    
       
    iArray[6]=new Array();
    iArray[6][0]="代理机构名称"; //列名
    iArray[6][1]="120px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许
        
    iArray[7]=new Array();
    iArray[7][0]="保费类型"; //列名
    iArray[7][1]="70px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=2;              //是否允许输入,1表示允许,0表示不允许   
    iArray[7][9]="保费类型|NotNull";
    iArray[7][10]="F01Code";
    iArray[7][11]="0|^01|基本保费|^02|额外保费|^03|追加保费|^10|普通保费类型";
    iArray[7][17]=1;
    
    iArray[8]=new Array();
    iArray[8][0]="保险期间"; //列名
    iArray[8][1]="60px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=3;              //是否允许输入,1表示允许,0表示不允许 
    //iArray[8][9]="保险期间|value<=99&value>=0&int";
    
   

	iArray[9]=new Array();
    iArray[9][0]="缴费方式"; //列名
    iArray[9][1]="80px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=2;              //是否允许输入,1表示允许,0表示不允许    
    iArray[9][9]="缴费方式|NotNull";
    iArray[9][10]="PayintvCode";
    iArray[9][11]="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴";
	
    
    iArray[10]=new Array();
    iArray[10][0]="缴费年期"; //列名
    iArray[10][1]="80px";        //列宽
    iArray[10][2]=100;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许 
    iArray[10][9]="缴费年期|NotNull&value>=0&int";
  //  iArray[10][10]="f03";
  //  iArray[10][11]="0|^0|趸交|^1|1年交|^3|3年交|^5|5年交|^10|10年交|^15|15年交|^20|20年交|^30|30年交";
    //iArray[10][15]="1";
    //iArray[10][16]=code1;
    //iArray[10][17]= "1";
    

    
     iArray[11]=new Array();
     iArray[11][0]="保单年度"; //列名
     iArray[11][1]="80px";        //列宽
     iArray[11][2]=100;          //列最大值
     iArray[11][3]=1;
     iArray[11][9]="保单年度|int";
     
     iArray[12]=new Array();
     iArray[12][0]="手续费比率"; //列名
     iArray[12][1]="80px";        //列宽
     iArray[12][2]=100;          //列最大值
     iArray[12][3]=1; 
     
     iArray[13]=new Array();
     iArray[13][0]="序号"; //列名
     iArray[13][1]="0px";        //列宽
     iArray[13][2]=100;          //列最大值
     iArray[13][3]=1; 
     
     iArray[14]=new Array();
     iArray[14][0]="险种编码"; //列名
     iArray[14][1]="0px";        //列宽
     iArray[14][2]=100;          //列最大值
     iArray[14][3]=3;
     
     iArray[15]=new Array();
     iArray[15][0]="管理机构编码"; //列名
     iArray[15][1]="0px";        //列宽
     iArray[15][2]=100;          //列最大值
     iArray[15][3]=3;
     
     iArray[16]=new Array();
     iArray[16][0]="代理机构编码"; //列名
     iArray[16][1]="0px";        //列宽
     iArray[16][2]=100;          //列最大值
     iArray[16][3]=3;
     
     iArray[17]=new Array();
     iArray[17][0]="保费类型"; //列名
     iArray[17][1]="0px";        //列宽
     iArray[17][2]=100;          //列最大值
     iArray[17][3]=3;
     
     iArray[18]=new Array();
     iArray[18][0]="保险期间"; //列名
     iArray[18][1]="0px";        //列宽
     iArray[18][2]=100;          //列最大值
     iArray[18][3]=3;
     
     iArray[19]=new Array();
     iArray[19][0]="缴费方式"; //列名
     iArray[19][1]="0px";        //列宽
     iArray[19][2]=100;          //列最大值
     iArray[19][3]=3;
     
     iArray[20]=new Array();
     iArray[20][0]="缴费年期"; //列名
     iArray[20][1]="0px";        //列宽
     iArray[20][2]=100;          //列最大值
     iArray[20][3]=3;
     
     iArray[21]=new Array();
     iArray[21][0]="保单年度"; //列名
     iArray[21][1]="0px";        //列宽
     iArray[21][2]=100;          //列最大值
     iArray[21][3]=3;
     
     
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
    //SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =1;
    //SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在InterProtocolPoundageInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

    iArray[1]=new Array();
	  iArray[1][0]="导入文件批次";          		//列名
	  iArray[1][1]="120px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在InterProtocolPoundageInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();    
    initSetGrid(); 
    initImportResultGrid();
  }
  catch(re)
  {
    alert("在InterProtocolPoundageInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initInpBox1()
{
  try
  {
	  fm2.all('queryType').value ="";
	  fm2.all('queryName').value="";
	  fm2.all('FileImportNo').value="";
    //alert(fm2.BranchType.value);
    //alert(getBranchType());
  
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
	  alert("在InterProtocolPoundageInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
</script>