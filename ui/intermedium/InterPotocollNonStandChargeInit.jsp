<%
//程序名称：LABComNSRateCommInit.jsp
//程序功能：银代非标准业务网点手续费录入
//创建日期：2008-01-24 
//创建人  ：Huxl
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%
     //添加页面控件的初始化。
%>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>                                           
<script language="JavaScript">

function initInpBox()
{ 
  try
  {           
		fm.all('ContNo').value = "";
		fm.all('BranchType').value = <%=BranchType%>;
//		fm.all('BranchType2').value = <%=BranchType2%>;
		fm.all('ChargeType').value = <%=ChargeType%>;
  }
  catch(ex)
  {
    alert("InterPotocollNonStandChargeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
                                       
function initForm()
{
  try
  {
    initInpBox();
    initArchieveGrid();
  }
  catch(re)
  {
    alert("InterPotocollNonStandChargeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//非标手续费录入列表的初始化
var ArchieveGrid;
function initArchieveGrid() {                               
  var iArray = new Array();
  try {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		
    iArray[0][3]=0;         		
    
    iArray[1]=new Array();
    iArray[1][0]="保单号";         		//列名
    iArray[1][1]="80px";                //列宽
    iArray[1][2]=200;                  //列最大值
    iArray[1][3]=0;                    //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[2]=new Array();
    iArray[2][0]="险种代码";         //列名
    iArray[2][1]="60px";      	  //列宽         			
    iArray[2][2]=200;             //列最大值
    iArray[2][3]=0;               //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[3]=new Array();
    iArray[3][0]="险种名称";         //列名
    iArray[3][1]="150px";      	  //列宽         			
    iArray[3][2]=200;             //列最大值
    iArray[3][3]=0;               //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构";         //列名
    iArray[4][1]="80px";      	  //列宽         			
    iArray[4][2]=200;             //列最大值
    iArray[4][3]=0;               //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[5]=new Array();
    iArray[5][0]="管理机构名称";      //列名
    iArray[5][1]="80px";      	  //列宽         			
    iArray[5][2]=200;             //列最大值
    iArray[5][3]=0;                //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

     
    iArray[6]=new Array();
    iArray[6][0]="代理机构";        //列名
    iArray[6][1]="80px";      	 //列宽         			
    iArray[6][2]=200;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[7]=new Array();
    iArray[7][0]="代理机构名称";     //列名
    iArray[7][1]="80px";      	 //列宽         			
    iArray[7][2]=200;            //列最大值
    iArray[7][3]=0;               //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    
    iArray[8]=new Array();
    iArray[8][0]="客户经理";         //列名
    iArray[8][1]="80px";      	  //列宽         			
    iArray[8][2]=200;             //列最大值
    iArray[8][3]=0;               //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[9]=new Array();
    iArray[9][0]="客户经理姓名";        //列名  		        
    iArray[9][1]="80px";      	    //列宽  		     			
    iArray[9][2]=200;               //列最大值
    iArray[9][3]=0;                 //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[10]=new Array();
    iArray[10][0]="保费";              //列名
    iArray[10][1]="60px";      	     //列宽         			
    iArray[10][2]=200;               //列最大值
    iArray[10][3]=0;                 //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    
    iArray[11]=new Array();
    iArray[11][0]="手续费比例";  
    iArray[11][1]="60px";      	      //列宽         			
    iArray[11][2]=200;                //列最大值
    iArray[11][3]=1;                  //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    iArray[12]=new Array();
    iArray[12][0]="险种号";  
    iArray[12][1]="0px";      	      //列宽         			
    iArray[12][2]=200;               //列最大值
    iArray[12][3]=1;                 //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
    
    ArchieveGrid = new MulLineEnter( "fm" , "ArchieveGrid" ); 
    //这些属性必须在loadMulLine前

    ArchieveGrid.mulLineCount = 0;   
    ArchieveGrid.displayTitle = 1;
    ArchieveGrid.hiddenPlus = 1;
    ArchieveGrid.hiddenSubtraction = 1;    

    ArchieveGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //ArchieveGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
