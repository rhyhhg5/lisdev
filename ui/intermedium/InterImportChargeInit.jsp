<%
//程序名称：LaratecommisionSetInit.jsp
//程序功能：
//创建时间：2009-01-13
//创建人  ：miaoxz 
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>
<script language="JavaScript">
// var  tmanagecom=" 1 and  char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
//var tmanagecom =" 1 and   char(length(trim(comcode)))<=#4# ";
// var triskcode="1 and code in (select CODE from LDCODE where codetype=#activeriskcode#) ";
</SCRIPT>
<script language="JavaScript">
function initInpBox()
{
  try
  {
	    fm.all('ContNo').value=''; 
	    fm.all('GroupAgentCode').value='';
	    fm.all('AgentName').value='';
	    fm.all('StartDate').value='';
	    fm.all('EndDate').value='';
	    fm.all('AppntName').value='';
	    fm.all('MangeCom').value='';
	    fm.all('diskimporttype').value='LAGetOtherCharge';
	    fm2.all('diskimporttype').value='LAGetOtherCharge';  
	    fm.BranchType.value=getBranchType();
	    fm2.BranchType.value=getBranchType();
	    fm.branchtype2.value='<%= BranchType2%>';
	    fm2.branchtype2.value='<%= BranchType2%>';
	    //alert(fm2.Branchtype2.value);
	    //alert(getBranchType());
	
    //alert(fm.all('branchtype2').value);                        
  }
  catch(ex)
  {
    alert("在InterImportChargeInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}

function initSetGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=90;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       
    
    iArray[1]=new Array();
    iArray[1][0]="结算单号"; //列名
    iArray[1][1]="70px";        //列宽
    iArray[1][2]=90;            //列最大值
    iArray[1][3]=1;
    
    iArray[2]=new Array();
    iArray[2][0]="销售机构"; //列名
    iArray[2][1]="70px";        //列宽
    iArray[2][2]=90;            //列最大值
    iArray[2][3]=1;    
    
    iArray[3]=new Array();
    iArray[3][0]="销售人员姓名"; //列名
    iArray[3][1]="70px";        //列宽
    iArray[3][2]=90;            //列最大值
    iArray[3][3]=1;    
    
    iArray[4]=new Array();
    iArray[4][0]="销售人员工号"; //列名
    iArray[4][1]="70px";        //列宽
    iArray[4][2]=90;            //列最大值
    iArray[4][3]=1;    
    
    iArray[5]=new Array();
	iArray[5][0]="保单号";          		//列名
	iArray[5][1]="90px";      	      		//列宽
	iArray[5][2]=20;            			//列最大值
    iArray[5][3]=1;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的
	

    iArray[6]=new Array();
    iArray[6][0]="归属机构代码"; //列名
    iArray[6][1]="110px";        //列宽
    iArray[6][2]=90;            //列最大值
    iArray[6][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
    iArray[7]=new Array();
    iArray[7][0]="归属机构名称"; //列名
    iArray[7][1]="90px";        //列宽
    iArray[7][2]=30;            //列最大值
    iArray[7][3]=1;              //是否允许输入,1表示允许,0表示不允许
  
    
    iArray[8]=new Array();
    iArray[8][0]="险种代码"; //列名
    iArray[8][1]="90px";        //列宽
    iArray[8][2]=90;            //列最大值
    iArray[8][3]=1;              //是否允许输入,1表示允许,0表示不允许
  
    iArray[9]=new Array();
    iArray[9][0]="险种名称"; //列名
    iArray[9][1]="90px";        //列宽
    iArray[9][2]=90;            //列最大值
    iArray[9][3]=1;              //是否允许输入,1表示允许,0表示不允许    

     
    iArray[10]=new Array();
    iArray[10][0]="实收保费"; //列名
    iArray[10][1]="90px";        //列宽
    iArray[10][2]=90;            //列最大值
    iArray[10][3]=1;              //是否允许输入,1表示允许,0表示不允许 

    
    iArray[11]=new Array();
    iArray[11][0]="佣金跟单比例"; //列名
    iArray[11][1]="90px";        //列宽
    iArray[11][2]=90;            //列最大值
    iArray[11][3]=1;              //是否允许输入,1表示允许,0表示不允许
   
     
    iArray[12]=new Array();
    iArray[12][0]="佣金"; //列名
    iArray[12][1]="90px";        //列宽
    iArray[12][2]=90;            //列最大值
    iArray[12][3]=1;              //是否允许输入,1表示允许,0表示不允许    
  
    
    iArray[13]=new Array();
    iArray[13][0]="起保日期"; //列名
    iArray[13][1]="70px";        //列宽
    iArray[13][2]=90;            //列最大值
    iArray[13][3]=1;   
    
    iArray[14]=new Array();
    iArray[14][0]="终保日期"; //列名
    iArray[14][1]="70px";        //列宽
    iArray[14][2]=90;            //列最大值
    iArray[14][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[15]=new Array();
    iArray[15][0]="投保人"; //列名
    iArray[15][1]="70px";        //列宽
    iArray[15][2]=90;            //列最大值
    iArray[15][3]=1;    
    
    iArray[16]=new Array();
    iArray[16][0]="车牌号码"; //列名
    iArray[16][1]="70px";        //列宽
    iArray[16][2]=90;            //列最大值
    iArray[16][3]=1;    
    
    iArray[17]=new Array();
    iArray[17][0]="收款人姓名"; //列名
    iArray[17][1]="70px";        //列宽
    iArray[17][2]=90;            //列最大值
    iArray[17][3]=1;     //是否允许输入,1表示允许,0表示不允许
    
    iArray[18]=new Array();
    iArray[18][0]="开户银行"; //列名
    iArray[18][1]="70px";        //列宽
    iArray[18][2]=90;            //列最大值
    iArray[18][3]=1;    
    
    iArray[19]=new Array();
    iArray[19][0]="帐号/卡号"; //列名
    iArray[19][1]="70px";        //列宽
    iArray[19][2]=90;            //列最大值
    iArray[19][3]=1;
    
    iArray[20]=new Array();
    iArray[20][0]="签单日期"; //列名
    iArray[20][1]="80px";        //列宽
    iArray[20][2]=90;            //列最大值
    iArray[20][3]=1;              //是否允许输入,1表示允许,0表示不允许
  
    iArray[21]=new Array();
    iArray[21][0]="佣金生成日期"; //列名
    iArray[21][1]="80px";        //列宽
    iArray[21][2]=90;            //列最大值
    iArray[21][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[22]=new Array();
    iArray[22][0]="佣金支付批次"; //列名
    iArray[22][1]="80px";        //列宽
    iArray[22][2]=90;            //列最大值
    iArray[22][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[23]=new Array();
    iArray[23][0]="业务单证ID"; //列名
    iArray[23][1]="80px";        //列宽
    iArray[23][2]=90;            //列最大值
    iArray[23][3]=1;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[24]=new Array();
    iArray[24][0]="序号"; //列名
    iArray[24][1]="0px";        //列宽
    iArray[24][2]=90;            //列最大值
    iArray[24][3]=3; 
    

   
    SetGrid = new MulLineEnter( "fm" , "SetGrid" );
    SetGrid.canChk = 1;
		//SetGrid.mulLineCount = 10;
    SetGrid.displayTitle = 1;
    SetGrid.hiddenSubtraction =1;
    SetGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    SetGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在InterImportChargeInit.jsp-->initSetGrid函数中发生异常:初始化界面错误!");
  }
}
function initImportResultGrid()
{
  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许       

      iArray[1]=new Array();
	  iArray[1][0]="导入文件批次";          		//列名
	  iArray[1][1]="120px";      	      		//列宽
	  iArray[1][2]=20;            			//列最大值
	  iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

    iArray[2]=new Array();
    iArray[2][0]="所在行"; //列名
    iArray[2][1]="60px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许
   
                                       
    iArray[3]=new Array();
    iArray[3][0]="日志信息"; //列名
    iArray[3][1]="160px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
      
    ImportResultGrid = new MulLineEnter( "fm2" , "ImportResultGrid" );
    ImportResultGrid.canChk = 0;
		//SetGrid.mulLineCount = 10;
    //ImportResultGrid.displayTitle = 1;
    ImportResultGrid.hiddenSubtraction =1;
    ImportResultGrid.hiddenPlus=1;
    //SetGrid.locked=1;
    //SetGrid.canSel=0;
    ImportResultGrid.loadMulLine(iArray);    
  }
  catch(ex)
  {
    alert("在InterImportChargeInit.jsp-->initImportResultGrid函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    
    //debugger;
    initInpBox();    
    initSetGrid();
    initImportResultGrid();
  }
  catch(re)
  {
    alert("在InterImportChargeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>