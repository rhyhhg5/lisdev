 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var saveClick=false;
var arrDataSet;
var mOperate="";
var turnPage = new turnPageClass();
var outWorkDate="";
var outWorkDate1="";
var ascriptiondate="";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if(!beforeSubmit())
  {
   return false;
  }
 
  fm.all("hideOperate").value=mOperate;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ; 
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    initForm();
  }  
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{  
} 

//取消按钮对应操作
function cancelForm()
{
}
 

 	
function  checkname()
{ 
	//alert("23232");
  if((fm.ContNo.value ==null || fm.ContNo.value =="")&&(fm.GrpContNo.value ==null || fm.GrpContNo.value ==""))
  {
	alert("请先输入个单或团单号！");
    fm.all('GroupAgentNew').value = '';
	return false;
	
  }	
  if(fm.GroupAgentNew.value != null && fm.GroupAgentNew.value != ""){
  	var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+fm.GroupAgentNew.value+"' ";
	var strQueryResult = easyExecSql(tYAgentCodeSQL);
	if(!strQueryResult){
		alert("获取业务员编码失败！");
	    fm.all('GroupAgentNew').value = '';
		return false;
	}
	fm.AgentNew.value = strQueryResult[0][0];
  }
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  var tPreName=fm.all('PreName').value;
  /*if(tPreName==null || tPreName==''){
  	alert('请输入原销售人员代码');
  	fm.all('AgentNew').value='';
  	return false;  	
  }*/
  var  sql="select 1 from laagent where 1=1 "
  		+ getWherePart('agentcode','AgentOld')
  		+" and managecom =(select managecom from laagent where 1=1 "
  		+ getWherePart('agentcode','AgentNew')
  		+")";
  var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("原代理人与新代理人不在同一个管理机构！操作失败。");
    fm.all('NameNew').value='';
    fm.all('AgentNew').value='';
    fm.all('GroupAgentNew').value = '';
    return false;
  } 	

  sql = "select a.name,a.agentgroup from laagent a where 1=1 "
	   + getWherePart('a.agentcode','AgentNew') + " and Agentstate<'03'"
	   + " and branchtype='"+tBranchType+"'"
	   + " and branchtype2='"+tBranchType2+"'";
	    
  strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("没有此代理人！");
    fm.all('NameNew').value='';
    fm.all('AgentNew').value = '';
    fm.all('GroupAgentNew').value = '';
    return;
  }  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);
　//<rem>######//　
  fm.all('NameNew').value  = tArr[0][0];
  fm.all('NeWAgentGroup').value = tArr[0][1];
 
 var wagesql = "select max(indexcalno) a from lawage where agentcode='"+fm.all('AgentNew').value+"' and branchtype='5' and branchtype2='01' "
// +" union select max(indexcalno) a from lawagetemp where agentcode='"+fm.all('AgentNew').value+"' and branchtype='5' and branchtype2='01' "
 +" order by a desc fetch first 1 rows only" ;
  var strQueryResult = easyQueryVer3(wagesql, 1, 1, 1);
  if (strQueryResult)
  {
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    var days= tArr[0][0];
    var sql=
    	"select enddate + 15 days "
    	+"from lastatsegment "
    	+"where yearmonth ='"+days+"' and stattype='1'";
    var strQueryResult2 = easyQueryVer3(sql, 1, 1, 1);
    if (strQueryResult2)
    {
    	tArr = decodeEasyQueryResult(strQueryResult2);
	    fm.all('MaxCalDate').value = tArr[0][0];
    }
  // ascriptiondate=fm.all('AscriptionDate').value;
 }
  if(fm.all('AgentCom').value != null && fm.all('AgentCom').value !="")
  {
  var sql1 ="select agentcom from lacomtoagent where relatype ='1'and agentcom ='"+fm.all('AgentCom').value+"' and agentcode='"+fm.all('AgentNew').value+"'";
  var strQueryResult1 = easyQueryVer3(sql1,1,1,1);
  if(!strQueryResult1){
	  alert(" 中介出单，则手工归属时，原互动专员与新互动专员必须是同一中介机构、同一销售机构 ! ");
	  fm.all('NameNew').value ='';
	  fm.all('GroupAgentNew').value='';
	  fm.all('GroupAgentNew').focus();
	  return false ;
  }
  else
  {
	  var tArr = new Array();
	  tArr = decodeEasyQueryResult(strQueryResult1);
	  var tcom = tArr[0][0];
	  fm.all('AgentComNew').value = tcom; 
  }
  }
}



	
function checkCount()
{
	var sqll = "select contno from lccont where contno ='"+fm.all('ContNo').value+"'";
	var contstrQueryResult =  easyQueryVer3(sqll, 1, 1, 1);
	  if (!contstrQueryResult)
	  {
	    alert("此保单不存在！");
	    fm.all('ContNo').value='';
	    return false;
	   }
//	  var sql2 ="select '1' from laascription  where AscripState ='3' and contno ='"+fm.all('ContNo').value+"' ";
//		 var contstrQueryResult1 = easyQueryVer3(sql2,1,1,1); 
//		 if(contstrQueryResult1){
//			 alert("此保单已经归属确认！")
//			 fm.all('ContNo').value='';
//			 return false;
//		 }  
	var sql="select a.contno,a.agentcode,a.agentcom,a.signdate,case when a.customgetpoldate is null then a.customgetpoldate else a.getpoldate end " +
			" from lccont a ,laagent b  where 1=1 and b.agentcode =a.agentcode" +
			" and a.salechnl in ('14','15') and b.agentstate >=6 and a.grpcontno ='00000000000000000000'"+
			"  and a.stateflag<>'3' and a.stateflag<>'2' and a.contno ='"+fm.all('ContNo').value+"'  "+
			"  and ((a.uwflag <> 'a' and a.uwflag <> '8' and a.uwflag <> '2' and  a.uwflag <> '1') or a.uwflag is null) "+
			" and not exists (select 'X' from lcrnewstatelog where newcontno = a.contno)"; 
	    
   var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("此保单不是离职人员下的保单！");
    fm.all('ContNo').value='';
    return false;
   }
   //查询保单所属的业务员
   else
   {
     var ttArr = new Array();
     ttArr = decodeEasyQueryResult(strQueryResult);
     var tcontno = ttArr[0][0];
     var tagentcode =ttArr[0][1];
     var tagentcom =ttArr[0][2];
     fm.all('ContNo').value = ttArr[0][0];
     fm.all('AgentOld').value =ttArr[0][1];
     fm.all('AgentCom').value = ttArr[0][2];
     //fm.all('AgentComOld').value = ttArr[0][2]];
    // fm.all('AgentComOldName').value = ttArr[0][3];
    // fm.all('AgentComOld').value = ttArr[0][2];
     //fm.all('AscriptionDate').value=ttArr[0][4];
     var tsql = "select getUniteCode(a.agentcode),a.name,(select branchattr from labranchgroup where agentgroup=a.agentgroup),"
      +" a.outworkdate,a.managecom,a.agentgroup"
      +"  from laagent a where agentcode='"+tagentcode+"' ";
     var strQueryResult = easyQueryVer3(tsql, 1, 1, 1);
     if(!strQueryResult)
     {
      alert("保单所属的代理人不存在！");
      fm.all('ContNo').value='';
      return false;
     }
     else
     {
       var tttArr1 = new Array();
       tttArr1 = decodeEasyQueryResult(strQueryResult);
       fm.all('GroupAgentOld').value = tttArr1[0][0];
       fm.all('PreName').value = tttArr1[0][1];
       fm.all('Branchattr').value = tttArr1[0][2];
       fm.all('OutWorkDate').value = tttArr1[0][3];
       fm.all('ManageCom').value = tttArr1[0][4];
       fm.all('OldAgentGroup').value =tttArr1[0][5];
     }
   }
  queryAgentCont(fm.all('AgentOld').value);
   
}

function checkCount1()
{
	var sqll = "select grpcontno from lcgrpcont where grpcontno ='"+fm.all('GrpContNo').value+"'";
	var contstrQueryResult =  easyQueryVer3(sqll, 1, 1, 1);
	  if (!contstrQueryResult)
	  {
	    alert("此保单不存在！");
	    fm.all('GrpContNo').value='';
	    return false;
	   }
//	 var sql2 ="select '1' from laascription  where AscripState ='3' and grpcontno ='"+fm.all('GrpContNo').value+"' ";
//	 var contstrQueryResult1 = easyQueryVer3(sql2,1,1,1); 
//	 if(contstrQueryResult1){
//		 alert("此保单已经归属确认！")
//		 fm.all('GrpContNo').value='';
//		 return false;
//	 }
	var sql="select a.grpcontno,a.agentcode,a.agentcom,a.signdate, case when a.customgetpoldate is null then a.customgetpoldate else a.getpoldate end " +
			" from lcgrpcont a ,laagent b  where 1=1 and a.agentcode =b.agentcode and b.agentstate >=6  and a.salechnl in ('14','15') "
	     +"  and a.stateflag<>'3' and a.stateflag<>'2'and a.grpcontno ='"+fm.all('GrpContNo').value+"' "
	     +"  and ((a.uwflag <> 'a' and a.uwflag <> '8' and a.uwflag <> '2' and a.uwflag <> '1') or a.uwflag is null) "
	     +" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = a.grpcontno)" 
	      //+ getWherePart('AgentCode','AgentOld') ;
   var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert("此保单不是离职人员下的保单！");
    fm.all('ContNo').value='';
    return false;
   }
   //查询保单所属的业务员
   else
   {
     var ttArr = new Array();
     
     
     ttArr = decodeEasyQueryResult(strQueryResult);
     var tgrpcontno = ttArr[0][0];
     var tagentcode =ttArr[0][1];
     var tagentcom =ttArr[0][2];
     fm.all('GrpContNo').value = ttArr[0][0];
     fm.all('AgentOld').value =ttArr[0][1];
     fm.all('AgentCom').value = ttArr[0][2];
     //fm.all('AgentComOld').value = ttArr[0][2]];
    // fm.all('AgentComOldName').value = ttArr[0][3];
    // fm.all('AgentComOld').value = ttArr[0][2];
     //fm.all('AscriptionDate').value=ttArr[0][4];
     var tsql = "select getUniteCode(a.agentcode),a.name,(select branchattr from labranchgroup where agentgroup=a.agentgroup),"
      +" a.outworkdate,a.managecom,a.agentgroup "
      +"  from laagent a where agentcode='"+tagentcode+"' ";
     var strQueryResult = easyQueryVer3(tsql, 1, 1, 1);
     if(!strQueryResult)
     {
      alert("保单所属的代理人不存在！");
      fm.all('GrpContNo').value='';
      return false;
     }
     else
     {
       var tttArr1 = new Array();
       tttArr1 = decodeEasyQueryResult(strQueryResult);
       fm.all('GroupAgentOld').value = tttArr1[0][0];
       fm.all('PreName').value = tttArr1[0][1];
       fm.all('Branchattr').value = tttArr1[0][2];
       fm.all('OutWorkDate').value = tttArr1[0][3];
       fm.all('ManageCom').value = tttArr1[0][4];
       fm.all('OldAgentGroup').value = tttArr1[0][5];
     }
   }
  queryAgentCont(fm.all('AgentOld').value);
   
}
 	
//提交前的校验、计算  
function beforeSubmit()
{
  if (((fm.all('ContNo').value == '')||(fm.all('ContNo').value == null))&&((fm.all('GrpContNo').value == '')||(fm.all('GrpContNo').value == null)))
  {
  	alert("团单号与个单号请选择一个！");
  	fm.all('ContNo').focus();
  	return false;
  }
 if ((fm.all('GroupAgentNew').value == '')||(fm.all('GroupAgentNew').value == null))
  {
  	alert("请输入新互动专员代码！");
  	fm.all('GroupAgentNew').focus();
  	return false;
  }
 if((fm.all('GroupAgentOld').value =='')||(fm.all('GroupAgentOld').value == null))
 {
	 alert("原互动专员代码不能为空");
	 return false;
 }
 if((fm.all('OutWorkDate').value =='')||(fm.all('OutWorkDate').value == null))
 {
	 alert("离职日期不能为空");
	 return false;
 }
 
  if(fm.all('AscriptionDate').value <= fm.all('OutWorkDate').value)
  {
    alert("归属日期必须大于人员的离职日期！");
  	fm.all('AscriptionDate').focus();
  	return false;
  }
  if((fm.all('AscriptionDate').value >= fm.all('MaxCalDate').value)&&(fm.all('MaxCalDate').value!=''))
  {
    alert("归属日期必须小于新人员下次计算薪资月"+fm.all('MaxCalDate').value+" ！");
  	fm.all('AscriptionDate').focus();
  	return false;
  }   
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
	parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
  	parent.fraMain.rows = "0,0,0,82,*";
  }
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
	if(!beforeSubmit())
	  {
	   return false;
	  }
	
  if(fm.GroupAgentOld.value != null && fm.GroupAgentOld.value != ""){
		var tYAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+fm.GroupAgentOld.value+"' ";
		var strQueryResult = easyExecSql(tYAgentCodeSQL);
		if(!strQueryResult){
			alert("获取业务员编码失败！");
			return false;
		}
		fm.AgentOld.value = strQueryResult[0][0];
	}
  if(fm.GroupAgentNew.value != null && fm.GroupAgentNew.value != ""){
  	var tYNAgentCodeSQL = "select agentcode from laagent where groupagentcode = '"+fm.GroupAgentNew.value+"' ";
	var strNQueryResult = easyExecSql(tYNAgentCodeSQL);
	if(!strNQueryResult){
		alert("获取业务员编码失败！");
		return false;
	}
	fm.AgentNew.value = strNQueryResult[0][0];
  }
  
 
  if(fm.all('AgentOld').value ==fm.all('AgentNew').value)
  {
  	  	alert("原代理人不能与新代理人相同!!");
  	  	return false;
  }
  //添加注释 modify by Elsa 20101122
  //if(!checkCostcenter()){
  // return false;
  //}
  
  if(!checkContNoGrp()){
   return false;
  }
   
  if(fm.all('AgentCom').value!=null && fm.all('AgentCom').value!="")
  {
	  mOperate ="INTER||MAIN";
  }else
  {
	  mOperate="DIRECT||MAIN"; 
  }
  
  submitForm();
}           

function checkContNoGrp()
{
  if(fm.all('ContNo').value!=null&&fm.all('ContNo').value!=""
   &&fm.all('GrpContNo').value!=null&&fm.all('GrpContNo').value!="")
  {
    alert("个单号和团单号只能存在一个，不能同时存在！");
    return false;
  }
  return true;
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function queryAgentCont(pmAgentCode)
{
	var tSQL = "";
	tSQL =" select signdate,"
	    +" case when customgetpoldate is null then customgetpoldate else getpoldate end "
	    +" from  lccont where  agentcode='"+pmAgentCode+"' and grpcontno='00000000000000000000' "
	    +" and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and stateflag<>'2' "
	    +" and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2' and uwflag<>'1') or uwflag is null) "
	    +" and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;
	    
	tSQL+=" union signdate,"
	    +" case when customgetpoldate is null then customgetpoldate else getpoldate end "
	    +" from  lccont where  agentcode='"+pmAgentCode+"'  and grpcontno='00000000000000000000'  "
	    +" and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and stateflag<>'2' "
	    +" and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'1' and uwflag<>'2') or uwflag is null) "
	    +" and not exists (select 'X' from lcrnewstatelog where newcontno = lccont.contno)" ;		
	 
	 tSQL +=" union signdate,"
	     +" case when customgetpoldate is null then customgetpoldate else getpoldate end   "
	     +" from  lcgrpcont where  agentcode='"+pmAgentCode+"' "
	     +" and   (appflag<>'1' or appflag is null ) and stateflag<>'3' and stateflag<>'2' "
	     +" and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2'  and uwflag<>'1') or uwflag is null) "
	     +" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = lcgrpcont.grpcontno)" ;
	     
		tSQL +=" union signdate,"
	     +" case when customgetpoldate is null then customgetpoldate else getpoldate end   "
	     +" from  lcgrpcont where  agentcode='"+pmAgentCode+"' "
	     +" and  (customgetpoldate is null or getpoldate is null) and stateflag<>'3' and stateflag<>'2' "
	     +" and ((uwflag<>'a' and uwflag<>'8' and uwflag<>'2'  and uwflag<>'1') or uwflag is null) "
	     +" and not exists (select 'X' from lcrnewstatelog where newgrpcontno = lcgrpcont.grpcontno)" ;
	
	
	//执行查询并返回结果
	
}


