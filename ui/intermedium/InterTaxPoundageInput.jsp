<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAAChargePackageRateInput.jsp
//程序功能：银代手续费比例录入界面
//创建时间：2008-01-24
//创建人  ：Huxl

%>
<%
  
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String ManageCom=tG.ManageCom;
  System.out.println(ManageCom);
  String BranchType2=request.getParameter("BranchType2");
  if (BranchType2==null || BranchType2.equals(""))
      {
         BranchType2="";
      }
%>
<%@page contentType="text/html;charset=GBK" %>

<head>
<script>
 var  msql2=" 1 ";
 
 var  MngSQL=" 1 and length(trim(comcode)) in (#4#,#8#)";
 var StrSql=" 1 and riskcode  in (select riskcode from lmriskapp where risktype4 =#4# and taxoptimal=#Y# )";

 </script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>      
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css> 
   <%@include file="./InterTaxPoundageInit.jsp"%>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <SCRIPT src="InterTaxPoundageInput.js"></SCRIPT>
</head>

<body  onload="initForm();initElementtype();" >
 <form action="./InterTaxPoundageSave.jsp" method=post name=fm target="fraSubmit">
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 
    查询条件 
    </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
  <table class=common >
   	<TD  class= title>
   	管理机构
   	</TD>
    <TD  class= input>
      <Input class="codeno" name=ManageCom verify="管理机构|notnull"
         ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,MngSQL,1);"  
         onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,MngSQL,1);"
           ><input class=codename name=ManageComName readonly=true elementtype=nacessary>
      </TD>
   	<td  class= title> 
   	代理机构
   	 </td>
    <td  class= input> 
    <input class=codeno name=AgentCom 	
         ondblclick="return getAgentCom(this,Name);"
         onkeyup="return getAgentCom(this,Name);" 
         ><input class="codename" name = "Name"  readOnly > 
         </td> 
      </TR>       
   	
   	<tr class=common>
   	  <td class=title>
   	   险种
   	  </td>
   	  <td class= input>
     	<Input class=codeno name=RiskCode verify="险种|code:RiskCode"
            ondblclick="return showCodeList('RiskCode',[this,RiskCodename],[0,1],null,StrSql,1,1);" 
            onkeyup="return showCodeListKey('RiskCode',[this,RiskCodename],[0,1],null,StrSql,1,1);"  ><input class=codename name=RiskCodename readonly=true  >
    </td> 
<!--   	  <td class=title>-->
<!--   	 保险期间   -->
<!--   	  </td>-->
<!--   	   <TD  class= input>-->
<!--            <Input class= "common"  name=Year verify="保险期间|value<=99&value>=0&int" >-->
<!--          </TD>-->
          <td  class= title> 保单年度 </td>
          <TD  class= input> <Input class= "common" name=CurYear verify="保单年度|value>=0&int"></TD>
   	</tr>
    <tr class=common>		
           <td  class= title>手续费比率</td>
          <TD  class= input> <Input class= "common" name=Rate  verify="手续费比率|value<=1&value>=0&num"></TD>      		
      <td  class= title> 
      保费类型
      </td>
        <td class=input>
			<Input class="codeno" name=F05Code
			 CodeData="0|^04|帐户保费|^05|风险保费"
			 ondblclick="return showCodeListEx('F05list',[this,F05Name],[0,1]);"
			 onkeyup="return showCodeListKeyEx('F05list',[this,F05Name],[0,1]);"
			 onchange=""><input class=codename name=F05Name readonly=true>
						</td>
      </tr>
      <tr class=common>
        <TD  class= title>
           缴费方式
          </TD>
          <TD  class= input>
             <Input class="codeno" name=PayintvCode  CodeData="0|^12|年缴|^6|半年缴|^3|季缴|^1|月缴|^0|趸缴" 
             ondblclick="return showCodeListEx('payintvlist',[this,PayintvName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('payintvlist',[this,PayintvName],[0,1]);" onchange=""
             ><input class=codename name=PayintvName  readonly=true ></TD>  
          </TD>
      </tr>
  </table>
  </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="新  增" class=cssButton onclick="return DoInsert();">
     		<input type=button value="修  改" class=cssButton onclick="return DoSave();"> 
    		<input type=button value="删  除" class=cssButton onclick="return DoDel();">
    		<input type=button value="重  置" class=cssButton onclick="return DoReset();"> 
    		<input type=button value="下  载" class=cssButton onclick="doDownLoad();"> 
    	</td>
   </tr>      
  </table>
					<br/><font color="#ff0000">(注：手续费比例必须为0-1的数，如23%，记为0.23!保险期间为0-99的数字，99表示终身) 
					</font>
<!--					<br/><font color="#ff0000">(注：保险期间暂不作为手续费计算维度) -->
<!--					</font>-->
  <table>
  	<tr>
    	<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
			</td>
    	<td class= titleImg>手续费计算比率设置
    	
    	</td>
		</tr>
  </table>

  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanSetGrid" ></span>
     </td>
    </tr>    
   </table>      
   <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
   <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
   <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
   <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div>  
  <Input class="readonly" type=hidden name=diskimporttype>
  <input type=hidden id="sql_where" name="sql_where" >
  <Input type=hidden name=branchtype2 value ='01'> 
  <Input type=hidden id="BranchType" name=BranchType value='5'>
  <input type=hidden id="fmAction" name="fmAction">   
  <input type="hidden" name=querySql value="">
  <input type=hidden name=LogManagecom value='<%=ManageCom%>'>
 </form>
 <form action="./LADiskImportTaxRateSave.jsp" method=post name=fm2 enctype="multipart/form-data" target="fraSubmit">
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportFile);">
    		</td>
    		<td class= titleImg>手续费比率磁盘导入</td>
    		
    	</tr>
  </table>
  <div id="divImportFile" style="display:''">   

  <table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">文件名</TD>
				<TD class=input>
					<Input type="file" class= "common" style="width:300px" name="FileName" id="FileName"/>
				</TD><td >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()"></td>
			</TR>
  </table>
				  <Input type=button class=cssButton name="goDiskImport" value="导  入" id="goDiskImport"  onclick="diskImport()">
				  <Input type=button class=cssButton value="重  置" onclick="clearImport()">

<!--
 <Input type=button class=cssButton name="goDiskImport" value="磁盘导入" id="goDiskImport"  onclick="diskImport()">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Input type=button class=cssButton name="goDiskImport" value="磁盘导入模版下载" id="goDiskImport" class=cssButton onclick="moduleDownload()">
-->
<table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divImportQuery);">
    		</td>
    		<td class= titleImg>手续费比率磁盘导入日志查询</td>
    		
    	</tr>
  </table>
  <div id="divImportQuery" style="display:''">
  	<table class=common>
			<TR class=common>
				<TD  class=title style="width:100px">导入文件批次</TD>
				<TD class=input>
					<Input type="text" class= "common"  name="FileImportNo"/>
				</TD>
				
				<TD  class=title style="width:100px">
				查询类型
				</td>
				<td class=input>
				<Input class="codeno" name=queryType  CodeData="0|^0|全部|^1|导入错误行|^2|导入成功行" ondblclick="return showCodeListEx('querytypelist',[this,queryName],[0,1]);" onkeyup="return showCodeListKeyEx('querytypelist',[this,queryName],[0,1]);" onchange=""><input class=codename name=queryName  readonly=true >
				</TD>
				<td></td>
			</TR>
   </table>
  <Input type=button class=cssButton value="查  询" onclick="queryImportResult()"/>
  <div id="divImportResultGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanImportResultGrid" ></span>
     </td>
    </tr>    
   </table>  
      <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage2.firstPage();">
      <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage2.previousPage();">
      <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage2.nextPage();">
      <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage2.lastPage();">
  </div>
  </div>  
 </div>
  <Input type=hidden name=branchtype2> 
  <Input type=hidden  name=BranchType >
  <Input type=hidden name=diskimporttype>
 </form>  
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
</html>




