<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：ALAAscriptionInput.jsp
//程序功能：
//创建日期：2002-08-16 16:25:40
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<head >
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="InterALAAscriptionInit.jsp"%>
    <SCRIPT src="InterALAAscriptionInput.js"></SCRIPT>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
<title></title>
</head>
<body  onload="initForm();initElementtype();">
  <form action="./InterALAAscriptionSave.jsp" method=post name=fm target="fraSubmit">  
  <!--    <%@include file="../agent/AgentOp2.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>-->
   <div id="inputButton" style="display: """>
	<table class="common" align=center>
		<tr align=right>
			<td class=button>
				&nbsp;&nbsp;
			</td>		
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="重  置"  TYPE=button onclick="return initForm();">
			</td>
			<td class=button width="10%">
				<INPUT class=cssButton VALUE="手工归属"  TYPE=button onclick="return addClick();">
			</td>	
		</tr>
	</table>
</div>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAscription);">
    <td class=titleImg>
    保单归属信息
    </td>
    </tr>
    </table>
  <Div  id= "divLAAscription" style= "display: ''">      
    <table  class= common>
    <TR>
      <TD  class= title>
           个单号
          </TD>          
          <TD  class= input>
            <Input class=common name=ContNo onchange="return checkCount();">
          </TD> 
            <TD  class= title>
           团单号
          </TD>          
          <TD  class= input>
            <Input class=common name=GrpContNo onchange="return checkCount1();">
          </TD>
    </TR>
       <TR  class= common> 
       
          <TD  class= title>
            原互动专员代码
          </TD>          
          <TD  class= input>
            <Input class=common name=AgentOld  verify="原代理人编码|notnull&len=10" type=hidden>
            <Input class='readonly' readonly name=GroupAgentOld  verify="原代理人编码|notnull&len=10" >
          </TD>  
          <td  class= title>
		        原互动专员姓名
		</td>
        <td  class= input>
		  <input name=PreName class='readonly' readonly >
		</td>   
		</TR>
			
		<TR class=common>
		 <TD class=title>
		  原销售机构代码
		</TD>
		<TD  class= input>
          <input class='readonly' name =Branchattr readOnly>
        </TD>
        <TD class=title>原互动专员离职日期</TD>
        <TD class=input>
        	<input class='readonly' name=OutWorkDate readOnly>
        </TD>
        
		</TR>     
		
		<TR  class= common> 
       <TD  class= title>
            管理机构
          </TD>          
          <TD  class= input>
            <Input class='readonly' readonly name=ManageCom >
          </TD>  
          <td  class= title>
		        中介机构
		</td>
        <td  class= input>
		  <input name=AgentCom class='readonly' readonly >
		</td>   
		</TR>
		  
		<TR  class= common> 
		  
		 <td  class= title>
		  新互动专员代码
		</td>
        <td  class= input>
         <input name=AgentNew class=common onchange="return checkname();"  type=hidden>
		 <input name=GroupAgentNew class=common onchange="return checkname();" elementtype=nacessary>
		</td>   
          <TD  class= title>
                  新互动专员姓名
          </TD>          
          <TD  class= input>
            <Input class='readonly' readonly name=NameNew >
          </TD>  
     
	</TR>  
		  
     <TR  class= common> 
        <td  class= title> 
		 归属日期
		</td>
        <td  class= input> 
		  <Input class= "coolDatePicker" dateFormat="short" name=AscriptionDate verify="归属日期|DATE"  elementtype=nacessary> 
		</td> 
      <td  class= title>
		    操作员
		  </td>
      <td  class= input>
		    <input name=Operator class='readonly' readonly >
		  </td>   
                                   
       </TR> 
         
    </table>  
	 
	<p><font  style="font-size: 18px;"; color='red'>注：只变更离职人员名下的保单，归属日期必须大于人员的离职日期及小于新人员下次计算薪资月15号。 </font></p>
    <p><font  style="font-size: 18px;"; color='red'>注：未签单或未回执回销的保单不能做保单分配。</font></p>
    <p><Strong  style="font-size: 20px; color: red" >注：手工归属的保单会直接变更互动专员，请谨慎操作。</Strong></p>  
   </div>
     <input type=hidden name=OldAgentGroup value=''>
     <input type=hidden name=NeWAgentGroup value=''>
     <input type=hidden name=hideOperate value=''>
     <input type=hidden name=AgentComNew value=''>
     <input type=hidden name=BranchType value=''>
     <input type=hidden name=BranchType2 value=''>
     <input type=hidden name=AscripNo value=''>
      <input type=hidden name=State value=''>
     <input type=hidden name=AgentNew1 value=''>
     <input type=hidden name=ContNo1 value=''>
     <input type=hidden name=GrpContNo1 value=''>
     <input type =hidden name=MaxCalDate value='' >
     <input type=hidden class=Common name=querySql > 
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
