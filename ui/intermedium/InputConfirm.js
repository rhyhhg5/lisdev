//因为在合同和被保人录入时候会调用此函数，所以写在文件中调用


/*********************************************************************
 *  把合同所有信息录入结束确认
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function inputConfirm(wFlag)
{   
    if(fm.all('ProposalContNo').value == "")
    {
        alert("未查询出合同信息,不容许您进行 本操作！");
        return false;
    }

    fm.all('fmAction').value="Confirm";

    var sql = "select ProcessID, ActivityID "
    + "from LWMission "
    + "where MissionID = '" + tMissionID + "' ";
    var rs = easyExecSql(sql);
    if(rs)
    {
        if(rs[0][1] != tActivityID)
        {
            alert("不能重复执行过本操作");
            return false;
        }
    }

    fm.WorkFlowFlag.value = tActivityID;
    fm.MissionID.value = tMissionID;
    fm.SubMissionID.value = tSubMissionID;

    //渠道校验
    if(!checkSaleChnl())
    {
        return false;
    }
    //险种校验
    if(!checkRiskInfo())
    {
        return false;
    }
    
    //校验集团交叉业务销售渠道
    if(!checkCrsBussSaleChnl())
    {
    	return false;
    }
    
    //校验被保人职业类别与职业代码
    if(!checkOccTypeOccCode())
    {
    	return false;
    }
    
    // 校验被保人告知相关信息
    if(!chkImpartInfo())
    {
        return false;
    }
    
    //校验被保人唯一，主险唯一，存在附加险必须存在主险的情况
    if(!checkInsuredAndRisk()){
    	return false;
    }
    
    if(!checkGD()){
    	return false;
    }
    
    if(!checkInitFeeRate()){
    	return false;
    }
    
    if(!checkFeeRate()){
    	return false;
    }
    
    if(!checkInitFeeRate1()){
    	return false;
    }
    
    //校验代理机构及代理机构业务员
    if(!checkAgentComAndSaleCdoe()){
    	return false;
    }
    
    if(!checkAgentCode()){
    	return false;
    }
    
    if(!checkAppnt()){
    	return false;
    }
    
    if(!checkInsured()){
    	return false;
    }
    
    if(!checkPayMethod()){
    	return false;
    }

    if (wFlag ==1 ) //录入完毕确认
    {
    }
    else if (wFlag ==2)
    {
        if(!chenkBnfBlack()){
           return false;
        }
        
        //校验广东长期险是否调阅录音录像
        if(!checkVideo()){
          return false;
        }
        
        var qurSql="select bussno from es_issuedoc where bussno like '"+fm.PrtNo.value+"%' and ((status='1' and stateflag='1') or (status='0' and stateflag='2')  )";
        var arrqurSql=easyExecSql(qurSql);
        if(arrqurSql)
        {
//            if (!confirm("该单还有扫描修改申请待审批，确认要继续吗？"))
//            {
//                return;
//            }
            alert("保单有扫描修改申请尚未处理，须修改完毕后再次提交。");
            return;
        }

        var strsql1=" select idtype,idno,name,RelationToMainInsured,RelationToAppnt from lcinsured where prtno='"+fm.PrtNo.value+"' and GrpContNo = '00000000000000000000'";
        var arr=easyExecSql(strsql1);

        for(var i=0;i<arr.length;i++)
        {

            if(arr[i][0]==0&arr[i][1]=="")
            {
                alert("被保险人"+arr[i][2]+"证件类型为身份证,但是证件号码为空,请首先将证件类型修改为其它!");
                return false;
            }
            if(arr[i][3]=="")
            {
                alert("被保险人"+arr[i][2]+"与主被保人的关系为空，请进行修改！");
                return false;
            }
            if(arr[i][4]==""&arr[i][3]==00)
            {
                alert("被保险人"+arr[i][2]+"与投保人的关系为空，请进行修改！");
                return false;
            }
        }
        
				//黑名单校验 
        if(!checkBlackName()){
           	return false;
        }
        
        
        var tPrtNo = fm.PrtNo.value;
        if(!chkAppAcc(tPrtNo))
        {
            return false;
        }
        
        approvefalg="2";
        
    }
    else if (wFlag ==3)
    {
        if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [复核修改完毕] 确认！");
            return;
        }
    }
    else if(wFlag == 4)
    {
        if(fm.all('ProposalContNo').value == "")
        {
            alert("未查询出合同信息,不容许您进行 [修改完毕] 确认！");
            return;
        }
    }
    else
        return;
    
    if(!valiAddress()){
    	return false;
    }

    var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo = window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

    var fmAction = fm.action;
    fm.action = "../app/InputConfirm.jsp";
    fm.submit(); //提交

    fm.action = fmAction;
    //alert();
    //top.window.close();
}


//初始化全局变量
function initGlobalvary()
{
  var sql = "select ProcessID, ActivityID "
          + "from LWMission "
          + "where MissionID = '" + tMissionID + "' ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    tProcessID = rs[0][0];
    tActivityID = rs[0][1];
  }
}

/**
 * 校验销售渠道。
 * 当渠道为：04－银行代理；AgentCom不能为空。
 */
function checkSaleChnl()
{
    var saleChnlSQL = "select 1 from LCCont where PrtNo = '" + fm.PrtNo.value 
        + "' and SaleChnl = '04' and (AgentCom is null or AgentCom = '')";
    var result = easyExecSql(saleChnlSQL);
    if(result)
    {
        alert("销售渠道为：04－银行代理，银行网点为空。");
        return false;
    }
    //业务员和销售渠道的校验
    // var agentCodeSql = "select 1 from LAAgent a, LDCode1 b, LCCont c "
    //     + "where c.PrtNo = '" + fm.PrtNo.value + "' "
    //     + "and a.AgentState < '06' and b.CodeType = 'salechnl' "
    //     + "and a.AgentCode = c.AgentCode and b.Code = c.SaleChnl "
    //     + "and a.BranchType = b.Code1 and a.BranchType2 = b.CodeName "
    //     + "with ur";
    // var arr = easyExecSql(agentCodeSql);
    // if(!arr){
    //     alert("业务员和销售渠道不相符！");
    //     return false;
    // }

    return true;
}

//险种的校验
function checkRiskInfo()
{
    //该分公司险种是否停售校验
    var riskEndSale = "select 1 from LCPol a, LMRiskApp b, LDCode c where a.PrtNo = '" 
        + fm.PrtNo.value + "' and b.RiskType4 = '4' and c.CodeType = 'UniversalSale' "
        + "and a.RiskCode = b.RiskCode and c.Code = substr(a.ManageCom, 1, 4) ";
    var result = easyExecSql(riskEndSale);
    if(result)
    {
        alert("该分公司万能险已停售！");
        return false;
    }
    
    //保险期间单位为空的校验
    var riskInsu = "select 1 from LCPol where PrtNo = '" 
        + fm.PrtNo.value + "' and (InsuYearFlag is null or InsuYearFlag = '')";
    result = easyExecSql(riskInsu);
    if(result)
    {
        alert("保险期间单位为空，请修改！");
        return false;
    }
    
    //健康人生C录入了趸缴就不予录入通过。
    // var strSQL = "select payintv from lcpol where riskcode = '331301'";
    var strSQL = "select payintv from lcpol where riskcode = '331301' and prtno = '"+fm.PrtNo.value+"'";
    
    var arrResult = easyExecSql(strSQL);
    if(arrResult){
    	if(arrResult[0][0] == '0'){
    		alert("健康人生C录入了趸缴不予录入通过,请修改保费频次");
    		return false;
    	}
    }
    
    return true;
}

//校验被保人职业类别与职业代码
function checkOccTypeOccCode()
{
	var strSql = "select distinct OccupationType,OccupationCode from lcinsured "
				+ " where prtno = '" + fm.PrtNo.value + "'";
	var arrResult = easyExecSql(strSql);
	var i = 0;
	
	if(arrResult != null) {
		for(i = 0; i < arrResult.length; i++)
		{
			var OccupationType = arrResult[i][0];
			var OccupationCode = arrResult[i][1];
			
			if(OccupationType != null && OccupationType != "")
			{
				if(!CheckOccupationType(OccupationType))
				{
					return false;
				}
			}
			
			if(OccupationCode != null && OccupationCode != "")
			{
				
				if(!CheckOccupationCode(OccupationType,OccupationCode))
				{
					return false;
				}
				
			}
		}
	}
	return true;
}

//校验职业代码
function CheckOccupationCode(Type,Code)
{
	var strSql = "select 1 from ldoccupation where OccupationCode = '" + Code +"' and OccupationType = '" + Type + "'";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别和职业代码与系统描述不一致，请查看！");
		return false
	} 
	return true;
}

//校验职业类别
function CheckOccupationType(Type)
{
	var strSql = "select 1 from dual where '" + Type + "' in (select distinct occupationtype from ldoccupation)";
	var arrResult = easyExecSql(strSql);
	if(arrResult == null)
	{
		alert("被保人有职业类别与系统描述不一致，请查看！\n系统规定职业类别为半角数字的1至6");
		return false
	} 
	return true;
}

/**
 * 校验投保人帐户余额是否足额
 * 不通过约定：
 * 1、该投保单中“缴费模式”选择“01 - 从投保人帐户缴纳首期保费”；并且投保人帐户中可用剩余金额，小于该单首期保费。
 * 2、该投保单中“缴费模式”选择“01 - 从投保人帐户缴纳首期保费”；并且帐户信息出现多条。
 */
function chkAppAcc(cPrtNo)
{
    var tStrSql = ""
        + " select "
        + " tmp.ContNo, tmp.AppntNo, tmp.AppntName, tmp.SumPrem, nvl(sum(lcaa.AccGetMoney), 0) SumAccGetMoney "
        + " from "
        + " ( "
        + " select "
        + " lcc.ContNo, lcc.AppntNo, lcc.AppntName, nvl(sum(lcp.Prem), 0) SumPrem  "
        + " from LCCont lcc "
        + " inner join LCPol lcp on lcc.ContNo = lcp.ContNo "
        + " where 1 = 1 "
        + " and lcc.ContType = '1' "
        + " and lcc.PayMethod = '01' "
        + " and lcc.PrtNo = '" + cPrtNo + "' "
        + " group by lcc.ContNo, lcc.AppntNo, lcc.AppntName "
        + " ) as tmp "
        + " left join LCAppAcc lcaa on lcaa.CustomerNo = tmp.AppntNo and lcaa.State = '1' "
        + " group by tmp.ContNo, tmp.AppntNo, tmp.AppntName, tmp.SumPrem "
        ;
    var arrAppAccInfo = easyExecSql(tStrSql);
    if(arrAppAccInfo != null)
    {
        if(arrAppAccInfo.length >= 2)
        {
            alert("投保人帐户信息出现异常。");
            return false;
        }
        var tAppAccInfo = arrAppAccInfo[0];
        var tSumPrem = tAppAccInfo[3];
        var tSumAccGetMoney = tAppAccInfo[4];
        if(Subtr(tSumPrem, tSumAccGetMoney) > 0)
        {
            alert("该单投保人帐户可用余额不足。该单首期保费[" + tSumPrem + "]，投保人帐户余额[" + tSumAccGetMoney + "]");
            return false;
        }
    }
    
    return true;
}

function Subtr(arg1, arg2)
{
    var r1, r2, m, n;
    try{r1=arg1.toString().split(".")[1].length}catch(e){r1=0}
    try{r2=arg2.toString().split(".")[1].length}catch(e){r2=0}
    m=Math.pow(10,Math.max(r1,r2));
    n=(r1>=r2)?r1:r2;
    return ((arg1*m-arg2*m)/m).toFixed(n);
}


function chkImpartInfo()
{
    if(!checkGZ19())
    {
        return false;
    }
    return true;
}


function checkGZ19()
{
    var tPrtNo = fm.PrtNo.value;
    
	var strSql = "select insuredno,insuredappage from lcpol where prtno = '"+tPrtNo+"' and insuredappage<18";
	var arr = easyExecSql(strSql);
	if(arr)
	{
		for(var i=0;i<arr.length;i++)
        {
			var CustomerimpartSql = "select * from lccustomerimpart where prtno = '"+tPrtNo+"' and customerno = '"+arr[i][0]+"' and impartver = '001' and impartcode='250'";
			var tArrCustomer = easyExecSql(CustomerimpartSql);
			if(!tArrCustomer)
            {
				alert("被保人（"+arr[i][0]+"）年龄为"+arr[i][1]+"岁，请填写未成年被保人告知项！");
				return false;
			}
		}
	}
	return true;
}

//联系地址不能全为数字
function checkPostalAddressID(){
	var tPrtNo = fm.PrtNo.value;
	var strSql = "select PostalAddress from lcaddress lcd inner join lcappnt lca on lca.appntno = lcd.customerno and lca.addressno = lcd.addressno where lca.prtno = '"+tPrtNo+"' ";
	var arr = easyExecSql(strSql);
	if(arr){
		for(var i=0;i<arr.length;i++)
        {
			if(isNumeric(arr[i][0])){
				alert("投保人联系地址全为数字，请核对。");
				return false;
			}
		}
	}else{
		alert("投保人联系地址不能为空。");
		return false;
	}
	return true;
}

//投保人联系电话不能与其他4个投保人的联系电话一样
function checkPhone()
{
//alert(fm.PhoneInputID.value);
	var tPrtNo = fm.PrtNo.value;
	var strSql = "select 1 from lcaddress a  inner join lcappnt b  on b.appntno = a.customerno and a.addressno = b.addressno where a.phone in (select phone from lcappnt c ,lcaddress d where c.appntno = d.customerno and c.addressno = d.addressno and c.prtno = '"+tPrtNo+"') and b.prtno <>'"+tPrtNo+"'";
	var arr = easyExecSql(strSql);
	
	var j = 0;
	if(!arr){
	return true;
	}else{
	for(var i=0 ; i<arr.length ; i++)
	      {
	     
	        j ++;
	       
	      }
	      
	   if(j >= 4)
	   {
	   alert("投保人的联系电话已经与其他4个以上投保人联系电话重复，请投保人填写《联系信息确认登记表》，由录入人员审核并确认通过。");
	   //return false;
	   if(!confirm("是否审核并确认？"))
		{
			return false;
		}
	   }else{
	   		return true;
	   }
	}
	return true;
}
//校验身份证
function checkId()
{
   
	var tPrtNo = fm.PrtNo.value;
	var z = 0;
	
	var strSql = "select distinct appntno from lcappnt a where a.idno in (select b.idno from lcappnt b where b.prtno = '"+tPrtNo+"' and b.idtype = '0') and a.idtype = '0'";
	
	var arr = easyExecSql(strSql);
	if(arr){
		for(var i=0 ; i<arr.length ; i ++)
		{
		  z ++; 
		}
		if(z >= 2)
		{
		alert("投保人身份证与其他投保人身份证重复，请核对!");
		if(!confirm("是否审核并确认？"))
			{
				return false;
			}
		
		}
	}
	
	return true;
}

function checkGD(){
	var tPrtNo = fm.PrtNo.value;
	var strSql = "select 1 from lccont  where prtno = '"+tPrtNo+"' and managecom like '8644%'";	
	var arr = easyExecSql(strSql);
	if(arr){
		//投保人联系地址不能全为数字
	    if(!checkPostalAddressID()){
	    	return false;
	    }
	    //电话校验
	    if(!checkPhone()){
	      
	      return false;
	    }
	    //身份证校验
	    if(!checkId())
	    {
	      return false;
	    }
	}
	return true;
}

//对险种332601特殊处理
function checkInitFeeRate(){
	var tPrtNo = fm.PrtNo.value;
	var tSql = "select InitFeeRate from lcpol where prtno = '"+tPrtNo+"' and riskcode = '332601'";
	var arr = easyExecSql(tSql);
	if(arr){
		var tFeeRate = arr[0][0];
		if(!isNumeric(tFeeRate)){
			alert("初始费率的值必须为数字！");
			return false;
		}
		if(parseFloat(tFeeRate)<0 || parseFloat(tFeeRate)>5){
			alert("系统初始费率为["+parseFloat(tFeeRate)+"],险种332601的初始费率值必须在0-5之间！");
			return false;
		}
		var tFeeRate1 = tFeeRate.substring(0,tFeeRate.indexOf(".") + 2);
		if(parseFloat(tFeeRate1) != parseFloat(tFeeRate)){
			alert("系统初始费率为["+parseFloat(tFeeRate)+"],险种332601的初始费率值只能包含一位小数！");
			return false;
		}
				
	}
	return true;
}

//校验黑名单
function checkBlackName(){
	//黑名单校验投保人
	var sql1 =  "select appntname,idtype,idno from lcappnt where prtno='"+fm.PrtNo.value+"' ";
	var arrResult = easyExecSql(sql1);
	if(arrResult){
		var sqlblack  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
						"  and name='"+arrResult[0][0]+"' and idnotype='"+arrResult[0][1]+"' and idno='"+arrResult[0][2]+"' and type='0' "+
						"  union  "+
						"  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
						"  and name='"+arrResult[0][0]+"' and idno='"+arrResult[0][2]+"' and type='0' "+
						"  union   "+
						"  select 1 from lcblacklist where (idno is null or  idno='' or  idno='无')  and name='"+arrResult[0][0]+"' and type='0'";
		var arrResult1 = easyExecSql(sqlblack);
		if(arrResult1){
			if(!confirm("该保单投保人："+arrResult[0][0]+"，存在于黑名单中，确认要复核通过吗？")){
				return false;
			}
		}
	}
	//黑名单校验被保人
	var sql2 =  "select name, idtype, idno from lcinsured where prtno='"+fm.PrtNo.value+"'  ";
	var arrRes = easyExecSql(sql2);
	var tInsuNames = "";
	if(arrRes){
		for(var i = 0; i < arrRes.length; i++){
			var sqlblack2  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrRes[i][0]+"' and idnotype='"+arrRes[i][1]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrRes[i][0]+"' and idno='"+arrRes[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrRes[i][0]+"' and type='0'";
			
			var arrRes1 = easyExecSql(sqlblack2);
			if(arrRes1){
				if(tInsuNames!=""){
					tInsuNames = tInsuNames+ " , " + arrRes[i][0];
				}else{
					tInsuNames = tInsuNames + arrRes[i][0];
				}
			}
		}
	}
	if(tInsuNames != ""){
		if (!confirm("该保单被保人姓名："+tInsuNames+",存在于黑名单中，确认要复核通过吗？")){
			return false;
		}
	}
	//黑名单校验受益人	
	var sql3 =  "select distinct name, idtype, idno from lcbnf where contno=(select contno from lccont where prtno='"+fm.PrtNo.value+"')  ";
	var arrR = easyExecSql(sql3);
	var tBnfNames = "";
	if(arrR){
		for(var i = 0; i < arrR.length; i++){
			var sqlblack3  = "  select 1 from lcblacklist where idnotype is not null and idno is not null  "+
							 "  and name='"+arrR[i][0]+"' and idnotype='"+arrR[i][1]+"' and idno='"+arrR[i][2]+"' and type='0' "+
							 "  union  "+
							 "  select 1 from lcblacklist where (idnotype is null or idnotype='') and idno is not null   "+
							 "  and name='"+arrR[i][0]+"' and idno='"+arrR[i][2]+"' and type='0' "+
							 "  union   "+
							 "  select 1 from lcblacklist where  (idno is null or  idno='' or  idno='无')  and name='"+arrR[i][0]+"' and type='0' ";
			var arrR1 = easyExecSql(sqlblack3);
			if(arrR1){
				if(tBnfNames!=""){
					tBnfNames = tBnfNames+ " , "+arrR[i][0];
				}else{
					tBnfNames = tBnfNames + arrR[i][0];
				}
			}
		}
	}
	if(tBnfNames != ""){
		if (!confirm("该保单受益人："+tBnfNames+",存在于黑名单中，确认要复核通过吗？")){
			return false;
		}
	}
	return true;
}

function checkFeeRate(){   
	//var sql = "select 1 from lcpol lcp where  lcp.riskcode in ('332901','333001','334601') and lcp.prtno = '" + prtNo + "'";
	var sql = "select 1 from lcriskdutywrap lcp where lcp.prtno = '" + prtNo + "' and exists (select 1 from ldcode1 where codetype='changerate' and code=lcp.riskwrapcode)";
	var arrResult = easyExecSql(sql);
	sql = "select distinct code from ldcode1 where codetype='changerate' ";
	var riskResult = easyExecSql(sql);
	var riskWrap = "";
	if(riskResult){
	    for(var index = 0; index < riskResult.length; index++){
	        if(index==0){
		        riskWrap += riskResult[index][0];
		    }else{
		        riskWrap +="、"+riskResult[index][0];
		    }
	    }
	}
	if(arrResult != null){
		var sql2 = "select 1 from LCRiskFeeRate where prtno='" + prtNo + "'";
		var arrResult2 = easyExecSql(sql2);
		if(arrResult2 == null){
			alert("选择"+riskWrap+"套餐，需要进行费率比例录入");
			return false;
		}
	} else {
		var sql2 = "select 1 from LCRiskFeeRate where prtno='" + prtNo + "'";
		var arrResult2 = easyExecSql(sql2);
		if(arrResult2 != null){
			alert("未选择"+riskWrap+"套餐，无需进行费率比例录入，请进入费用比例录入界面对已录入比例进行删除");
			return false;
		}
	}
	return true;
}

function checkInitFeeRate1(){
	var tSql = "select Calfactorvalue from LCRiskDutyWrap where prtno = '"+fm.PrtNo.value+"' and calfactor = 'InitFeeRate' and riskwrapcode = 'WR0260'";
	var arrResult = easyExecSql(tSql);
	if(arrResult){
		var tInitFeeRate = parseFloat(arrResult[0][0])+"";
		var tStrs = new Array();    
		tStrs = tInitFeeRate.split(".");
		if(tStrs.length>1){
			if(tStrs[1].length>2){
				alert("套餐WR0260的初始费率只能保留两位小数！");
				return false;
			}
		}
	}
	return true;
}

function queryAgentSaleCode()
{
    
    if(fm.all('ManageCom').value == "")
    {
        alert("请先录入管理机构信息！");
        fm.all('ManageCom').focus();
        return ;
    }
    
    if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom').focus();
        return ;
    }
    var tAgentCom = fm.all('AgentCom').value;
    if(fm.all('AgentSaleCode').value == "")
    {
        var strURL = "../sys/AgentSaleCommonQueryMain.jsp?ManageCom=" + fm.all('ManageCom').value
            + "&AgentCom=" + tAgentCom;
        var newWindow = window.open(strURL, "AgentSaleCommonQueryMain", "width=" + screen.availWidth + ", height=" + screen.availHeight + ", top=0, left=0, toolbar=0, location=0, directories=0, menubar=0, scrollbars=1, resizable=1, status=0");
    }
    
    if(fm.all('AgentSaleCode').value != "")
    {
        var cAgentCode = fm.AgentSaleCode.value;  //保单号码
        var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom').value + "'";
        var arrResult = easyExecSql(strSql);
        //alert(arrResult);
        if (arrResult != null)
        {
            fm.AgentSaleName.value = arrResult[0][1];
            //alert("查询结果:  代理销售业务员代码:["+arrResult[0][0]+"] ，代理销售业务员名称为:["+arrResult[0][1]+"]");
        }
        else
        {
            fm.AgentGroup.value = "";
            alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
        }
    }
}

function queryAgentSaleCode2() {

  if(fm.all('AgentSaleCode').value != ""){
  	if(fm.all('AgentCom').value == "")
    {
        alert("请先录入中介公司信息！");
        fm.all('AgentCom').focus();
        return ;
    }
    var cAgentCode = fm.AgentSaleCode.value;  //保单号码
    var strSql = "select AgentCode,Name from laagenttemp where AgentCode='" + cAgentCode + "' and entryno = '" + fm.all('AgentCom').value + "'";
    var arrResult = easyExecSql(strSql);
    //alert(arrResult);
    if (arrResult != null) {
      fm.AgentSaleName.value = arrResult[0][1];
      //alert("查询结果:  业务员代码:["+arrResult[0][0]+"] 业务员名称为:["+arrResult[0][1]+"]");
    } else {
      fm.AgentGroup.value="";
      alert("代码为:["+fm.all('AgentSaleCode').value+"]的代理销售业务员不存在，请确认!");
    }
  }
}

function afterQuery5(arrResult){
  if(arrResult!=null) {
    fm.AgentSaleCode.value = arrResult[0][0];
    fm.AgentSaleName.value = arrResult[0][1];
  }
}

//北分中介时，中介机构及代理销售业务员必须填写
function checkAgentComAndSaleCdoe(){
	var strSql = "select AgentCom,AgentSaleCode from lccont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%' and salechnl = '04'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCom = arrResult[0][0];
	    var tAgentSaleCode = arrResult[0][1];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCom == null || tAgentCom ==''){
	   		alert("银行网点代码必须录入，请核查！");
	   		return false;    	
	   	}
	   	if(tAgentSaleCode == null || tAgentSaleCode ==''){
	   		alert("银代专员代码必须录入，请核查！");
	   		return false;    	
	   	}
	   	
	   	var tSqlCode = "select ValidStart,ValidEnd from LAQualification where agentcode = '"+tAgentSaleCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tValidStart = arrCodeResult[0][0];
	   		var tValidEnd = arrCodeResult[0][1];
	   		if(tValidStart == null || tValidStart == ''){
	   			alert("网点销售人员资格证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tValidEnd == null || tValidEnd == ''){
	   			alert("网点销售人员资格证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
	   			alert("网点销售人员资格证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("网点销售人员资格证信息不完整，请核查！");
	   		return false;
	   	}
	   	
	   	var tSqlCom = "select LicenseStartDate,LicenseEndDate,EndFlag from lacom where agentcom = '"+tAgentCom+"'";
	   	var arrComResult = easyExecSql(tSqlCom);
	   	if(arrComResult){
	   		var tLicenseStartDate = arrComResult[0][0];
	   		var tLicenseEndDate = arrComResult[0][1];
	   		var tEndFlag = arrComResult[0][2];
	   		if(tLicenseStartDate == null || tLicenseStartDate == ''){
	   			alert("银行网点许可证有效起期为空，请核查！");
	   			return false;
	   		}
	   		if(tLicenseEndDate == null || tLicenseEndDate == ''){
	   			alert("银行网点许可证有效止期为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == null || tEndFlag == ''){
	   			alert("银行网点合作终止状态为空，请核查！");
	   			return false;
	   		}
	   		if(tEndFlag == 'Y'){
	   			alert("银行网点合作终止状态为失效，请核查！");
	   			return false;
	   		}
	   		if(compareDate(tCurrentDate,tLicenseEndDate)=='1'){
	   			alert("银行网点许可证已失效，请核查！");
	   			return false;
	   		}
	   	}else{
	   		alert("银行网点许可证信息不完整，请核查！");
	   		return false;
	   	}
    }
    return true;
}
function initAgentSaleCode(){
	var tSqlCode = "select AgentSaleCode from LCCont where prtno='" + fm.PrtNo.value + "'";
	var arrCodeResult = easyExecSql(tSqlCode);
	if(arrCodeResult){
		if(arrCodeResult[0][0] != null && arrCodeResult[0][0] != ""){
			var tSQL = "select agentcode,name from laagenttemp where agentcode = '"+arrCodeResult[0][0]+"'";
			var arrResult = easyExecSql(tSQL);
			if(arrResult){
				fm.AgentSaleCode.value = arrResult[0][0];
    			fm.AgentSaleName.value = arrResult[0][1];
			}
		}
	}
}
//北分业务员必须填写
function checkAgentCode(){
	var strSql = "select AgentCode from lccont where prtno='" + fm.PrtNo.value + "' and managecom like '8611%'";
    var arrResult = easyExecSql(strSql);
    
    if (arrResult != null) {
	    var tAgentCode = arrResult[0][0];
	    var tCurrentDate = getCurrentDate();
	   	if(tAgentCode == null || tAgentCode ==''){
	   		alert("业务员编码必须录入，请核查！");
	   		return false;    	
	   	}
	   	var tSqlCode = "select branchtype,branchtype2 from laagent where agentcode = '"+tAgentCode+"'";
	   	var arrCodeResult = easyExecSql(tSqlCode);
	   	if(arrCodeResult){
	   		var tSqlValid = "";
	   		if(arrCodeResult[0][0] == "1" || arrCodeResult[0][0] == "2" || arrCodeResult[0][0] == "4" || arrCodeResult[0][0] == "5" || arrCodeResult[0][0] == "7"){
	   			tSqlValid = "select ValidStart,ValidEnd from LAQUALIFICATION where agentcode = '"+tAgentCode+"' and state='0'";
	   		} else if (arrCodeResult[0][0] == "3"){
	   			tSqlValid = "select Quafstartdate,QuafEndDate from laagent where agentcode = '"+tAgentCode+"'";
	   		} else if(arrCodeResult[0][0]=="6"){
	   			return true;
	   		} else {
	   			alert("业务员资销售渠道异常，请核查！");
	   			return false;
	   		}
	   		if((arrCodeResult[0][0] == "1")&&(arrCodeResult[0][1]='06')){
				return true;
			}else{
	   		var arrValid = easyExecSql(tSqlValid);
	   		if(arrValid){
		   		var tValidStart = arrValid[0][0];
		   		var tValidEnd = arrValid[0][1];
		   		if(tValidStart == null || tValidStart == ''){
		   			alert("业务员资格证有效起期为空，请核查！");
		   			return false;
		   		}
		   		if(tValidEnd == null || tValidEnd == ''){
		   			alert("业务员资格证有效止期为空，请核查！");
		   			return false;
		   		}
		   		if(compareDate(tCurrentDate,tValidEnd)== '1'){
		   			alert("业务员资格证已失效，请核查！");
		   			return false;
		   		}
	   		}
		   	}else{
		   		alert("业务员不存在有效资格证信息，请核查！");
		   		return false;
		   	}
	   	}else{
	   		alert("业务员不存在，请核查！");
	   		return false;
	   	}
    }
    return true;
}

function checkAppnt(){
	var tSql = "select cc.prtno,cad.Customerno,cc.Managecom,cad.Mobile,cad.Phone,cad.homephone,ca.idno,ca.idtype,ca.appntbirthday,ca.appntsex,ca.appntname, " 
		+" cad.email "
		+" from lccont cc inner join lcappnt ca on ca.appntno=cc.appntno and ca.contno=cc.contno " +
				"inner join lcaddress cad on cad.customerno=ca.appntno and cad.addressno=ca.addressno where cc.prtno='" + fm.PrtNo.value + "'";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		// 印刷号
		var prtno = appntResult[0][0];
		// 投保人客户号
		var customerno = appntResult[0][1];
		// 管理机构
		var managecom = appntResult[0][2];
		// 移动电话
		var mobile = appntResult[0][3];
		// 联系电话
		var phone = appntResult[0][4];
		// 家庭电话
		var homephone = appntResult[0][5];
		// 证件号
		var id = appntResult[0][6];
		// 证件类型
		var idtype = appntResult[0][7];
		// 出生日期
		var birthday = appntResult[0][8];
		// 性别
		var sex = appntResult[0][9];
		// 姓名
		var name = appntResult[0][10];
		// 邮箱
		var email = appntResult[0][11];
		if(!isNull(email)){
			   var checkemail =CheckEmail(email);
			   if(checkemail !=""){
				   alert("投保人"+checkemail);
				   return false;
			   }
		   }
		// 共有校验
		if(!isNull(mobile)) {
			if(!isInteger(mobile) || mobile.length != 11){
				alert("投保人移动电话需为11位数字，请核查！");
		   		return false;
			}
		}
		if(!checkMobile2(1,mobile)){
			  return false;
			}
		
		if(idtype == "0"){
		
			if(birthday != getBirthdatByIdNo(id)){
				alert("投保人身份证号与出生日期不符，请核查！");
				return false;
			}
			
			if(sex != getSexByIDNo(id)){
				alert("投保人身份证号与性别不符，请核查！");
				return false;
			}
		}
		
		// 机构特殊校验  --需要在数据库中配置 codetype = 字段名 + check

		// 暂时统一对投保人进行校验，若需要单独校验，可以调整为按字段进行校验
		// 若配置为空则会对全部机构进行校验
		if(manageCheck(managecom.substring(0, 4),"appntY")){
			
			if(isNull(id)){
				alert("投保人证件号码不能为空，请核查！");
				return false;
			}
			
			if(isNull(mobile) && isNull(phone)){
				alert("投保人联系电话、移动电话不能同时为空，请核查！");
		   		return false;
			}
			
		}
		
		if(manageCheck(managecom.substring(0, 4),"phoneY")){
		
			if(!isNull(phone)){
				if(phone.length < 7){
					alert("投保人联系电话不能少于7位，请核查！");
			   		return false;
				}
			}
			
			if(!isNull(phone)){
				var phoneSql = "select count(distinct customerno) from lcaddress where phone='" + phone + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and addressno=lcaddress.addressno) ";
				var result = easyExecSql(phoneSql);
				if(result){
					var count = result[0][0];
					if(count >= 2){
						alert("该投保人联系电话已在三个以上不同投保人的保单中出现！");
						return false;
					}
				}
			}
			
			if(!isNull(mobile)){
				var mobilSql = "select count(distinct customerno) from lcaddress where mobile='" + mobile + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and addressno=lcaddress.addressno) ";
				var result = easyExecSql(mobilSql);
				if(result){
					var count = result[0][0];
					if(count >= 2){
						alert("该投保人移动电话已在三个以上不同投保人的保单中出现！");
						return false;
					}
				}
			}
			if(!isNull(homephone)){
				var mobilSql = "select count(distinct customerno) from lcaddress where homephone='" + homephone + "' and Customerno<>'" + customerno + "' and exists (select 1 from lcappnt where lcaddress.customerno=appntno and addressno=lcaddress.addressno) ";
				var result = easyExecSql(mobilSql);
				if(result){
					var count = result[0][0];
					if(count >= 2){
						alert("该投保人固定电话已在三个以上不同投保人的保单中出现！");
						return false;
					}
				}
			}
		}
		
		if(manageCheck(managecom.substring(0, 4),"agentY")){
			//校验是否是业务员本人，如果不是进行手机号校验
		    	// 业务员手机号码校验
			if(!isNull(phone)){
				var mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + phone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人联系电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			
			if(!isNull(mobile)){
				var phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人移动电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where mobile='" + mobile + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人移动电话与代理销售业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
			}
			if(!isNull(homephone)){
				var phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人固定电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("该投保人固定电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + homephone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("该投保人固定电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
		}
	}
	return true;
}

function checkInsured(){
	var tSql = "select lci.name,lci.idno,lci.idtype,lci.birthday,lci.sex,lca.phone,lca.mobile,lca.homephone,lca.email from lcinsured lci,lcaddress lca where lci.prtno='" + fm.PrtNo.value + "'"
	         +" and lci.addressno=lca.addressno and lca.customerno=lci.insuredno ";
	var appntResult = easyExecSql(tSql);
	// 必录项相关校验
	if(appntResult){
		for(var index = 0; index < appntResult.length; index++){
			// 被保人姓名
			var name = appntResult[index][0];
			// 证件号
			var id = appntResult[index][1];
			// 证件类型
			var idtype = appntResult[index][2];
			// 出生日期
			var birthday = appntResult[index][3];
			// 性别
			var sex = appntResult[index][4];
			// 联系电话
			var phone = appntResult[index][5];
			// 移动电话
			var mobile = appntResult[index][6];
			// 固定电话
			var homephone = appntResult[index][7];
			// 邮箱
			var email = appntResult[index][8];
			
			if(!isNull(email)){
				   var checkemail =CheckEmail(email);
				   if(checkemail !=""){
					   alert("被保人"+checkemail);
					   return false;
				   }
			   }
			
			if(!isNull(mobile)) {
			    if(!isInteger(mobile) || mobile.length != 11){
				    alert("被保人移动电话需为11位数字，请核查！");
		   	      	return false;
			    } 
			    if(!checkMobile2(2,mobile)){
			        return false;
			    }
		    }
			
			if(idtype == "0"){
		
				if(birthday != getBirthdatByIdNo(id)){
					alert("被保人" + name + "身份证号与出生日期不符，请核查！");
					return false;
				}
			
				if(sex != getSexByIDNo(id)){
					alert("被保人" + name + "身份证号与性别不符，请核查！");
					return false;
				}
			}
			
			if(!isNull(phone)){
				var mobilSql = "select groupagentcode from laagent where phone='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select groupagentcode from laagent where mobile='" + phone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + phone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "联系电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
			
			if(!isNull(mobile)){
				var phoneSql = "select groupagentcode from laagent where mobile='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "移动电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where phone='" + mobile + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "移动电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where mobile='" + mobile + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "移动电话与代理销售业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				
			}
			if(!isNull(homephone)){
				var phoneSql = "select groupagentcode from laagent where phone='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "固定电话与本方业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
				var phoneSql = "select groupagentcode from laagent where mobile='" + homephone + "' and idno <> '"+id+"' with ur";
				var result = easyExecSql(phoneSql);
				if(result){
					alert("被保人" + name + "固定电话与本方业务员" + result[0][0] + "手机号码相同，请核查！");
					return false;
				}
				var mobilSql = "select agentcode from laagenttemp where phone='" + homephone + "' and name <> '"+name+"' with ur";
				var result = easyExecSql(mobilSql);
				if(result){
					alert("被保人" + name + "固定电话与代理销售业务员" + result[0][0] + "电话号码相同，请核查！");
					return false;
				}
			}
		}
	}

	return true;
}

// 判断机构是否需要进行校验
// managecom 机构
// checkflag 校验项
function manageCheck(managecom, checkflag){
	var checkSql = "select (case (select count(1) from ldcode where codetype = '" + checkflag + "check') " +  
			"when 0 then 1 else (select distinct 1 from ldcode where codetype = '" + checkflag + "check' and code = '" + managecom + "') end ) " + 
			"from dual";
	var result = easyExecSql(checkSql);
	if(result){
		if(result[0][0] == "1"){
			// 该机构需要进行校验
			return true;
		}
	}
	// 该机构不需要进行校验
	return false;
}

// 判断是否为空
function isNull(checkStr){
	if(checkStr == null || checkStr == ""){
		return true;
	}
	return false;
}
function valiAddress(){
	var getAddrSQL = "select lcad.postaladdress from LCAppnt lca inner join LCAddress lcad on lca.appntno = lcad.customerno and lca.addressno = lcad.addressno where lca.prtno = '"+fm.PrtNo.value+"' ";
	var addrResult = easyExecSql(getAddrSQL);
	if(addrResult){
		var addr = addrResult[0][0];
		var managecom = fm.ManageCom.value;
		if(manageCheck(managecom.substring(0, 4),"addr")){
			if(addr.trim().length<6){
				alert("投保人联系地址信息低于字符限制要求，须提详细地址信息!");
				return false;
			}
		}
	}
	
	return true;
}
function checkPayMethod(){
	var valiRiskSQL = "select 1 from lcriskdutywrap where prtno = '"+fm.PrtNo.value+"' and riskwrapcode in ( select code from ldcode where codetype = 'checkpaymethod' and othersign = '1' ) ";
	var RiskResult = easyExecSql(valiRiskSQL);
	if(RiskResult){
		valiSQL = "select 1 from lccont where prtno = '"+ fm.PrtNo.value +"' and PayMethod = '01' ";
		 var arrResult = easyExecSql(valiSQL);
	     if(!arrResult){ 
	        alert("该产品缴费模式必须选择“01-从投保人帐户缴纳首期保费！”");
	    	return false;
	     }
	}
    return true;
}

function chenkBnfBlack(){
      var insuredSql= " select insuredno ,insuredname,paymode ,sum(paymoney) "
             + " from "
             + " (select insuredno ,insuredname,paymode ,"
             + "(Case When Payintv = '0' Then Prem + Nvl(Supplementaryprem, 0) "
             + " When Payyears <> 0 Then Prem * Double(12 / Payintv) * Payyears + Nvl(Supplementaryprem, 0) "
             + " When Payyears = 0 Then "
             + " Case When Payintv='12' Then Prem + Nvl(Supplementaryprem, 0) "
             + " Else  Prem * (to_month(payenddate)-to_month(cvalidate))/payintv + Nvl(Supplementaryprem, 0) End "
             + " End )paymoney "
             + " from lcpol where prtno='" + fm.PrtNo.value + "'"
             + " ) temp group by insuredno ,insuredname,paymode "
             +" having ((paymode='1' and sum(paymoney)>=20000) or (paymode<>'1' and sum(paymoney)>=200000)) ";
      var insuredResult = easyExecSql(insuredSql); 
      var bnfSql="select a.name from lcbnf a,lcpol b where BnfType='1' and (trim(NativePlace) is null or trim(NativePlace)=''"
                +" or trim(OccupationCode) is null or trim(OccupationCode)='' "
                +" or trim(Phone) is null or trim(Phone)='' "
                +" or trim(PostalAddress) is null or trim(PostalAddress)='' "
                +" or trim(IDStartDate) is null or trim(IDStartDate)='' "
                +" or trim(IDEndDate) is null or trim(IDEndDate)='' ) "
                +" and a.RelationToInsured not in ('01','02','03','04','05' ) "
                +" and a.contno=b.contno and a.polno=b.polno and b.prtno='" + fm.PrtNo.value + "' ";

      if(insuredResult){
		for(var index = 0; index < insuredResult.length; index++){
		    bnfSql+=" and a.insuredno='"+insuredResult[index][0]+"' ";
		    var bnfResult = easyExecSql(bnfSql); 
		    if(bnfResult){
		         alert("达“反洗钱”标准且非法定继承人以外的身故受益人："+bnfResult[0][0]+" 信息不全!");
		         return false;
		    }		  
		}
	  }
          
      return true;
}

function checkVideo(){
   var videoSql="select 1 from lcpol lcp,lmriskapp lm,lccont lcc where lcc.contno=lcp.contno and lcc.managecom like '8644%' "
               +"and lcc.contno='"+ fm.ContNo.value + "' and lcp.riskcode=lm.riskcode and Year(lcc.PolApplyDate - lcc.AppntBirthday)>=60 and lm.RiskPeriod='L' ";
   var result = easyExecSql(videoSql);
   if(result&&fm.VideoFlag.value!='1'){
      alert("请进行录音录像调阅！");
      return false;
   }
   return true;
} 

//校验个险万能录单  校验销售相互代理业务的销售渠道与中介机构是否符合描述。   add by 赵庆涛 2016-06-28
function checkCrsBussSaleChnl()
{
	var strSQL = "select 1 from lccont lcc where lcc.ContType = '1' "  
			   + " and lcc.Crs_SaleChnl is not null " 
			   + " and lcc.Crs_BussType = '01' " 
			   + " and not (lcc.SaleChnl in (select code1 from ldcode1 where codetype='crssalechnl' and code='01') and exists (select 1 from LACom lac where lac.AgentCom = lcc.AgentCom and lac.BranchType in (select codealias from ldcode1 where codetype='crssalechnl' and code='01') and lac.AcType NOT in ('05')))  " 
			   + " and lcc.PrtNo = '" + fm.PrtNo.value + "' ";
	var arrResult = easyExecSql(strSQL);
    if (arrResult != null){
        alert("选择集团交叉销售相互代理业务类型时，销售渠道必须为团险中介或互动中介，并且机构必须为对应的中介机构！");
        return false;
    }
	return true;
}
function checkMobile2(type,mobile){
	var str= "";
	if(type== "1"){
		str="投保人";
	}else{
		str="被保人";
	}
	   if(mobile != null && mobile !="") {
		    if(!isInteger(mobile) || mobile.length != 11){
			    alert(str+"移动电话需为11位数字，请核查！");
	  	      	return false;
		    } 
		}
	   if(mobile != "" && mobile != null){
	      if(mobile.length!=11){
	          alert(str+"移动电话不符合规则，请核实！");
	          return false;
	      }else{
	         if(mobile.substring(0,2)!=13&&mobile.substring(0,2)!=16&&mobile.substring(0,2)!=14&&mobile.substring(0,2)!=15&&mobile.substring(0,2)!=18&&mobile.substring(0,2)!=17&&mobile.substring(0,2)!=19){
	            alert(str+"移动电话不符合规则，请核实！");
	            return false;
	         }
	      }
	   }else{
	      if(!confirm(str+"移动电话为空，是否继续？")){
	         return false;
		  }
	   }
	   return true;
	}
