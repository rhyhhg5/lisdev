<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：LAGbuildInforDownInput.jsp
//程序功能：团建信息明细表
//创建时间：2013-7-22
//创建人  ：

%>
<%@page contentType="text/html;charset=GBK" %>

<head>
<script>
 </script>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
   <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
   <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
   <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
   <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
   <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
   <%@include file="LAGbuildAgentInforDownInit.jsp"%> 
   <SCRIPT src="LAGbuildAgentInforDownInput.js"></SCRIPT>
   <%@include file="../agent/SetBranchType.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   
</head>

<body  onload="initForm();initElementtype();" >
 <form action="LAInteractiveSave.jsp" method=post name=fm target="fraSubmit">
  <table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryModify);"></td>
    <td class=titleImg> 查询条件 </td>    
   </tr>
  </table>
  <div id="divQryModify" style="display:''">
  <table class=common >
  	<tr>
  			<td class=title>管理机构</td>
  			<td class=input>
  				<Input class="codeno" name=ManageCom  verify="管理机构|NOTNULL"
            	      ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
                      onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><Input class="codename" name=ManageComName readonly=true  elementtype=nacessary> 
  			</td>
  			<td class=title>团队编码</td>
  			<td class=input>
  				<input class="common" name=BranchAttr>
  			</td>
  		</tr>
  		  <tr>
  			<td class=title>人员编码</td>
  			<td class=input>
  			<input class="common" name=GroupAgentCode onchange="return checkAgentCode()">
  			</td>
  			<td class=title>人员姓名</td>
  			<td class=input>
  			<input class="common" name=Name>
  			</td>
          </tr>
  </table>
  </div>
  <table>
   <tr>      
     	<td>
     		<input type=button value="查  询" class=cssButton onclick="easyQueryClick();">
     		<input type=button value="下  载" class=cssButton onclick="DoDownload();">
     		<input type=button value="重  置" class=cssButton onclick="reset();">   
    	</td>
   </tr>      
  </table>
  <table>
  	<tr>
    	<td class=common>
				<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divSetGrid);">
			</td>
    	<td class= titleImg>查询结果
    	</td>
		</tr>
  </table>
  <div id="divSetGrid" style="display:''">      
   <table class=common>   
    <tr class=common>
     <td text-align:left colSpan=1>
      <span id="spanBranchGrid" ></span>
     </td>
    </tr>    
   </table>      
   <INPUT VALUE=" 首页 "  TYPE="button" class=cssButton onclick="turnPage.firstPage();">
   <INPUT VALUE="上一页"  TYPE="button" class=cssButton onclick="turnPage.previousPage();">
   <INPUT VALUE="下一页"  TYPE="button" class=cssButton onclick="turnPage.nextPage();">
   <INPUT VALUE=" 尾页 "  TYPE="button" class=cssButton onclick="turnPage.lastPage();">
  </div> 
   	      <input type=hidden class=Common name=querySql > 
          <input type=hidden class=Common name=querySqlTitle > 
          <input type=hidden class=Common name=Title >
          <input type=hidden name=AgentCode value=''>
 </form> 
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span> 

</body>
</html>




