<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-16 15:39:06
//创建人  ：xjh
//更新记录：  更新人    更新日期     更新原因/内容
//
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="AgentGroupInfo.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentGroupInfoInit.jsp"%>
  <title>销售队伍情况打印 </title>   
</head>
<body  onload="initForm();" >
  <form action="./AgentGroupInfoSave.jsp" method=post name=fm target="f1print">
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>管理机构</TD>
          <TD  class= input>
            <Input class="code" name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);">
          </TD>          
        </TR>
        <TR  class= common>
          <TD  class= title>开始日期</TD>
          <TD  class= input>
             <Input name=StartDate class='coolDatePicker' dateFormat='short' verify="开始日期|notnull&Date" elementtype=nacessary>
          </TD> 
          <TD  class= title>结束日期</TD>
          <TD  class= input>
            <Input name=EndDate class='coolDatePicker' dateFormat='short' verify="结束日期|notnull&Date" elementtype=nacessary>
          </TD> 
        </TR>        
    </table>
    		<p>
          <INPUT VALUE="打  印" TYPE=button onclick="printPol();" class=cssbutton >
        </p>
  </form> 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
