<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：LABankWagePay.jsp
//程序功能：下载列表 
//创建日期：2016-3-24
//创建人  ：XX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
  String tAgentType = request.getParameter("AgentType");
%>
<script>
   var msql="1 and char(length(trim(comcode))) in (#4#,#2#,#8#) and comcode<>#86000000# ";
</script>
<head>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="LABankWagePay.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<%@include file="../common/jsp/ManageComLimit.jsp"%>

</head>
<body onload="initElementtype();" >    
  <form action="./LABankWagePaySave.jsp" method=post name=fm target="f1print">
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			 查询条件
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>
        <TR  class= common> 
      	  <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,msql,'1',1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,msql,'1',1);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>
          <TD  class= title>
            销售单位代码 
          </TD>
          <TD  class= input>
            <Input  class=common  name=BranchAttr onchange="return checkBranchAttr()">
          </TD>            
        </TR>      
        <TR>
          <TD  class= title>
            业务员代码 
          </TD>
          <TD  class= input>
            <Input  class=common  name=GroupAgentCode onchange="return checkAgentCode()">
          </TD> 
          <TD  class= title>
            业务员名称  
          </TD>
          <TD  class= input>
            <Input  class=common  name=AgentName onchange="return checkAgentName()">
          </TD>           
        </TR>      
        <TR  class= common>
         <TD  class= title>
            薪资年月
          </TD>
          <TD  class= input>
            <Input class=common  name=IndexCalNo verify="薪资所属年|len=6&NOTNULL"  elementtype=nacessary>
          </TD>                 
         </TR>
    </table>

    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden class=Common name=querySql > 
    <input type=hidden class=Common name=AgentType value='<%=tAgentType%>' > 
    <input type=hidden class=Common name=AgentGroup > 
    <input type=hidden name=AgentCode value=''>
    <INPUT VALUE="打  印" class="cssButton" TYPE="button" onclick="submitForm()">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->   </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 