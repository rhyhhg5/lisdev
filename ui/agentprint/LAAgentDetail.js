//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAAgentQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //window.showModalDialog("./ProposalQuery.jsp",window,"status:0;help:0;edge:sunken;dialogHide:0;dialogWidth=15cm;dialogHeight=12cm");
  //查询命令单独弹出一个模态对话框，并提交，和其它命令是不同的
  //因此，表单中的活动名称也可以不用赋值的
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


 	
function getAgent()
{	
	var  sql = "select a.name,a.managecom from laagent a where 1=1 "
	    + " and a.agentcode ='"+fm.all('AgentCodeInit').value+"'";
	var strQueryResult = easyQueryVer3(sql, 1, 1, 1);
     if (!strQueryResult)
  {
    alert("没有此代理人！");
    fm.all('AgentCodeInit').value='';
    fm.all('NameInit').value='';
    return false;
  }  
  var tArr = new Array();
  tArr = decodeEasyQueryResult(strQueryResult);　
  fm.all('NameInit').value  = tArr[0][0];

  
 }
	
// 查询按钮
function easyQueryClick()
{
	// 初始化表格
	if (verifyInput() == false)
    return false;
	// 书写SQL语句
	var strSQL = "";
	strSQL = "select a.agentcode,a.name,(select codename from ldcode where  codetype='sex' and code=a.sex),"
	         +"a.birthday,(select codename from ldcode where  codetype='idtype' and code=a.IDNoType),a.idno,"
	         +"(select codename from ldcode where codetype='nativeplacebak' and code=a.NativePlace),"
	         +"(select codename from ldcode where  codetype='polityvisage' and code=a.PolityVisage),"
	         +"(select codename from ldcode where  codetype='nativeplacebak' and code=a.RgtAddress),"
	         +"(select codename from ldcode where  codetype='degree' and code=a.Degree),"
	         +"a.GraduateSchool,a.Speciality,"
	         +"(select codename from ldcode where  codetype='nationality' and code=a.Nationality),"
	         +"a.HomeAddress,a.ZipCode,a.Phone,a.Mobile,a.EMail,a.OldOccupation,a.OldCom,a.HeadShip,"
	          +"(select certifno from LACertification where agentcode=a.agentcode order by grantdate desc fetch first 1 rows only),"
	        +"(select qualifno from LAQualification where agentcode=a.agentcode order by grantdate desc    fetch first 1 rows only),"
	        +"(select account from laaccounts where agentcode=a.agentcode order by opendate desc    fetch first 1 rows only),"
	         +"c.IntroAgency,(select name from laagent where agentcode=c.IntroAgency),"
	         +"c.agentgrade,b.BranchAttr,b.name,"
	          +"(select name from labranchgroup where agentgroup=substr(b.branchseries,1,12)),"
	         +"(SELECT  branchmanagername  FROM labranchgroup f  WHERE   substr(b.branchattr,1,14)=f.branchattr "
             +" AND  f.branchlevel='01' and   f.branchtype='1' AND  f.branchtype2='01' fetch first 1 rows only),"
	         +"(SELECT  branchmanagername  FROM  labranchgroup f WHERE substr(b.branchattr,1,12)=f.branchattr "
             +" AND  f.branchlevel='02' and  f.branchtype='1' AND f.branchtype2='01' fetch first 1 rows only),"   
             +"(SELECT  branchmanagername  FROM  labranchgroup f WHERE substr(b.branchattr,1,10)=f.branchattr "
             +" AND f.branchlevel='03' and  f.branchtype='1'    AND f.branchtype2='01'  fetch first 1 rows only) ,"             
	         + "case when a.agentstate='01' then '在职'  when a.agentstate='02' then '二次增员' "
	         + " when a.agentstate='03' then '离职登记' when a.agentstate='04' then '二次离职登记' "
	         + " when a.agentstate='06' then '离职确认' when a.agentstate='07' then '二次离职确认' end"
	         + " ,a.employdate,a.outworkdate "
	         + " from LAAgent a,LABranchGroup b,LATree c "
	         +"where 1=1 "
	         +"and a.Agentgroup = b.agentGroup and c.agentcode=a.agentcode "
	         + getWherePart('a.AgentCode','AgentCodeInit')
	         + getWherePart('a.ManageCom','ManageCom','like');		

     	var strQueryResult = easyQueryVer3(strSQL, 1, 1, 1);
		if (!strQueryResult)
		{
			alert('不存在该业务员基本信息！');
			fm.all('AgentCodeInit').value = '';
			return false;
		}
		
		var arr = decodeEasyQueryResult(strQueryResult);
                       
    fm.all('AgentCode').value =arr[0][0];
    fm.all('Name').value =arr[0][1];
    fm.all('SexName').value =arr[0][2];
    fm.all('Birthday').value =arr[0][3];
    fm.all('IDNoTypeName').value = arr[0][4];
    fm.all('IDNo').value = arr[0][5];
    fm.all('NativePlaceName').value = arr[0][6];
    fm.all('NationalityName').value = arr[0][7];
    fm.all('RgtAddressName').value = arr[0][8];
    fm.all('DegreeName').value = arr[0][9];
    fm.all('GraduateSchool').value =arr[0][10];
    fm.all('Speciality').value =arr[0][11];
    fm.all('PolityVisageName').value = arr[0][12];    
    fm.all('HomeAddress').value = arr[0][13];
    fm.all('ZipCode').value = arr[0][14];
    fm.all('Phone').value = arr[0][15];
    fm.all('Mobile').value = arr[0][16];
    fm.all('EMail').value = arr[0][17];
    fm.all('OldOccupation').value = arr[0][18];
    fm.all('OldCom').value = arr[0][19];
    fm.all('HeadShip').value =arr[0][20];
    fm.all('CertifiNo').value = arr[0][21];
    fm.all('QualifNo').value =arr[0][22];
    fm.all('Account').value = arr[0][23];
    fm.all('IntroAgency').value = arr[0][24];
    fm.all('IntroAgencyName').value = arr[0][25];
    fm.all('AgentGrade').value =arr[0][26];
    fm.all('BranchAttr').value = arr[0][27];
    fm.all('BranchName').value =arr[0][28];
    fm.all('MinisterCode').value = arr[0][29];
    fm.all('GroupManagerName').value = arr[0][30];
    fm.all('DepManagerName').value =arr[0][31];
    fm.all('Minister').value =arr[0][32];
    fm.all('AgentState').value =arr[0][33];
    
    fm.all('EmployDate').value = arr[0][34];
    fm.all('OutWorkDate').value = arr[0][35];
}


function easyQueryClick1()
{	
	
  
	// 书写SQL语句
	var strSQL = "";
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;

    strSQL = "select c.name,c.branchattr,a.name,a.agentcode, "
+" (select gradename from laagentgrade where laagentgrade.gradecode=b.agentgrade), "
+"  b.agentgrade,a.sex,a.employdate,b.initgrade,b.agentgrade,b.startdate,b.introagency,a.agentstate "
+"  from laagent a,latree b ,labranchgroup c "
+" where " 
+" a.agentcode=b.agentcode and a.agentgroup=c.agentgroup "
//+" and a.managecom like '" + mManageCom + "%' "
+" and a.branchtype='1' and a.branchtype2='01'  "
+" and a.agentcode = '" + fm.all('AgentCode').value + "' "
+" union "
+" select distinct  c.name,c.branchattr,a.name,a.agentcode,"
+"(select gradename from laagentgrade where laagentgrade.gradecode=b.agentgrade),  "
+"  b.agentgrade,a.sex,a.employdate,b.initgrade,d.agentgrade,d.startdate,b.introagency,a.agentstate "
+" from laagent a,latree b ,labranchgroup c,latreeb  d "
+" where  a.agentcode=b.agentcode and a.agentgroup=c.agentgroup  "
+" and b.agentcode=d.agentcode and d.removetype  in ('01','31') "
+" and a.branchtype='1' "
+" and a.branchtype2='01'   "
+" and a.agentcode = '" + fm.all('AgentCode').value + "' "
+" with ur ";


	//alert(strSQL);
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

 
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	WarrantorGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = WarrantorGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}


