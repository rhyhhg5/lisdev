<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAAgentQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:31:08
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);

%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<script>
   var manageCom = <%=tG.ManageCom%>;
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="./LAAgentDetail.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAgentDetailInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>

<title>营销员查询 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAAgentDetailSave.jsp" method=post name=fm target="fraSubmit">
  <!--代理人查询条件 -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent);">
            <td class= titleImg>
                查询条件
            </td>
            </td>
    	</tr>
     </table>
  <Div  id= "divLAAgent" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
       
      
        <TD class= title>
          管理机构
        </TD>
        <TD  class= input>
            <Input class="codeno" name=ManageCom  verify = "管理机构|notnull"
             ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
             onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
             ><Input name=ManageComName class="codename"  elementtype=nacessary> 
        </TD>
         <TD class= title> 
          营销员编码 
        </TD>
        <TD  class= input> 
          <Input class=common  name=AgentCodeInit  verify = "营销员编码|notnull"  onchange="return getAgent();" elementtype=nacessary>
        </TD> 
      </TR>
      <TR  class= common> 
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=NameInit class= 'readonly' >
        </TD>        
      </TR> 
    </table>
          <input type=hidden name=BranchType value=''>
          <input type=hidden name=BranchType2 value=''>
          <INPUT VALUE="查  询" class="cssbutton" TYPE=button onclick="easyQueryClick();"> 
    </Div>      
          				
    <table>
    	<tr>
        	<td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid);">
    		</td>
    		<td class= titleImg>
    			 营销员结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgentGrid" style= "display: ''">
      	 <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          营销员代码 
        </TD>
        <TD  class= input> 
          <Input class= 'readonly' readonly name=AgentCode  >
        </TD>        
        <TD  class= title>
          姓名
        </TD>
        <TD  class= input>
          <Input name=Name class= readonly  readonly>
        </TD>
        <TD  class= title>
          性别
        </TD>
        <TD  class= input>
          <Input  name=SexName class="readonly" readonly >
        </TD> 
     
      </TR> 
      <TR  class= common> 
      
        <TD  class= title>
          出生日期 
        </TD>
        <TD  class= input>
          <Input name=Birthday class=readonly  readonly > 
        </TD>
        <TD  class= title>
          证件类型 
        </TD>
        
        <TD  class= input> 
          <Input  name=IDNoTypeName class="readonly" readonly > 
        </TD>      
        <TD  class= title>
          证件号码 
        </TD>
        <TD  class= input> 
          <Input name=IDNo class= readonly  readonly> 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title> 
          籍贯
        </TD>
        <TD  class= input>
          <Input  name=NativePlaceName class="readonly"  >
        </TD>   
        <TD  class= title>
          政治面貌 
        </TD>
        <TD  class= input> 
          <Input name=PolityVisageName class="readonly"  > 
        </TD> 
        <TD  class= title>
          户口所在地
        </TD>
        <TD  class= input> 
          <Input  name=RgtAddressName class="readonly"  > 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          学历 
        </TD>
        <TD  class= input> 
          <Input  name=DegreeName class="readonly"  >  
        </TD>
        <TD  class= title> 
          毕业院校
        </TD>
        <TD  class= input> 
          <Input name=GraduateSchool class= readonly  readonly> 
        </TD>
        <TD  class= title>
          专业 
        </TD>
        <TD  class= input> 
          <Input name=Speciality class= readonly readonly> 
        </TD>
      </TR>
      <TR  class= common>      
        <TD  class= title>
          民族
        </TD>
        <TD  class= input>
          <Input  name=NationalityName class="readonly"  > 
        </TD>        

      
        <TD  class= title>
          家庭地址 
        </TD>
        <TD  class= input> 
          <Input name=HomeAddress class= readonly readonly> 
        </TD>
       
        <TD  class= title>
          邮政编码
        </TD>
        <TD  class= input> 
          <Input name=ZipCode class= readonly readonly> 
        </TD>
      </TR>
      <TR  class= common> 
        <TD  class= title>
          联系电话 
        </TD>
        <TD  class= input> 
          <Input name=Phone class= readonly  readonly>
        </TD>
       
        <TD  class= title>
          手机 
        </TD>
        <TD  class= input> 
          <Input name=Mobile class= readonly  readonly> 
        </TD>
    
        <TD  class= title>
          E_mail 
        </TD>
        <TD  class= input> 
           <Input name=EMail class= readonly  readonly > 
        </TD>
      
      
      </TR>
      <TR  class= common>
              <TD  class= title> 
          原职业 
        </TD>
        <TD  class= input> 
          <Input name=OldOccupation class='readonly'  readonly> 
        </TD>
        <TD  class= title>
          原工作单位 
        </TD>
        <TD  class= input> 
          <Input name=OldCom class= readonly  readonly> 
        </TD>

        <TD  class= title>
          原工作职务 
        </TD>
        <TD  class= input> 
          <Input name=HeadShip class= readonly  readonly> 
        </TD>
      </TR>

       <TR  class= common>
              <TD  class= title> 
          展业证号 
        </TD>
        <TD  class= input> 
        
          <Input name=CertifiNo class='readonly' readonly> 
        </TD>
        <TD  class= title>
          资格证号 
        </TD>
        <TD  class= input> 
          <Input name=QualifNo class= readonly readonly > 
        </TD>

        <TD  class= title>
          佣金存折帐号 
        </TD>
        <TD  class= input> 
          <Input name=Account class= readonly readonly > 
        </TD>
      </TR>
     <TR  class= common>
              <TD  class= title> 
          推荐人代码 
        </TD>
        <TD  class= input> 
        
          <Input name=IntroAgency class='readonly' readonly> 
        </TD>
        <TD  class= title>
          推荐人姓名 
        </TD>
        <TD  class= input> 
          <Input name=IntroAgencyName class= readonly readonly > 
        </TD>

        <TD  class= title>
         现职级 
        </TD>
        <TD  class= input> 
          <Input name=AgentGrade class= readonly  readonly> 
        </TD>
      </TR>
     <TR  class= common>
              <TD  class= title> 
          销售单位代码 
        </TD>
        <TD  class= input> 
        
          <Input name=BranchAttr class='readonly' readonly> 
        </TD>
        <TD  class= title>
          销售单位名称 
        </TD>
        <TD  class= input> 
          <Input name=BranchName class= readonly  readonly> 
        </TD>

        <TD  class= title>
         营销部名称 
        </TD>
        <TD  class= input> 
          <Input name=MinisterCode class= readonly  readonly> 
        </TD>
      </TR>
      
       <TR  class= common>
              <TD  class= title> 
          处经理姓名 
        </TD>
        <TD  class= input> 
        
          <Input name=GroupManagerName class='readonly' readonly> 
        </TD>
        <TD  class= title>
          区经理姓名 
        </TD>
        <TD  class= input> 
          <Input name=DepManagerName class= readonly  readonly> 
        </TD>
        <TD  class= title>
          部经理姓名 
        </TD>
        <TD  class= input> 
          <Input name=Minister class= readonly  readonly> 
        </TD>
      </TR>      
      <TR  class= common>        
        <TD  class= title>
          代理人状态 
        </TD>
        <TD  class= input> 
          <Input name=AgentState class='readonly' readonly>
        </TD>
        <TD  class= title>
          入职时间 
        </TD>
        <TD  class= input> 
          <Input name=EmployDate class= 'readonly' readonly > 
        </TD>
        <TD  class= title>
          离职时间 
        </TD>
        <TD  class= input> 
          <Input name=OutWorkDate class= 'readonly' readonly > 
        </TD>       
      </TR>
    	</table>
        <INPUT VALUE="历史轨迹查询" class="cssbutton" TYPE=button onclick="easyQueryClick1();"> 					
  	</div>
  	 <table>
 	<tr>
 		<td class=common>
 			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent2);">
 			<td class= titleImg>
 				轨迹信息
 			</td>
 		</td>
 	</tr>
 </table>
 <Div  id= "divLAAgent2" style= "display: ''">
 	<table  class= common>
 		<tr  class= common>
 
 			<td text-align: left colSpan=1>
 				<span id="spanWarrantorGrid" >
 				</span>
 			</td>
 		</tr>
 	</table>
 </div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
