<%
//程序名称：AdjustAgentInit.jsp
//程序功能：添加页面控件的初始化。
//创建日期：2002-06-27 08:49:52
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script language="JavaScript">

	function initForm(){
		try{
			initInpBox();
			initBranchGrid();
		}
		catch(re){
			alert("LAGbuildInforDownInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	
	function initInpBox(){
		try{
          fm.all('ManageCom').value='';
          fm.all('ManageComName').value='';
          fm.all('BranchAttr').value='';
          fm.all('AgentCode').value='';
          fm.all('Name').value='';
 		}
		catch(ex){
			alert("在LAGbuildInforDownInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	
	// 险种授权的初始化
	function initBranchGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array();
			iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
			iArray[0][1]="30px"; 	           	    //列宽
			iArray[0][2]=1;            				//列最大值
			iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[1]=new Array();
			iArray[1][0]="分公司编码";
			iArray[1][1]="80px";
			iArray[1][2]=20;
			iArray[1][3]=0;

			iArray[2]=new Array();
			iArray[2][0]="分公司名称";
			iArray[2][1]="80px";
			iArray[2][2]=20;
			iArray[2][3]=0;

			iArray[3]=new Array();
			iArray[3][0]="支公司编码";
			iArray[3][1]="80px";
			iArray[3][2]=10;
			iArray[3][3]=0;
			
			iArray[4]=new Array();
			iArray[4][0]="支公司名称";
			iArray[4][1]="80px";
			iArray[4][2]=10;
			iArray[4][3]=0;
						
			iArray[5]=new Array();
			iArray[5][0]="人员编码";
			iArray[5][1]="100px";
			iArray[5][2]=10;
			iArray[5][3]=0;
			
		    iArray[6]=new Array();
			iArray[6][0]="人员姓名";
			iArray[6][1]="80px";
			iArray[6][2]=10;
			iArray[6][3]=0;
			
			
			iArray[7]=new Array();
			iArray[7][0]="团队建设类型";
			iArray[7][1]="80px";
			iArray[7][2]=10;
			iArray[7][3]=0;
			
			iArray[8]=new Array();
			iArray[8][0]="团队建设起期";
			iArray[8][1]="80px";
			iArray[8][2]=10;
			iArray[8][3]=0;

            iArray[9]=new Array();
			iArray[9][0]="团队建设止期";
			iArray[9][1]="80px";
			iArray[9][2]=10;
			iArray[9][3]=0;
			
		    iArray[8]=new Array();
			iArray[8][0]="团队建设起期";
			iArray[8][1]="80px";
			iArray[8][2]=10;
			iArray[8][3]=0;

            iArray[9]=new Array();
			iArray[9][0]="团队建设止期";
			iArray[9][1]="80px";
			iArray[9][2]=10;
			iArray[9][3]=0;
			
		    iArray[10]=new Array();
			iArray[10][0]="筹备标记";
			iArray[10][1]="80px";
			iArray[10][2]=10;
			iArray[10][3]=0;

            iArray[11]=new Array();
			iArray[11][0]="筹备开始日期";
			iArray[11][1]="80px";
			iArray[11][2]=10;
			iArray[11][3]=0;


			BranchGrid = new MulLineEnter( "fm" , "BranchGrid" );
			//这些属性必须在loadMulLine前
	        BranchGrid.mulLineCount = 0;   
            BranchGrid.displayTitle = 1;
            BranchGrid.locked=1;
            BranchGrid.canSel=0;
            BranchGrid.canChk=0;
            BranchGrid.loadMulLine(iArray);  
		}
		catch(ex){
			alert(ex);
		}
	}


</script>
