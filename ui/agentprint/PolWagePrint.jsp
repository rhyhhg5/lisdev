<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="PolWagePrint.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PolWagePrintInit.jsp"%>
  <title>打印人员薪资 </title>   
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>   管理机构  </TD>
          <TD  class= input>  <Input class="codeNo"  name=ManageCom elementtype=nacessary verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true ></TD>   
 
          <TD  class= title width="25%">
            年月
          </TD>
          <TD  class= input width="25%">
            <Input class= common name=YearMonth verify="年月|NOTNULL">
          </TD>       
        </TR>
    </table>
    <table  class= common >
        <TR class= common> 
		<TD  class= input width="36%">
			<input type=Button value="人员工资打印(有金额，有折号)" onclick="PolWagePrint();">
		</TD>
		<TD  class= input width="26%">
			<input type=Button value="人员工资打印(有金额，无折号)" onclick="PolWagePrint1();">
		</TD>				
	</TR> 
	<TR class= common>   	 
	<TD  class= input width="26%">
			<input type=Button value="区工资合计" onclick="SumWagePrint();">
		</TD>
					
	</TR> 
      </table>
      <input type=hidden id="Operate" name="Operate">
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
