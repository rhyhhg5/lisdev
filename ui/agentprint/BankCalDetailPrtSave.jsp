<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%
	GlobalInput tG = (GlobalInput)session.getValue("GI");
	String FlagStr = "";
	String Content = "";	
	
  	VData tVData = new VData();
	VData mResult = new VData();
    
    String tStartDate = request.getParameter("StartDate");
    String tEndDate = request.getParameter("EndDate");
    String tAgentCom = request.getParameter("AgentCom");
    String tCalType = request.getParameter("CalType");
    String tRiskCode = "";
    tRiskCode = request.getParameter("RiskCode");
    
    tVData.clear();
    tVData.addElement(tStartDate);
    tVData.addElement(tEndDate);
    tVData.addElement(tAgentCom);
    tVData.addElement(tCalType);
    tVData.addElement(tRiskCode);
    
    tVData.addElement(tG);
    
	String strErrMsg = "";
	String Flag="";	
	
    BankCalDetailPrt tPrt = new BankCalDetailPrt();

	if(!tPrt.getInputData(tVData)) {
		if( tPrt.mErrors.needDealError() ) {
			strErrMsg = tPrt.mErrors.getFirstError();
		} else {
			strErrMsg = "BankCalDetailPrt发生错误，但是没有提供详细的出错信息";
		}
		Flag = "Fail";
%>		
<script language="javascript">	
	alert("<%=strErrMsg%>");
	top.close();	
	//window.opener.afterSubmit("<%=Flag%>","<%=strErrMsg%>");		
</script>
<%
		return;
  }
  if ( !tPrt.prepareData() ) {
    if (tPrt.mErrors.needDealError() )  {
      strErrMsg = tPrt.mErrors.getFirstError();      
    }else  {
      strErrMsg = "BankCalDetailPrt发生错误，但是没有提供详细的出错信息";
    }  
    System.out.println("---strErrMsg----" + strErrMsg);
    System.out.println("---Error----");
    Flag = "Fail";
    %>		
<script language="javascript">	
	alert("<%=strErrMsg%>");
	top.close();	
	//window.opener.afterSubmit("<%=Flag%>","<%=strErrMsg%>");		
</script>
<%
    return;
  }    

  mResult = tPrt.getResult();
  
  XmlExport txmlExport = (XmlExport)mResult.getObjectByObjectName("XmlExport",0);
	
  if (txmlExport==null) {
	System.out.println("null");
  }
  
  session.putValue("PrintStream", txmlExport.getInputStream());
  System.out.println("put session value");
  response.sendRedirect("../f1print/GetF1Print.jsp");
%>

