<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="BankRiskTransmoney.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BankRiskTransmoneyInit.jsp"%>
  <title>分险种渠道保费统计</title>   
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>      	  
          <TD  class= title width="25%">
            起始日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=StartDay verify="起始日期|NOTNULL">
          </TD>      
          <TD  class= title width="25%">
            结束日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=EndDay  verify="结束日期|NOTNULL">
          </TD>
          </TR> 
          <TR  class= common> 
          <TD   class=title>
          银行类型</td>
          <td class=input>
          <Input class="codeno" name=BankType
          CodeData="0|^01|分行|^02|支行"
          ondblClick="showCodeListEx('BankType1List',[this,BankTypeName],[0,1]);"
          onkeyup="showCodeListKeyEx('BankType1List',[this,BankTypeName],[0,1]);"><input class=codename name=BankTypeName readonly=true > 
          </td>
          <td class=title>
          网点类型</td>
          <td class=input>
          <Input class="codeno" name=NetType
           CodeData="0|^02|支行|^03|分理处|^04|网点"
           ondblClick="showCodeListEx('BankType2List',[this,NetTypeName],[0,1]);"
           onkeyup="showCodeListKeyEx('BankType2List',[this,NetTypeName],[0,1]);"><input class=codename name=NetTypeName readonly=true >     
         </td>
        </TR>
          <TR  class= common>
           <td  class= title> 险种	</td>
        <td  class= input><input name=RiskCode class="codeno"  verify="险种|code:Riskcode" 
		         ondblclick="return showCodeList('riskcode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('riskcode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true > 
        </td>
  </tr> 
    </table>
    <table  class= common>
        <TR class= common> 
		<TD  class= input width="26%">
			<input class= common type=Button value="打印报表" onclick="PolPrint();">
		</TD>			
	</TR>    	 
      </table>
      <input type=hidden id="Operate" name="Operate">      
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
