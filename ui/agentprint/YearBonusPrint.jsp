<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>    
<%
//程序名称：YearBonusPrint.jsp
//程序功能：F1报表生成
//创建日期：2009-04-15
//创建人  ：XX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT> 
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
<SCRIPT src="YearBonusPrint.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="../common/jsp/ManageComLimit.jsp"%>

</head>
<body onload="initElementtype();" >    
  <form action="./YearBonusPrintSave.jsp" method=post name=fm target="f1print">
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			 查询条件
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>
      	<TR  class= common>
      	 <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class='codeno' name=ManageCom verify = "管理机构|notnull&code:ComCode" 
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,4,'char(length(trim(comcode)))',1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,4,'char(length(trim(comcode)))',1);"
            ><Input  class='codename' name=ManageComName elementtype=nacessary> 
          </TD>
            <TD  class= title>
            销售机构代码 
          </TD>
          <TD  class= input>
            <Input  class=common  name=BranchAttr onchange="return checkBranchAttr()">
          </TD> 
           
        </TR>
        
         <TR>
          <TD  class= title>
            业务员代码 
          </TD>
          <TD  class= input>
            <Input  class=common  name=GroupAgentCode onchange="return checkAgentCode()">
          </TD> 
          <TD  class= title>
            业务员名称  
          </TD>
          <TD  class= input>
            <Input  class=common  name=AgentName onchange="return checkAgentName()">
          </TD> 
           
        </TR>
        
        <TR  class= common>
          <td  class= title> 统计方式
          	</td>
        <td  class= input>
         <Input class="codeno" name=Type verify="统计方式|notnull"  	CodeData="0|^0|按年统计|^1|按月统计" 
            ondblclick="return showCodeListEx('type',[this,TypeName],[0,1]);" 
            onkeyup="return showCodeListKeyEx('type',[this,TypeName],[0,1]);"><input class=codename name=TypeName readonly=true elementtype=nacessary>
         </td>    	
         </TR>
     </table>
    <Div id="Type1" style= "display:none" >
    <table class= common border=0 width=100%> 
       <TR  class= common>
          <td  class= title> 
            薪资年
           	</td>
        <td  class= input>
            <Input  class=common  name=WageNo verify="薪资年"  elementtype=nacessary>
          </TD>
         <TD  class= title>
           
          </TD>
          <TD  class= input>
           
          </TD>                   	 
      </TR>
    </table> 
    </Div>
 
 
    <Div id="Type2" style= "display:none">
    <table class= common> 
    <TR  class= common>
         <TD  class= title>
            薪资年月起期
          </TD>
          <TD  class= input>
            <Input class=common  name=StartCalNo verify="薪资年月起期"  elementtype=nacessary>
          </TD>        
          <TD  class= title>
            薪资年月止期
          </TD>
          <TD  class= input>
            <Input class=common  name=EndCalNo verify="薪资年月止期"  elementtype=nacessary>
          </TD>       
         </TR>
    </table> 
    </Div>
      
       


    <input type="hidden" name=op value="">
    <input type="hidden" name=name value="">
    <input type=hidden name=AgentGroup1 value="">   
    <input type=hidden name=AgentCode value=''>
    <INPUT TYPE="button" VALUE="打  印" class="cssButton"  onclick="submitForm();">
		<!--INPUT VALUE="下  载" class="cssButton" TYPE="button" onclick="download()"-->   </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 