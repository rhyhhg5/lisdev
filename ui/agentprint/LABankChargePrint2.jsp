<%
//程序名称：LABankChargePrint2.jsp
//程序功能：F1报表生成
//创建日期：2009-04-15
//创建人  ：XX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script>
 var tsql =" 1  and char(length(trim(comcode))) in (#4#,#8#) and comcode like #8644%# " ;
 var msql =" 1  and char(length(trim(comcode))) =#8# " ;
</script> 
<html>    

<%@page contentType="text/html;charset=GBK" %>
<%@page import="java.util.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String tManageCom=tG.ManageCom;
  int len=tManageCom.length();
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT> 
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>   
<SCRIPT src="LABankChargePrint2.js"></SCRIPT>   
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="../common/jsp/ManageComLimit.jsp"%>

</head>
<body onload="initElementtype();" >    
  <form action="./LABankChargePrint2Save.jsp" method=post name=fm target="f1print">
     <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			 查询条件
    		</td>
    	</tr>
      </table>
    <table class= common border=0 width=100%>
      <TR  class= common>
      	<TD class= title>
          管理机构
        </TD>
        <TD class= input>
          <Input name=ManageCom class='codeno' id="ManageCom"  verify="管理机构|NOTNULL"
           ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,1);"  
           onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,1);"
          ><Input name=ManageComName class="codename" readonly=true elementtype=nacessary>
        </TD>
        <td  class= title> 
          代理机构 
        </td>
        <td  class= input>
          <input class='codeno' name=AgentCom id="AgentCom"  verify="代理机构|NOTNULL"
           ondblclick="return getAgentCom(this,AgentComName);"
           onkeyup="return getAgentCom(this,AgentComName);"
         ><input class=codename readonly  name=AgentComName  elementtype=nacessary > 
         </td>    	
       </TR>
       <TR>
         <TD  class= title>
           查询年月
         </td>
         <td class= input>
          <Input class= 'common'   name=WageNo verify="查询年月|NOTNULL&len=6" elementtype=nacessary>
          <font color="red">(yyyymm)
         </td>
         <td  class= title>
           结算状态
         </td>
         <TD  class= input> 
          <Input class= "codeno" name=ChargeState CodeData="0|^1|已结算|^0|未结算|" 
           ondblclick="return showCodeListEx('F01list',[this,StateName],[0,1]);" 
           onkeyup="return showCodeListKeyEx('F01list',[this,StateName],[0,1]);" 
           onchange="" 
          ><input class=codename name=StateName  readonly=true >
         </TD> 
        </TR>
     </table>
   
    <input type="hidden" name=operator value="">

    <INPUT TYPE="button" VALUE="下  载" class="cssButton"  onclick="submitForm();">
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 