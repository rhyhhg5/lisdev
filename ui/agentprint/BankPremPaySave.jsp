<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：BankPremPaySave.jsp
//程序功能：F1报表生成
//创建日期：2009-03-04
//创建人  ：XX
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>

<html>
<title>
ReportUI
</title>
<head>
</head>
<body>
<%
boolean operFlag = true;
String flag = "0";
String FlagStr = "";
String Content = "";

CError cError = new CError( );
CErrors tError = null;
XmlExport txmlExport = new XmlExport();
CombineVts tcombineVts = null;

GlobalInput tG = new GlobalInput();
tG=(GlobalInput)session.getValue("GI");
System.out.println("111111111111111111111");

String ManageCom = request.getParameter("ManageCom");
String tBranchAttr = request.getParameter("BranchAttr");
String tAgentGroup = request.getParameter("AgentGroup1");
String tAgentCode = request.getParameter("AgentCode");
String tIndexCalNo = request.getParameter("IndexCalNo");
String tAgentName = request.getParameter("AgentName");
tAgentName=StrTool.unicodeToGBK(tAgentName);
TransferData tTransferData= new TransferData();
tTransferData.setNameAndValue("tManageCom",ManageCom);
tTransferData.setNameAndValue("tBranchAttr",tBranchAttr);
tTransferData.setNameAndValue("tAgentGroup",tAgentGroup);
tTransferData.setNameAndValue("tAgentCode",tAgentCode); 
tTransferData.setNameAndValue("tIndexCalNo",tIndexCalNo); 
tTransferData.setNameAndValue("tAgentName",tAgentName); 

System.out.println("2222222222222");
Calendar cal = new GregorianCalendar();
String year = String.valueOf(cal.get(Calendar.YEAR));
String month=String.valueOf(cal.get(Calendar.MONTH)+1);
String date=String.valueOf(cal.get(Calendar.DATE));
String hour=String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
String min=String.valueOf(cal.get(Calendar.MINUTE));
String sec=String.valueOf(cal.get(Calendar.SECOND));
String now = year + month + date + hour + min + sec + "_" ;

String  millis = String.valueOf(System.currentTimeMillis());  
String fileName = now + millis.substring(millis.length()-3, millis.length()) + ".xls";
String tOutXmlPath = application.getRealPath("vtsfile") + "/" + fileName;
tTransferData.setNameAndValue("OutXmlPath",tOutXmlPath);
System.out.println("33333333333333"+tOutXmlPath);

VData tVData = new VData();
tVData.addElement(tG);
tVData.addElement(tTransferData);
          
BankPremPayUI tBankPremPayUI = new BankPremPayUI(); 
    
try
{
    if (!tBankPremPayUI.submitData(tVData, "PRINT"))
    {
        Content = "报表下载失败，原因是:" + tBankPremPayUI.mErrors.getFirstError();
        FlagStr = "Fail";
    }
	  else
	  {
	 	  String FileName = tOutXmlPath.substring(tOutXmlPath.lastIndexOf("/") + 1);
	 	  File file = new File(tOutXmlPath);
	 	  
	        response.reset();
        response.setContentType("application/octet-stream"); 
        response.setHeader("Content-Disposition","attachment; filename="+FileName+"");
        response.setContentLength((int) file.length());
    
        byte[] buffer = new byte[10000];
        BufferedOutputStream output = null;
        BufferedInputStream input = null;    
        //写缓冲区
        try 
        {
            output = new BufferedOutputStream(response.getOutputStream());
            input = new BufferedInputStream(new FileInputStream(file));
      
        int len = 0;
        while((len = input.read(buffer)) >0)
        {
            output.write(buffer,0,len);
        }
        input.close();
        output.close();
        }
        catch (Exception e) 
        {
          e.printStackTrace();
         } // maybe user cancelled download
        finally 
        {
            if (input != null) input.close();
            if (output != null) output.close();
            file.delete();
        }
	   }
	}
	catch(Exception ex)
	{
	  ex.printStackTrace();
	}

if (!FlagStr.equals("Fail"))
{
	Content = "";
	FlagStr = "Succ";
}
%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
	 	
</script>
</html>
