<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="BankDayReportInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>民生人寿银行保险业绩日报表</title>
</head>
<body  onload="initForm();" >
<form action="./BankDayReportSave.jsp" method=post name=fm target="f1print">
 <table class=common>
  <tr class=common>
    <td class=title>统计时间</td>
    <td class=input><Input class="coolDatePicker" dateFormat="short" name=StaticDate verify="统计时间|NOTNULL"></td>
    <td class=input colspan=2><input class=common type=button value="银代业绩打印" onclick="submitForm();"></td>
  </tr>  
 </table>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
<script>
function initForm()
{
 fm.all("StaticDate").value = "";
}
</script>
</body>
</html> 