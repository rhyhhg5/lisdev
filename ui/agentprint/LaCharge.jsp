<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="LaCharge.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LaChargeInit.jsp"%>
  <title>机构代理业务费用汇总表 </title>   
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
	<td class= titleImg align= center>请输入查询条件：</td>
	</tr>
    </table>
    <table  class= common align=center>
        <TR>
          <TD  class= title width="25%">起始日期</TD>
          <TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=StartDay verify="起始日期|NOTNULL"> </TD>      
          <TD  class= title width="25%">结束日期</TD>
          <TD  class= input width="25%"><Input class= "coolDatePicker" dateFormat="short" name=EndDay  verify="结束日期|NOTNULL"></TD>  
      </TR> 
      	 <tr  class= common> 
           <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
              <Input class="codeno" name=AgentCom ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null,null,null,1);"><input class=codename name=AgentComName readonly=true >         
 
                      </TD>    
         <TD   class=title>
          银行类型</td>
          <td class=input>
          <Input class="codeno" name=BankType
          CodeData="0|^01|分行|^02|支行"
          ondblClick="showCodeListEx('BankType1List',[this,BankTypeName],[0,1]);"
          onkeyup="showCodeListKeyEx('BankType1List',[this,BankTypeName],[0,1]);"><input class=codename name=BankTypeName readonly=true > 
          </td>
          </TR> 
      	 <tr  class= common> 
          <td  class= title>手续费类型</td>
        <td  class= input> <input name=ChargeType class="codeno"  verify="手续费类型|NOTNULL"  verify="手续费类型|code:BankCharge" 
					ondblclick="EdorType2(this,ChargeTypeName);" onkeyup="KeyUp2(this,ChargeTypeName);"><input class=codename name=ChargeTypeName readonly=true > 	
	</td> 
           <td  class= title> 险种	</td>
        <td  class= input><input name=RiskCode class="codeno"  verify="险种|code:Riskcode" 
		         ondblclick="return showCodeList('riskcode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('riskcode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true > 
        </td>  
    </tr>
    </table>       
<table  class= common>
        <TR class= common> 
		<TD  class= input width="26%">
			<input class= common type=Button value="打印报表" onclick="PolPrint();">
		</TD>			
	</TR>    	 
      </table>
      <input type=hidden id="Operate" name="Operate">      
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
