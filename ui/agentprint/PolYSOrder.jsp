<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="PolYSOrder.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PolYSOrderInit.jsp"%>
  <title>个人业务竞赛排行榜 </title>   
</head>
<body  onload="initForm();" >
  <form method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
      	  <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" readonly name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true > 
          </TD>          
          </TR>          
          <TR>
          <TD  class= title width="25%">
            起始日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=StartDay verify="起始起期|NOTNULL">
          </TD>      
          <TD  class= title width="25%">
            结束日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=EndDay  verify="结束日期|NOTNULL">
          </TD>  
        </TR>
    </table>
    <table  class= common>
        <TR class= common> 
		<TD  class= input width="26%">
			<input class= common type=Button value="排行榜打印" onclick="PolPrint();">
		</TD>		
	</TR>    	 
      </table>
      <input type=hidden id="Operate" name="Operate">
      
  </form> 
   <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
