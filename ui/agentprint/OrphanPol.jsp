<%@include file="../common/jsp/UsrCheck.jsp"%> 
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：OrphanPol.jsp
//程序功能：
//创建日期：2003-5-19
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容 
%>
<%
  GlobalInput tGI = new GlobalInput();
     tGI=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
 %>
 <script language="javascript"> 
 function parselike()
	{
		var ManageCom = '<%=tGI.ManageCom%>';		
		var tReturn = " and branchattr like '"+ ManageCom +"%25' ";		
		return tReturn;
	}
 </script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="OrphanPol.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="OrphanPolInit.jsp"%>  
  <title>展业机构查询</title>   
</head>
<body  onload="initForm();" >
  <form action="./BranchGroupQueryQuery.jsp" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入查询条件：</td>
		</tr>
	</table>
    <table  class= common>
       <TR  class= common> 
          <TD  class= title>
            主险保单号
          </TD>          
          <TD  class= input>
            <Input class=common name=MainPolno >
          </TD>                
       </TR> 
       <TR  class= common> 
          <TD  class= title width="25%">
            起始日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=StartDay >
          </TD>      
          <TD  class= title width="25%">
            结束日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=EndDay >
          </TD>       
       </TR> 
       <TR  class= common> 
          <td  class= title>
		   原销售人员
		</td>
        <td  class= input>
		  <input class=common  name=AgentOld >
		</td>    
		<td  class= title>
		   现销售人员
		</td>
        <td  class= input>
		  <input class=common  name=AgentNew >
		</td>       
       </TR> 
    </table>  
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./OrphanPolPrt.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 归属保单信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
			<INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>

  	<p>
      <INPUT VALUE="打印变更通知书" TYPE=button onclick="agentQuery();"> 
  	</p>
     <input type=hidden id="fmtransact" name="fmtransact">
  	 <input type=hidden id="Polno" name="Polno">
     <input type=hidden name=AscriptionCount >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
