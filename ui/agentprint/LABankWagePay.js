//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
	if (verifyInput() == false) return false;
    //if (checkWage() == false) return false;
	//业务员类型：1--营销（代理） 1--直销（合同 ）
	var tAgentType =fm.all("AgentType").value;
	var tIndexCalNo = fm.all("IndexCalNo").value;
	var tSql ="";
//	if(1==tAgentType)
//	{
	tSql ="select * from (select a.managecom,a.indexcalno,b.branchattr,getUniteCode(a.AgentCode) ord,c.Name,"
			 +"(select gradename from laagentgrade where gradecode=a.agentgrade),"
	         +" case when c.agenttype='1' then '营销人员' when c.agenttype = '2' then '直销人员' else '人员类型' end,c.employdate,"
	         +" case when c.agentstate<='02' then '在职' when c.agentstate='03' then '离职登记' else '离职确认' end,"
	         +" c.outworkdate" 
	         +",(select cond1 from LAWageRadix2 where branchtype =a.branchtype and branchtype2=a.branchtype2 and wagecode ='YD001' and agentgrade=d.agentgrade and areatype = (select wageflag from LABankIndexRadix " +
  	          " where branchtype ='3' and branchtype2='01' and managecom=trim(substr(a.managecom,1,4))))"
	         +",(select to_char(T35)||'%' from laindexinfo where agentcode = a.agentcode and indexcalno =a.indexcalno and indextype = '00' )"
	          +",a.F07,a.F09"
	          if(tAgentType=='1')
	          {
	        	  tSql+=",a.F11,a.F08,a.F13,";
	          }
		      if(tAgentType=='2')
		      {
		    	  tSql+=",a.F20,a.F21,";
		      }
		      
		//如果为12月份需要添加年终奖字段
//		if("12"==tIndexCalNo.substring(4,6))
//		{
//			tSql +="'agenttype1_18',";
//		}
		      tSql+="a.F30,a.K12,a.F16,a.F17,a.F14,a.F15,a.F18,a.F19,a.F06, "
		    +" a.CurrMoney,a.CurrMoney,"
			 +" (case when a.state ='1' then '薪资已确认' else '薪资未确认' end)"
			 +" from LAWage a,labranchgroup b,laagent c ,latree d"
	         +" where a.agentcode = c.agentcode and b.agentgroup = a.agentgroup "
	         +" and c.agentgroup=b.agentgroup "
	         +" and c.agentcode=d.agentcode  and a.branchtype='3' "
	         +" and a.state = '0'"              
	         +" and c.BranchType='3' and a.IndexCalNo = '"+tIndexCalNo+"' and (b.state<>'1' or b.state is null)"
	         +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
	         +" and c.agenttype = '"+tAgentType+"'"
	         +" and a.branchtype = '3' and  a.branchtype2 = '01' "
	         +getWherePart('b.branchattr','BranchAttr')
	         +getWherePart('c.groupagentcode','GroupAgentCode')
	         +getWherePart('c.name','AgentName')
			 +"union"
			 +" select '合计','','','','','','',NULL,'',NULL,0,NULL"
	          +",value(sum(a.F07),0),value(sum(a.F09),0),"
	          if(tAgentType=='2')
	          {
	        	  tSql+="value(sum(a.F20),0),value(sum(a.F21),0),"
	          }
		      if(tAgentType=='1')
              {
		    	  tSql+=" value(sum(a.F11),0),value(sum(a.F08),0),value(sum(a.F13),0),"
              }
	          +",value(sum(a.F14-a.F15),0),value(sum(a.F18-a.F19),0), "
//		if("12"==tIndexCalNo.substring(4,6))
//		{
//			tSql +="'agenttype1_18',";
//		}
	          tSql+="value(sum(a.F30),0),value(sum(a.K12),0),value(sum(a.F16),0),value(sum(a.F17),0),value(sum(a.F14),0), "     
		 +"sum(a.F15),value(sum(a.F18),0),value(sum(a.F19),0),value(sum(a.F06),0),value(sum(a.CurrMoney),0),value(sum(a.CurrMoney),0),"
			 +" ''"
			 +" from LAWage a,labranchgroup b,laagent c ,latree d"
	         +" where a.agentcode = c.agentcode and b.agentgroup = a.agentgroup "
	         +" and c.agentgroup=b.agentgroup "
	         +" and c.agentcode=d.agentcode  and a.branchtype='3' "
	         +" and a.state = '0'"              
	         +" and c.BranchType='3' and a.IndexCalNo = '"+tIndexCalNo+"' and (b.state<>'1' or b.state is null)"
	         +" and a.ManageCom like '"+fm.all('ManageCom').value+"%' "
	         +" and c.agenttype = '"+tAgentType+"'"
	         +" and a.branchtype = '3' and  a.branchtype2 = '01' "
	         +getWherePart('b.branchattr','BranchAttr')
	         +getWherePart('c.groupagentcode','GroupAgentCode')
	         +getWherePart('c.name','AgentName')
	          +") kk order by ord desc with ur"; 
//	}
//	else if(2==tAgentType)
//	{
//		tSql ="select 'agenttype2_1','agenttype2_2','agenttype2_3','agenttype2_4','agenttype2_5','agenttype2_6','agenttype2_7','agenttype2_8','agenttype2_9','agenttype2_10','agenttype2_11','agenttype2_12','agenttype2_13','agenttype2_14','agenttype2_15','agenttype2_16','agenttype2_17',";
//		//如果为12月份需要添加年终奖字段
//		if("12"==tIndexCalNo.substring(4,6))
//		{
//			tSql +="'agenttype2_18',";
//		}
//		tSql +="'agenttype2_19','agenttype2_20','agenttype2_21','agenttype2_22' from dual" ;
//	}
	var i = 0;
	fm.all("querySql").value = tSql;
	var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
	fm.reset();
	showInfo.close();
}
function checkBranchAttr()
{
	var tReturn = getManageComLimitlike("ManageCom");
  var sql=" select agentgroup,branchlevel  from labranchgroup  where branchattr='"+fm.BranchAttr.value+"'"
         +" and  branchtype='3'	 and branchtype2='01'    "
         +tReturn
         +getWherePart("ManageCom","ManageCom",'like')	;
  var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
 // alert(sql);
   if (!strQueryResult) 
    {
      alert("此管理机构没有此销售单位！");  
      fm.BranchAttr.value="";
      return;
    }	
    var arr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentGroup').value=arr[0][0];
    
}	
function checkAgentCode()
{
	var tReturn = getManageComLimitlike("ManageCom");
  var sql=" select agentcode  from laagent   where  groupagentcode='"+fm.GroupAgentCode.value+"'"
         +" and  branchtype='3'	 and branchtype2='01'     "
         + tReturn  
         + getWherePart("ManageCom","ManageCom",'like')	;
   var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
   if (!strQueryResult) 
    {
     alert("此管理机构没有此销售员！");  
     fm.GroupAgentCode.value="";
     return;
    }
 //xqq 2014-11-30   
     var arrDataSet = decodeEasyQueryResult(strQueryResult);
    var tArr = new Array();
    tArr = decodeEasyQueryResult(strQueryResult);
    fm.all('AgentCode').value  =tArr[0][0];
//    alert(fm.all('AgentCode').value );
}	
function checkAgentName()
{
	var tReturn = getManageComLimitlike("ManageCom");
  var sql=" select agentcode  from laagent   where  name='"+fm.AgentName.value+"'"
         +" and  branchtype='3'	 and branchtype2='01'     "
         + tReturn  
         + getWherePart("ManageCom","ManageCom",'like')	;
   var strQueryResult = easyQueryVer3(sql, 1, 0, 1);
  //判断是否查询成功
   if (!strQueryResult) 
    {
     alert("此管理机构没有此销售员！");  
     fm.AgentCode.value="";
     return;
    }
}	
function checkWage()
{
var tindexcalno = fm.all('IndexCalNo').value;
var tmanagecom = fm.all('ManageCom').value;
var checkSql = "select state from lawage where branchtype = '3' and branchtype2 = '01' and managecom like '"+tmanagecom+"%' and indexcalno ='"+tindexcalno+"' ";
  var strQueryResult  = easyQueryVer3(checkSql, 1, 1, 1);
  var arr = decodeEasyQueryResult(strQueryResult);
  if(strQueryResult){
  if(arr[0][0]!="1"){
     alert("本月薪资计算未确认,不能下载当月银代薪资报表!");
     return false;
  }
  return true;
}
}