<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var managecom = "<%=tGI.ManageCom%>"; //记录管理机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="BankCalDetailPrtInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
  <title>银行结算明细清单</title>
</head>
<body  onload="initForm();" >
<form action="./BankCalDetailPrtSave.jsp" method=post name=fm target="f1print">
 <table class=common>
  <tr class=common>
    <td class=title>统计起期</td>
    <td class=input><Input class="coolDatePicker" dateFormat="short" name=StartDate verify="统计起期|NOTNULL"></td>
    <td class=title>统计止期</td>
    <td class=input><Input class="coolDatePicker" dateFormat="short" name=EndDate verify="统计止期|NOTNULL"></td>
  </tr>
  <tr class=common>
           <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
              <Input class="codeno" name=AgentCom ondblclick="return showCodeList('AgentCom',[this,AgentComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('AgentCom',[this,AgentComName],[0,1],null,null,null,1);"><input class=codename name=AgentComName readonly=true >          
                      </TD>   
    
    <td class=title>计算类型</td>
    <td class=input>
    <input name=CalType class="codeno" verify="计算类型|NOTNULL"
		   ondblclick="return showCodeList('bankcharge',[this,CalTypeName],[0,1]);" 
		   onkeyup="return showCodeListKey('bankcharge',[this,CalTypeName],[0,1]);"><input class=codename name=CalTypeName readonly=true >    
    </td>
  </tr>
  <tr class=common>  
           <td  class= title> 险种	</td>
        <td  class= input><input name=RiskCode class="codeno"  verify="险种|code:Riskcode" 
		         ondblclick="return showCodeList('riskcode',[this,RiskCodeName],[0,1]);" onkeyup="return showCodeListKey('riskcode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true > 
        </td>
    <td class=input colspan=2><input class=common type=button value="打    印" onclick="submitForm();"></td>
  </tr>  
 </table>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
<script>
function initForm()
{
 fm.all("StartDate").value = "";
 fm.all("EndDate").value = "";
 fm.all("AgentCom").value = "";
 fm.all("CalType").value = "";
 fm.all("RiskCode").value = "";
}
</script>
</body>
</html> 