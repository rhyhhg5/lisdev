 <%
//程序名称：
//程序功能：
//创建日期：
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">                                

function initForm()
{
  try
  {
    initInpBox();
    initAgentGrid();  
    initContGrid();  
  }
  catch(re)
  {
    alert("LAJGrpContInput.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initInpBox()
{ 

  try
  {                                   

    fm.all('ManageCom').value = '';
    fm.all('Type').value = '';
    fm.all('StartMonth').value = '';
    fm.all('EndMonth').value = '';
    fm.all('StartDate').value = '';
    fm.all('EndDate').value = '';
    fm.all('ContNo').value = '';
    fm.all('RiskCode').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
    fm.all('WrapCode').value = '';
    fm.all('WrapCodeName').value = '';
    
    fm.all('BranchType').value = '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';
   
  }
  catch(ex)
  {
    alert("在LAJGrpContInput.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 险种授权的初始化
function initAgentGrid()
  {                               
      var iArray = new Array();
      
      try
      {
     iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="所属年月";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="展业机构号码";         		//列名
      iArray[2][1]="160px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="业务员代码";         		//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="业务员姓名";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="团单号";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="投保人";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="被保人";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[8]=new Array();
      iArray[8][0]="套餐编码";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 									//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="险种";         		//列名
      iArray[9][1]="100px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0; 				
      
      
      
  
      iArray[10]=new Array();
      iArray[10][0]="险种编码";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0; 									//是否允许输入,1表示允许，0表示不允许      

 			iArray[11]=new Array();
      iArray[11][0]="保费";         		//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0; 									//是否允许输入,1表示允许，0表示不允许  
      
      iArray[12]=new Array();
      iArray[12][0]="佣金比例";         		//列名
      iArray[12][1]="100px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0; 									//是否允许输入,1表示允许，0表示不允许   
      
      iArray[13]=new Array();
      iArray[13][0]="佣金";         		//列名
      iArray[13][1]="100px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0; 									//是否允许输入,1表示允许，0表示不允许     
      
      iArray[14]=new Array();
      iArray[14][0]="缴费方式";         		//列名
      iArray[14][1]="100px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=0; 	  								//是否允许输入,1表示允许，0表示不允许       
      
      iArray[15]=new Array();
      iArray[15][0]="缴费期";         		//列名
      iArray[15][1]="100px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=0; 									//是否允许输入,1表示允许，0表示不允许     
      
      iArray[16]=new Array();
      iArray[16][0]="缴费次数";         		//列名
      iArray[16][1]="100px";            		//列宽
      iArray[16][2]=100;            			//列最大值
      iArray[16][3]=0; 	

      iArray[17]=new Array();                             
      iArray[17][0]="缴费时间";         		//列名          
      iArray[17][1]="100px";            		//列宽          
      iArray[17][2]=100;            			//列最大值      
      iArray[17][3]=0; 	                                  
      
      iArray[18]=new Array();                             
      iArray[18][0]="客户签字日期";         		//列名          
      iArray[18][1]="100px";            		//列宽          
      iArray[18][2]=100;            			//列最大值      
      iArray[18][3]=0; 	                                  

      iArray[19]=new Array();
      iArray[19][0]="保单回执回销日期";         		//列名
      iArray[19][1]="0px";            		//列宽
      iArray[19][2]=100;            			//列最大值
      iArray[19][3]=0; 	      
      
      
      iArray[20]=new Array();
      iArray[20][0]="交单时间";         		//列名
      iArray[20][1]="100px";            		//列宽
      iArray[20][2]=100;            			//列最大值
      iArray[20][3]=0; 	          
      
      iArray[21]=new Array();
      iArray[21][0]="新单标识";         		//列名
      iArray[21][1]="100px";            		//列宽
      iArray[21][2]=100;            			//列最大值
      iArray[21][3]=0;
      
      iArray[22]=new Array();
      iArray[22][0]="类型";         		//列名
      iArray[22][1]="50px";            		//列宽
      iArray[22][2]=100;            			//列最大值
      iArray[22][3]=0;
      
      iArray[23]=new Array();
      iArray[23][0]="人员状态";         		//列名
      iArray[23][1]="50px";            		//列宽
      iArray[23][2]=100;            			//列最大值
      iArray[23][3]=0; 	   
      
      AgentGrid = new MulLineEnter( "fm" , "AgentGrid" ); 
      //这些属性必须在loadMulLine前
      AgentGrid.mulLineCount = 0;   
      AgentGrid.displayTitle = 1;
      AgentGrid.locked=1;   
      AgentGrid.canSel=1;
      AgentGrid.canChk=0;  
      AgentGrid.hiddenPlus = 1;
      AgentGrid.hiddenSubtraction = 1;
      AgentGrid.selBoxEventFuncName = "setContValue";
      AgentGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}
/************************************************************
 *initContGrid            
 *输入：          没有
 *输出：          没有
 *功能：          初始化ContGrid
 ************************************************************
 */
function initContGrid()
  {                               
    var iArray = new Array();
      
      try
      {
           
	      iArray[0]=new Array();
		    iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
		    iArray[0][1]="30px";            		//列宽
		    iArray[0][2]=10;            			//列最大值
		    iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		    iArray[1]=new Array();
		    iArray[1][0]="导入批次号";         		//列名
		    iArray[1][1]="80px";            		//列宽
		    iArray[1][2]=100;            			//列最大值
		    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		    
		    iArray[2]=new Array();
		    iArray[2][0]="管理机构";         		//列名
		    iArray[2][1]="80px";            		//列宽
		    iArray[2][2]=100;            			//列最大值
		    iArray[2][3]=0; 
		  
		    iArray[3]=new Array();
		    iArray[3][0]="结算单号";         		//列名
		    iArray[3][1]="80px";            		//列宽
		    iArray[3][2]=100;            			//列最大值
		    iArray[3][3]=0; 
		    
		    iArray[4]=new Array();
		    iArray[4][0]="单证流水号";         		//列名
		    iArray[4][1]="80px";            		//列宽                                              
		    iArray[4][2]=100;            			//列最大值                                              
		    iArray[4][3]=0;                   
		    
		    iArray[5]=new Array();
		    iArray[5][0]="单证状态";         		//列名
		    iArray[5][1]="80px";            		//列宽
		    iArray[5][2]=100;            			//列最大值
		    iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		    iArray[6]=new Array();
		    iArray[6][0]="业务员";         		//列名
		    iArray[6][1]="80px";            		//列宽
		    iArray[6][2]=100;            			//列最大值
		    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		    
		    
		    iArray[7]=new Array();
		    iArray[7][0]="被保险人";         		//列名
		    iArray[7][1]="80px";            		//列宽
		    iArray[7][2]=100;            			//列最大值
		    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		    
		    iArray[8]=new Array();
		    iArray[8][0]="保费";         		//列名
		    iArray[8][1]="80px";            		//列宽
		    iArray[8][2]=200;            			//列最大值
		    iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
		  
		    iArray[9]=new Array();
		    iArray[9][0]="单证类型";         		//列名
		    iArray[9][1]="80px";            		//列宽
		    iArray[9][2]=200;            			//列最大值
		    iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许		
		    
		    iArray[10]=new Array();
		    iArray[10][0]="单证名称";         		//列名
		    iArray[10][1]="80px";            		//列宽
		    iArray[10][2]=200;            			//列最大值
		    iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许		  
		    
		    iArray[11]=new Array();
		    iArray[11][0]="导入日期";         		//列名
		    iArray[11][1]="80px";            		//列宽
		    iArray[11][2]=200;            			//列最大值
		    iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许		    
		   
        iArray[12]=new Array();
		    iArray[12][0]="结算日期";         		//列名
		    iArray[12][1]="80px";            		//列宽
		    iArray[12][2]=200;            			//列最大值
		    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许		    
		   
		    iArray[13]=new Array();
		    iArray[13][0]="签单日期";         		//列名
		    iArray[13][1]="80px";            		//列宽
		    iArray[13][2]=200;            			//列最大值
		    iArray[13][3]=0;              			//是否允许输入,1表示允许，0表示不允许		    
		   
		    iArray[14]=new Array();
		    iArray[14][0]="生效日期";         		//列名
		    iArray[14][1]="80px";            		//列宽
		    iArray[14][2]=200;            			//列最大值
		    iArray[14][3]=0;              			//是否允许输入,1表示允许，0表示不允许		    
		   
        ContGrid = new MulLineEnter( "fm" , "ContGrid" ); 
      
        //这些属性必须在loadMulLine前
        ContGrid.mulLineCount = 0;   
        ContGrid.displayTitle = 1;
        ContGrid.canSel=0;
        ContGrid.canChk=0;
        ContGrid.locked=1;
        ContGrid.hiddenPlus = 1;
      	ContGrid.hiddenSubtraction = 1;
      
        ContGrid.loadMulLine(iArray);  
      
      }
      catch(ex)
      {
        alert("初始化ContGrid时出错："+ ex);
      }
    } 
      


</script>
