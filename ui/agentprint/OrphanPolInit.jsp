<%
//程序名称：OrphanPolInit.jsp
//程序功能：
//创建日期：2003-05-12
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {     
    fm.all('MainPolno').value ='';
  fm.all('StartDay').value ='';
  fm.all('EndDay').value ='';
  fm.all('AgentOld').value ='';
  fm.all('AgentNew').value =''; 
  }
  catch(ex)
  {
    alert("在OrphanPolInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    

function initForm()
{
  try
  {
    initInpBox();    
    initPolGrid();   
  }
  catch(re)
  {
    alert("OrphanPolInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var PolGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="主险保单号码";          		        //列名
      iArray[1][1]="200px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="归属次数";         		        //列名
      iArray[2][1]="80px";            			//列宽
      iArray[2][2]=20;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="原销售人员";      	   		//列名
      iArray[3][1]="100px";            			//列宽
      iArray[3][2]=10;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="现销售人员";      	   		//列名
      iArray[4][1]="100px";            			//列宽
      iArray[4][2]=10;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="归属日期";      	   		//列名
      iArray[5][1]="100px";            			//列宽
      iArray[5][2]=10;            			//列最大值
      iArray[5][3]=0;           			//是否允许输入,1表示允许，0表示不允许          

			
      PolGrid = new MulLineEnter( "fmSave" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 10;   
      PolGrid.displayTitle = 1;
      //PolGrid.locked = 1;
      PolGrid.canSel = 1;
      PolGrid.hiddenPlus=1;
      PolGrid.hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>