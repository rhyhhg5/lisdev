<%
//程序名称：LAChnlActiveInput.jsp
//程序功能：渠道报表
//创建日期：2009-04-20
//创建人  ：Elsa
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<script>
 var tsql =" 1  and char(length(trim(comcode))) in (#4#,#8#,#2#)  " ;
 var msql =" 1  and char(length(trim(comcode))) =#8# " ;

</script> 
<html>    
<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAChnlActiveInput.js"></SCRIPT>   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAChnlActiveInit.jsp"%>     
  <title>网点活动率报表</title>
</head>      
<body  onload="initForm();initElementtype();" >    
  <form action= "./LAChnlActiveReport.jsp" method=post name=fm target="fraSubmit">
  <table>
    	<tr>
    		<td>
    		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		 <td class= titleImg>
        		网点活动率统计条件
       		 </td>   		 
    	</tr>
    </table>
    <Div  id= "divAgent1" style= "display: ''">
   <Table class= common>
     <TR class= common> 
     	    <TD class= title>
          管理机构
        </TD>
        <TD class= input>
          <Input name=ManageCom class='codeno' id="ManageCom"  verify="管理机构|NOTNULL"
           ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,1);"  
           onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,1);"
          ><Input name=ManageComName class="codename" readonly=true elementtype=nacessary>
        </TD>
          <TD  class= title>统计层级</TD>
          <TD  class= input>
            <Input class="codeno" name=Level verify="统计层级|notnull"  	CodeData="0|^0|分公司|^1|中支公司|^2|营业部|^3|营业组|^4|业务经理" 
            ondblclick="return showCodeListEx('Level',[this,LevelName],[0,1]);" 
            onkeyup="return showCodeListKeyEx('Level',[this,LevelName],[0,1]);"><input class=codename name=LevelName readonly=true elementtype=nacessary>
          </TD>
         
          </TR>
          <tr class=common>
		    	<TD  class= title>
          统计止期
          </td>
          <td class= input>
          <Input class= 'common'   name=EndMonth verify="统计止期|NOTNULL&len=6" elementtype=nacessary>
          <font color="red">(yyyymm)
          </td>
          <td  class= title> 统计方式</td>
        <td  class= input>
         <Input class="codeno" name=Mode verify="统计方式|notnull"  	CodeData="0|^0|按月统计|^1|按季度统计|^2|按年统计" 
            ondblclick="return showCodeListEx('mode',[this,ModeName],[0,1]);" 
            onkeyup="return showCodeListKeyEx('mode',[this,ModeName],[0,1]);"><input class=codename name=ModeName readonly=true elementtype=nacessary>
         </td>    	
       	</TR>
       	 </TR>
         
          <td  class= title> 银行机构 </td>
        <td  class= input><input class=codeno name=AgentCom verify="银行机构编码"
         ondblclick="return getAgentCom(this,AgentComName);"
         onkeyup="return getAgentCom(this,AgentComName);"><input class=codename readonly  name=AgentComName > 
         </td>    	
       	</TR>
       	
   	</Table>  
   	</Div>
    <INPUT VALUE="打  印" class= cssbutton TYPE=button onclick="submitForm();"> 	
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html> 	
