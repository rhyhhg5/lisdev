<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAChnlActiveReport.jsp
//程序功能：
//创建日期：2009-04-20 18:03:35
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.f1print.*"%>
<%@page import="com.sinosoft.lis.agentprint.*"%>
<%@page import="java.io.*"%>

<%
  //接收信息，并作校验处理。
 
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  boolean operFlag=true;
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");
	
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   String tLevel = request.getParameter("Level");
   String tManageCom = request.getParameter("ManageCom");
   String tEndMonth = request.getParameter("EndMonth");
   String tMode = request.getParameter("Mode");
   String tAgentCom = request.getParameter("AgentCom");
  
   XmlExport txmlExport = new XmlExport();
   VData tVData = new VData();
   CombineVts tcombineVts = null;
  // 准备传输数据 VData
	tVData.add(tLevel);
	tVData.add(tManageCom);
	tVData.add(tEndMonth);
	tVData.add(tMode);
	tVData.add(tAgentCom);
 	tVData.add(tG);
 
  LAChnlActiveReportBL tLAChnlActiveReportBL   = new LAChnlActiveReportBL();
 
 try{

    if(!tLAChnlActiveReportBL.submitData(tVData,"PRINT"))
    {
       operFlag = false;
       Content = tLAChnlActiveReportBL.mErrors.getFirstError();  
       System.out.println(Content);
    }
    
    VData mResult = tLAChnlActiveReportBL.getResult();
    System.out.println("=======qwewewe===========");	
    txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
    if (txmlExport==null)
    {
	  System.out.println("null");
    }
    System.out.println("开始打开报表!");
    ExeSQL tExeSQL = new ExeSQL();
    //获取临时文件名
    String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
    String strFilePath = tExeSQL.getOneValue(strSql);
    String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
    //获取存放临时文件的路径
    String strRealPath = application.getRealPath("/").replace('\\','/');
    String strVFPathName = strRealPath +"//"+ strVFFileName;

    //合并VTS文件
   String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
   tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);

   ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
   tcombineVts.output(dataStream);

   //把dataStream存储到磁盘文件
   AccessVtsFile.saveToFile(dataStream,strVFPathName);
   System.out.println("==> Write VTS file to disk ");
   System.out.println("===strVFFileName : "+strVFFileName);
   //本来打算采用get方式来传递文件路径
   response.sendRedirect("../uw/GetF1PrintJ1.jsp?Code=07&RealPath="+strVFPathName);
}
catch(Exception ex)
{
    Content = "失败，原因是:" + ex.toString();
    FlagStr = "Fail";
    tError = tLAChnlActiveReportBL.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 处理失败! ";
      FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<script language="javascript">	
	alert("<%=Content%>");
	top.close();	
	 	
</script>
</html>
<%
  }
%>