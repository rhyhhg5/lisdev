<%
//程序名称：AgentMoveTrackInit.jsp
//程序功能：
//创建日期：2003-07-16
//创建人  ：liujw
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="LAAssessInput.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//添加页面控件的初始化。
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("session has expired");
		return;
	}
	
	String strOperator = globalInput.Operator;
	//String strManageCom = globalInput.ManageCom;
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {     
    //fm.all('AgentCode').value = '<%=request.getParameter("AgentCode")%>';
    if (top.opener != null)
    {
	    fm.all('AgentCode').value = top.opener.fm.AgentCode.value;    
	    if (fm.all('AgentCode').value == 'null')
	      fm.all('AgentCode').value = '';
	      
	    fm.all('Name').value = top.opener.fm.Name.value;
	    if (fm.all('Name').value == 'null')
	      fm.all('Name').value = '';
	      
	    fm.all('ManageCom').value = top.opener.fm.ManageCom.value;    
	    if (fm.all('ManageCom').value == 'null')
	      fm.all('ManageCom').value = '';
	    
	    fm.all('BranchType').value = top.opener.fm.BranchType.value;    
	    if (fm.all('BranchType').value == 'null')
	      fm.all('BranchType').value = '';
    }
    else
    {
      fm.all('AgentCode').value = '';
      fm.all('Name').value = '';
      fm.all('ManageCom').value = '';
      fm.all('BranchType').value = '';
    }
    //fm.all('BranchType').value = getBranchType();
  }
  catch(ex)
  {
    alert("在AgentMoveTrackInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AgentMoveTrackInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox(); 
    initMoveTrackGrid();   
  }
  catch(re)
  {
    alert("AgentMoveTrackInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
 var MoveTrackGrid;          //定义为全局变量，提供给displayMultiline使用
// 投保单信息列表的初始化
function initMoveTrackGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="代理人代码";         		//列名
      iArray[1][1]="0px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="职级";      		//列名
      iArray[2][1]="50px";            		//列宽
      iArray[2][2]=200;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许，2表示下拉
      //iArray[2][9]="职级|code:AgentGrade"
      //iArray[2][10]='AgentGrade';		
      //iArray[2][11]=strRT;
      //iArray[2][12]="1|4|2|5";     //引用代码对应第几列，'|'为分割符 --Muline中的列位置
      //iArray[2][13]="0|2|3|4";    //上面的列中放置引用代码中第几位值 --sql中位置
           
      iArray[3]=new Array();
      iArray[3][0]="代理人组别";         		//列名
      iArray[3][1]="155px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="上级代理人";         		//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许	
      
      iArray[5]=new Array();
      iArray[5][0]="推荐人";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="组育成人";         		//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
      iArray[7]=new Array();
      iArray[7][0]="部育成人";         		//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
      iArray[8]=new Array();
      iArray[8][0]="督导长育成人";         		//列名
      iArray[8][1]="80px";            		//列宽
      iArray[8][2]=200;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
      iArray[9]=new Array();
      iArray[9][0]="区督导长育成人";         		//列名
      iArray[9][1]="85px";            		//列宽
      iArray[9][2]=200;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
	
      iArray[10]=new Array();
      iArray[10][0]="起期";         		//列名
      iArray[10][1]="75px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
			
      MoveTrackGrid = new MulLineEnter( "fm" , "MoveTrackGrid" ); 
      //这些属性必须在loadMulLine前
      MoveTrackGrid.displayTitle = 1;
      MoveTrackGrid.locked = 1;
      MoveTrackGrid.canSel = 0;
      MoveTrackGrid.hiddenPlus = 1;
      MoveTrackGrid.hiddenSubtraction = 1; 
      MoveTrackGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //MoveTrackGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>