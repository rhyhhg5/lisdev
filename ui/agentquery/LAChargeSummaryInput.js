//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LAChargeSummaryInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
	//fm.all('divBankLicense').style.display='';
	//fm.all('divSpanBankLicenseGrid').style.display='none';
    //首先检验录入框
  if(!verifyInput()) return false;
  var strSQL="";
  var tEndFlag=fm.all('EndFlag').value;
  var strSQL = "select temp.managecom,temp.managecomname,temp.agentcom,temp.agentcomname,"	 
             +"temp.actype,value(sum(temp.charge1),0),value(sum(temp.charge2),0),temp.endflag,temp.enddate"
             +" from "
             +" ( "
             +"select a.managecom managecom,(select name from ldcom where comcode=a.managecom) managecomname,"
             +"a.agentcom agentcom,(select name from lacom where agentcom=a.agentcom) agentcomname,"
             +"(case (select actype from lacom where agentcom=a.agentcom)"
             +"when '02' then '兼业代理'when '03' then '专业保险代理' when '04' then '保险经纪' "
             +"when '05' then '共保机构' when '06' then '财险' when '07' then '寿险' when '99' then '其他' end) actype,"
             +"(case when a.chargestate in ('1','0') then value(sum(a.charge),0) end ) charge1,"
             +"(case when a.chargestate ='1' then value(sum(a.charge),0) end ) charge2, "
             +"(case (select endflag from lacom where agentcom=a.agentcom) when 'N' then '有效' when 'Y' then '无效' end) endflag ,"
             +"(select enddate from lacom where agentcom=a.agentcom) enddate "
	         +"from lacharge a  where 1=1"
	         +" and a.tmakedate>='"+fm.all('StartDate').value+"' and a.tmakedate<='"+fm.all('EndDate').value+"'"
	         +" and a.managecom like '"+fm.all('ManageCom').value+"%'"
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
        if(fm.all('AgentCom').value!=null&&fm.all('AgentCom').value!='')
        {
        strSQL=strSQL+" and a.agentcom ='"+fm.all('AgentCom').value+"'";
        }
        if(fm.all('ACType').value!=null&&fm.all('ACType').value!='')
        {
        strSQL=strSQL+" and a.agentcom in (select agentcom from lacom where branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"' and actype='"+fm.all('ACType').value+"')";
        }
        if(tEndFlag!=null && tEndFlag!='' && tEndFlag=='N')
        {
        strSQL=strSQL+" and a.agentcom in (select agentcom from lacom where endflag='N' and agentcom=a.agentcom)";
        }
        if(tEndFlag!=null && tEndFlag!='' && tEndFlag=='Y')
        {
        strSQL=strSQL+" and a.agentcom in (select agentcom from lacom where endflag='Y' and agentcom=a.agentcom)";
        }
        strSQL=strSQL+" group by a.agentcom,a.chargestate,a.managecom";  
        
        strSQL=strSQL+") as temp ";
        
        strSQL=strSQL+" group by temp.agentcom,temp.agentcomname,temp.managecom,temp.managecomname,temp.actype,temp.endflag,temp.enddate";
	 
	    turnPage.queryModal(strSQL, ChargeSummaryGrid);  
	         
	  if (ChargeSummaryGrid.mulLineCount == 0)
	  {
	    alert("没有查询到相应的代理机构信息！");
	    return false;
	  }
	
}

function beforeSubmit()
{
   return true;
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#2# and BranchType2=#02# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}

function ListExecl()
{
if (ChargeSummaryGrid.mulLineCount == 0)
{
  alert("列表中没有数据可下载");
  return false;
}
//定义查询的数据
var strSQL = "";
var tEndFlag=fm.all('EndFlag').value;
var strSQL = "select temp.managecom,temp.managecomname,temp.agentcom,temp.agentcomname,"	 
             +"temp.actype,value(sum(temp.charge1),0),value(sum(temp.charge2),0),temp.endflag,temp.enddate"
             +" from "
             +" ( "
             +"select a.managecom managecom,(select name from ldcom where comcode=a.managecom) managecomname,"
             +"a.agentcom agentcom,(select name from lacom where agentcom=a.agentcom) agentcomname,"
             +"(case (select actype from lacom where agentcom=a.agentcom)"
             +"when '02' then '兼业代理'when '03' then '专业保险代理' when '04' then '保险经纪' "
             +"when '05' then '共保机构' when '06' then '财险' when '07' then '寿险' when '99' then '其他' end) actype,"
             +"(case when a.chargestate in ('1','0') then value(sum(a.charge),0) end ) charge1,"
             +"(case when a.chargestate ='1' then value(sum(a.charge),0) end ) charge2, "
             +"(case (select endflag from lacom where agentcom=a.agentcom) when 'N' then '有效' when 'Y' then '无效' end) endflag ,"
             +"(select enddate from lacom where agentcom=a.agentcom) enddate "
	         +"from lacharge a  where 1=1"
	         +" and a.tmakedate>='"+fm.all('StartDate').value+"' and a.tmakedate<='"+fm.all('EndDate').value+"'"
	         +" and a.managecom like '"+fm.all('ManageCom').value+"%'"
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
        if(fm.all('AgentCom').value!=null&&fm.all('AgentCom').value!='')
        {
        strSQL=strSQL+" and a.agentcom ='"+fm.all('AgentCom').value+"'";
        }
        if(fm.all('ACType').value!=null&&fm.all('ACType').value!='')
        {
        strSQL=strSQL+" and a.agentcom in (select agentcom from lacom where branchtype='"+fm.all('BranchType').value+"' and branchtype2='"+fm.all('BranchType2').value+"' and actype='"+fm.all('ACType').value+"')";
        }
        if(tEndFlag!=null && tEndFlag!='' && tEndFlag=='N')
        {
        strSQL=strSQL+" and a.agentcom in (select agentcom from lacom where endflag='N' and agentcom=a.agentcom)";
        }
        if(tEndFlag!=null && tEndFlag!='' && tEndFlag=='Y')
        {
        strSQL=strSQL+" and a.agentcom in (select agentcom from lacom where endflag='Y' and agentcom=a.agentcom)";
        }
        strSQL=strSQL+" group by a.agentcom,a.chargestate,a.managecom";  
        
        strSQL=strSQL+") as temp ";
        
        strSQL=strSQL+" group by temp.agentcom,temp.agentcomname,temp.managecom,temp.managecomname,temp.actype,temp.endflag,temp.enddate";
     
fm.querySql.value = strSQL;

//定义列名
var strSQLTitle = "select '管理机构代码','管理机构名称','中介机构代码','代理机构名称','中介机构类型','应付手续费金额','已结算手续费金额','中介机构状态','合作终止日期' from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
//定义表名
fm.all("Title").value="select '中介机构团险业务手续费金额汇总报表' from dual where 1=1  ";  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}

