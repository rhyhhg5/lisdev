<%
//程序名称：GroupDayQryInit.jsp
//程序功能：
//创建日期：2003-10-18
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>      
<%@page import="com.sinosoft.utility.*"%>       
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
     ExeSQL tExe = new ExeSQL();
     String tSql = "select getbranchattr(branchcode) from laagent where agentcode = '"
                 + tG.Operator.trim() + "'";
     String tBranchAttr = tExe.getOneValue(tSql);    
     
     ExeSQL tExe1 = new ExeSQL();        
     tSql = "select getbranchattr(agentgroup) from latree where agentcode = '"
          + tG.Operator.trim() + "'";
     String tAgentGroup = tExe1.getOneValue(tSql);
%>                                       
  
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
    fm.all('AgentCode').value = '<%=tG.Operator%>';
    fm.all('BranchAttr').value = '<%=tBranchAttr%>';
    fm.all('AgentGroup').value = '<%=tAgentGroup%>';
    fm.all('QueryDate').value = '';
    
  }
  catch(ex)
  {
    alert("在GroupDayQryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                                

function initForm()
{
  try
  {
    initInpBox();
    initGroupDayGrid();
  }
  catch(re)
  {
    alert("GroupDayQryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentGrid
 ************************************************************
 */
function initGroupDayGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名
        
        iArray[1]=new Array();
        iArray[1][0]="代理人编码";         //列名
        iArray[1][1]="130px";         //列名
        iArray[1][2]=100;         //列名
        iArray[1][3]=0;         //列名
        
        iArray[2]=new Array();
        iArray[2][0]="代理人姓名";         //列名
        iArray[2][1]="100px";         //列名
        iArray[2][2]=100;         //列名
        iArray[2][3]=0;         //列名
        
        iArray[3]=new Array();
        iArray[3][0]="代理人组别";         //列名
        iArray[3][1]="130px";         //列名
        iArray[3][2]=100;         //列名
        iArray[3][3]=0;         //列名

        iArray[4]=new Array();
        iArray[4][0]="保单号码";         //列名
        iArray[4][1]="200px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="交易号";         //列名
        iArray[5][1]="200px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="交易金额";         //列名
        iArray[6][1]="80px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[7]=new Array();
        iArray[7][0]="交易交费日期";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[8]=new Array();
        iArray[8][0]="交易确认日期";         //列名
        iArray[8][1]="100px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[9]=new Array();
        iArray[9][0]="系列号";         //列名
        iArray[9][1]="0px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许

          
        GroupDayGrid = new MulLineEnter( "fm" , "GroupDayGrid" ); 

        //这些属性必须在loadMulLine前
        GroupDayGrid.mulLineCount = 3;   
        GroupDayGrid.displayTitle = 1;
        GroupDayGrid.canSel=1;
    	GroupDayGrid.hiddenPlus=1;
	    GroupDayGrid.hiddenSubtraction=1;
	    
        GroupDayGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化GroupDayGrid时出错："+ ex);
      }
    }


</script>