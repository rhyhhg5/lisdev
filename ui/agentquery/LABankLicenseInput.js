//该文件中包含客户端需要处理的函数和事件
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var showInfo;
var mDebug="0";

//提数操作
function submitForm()
{
    //首先检验录入框
  if(!verifyInput()) return false;
  if(!beforeSubmit()) return false; 
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
  fm.submit(); //提交

}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
	 
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在LaBankLicenseInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 
         
//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}
   

function easyQueryClick()
{	 
	//fm.all('divBankLicense').style.display='';
	//fm.all('divSpanBankLicenseGrid').style.display='none';
    //首先检验录入框
  if(!verifyInput()) return false;
 
  var strSQL = "select a.managecom,(select name from ldcom where comcode=a.managecom),"	 
             +"a.agentcom,a.name,(case a.actype when '01' then '兼业代理' else '其他' end ),"
             +"a.upagentcom,(case a.banktype when '01' then '分行' when '02' then '支行' when '03' then '分理处' when '04' then '网点' end ),"
             +"(case a.endflag when 'Y' then '是' when 'N' then '否' end ),a.enddate,a.licensestartdate,a.licenseenddate,"
             +"(case a.sellflag when 'Y' then '有' when 'N' then '无' end ),a.BusiLicenseCode"
	         +" from LACom a where 1=1 and a.actype='01' "
             //+ getWherePart('a.ManageCom', 'ManageCom','like')
	         +"and a.managecom like '"+fm.all('ManageCom').value+"%'"
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('a.AgentCom', 'AgentCom','like')
          	 + getWherePart('a.SellFlag','SellFlag')
          	 + getWherePart('a.Name','Name')
          	 + getWherePart('a.BankType','BankType')
          	 + getWherePart('a.BusiLicenseCode','BusiLicenseCode');
          	 
     if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!='')
    {
      strSQL=strSQL+" and a.agentcom in (select agentcom from lacomtoagent where agentcode=getagentcode('"+fm.all('AgentCode').value+"'))";
     } 
    if(fm.all('AgentName').value!=null&&fm.all('AgentName').value!='')
    {
      strSQL=strSQL+" and a.agentcom in (select agentcom from lacomtoagent where agentcode in (select agentcode from laagent where name='"+fm.all('AgentName').value+"' and branchtype='3' and branchtype2='01'))";
     }
     if(fm.all('EndFlag').value!=null && fm.all('EndFlag').value=='Y'){
     	strSQL=strSQL+" and a.endflag='Y'";
     }    
     if(fm.all('EndFlag').value!=null && fm.all('EndFlag').value=='N'){
     	strSQL=strSQL+" and (a.endflag='N' or a.endflag is null)";
     }
     if(fm.all('StartDate').value!=null && fm.all('StartDate').value!=''){
     	strSQL=strSQL+" and a.LicenseEndDate>='"+fm.all('StartDate').value+"' ";
     }
     if(fm.all('EndDate').value!=null && fm.all('EndDate').value!=''){
     	strSQL=strSQL+" and a.LicenseEndDate<='"+fm.all('EndDate').value+"' ";
     }
        strSQL=strSQL+" order by a.AgentCom";  
	 
	    turnPage.queryModal(strSQL, BankLicenseGrid);  
	         
	  if (BankLicenseGrid.mulLineCount == 0)
	  {
	    alert("没有查询到相应的代理机构信息！");
	    return false;
	  }
	
}

function beforeSubmit()
{
   return true;
}

function getAgentComName(cObj,cName)
{
  if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
	var strsql =" 1 and BranchType=#3# and BranchType2=#01# and ManageCom like #" + fm.all('ManageCom').value + "%#  ";
  
	showCodeList('AgentCom',[cObj,cName],[0,1],null,strsql,'1',1);
}

function ListExecl()
{
if (BankLicenseGrid.mulLineCount == 0)
{
  alert("列表中没有数据可下载");
  return false;
}
//定义查询的数据
var strSQL = "";
var strSQL = "select a.managecom,(select name from ldcom where comcode=a.managecom),"	 
             +"a.agentcom,a.name,(case a.actype when '01' then '兼业代理' else '其他' end ),"
             +"a.upagentcom,(case a.banktype when '01' then '分行' when '02' then '支行' when '03' then '分理处' when '04' then '网点' end ),"
             +"(case a.endflag when 'Y' then '是' when 'N' then '否' end ),a.enddate,a.licensestartdate,a.licenseenddate,"
             +"(case a.sellflag when 'Y' then '有' when 'N' then '无' end ),a.BusiLicenseCode"
	         +" from LACom a where 1=1 and a.actype='01' "
             //+ getWherePart('a.ManageCom', 'ManageCom','like')
	         +"and a.managecom like '"+fm.all('ManageCom').value+"%'"
          	 + getWherePart('a.BranchType', 'BranchType')
          	 + getWherePart('a.BranchType2', 'BranchType2')
          	 + getWherePart('a.AgentCom', 'AgentCom','like')
          	 //+ getWherePart('a.EndFlag','EndFlag')
          	 + getWherePart('a.Name','Name')
          	 + getWherePart('a.BankType','BankType')
          	 + getWherePart('a.BusiLicenseCode','BusiLicenseCode');
          	 
     if(fm.all('AgentCode').value!=null&&fm.all('AgentCode').value!='')
    {
      strSQL=strSQL+" and a.agentcom in (select agentcom from lacomtoagent where agentcode=getagentcode('"+fm.all('AgentCode').value+"'))";
     } 
    if(fm.all('AgentName').value!=null&&fm.all('AgentName').value!='')
    {
      strSQL=strSQL+" and a.agentcom in (select agentcom from lacomtoagent where agentcode in (select agentcode from laagent where name='"+fm.all('AgentName').value+"' and branchtype='3' and branchtype2='01'))";
     }
     if(fm.all('EndFlag').value!=null && fm.all('EndFlag').value=='Y'){
     	strSQL=strSQL+" and a.endflag='Y'";
     }    
     if(fm.all('EndFlag').value!=null && fm.all('EndFlag').value=='N'){
     	strSQL=strSQL+" and (a.endflag='N' or a.endflag is null)";
     }
     if(fm.all('StartDate').value!=null && fm.all('StartDate').value!=''){
     	strSQL=strSQL+" and a.LicenseEndDate>='"+fm.all('StartDate').value+"' ";
     }
     if(fm.all('EndDate').value!=null && fm.all('EndDate').value!=''){
     	strSQL=strSQL+" and a.LicenseEndDate<='"+fm.all('EndDate').value+"' ";
     }
        strSQL=strSQL+" order by a.AgentCom";
     
fm.querySql.value = strSQL;

//定义列名
var strSQLTitle = "select '管理机构编码','管理机构名称','代理机构编码','代理机构名称','代理机构类型','上级机构编码','级别','停业属性','停业日期','许可证起始日期','许可证到期日期','销售资格','保险兼业代理资格证号'  from dual where 1=1 ";
fm.querySqlTitle.value = strSQLTitle;
//定义表名
fm.all("Title").value="select '银保兼业代理机构许可证信息 查询下载' from dual where 1=1  ";  
fm.action = " ../agentquery/LAPrintTemplateSave.jsp";
fm.submit();

}

