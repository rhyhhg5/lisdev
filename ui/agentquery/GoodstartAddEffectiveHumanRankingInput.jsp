<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
	String tFlag = "";
  	GlobalInput tGI = new GlobalInput();
	tGI=(GlobalInput)session.getValue("GI");
	tFlag = request.getParameter("type"); 
%>
<script>	         
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var type = "<%=tFlag%>";
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>开门红新增合格人力排名</title>
  <SCRIPT src="GoodstartAddEffectiveHumanRankingInput.js"></SCRIPT>
  <%@include file="GoodstartAddEffectiveHumanRankingInit.jsp"%>
 </head>
 <body onload="initForm();initElementtype();">
 	<form action="" method=post name=fm target=fraSubmit>
 		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCond);">
				</td>
				<td class= titleImg>查询条件</td>
			</tr>
		</table>
		<div id= "divCond" style= "display: ''">
			<table class=common >
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input>
					    <input class=codeNo name=ManageCom verify="管理机构|code:comcode&NOTNULL" 
					    ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
					    onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);" ><input class=codename name=ManageComName readonly=true elementtype=nacessary >
					</td>
					<td class=title>管理机构层级</td>
					<td class=input>
						<input class=code name=ManageComHierarchy CodeData="0|^1|总公司^2|省公司^3|三级机构" verify="管理机构层级|notnull" elementtype=nacessary
							ondblclick="return showCodeListEx('ManageComHierarchy',[this],[1],null,null,null,1);">
					</td>
					</tr>
					<tr style="display: none">
					<TD  class= title >
							提取数据标志
					</TD>
					<TD  class= input >
						<Input name=Flag class=codeNo  CodeData="0|^1|预提数据^2|最终数据" verify="提取数据标志|NOTNULL"
						ondblclick="return showCodeListEx('flag',[this,FlagName],[0,1]);" 
					    onkeyup="return showCodeListKeyEx('flag',[this,FlagName],[0,1]);" ><input class=codename name ="FlagName" readonly=true elementtype=nacessary >
					</TD>
					
				</tr>
			</table>
		</div>
		<br>
		<input type="hidden" class="common" name="querySql" >
		<input style="font-weight:bold" class=cssButton type=button value=" 查   询 " onclick="query();">
		<input style="font-weight:bold" class=cssButton type=button value=" 下   载 " onclick="download();">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLoadExplain);">
				</td>
				<td class= titleImg>下载清单列表</td>
			</tr>
		</table>
		<div id="divLoadExplain" style="display:''">
			<table class= common>
				<tr class= common>
					<td text-align:left colSpan=1>
						<span id="spanExplainGrid" ></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
		<input class= common type=hidden name=tFlag >
 	</form>
 	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
 </body>
 </html>
  