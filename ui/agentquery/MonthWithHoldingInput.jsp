<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：MonAuditingGatherInput.jsp
//程序功能：
//创建日期：2015-02-10
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<html>
<%
	String tFlag = "";
  	GlobalInput tGI = (GlobalInput)session.getValue("GI");
	//tFlag = request.getParameter("type"); 
%>
<script>	         
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	<%--var type = "<%=tFlag%>";--%>
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>月度决算汇总报表</title>
  <SCRIPT src="MonthWithHodingInput.js"></SCRIPT>
</head>
<body onload="">
	<form action="MonthlyWithHodingDownload.jsp" method="post" name="fm" target="fraSubmit">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divMonAudGa);">
				</td>
				<td class= titleImg>查询条件<span style="font-weight:normal;color:silver;font:italic arial,sans-serif"> *为必录项</span></td>
			</tr>
		</table>
		<div id="divMonAudGa" style="display:''">
			<table class=common>
			<tr class=common>
					<td class=title>登陆机构</td>
					<td class=input>
						<input class=readonly name=ManageCom value=<%=tGI.ManageCom%> >
					</td>
					<td class=title>管理机构层级</td>
					<td class=input>
						<input class=code name=ManageComHierarchy CodeData="0|^1|总公司^2|省公司^3|三级机构" verify="管理机构层级|notnull" elementtype=nacessary
							ondblclick="return showCodeListEx('ManageComHierarchy',[this],[1],null,null,null,1);">
					</td>
				</tr>				
			</table>
			
		</div>
		<div>
			<table class=common>
				<tr><td><label style="color:red;">注：由于季度考核时，组织归属时间较晚原因，会影响预提的结果！</label></td></tr>
			</table>
		</div>
		<input class=common type=hidden name=subLength>
		<input class=common type=hidden name=type>
		<input type=button value=" 下  载 " class="cssButton" onclick="download();">
		
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>