//               该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var mOperate="";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

function submitForm()
{
   if (!verifyInput())
     return false;
   var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	} 
  var strSQL = "";
  strSQL = "select a.ContNo,a.branchattr ,"
  +" a.agentcom ,(select name from LACom where AgentCom =  a.AgentCom),a.p11,a.riskcode,"
  +" (select riskname from lmrisk where lmrisk.riskcode=a.riskcode),a.transmoney,a.FYCRate,a.fyc,"
  +" (select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,"  
  +" a.signdate,((select customgetpoldate from lccont where contno=a.contno fetch first 1 row only) union all (select customgetpoldate from lcgrpcont where grpcontno=a.contno fetch first 1 row only))," 
  +"((select getpoldate from lccont where contno=a.contno fetch first 1 row only) union all (select getpoldate from lcgrpcont where grpcontno=a.contno fetch first 1 row only))," 
  +"getUniteCode(a.agentcode),b.name, "
  +" (select codename from ldcode where codetype='agentstate' and code=b.agentstate)  "
  +" from LACommision a,LAAgent b where  a.agentcode=b.agentcode   "	
  +" and a.ManageCom like '"+ fm.all('ManageCom').value+"%'"
	//+ getWherePart("a.AgentCode","AgentCode")
  +strAgent
	+ getWherePart("a.ContNo","ContNo")
	+ getWherePart("a.WageNo","Month")
	+ getWherePart("a.RiskCode","RiskCode")
	//+ getWherePart("a.ManageCom","ManageCom","like")
	+ getWherePart("b.branchType","BranchType")
	+ getWherePart("b.branchType2","BranchType2")
	+ getWherePart("a.branchType","BranchType")
	+ getWherePart("a.branchType2","BranchType2")
	+ " order by a.wageno,a.branchattr,a.ContNo ";	
  fm.querySql.value = strSQL;
  
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
  fm.target = "f1print";
  fm.submit();
  showInfo.close();
}
function DoSDownload()
{
 // 书写SQL语句
    	
}

function easyQueryClick()
{

  if (!verifyInput())
   return false;
   
  initGrpPolGrid();
	// 书写SQL语句
  var strAgent = "";
	if(fm.AgentCode.value!=null&&fm.AgentCode.value!=''){
		strAgent = " and a.agentcode=getAgentCode('"+fm.AgentCode.value+"') ";
	}
  var strSQL = "";
  var tBranchType=fm.all('BranchType').value;
  var tBranchType2=fm.all('BranchType2').value;
  strSQL = "select a.ContNo,a.branchattr ,"
  +" a.agentcom ,(select name from LACom where AgentCom =  a.AgentCom),a.p11,a.riskcode,"
  +" (select riskname from lmrisk where lmrisk.riskcode=a.riskcode),a.transmoney,a.FYCRate,a.fyc,"
  +" (select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,"  
  +" a.signdate," 
  +"((select customgetpoldate from lccont where contno=a.contno fetch first 1 row only) union all (select customgetpoldate from lcgrpcont where grpcontno=a.contno fetch first 1 row only))," 
  +"((select getpoldate from lccont where contno=a.contno fetch first 1 row only) union all (select getpoldate from lcgrpcont where grpcontno=a.contno fetch first 1 row only))," 
  +"getunitecode(a.agentcode) ,b.name, "
  +" (select codename from ldcode where codetype='agentstate' and code=b.agentstate)  "
  +" from LACommision a,LAAgent b where  a.agentcode=b.agentcode   "	
	//+ getWherePart("a.AgentCode","AgentCode")
  +strAgent
	+ getWherePart("a.ContNo","ContNo")
	+ getWherePart("a.WageNo","Month")
	+ getWherePart("a.RiskCode","RiskCode")
	+ getWherePart("a.ManageCom","ManageCom","like")
	+ getWherePart("b.branchType","BranchType")
	+ getWherePart("b.branchType2","BranchType2")
	+ getWherePart("a.branchType","BranchType")
	+ getWherePart("a.branchType2","BranchType2")
	+ " order by a.wageno,a.branchattr,a.ContNo ";	
	
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}


