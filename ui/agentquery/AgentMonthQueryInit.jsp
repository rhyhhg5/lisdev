<%
//程序名称：AgentMonthQueryInit.jsp
//程序功能：
//创建日期：2003-4-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                                       
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  { try{   
    fm.all('AgentCode').value = <%=tG.Operator%> ;
    }
    catch(e)
    {
    alert(e);
    }
    //fm.all('Name').value = '';
    //fm.all('QueryMonth').value = '';
    
  }
  catch(ex)
  {
    alert("在AgentMonthQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AgentMonthQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initAgentMonthGrid();
    getAgnetName();
  }
  catch(re)
  {
    alert("AgentMonthQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentGrid
 ************************************************************
 */
function initAgentMonthGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="保单号码";         //列名
        iArray[1][1]="200px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="交易号";         //列名
        iArray[2][1]="200px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="交易金额";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="交易交费日期";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="交易确认日期";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="系列号";         //列名
        iArray[6][1]="0px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

          
        AgentMonthGrid = new MulLineEnter( "fm" , "AgentMonthGrid" ); 

        //这些属性必须在loadMulLine前
        AgentMonthGrid.mulLineCount = 6;   
        AgentMonthGrid.displayTitle = 1;
        AgentMonthGrid.canSel=1;
//      AgentMonthGrid.canChk=0;
        AgentMonthGrid.locked=1;
	AgentMonthGrid.hiddenPlus=1;
	AgentMonthGrid.hiddenSubtraction=1;
        AgentMonthGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AgentMonthGrid时出错："+ ex);
      }
    }


</script>