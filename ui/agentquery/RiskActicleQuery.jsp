<%@include file="../common/jsp/AgentCheck.jsp"%>
 <html> 
<%
//程序名称：menuGrp.jsp
//程序功能：菜单组的输入
//创建日期：2002-10-10 
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>

  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
 
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
   
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  
</head>
<body >

<form action="" method=post name=fm target="fraSubmit">
   <Table class = common>
      <TR class= common >
          <TD  class= titleImg >
            险种条款列表
          </TD>  
      </TR>
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/S2-2.htm" target="_blank">附加豁免保险费定期寿险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/S1-2.htm" target="_blank"> 民生消费信贷定期寿险条款>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/1-2.htm" target="_blank">民生安康定期保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/2-2.htm" target="_blank"> 附加每日住院给付保险条款>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/3-2.htm" target="_blank"> 民生康顺个人意外伤害保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/4-2.htm" target="_blank">附加意外伤害医疗保险条款>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/5-2.htm" target="_blank">附加住院医疗保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/6-2.htm" target="_blank">高倍给付特约保险条款>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/7-2.htm" target="_blank"> 提前给付特约保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/8-2.htm" target="_blank">民生康吉重大疾病保险条款>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/9-2.htm" target="_blank">附加团体住院医疗保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/10-2.htm" target="_blank">附加团体每日住院给付保险条款>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/11-2.htm" target="_blank"> 附加团体意外伤害医疗保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/12-2.htm" target="_blank"> 团体意外伤害保险>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/13-2.htm" target="_blank">团体重大疾病保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/14-2.htm" target="_blank"> 航空旅客人身意外伤害保险条款>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/15-2.htm" target="_blank">民生康泰重大疾病两全保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/16-2.htm" target="_blank">民生康顺个人意外伤害保险(老年计划）条款>></a></li>
			</td>
      </TR>
      
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/17-2.htm" target="_blank">学生团体意外伤害保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/18-2.htm" target="_blank">旅游意外伤害保险条款>></a></li>
			</td>
      </TR>
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/19-2.htm" target="_blank">民生团体年金保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/20-2.htm" target="_blank"> 附加特种疾病每日住院给付保险条款>></a></li>
			</td>
      </TR>
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/21-2.htm" target="_blank">民生关爱特种疾病定期寿险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/22-2.htm" target="_blank"> 民生阳光旅程交通工具意外伤害保险条款>></a></li>
			</td>
      </TR>
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/23-2.htm" target="_blank">民生长裕两全保险条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/24-2.htm" target="_blank"> 民生长宁终身保险（分红型）条款>></a></li>
			</td>
      </TR>
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/25-2.htm" target="_blank">民生长顺两全保险（分红型）条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/26-2.htm" target="_blank">  民生长乐两全保险（分红型）条款>></a></li>
			</td>
      </TR>
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/27-2.htm" target="_blank">民生金榜题名两全保险（分红型）条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/28-2.htm" target="_blank"> 民生众悦团体年金保险（分红型）条款>></a></li>
			</td>
      </TR>
      
      <TR  class= common> 
      <td class = common>
      <li><a href="./RiskArticle/29-2.htm" target="_blank"> 民生金玉满堂两全保险（分红型）条款>></a></li>
			</td>
    
      <td class = common>
      <li><a href="./RiskArticle/30-2.htm" target="_blank"> 民生金喜年年两全保险（分红型）条款>></a></li>
			</td>
      </TR>
      
     
      
   </table>
           
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
