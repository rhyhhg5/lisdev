 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


//提交，保存按钮对应操作
function ListExecl()
{
  if (verifyInput() == false)
    return false;

 var strSQL = "";
	strSQL =" select t,s,m,n,p,q,x  from ( "
	+" select a.comcode a ,a.name t ,value(decimal(sum(b.ShouldMoney),12,2),0) s,"
	+" value(decimal(sum(b.K01),12,2),0)  m ,"
	+" value(decimal(sum(b.K02),12,2),0)  n,"
	+" 0 p,"
	+" value(decimal(sum(b.K02+b.K01),12,2),0) q ,value(decimal(sum(b.ShouldMoney-b.K02-b.K01),12,2),0) x  "
  +" from ldcom a left join lawage b on a.comcode=substr(b.managecom,1,4)  "
  +" and b.branchtype='1'  and b.branchtype2='01' "
  +" and indexcalno >='"+fm.all('Month').value+"'  and indexcalno <='"+fm.all('MonthEnd').value+"'"
  +" and b.state='1'"
  +" where a.comgrade='02' and a.sign='1'"
  +" group by  a.comcode,a.name   order by a.comcode ) as dual ";
  fm.querySql.value = strSQL;
  var oldAction = fm.action;
  fm.action = "LATaxReportSave.jsp";
  fm.submit();
  fm.action = oldAction;
  }


//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;


  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交

}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
    //fm.all('AdjustBranchCode').value = '';
    //BranchChange();
  }

}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{

}

//取消按钮对应操作
function cancelForm()
{

}

//提交前的校验、计算
function beforeSubmit()
{
  //添加操作


}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{

}

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
}

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}




function clearAll()
{
   clearGrid();
}
function clearGrid()
{
   fm.all('ManageCom').value = '';
   fm.all('ManageComName').value = '';
   fm.all('Month').value = '';
   fm.all('MonthEnd').value = '';
}

 
 
