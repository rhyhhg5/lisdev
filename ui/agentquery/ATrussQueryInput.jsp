<%
//程序名称：ATrussQueryInput.jsp
//程序功能：
//创建日期：2003-05-09 
//创建人  ：lh程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./ATrussQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./ATrussQueryInit.jsp"%>
  <title>人员构架查询</title>
</head>
<body  onload="initForm();" >
  <form action="./ATrussQuerySubmit.jsp" method=post name=fm target="fraSubmit">
  	<table>
     <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgentGrid1);">
      </td>
      <td class= titleImg>
        请您输入查询条件： 
      </td>
     </tr>   
    </table>
  <Div  id= "divAgentGrid1" style= "display: ''">    
  <table  class= common>
  <TR  class= common>
    <TD  class= title>
      业务员编码
    </TD>
    <TD  class= input>
      <Input class= common name=AgentCode >
    </TD>
    <TD  class= title>
      编码
    </TD>
    <TD  class= input>
      <Input class= common name=Code >
    </TD>
  </TR>
</table>
 </Div>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCodeGrid);">
    		</td>
    		<td class= titleImg>
    			 公用代码表结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divCodeGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCodeGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
        <INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
