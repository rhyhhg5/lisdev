<%
//程序名称：LAIndexInfoQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 10:07
//创建人  ：方波
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                            

<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('AgentCode').value = '<%=tG.Operator%>';
//    fm.all('AgentName').value = tArr_AgentInfo[5]; 
    fm.all('IndexCalNo').value = '';
       
  }
  catch(ex)
  {
    alert("在LAIndexInfoQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAIndexInfoQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initIndexInfoGrid1();
	  
  }
  catch(re)
  {
    alert("LAIndexInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initFormMoreResult()
{
  try
  {
    
    //initIndexInfoGrid2();
  }
  catch(re)
  {
    alert("LAIndexInfoQueryInit.jsp-->initFormMoreResult函数中发生异常:初始化界面错误!");
  }
}


function initIndexInfoGrid1()
{                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="60px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="指标计算编码";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="指标类型";         //列名
        iArray[2][1]="50px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="代理人编码";         //列名
        iArray[3][1]="120px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="销售机构";         //列名
        iArray[4][1]="120px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="管理机构";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="统计起期";         //列名
        iArray[6][1]="100px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="统计止期";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许       
       

        IndexInfoGrid1 = new MulLineEnter( "fm" , "IndexInfoGrid1" ); 

        //这些属性必须在loadMulLine前
        IndexInfoGrid1.mulLineCount = 0;   
        IndexInfoGrid1.displayTitle = 1;
        IndexInfoGrid1.locked=1;
        IndexInfoGrid1.canSel=1;
        IndexInfoGrid1.canChk=0;
        IndexInfoGrid1.loadMulLine(iArray);  
	IndexInfoGrid1.selBoxEventFuncName = "selectItem";
      }
      catch(ex)
      {
        alert("初始化IndexInfoGrid1时出错："+ ex);
      }
}
</script>