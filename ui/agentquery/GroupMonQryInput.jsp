<%
//程序名称：GroupMonQryInput.jsp
//程序功能：
//创建日期：2003-10-8
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./GroupMonQryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./GroupMonQryInit.jsp"%>
 
  
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryCond);">
            </td>
            <td class= titleImg>
                销售机构日业务查询
            </td>            
    	</tr>
    </table>
  <Div  id= "divQryCond" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          代理人编码 
        </TD>
        <TD  class= input> 
          <Input class='readonly' readonly  name=AgentCode >
        </TD>
        <TD class= title>
          代理人组别 
        </TD>
        <TD class= input>
          <Input class='readonly' readonly name=BranchAttr > 
        </TD>
      </TR>      
      <TR  class= common>        
        <TD  class= title nowrap>
          查询月份(YYYYMM eg:200301) 
        </TD>
        <TD  class= input>
          <Input name=WageNo class=common > 
        </TD>        
        <td class=input colspan=2></td>
      </TR>      
    </table>
                    
	   <table> 
		    <tr>		
			    <td>
			      <INPUT class=common VALUE="查  询" TYPE=button onclick="easyQueryClick();"> 
			    </td>
			    <td>  
			      <INPUT class=common VALUE="业务详细信息" TYPE=button onclick="returnParent();">  
			    </td>
		    </tr>  		    
	   </table> 
  </Div>      
          				
    <table>
    	<tr>
                <td class=common>
						<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divGroupMonGrid);">
    		</td>
    		<td class= titleImg>
    			 月业务信息查询结果
    		</td>
    	</tr>
    </table>
  	<Div  id= "divGroupMonGrid" style= "display: ''">
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanGroupMonGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<INPUT  VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
		<INPUT  VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		<INPUT  VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		<INPUT  VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 	
		
		<input type=hidden name=AgentGroup>					
	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
 