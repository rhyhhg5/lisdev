<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAIndexInfoQueryInput.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>    
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="./LAIndexInfoQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAIndexInfoQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title></title>
  <script>
   var msql=" 1 and branchtype=#"+'<%=tBranchType%>'+"# and branchtype2=#"+'<%=tBranchType2%>'+"# and gradecode < #B21# ";
   var tsql="1 and char(length(db2inst1.trim(comcode)))<=#8# and comcode<>#8600#  ";
</script>
</head>

<body  onload="initForm(); initElementtype();" >
  <form action="./LAIndexInfoDownload.jsp" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAIndexInfo1);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAIndexInfo1" style= "display: ''">
    
  <table  class= common>
    <tr  class= common>        
       <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len<9"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,tsql,'1',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>
          <TD  class= title>
            考核年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo verify="考核年月|notnull&len<=6&int" elementtype=nacessary >
            <font color="red"> 'YYYYMM'</font>
          </TD>
     </TR>
      <TR class=common>
        <TD class=title>
          销售单位代码
        </TD>
        <TD class=title>
          <Input class=common name=BranchAttr>
        </TD>
        <TD class= title>
         	职级系列
         </TD>
        <TD class=title>
          <Input name=AgentSeries class="codeno"  
          CodeData="0|3^0|业务系列^1|处级主管^2|区级主管 " verify="职级系列|notnull" 
          ondblclick="showCodeListEx('AgentSeries',[this,AgentSeriesName],[0,1]);" 
          onkeyup="showCodeListKeyEx('AgentSeries',[this,AgentSeriesName],[0,1]);" 
          ><Input name=AgentSeriesName class="codename" elementtype=nacessary readonly> 
        </TD>
      </TR>
      <TR  class= common>        
       <TD  class= title>
        代理人编码
       </TD>
       <TD  class= input>
            <Input class=common name=AgentCode >
       </TD>
      <!--
       <TD class= title>代理人职级</TD>
       
        <TD class= input>
          <Input name=AgentGrade class="code"  verify="代理人职级|notnull&code:AgentGrade" 
          ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
          onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
        </TD>
      
        <TD class= input>
          <Input name=AgentGrade class="codeno"  verify="代理人职级|notnull&code:AgentGrade" 
           ondblclick="return showCodeList('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
          onkeyup="return showCodeListKey('AgentGrade',[this,AgentGradeName],[0,1],null,msql,1);" 
          ><Input name=AgentGradeName class="codename" elementtype=nacessary > 
        </TD>
	        -->
      <TD  class=title>指标类型</TD>
      <TD class= common>
          <Input name=IndexType class="codeno"  
          CodeData="0|2^02|维持^03|晋升" 
          ondblclick="showCodeListEx('IndexType',[this,IndexTypeName],[0,1]);" 
          onkeyup="showCodeListKeyEx('IndexType',[this,IndexTypeName],[0,1]);" 
          ><Input name=IndexTypeName class="codename"  readonly> 
      </TD> 
    </tr>
  </table>
   
   <INPUT class=cssbutton VALUE="查  询" TYPE=button onclick="easyQueryClick();" >
   <INPUT VALUE="下  载" TYPE=button onclick="submitForm()" class=cssbutton >
    <p> <font color="#ff0000">注：续保佣金按首年度佣金计入考核，故此处FYC=首年FYC+续保FYC！</font></p>
  <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divIndexInfoGrid1);">
    		</td>
    		<td class= titleImg>
    			 考核指标信息
    		</td>
    </tr>
    </table>
  	<Div  id= "divIndexInfoGrid1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanIndexInfoGrid1" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
  	  <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton > 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
  	</div>

	      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
	      <input type=hidden name=HiddenStrMoreResult value="">
	      <input type=hidden name=BranchType value="">
	      <input type=hidden class=Common name=querySql > 
  </form>
</body>
</html>
