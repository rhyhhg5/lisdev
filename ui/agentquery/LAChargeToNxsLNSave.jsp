<%
//程序名称：LRCessClaimSave.jsp
//程序功能：
//创建日期：2010-09-25
//创建人  ：龙程彬
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentquery.*" %>
<%@page contentType="text/html;charset=GBK" %>

<%
   System.out.println("开始执行LRCessClaimSave页面");
   GlobalInput globalInput = new GlobalInput( );
   globalInput.setSchema( (GlobalInput)session.getValue("GI") );

   LAChargeToNxsLNUI mLAChargeToNxsLNUI = new LAChargeToNxsLNUI();

  
  
  CErrors tError = null;
  String mOperateType=request.getParameter("OperateType"); 
  String mManageCom = request.getParameter("ManageCom");
  //System.out.println("操作的类型是"+mOperateType);
  String tRela  		= "";
  String FlagStr 		= "";
  String Content 		= "";
  String mDescType 	= ""; //将操作标志的英文转换成汉字的形式
  
  System.out.println("开始进行获取数据的操作！！！");
  
	String IndexCalNo=request.getParameter("IndexCalNo"); 
    mDescType = "信保提数";
  
	
  VData tVData = new VData(); 
  //将团单的公共信息通过TransferData传到UI
  try
  {
	  System.out.println(mManageCom);
  	tVData.addElement(globalInput);
  	TransferData getCessData = new TransferData();
	getCessData.setNameAndValue("IndexCalNo",IndexCalNo); 
	getCessData.setNameAndValue("ManageCom",mManageCom);
  	tVData.addElement(getCessData);
  	mLAChargeToNxsLNUI.submitData(tVData,mOperateType);
  	
	  
  }
  catch(Exception ex)
  {
    Content = mDescType+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
	
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  
	if (!FlagStr.equals("Fail"))
	{
		tError = mLAChargeToNxsLNUI.mErrors;
		if (!tError.needDealError())
		{
		    Content = " "+ mDescType+"完成 !";
		  	FlagStr = "Succ";
		}
		else
		{
		  	Content = mDescType+" 失败，原因是:" + tError.getFirstError();
		  	FlagStr = "Fail";
		}	 
	}
	 	  
%>

<html>
	<script language="javascript">
			parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>