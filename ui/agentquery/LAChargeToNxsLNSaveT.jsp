<%
//程序名称：LRCessClaimSave.jsp
//程序功能：
//创建日期：2010-09-25
//创建人  ：龙程彬
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.agentquery.*" %>
    <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%
  //接收信息，并作校验处理。
  //输入参数
  LAChargeToNxsLNSchema mLAChargeToNxsLNSchema = new LAChargeToNxsLNSchema();
  LAChargeToNxsLNSet  mLAChargeToNxsLNSet = new LAChargeToNxsLNSet();
  LAAgentSchema  mLAAgentSchema=new  LAAgentSchema();
  LAChargeToNxsLNTUI mLAChargeToNxsLNTUI = new LAChargeToNxsLNTUI();
  //输出参数
  CErrors tError = null;
  CErrors tError2 = null;
  String tOperate="UPDATE||MAIN";
  String tRela  = "";
  String FlagStr = "Fail";
  String Content = "";
  String flag="0";            //验证往后台传入SET还是SHEMA
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");

  System.out.println("begin LAWage schema...");
  String tIndexCalNo= request.getParameter("IndexNo");
  System.out.println("begin LAWage schema..."+tIndexCalNo);
  //取得佣金信息
  int lineCount = 0;
  String tSel[] = request.getParameterValues("InpWageGridChk");
  String tAgentCom[] = request.getParameterValues("WageGrid1");
  String mManageCom = request.getParameter("ManCom");
  String IndexCalNo= request.getParameter("IndexNo"); 
  if (tSel!=null)
  {
     lineCount = tSel.length; //行数s
     for(int i=0;i<lineCount;i++)
     {
       if(tSel[i].equals("1"))
       {
   	   mLAChargeToNxsLNSchema=new LAChargeToNxsLNSchema();
       mLAChargeToNxsLNSchema.setAgentCom(tAgentCom[i]);
       mLAChargeToNxsLNSet.add(mLAChargeToNxsLNSchema);
       }
     }
  }

  VData tVData = new VData();
  TransferData getCessData = new TransferData();
  getCessData.setNameAndValue("IndexCalNo",IndexCalNo); 
  getCessData.setNameAndValue("ManageCom",mManageCom);
  FlagStr="";
  tVData.add(tG);
  tVData.addElement(mLAChargeToNxsLNSet);
  tVData.addElement(getCessData);

  try
  {
    mLAChargeToNxsLNTUI.submitData(tVData);
  }
  catch(Exception ex)
  {
    Content = "保存失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  if (!FlagStr.equals("Fail"))
  {
    tError = mLAChargeToNxsLNTUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 保存成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>
