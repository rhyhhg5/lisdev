<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput" %>
<% 
//程序名称：SanctionFormInput.jsp
//程序功能：
//创建日期：2015-01-22
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<html>
<%
	String tFlag = "";
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
%>
<script>
	var operator = <%=tGI.Operator%>
	var manageCom = <%=tGI.ManageCom%>
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>   
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<title>奖罚报表</title>
  	<%@include file="SanctionFormInit.jsp" %>
  	<script src="SanctionFormInput.js"></script>
</head>
<body onload="initForm();initElementtype();">
	<form action="" method=post name=fm target="f1print">
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>查询条件：</td>
			</tr>
		</table>
		<table class=common>
			<tr class=common>
			    <td class=title>登陆机构</td>
			    <td class=input>
				 <input class=readonly name=ManageCom value=<%=tGI.ManageCom%> >
			    </td>
			    <td class=title>管理机构层级</td>
			    <td class=input>
				 <input class=code name=ManageComHierarchy CodeData="0|^1|总公司^2|省公司^3|三级机构" verify="管理机构层级|notnull" elementtype=nacessary
					ondblclick="return showCodeListEx('ManageComHierarchy',[this],[1],null,null,null,1);">
			    </td>
			    <td class=title>决算月份</td>
					<td class=input>
						<input class=code name=month> 
					</td>
			</tr>
		</table>
		<input class=common type=hidden name=querySql>
		<input style="font-weight:bold" class=cssButton type=button value=" 查   询 " onclick="query();">
		<input style="font-weight:bold" class=cssButton type=button value=" 下   载 " onclick="download();">
		<br/>
		<div id="loadTo">
			<table class= common>
       			<tr class= common>
      	  			<td text-align: left colSpan=1>
  						<span id="spanSanctionGrid" ></span> 
  			  		</td>
  				</tr>
    		</table>
    		<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
		<input class= common type=hidden name=tFlag value="<%=tFlag%>">
        <Input class= common type=hidden name=Operator >
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>