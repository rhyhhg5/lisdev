<%@include file="../common/jsp/UsrCheck.jsp"%> 
<%@page contentType="text/html;charset=GBK" %>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-5-19
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容 
%>
<%
	String tQueryType = "";
	try
	{
		tQueryType = request.getParameter("QueryType");
	}
	catch( Exception e )
	{
		tQueryType = "";
	}
%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
      <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT> 

  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="BranchGroupQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="BranchGroupQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>展业机构查询</title>   
</head>
<body  onload="initForm();" >
  <form action="./BranchGroupQueryQuery.jsp" method=post name=fm target="fraSubmit">

    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>请输入展业机构查询条件：</td>
		</tr>
	</table>
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            展业机构代码 
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAttr >
          </TD>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom verify = "管理机构|notnull&code:ComCode" ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
  </form>
  <form action="./AgentDetailMain.jsp" method=post name=fmSave target="fraSubmit">
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 展业机构信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
			<INPUT VALUE="首页" TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="getLastPage();"> 					
  	</div>
<%
	if (tQueryType.equals("Agent"))
	{
%>
  	<p>
      <INPUT VALUE="人员清单查询" TYPE=button onclick="agentQuery();"> 
  	</p>
<%
	}
	else if (tQueryType.equals("Operation")) 
	{
%>
			<p>
      			<INPUT class=common VALUE="预收查询" TYPE=button onclick="PrepTakeQuery();"> 
      			<INPUT class=common VALUE="承保查询" TYPE=button onclick="AssumeQuery();"> 					      		
      			<INPUT class=common VALUE="回单查询" TYPE=button onclick="BackQuery();">       		
      			<INPUT class=common VALUE="退保查询" TYPE=button onclick="ZTQuery();">				
      			<INPUT class=common VALUE="预收未承保查询" TYPE=button onclick="PTNotAQQuery();"> 
      			<INPUT class=common VALUE="承保未回单查询" TYPE=button onclick="AQNotZTQuery();"> 					     		
      			<INPUT class=common VALUE="犹豫期撤件查询" TYPE=button onclick="WTQuery();"> 
			</p> 	
<%
	}
%>
  	<input type=hidden id="fmtransact" name="fmtransact">
  	<input type=hidden id="BranchAttr" name="BranchAttr">
  	<input type=hidden id="BranchType" name="BranchType">
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
