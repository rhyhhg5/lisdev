<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/Download.jsp"%>
<%
    boolean errorFlag = false;
    
    //获得session中的人员信息
    GlobalInput tG = (GlobalInput)session.getValue("GI");
    
    //生成文件名
    Calendar cal = new GregorianCalendar();
    String min=String.valueOf(cal.get(Calendar.MINUTE));
    String sec=String.valueOf(cal.get(Calendar.SECOND));
    String downLoadFileName = "月度结算报表_"+tG.Operator+"_"+ min + sec + ".xls";
    String filePath = application.getRealPath("temp");
    String tOutXmlPath = filePath +File.separator+ downLoadFileName;
    System.out.println("OutXmlPath:" + tOutXmlPath);
    
    String querySql2 = request.getParameter("querySql2");
  //  querySql2 = querySql2.replaceAll("%25","%");
    //设置表头
    String[][] tTitle = {{"管理机构","机构名称","方案类别(A/B/C/D)","预算年月","至少合格人力数","上月在职人数","当月在职人数","月均平均人力数","月合格人力数","截止当月净增有效人力","当月净增有效人力","计划净增有效人力","净增有效人力计划达成率%","月获得发展费用"}};
    //表头的显示属性
    int []displayTitle = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
    
    //数据的显示属性
    int []displayData = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
    //生成文件
    CreateExcelList createexcellist = new CreateExcelList("");//指定文件名
    createexcellist.createExcelFile();
    String[] sheetName ={"list"};
    createexcellist.addSheet(sheetName);
    int row = createexcellist.setData(tTitle,displayTitle);
    if(row ==-1) errorFlag = true;
        createexcellist.setRowColOffset(row,0);//设置偏移
    if(createexcellist.setData(querySql2,displayData)==-1)
        errorFlag = true;
    if(!errorFlag)
        //写文件到磁盘
        try{
            createexcellist.write(tOutXmlPath);
        }catch(Exception e)
        {
            errorFlag = true;
            System.out.println(e);
        }
    //返回客户端
    if(!errorFlag)
        downLoadFile(response,filePath,downLoadFileName);
    out.clear();
    out = pageContext.pushBody();
%>
