<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：AgentGradeChangeDetailInput.jsp
//程序功能：
//创建日期：2003-4-8 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
	String tEdorNO = "";
	String tAgentCode = "";
	try
	{
		tEdorNO = request.getParameter("EdorNO");
		tAgentCode = request.getParameter("AgentCode");
	}
	catch( Exception e )
	{
		tEdorNO = "";
		tAgentCode = "";
	}
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <!--SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT-->
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="AgentGradeChangeDetailInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentGradeChangeDetailInit.jsp"%>
  <title>代理人职级变动信息</title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <INPUT VALUE="返回" TYPE=button onclick="returnParent();">
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgentGradeChange1);">
          <td class=titleImg>
            代理人职级变动信息
          </td>
        </td>
      </tr> 
    </table>
   
   <Div  id= "divLAAgentGradeChange1" style= "display: ''">      
    <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          批改号 
        </TD>
        <TD  class= input> 
          <Input name=EdorNO class= 'readonly' readonly >
        </TD>        
        <!--TD  class= title>
          交易号
        </TD>
        <TD  class= input>
          <Input name=ReceiptNo class= 'readonly' readonly >
        </TD-->        
      </TR>
      
      <TR  class= common> 
        <TD  class= title>
          代理人系列 
        </TD>
        <TD  class= input> 
          <Input name=AgentSeries class= 'readonly' readonly > 
        </TD>
        <TD  class= title>
          代理人级别 
        </TD>
        <TD  class= input>
          <Input name=AgentGrade class= 'readonly' readonly >
        </TD>        
      </TR>
      
      <TR  class= common> 
        <TD  class= title>
          代理人上次系列 
        </TD>
        <TD  class= input> 
          <Input name=AgentLastSeries class= 'readonly' readonly > 
        </TD>
        <TD  class= title>
          代理人上次级别
        </TD>
        <TD  class= input>
          <Input name=AgentLastGrade class= 'readonly' readonly > 
        </TD>        
      </TR>
      
      <TR  class= common> 
        <TD  class= title>
          前职级起聘日期 
        </TD>
        <TD  class= input> 
          <Input name=OldStartDate class= 'readonly' readonly > 
        </TD>        
        <TD  class= title> 
          前职级解聘日期
        </TD>
        <TD  class= input> 
          <Input name=OldEndDate class= 'readonly' readonly > 
        </TD>                
      </TR>
      
      <TR class=common>           
        <TD  class= title> 
          现职级起聘日期
        </TD>
        <TD  class= input> 
          <Input name=StartDate class= 'readonly' readonly > 
        </TD>        
        <TD  class= title>
          考核开始日期 
        </TD>
        <TD  class= input> 
          <Input name=AstartDate class= 'readonly' readonly > 
        </TD>         
      </TR>
      
      <TR class=common>           
        <TD  class= title> 
          考核类型
        </TD>
        <TD  class= input> 
          <Input name=AssessType class= 'readonly' readonly > 
        </TD>        
        <TD  class= title>
          考核状态 
        </TD>
        <TD  class= input> 
          <Input name=State class= 'readonly' readonly > 
        </TD>         
      </TR>
      
    </table>        
   </Div>	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>    
 </form>
</body>
</html>

        