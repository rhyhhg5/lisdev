<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput" %>
<% 
//程序名称：YearlyAuditingFormInput.jsp
//程序功能：
//创建日期：2015-01-22
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<html>
<%
	String tFlag = "";
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	tFlag = request.getParameter("type");
%>
<script>
	var operator = <%=tGI.Operator%>
	var manageCom = <%=tGI.ManageCom%>
	var type = <%=tFlag%>
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>   
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  	<title>年度决算报表</title>
  	<%@include file="YearlyAuditingFormInit.jsp" %>
  	<script src="YearlyAuditingFormInput.js"></script>
</head>
<body onload="initForm();">
	<form action="" method=post name=fm target="fraSubmit">
		<table class=common border=0 width=100%>
			<tr>
				<td class=titleImg align=center>查询条件：</td>
			</tr>
		</table>
		<table class=common>
			<tr class=common>
					<td class=title>管理机构</td>
					<td class=input>
						<input class=readonly name=ManageCom value=<%=tGI.ManageCom%> >
					</td>
				<td class=title>管理机构层级</td>
			    <td class=input>
				 <input class=code name=ManageComHierarchy CodeData="0|^1|总公司^2|省公司^3|三级机构" verify="管理机构层级|notnull" elementtype=nacessary
					ondblclick="return showCodeListEx('ManageComHierarchy',[this],[1],null,null,null,1);">
			    </td>
			</tr>
			<td class=title>考核年月</td>
					<td class=input>
						<input maxLength=6 name=AssessYearMonth verify="考核年月|YYYYMM&notnull" elementtype=nacessary>
						<label style="font-size:11px;color:red;font:italic arial,sans-serif">考核年月录入格式如:201603</label>
					</td>
		</table>.
		<input type="hidden" class="common" name="querySql" >
		<input type="hidden" class="common" name="lenh" >
		<input style="font-weight:bold" class=cssButton type=button value=" 下   载 " onclick="download();"><br/>
		<table>
	 		<tr>
		 		<td class= titleImg>统计字段说明：</td>
			</tr>
	 	</table>
	 	<div id="divCare" style="display:'none'">
	 		<table class=common>
	 			<tr><td><label style="color:red;">方案类别(A/B)：</label>A或者B</td></tr>
	 			<tr><td><label style="color:red;">1-12月平均人力合计：</label>1-12个月，每月的平均人力合计之和</td></tr>
	 			<tr><td><label style="color:red;">1-12月合格人力合计：</label>根据A/B不同方案，计算的1-12个月，合格人力数之和</td></tr>
	 			<tr><td><label style="color:red;">年度销售人力合格率：</label>∑（1-12月合格人力）/ ∑（1-12月平均人力）× 100%</td></tr>
	 			<tr><td><label style="color:red;">截至本月年度净增有效人力：</label>2015-12-16（含）之后截至本月新增有效累计人力-基础人力中的离职人力</td></tr>
	 			<tr><td><label style="color:red;">年度计划净增有效人力：</label>取自A/B方案年度净增有效人力月度目标分配表（5.2已提供），根据要素：管理机构、方案A/B、档次，从上表中查到对应档次的净增目标，即年度计划净增有效人力。</td></tr>
	 			<tr><td><label style="color:red;">年度净增有效人力达成率：</label>实际净增有效人力÷年度计划净增有效人力</td></tr>
	 			<tr><td><label style="color:red;">年度净增有效人力达成奖：</label>预设额度和机构FYC100%较大者</td></tr>
	 			<tr><td><label style="color:red;">合格人力达成特别奖：</label>预设额度和机构FYC100%较大者*（年度销售人力合格率-40%）</td></tr>
	 			<tr><td><label style="color:red;">2016年获得人力发展费用总额：</label>年度净增有效人力达成奖 + 合格人力达成特别奖 </td></tr>
	 			<tr><td><label style="color:red;">至上月累计实发总额：</label>从201601至当月(不包括当月)，所有的实发金额之和</td></tr>
	 			<tr><td><label style="color:red;">2015年欠款：</label>2015年省公司多发的奖励</td></tr>
	 			<tr><td><label style="color:red;">年决算总计（扣除欠款）：</label>2016年获得人力发展费用总额－至上月累计实发总额－2015年欠款</td></tr>
	 		</table>
	 	</div>
	 	<div id="divCare1" style="display:''">
	 		<table class=common>
	 			<tr><td><label style="color:red;">方案类别(A/B)：</label>A或者B</td></tr>
	 			<tr><td><label style="color:red;">1-12月平均人力合计：</label>1-12个月，每月的平均人力合计之和</td></tr>
	 			<tr><td><label style="color:red;">1-12月合格人力合计：</label>根据A/B不同方案，计算的1-12个月，合格人力数之和</td></tr>
	 			<tr><td><label style="color:red;">年度销售人力合格率：</label>∑（1-12月合格人力）/ ∑（1-12月平均人力）× 100%</td></tr>
	 			<tr><td><label style="color:red;">截至本月年度净增有效人力：</label>2015-12-16（含）之后截至本月新增有效累计人力-基础人力中的离职人力</td></tr>
	 			<tr><td><label style="color:red;">年度计划净增有效人力：</label>取自A/B方案年度净增有效人力月度目标分配表（5.2已提供），根据要素：管理机构、方案A/B、档次，从上表中查到对应档次的净增目标，即年度计划净增有效人力。</td></tr>
	 			<tr><td><label style="color:red;">年度净增有效人力达成率：</label>实际净增有效人力÷年度计划净增有效人力</td></tr>
	 			<tr><td><label style="color:red;">年度净增有效人力达成奖：</label>预设额度和机构FYC100%较大者</td></tr>
	 			<tr><td><label style="color:red;">合格人力达成特别奖：</label>预设额度和机构FYC100%较大者*（年度销售人力合格率-40%）</td></tr>
	 			<tr><td><label style="color:red;">2016年获得人力发展费用总额：</label>年度净增有效人力达成奖 + 合格人力达成特别奖 </td></tr>
	 			<tr><td><label style="color:red;">截止上月实际发放金额：</label>从201601至当月(不包括当月)，所有的实发金额之和</td></tr>
	 			<tr><td><label style="color:red;">年终差额：</label>年终发给机构或需从机构扣回的金额</td></tr>
	 		</table>
	 	</div>
		<input class= common type=hidden name=tFlag value="<%=tFlag%>">
        <input class= common type=hidden name=Operator >
        <input type= hidden name=MngName>
        <input type= hidden name=fmtransact>
	</form>
	<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</body>
</html>

