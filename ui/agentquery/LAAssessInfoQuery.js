//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet1,arrDataSet2; 
var turnPage = new turnPageClass();


//<addcode>############################################################//
var old_AgentGroup="";
var new_AgentGroup="";
//</addcode>############################################################//

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAAssessQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}

function DetailPrint()
{
  fm.action="../agentprint/LAAssessInfoPrt.jsp";
	fm.target="f1print";
	submitForm();
}

function submitForm()
{
	
if(verifyInput()) 
  {	  	
  	var i = 0;
	  var showStr="正在准备打印数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	  fm.submit(); //提交
	  showInfo.close();
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
    //执行下一步操作
  }
}



// 查询按钮
function easyQueryClick()
{
  LAAssessInfoGrid.clearData();
  
  if (!verifyInput())
    return false;
    
  var tSql="";
  var tReturn = getManageComLimitlike("a.ManageCom");
  if(fm.StandAssessFlag.value == 'Y') 
  {
	  tSql = "SELECT a.AgentGrade,a.BranchAttr,getUniteCode(a.AgentCode),b.name,"
	              +"(SELECT Name FROM LAAgent WHERE AgentCode = a.AgentCode),"
	              +"a.AgentGrade1 "
	        +"FROM labranchgroup b ,LAAssess a WHERE a.StandAssessFlag = '1' and a.branchattr=b.branchattr"
	        +tReturn
	        +getWherePart('a.ModifyFlag','AssessType')
	        +getWherePart('a.ManageCom','ManageCom','like')
	        +getWherePart('a.IndexCalNo','IndexCalNo')
	        +getWherePart('a.AgentGrade','AgentGrade')
	        +getWherePart('a.BranchType','BranchType');
	        
	       
  }
  else
  {
	  tSql = "SELECT a.AgentGrade,a.BranchAttr,getUniteCode(a.AgentCode),b.name,"
	              +"(SELECT Name FROM LAAgent WHERE AgentCode = a.AgentCode),"
	              +"a.AgentGrade1 "
	        +"FROM labranchgroup b,LAAssess a WHERE a.StandAssessFlag = '0' and a.branchattr=b.branchattr"
	        +tReturn
	        +getWherePart('a.ModifyFlag','AssessType')
	        +getWherePart('a.ManageCom','ManageCom','like')
	        +getWherePart('a.IndexCalNo','IndexCalNo')
	        +getWherePart('a.AgentGrade','AgentGrade')
	        +getWherePart('a.BranchType','BranchType');   
  }	
  var tAssessType = fm.all('AssessType').value;
  switch(tAssessType)
  {
     case '01': 
       tSql += "AND a.AgentGrade > a.AgentGrade1 ";
       break;
     case '02':
       tSql += "AND a.AgentGrade = a.AgentGrade1 ";
       break;
     case '03':
       tSql += "AND a.AgentGrade < a.AgentGrade1 ";
       break;
  }
  tSql += "Order by a.AgentCode,a.branchattr";	          
  //alert(tSql);
  turnPage.strQueryResult  = easyQueryVer3(tSql, 1, 1, 1);  
  
  //alert(strSQL);
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询无数据！！");
    return false;
  }  
  
  //查询成功则拆分字符串，返回二维数组
  arrDataSet1 = decodeEasyQueryResult(turnPage.strQueryResult);
  
  turnPage.arrDataCacheSet = arrDataSet1;

  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = LAAssessInfoGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = tSql; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var tArr = new Array();
  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

  //调用MULTILINE对象显示查询结果
  displayMultiline(tArr, turnPage.pageDisplayGrid);
}

/*function selectItem(spanID,parm2)
{
  var arrSelect = new Array();
  arrSelect = getQueryResult1();
  var strIndexCalNo=arrSelect[0][0];
  var strIndexType=arrSelect[0][1];
  var strAgentCode=arrSelect[0][2];
  if (strIndexCalNo!="")
    strIndexCalNo=" and IndexCalNo='"+strIndexCalNo+"'";
  if (strIndexType!="")
    strIndexType=" and IndexType='"+strIndexType+"'";
  if (strAgentCode!="")
    strAgentCode=" and AgentCode='"+strAgentCode+"'";

  var strSQLMoreResult="select * from LAIndexInfo where 1=1 "
                 + strIndexCalNo
	         + strIndexType
	         + strAgentCode;
	        
  var strMoreResult=easyQueryVer3(strSQLMoreResult, 1, 1, 1);
  fm.all('HiddenStrMoreResult').value=strMoreResult; 

  if (strMoreResult)
  {
   showInfo=window.open("./LAIndexInfoQueryMoreResult.jsp");
  }
  else
    alert("查询失败");

}*/








