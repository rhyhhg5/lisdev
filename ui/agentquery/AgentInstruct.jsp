<% 
//程序名称：AgentInstruct.jsp
//程序功能：
//创建日期：
//创建人  ：ll
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="./AgentInstruct.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./AgentInstructInit.jsp"%>
   <%@include file="../common/jsp/ManageComLimit.jsp"%>
   <%@include file="../common/jsp/UsrCheck.jsp"%>
  <title>育成查询 </title>
</head>
<body  onload="initForm();" >
  <form  method=post name=fm target="fraSubmit">
  <!--增员查询条件 -->
    <table>
    	<tr>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgentAdd);">
        </td>
        <td class= titleImg>
          育成查询
        </td>            
    	</tr>
    </table>
  <Div  id= "divLAAgentAdd" style= "display: ''">
  <table  class= common>
  <TR>
    	<TD  class= title width="25%">
  	    	起始日期
   	  </TD>
      <TD  class= input width="25%">
         <Input class= "coolDatePicker" dateFormat="short" name=StartDay >
       </TD>      
          <TD  class= title width="25%">
            结束日期
          </TD>
          <TD  class= input width="25%">
            <Input class= "coolDatePicker" dateFormat="short" name=EndDay  >
          </TD>  
        </TR>
    <TR  class= common>     
      <TD class= title> 
        育成人编码 
      </TD>
      <TD  class= input> 
       <Input class= common  name=Instruct verify="育成人编码|NOTNULL">
      </TD>        
      </TR>         
   </table>
	   <table> 
		    <tr>		
			    <td>
			      <INPUT class=common VALUE="查  询" TYPE=button onclick="easyQueryClick();"> 
			    </td>			    
		    </tr>  		    
	   </table> 
  </Div>  
  	<Div  id= "divAgentAddGrid" style= "display: ''">
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanAgentAddGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table>
    		<tr>
    			<td>
			      <INPUT class=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
			    </td>
			    <td>  
			      <INPUT class=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			    </td>
			    <td> 			      
			      <INPUT class=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			    </td>
			    <td> 			      
			      <INPUT class=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 						
			    </td>  			
  			</tr>
  		</table>
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
