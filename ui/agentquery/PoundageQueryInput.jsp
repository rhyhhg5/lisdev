<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-6-27 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tBranchType = "";
	try
	{
		tBranchType = request.getParameter("BranchType");
	}
	catch( Exception e )
	{
		tBranchType = "";
	}
%>
<Script>
var tBranchType = "<%=tBranchType%>";
</Script>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="PoundageQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="PoundageQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title>手续费查询</title>
</head>
<body  onload="initForm();">
  <form action="./PoundageDetailQueryMain.jsp" method=post name=fm >
    <!-- 保单信息部分 -->
  <table class= common border=0 width=100%>
    <tr>
			<td class= titleImg align= center>
				请输入查询条件：
			</td>
		</tr>
	</table>
	
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            代理机构
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCom >
          </TD>
          <TD>
          </TD>
          <TD>
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            开始日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=BeginDate >
          </TD>
          <TD  class= title>
            结束日期
          </TD>
          <TD  class= input>
            <Input class= "coolDatePicker" dateFormat="short" name=EndDate >
          </TD>
        </TR>
    </table>
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLPEdorMain1);">
    		</td>
    		<td class= titleImg>
    			 手续费信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLPEdorMain1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1 >
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  	<table>
  	<tr>
  		<td  class= title> 
  			银行类型 
  		</td>
      <td  class= input> 
      	<input name=BankType class='codeno' ondblclick="return showCodeList('BankType',[this,BankTypeName],[0,1]);" onkeyup="return showCodeListKey('BankType',[this,BankTypeName],[0,1]);"><input class=codename name=BankTypeName readonly=true > 
      </td>
     </tr>
     <tr>
     	<td>
     		<INPUT VALUE="明细查询" TYPE=button onclick="DetailQuery();">
     	</td>
     </tr>
  	 		<Input type=hidden name=BranchType >
  	</table>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
