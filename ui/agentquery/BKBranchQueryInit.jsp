<%
//程序名称：BKBranchQueryInit.jsp
//程序功能：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）

function afterQuery( arrQueryResult )
{
  try
  {   
  var arrResult = new Array();	
  if( arrQueryResult != null )
	{
	  arrResult = arrQueryResult;                            
	// 代理人查询条件  	
		fm.all('BranchName').value = arrResult[0][1];                                              
		fm.all('ManageCom').value = arrResult[0][2];                                              
		fm.all('UpBranchAttr').value = arrResult[0][3];                         
    fm.all('BranchAttr').value = arrResult[0][0];   
		fm.all('FieldFlag').value = arrResult[0][5];                                             
		fm.all('BranchLevel').value = arrResult[0][4];                                           
		fm.all('BranchManager').value = arrResult[0][6];
		fm.all('BranchAddress').value = arrResult[0][8];                                               
		fm.all('BranchPhone').value = arrResult[0][10];                                          
		fm.all('BranchZipcode').value = arrResult[0][9];                                               
		fm.all('FoundDate').value = arrResult[0][11];                                               
		fm.all('EndDate').value = arrResult[0][13];                                               
		fm.all('EndFlag').value = arrResult[0][12];		                                               
		fm.all('FieldFlag').value = arrResult[0][5];  
		fm.all('BranchManagerName').value = arrResult[0][7];
		fm.all('AttrManager').value = arrResult[0][14];
		}
  }
  catch(ex)
  {
    alert("在BranchPolQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {  
   var arrReturn = new Array();  
   arrReturn = getQueryResult();
	 afterQuery( arrReturn );        
  }
  catch(re)
  {
    alert("BranchPolQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

</script>