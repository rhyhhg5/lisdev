//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet; 
var turnPage = new turnPageClass();


//查询按钮
function easyQueryClick()
{	
  var strSQL = "select a.agentcode,b.name,getbranchattr(a.agentgroup),a.indexcalno,a.F01,a.F02,a.F03,"
               + " a.F04,a.F05,a.F06,a.F07,a.F08,a.F09,a.F10,a.F11,a.F12,a.F13,a.F14,"
               + " a.F15,a.F16,a.F17,a.F18,a.F19,a.F20,a.F21,a.F22,a.F23,a.F24,"
               + " a.K02,a.K03,a.F30,a.K11,a.K12,"
               + " a.shouldmoney,a.lastmoney,a.summoney from LAWage a,laagent b"
               + " where a.agentcode = b.agentcode "
			   + getWherePart('a.agentcode','AgentCode')
			   + getWherePart('a.indexcalno','IndexCalNo');
					 
  //查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("没有符合条件的数据，请重新录入查询条件！");
    return false;
  }
  
  
  //查询成功则拆分字符串，返回二维数组  
  turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = WageGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  arrGrid = arrDataSet;
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
}
	
