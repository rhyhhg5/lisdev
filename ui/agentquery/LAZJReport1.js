 //               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
//var saveClick=false;
var arrDataSet;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


//提交，保存按钮对应操作
function ListExecl()
{
  if (verifyInput() == false)
    return false;
  var yearmonth=fm.all('AssessYear').value+fm.all('AssessMonth').value;
  var tSql = " select enddate from LAStatSegment where stattype='5' and yearmonth="+yearmonth+" with ur ";
  //alert(tSql);
  var strQueryResult  = easyQueryVer3(tSql, 1, 1, 1);
  if (!strQueryResult)
  {
    alert('年,季度信息获取失败!');
    return false;
  }  
  var arr = decodeEasyQueryResult(strQueryResult);
  //alert(arr[0][0]);
  var strSQL1 =" select  count(1) from lacom where branchtype2='02' and EndFlag='N' and actype='02' and managecom like '"+fm.all('ManageCom').value+"%' or (EndFlag='Y' and EndDate<='"+arr[0][0]+"') with ur"	;
  var strSQL2 =" select  count(1) from lacom where branchtype2='02' and EndFlag='N' and actype='03' and managecom like '"+fm.all('ManageCom').value+"%' or (EndFlag='Y' and EndDate<='"+arr[0][0]+"') with ur"	;
  var strSQL3 =" select  count(1) from lacom where branchtype='3' and branchtype2='01' and endflag='N' and managecom like '"+fm.all('ManageCom').value+"%'  with ur"	;
  var strSQL4 =" select  count(1) from lacom where branchtype2='02' and EndFlag='N' and actype='04' and managecom like '"+fm.all('ManageCom').value+"%' or (EndFlag='Y' and EndDate<='"+arr[0][0]+"') with ur"	;
  var strSQL5 =" select  count(1) from laagent where branchtype='1' and branchtype2='01' and managecom like '"+fm.all('ManageCom').value+"%'  "
  +" and employdate<=(select enddate from LAStatSegment where yearmonth=int('"+yearmonth+"') and stattype='1'  )"
  +" and ( outworkdate>=(select startdate from LAStatSegment where yearmonth=int('"+yearmonth+"') and stattype='1'  )"
  +" or outworkdate is null ) "
  +" with ur"	;
	 
fm.querySql1.value = strSQL1;
fm.querySql2.value = strSQL2;
fm.querySql3.value = strSQL3;
fm.querySql4.value = strSQL4;
fm.querySql5.value = strSQL5;

fm.submit();

  }
  
//提交，保存按钮对应操作
function currentListExecl()
{
  if(fm.all('ManageCom').value == '' || fm.all('ManageCom').value == null ){
    alert("请选择管理机构!");
    return false;
  }
  var strSQL1 =" select  count(1) from lacom where branchtype2='02' and EndFlag='N' and actype='02' and managecom like '"+fm.all('ManageCom').value+"%'  with ur "	;
  var strSQL2 =" select  count(1) from lacom where branchtype2='02' and EndFlag='N' and actype='03' and managecom like '"+fm.all('ManageCom').value+"%'  with ur "	;
  var strSQL3 =" select  count(1) from lacom where branchtype='3' and branchtype2='01' and endflag='N' and managecom like '"+fm.all('ManageCom').value+"%'  with ur "	;
  var strSQL4 =" select  count(1) from lacom where branchtype2='02' and EndFlag='N' and actype='04' and managecom like '"+fm.all('ManageCom').value+"%'  with ur "	;
  var strSQL5 =" select  count(1) from laagent where branchtype='1' and branchtype2='01' and managecom like '"+fm.all('ManageCom').value+"%' and agentstate<'06' and outworkdate is null with ur "	;
	 
  fm.querySql1.value = strSQL1;
  fm.querySql2.value = strSQL2;
  fm.querySql3.value = strSQL3;
  fm.querySql4.value = strSQL4;
  fm.querySql5.value = strSQL5;

fm.submit();

  }
 

//提交，保存按钮对应操作
function submitForm()
{
  if (verifyInput() == false)
    return false;

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

    //执行下一步操作
    AgentGrid.clearData("AgentGrid");
    //fm.all('AdjustBranchCode').value = '';
    //BranchChange();
  }

}



//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}




 
 
