<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-6-28 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
	String tAgentCom = "";
	try
	{
		tAgentCom = request.getParameter("AgentCom");
	}
	catch( Exception e )
	{
		tAgentCom = "";
	}
		String tBranchType = "";
	try
	{
		tBranchType = request.getParameter("BranchType");
	}
	catch( Exception e )
	{
		tBranchType = "";
	}
			String tBankType = "";
	try
	{
		tBankType = request.getParameter("BankType");
	}
	catch( Exception e )
	{
		tBankType = "";
	}
			String tBeginDate = "";
	try
	{
		tBeginDate = request.getParameter("BeginDate");
	}
	catch( Exception e )
	{
		tBeginDate = "";
	}
			String tEndDate = "";
	try
	{
		tEndDate = request.getParameter("EndDate");
	}
	catch( Exception e )
	{
		tEndDate = "";
	}
%>
<head>
<script> 
	var tAgentCom = "<%=tAgentCom%>";
	var tBankType = "<%=tBankType%>";
	var tBranchType = "<%=tBranchType%>";
	var tBeginDate = "<%=tBeginDate%>";
	var tEndDate = "<%=tEndDate%>";
</script>

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="PoundageDetailQuery.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="PoundageDetailQueryInit.jsp"%>
	
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>手续费明细查询 </title>
</head>

<body  onload="initForm();easyQueryClick();" >
  <form  name=fm >
  <table >
    	<tr>
    	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    	</td>
			<td class= titleImg>
				机构信息
			</td>
		</tr>
	</table>
	<Div  id= "divLCPol1" style= "display: ''">
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            机构代码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCom >
          </TD>
				</TR>
     </table>
  </Div>
  
  
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCClaim1);">
    		</td>
    		<td class= titleImg>
    			 手续费信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCClaim1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">				
  	</div>
  	<Input type=hidden name=BeginDate >
  	<Input type=hidden name=EndDate >
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
<script>
var turnPage = new turnPageClass(); 
function easyQueryClick()
{	
	var sql = "select BankType from lacom where agentcom='" + tAgentCom + "'";
     	var Result = easyQueryVer3(sql, 1, 0, 1);
     	var tArray = decodeEasyQueryResult(Result);
     	var aBankType = tArray[0][0];
	
	//alert("here");
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";	
	var strSQL = "select b.AgentCom,b.Name,b.ChannelType,sum(c.TransMoney),sum(c.charge) from LACom b,lacharge c " 
							+ "where c.AgentCom=b.AgentCom "
							+ "and c.BranchType='" + tBranchType + "' "
							+ "and BankType='" + tBankType +"' "
							+ "and c.AgentCom like'" + tAgentCom +"%%' "
							+ getWherePart( 'c.calDate','BeginDate','>=' )
							+ getWherePart( 'c.calDate','EndDate','<=' )
							+ "group by b.agentcom,b.Name,b.ChannelType order by b.agentcom";
	//alert(strSQL);
	
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}
</script>
</html>


