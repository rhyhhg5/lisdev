<%
//程序名称：BKAgentQueryInit.jsp
//程序功能：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）

function afterQuery( arrQueryResult )
{
  try
  {   
  var arrResult = new Array();	
  if( arrQueryResult != null )
	{
	arrResult = arrQueryResult;                           
  	fm.all('AgentCode').value = arrResult[0][0];
    fm.all('Name').value = arrResult[0][1];
    fm.all('Sex').value = arrResult[0][2];
    fm.all('Birthday').value = arrResult[0][3];
    fm.all('NativePlace').value = arrResult[0][4];
    fm.all('Nationality').value = arrResult[0][5];
    fm.all('RgtAddress').value = arrResult[0][6];
    fm.all('PolityVisage').value = arrResult[0][7];
    fm.all('IDNo').value = arrResult[0][8];
    fm.all('Degree').value = arrResult[0][9];
    fm.all('GraduateSchool').value = arrResult[0][10];
    fm.all('Speciality').value = arrResult[0][11];
    fm.all('PostTitle').value = arrResult[0][12];
    fm.all('HomeAddress').value = arrResult[0][13];
    fm.all('ZipCode').value = arrResult[0][14];
    fm.all('Phone').value = arrResult[0][15];
    fm.all('BP').value = arrResult[0][16];
    fm.all('Mobile').value = arrResult[0][17];
    fm.all('EMail').value = arrResult[0][18];
    fm.all('OldCom').value = arrResult[0][19];
    fm.all('OldOccupation').value = arrResult[0][20];
    fm.all('HeadShip').value = arrResult[0][21];
    fm.all('EmployDate').value = arrResult[0][22];
    fm.all('Remark').value = arrResult[0][23];
    fm.all('AgentKind').value = arrResult[0][24];
    fm.all('Operator').value = arrResult[0][25]; 
    
    fm.all('ChannelName').value=arrResult[0][27];
    fm.all('AgentGrade').value = arrResult[0][28];
    fm.all('AgentGroup').value = arrResult[0][30];
    fm.all('UpAgent').value = arrResult[0][31]; 
    
		}
  }
  catch(ex)
  {
    alert("在BKAgentQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {  
   var arrReturn = new Array();  
   arrReturn = getQueryResult();
	 afterQuery( arrReturn );        
  }
  catch(re)
  {
    alert("BKAgentQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


// 人员职级变动查询列表的初始化
function initAgentGrade()
  {                               
    var iArray = new Array();
      
      try
      {
        
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名
        
        iArray[1]=new Array();
        iArray[1][0]="员工编码";         //列名
        iArray[1][1]="80px";         //列名
        iArray[1][2]=100;         //列名
        iArray[1][3]=0;         //列名

        iArray[2]=new Array();
        iArray[2][0]="员工姓名";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="员工职级";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="职级起聘日期";         //列名
        iArray[4][1]="80px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="上级代理人";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="操作人";         //列名
        iArray[6][1]="80px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

       

      AgentGrade = new MulLineEnter( "fm" , "AgentGrade" ); 
      //这些属性必须在loadMulLine前
      AgentGrade.mulLineCount = 10;   
      AgentGrade.displayTitle = 1;
      AgentGrade.locked = 1;
      AgentGrade.canSel = 0;
      AgentGrade.hiddenPlus = 1;
      AgentGrade.hiddenSubtraction = 1;
      AgentGrade.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      //AgentGrade.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>

