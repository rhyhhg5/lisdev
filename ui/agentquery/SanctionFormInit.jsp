<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：SanctionFormInit.jsp
//程序功能：
//创建日期：2015-01-22
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	PubFun tpubFun = new PubFun();
	String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<script src="../common/javascript/Common.js"></script>
<script language="JavaScript">
	function initInpBox(){
		try{
//			fm.all('ManageCom').value = <%=strManageCom%>;
//		    if(fm.all('ManageCom').value==86){
//		    	fm.all('ManageCom').readOnly=false;
//		    } else{
//		    	fm.all('ManageCom').readOnly=true;
//		    }
//		    if(fm.all('ManageCom').value!=null){
//		    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
//		        //显示代码选择中文
//		        if (arrResult != null){
//		        	fm.all('ManageComName').value=arrResult[0][0];
//		        } 
//		    }
		}catch(ex){
			alert("在SanctionFormInit.jsp-->IninInpBox函数中发生异常 ：初始化界面错误！");
		}
	}
	
	function initForm(){
		try{
			initInpBox();
			initSanctionGrid();
			showAllCodeName();
		}catch(re){
			alert("SanctionFormInit.jsp-->InitForm函数中发生异常:初始化界面错误");
		}
	}
	
	function initSanctionGrid(){
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "管理机构";
			iArray[1][1] = "60px";
			iArray[1][2] = 30;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "机构名称";
			iArray[2][1] = "50px";
			iArray[2][2] = 100;
			iArray[2][3] = 0;
			
			iArray[3] = new Array();
			iArray[3][0] = "方案类别(A/B)";
			iArray[3][1] = "80px";
			iArray[3][2] = 50;
			iArray[3][3] = 0;
			
			iArray[4] = new Array();
			iArray[4][0] = "年度净增有效人力达成奖费用支持上限";
			iArray[4][1] = "130px";
			iArray[4][2] = 100;
			iArray[4][3] = 0;
			
			iArray[5] = new Array();
			iArray[5][0] = "合格人力达成特别奖费用支持上限";
			iArray[5][1] = "110px";
			iArray[5][2] = 50;
			iArray[5][3] = 0;
			
			iArray[6] = new Array();
			iArray[6][0] = "年度支持费用支持上限合计";
			iArray[6][1] = "110px";
			iArray[6][2] = 50;
			iArray[6][3] = 0;
			
			iArray[7] = new Array();
			iArray[7][0] = "省公司组织推动费用";
			iArray[7][1] = "120px";
			iArray[7][2] = 50;
			iArray[7][3] = 0;
			        
			iArray[8] = new Array();
			iArray[8][0] = "省公司奖金";
			iArray[8][1] = "70px";
			iArray[8][2] = 50;
			iArray[8][3] = 0;
			        
			iArray[9] = new Array();
			iArray[9][0] = "中心支公司奖金";
			iArray[9][1] = "90px";
			iArray[9][2] = 50;
			iArray[9][3] = 0;
			
			iArray[10] = new Array();
			iArray[10][0] = "总经理奖金";
			iArray[10][1] = "70px";
			iArray[10][2] = 50;
			iArray[10][3] = 0;
			         
			iArray[11] = new Array();
			iArray[11][0] = "分管个险总经理奖金";
			iArray[11][1] = "120px";
			iArray[11][2] = 50;
			iArray[11][3] = 0;
			
			iArray[12] = new Array();
			iArray[12][0] = "个人保险部负责人奖金";
			iArray[12][1] = "120px";
			iArray[12][2] = 50;
			iArray[12][3] = 0;
			
			iArray[13] = new Array();
			iArray[13][0] = "2015年获得人力发展费用总额";
			iArray[13][1] = "140px";
			iArray[13][2] = 50;
			iArray[13][3] = 0;
			
			SanctionGrid = new MulLineEnter("fm","SanctionGrid");
		     
			SanctionGrid.mulLineCount = 10;   
			SanctionGrid.displayTitle = 1;
			SanctionGrid.canSel = 0;
			SanctionGrid.canChk = 0;
			SanctionGrid.locked = 1;
			SanctionGrid.hiddenSubtraction = 1;
			SanctionGrid.hiddenPlus = 1;
			SanctionGrid.selBoxEventFuncName ="";
		     
			SanctionGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
	
</script>