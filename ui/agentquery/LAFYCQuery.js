//               该文件中包含客户端需要处理的函数和事件

var arrDataSet;
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass(); 

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在BlacklistQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}

function returnParent()
{
  var arrReturn = new Array();
	var tSel = PolGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{		
			try
			{	
				//alert(tSel);
				arrReturn = getQueryResult();
				top.opener.afterBlackQuery( arrReturn );
			}
			catch(ex)
			{
				alert( "没有发现父窗口的afterQuery接口。" + ex );
			}
			top.close();
		
	}
}


function getQueryResult()
{
	var arrSelected = null;
	tRow = PolGrid.getSelNo();
	
	if( tRow == 0 || tRow == null || arrDataSet == null )
	
	return arrSelected;
	
	arrSelected = new Array();
	tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
	
	return arrSelected;
}


function easyQueryClick()
{
	var tReturn = getManageComLimitlike("a.ManageCom");
 if ((fm.all('StartDate').value == '')||(fm.all('EndDate').value == ''))
       { 
  	 alert("请输入日期时间！");
  	 fm.all('ManageCom').focus();
  	 return false;
       }
 
	//查询修改
	var managecom=fm.all('ManageCom').value;
	var branchattr=fm.all('BranchAttr').value;
	var startdate=fm.all('StartDate').value;
	var enddate=fm.all('EndDate').value;
	var agentcode=fm.all('Agentcode').value;	
	
	
	var strSQL1 = "";
	strSQL1 ="select sum(A.TransMoney),sum(A.FYC)"
          +" from LACommision A Where "          
          +"A.Caldate>='"+startdate+"' and A.Caldate<='"+enddate+"' "
          + tReturn
          +getWherePart('A.ManageCom','ManageCom')
          +getWherePart('A.BranchAttr','BranchAttr','like')
          +getWherePart('A.AgentCode','Agentcode');   	
	
	

	turnPage1.strQueryResult  = easyQueryVer3(strSQL1, 1, 0, 1);
	
	  //判断是否查询成功
  if (!turnPage1.strQueryResult) {

  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage1.arrDataCacheSet = decodeEasyQueryResult(turnPage1.strQueryResult);
  
  //设置初始化过的MULTILINE对象
          
  //保存SQL语句
  turnPage1.strQuerySql     = strSQL1; 
  
  //设置查询起始位置
  turnPage1.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet1 = turnPage1.getData(turnPage1.arrDataCacheSet, turnPage1.pageIndex, MAXSCREENLINES);
  
  fm.all('TransMoneySum').value = arrDataSet1[0][0];
  fm.all('FYCSum').value = arrDataSet1[0][1];
  
	// 初始化表格
	initPolGrid();
	
	// 书写SQL语句
	var strSQL = "";
	strSQL ="select A.AgentCode,B.Name,A.PolNo,p11,p13,A.RiskCode,TransMoney,A.PayYears,A.FYCRate,A.FYC"
           +" from LACommision A,LAAgent B Where A.AgentCode=B.AgentCode  "
           + tReturn
           +getWherePart('A.ManageCom','ManageCom')
           +" and A.Caldate>='"+startdate+"' and A.Caldate<='"+enddate+"'"                                 
           +getWherePart('A.BranchAttr','BranchAttr','like')
           +getWherePart('A.AgentCode','Agentcode');
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);  
}
