<%
//程序名称：AgentDetailInit.jsp
//程序功能：
//创建日期：2003-4-8 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            
<script> 
	var tAgentCode = "<%=tAgentCode%>"; 	
</script>
<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('AgentCode').value = '';    
    
    fm.all('EntryNo').value = '';
    fm.all('Name').value = '';
    fm.all('Sex').value = '';
    fm.all('Birthday').value = '';
    fm.all('NativePlace').value = '';
    fm.all('Nationality').value = '';
    fm.all('Marriage').value = '';
    fm.all('CreditGrade').value = '';
    fm.all('HomeAddressCode').value = '';
    fm.all('HomeAddress').value = '';
    fm.all('PostalAddress').value = '';
    fm.all('ZipCode').value = '';
    fm.all('Phone').value = '';
    fm.all('BP').value = '';
    fm.all('Mobile').value = '';
    fm.all('EMail').value = '';
    fm.all('MarriageDate').value = '';
    fm.all('IDNo').value = '';
    fm.all('Source').value = '';
    fm.all('BloodType').value = '';
    fm.all('PolityVisage').value = '';
    fm.all('Degree').value = '';
    fm.all('GraduateSchool').value = '';
    fm.all('Speciality').value = '';
    fm.all('PostTitle').value = '';
    fm.all('ForeignLevel').value = '';
    fm.all('WorkAge').value = '';
    fm.all('OldCom').value = '';
    fm.all('OldOccupation').value = '';
    fm.all('HeadShip').value = '';
    
    fm.all('Business').value = '';
    fm.all('SaleQuaf').value = '';
    fm.all('QuafNo').value = '';
    fm.all('QuafStartDate').value = '';
    fm.all('QuafEndDate').value = '';
    fm.all('DevNo1').value = '';
    fm.all('DevNo2').value = '';
    fm.all('RetainContNo').value = '';
    fm.all('AgentKind').value = '';
    fm.all('DevGrade').value = '';
    fm.all('InsideFlag').value = '';
    fm.all('FullTimeFlag').value = '';
    fm.all('NoWorkFlag').value = '';
    fm.all('TrainDate').value = '';
    fm.all('EmployDate').value = '';
    fm.all('InDueFormDate').value = '';
    fm.all('OutWorkDate').value = '';
    fm.all('AssuMoney').value = '';
    fm.all('AgentState').value = '';  //增员状态
    
    fm.all('SmokeFlag').value = '';
    fm.all('RgtAddress').value = '';
    fm.all('BankCode').value = '';
    fm.all('BankAccNo').value = '';
    fm.all('Remark').value = '';
    
    //行政信息
    fm.all('Branchattr').value = '';
    fm.all('UpAgent').value = '';
    fm.all('IntroAgency').value = '';    
    fm.all('ManageCom').value = '';
    fm.all('AgentSeries').value = '';
    fm.all('AgentGrade').value = '';
    fm.all('RearAgent').value = '';
    fm.all('RearDepartAgent').value = '';
    fm.all('RearSuperintAgent').value = '';

  }
  catch(ex)
  {
    alert("在AddAgentDetailQryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initWarrantorGrid(); 
    
    afterQuery(tAgentCode);   
  }
  catch(re)
  {
    alert("在AddAgentDetailQryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}  
  
// 担保人信息的初始化
function initWarrantorGrid()
  {                               
    var iArray = new Array();      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px"; 	           		//列宽
      iArray[0][2]=1;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="名称";          		        //列名
      iArray[1][1]="60px";      	      		//列宽
      iArray[1][2]=20;            			//列最大值
      iArray[1][3]=0;             //是否允许输入,1表示允许，0表示不允许,2表示代码选择，3表示该列是隐藏的

      iArray[2]=new Array();
      iArray[2][0]="性别";         		        //列名
      iArray[2][1]="30px";            			//列宽
      iArray[2][2]=10;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许


      iArray[3]=new Array();
      iArray[3][0]="身份证号码";      	   		//列名
      iArray[3][1]="130px";            			//列宽
      iArray[3][2]=20;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="出生日期";      	   		//列名
      iArray[4][1]="80px";            			//列宽
      iArray[4][2]=10;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="单位";      	   		//列名
      iArray[5][1]="110px";            			//列宽
      iArray[5][2]=10;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[6]=new Array();
      iArray[6][0]="家庭地址编码";      	   	//列名
      iArray[6][1]="80px";            			//列宽
      iArray[6][2]=10;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="家庭地址";      	   		//列名
      iArray[7][1]="120px";            			//列宽
      iArray[7][2]=10;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();
      iArray[8][0]="邮政编码";      	   		//列名
      iArray[8][1]="60px";            			//列宽
      iArray[8][2]=6;            			//列最大值
      iArray[8][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="电话";      	   		//列名
      iArray[9][1]="80px";            			//列宽
      iArray[9][2]=10;            			//列最大值
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[10]=new Array();
      iArray[10][0]="关系";      	   		//列名
      iArray[10][1]="50px";            			//列宽
      iArray[10][2]=10;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      

      WarrantorGrid = new MulLineEnter( "fm" , "WarrantorGrid" ); 
     
      WarrantorGrid.mulLineCount = 1;   
      WarrantorGrid.displayTitle = 1;
     
      WarrantorGrid.hiddenPlus=1;
	  WarrantorGrid.hiddenSubtraction=1;    
	  
      WarrantorGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert(ex);
      }
}


</script>