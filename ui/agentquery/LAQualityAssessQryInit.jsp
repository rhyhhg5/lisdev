<%
//程序名称：LAQualityAssessQryInit.jsp
//程序功能：
//创建日期：2003-10-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                            
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
    fm.all('AgentCode').value = <%=tG.Operator%>;    
  }
  catch(ex)
  {
    alert("在LAQualityAssessQryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

                               

function initForm()
{
  try
  {
    initInpBox();
    initQualityGrid();
  }
  catch(re)
  {
    alert("LAQualityAssessQryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化PresenceGrid
 ************************************************************
 */
function initQualityGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

                    
        iArray[1]=new Array();
        iArray[1][0]="扣分类型";         //列名
        iArray[1][1]="80px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="扣分代码";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许        
        
        iArray[3]=new Array();
        iArray[3][0]="所扣分数";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="执行日期";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[5]=new Array();
        iArray[5][0]="扣分内容";         //列名
        iArray[5][1]="400px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
               
        iArray[6]=new Array();
        iArray[6][0]="扣分原因";         //列名
        iArray[6][1]="400px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
                      
        iArray[7]=new Array();
        iArray[7][0]="批注";         //列名
        iArray[7][1]="400px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
                     
        QualityGrid = new MulLineEnter( "fm" , "QualityGrid" ); 

        //这些属性必须在loadMulLine前
        QualityGrid.mulLineCount = 3;   
        QualityGrid.displayTitle = 1;
        
        QualityGrid.locked=1;
		QualityGrid.hiddenPlus=1;
		QualityGrid.hiddenSubtraction=1;
		
        QualityGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化QualityGrid时出错："+ ex);
      }
    }


</script>