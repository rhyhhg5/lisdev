<%
//程序名称：LAAllAssessInit.jsp
//程序功能：
//创建日期：2003-10-14 10:07
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('AgentCode').value = '<%=tG.Operator%>';
    fm.all('StartDate').value = '';
    fm.all('EndDate').value = '';
    fm.all('TransMoneySum').value = '0';
    fm.all('FYCSum').value = '0';
       
  }
  catch(ex)
  {
    alert("LAAllAssessInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}


function initForm()
{
  try
  {
  
    initInpBox();
    initCommisionGrid();
	  
  }
  catch(re)
  {
    alert("LAAllAssessInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initCommisionGrid()
{                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="60px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="保单号";         //列名
        iArray[1][1]="200px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="投保人";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="被保人";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="险种";         //列名
        iArray[4][1]="80px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0; 
        
        iArray[5]=new Array();
        iArray[5][0]="保费";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="缴费期";         //列名
        iArray[6][1]="50px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="FYC率";         //列名
        iArray[7][1]="70px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="FYC";         //列名
        iArray[8][1]="100px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;        
       

        CommisionGrid = new MulLineEnter( "fm" , "CommisionGrid" ); 

        //这些属性必须在loadMulLine前
        CommisionGrid.mulLineCount = 3;   
        CommisionGrid.displayTitle = 1;
        CommisionGrid.hiddenPlus = 1;
        CommisionGrid.hiddenSubtraction = 1;
        
        CommisionGrid.loadMulLine(iArray);  
	
      }
      catch(ex)
      {
        alert("初始化CommisionGrid时出错："+ ex);
      }
}
</script>