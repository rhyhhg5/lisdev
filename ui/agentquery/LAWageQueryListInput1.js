var turnPage = new turnPageClass(); 
function submitForm()
{
  
   if(!check())
    return false ;
    
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
	fm.submit();
	showInfo.close();
}
function check()
{
 if (!verifyInput())
 return false;
 return true; 
}
function easyQueryClick()
{	
	
   if(!check())
    return false ;
	// 初始化表格	
	// 书写SQL语句
	var tStartMonth=fm.all('StartMonth').value;
	var tEndMonth=fm.all('EndMonth').value;
	
		if(trim(tStartMonth.substring(0,4))!=trim(tEndMonth.substring(0,4)))
	{
	  alert("统计起期与统计止期必须在同一年！");  
	  return false;
	}
	var temp=trim(tEndMonth.substring(4,6))-trim(tStartMonth.substring(4,6));
	if(temp>=3)
	{
		alert("因为数据量较大，故请将查询间隔控制在3个月内，谢谢！");  
	  return false;
		}

	// 书写SQL语句
	var strSQL = "";
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
	
	
	
  	var tStartMonth=fm.all('StartMonth').value;
	  var tEndMonth=fm.all('EndMonth').value;
    strSQL = "select a.wageno,a.branchattr aa,a.agentcode ab,b.name,a.contno ac,a.p11,a.p13, "
    +" a.riskcode,(select riskname from lmrisk where lmrisk.riskcode=a.riskcode),a.transmoney,a.FYCRate,a.fyc,"
    +" (select codename from ldcode where codetype='payintv' and int(code)=a.payintv),a.payyears,a.paycount,"
    +" a.tconfdate,a.customgetpoldate,a.getpoldate,a.commisionsn,a.makepoldate,"
    +"case when a.renewcount=0 and a.payyear>0 then '续期'"
    +" when a.renewcount>0 then '续保' else '新单' end,"
    +"case  when substr(PayPlanCode,1,6)='000000' "
    +" and transtype='ZC'  then '加费'  when payplancode='111111' then '退加费'  else '正常' end  ,"
    +" case  when b.agentstate<='02'  then '在职'  when b.agentstate>'02'  and b.agentstate<'06' then '离职未确认'  else '离职' end,a.f3  "
    +" from LACommision a,LAAgent b  where  a.agentcode=b.agentcode " 
    +"and ((a.branchtype='1'and a.branchtype2 = '01' and b.branchtype= '1' and b.branchtype2='01') or (a.branchtype='1' and a.branchtype2='03')) "    
    +" and a.wageno>='"+tStartMonth+"' and a.wageno<='"+tEndMonth+"' and a.fycrate>=0  "	
	  + getWherePart("a.AgentCode","AgentCode")
	  + getWherePart("b.Name","AgentName")
	  + getWherePart("a.ContNo","ContNo")
	  + getWherePart("a.RiskCode","RiskCode")
	  + getWherePart("a.ManageCom","ManageCom")
	  + getWherePart("a.F3","WrapCode")              
      + " order by aa,ab,ac";	          	
	//alert(strSQL);
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}