//               该文件中包含客户端需要处理的函数和事件

var mDebug="0";
var mOperate="";
var showInfo;
var arrDataSet;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
	  try
	  {
	    showInfo.focus();  
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在Agent.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  
    
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
  fm.all('AgentCode').value = '';
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  alert('在此不能修改！');	
  /*
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要修改的代理人记录！');    
  }else
  {
    //下面增加相应的代码
    if (confirm("您确实想修改该记录吗?"))
    {  	
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }*/
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./AgentQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  alert('在此不能删除！');	
  /*
  if ((fm.all("AgentCode").value == null)||(fm.all("AgentCode").value == ''))
  {
    alert('请先查询出要删除的代理人记录！');
  }else
  {	
  //下面增加相应的删除代码
  if (confirm("您确实想删除该记录吗?"))
  {
    mOperate="DELETE||MAIN";  
    submitForm();
  }
  else
  {
    mOperate="";
    alert("您取消了删除操作！");
  }
  }*/
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}
function returnParent()
{
  top.close();	
}

function afterQuery( tEdorNO , tAgentCode )
{	
     var arrSelected = new Array();	
     var strSQL = "";
     strSQL = "select a.* from LATreeB a where  a.EdorNO='" + tEdorNO + "'"+" and a.AgentCode= '" +tAgentCode+ "'";   

     var strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);  
  
  if (!strQueryResult) {
    alert("查询失败！");
    return false;
    }

//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(strQueryResult);
	
	var arrResult = new Array();
	
	if( arrSelected != null )
	{       
	   arrResult = arrSelected;
				                                  
           fm.all('EdorNO').value = arrResult[0][30];
           fm.all('AgentSeries').value =arrResult[0][3] ;
           fm.all('AgentGrade').value = arrResult[0][4];
           fm.all('AgentLastSeries').value = arrResult[0][5];
           fm.all('AgentLastGrade').value = arrResult[0][6];
           fm.all('OldStartDate').value = arrResult[0][18];
           fm.all('OldEndDate').value = arrResult[0][19];
           fm.all('StartDate').value = arrResult[0][20];
           fm.all('AstartDate').value = arrResult[0][21];
           fm.all('AssessType').value = arrResult[0][22];
           fm.all('State').value = arrResult[0][23] ;
        }
}
