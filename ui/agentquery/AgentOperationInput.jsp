<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2003-5-15 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="AgentOperation.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<%@include file="AgentOperationInit.jsp"%>
	<%@include file="../common/jsp/ManageComLimit.jsp"%>
	
	<title>代理人业务查询</title>
</head>

<body  onload="initForm();" >
  <form  name=fm >
  <table>
    	<tr>
    	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLDPerson1);">
    	</td>
			<td class= titleImg>
				查询条件
			</td>
		</tr>
	</table>
	<Div  id= "divLDPerson1" style= "display: ''">
    <table  class= common align=center>
      	<TR  class= common>
          <TD  class= title>
            代理人编码 
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCode >
          </TD>
        </TR>
		</table>
        <tr>		
					<td>
						 <INPUT class=common VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
					</td>
		   </tr>
  </Div>  
    <table>
    	<tr>
        <td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divAgent1);">
    		</td>
    		<td class= titleImg>
    			 代理人信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divAgent1" style= "display: ''">
      <table  class= common>
       	<tr  class= common>
      	  	<td text-align: left colSpan=1>
  						<span id="spanPolGrid" >
  						</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table>
       	<tr>
       		<td>
      			<INPUT class=common VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      		</td>
      		<td>
      			<INPUT class=common VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      		</td>
      		<td>      		
      			<INPUT class=common VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      		</td>
      		<td>      		
      			<INPUT class=common VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">				
					</td> 
  	  	</tr>
  	  	<tr>
       		<td>
      			<INPUT class=common VALUE="预收查询" TYPE=button onclick="PrepTakeQuery();"> 
      		</td>
      		<td>
      			<INPUT class=common VALUE="承保查询" TYPE=button onclick="AssumeQuery();"> 					
      		</td>
      		<td>      		
      			<INPUT class=common VALUE="回单查询" TYPE=button onclick="BackQuery();"> 
      		</td>
      		<td>      		
      			<INPUT class=common VALUE="退保查询" TYPE=button onclick="ZTQuery();">				
					</td> 
  	  	</tr>
  	  	<tr>
       		<td>
      			<INPUT class=common VALUE="预收未承保查询" TYPE=button onclick="PTNotAQQuery();"> 
      		</td>
      		<td>
      			<INPUT class=common VALUE="承保未回单查询" TYPE=button onclick="AQNotZTQuery();"> 					
      		</td>
      		<td>      		
      			<INPUT class=common VALUE="犹豫期撤件查询" TYPE=button onclick="WTQuery();"> 
      		</td>
      		<!--td>      		
      			<INPUT class=common VALUE="退保查询" TYPE=button onclick="turnPage.lastPage();">				
					</td--> 
  	  	</tr>
    	</table>
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>


