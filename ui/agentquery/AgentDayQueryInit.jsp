<%
//程序名称：AgentDayQueryInit.jsp
//程序功能：
//创建日期：2003-4-8
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                                       
  
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
    fm.all('AgentCode').value = '<%=tG.Operator%>';
    fm.all('Name').value = '';
    fm.all('QueryDate').value = '';
    
  }
  catch(ex)
  {
    alert("在AgentDayQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在AgentDayQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initAgentDayGrid();
    getAgnetName()
  }
  catch(re)
  {
    alert("AgentDayQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化AgentGrid
 ************************************************************
 */
function initAgentDayGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="保单号码";         //列名
        iArray[1][1]="200px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="交易号";         //列名
        iArray[2][1]="200px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="交易金额";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="交易交费日期";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="交易确认日期";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[6]=new Array();
        iArray[6][0]="系列号";         //列名
        iArray[6][1]="0px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许

          
        AgentDayGrid = new MulLineEnter( "fm" , "AgentDayGrid" ); 

        //这些属性必须在loadMulLine前
        AgentDayGrid.mulLineCount = 6;   
        AgentDayGrid.displayTitle = 1;
        AgentDayGrid.canSel=1;
//      AgentGrid.canChk=0;
        AgentDayGrid.locked=1;
	AgentDayGrid.hiddenPlus=1;
	AgentDayGrid.hiddenSubtraction=1;
        AgentDayGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化AgentDayGrid时出错："+ ex);
      }
    }


</script>