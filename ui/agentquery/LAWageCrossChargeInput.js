//               该文件中包含客户端需要处理的函数和事件
//程序名称：
//程序功能：
//创建日期：
//创建人  ：   
//更新记录：  更新人    更新日期     	更新原因/内容

var showInfo;
var mDebug="0";
var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();



// 查询按钮
function easyQueryClick()
{
	//alert ("start easyQueryClick");
	// 初始化表格
	initSetGrid();
	var tStartDate = fm.all('StartDate').value;
	var tEndDate = fm.all('EndDate').value;
	
	
	// 书写SQL语句
	if (tStartDate == null || tStartDate == "") {
		alert("请输入佣金生成时间的起始日期！");
		return;
	}
	if (tEndDate == null || tEndDate == "") {
		alert("请输入佣金生成时间的终止日期！");
		return;
	}
	if(dateDiff(tStartDate,tEndDate,"M") > 3)
	{
		alert("起止日期间隔必须含3个月内！");
		return false;
	}
	if(tStartDate.length != 10||tEndDate.length !=10){
		alert("起始日期的长度不符合要求")
		return false;
	}
	var strSQL = "";
	strSQL = "select contno,compcode,compname,cvalidate,signdate,riskcode,riskname,actno,tmakedate,feepaytypecode,feepaytypename,charge,chargerate,actcode,actname," +
			" OrgBelgCode,orgbelgname,orgcrscode,orgcrsname,orgcrscompcode,polsalescode,polsalesname,crssalescode,crssalesname,cstname,insrtarg,prem,datesend"
	+ " from lapayowncharge  "
	+" where 1=1 and  tmakedate between replace('"+tStartDate+"','-','') and replace('"+tEndDate+"','-','') "
	+ getWherePart( 'polsalescode','CustomerNo')
	+ getWherePart( 'polsalesname','Name','like')
	+ " order by TmakeDate,contno";
	//查询SQL，返回结果字符串
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
	if (!turnPage.strQueryResult) {
		alert("没有符合条件的数据，请重新录入查询条件！");
		return false;
	}

//查询成功则拆分字符串，返回二维数组
  arrDataSet = decodeEasyQueryResult(turnPage.strQueryResult);
  turnPage.arrDataCacheSet = arrDataSet;

	//查询成功则拆分字符串，返回二维数?				//	turnPage.arrDataCacheSet= decodeEasyQueryResult(turnPage.strQueryResult);
	//设置初始化过的MULTILINE对象
	turnPage.pageDisplayGrid = SetGrid;
	//保存SQL语句
	turnPage.strQuerySql = strSQL;

	//设置查询起始位置
	turnPage.pageIndex = 0;

	//在查询结果数组中取出符合页面显示大小设置的数?					var tArr = new Array();
	tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);

	//调用MULTILINE对象显示查询结果
	displayMultiline(tArr, turnPage.pageDisplayGrid);
	//return true;
	fm.all('querySql').value=strSQL;
}


				//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
	{
	showInfo.close();
	if (FlagStr == "Fail" )
		{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:440px;dialogHeight:340px");
		}
	else
		{
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:440px;dialogHeight:340px");
			//showDiv(operateButton,"true");
			//showDiv(inputButton,"false");
			initForm();
		}
		}

	//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
	function DoReset()
		{
		try
		{
			initForm();
		}
		catch(re)
		{
			alert("初始化页面错误，重置出错");
		}
		}




	//显示frmSubmit框架，用来调试
	function showSubmitFrame(cDebug)
		{
		if(cDebug=="1")
		{
			parent.fraMain.rows = "0,0,0,0,*";
		}
		else
			{
				parent.fraMain.rows = "0,0,0,0,*";
			}
		}




function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
		{
			cDiv.style.display="none";
		}
}

									

						

//新增下载函数
function DoNewDownload()
{

	if (verifyInput() == false)
	    return false;
	//alert( fm.querySql.value);
	  var sql = fm.querySql.value;
	  if(sql==""||sql==null)
	  {
		  alert("请先查询");
		  return false;
	  }
	
	  var oldAction = fm.action;
	  fm.action = "./LAWageCrossChargeSave.jsp";
	  fm.submit();
	  fm.action = oldAction;
  /*var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	//fm.all('hideOperate').value = 'download';
	fm.submit();
	showInfo.close();*/
	
}	



