<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LASpecialAssessQuery.jsp
//程序功能：
//创建日期：2007-7-31
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>

<html>
<head>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LASpecialAssessQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LASpecialAssessQueryInit.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title></title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAIndexTemp1);">
    </IMG>
      <td class=titleImg>
      挂起人员信息查询报表
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAIndexTemp1" style= "display: ''">
    
  <table class= common>
    <tr class= common>      
      <td class= title>
        管理机构
      </td>
      <TD class= input>
          <Input class="code" name=ManageCom 
           ondblclick="return showCodeList('comcode',[this],null,null,8,'char(length(trim(comcode)))',1);"
           onkeyup="return showCodeListKey('comcode',[this],null,null,8,'char(length(trim(comcode)))',1);"> 
      </TD>    
      <TD class= title>
        考核年月
      </TD>
      <TD class= input>
          <Input name=AssessDate class=common verify="考核年月|notnull&len=6" elementtype=nacessary>
          <font color="red">注:'YYYYMM'</font> 
      </TD>       
    </tr>
  </table>
          <INPUT VALUE="查   询" class="cssButton" TYPE=button onclick="QueryClick();">
          <INPUT VALUE="打   印" class="cssButton" TYPE=button onclick="DetailPrint();">      
		<Div  id= "divLAAgent1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanSpecialAgentGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
  	</div>
  	<Div id= "divPage" align=center style= "display: 'none' ">
      <INPUT VALUE="首  页" class=cssbutton TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class=cssbutton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class=cssbutton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" class=cssbutton TYPE=button onclick="turnPage.lastPage();"> 				
    </div>     
       <Input type=hidden name=BranchType value=''> 
       <Input type=hidden name=BranchType2 value=''>
       <Input type=hidden name=fmtransact value=''>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
