<%
//程序名称：LAAssessInfoQueryInit.jsp
//程序功能：
//创建日期：2003-08-05 10:07
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  
    fm.all('IndexCalNo').value='';
    fm.all('AssessType').value='';
    fm.all('AgentGrade').value='';    
    fm.all('ManageCom').value = '';
    fm.all('StandAssessFlag').value = '';
    fm.all('BranchType').value = getBranchType();
    
  }
  catch(ex)
  {
    alert("在LAAssessInfoQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAAssessInfoQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    
    initInpBox();
    initSelBox();    
    initLAAssessInfoGrid();
	  
  }
  catch(re)
  {
    alert("LAAssessInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initLAAssessInfoGrid()
{                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名        

        iArray[1]=new Array();
        iArray[1][0]="考核前职级";         //列名
        iArray[1][1]="70px";         //宽度
        iArray[1][2]=70;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="机构代码";         //列名
        iArray[2][1]="150px";         //宽度
        iArray[2][2]=150;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="机构名称";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="被考核人代码";         //列名
        iArray[4][1]="80px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="被考核人姓名";         //列名
        iArray[5][1]="70px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许  
        
        iArray[6]=new Array();
        iArray[6][0]="考核后职级";         //列名
        iArray[6][1]="70px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        LAAssessInfoGrid = new MulLineEnter( "fm" , "LAAssessInfoGrid" ); 

        //这些属性必须在loadMulLine前
        LAAssessInfoGrid.mulLineCount = 0;   
        LAAssessInfoGrid.displayTitle = 1;
        LAAssessInfoGrid.locked=1;
        LAAssessInfoGrid.canSel=0;
        LAAssessInfoGrid.canChk=0;
      LAAssessInfoGrid.hiddenPlus = 1;
      LAAssessInfoGrid.hiddenSubtraction = 1;

        LAAssessInfoGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化LAAssessInfoGrid时出错："+ ex);
      }
}


</script>