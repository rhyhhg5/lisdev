<%
//程序名称：LAAMQueryInit.jsp
//程序功能：个险中介业务保单明细查询
//创建日期：2011-3-30
//创建人  ：龙程彬
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {  
    fm.all('ManageCom').value = '';
 	fm.all('Month').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentCom').value = '';
    fm.all('ContNo').value = '';
    fm.all('RiskCode').value = '';  
    fm.all('BranchType').value =  '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';     
                           
  }
  catch(ex)
  {
    alert("在Init.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();
    initGrpPolGrid();	
  }
  catch(re)
  {
    alert("Init.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var PolGrid; 
function initGrpPolGrid()
  {                               
    var iArray = new Array();
     
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保单号";         		    //列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="展业机构编码";            //列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="中介机构编码";         	//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=200;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="中介机构名称";         	//列名
      iArray[4][1]="200px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      

      iArray[5]=new Array();
      iArray[5][0]="投保人";         		    //列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许
   
      iArray[6]=new Array();
      iArray[6][0]="险种编码";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0; 						//是否允许输入,1表示允许，0表示不允许
      
      iArray[7]=new Array();
      iArray[7][0]="险种名称";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0; 				
      
      iArray[8]=new Array();
      iArray[8][0]="保费";         		    //列名
      iArray[8][1]="60px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 						//是否允许输入,1表示允许，0表示不允许      

      iArray[9]=new Array();
      iArray[9][0]="提奖比例";         		//列名
      iArray[9][1]="60px";            		//列宽
      iArray[9][2]=70;            			//列最大值
      iArray[9][3]=0; 						//是否允许输入,1表示允许，0表示不允许  
      
      iArray[10]=new Array();
      iArray[10][0]="提奖";         		    //列名
      iArray[10][1]="60px";            		//列宽
      iArray[10][2]=100;            		//列最大值
      iArray[10][3]=0; 						//是否允许输入,1表示允许，0表示不允许   
      
      iArray[11]=new Array();
      iArray[11][0]="缴费频次";         		//列名
      iArray[11][1]="60px";            		//列宽
      iArray[11][2]=100;            		//列最大值
      iArray[11][3]=0; 						//是否允许输入,1表示允许，0表示不允许     
      
      iArray[12]=new Array();
      iArray[12][0]="缴费期";         		//列名
      iArray[12][1]="60px";            		//列宽
      iArray[12][2]=100;            		//列最大值
      iArray[12][3]=0; 	  					//是否允许输入,1表示允许，0表示不允许       
      
      iArray[13]=new Array();
      iArray[13][0]="缴费次数";         		//列名
      iArray[13][1]="60px";            		//列宽
      iArray[13][2]=100;            		//列最大值
      iArray[13][3]=0; 						//是否允许输入,1表示允许，0表示不允许     
      
      iArray[14]=new Array();
      iArray[14][0]="签单日期";         		//列名
      iArray[14][1]="80px";            		//列宽
      iArray[14][2]=100;            		//列最大值
      iArray[14][3]=0; 	

      iArray[15]=new Array();
      iArray[15][0]="回执回销日期";           //列名
      iArray[15][1]="80px";            		//列宽
      iArray[15][2]=100;            		//列最大值
      iArray[15][3]=0; 	                    
      
      iArray[16]=new Array();
      iArray[16][0]="交单日期";         		//列名
      iArray[16][1]="80px";            		//列宽
      iArray[16][2]=100;            		//列最大值
      iArray[16][3]=0; 	                    
      
      iArray[17]=new Array();
      iArray[17][0]="专员代码";         		//列名
      iArray[17][1]="80px";            		//列宽
      iArray[17][2]=200;            			//列最大值
      iArray[17][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[18]=new Array();
      iArray[18][0]="专员姓名";         		//列名
      iArray[18][1]="60px";            		//列宽
      iArray[18][2]=200;            			//列最大值
      iArray[18][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
      iArray[19]=new Array();
      iArray[19][0]="人员状态";         		//列名
      iArray[19][1]="60px";            		//列宽
      iArray[19][2]=100;            		//列最大值
      iArray[19][3]=0; 
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
       
      PolGrid.mulLineCount = 10;   
      PolGrid.displayTitle = 1;
      PolGrid.hiddenPlus = 1;
      PolGrid.hiddenSubtraction = 1;    
      PolGrid.locked = 1;
      PolGrid.canSel = 0;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>