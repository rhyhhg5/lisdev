<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="LANoRevisitContQueryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LANoRevisitContQueryInit.jsp"%>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <title>回访不成功保单查询 </title>  
</head>
<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
%>
<script>
  Operator = "<%=tGlobalInput.Operator%>";
  var  mBranchType =  <%=BranchType%>;
  var tsql ="8 and db2inst1.trim(comcode) in (select code from ldcode where codetype = #yindaihuifang#) "
</script>
<body  onload="initForm(); initElementtype();" >
  <form action="./LANoRevisitContQuerySave.jsp" method=post name=fm target="fraSubmit">
    <!-- 保单信息部分 -->
    <table class= common border=0 width=100%>
    	<tr>
			<td class= titleImg align= center>回访不成功保单信息查询：</td>
		  </tr>
	  </table>
    <table  class= common align=center>
      	 <TR class = common>
          <TD class = title>
             管理机构
          </TD>
          <TD  class= input>
            <Input class="codeno" name=ManageCom 
             verify="管理机构|notnull&code:comcode"
             ondblclick="return showCodeList('ComCode',[this,ManageComName],[0,1],null,tsql,'to_char(length(trim(comcode)))',1);"
             onkeyup="return showCodeListKey('ComCode',[this,ManageComName],[0,1],null,tsql,'to_char(length(trim(comcode)))',1);" 
             ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
            
          </TD>          
           <TD class = title>
             银代机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom ondblclick="return getAgentComName(this,AgentComName);" onkeyup="return getAgentComName(this,AgentComName);"  ><Input class=codename name=AgentComName readOnly >
          </TD>
        </TR>
        <TR  class = common>
           <TD  class = title>
           财务结算起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="财务结算起期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            财务结算止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="财务结算止期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          
        </TR> 
        
    </table>
          <INPUT CLASS=cssButton VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT CLASS=cssButton VALUE="下载" TYPE=button onclick="DownLoadClick();"> 
          
          <input type="hidden" class=input name=fmAction >
          <input type="hidden" class=input name=BranchType value="<%=BranchType%>">
          <input type="hidden" class=input name=querySql >
          <input type ="hidden" class=input name=querySqlTitle >
          <input type=hidden class=Common name=Title > 
          
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCPol1);">
    		</td>
    		<td class= titleImg>
    			 信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLCPol1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <Div  id= "divPage" align=center style= "display: 'none' ">
      <INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
      </Div>			
  	</div>    
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
