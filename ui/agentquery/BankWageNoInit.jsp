<%
//程序名称：WageQueryListInit.jsp
//程序功能：
//创建日期：
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {  
    fm.all('ManageCom').value = '';
    fm.all('WageNo').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentCom').value = '';
    fm.all('ContNo').value = '';
    fm.all('RiskCode').value = ''; 
    fm.all('BranchType').value =  '<%=BranchType%>';
    fm.all('BranchType2').value = '<%=BranchType2%>';     
                           
  }
  catch(ex)
  {
    alert("在BankWageNoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在BankWageNoInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
   	initPolGrid();	
  }
  catch(re)
  {
    alert("WageQueryListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

var PolGrid; 
// 保单信息列表的初始化
function initPolGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[1]=new Array();
      iArray[1][0]="薪资年月";         		//列名
      iArray[1][1]="80px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[2]=new Array();
      iArray[2][0]="管理机构";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="所属团队名称";         		//列名
      iArray[3][1]="160px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="业务员代码";         		//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="业务员姓名";         		//列名
      iArray[5][1]="100px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="员工类型";         		//列名
      iArray[6][1]="100px";            		//列宽
      iArray[6][2]=200;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[7]=new Array();
      iArray[7][0]="银代机构编码";         		//列名
      iArray[7][1]="100px";            		//列宽
      iArray[7][2]=200;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      
    
      
      iArray[8]=new Array();
      iArray[8][0]="银代机构名称";         		//列名
      iArray[8][1]="100px";            		//列宽
      iArray[8][2]=100;            			//列最大值
      iArray[8][3]=0; 									//是否允许输入,1表示允许，0表示不允许
      
      iArray[9]=new Array();
      iArray[9][0]="险种编码";         		//列名
      iArray[9][1]="100px";            		//列宽
      iArray[9][2]=100;            			//列最大值
      iArray[9][3]=0; 				
      
      
      iArray[10]=new Array();
      iArray[10][0]="险种名称";         		//列名
      iArray[10][1]="100px";            		//列宽
      iArray[10][2]=200;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许
             
      

      iArray[11]=new Array();
      iArray[11][0]="保单号";         		//列名
      iArray[11][1]="100px";            		//列宽
      iArray[11][2]=100;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[12]=new Array();
      iArray[12][0]="投保人姓名";         		//列名
      iArray[12][1]="100px";            		//列宽
      iArray[12][2]=100;            			//列最大值
      iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许

     
   
      iArray[13]=new Array();
      iArray[13][0]="被保人姓名";         		//列名
      iArray[13][1]="100px";            		//列宽
      iArray[13][2]=100;            			//列最大值
      iArray[13][3]=0; 									//是否允许输入,1表示允许，0表示不允许      

 	  iArray[14]=new Array();
      iArray[14][0]="保费";         		//列名
      iArray[14][1]="100px";            		//列宽
      iArray[14][2]=100;            			//列最大值
      iArray[14][3]=0; 									//是否允许输入,1表示允许，0表示不允许  
      
      iArray[15]=new Array();
      iArray[15][0]="提奖比例";         		//列名
      iArray[15][1]="100px";            		//列宽
      iArray[15][2]=100;            			//列最大值
      iArray[15][3]=0; 									//是否允许输入,1表示允许，0表示不允许   
      
      iArray[16]=new Array();                             
      iArray[16][0]="提奖金额";         		//列名          
      iArray[16][1]="100px";            		//列宽          
      iArray[16][2]=100;            			//列最大值      
      iArray[16][3]=0;
      
      iArray[17]=new Array();                             
      iArray[17][0]="主管间接提奖比例";         		//列名          
      iArray[17][1]="100px";            		//列宽          
      iArray[17][2]=100;            			//列最大值      
      iArray[17][3]=0; 	                                  
      
      
      iArray[18]=new Array();                             
      iArray[18][0]="主管提奖金额";         		//列名          
      iArray[18][1]="100px";            		//列宽          
      iArray[18][2]=100;            			//列最大值      
      iArray[18][3]=0; 	                                  
      
      
      iArray[19]=new Array();
      iArray[19][0]="投保日期";         		//列名
      iArray[19][1]="100px";            		//列宽
      iArray[19][2]=100;            			//列最大值
      iArray[19][3]=0; 									//是否允许输入,1表示允许，0表示不允许     
      
      iArray[20]=new Array();
      iArray[20][0]="签单日期";         		//列名
      iArray[20][1]="100px";            		//列宽
      iArray[20][2]=100;            			//列最大值
      iArray[20][3]=0;                                   //是否允许输入,1表示允许，0表示不允许
     
      iArray[21]=new Array();
      iArray[21][0]="回执回销日期";         		//列名
      iArray[21][1]="100px";            		//列宽
      iArray[21][2]=100;            			//列最大值
      iArray[21][3]=0; 									//是否允许输入,1表示允许，0表示不允许     
      
     
      iArray[22]=new Array();
      iArray[22][0]="保费类型";         		//列名
      iArray[22][1]="100px";            		//列宽
      iArray[22][2]=100;            			//列最大值
      iArray[22][3]=0; 									//是否允许输入,1表示允许，0表示不允许     
      
      iArray[23]=new Array();
      iArray[23][0]="保单年度";         		//列名
      iArray[23][1]="100px";            		//列宽
      iArray[23][2]=100;            			//列最大值
      iArray[23][3]=0; 	                                 //是否允许输入,1表示允许，0表示不允许 
     
      iArray[24]=new Array();
      iArray[24][0]="犹豫期撤保日期";         		//列名
      iArray[24][1]="100px";            		//列宽
      iArray[24][2]=100;            			//列最大值
      iArray[24][3]=0;                                   //是否允许输入,1表示允许，0表示不允许
      
      iArray[25]=new Array();
      iArray[25][0]="是否银代直销";         		//列名
      iArray[25][1]="0px";            		//列宽
      iArray[25][2]=100;            			//列最大值
      iArray[25][3]=0;                                   //是否允许输入,1表示允许，0表示不允许

      iArray[26]=new Array();
      iArray[26][0]="系列号";         		//列名
      iArray[26][1]="100px";            		//列宽
      iArray[26][2]=100;            			//列最大值
      iArray[26][3]=0;                                   //是否允许输入,1表示允许，0表示不允许
      
      iArray[27]=new Array();
      iArray[27][0]="缴费年限";         		//列名
      iArray[27][1]="100px";            		//列宽
      iArray[27][2]=100;            			//列最大值
      iArray[27][3]=0;                                   //是否允许输入,1表示允许，0表示不允许
      
      iArray[28]=new Array();
      iArray[28][0]="保险期间";         		//列名
      iArray[28][1]="100px";            		//列宽
      iArray[28][2]=100;            			//列最大值
      iArray[28][3]=0;                                   //是否允许输入,1表示允许，0表示不允许

      iArray[29]=new Array();
      iArray[29][0]="保险期间标记";         		//列名
      iArray[29][1]="100px";            		//列宽
      iArray[29][2]=100;            			//列最大值
      iArray[29][3]=0;                                   //是否允许输入,1表示允许，0表示不允许

      iArray[30]=new Array();
      iArray[30][0]="回访结果";         		//列名
      iArray[30][1]="100px";            		//列宽
      iArray[30][2]=100;            			//列最大值
      iArray[30][3]=0;                                   //是否允许输入,1表示允许，0表示不允许

      iArray[31]=new Array();
      iArray[31][0]="回访成功时间";         		//列名
      iArray[31][1]="100px";            		//列宽
      iArray[31][2]=100;            			//列最大值
      iArray[31][3]=0;                                   //是否允许输入,1表示允许，0表示不允许
      
      PolGrid = new MulLineEnter( "fm" , "PolGrid" ); 
      //这些属性必须在loadMulLine前
      PolGrid.mulLineCount = 10;   
      PolGrid.displayTitle = 1;
      PolGrid.locked = 1;
      PolGrid.canSel = 0;
      PolGrid.hiddenPlus=1;       
      PolGrid.hiddenSubtraction=1;
      PolGrid.loadMulLine(iArray);  
      
      //这些操作必须在loadMulLine后面
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>