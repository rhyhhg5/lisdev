<%
//程序名称：LAAssessInfoQueryInit.jsp
//程序功能：
//创建日期：2003-08-05 10:07
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<%
 String tBranchType = request.getParameter("BranchType");
 String tBranchType2 = request.getParameter("BranchType2");
%>
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                    
    fm.all('ManageCom').value = '';   
    fm.all('AssessDate').value = '';
    fm.all('BranchType').value = '<%=tBranchType%>';
    fm.all('BranchType2').value ='<%=tBranchType2%>';
    fm.all('fmtransact').value  = '';
  }
  catch(ex)
  {
    alert("在LAIndexTempQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
    initInpBox();  
    initSpecialAgentGrid();
  }
  catch(re)
  {
    alert("LAAssessInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
var SpecialAgentGrid;
function initSpecialAgentGrid()
{
  var iArray = new Array();
  try{
     iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="40px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="代理人编码";         //列名
        iArray[1][1]="100px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="代理人姓名";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
      
        iArray[3]=new Array();
        iArray[3][0]="管理机构";         //列名
        iArray[3][1]="100px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许   
        
        iArray[4]=new Array();
        iArray[4][0]="管理机构名称";         //列名
        iArray[4][1]="100px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许   
        
        iArray[5]=new Array();
        iArray[5][0]="销售机构";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许    
        
        iArray[6]=new Array();
        iArray[6][0]="销售机构名称";         //列名
        iArray[6][1]="100px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许    
        
        iArray[7]=new Array();
        iArray[7][0]="考核年月";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许    
      SpecialAgentGrid = new MulLineEnter( "fm" , "SpecialAgentGrid" );
    //这些属性必须在loadMulLine前
      SpecialAgentGrid.mulLineCount = 0;   
      SpecialAgentGrid.displayTitle = 1;
      SpecialAgentGrid.locked = 1;
      //SpecialAgentGrid.canSel = 1;
      SpecialAgentGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
    alert(ex);
  }
      
}
</script>