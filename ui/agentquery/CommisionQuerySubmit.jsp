<%
//程序名称：LABranchGroupQuerySubmit.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.wagecal.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>

<%
  //输出参数
  CErrors tError = null;
  String FlagStr = "Fail";
  String Content = "";
  LABranchGroupSchema tLABranchGroupSchema   = new LABranchGroupSchema();

  LABranchGroupUI tLABranchGroupQueryUI   = new LABranchGroupUI();
  //读取Session中的全局类
	GlobalInput tG = new GlobalInput();

	tG.Operator = "Admin";
	tG.ComCode  = "001";
  session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");

    tLABranchGroupSchema.setBranchCode(request.getParameter("BranchCode"));
    tLABranchGroupSchema.setBranchName(request.getParameter("BranchName"));
    tLABranchGroupSchema.setManageCom(request.getParameter("ManageCom"));
    tLABranchGroupSchema.setUpBranch(request.getParameter("UpBranch"));
    tLABranchGroupSchema.setBranchType(request.getParameter("BranchType"));
    tLABranchGroupSchema.setBranchAttr(request.getParameter("BranchAttr"));
    tLABranchGroupSchema.setBranchLevel(request.getParameter("BranchLevel"));
    tLABranchGroupSchema.setBranchManager(request.getParameter("BranchManager"));
    tLABranchGroupSchema.setBranchAddressCode(request.getParameter("BranchAddressCode"));
    tLABranchGroupSchema.setBranchAddress(request.getParameter("BranchAddress"));
    tLABranchGroupSchema.setBranchPhone(request.getParameter("BranchPhone"));
    tLABranchGroupSchema.setBranchFax(request.getParameter("BranchFax"));
    tLABranchGroupSchema.setBranchZipcode(request.getParameter("BranchZipcode"));
    tLABranchGroupSchema.setFoundDate(request.getParameter("FoundDate"));
    tLABranchGroupSchema.setEndData(request.getParameter("EndData"));
    tLABranchGroupSchema.setEndFlag(request.getParameter("EndFlag"));
    tLABranchGroupSchema.setCertifyFlag(request.getParameter("CertifyFlag"));
    tLABranchGroupSchema.setFieldFlag(request.getParameter("FieldFlag"));
    tLABranchGroupSchema.setState(request.getParameter("State"));



  // 准备传输数据 VData
  VData tVData = new VData();

	tVData.addElement(tLABranchGroupSchema);
	tVData.add(tG);

  FlagStr="";
  // 数据传输
	if (!tLABranchGroupQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " +  tError.getFirstError();
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tLABranchGroupQueryUI.getResult();
		
		// 显示
		LABranchGroupSet mLABranchGroupSet = new LABranchGroupSet();
		mLABranchGroupSet.set((LABranchGroupSet)tVData.getObjectByObjectName("LABranchGroupSet",0));
		int n = mLABranchGroupSet.size();
		LABranchGroupSchema mLABranchGroupSchema;
		String recordString;
		for (int i = 1; i <= n; i++)
		{
		        recordString = "";
		  	mLABranchGroupSchema = mLABranchGroupSet.get(i);
		  	recordString=recordString+mLABranchGroupSchema.getBranchCode().trim()+"|";
		  	recordString=recordString+mLABranchGroupSchema.getBranchName().trim()+"|";
		  	recordString=recordString+mLABranchGroupSchema.getManageCom().trim()+"|";
                        if ((mLABranchGroupSchema.getUpBranch() != null)&&(!mLABranchGroupSchema.getUpBranch().equals("")))
                          recordString=recordString+mLABranchGroupSchema.getUpBranch().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchAttr() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchAttr().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchType() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchType().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchLevel() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchLevel().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchManager() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchManager().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchAddressCode() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchAddressCode().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchAddress() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchAddress().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchPhone() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchPhone().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchFax() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchFax().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getBranchZipcode() != null)
                          recordString=recordString+mLABranchGroupSchema.getBranchZipcode().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getFoundDate() != null)
                          recordString=recordString+mLABranchGroupSchema.getFoundDate().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getEndData() != null)
                          recordString=recordString+mLABranchGroupSchema.getEndData().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getEndFlag() != null)
                          recordString=recordString+mLABranchGroupSchema.getEndFlag().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getCertifyFlag() != null)
                          recordString=recordString+mLABranchGroupSchema.getCertifyFlag().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getFieldFlag() != null)
                          recordString=recordString+mLABranchGroupSchema.getFieldFlag().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
                        if (mLABranchGroupSchema.getState() != null)
                          recordString=recordString+mLABranchGroupSchema.getState().trim()+"|";
                        else
                          recordString=recordString+"null"+"|";
		   	%>
		   	<script language="javascript">
        parent.fraInterface.BranchGroupGrid.addOne("BranchGroupGrid");
parent.fraInterface.BranchGroupGrid.setRowColData(<%=i-1%>,1,"<%=mLABranchGroupSchema.getBranchCode()%>");
parent.fraInterface.BranchGroupGrid.setRowColData(<%=i-1%>,2,"<%=mLABranchGroupSchema.getBranchName()%>");
parent.fraInterface.BranchGroupGrid.setRowColData(<%=i-1%>,3,"<%=mLABranchGroupSchema.getManageCom()%>");
parent.fraInterface.BranchGroupGrid.setRowColData(<%=i-1%>,4,"<%=mLABranchGroupSchema.getUpBranch()%>");

        var innerHTML = "";
        var innerHTMLOld = "";
        innerHTML = innerHTML +"<tr>";
        innerHTML = innerHTML +"<input type=hidden name=LAGroupInformation<%=i-1%> value='<%=recordString%>'> ";
        innerHTML = innerHTML +"</tr>";

        try
         {
          //隐藏字段，用于存放信息表的数据（字符串）
           innerHTMLOld = parent.fraInterface.fm.all('spanCode').innerHTML;
           parent.fraInterface.fm.all('spanCode').innerHTML = innerHTMLOld+innerHTML;
         }
        catch(ex)
        {
          alert(ex);
        }
			</script>
			<%
		} // end of for
	} // end of if
  
  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (!FlagStr.equals("Fail"))
  {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
  }

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

