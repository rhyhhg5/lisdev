<%@include file="../common/jsp/AgentCheck.jsp"%>
<%
//程序名称：LAAllAssessQry.jsp
//程序功能：
//创建日期：2003-10-19
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/verifyInput.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAAllAssessQry.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAAllAssessInit.jsp"%>
  <title></title>
</head>
<body  onload="initForm();" >
 <form method=post name=fm target="fraSubmit">
  <table>
    <tr >
    <td >
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divQryCond);">
    </IMG>
      <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
  </table>
  <div id="divQryCond" style="display:''">  
  <table  class= common>
  	<tr class=common> 
      <td class=title>代理人编码</td>
      <TD class=input><Input class=readonly readonly=true name=Agentcode ></TD>      
      <td class=title> 计算起期 </td>
      <TD class=input><Input class= "coolDatePicker" dateFormat="short" name=StartDate verify="计算起期|DATE&NOTNULL" ></TD>  
    </tr>   
    <tr  class= common>           
      <td class=title>计算止期</td>
      <TD class=Input><Input class= "coolDatePicker" dateFormat="short" name=EndDate verify="计算止期|DATE&NOTNULL" ></TD>             
      <td class=input colspan=2><INPUT VALUE="查询" TYPE=button class=common onclick="easyQueryClick();"></td>
    </tr>     
  </table>
  </div>
          
  <table>
    <tr>
     <td class=common>
		 <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCommisionGrid);">
     </td>
     <td class=titleImg>查询结果</td>
   </tr>
  </table>
  
  
  <Div  id= "divCommisionGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanCommisionGrid" >
  					</span> 
  			  	</td>
  			</tr>
  		
    	</table>
    	<table class=common>
    	<tr class=common>
  			<td class=title>保费合计</td>
            <TD class=input><Input class= "readonly" readonly name=TransMoneySum></TD> 
            <td class=title>FYC合计</td>
            <TD class=input><Input class= "readonly" readonly name=FYCSum></TD>
        </tr>
       </table>
       <tr>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 	
      </tr>      
  	</div>
  	
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
    
  </form>
</body>
</html>
