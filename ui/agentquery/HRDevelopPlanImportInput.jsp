<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<html>
<%
	String tFlag = "";
  	GlobalInput tGI = new GlobalInput();
	tGI=(GlobalInput)session.getValue("GI");
	tFlag = request.getParameter("type"); 
%>
<script>	         
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var type = "<%=tFlag%>";
</script>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <title>个险三级机构销售人力发展方案类别报表手工导入系统</title>
  <SCRIPT src="HRDevelopPlanImportInput.js"></SCRIPT>
  <%@include file="HRDevelopPlanImportInit.jsp"%>
 </head>
 <body onload="initElementtype();initForm();">
 	<form action="./HRDevelopPlanImportSave.jsp" method="post" name="fm" target="fraSubmit">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divHR1);">
				</td>
				<td class= titleImg>查询条件</td>
			</tr>
		</table>
		<div id= "divHR1" style= "display: ''">
			<table class=common >
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input>
						<input class=codeNo name=ManageCom verify="管理机构|code:comcode&NOTNULL" ondblclick="return showCodeList('comcode123',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode123',[this,ManageComName],[0,1],null,null,null,1);" ><input class=codename name=ManageComName readonly=true elementtype=nacessary >
					</td>
					<td class=title>类别选择(A/B)</td>
					<td class=input>
						<input class=code name=TypeChoose  CodeData="0|^0|A^1|B" ondblclick="return showCodeListEx('TypeChoose',[this],[1]);" readonly=true >
					</td>					
				</tr>
				<tr class=common>
					<td class=title>净增有效人力达成奖档次</td>
					<td class=input>
						<!-- <input class=codeNo name=RewardReach verify="净增有效人力达成奖档次|code:RewardReach" ondblclick="return showCodeList('RewardReach',[this,RewardReachName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('RewardReach',[this,RewardReachName],[0,1],null,null,null,1);"><input class=codename name=RewardReachName readonly=true> -->
						<input class=codeNo name=RewardReach CodeData="0|^1|一档^2|二档^3|三档^4|四档^5|五档^6|六档" ondblclick="return showCodeListEx('RewardReach',[this,RewardReachName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('RewardReach',[this,RewardReachName],[0,1],null,null,null,1);"><input class=codename name=RewardReachName readonly=true>
					</td>
				</tr>
			</table>
		</div>
		<INPUT type=button value=" 查   询  " class="cssButton" onclick="easyQueryClick();">
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divResult1);">
				</td>
				<td class= titleImg>查询结果</td>
			</tr>
		</table>
		<div id="divResult1" style="display:''">
			<table class= common>
				<tr class= common>
					<td text-align:left colSpan=1>
						<span id="spanHRPlanGrid" ></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="turnPage.firstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="turnPage.previousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="turnPage.nextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="turnPage.lastPage();">
			</table>
		</div>
		<input type=hidden id="fmtransact" name="fmtransact">
		<input class= common type=hidden name=tFlag value="<%=tFlag%>">
        <Input class= common type=hidden name=Operator >
 	</form>
 	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
 </body>
 
 </html>
