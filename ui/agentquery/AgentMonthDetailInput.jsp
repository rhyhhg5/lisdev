<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：AgentMonthDetailInput.jsp
//程序功能：
//创建日期：2003-4-8 
//创建人  ：lh
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%
	String tCommisionSN = "";
	try
	{
		tCommisionSN = request.getParameter("CommisionSN");
	}
	catch( Exception e )
	{
		tCommisionSN = "";
	}
%>
<head >
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <!--SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT-->
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="AgentMonthDetailInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="AgentMonthDetailInit.jsp"%>
  <title>个人日业务信息</title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
    <INPUT VALUE="返回" TYPE=button onclick="returnParent();">
    <table>
      <tr class=common>
        <td class=common>
          <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgentMonth1);">
          <td class=titleImg>
            个人日业务信息
          </td>
        </td>
      </tr> 
    </table>
   
   <Div  id= "divLAAgentMonth1" style= "display: ''">      
    <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          保单号码 
        </TD>
        <TD  class= input> 
          <Input name=PolNo class= 'readonly' readonly >
        </TD>        
        <TD  class= title>
          交易号
        </TD>
        <TD  class= input>
          <Input name=ReceiptNo class= 'readonly' readonly >
        </TD>        
      </TR>
      
      <TR  class= common> 
        <TD  class= title>
          险种编码 
        </TD>
        <TD  class= input> 
          <Input name=RiskCode class= 'readonly' readonly > 
        </TD>
        <TD  class= title>
          交费方式 
        </TD>
        <TD  class= input>
          <Input name=PayMode class= 'readonly' readonly >
        </TD>        
      </TR>
      
      <TR  class= common> 
        <TD  class= title>
          交易金额 
        </TD>
        <TD  class= input> 
          <Input name=TransMoney class= 'readonly' readonly > 
        </TD>
        <TD  class= title>
          第几次交费
        </TD>
        <TD  class= input>
          <Input name=PayCount class= 'readonly' readonly > 
        </TD>        
      </TR>
      
      <TR  class= common> 
        <TD  class= title>
          交费年度 
        </TD>
        <TD  class= input> 
          <Input name=PayYear class= 'readonly' readonly > 
        </TD>        
        <TD  class= title> 
          交易交费日期
        </TD>
        <TD  class= input> 
          <Input name=TpayDate class= 'readonly' readonly > 
        </TD>                
      </TR>
      
      <TR class=common>           
        <TD  class= title> 
          生成扎账信息日期
        </TD>
        <TD  class= input> 
          <Input name=CommDate class= 'readonly' readonly > 
        </TD>        
        <TD  class= title>
          交易确认日期 
        </TD>
        <TD  class= input> 
          <Input name=TconfDate class= 'readonly' readonly > 
        </TD>         
      </TR>
    </table>        
   </Div>	
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>    
 </form>
</body>
</html>

        