<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAChargeSummaryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<script> 
   var BranchType=<%=BranchType%>;
   var BranchType2=<%=BranchType2%>;
   var msql='1 and code <> #01# ';
</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAChargeSummaryInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAChargeSummaryInit.jsp"%>
  <title>中介机构团险业务手续费金额汇总报表 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LAChargeSummaryInputSave.jsp" method=post name=fm target="f1print">
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divChargeSummary);"></IMG></td>
        <td class=titleImg> 查询条件 </td>
      </tr>
    </table>
    <Div  id= "divChargeSummary" style= "display: ''">
    <table  class= common>
    	 <tr  class= common>
        <td  class= title> 管理机构 </td>
        <td  class= input><Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
         </td> 
            <!--td  class= title> 代理机构类别 </td>
        <td  class= input> <input name=ACType class=codeno CodeData="0|^1|专业代理|^2|兼业代理|^3|其他" ondblClick="showCodeListEx('ACType',[this,ACTypeName],[0,1])" onkeyup="showCodeListKeyEx('ACType',[this,ACTypeName],[0,1])" ><Input class=codename name=ACTypeName readOnly > </td--> 
      	<TD class = title>
             中介机构
          </TD>
          <TD  class= input>          	
            <Input class='codeno' name=AgentCom ondblclick="return getAgentComName(this,AgentComName);" onkeyup="return getAgentComName(this,AgentComName);"  ><Input class=codename name=AgentComName readOnly >
          </TD>
      </tr>
      <TR  class = common>
           <TD  class = title>
           财务结算起期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="财务结算起期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            财务结算止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="财务结算止期|NUM&NOTNULL" elementtype=nacessary >  
          </TD>
          
          <tr  class= common>
	      <td  class= title> 中介机构类型 </td>  
        <td  class= input> <input class="codeno" name="ACType" 
        	   ondblclick="return showCodeList('ACType',[this,ACTypeName],[0,1],null,msql,1);"
        	   onkeyup="return showCodeListKey('ACType',[this,ACTypeName],[0,1],null,msql,1);"
        	   ><Input class=codename name=ACTypeName readOnly>
        </td>
        <td  class= title>中介机构状态</td>
          <TD  class= input> <Input class= "codeno" name=EndFlag CodeData="0|^Y|无效|^N|有效|" ondblclick="return showCodeListEx('EndFlag',[this,EndFlagName],[0,1]);" onkeyup="return showCodeListKeyEx('EndFlag',[this,EndFlagName],[0,1]);" onchange="" ><input class=codename name=EndFlagName  readonly=true ></TD>
      </tr>
          
        </TR>
   </table>
   <BR>
    </Div>
    <div style="display: none">
    <TD  class= title>
            网点信息
        </TD>
        <TD  class= input><Input class='common' name=BranchType ></TD>
        <TD  class= input><Input class='common' name=BranchType2 ></TD>
    </div>
    <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
    <INPUT VALUE="下  载"  class = cssButton TYPE=button onclick="ListExecl();">					
   <BR>
   <BR>
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBankLicenseGrid);"></td>
    	<td class= titleImg> 查询结果 </td>
      </tr>
    </table>
    <Div  id= "divSpanChargeSummaryGrid" style= "display: ''">
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  	    <span id="spanChargeSummaryGrid" >
  	    </span> 
  	  </td>
  	</tr>
      </table>
      <input type=hidden class=Common name=querySql > 
      <input type=hidden class=Common name=querySqlTitle >
      <input type=hidden class=Common name=Title > 
      <INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage.lastPage();"> 					
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
