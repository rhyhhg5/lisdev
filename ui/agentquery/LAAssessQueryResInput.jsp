 <%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  String AgentSeries=request.getParameter("AgentSeries");
%>
<script>
   var msql=" 1 and branchtype=#"+'<%=BranchType%>'+"# and branchtype2=#"+'<%=BranchType2%>'+"#";
</script>
<%@page contentType="text/html;charset=GBK" %>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="LAAssessQueryResInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LAAssessQueryResInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <title>考核调整 </title>   
</head>
<%
  String tTitleAgent="";
  if("1".equals(BranchType))
  {
    tTitleAgent = "营销员";
  }
%>
<body  onload="initForm();initElementtype();" >
  <form action="LAAssessQueryResSave.jsp" method=post name=fm target="f1print">

    <table class= common border=0 width=100%>
    	<tr>
	  <td class= titleImg align= center>请输入考核信息的查询条件：</td>
	</tr>
	</table>
    <table  class= common align=center>
      <TR  class= common>
          <TD  class= title>
            管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL&len>7"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,8,'char(length(trim(comcode)))',1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
          </TD>
          
          <TD  class= title>
            考核年月
          </TD>
          <TD  class= input>
            <Input class=common name=IndexCalNo verify="考核年月|notnull&len<=6&int" elementtype=nacessary >
            <font color="red">  'YYYYMM'</font>
          </TD>
      </TR>
      <TR class=common>
        <TD class=title>
          销售单位代码
        </TD>
        <TD class=title>
          <Input class=common name=BranchAttr>
        </TD>
        <TD class=title>
          晋降类型
        </TD>
        <TD class=title>
           <Input name=AssessType class="codeno"  
          CodeData="0|3^01|降级^02|维持^03|晋升 " 
          ondblclick="showCodeListEx('AssessType',[this,AssessTypeName],[0,1]);" 
          onkeyup="showCodeListKeyEx('AssessType',[this,AssessTypeName],[0,1]);" 
          ><Input name=AssessTypeName class="codename"  readonly> 
        </TD>
      </TR>
      <TR class=common>
        <TD class=title>
          代理人编码
        </TD>
        <TD class=title>
          <Input class=common name=AgentCode>
        </TD>
        <TD class=title>
          代理人姓名
        </TD>
        <TD class=title>
          <Input class='common' name=AgentName >
        </TD>
      </TR>
    </table>
    <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class=cssbutton >
    <INPUT VALUE="打  印" TYPE=button   class=cssbutton onclick="submitForm();">  

    <input type=hidden name=BranchType value='<%=BranchType%>'>
    <input type=hidden name=BranchType2 value='<%=BranchType2%>'>
    <input type=hidden name=AgentSeries value='<%=AgentSeries%>'>
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAssess1);">
    		</td>
    		<td class= titleImg>
    			 考核信息
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLAAssess1" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanEvaluateGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="getFirstPage();" class=cssbutton > 
      <INPUT VALUE="上一页" TYPE=button onclick="getPreviousPage();" class=cssbutton > 					
      <INPUT VALUE="下一页" TYPE=button onclick="getNextPage();" class=cssbutton > 
      <INPUT VALUE="尾  页" TYPE=button onclick="getLastPage();" class=cssbutton > 
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html> 
