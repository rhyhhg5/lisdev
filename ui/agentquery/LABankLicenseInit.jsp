<%
//程序名称：LAComQueryInit.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                           
   fm.all('AgentCom').value = '';
   fm.all('ManageCom').value = '';
   fm.all('Name').value = '';
   fm.all('BusiLicenseCode').value = '';
   fm.all('BankType').value = '';
   fm.all('SellFlag').value = '';
   fm.all('StartDate').value = '';
   fm.all('EndDate').value = '';
   fm.all('BranchType').value = '<%=BranchType%>';
   fm.all('BranchType2').value = '<%=BranchType2%>';
   
  }
  catch(ex)
  {
    alert("在LABankLicenseInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在LABankLicenseInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    //initSelBox();   
    initBankLicenseGrid(); 
  }
  catch(re)
  {
    alert("LABankLicenseInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化ComGrid
 ************************************************************
 */
function initBankLicenseGrid()
{                               
  var iArray = new Array();
  try
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";         //列名
    iArray[0][2]=100;         //列名
    iArray[0][3]=0;         //列名

    iArray[1]=new Array();
    iArray[1][0]="管理机构编码";         //列名
    iArray[1][1]="100px";         //宽度
    iArray[1][2]=100;         //最大长度
    iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[2]=new Array();
    iArray[2][0]="管理机构名称";         //列名
    iArray[2][1]="100px";         //宽度
    iArray[2][2]=100;         //最大长度
    iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[3]=new Array();
    iArray[3][0]="代理机构代码";         //列名
    iArray[3][1]="100px";         //宽度
    iArray[3][2]=100;         //最大长度
    iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[4]=new Array();
    iArray[4][0]="代理机构名称";         //列名
    iArray[4][1]="100px";         //宽度
    iArray[4][2]=100;         //最大长度
    iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

    iArray[5]=new Array();
    iArray[5][0]="代理机构类型";         //列名
    iArray[5][1]="100px";         //宽度
    iArray[5][2]=100;         //最大长度
    iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[6]=new Array();
    iArray[6][0]="上级机构编码";         //列名
    iArray[6][1]="100px";         //宽度
    iArray[6][2]=100;         //最大长度
    iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[7]=new Array();
    iArray[7][0]="级别";         //列名
    iArray[7][1]="100px";         //宽度
    iArray[7][2]=100;         //最大长度
    iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[8]=new Array();
    iArray[8][0]="停业属性";         //列名
    iArray[8][1]="100px";         //宽度
    iArray[8][2]=100;         //最大长度
    iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[9]=new Array();
    iArray[9][0]="停业日期";         //列名
    iArray[9][1]="100px";         //宽度
    iArray[9][2]=100;         //最大长度
    iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[10]=new Array();
    iArray[10][0]="许可证起始日期";         //列名
    iArray[10][1]="100px";         //宽度
    iArray[10][2]=100;         //最大长度
    iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[11]=new Array();
    iArray[11][0]="许可证到期日期";         //列名
    iArray[11][1]="100px";         //宽度
    iArray[11][2]=100;         //最大长度
    iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[12]=new Array();
    iArray[12][0]="销售资格";         //列名
    iArray[12][1]="100px";         //宽度
    iArray[12][2]=100;         //最大长度
    iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
    
    iArray[13]=new Array();
    iArray[13][0]="保险兼业代理资格证号";         //列名
    iArray[13][1]="130px";         //宽度
    iArray[13][2]=100;         //最大长度
    iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
 
    BankLicenseGrid = new MulLineEnter( "fm" , "BankLicenseGrid" ); 

    //这些属性必须在loadMulLine前
    BankLicenseGrid.mulLineCount = 0;   
    BankLicenseGrid.displayTitle = 1;
    BankLicenseGrid.canSel=1;
    BankLicenseGrid.hiddenPlus = 1;
    BankLicenseGrid.hiddenSubtraction = 1;
    BankLicenseGrid.loadMulLine(iArray);  
  }
  catch(ex)
  {
        alert("初始化BankLicenseGrid时出错："+ ex);
  }
}

</script>