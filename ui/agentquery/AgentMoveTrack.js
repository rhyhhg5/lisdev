//               该文件中包含客户端需要处理的函数和事件
//var strRT=""; 
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  if (!beforeSubmit())
    return false;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug);
  //initLAAssessGrid();
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    
    //initLAAssessGrid();
    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在AgentMoveTrack.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
	if (!verifyInput()) 
    return false;  
  //添加操作	
  
  MoveTrackGrid.clearData("MoveTrackGrid");
        
  var tAgentCode = trim(fm.all('AgentCode').value);
  var tName = trim(fm.all('Name').value);
  if ((tAgentCode ==null || tAgentCode == '')&&
      (tName == null || tName == ''))
  {
    alert("请输入代理人代码或名称！");
    return false;
  }  
  //校验该代理人是否存在
  var tSql = "select agentcode from latree where 1=1 "
            +"and agentcode in (select distinct AgentCode from laagent where 1=1"
            +getWherePart('Name')+")"
            +getWherePart('AgentCode')
            +getWherePart('ManageCom');
  var queryResult = easyQueryVer3(tSql,1,1,1);
  if (!queryResult)
  {  	
     tSql = "select agentcode from latreeb where 1=1"
            +"and agentcode in (select distinct AgentCode from laagentb where 1=1"
            +getWherePart('Name')+")"
            +getWherePart('AgentCode')
            +getWherePart('ManageCom');
     queryResult = easyQueryVer3(tSql,1,1,1);
     if (!queryResult)
     {
       alert("不存在该代理人！");
       fm.all('TableFlag').value = '-1';
       return false;
     }
     fm.all('TableFlag').value = '0';
  }else
    fm.all('TableFlag').value = '1';
  return true;
}           

//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  alert("update click");
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  alert("delete click");
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else 
  {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


/*
function afterCodeSelect(cCodeName,Field)
{
   var tValue = Field.value;
   try	
   {
     //alert(cCodeName);
     if( cCodeName == "AgentGrade" )	
     {
     	if (verifyInput() == false) 
     	{     		
          LAAssessGrid.hiddenPlus=1;
          LAAssessGrid.hiddenSubtraction=1;
          LAAssessGrid.muLineCount = 0;
          fm.all('AgentGrade').value = "";
     	  return false;
     	}
     	//alert("value:"+tValue);
     	if (tValue!=null && tValue != "")
        {
           //为下拉框查出结果集
           if (!easyQuery())
           {
             return false;
           }
        }else
        {        	
           strRT=null;
           initLAAssessGrid();
           alert("null");
        }
     }else if (cCodeName == 'station')
     {
     	fm.all('AgentGrade').value = "";
        initLAAssessGrid();
     	strRT=null;
     }
   }
   catch( ex ) 
   {
     alert('代理人职级选择出错!');
   }
   return true;
}

function changeYearMonth()
{
  fm.all('AgentGrade').value = "";
  strRT=null;
  initLAAssessGrid();
  return true; 
}*/