<%
//程序名称：BankQueryInit.jsp
//程序功能：
//创建日期：2003-10-15 11:48:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
 
<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
var turnPage = new turnPageClass(); 


function initBankGrid()
{

  try
  {
    var iArray = new Array();

    iArray[0]=new Array();
    iArray[0][0]="序号";         //列名
    iArray[0][1]="30px";        //列宽
    iArray[0][2]=100;            //列最大值
    iArray[0][3]=0;              //是否允许输入,1表示允许，0表示不允许

    iArray[1]=new Array();
    iArray[1][0]="银行网点代码"; //列名
    iArray[1][1]="160px";        //列宽
    iArray[1][2]=100;            //列最大值
    iArray[1][3]=0;              //是否允许输入,1表示允许，0表示不允许


    iArray[2]=new Array();
    iArray[2][0]="银行网点名称"; //列名
    iArray[2][1]="260px";        //列宽
    iArray[2][2]=100;            //列最大值
    iArray[2][3]=0;              //是否允许输入,1表示允许,0表示不允许

	iArray[3]=new Array();
    iArray[3][0]="网点地址"; //列名
    iArray[3][1]="260px";        //列宽
    iArray[3][2]=100;            //列最大值
    iArray[3][3]=0;              //是否允许输入,1表示允许,0表示不允许    
    
    iArray[4]=new Array();
    iArray[4][0]="网点电话"; //列名
    iArray[4][1]="60px";        //列宽
    iArray[4][2]=100;            //列最大值
    iArray[4][3]=0;              //是否允许输入,1表示允许,0表示不允许                              
                              
	iArray[5]=new Array();    
    iArray[5][0]="网点邮编"; //列名
    iArray[5][1]="60px";        //列宽
    iArray[5][2]=100;            //列最大值
    iArray[5][3]=0;              //是否允许输入,1表示允许,0表示不允许

 	iArray[6]=new Array();
    iArray[6][0]="网点传真"; //列名
    iArray[6][1]="60px";        //列宽
    iArray[6][2]=100;            //列最大值
    iArray[6][3]=0;              //是否允许输入,1表示允许,0表示不允许

	iArray[7]=new Array();
    iArray[7][0]="网点EMail"; //列名
    iArray[7][1]="100px";        //列宽
    iArray[7][2]=100;            //列最大值
    iArray[7][3]=0;              //是否允许输入,1表示允许,0表示不允许
    
    iArray[8]=new Array();
    iArray[8][0]="网点联系人"; //列名
    iArray[8][1]="80px";        //列宽
    iArray[8][2]=100;            //列最大值
    iArray[8][3]=0;              //是否允许输入,1表示允许,0表示不允许

	iArray[9]=new Array();
    iArray[9][0]="网点网址"; //列名
    iArray[9][1]="100px";        //列宽
    iArray[9][2]=100;            //列最大值
    iArray[9][3]=0;              //是否允许输入,1表示允许,0表示不允许

    BankGrid = new MulLineEnter( "fm" , "BankGrid" );
    
    BankGrid.mulLineCount = 0;
    BankGrid.displayTitle = 1;
    BankGrid.hiddenPlus = 1;
    BankGrid.hiddenSubtraction = 1;
    BankGrid.loadMulLine(iArray);
  }
  catch(ex)
  {
    alert("在BankQueryInit.jsp-->InitMulInp函数中发生异常:初始化界面错误!");
  }
}
function initForm()
{
	
    initBankGrid();
    initData();
  try
  {
    
  }
  catch(re)
  {
    
    alert("在BankQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initData()
{ 
	getData(); 
}
</script>