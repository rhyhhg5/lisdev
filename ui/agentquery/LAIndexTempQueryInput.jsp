<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAIndexTempQueryInput.jsp
//程序功能：
//创建日期：2002-3-19
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=gb2312" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAIndexTempQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAIndexTempQueryInit.jsp"%>
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
  <title></title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAIndexTemp1);">
    </IMG>
      <td class=titleImg>
      打印条件
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divLAIndexTemp1" style= "display: ''">
    
  <table class= common>
    <tr class= common>      
      <td class= title>
        管理机构
      </td>
      <TD class= input>
          <Input class="code" name=ManageCom verify = "管理机构|notnull&code:ComCode"
                                             ondblclick="return showCodeList('comcode',[this],null,null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this],null,null,null,null,1);"> 
      </TD>
    </tr>
    
    <tr class= common>      
      <TD class= title>
        代理人职级
      </TD>
      <TD class= input>
          <Input name=AgentGrade class="code"  verify="代理人职级|notnull&code:AgentGrade" 
                                               ondblclick="return showCodeList('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" 
                                               onkeyup="return showCodeListKey('AgentGrade',[this],[0,1],null,'<%=tSqlAgentGrade%>','1');" > 
      </TD>   
      <TD class= title>
        代理人代码
      </TD>
      <TD class= input>
          <Input name=AgentCode class=common > 
      </TD>
    </tr>    
    
    <tr class= common>      
      <TD class= title>
        计算起期
      </TD>
      <TD class= input>
          <Input name=StartDate class='coolDatePicker' dateFormat= 'short' verify="计算起期|notnull&Date"> 
      </TD>   
      <TD class= title>
        计算止期
      </TD>
      <TD class= input>
          <Input name=EndDate class='coolDatePicker' dateFormat= 'short' verify="计算止期|notnull&Date"> 
      </TD>
    </tr>
  </table>
          <INPUT VALUE="预警明细打印" TYPE=button onclick="DetailPrint();">
          <Input type=hidden name=BranchType > 
	     
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
