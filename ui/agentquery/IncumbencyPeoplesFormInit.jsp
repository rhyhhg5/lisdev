<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：IncumbencyPeoplesFormInit.jsp
//程序功能：
//创建日期：2015-02-04
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script src="../common/javascript/Common.js"></script>
<%
	PubFun tpubFun = new PubFun();
	String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
function initInpBox(){
	try{
		fm.all('ManageCom').value = <%=strManageCom%>;
	    if(fm.all('ManageCom').value==86){
	    	fm.all('ManageCom').readOnly=false;
	    } else{
	    	fm.all('ManageCom').readOnly=true;
	    }
	    if(fm.all('ManageCom').value!=null){
	    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
	        //显示代码选择中文
	        if (arrResult != null){
	        	fm.all('ManageComName').value=arrResult[0][0];
	        } 
	    }
	}catch(ex){
		alert("在IncumbencyPeoplesFormInit.jsp-->IninInpBox函数中发生异常 ：初始化界面错误！");
	}
}
function initForm(){
	try{
		initElementtype();
		initInpBox();
		initIncumbencyGrid();
		showAllCodeName();
	}catch(re){
		alert("IncumbencyPeoplesFormInit.jsp-->InitForm函数中发生异常:初始化界面错误");
	}
}
function initIncumbencyGrid(){
	var iArray = new Array();
	try{
		iArray[0] = new Array();
		iArray[0][0] = "序号";
		iArray[0][1] = "40px";
		iArray[0][2] = 10;
		iArray[0][3] = 0;
		
		iArray[1] = new Array();
		iArray[1][0] = "考核年月";
		iArray[1][1] = "60px";
		iArray[1][2] = 60;
		iArray[1][3] = 0;
		
		iArray[2] = new Array();
		iArray[2][0] = "代理人姓名";
		iArray[2][1] = "60px";
		iArray[2][2] = 60;
		iArray[2][3] = 0;
		
		iArray[3] = new Array();
		iArray[3][0] = "代理人编码";
		iArray[3][1] = "60px";
		iArray[3][2] = 60;
		iArray[3][3] = 0;
		
		iArray[4] = new Array();
		iArray[4][0] = "管理机构";
		iArray[4][1] = "60px";
		iArray[4][2] = 60;
		iArray[4][3] = 0;
		
		iArray[5] = new Array();
		iArray[5][0] = "机构名称";
		iArray[5][1] = "60px";
		iArray[5][2] = 60;
		iArray[5][3] = 0;
		
		IncumbencyGrid = new MulLineEnter("fm","IncumbencyGrid");
	     
		IncumbencyGrid.mulLineCount = 10;   
		IncumbencyGrid.displayTitle = 1;
		IncumbencyGrid.canSel = 1;
		IncumbencyGrid.canChk = 0;
		IncumbencyGrid.locked = 1;
		IncumbencyGrid.hiddenSubtraction = 1;
		IncumbencyGrid.hiddenPlus = 1;
		IncumbencyGrid.selBoxEventFuncName ="";
	     
		IncumbencyGrid.loadMulLine(iArray);
	}catch(ex){
		alert(ex);
	}
}
</script>
