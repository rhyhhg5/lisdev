
<!--
*******************************************************
* 程序名称：UsrCheck.jsp
* 程序功能：用户信息校验页面
* 创建日期：2002-11-25
* 更新记录：  更新人    更新日期     更新原因/内容
*******************************************************
-->



<html> 
<head >
  <script>
 var strsql =" 1 and  comcode Like #"+'86'+"%# " ;

</script> 

	<meta http-equiv="Content-Type" content="text/html; charset=GBK">
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
                           
<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {  
    fm.all('ManageCom').value = '';
    fm.all('BranchAttr').value = '';
    fm.all('AgentCode').value = '';
    fm.all('AgentName').value = '';
    fm.all('StartDate').value = '';
    fm.all('EndDate').value = '';
    fm.all('GetDate').value = '';
    fm.all('Level').value = '';
    fm.all('EndDate').value = ''; 
    fm.all('BranchType').value =  '3';
    fm.all('BranchType2').value = '01';     
                           
  }
  catch(ex)
  {
    alert("在BankWageNoInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("在BankWageNoInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
  }
  catch(re)
  {
    alert("WageQueryListInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

                            


</script>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<title>Sinosoft </title>
</head>

<body  onload="initForm();initElementtype();" >
  <form  action="./LABankCommQuerySave.jsp" method=post name=fm target="f1print" >
      <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLAAgent1);">
    		</td>
    		<td class= titleImg>
    			 查询条件
    		</td>
    	</tr>
      </table>
      <table  class= common align='center'>
       <TR  class= common>
       	  <TD  class= title>统计层级</TD>
          <TD  class= input>
            <Input class="codeno" name=Level verify="统计层级|notnull"  	CodeData="0|^0|分公司|^1|中支公司|^2|营业部|^3|业务经理" 
            ondblclick="return showCodeListEx('Level',[this,LevelName],[0,1]);" 
            onkeyup="return showCodeListKeyEx('Level',[this,LevelName],[0,1]);"><input class=codename name=LevelName readonly=true elementtype=nacessary>
          </TD>
       	</TR>	
       <TR  class= common>
          <TD  class= title>
             管理机构
          </TD>
          <TD  class= input>
          <Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL"
            ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,strsql,null,1);" 
            onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,strsql,null,1);"
          ><Input class="codename" name=ManageComName  elementtype=nacessary > 
          </TD> 
         
         
          <TD  class= title>
          销售单位编码 
          </td>
          <td class= input>
          <Input class= 'common'   name=BranchAttr >
          
          </td>
       </TR>         
       <TR  class= common>
        	<TD  class= title>
           业务员代码
           </td>
           <td class= input>
           <Input class= 'common'   name=AgentCode >
           </td>     	
           <TD  class= title>
           业务员姓名
           </td>
           <td class= input>
           <Input class= 'common'   name=AgentName >
           </td>        
        </tr>
        <TR  class = common>
           <TD  class = title>
           签单始期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=StartDate verify="统计始期|Date&NOTNULL" elementtype=nacessary >  
          </TD>
          <TD  class = title>
            签单止期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=EndDate verify="统计止期|Date&NOTNULL" elementtype=nacessary >  
          </TD>
          
        </TR> 
         <TR  class = common>     
         <TD  class = title>
           犹豫期截止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"  name=GetDate verify="犹豫期截止日期|Date&NOTNULL" elementtype=nacessary >  
          </TD>
        　　<TD  class= title>险种号</TD>
          <TD  class= input>
            <Input class="codeno" name=RiskCode  	
            CodeData="0|^320106|成长无忧日常看护个人护理保险|^230701|健康专家个人防癌疾病保险|^331201|健康人生个人护理保险(万能型，B款)|^331301|健康人生个人护理保险(万能型，C款)|^331601|健康人生个人护理保险(万能型，D款)|^331701|金利宝个人护理保险（万能型）|^240501|安心宝个人终身重大疾病保险|^730101|康利人生两全保险（分红型）|^332401|附加康利人生护理保险|^332501|健康人生个人护理保险（万能型，E款）|^531701|附加健康人生个人意外伤害保险（E款）|^332601|健康人生个人护理保险（万能型，F款）|^531801|附加健康人生个人意外伤害保险（F款）|^333001|健康人生个人护理保险（万能型，H款）|^532201|附加健康人生个人意外伤害保险（H款）|^730201|福满人生两全保险（分红型）|^332801|附加福满人生护理保险|^333501|百万安行个人护理保险|^532401|附加百万安行个人意外伤害保险|^532501|附加百万安行个人交通意外伤害保险|^333201|惠享连年个人护理保险（万能型）|^532301|附加惠享连年个人意外伤害保险|^333701|福利双全个人护理保险|^532601|附加福利双全个人意外伤害保险|^231701|康乐人生个人重大疾病保险（A款）|^333801|附加康乐人生个人护理保险(A款)|^232101|北肿防癌管家个人疾病保险（A款）|^334301|附加北肿防癌管家个人护理保险（A款）|^230801|守护天使少儿特定疾病保险|^231601|天使之翼少儿重大疾病保险|^333601|附加天使之翼少儿护理保险|^334001|福惠双全个人护理保险|^532801|附加福惠双全个人意外伤害保险" 
            ondblclick="return showCodeListEx('RiskCode',[this,RiskCodeName],[0,1]);" 
            onkeyup="return showCodeListKeyEx('RiskCode',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true>
          </TD>
        </TR>
        <tr>
         <TD  class= title>
           销售渠道
           </td>
           <TD  class= input>
            <Input class="codeno" name=SaleChnl  verify="销售渠道|notnull" CodeData="0|^0|直销|^1|代理|^2|全部" ondblclick="return showCodeListEx('SaleChnl',[this,SaleChnlName],[0,1]);" 
            onkeyup="return showCodeListKeyEx('SaleChnl',[this,SaleChnlName],[0,1]);"><input class=codename name=SaleChnlName readonly=true elementtype=nacessary>
           </td> 
        </tr>
       </table>
       <table class=common border=0 width=100%>
        <TR class=common>  
           <TD  class=button width="10%" align=left>
           <!--INPUT VALUE="查 询"   TYPE=button   class=cssbutton onclick="easyQueryClick();"--> 
           <INPUT VALUE="下 载"   TYPE=button   class=cssbutton onclick="doDownLoad();"> 
           <font color='red'>&nbsp;&nbsp;（注意：1.保费统计是以签单日期为准且只计算犹豫期过"犹豫期截止日期"的保费; ）</font>
           </TD>
        </TR>
  </table>            
        
  	<Div  id= "LAAgent1" style= "display: none ">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPolGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT CLASS=cssButton VALUE="首页"   TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT CLASS=cssButton VALUE="尾页"   TYPE=button onclick="turnPage.lastPage();">				
  	</div>
      <input type=hidden name=BranchType value=''> 
      <input type=hidden name=BranchType2 value=''>
      <input type=hidden class=Common name=querySql > 
        	
  </form>
 
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  
</body>
<script>
var turnPage = new turnPageClass(); 
var strSQL = "";
function doDownLoad()
{

   if(!check())
    return false ;
    var tManagecom=fm.all('ManageCom').value;
	var tStartDate=fm.all('StartDate').value;
	var tEndDate=fm.all('EndDate').value;
	var tGetDate=fm.all('GetDate').value;
	// 书写SQL语句
	var tRiskCode=fm.all('RiskCode').value;
	if(tRiskCode ==null || tRiskCode==""){
		tRiskCode="'331201','331301','331601','230701','331401','320106','120706','331701','240501','340201','730101','332401',"+
		"'332601','531801','332501','531701','332901','532001','333001','532201','730201','332801','333501','532401','532501',"+
		"'333201','532301','333701','532601','231701','333801','232101','334301','230801','231601','333601','334001','532801'";
	}else if(tRiskCode=="320106"){
		tRiskCode="'320106','120706'";
	}else if(tRiskCode=="230701"){
		tRiskCode="'230701','331401'";
	}else if(tRiskCode=="240501"){
		tRiskCode="'240501','340201'";
	}else if(tRiskCode=="332501"){
		tRiskCode="'332501','531701'";
	}else if(tRiskCode=="332601"){
		tRiskCode="'332601','531801'";
	}else if(tRiskCode=="332901"){
	    tRiskCode="'332901','532001'"
	}else if(tRiskCode=="333201"){
	    tRiskCode="'333201','532301'"
	}else if(tRiskCode=="333501"){
	    tRiskCode="'333501','532401','532501'"
	}else if(tRiskCode=="333701"){
	    tRiskCode="'333701','532601'"
	}else if(tRiskCode=="231701"){
	    tRiskCode="'231701','333801'"
	}else if(tRiskCode=="232101"){
	    tRiskCode="'232101','334301'"
	}else if(tRiskCode=="232101"){
	    tRiskCode="'232101','334301'"
	}else{
		tRiskCode="'"+tRiskCode+"'";
	}
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;
  var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   

	var tlevel=fm.all('Level').value;
	var tSaleChnl = fm.all('SaleChnl').value;
	//分公司
	if(tlevel=='0'){
	strSQL =
  	" select cc ,dd ,xx ,count(distinct ss),"
  	+" decimal(value(sum(case when vv ='331201' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='331301' and ww='00' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='331301' and ww='03' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='331601' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332501' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332601' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332901' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='230701' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='331401' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='320106' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='120706' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='331701' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='240501' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='340201' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy = 5 and zz = 0 then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy = 6 and zz = 0  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy =3  and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy =5  and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and zz = 0 and yy =5 then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and zz = 0 and yy =6 then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and yy =3 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and yy =5 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='333001' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='532201' then uu else 0 end),0),12,2)," 	
  	+" decimal(value(sum(case when vv ='730201' and zz = 0 then uu else 0 end),0),12,2)," // 趸交情况
  	+" decimal(value(sum(case when vv ='730201' and yy =3  and zz = 12  then uu else 0 end),0),12,2)," // 3年期
  	+" decimal(value(sum(case when vv ='730201' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='730201' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='730201' and yy =20  and zz = 12  then uu else 0 end),0),12,2)," // 20年期
  	+" decimal(value(sum(case when vv ='332801' and zz = 0 then uu else 0 end),0),12,2)," // 趸交情况
  	+" decimal(value(sum(case when vv ='332801' and yy =3  and zz = 12  then uu else 0 end),0),12,2)," // 3年期
  	+" decimal(value(sum(case when vv ='332801' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='332801' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='332801' and yy =20  and zz = 12  then uu else 0 end),0),12,2)," // 20年期
  	+" decimal(value(sum(case when vv ='333501' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='333501' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='532401' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='532401' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='532501' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='532501' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='333201' then uu else 0 end),0),12,2),"// add new 惠享连年
  	+" decimal(value(sum(case when vv ='532301' then uu else 0 end),0),12,2),"// add new 惠享连年
  	+" decimal(value(sum(case when vv ='333701' then uu else 0 end),0),12,2),"// add new 福利双全
  	+" decimal(value(sum(case when vv ='532601' then uu else 0 end),0),12,2),"// add new 福利双全
  	+" decimal(value(sum(case when vv ='231701' then uu else 0 end),0),12,2),"// add new 康乐人生个人重大疾病保险（A款）
  	+" decimal(value(sum(case when vv ='333801' then uu else 0 end),0),12,2),"// add new 附加康乐人生个人护理保险（A款）
  	+" decimal(value(sum(case when vv ='232101' then uu else 0 end),0),12,2),"// add new 北肿防癌管家个人疾病保险（A款）
  	+" decimal(value(sum(case when vv ='334301' then uu else 0 end),0),12,2),"// add new 附加北肿防癌管家个人护理保险（A款）
  	+" decimal(value(sum(case when vv ='230801' then uu else 0 end),0),12,2),"// add new 守护天使少儿特定疾病保险
  	+" decimal(value(sum(case when vv ='231601' then uu else 0 end),0),12,2),"// add new 天使之翼少儿重大疾病保险
  	+" decimal(value(sum(case when vv ='333601' then uu else 0 end),0),12,2),"// add new 附加天使之翼少儿护理保险
  	+" decimal(value(sum(case when vv ='334001' then uu else 0 end),0),12,2),"// add new 福惠双全个人护理保险
  	+" decimal(value(sum(case when vv ='532801' then uu else 0 end),0),12,2),"// add new 附加福惠双全个人意外伤害保险    
  	+" decimal(value(sum(uu),0),12,2) ddd " 
    +" from (   "
    +" select substr(a.managecom,1,4) cc,"
    +" (select name from ldcom where comcode=substr(a.managecom,1,4)) dd,"
    +" value((select count(distinct agentcom) from lacomtoagent "
    +" where  agentcom in (select agentcom from lacom where (endflag='N' or endflag is null) "
    +" and substr(managecom,1,4)=substr(a.managecom,1,4) and branchtype=c.branchtype and branchtype2=c.branchtype2 )),0) xx,"
    +" a.agentcom ss,"
    +" a.transmoney uu,"
    +" a.riskcode vv , "
    +" a.transstate ww , " 
    +" a.payintv zz,"  
    +" a.payyears yy"	
   	+" from LACommision a,LAAgent b,latree d,labranchgroup c "
    +" where  a.agentcode=b.agentcode and a.agentcode=d.agentcode"     
    +" and a.agentgroup=c.agentgroup and a.branchtype='3' and a.branchtype2='01' "	
    +" and a.signdate between '"+tStartDate+"' and '"+tEndDate+"'"
    +" and a.customgetpoldate + 10 days <= '"+tGetDate+"'"
    +" and a.managecom like '"+tManagecom+"%'"
    +" and a.payyear=0 and a.renewcount=0 ";
    if(tSaleChnl=='0')
     {
      strSQL +=" and a.agentcom is  null";
     }else if(tSaleChnl=='1')
     {
      strSQL +=" and a.agentcom is not null";
     }else{
     }
   strSQL +=" and a.riskcode in ("+tRiskCode+") "
    + getWherePart("c.branchattr","BranchAttr")
	  + getWherePart("a.agentcode","AgentCode")
	  + getWherePart("b.name","AgentName")
	  +" ) as aaa group by cc,dd,xx "
	  +" order by ddd desc,cc,dd,xx with ur ";
}
//支公司
else if(tlevel=='1')
{
	strSQL =
    "select cc ,dd ,ee,ff,xx ,count(distinct ss),"
    +" decimal(value(sum(case when vv ='331201' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331301' and ww='00' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331301' and ww='03' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331601' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='332501' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332601' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332901' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='230701' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331401' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='320106' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='120706' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='331701' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='240501' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='340201' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and zz = 0 and yy =5  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and zz = 0 and yy =6  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy =3 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy =5 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and zz = 0 and yy =5  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and zz = 0 and yy =6  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and yy =3 and zz = 12 then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and yy =5 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='333001' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='532201' then uu else 0 end),0),12,2)," 
  	+" decimal(value(sum(case when vv ='730201' and zz = 0 then uu else 0 end),0),12,2)," // 趸交情况
  	+" decimal(value(sum(case when vv ='730201' and yy =3  and zz = 12  then uu else 0 end),0),12,2)," // 3年期
  	+" decimal(value(sum(case when vv ='730201' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='730201' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='730201' and yy =20  and zz = 12  then uu else 0 end),0),12,2)," // 20年期
  	+" decimal(value(sum(case when vv ='332801' and zz = 0 then uu else 0 end),0),12,2)," // 趸交情况
  	+" decimal(value(sum(case when vv ='332801' and yy =3  and zz = 12  then uu else 0 end),0),12,2)," // 3年期
  	+" decimal(value(sum(case when vv ='332801' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='332801' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='332801' and yy =20  and zz = 12  then uu else 0 end),0),12,2)," // 20年期
  	+" decimal(value(sum(case when vv ='333501' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='333501' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='532401' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='532401' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='532501' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='532501' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='333201' then uu else 0 end),0),12,2),"// add new 惠享连年
  	+" decimal(value(sum(case when vv ='532301' then uu else 0 end),0),12,2),"// add new 惠享连年
  	+" decimal(value(sum(case when vv ='333701' then uu else 0 end),0),12,2),"// add new 福利双全
  	+" decimal(value(sum(case when vv ='532601' then uu else 0 end),0),12,2),"// add new 福利双全	
  	+" decimal(value(sum(case when vv ='231701' then uu else 0 end),0),12,2),"// add new 康乐人生个人重大疾病保险（A款）
  	+" decimal(value(sum(case when vv ='333801' then uu else 0 end),0),12,2),"// add new 附加康乐人生个人护理保险（A款）
  	+" decimal(value(sum(case when vv ='232101' then uu else 0 end),0),12,2),"// add new 北肿防癌管家个人疾病保险（A款）
  	+" decimal(value(sum(case when vv ='334301' then uu else 0 end),0),12,2),"// add new 附加北肿防癌管家个人护理保险（A款）
  	+" decimal(value(sum(case when vv ='230801' then uu else 0 end),0),12,2),"// add new 守护天使少儿特定疾病保险
  	+" decimal(value(sum(case when vv ='231601' then uu else 0 end),0),12,2),"// add new 天使之翼少儿重大疾病保险
  	+" decimal(value(sum(case when vv ='333601' then uu else 0 end),0),12,2),"// add new 附加天使之翼少儿护理保险
   	+" decimal(value(sum(case when vv ='334001' then uu else 0 end),0),12,2),"// add new 福惠双全个人护理保险
  	+" decimal(value(sum(case when vv ='532801' then uu else 0 end),0),12,2),"// add new 附加福惠双全个人意外伤害保险    
    +" decimal(value(sum(uu),0),12,2) ddd "
    +" from ( "
    +" select substr(a.managecom,1,4) cc,"
    +" (select name from ldcom where comcode=substr(a.managecom,1,4)) dd,"
    +" a.managecom ee,"
    +" (select name from ldcom where comcode=a.managecom) ff,"
    +" value((select count(distinct agentcom) from lacomtoagent "
    +" where  agentcom in (select agentcom from lacom "
    +" where (endflag='N' or endflag is null) and managecom=a.managecom "
    +" and branchtype=c.branchtype and branchtype2=c.branchtype2 )),0) xx,"
    +" a.agentcom ss,"
    +" a.transmoney uu,"
    +" a.riskcode vv , "
    +" a.transstate ww , "  
    +" a.payintv zz,"  
    +" a.payyears yy" 	
   	+" from LACommision a,LAAgent b,latree d,labranchgroup c "
    +" where  a.agentcode=b.agentcode and a.agentcode=d.agentcode"     
    +" and a.agentgroup=c.agentgroup and a.branchtype='3' and a.branchtype2='01' "	
    +" and a.signdate between '"+tStartDate+"' and '"+tEndDate+"'"
    +" and a.customgetpoldate + 10 days <= '"+tGetDate+"'"
    +" and a.managecom like '"+tManagecom+"%'"
    +" and a.payyear=0 and a.renewcount=0 ";
    if(tSaleChnl=='0')
     {
      strSQL +=" and a.agentcom is  null";
     }else if(tSaleChnl=='1')
     {
      strSQL +=" and a.agentcom is not null";
     }else{
     
     }
    strSQL+=" and a.riskcode in ("+tRiskCode+") "
    + getWherePart("c.branchattr","BranchAttr")
	  + getWherePart("a.agentcode","AgentCode")
	  + getWherePart("b.name","AgentName")
	  +" ) as aaa group by cc,dd,ee,ff,xx "
    +" order by ddd desc,cc,dd,ee,ff,xx with ur";
	}
else if(tlevel=='2')
{
	strSQL =
  	"select cc ,dd ,ee,ff,gg,hh,xx ,count(distinct ss),"
    +" decimal(value(sum(case when vv ='331201' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331301' and ww='00' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331301' and ww='03' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331601' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='332501' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332601' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332901' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='230701' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331401' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='320106' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='120706' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='331701' then uu else 0 end),0),12,2),"
    +" decimal(value(sum(case when vv ='240501' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='340201' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and zz = 0 and yy =5  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and zz = 0 and yy =6  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy =3 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy =5 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and zz = 0 and yy = 5  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and zz = 0 and yy = 6 then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and yy =3 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and yy =5 and zz = 12 then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='333001' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='532201' then uu else 0 end),0),12,2)," 
  	+" decimal(value(sum(case when vv ='730201' and zz = 0 then uu else 0 end),0),12,2)," // 趸交情况
  	+" decimal(value(sum(case when vv ='730201' and yy =3  and zz = 12  then uu else 0 end),0),12,2)," // 3年期
  	+" decimal(value(sum(case when vv ='730201' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='730201' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='730201' and yy =20  and zz = 12  then uu else 0 end),0),12,2)," // 20年期
  	+" decimal(value(sum(case when vv ='332801' and zz = 0 then uu else 0 end),0),12,2)," // 趸交情况
  	+" decimal(value(sum(case when vv ='332801' and yy =3  and zz = 12  then uu else 0 end),0),12,2)," // 3年期
  	+" decimal(value(sum(case when vv ='332801' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='332801' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='332801' and yy =20  and zz = 12  then uu else 0 end),0),12,2)," // 20年期
  	+" decimal(value(sum(case when vv ='333501' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='333501' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='532401' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='532401' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='532501' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='532501' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='333201' then uu else 0 end),0),12,2),"// add new 惠享连年
  	+" decimal(value(sum(case when vv ='532301' then uu else 0 end),0),12,2),"// add new 惠享连年
  	+" decimal(value(sum(case when vv ='333701' then uu else 0 end),0),12,2),"// add new 福利双全
  	+" decimal(value(sum(case when vv ='532601' then uu else 0 end),0),12,2),"// add new 福利双全
  	+" decimal(value(sum(case when vv ='231701' then uu else 0 end),0),12,2),"// add new 康乐人生个人重大疾病保险（A款）
  	+" decimal(value(sum(case when vv ='333801' then uu else 0 end),0),12,2),"// add new 附加康乐人生个人护理保险（A款）
  	+" decimal(value(sum(case when vv ='232101' then uu else 0 end),0),12,2),"// add new 北肿防癌管家个人疾病保险（A款）
  	+" decimal(value(sum(case when vv ='334301' then uu else 0 end),0),12,2),"// add new 附加北肿防癌管家个人护理保险（A款）
  	+" decimal(value(sum(case when vv ='230801' then uu else 0 end),0),12,2),"// add new 守护天使少儿特定疾病保险
  	+" decimal(value(sum(case when vv ='231601' then uu else 0 end),0),12,2),"// add new 天使之翼少儿重大疾病保险
  	+" decimal(value(sum(case when vv ='333601' then uu else 0 end),0),12,2),"// add new 附加天使之翼少儿护理保险
    +" decimal(value(sum(case when vv ='334001' then uu else 0 end),0),12,2),"// add new 福惠双全个人护理保险
  	+" decimal(value(sum(case when vv ='532801' then uu else 0 end),0),12,2),"// add new 附加福惠双全个人意外伤害保险    
    +" decimal(value(sum(uu),0),12,2) ddd "
    +" from ( "
    +" select substr(a.managecom,1,4) cc,"
    +" (select name from ldcom where  comcode=substr(a.managecom,1,4)) dd,"
    +" a.managecom ee,"
    +" (select name from ldcom where comcode=a.managecom) ff,"
    +" c.branchattr gg,"
    +" (select name from labranchgroup where agentgroup = c.agentgroup) hh,"
    +" value((select count(distinct agentcom) from lacomtoagent where  "
    +" agentcom in (select agentcom from lacom where (endflag='N' or endflag is null) and managecom=a.managecom  "
    +" and branchtype=c.branchtype and branchtype2=c.branchtype2)"
    +" and agentgroup in (select agentgroup from labranchgroup where branchtype=c.branchtype and branchtype2=c.branchtype2 and "
    +" substr(branchattr,1,12)=substr(c.branchattr,1,12) )),0) xx,"
    +" a.agentcom ss,"
    +" a.transmoney uu,"
    +" a.riskcode vv , "
    +" a.transstate ww ,  " 
    +" a.payintv zz,"  
    +" a.payyears yy"  	
   	+" from LACommision a,LAAgent b,latree d,labranchgroup c "
    +" where  a.agentcode=b.agentcode and a.agentcode=d.agentcode"     
    +" and a.agentgroup=c.agentgroup and a.branchtype='3' and a.branchtype2='01' "	
    +" and a.signdate between '"+tStartDate+"' and '"+tEndDate+"'"
    +" and a.customgetpoldate + 10 days <= '"+tGetDate+"'"
    +" and a.managecom like '"+tManagecom+"%'"
    +" and a.payyear=0 and a.renewcount=0 ";
    if(tSaleChnl=='0')
     {
      strSQL +=" and a.agentcom is  null";
     }else if(tSaleChnl=='1')
     {
      strSQL +=" and a.agentcom is not null";
     }else{
     
     }
   strSQL +=" and a.riskcode in ("+tRiskCode+") "
    + getWherePart("c.branchattr","BranchAttr")
	  + getWherePart("a.agentcode","AgentCode")
	  + getWherePart("b.name","AgentName")
	  +" ) as aaa "
	  +" group by cc,dd,ee,ff,gg,hh,	xx "
    +" order by ddd desc,cc,dd,ee,ff,gg,hh,xx  with ur";
	}
//	//业务经理
else if(tlevel=='3')
{
	strSQL =
  	"select cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,xx,count(distinct ss)"
  	+",decimal(value(sum(case when vv ='331201' then uu else 0 end),0),12,2)"
  	+",decimal(value(sum(case when vv ='331301' and ww='00' then uu else 0 end),0),12,2)"
  	+",decimal(value(sum(case when vv ='331301' and ww='03' then uu else 0 end),0),12,2)"
  	+",decimal(value(sum(case when vv ='331601' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332501' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332601' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332901' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='230701' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='331401' then uu else 0 end),0),12,2)"
  	+", decimal(value(sum(case when vv ='320106' then uu else 0 end),0),12,2)"
  	+" ,decimal(value(sum(case when vv ='120706' then uu else 0 end),0),12,2)"
  	+" ,decimal(value(sum(case when vv ='331701' then uu else 0 end),0),12,2)"
  	+", decimal(value(sum(case when vv ='240501' then uu else 0 end),0),12,2)"
  	+", decimal(value(sum(case when vv ='340201' then uu else 0 end),0),12,2)"
  	+" ,decimal(value(sum(case when vv ='730101' and zz = 0 and yy =5  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and zz = 0 and yy =6  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy =3 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730101' and yy =5 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and zz = 0 and yy =5  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and zz = 0 and yy =6  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and yy =3 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332401' and yy =5 and zz = 12  then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='333001' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='532201' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='730201' and zz = 0 then uu else 0 end),0),12,2)," // 趸交情况
  	+" decimal(value(sum(case when vv ='730201' and yy =3  and zz = 12  then uu else 0 end),0),12,2)," // 3年期
  	+" decimal(value(sum(case when vv ='730201' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='730201' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='730201' and yy =20  and zz = 12  then uu else 0 end),0),12,2)," // 20年期
  	+" decimal(value(sum(case when vv ='332801' and zz = 0 then uu else 0 end),0),12,2)," // 趸交情况
  	+" decimal(value(sum(case when vv ='332801' and yy =3  and zz = 12  then uu else 0 end),0),12,2)," // 3年期
  	+" decimal(value(sum(case when vv ='332801' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='332801' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='332801' and yy =20  and zz = 12  then uu else 0 end),0),12,2)," // 20年期
  	+" decimal(value(sum(case when vv ='333501' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='333501' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='532401' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='532401' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='532501' and yy =5  and zz = 12  then uu else 0 end),0),12,2)," // 5年期
  	+" decimal(value(sum(case when vv ='532501' and yy =10  and zz = 12  then uu else 0 end),0),12,2)," // 10年期
  	+" decimal(value(sum(case when vv ='333201' then uu else 0 end),0),12,2),"// add new 惠享连年
  	+" decimal(value(sum(case when vv ='532301' then uu else 0 end),0),12,2),"// add new 惠享连年
  	+" decimal(value(sum(case when vv ='333701' then uu else 0 end),0),12,2),"// add new 福利双全
  	+" decimal(value(sum(case when vv ='532601' then uu else 0 end),0),12,2),"// add new 福利双全
  	+" decimal(value(sum(case when vv ='231701' then uu else 0 end),0),12,2),"// add new 康乐人生个人重大疾病保险（A款）
  	+" decimal(value(sum(case when vv ='333801' then uu else 0 end),0),12,2),"// add new 附加康乐人生个人护理保险（A款）
  	+" decimal(value(sum(case when vv ='232101' then uu else 0 end),0),12,2),"// add new 北肿防癌管家个人疾病保险（A款）
  	+" decimal(value(sum(case when vv ='334301' then uu else 0 end),0),12,2),"// add new 附加北肿防癌管家个人护理保险（A款）
  	+" decimal(value(sum(case when vv ='230801' then uu else 0 end),0),12,2),"// add new 守护天使少儿特定疾病保险
  	+" decimal(value(sum(case when vv ='231601' then uu else 0 end),0),12,2),"// add new 天使之翼少儿重大疾病保险
  	+" decimal(value(sum(case when vv ='333601' then uu else 0 end),0),12,2),"// add new 附加天使之翼少儿护理保险
  	+" decimal(value(sum(case when vv ='334001' then uu else 0 end),0),12,2),"// add new 福惠双全个人护理保险
  	+" decimal(value(sum(case when vv ='532801' then uu else 0 end),0),12,2),"// add new 附加福惠双全个人意外伤害保险    
  	+" decimal(value(sum(uu),0),12,2) ddd from ("
  	+" select substr(a.managecom,1,4) cc,(select name from ldcom where comcode=substr(a.managecom,1,4)) dd,"
    +" a.managecom ee,(select name from ldcom where comcode=a.managecom) ff,"
    +" c.branchattr gg,c.name hh,b.agentcode ii,b.name jj,(select gradename from laagentgrade where gradecode=d.agentgrade) kk"
    +" ,b.employdate ll,case b.agentstate when '01' then '在职' when '02' then '在职' when '03' then '离职登记' when '04' then '离职登记' when '06' then '离职确认' when '07' then '离职确认' end mm,"
    +" b.outworkdate nn,"
   //  +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='331201' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) oo,"
   //  +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='331301' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) pp,"
   //  +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='331601' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) qq,"
   //  +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='332501' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) pp1,"
  //   +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='332601' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) pp2,"
  //   +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='332901' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) pp3,"
  //   +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='230701' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) rr,"
  //   +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='331701' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) sss,"
  //   +" decimal(value((select standprem from ladiscount where discounttype='05' and riskcode='240501' and managecom=a.managecom and agentgrade=d.agentgrade),0),12,2) tt,"
    +" value((select standprem from LADiscount where branchtype = '3' and branchtype2 = '01' and discounttype = '05' and managecom =d.managecom and agentgrade = d.agentgrade),0) oo,"
    +" value((select count(distinct agentcom) from lacomtoagent where agentcode=a.agentcode and agentcom in (select agentcom from lacom where endflag='N' or endflag is null)),0) xx,"
    +" a.agentcom ss,a.transmoney uu,a.riskcode vv,a.transstate ww, " 
    +" a.payintv zz,"  
    +" a.payyears yy"  	
   	+" from LACommision a,LAAgent b,latree d,labranchgroup c "
    +" where  a.agentcode=b.agentcode and a.agentcode=d.agentcode"     
    +" and b.agentgroup=c.agentgroup and a.branchtype='3' and a.branchtype2='01' "	
    +" and a.signdate between '"+tStartDate+"' and '"+tEndDate+"'"
    +" and a.customgetpoldate + 10 days <= '"+tGetDate+"'"
    +" and a.managecom like '"+tManagecom+"%'"
    +" and a.payyear=0 and a.renewcount=0 ";
    if(tSaleChnl=='0')
     {
      strSQL +=" and a.agentcom is  null";
     }else if(tSaleChnl=='1')
     {
      strSQL +=" and a.agentcom is not null";
     }else{
     
     }
    strSQL+=" and a.riskcode in ("+tRiskCode+") "
    + getWherePart("c.branchattr","BranchAttr")
	  + getWherePart("a.agentcode","AgentCode")
	  + getWherePart("b.name","AgentName")
	  +" ) as aaa group by cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,xx "
	  +" order by ddd desc,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,xx with ur";
	}

	
	fm.querySql.value = strSQL;
	fm.target = "f1print";
	fm.submit();
	showInfo.close();
}
function check()
{
 if (!verifyInput())
 return false;
 var managecom=fm.all('ManageCom').value;
 var level = fm.all('Level').value;
  if((managecom.length==8)&&level=='0')
 {
 	 alert("选择支公司编码时，不能选择统计层级为（0-分公司）！");
   return false;
 }
 return true; 
}


function getAgentCom(cObj,cName)
{
	if (fm.all('ManageCom').value == null || trim(fm.all('ManageCom').value) == '')
  {
  	alert("请先输入管理机构！");
  	return false;
  } 
var strsql =" 1  and managecom  like #" + fm.all('ManageCom').value + "%#  and branchtype=#3#  and branchtype2=#01# " ;
showCodeList('agentcom',[cObj,cName],[0,1],null,strsql,'1',1);
	
	
}
function easyQueryClick()
{	
	
   if(!check())
    return false ;
	// 初始化表格	
	var tManagecom=fm.all('ManageCom').value;
	var tStartDate=fm.all('StartDate').value;
	var tEndDate=fm.all('EndDate').value;
	var tGetDate=fm.all('GetDate').value;
	// 书写SQL语句
	
	var tBranchType=fm.all('BranchType').value;
	var tBranchType2=fm.all('BranchType2').value;

  strSQL =
  	"select cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,pp1,pp2,pp3,pp,qq,rr,xx,count(distinct ss),"
  	+"decimal(value(sum(case when vv ='331201' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='331301' and ww='00' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='331301' and ww='03' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='331601' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332501' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332601' then uu else 0 end),0),12,2),"
  	+" decimal(value(sum(case when vv ='332901' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='230701' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='331401' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='320106' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='120706' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='331701' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='240501' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(case when vv ='340201' then uu else 0 end),0),12,2),"
  	+"decimal(value(sum(uu),0),12,2) ddd from ("
  	+" select substr(a.managecom,1,4) cc,(select name from ldcom where comcode=substr(a.managecom,1,4)) dd,"
    +" a.managecom ee,(select name from ldcom where comcode=a.managecom) ff,"
    +" c.branchattr gg,c.name hh,b.agentcode ii,b.name jj,(select gradename from laagentgrade where gradecode=d.agentgrade) kk,"
    +" b.employdate ll,case b.agentstate when '01' then '在职' when '02' then '在职' when '03' then '离职登记' when '04' then '离职登记' when '06' then '离职确认' when '07' then '离职确认' end mm,"
    +" b.outworkdate nn,value((select standprem from ladiscount where discounttype='05' and riskcode='331201' and managecom=a.managecom and agentgrade=d.agentgrade),0) oo,"
    +" value((select standprem from ladiscount where discounttype='05' and riskcode='331301' and managecom=a.managecom and agentgrade=d.agentgrade),0) pp,"
    +" value((select standprem from ladiscount where discounttype='05' and riskcode='331601' and managecom=a.managecom and agentgrade=d.agentgrade),0) qq,"
    +" value((select standprem from ladiscount where discounttype='05' and riskcode='332501' and managecom=a.managecom and agentgrade=d.agentgrade),0) pp1,"
    +" value((select standprem from ladiscount where discounttype='05' and riskcode='332601' and managecom=a.managecom and agentgrade=d.agentgrade),0) pp2,"
    +" value((select standprem from ladiscount where discounttype='05' and riskcode='332901' and managecom=a.managecom and agentgrade=d.agentgrade),0) pp3,"
    +" value((select standprem from ladiscount where discounttype='05' and riskcode='230701' and managecom=a.managecom and agentgrade=d.agentgrade),0) rr,"
    +" value((select standprem from ladiscount where discounttype='05' and riskcode='240501' and managecom=a.managecom and agentgrade=d.agentgrade),0) ss,"
    +" value((select standprem from ladiscount where discounttype='05' and riskcode='340201' and managecom=a.managecom and agentgrade=d.agentgrade),0) tt,"   
    +" value((select count(distinct agentcom) from lacomtoagent where agentcode=a.agentcode and agentcom in (select agentcom from lacom where endflag='N' or endflag is null)),0) xx,"
    +" a.agentcom ss,a.transmoney uu,a.riskcode vv,a.transstate ww "  
    +" a.payintv zz"  
    +" a.payyears yy" 	
   	+" from LACommision a,LAAgent b,latree d,labranchgroup c "
    +" where  a.agentcode=b.agentcode and a.agentcode=d.agentcode"     
    +" and a.agentgroup=c.agentgroup and a.branchtype='3' and a.branchtype2='01' "	
    +" and a.signdate between '"+tStartDate+"' and '"+tEndDate+"'"
    +" and a.customgetpoldate + 10 days <= '"+tGetDate+"'"
    +" and a.managecom like '"+tManagecom+"%'"
    +" and a.riskcode in ("+tRiskCode+") "
    + getWherePart("c.branchattr","BranchAttr")
	  + getWherePart("a.agentcode","AgentCode")
	  + getWherePart("b.name","AgentName")
	  +" ) as aaa group by cc,dd,ee,ff,gg,hh,ii,jj,kk,ll"
	  +",mm,nn,oo,pp1,pp2,pp,qq,rr,xx "
	  +" order by ddd desc,cc,dd,ee,ff,gg,hh,ii,jj,kk,ll,mm,nn,oo,pp,pp1,pp2,pp3,qq,rr,ss,tt,xx with ur";	          	
	//alert(strSQL);
	//查询SQL，返回结果字符串
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
 
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
  	PolGrid.clearData();
  	alert("数据库中没有满足条件的数据！");
    //alert("查询失败！");
    return false;
  }
  
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //设置初始化过的MULTILINE对象
  turnPage.pageDisplayGrid = PolGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  
}
</script>
</html>



