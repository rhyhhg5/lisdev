<%
//程序名称：WageQueryInit.jsp
//程序功能：
//创建日期：2003-02-16 15:12:44
//创建人  ：程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
     GlobalInput tG = new GlobalInput();
     tG = (GlobalInput)session.getValue("GI");
%>                                                                 

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {                                   
  //$initfield$
  fm.all('AgentCode').value=<%=tG.Operator%>;
  fm.all('IndexCalNo').value="";
  
  
  }
  catch(ex)
  {
    alert("PersonWageQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("PersonWageQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
	  initWageGrid();
  }
  catch(re)
  {
    alert("PersonWageQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化WageGrid
 ************************************************************
 */
function initWageGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="业务员代码";         //列名
        iArray[1][1]="100px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="业务员名称";         //列名
        iArray[2][1]="100px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[3]=new Array();
        iArray[3][0]="展业机构代码";         //列名
        iArray[3][1]="200px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[4]=new Array();
        iArray[4][0]="佣金发放年月";         //列名
        iArray[4][1]="90px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="创业扶持金";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许
		
		    iArray[6]=new Array();
        iArray[6][0]="创业金";         //列名
        iArray[6][1]="100px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[7]=new Array();
        iArray[7][0]="转正奖";         //列名
        iArray[7][1]="100px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[8]=new Array();
        iArray[8][0]="初年度佣金";         //列名
        iArray[8][1]="100px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[9]=new Array();
        iArray[9][0]="续年度佣金";         //列名
        iArray[9][1]="100px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[10]=new Array();
        iArray[10][0]="伯乐奖金";         //列名
        iArray[10][1]="100px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[11]=new Array();
        iArray[11][0]="育才奖金";         //列名
        iArray[11][1]="100px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[12]=new Array();
        iArray[12][0]="增员年终奖";         //列名
        iArray[12][1]="100px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[13]=new Array();
        iArray[13][0]="个人年终奖";         //列名
        iArray[13][1]="100px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[14]=new Array();
        iArray[14][0]="见习业务经理特别津贴";         //列名
        iArray[14][1]="140px";         //宽度
        iArray[14][2]=100;         //最大长度
        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[15]=new Array();
        iArray[15][0]="职务津贴";         //列名
        iArray[15][1]="100px";         //宽度
        iArray[15][2]=100;         //最大长度
        iArray[15][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[16]=new Array();
        iArray[16][0]="直辖组管理津贴";         //列名
        iArray[16][1]="100px";         //宽度
        iArray[16][2]=100;         //最大长度
        iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[17]=new Array();
        iArray[17][0]="组育成津贴";         //列名
        iArray[17][1]="100px";         //宽度
        iArray[17][2]=100;         //最大长度
        iArray[17][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[18]=new Array();
        iArray[18][0]="小组年终奖金";         //列名
        iArray[18][1]="100px";         //宽度
        iArray[18][2]=100;         //最大长度
        iArray[18][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[19]=new Array();
        iArray[19][0]="部直辖管理津贴";         //列名
        iArray[19][1]="100px";         //宽度
        iArray[19][2]=100;         //最大长度
        iArray[19][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[20]=new Array();
        iArray[20][0]="增部津贴";         //列名
        iArray[20][1]="100px";         //宽度
        iArray[20][2]=100;         //最大长度
        iArray[20][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[21]=new Array();
        iArray[21][0]="营销部年终奖金";         //列名
        iArray[21][1]="100px";         //宽度
        iArray[21][2]=100;         //最大长度
        iArray[21][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[22]=new Array();
        iArray[22][0]="督导长职务底薪";         //列名
        iArray[22][1]="100px";         //宽度
        iArray[22][2]=100;         //最大长度
        iArray[22][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[23]=new Array();
        iArray[23][0]="督导长育成津贴";         //列名
        iArray[23][1]="100px";         //宽度
        iArray[23][2]=100;         //最大长度
        iArray[23][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[24]=new Array();
        iArray[24][0]="区域督导长职务底薪";         //列名
        iArray[24][1]="140px";         //宽度
        iArray[24][2]=100;         //最大长度
        iArray[24][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[25]=new Array();
        iArray[25][0]="理财服务主任补发FYC";         //列名
        iArray[25][1]="140px";         //宽度
        iArray[25][2]=100;         //最大长度
        iArray[25][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[26]=new Array();
        iArray[26][0]="同业衔接人员支助薪资";         //列名
        iArray[26][1]="100px";         //宽度
        iArray[26][2]=100;         //最大长度
        iArray[26][3]=0;         //是否允许录入，0--不能，1--允许  
      
        iArray[27]=new Array();
        iArray[27][0]="同业招募基金";         //列名
        iArray[27][1]="100px";         //宽度
        iArray[27][2]=100;         //最大长度
        iArray[27][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[28]=new Array();
        iArray[28][0]="同业衔接创业奖";         //列名
        iArray[28][1]="100px";         //宽度
        iArray[28][2]=100;         //最大长度
        iArray[28][3]=0;         //是否允许录入，0--不能，1--允许
    
     	iArray[29]=new Array();
        iArray[29][0]="个人所得税";         //列名
        iArray[29][1]="100px";         //宽度
        iArray[29][2]=100;         //最大长度
        iArray[29][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[30]=new Array();
        iArray[30][0]="差勤扣款";         //列名
        iArray[30][1]="100px";         //宽度
        iArray[30][2]=100;         //最大长度
        iArray[30][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[31]=new Array();
        iArray[31][0]="附加佣金加扣款";         //列名
        iArray[31][1]="100px";         //宽度
        iArray[31][2]=100;         //最大长度
        iArray[31][3]=0;         //是否允许录入，0--不能，1--允许
  
        iArray[32]=new Array();
        iArray[32][0]="品质加扣款";         //列名
        iArray[32][1]="100px";         //宽度
        iArray[32][2]=100;         //最大长度
        iArray[32][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[33]=new Array();
        iArray[33][0]="同业衔接加扣款";         //列名
        iArray[33][1]="100px";         //宽度
        iArray[33][2]=100;         //最大长度
        iArray[33][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[34]=new Array();
        iArray[34][0]="税前薪资";         //列名
        iArray[34][1]="100px";         //宽度
        iArray[34][2]=100;         //最大长度
        iArray[34][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[35]=new Array();
        iArray[35][0]="上次佣金余额";         //列名
        iArray[35][1]="100px";         //宽度
        iArray[35][2]=100;         //最大长度
        iArray[35][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[36]=new Array();
        iArray[36][0]="本次实发佣金";         //列名
        iArray[36][1]="100px";         //宽度
        iArray[36][2]=100;         //最大长度
        iArray[36][3]=0;         //是否允许录入，0--不能，1--允许
  
        WageGrid = new MulLineEnter( "fm" , "WageGrid" ); 

        //这些属性必须在loadMulLine前
        WageGrid.mulLineCount = 0;   
        WageGrid.displayTitle = 1;
        WageGrid.locked=1;
        WageGrid.loadMulLine(iArray);
      }
      catch(ex)
      {
        alert("初始化WageGrid时出错："+ ex);
      }
    }


</script>