<%
//程序名称：LAIndexInfoQueryInit.jsp
//程序功能：
//创建日期：2003-03-14 10:07
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
    fm.all('ManageCom').value = '';
    fm.all('ManageComName').value = '';	
    fm.all('IndexCalNo').value='';
    fm.all('BranchAttr').value='';
    fm.all('AgentCode').value = '';
    fm.all('AgentSeries').value='0';
    fm.all('AgentSeriesName').value='业务系列';
    fm.all('querySql').value = '';
  }
  catch(ex)
  {
    alert("在LAIndexInfoQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LAIndexInfoQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    
    initInpBox();
    initSelBox();    
    
	  initIndexInfoGrid1();
	  
  }
  catch(re)
  {
    alert("LAIndexInfoQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initFormMoreResult()
{
  try
  {
    
    initIndexInfoGrid2();
  }
  catch(re)
  {
    alert("LAIndexInfoQueryInit.jsp-->initFormMoreResult函数中发生异常:初始化界面错误!");
  }
}


function initIndexInfoGrid1()
{                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名        

        iArray[1]=new Array();
        iArray[1][0]="代理人编码";         //列名
        iArray[1][1]="90px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[2]=new Array();
        iArray[2][0]="代理人姓名";         //列名
        iArray[2][1]="80px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[3]=new Array();
        iArray[3][0]="代理人职级";         //列名
        iArray[3][1]="80px";         //宽度
        iArray[3][2]=100;         //最大长度
        iArray[3][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[4]=new Array();
        iArray[4][0]="管理机构";         //列名
        iArray[4][1]="70px";         //宽度
        iArray[4][2]=100;         //最大长度
        iArray[4][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[5]=new Array();
        iArray[5][0]="销售机构代码";         //列名
        iArray[5][1]="100px";         //宽度
        iArray[5][2]=100;         //最大长度
        iArray[5][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[6]=new Array();
        iArray[6][0]="销售机构名称";         //列名
        iArray[6][1]="100px";         //宽度
        iArray[6][2]=100;         //最大长度
        iArray[6][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[7]=new Array();
        iArray[7][0]="统计起期";         //列名
        iArray[7][1]="0px";         //宽度
        iArray[7][2]=100;         //最大长度
        iArray[7][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[8]=new Array();
        iArray[8][0]="统计止期";         //列名
        iArray[8][1]="0px";         //宽度
        iArray[8][2]=100;         //最大长度
        iArray[8][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[9]=new Array();
        iArray[9][0]="指标类型";         //列名
        iArray[9][1]="80px";         //宽度
        iArray[9][2]=100;         //最大长度
        iArray[9][3]=0;         //是否允许录入，0--不能，1--允许
        
        
     if(fm.all('AgentSeries').value=='0')
       {
        //---2
        iArray[10]=new Array();
        iArray[10][0]="个人月均FYC";         //列名
        iArray[10][1]="90px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
        //---3
        iArray[11]=new Array();
        iArray[11][0]="个人月均新标准客户";         //列名
        iArray[11][1]="100px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
        //---4
        iArray[12]=new Array();
        iArray[12][0]="个人季继续率";         //列名
        iArray[12][1]="80px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许        
        //---5
        iArray[13]=new Array();
        iArray[13][0]="FYC挂零月数";         //列名
        iArray[13][1]="70px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
        //---6
        iArray[14]=new Array();
        iArray[14][0]="直接推荐人数";         //列名
        iArray[14][1]="80px";         //宽度
        iArray[14][2]=100;         //最大长度
        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许
        //---7
        iArray[15]=new Array();
        iArray[15][0]="累计推荐人数";         //列名
        iArray[15][1]="80px";         //宽度
        iArray[15][2]=100;         //最大长度
        iArray[15][3]=0;         //是否允许录入，0--不能，1--允许        
        //---8
        iArray[16]=new Array();
        iArray[16][0]="推荐客户顾问数";         //列名
        iArray[16][1]="90px";         //宽度
        iArray[16][2]=100;         //最大长度
        iArray[16][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[17]=new Array();
        iArray[17][0]="考核月数";         //列名
        iArray[17][1]="0px";         //宽度
        iArray[17][2]=100;         //最大长度
        iArray[17][3]=0;         //是否允许录入，0--不能，1--允许  
       }
       else if(fm.all('AgentSeries').value=='1')
       {
        
        iArray[10]=new Array();
        iArray[10][0]="营业处累计FYC";   //包含回算人力
        iArray[10][1]="110px";         //宽度
        iArray[10][2]=100;         //最大长度         
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许       
               
        iArray[11]=new Array();
        iArray[11][0]="营业处管客户顾问数";          
        iArray[11][1]="120px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[12]=new Array();
        iArray[12][0]="营业处季平均继续率";         //列名
        iArray[12][1]="120px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许    
        
        iArray[13]=new Array();
        iArray[13][0]="直接培养营业处个数";         //列名
        iArray[13][1]="120px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许  
        
        iArray[14]=new Array();
        iArray[14][0]="考核月数";         //列名
        iArray[14][1]="0px";         //宽度
        iArray[14][2]=100;         //最大长度
        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许  
        
       }
       else if(fm.all('AgentSeries').value=='2')
       {

        iArray[10]=new Array();
        iArray[10][0]="营业区累计FYC";         //包含回算FYC
        iArray[10][1]="100px";         //宽度
        iArray[10][2]=100;         //最大长度
        iArray[10][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[11]=new Array();
        iArray[11][0]="营业区管客户顾问数";     //包含回算人力
        iArray[11][1]="120px";         //宽度
        iArray[11][2]=100;         //最大长度
        iArray[11][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[12]=new Array();
        iArray[12][0]="营业区季平均继续率";       //列名
        iArray[12][1]="120px";         //宽度
        iArray[12][2]=100;         //最大长度
        iArray[12][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[13]=new Array();
        iArray[13][0]="营业区管营业处个数";         //列名
        iArray[13][1]="120px";         //宽度
        iArray[13][2]=100;         //最大长度
        iArray[13][3]=0;         //是否允许录入，0--不能，1--允许
        
        iArray[14]=new Array();
        iArray[14][0]="直接培养营业区个数";         //列名
        iArray[14][1]="120px";         //宽度
        iArray[14][2]=100;         //最大长度
        iArray[14][3]=0;         //是否允许录入，0--不能，1--允许     
        
        iArray[15]=new Array();
        iArray[15][0]="考核月数";         //列名
        iArray[15][1]="0px";         //宽度
        iArray[15][2]=100;         //最大长度
        iArray[15][3]=0;         //是否允许录入，0--不能，1--允许  
       }
       
        
//下面字段暂时不用
/*
        //---19
        iArray[27]=new Array();
        iArray[27][0]="处回算人力";         //列名
        iArray[27][1]="120px";         //宽度
        iArray[27][2]=100;         //最大长度
        iArray[27][3]=0;         //是否允许录入，0--不能，1--允许
        //---20
        iArray[28]=new Array();
        iArray[28][0]="区回算人力";         //列名
        iArray[28][1]="120px";         //宽度
        iArray[28][2]=100;         //最大长度
        iArray[28][3]=0;         //是否允许录入，0--不能，1--允许
        //---21        
        iArray[29]=new Array();
        iArray[29][0]="参与考核直辖组FYC";         //列名
        iArray[29][1]="120px";         //宽度
        iArray[29][2]=100;         //最大长度
        iArray[29][3]=0;         //是否允许录入，0--不能，1--允许
        //---22
        iArray[30]=new Array();
        iArray[30][0]="直辖及育成组平均继续率";         //列名
        iArray[30][1]="150px";         //宽度
        iArray[30][2]=100;         //最大长度
        iArray[30][3]=0;         //是否允许录入，0--不能，1--允许
        //---23
        iArray[31]=new Array();
        iArray[31][0]="直辖及育成组累计FYC";         //列名
        iArray[31][1]="130px";         //宽度
        iArray[31][2]=100;         //最大长度
        iArray[31][3]=0;         //是否允许录入，0--不能，1--允许
        //---24
        iArray[32]=new Array();
        iArray[32][0]="直辖及育成组月均有效人力数";         //列名
        iArray[32][1]="155px";         //宽度
        iArray[32][2]=100;         //最大长度
        iArray[32][3]=0;         //是否允许录入，0--不能，1--允许
        //---25   ------------------------------------------------------------------     
        iArray[33]=new Array();
        iArray[33][0]="直辖部累计FYC";         //列名
        iArray[33][1]="90px";         //宽度
        iArray[33][2]=100;         //最大长度
        iArray[33][3]=0;         //是否允许录入，0--不能，1--允许
        //---26
        iArray[34]=new Array();
        iArray[34][0]="直接育成部回算金额";         //列名
        iArray[34][1]="120px";         //宽度
        iArray[34][2]=100;         //最大长度
        iArray[34][3]=0;         //是否允许录入，0--不能，1--允许
        //---27
        iArray[35]=new Array();
        iArray[35][0]="多出育成部转加FYC";         //列名
        iArray[35][1]="120px";         //宽度
        iArray[35][2]=100;         //最大长度
        iArray[35][3]=0;         //是否允许录入，0--不能，1--允许
        //---28      
        iArray[36]=new Array();
        iArray[36][0]="参与考核直辖部FYC";         //列名
        iArray[36][1]="120px";         //宽度
        iArray[36][2]=100;         //最大长度
        iArray[36][3]=0;         //是否允许录入，0--不能，1--允许
        //---29
        iArray[37]=new Array();
        iArray[37][0]="直接育成营业部数";         //列名
        iArray[37][1]="110px";         //宽度
        iArray[37][2]=100;         //最大长度
        iArray[37][3]=0;         //是否允许录入，0--不能，1--允许          
        //---30
        iArray[38]=new Array();
        iArray[38][0]="育成营业部数";         //列名
        iArray[38][1]="80px";         //宽度
        iArray[38][2]=100;         //最大长度
        iArray[38][3]=0;         //是否允许录入，0--不能，1--允许            
        //---31
        iArray[39]=new Array();
        iArray[39][0]="督导部的直辖部月均FYC";         //列名
        iArray[39][1]="140px";         //宽度
        iArray[39][2]=100;         //最大长度
        iArray[39][3]=0;         //是否允许录入，0--不能，1--允许      
        //---32
        iArray[40]=new Array();
        iArray[40][0]="督导部的直辖部合计FYC";         //列名
        iArray[40][1]="140px";         //宽度
        iArray[40][2]=100;         //最大长度
        iArray[40][3]=0;         //是否允许录入，0--不能，1--允许            
        //---33
        iArray[41]=new Array();
        iArray[41][0]="直辖部平均继续率";         //列名
        iArray[41][1]="110px";         //宽度
        iArray[41][2]=100;         //最大长度
        iArray[41][3]=0;         //是否允许录入，0--不能，1--允许
        //---34
        iArray[42]=new Array();
        iArray[42][0]="直辖部及育成部月均有效人力数";         //列名
        iArray[42][1]="170px";         //宽度
        iArray[42][2]=100;         //最大长度
        iArray[42][3]=0;         //是否允许录入，0--不能，1--允许
        //---35
        iArray[43]=new Array();
        iArray[43][0]="直辖部及育成部平均继续率";         //列名
        iArray[43][1]="155px";         //宽度
        iArray[43][2]=100;         //最大长度
        iArray[43][3]=0;         //是否允许录入，0--不能，1--允许
        //---36
        iArray[44]=new Array();
        iArray[44][0]="所辖营业部理财主任数";         //列名
        iArray[44][1]="130px";         //宽度
        iArray[44][2]=100;         //最大长度
        iArray[44][3]=0;         //是否允许录入，0--不能，1--允许
        //---37
        iArray[45]=new Array();
        iArray[45][0]="直辖部及育成部有效人力数";         //列名
        iArray[45][1]="155px";         //宽度
        iArray[45][2]=100;         //最大长度
        iArray[45][3]=0;         //是否允许录入，0--不能，1--允许 
        //---38
        iArray[46]=new Array();
        iArray[46][0]="直接育成督导长数";         //列名
        iArray[46][1]="110px";         //宽度
        iArray[46][2]=100;         //最大长度
        iArray[46][3]=0;         //是否允许录入，0--不能，1--允许
        //---39
        iArray[47]=new Array();
        iArray[47][0]="育成督导长人数";         //列名
        iArray[47][1]="90px";         //宽度
        iArray[47][2]=100;         //最大长度
        iArray[47][3]=0;         //是否允许录入，0--不能，1--允许               
        //---40   
        iArray[48]=new Array();
        iArray[48][0]="所辖支公司理财服务主任数";         //列名
        iArray[48][1]="160px";         //宽度
        iArray[48][2]=100;         //最大长度
        iArray[48][3]=0;         //是否允许录入，0--不能，1--允许    
        //---41
        iArray[49]=new Array();
        iArray[49][0]="最近12个月直接育成部数";         //列名
        iArray[49][1]="145px";         //宽度
        iArray[49][2]=100;         //最大长度
        iArray[49][3]=0;         //是否允许录入，0--不能，1--允许
        //---42
        iArray[50]=new Array();
        iArray[50][0]="区平均继续率";         //列名
        iArray[50][1]="80px";         //宽度
        iArray[50][2]=100;         //最大长度
        iArray[50][3]=0;         //是否允许录入，0--不能，1--允许    
*/       

        IndexInfoGrid1 = new MulLineEnter( "fm" , "IndexInfoGrid1" ); 

        //这些属性必须在loadMulLine前
        IndexInfoGrid1.mulLineCount = 0;   
        IndexInfoGrid1.displayTitle = 1;
        IndexInfoGrid1.locked=1;
        IndexInfoGrid1.canSel=0;
        IndexInfoGrid1.canChk=0;
        IndexInfoGrid1.loadMulLine(iArray);  
	      IndexInfoGrid1.selBoxEventFuncName = "selectItem";
      }
      catch(ex)
      {
        alert("初始化IndexInfoGrid1时出错："+ ex);
      }
}


</script>