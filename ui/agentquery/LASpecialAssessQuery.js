//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量

window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
  if(showInfo!=null)
  {
    try
    {
      showInfo.focus();  
    }
    catch(ex)
    {
      showInfo=null;
    }
  }
}
//提交，保存按钮对应操作
function submitForm()
{	 
  if(!verifyInput())
  {
    return false;
  }
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  fm.all('fmtransact').value=mOperate;
  fm.submit(); //提交
  showInfo.close();
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content, resultStr)
{ 
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
   // showResult(resultStr);
  }
}

//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
    initForm();
  }
  catch(re)
  {
    alert("在LAIndexTempQuery.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}          

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
    parent.fraMain.rows = "0,0,50,82,*";
  }
  else {
    parent.fraMain.rows = "0,0,0,82,*";
  }
}


function DetailPrint()
{
  mOperate = "PRINT";
  fm.action="./LASpecialAssessSubmit.jsp";
  fm.target="f1print";
  submitForm();
}


// 查询按钮
function QueryClick()
{
  if(verifyInput()==false)
  {
   return false;
  }
 var tManageCom = getManageComLimit();
 //alert(tManageCom);
 //var SpecialAgentGrid;	
  SpecialAgentGrid.clearData("SpecialAgentGrid");
  var SQL = "select getunitecode(a.agentcode),a.name,a.managecom,(select name from ldcom where comcode=a.managecom),"
           + "b.branchattr,"
           + " (select name from labranchgroup where labranchgroup.agentgroup=a.branchcode), b.indexcalno" 
           +" from laagent a, laassess b "
           + " where a.agentcode= b.agentcode and a.agentstate<='02' and b.state='2' and b.agentgrade1='A00' and b.branchtype='"+fm.all('BranchType').value+"' and b.branchtype2='"+fm.all('BranchType2').value+"'"
          + " and a.managecom like '"+tManageCom+"%'"
           + getWherePart('a.ManageCom','ManageCom','like')
           + getWherePart('b.indexcalno','AssessDate')
           + " order by a.agentcode desc";
       //    alert(SQL);
   turnPage.queryModal(SQL, SpecialAgentGrid,1);
  if (!turnPage.strQueryResult) 
  {
    alert("没有挂起人员信息！");
    return false;
  }
}

function showResult(cResultStr)
{
    //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
    turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
    
    //保存查询结果字符串
    turnPage.strQueryResult  = cResultStr;
    
    //使用模拟数据源，必须写在拆分之前
    turnPage.useSimulation = 1;  

    //查询成功则拆分字符串，返回二维数组
    var tArr   = decodeEasyQueryResult(turnPage.strQueryResult,0);
    
    if (tArr != null) 
    {
       turnPage.arrDataCacheSet = chooseArray(tArr,[2,21,7,10,12,13]);
       
       //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
       turnPage.pageDisplayGrid = LAIndexTempGrid;    
		  
       //设置查询起始位置
       turnPage.pageIndex = 0;
       
       //在查询结果数组中取出符合页面显示大小设置的数组
       var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
       
       //调用MULTILINE对象显示查询结果
       displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
     } 
}








