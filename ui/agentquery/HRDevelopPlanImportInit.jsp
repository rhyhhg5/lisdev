<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>
<%@page import = "com.sinosoft.lis.bl.*"%>
<%@page import = "com.sinosoft.lis.vbl.*"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
	PubFun tpubFun = new PubFun();
	String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
	function initInpBox(){
		try{
			fm.all('ManageCom').value = <%=strManageCom%>;
		    if(fm.all('ManageCom').value==86){
		    	fm.all('ManageCom').readOnly=false;
		    } else{
		    	fm.all('ManageCom').readOnly=true;
		    }
		    if(fm.all('ManageCom').value!=null){
		    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
		        //显示代码选择中文
		        if (arrResult != null){
		        	fm.all('ManageComName').value=arrResult[0][0];
		        } 
		    }
	        fm.all('TypeChoose').value = '';
	        fm.all('RewardReach').value = '';
	        fm.all('RewardReachName').value = '';
	    }
	    catch(ex){
	        alert("在HRDevelopPlanImportInit.jsp-->InitInpBox函数中发生异常:初始化界面错误");
	    }
	}
	function initForm(){
		try{
			initInpBox();
			initHRPlanGrid();
			showAllCodeName();
		}catch(re){
			alert("HRDevelopPlanImportInit.jsp-->InitForm函数中发生异常:初始化界面错误");
		}
	}
	function initHRPlanGrid(){
		var iArray = new Array();
		try{
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "分公司名称";
			iArray[1][1] = "70px";
			iArray[1][2] = 100;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "中心支公司名称";
			iArray[2][1] = "70px";
			iArray[2][2] = 100;
			iArray[2][3] = 0;
			
			iArray[3] = new Array();
			iArray[3][0] = "基础人力1216";
			iArray[3][1] = "45px";
			iArray[3][2] = 60;
			iArray[3][3] = 0;
			
			iArray[4] = new Array();
			iArray[4][0] = "类别选择(A/B)";
			iArray[4][1] = "45px";
			iArray[4][2] = 20;
			iArray[4][3] = 0;
			
			iArray[5] = new Array();
			iArray[5][0] = "年度净增有效人力标准";
			iArray[5][1] = "60px";
			iArray[5][2] = 100;
			iArray[5][3] = 0;
			
			iArray[6] = new Array();
			iArray[6][0] = "净增有效人力达成奖档次";
			iArray[6][1] = "60px";
			iArray[6][2] = 100;
			iArray[6][3] = 0;
			
			iArray[7] = new Array();
			iArray[7][0] = "预设额度";
			iArray[7][1] = "40px";
			iArray[7][2] = 100;
			iArray[7][3] = 0;
			
			HRPlanGrid = new MulLineEnter("fm","HRPlanGrid");
		     
		    HRPlanGrid.mulLineCount = 10;   
		    HRPlanGrid.displayTitle = 1;
		    HRPlanGrid.canSel = 1;
		    HRPlanGrid.canChk = 0;
		    HRPlanGrid.locked = 1;
		    HRPlanGrid.hiddenSubtraction = 1;
		    HRPlanGrid.hiddenPlus = 1;
		    HRPlanGrid.selBoxEventFuncName ="";
		     
		    HRPlanGrid.loadMulLine(iArray);

		}catch(ex){
			alert(ex);
		}
	}
	
</script>
