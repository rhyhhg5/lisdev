<%
//程序名称：LABankAgentQuafInit.jsp
//程序功能：功能描述
//创建日期：2012-05-28 10:20:58
//创建人  ：ctrHTML
//更新人  ：  
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>                  
<script language="JavaScript">
function initInpBox() { 
  try {     
    fm.all('ManageCom').value ="<%=tG.ManageCom%>";
    //fm.all('EndDate').value = "<%=PubFun.getCurrentDate()%>";
    fm.all('SpareDays').value = "";
  }
  catch(ex) {
    alert("在LABankAgentQuafInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}                                    
function initForm() {
  try {
    initInpBox();
    initLABankAgentQuafGrid();  
  }
  catch(re) {
    alert("LABankAgentQuafInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//领取项信息列表的初始化
var LABankAgentQuafGrid;
function initLABankAgentQuafGrid() {                               
  var iArray = new Array();
    
  try {
  
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名
    iArray[0][3]=0;         		//列名
    iArray[0][4]="station";         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="分公司";         		//列名
    iArray[1][1]="80px";         		//列名
    iArray[1][3]=0;         		//列名
    
    iArray[2]=new Array();
    iArray[2][0]="支公司";         		//列名
    iArray[2][1]="120px";         		//列名
    iArray[2][3]=0;         		//列名
    
    iArray[3]=new Array();
    iArray[3][0]="姓名";         		//列名
    iArray[3][1]="100px";         		//列名
    iArray[3][3]=0;         		//列名
    
    
    
    iArray[4]=new Array();
    iArray[4][0]="业务员编码";         		//列名
    iArray[4][1]="100px";         		//列名
    iArray[4][3]=0;         		//列名
    
    iArray[5]=new Array();
    iArray[5][0]="职级";         		//列名
    iArray[5][1]="100px";         		//列名
    iArray[5][3]=0;         		//列名
    
    iArray[6]=new Array();
    iArray[6][0]="资格证书号";         		//列名
    iArray[6][1]="100px";         		//列名
    iArray[6][3]=0;         		//列名
    
    iArray[7]=new Array();
    iArray[7][0]="初次取证日期";         		//列名
    iArray[7][1]="100px";         		//列名
    iArray[7][3]=0;         		//列名
    
    iArray[8]=new Array();
    iArray[8][0]="到期日期";         		//列名
    iArray[8][1]="100px";         		//列名
    iArray[8][3]=0;         		//列名
    
    iArray[9]=new Array();
    iArray[9][0]="手机号码";         		//列名
    iArray[9][1]="100px";         		//列名
    iArray[9][3]=0;         		//列名
    
    iArray[10]=new Array();
    iArray[10][0]="身份证号";         		//列名
    iArray[10][1]="100px";         		//列名
    iArray[10][3]=0;         		//列名
    


   LABankAgentQuafGrid = new MulLineEnter( "fm" , "LABankAgentQuafGrid" ); 
    //这些属性必须在loadMulLine前
 
    LABankAgentQuafGrid.mulLineCount = 10;   
    LABankAgentQuafGrid.displayTitle = 1;
    LABankAgentQuafGrid.hiddenPlus = 1;
    LABankAgentQuafGrid.hiddenSubtraction = 1;
    LABankAgentQuafGrid.canSel = 0;
    LABankAgentQuafGrid.canChk = 0;
    LABankAgentQuafGrid.selBoxEventFuncName = "showOne";
  
    LABankAgentQuafGrid.loadMulLine(iArray);  
    //这些操作必须在loadMulLine后面
    //LAContGrid.setRowColData(1,1,"asdf");
  }
  catch(ex) {
    alert(ex);
  }
}
</script>
