var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();

function query(){
	if(!verifyInput2()){
		return false;
	}
	var tExamineDate = fm.AssessYearMonth.value;
	var RegExp = /\d{6}/g;
	if(!RegExp.test(tExamineDate)){
		alert("请输入数字！");
		return;
	}
	if(tExamineDate<201601 ||tExamineDate>201612 ){
		alert("考核年月只能是2016年内！");
		return;
	}
	
	var subEDate = parseInt(tExamineDate.substring(4));
	if(subEDate<1 ||subEDate>12){
		alert("月份输入有误！");
		return;
	}
	if(fm.AveragePeopleFlag.value=='0'){
	    alert("平均人力为否时，无数据！");
		return;
	}
	/**    var strSQL = "";
    	var mngcom = fm.ManageCom.value;
	     if(mngcom.substring(0,4)=="8612"){
	     strSQL= " select b.remark1,(select name from laagent where agentcode=a.agentcode), "
				+" getUniteCode(a.agentcode),(case a.mngcom when '86120100' then '86120000' else a.mngcom end),b.plan,c.tmakedate,'是',"
				+" case when c.managecom='86120100' then (case when sum(c.fyc)>=(select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " 
			    +" and code =(select codename from ldcode where codetype='humandeveidesc' and code='86120000')) " 
			    +"  then '是' else '否' end) " 
			    +" else  (case when sum(c.fyc)>=(select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " 
			    +" and code =(select codename from ldcode where codetype='humandeveidesc' and code=c.managecom)) " 
			    +"  then '是' else '否' end) end ," 
			    +" case when sum(c.fyc)>0 then '是' else '否' end,  " 
				+" c.contno,c.riskcode,sum(c.fyc) "
				+"  from LAhumanDesc a,LAhumandevei b,lacommision c   "
				+" where  a.agentcode=c.agentcode and a.mngcom=c.managecom and b.remark1=c.wageno "
				+" and a.mngcom=b.mngcom and a.year=b.year  "
				+" and a.month=b.month   "
				+" and (c.Payyear =0 and c.renewcount = 0) and c.BranchType='1'  and c.branchtype2='01' "
				+" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=c.polno and d.wageno>'201411' and d.managecom=c.managecom) "
				+" and a.mngcom in('86120000','86120100')"
				+" and c.wageno = '"+fm.ExamineDate.value+"' ";
			 if(fm.AgentCode.value!='' && fm.AgentCode.value!=null){
		         strSQL  +=" and a.agentcode =getAgentCode('"+fm.AgentCode.value+"')";
            	}
				strSQL  +=" group by b.remark1,a.agentcode,a.mngcom,b.plan,c.tmakedate,c.managecom, c.contno,c.riskcode ";
				         
			}else{
			   strSQL= " select b.remark1,(select name from laagent where agentcode=a.agentcode), "
				+" getUniteCode(a.agentcode),(case a.mngcom when '86120100' then '86120000' else a.mngcom end),b.plan,c.tmakedate,'是',"
				+" case when c.managecom='86120100' then (case when sum(c.fyc)>=(select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " 
			    +" and code =(select codename from ldcode where codetype='humandeveidesc' and code='86120000')) " 
			    +"  then '是' else '否' end) " 
			    +" else  (case when sum(c.fyc)>=(select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " 
			    +" and code =(select codename from ldcode where codetype='humandeveidesc' and code=c.managecom)) " 
			    +"  then '是' else '否' end) end ," 
			    +" case when sum(c.fyc)>0 then '是' else '否' end,  " 
				+" c.contno,c.riskcode,sum(c.fyc) "
				+"  from LAhumanDesc a,LAhumandevei b,lacommision c   "
				+" where  a.agentcode=c.agentcode and a.mngcom=c.managecom and b.remark1=c.wageno "
				+" and a.mngcom=b.mngcom and a.year=b.year  "
				+" and a.month=b.month   "
				+" and (c.Payyear =0 and c.renewcount = 0) and c.BranchType='1'  and c.branchtype2='01' "
				+" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=c.polno and d.wageno>'201411' and d.managecom=c.managecom) "
				+" and a.mngcom like '"+fm.ManageCom.value+"%' "
				+" and c.wageno = '"+fm.ExamineDate.value+"' ";
			   if(fm.AgentCode.value!='' && fm.AgentCode.value!=null){
		         strSQL  +=" and a.agentcode =getAgentCode('"+fm.AgentCode.value+"')";
            	}
            	strSQL  +=" group by b.remark1,a.agentcode,a.mngcom,b.plan,c.tmakedate,c.managecom, c.contno,c.riskcode ";
				        
			}	
			if(fm.PassedPeopleFlag.value!=''&&fm.ValidPeopleFlag.value!=''){
	      	  if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='0'){
			       strSQL +=" having  sum(c.fyc)<=0 ";
	        	}
	       	  if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='1'){
			
					strSQL +=" having  sum(c.fyc)>0 and sum(c.fyc) < " +
					" case when c.managecom='86120100' then (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					" and code =(select codename from ldcode where codetype='humandeveidesc' and code='86120000')) " +
					" else  (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					"  and code =(select codename from ldcode where codetype='humandeveidesc' and code=c.managecom)) end " ;
				 }
				
				if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='0'){
					
					strSQL +=" having sum(c.fyc) >= " +
					" case when c.managecom='86120100' then (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					" and code =(select codename from ldcode where codetype='humandeveidesc' and code='86120000')) " +
					" else  (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					"  and code =(select codename from ldcode where codetype='humandeveidesc' and code=c.managecom)) end " ;
				}	
				if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='1'){
					
					strSQL +=" having sum(c.fyc) >= " +
					" case when c.managecom='86120100' then (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					" and code =(select codename from ldcode where codetype='humandeveidesc' and code='86120000')) " +
					" else  (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					"  and code =(select codename from ldcode where codetype='humandeveidesc' and code=c.managecom)) end " ;
				}	
		
	    }else{
				if(fm.PassedPeopleFlag.value=='0'){
					
					strSQL +=" having  sum(a.fyc) < " +
					" case when c.managecom='86120100' then (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					" and code =(select codename from ldcode where codetype='humandeveidesc' and code='86120000')) " +
					" else  (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					"  and code =(select codename from ldcode where codetype='humandeveidesc' and code=c.managecom)) end " ;
				}
				if(fm.ValidPeopleFlag.value=='1'){
					
					strSQL  +=" having  sum(c.fyc)>0 ";
				}
				if(fm.ValidPeopleFlag.value=='0'){
					strSQL +=" having  sum(c.fyc)<=0 ";
				}
				if(fm.PassedPeopleFlag.value=='1'){
					
					strSQL +=" having sum(c.fyc) >= " +
					" case when c.managecom='86120100' then (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					" and code =(select codename from ldcode where codetype='humandeveidesc' and code='86120000')) " +
					" else  (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
					"  and code =(select codename from ldcode where codetype='humandeveidesc' and code=c.managecom)) end " ;
				}	
	}	*/
	
		var cSql = "select codealias,comcode,othersign from ldcode1 where codetype='humandeveimonth' and codename='"+fm.AssessYearMonth.value+"'";
	arrResult = easyExecSql(cSql);
	var strSQL = " select t.ym,t.name,t.agentcode,t.managecom,t.plan,t.tmakedate,'是',t.hg,t.yx,t.contno,t.riskcode,t.sfyc" +
	" from(select  temp.ym,temp.name,temp.agentcode,temp.managecom,temp.comname,temp.plan,temp.Tmakedate,temp.contno,temp.riskcode,temp.sfyc " +
	" ,case when sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom)>=temp.prefyc then '是' else '否' end hg " +
	" ,case when sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom)>0 then '是' else '否' end  yx " +
	" ,sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom) ofyc,temp.prefyc " +
	" from (select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" case when a.managecom='86120100' then  (select codealias from ldcode1 where codetype='2016humandeveiplan' and code =(select codename from ldcode where codetype='2016humandeveidesc' and code='86120000'))" +
	" when a.managecom in ('86440102','86440103','86440104','86440101') then  (select codealias from ldcode1 where codetype='2016humandeveiplan' and code =(select codename from ldcode where codetype='2016humandeveidesc' and code='86440100'))" +
	" else (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) end plan, " +
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc,  " +
	" case when a.managecom='86120100' then (select riskwrapplanname from ldcode1 where codetype='2016humandeveiplan' " +
	" and code =(select codename from ldcode where codetype='2016humandeveidesc' and code='86120000'))  " +
	" when a.managecom in ('86440102','86440103','86440104','86440101') then (select riskwrapplanname from ldcode1 where codetype='2016humandeveiplan' " +
	" and code =(select codename from ldcode where codetype='2016humandeveidesc' and code='86440100')) " +
	" else  (select riskwrapplanname from ldcode1 where codetype='2016humandeveiplan' " +
	" and code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) end prefyc" +
	" from lacommision a  where a.BranchType='1' and a.branchtype2='01' and a.managecom like  " +
	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end)  " +
	" and  Payyear = 0 and a.renewcount ='0'  " +
	" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201512' and d.managecom like " +
	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end)) " +
	" and exists(select 1 from lahumandesc where a.agentcode =agentcode and mngcom like " +
	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end) " +
	"  and year=substr('"+fm.AssessYearMonth.value+"',1,4)  " +
	" and  month=substr('"+fm.AssessYearMonth.value+"',5,2))" +
	" and ((a.wageno='"+arrResult[0][2]+"' and Tmakedate>'"+arrResult[0][0]+"')or " +
	" (a.wageno='"+fm.AssessYearMonth.value+"' and tmakedate<='"+arrResult[0][1]+"'))  ";
	if(fm.AgentCode.value!='' && fm.AgentCode.value!=null){
	strSQL  +=" and a.agentcode =getAgentCode('"+fm.AgentCode.value+"')";
	}
	strSQL  +=" group by a.agentcode,a.managecom,a.Tmakedate,a.contno,a.riskcode)temp)t ";
	
	if(fm.PassedPeopleFlag.value!=''&&fm.ValidPeopleFlag.value!=''){
	if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='0'){
		strSQL +=" where  t.ofyc<=0 ";
	}
	if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='1'){
		
		strSQL +=" where t.ofyc>0 and " +
				" t.ofyc < t.prefyc " ;
	}
	
	if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='0'){
		
		strSQL +=" where  t.ofyc >= t.prefyc " ;
	}	
	if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='1'){
		
		strSQL +=" where  t.ofyc >= t.prefyc " ;
	}	
	
	}else{
	if(fm.PassedPeopleFlag.value=='0'){
		
		strSQL +=" where  t.ofyc < t.prefyc " ;
	
	}
	if(fm.ValidPeopleFlag.value=='1'){
		
		strSQL  +=" where   t.ofyc>0 ";
	}
	if(fm.ValidPeopleFlag.value=='0'){
		strSQL +=" where   t.ofyc<=0 ";
	}
	if(fm.PassedPeopleFlag.value=='1'){
		
		strSQL +=" where  t.ofyc >= t.prefyc";
	}	
	}
			
	turnPage.queryModal(strSQL,YearAudDeTailsGrid);
}

function download(){
	
		var cSql = "select codealias,comcode,othersign from ldcode1 where codetype='humandeveimonth' and codename='"+fm.AssessYearMonth.value+"'";
	arrResult = easyExecSql(cSql);
	var strSQL = " select t.ym,t.name,t.agentcode,t.managecom,t.plan,t.tmakedate,'是',t.hg,t.yx,t.contno,t.riskcode,t.sfyc" +
	" from(select  temp.ym,temp.name,temp.agentcode,temp.managecom,temp.comname,temp.plan,temp.Tmakedate,temp.contno,temp.riskcode,temp.sfyc " +
	" ,case when sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom)>=temp.prefyc then '是' else '否' end hg " +
	" ,case when sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom)>0 then '是' else '否' end  yx " +
	" ,sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom) ofyc,temp.prefyc " +
	" from (select '"+fm.AssessYearMonth.value+"' ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" case when a.managecom='86120100' then  (select codealias from ldcode1 where codetype='2016humandeveiplan' and code =(select codename from ldcode where codetype='2016humandeveidesc' and code='86120000'))" +
	" when a.managecom in ('86440102','86440103','86440104','86440101') then  (select codealias from ldcode1 where codetype='2016humandeveiplan' and code =(select codename from ldcode where codetype='2016humandeveidesc' and code='86440100'))" +
	" else (select codealias from ldcode1 where codetype='2016humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) end plan, " +
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc,  " +
	" case when a.managecom='86120100' then (select riskwrapplanname from ldcode1 where codetype='2016humandeveiplan' " +
	" and code =(select codename from ldcode where codetype='2016humandeveidesc' and code='86120000'))  " +
	" when a.managecom in ('86440102','86440103','86440104','86440101') then (select riskwrapplanname from ldcode1 where codetype='2016humandeveiplan' " +
	" and code =(select codename from ldcode where codetype='2016humandeveidesc' and code='86440100')) " +
	" else  (select riskwrapplanname from ldcode1 where codetype='2016humandeveiplan' " +
	" and code =(select codename from ldcode where codetype='2016humandeveidesc' and code=a.managecom)) end prefyc" +
	" from lacommision a  where a.BranchType='1' and a.branchtype2='01' and a.managecom like  " +
	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end)  " +
	" and  Payyear = 0 and a.renewcount ='0'  " +
	" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'201512' and d.managecom like " +
	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end)) " +
	" and exists(select 1 from lahumandesc where a.agentcode =agentcode and mngcom like " +
	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end) " +
	"  and year=substr('"+fm.AssessYearMonth.value+"',1,4)  " +
	" and  month=substr('"+fm.AssessYearMonth.value+"',5,2))" +
	" and ((a.wageno='"+arrResult[0][2]+"' and Tmakedate>'"+arrResult[0][0]+"')or " +
	" (a.wageno='"+fm.AssessYearMonth.value+"' and tmakedate<='"+arrResult[0][1]+"'))  ";
	if(fm.AgentCode.value!='' && fm.AgentCode.value!=null){
	strSQL  +=" and a.agentcode =getAgentCode('"+fm.AgentCode.value+"')";
	}
	strSQL  +=" group by a.agentcode,a.managecom,a.Tmakedate,a.contno,a.riskcode)temp)t ";
	
	if(fm.PassedPeopleFlag.value!=''&&fm.ValidPeopleFlag.value!=''){
	if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='0'){
		strSQL +=" where  t.ofyc<=0 ";
	}
	if(fm.PassedPeopleFlag.value=='0'&&fm.ValidPeopleFlag.value=='1'){
		
		strSQL +=" where t.ofyc>0 and " +
				" t.ofyc < t.prefyc " ;
	}
	
	if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='0'){
		
		strSQL +=" where  t.ofyc >= t.prefyc " ;
	}	
	if(fm.PassedPeopleFlag.value=='1'&&fm.ValidPeopleFlag.value=='1'){
		
		strSQL +=" where  t.ofyc >= t.prefyc " ;
	}	
	
	}else{
	if(fm.PassedPeopleFlag.value=='0'){
		
		strSQL +=" where  t.ofyc < t.prefyc " ;
	
	}
	if(fm.ValidPeopleFlag.value=='1'){
		
		strSQL  +=" where   t.ofyc>0 ";
	}
	if(fm.ValidPeopleFlag.value=='0'){
		strSQL +=" where   t.ofyc<=0 ";
	}
	if(fm.PassedPeopleFlag.value=='1'){
		
		strSQL +=" where  t.ofyc >= t.prefyc";
	}	
	}
			
	
	fm.querySql.value = strSQL;
	if(fm.querySql.value!=null && fm.querySql.value!=""){
		var formAction = fm.action;
		fm.action = "YearAuditingDetailsDownload.jsp";
		fm.submit();
		fm.target = "fraSubmit";
		fm.action = formAction;
	}else{
		alert("没有数据");
		return;
	}
}