var showInfo;
var mDebug="0";
var turnPage = new turnPageClass(); 
var queryFlag = false;
function query(){
	
	if(!verifyInput2()){
		return false;
	}
	
	 /**var strSQL = " select  "
		+" b.wageno,a.name,a.groupagentcode,a.managecom,(select name from ldcom where comcode=a.managecom),(select substr(codename,1,1) from ldcode where codetype='"+year+"humandeveidesc' and code=b.managecom),b.tmakedate, "
		+" (case b.grpcontno when '00000000000000000000' then b.contno else b.grpcontno end),b.riskcode,sum(b.fyc), "
		+" (case when b.managecom='86120100' then (case when sum(b.fyc)>=(select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " 
		+" and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code='86120000')) " 
		+"  then '是' else '否' end) " 
		+" else  (case when sum(b.fyc)>=(select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " 
		+" and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code=b.managecom)) " 
		+"  then '是' else '否' end) end  )as month"
		+" from laagent a,lacommision b where a.agentcode=b.agentcode  "
		+" and a.employdate between '2014-12-16' and '"+year+"-3-31' "
		+" and (b.Payyear =0 and b.renewcount = 0) "
		+" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=b.polno and d.wageno>'201411' and d.managecom=b.managecom) "
		+" and a.agentstate='01' and b.branchtype='1'  and b.branchtype2='01' "
		+" and b.modifydate>='2014-12-16' "
		+" and b.managecom like '"+fm.ManageCom.value+"%' "
		+" and b.wageno between '201412' and '"+year+"03' and b.tmakedate>='2014-12-16' "
		+ getWherePart( 'a.groupagentcode','Groupagentcode' )
		+" group by b.wageno,a.name,a.groupagentcode,a.managecom,b.managecom,b.tmakedate,b.grpcontno,b.contno,b.riskcode"
		+" with ur ";
		*/
	var year = fm.all('year').value;
	var lastYear = parseInt(year)-1;
	var sqlWhere ="";
	var managecom = fm.ManageCom.value;
	if(managecom=='86'|| managecom.length==4)
	{
		sqlWhere = " and  a.managecom like '"+managecom+"%' ";
	}
	else if(managecom.length==8) 
	{
//		if(managecom.substr(6,2)=='00')
//		{
//			sqlWhere = " and (a.managecom in (select code1 from ldcode1 where code = '"+managecom+"' and codetype ='"+year+"includemanagecom') or a.managecom like '"+managecom.substr(0,6)+"%' )";
//		}
//		else
//		{
			sqlWhere = " and a.managecom in (select code1 from ldcode1 where code = '"+managecom+"' and codetype ='"+year+"includemanagecom' union select '"+managecom+"' from dual) "; 
//		}
	}
	var  tWorkSQL ="(select  agentcode  from lahumandesc where year ='"+year+"' and month ='03' )";
	var   tCurrentMonth = new Date().getMonth()+1;
	if(parseInt(tCurrentMonth, "10")<=3)
	{
		tWorkSQL ="(select a.Agentcode from laagent a,LADimission f "
			+ "where a.agentcode=f.agentcode and a.BranchType='1' and a.branchtype2='01'  " 
			+sqlWhere+" and a.AgentState='02' and a.employdate >='"+lastYear+"-12-16' "
			+ "group by a.agentcode "
			+ "having  min(days(a.employdate)-days(f.DepartDate)) >= '181' "
			+ "union "
			+"select a.Agentcode from laagent a "
			+ "where  a.BranchType='1' and a.branchtype2='01' "+sqlWhere+" and  a.AgentState='02' and a.employdate <'"+lastYear+"-12-16' "
			+ "union "
			+ "select a.Agentcode from laagent a "
			+ "where a.BranchType='1' "+sqlWhere+" and  a.AgentState='01' and a.branchtype2='01') ";
	}
		var strSQL = " select t.ym,t.name,t.agentcode,t.managecom,(select name from ldcom where comcode=t.managecom),t.plan,t.tmakedate,t.contno,t.riskcode,t.sfyc,t.hg" +
	" from(select  temp.ym,temp.name,temp.agentcode,temp.managecom,temp.comname,temp.plan,temp.Tmakedate,temp.contno,temp.riskcode,temp.sfyc " +
	" ,case when sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom)>=temp.prefyc*int(temp.month) then '是' else '否' end hg " +
//	" ,case when sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom)>0 then '是' else '否' end  yx " +
	" ,sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom) ofyc,temp.prefyc " +
	" from (select a.wageno ym,(select name from laagent where agentcode=a.agentcode )name, " +
	" getUniteCode(a.agentcode) agentcode,(case when (select code from ldcode1 where code1 = a.managecom and codetype ='"+year+"includemanagecom') is null then  a.managecom  else (select code from ldcode1 where code1 = a.managecom and codetype ='"+year+"includemanagecom') end) managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
	" case when (select code from ldcode1 where code1 = a.managecom and codetype ='"+year+"includemanagecom') is null then  (select codealias from ldcode1 where codetype='"+year+"humandeveiplan' and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code=a.managecom))" +
	" else (select codealias from ldcode1 where codetype='"+year+"humandeveiplan' and  " +
	" code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code=(select code from ldcode1 where code1 = a.managecom and codetype ='"+year+"includemanagecom'))) end plan, " +
	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc,  " +
//	" case when (select code from ldcode1 where code1 = a.managecom and codetype ='"+year+"includemanagecom')  is  not null then (select riskwrapplanname from ldcode1 where codetype='"+year+"humandeveiplan' " +
//	" and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code=(select code from ldcode1 where code1 = a.managecom and codetype ='"+year+"includemanagecom')))  " +
//	" else  (select riskwrapplanname from ldcode1 where codetype='"+year+"humandeveiplan' " +
//	" and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code=a.managecom)) end 
	" 200 as prefyc," +
	"(select (case when employdate<='"+year+"-1-31' then '3' when employdate between '"+year+"-2-1' and  last_day(date('"+year+"-2-1')) then '2' else '1' end )  from laagent b where a.agentcode = b.agentcode ) as month" +
	" from lacommision a  where a.BranchType='1' and a.branchtype2='01' " +
	sqlWhere+ 
	" and  Payyear = 0 and a.renewcount ='0'  " +
	" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'"+lastYear+"11' and d.managecom =a.managecom)" +
	" and agentcode in  (select agentcode from laagent where  employdate between '"+lastYear+"-12-16' and '"+year+"-3-31') "+
	" and agentcode in "+tWorkSQL+
	" and a.wageno between '"+lastYear+"12' and '"+year+"03' and a.tmakedate>='"+lastYear+"-12-16' ";
	
	 if(fm.Groupagentcode.value!='' && fm.Groupagentcode.value!=null){
	 strSQL += " and getUniteCode(a.agentcode)='"+fm.Groupagentcode.value+"'";
	 }
      strSQL +=" group by a.wageno,a.agentcode,a.managecom,a.Tmakedate,a.contno,a.riskcode)temp)t  with ur";
	

	turnPage.queryModal(strSQL,GoodStartGrid);
	fm.querySql.value = strSQL;
	queryFlag= true;
}

function download(){
	if(!queryFlag)
	{
		alert("下载前请先进行【查询】操作！");
		return false;
	}
	if(!verifyInput2()){
		return false;
	}
	
//		var strSQL = " select t.ym,t.name,t.agentcode,t.managecom,(select name from ldcom where comcode=t.managecom),t.plan,t.tmakedate,t.contno,t.riskcode,t.sfyc,t.hg" +
//	" from(select  temp.ym,temp.name,temp.agentcode,temp.managecom,temp.comname,temp.plan,temp.Tmakedate,temp.contno,temp.riskcode,temp.sfyc " +
//	" ,case when sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom)>=temp.prefyc then '是' else '否' end hg " +
//	" ,case when sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom)>0 then '是' else '否' end  yx " +
//	" ,sum(temp.sfyc) over (partition by temp.agentcode,temp.managecom) ofyc,temp.prefyc " +
//	" from (select a.wageno ym,(select name from laagent where agentcode=a.agentcode )name, " +
//	" getUniteCode(a.agentcode) agentcode,a.managecom, (select name from ldcom where comcode=a.managecom) comname,  " +
//	" case when a.managecom='86120100' then  (select codealias from ldcode1 where codetype='humandeveiplan' and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code='86120000'))" +
//	" when a.managecom in ('86440102','86440103','86440104','86440101') then  (select codealias from ldcode1 where codetype='humandeveiplan' and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code='86440100'))" +
//	" else (select codealias from ldcode1 where codetype='humandeveiplan' and  " +
//	" code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code=a.managecom)) end plan, " +
//	" a.Tmakedate,a.contno,a.riskcode,sum(a.fyc) sfyc,  " +
//	" case when a.managecom='86120100' then (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
//	" and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code='86120000'))  " +
//	" when a.managecom in ('86440102','86440103','86440104','86440101') then (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
//	" and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code='86440100')) " +
//	" else  (select riskwrapplanname from ldcode1 where codetype='humandeveiplan' " +
//	" and code =(select codename from ldcode where codetype='"+year+"humandeveidesc' and code=a.managecom)) end prefyc" +
//	" from lacommision a  where a.BranchType='1' and a.branchtype2='01' and a.managecom like  " +
//	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end)  " +
//	" and  Payyear = 0 and a.renewcount ='0'  " +
//	" and not exists(select 1 from lacommision  d where d.TRANSTYPE='WT'  and d.polno=a.polno and d.wageno>'"+lastYear+"11' and d.managecom like " +
//	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end)) " +
//	" and exists(select 1 from lahumandesc where a.agentcode =agentcode and mngcom like " +
//	" (case when '"+fm.ManageCom.value+"'='86120000' then '8612%'  when '"+fm.ManageCom.value+"'='86440100' then '864401%' else '"+fm.ManageCom.value+"%' end)) " +
//	" and exists (select 1 from laagent where agentcode=a.agentcode and employdate between '"+lastYear+"-12-16' and '"+year+"-3-31') "+
//	" and a.wageno between '"+lastYear+"12' and '"+year+"03' and a.tmakedate>='"+lastYear+"-12-16' ";
//	
//	 if(fm.Groupagentcode.value!='' && fm.Groupagentcode.value!=null){
//	 strSQL += " and getUniteCode(a.agentcode)='"+fm.Groupagentcode.value+"'";
//	 }
//      strSQL +=" group by a.wageno,a.agentcode,a.managecom,a.Tmakedate,a.contno,a.riskcode)temp)t  with ur";
//					
//	fm.querySql.value = strSQL;
	if(fm.querySql.value!=null && fm.querySql.value!=""){
		var formAction = fm.action;
		fm.action = "GoodStartDetailsDownload.jsp";
		fm.submit();
		fm.target = "fraSubmit";
		fm.action = formAction;
	}else{
		alert("没有数据");
		return;
	}
}