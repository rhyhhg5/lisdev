<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：YearAuditingDetailsInit.jsp
//程序功能：
//创建日期：2015-02-11
//创建人：CZ
//更新记录：    更新人	更新日期	更新原因/内容
%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<script src="../common/javascript/Common.js"></script>
<%
	PubFun tpubFun = new PubFun();
	String strCurDay = tpubFun.getCurrentDate();
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = globalInput.ComCode;
%>
<script language="JavaScript">
	function initInpBox(){
		try{
			fm.all('ManageCom').value = <%=strManageCom%>;
		    if(fm.all('ManageCom').value==86){
		    	fm.all('ManageCom').readOnly=false;
		    } else{
		    	fm.all('ManageCom').readOnly=true;
		    }
		    if(fm.all('ManageCom').value!=null){
		    	var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");                        
		        //显示代码选择中文
		        if (arrResult != null){
		        	fm.all('ManageComName').value=arrResult[0][0];
		        } 
		    }
		}catch(ex){
			alert("在YearAuditingDetailsInit.jsp-->IninInpBox函数中发生异常 ：初始化界面错误！");
		}
	}
	function initForm(){
		try{
			initElementtype();
			initInpBox();
			initYearAudDeTailsGrid();
			showAllCodeName();
		}catch(re){
			alert("YearAuditingDetailsInit.jsp-->InitForm函数中发生异常:初始化界面错误");
		}
	}
	function initYearAudDeTailsGrid(){
		try{
			var iArray = new Array();
			
			iArray[0] = new Array();
			iArray[0][0] = "序号";
			iArray[0][1] = "30px";
			iArray[0][2] = 10;
			iArray[0][3] = 0;
			
			iArray[1] = new Array();
			iArray[1][0] = "考核年月";
			iArray[1][1] = "40px";
			iArray[1][2] = 40;
			iArray[1][3] = 0;
			
			iArray[2] = new Array();
			iArray[2][0] = "代理人姓名";
			iArray[2][1] = "40px";
			iArray[2][2] = 40;
			iArray[2][3] = 0;
			
			iArray[3] = new Array();
			iArray[3][0] = "代理人编码";
			iArray[3][1] = "40px";
			iArray[3][2] = 40;
			iArray[3][3] = 0;
			
			iArray[4] = new Array();
			iArray[4][0] = "管理机构";
			iArray[4][1] = "40px";
			iArray[4][2] = 40;
			iArray[4][3] = 0;
			
			iArray[5] = new Array();
			iArray[5][0] = "方案类别";
			iArray[5][1] = "40px";
			iArray[5][2] = 40;
			iArray[5][3] = 0;
			
			iArray[6] = new Array();
			iArray[6][0] = "业务日期";
			iArray[6][1] = "40px";
			iArray[6][2] = 40;
			iArray[6][3] = 0;
			
			iArray[7] = new Array();
			iArray[7][0] = "是否平均人力";
			iArray[7][1] = "40px";
			iArray[7][2] = 40;
			iArray[7][3] = 0;
			
			iArray[8] = new Array();
			iArray[8][0] = "是否合格人力";
			iArray[8][1] = "40px";
			iArray[8][2] = 40;
			iArray[8][3] = 0;
			
			iArray[9] = new Array();
			iArray[9][0] = "是否有效人力";
			iArray[9][1] = "40px";
			iArray[9][2] = 40;
			iArray[9][3] = 0;
			
			iArray[10] = new Array();
			iArray[10][0] = "保单号";
			iArray[10][1] = "40px";
			iArray[10][2] = 40;
			iArray[10][3] = 0;
			
			iArray[11] = new Array();
			iArray[11][0] = "险种";
			iArray[11][1] = "40px";
			iArray[11][2] = 40;
			iArray[11][3] = 0;
			
			iArray[12] = new Array();
			iArray[12][0] = "FYC";
			iArray[12][1] = "40px";
			iArray[12][2] = 40;
			iArray[12][3] = 0;
			
			YearAudDeTailsGrid = new MulLineEnter("fm","YearAudDeTailsGrid");
		     
			YearAudDeTailsGrid.mulLineCount = 10;   
			YearAudDeTailsGrid.displayTitle = 1;
			YearAudDeTailsGrid.canSel = 1;
			YearAudDeTailsGrid.canChk = 0;
			YearAudDeTailsGrid.locked = 1;
			YearAudDeTailsGrid.hiddenSubtraction = 1;
			YearAudDeTailsGrid.hiddenPlus = 1;
			YearAudDeTailsGrid.selBoxEventFuncName ="";
		     
			YearAudDeTailsGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>