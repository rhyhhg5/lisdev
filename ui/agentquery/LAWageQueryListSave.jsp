<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：
//程序功能：
//创建人  : 樊霆
//创建日期：2010-8-9
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.agentbranch.*"%>
  <%@page import="com.sinosoft.lis.agentquery.*"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>
  <%@page import="com.sinosoft.lis.vbl.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.f1print.*"%>
  <%@page import="com.sinosoft.lis.brieftb.*"%>
  <%@page import="java.io.*"%>
<%
	  System.out.println("开始执行打印操作");
	  String Content = "";
	  CErrors tError = null;
	  String FlagStr = "Fail";
	  boolean operFlag=true;
	  //接受前台传输过来的数据
	  String mManageCom = request.getParameter("ManageCom");
	  String mStartMonth = request.getParameter("StartMonth");
	  String mEndMonth = request.getParameter("EndMonth");
	  String mMonth = request.getParameter("Month");
	  String mContNo = request.getParameter("ContNo");
	  String mRiskCode = request.getParameter("RiskCode");
	  String mAgentCode = request.getParameter("AgentCode");
	  String mAgentName = request.getParameter("AgentName");
	  //WrapCode 对应与LACommision表中的F3字段.
	  String mWrapCode = request.getParameter("WrapCode");
    mAgentName=StrTool.unicodeToGBK(mAgentName);
  	GlobalInput tG = new GlobalInput();	
	  tG=(GlobalInput)session.getValue("GI");
  	CErrors mErrors = new CErrors();
  	//WageQueryListUI tWageQueryListUI = new WageQueryListUI();
  	WageQueryListReport tWageQueryListReport = new WageQueryListReport();
  	//WageQueryListReport tWageQueryListReport = new WageQueryListReport();
  	XmlExport txmlExport = new XmlExport();
    //将数据封装起来
    VData tVData = new VData();
  	VData mResult = new VData();
  	try{		    
		    tVData.addElement(mManageCom);
		    tVData.addElement(mStartMonth);
		    tVData.addElement(mEndMonth);
		    tVData.addElement(mMonth);
		    tVData.addElement(mContNo);
		    tVData.addElement(mRiskCode);
		    tVData.addElement(mAgentName);
		    tVData.addElement(mAgentCode);
		    tVData.addElement(mWrapCode);
		    tVData.addElement(tG);
    
		    //调用批单打印的类
		    System.out.println("...................WageQueryListSave1:here begin java");
		    if (!tWageQueryListReport.submitData(tVData,"PRINT")){
			      operFlag=false;
			      Content=tWageQueryListReport.mErrors.getFirstError().toString();    
		    }else{
		    		mResult = tWageQueryListReport.getResult();
			      txmlExport=(XmlExport)mResult.getObjectByObjectName("XmlExport",0);
			      if(txmlExport==null){
			      		operFlag=false;
			      		Content="没有得到要显示的数据文件";
			      }
		    }
 	  }catch(Exception ex){
    		Content = "打印失败，原因是:" + ex.toString();
    		FlagStr = "Fail";
  	}
  	
  	
		ExeSQL tExeSQL = new ExeSQL();
		//获取临时文件名
		//System.out.println("..................here1");
		String strSql = "select SysVarValue from ldsysvar where Sysvar='VTSFilePath'";
		String strFilePath = tExeSQL.getOneValue(strSql);
		String strVFFileName = strFilePath + tG.Operator + "_" + FileQueue.getFileName()+".vts";
		System.out.println("strFilePath========"+strFilePath);
		System.out.println("strVFFileName========"+strVFFileName);
		//获取存放临时文件的路径
		//System.out.println("..................here2");
		String strRealPath = application.getRealPath("/").replace('\\','/');
		System.out.println("strRealPath========"+strRealPath);
		String strVFPathName = strRealPath +"/"+ strVFFileName;
		System.out.println("strVFPathName========"+strVFPathName);
		CombineVts tcombineVts = null;
		
		if (operFlag==true){
			//合并VTS文件
			String strTemplatePath = application.getRealPath("f1print/picctemplate/") + "/";
			tcombineVts = new CombineVts(txmlExport.getInputStream(),strTemplatePath);
			ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
			tcombineVts.output(dataStream);
			//把dataStream存储到磁盘文件
			System.out.println("..................here4");
			AccessVtsFile.saveToFile(dataStream,strVFPathName);
			System.out.println("==> Write VTS file to disk ");		
			System.out.println("===strVFFileName : "+strVFFileName);
			//本来打算采用get方式来传递文件路径
			response.sendRedirect("../f1print/GetF1PrintJ1_new.jsp?RealPath="+strVFPathName);
		}else{
			FlagStr = "Fail";
%>
		<html>
		<script language="javascript">
			alert("<%=Content%>");
			top.opener.focus();
			top.close();				
		</script>
		</html>
<%
		}
%>