<%@include file="../common/jsp/AgentCheck.jsp"%>
<% 
//程序名称：LAPresenceQryInput.jsp
//程序功能：
//创建日期：2003-10-22
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LAPresenceQryInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LAPresenceQryInit.jsp"%>
  <title>考勤信息查询 </title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">  
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPresence);">
            </td>
            <td class= titleImg>
                考勤信息查询
            </td>            
    	</tr>
    </table>
  <Div  id= "divPresence" style= "display: ''">
  <table  class= common>
      <TR  class= common> 
        <TD class= title> 
          代理人编码 
        </TD>
        <TD  class=input> 
          <Input class=common  name=AgentCode readonly=true >
        </TD>
        <td class=input colspan=2><INPUT class=common VALUE="查  询" TYPE=button onclick="easyQueryClick();"></td>		            
      </TR>         
   </table>                    	   
  </Div>      
          				
    <table>
      <tr class=common>
       <td class=common>
		<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divPresenceGrid);">
       </td>
       <td class= titleImg>
    	 代理人考勤信息
       </td>
      </tr>
    </table>
  	<Div  id= "divPresenceGrid" style= "display: ''">
      <table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanPresenceGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<table>
    		<INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
			<INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
			<INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
			<INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();"> 						
		</table>
  	</div>
      <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
