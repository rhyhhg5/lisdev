<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>   
<%
//程序名称：BKBranchQuery.jsp
//程序功能：
//更新记录：  更新人    更新日期     更新原因/内容
%>     
<%
  GlobalInput tGlobalInput = new GlobalInput(); 
  tGlobalInput = (GlobalInput)session.getValue("GI");
  String Branchattr = request.getParameter("Branchattr");
  System.out.println(Branchattr);
  
%>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=GBK">	  
  <SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
  <%@include file="../agent/SetBranchType.jsp"%>
  <%@include file="../common/jsp/ManageComLimit.jsp"%>
	<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>	
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>	
	<SCRIPT src="BKBranchQuery.js"></SCRIPT>
	<%@include file="BKBranchQueryInit.jsp"%>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>	
	<SCRIPT src="../common/EasyScanQuery/ShowPicControl.js"></SCRIPT>
  <SCRIPT>window.document.onkeydown = document_onkeydown;</SCRIPT>
</head>

<script>
  var tBranchAttr = "<%=Branchattr%>";
  //alert(tBranchAttr);
</script>


<body onload="initForm();  parent.fraInterface.showCodeName();">
  <form action="./BKBranchQuerySave.jsp" method=post name=fm target="fraSubmit">
    <Div  id= "divButton" style= "display: ''">
    <%@include file="../common/jsp/ProposalOperateButton.jsp"%>
    </DIV>
    <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLABranchGroup1);">
    </IMG>
      <td class=titleImg>
      销售单位
      </td>
    </td>
    </tr>
    </table>
    <Div  id= "divRiskCode0">
    <table class=common>
    <TR  class= common>        
          <TD  class= title>
            分支机构代码
          </TD>
          <TD  class= input>
            <Input class=common name=BranchAttr >
          </TD>
          <TD  class= title>
            分支机构名称
          </TD>
          <TD  class= input>
            <Input class= common name=BranchName >
          </TD>	 
     </TR>
     <TR  class= common>
          <TD  class= title>
            所属管理机构
          </TD>
          <TD  class= input>
            <Input class="code" name=ManageCom  ondblclick="showCodeList('comcode',[this]);" onkeyup="return showCodeListKey('comcode',[this]);"> 
          </TD> 
	        <TD  class= title>
            上一级机构代码
          </TD>
          <TD  class= input>
            <Input class=common name=UpBranchAttr onChange=Checkattr();>
          </TD>
     </TR>
     <TR  class= common>
          <TD  class= title>
            级别
          </TD>
          <TD  class= input>
            <Input class= 'code' name=BranchLevel 
		                               ondblclick="showCodeList('<%=tSql1%>',[this],[0,1],null,'<%=tSqlBranchLevel%>','1');" 
                                   onkeyup="return showCodeListKey('<%=tSql1%>',[this],[0,1],null,'<%=tSqlBranchLevel%>','1');" >
          </TD>
          <TD  class= title>
            工作属性
          </TD>
          <TD  class= input>
            <Input class= 'code' name=FieldFlag 
		             ondblclick="showCodeList('outside',[this]);" 
                 onkeyup="return showCodeListKey('outside',[this]);" >
          </TD>
      </TR>
      <TR  class= common>
	        <TD  class= title>
            管理人员代码
          </TD>
          <TD  class= input>
            <Input class="readonly"readonly name=BranchManager >
          </TD>
          <TD  class= title>
            管理人员姓名
          </TD>
          <TD  class= input>
            <Input class="readonly"readonly name=BranchManagerName >
          </TD>
      </TR>
      <TR  class= common>
	        <TD  class= title>
            地址
          </TD>
          <TD  class= input>
            <Input class= common name=BranchAddress >
          </TD>
          <TD  class= title>
            邮政编码
          </TD>
          <TD  class= input>
            <Input class= common name=BranchZipcode verify="展业机构邮编|len<=6" >
          </TD>        
      </TR>
      <TR  class= common>
	        <TD  class= title>
            电话
          </TD>
          <TD  class= input>
            <Input class= common name=BranchPhone >
          </TD>
          
          <TD  class= title>
            成立日期
          </TD>
          <TD  class= input>
            <Input class= 'coolDatePicker' name=FoundDate format='short'>
          </TD>
      </TR>
      <TR  class= common>
          <TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='code' name=EndFlag  ondblclick="showCodeList('yesno',[this]);" 
                                              onkeyup="return showCodeListKey('yesno',[this]);">
          </TD>
	        <TD  class= title>
            停业日期
          </TD>
          <TD  class= input>
            <Input class='coolDatePicker' name=EndDate format='short'>
          </TD>
        </TR> 
        <TR  class= common>
          <TD  class= title>
            主管属性
          </TD>
          <TD  class= input>
            <Input class=common name=AttrManager  >
          </TD>
           </TR>         
    </table>
    </Div>
    <Div  id= "divALL0" style= "display: 'none'">
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  <span id="spanApprove"  style="display: none; position:relative; slategray"></span>
</body>
</html>
