<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LAComQueryInput.jsp
//程序功能：
//创建日期：2002-08-16 15:12:44
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
  GlobalInput tG = new GlobalInput();
  tG=(GlobalInput)session.getValue("GI");
  String BranchType=request.getParameter("BranchType");
  String BranchType2=request.getParameter("BranchType2");
  System.out.println("BranchType:"+BranchType);
  System.out.println("BranchType2:"+BranchType2);
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LABankLicenseInput.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LABankLicenseInit.jsp"%>
  <title>银代兼业代理机构许可证查询报表 </title>
</head>
<body  onload="initForm();initElementtype();" >
  <form action="./LABankLicenseSave.jsp" method=post name=fm target="f1print">
    <table>
      <tr class=common>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBankLicense);"></IMG></td>
        <td class=titleImg> 查询条件 </td>
      </tr>
    </table>
    <Div  id= "divBankLicense" style= "display: ''">
    <table  class= common>
    	 <tr  class= common>
        <td  class= title> 管理机构 </td>
        <td  class= input><Input class="codeno" name=ManageCom verify="管理机构|code:comcode&NOTNULL"
          ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
          onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
         ><Input class=codename name=ManageComName readOnly elementtype=nacessary>
         </td> 
            <!--td  class= title> 代理机构类别 </td>
        <td  class= input> <input name=ACType class=codeno CodeData="0|^1|专业代理|^2|兼业代理|^3|其他" ondblClick="showCodeListEx('ACType',[this,ACTypeName],[0,1])" onkeyup="showCodeListKeyEx('ACType',[this,ACTypeName],[0,1])" ><Input class=codename name=ACTypeName readOnly > </td--> 
      	<TD  class= title>
            停业属性
          </TD>
          <TD  class= input>
            <Input class='codeno' name=EndFlag  CodeData = "0|^Y|是|^N|否" 
             ondblclick="return showCodeListEx('EndFlag',[this,EndFlagName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('EndFlag',[this,EndFlagName],[0,1]);"
             ><Input class=codename name=EndFlagName readOnly>
          </TD>
      </tr>
      <tr  class= common> 
        <td  class= title> 代理机构编码  </td>
        <td  class= input> <input class=code name=AgentCom 	
 	           ondblclick="return showCodeList('AgentCom',[this,Name],[0,1],null,'1 and BranchType=#3# and BranchType2=#01# and actype=#01#',1,1);" 
		         onkeyup="return showCodeListKey('AgentCom',[this,Name],[0,1],null,'1 and BranchType=#3# and BranchType2=#01# and actype=#01#',1,1);"></td>
        <td  class= title> 代理机构名称 </td>
        <td  class= input> <input class=common  name=Name > </td>
      </tr>
      <!--tr  class= common>
        <td  class= title> 联系人 </td>
        <td  class= input> <input  class= common name=LinkMan> </td>
	      <td  class= title> 单位性质 </td>
        <td  class= input> <input   name=GrpNature class='codeno'
		           ondblclick="return showCodeList('GrpNature',[this,GrpNatureName],[0,1]);" 
		           onkeyup="return showCodeListKey('GrpNature',[this,GrpNatureName],[0,1]);"><Input class=codename name=GrpNatureName readOnly ></td>
      </tr-->     
      <tr  class= common>
	      <td  class= title> 级别 </td>
        <td  class= input> <input name=BankType class='codeno' 
        	   ondblclick="return showCodeList('BankType2',[this,BankTypeName],[0,1]);" 
		         onkeyup="return showCodeListKey('BankType2',[this,BankTypeName],[0,1]);"><Input class=codename name=BankTypeName readOnly > </td> 
	      <td  class= title> 销售资格 </td>
        <td  class= input> <input  name=SellFlag class= 'codeno'
		           CodeData = "0|^Y|有|^N|无" 
             ondblclick="return showCodeListEx('SellFlag',[this,SellFlagName],[0,1]);" 
             onkeyup="return showCodeListKeyEx('SellFlag',[this,SellFlagName],[0,1]);"><Input class=codename name=SellFlagName readOnly > 
	      </td>
      </tr>
     
       <tr  class= common>
          <TD  class= title>
            网点负责人代码
          </TD>
          <TD  class= input>
            <Input class= common name=AgentCode >
          </TD> 
           <TD  class= title>
            网点负责人名称
          </TD>
          <TD  class= input>
            <Input class= common name=AgentName >
          </TD> 
            </tr> 
            
         <TR  class = common>
           <TD  class = title>
           许可证到期日查询起期
          </TD>
          <TD  class= input>
            <Input name=StartDate class='coolDatePicker' dateFormat="short"> 
          </TD>
          <TD  class = title>
            许可证到期日查询止期
          </TD>
          <TD  class= input>
            <Input name=EndDate class='coolDatePicker' dateFormat="short">  
          </TD>
            
        <tr  class= common>
           <td  class= title> 
           保险兼业代理资格证号 
           </td>
        <td  class= input>
         <input  class= common name=BusiLicenseCode > 
         </td>
      </tr> 
            
   </table>
   <BR>
    </Div>
    <div style="display: none">
    <TD  class= title>
            网点信息
        </TD>
        <TD  class= input><Input class='common' name=BranchType ></TD>
        <TD  class= input><Input class='common' name=BranchType2 ></TD>
    </div>
    <INPUT VALUE="查  询" class = cssButton TYPE=button onclick="easyQueryClick();"> 
    <INPUT VALUE="下  载"  class = cssButton TYPE=button onclick="ListExecl();">					
   <BR>
   <BR>
    <table>
      <tr>
        <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divBankLicenseGrid);"></td>
    	<td class= titleImg> 查询结果 </td>
      </tr>
    </table>
    <Div  id= "divSpanBankLicenseGrid" style= "display: ''">
      <table  class= common>
       	<tr  class= common>
      	  <td text-align: left colSpan=1>
  	    <span id="spanBankLicenseGrid" >
  	    </span> 
  	  </td>
  	</tr>
      </table>
      <input type=hidden class=Common name=querySql > 
      <input type=hidden class=Common name=querySqlTitle >
      <input type=hidden class=Common name=Title > 
      <INPUT VALUE="首页" class = cssButton  TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" class = cssButton TYPE=button onclick="turnPage.lastPage();"> 					
    </div>
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
  </form>
</body>
</html>
