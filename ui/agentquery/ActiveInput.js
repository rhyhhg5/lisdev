//   该文件中包含客户端需要处理的函数和事件
var mDebug="0";
var showInfo;
var turnPage = new turnPageClass();
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}

//Click事件，当点击“查询”图片时触发该函数
function easyQueryClick()
{ 
      initPolGrid();
  //下面增加相应的代码
	//alert("wwwwwwwww")
	/*var strsql="select distinct a.wageno,a.managecom, labranchgroup.name,b.groupagentcode,b.name,a.riskcode," +
			"c.riskname,a.contno, a.transmoney,d.rate,d.money from lacommision a,laagent b,lmriskapp c,lacontfycrate d,labranchgroup "
	        +"where a.agentcode=b.agentcode and a.riskcode=c.riskcode and "
			+ "c.riskcode=d.riskcode  and b.AgentGroup=labranchgroup.AgentGroup" 
				+getWherePart('wageno',"WageNo")
				+getWherePart('labranchgroup.name',"BranchAttrName")
				+getWherePart('contno','GrpContNo')
				+getWherePart('a.riskcode','RiskCode')
				+getWherePart('groupagentcode','GroupAgentCode')
				+"with ur";
	//alert("eee:"+strsql);
	var arrResult=new Array();
	//alert("rrrrrrrr");
	turnPage.queryModal(strsql ,PolGrid);*/
	//alert("qqqqqqqqqqq");
      if (verifyInput() == false)
    	  return false;
    	  var whereSql =" and 1=1 ";
    	  var grpcontno = fm.all('GrpContNo').value;
    	  if(grpcontno!=""&&grpcontno!=null){
    		  whereSql += " and (grpcontno ='"+grpcontno+"' or contno='"+grpcontno+"')";
    	  }
    	  //alert(fm.all('ManageCom').value);
    	strSQL =
    		"select wageno,managecom,agentgroup,"
 		+"(select name from labranchgroup where a.branchattr = branchattr fetch first 1 rows only), "
 		+"getUniteCode(agentcode),(select name from laagent where a.agentcode = agentcode),"
    		+"(select laagentgrade.gradename from latree,laagentgrade where laagentgrade.gradecode=latree.agentgrade and a.agentcode =agentcode),"
    		+"riskcode,(select riskname from lmriskapp where a.riskcode = riskcode ),"
    		+"contno,sum(transmoney),fycrate,sum(fyc) from "
    		+"(select wageno,managecom,agentgroup,branchattr,agentcode,riskcode,(case when grpcontno='00000000000000000000' then contno else grpcontno end ) contno,transmoney,fycrate,fyc from "
    		+"lacommision where branchtype ='5' and branchtype2='01' and exists(select wageno from lawage where lacommision.agentcode=lawage.agentcode and lacommision.wageno= lawage.indexcalno and state='1')" 
    		+getWherePart('ManageCom','ManageCom','like')
    		+getWherePart('WageNo','WageNo')
    		+getWherePart('RiskCode','RiskCode')
    		+getWherePart('BranchAttr','BranchAttr')
    		+getWherePart('getUniteCode(agentcode)','GroupAgentCode')
    		+whereSql
    		+") a "
    		+"group by  wageno,managecom,agentgroup,agentcode,riskcode,fycrate,branchattr,contno"
    		+" order by wageno,managecom,agentcode,contno,riskcode with ur";
//alert(strSQL);
    	  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    	  if (!turnPage.strQueryResult)
    	  {
    	  	alert('不存在符合条件的有效纪录！');
    	  	return false;
    	  }
    	  //查询成功则拆分字符串，返回二维数组

    	  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    	  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
    	  turnPage.pageDisplayGrid = PolGrid;
    	  //保存SQL语句
    	  turnPage.strQuerySql     = strSQL;
    	  //设置查询起始位置
    	  turnPage.pageIndex       = 0;
    	  //在查询结果数组中取出符合页面显示大小设置的数组
    	  //arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    	  var tArr = new Array();
    	  tArr = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, MAXSCREENLINES);
    	  //调用MULTILINE对象显示查询结果
    	  displayMultiline(tArr, turnPage.pageDisplayGrid);
    	  fm.all('querySql').value=strSQL;
}
//Click事件，当点击“下载”图片时触发该函数
function ListExecl()
{
	if (verifyInput() == false)
	    return false;
	//alert( fm.querySql.value);
	  var sql = fm.querySql.value;
	  if(sql==""||sql==null)
	  {
		  alert("请先查询");
		  return false;
	  }
	
	  var oldAction = fm.action;
	  fm.action = "./ActiveSave.jsp";
	  fm.submit();
	  fm.action = oldAction;
  /*var i = 0;
  var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //showSubmitFrame(mDebug); 
	//fm.fmtransact.value = "PRINT";
	fm.target = "f1print";
	//fm.all('hideOperate').value = 'download';
	fm.submit();
	showInfo.close();*/
}

function afterSubmit(FlagStr, content)
{ showInfo.close();
if (FlagStr == "Fail" )
{
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  fm.hideOperate.value = "";
}
else
{
  var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  fm.hideOperate.value = "";
 }      
}

