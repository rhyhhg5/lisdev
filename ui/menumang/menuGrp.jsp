<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
GlobalInput tG1 = new GlobalInput();
tG1=(GlobalInput)session.getValue("GI");
String Operator = tG1.Operator;;
%>
<html>
<%
//程序名称：menuGrp.jsp
//程序功能：菜单组的输入
//创建日期：2002-10-10
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<%@page import = "java.util.*"%>
<%@page import = "com.sinosoft.utility.*"%>

<head>
<!--
<style>
#foldheader{cursor:hand ; font-weight:bold ;
list-style-image:url(butCollapse.gif)}
#foldinglist{list-style-image:url(butCollapse.gif)}
</style>
<script language="JavaScript1.2">
var head="display:''"
img1=new Image()
img1.src="butCollapse.gif"
img2=new Image()
img2.src="butCollapse.gif"
</script>
-->
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="menuGrp.js"></SCRIPT>
<script src="treeMenu.js"></script>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="menuGrpInit.jsp"%>
</head>
<body  onload="initForm();" >
<form action="./menuGrpMan.jsp" method=post name=fm target="fraSubmit">
   <Table class="common">
      <TR class= common style="display:none">
          <TD  class= title>
            操作员
          </TD>
          <TD  class= input>
            <Input class=common name=Operator value=<%=Operator%> >
          </TD>
      </TR>
      <TR  class= common>
          <TD  class= title>
            菜单组编码
            <!--<textarea name=tt></textarea>-->
          </TD>
          <TD  class= input>
            <Input class=common name=MenuGrpCode maxlength="5">
          </TD>
          <TD class= title>
            菜单组名称
          </TD>
          <TD  class= input>
            <Input class=common name=MenuGrpName >
          </TD>
      </TR>
      <TR class = common>
         <TD  class= title>
            菜单组描述
          </TD>
          <TD  class= input>
            <Input class=common name=MenuGrpDescription >
          </TD>
          <TD class= title>
            菜单标志
          </TD>
          <TD  class= input>
            <Input class=common name=MenuSign >
          </TD>

      </TR>
    </Table>
  <Div id= divCmdButton style="display: ''">
     <INPUT VALUE="查询菜单组" TYPE=button onclick="queryClick()" class="cssButton">
     <INPUT VALUE="增加菜单组" TYPE=button onclick="insertClick()" class="cssButton">
     <INPUT VALUE="更新菜单组" TYPE=button onclick="updateClick()" class="cssButton">
    <INPUT VALUE="删除菜单组" TYPE=button onclick="deleteClick()" class="cssButton">
  </Div>
  <Div  id= "divQueryGrp" style= "display: ''">
    <table>
      <td class= titleImg>
    	 菜单组表
      </td>
    </table>
     <table  class= common>
        <tr>
    	  	<td text-align: left colSpan=1>
	         <span id="spanQueryGrpGrid" ></span>
		</td>
	</tr>
     </table>
        <INPUT VALUE="首  页" TYPE=button  onclick="userFirstPage()" class="cssButton">
        <INPUT VALUE="上一页" TYPE=button  onclick="userPageUp()" class="cssButton">
        <INPUT VALUE="下一页" TYPE=button  onclick="userPageDown()" class="cssButton">
        <INPUT VALUE="尾  页" TYPE=button  onclick="userLastPage()" class="cssButton">
</div>
 <Div id= divSubCmdButton style="display: none">
     <INPUT VALUE="确  定" TYPE=button  onclick="okClick()" class="cssButton">
     <INPUT VALUE="退  出" TYPE=button  onclick="cancelClick()" class="cssButton">
  </Div>
  <Div  id= "divmenuGrid2" style= "display: none">
   <table class=common>
   <tr>
      <td class= titleImg>
    	 菜单组已有菜单表
      </td>
      <td>
        <input VALUE="<-" TYPE=button onclick="addMenus()" class="cssButton">
        <input value="->" type=button onclick="removeMenus()" class="cssButton">
      </td>
      <td class= titleImg>
    	 菜单组未有菜单表
      </td>
     </tr>
    </table>
	<table class=common>
    <tr>
	    <span id="spanSelectMenuGrpTree" style="display: ''; position:absolute; slategray"></span>
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	         <span id="spanunSelectMenuGrpTree" style="display: ''; position:absolute; slategray"></span>
	        <td text-align: left colSpan=1">
	         <span id="spanHideMenuGrpGrid1" style="display: none"></span>
	         <span id="spanHideMenuGrpGrid2" style="display: none"></span>
	         <Input class=common name=hideString style="display:none">
		 <Input class=common name=hideRemoveString style="display:none">
		    </td>
	    </tr>
		<tr>
		<TD  class= input style= "display: none">
                 <Input class="code" name=hide1  >
                 <Input class="code" name=hide2 >
                 <Input class="code" name=Action>
        	</TD>
		</tr>
     </table>
</Div>
 <input type="hidden" name="UserStr" value="<%=userStr%>">
 <input type="hidden" name="MenuOpe" value="">
 
</form>
 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>