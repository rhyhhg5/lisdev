<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.lis.pubfun.GlobalInput"%>
<%
//程序名称：menuGrp.jsp
//程序功能：菜单组的输入
//创建日期：2002-10-10
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
  GlobalInput tG1 = new GlobalInput();
	tG1=(GlobalInput)session.getValue("GI");
	String Operator = tG1.Operator;;
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="menuFunInit.jsp"%>
  <SCRIPT src="menuFunGrp.js"></SCRIPT>
  <script src="treeMenu.js"></script>
  
</head>

<body  onload="initForm();" >
<form action="./menuFunMan.jsp" method=post name=fm target="fraSubmit">
   <table class= common >
      <TR  class= common>
          <TD  class= title> 菜单节点名称</TD>
          <TD  class= input><Input class=common name=NodeName1 > </TD>
          <TD  class= title> 父节点名称</TD>
          <TD  class= input><Input class=common name=ParentNodeName1 > </TD>
          <TD class= title> 映射文件</TD>
          <TD  class= input> <Input class=common name=RunScript1 ></TD>
      </TR> 
   </Table>
   <INPUT VALUE="查询菜单" TYPE=button class="cssButton" onclick="queryClick()">
	<br><br>
  <Div  id= "divQueryGrp" style= "display: ''">
    <table>
      <td class= titleImg>
    	 菜单列表
      </td>
    </table>

     <table  class= common>
        <tr>
    	  	<td text-align: left colSpan=1>
	         <span id="spanQueryGrpGrid" ></span>
		</td>
	</tr>
     </table>
        <Div  id= "divPage" align=center style= "display: 'none' ">
		      <INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage.firstPage();"> 
		      <INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
		      <INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
		      <INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();">
	      </Div>
	</div>
	
	<hr>
	
	<table class= common >
      <TR  class= common>
          <TD  class= title> 菜单节点名称</TD>
          <TD  class= input><Input class=common name=NodeName > </TD>
          <TD class= title> 映射文件</TD>
          <TD  class= input> <Input class=common name=RunScript  ></TD>
      </TR> 
    </Table>
  <p>
  <Div id= divCmdButton style="display: ''">
     
     <INPUT VALUE="平行向上插入" TYPE=button class="cssButton" onclick="insertUpClick()">
     <INPUT VALUE="平行向下插入" TYPE=button class="cssButton" onclick="insertDownClick()">
     <INPUT VALUE="做为下级插入" TYPE=button class="cssButton" onclick="insertInnerClick()">
     <INPUT VALUE="删除选择结点" TYPE=button class="cssButton" onclick="deleteClick()"> 
     <INPUT VALUE="刷新左部菜单" TYPE=button class="cssButton" onclick="flashClick()"> 
     <INPUT VALUE="修 改 菜 单" TYPE=button class="cssButton" onclick="updateClick()"> 
  </Div>
	
	<Input type=hidden name=Action>
	<Input type=hidden name=isChild>
	<Input type=hidden name=SelNodeCode >	
	<Input type=hidden name=SelParentNodeCode >
</form>

 <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
