
var showInfo;

//控制界面上的mulLine的显示条数
var mulLineShowCount = 15;

var sqlcount = 0;

var selectArray = new Array();
var unselectArray = new Array();

var userArray = new Array();
var userArrayLen = 0;
var userCurPage = 0;

var removeArray = new Array() //更新菜单组中的删除纪录
var showDiv = "false";

var turnPage = new turnPageClass();

function change()
{
	if(!document.all)
		return;

	if (event.srcElement.type=="checkbox") {
		tArray = searchArray(event.srcElement.id);
       		tagInArray(tArray,event.srcElement.id);
	}

	if (event.srcElement.name=="foldheader") {
		tArray = searchArray(event.srcElement.id);
		var srcIndex = event.srcElement.sourceIndex;
		var nested = document.all[srcIndex+1];
		if (nested.style.display=="none") {
			nested.style.display='';
		} else {
			nested.style.display="none";
		}
		nested = document.all[srcIndex+2];
		if (nested.style.display=="none") {
			nested.style.display='';
		} else {
			nested.style.display="none";
		}

		tagShowlistInArray(tArray,event.srcElement.id);
	}
}

document.onclick=change;

function initUserArray()
{
	var userArray = new Array();
	var userArrayLen = 0;
	var userCurPage = 0;
}

function fillUserGrid()
{
   QueryGrpGrid.clearData("QueryGrpGrid");
 // alert(mulLineShowCount);
   for (var i = 0; i < mulLineShowCount; i++) {

        QueryGrpGrid.addOne("QueryGrpGrid");
   	var offset = i  + userCurPage*mulLineShowCount;

   	if (offset < userArrayLen) {
   	    QueryGrpGrid.setRowColData(i,1,userArray[offset][0]);
   	    QueryGrpGrid.setRowColData(i,2,userArray[offset][1]);
   	    QueryGrpGrid.setRowColData(i,3,userArray[offset][3]);
   	    QueryGrpGrid.setRowColData(i,4,userArray[offset][2]);
   	    QueryGrpGrid.setRowColData(i,5,userArray[offset][4]);
//   	    alert(userArray[offset][4]);
   	} else {
   	    QueryGrpGrid.setRowColData(i,1,"");
   	    QueryGrpGrid.setRowColData(i,2,"");
            QueryGrpGrid.setRowColData(i,3,"");
   	    QueryGrpGrid.setRowColData(i,4,"");
   	    QueryGrpGrid.setRowColData(i,5,"");
   	}
   }

   //下面的代码是为了使翻页时序号能正确显示
   for (var i = 0; i < mulLineShowCount; i++) {
		var offset = i  + userCurPage*mulLineShowCount;
        fm.all("QueryGrpGridNo")[i].value = offset + 1;
   }

}

function userFirstPage()
{
	if (userArrayLen == 0)
	    return;

	userCurPage = 0;
	fillUserGrid();
}

function userLastPage()
{
	if (userArrayLen == 0)
	    return;

	while ((userCurPage + 1)*mulLineShowCount < userArrayLen)
	    userCurPage++;

	fillUserGrid();
}


function userPageDown()
{
	if (userArrayLen == 0)
	    return;

    if (userArrayLen <= (userCurPage + 1) * mulLineShowCount) {
    	alert("已达尾页");
    } else {
        userCurPage++;
        fillUserGrid();
    }
}

function userPageUp()
{
	if (userArrayLen == 0)
	    return;

    if (userCurPage == 0) {
    	alert("已到首页");
    } else {
        userCurPage--;
        fillUserGrid();
    }
}


function searchArray(id)
{
//	alert(id);
	for (var i = 0; i < selectArray.length; i++) {
		var aryId1 = "chk_" + selectArray[i][8];
		var aryId2 = "fld_" + selectArray[i][8];
		if (aryId1 == id || aryId2 == id)
		    return selectArray;
    	}

	for (var i = 0; i < unselectArray.length; i++) {
		var aryId1 = "chk_" + unselectArray[i][8];
		var aryId2 = "fld_" + unselectArray[i][8];
		if (aryId1 == id || aryId2 == id)
		    return unselectArray;
    	}
}

//提交前进行必要的检查
function beforeSubmit()
{
    if (fm.all('MenuGrpCode').value =="") {
        alert("菜单组编码必须填写");
        return false;
    }
    return true;
}

function resetForm()
{
	fm.all('MenuGrpCode').value = "";
	fm.all('MenuGrpName').value = "";
	fm.all('MenuGrpDescription').value = "";
	fm.all('MenuSign').value = "";
}

function deleteClick()
{
	var selMenuGrpNo = QueryGrpGrid.getSelNo();
        if (selMenuGrpNo == 0) {
	  alert("您还没有选择需要删除的菜单组");
	  return;
	}

	var thisOperator = fm.all("Operator").value;
  var Operator = "'"+QueryGrpGrid.getRowColData(selMenuGrpNo - 1,5)+"'";
  
  //var thisOperator = fm.all("Operator").value;
  var userStr = fm.all("UserStr").value;
  if(userStr.indexOf(Operator)<0)
  {
  	alert("您无权删除此菜单组");
  	return;
  }
  /*
   if (thisOperator != Operator) {
   alert("您无权删除此菜单组");
   return;
   }
	*/
	if (!confirm("您确实要删除这个菜单组吗？"))
	  return;

	fm.all("Action").value = "delete";

    submitForm();
}


function queryClick()
{
    QueryGrpGrid.clearData();
    userCurPage = 0;
		
    var MenuGrpName = fm.all("MenuGrpName").value;
    var MenuGrpCode = fm.all("MenuGrpCode").value;
    var MenuGrpDescription = fm.all("MenuGrpDescription").value;
    var MenuSign = fm.all("MenuSign").value;
		var userStr = fm.all("UserStr").value;
		var operator = fm.all("Operator").value;
		
		//alert(operator);
		
    sqlcount++;
    
    var sqlStr1="select b.Menugrpcode from ldusertomenugrp a ,LDMenuGrp b "
				+ " where a.menugrpcode=b.menugrpcode and usercode='"+ operator +"' order by a.usercode";
    var strTemp1 = easyExecSql(sqlStr1, 1, 0, 1);
    var menugrpStr="''";
    
    for (var i=0;i<strTemp1.length;i++)
    {
    	menugrpStr=menugrpStr+", '"+strTemp1[i]+"'";
    }
    //alert(menugrpStr);
  	//modify by fuxin 2011/12/5  由于菜单组过多需要支持模糊查询
  	//处理查询条件
  	var tCondition = "";
    if (MenuGrpName != "")
    	tCondition += "and MenuGrpName like '%" + MenuGrpName + "%'"; 

    if (MenuGrpCode != "")
    	tCondition += "and MenuGrpCode like '%" + MenuGrpCode + "%'";

    if (MenuGrpDescription != "")
    	tCondition += "and MenuGrpDescription like '%" + MenuGrpDescription + "%'";

    if (MenuSign != "")
    	tCondition += "and MenuSign  like '%" + MenuSign + "%'";
    	
    var sqlStr;
    if(operator=="001"||operator=="it001") //如果001和it001则可查看全部菜单组
    {
    	sqlStr = "select MenuGrpName,MenuGrpCode,MenuGrpDescription,MenuSign,Operator from LDMenuGrp where " 
	    + sqlcount + "=" + sqlcount + " "
	    + tCondition;
  	}
  	else //否则只可查看自己创建,下属创建和自己属于的菜单组 
  	{
  		sqlStr = "select MenuGrpName,MenuGrpCode,MenuGrpDescription,MenuSign,Operator from LDMenuGrp where " 
	    + sqlcount + "=" + sqlcount + " and "
	    + "(MenuGrpCode in(" + menugrpStr +") "
	    + "or Operator in ("+userStr+"))"
	    + tCondition; 
	    //alert(sqlStr);
  	}

		//modify qulq 2007-8-22 16:52
		sqlStr += " order by MenuGrpCode ";
   //modifydate-2009-06-15 sgh
 /* turnPage1.queryModal(sqlStr, QueryGrpGrid,1);
   if (!turnPage1.strQueryResult) 
	  {
	    alert("没有符合条件的查询信息！");
	    return false;
	  }
	//alert(sqlStr);	
*/
    var strTemp =  easyQueryVer3(sqlStr,1,0,1,1);
    //alert(strTemp);
    var tempArray = new Array;
    userArray = decodeEasyQueryResult(strTemp,0,1);
   
    if (userArray == null) {
    	alert("没有找到您指定的菜单组。");
    	return;
    }
    
    userArrayLen = userArray.length;

    fillUserGrid();
 
}

function insertClick()
{
	resetForm();

	//隐去需要的元素
	divQueryGrp.style.display = "none";
	divCmdButton.style.display = "none";

	showAllMenuInUnselect();

	//显示需要的元素
	divSubCmdButton.style.display = "";
	divmenuGrid2.style.display= "";

	fm.all("Action").value = "insert";
}

function showAllMenuInUnselect(){
	var sqlStr = "select ParentNodeCode,ChildFlag,nodename,nodecode from LDMenu order by nodeorder";
	//使用模拟数据源，必须写在拆分之前
	turnPage.useSimulation   = 1;
	//取得前200条节点
	var strTemp = easyQueryVer3(sqlStr, 1, 0, 1);
	var tempArray = new Array;
	tempArray = decodeEasyQueryResult(strTemp);
	var times = 1;
	while (times * 200 < turnPage.queryAllRecordCount)
	times = times + 1;
	for ( var i = 2; i <= times; i++) {
		var strTemp1 = easyQueryVer3(sqlStr,1,0, (i-1)*200 + 1);
		//去除头部的o|xxx^
		var sss = strTemp1.substring(6,strTemp1.length);
		strTemp = strTemp + '^' + sss;
	}
/*
    if (turnPage.queryAllRecordCount > 200) {
        //取得201条以后的节点
	var strTemp1 =  easyQueryVer3(sqlStr, 1, 0, 201);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
	//得到所有的节点信息，目前在400条内有效
       strTemp = strTemp + '^' + sss;
    }
*/
	tempArray = decodeEasyQueryResult(strTemp);
	if (tempArray == null) return;
	//alert(tempArray.length);

	//初始化selectArray
	for (var i = 0; i < tempArray.length ; i++) {
		selectArray[i] = new Array();
		selectArray[i][0] = 0;
		selectArray[i][1] = tempArray[i][0];
		selectArray[i][2] = 0;
		selectArray[i][3] = tempArray[i][2];
		selectArray[i][4] = tempArray[i][3];
		selectArray[i][5] = 1;
		selectArray[i][6] = 0;
		selectArray[i][7] = 0;
		selectArray[i][8] = "sel_" + tempArray[i][3];
		if (tempArray[i][1] > 0)
			selectArray[i][9] = 0; //不是叶子
		else
			selectArray[i][9] = 1;
	}
    //初始化unselectArray
    for (var i = 0; i < tempArray.length ; i++) {
    	unselectArray[i] = new Array();
    	unselectArray[i][0] = 0;
    	unselectArray[i][1] = tempArray[i][0];
    	unselectArray[i][2] = 0;

    	unselectArray[i][3] = tempArray[i][2];
    	unselectArray[i][4] = tempArray[i][3];
    	unselectArray[i][5] = 1;
    	unselectArray[i][6] = 0;
    	unselectArray[i][7] = 0;
    	unselectArray[i][8] = "unsel_" + tempArray[i][3];
    	if (tempArray[i][1] > 0)
    	    unselectArray[i][9] = 0; //不是叶子
    	else
    	    unselectArray[i][9] = 1;
    }

    var Operator = fm.all("Operator").value;
    var sqlStr = "select nodecode from LDMenu " ;
    if(Operator == "001")
    {
    	sqlStr += " order by nodeorder" ;
    }
	else
	{
        sqlStr += " where nodecode in (select nodecode from ldmenugrptomenu ";
        sqlStr += " where menuGrpCode in (select menuGrpCode from ldusertomenugrp ";
        sqlStr += " where usercode = '" + Operator + "' ) )";
        sqlStr += " order by nodeorder";
    }

    strTemp =  easyQueryVer3(sqlStr, 1, 0, 1);
    tempArray = new Array;
    tempArray = decodeEasyQueryResult(strTemp);
	//alert(tempArray.length);

    times = 1;
    while (times * 200 < turnPage.queryAllRecordCount)
        times = times + 1;

    for ( var i = 2; i <= times; i++) {
    	var strTemp1 = easyQueryVer3(sqlStr,1,0, (i-1)*200 + 1);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
       strTemp = strTemp + '^' + sss;
    }
/*
     if (turnPage.queryAllRecordCount > 200) {
        //取得201条以后的节点
	var strTemp1 =  easyQueryVer3(sqlStr, 1, 0, 201);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
	//得到所有的节点信息，目前在400条内有效
       strTemp = strTemp + '^' + sss;
    }
*/
    tempArray = decodeEasyQueryResult(strTemp);
   //这个循环里面报错
    if (tempArray != null) {
	    for (var i = 0; i < tempArray.length; i++) {
	    
	    	var nodecode = tempArray[i][0];
	        try{
	    	for (var j = 0; j < selectArray.length; j++) {
	    		try{
	    		if (unselectArray[j][4] == nodecode)
	    		    break;
	    		}
	    		catch(ex)
	    		{
	    		//  alert(selectArray[j][4]);
	    		}    
	        }
	        addNode(unselectArray,j);
	        //removeNode(selectArray,j);
	        }
	        catch(ex)
	        {
	         // alert(tempArray[i][0]);
	        }
	    }
    }
//         alert("here11");
    paintTree(selectArray,"spanSelectMenuGrpTree");
    paintTree(unselectArray,"spanUnselectMenuGrpTree");
}

function updateClick()
{

  var selMenuGrpNo = QueryGrpGrid.getSelNo();

  if (selMenuGrpNo == 0) {
  	alert("您还没有选择需要更新的菜单组");
  	return;
  }

  var Operator = "'"+QueryGrpGrid.getRowColData(selMenuGrpNo - 1,5)+"'"; //该菜单组的operator
  fm.all("MenuOpe").value=QueryGrpGrid.getRowColData(selMenuGrpNo - 1,5);
  //var thisOperator = fm.all("Operator").value;
  var userStr = fm.all("UserStr").value; //包括登陆人及其下级
  
  if(userStr.indexOf(Operator)<0)
  {
  	alert("您无权修改此菜单组");
  	return;
  }
  
  /*
  sqlStr = "select MenuGrpName,MenuGrpCode,MenuGrpDescription,MenuSign,Operator from LDMenuGrp where " 
	    + sqlcount + "=" + sqlcount + " and "
	    + "MenuGrpCode in(" + menugrpStr +") "
	    + "or Operator in ("+userStr+")"; 
  
  
  if (thisOperator != Operator) {
  	alert("您无权修改此菜单组");
  	return;
  }
  */

  //隐去需要的元素
  divQueryGrp.style.display = "none";
  divCmdButton.style.display = "none";

  //将选择的用户信息拷贝到各个信息输入框内
  var offset = selMenuGrpNo -1;
  fm.all("MenuGrpName").value = QueryGrpGrid.getRowColData(offset,1);
  fm.all("MenuGrpCode").value = QueryGrpGrid.getRowColData(offset,2);
  fm.all("MenuSign").value = QueryGrpGrid.getRowColData(offset,3);
  fm.all("MenuGrpDescription").value = QueryGrpGrid.getRowColData(offset,4);

  
  //取得选中用户对应的菜单组
  showMenuGrp();

  //显示需要显示的元素
  divSubCmdButton.style.display = "";
  divmenuGrid2.style.display= "";

  fm.all("Action").value = "update";
}

function showMenuGrp()
{
    var selMenuGrpNo = QueryGrpGrid.getSelNo();
    if (selMenuGrpNo == 0)
        return;

    var menuGrpCode = QueryGrpGrid.getRowColData(selMenuGrpNo - 1,2);

    var sqlStr = "select ParentNodeCode,ChildFlag,nodename,nodecode from LDMenu order by nodeorder";
    
    //使用模拟数据源，必须写在拆分之前
    turnPage.useSimulation   = 1;
    var tempArray = new Array;

    //取得前200条节点
    var strTemp = easyQueryVer3(sqlStr, 1, 0, 1);
    tempArray = decodeEasyQueryResult(strTemp);

    var times = 1;
    while (times * 200 < turnPage.queryAllRecordCount)
        times = times + 1;

    for ( var i = 2; i <= times; i++) {
    	var strTemp1 = easyQueryVer3(sqlStr,1,0, (i-1)*200 + 1);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
       strTemp = strTemp + '^' + sss;
    }

/*
    if (turnPage.queryAllRecordCount > 200) {

        //取得201条以后的节点
	var strTemp1 =  easyQueryVer3(sqlStr, 1, 0, 201);
	//alert(strTemp1);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
	//得到所有的节点信息，目前在400条内有效
       strTemp = strTemp + '^' + sss;
    }
*/
    tempArray = decodeEasyQueryResult(strTemp);
    //alert(tempArray.length);

    if (tempArray != null) {
	    //初始化selectArray
	    for (var i = 0; i < tempArray.length ; i++) {

	    	selectArray[i] = new Array();

	    	selectArray[i][0] = 0;
	    	selectArray[i][1] = tempArray[i][0];
	    	selectArray[i][2] = 0;
	    	selectArray[i][3] = tempArray[i][2];
	    	selectArray[i][4] = tempArray[i][3];
	    	selectArray[i][5] = 1;
	    	selectArray[i][6] = 0;
	    	selectArray[i][7] = 0;
	    	selectArray[i][8] = "sel_" + tempArray[i][3];
	    	if (tempArray[i][1] > 0)
    	    	    selectArray[i][9] = 0; //不是叶子
    		else
    	    	    selectArray[i][9] = 1;
    	    }

     }

    //初始化unselectArray
    for (var i = 0; i < tempArray.length ; i++) {

    	unselectArray[i] = new Array();
    	unselectArray[i][0] = 0;
    	unselectArray[i][1] = tempArray[i][0];
    	unselectArray[i][2] = 0;

    	unselectArray[i][3] = tempArray[i][2];
    	unselectArray[i][4] = tempArray[i][3];
    	unselectArray[i][5] = 1;
    	unselectArray[i][6] = 0;
    	unselectArray[i][7] = 0; // check
    	unselectArray[i][8] = "unsel_" + tempArray[i][3];
    	if (tempArray[i][1] > 0)
    	    unselectArray[i][9] = 0; //不是叶子
    	else
    	    unselectArray[i][9] = 1;
    }

    var Operator = fm.all("Operator").value;
    var sqlStr = "select nodecode from LDMenu ";
    if(Operator == "001")
    {
    	sqlStr += " order by nodeorder";
    }
	else
	{
        sqlStr += " where nodecode in (select nodecode from ldmenugrptomenu ";
        sqlStr += " where menuGrpCode in (select menuGrpCode from ldusertomenugrp "
        sqlStr += " where usercode = '" + Operator + "' ) )";
        sqlStr += " order by nodeorder";
    }

    strTemp =  easyQueryVer3(sqlStr, 1, 0, 1);
    tempArray = decodeEasyQueryResult(strTemp);

    times = 1;
    while (times * 200 < turnPage.queryAllRecordCount)
        times = times + 1;

    for ( var i = 2; i <= times; i++) {
    	var strTemp1 = easyQueryVer3(sqlStr,1,0, (i-1)*200 + 1);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
       strTemp = strTemp + '^' + sss;
    }

/*
    if (turnPage.queryAllRecordCount > 200) {
        //取得201条以后的节点
	var strTemp1 =  easyQueryVer3(sqlStr, 1, 0, 201);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
	//得到所有的节点信息，目前在400条内有效
       strTemp = strTemp + '^' + sss;
    }
*/
    tempArray = new Array;
    tempArray = decodeEasyQueryResult(strTemp);
    if (tempArray != null) {
	    for (var i = 0; i < tempArray.length; i++) {
	    	var nodecode = tempArray[i][0];
	    	for (var j = 0; j < unselectArray.length; j++) {
	    		if (unselectArray[j][4] == nodecode)
	    		    break;
	        }
	        addNode(unselectArray,j);
	    }
	}

    var sqlStr = "select nodecode from LDMenu where nodecode in "
    sqlStr +="(select nodecode from ldmenuGrpTomenu where menuGrpCode ='" + menuGrpCode + "') and " +  sqlcount + " = " + sqlcount;
    sqlcount++;

    strTemp =  easyQueryVer3(sqlStr, 1, 0, 1);
    tempArray = decodeEasyQueryResult(strTemp);

    times = 1;
    while (times * 200 < turnPage.queryAllRecordCount)
        times = times + 1;

    for ( var i = 2; i <= times; i++) {
    	var strTemp1 = easyQueryVer3(sqlStr,1,0, (i-1)*200 + 1);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
       strTemp = strTemp + '^' + sss;
    }

/*
    if (turnPage.queryAllRecordCount > 200) {
        //取得201条以后的节点
	var strTemp1 =  easyQueryVer3(sqlStr, 1, 0, 201);
	//去除头部的o|xxx^
	var sss = strTemp1.substring(6,strTemp1.length);
	//得到所有的节点信息，目前在400条内有效
       strTemp = strTemp + '^' + sss;
    }
*/

    var tempArray = new Array;

    tempArray = decodeEasyQueryResult(strTemp);


 //这个循环里面报错
    if (tempArray != null) {
	    for (var i = 0; i < tempArray.length; i++) {
	    
	    	var nodecode = tempArray[i][0];
	        try{
	    	for (var j = 0; j < selectArray.length; j++) {
	    		try{
	    		if (selectArray[j][4] == nodecode)
	    		    break;
	    		}
	    		catch(ex)
	    		{
	    		//  alert(selectArray[j][4]);
	    		}    
	        }
	        addNode(selectArray,j);
	        removeNode(unselectArray,j);
	        }
	        catch(ex)
	        {
	         // alert(tempArray[i][0]);
	        }
	    }
    }

    //拷贝原始选择菜单到removeArray中
    removeArray = new Array();
    var index = 0;
    for (var i = 0; i < selectArray.length; i++) {
    	if (selectArray[i][5] == 1)
    	    continue;
    	removeArray[index] = selectArray[i][4];
    	index++;
    }

    paintTree(selectArray,"spanSelectMenuGrpTree");
    paintTree(unselectArray,"spanunSelectMenuGrpTree");
}

function update()
{
    if (!beforeSubmit())
       return;

    //计算出需要更新删除的removeArray,然后编成字符串放入hideRemoveString中
    fm.all("hideRemoveString").value = "";
 //   alert("hereh");
    var hideRemoveStr = "";
    var count = 0;
//    alert(removeArray.length);
    for (var i = 0; i < removeArray.length; i++) {

    	var nodecode = removeArray[i];
    	var j = 0;
//    	alert(selectArray.length);
    	for (; j < selectArray.length; j++) {
    	    if (selectArray[j][5] == 1)
    	        continue;
//    	    alert(selectArray[j][3]);
    	    if (selectArray[j][4] == nodecode)
    		break;
    	}
    	if (j == selectArray.length) { //此节点被删除了
    	    	hideRemoveStr = hideRemoveStr + " |" + nodecode + "|^";
    	}
//    	alert(hideRemoveStr);
    }

//    alert("hideRemoveStr is " + hideRemoveStr);

    fm.all("hideRemoveString").value = hideRemoveStr;


    var menuGrpCode = fm.all("MenuGrpCode").value;
    fm.all("hideString").value = arrayToString(selectArray);
    HideMenuGrpGrid1.clearData("HideMenuGrpGrid1");

    submitForm();
}

function remove()
{
	submitForm();
}

function insert()
{
    if (!beforeSubmit())
        return;

    var menuGrpCode = fm.all("MenuGrpCode").value;
    // 将SelectArray里面的数据拷贝到HideMenuGrpGrid中
	HideMenuGrpGrid1.clearData("HideMenuGrpGrid1");


    fm.all("hideString").value = arrayToString(selectArray);

    submitForm();
}

function arrayToString(tArray)
{
	var menuGrpCode = fm.all("MenuGrpCode").value;
	var resultString = "";
	for (var i = 0; i < tArray.length; i++) {
		if (tArray[i][5]== 1)
		    continue;
                resultString += menuGrpCode + "|";
                resultString += tArray[i][4] + "|^";

        }
//        alert(resultString);
        return resultString;

}
//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.submit(); //提交
}

function afterSubmit(FlagStr, Content)
{
    showInfo.close();
    window.focus();
    if (fm.all('action').value == "insert") {
        if (FlagStr == "success")
            alert("增加菜单组成功");
        else{
          if("" == Content){
            alert("增加菜单组失败，可能的原因是此菜单组已存在");
          }
          else{
            alert(Content);
          }
        }
    }

    if (fm.all('action').value == "update") {
        if (FlagStr == "success")
            alert("更新菜单组成功！");
        else{
          if("" == Content){
            alert("更新菜单组失败，可能的原因是此菜单组不存在");
          }
          else{
            alert(Content);
          }
        }
    }

    if (fm.all('action').value == "delete") {
        if (FlagStr == "success")
            alert("删除菜单组成功！");
        else
            alert("删除菜单组失败");
    }
}

function addMenus()
{
//	alert(unselectArray[0][2]);
//	alert(selectArray[0][2]);
	for (var i = 0; i < unselectArray.length; i++) {
  		if (unselectArray[i][5] == 1)
  			continue;
  		if (unselectArray[i][7] == 1) {
            		removeNode(unselectArray,i);
            		addNode(selectArray,i);
         	}
       }
//    alert(unselectArray[0][2]);
//    alert(selectArray[0][2]);
    paintTree(selectArray,"spanSelectMenuGrpTree");
    paintTree(unselectArray,"spanunSelectMenuGrpTree");
}

function removeMenus()
{
//	alert(selectArray[0][2]);
	for (var i = 0; i < selectArray.length; i++) {
		if (selectArray[i][2] != 0)
		    continue;
  		if (selectArray[i][5] == 1)
  			continue;
  		if (selectArray[i][7] == 1) {
                        removeNode(selectArray,i);
            		addNode(unselectArray,i);
         	}
        }
//    alert(selectArray[0][2]);
    paintTree(selectArray,"spanSelectMenuGrpTree");
    paintTree(unselectArray,"spanunSelectMenuGrpTree");
}

function initGrid()
{
	initmenuGrpGrid();
	initunSelectMenuGrpGrid();
}

function showDiv()
{
	divmenuGrid2.style.display="";
}


function okClick()
{
    if (fm.all("Action").value == "insert") {
    	insert()
    }

    if (fm.all("Action").value == "update") {
    	update();
    }

    if (fm.all("Action").value == "delete") {
    	remove();
    }
}

function cancelClick()
{
    divQueryGrp.style.display = '';
    divmenuGrid2.style.display="none";
    divCmdButton.style.display='';
    divSubCmdButton.style.display="none";
    initForm();

}

