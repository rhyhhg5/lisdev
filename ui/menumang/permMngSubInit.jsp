<%
//程序名称：permMngSubInit.jsp
//程序功能：
//创建日期：2005-12-13 11:10:36
//创建人  ：Zhangbin程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {
    
  }
  catch(ex)
  {
    alert("在ProductQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
  }
  catch(ex)
  {
    alert("在ProductQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
		initUserPermGrid();
		//initUnderLingGrid();
//		initContGrid();
	
  }
  catch(re)
  {
    alert("ProductQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

// 产品查询信息列表的初始化
function initUserPermGrid()
  {                               
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="NO.";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="25px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="用户编码";         		//列名
      iArray[1][1]="50px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

			iArray[2]=new Array();                                                 
      iArray[2][0]="用户姓名";         		//列名                     
      iArray[2][1]="50px";            		//列宽                             
      iArray[2][2]=100;            			//列最大值                           
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[3]=new Array();
      iArray[3][0]="用户类别";         		//列名
      iArray[3][1]="50px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="级别编号";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=200;            			//列最大值
      iArray[4][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="管理机构";         		//列名
      iArray[5][1]="50px";            		//列宽
      iArray[5][2]=200;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="机构性质";         		//列名
      iArray[6][1]="0px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      
      /******/
      
      iArray[7]=new Array();
      iArray[7][0]="上级用户";         		//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[8]=new Array();                                                 
      iArray[8][0]="下级用户";         		//列名                     
      iArray[8][1]="0px";            		//列宽                             
      iArray[8][2]=100;            			//列最大值                           
      iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许
      iArray[8][4]="underlingname";
      iArray[8][5]="1";
      //iArray[8][6]="0";
      iArray[8][15]= "operator";  //要依赖的列的名称 
      iArray[8][17]="1"
      iArray[8][19]=1;   		//1是需要强制刷新.
      
			iArray[9]=new Array();                                                 
      iArray[9][0]="用户状态";         		//列名                     
      iArray[9][1]="50px";            		//列宽                             
      iArray[9][2]=100;            			//列最大值                           
      iArray[9][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[10]=new Array();
      iArray[10][0]="生效日期";         		//列名
      iArray[10][1]="70px";            		//列宽
      iArray[10][2]=100;            			//列最大值
      iArray[10][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[11]=new Array();
      iArray[11][0]="终止日期";         		//列名
      iArray[11][1]="70px";            		//列宽
      iArray[11][2]=200;            			//列最大值
      iArray[11][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      
      UserPermGrid = new MulLineEnter( "fm" , "UserPermGrid" ); 
      //这些属性必须在loadMulLine前
      UserPermGrid.mulLineCount = 0;   
      UserPermGrid.displayTitle = 1;
      UserPermGrid.locked = 1;
      UserPermGrid.canSel = 0;
      UserPermGrid.hiddenPlus = 1;
      UserPermGrid.hiddenSubtraction = 1;
      UserPermGrid.loadMulLine(iArray); 
      UserPermGrid.selBoxEventFuncName = "showButtons";
      //UserPermGrid.recordNo=0;   //设置序号起始基数为10，如果要分页显示数据有用
      
      //这些操作必须在loadMulLine后面
      //PolGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

</script>
