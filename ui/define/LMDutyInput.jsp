<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人  GYJ  更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMDutyInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMDutyInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMDutysave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMDuty1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMDuty1);">
      </td>
      <td class= titleImg>
        LMDuty1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMDuty1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            责任代码
          </TD>
          <TD  class= input>
            <Input class= common name=DutyCode >
          </TD>
          <TD  class= title>
            责任名称
          </TD>
          <TD  class= input>
            <Input class= common name=DutyName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费终止期间
          </TD>
          <TD  class= input>
            <Input class= common name=PayEndYear >
          </TD>
          <TD  class= title>
            缴费终止期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=PayEndYearFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费终止日期计算参照
          </TD>
          <TD  class= input>
            <Input class=code  name=PayEndDateCalRef >
          </TD>
          <TD  class= title>
            缴费终止日期计算方式
          </TD>
          <TD  class= input>
            <Input class= code name=PayEndDateCalMode value="" ondblclick="return showCodeList('PayEndDateCalMode',[this]);" 
            onkeyup="return showCodeListKey('PayEndDateCalMode',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起领期间
          </TD>
          <TD  class= input>
            <Input class= common name=GetYear >
          </TD>
          <TD  class= title>
            起领期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=GetYearFlag value="" ondblclick="return showCodeList('GetYearFlag',[this]);" 
            onkeyup="return showCodeListKey('GetYearFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保险期间
          </TD>
          <TD  class= input>
            <Input class= common name=InsuYear >
          </TD>
          <TD  class= title>
            保险期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=InsuYearFlag value="" ondblclick="return showCodeList('InsuYearFlag',[this]);" 
            onkeyup="return showCodeListKey('InsuYearFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            意外责任期间
          </TD>
          <TD  class= input>
            <Input class= common name=AcciYear >
          </TD>
          <TD  class= title>
            意外责任期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=AcciYearFlag value="" ondblclick="return showCodeList('AcciYearFlag',[this]);" 
            onkeyup="return showCodeListKey('AcciYearFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            计算方法
          </TD>
          <TD  class= input>
            <Input class= code name=CalMode ondblclick="return showCodeList('CalMode',[this]);" 
            onkeyup="return showCodeListKey('CalMode',[this]);">
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
