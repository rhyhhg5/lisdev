<%
//程序名称：LMDutyGetClmInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('GetDutyCode').value = '';
    fm.all('GetDutyName').value = '';
  //  fm.all('GetDutyKind').value = '';
  //  fm.all('Type').value = '';
  //  fm.all('DefaultVal').value = '';
  //  fm.all('CalCode').value = '';
  //  fm.all('CnterCalCode').value = '';
  //  fm.all('OthCalCode').value = '';
  //  fm.all('InpFlag').value = '';
  //  fm.all('StatType').value = '';
  //  fm.all('MinGet').value = '';
  //  fm.all('AfterGet').value = '';
  //  fm.all('MaxGet').value = '';
  //  fm.all('ClaimRate').value = '';
  //  fm.all('ClmDayLmt').value = '';
 //   fm.all('SumClmDayLmt').value = '';
 //   fm.all('Deductible').value = '';
 //   fm.all('DeDuctDay').value = '';
 //   fm.all('ObsPeriod').value = '';
  //  fm.all('DeadValiFlag').value = '';
  //  fm.all('DeadToPValueFlag').value = '';

  }
  catch(ex)
  {
    alert("在LMDutyGetClmInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
function lMDutyGetClmDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divLMDutyGetDetail.style.left=ex;
  	divLMDutyGetDetail.style.top =ey;
    divLMDutyGetDetail.style.display ='';
}

function initGetDrawGrid()
  {

      var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="给付责任类型";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="给付类型";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="默认值";         			//列名
      iArray[3][1]="160px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;
     
      iArray[4]=new Array();
      iArray[4][0]="算法";         		//列名
      iArray[4][1]="140px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="反算法";         		//列名
      iArray[5][1]="140px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      LMDutyGetClmGrid = new MulLineEnter( "fm" , "LMDutyGetClmGrid" );
      //这些属性必须在loadMulLine前
      LMDutyGetClmGrid.mulLineCount = 4;
      LMDutyGetClmGrid.displayTitle = 1;
      LMDutyGetClmGrid.canSel = 0;
      LMDutyGetClmGrid.canChk=0;
      LMDutyGetClmGrid.loadMulLine(iArray);
      LMDutyGetClmGrid.detailInfo="单击显示详细信息";
      
      LMDutyGetClmGrid.detailClick=lMDutyGetClmDetailClick;
      
      //这些操作必须在loadMulLine后面
      //LMDutyGetClmGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert("=========gyj=="+ex);
      }
}
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LMDutyGetClmInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();   
    initGetDrawGrid(); 
  }
  catch(re)
  {
    alert("LMDutyGetClmInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>