<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人  gyj  更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMRiskPayInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMRiskPayInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMRiskPaySave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMRiskPay1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMRiskPay1);">
      </td>
      <td class= titleImg>
        LMRiskPay1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMRiskPay1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class= common name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVer >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种名称
          </TD>
          <TD  class= input>
            <Input class= common name=RiskName >
          </TD>
          <TD  class= title>
            催收标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=UrgePayFlag >催收
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            手续费类型
          </TD>
          <TD  class= input>
            <Input class= common name=ChargeType >
          </TD>
          <TD  class= title>
            分解缴费间隔
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=CutPayIntv >可分解
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            免交涉及加费
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=PayAvoidType >涉及加费
          </TD>
          <TD  class= title>
            手续费与保费关系
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=ChargeAndPrem >手续费可以冲减保费
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            结算日类型
          </TD>
          <TD  class= input>
            <Input class= common name=BalaDateType >
          </TD>
          <TD  class= title>
            免交标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=PayAvoidFlag >能
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费与复效关系
          </TD>
          <TD  class= input>
            <Input class= common name=PayAndRevEffe >
          </TD>
          <TD  class= title>
            缴费宽限期
          </TD>
          <TD  class= input>
            <Input class= common name=GracePeriod >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            宽限期单位
          </TD>
          <TD  class= input>
            <Input class=codeno name=GracePeriodUnit value="" ondblclick="return showCodeList('GracePeriodUnit',[this,GracePeriodUnitName],[0,1]);" 
            onkeyup="return showCodeListKey('GracePeriodUnit',[this,GracePeriodUnitName],[0,1]);"><input class=codename name=GracePeriodUnitName>          
          </TD>
          <TD  class= title>
            宽限日期计算方式
          </TD>
          <TD  class= input>
            <Input class= codeno name=GraceDateCalMode value="" ondblclick="return showCodeList('GraceDateCalMode',[this,GraceDateCalModeName],[0,1]);" 
            onkeyup="return showCodeListKey('GraceDateCalMode',[this,GraceDateCalModeName],[0,1]);"><input class=codename name=GraceDateCalModeName>          
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
