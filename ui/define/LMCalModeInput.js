//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var arrDataSet;
var turnPage = new turnPageClass();

//提交，保存按钮对应操作
function submitForm()
{
  if (mOperate != "DELETE||MAIN")
  {
    if (!beforeSubmit())
      return false;
  }
  
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  //alert(mOperate);
  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //showSubmitFrame(mDebug);
//  fm.CalSQL2.value = invertedComma(fm.CalSQL.value,'ADD');
  alert(fm.CalSQL.value);
  fm.CalSQL2.value = invertedComma(fm.CalSQL.value,"'","''");
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LMCalMode.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作  
  if (!verifyInput())
    return false;
    
  LMCalModeGrid.delBlankLine("LMCalModeGrid");	
  if (!LMCalModeGrid.checkValue())
    return false;
  
  //校验是否有重复的要素代码
  var lineCount = 0;
  lineCount = LMCalModeGrid.mulLineCount;
 
  if (lineCount > 0)
  {
      var sValue = '';
      var sOldValue = '';	
      for(var i=0;i<lineCount;i++)
      {
      	sValue = LMCalModeGrid.getRowColData(i,1);
      	//alert(sValue);
      	if (sValue == null) sValue = "";
      	if (sOldValue != null && sOldValue != "" && sOldValue == trim(sValue))
      	{
      	   alert('要素代码不能重复！');
      	   return false;
      	}
      	sOldValue = sValue;
      }	//end of for
  }
  return true;
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  mOperate="INSERT||MAIN";
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
  mOperate="UPDATE||MAIN";
  if ((fm.all("CalCode").value == null)||(fm.all("CalCode").value == ''))
  {
       alert('请先查询出要修改的算法编码记录！');    
       fm.all('CalCode').focus();
  }else
  {
       //下面增加相应的代码
       if (confirm("您确实想修改该记录吗?"))
       {  	
          mOperate="UPDATE||MAIN";
          submitForm();
       }
       else
       {
         mOperate="";
         alert("您取消了修改操作！");
       }
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  mOperate="QUERY||MAIN";
  showInfo=window.open("./LMCalModeQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
  mOperate="DELETE||MAIN";
  if ((fm.all("CalCode").value == null)||(fm.all("CalCode").value == ''))
  {
       alert('请先查询出要删除的算法编码记录！');    
       fm.all('CalCode').focus();
  }else
  {	
         //下面增加相应的删除代码
         if (confirm("您确实想删除该记录吗?"))
         {
           mOperate="DELETE||MAIN";  
           submitForm();
         }
         else
         {
           mOperate="";
           alert("您取消了删除操作！");
         }
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}

function afterQuery(arrQueryResult)
{	
	var arrResult = new Array();
	
//	initForm();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;		                                  
                fm.all('CalCode').value = arrResult[0][0];
                fm.all('CalSQL').value = arrResult[0][1];
//                fm.all('CalSQL').value = invertedComma(arrResult[0][1],'MINUS');
                fm.all('Remark').value = arrResult[0][2];
        }
        LMCalModeGrid.clearData();
        queryFactor();
}

function queryFactor()
{
   // 书写SQL语句
   var strSQL = "";
   strSQL = "select * from LMCalFactor where 1=1 "
           + getWherePart('CalCode');		
     	 //alert(strSQL);
   turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  

 
  //判断是否查询成功
  if (turnPage.strQueryResult) 
  {
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  //tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  //turnPage.arrDataCacheSet = chooseArray(tArr,[0,1,2,3,4,5]);
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  turnPage.pageDisplayGrid = LMCalModeGrid;    
          
  //保存SQL语句
  turnPage.strQuerySql     = strSQL; 
  
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  arrDataSet           = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.arrDataCacheSet.length);
  var tArr = new Array();
  tArr = chooseArray(arrDataSet,[1,2,3,4,5,6]); 
  //调用MULTILINE对象显示查询结果
  
  //displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
  displayMultiline(tArr, turnPage.pageDisplayGrid);
  }	
}
function invertedComma(cOriginStr,cOperType)
{
    if (cOriginStr == "" || cOriginStr == "undefine" || cOriginStr == "null")
      return "";
      
    var strTemp = cOriginStr;
    var strReturn = "";
    while(strTemp.indexOf("'") != -1 )
    {
    	strReturn += strTemp.substring(0,strTemp.indexOf("'")+1)+"'";
    	strTemp = strTemp.substring(strTemp.indexOf("'")+1);
    	   
    	//alert(strReturn);
    }
    strReturn += strTemp;
    	
    //alert(strReturn);
    return strReturn;
}