<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LMCalModeQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMCalModeQueryInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
        <!-- 显示或隐藏LMCalMode1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMCalMode1);">
      </td>
      <td class= titleImg>
        算法定义信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMCalMode1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            算法编码
          </TD>
          <TD  class= input>
            <Input class= common name=CalCode >
          </TD>
          <!--TD  class= title>
            算法类型
          </TD>
          <TD  class= input>
            <Input class= common name=Type >
          </TD-->
        </TR>
        <TR  class= common>
          <TD  class= title>
            算法内容
          </TD>
          <TD class=input>
		<textarea class=common name=CalSQL rows="2" cols="40" verify="算法内容|notnull">
		</textarea>
	  </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            算法描述
          </TD>
          <TD class=input>
		<textarea class=common name=Remark rows="2" cols="40">
		</textarea>
	  </TD>
        </TR>
      </table>
    </Div>    
          <INPUT VALUE="查询" TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" TYPE=button onclick="returnParent();"> 	
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divspanLMCalModeQueryGrid);">
            </td>
            <td class= titleImg>
                     查询结果
            </td>
    	</tr>
     </table>
    <Div  id= "divspanLMCalModeQueryGrid" style= "display: ''">
    <table  class= common>
            <tr  class= common>
                    <td text-align: left colSpan=1>
                                  <span id="spanLMCalModeQueryGrid" >
                                  </span>
                            </td>
                    </tr>
            </table>
    </div>
  	
      <INPUT VALUE="首页" TYPE=button onclick="turnPage.firstPage();"> 
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();"> 					
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();"> 
      <INPUT VALUE="尾页" TYPE=button onclick="turnPage.lastPage();"> 	
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
