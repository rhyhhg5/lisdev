<%
//程序名称：LMRiskInsuAccInput.jsp
//程序功能：
//创建日期：2002-08-09 18:07:34
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('RiskCode').value = '';
    fm.all('RiskVer').value = '';
    fm.all('RiskName').value = '';
   // fm.all('InsuAccNo').value = '';
   // fm.all('InsuAccName').value = '';
   // fm.all('Type').value = '';

  }
  catch(ex)
  {
    alert("在LMRiskInsuAccInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LMRiskInsuAccInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
// 被保人信息列表的初始化
function initGetDrawGrid()
  {

      var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="保险账户编码";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="保险账户名称";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="账号类型";         			//列名
      iArray[3][1]="160px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=2;              			//是否允许输入,1表示允许，0表示不允许
      iArray[3][4]='Type';
      LMRiskInsuAccGrid = new MulLineEnter( "fm" , "LMRiskInsuAccGrid" );
      //这些属性必须在loadMulLine前
      LMRiskInsuAccGrid.mulLineCount = 4;
      LMRiskInsuAccGrid.displayTitle = 1;
      LMRiskInsuAccGrid.canSel = 0;
      LMRiskInsuAccGrid.canChk=0;
      LMRiskInsuAccGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
      //LMRiskInsuAccGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initGetDrawGrid();
  }
  catch(re)
  {
    alert("LMRiskInsuAccInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>