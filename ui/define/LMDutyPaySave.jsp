<%
//程序名称：LMDutyPayInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LMDutyPaySchema tLMDutyPaySchema   = new LMDutyPaySchema();

  UILMDutyPay tLMDutyPay   = new UILMDutyPay();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "insert";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

    tLMDutyPaySchema.setPayPlanCode(request.getParameter("PayPlanCode"));
    tLMDutyPaySchema.setPayPlanName(request.getParameter("PayPlanName"));
    tLMDutyPaySchema.setType(request.getParameter("Type"));
    tLMDutyPaySchema.setPayIntv(request.getParameter("PayIntv"));
    tLMDutyPaySchema.setPayEndYearFlag(request.getParameter("PayEndYearFlag"));
    tLMDutyPaySchema.setPayEndYear(request.getParameter("PayEndYear"));
    tLMDutyPaySchema.setPayEndDateCalRef(request.getParameter("PayEndDateCalRef"));
    tLMDutyPaySchema.setPayEndDateCalMode(request.getParameter("PayEndDateCalMode"));
    tLMDutyPaySchema.setDefaultVal(request.getParameter("DefaultVal"));
    tLMDutyPaySchema.setCalCode(request.getParameter("CalCode"));
    tLMDutyPaySchema.setCnterCalCode(request.getParameter("CnterCalCode"));
    tLMDutyPaySchema.setOthCalCode(request.getParameter("OthCalCode"));
    tLMDutyPaySchema.setRate(request.getParameter("Rate"));
    tLMDutyPaySchema.setMinPay(request.getParameter("MinPay"));
    tLMDutyPaySchema.setAssuYield(request.getParameter("AssuYield"));
    tLMDutyPaySchema.setFeeRate(request.getParameter("FeeRate"));
    tLMDutyPaySchema.setPayToDateCalMode(request.getParameter("PayToDateCalMode"));
    tLMDutyPaySchema.setUrgePayFlag(request.getParameter("UrgePayFlag"));
    tLMDutyPaySchema.setPayLackFlag(request.getParameter("PayLackFlag"));
    tLMDutyPaySchema.setPayOverFlag(request.getParameter("PayOverFlag"));
    tLMDutyPaySchema.setPayOverDeal(request.getParameter("PayOverDeal"));
    tLMDutyPaySchema.setAvoidPayFlag(request.getParameter("AvoidPayFlag"));
    tLMDutyPaySchema.setGracePeriod(request.getParameter("GracePeriod"));
    tLMDutyPaySchema.setPubFlag(request.getParameter("PubFlag"));


  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLMDutyPaySchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tLMDutyPay.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = t%functionname%.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

