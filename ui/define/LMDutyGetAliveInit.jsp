<%
//程序名称：LMDutyGetAliveInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>

<script language="JavaScript">
function initInpBox()
{
  try
  {
    fm.all('GetDutyCode').value = '';
    fm.all('GetDutyName').value = '';
  //  fm.all('GetDutyKind').value = '';
  //  fm.all('Type').value = '';
  //  fm.all('GetIntv').value = '';
  //  fm.all('DefaultVal').value = '';
  //  fm.all('CalCode').value = '';
  //  fm.all('CnterCalCode').value = '';
  //  fm.all('OthCalCode').value = '';
   // fm.all('GetStartPeriod').value = '';
   // fm.all('GetStartUnit').value = '';
   // fm.all('StartDateCalRef').value = '';
   // fm.all('StartDateCalMode').value = '';
   // fm.all('MinGetStartPeriod').value = '';
   // fm.all('GetEndPeriod').value = '';
   // fm.all('GetEndUnit').value = '';
  // fm.all('EndDateCalRef').value = '';
   // fm.all('EndDateCalMode').value = '';
   // fm.all('MaxGetEndPeriod').value = '';
   // fm.all('AddFlag').value = '';
   // fm.all('AddIntv').value = '';
   // fm.all('AddStartPeriod').value = '';
   // fm.all('AddStartUnit').value = '';
   // fm.all('AddEndPeriod').value = '';
   // fm.all('AddEndUnit').value = '';
   // fm.all('AddType').value = '';
   // fm.all('AddValue').value = '';
   // fm.all('MaxGetCount').value = '';
   // fm.all('AfterGet').value = '';
   // fm.all('GetActionType').value = '';
   // fm.all('UrgeGetFlag').value = '';
   // fm.all('DiscntFlag').value = '';
   // fm.all('GetCond').value = '';

  }
  catch(ex)
  {
    alert("在LMDutyGetAliveInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}
function lMDutyGetAliveDetailClick(cObj)
{
  	var ex,ey;
  	ex = window.event.clientX+document.body.scrollLeft;  //得到事件的坐标x
  	ey = window.event.clientY+document.body.scrollTop;   //得到事件的坐标y
  	divLMDutyGetAliveDetail.style.left=ex;
  	divLMDutyGetAliveDetail.style.top =ey;
        divLMDutyGetAliveDetail.style.display ='';
}
function initSelBox()
{
  try
  {
//    setOption("t_sex","0=男&1=女&2=不详");
//    setOption("sex","0=男&1=女&2=不详");
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");
  }
  catch(ex)
  {
    alert("在LMDutyGetAliveInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}
function initGetDrawGrid()
  {

      var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="给付责任类型";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="给付类型";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="给付间隔";         			//列名
      iArray[3][1]="160px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;

      iArray[4]=new Array();
      iArray[4][0]="默认值";         		//列名
      iArray[4][1]="140px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="算法";         		//列名
      iArray[5][1]="140px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      LMDutyGetAliveGrid = new MulLineEnter( "fm" , "LMDutyGetAliveGrid" );
      //这些属性必须在loadMulLine前
      LMDutyGetAliveGrid.mulLineCount = 4;
      LMDutyGetAliveGrid.displayTitle = 1;
      LMDutyGetAliveGrid.canSel = 0;
      LMDutyGetAliveGrid.canChk=0;
      LMDutyGetAliveGrid.loadMulLine(iArray);
      LMDutyGetAliveGrid.detailInfo="单击显示详细信息";

      LMDutyGetAliveGrid.detailClick=lMDutyGetAliveDetailClick;

      //这些操作必须在loadMulLine后面
      //LMDutyGetClmGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}
function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initGetDrawGrid()
  }
  catch(re)
  {
    alert("LMDutyGetAliveInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>