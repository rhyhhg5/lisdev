<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMUWInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMUWInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMUWsave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMUW1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMUW1);">
      </td>
      <td class= titleImg>
        LMUW1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMUW1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            核保编码
          </TD>
          <TD  class= input>
            <Input class= common name=UWCode >
          </TD>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class= common name=RiskCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVer >
          </TD>
          <TD  class= title>
            险种名称
          </TD>
          <TD  class= input>
            <Input class= common name=RiskName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            关联保单类型
          </TD>
          <TD  class= input>
            <Input class= codeno name=RelaPolType value="" ondblclick="return showCodeList('RelaPolType',[this,RelaPolTypeName],[0,1]);" 
            onkeyup="return showCodeListKey('RelaPolType',[this,RelaPolTypeName],[0,1]);"><input class=codename name=RelaPolTypeName>          
          </TD>
          <TD  class= title>
            核保类型
          </TD>
          <TD  class= input>
            <Input class= codeno name=UWType value="" ondblclick="return showCodeList('UWType',[this,UWTypeName],[0,1]);" 
            onkeyup="return showCodeListKey('UWType',[this,UWTypeName],[0,1]);"><input class=codename name=UWTypeName>          
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            核保顺序号
          </TD>
          <TD  class= input>
            <Input class= common name=UWOrder >
          </TD>
          <TD  class= title>
            算法
          </TD>
          <TD  class= input>
            <Input class= common name=CalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            其他算法
          </TD>
          <TD  class= input>
            <Input class= common name=OthCalCode >
          </TD>
          <TD  class= title>
            备注
          </TD>
          <TD  class= input>
            <Input class= common name=Remark >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            核保级别
          </TD>
          <TD  class= input>
            <Input class= common name=UWGrade >
          </TD>
          <TD  class= title>
            核保结果控制
          </TD>
          <TD  class= input>
            <Input class= common name=UWResult >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            核保通过标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=PassFlag >通过
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
