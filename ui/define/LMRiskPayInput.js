//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";

//提交，保存按钮对应操作
function submitForm()
{
  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
//    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  if (fm.RiskCode.value==""||fm.RiskVer.value==""||fm.RiskName.value==""){
      alert("所填写信息不全,请确认后再提交保存!");
      return false;
  }
  if (fm.UrgePayFlag.checked == true){
      fm.UrgePayFlag.value = "Y";	
  }else{
      fm.UrgePayFlag.value = "N";
  }

  if (fm.CutPayIntv.checked == true){
      fm.CutPayIntv.value = "Y";	
  }else{
      fm.UrgePayFlag.value = "N";
  }

  if (fm.PayAvoidType.checked == true){
      fm.PayAvoidType.value = "Y";	
  }else{
      fm.PayAvoidType.value = "N";
  }

  if (fm.ChargeAndPrem.checked == true){
      fm.ChargeAndPrem.value = "Y";	
  }else{
      fm.ChargeAndPrem.value = "N";
  }
  
  if (fm.PayAvoidFlag.checked == true){
      fm.PayAvoidFlag.value = "Y";	
  }else{
      fm.PayAvoidFlag.value = "N";
  }

  fm.action = "./LMRiskPaySave.jsp?RiskCode="+fm.RiskCode.value+"&RiskVer="+fm.RiskVer.value+"&RiskName="+fm.RiskName.value+
  "&UrgePayFlag="+fm.UrgePayFlag.value+"&ChargeType="+fm.ChargeType.value+"&CutPayIntv="+fm.CutPayIntv.value+
  "&PayAvoidType="+fm.PayAvoidType.value+"&ChargeAndPrem="+fm.ChargeAndPrem.value+"&BalaDateType="+fm.BalaDateType.value+
  "&PayAvoidFlag="+fm.PayAvoidFlag.value+"&PayAndRevEffe="+fm.PayAndRevEffe.value+"&GracePeriod="+fm.GracePeriod.value+
  "&GracePeriodUnit="+fm.GracePeriodUnit.value+"&GraceDateCalMode="+fm.GraceDateCalMode.value+"&fmtransact=INSERT||MAIN";
  //showSubmitFrameChargeType;
  fm.submit(); //提交
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  alert(FlagStr);
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true"); 
//    showDiv(inputButton,"false"); 
	  initForm();
  }
  catch(re)
  {
  	alert("在LMRiskPay.js-->resetForm函数中发生异常:初始化界面错误!");
  }
} 

//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
}
 
//提交前的校验、计算  
function beforeSubmit()
{
  //添加操作	
}           


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//Click事件，当点击增加图片时触发该函数
function addClick()
{
  //下面增加相应的代码
  showDiv(operateButton,"false"); 
  showDiv(inputButton,"true"); 
}           

//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
  //下面增加相应的代码
if(fm.RiskCode.value == ""||fm.RiskVer.value == ""||fm.RiskName.value == "")
 {
 	alert("所填写信息不全,请确认后再提交修改!");
 	return ;
 }

    if (fm.UrgePayFlag.checked == true){
      fm.UrgePayFlag.value = "Y";	
  }else{
      fm.UrgePayFlag.value = "N";
  }

  if (fm.CutPayIntv.checked == true){
      fm.CutPayIntv.value = "Y";	
  }else{
      fm.CutPayIntv.value = "N";
  }

  if (fm.PayAvoidType.checked == true){
      fm.PayAvoidType.value = "Y";	
  }else{
      fm.PayAvoidType.value = "N";
  }

  if (fm.ChargeAndPrem.checked == true){
      fm.ChargeAndPrem.value = "Y";	
  }else{
      fm.ChargeAndPrem.value = "N";
  }
  
  if (fm.PayAvoidFlag.checked == true){
      fm.PayAvoidFlag.value = "Y";	
  }else{
      fm.PayAvoidFlag.value = "N";
  } 
   
  if (confirm("您确实想修改该记录吗?"))
  {
  var i = 0;
  var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 // showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(mDebug);
 // fm.fmtransact.value = "UPDATE||MAIN";
  
   fm.action = "./LMRiskPaySave.jsp?RiskCode="+fm.RiskCode.value+"&RiskVer="+fm.RiskVer.value+"&RiskName="+fm.RiskName.value+
  "&UrgePayFlag="+fm.UrgePayFlag.value+"&ChargeType="+fm.ChargeType.value+"&CutPayIntv="+fm.CutPayIntv.value+
  "&PayAvoidType="+fm.PayAvoidType.value+"&ChargeAndPrem="+fm.ChargeAndPrem.value+"&BalaDateType="+fm.BalaDateType.value+
  "&PayAvoidFlag="+fm.PayAvoidFlag.value+"&PayAndRevEffe="+fm.PayAndRevEffe.value+"&GracePeriod="+fm.GracePeriod.value+
  "&GracePeriodUnit="+fm.GracePeriodUnit.value+"&GraceDateCalMode="+fm.GraceDateCalMode.value+
  "&fmtransact=UPDATE||MAIN";
 
  fm.submit(); //提交
  }
  else
  {
    //mOperate="";
    alert("您取消了修改操作！");
  }
}           

//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  //下面增加相应的代码
  //alert("query click");
    showInfo=window.open("./LMRiskPayQuery.html");
}           

//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的代码
if(fm.RiskCode.value == ""||fm.RiskVer.value == "")
 {
 	alert("所填写信息不全,请确认后再提交删除!");
 	return ;
 }  

if (confirm("您确实想删除该记录吗?"))
  {
  var i = 0;
  var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
 // showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  
  //showSubmitFrame(1);
  // fm.fmtransact.value = "DELETE||MAIN";
  fm.action = "./LMRiskPaySave.jsp?RiskCode="+fm.RiskCode.value+"&RiskVer="+fm.RiskVer.value+"&fmtransact=DELETE||MAIN";
  fm.submit(); //提交
  initForm();
  }
   else
  {
    alert("您取消了删除操作！");
  }
}           

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";  
  }
}


/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	
	if( arrQueryResult != null )
	{
		
		arrResult = arrQueryResult;
        fm.all('RiskCode').value= arrResult[0][0];
        fm.all('RiskVer').value= arrResult[0][1];
        fm.all('RiskName').value= arrResult[0][2];
        if(arrResult[0][3] == "Y")
        {
        	fm.UrgePayFlag.checked = true;
        }else{
        	fm.UrgePayFlag.checked = false;
	}
        fm.all('ChargeType').value= arrResult[0][4];
        if(arrResult[0][5] == "Y")
        {
        	fm.CutPayIntv.checked = true;
	}else{
        	fm.CutPayIntv.checked = false;
	}
        if(arrResult[0][6] == "Y")
        {
        	fm.PayAvoidType.checked = true;
	}else{
        	fm.PayAvoidType.checked = false;
	}
        if(arrResult[0][7] == "Y")
        {
        	fm.ChargeAndPrem.checked = true;
	}else{
        	fm.ChargeAndPrem.checked = false;
	}
        fm.all('BalaDateType').value= arrResult[0][8];
        if(arrResult[0][9] == "Y")
        {
        	fm.PayAvoidFlag.checked = true;
	}else{
        	fm.PayAvoidFlag.checked = false;
	}
        fm.all('PayAndRevEffe').value= arrResult[0][10];
        fm.all('GracePeriod').value= arrResult[0][11];
        fm.all('GracePeriodUnit').value= arrResult[0][12];
        fm.all('GraceDateCalMode').value= arrResult[0][13];
	}
}    
 