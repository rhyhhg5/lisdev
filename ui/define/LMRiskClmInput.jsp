<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人  gyj  更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMRiskClmInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMRiskClmInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMRiskClmsave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMRiskClm1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMRiskClm1);">
      </td>
      <td class= titleImg>
        LMRiskClm1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMRiskClm1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class= common name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVer >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种名称
          </TD>
          <TD  class= input>
            <Input class= common name=RiskName >
          </TD>
          <TD  class= title>
            调查标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=SurveyFlag >需要调查
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            调查开始位置
          </TD>
          <TD  class= input>
            <Input class= codeno name=SurveyStartFlag value="" ondblclick="return showCodeList('SurveyStartFlag',[this,SurveyStartFlagName],[0,1]);" 
            onkeyup="return showCodeListKey('SurveyStartFlag',[this,SurveyStartFlagName],[0,1]);"><input class=codename name=SurveyStartFlagName>          
          </TD>
          <TD  class= title>
            控制赔付比例标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=ClmRateCtlFlag >陪付比例控制
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            退预缴保费标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=PrePremFlag >退预交保费
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
