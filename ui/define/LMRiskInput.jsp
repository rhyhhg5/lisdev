<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
 <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMRiskInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMRiskInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMRisksave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMRisk1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMRisk1);">
      </td>
      <td class= titleImg>
        LMRisk1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMRisk1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class= common name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVer >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种名称
          </TD>
          <TD  class= input>
            <Input class= common name=RiskName >
          </TD>
          <TD  class= title>
            选择责任标记
          </TD>
          <TD  class= input>
            <Input class= common name=ChoDutyFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            续期收费标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=CPayFlag >是
          </TD>
          <TD  class= title>
            生存给付标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=GetFlag >是
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保全标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=EdorFlag >是
          </TD>
          <TD  class= title>
            续保标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=RnewFlag >是
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            核保标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=UWFlag >是
          </TD>
          <TD  class= title>
            分保标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=RinsFlag >是
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保险帐户标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=InsuAccFlag >是
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
