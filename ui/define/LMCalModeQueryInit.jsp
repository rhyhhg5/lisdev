<%
//程序名称：LMCalModeQueryInit.jsp
//程序功能：
//创建日期：2003-01-13 15:31:09
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">

// 输入框的初始化（单记录部分）
function initInpBox()
{ 

  try
  {    
  	fm.all('CalCode').value = '';
  	fm.all('CalSQL').value = '';
  	fm.all('Remark').value = '';
  }
  catch(ex)
  {
    alert("在LMCalModeQueryInit..jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LMCalModeQueryInit..jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initLMCalModeQueryGrid();
  }
  catch(re)
  {
    alert("LMCalModeQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化LMCalModeGrid
 ************************************************************
 */
function initLMCalModeQueryGrid()
  {                               
    var iArray = new Array();
      
      try
      {
        iArray[0]=new Array();
        iArray[0][0]="序号";         //列名
        iArray[0][1]="30px";         //列名
        iArray[0][2]=100;         //列名
        iArray[0][3]=0;         //列名

        iArray[1]=new Array();
        iArray[1][0]="算法编码";         //列名
        iArray[1][1]="70px";         //宽度
        iArray[1][2]=100;         //最大长度
        iArray[1][3]=0;         //是否允许录入，0--不能，1--允许

        iArray[2]=new Array();
        iArray[2][0]="算法描述";         //列名
        iArray[2][1]="150px";         //宽度
        iArray[2][2]=100;         //最大长度
        iArray[2][3]=0;         //是否允许录入，0--不能，1--允许
        
        //CalSQL字段的值无法显示报错，可能由于太长
  
        LMCalModeQueryGrid = new MulLineEnter( "fm" , "LMCalModeQueryGrid" ); 

        //这些属性必须在loadMulLine前
        LMCalModeQueryGrid.mulLineCount = 0;   
        LMCalModeQueryGrid.displayTitle = 1;
        LMCalModeQueryGrid.canSel=1;
        LMCalModeQueryGrid.canChk=0;
        LMCalModeQueryGrid.locked=1;
        LMCalModeQueryGrid.hiddenPlus=1;
      LMCalModeQueryGrid.hiddenSubtraction=1;
        LMCalModeQueryGrid.loadMulLine(iArray);  

      }
      catch(ex)
      {
        alert("初始化LMCalModeQueryGrid时出错："+ ex);
      }
    }


</script>