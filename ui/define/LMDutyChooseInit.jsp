<%
//程序名称：LMDutyChooseInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->

<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">
function initInpBox()
{ 
  try
  {                                   
    fm.all('RiskCode').value = '';
    fm.all('RiskVer').value = '';
    //fm.all('DutyCode').value = '';
    //fm.all('DutyName').value = '';
    //fm.all('RelaDutyCode').value = '';
    //fm.all('RelaDutyName').value = '';
    //fm.all('Relation').value = '';

  }
  catch(ex)
  {
    alert("在LMDutyChooseInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{  
  try                 
  {
//    setOption("t_sex","0=男&1=女&2=不详");      
//    setOption("sex","0=男&1=女&2=不详");        
//    setOption("reduce_flag","0=正常状态&1=减额交清");
//    setOption("pad_flag","0=正常&1=垫交");   
  }
  catch(ex)
  {
    alert("在LMDutyChooseInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
// 被保人信息列表的初始化
function initGetDrawGrid()
  {

      var iArray = new Array();
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="责任代码";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=80;            			//列最大值
      iArray[1][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="责任名称";         			//列名
      iArray[2][1]="100px";            		//列宽
      iArray[2][2]=80;            			//列最大值
      iArray[2][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="关联责任";         			//列名
      iArray[3][1]="160px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[4]=new Array();
      iArray[4][0]="关联责任";         		//列名
      iArray[4][1]="140px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="关联关系";         		//列名
      iArray[5][1]="140px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=1;              			//是否允许输入,1表示允许，0表示不允许

      LMDutyChooseGrid = new MulLineEnter( "fm" , "LMDutyChooseGrid" );
      //这些属性必须在loadMulLine前
      LMDutyChooseGrid.mulLineCount = 4;
      LMDutyChooseGrid.displayTitle = 1;
      LMDutyChooseGrid.canSel = 0;
      LMDutyChooseGrid.canChk=0;
      LMDutyChooseGrid.loadMulLine(iArray);
      //这些操作必须在loadMulLine后面
      //LMDutyChooseGrid.setRowColData(1,1,"asdf");
      }
      catch(ex)
      {
        alert(ex);
      }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initGetDrawGrid();
  }
  catch(re)
  {
    alert("LMDutyChooseInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>