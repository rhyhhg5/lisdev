<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人   gyj 更新日期     更新原因/内容
%>
<html>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMDutyPayInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMDutyPayInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMDutyPaysave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMDutyPay1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMDutyPay1);">
      </td>
      <td class= titleImg>
        LMDutyPay1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMDutyPay1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            缴费编码
          </TD>
          <TD  class= input>
            <Input class= common name=PayPlanCode >
          </TD>
          <TD  class= title>
            缴费名称
          </TD>
          <TD  class= input>
            <Input class= common name=PayPlanName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费类型
          </TD>
          <TD  class= input>
            <Input class= code name=Type >
          </TD>
          <TD  class= title>
            交费间隔
          </TD>
          <TD  class= input>
            <Input class= common name=PayIntv >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费终止期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=PayEndYearFlag value="" ondblclick="return showCodeList('PayEndYearFlag',[this]);" 
            onkeyup="return showCodeListKey('PayEndYearFlag',[this]);">
          </TD>
          <TD  class= title>
            缴费终止期间
          </TD>
          <TD  class= input>
            <Input class= common name=PayEndYear >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费终止日期计算参照
          </TD>
          <TD  class= input>
            <Input class= code name=PayEndDateCalRef value="" ondblclick="return showCodeList('PayEndDateCalRef',[this]);" 
            onkeyup="return showCodeListKey('PayEndDateCalRef',[this]);">
          </TD>
          <TD  class= title>
            缴费终止日期计算方式
          </TD>
          <TD  class= input>
            <Input class= code name=PayEndDateCalMode value="" ondblclick="return showCodeList('PayEndDateCalMode',[this]);" 
            onkeyup="return showCodeListKey('PayEndDateCalMode',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            默认值
          </TD>
          <TD  class= input>
            <Input class= common name=DefaultVal >
          </TD>
          <TD  class= title>
            算法
          </TD>
          <TD  class= input>
            <Input class= common name=CalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            反算算法
          </TD>
          <TD  class= input>
            <Input class= common name=CnterCalCode >
          </TD>
          <TD  class= title>
            其他算法
          </TD>
          <TD  class= input>
            <Input class= common name=OthCalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保费分配比例
          </TD>
          <TD  class= input>
            <Input class= common name=Rate >
          </TD>
          <TD  class= title>
            最低限额
          </TD>
          <TD  class= input>
            <Input class= common name=MinPay >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保证收益率
          </TD>
          <TD  class= input>
            <Input class= common name=AssuYield >
          </TD>
          <TD  class= title>
            提取管理费比例
          </TD>
          <TD  class= input>
            <Input class= common name=FeeRate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴至日期计算方法
          </TD>
          <TD  class= input>
            <Input class= code name=PayToDateCalMode value="" ondblclick="return showCodeList('PayToDateCalMode',[this]);" 
            onkeyup="return showCodeListKey('PayToDateCalMode',[this]);">
          </TD>
          <TD  class= title>
            催缴标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=UrgePayFlag >催缴
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            部分缴费标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=PayLackFlag >是
          </TD>
          <TD  class= title>
            挂帐标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=PayOverFlag >是
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            挂帐处理
          </TD>
          <TD  class= input>
            <Input class= common name=PayOverDeal >
          </TD>
          <TD  class= title>
            免交标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=AvoidPayFlag >是
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费宽限期
          </TD>
          <TD  class= input>
            <Input class= common name=GracePeriod >
          </TD>
          <TD  class= title>
            公用标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=PubFlag >公用账户
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>