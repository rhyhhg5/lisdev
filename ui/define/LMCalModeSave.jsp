<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LMCalModeSave.jsp
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LMCalModeSchema tLMCalModeSchema   = new LMCalModeSchema();
  LMCalFactorSchema tLMCalFactorSchema = new LMCalFactorSchema();
  LMCalFactorSet tLMCalFactorSet = new LMCalFactorSet();

  LMCalModeUI tLMCalMode = new LMCalModeUI();

	GlobalInput tG = new GlobalInput();

	//tG.Operator = "Admin";
	//tG.ComCode  = "001";
  //session.putValue("GI",tG);

  tG=(GlobalInput)session.getValue("GI");
  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String tOperate=request.getParameter("hideOperate");
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

    tLMCalModeSchema.setCalCode(request.getParameter("CalCode"));
    tLMCalModeSchema.setRiskCode("000000");
    tLMCalModeSchema.setType("T");
    tLMCalModeSchema.setCalSQL(request.getParameter("CalSQL2"));
    tLMCalModeSchema.setRemark(request.getParameter("Remark"));

  //取得计算要素信息
  if (!tOperate.equals("DELETE||MAIN"))
  {
  int lineCount = 0;
  System.out.println("---come");
  String arrCount[] = request.getParameterValues("LMCalModeGridNo");
  if (arrCount != null)
    lineCount = arrCount.length; //行数
  System.out.println("lineCount:"+lineCount);
  if (lineCount > 0)
  {
  String tFactorCode[] = request.getParameterValues("LMCalModeGrid1");
  String tFacotrName[] = request.getParameterValues("LMCalModeGrid2");
  String tFactorType[] = request.getParameterValues("LMCalModeGrid3");
  String tFactorGrade[] = request.getParameterValues("LMCalModeGrid4");
  String tFactorCalCode[] = request.getParameterValues("LMCalModeGrid5");
  String tFactorDefault[] = request.getParameterValues("LMCalModeGrid6");
  for(int i=0;i<lineCount;i++)
  {
    tLMCalFactorSchema = new LMCalFactorSchema();
    tLMCalFactorSchema.setCalCode(request.getParameter("CalCode"));    
    tLMCalFactorSchema.setFactorCode(tFactorCode[i]);
    tLMCalFactorSchema.setFactorName(tFacotrName[i]);
    tLMCalFactorSchema.setFactorType(tFactorType[i]);
    tLMCalFactorSchema.setFactorGrade(tFactorGrade[i]);
    tLMCalFactorSchema.setFactorCalCode(tFactorCalCode[i]);
    tLMCalFactorSchema.setFactorDefault(tFactorDefault[i]);
    tLMCalFactorSet.add(tLMCalFactorSchema);
    System.out.println("for:"+tFactorCode[i]);
  }
  }
  }
  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.add(tG);
   tVData.addElement(tLMCalModeSchema);
   tVData.addElement(tLMCalFactorSet);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tLMCalMode.submitData(tVData,tOperate);
  }
  catch(Exception ex)
  {
    Content = tOperate+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLMCalMode.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

