<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LMDutyQueryInput.jsp
//程序功能：
//创建日期：2003-10-28 15:15:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312">
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="./LMDutyQuery.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="./LMDutyQueryInit.jsp"%>
  <title>险种机构操作岗位权限控制表</title>
</head>
<body  onload="initForm();" >
  <form action="" method=post name=fm target="fraSubmit">
  <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMDutyGrid1);">
      </td>
      <td class= titleImg>
        请您输入查询条件： 
      </td>
    	</tr>   
    </table>
  <Div  id= "divLMDutyGrid1" style= "display: ''">    
<table  class= common align='center' >
   <TR  class= common>
          <TD  class= title>
            责任代码
          </TD>
          <TD  class= input>
            <Input class= common name=DutyCode >
          </TD>
          <TD  class= title>
            责任名称
          </TD>
          <TD  class= input>
            <Input class= common name=DutyName >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费终止期间
          </TD>
          <TD  class= input>
            <Input class= common name=PayEndYear >
          </TD>
          <TD  class= title>
            缴费终止期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=PayEndYearFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            缴费终止日期计算参照
          </TD>
          <TD  class= input>
            <Input class=code  name=PayEndDateCalRef >
          </TD>
          <TD  class= title>
            缴费终止日期计算方式
          </TD>
          <TD  class= input>
            <Input class= code name=PayEndDateCalMode value="" ondblclick="return showCodeList('PayEndDateCalMode',[this]);" 
            onkeyup="return showCodeListKey('PayEndDateCalMode',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起领期间
          </TD>
          <TD  class= input>
            <Input class= common name=GetYear >
          </TD>
          <TD  class= title>
            起领期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=GetYearFlag value="" ondblclick="return showCodeList('GetYearFlag',[this]);" 
            onkeyup="return showCodeListKey('GetYearFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保险期间
          </TD>
          <TD  class= input>
            <Input class= common name=InsuYear >
          </TD>
          <TD  class= title>
            保险期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=InsuYearFlag value="" ondblclick="return showCodeList('InsuYearFlag',[this]);" 
            onkeyup="return showCodeListKey('InsuYearFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            意外责任期间
          </TD>
          <TD  class= input>
            <Input class= common name=AcciYear >
          </TD>
          <TD  class= title>
            意外责任期间单位
          </TD>
          <TD  class= input>
            <Input class= code name=AcciYearFlag value="" ondblclick="return showCodeList('AcciYearFlag',[this]);" 
            onkeyup="return showCodeListKey('AcciYearFlag',[this]);">
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            计算方法
          </TD>
          <TD  class= input>
            <Input class= code name=CalMode ondblclick="return showCodeList('CalMode',[this]);" 
            onkeyup="return showCodeListKey('CalMode',[this]);">
          </TD>
        </TR>
</table>
  </Div>
          <INPUT VALUE="查询" class= common TYPE=button onclick="easyQueryClick();"> 
          <INPUT VALUE="返回" class= common TYPE=button onclick="returnParent();"> 					
    <table>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMDutyGrid);">
    		</td>
    		<td class= titleImg>
    			 险种机构操作岗位权限控制表查询结果:
    		</td>
    	</tr>
    </table>
  	<Div  id= "divLMDutyGrid" style= "display: ''">
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanLMDutyGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首页" class= common TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class= common TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class= common TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾页" class= common TYPE=button onclick="getLastPage();"> 					
  	</div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
