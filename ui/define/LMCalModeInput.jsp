<%@include file="../common/jsp/UsrCheck.jsp"%>
<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="LMCalModeInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMCalModeInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMCalModeSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMCalMode1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMCalMode1);">
      </td>
      <td class= titleImg>
        算法定义信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMCalMode1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            算法编码
          </TD>
          <TD  class= input>
            <Input class= common name=CalCode verify="算法编码|notnull&len<=6">
          </TD>
          <!--TD  class= title>
            算法类型
          </TD>
          <TD  class= input>
            <Input class= common name=Type verify="算法类型|notnull&len<=1">
          </TD-->
        </TR>
        <TR  class= common>
          <TD  class= title>
            算法内容
          </TD>
          <TD class=input>
		<textarea class=common name=CalSQL rows="2" cols="40" verify="算法内容|notnull">
		</textarea>
	  </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            算法描述
          </TD>
          <TD class=input>
		<textarea class=common name=Remark rows="2" cols="40">
		</textarea>
	  </TD>
        </TR>
      </table>
      <input type=hidden name=CalSQL2 value=''>
    </Div>
    <input name=hideOperate type=hidden value=''>
    <!--算法要素定义（列表） -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
            </td>
            <td class= titleImg>
                     算法要素定义明细
            </td>
    	</tr>
     </table>
    <Div  id= "divLCInsured2" style= "display: ''">
    <table  class= common>
            <tr  class= common>
                    <td text-align: left colSpan=1>
                                  <span id="spanLMCalModeGrid" >
                                  </span>
                            </td>
                    </tr>
            </table>
    </div>
    <!-- 显示或隐藏LMCalModeGrid的信息 -->
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
