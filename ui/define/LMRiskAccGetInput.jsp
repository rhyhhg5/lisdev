<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:49
//创建人  ：CrtHtml程序创建
//更新记录：  更新人 gyj   更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMRiskAccGetInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMRiskAccGetInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMRiskAccGetsave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMRiskAccGet1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMRiskAccGet1);">
      </td>
      <td class= titleImg>
        LMRiskAccGet1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMRiskAccGet1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class= common name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVer >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种名称
          </TD>
          <TD  class= input>
            <Input class= common name=RiskName >
          </TD>
          <TD  class= title>
            保险帐户号码
          </TD>
          <TD  class= input>
            <Input class= common name=InsuAccNo >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保险帐户名称
          </TD>
          <TD  class= input>
            <Input class= common name=InsuAccName >
          </TD>
         </TR>
          <!--TD  class= title>
            给付代码
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            给付名称
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyName >
          </TD>
        </TR-->
      </table>
    </Div>
     <!--（列表） -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
            </td>
            <td class= titleImg>
                     缴费责任明细
            </td>
    	</tr>
     </table>
    <Div  id= "divLCInsured2" style= "display: ''">
    <table  class= common>
            <tr  class= common>
                    <td text-align: left colSpan=1>
                                    <span id="spanLMRiskAccGetGrid" >
                                    </span>
                            </td>
                    </tr>
            </table>
    </div>
     <!--（列表） -->
    <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured5);">
            </td>
            <td class= titleImg>
                     给付责任明细
            </td>
    	</tr>
     </table>
    <Div  id= "divLCInsured5" style= "display: ''">
    <table  class= common>
            <tr  class= common>
                    <td text-align: left colSpan=1>
                                    <span id="spanLMRiskAccPayGrid2" >
                                    </span>
                            </td>
                    </tr>
            </table>
    </div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
