<%
//程序名称：LMRiskPayQuery.js
//程序功能：
//创建日期：2003-10-28 15:15:24
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%
     //添加页面控件的初始化。
%>                            
<script language="JavaScript">
// 输入框的初始化（单记录部分）
function initInpBox()
{ 
  try
  {                                   
 
  }
  catch(ex)
  {
    alert("在LMRiskPayQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}
// 下拉框的初始化
function initSelBox()
{  
  try                 
  {
 
  }
  catch(ex)
  {
    alert("在LMRiskPayQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                        
function initForm()
{
   
  try
  {
     
     initLMRiskPayGrid();
    
  }
  catch(re)
  {
    alert("LMRiskPayQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

/************************************************************
 *               
 *输入：          没有
 *输出：          没有
 *功能：          初始化LMRiskPayGrid
 ************************************************************
 */
var LMRiskPayGrid;          //定义为全局变量，提供给displayMultiline使用
function initLMRiskPayGrid()
  {  
                         
    var iArray = new Array();     
      try
      {
        
        iArray[0]=new Array();
        iArray[0][0]="序号";
        iArray[0][1]="30px";
        iArray[0][2]=100;
        iArray[0][3]=0; 
        
        iArray[1]=new Array();
        iArray[1][0]="险种编码";
        iArray[1][1]="80px";
        iArray[1][2]=100;
        iArray[1][3]=2; 
        iArray[1][4]="RiskCode";              	        //是否引用代码:null||""为不引用
        iArray[1][5]="4";              	                //引用代码对应第几列，'|'为分割符
        iArray[1][9]="险种编码|code:RiskCode&NOTNULL";
        iArray[1][18]=250;
        iArray[1][19]= 0 ;
        
        iArray[2]=new Array();
        iArray[2][0]=" 险种版本";
        iArray[2][1]="120px";
        iArray[2][2]=100;
        iArray[2][3]=2; 
        iArray[2][4]="RiskVer";              	        //是否引用代码:null||""为不引用
        iArray[2][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[2][9]=" 险种版本|code:RiskVer&NOTNULL";
        iArray[2][18]=250;
        iArray[2][19]= 0 ;
        
        iArray[3]=new Array();
        iArray[3][0]="险种名称";
        iArray[3][1]="120px";
        iArray[3][2]=100;
        iArray[3][3]=2; 
        iArray[3][4]="RiskName";              	        //是否引用代码:null||""为不引用
        iArray[3][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[3][9]="险种名称|code:RiskName&NOTNULL";
        iArray[3][18]=250;
        iArray[3][19]= 0 ;    
    
         iArray[4]=new Array();
        iArray[4][0]="催收标记";
        iArray[4][1]="120px";
        iArray[4][2]=100;
        iArray[4][3]=2; 
        iArray[4][4]="UrgePayFlag";              	        //是否引用代码:null||""为不引用
        iArray[4][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[4][9]="催收标记|code:UrgePayFlag&NOTNULL";
        iArray[4][18]=250;
        iArray[4][19]= 0 ;

         iArray[5]=new Array();
        iArray[5][0]="手续费类型";
        iArray[5][1]="120px";
        iArray[5][2]=100;
        iArray[5][3]=2; 
        iArray[5][4]="ChargeType";              	        //是否引用代码:null||""为不引用
        iArray[5][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[5][9]="手续费类型|code:ChargeType";
        iArray[5][18]=250;
        iArray[5][19]= 0 ;
        
         iArray[6]=new Array();
        iArray[6][0]="分解缴费间隔";
        iArray[6][1]="120px";
        iArray[6][2]=100;
        iArray[6][3]=2; 
        iArray[6][4]="CutPayIntv";              	        //是否引用代码:null||""为不引用
        iArray[6][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[6][9]="分解缴费间隔|code:CutPayIntv";
        iArray[6][18]=250;
        iArray[6][19]= 0 ;
        
         iArray[7]=new Array();
        iArray[7][0]="免交涉及加费";
        iArray[7][1]="120px";
        iArray[7][2]=100;
        iArray[7][3]=2; 
        iArray[7][4]="PayAvoidType";              	        //是否引用代码:null||""为不引用
        iArray[7][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[7][9]="免交涉及加费|code:PayAvoidType";
        iArray[7][18]=250;
        iArray[7][19]= 0 ;
        
         iArray[8]=new Array();
        iArray[8][0]="手续费与保费关系";
        iArray[8][1]="120px";
        iArray[8][2]=100;
        iArray[8][3]=2; 
        iArray[8][4]="ChargeAndPrem";              	        //是否引用代码:null||""为不引用
        iArray[8][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[8][9]="手续费与保费关系|code:ChargeAndPrem";
        iArray[8][18]=250;
        iArray[8][19]= 0 ;
        
         iArray[9]=new Array();
        iArray[9][0]="结算日类型";
        iArray[9][1]="120px";
        iArray[9][2]=100;
        iArray[9][3]=2; 
        iArray[9][4]="BalaDateType";              	        //是否引用代码:null||""为不引用
        iArray[9][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[9][9]="结算日类型|code:BalaDateType";
        iArray[9][18]=250;
        iArray[9][19]= 0 ;

         iArray[10]=new Array();
        iArray[10][0]="免交标记";
        iArray[10][1]="120px";
        iArray[10][2]=100;
        iArray[10][3]=2; 
        iArray[10][4]="PayAvoidFlag";              	        //是否引用代码:null||""为不引用
        iArray[10][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[10][9]="免交标记|code:PayAvoidFlag";
        iArray[10][18]=250;
        iArray[10][19]= 0 ;

         iArray[11]=new Array();
        iArray[11][0]="缴费与复效关系";
        iArray[11][1]="120px";
        iArray[11][2]=100;
        iArray[11][3]=2; 
        iArray[11][4]="PayAndRevEffe";              	        //是否引用代码:null||""为不引用
        iArray[11][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[11][9]="缴费与复效关系|code:PayAndRevEffe";
        iArray[11][18]=250;
        iArray[11][19]= 0 ;
        
         iArray[12]=new Array();
        iArray[12][0]="缴费宽限期";
        iArray[12][1]="120px";
        iArray[12][2]=100;
        iArray[12][3]=2; 
        iArray[12][4]="GracePeriod";              	        //是否引用代码:null||""为不引用
        iArray[12][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[12][9]="缴费宽限期|code:GracePeriod";
        iArray[12][18]=250;
        iArray[12][19]= 0 ;
        
         iArray[13]=new Array();
        iArray[13][0]="宽限期单位";
        iArray[13][1]="120px";
        iArray[13][2]=100;
        iArray[13][3]=2; 
        iArray[13][4]="GracePeriodUnit";              	        //是否引用代码:null||""为不引用
        iArray[13][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[13][9]="宽限期单位|code:GracePeriodUnit";
        iArray[13][18]=250;
        iArray[13][19]= 0 ;
        
         iArray[14]=new Array();
        iArray[14][0]="宽限日期计算方式";
        iArray[14][1]="120px";
        iArray[14][2]=100;
        iArray[14][3]=2; 
        iArray[14][4]="GraceDateCalMode";              	        //是否引用代码:null||""为不引用
        iArray[14][5]="6";              	                //引用代码对应第几列，'|'为分割符
        iArray[14][9]="宽限日期计算方式|code:GraceDateCalMode";
        iArray[14][18]=250;
        iArray[14][19]= 0 ;
              
        LMRiskPayGrid = new MulLineEnter( "fm" , "LMRiskPayGrid" ); 
        //这些属性必须在loadMulLine前
        LMRiskPayGrid.mulLineCount = 5;   
        LMRiskPayGrid.displayTitle = 1;
        LMRiskPayGrid.canSel=1;
        LMRiskPayGrid.loadMulLine(iArray);  
      }
      catch(ex)
      {
        alert("初始化LMRiskPayGrid时出错："+ ex);
      }
    }
</script>
