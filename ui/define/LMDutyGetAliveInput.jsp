<html>
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人   GYJ 更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
   <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMDutyGetAliveInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMDutyGetAliveInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMDutyGetAliveSave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMDutyGetAlive1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMDutyGetAlive1);">
      </td>
      <td class= titleImg>
        LMDutyGetAlive1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMDutyGetAlive1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            给付代码
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyCode >
          </TD>
          <TD  class= title>
            给付名称
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyName >
          </TD>
        </TR>
       </table>
     </Div>
     <!--（列表） -->
     <table>
    	<tr>
            <td class=common>
                <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLCInsured2);">
            </td>
            <td class= titleImg>
                     责任给付陪付明细
            </td>
    	</tr>
     </table>
     <Div  id= "divLCInsured2" style= "display: ''">
     <table  class= common>
            <tr  class= common>
                    <td text-align: left colSpan=1>
                                  <span id="spanLMDutyGetAliveGrid" >
                                  </span>
                            </td>
                    </tr>
            </table>
     </div>
     <script language="javascript">
	function hideMe()
	{
 		 divLMDutyGetAliveDetail.style.display ='none';
	}
    </script>
    <Div  id= "divLMDutyGetAliveDetail" style="display: none; position:absolute; slategray">
      <table  class= common BORDER=1 CELLPADDING=2 CELLSPACING=0>
        <TR  class= common>
          <TD  class= title>
            序号/给付责任类型
          </TD>
          <TD  class= input>
            <Input class= common name=GetDutyKind >
          </TD>
          <TD  class= title>
            给付类型
          </TD>
          <TD  class= input>
            <Input class= common name=Type >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            给付间隔
          </TD>
          <TD  class= input>
            <Input class= common name=GetIntv >
          </TD>
          <TD  class= title>
            默认值
          </TD>
          <TD  class= input>
            <Input class= common name=DefaultVal >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            算法
          </TD>
          <TD  class= input>
            <Input class= common name=CalCode >
          </TD>
          <TD  class= title>
            反算算法
          </TD>
          <TD  class= input>
            <Input class= common name=CnterCalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            其他算法
          </TD>
          <TD  class= input>
            <Input class= common name=OthCalCode >
          </TD>
          <TD  class= title>
            起领期间
          </TD>
          <TD  class= input>
            <Input class= common name=GetStartPeriod >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起领期间单位
          </TD>
          <TD  class= input>
            <Input class= common name=GetStartUnit >
          </TD>
          <TD  class= title>
            起领日期计算参照
          </TD>
          <TD  class= input>
            <Input class= common name=StartDateCalRef >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            起领日期计算方式
          </TD>
          <TD  class= input>
            <Input class= common name=StartDateCalMode >
          </TD>
          <TD  class= title>
            起领期间上限
          </TD>
          <TD  class= input>
            <Input class= common name=MinGetStartPeriod >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            止领期间
          </TD>
          <TD  class= input>
            <Input class= common name=GetEndPeriod >
          </TD>
          <TD  class= title>
            止领期间单位
          </TD>
          <TD  class= input>
            <Input class= common name=GetEndUnit >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            止领日期计算参照
          </TD>
          <TD  class= input>
            <Input class= common name=EndDateCalRef >
          </TD>
          <TD  class= title>
            止领日期计算方式
          </TD>
          <TD  class= input>
            <Input class= common name=EndDateCalMode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            止领期间下限
          </TD>
          <TD  class= input>
            <Input class= common name=MaxGetEndPeriod >
          </TD>
          <TD  class= title>
            递增标记
          </TD>
          <TD  class= input>
            <Input class= common name=AddFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            递增间隔
          </TD>
          <TD  class= input>
            <Input class= common name=AddIntv >
          </TD>
          <TD  class= title>
            递增开始期间
          </TD>
          <TD  class= input>
            <Input class= common name=AddStartPeriod >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            递增开始期间单位
          </TD>
          <TD  class= input>
            <Input class= common name=AddStartUnit >
          </TD>
          <TD  class= title>
            递增终止期间
          </TD>
          <TD  class= input>
            <Input class= common name=AddEndPeriod >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            递增终止期间单位
          </TD>
          <TD  class= input>
            <Input class= common name=AddEndUnit >
          </TD>
          <TD  class= title>
            递增类型
          </TD>
          <TD  class= input>
            <Input class= common name=AddType >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            递增值
          </TD>
          <TD  class= input>
            <Input class= common name=AddValue >
          </TD>
          <TD  class= title>
            给付最大次数
          </TD>
          <TD  class= input>
            <Input class= common name=MaxGetCount >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            给付后动作
          </TD>
          <TD  class= input>
            <Input class= common name=AfterGet >
          </TD>
          <TD  class= title>
            领取动作类型
          </TD>
          <TD  class= input>
            <Input class= common name=GetActionType >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            催收标记
          </TD>
          <TD  class= input>
            <Input class= common name=UrgeGetFlag >
          </TD>
          <TD  class= title>
            贴现领取标记
          </TD>
          <TD  class= input>
            <Input class= common name=DiscntFlag >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            领取条件
          </TD>
          <TD  class= input>
            <Input class= common name=GetCond >
          </TD>
        </TR>
        <tr ><td align=right><input type="button" value="确认修改" onclick="hideMe();">
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
