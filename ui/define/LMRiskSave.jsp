<%
//程序名称：LMRiskInput.jsp
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.db.*"%>
  <%@page import="com.sinosoft.lis.vdb.*"%>
  <%@page import="com.sinosoft.lis.sys.*"%>

<%@page contentType="text/html;charset=GBK" %>

<%
  //接收信息，并作校验处理。
  //输入参数
  LMRiskSchema tLMRiskSchema   = new LMRiskSchema();

  UILMRisk tLMRisk   = new UILMRisk();

  //输出参数
  CErrors tError = null;
  //后面要执行的动作：添加，修改，删除
  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  String transact = "insert";
  
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";

    tLMRiskSchema.setRiskCode(request.getParameter("RiskCode"));
    tLMRiskSchema.setRiskVer(request.getParameter("RiskVer"));
    tLMRiskSchema.setRiskName(request.getParameter("RiskName"));
    tLMRiskSchema.setChoDutyFlag(request.getParameter("ChoDutyFlag"));
    tLMRiskSchema.setCPayFlag(request.getParameter("CPayFlag"));
    tLMRiskSchema.setGetFlag(request.getParameter("GetFlag"));
    tLMRiskSchema.setEdorFlag(request.getParameter("EdorFlag"));
    tLMRiskSchema.setRnewFlag(request.getParameter("RnewFlag"));
    tLMRiskSchema.setUWFlag(request.getParameter("UWFlag"));
    tLMRiskSchema.setRinsFlag(request.getParameter("RinsFlag"));
    tLMRiskSchema.setInsuAccFlag(request.getParameter("InsuAccFlag"));


  try
  {
  // 准备传输数据 VData
   VData tVData = new VData();

   //此处需要根据实际情况修改
   tVData.addElement(tLMRiskSchema);
    
   //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
   tLMRisk.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = transact+"失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = t%functionname%.mErrors;
    if (!tError.needDealError())
    {                          
      Content = " 保存成功";
    	FlagStr = "Succ";
    }
    else                                                                           
    {
    	Content = " 保存失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
  //添加各种预处理

%>                      
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

