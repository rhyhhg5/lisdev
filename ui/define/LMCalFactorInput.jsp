<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:48
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="LMCalFactorInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMCalFactorInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMCalFactorsave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMCalFactor1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMCalFactor1);">
      </td>
      <td class= titleImg>
        LMCalFactor1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMCalFactor1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            算法编码
          </TD>
          <TD  class= input>
            <Input class= common name=CalCode >
          </TD>
          <TD  class= title>
            要素编码
          </TD>
          <TD  class= input>
            <Input class= common name=FactorCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            要素名称
          </TD>
          <TD  class= input>
            <Input class= common name=FactorName >
          </TD>
          <TD  class= title>
            要素类型
          </TD>
          <TD  class= input>
            <Input class= common name=FactorType >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            要素优先级
          </TD>
          <TD  class= input>
            <Input class= common name=FactorGrade >
          </TD>
          <TD  class= title>
            要素算法编码
          </TD>
          <TD  class= input>
            <Input class= common name=FactorCalCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            默认值
          </TD>
          <TD  class= input>
            <Input class= common name=FactorDefault >
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
