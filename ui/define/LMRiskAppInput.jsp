<html> 
<%
//程序名称：
//程序功能：
//创建日期：2002-08-09 17:55:57
//创建人  ：CrtHtml程序创建
//更新记录：  更新人:GYJ    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head >
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <!--SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT-->
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="LMRiskAppInput.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <%@include file="LMRiskAppInit.jsp"%>
</head>
<body  onload="initForm();" >
  <form action="./LMRiskAppsave.jsp" method=post name=fm target="fraSubmit">
    <%@include file="../common/jsp/OperateButton.jsp"%>
    <%@include file="../common/jsp/InputButton.jsp"%>
        <!-- 显示或隐藏LMRiskApp1的信息 -->
    <table>
      <tr>
      <td>
        <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divLMRiskApp1);">
      </td>
      <td class= titleImg>
        LMRiskApp1信息
      </td>
    	</tr>
    </table>

    <Div  id= "divLMRiskApp1" style= "display: ''">
      <table  class= common>
        <TR  class= common>
          <TD  class= title>
            险种编码
          </TD>
          <TD  class= input>
            <Input class= common name=RiskCode >
          </TD>
          <TD  class= title>
            险种版本
          </TD>
          <TD  class= input>
            <Input class= common name=RiskVer >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种名称
          </TD>
          <TD  class= input>
            <Input class= common name=RiskName >
          </TD>
          <TD  class= title>
            险类编码
          </TD>
          <TD  class= input>
            <Input class= common name=KindCode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种分类
          </TD>
          <TD  class= input>
            <Input class= common name=RiskType >
          </TD>
          <TD  class= title>
            险种分类1
          </TD>
          <TD  class= input>
            <Input class= common name=RiskType1 >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种分类2
          </TD>
          <TD  class= input>
            <Input class= common name=RiskType2 >
          </TD>
          <TD  class= title>
            险种性质
          </TD>
          <TD  class= input>
            <Input class= common name=RiskProp >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种类别
          </TD>
          <TD  class= input>
            <Input class= common name=RiskPeriod >
          </TD>
          <TD  class= title>
            险种细分
          </TD>
          <TD  class= input>
            <Input class= common name=RiskTypeDetail >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            险种标记
          </TD>
          <TD  class= input>
            <Input class= codeno name=RiskFlag  ondblclick="return showCodeList('RiskFlag',[this,RiskFlagName],[0,1]);"  onkeyup="return showCodeListKey('RiskFlag',[this,RiskFlagName],[0,1]);"><input class=codename name=RiskFlagName>          
          </TD>
          <TD  class= title>
            保单类型
          </TD>
          <TD  class= input>
            <Input class= codeno name=PolType value="" ondblclick="return showCodeList('PolType',[this,PolTypeName],[0,1]);" 
            onkeyup="return showCodeListKey('PolType',[this,PolTypeName],[0,1]);"><input class=codename name=PolTypeName>          
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            投资标记
          </TD>
          <TD  class= input>
            
            <input type="checkbox" name=InvestFlag >是
          </TD>
          <TD  class= title>
            分红标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=BonusFlag >是
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            红利领取方式
          </TD>
          <TD  class= input>
            <Input class= codeno name=BonusMode value="" ondblclick="return showCodeList('BonusMode',[this,BonusModeName],[0,1]);" 
            onkeyup="return showCodeListKey('BonusMode',[this,BonusModeName],[0,1]);"><input class=codename name=BonusModeName>          
          </TD>
          <TD  class= title>
            有无名单标记
          </TD>
          <TD  class= input>
            <Input class= codeno name=ListFlag value=""  ondblclick="return showCodeList('ListFlag',[this,ListFlagName],[0,1]);" 
            onkeyup="return showCodeListKey('ListFlag',[this,ListFlagName],[0,1]);"><input class=codename name=ListFlagName>          
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            主附险标记
          </TD>
          <TD  class= input>
            <Input class= codeno name=SubRiskFlag value="" ondblclick="return showCodeList('SubRiskFlag',[this,SubRiskFlagName],[0,1]);" 
            onkeyup="return showCodeListKey('SubRiskFlag',[this,SubRiskFlagName],[0,1]);"><input class=codename name=SubRiskFlagName>          
          </TD>
          <TD  class= title>
            计算精确位
          </TD>
          <TD  class= input>
            <Input class= common name=CalDigital >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            计算取舍方法
          </TD>
          <TD  class= input>
            <Input class= common name=CalChoMode >
          </TD>
          <TD  class= title>
            风险保额倍数
          </TD>
          <TD  class= input>
            <Input class= common name=RiskAmntMult >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保险期限标志
          </TD>
          <TD  class= input>
            <Input class= codeno name=InsuPeriodFlag value="" ondblclick="return showCodeList('InsuPeriodFlag',[this,InsuPeriodFlagName],[0,1]);" 
            onkeyup="return showCodeListKey('InsuPeriodFlag',[this,InsuPeriodFlagName],[0,1]);"><input class=codename name=InsuPeriodFlagName>          
          </TD>
          <TD  class= title>
            保险期间上限
          </TD>
          <TD  class= input>
            <Input class= common name=MaxEndPeriod >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            满期截至年龄
          </TD>
          <TD  class= input>
            <Input class= common name=AgeLmt >
          </TD>
          <TD  class= title>
            签单日算法
          </TD>
          <TD  class= input>
            <Input class= common name=SignDateCalMode >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            协议险标记
          </TD>
          <TD  class= input>
            <Input class= common name=ProtocolFlag >
          </TD>
          <TD  class= title>
            协议险给付金改变标记
          </TD>
          <TD  class= input>
            <input type="checkbox"  name=GetChgFlag >可改
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            协议缴费标记
          </TD>
          <TD  class= input>
            <Input class= codeno name=ProtocolPayFlag value="" ondblclick="return showCodeList('ProtocolPayFlag',[this,ProtocolPayFlagName],[0,1]);" 
            onkeyup="return showCodeListKey('ProtocolPayFlag',[this,ProtocolPayFlagName],[0,1]);"><input class=codename name=ProtocolPayFlagName>          
          </TD>
          <TD  class= title>
            保障计划标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=EnsuPlanFlag >固定档期
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            保障计划调整标记
          </TD>
          <TD  class= input>
            <Input class=codeno name=EnsuPlanAdjFlag value="" ondblclick="return showCodeList('EnsuPlanAdjFlag',[this,EnsuPlanAdjFlagName],[0,1]);" 
            onkeyup="return showCodeListKey('EnsuPlanAdjFlag',[this,EnsuPlanAdjFlagName],[0,1]);"><input class=codename name=EnsuPlanAdjFlagName>          
          </TD>
          <TD  class= title>
            开办日期
          </TD>
          <TD  class= input>
            <Input class= common name=StartDate >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            停办日期
          </TD>
          <TD  class= input>
            <Input class= common name=EndDate >
          </TD>
          <TD  class= title>
            最小投保人年龄
          </TD>
          <TD  class= input>
            <Input class= common name=MinAppntAge >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最大投保人年龄
          </TD>
          <TD  class= input>
            <Input class= common name=MaxAppntAge >
          </TD>
          <TD  class= title>
            最大被保人年龄
          </TD>
          <TD  class= input>
            <Input class= common name=MaxInsuredAge >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            最小被保人年龄
          </TD>
          <TD  class= input>
            <Input class= common name=MinInsuredAge >
          </TD>
          <TD  class= title>
            投保使用利率
          </TD>
          <TD  class= input>
            <Input class= common name=AppInterest >
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            投保使用费率
          </TD>
          <TD  class= input>
            <Input class= common name=AppPremRate >
          </TD>
          <TD  class= title>
            多被保人标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=InsuredFlag >多被保人
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            共保标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=ShareFlag >可供保
            
          </TD>
          <TD  class= title>
            受益人标记
          </TD>
          <TD  class= input>
            <Input class=codeno name=BnfFlag value="" ondblclick="return showCodeList('BnfFlag',[this,BnfFlagName],[0,1]);" 
            onkeyup="return showCodeListKey('BnfFlag',[this,BnfFlagName],[0,1]);"><input class=codename name=BnfFlagName>          
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            暂缴费标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=TempPayFlag >需要
          </TD>
          <TD  class= title>
            录入缴费计划
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=InpPayPlan >要求
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            告知标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=ImpartFlag >是
          </TD>
          <TD  class= title>
            保险经历标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=InsuExpeFlag >是
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            提供借款标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=LoanFalg >是
          </TD>
          <TD  class= title>
            抵押标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=MortagageFlag >是
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            利差返还标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=IDifReturnFlag >是
          </TD>
          <TD  class= title>
            减额缴清标记
          </TD>
          <TD  class= input>
            <Input type="checkbox" name=CutAmntStopPay >能
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            分保率
          </TD>
          <TD  class= input>
            <Input class= common name=RinsRate >
          </TD>
          <TD  class= title>
            销售标记
          </TD>
          <TD  class= input>
            <Input class= codeno name=SaleFlag value="" ondblclick="return showCodeList('SaleFlag',[this,SaleFlagName],[0,1]);" 
            onkeyup="return showCodeListKey('SaleFlag',[this,SaleFlagName],[0,1]);"><input class=codename name=SaleFlagName>          
          </TD>
        </TR>
        <TR  class= common>
          <TD  class= title>
            磁盘文件投保标记
          </TD>
          <TD  class= input>
            <input type="checkbox" name=FileAppFlag >允许
          </TD>
          <TD  class= title>
            管理部门
          </TD>
          <TD  class= input>
            <Input class= codeno name=MngCom value="" ondblclick="return showCodeList('MngCom',[this,MngComName],[0,1]);" 
            onkeyup="return showCodeListKey('MngCom',[this,MngComName],[0,1]);"><input class=codename name=MngComName>          
          </TD>
        </TR>
      </table>
    </Div>
  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
