var turnPage = new turnPageClass();

function beforeSubmit(){
/*
	if(fm.all('StartDate').value<=getCurrentDate('-')){
		alert("保证利率的起始日期只能晚于当前日期.");
		return false;
	}
*/
	var strSQL = "select RiskCode,Rate,RateType,RateIntv,StartDate,EndDate from LDPromiseRate where "	
		         +"1=1"			 	
				 + getWherePart( 'RiskCode','RiskCode' )
				 + getWherePart( 'Rate','Rate','',1 )
				 + getWherePart( 'RateType','RateType' )
				 + getWherePart( 'RateIntv','RateIntv' )
				 + getWherePart( 'PromulgateDate','PromulgateDate' )
				 + getWherePart( 'StartDate','StartDate' )
				 + getWherePart( 'EndDate','EndDate');
          
    var resultSQL = easyExecSql(strSQL);
    if(resultSQL!=null)
    {
      alert("该保证利率设定已存在！不能重复保存");
      return false;
    }
    
	strSQL = "SELECT max(EndDate)  FROM LDPROMISERATE WHERE RISKCODE = '"+fm.RiskCode.value+"' "
	resultSQL=easyExecSql(strSQL);
	if (resultSQL !="" && resultSQL!=null && resultSQL[0][0] != fm.StartDate.value){
		alert("保证利率的起始日期必须为上次配置保证利率的结束日期。");
		return false;
	}	
	strSQL="SELECT *  FROM LDPROMISERATE WHERE RISKCODE = '"+fm.RiskCode.value+"' "
				+" AND ((DATE('"+fm.StartDate.value+"') >= STARTDATE AND "
				+" DATE('"+fm.EndDate.value+"') <= ENDDATE) OR"
				+" (DATE('"+fm.StartDate.value+"') < ENDDATE AND"
				+" DATE('"+fm.EndDate.value+"') >= ENDDATE) OR"
				+" (DATE('"+fm.StartDate.value+"') <= STARTDATE AND"
				+" DATE('"+fm.EndDate.value+"') >= STARTDATE) OR"
				+" DATE('"+fm.EndDate.value+"') <= STARTDATE)";
				//+" AND enddate <> DATE('3000-01-01')"
	resultSQL=easyExecSql(strSQL);
	if (resultSQL!=null){
		alert("该险种保证利率设定的时间区间与已有的时间区间部分重复！不能保存。");
		return false;
	}
	return true;
}

function submitForm()
{
   if( verifyInput2() == false ) return false;
   if(!beforeSubOrUP()){
		return false;
  	}
   if(!beforeSubmit()){
		return false;
	}
   fm.fmtransact.value="INSERT";
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   fm.submit(); //提交
   queryClick();
   initDiv();
}

function afterSubmit(FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}
function updateClick()
{
   if (RateGrid.getSelNo() == 0)
   {
    alert("请先选择一条要修改的保证利率信息！");
    return false;
   }
   if(!beforeSubOrUP()){
	return false;
   }
   //if(!beforeStartDate()){
   //    	return false;
   //}
   if(!beforeDelOrUp()){
          	return false;
   }
       /*
       if(fm.StartDate.value!=RateGrid.getRowColData(RateGrid.getSelNo() - 1, 5) ){
       	alert("不能修改起始日期!");
       	return false;
       }
       */
  if(fm.all('Rate').value!=fm.all('hiddenRate').value){
  	 if(!confirm("利率发生了变更，原来是"+fm.all('hiddenRate').value+"，现在变更为"+fm.all('Rate').value+"，是否确定？")) 
  		 return false;
  }
       if (confirm("您确实想修改该记录吗?"))
       {
          var i = 0;
          var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
          fm.fmtransact.value = "UPDATE";
          fm.submit(); 
          queryClick();
//          alert("query");
			initDiv();
       }
       else
       {
         alert("您取消了修改操作！");
       }
}

function queryClick()
{
		strSQL = "select RiskCode,Rate,RateType,RateIntv,StartDate,EndDate,PromulgateDate from LDPromiseRate where "	
		         +"1=1"			 	
				 + getWherePart( 'RiskCode','RiskCode' )
				 + getWherePart( 'Rate','Rate','',1 )
				 + getWherePart( 'PromulgateDate','PromulgateDate' )
				 + getWherePart( 'StartDate','StartDate' )
				 + getWherePart( 'EndDate','EndDate')
				 + getWherePart( 'RateType','RateType' )
				 + getWherePart( 'RateIntv','RateIntv' )
				 +" order by riskcode,startdate";
	   turnPage.queryModal(strSQL,RateGrid,1,1);
}

function deleteClick()
{
          if (RateGrid.getSelNo() == 0)
          {
           alert("请先选择一条要删除的保证利率信息！");
           return false;
          }
          /*
          if(fm.EndDate.value!='3000-01-01'){
          	alert("不能删除险种整个公布时间中间区间的保证利率.");
          	return false;
          }
          */
          //by gzh 20101224
                
          strSQL2 = "select RiskCode,Rate,StartDate,EndDate from LDPromiseRate where "	
		         +"1=1"			 	
				 + getWherePart( 'RiskCode','RiskCode' )
				 + getWherePart( 'Rate','Rate','',1 )
				 + getWherePart( 'StartDate','StartDate' )
				 + getWherePart( 'EndDate','EndDate');
          
          result1 = easyExecSql(strSQL2);
          
          if(result1==null)
          {
            alert("该保证利率设定不存在！");
            return false;
          }
          if(!beforeStartDate()){
          	return false;
          }
          if(!beforeDelOrUp()){
          	return false;
          }
  if (confirm("您确实想删除该日期下的信息记录吗?"))
  {
     var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     fm.fmtransact.value = "DELETE";
     fm.submit(); //提交
     initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}

function RateShow()
{
    fm.RiskCode.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 1);
    fm.Rate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 2);
    fm.DRate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 2);
    fm.hiddenRate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 2);
    fm.RateType.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 3);
    if(fm.RateType.value=="S"){
    	fm.RateTypeName.value="单利";
    }else if(fm.RateType.value=="C"){
    	fm.RateTypeName.value="复利";
    }
    fm.RateIntv.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 4);
    if(fm.RateIntv.value=="D"){
    	fm.RateIntvName.value="日";
    }else if(fm.RateIntv.value=="M"){
    	fm.RateIntvName.value="月";
    }else if(fm.RateIntv.value=="Y"){
    	fm.RateIntvName.value="年";
    }
    fm.StartDate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 5);
    fm.EndDate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 6);

    fm.RiskCode1.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 1);
    fm.Rate1.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 2);
//    fm.RateType1.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 3);
//    fm.RateIntv1.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 4);
    fm.StartDate1.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 5);
    fm.EndDate1.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 6);
    fm.PromulgateDate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 7);
    
              strSQL2 = "select makedate,maketime from LDPromiseRate where "	
		         +"1=1"			 	
				 + getWherePart( 'RiskCode','RiskCode' )
				 + getWherePart( 'Rate','Rate','',1 )
				 + getWherePart( 'StartDate','StartDate' )
				 + getWherePart( 'EndDate','EndDate');
          
          result1 = easyExecSql(strSQL2);
          fm.MakeDate.value=result1[0][0];
          fm.MakeTime.value=result1[0][1];
}

function add2Months(){
	//alert("add2Months");
	if(!isDate(fm.PromulgateDate.value)){
		alert("公布日期不是合法的日期格式!");
		return false;
	}
	//var sql="SELECT add_months(to_date('"+fm.PromulgateDate.value+"','YYYY-MM-DD'),2) FROM dual ";
	// by gzh 20101223
	var sql="SELECT add_months(date('"+fm.PromulgateDate.value+"'),2) FROM dual ";
	fm.StartDate.value=easyExecSql(sql);
}
function beforeStartDate(){
	//alert(getCurrentDate("-")+' '+fm.StartDate.value);
	if(getCurrentDate("-")>fm.StartDate.value){
	  alert("不能删除或修改起始时间比系统早的险种保证利率.");
	  return false;
	}
	return true;
}
function afterStartDate(){
	//alert(getCurrentDate("-")+' '+fm.StartDate.value);
	if(getCurrentDate("-")>fm.StartDate.value){
	  alert("不能删除或修改起始时间比系统早的险种保证利率.");
	  return false;
	}
	return true;
}
//不能删除或修改起始时间比最晚结算日期早的险种保证利率
function beforeDelOrUp(){
	var delSQL="SELECT *  FROM LDPROMISERATE WHERE RISKCODE = '"+fm.RiskCode.value+"' "
				+" AND (DATE('"+fm.EndDate.value+"') < ENDDATE )";
  	var delReslut=easyExecSql(delSQL);
  	if(delReslut !=null){
        alert("不能删除或修改险种整个公布时间中间区间的保证利率.");
        return false;
    } 
	var sql = "select * from lcinsureacc where riskcode = '"+fm.RiskCode.value+"' and baladate>'"+fm.StartDate.value+"'";
	var resultSQL=easyExecSql(sql);
	if (resultSQL!=null){
		alert("不能删除或修改起始时间比最晚结算日期早的险种保证利率!");
		return false;
	}
	return true;	
}
//保存或修改前校验
function beforeSubOrUP(){
	var StartDate = fm.StartDate.value.split("-");
	var EndDate = fm.EndDate.value.split("-");
	if(!checkRate())
  	{
    return false;
  	}
	if(StartDate[1]!="01" || StartDate[2]!="01"){
		alert("保证利率的起始日期必须为1月1日！");
		fm.StartDate.focus();
		return false;
	}
	if(EndDate[1]!="01" || EndDate[2]!="01"){
		alert("保证利率的结束日期必须为1月1日！");
		fm.EndDate.focus();
		return false;
	}
	if((EndDate[0]-StartDate[0])!=1){
		alert("结束日期必须为起始日期的次年");
		return false;
	}
	if(fm.EndDate.value<fm.StartDate.value)
    {
    alert("结束时间不应早于起始时间！");
    return false;
    }
    return true;
}
//利率与确认利率是否一致
//检查利率
function checkRate()
{
  var rate1 = fm.Rate.value;
  var rate2 = fm.DRate.value;
  if(rate1 != rate2)
  {
    alert("两次输入的利率不一致，请校对后重新输入！");
    return false;
  }
  else
  {
    if(rate1 < 1)
    {
      return true;
    }
    alert("输入的利率有误，请输入一个小于1的小数！");
    return false;
  }
  return true;
}