<%
//程序名称：LDPromiseRateInit.jsp
//程序功能：保证汇率的设定
//创建日期：2007-05-17 12:06:45
//创建人  ：李磊
//更新记录：  更新人    更新日期     更新原因/内容
%>
<script language="JavaScript">
function initDiv(){
	fm.operator.value=operator;;
    fm.RiskCode.value="";
    fm.RiskCodeName.value="";
    fm.Rate.value="";
    fm.DRate.value="";
    fm.PromulgateDate.value="";
    fm.StartDate.value="";
    fm.EndDate.value="";
    fm.MakeDate.value="";
    fm.MakeTime.value="";
    fm.ModifyDate.value="";
    fm.ModifyTime.value="";
    fm.RiskCode1.value="";
    fm.Rate1.value="";
    fm.StartDate1.value="";
    fm.EndDate1.value="";
    fm.RateType.value="C";
    fm.RateTypeName.value="复利";
    fm.RateIntv.value="Y";
    fm.RateIntvName.value="年";
    setDateInfo();
    setTimeInfo();
	
}
function initForm()
{
  try
  {
    initRateGrid();
    initDiv();
  }
  catch(ex)
  {
    alert("在LDWorkTimeInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }   
}
function setDateInfo()//获取当前日期
{
	  var d = new Date();
	  var h = d.getYear();
	  var m = d.getMonth(); 
	  var day = d.getDate();  
	  var Date1;       
	  if(h<10){h = "0"+d.getYear();}  
	  if(m<9){ m++; m = "0"+m;}
	  else{m++;}
	  if(day<10){day = "0"+d.getDate();}
	  Date1 = h+"-"+m+"-"+day;                                  
    fm.MakeDate.value = Date1;
    fm.ModifyDate.value = Date1;
}  
function setTimeInfo()//获取当前时间
{
	  var d = new Date();              
	  var h = d.getHours();            
	  var m = d.getMinutes();         
	  if(h<10){h = "0"+d.getHours();}  
	  if(m<10){m = "0"+d.getMinutes();}
	  fm.MakeTime.value = h+":"+m+":"+"00";	
	  fm.ModifyTime.value = h+":"+m+":"+"00";		
} 

function initRateGrid()
  {                              
    var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许
  
      iArray[1]=new Array();
      iArray[1][0]="险种编码";    	//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="利率";         			//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=50;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="利率类型";         			//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
                           
      iArray[4]=new Array();
      iArray[4][0]="利率间隔";         			//列名
      iArray[4][1]="100px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[5]=new Array();
      iArray[5][0]="起始日期";         			//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=50;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[6]=new Array();
      iArray[6][0]="结束日期";         			//列名
      iArray[6][1]="80px";            		//列宽
      iArray[6][2]=50;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
     
      iArray[7]=new Array();
      iArray[7][0]="公布日期";         			//列名
      iArray[7][1]="80px";            		//列宽
      iArray[7][2]=50;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      RateGrid = new MulLineEnter( "fm" , "RateGrid" );                       
      RateGrid.mulLineCount = 0;
      RateGrid.displayTitle = 1;
      RateGrid.locked = 1;
      RateGrid.canSel = 1;
      RateGrid.hiddenPlus = 1;
      RateGrid.hiddenSubtraction = 1;

      RateGrid.loadMulLine(iArray); 
          
      RateGrid. selBoxEventFuncName = "RateShow";      

      }
      catch(ex)
      {
        alert(ex);
      }
}



</script>