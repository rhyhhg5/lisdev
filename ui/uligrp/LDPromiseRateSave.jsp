<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：LDPromiseRateSave.jsp
//程序功能：保证汇率的设定
//创建日期：2007-05-17 14:56:57
//创建人  ：李磊
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.uligrp.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  
<%
  //接收信息，并作校验处理。
  //输入参数
  LDPromiseRateSchema tLDPromiseRateSchema   = new LDPromiseRateSchema();  
  LDPromiseRateSchema tLDPromiseRateSchema1   = new LDPromiseRateSchema();
  
  LDPromiseRateUI tLDPromiseRateUI   = new LDPromiseRateUI();
  LDPromiseRateSet tLDPromiseRateSet=new LDPromiseRateSet();
  //输出参数
  CErrors tError = null;
  String tRela  = "";                
  String FlagStr = "";
  String Content = "";
  String transact = "";
  GlobalInput tG = new GlobalInput(); 
  tG=(GlobalInput)session.getValue("GI");

  //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
  transact = request.getParameter("fmtransact");
    String Operator  = tG.Operator ;  //保存登陆管理员账号
    
    
    tLDPromiseRateSchema.setRiskCode(request.getParameter("RiskCode"));
    tLDPromiseRateSchema.setPromulgateDate(request.getParameter("PromulgateDate"));
    tLDPromiseRateSchema.setStartDate(request.getParameter("StartDate"));
    tLDPromiseRateSchema.setEndDate(request.getParameter("EndDate"));
    tLDPromiseRateSchema.setRateType(request.getParameter("RateType"));
    tLDPromiseRateSchema.setRateIntv(request.getParameter("RateIntv"));
    if(transact.equals("DELETE"))
    {
    }
    else
    {
    tLDPromiseRateSchema.setRate(request.getParameter("Rate"));
    tLDPromiseRateSchema.setOperator(request.getParameter("operator"));
    tLDPromiseRateSchema.setMakeDate(request.getParameter("MakeDate"));
    tLDPromiseRateSchema.setMakeTime(request.getParameter("MakeTime"));
    tLDPromiseRateSchema.setModifyDate(request.getParameter("ModifyDate"));
    tLDPromiseRateSchema.setModifyTime(request.getParameter("ModifyTime"));
    
    }
    tLDPromiseRateSet.add(tLDPromiseRateSchema);
     if(transact.equals("UPDATE"))  
    {
	    tLDPromiseRateSchema1.setRiskCode(request.getParameter("RiskCode1"));
	    tLDPromiseRateSchema1.setStartDate(request.getParameter("StartDate1"));
	    tLDPromiseRateSchema1.setEndDate(request.getParameter("EndDate1"));  
	    tLDPromiseRateSet.add(tLDPromiseRateSchema1);
	}

  try
  {
  // 准备传输数据 VData
  	VData tVData = new VData();
  	tVData.add(tLDPromiseRateSet);
  	tVData.add(tG);
  	tLDPromiseRateUI.submitData(tVData,transact);
  }
  catch(Exception ex)
  {
    Content = "操作失败，原因是:" + ex.toString();
    FlagStr = "Fail";
  }
  
//如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr=="")
  {
    tError = tLDPromiseRateUI.mErrors;
    if (!tError.needDealError())
    {                          
    	Content = " 操作成功! ";
    	FlagStr = "Success";
    }
    else                                                                           
    {
    	Content = " 操作失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
 
  //添加各种预处理
%>                      
<%=Content%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>