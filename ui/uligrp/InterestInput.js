var turnPage = new turnPageClass();

function beforeSubmit(){
/*
	if(fm.all('StartDate').value<=getCurrentDate('-')){
		alert("结算利率的起始日期只能晚于当前日期.");
		return false;
	}
*/
	if(!checkInpBox())
  	{
    return false;
  	}
  	if(!checkInsert())
  	{
    return false;
  	}
  	if(fm.GrpContNo.value !="" && fm.GrpContNo.value !=null){
		var sql = "select 1 from lcgrppol where GrpContNo = '"+fm.GrpContNo.value+"' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')";
		var resultSQL=easyExecSql(sql);
		if(resultSQL==null || resultSQL =="")
    	{
      		alert("团体保单号为"+fm.GrpContNo.value+"的保单不是团体万能保单！");
      		return false;
    	}
	}
  	/*
	var strSQL = "select RiskCode,Rate,RateType,RateIntv,StartDate,EndDate from interest000001 where "	
		         +"1=1"			 	
				 + getWherePart( 'RiskCode','RiskCode' )
				 + getWherePart( 'Rate','Rate','',1 )
				 + getWherePart( 'RateType','RateType' )
				 + getWherePart( 'RateIntv','RateIntv' )
				 + getWherePart( 'PromulgateDate','PromulgateDate' )
				 + getWherePart( 'StartDate','StartDate' )
				 + getWherePart( 'EndDate','EndDate');
    var resultSQL = easyExecSql(strSQL);
    if(resultSQL!=null)
    {
      alert("该结算利率设定已存在！不能重复保存");
      return false;
    }
    var strSQL = "SELECT max(EndDate)  FROM interest000001 WHERE RISKCODE = '"+fm.RiskCode.value+"' and GrpContNo = '000000'"
		var resultSQL=easyExecSql(strSQL);
		if (resultSQL!=null && resultSQL!="" && resultSQL[0][0] != fm.StartDate.value){
		alert("结算利率的起始日期必须为上次配置结算利率的结束日期。");
		return false;
		}
	*/	
	return true;
}

function submitForm()
{
   if( verifyInput2() == false ) return false;
   
   if(!beforeSubmit()){
		return false;
	}
	/*
   if(!beforeSubOrUP()){
		return false;
  	}
  	*/
   fm.fmtransact.value="INSERT";
   var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
   var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
   showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
   fm.submit(); //提交
   queryClick();
   initDiv();
}

function afterSubmit(FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else
  { 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
}
function updateClick()
{
   if (RateGrid.getSelNo() == 0)
   {
    alert("请先选择一条要修改的结算利率信息！");
    return false;
   }
   if(!verifyInput2())
  	{
    return false;
  	}
  	if(!checkInpBox())
  	{
    return false;
  	}
   /*
   if(fm.StartDate.value!=RateGrid.getRowColData(RateGrid.getSelNo() - 1, 5) ){
   	alert("不能修改起始日期!");
   	return false;
   }
   if(fm.EndDate.value!=RateGrid.getRowColData(RateGrid.getSelNo() - 1, 6) ){
   	alert("不能修改结束日期!");
   	return false;
   }
   if(!beforeSubOrUP()){
	return false;
   }
   if(!beforeStartDate()){
       	return false;
   }
   */
    if(!checkUpdate())
  	{
    	return false;
  	}
   if(!beforeDelOrUp()){
       return false;
   }
       
  if(fm.all('Rate').value!=fm.all('DRate').value){
  	 if(!confirm("利率发生了变更，原来是"+fm.all('DRate').value+"，现在变更为"+fm.all('Rate').value+"，是否确定？")) 
  		 return false;
  }
       if (confirm("您确实想修改该记录吗?"))
       {
          var i = 0;
          var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
          var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
          showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
          fm.fmtransact.value = "UPDATE";
          fm.submit(); 
          queryClick();
//          alert("query");
			initDiv();
       }
       else
       {
         alert("您取消了修改操作！");
       }
}

function queryClick()
{
		var strSQL = "select RiskCode,Rate,RateType,RateIntv,"+
		         "ltrim(rtrim(char(year(StartDate))))||'-'||ltrim(rtrim(char(month(StartDate)))),"+
		         "PromulgateDate,case when GrpContNo = '000000' then '' else  GrpContNo end from interest000001 " +
		         "where 1=1 ";
		if(fm.BalaMonth.value != null && fm.BalaMonth.value != "" && fm.BalaMonth.value != "null")
  		{
    	strSQL = strSQL + " and (ltrim(rtrim(char(year(StartDate)))) || '-' || ltrim(rtrim(char(month(StartDate))))) = '" + getYearMonth() + "' ";
  		}
		strSQL = strSQL + getWherePart( 'RiskCode','RiskCode' )
				 	    + getWherePart( 'Rate','Rate','',1 )
				        + getWherePart( 'PromulgateDate','PromulgateDate' )
				        //+ getWherePart( 'StartDate','StartDate' )
				        //+ getWherePart( 'EndDate','EndDate')
				        + getWherePart( 'RateType','RateType' )
				        + getWherePart( 'RateIntv','RateIntv' )
				        + getWherePart( 'GrpContNo','GrpContNo' )
				        +" order by riskcode,startdate";
	   turnPage.queryModal(strSQL,RateGrid,1,1);
}

function deleteClick()
{
          if (RateGrid.getSelNo() == 0)
          {
           alert("请先选择一条要删除的结算利率信息！");
           return false;
          }
          /*
          if(fm.EndDate.value!='3000-01-01'){
          	alert("不能删除险种整个公布时间中间区间的结算利率.");
          	return false;
          }
          
          //by gzh 20101224
                
          strSQL2 = "select RiskCode,Rate,StartDate,EndDate from interest000001 where "	
		         +"1=1"			 	
				 + getWherePart( 'RiskCode','RiskCode' )
				 + getWherePart( 'Rate','Rate','',1 )
				 + getWherePart( 'StartDate','StartDate' )
				 + getWherePart( 'EndDate','EndDate');
          
          result1 = easyExecSql(strSQL2);
          
          if(result1==null)
          {
            alert("该结算利率设定不存在！");
            return false;
          }
          
          if(!beforeStartDate()){
          	return false;
          }
          */
          if(!beforeDelOrUp()){
          	return false;
          }
  if (confirm("您确实想删除该日期下的信息记录吗?"))
  {
     var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
     var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
     showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
     fm.fmtransact.value = "DELETE";
     fm.submit(); //提交
     initForm();
  }
  else
  {
    alert("您取消了删除操作！");
  }
}

function RateShow()
{
    fm.RiskCode.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 1);
    fm.DRiskCode.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 1);
    var sql = "select riskname from lmrisk  where riskcode = '"+fm.RiskCode.value+"'";
    var resultSQL = easyExecSql(sql);
    if (resultSQL==null || resultSQL ==""){
		alert("未获取到险种编码"+fm.RiskCode.value+"对应的险种名称!");
		return false;
	}
    fm.RiskCodeName.value = resultSQL[0][0];
    fm.Rate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 2);
    fm.DRate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 2);
    fm.RateConfirm.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 2);
    fm.RateType.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 3);
    fm.DRateType.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 3);
    if(fm.RateType.value=="S"){
    	fm.RateTypeName.value="单利";
    }else if(fm.RateType.value=="C"){
    	fm.RateTypeName.value="复利";
    }
    fm.RateIntv.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 4);
    fm.DRateIntv.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 4);
    if(fm.RateIntv.value=="D"){
    	fm.RateIntvName.value="日";
    }else if(fm.RateIntv.value=="M"){
    	fm.RateIntvName.value="月";
    }else if(fm.RateIntv.value=="Y"){
    	fm.RateIntvName.value="年";
    }
    fm.BalaMonth.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 5);
    fm.DBalaMonth.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 5);
//    fm.StartDate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 5);
//    fm.EndDate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 6);
//    fm.StartDate1.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 5);
//    fm.EndDate1.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 6);
    fm.PromulgateDate.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 6);
    fm.GrpContNo.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 7);
    fm.DGrpContNo.value = RateGrid.getRowColData(RateGrid.getSelNo() - 1, 7);
    
              strSQL2 = "select makedate,maketime from interest000001 where "	
		         +"1=1"			 	
				 + getWherePart( 'RiskCode','RiskCode' )
				 + getWherePart( 'Rate','Rate','',1 )
				 //+ getWherePart( 'StartDate','StartDate' )
				 //+ getWherePart( 'EndDate','EndDate');
          
          result1 = easyExecSql(strSQL2);
          fm.MakeDate.value=result1[0][0];
          fm.MakeTime.value=result1[0][1];
}

function add2Months(){
	//alert("add2Months");
	if(!isDate(fm.PromulgateDate.value)){
		alert("公布日期不是合法的日期格式!");
		return false;
	}
	//var sql="SELECT add_months(to_date('"+fm.PromulgateDate.value+"','YYYY-MM-DD'),2) FROM dual ";
	// by gzh 20101223
	var sql="SELECT add_months(date('"+fm.PromulgateDate.value+"'),2) FROM dual ";
	//fm.StartDate.value=easyExecSql(sql);
}
/*
function beforeStartDate(){
	//alert(getCurrentDate("-")+' '+fm.StartDate.value);
	if(getCurrentDate("-")>fm.StartDate.value){
	  alert("不能删除或修改起始时间比系统早的险种结算利率.");
	  return false;
	}
	return true;
}
function afterStartDate(){
	//alert(getCurrentDate("-")+' '+fm.StartDate.value);
	if(getCurrentDate("-")>fm.StartDate.value){
	  alert("不能删除或修改起始时间比系统早的险种结算利率.");
	  return false;
	}
	return true;
}
*/
//不能删除或修改起始时间比最晚结算日期早的险种结算利率
function beforeDelOrUp(){

	if(fm.GrpContNo.value==null || fm.GrpContNo.value =="" || fm.GrpContNo.value =="null"){
		var delSQL="SELECT *  FROM interest000001 WHERE RISKCODE = '"+fm.RiskCode.value+"' "
				+ "    and (ltrim(rtrim(char(year(StartDate)))) || '-' || ltrim(rtrim(char(month(StartDate))))) > '" + fm.BalaMonth.value + "' ";
  		var delReslut=easyExecSql(delSQL);
  		if(delReslut !=null){
        alert("不能删除或修改险种整个公布时间中间区间的结算利率.");
        return false;
    	}
	}
	
    /* 
	var sql = "select * from lcinsureacc where riskcode = '"+fm.RiskCode.value+"' and baladate>'"+fm.StartDate.value+"'";
	var resultSQL=easyExecSql(sql);
	if (resultSQL!=null){
		alert("不能删除或修改起始时间比最晚结算日期早的险种结算利率!");
		return false;
	}
*/
	if(!checkBalanced())
  	{
    	return false;
  	}
	return true;	
}
//保存或修改前校验
function beforeSubOrUP(){
	if(fm.EndDate.value<fm.StartDate.value)
    {
    alert("结束时间不应早于起始时间！");
    return false;
    }
	var StartDate = fm.StartDate.value.split("-");
	var EndDate = fm.EndDate.value.split("-");
	if(StartDate[2]!="01"){
		alert("结算利率的起始日期必须为某月1日！");
		fm.StartDate.focus();
		return false;
	}
	if(EndDate[2]!="01"){
		alert("结算利率的结束日期必须为某月1日！");
		fm.EndDate.focus();
		return false;
	}
	if((StartDate[1] == "12" && (EndDate[0]-StartDate[0])!=1) || (StartDate[1] != "12" && (EndDate[1]-StartDate[1])!=1)){
		alert("结束日期必须为起始日期的次月");
		return false;
	}
	if(fm.GrpContNo.value !="" && fm.GrpContNo.value !=null){
		var sql = "select 1 from lcgrppol where GrpContNo = '"+fm.GrpContNo.value+"' and riskcode in (select riskcode from lmriskapp where risktype4='4'and Riskprop='G')";
		var resultSQL=easyExecSql(sql);
		if(resultSQL==null || resultSQL =="")
    	{
      		alert("团体保单号为"+fm.GrpContNo.value+"的保单不是团体万能保单！");
      		return false;
    	}
	}
	var strSQL1="SELECT *  FROM interest000001 WHERE RISKCODE = '"+fm.RiskCode.value+"' "
				+" AND ((DATE('"+fm.StartDate.value+"') >= STARTDATE AND "
				+" DATE('"+fm.EndDate.value+"') <= ENDDATE) OR"
				+" (DATE('"+fm.StartDate.value+"') < ENDDATE AND"
				+" DATE('"+fm.EndDate.value+"') >= ENDDATE) OR"
				+" (DATE('"+fm.StartDate.value+"') <= STARTDATE AND"
				+" DATE('"+fm.EndDate.value+"') >= STARTDATE) OR"
				+" DATE('"+fm.EndDate.value+"') <= STARTDATE)";
	if(fm.GrpContNo.value =="" || fm.GrpContNo.value ==null){
		strSQL1 +=" and grpcontno = '000000'";
		resultSQL=easyExecSql(strSQL1);
		if (resultSQL!=null){
			alert("该险种结算利率设定的时间区间与已有的时间区间部分重复！不能保存。");
			return false;
		}
	}else{
		strSQL1 +=" and grpcontno = '"+fm.GrpContNo.value+"'";
		resultSQL=easyExecSql(strSQL1);
		if (resultSQL!=null){
			alert("该险种结算利率设定的时间区间与已有的时间区间部分重复！不能保存。");
			return false;
		}
	}
    return true;
}

//得到去掉无效0的年月,若原数据是2007-01,则返回2007-1
function getYearMonth()
{
  var tArray = (fm.BalaMonth.value).split("-");
  if(tArray.length != 2)
  {
    alert("请按格式录入结算月份");
    return false;
  }
  
  var tMonth = tArray[1];
  if(tMonth.length == 2 && tMonth.substring(0, 1) == "0")
  {
    tMonth = tMonth.substring(1, 2);
  }
  
  var tYearMonth = tArray[0] + "-" + tMonth;
  
  return tYearMonth;
}
//检查结算月份格式
function checkBalaMonth()
{
  var month = /^([12]\d{3})[-](([0][1-9])|([1-9])|(1[0-2]))$/
  if(!month.test(fm.BalaMonth.value))
  {
    alert("结算月份格式为 yyyy-mm 如:2007-12");
    return false;
  } 
    return true;
}
//保存时检查该条记录是否存在
function checkInsert()
{
	var sqlGrpContNo = "";
	if(fm.GrpContNo.value==null || fm.GrpContNo.value =="" || fm.GrpContNo.value =="null"){
		sqlGrpContNo = " and GrpContNo = '000000'";
	}else{
 	    sqlGrpContNo = " and GrpContNo = '"+fm.GrpContNo.value+"'";
	}
	
  var sql = " select count(*) from interest000001 where RiskCode = '" + fm.RiskCode.value + "' " 
             + "     and RateType = '" + fm.RateType.value + "' "
             + "     and RateIntv = '" + fm.RateIntv.value + "' "
             + "     and (ltrim(rtrim(char(year(StartDate)))) || '-' || ltrim(rtrim(char(month(StartDate))))) = '" + getYearMonth() + "' "
             + sqlGrpContNo;
  var arrReturn = easyExecSql(sql);
  if (arrReturn != 0)
  {
    alert("该结算利率设定已存在！不能重复保存！");
    return false;
  }
  
  sql = " select count(*) from interest000001 where RiskCode = '" + fm.RiskCode.value + "' " 
      + " and RateType = '" + fm.RateType.value + "' "
      + " and RateIntv = '" + fm.RateIntv.value + "' "
   	  + sqlGrpContNo;
  arrReturn = easyExecSql(sql);
  if(arrReturn !=0 && (fm.GrpContNo.value==null || fm.GrpContNo.value =="" || fm.GrpContNo.value =="null"))
  {
    var sql1 = " select count(*) from interest000001 where RiskCode = '" + fm.RiskCode.value + "' " 
             + "     and RateType = '" + fm.RateType.value + "' "
             + "     and RateIntv = '" + fm.RateIntv.value +"' " 
             + "     and (ltrim(rtrim(char(year(StartDate + 1 month)))) || '-' || ltrim(rtrim(char(month(StartDate + 1 month))))) = '" + getYearMonth() + "' "
             + sqlGrpContNo;
    var arrReturn1 = easyExecSql(sql1);
    if (arrReturn1 == 0)
    {
      alert("请先录入上个月结算利率！");
      return false;
    }
  }
  return true;
}
function checkBalanced()
{
  var sqlGrpContNo = "";
	if(fm.GrpContNo.value==null || fm.GrpContNo.value =="" || fm.GrpContNo.value =="null"){
		//sqlGrpContNo = " and GrpContNo = '000000'";
	}else{
 	    sqlGrpContNo = " and a.GrpContNo = '"+fm.GrpContNo.value+"'";
	}
  var sql = "select 1 from lcinsureacc a, interest000001 b "
          + "where a.BalaDate = b.EndDate "
          + " and b.RateType = '" + fm.RateType.value + "' "
          + " and b.RateIntv = '" + fm.RateIntv.value + "' "
          + " and b.riskcode = '"+fm.RiskCode.value+"'"
          + " and b.riskcode = a.riskcode "
          + " and (ltrim(rtrim(char(year(StartDate)))) || '-' || ltrim(rtrim(char(month(StartDate))))) = '" + fm.BalaMonth.value + "' "
          + sqlGrpContNo
          + " fetch first 1 rows only ";
  var rs = easyExecSql(sql);
  if(rs)
  {
    alert("该利率已结算过，不能执行修改或删除操作！");
    return false;
  }
  return true;
}
//检查记录能否修改
function checkUpdate()
{
  if(fm.RiskCode.value != fm.DRiskCode.value)
  {
    alert("险种编码不能修改！");
    return false;
  }
  if(fm.BalaMonth.value != fm.DBalaMonth.value)
  {
    alert("结算月份不能修改！");
    return false;
  }
  if(fm.RateType.value != fm.DRateType.value)
  {
    alert("利率类型不能修改！");
    return false;
  }
  if(fm.RateIntv.value != fm.DRateIntv.value)
  {
    alert("利率间隔不能修改！");
    return false;
  }
  if(fm.GrpContNo.value != fm.DGrpContNo.value)
  {
    alert("团体保单号不能修改！");
    return false;
  }
  
  if(fm.Rate.value == fm.DRate.value)
  {
    alert("请修改利率后再保存！");
    return false;
  }
  
  return true;
}
//检查利率
function checkRate()
{
  var rate1 = fm.Rate.value;
  var rate2 = fm.RateConfirm.value;
  if(rate1 != rate2)
  {
    alert("两次输入的利率不一致，请校对后重新输入！");
    return false;
  }
  else
  {
    if(rate1 < 1)
    {
      return true;
    }
    alert("输入的利率有误，请输入一个小于1的小数！");
    return false;
  }
  return true;
}

//检查各输入项是否符合要求
function checkInpBox()
{
  if(!checkBalaMonth())
  {
    return false;
  }
  if(!checkRate())
  {
    return false;
  }
  return true;
}