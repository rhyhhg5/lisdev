
<%
	//程序名称：LDPromiseRateInput.jsp
	//程序功能：保证汇率的设定
	//创建日期：2007-05-17 12:00:16
	//创建人  ：李磊
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html; charset=GBK"%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<SCRIPT src="LDPromiseRateInput.js"></SCRIPT>
		<%@include file="LDPromiseRateInit.jsp"%>
	</head>

	<%
		GlobalInput tG = new GlobalInput();
		tG = (GlobalInput) session.getValue("GI");
	%>
	<script>
  var comCode = "<%=tG.ComCode%>";
  var manageCom = "<%=tG.ManageCom%>";
  var operator = "<%=tG.Operator%>";
</script>
	<body onload="initForm(); initElementtype();">
		<form action="./LDPromiseRateSave.jsp" method=post name=fm
			target="fraSubmit">
			<%@include file="../common/jsp/OperateButton.jsp"%>
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divLDWorkTime1);">
					</td>
					<td class=titleImg>
						保证利率设定信息
					</td>
				</tr>
			</table>
			<Div id="divLDWorkTime1" style="display: ''">
				<table class=common align='center'>
					<TR class=common>
						<TD class=title8>
							险种编码
						</TD>
						<TD class=input8>
							<Input class=codeno name=RiskCode verify="险种编码|notnull&code:ULIRiskGrpBQ"
								ondblclick="return showCodeList('ULIRiskGrpBQ',[this,RiskCodeName],[0,1],null,null,null,1);"
								onkeyup="return showCodeListKey('ULIRiskGrpBQ',[this,RiskCodeName],[0,1]);"><input class=codename name=RiskCodeName readonly=true
								elementtype=nacessary>
						</TD>
						<TD class=title>
							公布日期
						</TD>
						<TD class=input>
							<Input class='coolDatePicker' dateFormat="Short" 
								style="width:152px" name=PromulgateDate verify="公布日期|notnull&DATE"
								elementtype=nacessary>
						</TD>
						<TD class=title>
							起始日期
						</TD>
						<TD class=input>
							<Input class='coolDatePicker' dateFormat="Short"
								style="width:152px" name=StartDate verify="起始日期|notnull&DATE"
								elementtype=nacessary>
						</TD>
					</TR>
					<TR>
						<TD class=title>
							结束日期
						</TD>
						<TD class=input>
							<Input class='coolDatePicker' dateFormat="Short" 
								style="width:152px" name=EndDate verify="结束日期|notnull&DATE"
								elementtype=nacessary>
						</TD>
						<TD class=title>
							利率间隔
						</TD>
						<TD class=input>
							<Input class=codeno CodeData="0|^D|日^M|月^Y|年" name=RateIntv
								verify="利率间隔|notnull"
								ondblclick="return showCodeListEx('RateIntv',[this,RateIntvName],[0,1]);"
								onkeyup="return showCodeListKeyEx('RateIntv',[this,RateIntvName],[0,1]);"><input class=codename name=RateIntvName readOnly=true
								elementtype=nacessary>
						</TD>
						<TD class=title>
							利率类型
						</TD>
						<TD  class= input>
						<Input class=codeno  CodeData="0|^S|单利^C|复利" name=RateType verify="利率类型|notnull" ondblclick="return showCodeListEx('RateType',[this,RateTypeName],[0,1]);" onkeyup="return showCodeListKeyEx('RateType',[this,RateTypeName],[0,1]);" readonly=true><input class=codename  elementtype=nacessary name=RateTypeName readOnly=true >
						</TD>
						</TR>
						<TR class=common>
						<TD class=title>
							利率
						</TD>
						<TD class=input>
							<Input class='common' name=Rate elementtype=nacessary
								verify="利率|notnull&num">
							<Input type=hidden name=hiddenRate >
						</TD>
						<TD class=title>
							确认利率
						</TD>
						<TD class=input>
							<Input class='common' name=DRate elementtype=nacessary
								verify="确认利率|notnull&num">
						</TD>
					</TR>
				</table>
			</Div>
			<INPUT VALUE="重 置" class=cssButton TYPE=button onclick="initForm();">
			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"
							OnClick="showPage(this,divRateGrid);">
					</td>
					<td class=titleImg>
						保证利率列表：
					</td>
				</tr>
			</table>
			<Div id="divRateGrid" style="display: ''" align=center>
				<table class=common>
					<tr class=common>
						<td text-align: left colSpan=1>
							<span id="spanRateGrid"> </span>
						</td>
					</tr>
				</table>
				<INPUT VALUE="首  页" class=cssButton TYPE=button
					onclick="getFirstPage();">
				<INPUT VALUE="上一页" class=cssButton TYPE=button
					onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class=cssButton TYPE=button
					onclick="getNextPage();">
				<INPUT VALUE="尾  页" class=cssButton TYPE=button
					onclick="getLastPage();">
			</div>



			<input type=hidden id="fmtransact" name="fmtransact">
			<input type=hidden id="fmAction" name="fmAction">
			<input type=hidden name="MakeDate">
			<input type=hidden name="MakeTime">
			<input type=hidden name="ModifyDate">
			<input type=hidden name="ModifyTime">
			<input type=hidden name="operator">
			<input type=hidden name="RiskCode1">
			<input type=hidden name="Rate1">
			<input type=hidden name="StartDate1">
			<input type=hidden name="EndDate1">
		</form>
		<span id="spanCode"
			style="display: none; position:absolute; slategray"></span>
	</body>
</html>


