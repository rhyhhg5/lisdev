<%@page contentType="text/html;charset=GBK" %>

<%
//程序名称：OtoFQuery.jsp
//程序功能：根据批次号在日志表中进行查询，查询出文件的路径和文件的名称
//创建人  ：刘岩松
//创建日期：2003-01-08
//更新记录：
//更新人
//更新日期
//更新原因/内容
%>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.otof.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
 <%@page import="org.w3c.dom.*"%>
<%@page import="javax.xml.parsers.*"%>
<%@page import="org.apache.xml.serialize.*"%>
<%@page  import="org.xml.sax.InputSource" %>
<%@page import="java.net.*"%>
<%@page import = "java.io.*"%>

<%
  //输出参数
  String Content = "";
  CErrors tError = null;
  String FlagStr = "Fail";
  String tFilePath;
  String tFileName;
  String tFileLocal;

  LITranLogSchema tLITranLogSchema = new LITranLogSchema();
  LITranLogSchema mLITranLogSchema = new LITranLogSchema();
  tLITranLogSchema.setBatchNo(request.getParameter("BatchNo"));
  System.out.println("批次号为"+tLITranLogSchema.getBatchNo());

  // 准备传输数据 VData，将数据通过vData进行传递
  VData tVData = new VData();
	tVData.addElement(tLITranLogSchema);
//定义ui
  OtoFQueryUI tOtoFQueryUI = new OtoFQueryUI();

	if (!tOtoFQueryUI.submitData(tVData,"QUERY||MAIN"))
	{
      Content = " 查询失败，原因是: " + tOtoFQueryUI.mErrors.getError(0).errorMessage;
      FlagStr = "Fail";
	}
	else
	{
		tVData.clear();
		tVData = tOtoFQueryUI.getResult();
    mLITranLogSchema = (LITranLogSchema)tVData.getObjectByObjectName("LITranLogSchema",0);
    tFilePath = mLITranLogSchema.getFilePath();
    tFileName = mLITranLogSchema.getFileName();
    System.out.println("文件路径为"+tFilePath);
    System.out.println("文件名称为"+tFileName);
    tFileLocal = tFilePath+""+tFileName+".xml";
    System.out.println("该文件的定位是"+tFileLocal);

    String str=tFileLocal;

    FileInputStream fi = new FileInputStream(str);
    int ch=0;
    String url = "http://10.0.0.10:8080/ui/ufin/addVoucherReturnXML.jsp";
    URL jspUrl=new URL(url);
    URLConnection uc= jspUrl.openConnection();
    uc.setDoOutput(true);
    OutputStream os=uc.getOutputStream();

    while ((ch=fi.read())!=-1)
    {
      os.write(ch);
    }
    os.flush();
    os.close();
    uc.getContent().toString();
    System.out.println("os close");


		String tFlag = "0";
	}

  //如果在Catch中发现异常，则不从错误类中提取错误信息
  if (FlagStr == "Fail")
  {
    tError = tOtoFQueryUI.mErrors;
    if (!tError.needDealError())
    {
    	Content = " 查询成功! ";
    	FlagStr = "Succ";
    }
    else
    {
    	Content = " 查询失败，原因是:" + tError.getFirstError();
    	FlagStr = "Fail";
    }
  }
System.out.println("------end------");
System.out.println(FlagStr);
System.out.println(Content);
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>

