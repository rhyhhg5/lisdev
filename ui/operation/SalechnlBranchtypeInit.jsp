<%@page import="java.util.*"%>
<%@page contentType="text/html;charset=GBK"%>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>

<script type="text/javascript">

function initForm(){
  try{
	  //初始化表格
	  initSaleChannelId();
	  //初始化数据
	  easyQueryClick();
  }
  catch(ex){
    alert("在QyModifyTakeBackInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
	/*初始化表  */
	function initSaleChannelId() {
		var iArray = new Array();

		iArray[0] = new Array();
		iArray[0][0] = "序号"; //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[0][1] = "30px"; //列宽
		iArray[0][2] = 15; //列最大值
		iArray[0][3] = 0; //是否允许输入,1表示允许，0表示不允许

		iArray[1] = new Array();
		iArray[1][0] = "销售渠道"; //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[1][1] = "200px"; //列宽
		iArray[1][2] = 150; //列最大值
		iArray[1][3] = 0; //是否允许输入,1表示允许，0表示不允许

		iArray[2] = new Array();
		iArray[2][0] = "展业类型"; //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[2][1] = "200px"; //列宽
		iArray[2][2] = 150; //列最大值
		iArray[2][3] = 0; //是否允许输入,1表示允许，0表示不允许

		iArray[3] = new Array();
		iArray[3][0] = "渠道类型"; //列名（此列为顺序号，列名无意义，而且不显示）
		iArray[3][1] = "200px"; //列宽
		iArray[3][2] = 150; //列最大值
		iArray[3][3] =0; //是否允许输入,1表示允许，0表示不允许

		SaleChannelId = new MulLineEnter("fm", "SaleChannelId");

		//这些属性必须在loadMulLine前
		SaleChannelId.canChk = 0              //显示复选框
		SaleChannelId.mulLineCount = 12;      //表格的行数
		SaleChannelId.displayTitle = 1;       //标题,显示1，不显示0
		SaleChannelId.locked = 1;             //表记录上锁，上锁1，不上锁0
		SaleChannelId.hiddenPlus = 1;         //加号 ,显示0，不显示1
 		SaleChannelId.canChk = 0;             //复选框 , 显示1，不显示0
		SaleChannelId.canSel = 1;             //单选框 ,显示1，不显示0
		SaleChannelId.hiddenSubtraction = 1;  //减号, 显示0，不显示1
		SaleChannelId.selBoxEventFuncName = "returnTableField"  ;//单击单选按钮激发一个函数
		SaleChannelId.loadMulLine(iArray);    //导入mulline中
	}

</script>


