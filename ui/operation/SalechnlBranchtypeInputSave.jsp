<%@page import="com.sinosoft.lis.certify.SysOperatorNoticeBL"%>
<%
	//程序名称：ReProposalPrintInput.jsp
	//程序功能：
	//创建日期：2002-11-25
	//创建人  ：Kevin
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.tb.*"%>
<%@page contentType="text/html;charset=GBK"%>

<%@page import=" com.sinosoft.lis.operation.SalechnlBranchtypeUI"%>

<%
	//==============================删除=======================================
	//输出参数
	String FlagStr = "";
	String Content = "";
	
	//设置字符编码
	request.setCharacterEncoding("gbk"); 
	
	//从页面取值
	String operatortype = request.getParameter("operatortype");
	String code = request.getParameter("code");
	String code1 = request.getParameter("code1");
	String codeName = request.getParameter("codeName");
    Content = request.getParameter("Content");
    
	//全局信息
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");

	//容器V
	VData tVData = new VData();

	//容器T
	TransferData transferData = new TransferData();
	transferData.setNameAndValue("code", code);
	transferData.setNameAndValue("code1", code1);
	transferData.setNameAndValue("codeName", codeName);
	transferData.setNameAndValue("operatortype", operatortype);

	//==============================删除=======================================
			
	//==============================修改=======================================
    //取值
	String SalesChannelsValue = request.getParameter("SalesChannels");
	String AcquisitionTypesValue = request.getParameter("AcquisitionTypes");
	String ChannelsTypeValue = request.getParameter("ChannelsType");
	
	//封装
	transferData.setNameAndValue("SalesChannelsValue",SalesChannelsValue);
	transferData.setNameAndValue("AcquisitionTypesValue",AcquisitionTypesValue);
	transferData.setNameAndValue("ChannelsTypeValue", ChannelsTypeValue);
	//==============================修改========================================
			
	//==============================增加========================================
	/**
	 *1.获取要增加的内容--复用修改
	 *2.传输到后台进行处理
	 */
	//==============================增加=========================================
	
	//==============================公共部分=====================================
	//数据装进容器V
	tVData.add(tG);
	tVData.add(transferData);

	//准备SalechnlBranchtypeUI的实例对象
	SalechnlBranchtypeUI salechnlBranchtypeUI = new SalechnlBranchtypeUI();
	try {
		boolean isSucccessed = salechnlBranchtypeUI.submitData(tVData,operatortype);
		System.out.println(isSucccessed);
		if (!isSucccessed) {
			FlagStr = "Succ";
		} else {
			FlagStr = "Fail";
		}
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//==============================公共部分=====================================

%>
<html>
<script language="javascript">
 parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>

</html>