<%
//程序名称：AllProposalQuerySubmit.jsp
//程序功能：查询测试
//创建日期：2004-8-13 13:59
//创建人  ：Minim
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.webservice.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
    //准备输入后台的数据  
    System.out.println("------Query Begin-----");
    
    CErrors tError = null;            
    String FlagStr = "";
    String Content = "";
    VData Result = null;
    
    String sql = "select PolNo,MainPolNo,PrtNo,RiskCode,AppntName,InsuredName,GrpPolNo from LCPol"
               + " where 1=1 and appflag = 1 "
			   + " and riskcode in (select riskcode from lmriskapp where subriskflag='M')"
			   + ReportPubFun.getWherePart("PrtNo", request.getParameter("PrtNo"))
			   + ReportPubFun.getWherePart("PolNo", request.getParameter("PolNo"));
	System.out.println("WSClient:" + sql);

    String strResult = "";
    WebServiceClient tWebServiceClient = new WebServiceClient();  
    
    try
    {
        VData tVData = new VData();
        tVData.addElement(sql);
    
        if(tWebServiceClient.submitData(tVData, ""))
        {
            Result = tWebServiceClient.getResult();
            strResult = (String) Result.get(0);
            
            FlagStr = "Success";
            Content = "查询成功！";
        }
        else 
        {
            tError = tWebServiceClient.mErrors;
            
            FlagStr = "Fail";
            Content = "查询失败，原因是:" + tError.getFirstError();
        }
        
    }
    catch(Exception ex)
    {
        Content = "查询失败，原因是:" + ex.toString();
        FlagStr = "Fail";
    }			
%>    
                  
<html>
<script language="javascript">
  parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=strResult%>");
</script>
</html>