<%
//程序名称：ProposalCopyInput.jsp
//程序功能：
//创建日期：2002-08-21 09:25:18
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK" %>

<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.brieftelesales.*"%>
<%@page import="java.io.*"%>
<%@page import="java.util.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%
  	//接收信息，并作校验处理。
  	//输入参数
  	// Variables
System.out.println("------开始执行电销导入BriefTSImportSave.jsp------");
/** 文件上传成功标志。 */
boolean tSucFlag = false;
String FlagStr = "Fail";
String Content = "";
String mFileName = null;       
String ImportPath = request.getParameter("ImportPath");
String tMissionId = request.getParameter("MissionId");
String tSubMissionId = request.getParameter("SubMissionId");
String tActivityId = request.getParameter("ActivityId");
String tBatchNo = request.getParameter("BatchNo");
System.out.println("...tBatchNo:" + tBatchNo );
System.out.println("...ImportPath:" + ImportPath );
String path = application.getRealPath("").replace('\\','/')+'/';	
ImportPath= path + ImportPath  ;
	
System.out.println("...pathL:" + path );
System.out.println("...开始上载文件");		
System.out.println("ImportPath .." + ImportPath );
// Initialization
DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
fu.setSizeMax(10000000);
// maximum size that will be stored in memory?
// 设置最多只允许在内存中存储的数据,单位:字节
fu.setSizeThreshold(4096);
// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(path+"temp");
//开始读取上传信息
System.out.println(path);
List fileItems = null;
try{
 fileItems = fu.parseRequest(request);
 tSucFlag = true;
}
catch(Exception ex)
{
	tSucFlag = false;
	ex.printStackTrace();
}
// 依次处理每个上传的文件
Iterator iter = fileItems.iterator();

while (iter.hasNext()) {
  FileItem item = (FileItem) iter.next();
  //忽略其他不是文件域的所有表单信息
  if (!item.isFormField()) {
    String name = item.getName();
    System.out.println(name);
    long size = item.getSize();
    if((name==null||name.equals("")) && size==0)
      continue;
    //ImportPath= path + ImportPath;
    mFileName = name.substring(name.lastIndexOf("\\") + 1);
    System.out.println("-----------importpath."+path + mFileName);
    //保存上传的文件到指定的目录
    try {
      	item.write(new File(ImportPath + mFileName));
      	tSucFlag = true;
    } catch(Exception e) {
    	tSucFlag = false;
    	e.printStackTrace();
      	System.out.println("upload file error ...");
    }
  }
}
	
	if(tSucFlag){
		try
	    {
	        String tImportBatchNo = mFileName.substring(0, mFileName.lastIndexOf("."));
	        System.out.println("导入文件名称批次号："+tImportBatchNo);
	        if(tBatchNo != null && tBatchNo.equals(tImportBatchNo))
	        {
				            
	            TSParseGuideInUI tParseGuideInUI   = new TSParseGuideInUI();
	            TransferData tTransferData = new TransferData(); 
	            GlobalInput tG = new GlobalInput();
			  	tG=(GlobalInput)session.getValue("GI");
			
			  	// 准备传输数据 VData
			  	VData tVData = new VData();
			 	FlagStr="";
			  	tTransferData.setNameAndValue("FileName",mFileName);	
			  	tTransferData.setNameAndValue("FilePath", path);
			  	tTransferData.setNameAndValue("MissionId", tMissionId);
			  	tTransferData.setNameAndValue("SubMissionId", tSubMissionId);
			  	tTransferData.setNameAndValue("ActivityId", tActivityId);
			  	tVData.add(tTransferData);
			  	tVData.add(tG);
	            if (!tParseGuideInUI.submitData(tVData,""))
	            {
	                Content = " 导入清单失败! 原因详见“导入批次日志”！";
	                FlagStr = "Fail";
	            }
	            else
	            {
	                Content = " 导入清单成功！";
	                FlagStr = "Succ";
	            }
	        }
	        else
	        {
	            Content = " 批次号与上传文件名不符，请确认后重新上传！";
	            FlagStr = "Fail";
	        }
	    }
	    catch (Exception e)
	    {
	        Content = " 获取批次信息失败。";
	        FlagStr = "Fail";
	        e.printStackTrace();
	    }
	}
  System.out.println("----mFileName:" + mFileName);
    
   System.out.println("------执行电销导入BriefTSImportSave.jsp完成------");

%>                      
<html>
<script language="javascript">
parent.fraInterface.afterImportBatchList("<%=FlagStr%>","<%=Content%>", "<%=tBatchNo%>");
</script>
</html>

