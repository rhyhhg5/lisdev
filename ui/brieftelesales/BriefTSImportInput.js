//程序名称：
//程序功能：
//创建日期：2008-11-11
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();

var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;


/**
 * 提交表单
 */
function submitForm()
{
    var i = 0;
    var showStr = "正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
    fm.submit(); //提交
}

/**
 * 初始化批次号。
 */
function initBatchNoInput(cBatchNo)
{    
    if(cBatchNo != null && cBatchNo != "" && cBatchNo != "null")
    {
        fmImport.BatchNo.value = cBatchNo;
    }
    else
    {
        fmImport.BatchNo.value = "";
    }
}

/**
 * 导入清单。
 */
function importBatchList()
{
	var i = 0;
	getImportPath();
	var tBatchNo = fmImport.BatchNo.value;
	ImportFile = fmImport.all('FileName').value;
	if ( ImportFile == null || ImportFile == "")
	{
	    alert("请选择上传数据文件!");
	    return;
	}
	else
	{
		var a= ImportFile.lastIndexOf("\\");
		var b= ImportFile.lastIndexOf(".");
		var importBatchNo = ImportFile.substring(a+1,b);
		 
		var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
		fmImport.action = "./BriefTSImportSave.jsp?ImportPath=" + ImportPath+"&BatchNo="+tBatchNo;
	    fmImport.submit();
	    fmImport.action = "";
	}
}

/**
 * 导入清单提交后动作。
 */
function afterImportBatchList(FlagStr, Content, cBatchNo)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        //alert("导入批次失败，请尝试重新进行申请。");
        fm.ImportBatchNo.value = cBatchNo;
        queryImportLog();
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        fm.ImportBatchNo.value = cBatchNo;
        queryImportLog();
    }
    
    fmImport.btnImport.disabled = false;
}

function queryImportLog()
{
    var tBatchNo = fmImport.BatchNo.value;
    if(tBatchNo =="" || tBatchNo == null){
    	alert("批次号不能为空！");
    	return false;
    }
    var tStrSql = "select batchno,prtno,errorinfo,varchar(MakeDate) || ' '|| MakeTime  "
                + " from lcgrpimportlog where batchno = '"+tBatchNo+"' "
                + " and contid not in ('C','X') ";
//prompt('',tStrSql);
    turnPage2.pageDivName = "divImportLogGridPage";
    turnPage2.queryModal(tStrSql, ImportLogGrid);

    if (!turnPage2.strQueryResult)
    {
        alert("没有该批次导入日志信息！");
        return false;
    }
    fmImport.querySql.value = tStrSql;
    return true;
}

//下载事件
function downloadList()
{
    if(fmImport.querySql.value != null && fmImport.querySql.value != "" && ImportLogGrid.mulLineCount > 0)
    {
        var formAction = fmImport.action;
        fmImport.action = "BriefTSDownload.jsp";
        //fm.target = "_blank";
        fmImport.submit();
        fmImport.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再下载！");
        return ;
    }
}

//获取上传路径
function getImportPath ()
{
  // 书写SQL语句
  var strSQL = "";
  strSQL = "select SysvarValue from ldsysvar where sysvar = 'XmlPath' ";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult)
  {
    alert("未找到上传路径");
    return;
  }

  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
  ImportPath = turnPage.arrDataCacheSet[0][0];
}

