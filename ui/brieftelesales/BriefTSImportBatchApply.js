//程序名称：
//程序功能：
//创建日期：2008-12-12
//创建人  ：LY
//更新记录:  更新人   更新日期    更新原因/内容 

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();

var mDebug = "0";
var mOperate = "";
var showInfo;
window.onfocus = myonfocus;

/**
 * 查询已申请结算单。
 */
function queryBriefTSImportBatchList()
{
    if(!verifyInput2())
    {
        return false;
    }

    var tStrSql = ""
        + " select lwm.MissionProp1, lwm.MissionProp2, lwm.MissionProp3, lwm.MissionProp4, "
        + " lwm.MissionId, lwm.SubMissionId, lwm.ProcessId, lwm.ActivityId, lwm.ActivityStatus "
        + " from LWMission lwm "
        + " where 1 = 1 "
        + " and lwm.ProcessId = '0000000007' "
        + " and lwm.ActivityId = '0000007100' "
        + getWherePart("lwm.MissionProp1", "BatchNo")
        + getWherePart("lwm.MissionProp2", "ApplyDate")
        + getWherePart("lwm.MissionProp3", "ManageCom",'like')
        + " with ur "
        ;
    
    turnPage1.pageDivName = "divImportBatchListGridPage";
    turnPage1.queryModal(tStrSql, ImportBatchListGrid);
    
    if (!turnPage1.strQueryResult)
    {
        alert("没有待处理的批次信息！");
        return false;
    }
    
    return true;
}

/**
 * 进入卡折录入。
 */
function importBatch()
{
    var tRow = ImportBatchListGrid.getSelNo() - 1;
    if(tRow == null || tRow < 0)
    {
        alert("请选择一条记录。");
        return false;
    }
    
    var tRowDatas = ImportBatchListGrid.getRowData(tRow);
    
    var tBatch = tRowDatas[0];
    var tMissionId = tRowDatas[4];
    var tSubMissionId = tRowDatas[5];
    var tProcessId = tRowDatas[6];
    var tActivityId = tRowDatas[7];
    var tActivityStatus = tRowDatas[8];
    
    var tStrUrl = "./BriefTSImportInput.jsp"
        + "?BatchNo=" + tBatch
        + "&MissionId=" + tMissionId
        + "&SubMissionId=" + tSubMissionId
        + "&ProcessId=" + tProcessId
        + "&ActivityId=" + tActivityId
        + "&ActivityStatus=" + tActivityStatus
        ;

    window.location = tStrUrl;
}

/**
 * 申请导入批次号。
 */
function applyImportBatch()
{
    fm.btnBatchNoApply.disabled = true;
    
    var showStr = "正在提交申请，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
    showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
    
    fm.action = "./BriefTSImportBatchApplySave.jsp";
    fm.submit();

    fm.action = "";
}

/**
 * 申请导入批次号提交后动作。
 */
function afterApplyImportBatchSubmit(FlagStr, Content, cBatchNo)
{
    showInfo.close();
    window.focus();
    
    if (FlagStr == "Fail" )
    {             
        var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        alert("申请批次失败，请尝试重新进行申请。");
    }
    else
    { 
        var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + Content ;  
        showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
        
        fm.BatchNo.value = cBatchNo;
        queryBriefTSImportBatchList();
    }

    fm.btnBatchNoApply.disabled = false;
}

