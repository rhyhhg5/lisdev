<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
//程序名称：
//程序功能：
//创建日期：2012-02-08
//创建人  ：LY
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.cbcheck.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.workflow.brieftb.BriefTbWorkFlowUI"%>


<%
System.out.println("BriefTSImportBatchApplySave.jsp start ...");

CErrors tError = null;
String FlagStr = "Fail";
String Content = "";

GlobalInput tGI = (GlobalInput)session.getValue("GI");
String strManageCom = tGI.ComCode;

String tBatchNo = null;

try
{
    int length = 8;
    if (strManageCom.length() < length)
    {
        strManageCom = PubFun.RCh(strManageCom, "0", length);
    }
    
    try
    {
        tBatchNo = PubFun1.CreateMaxNo("TSBATCHNO", strManageCom, null);
        System.out.println("电销BatchNo:" + tBatchNo);
    }
    catch(Exception e)
    {
        tBatchNo = null;
        e.printStackTrace();
    }
    
    if(tBatchNo != null && !tBatchNo.equals(""))
    {
        VData tVData = new VData();
        
        TransferData tTransferData = new TransferData();
        tTransferData.setNameAndValue("BatchNo", tBatchNo);
        tTransferData.setNameAndValue("ManageCom", request.getParameter("ManageCom"));
        tTransferData.setNameAndValue("InputDate", PubFun.getCurrentDate());
        tTransferData.setNameAndValue("Operator", tGI.Operator);
        
        tVData.add(tTransferData);
        tVData.add(tGI);
        
        System.out.println("开始进行电销批次号码申请");
        String tActivityID = "9999997100";//默认为电销开始节点标识
		BriefTbWorkFlowUI tBriefTbWorkFlowUI = new BriefTbWorkFlowUI();
		if (tBriefTbWorkFlowUI.submitData(tVData,tActivityID) == false)
		{
		  int n = tBriefTbWorkFlowUI.mErrors.getErrorCount();
		  System.out.println("n=="+n);
		  for (int j = 0; j < n; j++)
		    System.out.println("Error: "+tBriefTbWorkFlowUI.mErrors.getError(j).errorMessage);
		  Content = " 投保单申请失败，原因是: " + tBriefTbWorkFlowUI.mErrors.getError(0).errorMessage;
		  FlagStr = "Fail";
		}
		else       
        {
            
            Content = " 申请成功！";
            FlagStr = "Succ";
        }
    }
    else
    {
        Content = " 申请批次号失败！";
        FlagStr = "Fail";
    }
}
catch (Exception e)
{
    Content = " 申请失败。";
    FlagStr = "Fail";
    e.printStackTrace();
}

System.out.println("BriefTSImportBatchApplySave.jsp end ...");

%>

<html>
<script language="javascript">
    parent.fraInterface.afterApplyImportBatchSubmit("<%=FlagStr%>", "<%=Content%>", "<%=tBatchNo%>");
</script>
</html>
