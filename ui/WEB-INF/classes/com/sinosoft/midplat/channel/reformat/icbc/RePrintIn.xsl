<?xml version='1.0' encoding='GBK'?>
<!-- 保单重打 -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:java="http://xml.apache.org/xslt/java"
	exclude-result-prefixes="java">

<xsl:output indent='yes'/>
<xsl:template match="/">
<TranData> 
	<xsl:apply-templates select="TXLife/TXLifeRequest"/>
	<LCCont>
		<ContNo><xsl:value-of select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/PolNumber"/></ContNo>
		<PrtNo><xsl:value-of select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/ApplicationInfo/HOAppFormNumber"/></PrtNo>
    <ProposalContNo><xsl:value-of select="TXLife/TXLifeRequest/OLifE/FormInstance[FormName='2']/ProviderFormNumber"/></ProposalContNo>
		<!-- 工行不打印发票，所以也没有重打发票的功能。 -->
		<!-- 
    <TempFeeNo><xsl:value-of select="TXLife/TXLifeRequest/OLifE/FormInstance[FormName='1']/ProviderFormNumber"/></TempFeeNo>
		-->
		<TempFeeNo>0000000000</TempFeeNo>

		<!-- 工行的银保通接口中，不涉及保单密码。 -->
		<!--
    <Password><xsl:value-of select="/TXLife/TXLifeRequest/OLifE/Holding/OLifEExtension/Password"/></Password>
		-->
		<Password><xsl:value-of select="/TXLife/TXLifeRequest/OLifE/Holding/Policy/OLifEExtension/Password"/></Password>
	</LCCont>
</TranData>
</xsl:template>

<xsl:template name="BaseInfo" match="TXLife/TXLifeRequest">
	<BaseInfo>
		<BankDate><xsl:value-of select="TransExeDate"/></BankDate>
		<!-- 因为是工行的报文，直接将银行编码写死成001 -->
		<BankCode>01</BankCode>
		<ZoneNo><xsl:value-of select="OLifEExtension/RegionCode"/></ZoneNo>
		<BrNo><xsl:value-of select="OLifEExtension/Branch"/></BrNo>
		<TellerNo><xsl:value-of select="OLifEExtension/Teller"/></TellerNo>
		<TransrNo><xsl:value-of select="TransRefGUID"/></TransrNo>
		<FunctionFlag>02</FunctionFlag>
		<InsuID><xsl:value-of select="OLifEExtension/CarrierCode"/></InsuID>			
  </BaseInfo>
</xsl:template>

</xsl:stylesheet>
