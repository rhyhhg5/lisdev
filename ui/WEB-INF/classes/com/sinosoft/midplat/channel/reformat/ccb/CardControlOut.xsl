<?xml version="1.0" encoding="GBK"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:java="http://xml.apache.org/xslt/java" exclude-result-prefixes="java">
	<xsl:output method="xml" indent="yes"/>
	<xsl:template match="/">
		<Transaction>
			<!--报文体-->
			<Transaction_Body>
				<BkOldSeq>
					<xsl:value-of select="/TranData/LCCont/OldTransNo"/>
				</BkOldSeq>
				<!--原交易银行流水号-->
			</Transaction_Body>
		</Transaction>
	</xsl:template>
</xsl:stylesheet>
