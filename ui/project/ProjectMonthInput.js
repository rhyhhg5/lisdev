
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var turnPage2 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function cancel(){
	top.close();
}
function beforeSubmit() {
	if (!verifyInput2()) {
		return false;
	}
	for(var li=0;li<ProjectMGrid.mulLineCount;li++){
		if(ProjectMGrid.getRowColData(li,2) == null || ProjectMGrid.getRowColData(li,2)=="")
        {
           alert("第'"+(li+1)+"'行，项目年度不可为空！");
           return false;
        }
        if(ProjectMGrid.getRowColData(li,3) == null || ProjectMGrid.getRowColData(li,3)=="")
        {
           alert("第'"+(li+1)+"'行，费用年度不可为空！");
           return false;
        }
        if(ProjectMGrid.getRowColData(li,4) == null || ProjectMGrid.getRowColData(li,4)=="")
        {
           alert("第'"+(li+1)+"'行，费用月度不可为空！");
           return false;
        }
        if(ProjectMGrid.getRowColData(li,6) == null || ProjectMGrid.getRowColData(li,6)=="")
        {
           alert("第'"+(li+1)+"'行，费用类型不可为空！");
           return false;
        }
        if(ProjectMGrid.getRowColData(li,7) == null || ProjectMGrid.getRowColData(li,7)=="")
        {
           alert("第'"+(li+1)+"'行，费用值不可为空！");
           return false;
        }
        if(!isNumeric(ProjectMGrid.getRowColData(li,7))){
		   alert("第'"+(li+1)+"'行，费用值必须为数字！");
           return false;
		}
		var tStrs = new Array();
		var tCost = parseFloat(ProjectMGrid.getRowColData(li,6))+"";  
		tStrs = tCost.split(".");
		if(tStrs.length>1){
			if(tStrs[1].length>2){
				alert("费用值最多保留两位小数！");
				fm.AveragePrem.focus();
				return false;
			}
		}
	}
	return true;
}
function deleteClick() {
	var mSelNo = ProjectMonthGrid.getSelNo();
	if (mSelNo == 0) {
		alert("请先选择需要删除的月度费用！");
		return false;
	}
	if(!checkState()){
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "DELETE||MAIN";
	fm.submit();
	//initForm();
}
function submitForm() {
	if (!beforeSubmit()) {
		return false;
	}
	if(!checkState()){
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	var mSelNo = ProjectMonthGrid.getSelNo();
	fm.fmtransact.value = "INSERT||MAIN";
	fm.submit();
}
function updateForm() {
	var mSelNo = ProjectMonthGrid.getSelNo();
	if (mSelNo == null || mSelNo == 0) {
		alert("请先选择需要修改的月度费用！");
		return false;
	}
	if (!beforeSubmit()) {
		return false;
	}
	if(!checkState()){
		return false;
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	var mSelNo = ProjectMonthGrid.getSelNo();
	fm.fmtransact.value = "UPDATE||MAIN";
	fm.submit();
}
function afterSubmit(FlagStr, content, cOperate) {
	if (cOperate == "INSERT||MAIN") {
		if (FlagStr != "Fail") {
			queryClick();
			content = "\u4fdd\u5b58\u6210\u529f";
		}
	}
	if (cOperate == "DELETE||MAIN") {
		if (FlagStr != "Fail") {
			queryClick();
			content = "\u5220\u9664\u6210\u529f";
		}
	}
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		initForm();
	}
}
function initProjectInfo(){
	if (tProjectNo == null || tProjectNo == "" || tProjectNo == "null") {
		alert("获取项目编码失败！");
		return false;
	}
	var mSQL = "select ProjectNo,ProjectName,CreateDate from LCProjectInfo where ProjectNo='" + tProjectNo + "'";
	var strQueryResult = easyExecSql(mSQL);
	if (!strQueryResult) {
		alert("获取项目信息失败！");
		return false;
	}
	fm.all("ProjectNo").value = strQueryResult[0][0];
	fm.all("ProjectName").value = strQueryResult[0][1];
	fm.all("CreateDate").value = strQueryResult[0][2];
}
function initProjectMonthInfo(){
	var wheSQL = "";
	if("1" == tLookFlag){//综合查询功能，需显示选中年的月度费用
		wheSQL = " and ProjectYear = '"+tProjectYear+"' ";
	}
	var tSQL = "select MonthFeeNo,ProjectYear,PremYear,ProjectMonth,ProjectType,"
			 + "(select codename from LDCode where CodeType = 'projecttype2' and code = ProjectType), "
			 + " Cost,state,"
			 + "(select codename from LDCode where CodeType = 'projectmonthstate' and code = state) "
			 + " from LCProjectMonthInfo where ProjectNo='" + tProjectNo + "' "
			 + wheSQL
			 + " order by int(ProjectYear),int(PremYear),int(ProjectMonth),ProjectType";
	turnPage2.queryModal(tSQL, ProjectMonthGrid);
}
function getQueryResult() {
	var mSelNo = ProjectMonthGrid.getSelNo();
	fm.MonthFeeNo.value = ProjectMonthGrid.getRowColDataByName(mSelNo - 1, "MonthFeeNo");
	fm.State.value = ProjectMonthGrid.getRowColDataByName(mSelNo - 1, "State");
	if(fm.MonthFeeNo.value ==null || fm.MonthFeeNo.value == ""){
		alert("获取月度费用编码失败");
		return;
	}
	if(fm.State.value ==null || fm.State.value == ""){
		alert("获取月度费用状态失败");
		return;
	}
	initProjectMGrid();
	ProjectMGrid.hiddenPlus =1;
    ProjectMGrid.hiddenSubtraction = 1;
	var mSQL = " select lpm.MonthFeeNo,lpm.ProjectYear,lpm.PremYear,lpm.ProjectMonth,lpm.ProjectType,"
			 + " (select codename from ldcode where codetype = 'projecttype2' and code = lpm.ProjectType),lpm.cost "
			 + " from LCProjectMonthInfo lpm where lpm.MonthFeeNo='" + fm.MonthFeeNo.value + "'";
	turnPage1.queryModal(mSQL, ProjectMGrid);
}
function sendUW(){
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action="./ProjectMonthInputSendUWSave.jsp";
	fm.submit(); //提交
	fm.action="";
}
function checkState(){
	if(fm.State.value == '02'){
		alert("月度费用信息在审核中，不允许操作！");
		return false;
	}
	if(fm.State.value == '03'){
		alert("月度费用信息已审核通过，不允许操作！");
		return false;
	}
	if(fm.State.value == '05'){
		alert("月度费用信息已失效，不允许操作！");
		return false;
	}
	return true;
}
function queryUWYJ(){
	var checkFlag = 0;
	for (i=0; i<ProjectMonthGrid.mulLineCount; i++)
	{
	  if (ProjectMonthGrid.getSelNo(i))
	  {
	    checkFlag = ProjectMonthGrid.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tMonthfeeNo = ProjectMonthGrid.getRowColData(checkFlag - 1, 1);
		if(tMonthfeeNo == null || tMonthfeeNo == "" || tMonthfeeNo =="null"){
			alert("获取审核信息失败！");
			return false;
		}
		showInfo = window.open("./ProjectQueryPremUWInput.jsp?ProjectNo="+tProjectNo+"&MonthfeeNo="+tMonthfeeNo);
	}else{
		alert("请选择需要查看的费用信息！");
		return false;
	}
}
function initButton(){
	fm.all('AddID').style.display = "none";
	fm.all('DeleteID').style.display = "none";
	fm.all('SendUWID').style.display = "none";
	fm.all('UpdateID').style.display = "none";
	fm.all('divProjectMonthGrid2').style.display = "none";
	fm.all('divProjectYear1').style.display = "";
	fm.ProjectYear.value = tProjectYear;
	var mSQL = "select codename from LDCode where codetype = 'projectyear' and code = '"+tProjectYear+"'";
	var strQueryResult = easyExecSql(mSQL);
	if (strQueryResult) {
		fm.ProjectYearName.value = strQueryResult[0][0];
	}
}