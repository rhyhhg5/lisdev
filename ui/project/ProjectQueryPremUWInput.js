
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function initForm(){
	initProject();
	initPrem();
	queryClick();
}
function initProject(){
	var tSQL = "select ProjectNo,ManageCom,CreateDate,ProjectName from LCProjectInfo where ProjectNo = '"+tProjectNo+"'";
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("根据项目编码获取项目信息失败！");
		return false;
	}
	fm.all('ProjectNo').value = strQueryResult[0][0];
	fm.all('ManageCom').value = strQueryResult[0][1];
	fm.all('CreateDate').value = strQueryResult[0][2];
	fm.all('ProjectName').value = strQueryResult[0][3];
	var tMSQL = "select name from ldcom where comcode = '"+strQueryResult[0][1]+"'";
	var strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('ManageComName').value = strMResult[0][0];
	}
}
function initPrem(){
	var tSQL = "";
	if(tFlag == "1"){//查看当前审核意见
		tSQL = " select lpmt.ProjectYear,lpmt.PremYear,lpmt.ProjectMonth,lpmt.projecttype,lpmt.cost "
 			 + " from LCProjectPremUW lppu "
 			 + " inner join LCProjectMonthTrace lpmt on lppu.monthserialno = lpmt.monthserialno "
			 + " where lppu.serialno = '"+tSerialNo+"'";
	}else{
		tSQL = " select lpmi.ProjectYear,lpmi.PremYear,lpmi.ProjectMonth,lpmi.projecttype,lpmi.cost "
 			 + " from LCProjectMonthInfo lpmi "
			 + " where lpmi.monthfeeno = '"+tMonthfeeNo+"'";
	}
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("根据项目编码获取费用信息失败！");
		return false;
	}
	fm.all('ProjectYear').value = strQueryResult[0][0];
	fm.all('PremYear').value = strQueryResult[0][1];
	fm.all('ProjectMonth').value = strQueryResult[0][2];
	fm.all('ProjectType').value = strQueryResult[0][3];
	fm.all('Cost').value = strQueryResult[0][4];
	var tMSQL = "select codename from ldcode where codetype = 'projectyear' and code = '"+strQueryResult[0][0]+"'";
	var strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('ProjectYearName').value = strMResult[0][0];
	}
	tMSQL = "select codename from ldcode where codetype = 'projectyear' and code = '"+strQueryResult[0][1]+"'";
	strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('PremYearName').value = strMResult[0][0];
	}
	tMSQL = "select codename from ldcode where codetype = 'projectmonth' and code = '"+strQueryResult[0][2]+"'";
	strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('ProjectMonthName').value = strMResult[0][0];
	}
	var tMSQL = "select codename from ldcode where codetype = 'projecttype2' and code = '"+strQueryResult[0][3]+"'";
	var strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('ProjectTypeName').value = strMResult[0][0];
	}
}
function queryClick(){
	if(tFlag =="1"){//查询当时的审核意见
		whSQL = " where serialno = '"+tSerialNo+"' ";
	}else{//查询最后一条审核意见
		whSQL = " where monthfeeno = '"+tMonthfeeNo+"' order by uwdate desc,uwtime desc fetch first 1 rows only ";
	}
	var strSql = " select case when Conclusion is null or Conclusion ='' then '待审核' else (select codename from ldcode where codetype = 'projectconclusion' and code = Conclusion) end,auditopinion "
			   + " from LCProjectPremUW "
			   + whSQL;
	var strQueryResult = easyExecSql(strSql);
	if(strQueryResult){
		fm.Conclusion.value = strQueryResult[0][0];
		fm.AuditOpinion.value = strQueryResult[0][1];
	}	       				   
}
function cancel() {
	top.close();
}
