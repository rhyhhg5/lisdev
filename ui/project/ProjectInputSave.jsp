<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	//输入参数
	LCProjectInfoSchema mLCProjectInfoSchema = new LCProjectInfoSchema();
	LCProjectYearInfoSchema mLCProjectYearInfoSchema = new LCProjectYearInfoSchema();
	ProjectInfoUI mProjectInfoUI = new ProjectInfoUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");
	//String mProjectType = request.getParameter("ProjectType");
    String mProjectName=request.getParameter("ProjectName");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);

	if ("INSERT||MAIN".equals(transact)){
		if(mProjectNo == null || "".equals(mProjectNo))
		{
			//int length = 8;
		    //if (strManageCom.length() < length)
		    //{
		     //   strManageCom = PubFun.RCh(strManageCom, "0", length);
		   // }
		    //if(mProjectType == null || "".equals(mProjectType)){
		    	//Content = "该单已保存，可进行修改操作！";
				//FlagStr = "Fail";
		    //}else{
		    	//if(mProjectType.length() == 1){
		    		//strInput = strManageCom+"0"+mProjectType;
		    	//}else{
		    		//strInput = strManageCom+"00";
		    	//}
		    	//mProjectNo = PubFun1.CreateMaxNo("PROJECTNO", strInput, null);
		   // }
		    		//String tProjectNo = "";
		    		String tSQL = "select projectno from LCProjectInfo where projectname = '"+mProjectName+"' ";
		    		mProjectNo = new ExeSQL().getOneValue(tSQL);
		    		if("".equals(mProjectNo)){
		    			String tMaxProjectNoSQL = "select max(projectno) from LCProjectInfo where managecom = '"+strManageCom+"' and projectno like '"+strManageCom+"%' "; 
		    			String tMaxProjectNo = new ExeSQL().getOneValue(tMaxProjectNoSQL);
		    			if("".equals(tMaxProjectNo)){
		    				mProjectNo = strManageCom + "00" + "0001";
		    			}else{
		    			    mProjectNo = String.valueOf(Long.parseLong(tMaxProjectNo)+1);
		    			}
		    		}
		    
		}else{
			Content = "该单已保存，可进行修改操作！";
			FlagStr = "Fail";
		}
	}
	System.out.println("项目编号:" + mProjectNo);
	if(!"Fail".equals(FlagStr)){
		if (mProjectNo == null || "".equals(mProjectNo)) {
			Content = "保存失败，原因是:获取项目编号失败！";
			FlagStr = "Fail";
		} else {
		
			mLCProjectInfoSchema.setProjectNo(mProjectNo);
			mLCProjectInfoSchema.setProjectName(request.getParameter("ProjectName"));
			mLCProjectInfoSchema.setProvince(request.getParameter("Province"));
			mLCProjectInfoSchema.setCity(request.getParameter("City"));
			mLCProjectInfoSchema.setCounty(request.getParameter("County"));
			mLCProjectInfoSchema.setProjectType("");
			mLCProjectInfoSchema.setGrade(request.getParameter("Grade"));
			mLCProjectInfoSchema.setPersonType(request.getParameter("PersonType"));
			mLCProjectInfoSchema.setLimit(request.getParameter("Limit"));
			mLCProjectInfoSchema.setStartDate(request.getParameter("StartDate"));
			mLCProjectInfoSchema.setEndDate(request.getParameter("EndDate"));
			mLCProjectInfoSchema.setManageCom(request.getParameter("ManageCom"));
			
			mLCProjectYearInfoSchema.setProjectNo(mProjectNo);
			mLCProjectYearInfoSchema.setProjectYear(request.getParameter("ProjectYear"));
			mLCProjectYearInfoSchema.setCountyCount(request.getParameter("CountyCount"));
			mLCProjectYearInfoSchema.setInsuredPeoples(request.getParameter("InsuredPeoples"));
			mLCProjectYearInfoSchema.setAveragePrem(request.getParameter("AveragePrem"));
			
			String tPeoplesNumber = request.getParameter("PeoplesNumber");
			if(tPeoplesNumber == null || "".equals(tPeoplesNumber)){
				tPeoplesNumber = "-1";
			}
			mLCProjectYearInfoSchema.setPeoplesNumber(tPeoplesNumber);			
			
			mLCProjectYearInfoSchema.setBond(request.getParameter("Bond"));
			mLCProjectYearInfoSchema.setStopLine(request.getParameter("StopLine"));
			mLCProjectYearInfoSchema.setExpecteRate(request.getParameter("ExpecteRate"));
			mLCProjectYearInfoSchema.setBalanceTermFlag(request.getParameter("BalanceTermFlag"));
			mLCProjectYearInfoSchema.setPremCoverTermFlag(request.getParameter("PremCoverTermFlag"));
			mLCProjectYearInfoSchema.setBalanceTerm(request.getParameter("BalanceTerm"));
			mLCProjectYearInfoSchema.setPremCoverTerm(request.getParameter("PremCoverTerm"));
			mLCProjectYearInfoSchema.setCoInsuranceFlag(request.getParameter("CoInsuranceFlag"));
			mLCProjectYearInfoSchema.setManageCom(request.getParameter("ManageCom"));
			
			// 准备向后台传输数据 VData
			VData tVData = new VData();
			FlagStr = "";
			tVData.add(tG);
			tVData.addElement(mLCProjectInfoSchema);
			tVData.addElement(mLCProjectYearInfoSchema);
			try {
				mProjectInfoUI.submitData(tVData, transact);
			} catch (Exception ex) {
				Content = "保存失败，原因是:" + ex.toString();
				FlagStr = "Fail";
			}
			if (!FlagStr.equals("Fail")) {
				tError = mProjectInfoUI.mErrors;
				if (!tError.needDealError()) {
					Content = " 保存成功! ";
					FlagStr = "Succ";
				} else {
					Content = " 保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
				}
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProjectNo%>");
</script>
</html>