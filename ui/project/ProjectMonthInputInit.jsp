<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initProjectInfo();  
    initProjectMonthInfo();
    showAllCodeName();
    if("1" == tLookFlag){
    	initButton();
	}
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectMonthGrid();
	initProjectMGrid();
	divPage.style.display="";
	fm.MonthFeeNo.value = "";
	//fm.ProjectYearName.value = "";
	//fm.ProjectMonth.value = "";
	//fm.ProjectMonthName.value = "";
	//fm.ProjectType.value = "";
	//fm.ProjectTypeName.value = "";
	//fm.Cost.value = "";
	fm.State.value = "";
}
var ProjectMonthGrid;
function initProjectMonthGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
    
    iArray[1]=new Array();
    iArray[1][0]="月度费用编号";         	  		//列名
    iArray[1][1]="80px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[1][21]="MonthFeeNo";
            		
    iArray[2]=new Array();
    iArray[2][0]="项目年度";         	  		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[2][21]="projectYear";
    
    iArray[3]=new Array();
    iArray[3][0]="费用年度";         	  		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[3][21]="premYear";
    
    iArray[4]=new Array();
    iArray[4][0]="费用月度";         	
    iArray[4][1]="80px";            	
    iArray[4][2]=200;            		 
    iArray[4][3]=0; 
    iArray[4][21]="projectMonth";
    
    iArray[5]=new Array();
    iArray[5][0]="费用类型";         	  		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    iArray[5][21]="projectType"; 
    
    iArray[6]=new Array();
    iArray[6][0]="费用类型";         	  		//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][21]="projectTypeName"; 
    
    iArray[7]=new Array();
    iArray[7][0]="费用值";      				//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0; 
    iArray[7][21]="cost";
    
    iArray[8]=new Array();
    iArray[8][0]="费用状态";      				//列名
    iArray[8][1]="80px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=3;
    iArray[8][21]="State"; 
    
    iArray[9]=new Array();
    iArray[9][0]="费用状态";      				//列名
    iArray[9][1]="80px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;
     
     
    ProjectMonthGrid = new MulLineEnter("fm", "ProjectMonthGrid"); 
    //设置Grid属性
    ProjectMonthGrid.mulLineCount = 0;
    ProjectMonthGrid.displayTitle = 1;
    ProjectMonthGrid.locked = 1;
    ProjectMonthGrid.canSel = 1;
    ProjectMonthGrid.canChk = 0;
    ProjectMonthGrid.hiddenSubtraction = 1;
    ProjectMonthGrid.hiddenPlus = 1;
 	ProjectMonthGrid.selBoxEventFuncName = "getQueryResult";
    ProjectMonthGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
var ProjectMGrid;
function initProjectMGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][2]=10;
    iArray[0][3]=0;         			//列名
    
    iArray[1]=new Array();
    iArray[1][0]="月度费用编号";         	  		//列名
    iArray[1][1]="0px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=2;              			//是否允许输入,1表示允许，0表示不允许 
            		
    iArray[2]=new Array();
    iArray[2][0]="项目年度";         	  		//列名
    iArray[2][1]="80px";            		//列宽
    iArray[2][2]=200;            			//列最大值
    iArray[2][3]=2;                         //是否允许输入,1表示允许，0表示不允许 
    iArray[2][4]="projectyear1";
    iArray[2][15] = "projectno";
    iArray[2][16] = "#"+tProjectNo+"#";              			
    
    iArray[3]=new Array();
    iArray[3][0]="费用年度";         	  		//列名
    iArray[3][1]="80px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=2;                         //是否允许输入,1表示允许，0表示不允许 
    iArray[3][4]="projectyear";
    
    iArray[4]=new Array();
    iArray[4][0]="费用月度";         	
    iArray[4][1]="80px";            	
    iArray[4][2]=200;            		 
    iArray[4][3]=2;
    iArray[4][4]="projectmonth";  
    
    iArray[5]=new Array();
    iArray[5][0]="费用类型";         	  		//列名
    iArray[5][1]="80px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=3;                        //是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="费用类型";         	  		//列名
    iArray[6][1]="80px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=2;
    iArray[6][4]="projecttype2";              			//是否允许输入,1表示允许，0表示不允许
    iArray[6][5] = "6|5";
    iArray[6][6] = "1|0";  
     
    
    iArray[7]=new Array();
    iArray[7][0]="费用值";      				//列名
    iArray[7][1]="80px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=1; 
    
    ProjectMGrid = new MulLineEnter("fm", "ProjectMGrid"); 
    //设置Grid属性
    ProjectMGrid.mulLineCount = 1;
    ProjectMGrid.displayTitle = 1;
    ProjectMGrid.hiddenPlus =0;
    ProjectMGrid.hiddenSubtraction = 0;
    ProjectMGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
