<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
		String tProjectNo = request.getParameter("ProjectNo");
		String tProjectYear = request.getParameter("ProjectYear");
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectYearTermInput.js"></script>
		<%@include file="ProjectYearTermInputInit.jsp"%>
		<script>
  		var ManageCom = "<%=tGI.ManageCom%>"; 
  		var tProjectNo = "<%=tProjectNo%>";
  		var tProjectYear = "<%=tProjectYear%>";
  		</script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td class=titleImg>
						项目信息
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目编号
					</TD>
					<TD class=input>
						<Input class='common' name="ProjectNo" readonly>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<input class="codeNo" name="ManageCom" readonly><Input class="codeName" name="ManageComName" readonly>
					</TD>
					<TD class=title>
						建项日期
					</TD>
					<TD class=input>
						<input class="common" name="CreateDate" readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input colspan = '5'>
						<Input type="text"  class=readonly4  name=ProjectName  readonly>
					</TD>
				</TR>
			</table>

			<table>
				<tr>
					<td class=titleImg>
						年度信息
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目年度
					</TD>
					<TD class=input>
						<Input class=codeno name=ProjectYear verify="年度|notnull" readonly ondblclick="return showCodeList('projectYear',[this,ProjectYearName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('projectYear',[this,ProjectYearName],[0,1],null,null,null,1);"><input class=codename name=ProjectYearName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
						首次创建日期
					</TD>
					<TD class=input>
						<input type="text" class="common" name="FirstCreateDate" readonly>
					</TD>
					<TD class=title>
						首次创建用户
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=FirstOperator readonly>
					</TD>
				</TR>

				<TR class=common>
					<TD class=title>
						被保人人数
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=InsuredPeoples elementtype="nacessary" verify="被保人人数|notnull&int">
					</TD>
					<TD class=title>
						人均保费
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=AveragePrem elementtype="nacessary" verify="人均保费|notnull">
					</TD>
					<TD class=title>
						联合办公人力数
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=PeoplesNumber >
					</TD>
				</TR>

				<TR class=common>
					<TD class=title>
						保证金
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=Bond verify="保证金|num">
					</TD>
					<TD class=title>
						止损线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=StopLine verify="止损线|len<=100">
					</TD>
					<TD class=title>
						预计赔付率
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=ExpecteRate elementtype="nacessary" verify="预计赔付率|notnull&len<=100">
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						是否有结余返还条款
					</TD>
					<TD class=input>
						<Input class=codeno name=BalanceTermFlag verify="是否有结余返还条款|notnull" VALUE="" CodeData="0|^Y|是^N|否" ondblclick="return showCodeListEx('BalanceTermFlag',[this,BalanceTermFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('BalanceTermFlag',[this,BalanceTermFlagName],[0,1],null,null,null,1);"><input class=codename name=BalanceTermFlagName readonly=true elementtype="nacessary"> 		     		
					</TD>
					<TD class=title>
						是否有保费回补条款
					</TD>
					<TD class=input>
						<Input class=codeno name=PremCoverTermFlag verify="是否有保费回补条款|notnull" VALUE="" CodeData="0|^Y|是^N|否" ondblclick="return showCodeListEx('PremCoverTermFlag',[this,PremCoverTermFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PremCoverTermFlag',[this,PremCoverTermFlagName],[0,1],null,null,null,1);"><input class=codename name=PremCoverTermFlagName readonly=true elementtype="nacessary"> 		     		
					</TD>
					<TD class=title>
						覆盖市县数
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=CountyCount elementtype="nacessary" verify="覆盖县市数|notnull">
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						是否共保标识
					</TD>
					<TD class=input>
						<Input class=codeno name=CoInsuranceFlag verify="是否共保标识|notnull" VALUE="" CodeData="0|^Y|是^N|否" ondblclick="return showCodeListEx('CoInsuranceFlag',[this,CoInsuranceFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CoInsuranceFlag',[this,CoInsuranceFlagName],[0,1],null,null,null,1);"><input class=codename name=CoInsuranceFlagName readonly=true elementtype="nacessary"> 		     		
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>
			
			<table class=common align='center'>
				<TR class=common>
					<td class=title>
						结余返还条款
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name=BalanceTerm cols="100%" rows="3"></textarea>
					</TD>

				</TR>
				<TR class=common>
					<td class=title>
						保费回补条款
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name=PremCoverTerm cols="100%" rows="3"></textarea>
					</TD>

				</TR>
				<TR class=common>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</TR>
			</table>

			<table>
				<td class=button>
					<input type="button" class=cssButton value=" 返  回 " onclick="cancel()">
				</td>
			</table>
			
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
