
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function queryClick() {
	initProjectPremGrid();
	var strSql = " select * from ( " 
	+ " select distinct lcpi.ProjectNo, lcpi.ProjectName,lcpi.CreateDate,lcpi.ManageCom," 
	+ " (select SendDate from LCProjectPremUW where MonthFeeNo = lpmi.MonthFeeNo order by SerialNo desc fetch first 1 rows only) SendDate, " 
	+ " (select SendOperator from LCProjectPremUW where MonthFeeNo = lpmi.MonthFeeNo order by SerialNo desc fetch first 1 rows only) SendOperator " 
	+ " from LCProjectInfo lcpi " 
	+ " inner join LCProjectMonthInfo lpmi on lpmi.projectno = lcpi.projectno " 
	+ " where 1=1 and lcpi.state = '05' and lpmi.state = '02' " 
	+ getWherePart("lcpi.ManageCom", "ManageCom", "like") 
	+ getWherePart("lcpi.CreateDate", "StartDate", ">=") 
	+ getWherePart("lcpi.CreateDate", "EndDate", "<=") 
	+ getWherePart("lcpi.ProjectNo", "ProjectNo") 
	//+ getWherePart("lcpi.ProjectType", "ProjectType") 
	+ getWherePart("lcpi.ProjectName", "ProjectName", "like") 
	+ " order by lcpi.CreateDate desc,lcpi.projectno desc "
	+ ") as temp " 
	+ " where 1=1 " 
	+ getWherePart("temp.SendDate", "PremStartDate", ">=") 
	+ getWherePart("temp.SendDate", "PremEndDate", "<=");
	turnPage1.queryModal(strSql, ProjectPremGrid);
}
function ProjectPremUW() {
	var checkFlag = 0;
	for (i = 0; i < ProjectPremGrid.mulLineCount; i++) {
		if (ProjectPremGrid.getSelNo(i)) {
			checkFlag = ProjectPremGrid.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var tProjectNo = ProjectPremGrid.getRowColData(checkFlag - 1, 1);
		var tSendDate = ProjectPremGrid.getRowColData(checkFlag - 1, 5);
		var tSendOperator = ProjectPremGrid.getRowColData(checkFlag - 1, 6);
		if (tProjectNo == null || tProjectNo == "" || tProjectNo == "null") {
			alert("项目编码获取失败！");
			return false;
		}
		showInfo = window.open("./ProjectPremMain.jsp?ProjectNo=" + tProjectNo+"&SendDate="+tSendDate+"&SendOperator="+tSendOperator);
	} else {
		alert("请选择需要审核的项目！");
		return false;
	}
}

