<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	//输入参数
	LCProjectInfoSchema mLCProjectInfoSchema = new LCProjectInfoSchema();
	ProjectInfoUpdateUI mProjectInfoUpdateUI = new ProjectInfoUpdateUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");
	String mProjectType = request.getParameter("ProjectType");
	String mProjectName=request.getParameter("ProjectName");
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	//System.out.println(transact);
	if ("INSERT||MAIN".equals(transact)){

		if(mProjectNo == null || "".equals(mProjectNo))
		{
			/*int length = 8;
		    if (strManageCom.length() < length)
		    {
		        strManageCom = PubFun.RCh(strManageCom, "0", length);
		    }
		    if(mProjectType == null || "".equals(mProjectType)){
		    	Content = "该单已保存，可进行修改操作！";
				FlagStr = "Fail";
		    }else{
		    	if(mProjectType.length() == 1){
		    		strInput = strManageCom+"0"+mProjectType;
		    	}else{
		    		strInput = strManageCom+mProjectType;
		    	}
		    	mProjectNo = PubFun1.CreateMaxNo("PROJECTNO", strInput, null);
		    }
		    */
		String tSQL = "select projectno from LCProjectInfo where projectname = '"+mProjectName+"' ";
		mProjectNo = new ExeSQL().getOneValue(tSQL);
		if("".equals(mProjectNo)){
			String tMaxProjectNoSQL = "select max(projectno) from LCProjectInfo where managecom = '"+strManageCom+"' and projectno like '"+strManageCom+"%' "; 
			String tMaxProjectNo = new ExeSQL().getOneValue(tMaxProjectNoSQL);
			if("".equals(tMaxProjectNo)){
				mProjectNo = strManageCom + "00" + "0001";
			}else{
			    mProjectNo = String.valueOf(Long.parseLong(tMaxProjectNo)+1);
			}
		}
		}else{
			Content = "该单已保存，可进行修改操作！";
			FlagStr = "Fail";
		}
	}
	System.out.println("项目编号:" + mProjectNo);
	if(!"Fail".equals(FlagStr)){
		if (mProjectNo == null || "".equals(mProjectNo)) {
			Content = "保存失败，原因是:获取项目编号失败！";
			FlagStr = "Fail";
		} else {			
			//LCProjectInfo表的修改操作
			//获取页面上的字段值，并将该值放入本schema中，也即将从页面上获取的修改后值放入该表中，即修改操作
			mLCProjectInfoSchema.setProjectNo(mProjectNo);
			mLCProjectInfoSchema.setProjectName(request.getParameter("ProjectName"));
			mLCProjectInfoSchema.setProvince(request.getParameter("Province"));
			mLCProjectInfoSchema.setCity(request.getParameter("City"));
			mLCProjectInfoSchema.setCounty(request.getParameter("County"));
			mLCProjectInfoSchema.setProjectType(mProjectType);
			mLCProjectInfoSchema.setGrade(request.getParameter("Grade"));
			mLCProjectInfoSchema.setPersonType(request.getParameter("PersonType"));
			mLCProjectInfoSchema.setLimit(request.getParameter("Limit"));
			mLCProjectInfoSchema.setStartDate(request.getParameter("StartDate"));
			mLCProjectInfoSchema.setEndDate(request.getParameter("EndDate"));
			mLCProjectInfoSchema.setManageCom(request.getParameter("ManageCom"));
			
			//LCProjectTrace表的获取另一个表的字段并插入到本表的操作
			LCProjectTraceSchema tLCProjectTraceSchema = new LCProjectTraceSchema();    
			
			// 准备向后台传输数据 VData
			VData tVData = new VData();
			FlagStr = "";
			tVData.add(tG);
			tVData.addElement(mLCProjectInfoSchema);
			tVData.addElement(tLCProjectTraceSchema);
			//System.out.println("VData的值"+tLCProjectInfoSchema);
			try {
				mProjectInfoUpdateUI.submitData(tVData, transact);
			} catch (Exception ex) {
				Content = "保存失败，原因是:" + ex.toString();
				FlagStr = "Fail";
			}
			if (!FlagStr.equals("Fail")) {
				tError = mProjectInfoUpdateUI.mErrors;
				if (!tError.needDealError()) {
					Content = " 保存成功! ";
					FlagStr = "Succ";
				} else {
					Content = " 保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
				}
			}
		}
	}
%>
<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProjectNo%>");
	</script>
</html>