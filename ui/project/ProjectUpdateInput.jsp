<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
		String transact = request.getParameter("transact");
		String tProjectNo = request.getParameter("ProjectNo");
		tProjectNo = tProjectNo == null ? "" : tProjectNo;
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectUpdateInput.js"></script>
		<%@include file="ProjectUpdateInputInit.jsp"%>

		<script>
  	var ManageCom = "<%=tGI.ManageCom%>";
  <%/*	var tProjectNo = "86120000110001";*/%>
    var tProjectNo = "<%=tProjectNo%>";
  	var transact = "<%=transact%>";
  </script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./ProjectUpdateInputSave.jsp" method=post name=fm
			target="fraSubmit">

			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divProject);">
					</td>
					<td class=titleImg>
						项目修改
					</td>
				</tr>
			</table>

			<table>
				<tr>
					<td class=titleImg>
						项目关键信息
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=ProjectName elementtype="nacessary" verify="项目名称|notnull&len<=600">
					</TD>
					<TD class=title>
						项目编码
					</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=ProjectNo elementtype="nacessary" readonly>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<input class="codeNo" name="ManageCom" ondblclick="return showCodeList('comcode', [this,ManageComName], [0,1]);" onkeyup="return showCodeListKey('comcode', [this,ManageComName], [0,1]);"><Input class="codeName" name="ManageComName" readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						项目属地(省)
					</TD>
					<TD class=input>
						<Input class="codeno" name=Province verify="项目属地(省)|notnull"
							ondblclick="return showCodeList('Province',[this,ProvinceName],[0,1],null,'0','Code1',1);"
							onkeyup="return showCodeListKey('Province',[this,ProvinceName],[0,1],null,'0','Code1',1);"><input class=codename name=ProvinceName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
						项目属地(市)
					</TD>
					<TD class=input>
						<Input class="codeno" name=City
							ondblclick="return showCodeList('City',[this,CityName],[0,1],null,fm.Province.value,'Code1',1);"
							onkeyup="return showCodeListKey('City',[this,CityName],[0,1],null,fm.Province.value,'Code1',1);"><input class=codename name=CityName readonly=true>
					</TD>

					<TD class=title>
						项目属地(区县)
					</TD>
					<TD class=input>
						<Input class="codeno" name=County
							ondblclick="return showCodeList('County',[this,CountyName],[0,1],null,fm.City.value,'Code1',1);"
							onkeyup="return showCodeListKey('County',[this,CountyName],[0,1],null,fm.City.value,'Code1',1);"><input class=codename name=CountyName readonly=true>
					</TD>
				</TR>

				<TR class=common>
					<!-- <TD class=title>
						项目类型
					</TD>
					<TD class=input>
						<Input class=codeno name=ProjectType
							verify="项目类型|notnull&code:projecttype1"
							ondblclick="return showCodeList('projecttype1',[this,ProjectTypeName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKey('projecttype1',[this,ProjectTypeName],[0,1],null,null,null,1);"><input class=codename name=ProjectTypeName readonly=true verify="项目类型|notnull" elementtype=nacessary>
					</TD> -->
					<TD class=title>
						项目级别
					</TD>
					<TD class=input>
						<input class="codeNo" name="Grade"
							verify="项目级别|notnull&code:grade" readOnly
							ondblClick=" showCodeList('grade',[this,GradeName], [0,1],null,null,null,1);"
							onkeyup=" showCodeList('grade',[this,GradeName], [0,1]),null,null,null,1;"><Input class="codeName" name="GradeName" elementtype="nacessary" readonly>
					</TD>
					<TD class=title>
						覆盖人群
					</TD>
					<TD class=input>
						<input class="codeNo" name="PersonType"
							verify="覆盖人群|notnull&code:persontype" readOnly
							ondblClick=" showCodeList('persontype',[this,PersonTypeName], [0,1],null,null,null,1);"
							onkeyup=" showCodeListKey('persontype',[this,PersonTypeName], [0,1]),null,null,null,1;"><Input class="codeName" name="PersonTypeName" elementtype="nacessary" readonly>
					</TD>
				</TR>
				<br />
				<TR class=common>
					<TD class=title>
						合作期限
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=Limit elementtype="nacessary" verify="合作期限|notnull&len<=100">
						年
					</TD>
					<TD class=title>
						协议起期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="StartDate" readonly elementtype="nacessary" verify="协议起期|notnull">
					</TD>
					<TD class=title>
						协议止期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="EndDate" readonly elementtype="nacessary" verify=" 协议止期|notnull">
					</TD>
				</TR>
			</table>

			<table>
				<tr>
					<td class=common>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divProjectUW1);">
					</td>
					<td class=titleImg>
						年度信息
					</td>
				</tr>
			</table>

			<Div id="divProjectUW1" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectUW"></span>
						</td>
					</tr>
				</table>
			</div>

			<table align='center'>
				<input type=hidden id="fmtransact" name="fmtransact">
				<input type=hidden id="GSManageCom" name="GSManageCom">
				<td class=button width="10%">
					<input type="button" class=cssButton value=" 保  存 " name=save onclick="saveInfo()">
					<input type="button" class=cssButton value=" 相关业务资料上传 " name=insert11 onclick="queryMedic()">
					<input type="button" class=cssButton value="查看结余返还条款和保费回补条款" onclick="queryTerm()">
					<input type="button" class=cssButton value=" 退  出 " name=insert22 onclick="returnParentone()">
				</td>
			</table>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
