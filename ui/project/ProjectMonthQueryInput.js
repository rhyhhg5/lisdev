
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function beforeSubmit() {
	if (!verifyInput2()) {
		return false;
	}
	return true;
}
function ProjectMonthUpdate() {
	var checkFlag = 0;
	for (var i = 0; i < ProjectMonth.mulLineCount; i++) {
		if (ProjectMonth.getSelNo(i)) {
			checkFlag = ProjectMonth.getSelNo();
			break;
		}
	}
	if (checkFlag) {
		var tProjectNo = ProjectMonth.getRowColData(checkFlag - 1, 1);
		if(tProjectNo == null || tProjectNo == "null" || tProjectNo == ""){
			alert("未查询项目，不能进行维护操作！");
			return false;
		}
		showInfo = window.open("./ProjectMonthMain.jsp?ProjectNo=" + tProjectNo);
	} else {
		alert("请选择需要维护的项目！");
		return false;
	}
}
function queryClick() {
	var strSql = "select ProjectNo, ProjectName,  CreateDate, ManageCom, "
			   +" (select codename from ldcode where codetype = 'projecttype1' and code = projecttype), "
	           +" (select codename from ldcode where codetype = 'projectstate' and code = state), "
	           + " case when exists (select 1 from LCProjectMonthInfo where projectno = lcpi.projectno and state = '04') then '是' else '否' end "
	           + "from LCProjectInfo lcpi where 1=1 and state = '05' "
		       + getWherePart('ManageCom', 'ManageCom','like')
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('ProjectNo', 'ProjectNo')
		       //+ getWherePart('ProjectType', 'ProjectType')
		       + getWherePart('ProjectName', 'ProjectName','like')
		       + " order by lcpi.CreateDate desc,lcpi.projectno desc ";	
	turnPage1.queryModal(strSql, ProjectMonth);
}

