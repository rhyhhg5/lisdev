<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>    

<%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");	
  
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectGrid();
	initProjectPremUW();
	divPage.style.display="";                 
    fm.all('ManageCom').value = ManageCom;   
}

var ProjectGrid;
var ProjectPremUW;
function initProjectGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
            		//列名
 
    iArray[1]=new Array();
    iArray[1][0]="项目编号";         	  //列名
    iArray[1][1]="100px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="项目名称";         	
    iArray[2][1]="160px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    iArray[3]=new Array();
    iArray[3][0]="建项日期";         	  //列名
    iArray[3][1]="80px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构";      //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    ProjectGrid = new MulLineEnter("fm", "ProjectGrid"); 
    //设置Grid属性
    ProjectGrid.mulLineCount = 0;
    ProjectGrid.displayTitle = 1;
    ProjectGrid.locked = 1;
    ProjectGrid.canSel = 1;
    ProjectGrid.canChk = 0;
    ProjectGrid.hiddenSubtraction = 1;
    ProjectGrid.hiddenPlus = 1;
    ProjectGrid.selBoxEventFuncName = "getQueryResult";
    ProjectGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}

function initProjectPremUW() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
            		//列名
 
    iArray[1]=new Array();
    iArray[1][0]="项目年度";         	  //列名
    iArray[1][1]="60px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="费用年度";         	
    iArray[2][1]="60px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;   
    
    iArray[3]=new Array();
    iArray[3][0]="费用月度";         	
    iArray[3][1]="60px";            	
    iArray[3][2]=200;            		 
    iArray[3][3]=0;   
    
    iArray[4]=new Array();
    iArray[4][0]="费用类型";         	
    iArray[4][1]="60px";            	
    iArray[4][2]=200;            		 
    iArray[4][3]=0;     
    
    iArray[5]=new Array();
    iArray[5][0]="费用值";         	
    iArray[5][1]="60px";            	
    iArray[5][2]=200;            		 
    iArray[5][3]=0;        		 
    
    iArray[6]=new Array();
    iArray[6][0]="费用申请日期";         	  //列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="费用申请人";      //列名
    iArray[7][1]="60px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    
    iArray[8]=new Array();
    iArray[8][0]="审核人";      //列名
    iArray[8][1]="60px";            	//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=0;  
    
    iArray[9]=new Array();
    iArray[9][0]="审核结果";      //列名
    iArray[9][1]="60px";            	//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=0;  
    
    //iArray[8]=new Array();
    //iArray[8][0]="审核意见";      //列名
    //iArray[8][1]="120px";            	//列宽
    //iArray[8][2]=200;            			//列最大值
    //iArray[8][3]=0;  
    
    iArray[10]=new Array();
    iArray[10][0]="审核日期";      //列名
    iArray[10][1]="60px";            	//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=0;  
    
    iArray[11]=new Array();
    iArray[11][0]="流水号";      //列名
    iArray[11][1]="0px";            	//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=3; 
    
    iArray[12]=new Array();
    iArray[12][0]="项目编码";      //列名
    iArray[12][1]="0px";            	//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=3; 
     
    ProjectPremUW = new MulLineEnter("fm", "ProjectPremUW"); 
    //设置Grid属性
    ProjectPremUW.mulLineCount = 0;
    ProjectPremUW.displayTitle = 1;
    ProjectPremUW.locked = 1;
    ProjectPremUW.canSel = 1;
    ProjectPremUW.canChk = 0;
    ProjectPremUW.hiddenSubtraction = 1;
    ProjectPremUW.hiddenPlus = 1;
    ProjectPremUW.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
