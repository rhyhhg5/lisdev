<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	//输入参数
	LCProjectMonthInfoSet mLCProjectMonthInfoSet =new LCProjectMonthInfoSet();
	ProjectMonthInfoUI mLCProjectMonthInfoUI = new ProjectMonthInfoUI();
	String mMonthFeeNo[] = request.getParameterValues("ProjectMGrid1");
	String mProjectYear[] = request.getParameterValues("ProjectMGrid2");
	String mPremYear[] = request.getParameterValues("ProjectMGrid3");
	String mProjectMonth[] = request.getParameterValues("ProjectMGrid4");
	String mProjectType[] = request.getParameterValues("ProjectMGrid5");
	String mCost[] = request.getParameterValues("ProjectMGrid7");

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");
	String mState = request.getParameter("State");

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);

	System.out.println("项目编号:" + mProjectNo);
	if (mProjectNo == null || "".equals(mProjectNo)) {
		Content = "保存失败，原因是:获取项目编号失败！";
		FlagStr = "Fail";
	} else {
		if(mMonthFeeNo == null || mMonthFeeNo.length <1){
			Content = "处理失败，原因是:获取待处理信息失败！";
			FlagStr = "Fail";
		}
		if("UPDATE||MAIN".equals(transact) || "DELETE||MAIN".equals(transact)){
			if(mMonthFeeNo == null || mMonthFeeNo.length !=1){
				Content = "处理失败，原因是:获取月度费用编码失败！";
				FlagStr = "Fail";
			}
			if(mMonthFeeNo[0] == null || "".equals(mMonthFeeNo[0])){
				Content = "处理失败，原因是:获取月度费用编码失败！";
				FlagStr = "Fail";
			}
			System.out.println("修改时的月度费用编码："+mMonthFeeNo[0]);
			if(mState == null || "".equals(mState)){
				Content = "处理失败，原因是:获取月度费用状态失败！";
				FlagStr = "Fail";
			}
			System.out.println("修改时的月度费用状态："+mState);
		}
		if(!"Fail".equals(FlagStr)){
			for(int i=0;i<mMonthFeeNo.length;i++){
				LCProjectMonthInfoSchema tLCProjectMonthInfoSchema =new LCProjectMonthInfoSchema();
				tLCProjectMonthInfoSchema.setMonthFeeNo(mMonthFeeNo[i]);
				tLCProjectMonthInfoSchema.setProjectNo(mProjectNo);
				tLCProjectMonthInfoSchema.setProjectYear(mProjectYear[i]);
				tLCProjectMonthInfoSchema.setPremYear(mPremYear[i]);
				tLCProjectMonthInfoSchema.setProjectMonth(mProjectMonth[i]);
				tLCProjectMonthInfoSchema.setProjectType(mProjectType[i]);
				tLCProjectMonthInfoSchema.setCost(mCost[i]);
				tLCProjectMonthInfoSchema.setState(mState);
				mLCProjectMonthInfoSet.add(tLCProjectMonthInfoSchema);
			}
			
			// 准备向后台传输数据 VData
			VData tVData = new VData();
			FlagStr = "";
			tVData.add(tG);
			tVData.addElement(mLCProjectMonthInfoSet);
			try {
				mLCProjectMonthInfoUI.submitData(tVData, transact);
			} catch (Exception ex) {
				Content = "保存失败，原因是:" + ex.toString();
				FlagStr = "Fail";
			}
			if (!FlagStr.equals("Fail")) {
				tError = mLCProjectMonthInfoUI.mErrors;
				if (!tError.needDealError()) {
					Content = " 保存成功! ";
					FlagStr = "Succ";
				} else {
					Content = " 保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
				}
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProjectNo%>");
</script>
</html>