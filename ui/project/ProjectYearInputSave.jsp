<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	//输入参数
	LCProjectInfoSchema mLCProjectInfoSchema = new LCProjectInfoSchema();
	LCProjectYearInfoSchema mLCProjectYearInfoSchema = new LCProjectYearInfoSchema();
	ProjectYearInfoUI mLCProjectYearInfoUI = new ProjectYearInfoUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);

	if (mProjectNo == null || "".equals(mProjectNo)) {
		Content = "保存失败，原因是:获取项目编号失败！";
		FlagStr = "Fail";
	} else {
		mLCProjectYearInfoSchema.setProjectNo(mProjectNo);
		mLCProjectYearInfoSchema.setProjectYear(request.getParameter("ProjectYear"));
		mLCProjectYearInfoSchema.setInsuredPeoples(request.getParameter("InsuredPeoples"));
		mLCProjectYearInfoSchema.setAveragePrem(request.getParameter("AveragePrem"));
		mLCProjectYearInfoSchema.setBond(request.getParameter("Bond"));
		mLCProjectYearInfoSchema.setStopLine(request.getParameter("StopLine"));
		mLCProjectYearInfoSchema.setExpecteRate(request.getParameter("ExpecteRate"));
		
		String tPeoplesNumber = request.getParameter("PeoplesNumber");
		if(tPeoplesNumber == null || "".equals(tPeoplesNumber)){
			tPeoplesNumber = "-1";
		}
		mLCProjectYearInfoSchema.setPeoplesNumber(tPeoplesNumber);
		
		mLCProjectYearInfoSchema.setBalanceTermFlag(request.getParameter("BalanceTermFlag"));
		mLCProjectYearInfoSchema.setPremCoverTermFlag(request.getParameter("PremCoverTermFlag"));
		mLCProjectYearInfoSchema.setCountyCount(request.getParameter("CountyCount"));
		
		mLCProjectYearInfoSchema.setBalanceTerm(request.getParameter("BalanceTerm"));
		mLCProjectYearInfoSchema.setPremCoverTerm(request.getParameter("PremCoverTerm"));
		mLCProjectYearInfoSchema.setCoInsuranceFlag(request.getParameter("CoInsuranceFlag"));
		
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.addElement(mLCProjectYearInfoSchema);
		try {
			mLCProjectYearInfoUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")){
			tError = mLCProjectYearInfoUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProjectNo%>");
</script>
</html>