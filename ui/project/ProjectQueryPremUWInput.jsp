<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	String tProjectNo = request.getParameter("ProjectNo");
	String tSerialNo = request.getParameter("SerialNo");
	String tFlag = request.getParameter("Flag");
	String tMonthfeeNo = request.getParameter("MonthfeeNo");
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectQueryPremUWInput.js"></script>

		<script>
  		var tProjectNo = "<%=tProjectNo%>";
  		var tSerialNo = "<%=tSerialNo%>";
  		var tFlag = "<%=tFlag%>";
  		var tMonthfeeNo = "<%=tMonthfeeNo%>";
  		</script>
  		<script language="javascript">
			var intPageWidth=screen.availWidth;
			var intPageHeight=screen.availHeight;
			window.resizeTo(intPageWidth,intPageHeight);
			window.moveTo(-1, -1);
			//window.focus();
			
			var initWidth = 0;
		    //图片的队列数组
		    var pic_name = new Array();
		    var pic_place = 0;
		    var b_img	= 0;  //放大图片的次数
		    var s_img = 0;	//缩小图片的次数
		  
		    window.onunload = afterInput;
		  
		    function afterInput() {
		     try {
		       top.opener.afterInput();
		     }
		     catch(e) {}
		    }
		  
		    var mainPolNo = "";
		    var mainRisk = "";
			function focusMe()
			{
			  window.focus();
			}
		</script>
	</head>
	<body onload="initForm();">
		<form action="" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=titleImg>
						项目信息
					</td>
				</tr>
			</table>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目编号
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectNo readonly=true>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=codeNo readonly=true name=ManageCom><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
						建项日期
					</TD>
					<TD class=input>
						<input class="common" name="CreateDate" readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input colspan = '5'>
						<Input type="text"  class=readonly4  name=ProjectName  readonly>
					</TD>
				</TR>
			</table>
			
			<table>
				<tr>
					<td class=titleImg>
						费用信息
					</td>
				</tr>
			</table>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目年度
					</TD>
					<TD class=input>
						<Input class=codeno name=ProjectYear readonly=true><input class=codename name=ProjectYearName readonly=true>
					</TD>
					<TD class=title>
						费用年度
					</TD>
					<TD class=input>
						<Input class=codeno name=PremYear readonly=true><input class=codename name=PremYearName readonly=true>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>

				<TR class=common>
					<TD class=title>
						费用月度
					</TD>
					<TD class=input>
						<Input class=codeno name="ProjectMonth" readonly=true><input class=codename name=ProjectMonthName readonly=true >
					</TD>
					<TD class=title>
						费用类型
					</TD>
					<TD class=input>
						<Input class=codeno name="ProjectType" readonly=true><input class=codename name=ProjectTypeName readonly=true >
					</TD>
					<TD class=title>
						费用值
					</TD>
					<TD class=input>
						<Input class='common' name="Cost" readonly=true>
					</TD>
				</TR>
			</table>

			<div id="divQueryInput" , style="display:hidden">
				<table>
					<tr>
						<td class=titleImg>
							审核信息
						</td>
					</tr>
				</table>

				<table class=common align='center'>
					<TR class=common>
						<TD class=title>
							审核结论
						</TD>
						<TD class=input>
							<Input class='common' name=Conclusion readonly=true>
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>
						</TD>
						<TD class=title>
						</TD>
						<TD class=input>
						</TD>
					</TR>
				</table>
				<table class=common align='center'>
					<TR class=common>
						<TD class=title>
							审核意见
						</TD>
						<TD colspan="5">
							<textarea class="common" name="AuditOpinion" cols="100%" rows="3" readonly=true></textarea>
						</TD>
					</TR>
				</table>
			</div>
			<Div id="" style="display: '' ">
				<table>
					<tr>
						<td class=button align=left>
							<input type="button" class=cssButton value=" 退  出 " onclick="cancel()">
						</td>
					</tr>
				</table>
			</Div>
		</form>
	</body>
</html>
