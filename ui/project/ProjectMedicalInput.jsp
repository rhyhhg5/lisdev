<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	String tProjectNo = request.getParameter("ProjectNo");
	String tLookFlag = request.getParameter("LookFlag"); 
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectMedicalInput.js"></script>
		<%@include file="ProjectMedicalInputInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>";
  		var tProjectNo = "<%=tProjectNo%>";
  		var tLookFlag = "<%=tLookFlag%>";
  		</script>
	</head>
	<body onload="initForm();">
		<form action="" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=titleImg>
						项目信息
					</td>
				</tr>
			</table>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目编号
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectNo readonly=true>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=codeNo readonly=true name=ManageCom><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
						建项日期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="CreateDate" readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input colspan = '5'>
						<Input type="text"  class=readonly4  name=ProjectName  readonly>
					</TD>
				</TR>
			</table>

			<div id="divQueryInput" , style="display:hidden">
				<table>
					<tr>
						<td class=titleImg>
							查询条件
						</td>
					</tr>
				</table>

				<table class=common align='center'>

					<TR class=common>
						<TD class=title>
							文件编号
						</TD>
						<TD class=input>
							<Input class='common' name=FileNo verify="项目编号|len<=100">
						</TD>
						<TD class=title>
							上传日期
						</TD>
						<TD class=input>
							<input class="coolDatePicker" name="MakeDate" readonly>
						</TD>

						<TD class=title>
							上传用户
						</TD>
						<TD class=input>
							<Input class='common' name=Operator>
						</TD>
					</TR>

				</table>

				<table>
					<input type=hidden id="fmtransact" name="fmtransact">

					<td class=button align=left>
						<input type="button" class=cssButton value="查  询" id=QUERY onclick="queryClick()">
					</td>
				</table>

				<span id="spanCode"
					style="display: none; position:absolute; slategray"></span>

				<!-- 查询结果部分 -->
				<table>
					<tr>
						<td class=common>
							<IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divProjectMedical1);">
						</td>
						<td class=titleImg>
							文件信息
						</td>
					</tr>
				</table>
			</div>

			<!-- 信息（列表） -->
			<Div id="divProjectMedical1" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectMedicalGrid"> </span>
						</td>
					</tr>
				</table>
			</div>

			<Div id="divPage" align=center style="display: 'none' ">

				<table>
					<tr>
						<td class=button>
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
			<Div id="" align=center style="display: '' ">
				<table>
					<tr>
						<td class=button width="10%" align=left>
							<INPUT CLASS=cssButton VALUE="  下  载  " TYPE=button onclick="downLoad()">
							<INPUT CLASS=cssButton VALUE="  删  除  " id = "DeleteID" name = "DeleteID" TYPE=button onclick="Delete()">
							<INPUT CLASS=cssButton VALUE=" 上传文件 " id = "uploadID" name = "uploadID" TYPE=button onclick="upLoad()">
							<input type="button" class=cssButton value=" 退  出 " onclick="cancel()">
						</td>
					</tr>
				</table>
			</Div>

		</form>
	</body>
</html>
