
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function initProject(){
	var tSQL = "select ProjectNo,ManageCom,CreateDate,ProjectName from LCProjectInfo where ProjectNo = '"+tProjectNo+"'";
	var strQueryResult = easyExecSql(tSQL);
	if(!strQueryResult){
		alert("根据项目编码获取项目信息失败！");
		return false;
	}
	fm.all('ProjectNo').value = strQueryResult[0][0];
	fm.all('ManageCom').value = strQueryResult[0][1];
	fm.all('CreateDate').value = strQueryResult[0][2];
	fm.all('ProjectName').value = strQueryResult[0][3];
	var tMSQL = "select name from ldcom where comcode = '"+strQueryResult[0][1]+"'";
	var strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('ManageComName').value = strMResult[0][0];
	}
}
function queryClick(){
	initProjectMedicalGrid();
	var strSql = "select FileNo, FileName,Discription, MakeDate, Operator "
	           + "from LCProjectFileManage where 1=1 and projectno = '"+tProjectNo+"' and state = '01' "
		       + getWherePart('FileNo', 'FileNo')
		       + getWherePart('MakeDate', 'MakeDate')
		       + getWherePart('Operator', 'Operator')
		       ;
		       				   
	turnPage1.queryModal(strSql, ProjectMedicalGrid);
}
//下载
function downLoad(){
	var row = ProjectMedicalGrid.getSelNo();
	if (row == null || row == 0) {
		alert("请选择要下载的文件！");
		return false;
	}
	var fileName = ProjectMedicalGrid.getRowColData(row - 1, 2);
	if (fileName == null || fileName == "") {
		alert("没有附件，不能进行下载操作！");
		return false;
	}
	fm.action = "./ProjectfileDownLoadSave.jsp";
	fm.submit();
}
//上传
function upLoad(){
	showInfo = window.open("./ProjectFileUpLoadMain.jsp?ProjectNo="+tProjectNo);
}
function Delete(){
	var row = ProjectMedicalGrid.getSelNo();
	if (row == null || row == 0) {
		alert("请选择要删除的文件！");
		return false;
	}
	var fileName = ProjectMedicalGrid.getRowColData(row - 1, 2);
	if (fileName == null || fileName == "") {
		alert("没有附件，不能进行删除操作！");
		return false;
	}
	var i = 0;
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = "./ProjectMedicalInputSave.jsp?ProjectNo="+tProjectNo;
	fm.submit();
}
function cancel() {
	top.close();
}
function afterSubmit(FlagStr,content,aProjectNo) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	fm.all('ProjectNo').value  = aProjectNo;
	queryClick();
}
