<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
		String tProjectNo = request.getParameter("ProjectNo");
		String transact = request.getParameter("transact");
	%>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectInput.js"></script>
		<%@include file="ProjectInputInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>";
  		var tProjectNo = "";
  		var transact = "<%=transact%>";
  		</script>
	</head>
	<body onload="initForm();initElementtype();">
		<form action="./ProjectInputSave.jsp" method=post name=fm target="fraSubmit">
		<br />
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divProject);">
					</td>
					<td class=titleImg>
						项目信息
					</td>
				</tr>
			</table>
            <Div id="divProject" style="display:''" align=center>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=ProjectName elementtype="nacessary" verify="项目名称|notnull&len<=600">
					</TD>
					<TD class=title>
						项目编码
					</TD>
					<TD class=input>
						<Input type="text" class="readonly" name=ProjectNo elementtype="nacessary" readonly>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<Input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						项目属地(省)
					</TD>
					<TD class=input>
						<Input class="codeno" name=Province verify="项目属地(省)|notnull" ondblclick="return showCodeList('Province',[this,ProvinceName],[0,1],null,'0','Code1',1);" onkeyup="return showCodeListKey('Province',[this,ProvinceName],[0,1],null,'0','Code1',1);"><input class=codename name=ProvinceName readonly=true elementtype=nacessary>
					</TD>
					<TD class=title>
						项目属地(市)
					</TD>
					<TD class=input>
						<Input class="codeno" name=City ondblclick="return showCodeList('City',[this,CityName],[0,1],null,fm.Province.value,'Code1',1);" onkeyup="return showCodeListKey('City',[this,CityName],[0,1],null,fm.Province.value,'Code1',1);"><input class=codename name=CityName readonly=true>
					</TD>

					<TD class=title>
						项目属地(区县)
					</TD>
					<TD class=input>
						<Input class="codeno" name=County ondblclick="return showCodeList('County',[this,CountyName],[0,1],null,fm.City.value,'Code1',1);" onkeyup="return showCodeListKey('County',[this,CountyName],[0,1],null,fm.City.value,'Code1',1);"><input class=codename name=CountyName readonly=true>
					</TD>
				</TR>

				<TR class=common>
					<!-- <TD class=title>
						项目类型
					</TD>
					<TD class=input>
						<Input class=codeno name=ProjectType verify="项目类型|notnull&code:projecttype1" ondblclick="return showCodeList('projecttype1',[this,ProjectTypeName],[0,1],null,null,null,1);"	onkeyup="return showCodeListKey('projecttype1',[this,ProjectTypeName],[0,1],null,null,null,1);"><input class=codename name=ProjectTypeName readonly=true verify="项目类型|notnull" elementtype=nacessary>
					</TD> -->
					<TD class=title>
						项目级别
					</TD>
					<TD class=input>
						<input class="codeNo" name="Grade" verify="项目级别|notnull&code:grade" readOnly	ondblClick=" showCodeList('grade',[this,GradeName], [0,1],null,null,null,1);" onkeyup=" showCodeList('grade',[this,GradeName], [0,1]),null,null,null,1;"><Input class="codeName" name="GradeName" elementtype="nacessary" readonly>
					</TD>
					<TD class=title>
						覆盖人群
					</TD>
					<TD class=input>
						<input class="codeNo" name="PersonType" verify="覆盖人群|notnull&code:persontype" readOnly
							ondblClick=" showCodeList('persontype',[this,PersonTypeName], [0,1],null,null,null,1);"
							onkeyup=" showCodeListKey('persontype',[this,PersonTypeName], [0,1]),null,null,null,1;"><Input class="codeName" name="PersonTypeName" elementtype="nacessary" readonly>
					</TD>
				</TR>
				<br />
				<TR class=common>
					<TD class=title>
						合作期限
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=Limit elementtype="nacessary" verify="合作期限|notnull&num">年
					</TD>
					<TD class=title>
						协议起期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="StartDate" readonly elementtype="nacessary" verify="协议起期|notnull">
					</TD>
					<TD class=title>
						协议止期
					</TD>
					<TD class=input>
						<input class="coolDatePicker" name="EndDate" readonly elementtype="nacessary" verify=" 协议止期|notnull">
					</TD>
				</TR>
			</table>
			</Div>
			<br />
			<table>
				<tr>
					<td>
						<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divYear);">
					</td>
					<td class=titleImg>
						首年度信息
					</td>
				</tr>
			</table>
			<Div id="divYear" style="display:''" align=center>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						年度
					</TD>
					<TD class=input>
						<Input class=codeno name=ProjectYear verify="年度|notnull&code:projectyear"	ondblclick="return showCodeList('projectyear',[this,ProjectYearName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('projectyear',[this,ProjectYearName],[0,1],null,null,null,1);"><input class=codename name=ProjectYearName readonly=true	elementtype=nacessary>
					</TD>
					<TD class=title>
						覆盖市县数
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=CountyCount elementtype="nacessary" verify="覆盖县市数|notnull">
					</TD>
					<TD class=title>

					</TD>
					<TD class=input>

					</TD>
				</TR>

				<TR class=common>
					<TD class=title>
						被保人人数
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=InsuredPeoples elementtype="nacessary" verify="被保人人数|notnull&int">

					</TD>
					<TD class=title>
						人均保费
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=AveragePrem elementtype="nacessary" verify="人均保费|notnull">
					</TD>
					<TD class=title>
						联合办公人力数
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=PeoplesNumber >
					</TD>
				</TR>

				<TR class=common>
					<TD class=title>
						保证金
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=Bond >

					</TD>
					<TD class=title>
						止损线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=StopLine verify="止损线|len<=100">
					</TD>
					<TD class=title>
						预计赔付率
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=ExpecteRate elementtype="nacessary" verify="预计赔付率|notnull&len<=100">
					</TD>
				</TR>
				<!-- <tr class=common>
					
					<TD class=title8>
						核算周期
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=AccountCycle>
					</TD>
					<TD class=title8>
						结余线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=BalanceLine>
					</TD>
				</tr>
				<tr>
					<TD class=title8>
						成本占比
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=CostRate>
					</TD>
					<TD class=title8>
						返还比例
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=BalanceRate>
					</TD>
					<TD class=title8>
						返还类型
					</TD>
					<TD class=input>
						<Input class=codeno name=BalanceType VALUE=""
							CodeData="0|^0|常规^1|特殊"
							ondblclick="return showCodeListEx('BalanceType',[this,BalanceTypeName],[0,1],null,null,null,1);"
							onkeyup="return showCodeListKeyEx('BalanceType',[this,BalanceTypeName],[0,1],null,null,null,1);" readonly><input class=codename name=BalanceTypeName readonly=true>
					</TD>
				</tr>
				<tr>
					
					<TD class=title8>
						共担线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=SharedLine elementtype="nacessary">
					</TD>
					<TD class=title8>
						共担比例
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=SharedRate>
					</TD>
				</tr>
				<tr>
					<TD class=title8>
						回补线
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=RevertantLine elementtype="nacessary">
					</TD>
					<TD class=title8>
						回补比例
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=RevertantRate>
					</TD>
					<TD class=title8>
						
					</TD>
					<TD class=input>
						
					</TD>
				</tr> -->
			</table>
			<table class=common>
				
				<TR class=common>
					 <TD class=title>
						是否有结余返还条款
					</TD>
					<TD class=input>
						<Input class=codeno name=BalanceTermFlag verify="是否有结余返还条款|notnull" VALUE="" CodeData="0|^Y|是^N|否" ondblclick="return showCodeListEx('BalanceTermFlag',[this,BalanceTermFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('BalanceTermFlag',[this,BalanceTermFlagName],[0,1],null,null,null,1);"><input class=codename name=BalanceTermFlagName readonly=true elementtype="nacessary"> 		     		
					</TD> 
					<TD class=title>
						是否有保费回补条款
					</TD>
					<TD class=input>
						<Input class=codeno name=PremCoverTermFlag verify="是否有保费回补条款|notnull" VALUE="" CodeData="0|^Y|是^N|否" ondblclick="return showCodeListEx('PremCoverTermFlag',[this,PremCoverTermFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('PremCoverTermFlag',[this,PremCoverTermFlagName],[0,1],null,null,null,1);"><input class=codename name=PremCoverTermFlagName readonly=true elementtype="nacessary"> 		     		
					</TD>
					<TD class=title>
						是否共保标识
					</TD>
					<TD class=input>
						<Input class=codeno name=CoInsuranceFlag verify="是否共保标识|notnull" VALUE="" CodeData="0|^Y|是^N|否" ondblclick="return showCodeListEx('CoInsuranceFlag',[this,CoInsuranceFlagName],[0,1],null,null,null,1);" onkeyup="return showCodeListKeyEx('CoInsuranceFlag',[this,CoInsuranceFlagName],[0,1],null,null,null,1);"><input class=codename name=CoInsuranceFlagName readonly=true elementtype="nacessary"> 		     		
					</TD>
				</TR>
			</table>
			<table class=common align='center'>
			 <TR class=common>
					<!--<td class=title>
						风险调节文本
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name=BalanceTxt cols="100%" rows="3"></textarea>
					</TD>-->
				</TR>
				
				<TR class=common>
					<td class=title>
						结余返还条款
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name=BalanceTerm cols="100%" rows="3"></textarea>
					</TD>

				</TR>
				<TR class=common>
					<td class=title>
						保费回补条款
					</td>
					<TD class=input colspan="5">
						<textarea class="common" name=PremCoverTerm cols="100%" rows="3"></textarea>
					</TD>

				</TR>
				<TR class=common>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
					<td class=title></td>
					<td class=input></td>
				</TR>
			</table>
			</Div>
			<br />
			<table align='center'>
				<input type=hidden id="fmtransact" name="fmtransact">

				<td class=button width="10%">
					<input type="button" class=cssButton value=" 保  存 " name=insert onclick="submitForm()"> 
				</td>
				<td class=button width="10%"> 
					<input type="button" class=cssButton value="相关业务资料上传" in=DELETE	onclick="addMedical()">
				</td>
				<td class=button width="10%">
					<input type="button" class=cssButton value="增加次年度信息" id=UPDATE onclick="addYearInfo()">
				</td>
				<td class=button width="10%">
					<input type="button" class=cssButton value=" 项目送审 " id=QUERY onclick="sendUW()">
				</td>
				<td class=button width="10%">
					<input type="button" class=cssButton value=" 查看具体审核意见 " id=QUERY onclick="queryUWYJ()">
				</td>

			</table>
			<table>
				<br/>
				<tr class=common>
					<td>
					<font color = red  size = 2>
						录入说明：<br/>
						1、被保人人数：单位为人。<br>
						2、保证金：单位为元，可保留两位小数。<br/>
						3、预计赔付率：请填小数，如99%应录入0.99。<br/>
					</td>
				</tr>
			</table>
			<span id="spanCode"
				style="display: none; position:absolute; slategray"></span>

		</form>
	</body>
</html>
