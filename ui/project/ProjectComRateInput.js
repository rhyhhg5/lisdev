
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function submitForm() {
	var mSelNo = ProjectYearGrid.getSelNo();
	if (mSelNo == 0 || mSelNo == null){
		alert("请选择需要维护的年度！");
		return false;
	}
	if (!verifyInput2()){
		return false;
	}
	if(!isNumeric(fm.ComplexRate.value)){
		alert("综合赔付率必须为数字！");
		fm.ComplexRate.focus();
		return false;
	}
	var tStrs = new Array();
	var tComplexRate = parseFloat(fm.ComplexRate.value)+"";  
	tStrs = tComplexRate.split(".");
	if(tStrs.length>1){
		if(tStrs[1].length>6){
			alert("综合赔付率最多保留六位小数！");
			fm.AveragePrem.focus();
			return false;
		}
	}
	var showStr = "\u6b63\u5728\u4fdd\u5b58\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.fmtransact.value = "UPDATE||MAIN";
	fm.submit();
	return true;
}
function cancel() {
	top.close();
}
function afterSubmit(FlagStr, content) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	initForm();
}
function initProjectInfo() {
	if (tProjectNo == null || tProjectNo == "") {
		alert("获取项目编码失败！");
		return false;
	}
	var mSQL = "select ProjectNo,ManageCom,CreateDate,ProjectName from LCProjectInfo where ProjectNo=" + "'" + tProjectNo + "'";
	var strQueryResult = easyExecSql(mSQL);
	if (!strQueryResult) {
		alert("获取项目信息失败！");
		return false;
	}
	fm.all("ProjectNo").value = strQueryResult[0][0];
	fm.all("ManageCom").value = strQueryResult[0][1];
	fm.all("CreateDate").value = strQueryResult[0][2];
	fm.all("ProjectName").value = strQueryResult[0][3];
	var tMSQL = "select name from ldcom where comcode = '"+strQueryResult[0][1]+"'";
	var strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('ManageComName').value = strMResult[0][0];
	}
}
function initProjectYearInfo(){
	if (tProjectNo == null || tProjectNo == "") {
		alert("获取项目编码失败！");
		return false;
	}
	var tSQL = "select ProjectYear,InsuredPeoples,AveragePrem,case  when PeoplesNumber = -1 then '' else char(PeoplesNumber) end,Bond,StopLine,ExpecteRate,ComplexRate, "
			 + "case when BalanceTermFlag = 'Y' then '是' when BalanceTermFlag = 'N' then '否' else '' end,"
			 + "case when PremCoverTermFlag = 'Y' then '是' when PremCoverTermFlag = 'N' then '否' else '' end, "
			 + "CountyCount, "
			 + "case when CoInsuranceFlag = 'Y' then '是' when CoInsuranceFlag = 'N' then '否' else '' end "
			 + "from LCProjectYearInfo where ProjectNo='" + tProjectNo + "'";
	turnPage1.queryModal(tSQL, ProjectYearGrid);
}
function getQueryResult() {
	var mSelNo = ProjectYearGrid.getSelNo();
	fm.ProjectYear.value = ProjectYearGrid.getRowColData(mSelNo - 1,1);
	fm.ComplexRate.value = ProjectYearGrid.getRowColData(mSelNo - 1,8);
}
function queryTerm(){
	var checkFlag = 0;
	for (i=0; i<ProjectYearGrid.mulLineCount; i++)
	{
	  if (ProjectYearGrid.getSelNo(i))
	  {
	    checkFlag = ProjectYearGrid.getSelNo();
	    break;
	  }
	}
	if(checkFlag){
		var	tProjectYear = ProjectYearGrid.getRowColData(checkFlag - 1, 1);
		if(tProjectYear == null || tProjectYear == "" || tProjectYear =="null"){
			alert("获取年度失败！");
			return false;
		}
		showInfo = window.open("./ProjectYearTermInput.jsp?ProjectNo=" + tProjectNo+"&ProjectYear="+tProjectYear);
	}else{
		alert("请选择年度！");
		return false;
	}
}