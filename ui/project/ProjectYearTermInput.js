
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function cancel() {
	top.close();
}
function initProjectInfo() {
	if (tProjectNo == null || tProjectNo == "") {
		alert("获取项目编码失败！");
		return false;
	}
	var mSQL = "select ProjectNo,ManageCom,CreateDate,ProjectName from LCProjectInfo where ProjectNo=" + "'" + tProjectNo + "'";
	var strQueryResult = easyExecSql(mSQL);
	if (!strQueryResult) {
		alert("获取项目信息失败！");
		return false;
	}
	fm.all("ProjectNo").value = strQueryResult[0][0];
	fm.all("ManageCom").value = strQueryResult[0][1];
	fm.all("CreateDate").value = strQueryResult[0][2];
	fm.all("ProjectName").value = strQueryResult[0][3];
	var tMSQL = "select name from ldcom where comcode = '"+strQueryResult[0][1]+"'";
	var strMResult = easyExecSql(tMSQL);
	if(strMResult){
		fm.all('ManageComName').value = strMResult[0][0];
	}
}
function initProjectYearInfo(){
	if (tProjectNo == null || tProjectNo == "") {
		alert("获取项目编码失败！");
		return false;
	}
	var tSQL = "select ProjectYear,InsuredPeoples,AveragePrem,case  when PeoplesNumber = -1 then '' else char(PeoplesNumber) end,"
			 + "Bond,StopLine,ExpecteRate,CreateDate,FirstOperator,BalanceTermFlag,PremCoverTermFlag,"
			 + "CountyCount,CoInsuranceFlag,BalanceTerm,PremCoverTerm "
			 + "from LCProjectYearInfo where ProjectNo='" + tProjectNo + "' "
			 + "and ProjectYear = '"+tProjectYear+"' ";
	var strResult = easyExecSql(tSQL);
	if(strResult){
		fm.ProjectYear.value = tProjectYear;
		fm.InsuredPeoples.value = strResult[0][1];
		fm.AveragePrem.value = strResult[0][2];
		fm.PeoplesNumber.value = strResult[0][3];
		fm.Bond.value = strResult[0][4];
		fm.StopLine.value = strResult[0][5];
		fm.ExpecteRate.value = strResult[0][6];
		fm.FirstCreateDate.value = strResult[0][7];
		fm.FirstOperator.value = strResult[0][8];
		fm.BalanceTermFlag.value = strResult[0][9];
		fm.PremCoverTermFlag.value = strResult[0][10];
		fm.CountyCount.value = strResult[0][11];
		fm.CoInsuranceFlag.value = strResult[0][12];
		fm.BalanceTerm.value = strResult[0][13];
		fm.PremCoverTerm.value = strResult[0][14];
	}
	showAllCodeName();
}