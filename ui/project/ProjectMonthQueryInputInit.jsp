<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();  
    initElementtype();
    showAllCodeName();
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectMonth();
	divPage.style.display="";
	fm.all('ManageCom').value = ManageCom;
}

var ProjectMonth;
function initProjectMonth() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         		//列名
    
    iArray[1]=new Array();
    iArray[1][0]="项目编号";         	  //列名
    iArray[1][1]="80px";            	//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    
    iArray[2]=new Array();
    iArray[2][0]="项目名称";         	
    iArray[2][1]="160px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0; 
   
    iArray[3]=new Array();
    iArray[3][0]="建项日期";         	  //列名
    iArray[3][1]="80px";            	//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[4]=new Array();
    iArray[4][0]="管理机构";      //列名
    iArray[4][1]="80px";            	//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[5]=new Array();
    iArray[5][0]="项目类型";      //列名
    iArray[5][1]="80px";            	//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[6]=new Array();
    iArray[6][0]="项目状态";      //列名
    iArray[6][1]="80px";            	//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许
    
    iArray[7]=new Array();
    iArray[7][0]="是否有审核不通过的费用项";      //列名
    iArray[7][1]="120px";            	//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许       
     
    ProjectMonth = new MulLineEnter("fm", "ProjectMonth"); 
    //设置Grid属性
    ProjectMonth.mulLineCount = 0;
    ProjectMonth.displayTitle = 1;
    ProjectMonth.locked = 1;
    ProjectMonth.canSel = 1;
    ProjectMonth.canChk = 0;
    ProjectMonth.hiddenSubtraction = 1;
    ProjectMonth.hiddenPlus = 1;
    ProjectMonth.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
