<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script language="JavaScript">
function initForm()
{
  try
  {
    initInpBox();
    initProjectInfo();  
    initProjectYearInfo();
    showAllCodeName();
    if("1" == tLookFlag){
		fm.all('AddID').style.display = "none";
	}
  }
  catch(re)
  {
    alert("初始化界面错误!");
  }
}
function initInpBox()
{ 
	initProjectYearGrid();
	divPage.style.display="";
	fm.ProjectYear.value = "";
	fm.ProjectYearName.value = "";
	fm.FirstCreateDate.value = "";
	fm.FirstOperator.value = "";
	fm.InsuredPeoples.value = "";
	fm.AveragePrem.value = "";
	fm.Bond.value = "";
	fm.StopLine.value = "";
	fm.ExpecteRate.value = "";
	fm.BalanceTermFlag.value = "";
	fm.BalanceTermFlagName.value = "";
	fm.PremCoverTermFlag.value = "";
	fm.PremCoverTermFlagName.value = "";
	fm.PeoplesNumber.value = "";
	fm.CountyCount.value = "";
	fm.BalanceTerm.value = "";
	fm.PremCoverTerm.value = "";
	fm.CoInsuranceFlag.value = "";
	fm.CoInsuranceFlagName.value = "";
}

var ProjectYearGrid;
function initProjectYearGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		//列名
    iArray[0][1]="30px";         		//列名    
    iArray[0][3]=0;         			//列名
            		
    iArray[1]=new Array();
    iArray[1][0]="项目年度";         	  		//列名
    iArray[1][1]="50px";            		//列宽
    iArray[1][2]=200;            			//列最大值
    iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
    iArray[1][21]="ProjectYear";
    
    iArray[2]=new Array();
    iArray[2][0]="被保人人数";         	
    iArray[2][1]="50px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;    
    iArray[2][21]="InsuredPeoples";
    
    iArray[3]=new Array();
    iArray[3][0]="人均保费";         	  		//列名
    iArray[3][1]="50px";            		//列宽
    iArray[3][2]=200;            			//列最大值
    iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[3][21]="AveragePrem";
    
    iArray[4]=new Array();
    iArray[4][0]="联合办公人力数";         	  		//列名
    iArray[4][1]="60px";            		//列宽
    iArray[4][2]=200;            			//列最大值
    iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许
    iArray[4][21]="PeoplesNumber";
    
    iArray[5]=new Array();
    iArray[5][0]="保证金";      				//列名
    iArray[5][1]="50px";            		//列宽
    iArray[5][2]=200;            			//列最大值
    iArray[5][3]=0; 
    iArray[5][21]="Bond"; 
	
	iArray[6]=new Array();
    iArray[6][0]="止损线";      				//列名
    iArray[6][1]="50px";            		//列宽
    iArray[6][2]=200;            			//列最大值
    iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    iArray[6][21]="StopLine";
    
    iArray[7]=new Array();
    iArray[7][0]="预计赔付率";      			//列名
    iArray[7][1]="50px";            		//列宽
    iArray[7][2]=200;            			//列最大值
    iArray[7][3]=0;
    iArray[7][21]="ExpecteRate";
    
    iArray[8]=new Array();
    iArray[8][0]="首次创建日期";      				//列名
    iArray[8][1]="50px";            		//列宽
    iArray[8][2]=200;            			//列最大值
    iArray[8][3]=3;              			//是否允许输入,1表示允许，0表示不允许  
    iArray[8][21]="CreateDate";
    
    iArray[9]=new Array();
    iArray[9][0]="首次创建用户";      			//列名
    iArray[9][1]="50px";            		//列宽
    iArray[9][2]=200;            			//列最大值
    iArray[9][3]=3;
    iArray[9][21]="FirstOperator";
    
    iArray[10]=new Array();
    iArray[10][0]="是否有结余返还条款";      				//列名
    iArray[10][1]="50px";            		//列宽
    iArray[10][2]=200;            			//列最大值
    iArray[10][3]=3;              			//是否允许输入,1表示允许，0表示不允许  
    iArray[10][21]="BalanceTermFlag";
    
    iArray[11]=new Array();
    iArray[11][0]="是否有保费回补条款";      			//列名
    iArray[11][1]="50px";            		//列宽
    iArray[11][2]=200;            			//列最大值
    iArray[11][3]=3;
    iArray[11][21]="PremCoverTermFlag";
    
    iArray[12]=new Array();
    iArray[12][0]="是否有结余返还条款";      				//列名
    iArray[12][1]="80px";            		//列宽
    iArray[12][2]=200;            			//列最大值
    iArray[12][3]=0;              			//是否允许输入,1表示允许，0表示不允许  
    iArray[12][21]="BalanceTermFlag";
    
    iArray[13]=new Array();
    iArray[13][0]="是否有保费回补条款";      			//列名
    iArray[13][1]="80px";            		//列宽
    iArray[13][2]=200;            			//列最大值
    iArray[13][3]=0;
    iArray[13][21]="PremCoverTermFlag";
    
    iArray[14]=new Array();
    iArray[14][0]="覆盖市县数";      			//列名
    iArray[14][1]="80px";            		//列宽
    iArray[14][2]=200;            			//列最大值
    iArray[14][3]=0;
    iArray[14][21]="CountyCount";
    
    iArray[15]=new Array();
    iArray[15][0]="是否共保标识";      			//列名
    iArray[15][1]="80px";            		//列宽
    iArray[15][2]=200;            			//列最大值
    iArray[15][3]=0;
    iArray[15][21]="CoInsuranceFlag";
     
    ProjectYearGrid = new MulLineEnter("fm","ProjectYearGrid"); 
    //设置Grid属性
    ProjectYearGrid.mulLineCount = 5;
    ProjectYearGrid.displayTitle = 1;
    ProjectYearGrid.locked = 1;
    ProjectYearGrid.canSel = 1;
    ProjectYearGrid.canChk = 0;
    ProjectYearGrid.hiddenSubtraction = 1;
    ProjectYearGrid.hiddenPlus = 1;
 	ProjectYearGrid.selBoxEventFuncName = "getQueryResult";
    ProjectYearGrid.loadMulLine(iArray);
  }
  catch(ex) 
  {
    alert(ex);
  }
}
</script>
