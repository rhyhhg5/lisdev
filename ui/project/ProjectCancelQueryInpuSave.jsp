<jsp:directive.page import="com.sinosoft.lis.schema.LCProjectCancelSchema"/>
<jsp:directive.page import="com.sinosoft.lis.pubfun.PubFun"/><%@include file="../common/jsp/UsrCheck.jsp"%>


<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	ProjectCancelUI lCProjectCancelUI = new ProjectCancelUI();


	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	   
	String  projectno = request.getParameter("ProjectNo1"); //项目编号
	String cancelDate = request.getParameter("CancelDate");//日期
	String dealOperator =request.getParameter("CancelOperator"); //操作人
	String  dealOpinion  =request.getParameter("AuditOpinion"); //作废原因
    
    GlobalInput tG    = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	

	//验证
	if (projectno == null || "".equals(projectno)) {
		Content = "保存失败，原因是:获取项目编号失败！";
		FlagStr = "Fail";
	}if (cancelDate == null || "".equals(cancelDate)) {
		Content = "保存失败，原因是:获取作废日期失败！";
		FlagStr = "Fail";
	}if (dealOperator == null || "".equals(dealOperator)) {
		Content = "保存失败，原因是:获取操作人失败！";
		FlagStr = "Fail";
	}if (dealOpinion == null || "".equals(dealOpinion)) {
		Content = "保存失败，原因是:获取作废原因失败！";
		FlagStr = "Fail";
	}
	
	//验证不为空 开始提交
	if(!"Fail".equals(FlagStr)){
			LCProjectCancelSchema  tLCProjectCancelSchema  = new  LCProjectCancelSchema();
			tLCProjectCancelSchema.setProjectNo(projectno);
			tLCProjectCancelSchema.setDealDate(cancelDate);
			tLCProjectCancelSchema.setOperator(dealOperator);
			tLCProjectCancelSchema.setDealOpinion(dealOpinion);
			tLCProjectCancelSchema.setManageCom(strManageCom);
			tLCProjectCancelSchema.setMakeDate(PubFun.getCurrentDate());//创建日期
			tLCProjectCancelSchema.setMakeTime(PubFun.getCurrentTime());//创建时间
			tLCProjectCancelSchema.setModifyDate(PubFun.getCurrentDate());//最后一次修改日期
			tLCProjectCancelSchema.setModifyTime(PubFun.getCurrentTime());//最后一次修改时间
			
	//向后台传输数据,添加作废信息
			VData tVData = new VData();
			FlagStr = "";
			tVData.add(tG);
			tVData.addElement(tLCProjectCancelSchema);
			try {
				lCProjectCancelUI.submitData(tVData, "INSERT||MAIN");
			} catch (Exception ex) {
				Content = "保存失败，原因是:" + ex.toString();
				FlagStr = "Fail";
			}
			if (!FlagStr.equals("Fail")) {
				tError = lCProjectCancelUI.mErrors;
				if (!tError.needDealError()) {
					Content = " 保存成功! ";
					FlagStr = "Succ";
				} else {
					Content = " 保存失败，原因是:" + tError.getFirstError();
					FlagStr = "Fail";
				}
			}
		}
	
%>

<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>