
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}
function queryClick() {
	initProjectContGrid();
	var strSql = "select lgc.GrpContNo,lgc.prtno,lgc.ManageCom,lgc.SignDate," + "(select codename from ldcode where codetype = 'markettype' and code = lgc.MarketType),lgc.GrpName,lcpc.ProjectYear " + " from LCProjectCont lcpc " + " inner join LCGrpCont lgc on lcpc.prtno = lgc.prtno" + " where 1=1 and projectno = '" + tProjectNo + "' and lgc.appflag = '1' " + getWherePart("lgc.ManageCom", "ManageCom", "like") + getWherePart("lgc.SignDate", "StartDate", ">=") + getWherePart("lgc.SignDate", "EndDate", "<=") + getWherePart("lgc.MarketType", "MarketType") + getWherePart("lgc.GrpContNo", "GrpContNo") + getWherePart("lcpc.ProjectYear", "ProjectYear");
	turnPage1.queryModal(strSql, ProjectContGrid);
}
function submitForm() {
	var count = 0;
	for (var i = 0; i < ProjectContGrid.mulLineCount; i++) {
		if (ProjectContGrid.getChkNo(i) == true) {
			count++;
		}
	}
	if (count == 0) {
		alert("\u8bf7\u9009\u62e9\u9700\u8981\u5904\u7406\u7684\u4fdd\u5355\uff01");
		return;
	}
	fm.fmtransact.value = "DELETE||MAIN";
	var showStr = "\u6b63\u5728\u5904\u7406\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit(); //提交
}
function cancel() {
	top.close();
}
function afterSubmit(FlagStr, content, cOperate) {
	showInfo.close();
	if (FlagStr == "Fail") {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	}
	initForm();
}
function initProjectInfo() {
	if (tProjectNo == null || tProjectNo == "") {
		alert("\u83b7\u53d6\u9879\u76ee\u7f16\u7801\u5931\u8d25\uff01");
		return false;
	}
	var tSQL = "select lpi.ProjectNo,lpi.ProjectName,lpi.CreateDate,(select count(1) from LCProjectCont where ProjectNo = lpi.ProjectNo) " + "from LCProjectInfo lpi" + " where ProjectNo = '" + tProjectNo + "'";
	
	//执行查询并返回结果
	var strQueryResult = easyExecSql(tSQL);
	if (!strQueryResult) {
		alert("\u4fee\u6539\u64cd\u4f5c\u65f6\uff0c\u6839\u636e\u9879\u76ee\u7f16\u7801\u83b7\u53d6\u9879\u76ee\u4fe1\u606f\u5931\u8d25\uff01");
		return false;
	}
	fm.all("ProjectNo").value = strQueryResult[0][0];
	fm.all("ProjectName").value = strQueryResult[0][1];
	fm.all("CreateDate").value = strQueryResult[0][2];
	fm.all("ContCount").value = strQueryResult[0][3];
}
function contDetail() {
	fromListReturn();
}
/*********************************************************************
 *  投保信息列表中选择一条，查看投保信息
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function fromListReturn() {
	var cContNo = "";
	var cPrtNo = "";
	var cManageCom = "";
	var count = 0;
	for (var i = 0; i < ProjectContGrid.mulLineCount; i++) {
		if (ProjectContGrid.getChkNo(i) == true) {
			cContNo = ProjectContGrid.getRowColData(i, 1);
			cPrtNo = ProjectContGrid.getRowColData(i, 2);
			cManageCom = ProjectContGrid.getRowColData(i, 3);
			count++;
		}
	}
	if (count == 0) {
		alert("请选择需要查看明细的保单！");
		return;
	}
	if (count != 1) {
		alert("只能选择一个保单查看保单明细！");
		return;
	}
	var tContType = "2";
	var tTableType = "C";
	
	if (tTableType == "C") {
		tTableType = "1";
	} else {
		tTableType = "2";
	}
	if (tContType == "2") {
		var strSql = "SELECT 1 from lcgrppol l where prtno='" + cPrtNo + "'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode) union SELECT 1 from lbgrppol l where prtno='" + cPrtNo + "'and  exists (select riskcode from lmriskapp m where riskprop='G' and risktype4='4' and riskcode=l.riskcode)";
		var arrResult = easyExecSql(strSql);
		if (arrResult == null) {
			var str = "SELECT 1 from lwmission where missionprop1='" + cPrtNo + "' and activityid  in ('0000002098','0000002099') and  missionprop5='1'";
			var arr = easyExecSql(str);
			if (arr == null) {
				window.open("../sys/GrpPolDetailQueryMain.jsp?ContNo=" + cContNo + "&ContType=" + tTableType);
			} else {
				easyScanWin = window.open("../sys/GrpPolULIDetailQueryMain.jsp?LoadFlag=16&polNo=" + cContNo + "&prtNo=" + cPrtNo + "&ContType=" + tTableType, "", "status=no,resizable=yes,scrollbars=yes ");
			}
		} else {
			easyScanWin = window.open("../sys/GrpPolULIDetailQueryMain.jsp?LoadFlag=16&polNo=" + cContNo + "&prtNo=" + cPrtNo + "&ContType=" + tTableType, "", "status=no,resizable=yes,scrollbars=yes ");
		}
	} else {
		if (tContType == "1") {
			window.open("../sys/PolDetailQueryMain.jsp?ContNo=" + cContNo + "&IsCancelPolFlag=0" + "&ContType=" + tTableType);
		}
	}
}
