<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	String tProjectNo = request.getParameter("ProjectNo");
	String tSendDate = request.getParameter("SendDate");
	String tSendOperator = request.getParameter("SendOperator");
	 %>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectPremInput.js"></script>
		<%@include file="ProjectPremInputInit.jsp"%>

		<script>
	  		var ManageCom = "<%=tGI.ManageCom%>";
	  		var tProjectNo = "<%=tProjectNo%>";
	  		var tSendDate = "<%=tSendDate%>";
	  		var tSendOperator = "<%=tSendOperator%>";
  		</script>

	</head>
	<body onload="initForm();">
		<form action="./ProjectPremInputSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=titleImg>
						项目信息
					</td>
				</tr>
			</table>

			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目编号
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectNo readonly>
					</TD>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input>
						<Input class='common' name=ProjectName readonly>
					</TD>
					<TD class=title>
						建项日期
					</TD>
					<TD class=input>
						<Input class='common' name=CreateDate readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						费用报送日期
					</TD>
					<TD class=input>
						<Input class='common' name=SendDate readonly>
					</TD>
					<TD class=title>
						费用报送人
					</TD>
					<TD class=input>
						<Input class='common' name=SendOperator readonly>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>
			<table>
				<tr>
					<td class=titleImg>
						费用清单
					</td>
				</tr>
			</table>

			<Div id="divProjectMonthGrid1" align=center style="display:''">
				<table class=common align=center>
					<tr class=common>
						<td>
							<span id="spanProjectMonthGrid"></span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">
				<table>
					<tr align=center>
						<td class=button>
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
			<table class=common>
				<TR class=common>
					<td class=title>
						审核意见
					</td>
					<TD class=input>
						<textarea class="common" name="AuditOpinion" cols="90%" rows="5"></textarea>
					</TD>
				</TR>
				<TR class=common>
					<td class=title>
					</td>
					<TD class=input>

					</TD>
					<td class=title>
					</td>
					<TD class=input>

					</TD>
					<td class=title>
					</td>
					<TD class=input>

					</TD>

				</TR>
			</table>
			<table>
				<td class=button>
					<input type=hidden id="fmtransact" name="fmtransact">
					<input type="button" class=cssButton value=" 审核通过  " onclick="Allow()">
					<input type="button" class=cssButton value=" 审核不通过  "onclick="NoAllow()">
					<input type="button" class=cssButton value=" 返 回 " onclick="cancel()">
				</td>
			</table>
			<span id="spanCode" style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
