
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var showInfo;
window.onfocus = myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus() {
	if (showInfo != null) {
		try {
			showInfo.focus();
		}
		catch (ex) {
			showInfo = null;
		}
	}
}

//下载
function downLoad() {
	if(fm.querySql.value != null && fm.querySql.value != "" && ProjectGrid.mulLineCount > 0)
    {
        var formAction = fm.action;
        fm.action = "ProjectFinishRateDownload.jsp";
        //fm.target = "_blank";
        fm.submit();
        fm.target = "fraSubmit";
        fm.action = formAction;
    }
    else
    {
        alert("请先执行查询操作，再打印！");
        return ;
    }
}
function queryClick() {
	initProjectGrid();
	var comcode = fm.ManageCom.value;
	var minYLCount = fm.minYLCount.value;
	var maxYLCount = fm.maxYLCount.value;
	var minLRCount = fm.minLRCount.value;
	var maxLRCount = fm.maxLRCount.value;
	var minFGSCount = fm.minFGSCount.value;
	var maxFGSCount = fm.maxFGSCount.value;
	var minZGSCount = fm.minZGSCount.value;
	var maxZGSCount = fm.maxZGSCount.value;
	var minLXCount = fm.minLXCount.value;
	var maxLXCount = fm.maxLXCount.value;
	var minFinishRate = fm.minFinishRate.value;
	var maxFinishRate = fm.maxFinishRate.value;
	
	var strSQL =" select "
				+" (select name from ldcom where comcode = temp.ManageCom) 管理机构,"
				+" ldc.codename 应立项项目数,"
				+" sum(LRCount) 分公司录入中,"
				+" sum(FGSCount) 分公司审核中,"
				+" sum(ZGSCount) 总公司审核中,"
				+" sum(LXCount) 已立项项目数,"
				+" case when decimal(round(sum(LXCount)/double(ldc.codename)*100,2), 10, 2) =0 then '0%'"
				+" else decimal(round(sum(LXCount)/double(ldc.codename)*100,2), 10, 2) || '%' end 立项完成率"
				+" from "
				+" ("
				+" select "
				+" substr(managecom,1,4) ManageCom,"
				+" case when state in ('01','04','06') then 1 else 0 end LRCount,"
				+" case when state = '02' then 1 else 0 end FGSCount,"
				+" case when state = '03' then 1 else 0 end ZGSCount,"
				+" case when state = '05' then 1 else 0 end LXCount"
				+" from db2inst1.lcprojectinfo "
				+" ) as temp"
				+" inner join LDCode ldc on ldc.codetype = 'sprojectcount' and temp.ManageCom = ldc.code"
				+" where temp.ManageCom like '"+comcode+"%'"
				+" group by temp.ManageCom,ldc.codename having 1=1";
	
				if(minYLCount != ''&& minYLCount != null){
					if(!isInteger(minYLCount)){
						alert("应立项目数不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and  ldc.codename >= "+minYLCount
					}
				}
				if(maxYLCount != ''&& maxYLCount != null){
					if(!isInteger(maxYLCount)){
						alert("应立项目数不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and  ldc.codename <= "+maxYLCount
					}
				}
				if(minLRCount != ''&& minLRCount != null){
					if(!isInteger(minLRCount)){
						alert("分公司录入中不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and sum(LRCount) >= "+minLRCount
					}
				}
				if(maxLRCount != ''&& maxLRCount != null){
					if(!isInteger(maxLRCount)){
						alert("分公司录入中不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and sum(LRCount) <= "+maxLRCount
					}
				}
				if(minFGSCount != ''&& minFGSCount != null){
					if(!isInteger(minFGSCount)){
						alert("分公司审核中不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and sum(FGSCount) >= "+minFGSCount
					}
				}	
				if(maxFGSCount != ''&& maxFGSCount != null){
					if(!isInteger(maxFGSCount)){
						alert("分公司审核中不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and sum(FGSCount) <= "+maxFGSCount
					}
				}
				if(minZGSCount != ''&& minZGSCount != null){
					if(!isInteger(minZGSCount)){
						alert("总公司审核中不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and sum(ZGSCount) >= "+minZGSCount
					}
				}
				if(maxZGSCount != ''&& maxZGSCount != null){
					if(!isInteger(maxZGSCount)){
						alert("总公司审核中不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and sum(ZGSCount) <= "+maxZGSCount
					}
				}
				if(minLXCount != ''&& minLXCount != null){
					if(!isInteger(minLXCount)){
						alert("已立项项目数不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and sum(LXCount)  >= "+minLXCount
					}
				}
				if(maxLXCount != ''&& maxLXCount != null){
					if(!isInteger(maxLXCount)){
						alert("已立项项目数不是整数，请重新填写！");
						return;
					}else{
						strSQL += " and sum(LXCount)  <= "+maxLXCount
					}
				}
				
				if(minFinishRate != ''&& minFinishRate != null){
					if(!isNumeric(minFinishRate)){
						alert("立项完成率不是数字，请重新填写！");
						return;
					}else{
						strSQL += " and decimal(round(sum(LXCount)/double(ldc.codename)*100,2), 10, 2) >= "+minFinishRate
					}
				}
				if(maxFinishRate != ''&& maxFinishRate != null){
					if(!isNumeric(maxFinishRate)){
						alert("立项完成率不是数字，请重新填写！");
						return;
					}else{
						strSQL += " and decimal(round(sum(LXCount)/double(ldc.codename)*100,2), 10, 2) <= "+maxFinishRate
					}
				}
				strSQL += " with ur";
	turnPage1.queryModal(strSQL, ProjectGrid);
	showCodeName();	//显示编码代表的汉字
	fm.querySql.value = strSQL;
}

