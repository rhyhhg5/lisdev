<%
//程序名称：
//程序功能：
//创建日期：2013-4-16
//创建人  ：WS
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
 <%
  GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>                         
<script language="JavaScript">
var ManageCom = "<%=tGI.ManageCom%>"; 
function initForm()
{
    try
    {
        //initInputBox();
        initComCodeAndIpGrid();
        //initElementtype();
       initLXForm();
        
    }
    catch(e)
    {
        alert("网页初始化失败！");
    }
}

var ComCodeAndIpGrid;
function initComCodeAndIpGrid() 
{                               
  var iArray = new Array();
  try 
  {
    iArray[0]=new Array();
    iArray[0][0]="序号";         		
    iArray[0][1]="30px";         		
    iArray[0][3]=0;         		
    iArray[0][4]="station";         	
    
    iArray[1]=new Array();
    iArray[1][0]="机构编码";         	  
    iArray[1][1]="150px";            	
    iArray[1][2]=200;            			
    iArray[1][3]=0;              			
    
    iArray[2]=new Array();
    iArray[2][0]="应立项目数";         	
    iArray[2][1]="150px";            	
    iArray[2][2]=200;            		 
    iArray[2][3]=0;              		 
    
    ComCodeAndIpGrid = new MulLineEnter("fm", "ComCodeAndIpGrid"); 
    //初始化
    ComCodeAndIpGrid.mulLineCount = 0;
    ComCodeAndIpGrid.displayTitle = 1;
    ComCodeAndIpGrid.locked = 1;
    ComCodeAndIpGrid.canSel = 1;
    ComCodeAndIpGrid.canChk = 0;
    ComCodeAndIpGrid.hiddenSubtraction = 1;
    ComCodeAndIpGrid.hiddenPlus = 1;
    ComCodeAndIpGrid.loadMulLine(iArray);
    ComCodeAndIpGrid.selBoxEventFuncName = "onclkSelBox";
  }
  catch(ex) 
  {
    alert(ex);
  }
}

function initLXForm(){
	var getLXCountSQL = "select code,codeName from ldcode where codetype = 'sprojectcount'  with ur";
	var result = easyExecSql(getLXCountSQL);
	
	var strSqlTemp=easyQueryVer3(getLXCountSQL, 1, 0, 1); 
    turnPage.strQueryResult=strSqlTemp;
    if(!turnPage.strQueryResult)
    {
       window.alert("没有查询记录!");
       ComCodeAndIpGrid.clearData();
       return false;
    }
    else
    {
    	turnPage.queryModal(getLXCountSQL, ComCodeAndIpGrid);
    }
}
</script>