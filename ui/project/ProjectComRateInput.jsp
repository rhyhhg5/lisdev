<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
	<%
		//程序名称：
		//程序功能：
		//创建日期：2005-02-22 17:32:48
		//创建人  ：CrtHtml程序创建
		//更新记录：  更新人    更新日期     更新原因/内容
	%>
	<%
	String tProjectNo = request.getParameter("ProjectNo");
	 %>
	<%@page contentType="text/html;charset=GBK"%>
	<head>
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>

		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>

		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
		<script src="ProjectComRateInput.js"></script>
		<%@include file="ProjectComRateInputInit.jsp"%>

		<script>
  		var ManageCom = "<%=tGI.ManageCom%>";
  		var tProjectNo = "<%=tProjectNo%>"; 
  		</script>

	</head>
	<body onload="initForm();initElementtype();">
		<form action="./ProjectComRateInputSave.jsp" method=post name=fm target="fraSubmit">
			<table>
				<tr>
					<td class=titleImg>
						项目信息
					</td>
				</tr>
			</table>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						项目编号
					</TD>
					<TD class=input>
						<Input class='common' name="ProjectNo" readonly>
					</TD>
					<TD class=title>
						管理机构
					</TD>
					<TD class=input>
						<input class="codeNo" name="ManageCom" readonly><Input class="codeName" name="ManageComName" readonly>
					</TD>
					<TD class=title>
						建项日期
					</TD>
					<TD class=input>
						<input class="common" name="CreateDate" readonly>
					</TD>
				</TR>
				<TR class=common>
					<TD class=title>
						项目名称
					</TD>
					<TD class=input>
						<Input class='common' name="ProjectName" readonly>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>

			<table>
				<tr>
					<td class=titleImg>
						年度信息
					</td>
				</tr>
			</table>

			<Div id="divProjectAnnual1" style="display:''">
				<table class=common>
					<tr class=common>
						<td text-align:left colSpan=1>
							<span id="spanProjectYearGrid"> </span>
						</td>
					</tr>
				</table>
			</div>
			<Div id="divPage" align=center style="display: 'none' ">
				<table>
					<tr align=center>
						<td class=button width="10%">
							<INPUT CLASS=cssButton VALUE="首  页" TYPE=button onclick="turnPage1.firstPage();">
							<INPUT CLASS=cssButton VALUE="上一页" TYPE=button onclick="turnPage1.previousPage();">
							<INPUT CLASS=cssButton VALUE="下一页" TYPE=button onclick="turnPage1.nextPage();">
							<INPUT CLASS=cssButton VALUE="尾  页" TYPE=button onclick="turnPage1.lastPage();">
						</td>
					</tr>
				</table>
			</Div>
			<table>
				<tr>
					<td class=titleImg>
						综合赔付率
					</td>
				</tr>
			</table>
			<table class=common align='center'>
				<TR class=common>
					<TD class=title>
						综合赔付率
					</TD>
					<TD class=input>
						<Input type="text" class="common" name=ComplexRate elementtype="nacessary" verify="综合赔付率|notnull">
					</TD>
					<TD class=title>
					</TD>
					<TD class=input>
					</TD>
					<TD class=title>

					</TD>
					<TD class=input>
					</TD>
				</TR>
			</table>
			<table>
				<input type=hidden id="fmtransact" name="fmtransact">
				<input type=hidden id="ProjectYear" name="ProjectYear">

				<td class=button>
					<input type="button" class=cssButton value=" 保  存 " name=insert onclick="submitForm()">
				</td>
				<td>
					<input type="button" class=cssButton value="查看结余返还条款和保费回补条款" onclick="queryTerm()">
				</td>
				<td class=button>
					<input type="button" class=cssButton value=" 返  回 " onclick="cancel()">
				</td>
			</table>
			<span id="spanCode"	style="display: none; position:absolute; slategray"></span>
		</form>
	</body>
</html>
