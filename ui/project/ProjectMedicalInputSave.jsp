<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	//输入参数
	ProjectFileManageUI mProjectFileManageUI = new ProjectFileManageUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");
	
	String radio[] = request.getParameterValues("InpProjectMedicalGridSel");
	String FileNo[] = request.getParameterValues("ProjectMedicalGrid1");
	String tFileNo = "";
	for (int index = 0; index < radio.length; index++) {
		if (radio[index].equals("1")) {
			tFileNo = FileNo[index];
		}
	}

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = "DELETE||MAIN";
	System.out.println(transact);

	System.out.println("项目编号:" + mProjectNo+"; 文件编号："+tFileNo);
	if (mProjectNo == null || "".equals(mProjectNo)) {
		Content = "处理失败，原因是:获取项目编号失败！";
		FlagStr = "Fail";
	} 
	if (tFileNo == null || "".equals(tFileNo)) {
		Content = "处理失败，原因是:获取文件编号失败！";
		FlagStr = "Fail";
	}
	if(!"Fail".equals(FlagStr)){
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("ProjectNo", mProjectNo);
		transferData.setNameAndValue("FileNo", tFileNo);
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		try {
			mProjectFileManageUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = mProjectFileManageUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 保存成功! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	} 
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProjectNo%>");
</script>
</html>