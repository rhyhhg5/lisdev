<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
	//程序名称：
	//程序功能：
	//创建日期：
	//创建人  ：
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.projecttb.*"%>

<%
	//输入参数
	ProjectPremUWUI mProjectPremUWUI = new ProjectPremUWUI();

	//输出参数
	CErrors tError = null;
	String tOperate = "";
	String transact = "";
	String FlagStr = "";
	String Content = "";
	String mProjectNo = request.getParameter("ProjectNo");
	String mSendDate = request.getParameter("SendDate");
	String mSendOperator = request.getParameter("SendOperator");
	String mAuditOpinion = request.getParameter("AuditOpinion");
	String mMonthFeeNo[] = request.getParameterValues("ProjectMonthGrid8");

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String strManageCom = tG.ComCode;
	String strInput = "";

	//执行动作：insert 添加纪录 delete 删除记录 
	transact = request.getParameter("fmtransact");
	System.out.println(transact);

	System.out.println("项目编号:" + mProjectNo);
	if (mProjectNo == null || "".equals(mProjectNo)) {
		Content = "保存失败，原因是:获取项目编号失败！";
		FlagStr = "Fail";
	} else {
		LCProjectMonthInfoSet tLCProjectMonthInfoSet = new LCProjectMonthInfoSet();
		int tMonthFeeNoCount = mMonthFeeNo.length;
		for(int i = 0; i < tMonthFeeNoCount; i++){
			LCProjectMonthInfoSchema tLCProjectMonthInfoSchema = new LCProjectMonthInfoSchema();
			tLCProjectMonthInfoSchema.setMonthFeeNo(mMonthFeeNo[i]);
			tLCProjectMonthInfoSet.add(tLCProjectMonthInfoSchema);
		}
		TransferData transferData = new TransferData();
		transferData.setNameAndValue("ProjectNo", mProjectNo);
		transferData.setNameAndValue("SendDate", mSendDate);
		transferData.setNameAndValue("SendOperator", mSendOperator);
		transferData.setNameAndValue("AuditOpinion", mAuditOpinion);
		// 准备向后台传输数据 VData
		VData tVData = new VData();
		FlagStr = "";
		tVData.add(tG);
		tVData.add(transferData);
		tVData.add(tLCProjectMonthInfoSet);
		try {
			mProjectPremUWUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "审核失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
		if (!FlagStr.equals("Fail")) {
			tError = mProjectPremUWUI.mErrors;
			if (!tError.needDealError()) {
				Content = " 审核完成! ";
				FlagStr = "Succ";
			} else {
				Content = " 保存失败，原因是:" + tError.getFirstError();
				FlagStr = "Fail";
			}
		}
	}
%>
<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>","<%=mProjectNo%>");
</script>
</html>