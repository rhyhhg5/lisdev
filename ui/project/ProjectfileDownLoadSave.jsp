<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="java.net.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<!--%

  //特别提醒：
  //本文件使用的下载方法RequestDispatcher支持WebSphere，
  //但在tomcat上不能下载，还没找到更好的解决方法，敬请注意
  String fileNameDownload = request.getParameter("FilePath");
  fileNameDownload = fileNameDownload.substring(fileNameDownload.lastIndexOf("/temp/upload") + 1);
  
  System.out.println("\n\n\n\n\n\n\n\n\n\n\n\nfileNameDownload:" + fileNameDownload);
  
  String fileNameDisplay = fileNameDownload.substring(fileNameDownload.lastIndexOf("/") + 1);//下载文件时显示的文件保存名称
  System.out.println(fileNameDisplay);
  
  String fileNameDisplayUTF8 = URLEncoder.encode(fileNameDisplay,"UTF-8");
  System.out.println(fileNameDisplayUTF8);
  
  response.setContentType("application/x-download");//设置为下载application/x-download
  response.addHeader("Content-Disposition","attachment;filename=" + fileNameDisplayUTF8);
  
  try
  {
    RequestDispatcher dis = application.getRequestDispatcher(fileNameDownload);
    if(dis!= null)
    {
        dis.forward(request,response);
    }
    response.flushBuffer();
  }
  catch(Exception e)
  {
      e.printStackTrace();
  }
  finally
  {
  
  }
%-->
<%
	GlobalInput globalInput = new GlobalInput();
	globalInput = (GlobalInput) session.getValue("GI");
	String Content = "";

	String contextPath = request.getContextPath();
	System.out.println(contextPath);
	String serverPath = request.getScheme() + "://"+ request.getServerName() + ":" + request.getServerPort()+ contextPath + "/";
	System.out.println(serverPath);
	
	String pathInfo = "temp/qyproject";

	String radio[] = request.getParameterValues("InpProjectMedicalGridSel");
	String FileNo[] = request.getParameterValues("ProjectMedicalGrid1");
	String FileName[] = request.getParameterValues("ProjectMedicalGrid2");
	String tFileNo = "";
	String tFileName = "";
	for (int index = 0; index < radio.length; index++) {
		if (radio[index].equals("1")) {
			tFileNo = FileNo[index];
			tFileName = FileName[index];
		}
	}
	String fileformat = tFileName.substring(tFileName.lastIndexOf(".") + 1);
	String saveFileName = tFileNo + "." + fileformat;
	System.out.println("file path : " + pathInfo + " file name : "+ saveFileName);
	serverPath = application.getRealPath("").replace('\\', '/') + '/';
	String filePath = serverPath + pathInfo + "/" + saveFileName;
	System.out.println("file path : " + filePath);

	BufferedOutputStream output = null;
	BufferedInputStream input = null;
	try {
		//filePath = new String(filePath.getBytes("ISO-8859-1"),"gbk");

		File file = new File(filePath);

		response.reset();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition","attachment; filename="+ URLEncoder.encode(tFileName, "UTF-8"));
		//response.setHeader("Content-Disposition","attachment; filename="+tFileName );
		response.setContentLength((int) file.length());

		byte[] buffer = new byte[4096];
		//写缓冲区
		output = new BufferedOutputStream(response.getOutputStream());
		input = new BufferedInputStream(new FileInputStream(file));

		int len = 0;
		while ((len = input.read(buffer)) > 0) {
			output.write(buffer, 0, len);
		}
		Content = "下载成功！";
		out.clear();
		out = pageContext.pushBody();

		input.close();
		output.close();
	} catch (FileNotFoundException fe) {

		Content = "没有要下载的文件，请确认文件是否存在！";
	} catch (Exception e) {
		//e.printStackTrace();
		System.out.println("下载失败！");

		Content = "下载失败";
	} finally {
		if (input != null)
			input.close();
		if (output != null)
			output.close();
	}
%>

<html>
	<script language="javascript">
	parent.fraInterface.afterSubmit("<%=Content%>");
</script>
</html>

