<%
  //程序名称：FameworkOnDoubleClick.jsp
  //程序功能：核心框架CCodeOperate.js相关方法例子
  //创建日期：2016-11-15
  //创建人  ：yangyang
  //更新人  ：
%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
      GlobalInput tGI = new GlobalInput();
      tGI = (GlobalInput)session.getValue("GI");
%>
<script type="">
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构

</script>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
		<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
		<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
		<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
		<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
		<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
		<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
		<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
		<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
</head>
<body onload="initElementtype()">
<form>
<table>
   <tr>
    <td class=common><IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divCss);"></td>
    <td class=titleImg>双击下拉例子 </td>    
   </tr>
  </table>
  <div id="divCss" style="display:''">
<table class= common>
	<tr class= common>
		<td class='title'>性别</td>
		<td class='input'><input class ='codeno' name ='sex' CodeData="0|^1|男^2|女^3|不详"
			ondblclick="return showCodeListEx('sex',[this,sexName],[0,1]);"
			onkeyup="return showCodeListKeyEx('sex',[this,sexName],[0,1]);"
		><input	name ='sexName' class='codename'></td>
		<td>添加虚拟配置：CodeData="0|^1|男^2|女^3|不详"</td>
	</tr>
	<tr>
		<td class='title'>管理机构</td>
		<td class='input'><input class ='codeno' name ='managecom' verify="管理机构|code:comcode"
			ondblclick="return showCodeList('comcode',[this,managecomName],[0,1],null,null,null,1);"
			onkeyup="return showCodeListKey('comcode',[this,managecomName],[0,1],null,null,null,1);"
		><input		name ='managecomName' class='codename'></td>
		<td>添加校验：verify="管理机构|code:comcode"，用来校验录入的是否是下拉中的管理机构</td>
	</tr>
</table>
</div>
<input type ='button' class ='cssbutton' value='双击校验(code:校验用法)' onclick ="checkcode();">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span> 
</body>
<hr>
<font color='red'>
双击下拉说明 ：<br>
</font>
分为两类：<br>
一、虚拟数据源；二、真实数据源<br>
具体的用法 ，可以参考：CodeSelect代码引用功能说明文档.doc<br>
<font color ='red' style="size: 20px">
注意事项：
	如果页面中有双击下拉，则需要添加：&ltspan id="spanCode"  style="display: none; position:absolute; slategray"&gt&lt/span&gt 
	<br>在body里面最后一行添加 ，即&lt/body&gt前加载
	<br>afterCodeSelect用法：在双击下拉后，如果想添加事件或处理，可以在这里面写。
	<br>例子：
	<br>function afterCodeSelect(codename,codevalue)
<br>{
<br>	if(codename=="<font style="font-size: 20px">comcode</font>")//代码必须大小写与之前的一样
<br>	{
<br>		alert("点击 的是管理机构");
<br>	}
<br>	if(codename=="sex")
<br>	{
<br>		alert("点击 的是性别");
<br>	}
<br>}
	
</font>

<script type="">
	function checkcode()
{
	if(verifyInput()== false)
	{
		return false;
	}
}
function afterCodeSelect(codename,codevalue)
{
	if(codename=="comcode")
	{
		alert("点击 的是管理机构");
	}
	if(codename=="sex")
	{
		alert("点击 的是性别");
	}
}
	

</script>
</html>