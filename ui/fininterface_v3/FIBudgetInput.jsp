<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FIBusFeeTypeDefInput.jsp
 //程序功能：业务交易费用定义
 //创建日期：2011/8/25
 //创建人  ： 董健
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <title>现金流量规则查询</title>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIBudget.js"></SCRIPT>
  <%@include file="FIBudgetInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();">
<form  method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >业务类型</td>
  </tr>
</table>
<Div  id= "list" style= "display: ''" align=center>
	<table  class= common>
	   <tr  class= common>
	      <td text-align: left colSpan=1>
	     <span id="spanBudgetBusGrid" >
	     </span> 
	      </td>
	   </tr>
	</table>
	<input class=cssbutton value="首  页" type=button onclick="turnPage.firstPage();">
	<input class=cssbutton value="上一页" type=button onclick="turnPage.previousPage();">
	<input class=cssbutton value="下一页" type=button onclick="turnPage.nextPage();">
	<input class=cssbutton value="尾  页" type=button onclick="turnPage.lastPage();">
</Div>
<font color='red'>翻页时请不要选中任一条记录</font>
<hr></hr>
<table>
  <tr>
    <td class="titleImg" >现金流量</td>
  </tr>
</table>
<Div  id= "list2" style= "display: ''" align=center>
	<table  class= common>
	   <tr  class= common>
	      <td text-align: left colSpan=1>
	     <span id="spanBudgetGrid" >
	     </span> 
	      </td>
	   </tr>
	</table>
	<input class=cssbutton value="首  页" type=button onclick="turnPage1.firstPage();"> 
	<input class=cssbutton value="上一页" type=button onclick="turnPage1.previousPage();">      
	<input class=cssbutton value="下一页" type=button onclick="turnPage1.nextPage();"> 
	<input class=cssbutton value="尾  页" type=button onclick="turnPage1.lastPage();">
</Div>


<br>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
