<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FIBnInfoDefInput.jsp
 //程序功能：业务交易信息定义
 //创建日期：2011/8/25
 //创建人  ：董健
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
<title>业务信息录入</title>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIBusInfoGD.js"></SCRIPT>
  <%@include file="FIBusInfoGDInit.jsp"%>
  
</head>
<body  onload="initForm();initElementtype();">
<form action="./FIBusInfoDefSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >业务信息列表</td>
  </tr>
</table>
<Div  id= "list" style= "display: ''" align=center>
	<table  class= common>
	   <tr  class= common>
	      <td text-align: left colSpan=1>
	     <span id="spanBusTypeInfoGrid" >
	     </span> 
	      </td>
	   </tr>
	</table>
	<input class=cssbutton value="首  页"  type=button onclick="turnPage.firstPage();"> 
	<input class=cssbutton value="上一页" type=button onclick="turnPage.previousPage();">      
	<input class=cssbutton value="下一页" type=button onclick="turnPage.nextPage();"> 
	<input class=cssbutton value="尾  页"  type=button onclick="turnPage.lastPage();">
</Div>	
<br>
<hr>
<table>
  <tr>
    <td class="titleImg" >业务信息录入</td>
  </tr>
</table>
<table  class= common>
	<tr class="common">
	    <td class="title">信息编码</td>
	    <td class="input" >
	    	<input class="codeno" name="FIBusInfoID" ondblclick="return showCodeList('fibusinfo',[this,FIBusInfoIDName],[0,1]);" 
	    		onkeyup="return showCodeListKey('fibusinfo',[this,FIBusInfoIDName],[0,1]);" readonly = "true"><input class="codename" name="FIBusInfoIDName" readonly = "true" >
	    </td> 	
		<td class="title">信息描述</td>
		<TD class=input>
			 <Input class= common name=FIBusRemark value = '' >
		</TD><!--
		<td class="title">信息用途</td>
	    <td class="input" >
	    	<input class="codeno" name="PurPose" ondblclick="return showCodeList('fiusage',[this,PurPoseName],[0,1]);" 
	    		onkeyup="return showCodeListKey('fiusage',[this,PurPoseName],[0,1]);" readonly = "true"><input class="codename" name="PurPoseName" readonly = "true" >
	    </td> 		    
	--></tr>
	<tr class=common>
	    <td class=title>值域</td>
	    <td class=input><input class=common name=FeeValues></td>
	    <TD class= common >
	   	   <INPUT VALUE="追  加" TYPE=button class= cssbutton name="superaddbutton" onclick="return superaddClick();">
	   	   <INPUT VALUE="清  空" TYPE=button class= cssbutton name="clearbutton" onclick="return clearClick();">
	    </TD>
	    <TD class= common align=left>
	     	 <Input class=codeno name=FeeValuesB  ondblclick="return showCodeList('fibusvalues',[this,FeeValuesBName],[0,1],null,'1  and attno = #'+fm.FIBusInfoID.value+'#','1');" onkeyup=" return showCodeListKey('fibusvalues',[this,FeeValuesBName],[0,1],null,'1 and attno = #'+fm.FIBusInfoID.value+'#','1');"><input class=codename name=FeeValuesBName readonly=true >  
	    </TD>
	</tr>
</table>
<br>

<input value="添 加 "  onclick="addClick()" class="cssButton" type="button" >
<input value="删 除"   onclick="deleteClick()" class="cssButton" type="button" >

<br>
<input type=hidden name=pageflag >
<input type=hidden name=maintno >
<input type=hidden name=BusTypeId >
<input type=hidden name=PurPose value='02'>
<input type=hidden name=transact ><!-- 执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录-->
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
