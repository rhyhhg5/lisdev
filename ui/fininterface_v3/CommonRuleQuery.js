//               该文件中包含客户端需要处理的函数和事件
//该版本查询用于查询版本的全部信息
var showInfo;
var mDebug="1";
var arrDataSet;
var turnPage = new turnPageClass();

//返回按钮对应的操作
function returnParent()
{
  var arrReturn = new Array();
	var tSel = CommonRuleGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请您先选择一条记录，再点击返回按钮。" );
	else
	{

			try
			{
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				//alert( "没有发现父窗口的afterQuery接口。" + ex );
				alert(ex.message);
			}
			top.close();

	}
}



function getQueryResult()
{
	var arrSelected = null;
	tRow = CommonRuleGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	{
	  return arrSelected;
	}
	arrSelected = new Array();
	//tRow = 10 * turnPage.pageIndex + tRow; //10代表multiline的行数
	//设置需要返回的数组
	//arrSelected[0] = turnPage.arrDataCacheSet[tRow-1];
		// 书写SQL语句
	var strSQL = "";
	strSQL = "select VersionNo,RuleID,RuleName,(select code||'-'||codename from ldcode where codetype = 'firuletype' and code=ruletype),"
		 + " subruletype,rulereturn,returnremark,(select code||'-'||codename from ldcode where codetype='rulestate' and code=rulestate), "
		 + " (select code||'-'||codename from ldcode where codetype = 'ruledealmode' and code=ruledealmode ),ruledealmode,"
		 + " ruledealclass,ruledealsql,rulefilename"
		 + " from FICommonRules  where RuleID ='"+CommonRuleGrid.getRowColData(tRow-1,2)+"' ";
		 
    //alert(strSQL);
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("查询失败！");
    return false;
    }  
	//查询成功则拆分字符串，返回二维数组
  arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	return arrSelected;
}



// 查询按钮对应的操作
function easyQueryClick()
{
	// 初始化表格
	initCommonRuleGrid();
	//fm.all('BranchType').value = top.opener.fm.BranchType.value;
	// 书写SQL语句
	var strSQL = "";
	//var tReturn = parseManageComLimitlike();
	strSQL = "select VersionNo,RuleID,RuleName,(select codename from ldcode where codetype = 'firuletype' and code=ruletype)," +
	"subruletype,(select codename from ldcode where codetype = 'ruledealmode' and code=ruledealmode ),(select codename from ldcode where codetype='rulestate' and code=rulestate) from FICommonRules where ruletype='02' "
     + getWherePart('VersionNo','VersionNo','like')
     + getWherePart('RuleID','RuleID')
     + getWherePart('RuleName','RuleName','like')
     + getWherePart('RuleDealMode','RuleDealMode')
     + getWherePart('RuleState','RuleState')
     +" order by RuleID";
	turnPage.queryModal(strSQL,CommonRuleGrid);
}


//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}