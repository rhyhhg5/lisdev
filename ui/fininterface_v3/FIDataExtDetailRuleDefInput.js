//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mOperate="";
var turnPage = new turnPageClass();
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null) 
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//初始化修改页面
function initmaintpage()
{

}
//初始化普通页面
function initpage()
{
	initBusTypeInfoGrid();
	tsql="select ArithmeticID,ArithmeticName,RunObject,DealMode,ReturnRemark,VersionNo "
	+ "from FIDataExtractRules where VersionNo = '"+VersionNo+"' and PrArithmeticID ='"+RuleID+"' and ArithmeticType = '00' and SubType = '01'";
	
	turnPage.queryModal(tsql,BusTypeInfoGrid);
}

//进入明细补充规则
function intoDetailAddOn()
{
	//插入条件
	var tRow = BusTypeInfoGrid.getSelNo();
	if(tRow == "")
	{
		alert("请先选择一条明细规则！");
		return false;
	}
	showInfo=window.open("./FrameFIDataRuleExtMainAddOnDefInput.jsp?VersionNo="+fm.VersionNo.value+"&RuleID="+BusTypeInfoGrid.getRowColData(tRow-1,1)+"&RuleLevel=03&pageflag=C");  
}


function addClick()
{
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	//为了防止双击，点击增加后，屏蔽"增加"按钮
	mOperate="INSERT||MAIN";
	submitForm();
}


//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
	if ((fm.all("FinItemID").value==null)||(trim(fm.all("FinItemID").value)==''))
		alert("请确定要删除的规则编号！");
  
	else
	{

		if (confirm("您确实想删除该记录吗?"))
		{
			mOperate="DELETE||MAIN";
			submitForm();
		}
		else
		{
			mOperate="";
			alert("您取消了删除操作！");
		}
	}
}


function updateClick()
{
	//提交前的检验
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	else
	{
		if (confirm("您确实想修改该记录吗?"))
		{
			mOperate="UPDATE||MAIN";
			submitForm();
		}
		else
		{
			mOperate="";
			alert("您取消了修改操作！");
		}
	}
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
	  initForm();
  }
  catch(re)
  {
  	alert("FinItemDefInput.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}


//取消按钮对应操作
function cancelForm()
{
//  window.location="../common/html/Blank.html";
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}



function submitForm()
{

	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.hideOperate.value=mOperate;
	if (fm.hideOperate.value=="")
	{
	  alert("操作控制数据丢失！");
	}
	fm.PrArithmeticID.value = RuleID;
	fm.action="./DataExtMainRuleDefSave.jsp";
	//lockButton(); 
	fm.submit(); //提交

}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  //释放“增加”按钮

  if (FlagStr == "Fail" )
  {

  		
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;

    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //resetForm();
		mOperate="";
  }
  else
  {
    //alert(content);
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
  	//parent.fraInterface.initForm();
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

//    showDiv(operateButton,"true");
//    showDiv(inputButton,"false");
    //执行下一步操作
    //resetForm();
    if(mOperate=="DELETE||MAIN")
    {

    }
    mOperate="";
  }

}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


//提交前的校验、计算
function beforeSubmit()
{
	if((fm.VersionNo.value=="")||(fm.VersionNo.value=="null"))
	{
		alert("请查询获取版本编号！");
		return ;
	}

	if((fm.ArithmeticID.value=="")||(fm.ArithmeticID.value=="null"))
	{
		alert("请您录入规则编号！");
		return ;
	}
  
	if((fm.ArithmeticName.value=="")||(fm.ArithmeticName.value=="null"))
	{
		alert("请您录入规则名称！");
		return ;
	}

	if((fm.BuType.value=="")||(fm.BuType.value=="null"))
	{
		alert("请您录入规则类型！");
		return ;
	}
  
	if((fm.RunObject.value=="")||(fm.RunObject.value=="null"))
	{
		alert("请您录入规则对象！");
		return ;
	}
  
	if((fm.DealMode.value=="")||(fm.DealMode.value=="null"))
	{
		alert("请您录入处理方式！");
		return ;
	}
	
	if((fm.State.value=="")||(fm.State.value=="null"))
	{
		alert("请您录入规则状态！");
		return ;
	}
  
	if (!verifyInput2()) 
	{
		return false;
	}

    return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//代码选择后处理
function afterCodeSelect( cName, Filed)
{   
	if(fm.DealMode.value=='1')
	{
		 classdiv.style.display='';
		 sqldiv.style.display='none';
		 filediv.style.display='none';
		 fm.all('MainSQL').value='';
	}
			
	if(fm.DealMode.value=='2')
	{
		 classdiv.style.display='none';
		 sqldiv.style.display='';
		 filediv.style.display='none';
		 fm.all('DealClass').value='';
	}
	
	if(fm.DealMode.value=='3')
	{
		 filediv.style.display='';
		 sqldiv.style.display='none';
		 classdiv.style.display='none';
		 fm.all('FileName').value='';
	}	
}

//数据选择后动作
function dataselect()
{
	var tRow = BusTypeInfoGrid.getSelNo();
	
	querysql = "select ArithmeticID,ArithmeticName,RunObject,DealMode,DealClass,replace(MainSQL,'|','@'),FileName,ReturnRemark,State "
		+ "from FIDataExtractRules where VersionNo ='"+BusTypeInfoGrid.getRowColData(tRow-1,6)+"' and ArithmeticID ='"+BusTypeInfoGrid.getRowColData(tRow-1,1)+"'";
		
	turnPage.strQueryResult  = easyQueryVer3(querysql, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) 
	{
		alert("查询失败！");
		return false;
	}
	//查询成功则拆分字符串，返回二维数组
	arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	
	arrSelected[0][5]=arrSelected[0][5].split("@@").join("||");
	arrSelected[0][5]=arrSelected[0][5].split("@").join("|");
	
	fm.all('ArithmeticID').value = arrSelected[0][0];
	fm.all('ArithmeticName').value = arrSelected[0][1];
	fm.all('RunObject').value = arrSelected[0][2];
	fm.all('DealMode').value = arrSelected[0][3];
	fm.all('DealClass').value = arrSelected[0][4];
	fm.all('MainSQL').value = arrSelected[0][5];
	fm.all('FileName').value = arrSelected[0][6];
	fm.all('ReturnRemark').value = arrSelected[0][7];
	fm.all('State').value = arrSelected[0][8];
	if(fm.DealMode.value=='1')
	{
		 classdiv.style.display='';
		 sqldiv.style.display='none';
		 filediv.style.display='none';
		 fm.all('MainSQL').value='';
	}
			
	if(fm.DealMode.value=='2')
	{
		 classdiv.style.display='none';
		 sqldiv.style.display='';
		 filediv.style.display='none';
		 fm.all('DealClass').value='';
	}
	
	if(fm.DealMode.value=='3')
	{
		 filediv.style.display='';
		 sqldiv.style.display='none';
		 classdiv.style.display='none';
		 fm.all('FileName').value='';
	}	
	
//	showCodeName();
}