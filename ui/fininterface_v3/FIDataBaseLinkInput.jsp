<html>
<%
//程序名称 :FIDataBaseLinkInput.jsp
//程序功能 :数据接口配置管理
//创建人 :范昕
//创建日期 :2008-08-04
//
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 
<SCRIPT src = "FIDataBaseLinkInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FIDataBaseLinkInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
  <form action="./FIDataBaseLinkSave.jsp" method=post name=fm target="fraSubmit">
  
  <Div id= "divFIDataBaseLink" style= "display: ''">
		<table>
    	<tr>
    		 <td class= titleImg>
        		数据接口配置管理
       		 </td>   		 
    	</tr>
    </table>
   	<Table class= common>
		<TR  class= common>
			<TD  class= title>接口编码</TD>
			<TD  class= input><Input class=common name=InterfaceCode elementtype=nacessary ></TD>
			<TD class="title">操作员</TD>
      <TD class="input">
      <input class="readonly" readonly name="Operator" readonly=true ></TD>
		</TR>
		 <TR  class= common>
			<TD  class= title>接口名称</TD>
			<TD  class= input><Input class=common name=InterfaceName ></TD>
			<TD  class= title>数据库类型</TD>
			<TD  class= input><Input class=code name= DBType verify="数据库类型|NOTNULL"   CodeData="0|^ORACLE|ORACLE^DB2|DB2^INFORMIX|INFORMIX^SQLSERVER|SQLSERVER^WEBLOGICPOOL|WEBLOGICPOOL" ondblClick="showCodeListEx('DBType',[this],[0,1],null,null,null,[1]);" onkeyup="showCodeListKeyEx('DBType',[this],[0,1],null,null,null,[1]);" readonly=true></TD>
		</TR>
	</table>
 </div>
	<hr></hr>
	
<Div  id= "ORACLEdiv" style= "display: 'none'" align=left>
	<table class=common>
		<TR  class= common>
			<TD  class= title>IP</TD>
			<TD  class= input><Input class=common name=IP1 elementtype=nacessary ></TD>
			<TD  class= title>端口号</TD>
			<TD  class= input><Input class=common name=Port1 elementtype=nacessary ></TD>
		</TR>
		<tr class= common>
			<TD  class= title>数据库名称</TD>
			<TD  class= input><Input class=common name=DBName1 elementtype=nacessary ></TD>
			<TD  class= title>服务名称</TD>
			<TD  class= input><Input class=common name=ServerName1 ></TD>
		</TR>
		<TR  class= common>
			<TD  class= title>用户名</TD>
			<TD  class= input><Input class=common name=UserName1 elementtype=nacessary ></TD>
			<TD  class= title>密码</TD>
			<TD  class= input><Input class=common name=PassWord1 elementtype=nacessary ></TD>
		</TR>
	</table> 	  
</div>

<Div  id= "INFORMIXdiv" style= "display: 'none'" align=left>
	<table class=common>
  	<TR  class= common>
			<TD  class= title>IP</TD>
			<TD  class= input><Input class=common name=IP2 elementtype=nacessary ></TD>
			<TD  class= title>端口号</TD>
			<TD  class= input><Input class=common name=Port2 elementtype=nacessary ></TD>
		</TR>
		<TR  class= common>
			<TD  class= title>数据库名称</TD>
			<TD  class= input><Input class=common name=DBName2 elementtype=nacessary ></TD>
			<TD  class= title>服务名称</TD>
			<TD  class= input><Input class=common name=ServerName2 elementtype=nacessary ></TD>
		</TR>
		<TR  class= common>
			<TD  class= title>用户名</TD>
			<TD  class= input><Input class=common name=UserName2 elementtype=nacessary ></TD>
			<TD  class= title>密码</TD>
			<TD  class= input><Input class=common name=PassWord2 elementtype=nacessary ></TD>
		</TR>
 </table> 	  
</div>

	<Div  id= "SQLSERVERdiv" style= "display: 'none'" align=left>
	<table class=common>
		<TR  class= common>
			<TD  class= title>IP</TD>
			<TD  class= input><Input class=common name=IP3 elementtype=nacessary ></TD>
			<TD  class= title>端口号</TD>
			<TD  class= input><Input class=common name=Port3 elementtype=nacessary ></TD>
		</TR>
	 <tr class= common>
			<TD  class= title>数据库名称</TD>
			<TD  class= input><Input class=common name=DBName3 elementtype=nacessary ></TD>
			<TD  class= title>服务名称</TD>
			<TD  class= input><Input class=common name=ServerName3 ></TD>
		</TR>
		<TR  class= common>
			<TD  class= title>用户名</TD>
			<TD  class= input><Input class=common name=UserName3 elementtype=nacessary ></TD>
			<TD  class= title>密码</TD>
			<TD  class= input><Input class=common name=PassWord3 elementtype=nacessary ></TD>
		</TR>
 </table> 	  
</div>

<Div  id= "WEBLOGICPOOLdiv" style= "display: 'none'" align=left>
	<table class=common>
		<TR  class= common>
			<TD  class= title>IP</TD>
			<TD  class= input><Input class=common name=IP4 elementtype=nacessary ></TD>
			<TD  class= title>端口号</TD>
			<TD  class= input><Input class=common name=Port4 elementtype=nacessary ></TD>
		</TR>
		<tr class= common>
			<TD  class= title>数据库名称</TD>
			<TD  class= input><Input class=common name=DBName4 elementtype=nacessary ></TD>
			<TD  class= title>服务名称</TD>
			<TD  class= input><Input class=common name=ServerName4 ></TD>
		</TR>
		<TR  class= common>
			<TD  class= title>用户名</TD>
			<TD  class= input><Input class=common name=UserName4 ></TD>
			<TD  class= title>密码</TD>
			<TD  class= input><Input class=common name=PassWord4 ></TD>
		</TR>
	</table> 	  
</div>
   </Div>
   <input type=hidden id="OperateType" name="OperateType">
   <input type=hidden name=tManageCom value=''>
	 <INPUT class=cssButton name="addbutton" VALUE="增  加"  TYPE=button onclick="return submitForm();">
   <INPUT class=cssButton name="updatebutton" VALUE="修  改"  TYPE=button onclick="return updateClick();">
   <INPUT class=cssButton name="querybutton" VALUE="查  询"  TYPE=button onclick="return queryClick();">
   <INPUT class=cssButton name="deletebutton" VALUE="删  除"  TYPE=button onclick="return deleteClick();">
   <INPUT class=cssButton name="resetbutton" VALUE="重  置"  TYPE=button onclick="return resetAgain();">
 
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>