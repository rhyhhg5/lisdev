<%@page contentType="text/html;charset=GBK" %>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
 String CurrentDate = PubFun.getCurrentDate();
%>  
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>    
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
  <SCRIPT src="FIDataProcessInput.js"></SCRIPT>
  <%@include file="FIDataProcessInit.jsp"%>
  <title>二次数据处理</title>
</head>
<body  onload="initForm();initElementtype();" >
<form method=post name=fm action= "./FIDistillCertificateSave.jsp" target="fraSubmit">
   <table class= common border=0 width=100%>
     <tr>
	  <td class= titleImg align= center>请输入日期：</td>
     </tr>
   </table>
   <table  class= common align=center>
      	 
      <TR  class= common>
          <TD  class= title>
            起始日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"   elementtype = nacessary  name=Bdate  value = '<%=CurrentDate%>' >
          </TD>
          <TD  class= title>
            终止日期
          </TD>
          <TD  class= input>
            <Input class="coolDatePicker" dateFormat="short"   elementtype = nacessary  name=Edate  value = '<%=CurrentDate%>' >
          </TD>
        </TR>		    
    </table>
    <br>
    <INPUT  class=cssButton VALUE="数据查询" TYPE=Button onclick="QueryTask();">
    <hr>
    
      <table class= common border=0 width=100%>
     <tr>
	  <td class= titleImg align= center>查询结果</td>
     </tr>
   </table>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanResultGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
    	<Div  id= "Task" style= "display: ''" align=center>
      <br>
      <INPUT VALUE="首  页" class =  cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();"> 					
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();"> 
      <INPUT VALUE="尾  页" class =  cssButton TYPE=button onclick="getLastPage();"> 					
    </Div>
  <hr></hr>
   <INPUT  class=cssButton VALUE="数据处理" TYPE=Button onclick="DataProcess();">

</form>
</body>
</html>
