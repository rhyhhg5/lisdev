<%
//程序名称：FIDataBaseLinkInit.jsp
//程序功能：数据接口配置管理
//创建日期：2008-08-04
//创建人  ：范昕  
%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("网页超时，请重新登录");
		return;
	}
	String strOperator = globalInput.Operator;
%>                          
<script language="JavaScript">
function initInpBox()
{ 
  try
  {     
  	fm.reset();
  	fm.all('InterfaceCode').value = '';
	fm.Operator.value = '<%= strOperator %>';
  	fm.all('InterfaceName').value = '';
  	fm.all('DBType').value = '';
  	fm.all('IP1').value = '';
  	fm.all('Port1').value = '';
  	fm.all('DBName1').value = '';
  	fm.all('ServerName1').value = '';
  	fm.all('UserName1').value = '';
    fm.all('PassWord1').value = '';
    
    fm.all('IP2').value = '';
  	fm.all('Port2').value = '';
  	fm.all('DBName2').value = '';
  	fm.all('ServerName2').value = '';
  	fm.all('UserName2').value = '';
    fm.all('PassWord2').value = '';
    
    fm.all('IP3').value = '';
  	fm.all('Port3').value = '';
  	fm.all('DBName3').value = '';
  	fm.all('ServerName3').value = '';
  	fm.all('UserName3').value = '';
    fm.all('PassWord3').value = '';
    
    fm.all('IP4').value = '';
  	fm.all('Port4').value = '';
  	fm.all('DBName4').value = '';
  	fm.all('ServerName4').value = '';
  	fm.all('UserName4').value = '';
    fm.all('PassWord4').value = '';
  }
  catch(ex)
  {
    alert("在FIDataBaseLinkInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initForm()
{
  try
  {
      var comcode = "<%=globalInput.ComCode%>";
	  initInpBox();
  }
  catch(re)
  {
    alert("在FIDataBaseLinkInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
</script>
