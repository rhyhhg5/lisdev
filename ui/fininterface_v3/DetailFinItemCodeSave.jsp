<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DetailFinItemCodeSave.jsp
//程序功能：明细科目分支影射定义保存页面
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
	request.setCharacterEncoding("GBK");
	
	String pageflag = request.getParameter("pageflag");
	//输入参数
	FIDetailFinItemCodeSchema tFIDetailFinItemCodeSchema = new FIDetailFinItemCodeSchema();
	FMDetailFinItemCodeSchema tFMDetailFinItemCodeSchema = new FMDetailFinItemCodeSchema();
	
	FIDetailFinItemCodeUI tFIDetailFinItemCode = new FIDetailFinItemCodeUI();
	FMDetailFinItemCodeUI tFMDetailFinItemCodeUI = new FMDetailFinItemCodeUI();
	
	//输出参数
	CErrors tError = null;
	String tOperate=request.getParameter("hideOperate");
	tOperate=tOperate.trim();
	String mJudgementNo = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	//
	tFIDetailFinItemCodeSchema.setVersionNo(request.getParameter("VersionNo"));
	tFIDetailFinItemCodeSchema.setFinItemID(request.getParameter("FinItemID"));  
	tFIDetailFinItemCodeSchema.setJudgementNo(request.getParameter("JudgementNo"));
	tFIDetailFinItemCodeSchema.setLevelConditionValue(request.getParameter("LevelConditionValue"));
	tFIDetailFinItemCodeSchema.setLevelCode(request.getParameter("LevelCode"));
	tFIDetailFinItemCodeSchema.setNextJudgementNo(request.getParameter("NextJudgementNo"));
	tFIDetailFinItemCodeSchema.setReMark(request.getParameter("ReMark"));
	//
	tFMDetailFinItemCodeSchema.setMaintNo(request.getParameter("maintno"));
	tFMDetailFinItemCodeSchema.setVersionNo(request.getParameter("VersionNo"));
	tFMDetailFinItemCodeSchema.setFinItemID(request.getParameter("FinItemID"));  
	tFMDetailFinItemCodeSchema.setJudgementNo(request.getParameter("JudgementNo"));
	tFMDetailFinItemCodeSchema.setLevelConditionValue(request.getParameter("LevelConditionValue"));
	tFMDetailFinItemCodeSchema.setLevelCode(request.getParameter("LevelCode"));
	tFMDetailFinItemCodeSchema.setNextJudgementNo(request.getParameter("NextJudgementNo"));
	tFMDetailFinItemCodeSchema.setReMark(request.getParameter("ReMark"));

	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	
	tVData.add(tG);
	try
	{
		System.out.println("come========== in"+tFIDetailFinItemCodeSchema.getVersionNo());
		System.out.println("come========== in"+tFIDetailFinItemCodeSchema.getFinItemID());  
		System.out.println("come========== in"+tFIDetailFinItemCodeSchema.getJudgementNo()); 
		System.out.println("come========== in"+tFIDetailFinItemCodeSchema.getLevelConditionValue());  
		if("C".equals(pageflag))
		{
			tVData.addElement(tFIDetailFinItemCodeSchema);
			if(!tFIDetailFinItemCode.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFIDetailFinItemCode.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		else if("X".equals(pageflag))
		{
			tVData.addElement(tFMDetailFinItemCodeSchema);
			if(!tFMDetailFinItemCodeUI.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFMDetailFinItemCodeUI.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		
		System.out.println("come out" + tOperate);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail"))
	{
		tError = tFIDetailFinItemCode.mErrors;
		if (!tError.needDealError())
		{
			mJudgementNo = tFIDetailFinItemCode.getJudgementNo();
			Content = " 操作已成功! ";
			FlagStr = "Succ";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
</script>
</html>

