var mSwitch = parent.VD.gVSwitch;
var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var k = 0;



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{  
	try
	{
		showInfo.close();		
		if (FlagStr == "Fail" )
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
		else
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
		location.reload();
		
	}catch(ex){}
}

function rollBack()
{
	var tSel = RollBackGrid.getSelNo();
	if( tSel == 0 )
	{
		alert("请先选择一条记录，再进行回退操作！");
		return false;
	}
	if(fm.ProcessNode.value==""||fm.ProcessNode.value==null)
	{
	    alert("请选择回滚到的节点");
		return false;
	}
	var cProcessNode = RollBackGrid.getRowColData(tSel-1,8);

	if(cProcessNode<fm.ProcessNode.value&&fm.ProcessNode.value!='99')
	{
		alert("请选择当前节点之前要回滚的节点！");
		return false;
	}
	
    var showStr="正在提取数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");		
    
    fm.submit();
}
 
function ReturnData()
{
 
 	var tSel = RollBackGrid.getSelNo(); 	
	fm.all('batchno').value = RollBackGrid.getRowColData(tSel-1,1);
	fm.all('ErrAppNo').value = RollBackGrid.getRowColData(tSel-1,2);
	fm.all('DetailReMark').value = RollBackGrid.getRowColData(tSel-1,7);
 	
}
 
function easyQueryClick()
{ 
	// 书写SQL语句
	var strSQL = "";
  
	strSQL = "select b.batchno,b.errappno,(select codename from ldcode a where codetype = 'fiindexid' and code=b.indexcode fetch first 1 rows only),"+
   			" b.indexno,(select codename from ldcode where codetype = 'fieventnode' and code = b.CallPointID),'申请完毕',ErrReMark,CallPointID from FIErrHandingItem b where b.appstate='01' and b.DealType='02' "; 
				   		 
	turnPage.queryModal(strSQL, RollBackGrid);
	return true;
}

