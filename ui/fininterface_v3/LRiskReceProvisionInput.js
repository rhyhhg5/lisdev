//该文件中包含客户端需要处理的函数和事件

//程序名称：LRiskReceProvisionInput.js
//程序功能：长险应收计提数据提取
//创建日期：2015-8-18
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
var showInfo;
window.onfocus=myonfocus;


//提交前的校验、计算  
function beforeSubmit(){
	
	var Start = fm.all('StartDate').value;
	var End = fm.all('EndDate').value;
	if(Start == null||Start==''){
		alert("请输入开始日期！");
		return false;
	} 
	if(End == null||End==''){
		alert("请输入终止日期！");
		return false;
	}
	if(trim(Start.substring(8,10))!='01')
	{
	   alert("抽取起期必须为某月的第一天！");
	   return false;
	}
	if(trim(End.substring(8,10))!='01')
	{
	   alert("抽取止期必须为某月的第一天！");
	   return false;
	}
	//统计日期必须在同一月
	if(trim(Start.substring(5,7))!=trim(End.substring(5,7)))
	{
	  alert("抽取起期与抽取止期必须在同一月！");  
	  return false;
	}
	return true;
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
} 

function DataExt()
{	
	if(!beforeSubmit())
	 {
	   return false;
	 }
	fm.action="LRiskReceProvisionSave.jsp";
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.submit(); //提交
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  //释放“增加”按钮

  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;

    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
   
  }
  else
  {
    
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;

    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
}

