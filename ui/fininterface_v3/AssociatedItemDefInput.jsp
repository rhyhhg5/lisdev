 <html> 
 	<%
//创建日期：2011-08-11
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	String mStartDate = "";
	String mSubDate = "";
	String mEndDate = "";
    GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("1"+tGI.Operator);
	System.out.println("2"+tGI.ManageCom);
%>

<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 

<SCRIPT src="AssociatedItemDefInput.js"></SCRIPT>  
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="AssociatedItemDefInputInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >

<form name=fm   target=fraSubmit method=post>
<table>
	<tr>    		 
		 <td class= titleImg>
	   		 科目专项定义
	  	 </td>   		 
	</tr>
</table>
<div id=versionbutton>
<td class=button width="10%" align=right>
	<INPUT class=cssButton name="querybutton" VALUE="版本信息查询"  TYPE=button onclick="return queryClick1();">
</td>    
</div>
<table class= common border=0 width=100%>			  	
<tr class= common>       
	<TD class= title>版本编号</TD>
	<TD class=input>
 		<Input class= readonly name=VersionNo readonly=true>   					 	 
	</TD>

	<TD class= title>版本状态</TD>
	<TD class=input>
 		<Input class= readonly name=VersionState2 readonly=true>
	</TD>				 								
</tr>  
</table>      

<hr></hr>  
<div id=querybutton>
<td class=button width="10%" align=right>
	<INPUT class=cssButton name="querybutton" VALUE="科目专项定义查询"  TYPE=button onclick="return queryClick2();">
</td>
</div>
<table class= common border=0 width=100%>
	<tr class= common>
		<TD class= title>专项编号</TD>
		<TD class= input>
			<Input class=common name=AssociatedID elementtype=nacessary verify="专项编号|len<=15" >
		</TD>
		<TD class= title>专项名称</TD>
		<TD class=input>
			<Input class=common name=AssociatedName elementtype=nacessary verify="专项名称|len<=15" >
		</TD>		
	</tr>
	<tr class= common>
		<TD class= title>专项表字段标识</TD>
		<TD class= input>
			<Input class=codeno name=ColumnID verify="专项表字段标识|NOTNULL" ondblClick="showCodeList('ficolumnid',[this,columnidName],[0,1],null,null,null,1);" onkeyup="return showCodeListKey('ficolumnid',[this],[0,1],null,null,null,1);"><input class=codename name=columnidName readonly=true elementtype=nacessary>					
		</TD>	 
		<TD class= title>上游数据来源字段</TD>
		<TD class= input>
			<Input class=codeno name=SourceColumnID verify="上游数据来源字段|NOTNULL" ondblClick="showCodeList('fisourcecolumnid',[this,sourcecolumnName],[0,1],null,null,null);" onkeyup="showCodeListKey('fisourcecolumnid',[this,sourcecolumnName],[0,1],null, null,null);"><input class=codename name=sourcecolumnName readonly=true elementtype=nacessary>
		</TD>		
	</tr>
	<tr class= common>		
		<TD  class= title>转换标志</TD>
        <TD  class= input>
        	<Input class=codeno name=TransFlag verify="转换标志|NOTNULL" readonly=true  CodeData="0|^N|不转换^S|SQL转换^C|程序转换" ondblClick="showCodeListEx('TransFlag',[this,TransFlagName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKeyEx('TransFlag',[this,TransFlagName],[0,1],null,null,null,[1]);"><input class=codename name=TransFlagName readonly=true elementtype=nacessary>
        </TD>
		<TD  class= title>描述</TD>
        <TD  class= input>
        	<Input class=common name=ReMark verify="描述|len<=500" >
        </TD>         	
	</tr>			
</table> 
			   
<Div  id= "classdiv" style= "display: 'none'" align=left>
<table class=common>
	<tr class= common>
		<TD  class= title >转化类型处理类</TD>
		<TD  class= input >
			<Input class=common name=TransClass >
		</TD> 
		<TD  class= title ></TD>
		<TD  class= input ></TD> 	
	</tr>
</table> 	  
</div>		 
    
 <Div  id= "sqldiv" style= "display: 'none'" align=left>
<table class=common>
	<tr  class= common>
		<TD  class= common>转换SQL（4000字符以内）</TD>
	</tr>
	<tr  class= common>
		<TD  class= common>
			<textarea name="TransSQL" verify="转换SQL|len<=4000" verifyorder="1" cols="100%" rows="5" witdh=100% class="common" ></textarea>
		</TD>
	</tr>
</table>	
</div>	 
  
<p>
<Div  id= "allbuttondiv" style= "display: '' ">  
<INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
<INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
<INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
<INPUT VALUE="重  置" TYPE=button class= cssbutton name= resetbutton onclick="initForm()">
</Div>
<hr></hr>  
<Div  id= "budgetbuttondiv" style= "display: 'none' ">  
<INPUT VALUE="现金流量规则查询" TYPE=button class= cssbutton name= resetbutton onclick="buggetquery();">
</Div>

<INPUT type=hidden name=hideOperate value=''>
<INPUT type=hidden name=SourceTableID value=''>
<input type=hidden name=maintno >
<input type=hidden name=pageflag >
<INPUT type=hidden name=VersionState value=''> <!-- VersionState用来存版本状态：01，02，03；而VersionState2存版本状态：正常、维护、删除-->
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>


</body>
</html>
