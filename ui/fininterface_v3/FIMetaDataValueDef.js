//该文件中包含客户端需要处理的函数和事件

//程序名称：FIMetaDataValueDef.js
//程序功能：元数据值域定义
//创建日期：2011/9/13
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//初始化页面
function InitPage()
{
	var QuerySql = "select MetadataNo,SerialNo,AttNo,ValueType,ValueTypeName,PatternType,MainValues,SecondValues,ValueMeaning,Remark"
		+ " from FIMetadataAttValues where MetadataNo = '"+tMetaDataNo+"' and AttNo = '"+tAttNo+"' order by MainValues";
		
	turnPage.queryModal(QuerySql, MetadataValueGrid);
}


//增加按钮
function addClick()
{
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	//判断是否是新值插入
	var checksql="select count(*) from FIMetadataAttValues where MetadataNo = '"+fm.MetadataNo.value+"' and AttNo = '"+fm.AttNo.value+"' and MainValues = '"+fm.MainValues.value+"'";
	var re = easyExecSql(checksql);
	
	if(re!="0")
	{
		alert("元数据编码已存在！");
		return false;
	}
	
	//为了防止双击，点击增加后，屏蔽"增加"按钮
	mOperate="INSERT||MAIN";
	submitForm();
}

//删除按钮
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("MainValues").value==null)||(trim(fm.all("MainValues").value)==''))
    alert("请选择要删除的值域数据！");
  
  else
  {

    if (confirm("您确实想删除该记录吗?"))
    {
		//判断是否有原纪录
		var checksql="select count(*) from FIMetadataAttValues where MetadataNo = '"+fm.MetadataNo.value+"' and AttNo = '"+fm.AttNo.value+"' and MainValues = '"+fm.MainValues.value+"'";
		var re = easyExecSql(checksql);
		
		if(re=="0")
		{
			alert("元数据编码不存在！");
			return false;
		}
		mOperate="DELETE||MAIN";
		submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}

//修改按钮
function updateClick()
{
  //提交前的检验
  if (!beforeSubmit()) //beforeSubmit()函数
  {
  	return false;
  }
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
	   //判断是否有原纪录
		var checksql="select count(*) from FIMetadataAttValues where MetadataNo = '"+fm.MetadataNo.value+"' and AttNo = '"+fm.AttNo.value+"' and MainValues = '"+fm.MainValues.value+"'";
		var re = easyExecSql(checksql);
		
		if(re=="0")
		{
			alert("元数据编码不存在！");
			return false;
		}
		mOperate="UPDATE||MAIN";
		submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}

//提交操作
function submitForm()
{ 
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.hideOperate.value=mOperate;
	if (fm.hideOperate.value=="")
	{
	  alert("操作控制数据丢失！");
	}
	
	fm.action="./FIMetaDataValueDefSave.jsp";
	fm.submit(); //提交
}

//提交前的校验、计算  
function beforeSubmit()
{
	if(fm.MetadataNo.value=='')
	{
		alert("元数据编码不能为空！");
		return false;
	}
	
	if(fm.AttNo.value=='')
	{
		alert("属性编码不能为空！");
		return false;
	}
	
	if(fm.MainValues.value=='')
	{
		alert("主值域不能为空！");
		return false;
	}
	
	if(fm.ValueMeaning.value=='')
	{
		alert("值域含义不能为空！");
		return false;
	}
	
	return true;
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  location.reload();
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}

//获得GIRD数据
function getValuedata()
{
	var tSel = MetadataValueGrid.getSelNo();
	fm.MainValues.value = MetadataValueGrid.getRowColData(tSel-1,7);
	fm.ValueMeaning.value = MetadataValueGrid.getRowColData(tSel-1,9);
	fm.SerialNo.value = MetadataValueGrid.getRowColData(tSel-1,2);
	fm.Remark.value = MetadataValueGrid.getRowColData(tSel-1,10);
	
	
	showCodeName();
}