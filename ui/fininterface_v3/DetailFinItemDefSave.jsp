<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：DetailFinItemDefSave.jsp
//程序功能：明细科目判断条件定义保存页面
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%@page contentType="text/html;charset=GBK" %>
<%
	//接收信息，并作校验处理。
	request.setCharacterEncoding("GBK");
	
	String pageflag = request.getParameter("pageflag");
	//输入参数
	FIDetailFinItemDefSchema tFIDetailFinItemDefSchema = new FIDetailFinItemDefSchema();
	FMDetailFinItemDefSchema tFMDetailFinItemDefSchema = new FMDetailFinItemDefSchema();
	
	FIDetailFinItemDefUI tFIDetailFinItemDef = new FIDetailFinItemDefUI();
	FMDetailFinItemDefUI tFMDetailFinItemDefUI =new FMDetailFinItemDefUI();

	//输出参数
	CErrors tError = null;
	String tOperate=request.getParameter("hideOperate");
	tOperate=tOperate.trim();
	String mJudgementNo = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	//
	tFIDetailFinItemDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFIDetailFinItemDefSchema.setFinItemID(request.getParameter("FinItemID"));  
	tFIDetailFinItemDefSchema.setJudgementNo(request.getParameter("JudgementNo"));
	tFIDetailFinItemDefSchema.setLevelCondition(request.getParameter("LevelCondition"));
	tFIDetailFinItemDefSchema.setFirstMark(request.getParameter("FirstMark"));
	tFIDetailFinItemDefSchema.setReMark(request.getParameter("ReMark"));
	//
	tFMDetailFinItemDefSchema.setMaintNo(request.getParameter("maintno"));
	tFMDetailFinItemDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFMDetailFinItemDefSchema.setFinItemID(request.getParameter("FinItemID"));  
	tFMDetailFinItemDefSchema.setJudgementNo(request.getParameter("JudgementNo"));
	tFMDetailFinItemDefSchema.setLevelCondition(request.getParameter("LevelCondition"));
	tFMDetailFinItemDefSchema.setFirstMark(request.getParameter("FirstMark"));
	tFMDetailFinItemDefSchema.setReMark(request.getParameter("ReMark"));

	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	
	tVData.add(tG);
	try
	{
		System.out.println("come========== in"+tFIDetailFinItemDefSchema.getVersionNo());
		System.out.println("come========== in"+tFIDetailFinItemDefSchema.getFinItemID());  
		System.out.println("come========== in"+tFIDetailFinItemDefSchema.getJudgementNo());
		if("C".equals(pageflag))
		{
			tVData.addElement(tFIDetailFinItemDefSchema);
			if(!tFIDetailFinItemDef.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFIDetailFinItemDef.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		else if("X".equals(pageflag))
		{
			tVData.addElement(tFMDetailFinItemDefSchema);
			if(!tFMDetailFinItemDefUI.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFMDetailFinItemDefUI.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		
		System.out.println("come out" + tOperate);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail"))
	{
		tError = tFIDetailFinItemDef.mErrors;
		if (!tError.needDealError())
		{
			mJudgementNo = tFIDetailFinItemDef.getJudgementNo();
			Content = " 操作已成功! ";
			FlagStr = "Succ";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
</script>
</html>

