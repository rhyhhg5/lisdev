//该文件中包含客户端需要处理的函数和事件

//程序名称：FIBusFeeTypeDef.js
//程序功能：业务交易费用定义
//创建日期：2011/8/25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();



//模板下载按钮
function downloadmodel(){
	window.location.href="../fininterface_v3/Financialmodel/Financialmodel.xls";
}

//规则导入按钮
function uploadmodel(){
	if(fm.FileName.value==""||fm.FileName.value==null)
	{
		alert("录入导入文件路径！");
		return false;
	}
	
	getImportPath();
  
  ImportFile = fm.all('FileName').value;  

  var  lastname = ImportFile.substring(ImportFile.length-3,ImportFile.length);

    if(lastname!="xls")
    {
    	alert("上传文件模板不是2003版Excel,请重新上传2003版Excel模板！");
    	return false;
    }
  var showStr="正在上载数据……";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
  fm.action = "./FIRulesImportSave.jsp?ImportPath="+ImportPath;

  fm.submit(); //提交
  
}

function getImportPath(){
  var strSQL = "";
  strSQL = "select sysvarvalue from ldsysvar where sysvar='UFinModel'";
  turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 1, 1);

  //判断是否查询成功
  if (!turnPage.strQueryResult) {
    alert("未找到上传路径");
    return;
  }
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);

  //查询成功则拆分字符串，返回二维数组
  turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);

  ImportPath = turnPage.arrDataCacheSet[0][0];
}

//提交前的校验、计算  
function beforeSubmit(){
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}
