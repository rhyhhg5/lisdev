<%@page contentType="text/html;charset=GBK"%>

<%
//程序名称： BFDepartmentCodeInput.jsp
//程序功能： 录入业务机构代码、名称及其对应的财务机构代码
//创建日期： 2013-05-06
//创建人：卢翰林
//更新记录：  更新人    更新日期     更新原因/内容
%>

<!--用户校验类 -->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput)session.getValue("GI");
	String CurrentDate = PubFun.getCurrentDate();
%>  
<html>
<head>

	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

  <SCRIPT src="BFDepartmentCodeInput.js"></SCRIPT>
  <%@include file="BFDepartmentCodeInputInit.jsp"%>
  <title>机构代码录入</title>
</head>

<body  onload="initForm();initElementtype();">
  <form name=fm target="fraSubmit" action="./BFDepartmentCodeInputSave.jsp" method="post">
	  	<table>
			<tr>
				<td><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divCom);"></td>
				<td class=titleImg>业财机构代码配置</td>
			</tr>
		</table>
		<Div id="divCom" style="display: ''" align=center>
			<table class=common align=center>
				<tr class=common>
					<td class= title>业务机构代码：</td>
			        <td class= input>
						<Input class=common name=Code elementtype=nacessary verify="业务机构代码|NOTNULL&INT&LEN=8"/>
					</td>
					<td class= title>机构名称：</td>
			        <td class= input>
						<Input class=common name=CodeName elementtype=nacessary verify="机构名称|NOTNULL"/>
					</td>
			    </tr>
				<tr class=common>
					<td class= title>映射财务机构代码：</td>
					<td class= input>
						<Input class=common name=CodeAlias verify="财务机构代码|NOTNULL&INT&LEN=4" elementtype=nacessary />
					</td>
				</tr>
			</table>
			
		</div>
		<input  class=cssButton VALUE="保  存" TYPE="button" onclick="submitForm()">
  	</form> 
  	<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>