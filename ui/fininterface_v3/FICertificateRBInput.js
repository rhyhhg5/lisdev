 
//创建日期： 
//创建人   jw
//更新记录：  更新人    更新日期     更新原因/内容
var showInfo;
var showDealWindow;

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var k = 0;


//红冲处理
function AppDeal()
{

	var i = 0;
	var flag = 0;
	for( i = 0; i < RBResultGrid.mulLineCount; i++ )
	{
		if( RBResultGrid.getSelNo(i) >0 )
		{
			flag = 1;
			break;
		}
	}

	if( flag == 0 )
	{
		alert("请先选择一条申请记录");
		return false;
	}
	if(trim(fm.ReasonType.value)==""||trim(fm.ReasonType.value)==null)
	{
		alert("请先选择红冲原因类型!");
		return false;
	}
	
	showDealWindow = window.open("./FICertificateRBDealForOther.jsp?BusinessNo='" + fm.all('BusinessNo').value +"'&AppNo='" + fm.all('AppNo').value + "'&DetailIndexID='"+fm.DetailIndexID.value+"'"); 

}

//申请撤消
function AppDelete()
{
	var i = 0;
	var flag = 0;
	for( i = 0; i < RBResultGrid.mulLineCount; i++ )
	{
		if( RBResultGrid.getSelNo(i) >0 )
		{
			flag = 1;
			break;
		}
	}

	if( flag == 0 )
	{
		alert("请先选择一条申请记录");
		return false;
	}   
   
    var showStr="正在确认数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");		
    fm.action="./FICertificateRBSave.jsp";
    fm.target="fraSubmit";
    fm.submit(); //提交	
  
}


//红冲确认
function ReConfirm()
{
	if(trim(fm.ReasonType.value)==""||trim(fm.ReasonType.value)==null)
	{
		alert("请先选择红冲原因类型!");
		return false;
	}
    var showStr="正在确认数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");		
    fm.action="./FICertificateConfirmSave.jsp";
    fm.target="fraSubmit";
    fm.submit(); //提交	
	//?BusinessNo='" + fm.all('BusinessNo').value +"'
}



function queryResultGrid()
{
    
    initResultGrid() ;
     
    var strSQL = ""; 
    strSQL = "select AppNo,'1',DetailIndexID,BusinessNo,ReasonType,DetailReMark,AppDate,Applicant,(select codename from ldcode where codetype='appstate' and code=AppState ) from FIDataFeeBackApp where AppState not in ('04','05')";

    turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
    if (!turnPage.strQueryResult)
    {
		alert("未查询到满足条件的数据！");
		return false;
    }
    
    //设置查询起始位置
    turnPage.pageIndex = 0;
    //在查询结果数组中取出符合页面显示大小设置的数组
    turnPage.pageLineNum = 30 ;
    //查询成功则拆分字符串，返回二维数组
    turnPage.arrDataCacheSet = decodeEasyQueryResult(turnPage.strQueryResult);
    //设置初始化过的MULTILINE对象
    turnPage.pageDisplayGrid = RBResultGrid;
    //保存SQL语句
    turnPage.strQuerySql = strSQL ;
    arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex);
    //调用MULTILINE对象显示查询结果
    displayMultiline(arrDataSet, turnPage.pageDisplayGrid);
 
}


function ReturnData()
{
    var arrReturn = new Array();
	var tSel = RBResultGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null )

	    alert( "请先选择一条记录，再点击返回按钮。" );
	else
	{
		fm.all('AppNo').value = RBResultGrid.getRowColData(tSel-1,1);		
		fm.all('CertificateId').value = RBResultGrid.getRowColData(tSel-1,2);	
		fm.all('DetailIndexID').value = RBResultGrid.getRowColData(tSel-1,3);
		fm.all('BusinessNo').value = RBResultGrid.getRowColData(tSel-1,4);
		fm.all('ReasonType').value = RBResultGrid.getRowColData(tSel-1,5);
		fm.all('DetailReMark').value = RBResultGrid.getRowColData(tSel-1,6);    
		fm.all('AppDate').value = RBResultGrid.getRowColData(tSel-1,7);      
		fm.all('Applicant').value = RBResultGrid.getRowColData(tSel-1,8);
		//showCodeName();		
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
	 
	try
	{
		showInfo.close();
 
		if (FlagStr == "Fail" )
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
		else
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}

		fm.all('AppNo').value = "";		
		fm.all('CertificateId').value = "";	
		fm.all('CertificateIdName').value = "";
		fm.all('DetailIndexName').value = "";
		fm.all('DetailIndexID').value = "";
		fm.all('BusinessNo').value = "";
		fm.all('ReasonType').value = "";
		fm.all('ReasonTypeName').value = "";
		fm.all('DetailReMark').value = "";
		fm.all('AppDate').value = "";
		fm.all('Applicant').value = "";

		queryResultGrid();

 }
 catch(ex){}

}