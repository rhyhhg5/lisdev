<html>
<%
//程序名称 :FIRuleDealLogQueryForDownLoad.jsp
//程序功能 :系统操作日志查询
//创建人 :范昕
//创建日期 :2008-09-24
//
%>
	<%@page contentType="text/html;charset=GBK" %>
	<!--用户校验类-->
	<%@page import = "com.sinosoft.utility.*"%>
	<%@page import = "com.sinosoft.lis.schema.*"%>
	<%@page import = "com.sinosoft.lis.vschema.*"%>
	<%@page import = "com.sinosoft.lis.pubfun.*"%>
	<%@include file="../common/jsp/UsrCheck.jsp"%>
	<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。
 	%>
<script>
  var comcode = "<%=tGI1.ComCode%>";
</script>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
<SCRIPT src = "FIDifferentErrorLogInput.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FIDifferentErrorLogInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="" method=post name=fm target="fraSubmit">

<Div id= "divLLReport1" style= "display: ''">
  	<table>
    	<tr>
    		 <td class= titleImg>查询条件</td>   		 
    	</tr>
    </table>
	<table  class= common>
        <TR  class= common>
			<TD  class= title>起始时间</TD>
			<TD  class= input>
			  <Input class= "coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=StartDay >
			</TD>
			<TD  class= title>结束时间</TD>
			<TD  class= input>
			  <Input class= "coolDatePicker" verify="结束时间|notnull&date"  dateFormat="short" name=EndDay >
			</TD>
			<TD  class= title></TD>
			<TD  class= input>
			</TD>		
        </TR> 
	</table> 
	<br>
	<input class="cssButton" type=button value="查  询" onclick="OperationLogQuery()">
	<br><br>
	<table>
		<tr>
			<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFIRuleDealLogQueryForDownLoadGrid);">
			</td>
			<td class= titleImg>校验日志信息查询</td>
		</tr>
	</table>
	<Div  id= "divFIRuleDealLogGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanFIRuleDealLogGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 					
	</Div>
	
	
	<table  class= common>
    	<TR  class= common>
			<TD class= title>
				<INPUT class = cssButton name=DealErrdataButton VALUE="错误数据分析"  TYPE=button onclick="DealErrdata();" >  
			</TD>			
			<TD class= title></TD>
			<TD class= input></TD>
			<TD class= title></TD>
			<TD class= input></TD>
		</TR>
    </table>
	<hr></hr>
<input type=hidden name=RuleDealBatchNo>
<input type=hidden name=RulePlanID>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>