<html>
<%
//程序名称 :FIOperationLogQuery.jsp
//程序功能 :系统操作日志查询
//创建人 :范昕
//创建日期 :2008-09-24
//
%>
<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.pubfun.*"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
  GlobalInput tGI1 = new GlobalInput();
  tGI1=(GlobalInput)session.getValue("GI");//添加页面控件的初始化。  
 %>
<script>
  var comcode = "<%=tGI1.ComCode%>";
 

</script>
<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>  
<SCRIPT src = "FIOperationLogQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="FIOperationLogQueryinit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="" method=post name=fm target="fraSubmit">
<Div id= "divLLReport1" style= "display: ''">
  	<table>
    	<tr>
    		 <td class= titleImg>查询条件</td>   		 
    	</tr>
    </table>
	<input class="cssButton" type=button value="查  询" onclick="OperationLogQuery()">
 <table class= common border=0 width=100%>
	<table  class= common>
        <TR  class= common>
          <TD  class= title>  起始时间</TD>
          <TD  class= input>
            <Input class= "coolDatePicker" verify="起始时间|notnull&date" dateFormat="short" name=StartDay >
          </TD>
          <TD  class= title> 结束时间</TD>
          <TD  class= input>
            <Input class= "coolDatePicker" verify="结束时间|notnull&date"  dateFormat="short" name=EndDay >
          </TD>
        </TR> 
      	<TR  class= common>
          <TD  class= title>事件类型</TD>
          <TD  class= input>
	    <Input class="codeno" name=EventClass  ondblclick="return showCodeList('EventType',[this,EventClassName],[0,1]);" onkeyup="return showCodeList('EventType',[this,EventClassName],[0,1]);"  readonly=true ><input class=codename name=EventClassName readonly=true ></TD>	          
          </TD>
          <TD  class= title>事件状态</TD>
          <TD  class= input>
	    <Input class="codeno" name=EventFlag CodeData="0|^0|成功|M^1|失败|M" ondblclick="return showCodeListEx('EventFlag',[this,EventFlagName],[0,1]);" onkeyup="return showCodeListEx('EventFlag',[this,EventFlagName],[0,1]);"  readonly=true ><input class=codename name=EventFlagName readonly=true ></TD>
          </TD>
        </TR>      
      </table> 
      <table  class= common>
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick="showPage(this,divFIOperationLogGrid);">
    		</td>
    		<td class= titleImg>系统事件操作日志查询</td>
    	</tr>
    </table>
	<Div  id= "divFIOperationLogGrid" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  		<td text-align: left colSpan=1>
  					<span id="spanFIOperationLogGrid" >
  					</span> 
  			  	</td>
  			</tr>
    	</table>
		<INPUT VALUE="首  页" class=cssButton TYPE=button onclick="turnPage.firstPage();"> 
		<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="turnPage.previousPage();"> 					
		<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="turnPage.nextPage();"> 
		<INPUT VALUE="尾  页" class=cssButton TYPE=button onclick="turnPage.lastPage();"> 					
  <br>
</Div>
<table  class= common>
    	<TR  class= common>
          <TD class= title>
					  <input class="cssButton" type=button value="导出该日志下载地址" onclick="DownloadAddress()">
				 </TD>
       </TR>
    </table>
<hr></hr>

<Div  id= "Querydiv" style= "display: 'none'" align=left>
	<table class=common>
		 <TR  class= common>
          <TD class= title>
					  事件号码
				 </TD>
				 <TD class=input>
				 	 <Input class=readonly name=EventNo readonly = true>
				</TD>
	<TD class= title> 事件类型</TD>
				 <TD class=input>
				 	 <Input class=readonly name=EventType readonly = true>
				</TD>
        </TR>
      <TR  class= common>
      <TD  class= title>
        下载文件链接
      </TD>
      <TD  class= input>
        <A id=fileUrl name = filepath  href=""></A>
      </TD>           
    </TR>  
    <tr><td>
				<!--div id="filesList" style="margin-top:10px;"></div-->	
				<input value="下  载" class=cssButton type=button onclick="downLoad();">
			</tr></td> 
 </table> 	  
</div>
</table>	
   <input type=hidden id="OperateType" name="OperateType">
   <input type=hidden name="EventNo1" VALUE=''>
</form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>