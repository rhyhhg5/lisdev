<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FIRulesImportInput.jsp
 //程序功能：账务规则导入
 //创建日期：2011/8/25
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <title>费用信息定义</title>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIRulesImportInput.js"></SCRIPT>
  <%@include file="FIRulesImportInit.jsp"%>
  
  <script>
	var BusTypeId = '<%=request.getParameter("BusTypeId")%>';
  </script>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FIRulesImportSave.jsp" method=post name=fm target="fraSubmit" ENCTYPE="multipart/form-data"> 

<table>
  <tr>
    <td class="titleImg">账务规则导入</td>
  </tr>
</table>
<table  class="common" >
	<tr class="common">	
		<%-- 
		<td class="title">版本编号</td>
		<td class="input"><input class=common elementtype=nacessary name=VersionNo></td>
		<td class="title">模板类型</td>
		<td class="input">
			<input class="codeno" name="FItemplateType" 
				ondblclick="return showCodeList('firulesimport',[this,FItemplateTypeName],[0,1]);" 
				onkeyup="return showCodeListKey('firulesimport',[this,FItemplateTypeName],[0,1]);" nextcasing=><input class="codename" name="FItemplateTypeName" >

		</td>  
		--%>
		<td class="title">上传附件</td>
		<td class="input" colspan=3>
			<input class="common" type="file" name=FileName >
		</td>  
		
		<td class="title">
		</td>
		<td class="title">
		</td>
	</tr>
</table>

<br>
<input value="模板下载"  onclick="downloadmodel();" class="cssButton" type="button" >
<input value="规则导入"  onclick="uploadmodel();" class="cssButton" type="button" >
<br>

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
