<%
//程序名称 :FIRuleEngineServiceSave.jsp
//
%>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.core.check.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.tools.datalog.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
  
	request.setCharacterEncoding("GBK");
	System.out.println("1开始执行Save页面");
	//FIRuleEngineService1 mFIRuleEngineService1 = new FIRuleEngineService1();
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI");              //获得用户信息
	CErrors tError = null;
	String tRela  = "";
	String FlagStr = "";
	String Content = "";
	String Operator = "difference";
	
	String RuleDefID = request.getParameter("RuleDefID");
	String StartDate = request.getParameter("StartDate");
	String EndDate = request.getParameter("EndDate");
	
	FIOperationLog tLogDeal = new FIOperationLog(tG.Operator,"00");
	TransferData mTransferData  = new TransferData();
	mTransferData.setNameAndValue("RuleDefID",RuleDefID);
	mTransferData.setNameAndValue("StartDate",StartDate);
	mTransferData.setNameAndValue("EndDate",EndDate);
  
	VData tVData = new VData();
	
	tVData.add(mTransferData);
	tVData.add(tG);
	
	try
	{
		DataCheckService tDataCheckService = new DataCheckService(tLogDeal);
		if(!tDataCheckService.difDataCheck("01",tVData))
		{
			tError = tDataCheckService.mErrors;
			FlagStr = "Fail";
		}
	}
	catch(Exception ex)
	{
		Content = "失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")	
	{     
		Content = "操作已成功!";
		FlagStr = "Succ";
	}
	else
	{
		Content = " 操作失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>