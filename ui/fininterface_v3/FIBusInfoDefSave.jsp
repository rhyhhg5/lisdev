<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIBusInfoDefSave.jsp
//程序功能：业务交易信息定义
//创建日期：2011/8/25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>

<%
 //接收信息，并作校验处理。
request.setCharacterEncoding("GBK"); 
 //输入参数
 //××××Schema t××××Schema = new ××××Schema();
String tMaintNo=request.getParameter("maintno");
String tBusTypeId=request.getParameter("BusTypeId");
String FIBusInfoID=request.getParameter("FIBusInfoID");
String tFIBusInfoIDName=request.getParameter("FIBusInfoIDName");
String tBusClass=request.getParameter("FIDimension");
String tBusClassName=request.getParameter("FIDimensionName");
String tRemark=request.getParameter("FIBusRemark");
String tPurPose=request.getParameter("PurPose");
String tFeeValues=request.getParameter("FeeValues");

String apptype=request.getParameter("apptype");
String pageflag=request.getParameter("pageflag");

//打印参数
System.out.println("tMaintNo------------->"+tMaintNo);
System.out.println("tBusTypeId------------->"+tBusTypeId);
System.out.println("FIBusInfoID------------->"+FIBusInfoID);
System.out.println("tFIBusInfoIDName------------->"+tFIBusInfoIDName);
System.out.println("tBusClass------------->"+tBusClass);
System.out.println("tBusClassName------------->"+tBusClassName);
System.out.println("tRemark------------->"+tRemark);
System.out.println("tPurPose------------->"+tPurPose);
System.out.println("tFeeValues------------->"+tFeeValues);
System.out.println("apptype------------->"+apptype);
System.out.println("pageflag------------->"+pageflag);

FIBusInfoDefUI tFIBusInfoDefUI = new FIBusInfoDefUI();
FMBusInfoDefUI tFMBusInfoDefUI = new FMBusInfoDefUI();

 
//输出参数
CErrors tError = null;
String tRela  = "";                
String FlagStr = "";
String Content = "";
String transact = "";
 
GlobalInput tG = new GlobalInput(); 
tG=(GlobalInput)session.getAttribute("GI");
 
//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
transact = request.getParameter("transact");
 
//从url中取出参数付给相应的schema
//t××××Schema.set××××(request.getParameter("××××"));
 
try
{
	//准备传输数据VData
	VData tVData = new VData();
  
	tVData.add(tG);
	tVData.add(apptype);
	//传输schema
	if("X".equals(pageflag))
	{
		FMBnTypeInfoSchema tFMBnTypeInfoSchema = new FMBnTypeInfoSchema();
		tFMBnTypeInfoSchema.setVersionNo("00000000000000000001");
		tFMBnTypeInfoSchema.setMaintNo(tMaintNo);
		tFMBnTypeInfoSchema.setBusinessID(tBusTypeId);
		tFMBnTypeInfoSchema.setPropertyCode(tFIBusInfoIDName);
		tFMBnTypeInfoSchema.setBusClass(tBusClass);
		tFMBnTypeInfoSchema.setBusClassName(tBusClassName);
		tFMBnTypeInfoSchema.setRemark(tRemark);
		tFMBnTypeInfoSchema.setPurpose(tPurPose);
		tFMBnTypeInfoSchema.setBusValues(tFeeValues);
		
		tVData.add(tFMBnTypeInfoSchema);
		tFMBusInfoDefUI.submitData(tVData,transact);
	}
	else if("C".equals(pageflag))
	{
		FIBnTypeInfoSchema tFIBnTypeInfoSchema = new FIBnTypeInfoSchema();
		tFIBnTypeInfoSchema.setVersionNo("00000000000000000001");
		tFIBnTypeInfoSchema.setBusinessID(tBusTypeId);
		tFIBnTypeInfoSchema.setPropertyCode(FIBusInfoID);
		tFIBnTypeInfoSchema.setPropertyName(tFIBusInfoIDName);
		tFIBnTypeInfoSchema.setBusClass(tBusClass);
		tFIBnTypeInfoSchema.setBusClassName(tBusClassName);
		tFIBnTypeInfoSchema.setRemark(tRemark);
		tFIBnTypeInfoSchema.setPurpose(tPurPose);
		tFIBnTypeInfoSchema.setBusValues(tFeeValues);
		
		tVData.add(tFIBnTypeInfoSchema);
		tFIBusInfoDefUI.submitData(tVData,transact);
	}
  
}
catch(Exception ex)
{
	Content = "保存失败，原因是:" + ex.toString();
	FlagStr = "Fail";
}
 
//如果在Catch中发现异常，则不从错误类中提取错误信息
if (FlagStr=="")
{
	tError = tFIBusInfoDefUI.mErrors;
	if (!tError.needDealError())
	{                          
		Content = " 保存成功! ";
		FlagStr = "Success";
	}
	else
	{
		Content = " 保存失败，原因是:" + tError.getFirstError();
		FlagStr = "Fail";
	}
}
 
//添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");

</script>
</html>
