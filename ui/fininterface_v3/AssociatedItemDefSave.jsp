<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：AssociatedItemDefSave.jsp
//程序功能：科目专项定义保存页面
//创建日期：2011-9-26
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//接收信息，并作校验处理。
	request.setCharacterEncoding("GBK");
	String pageflag = request.getParameter("pageflag");
	String tSql = request.getParameter("TransSQL");
	
	//替换单引号
	tSql = tSql.replaceAll("'","''");
	
	System.out.println("tSql========="+tSql);
	
	System.out.println("pageflag=========="+pageflag);
	System.out.println("come========== in"+request.getParameter("AssociatedName"));
	//输入参数
	FIAssociatedItemDefSchema tFIAssociatedItemDefSchema = new FIAssociatedItemDefSchema();
	FMAssociatedItemDefSchema tFMAssociatedItemDefSchema = new FMAssociatedItemDefSchema();
	
	FIAssociatedItemDefUI tFIAssociatedItemDef = new FIAssociatedItemDefUI();
	FMAssociatedItemDefUI tFMAssociatedItemDefUI = new FMAssociatedItemDefUI();

	//输出参数
	CErrors tError = null;
	String tOperate=request.getParameter("hideOperate");
	tOperate=tOperate.trim();
	String mAssociatedID = "";
	String FlagStr = "Fail";
	String Content = "";

	GlobalInput tG = new GlobalInput();
	tG=(GlobalInput)session.getValue("GI");
	//
	tFIAssociatedItemDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFIAssociatedItemDefSchema.setAssociatedID(request.getParameter("AssociatedID"));
	tFIAssociatedItemDefSchema.setAssociatedName(request.getParameter("AssociatedName"));
	tFIAssociatedItemDefSchema.setColumnID(request.getParameter("ColumnID"));
	tFIAssociatedItemDefSchema.setSourceTableID(request.getParameter("SourceTableID"));
	tFIAssociatedItemDefSchema.setSourceColumnID(request.getParameter("SourceColumnID"));  
	tFIAssociatedItemDefSchema.setTransFlag(request.getParameter("TransFlag"));
	tFIAssociatedItemDefSchema.setTransSQL(tSql);
	tFIAssociatedItemDefSchema.setTransClass(request.getParameter("TransClass"));
	tFIAssociatedItemDefSchema.setReMark(request.getParameter("ReMark"));
	//
	tFMAssociatedItemDefSchema.setMaintNo(request.getParameter("maintno"));
	tFMAssociatedItemDefSchema.setVersionNo(request.getParameter("VersionNo"));
	tFMAssociatedItemDefSchema.setAssociatedID(request.getParameter("AssociatedID"));
	tFMAssociatedItemDefSchema.setAssociatedName(request.getParameter("AssociatedName"));
	tFMAssociatedItemDefSchema.setColumnID(request.getParameter("ColumnID"));
	tFMAssociatedItemDefSchema.setSourceTableID(request.getParameter("SourceTableID"));
	tFMAssociatedItemDefSchema.setSourceColumnID(request.getParameter("SourceColumnID"));  
	tFMAssociatedItemDefSchema.setTransFlag(request.getParameter("TransFlag"));
	tFMAssociatedItemDefSchema.setTransSQL(tSql);
	tFMAssociatedItemDefSchema.setTransClass(request.getParameter("TransClass"));
	tFMAssociatedItemDefSchema.setReMark(request.getParameter("ReMark"));

	// 准备传输数据 VData
	VData tVData = new VData();
	FlagStr="";
	tVData.add(tG);
	try
	{
		System.out.println("come========== in"+tFIAssociatedItemDefSchema.getVersionNo());    
		System.out.println("come========== in"+tFIAssociatedItemDefSchema.getAssociatedID());   
		   
		
		if("C".equals(pageflag))
		{
			System.out.println("pageflag=C");
			tVData.addElement(tFIAssociatedItemDefSchema);
			if(!tFIAssociatedItemDef.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFIAssociatedItemDef.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		else if("X".equals(pageflag))
		{
			System.out.println("pageflag=X");
			tVData.addElement(tFMAssociatedItemDefSchema);
			if(!tFMAssociatedItemDefUI.submitData(tVData,tOperate))
			{
				Content = "操作失败，原因是:" + tFMAssociatedItemDefUI.mErrors.getFirstError();
				FlagStr = "Fail";
			}
		}
		
		System.out.println("come out" + tOperate);
	}
	catch(Exception ex)
	{
		Content = "操作失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}

	if (!FlagStr.equals("Fail"))
	{
		tError = tFIAssociatedItemDef.mErrors;
		if (!tError.needDealError())
		{
			mAssociatedID = tFIAssociatedItemDef.getAssociatedID();
			Content = " 操作已成功! ";
			FlagStr = "Succ";
		}
		else
		{
			Content = " 操作失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

  //添加各种预处理

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
</script>
</html>

