<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<%
//程序名称：InfindatacheckInput.jsp
//程序功能：校验定义
//创建日期：2011/9/15
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

	<SCRIPT src="Infindatacheck.js"></SCRIPT>
	<%@include file="InfindatacheckInit.jsp"%>
</head>

<body onload="initForm();initElementtype();">
<form action="./FifindatacheckSave.jsp" method=post name=fm target="fraSubmit">
<table>
  <tr>
    <td class="titleImg" >规则</td>
  </tr>
</table>

<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanDataRuleGrid" >
     </span> 
      <br></td>
   </tr>
</table>
<table>
	<tr>
		<td class="titleImg">
			校验类型定义
		</td>
	</tr>
</table>
<table class="common">
	<tr class="common">
		<td class="title">算法编码</td>
		<td class="input">
			<input class="common" name="ArithmeticID" >
		</td>
		<td class="title">校验类型</td>
		<td class="input">
			<input class="codeno" name="Codetype"
				CodeData="0|^01|质验计划|^02|差异计划" ondblclick="showCodeListEx('Codetype',[this,CodetypeName],[0,1],null,null,null,1);"
	            onkeyup="showCodeListKeyEx('Codetype',[this,CodetypeName],[0,1],null,null,null,1);"verify="校验类型|notnull"
			    readonly><input class="codename" name="CodetypeName" readonly=true elementtype=nacessary >
		</td>
		<td class="title">算法明细类型</td>
		<td class="input">
			<input class="codeno" name="SubType"
				CodeData="0|^00|完整性校验|^01|正确性校验|^02|一致性校验" ondblclick="showCodeListEx('SubType',[this,SubTypeName],[0,1],null,null,null,1);"
	             onkeyup="showCodeListKeyEx('SubType',[this,SubTypeName],[0,1],null,null,null,1);"verify="校验类型|notnull"
			    readonly><input class="codename" name="SubTypeName" readonly elementtype=nacessary >
		</td>
	</tr>
	<tr class="common">	
		<td class="title">错误类型</td>
		<td class="input">
			<input class="common" name="errtype">
		</td>
		<td class="title">处理方式</td>
		<td class="input">
		<input class="codeno" name="dealmode"
				CodeData="0|^1|类处理|^2|SQL处理|^3|文件校验规则处理" ondblclick="showCodeListEx('dealmode',[this,dealmodeName],[0,1],null,null,null,1);"
	             onkeyup="showCodeListKeyEx('dealmode',[this,dealmodeName],[0,1],null,null,null,1);"verify="校验类型|notnull"
			    readonly><input class="codename" name="dealmodeName" readonly elementtype=nacessary >
		</td> 
		<td class="title">节点类型</td>
		<td class="input">
			<input class="codeno" name="NodeType" 
			ondblclick="return showCodeList('finodetype',[this,NodeTypeName],[0,1]);" 
			onkeyup="return showCodeListKey('finodetype',[this,NodeTypeName],[0,1]);"><input class="codename" name="NodeTypeName" ></td> 
	</tr>					
	<tr class="common">	
	<td class="title">父节点</td>
	<td class="input">
		<input class="common" name="ParentNode" />
	</td>
	<td class="title">规则状态</td>
	<td class="input">
		<input class="codeno" name="State" CodeData="0|^0|失效|^1|有效" verify="校验类型|notnull" 
		ondblclick="return showCodeListEx('RuleState',[this,RuleStateName],[0,1],null,null,null,1);" 
		onkeyup="return showCodeListKeyEx('RuleState',[this,RuleStateName],[0,1],null,null,null,1);" readOnly><input class="codename" name="RuleStateName" >
	</td> 
	</tr>
	<tr class="common">	 
		<td class="title">校验描述</td>
		<td class="input" colspan=3>
			<textarea class="common" name="ReturnRemark" cols="80%" rows="2" elementtype=nacessary></textarea>
		</td>
	</tr>
	<tr class="common">
		<td class="title">财务接口数据校检</td>
		<td class="input" colspan=3>
			<textarea class="common" name="FIDealSQL" cols="80%" rows="4" elementtype=nacessary></textarea>
		</td>
	</tr>
	<tr class="common">
		<td class="title">业务系统数据校检</td>
		<td class="input" colspan=3>
			<textarea class="common" name="BuDealSQL" cols="80%" rows="4" elementtype=nacessary></textarea>
		</td>
	</tr>
</table>
<br>
<div id=button style="display:'none'">
<input value="保   存" class="cssButton" type="button" onclick="SaveData()">			
<input value="删   除" class="cssButton" type="button" onclick="DeleteData();">		
<input value="关   闭" class="cssButton" type="button" onclick="closed();">		
<br>
</div>
<br>
<input class="codeno" name="" type=hidden>
<input class="common" name="VersionNo" cols="100%" rows="2" type=hidden>
<input class="common" name="ruledefid" cols="100%" rows="2" type=hidden>
</form>
<span id="spanCode" style="display: none; position: absolute;"></span>
</body>
</html>
