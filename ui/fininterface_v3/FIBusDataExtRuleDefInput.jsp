<html> 
<%
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	String mStartDate = "";
	String mSubDate = "";
	String mEndDate = "";
	GlobalInput tGI = new GlobalInput();
	//PubFun PubFun=new PubFun();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("1"+tGI.Operator);
	System.out.println("2"+tGI.ManageCom);
%>

<head >

<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>  
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT> 

<SCRIPT src="FIBusDataExtRuleDefInput.js"></SCRIPT>  
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="FIBusDataExtRuleDefInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >

<form name=fm target=fraSubmit method=post>
<table>
  	<tr>    		 
  		 <td class= titleImg>数据抽取规则定义</td>   		 
  	</tr>
</table>
<hr>
<div id=versionquerybutton>
<td class=button width="10%" align=right>
	<INPUT class=cssButton name="querybutton" VALUE="版本信息查询"  TYPE=button onclick="return queryClick1();">
</td>    
</div>
<table class= common border=0 width=100%>			  	
	<tr class= common>       
		<TD class= title>版本编号</TD>
		<TD class=input>
			<Input class= readonly name=VersionNo value= '' readonly=true>   					 	 
		</TD>
		<TD class= title>版本状态	</TD>
		<TD class=input>
			<Input class= readonly name=VersionState2 value = '' readonly=true>
		</TD>				 								
	</tr>  
</table>       

<hr></hr>  
<div id=finitemquerydiv>
<td class=button width="10%" align=right>
	<INPUT class=cssButton name="querybutton" VALUE="主业务数据抽取规则查询"  TYPE=button onclick="return queryClick2();">
</td>
</div>	
<table class= common border=0 width=100%>
	<tr class= common>
		<TD class= title>主业务数据抽取规则编号</TD>
		<TD class=input>
				<Input class=common name=ArithmeticID  elementtype=nacessary verify="主交易数据抽取规则|len<=20">
		</TD>
		<TD class= title>主业务数据抽取规则名称</TD>
	    <TD class= input>                                            
	  		 <Input class=common name=ArithmeticName  elementtype=nacessary verify=">主交易数据抽取规则名称|len<=40">
	    </TD>				
	</tr>
	<tr class= common>
		<TD class= title>主业务数据抽取规则类型</TD>
	    <TD class= input>
			<Input class=codeno name= BuType readonly=true ondblClick="showCodeList('fibustype',[this,RuleTypeName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKey('fibustype',[this,RuleTypeName],[0,1],null,null,null,[1]);"><input class=codename name=RuleTypeName readonly=true elementtype=nacessary>
	    </TD>
		<TD class= title>主业务数据抽取规则对象</TD>
		<TD class=input>
			<Input class=common name=RunObject  elementtype=nacessary >
		</TD>		    		    
	</tr>
	<tr class= common>
		<TD class= title>规则状态</TD>
	    <TD class= input>
			<Input class=codeno name= State readonly=true ondblClick="showCodeList('firulestate',[this,RuleStateName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKey('rulestate',[this,RuleStateName],[0,1],null,null,null,[1]);"><input class=codename name=RuleStateName readonly=true elementtype=nacessary>
	    </TD>
		<TD class= title>规则处理方式</TD>
		<TD class=input>
			<Input class=codeno name= DealMode readonly=true ondblClick="showCodeList('firulemode',[this,RuleDealModeName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKey('firulemode',[this,RuleDealModeName],[0,1],null,null,null,[1]);"><input class=codename name=RuleDealModeName readonly=true elementtype=nacessary>
		</TD>		    		    
	</tr>
	<tr class=common>
		<td class=title>规则描述</td>
		<td class=input colspan=3>
			<Input class=common name=ReturnRemark >
		</td>
	</tr>
</table>
<br>

<div id=sqldiv style="display:'none'">
	<table class= common>
	<tr class="common">
		<td class=title>处理SQL</td>
		<td colspan =3><textarea rows=4 cols=80% name = MainSQL></textarea></td>
	</tr>
	</table> 
</div> 
<div id=classdiv style="display:'none'">
	<table class= common>
	<tr class="common">
		<td class=title>处理类</td>
		<td class=input><input class=common name=DealClass ></td>
	</tr>
	</table> 
</div> 		
<div id=filediv style="display:'none'">
	<td class=title>处理文件</td>
	<td class=input><input class=common name=FileName ></td>
</div> 	 
  					 
  	<p>
    <INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
    <INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
    <INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
    <INPUT VALUE="重  置" TYPE=button class= cssbutton name="resetbutton" onclick="return resetAgain();">   
    <hr></hr>
    
    <INPUT VALUE="明细数据抽取规则定义" TYPE=button class= cssbutton name="intobutton1" onclick="intoDetailRulePage();">   
    <INPUT VALUE="主数据补充信息规则定义" TYPE=button class= cssbutton name="intobutton2" onclick="intoMainRuleAddOnPage();">
    <!-- 定义页面参数 -->
    <input type=hidden name=pageflag>
    <input type=hidden name=maintno>
    <input type=hidden name=apptype>
    <input type=hidden name=SubType value="00">
    <input type=hidden name=PrArithmeticID>
    <INPUT type=hidden name=hideOperate value=''>
    <INPUT type=hidden name=VersionState value='01'> <!-- VersionState用来存版本状态：01，02，03；而VersionState2存版本状态：正常、维护、删除-->
    <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>


</body>
</html>
