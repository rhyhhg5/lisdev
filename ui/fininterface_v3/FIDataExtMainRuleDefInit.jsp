<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>                    

<script language="JavaScript">
var pageflag = "";
var apptype = "";
var maintno = "";
var FinItemID = "";
// 输入框的初始化（单记录部分）
function initInpBox()
{ 

	try
	{
		pageflag = '<%=request.getParameter("pageflag")%>';
		FinItemID = '<%=request.getParameter("otherno")%>';
		apptype = '<%=request.getParameter("apptype")%>';
		maintno = '<%=request.getParameter("maintno")%>';
		
		//fm.all('FinItemID').readOnly=false;    	
		fm.all('RuleID').value = '';
		fm.all('RuleName').value = '';
		fm.all('RuleType').value = '';
		
		fm.pageflag.value = pageflag;
		
		if("X"==pageflag)
		{
			versionquerybutton.style.display = 'none';
			finitemquerydiv.style.display = 'none';
			
			if("insert"==apptype)
				addbuttondiv.style.display = '';
			else if("update"==apptype)
				updatebuttondiv.style.display = '';
			else if("delete"==apptype)
				deletebuttondiv.style.display = '';
			
			
			fm.maintno.value = maintno;
			fm.apptype.value = apptype;
			fm.FinItemID.value = FinItemID;
			
			initmaintpage();
		}
		else if ("C"==pageflag)
		{
			allbuttondiv.style.display = '';
			fm.all('updatebutton').disabled = true;		
			fm.all('deletebutton').disabled = true;  	
		}
  	  	
		       
	}
	catch(ex)
	{
    alert("在FinItemDefInputInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}



function initSelBox()
{  
  try                 
  {

  }
  catch(ex)
  {
    alert("在FinItemDefInputInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}                                   

function initForm()
{
  try
  {
    initInpBox(); 
    initSelBox();
    initBusTypeInfoGrid();
    
  }
  catch(re)
  {
    alert("FinItemDefInputInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
function initBusTypeInfoGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=1;

		iArray[1]=new Array();
		iArray[1][0]="业务编号";
		iArray[1][1]="90px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="信息维度";
		iArray[2][1]="50px";
		iArray[2][2]=100;
		iArray[2][3]=0;
		
		iArray[3]=new Array();
		iArray[3][0]="维度名称";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="信息编码";
		iArray[4][1]="50px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="信息名称";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=1;

		iArray[6]=new Array();
		iArray[6][0]="信息描述";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=1;


		BusTypeInfoGrid = new MulLineEnter( "fm" , "BusTypeInfoGrid" ); 

		BusTypeInfoGrid.mulLineCount=2;
		BusTypeInfoGrid.displayTitle=1;
		BusTypeInfoGrid.canSel=1;
		BusTypeInfoGrid.canChk=0;
		BusTypeInfoGrid.hiddenPlus=1;
		BusTypeInfoGrid.hiddenSubtraction=1;			
		BusTypeInfoGrid.selBoxEventFuncName="dataselect";

		BusTypeInfoGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}


</script>