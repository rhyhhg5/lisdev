<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>
<%
 //程序名称：FIVoucherFeeDefInput.jsp
 //程序功能：账务规则定义
 //创建日期：2011/8/27
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIVoucherFeeDef.js"></SCRIPT>
  <%@include file="FIVoucherFeeDefInit.jsp"%>
  <script>
	var voucherid = '<%=request.getParameter("voucherid")%>';
	var versionno = '<%=request.getParameter("versionno")%>';
  </script>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FIVoucherFeeDefSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >账务规则定义</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanVoucherFeeGrid" >
     </span> 
      </td>
   </tr>
</table>
<br>
<Div  id= "buttonsdiv" style= "display: '' "> 
<input value=" 保  存 "  onclick="addclick()" class="cssButton" type="button" >
</Div>
<br>
<input type=hidden name=pageflag>
<input type=hidden name=fmtransact value="DELETE&INSERT">
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
