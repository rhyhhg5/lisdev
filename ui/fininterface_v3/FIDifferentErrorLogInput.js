//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mOperate="";
var turnPage = new turnPageClass();
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null) //shwoInfo是什么？
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//进入页面自动查询

function OperationLogQuery() 
{	
	var sStartDay = fm.all('StartDay').value;
	var sEndDay = fm.all('EndDay').value;
	var strSQL = ""; 
	
  	strSQL = "select VersionNo,CheckBatchNo,EventNo,(select codename from ldcode where codetype = 'fieventnode' and code=a.RuleID),RuleResult,RulePlanID " 
  			+" from FIRuleDealLog a  where 1=1 and checktype='01' and exists (select 1 from FIRuleDealErrLog b where b.CheckBatchNo=a.CheckBatchNo and b.DealState='0' ) ";
		if(sStartDay!='')
	{
		strSQL = strSQL + " and a.MakeDate >='"+sStartDay+"' ";
	}
	if(sEndDay!=''){
		strSQL = strSQL + " and a.MakeDate <='"+sEndDay+"' ";
	}
  		
  	turnPage.queryModal(strSQL, FIRuleDealLogGrid);
} 

function DownloadAddress()
{
	if((fm.all('RuleDealBatchNo').value == '')||(fm.all('RuleDealBatchNo').value == null))
	{
		alert("请先在校验日志信息查询结果中选择一条记录再做该操作！");
    return ;
	}
	Querydiv.style.display='';
}

//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}


//增加、删除、修改提交前的校验、计算
function beforeSubmit()
{			
  if (!verifyInput2())
  {
  	return false;
  }
    
    return true;
}




//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


//选中mulline中一行，自动给输入框赋值
 function ReturnData()
{
 	var arrResult = new Array();

	var tSel = FIRuleDealLogGrid.getSelNo();	
	var strSQL="";
	if( tSel == 0 || tSel == null )
	{
		alert( "请先选择一条记录!" );
		return;
	}			
	else
	{
		//设置需要返回的数组
		// 书写SQL语句		
	  	var strSQL = "";
			strSQL = "select a.CheckBatchNo,a.RulePlanID,a.LogFileName,a.LogFilePath from FIRuleDealLog a where a.CheckBatchNo='"+
		 
		FIRuleDealLogGrid.getRowColData(tSel-1,3)+"'";
		turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);  
		
		
		//判断是否查询成功
		if (!turnPage.strQueryResult) 
		{
			alert("查询失败！");
			return false;
		}
  
		//查询成功则拆分字符串，返回二维数组
		arrResult = decodeEasyQueryResult(turnPage.strQueryResult);  	
		fm.all('RuleDealBatchNo').value = arrResult[0][0];
		fm.all('RulePlanID').value = arrResult[0][1];
		//fileUrl.href = arrResult[0][3]+arrResult[0][2]; 
		//fileUrl.innerText =  arrResult[0][0];  	  
	}	 
}

function DealErrdata()
{
	var tSel = FIRuleDealLogGrid.getSelNo();	
		
	if( tSel == 0 || tSel == null ){
		alert( "请先选择一条记录，再点击返回按钮。" );
		return;
	}	
	else
	{
		var RuleDealBatchNo = FIRuleDealLogGrid.getRowColData(tSel - 1,2);
		var EventNo = FIRuleDealLogGrid.getRowColData(tSel - 1,3);
	}

	window.open("./FrameFIQualityErrorLogInput.jsp?checktype=01&RuleDealBatchNo="+RuleDealBatchNo+"&EventNo="+EventNo);
}