<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：
//程序功能：
//创建日期：2003-03-26 15:39:06
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="FinDayCheck.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel="stylesheet"	type="text/css">
	<%@include file="FinDayCheckInit.jsp"%>
</head>
<body onload="initForm();">
<form method=post name=fm target="fraSubmit">

	<p>
		<strong>操作员日结打印</strong>
	</p>

	<table class=common>
		<tr class=common>
			<td class=title>请选择日结单类型</td>
			<td class="input" >
			<input class="codeno" name="FinDayCheckType" 
			    CodeData="0|^1|财务收费日结^2|财务付费日结^3|首期续期收入日结^4|保全保费日结^6|理赔应付日结^7|异地付费^8|异地收费^9|佣金日结^10|手续费应付"
				ondblclick="return showCodeListEx('FinDayCheckType',[this,FinDayCheckTypeName],[0,1]);" 
				onkeyup="return showCodeListKeyEx('FinDayCheckType',[this,FinDayCheckTypeName],[0,1]);" readonly = "true"><input class="codename" name="FinDayCheckTypeName" readonly = "true">
			</td> 
			<td></td><td></td>
		</tr>
	</table>
	<Div id="divOperator" style="display: ''">
	<p>
		<strong>输入查询的时间范围</strong>
	<Div id="divFCDay" style="display: ''">
		<table class="common">
			<TR class=common>
				<TD class=title>起始时间</TD>
				<TD class=input>
					<Input class="coolDatePicker" dateFormat="short" id="StartDay" name="StartDay" verify="起始时间|NOTNULL">
				</TD>
				<TD class=title>结束时间</TD>
				<TD class=input>
					<Input class="coolDatePicker" dateFormat="short" id="EndDay" name="EndDay" verify="结束时间|NOTNULL">
				</TD>
			</TR>
		</table>
	</Div>
	</p>

	</Div>

	<Div id="divMonthPrint" style="display: 'none'">
		<p>
			<label style="width:128px;"> 查询年月 :</label>
			<Input class="coolDatePicker" dateFormat="short" id="StartMonth" name="StartMonth" />
		</p>
	</Div>
	
	<table class=common>
	<tr class=common>
	<td class=input><input class=cssbutton type=button value="打  印" onclick="PrintData();"></td>
	<td></td><td></td>
	</tr>
	</table>

	<input type="hidden" id="fmtransact" name="fmtransact" />
	<input type="hidden" id="curManageCom" name="curManageCom" />
</form>

<span id="spanCode"	style="display: none; position:absolute; slategray"></span>
</body>
</html>
