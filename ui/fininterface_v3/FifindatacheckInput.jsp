<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FifindatacheckInput.jsp
 //程序功能：校验定义
 //创建日期：2011/9/15
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="Fifindatacheck.js"></SCRIPT>
  <%@include file="FifindatacheckInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FifindatacheckSave.jsp" method=post name=fm target="fraSubmit">
<Div id= "divVersionRule" style= "display: ''">
	<table>
    	<tr>
    		 <td class= titleImg>
        		财务规则版本
       		 </td>   		 
    	</tr>
    </table>
    <hr></hr>
	<td class=button width="10%" align=right>
		<INPUT class=cssButton name="querybutton" VALUE="版本信息查询"  TYPE=button onclick="return RulesVersionQuery();">
	</td>

<table class= common>
	<tr class= common>
		<TD class= title>版本编号</TD>
		<TD class=input>
		 	<input class=readonly name=VersionNo value = '' readonly=true>
		</TD>
		<TD class= title> 版本状态</TD>
		<TD class=input>
		 	<input class=readonly name=VersionState value = '' >
		</TD>
	</tr>	
	<input type=hidden name=StartDate>
	<input type=hidden name=EndDate>
	</table>
</div>
<hr></hr> 

<table>
	<input value="校验类型查询"   class="cssButton" type="button" onclick="QueryData();">
	<br>
	  <tr> 
	    <td class="titleImg" >校验类型定义</td>
	  </tr>
</table>

<table  class="common" >
  <tr class="common">
    <td class="title">校验类型</td><td class="input" ><input class="codeno" name="Ruletype" 
    CodeData="0|^01|质检规则|^02|差异规则" verify="校验类型|notnull"
    ondblclick="showCodeListEx('Ruletype',[this,RuletypeName],[0,1],null,null,null,1);"
	onkeyup="showCodeListKeyEx('Ruletype',[this,RuletypeName],[0,1],null,null,null,1);" readOnly  onpropertychange="changetype();"><input class="codename" name="RuletypeName"  readonly=true
	elementtype=nacessary>	</td> 
	<input class="common" name="Ruleid" type="hidden">						   
	<td id="td1" style="display:'';" class="title">校验节点</td><TD id="td2" style="display:'';" class= input><Input class=codeno name=CallPointID verify="校验节点|NOTNULL" 
      		ondblclick="return showCodeList('fichecknode',[this,CallPointIDName],[0,1],null,null,null,'1',null);" 
      		onkeyup="showCodeListKey('fichecknode',[this,CallPointIDName],[0,1],null,null,null,'1',null)" readonly><input 
      		class=codename name=CallPointIDName readonly=true elementtype=nacessary></TD>	
       <td class="title">规则状态</td><td class="input"><input class="codeno" name="RuleState" 
    CodeData="0|^01|启用|^02|未启用" verify="校验类型|notnull"
    ondblclick="return showCodeListEx('RuleState',[this,RuleStateName],[0,1],null,null,null,1);" 
    onkeyup="return showCodeListKeyEx('RuleState',[this,RuleStateName],[0,1],null,null,null,1);" readOnly><input class="codename" name="RuleStateName" ></td>  
 	
	</tr>
	<tr>
	<td class="title">规则名称</td><TD  class= input><input class=common name=RuleName elementtype=nacessary></TD>
  </tr>
</table>
<div id=button style="display:'none'">
<br>
	<INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
    <INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
    <INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
    <INPUT VALUE="重  置" TYPE=button class= cssbutton name= resetbutton onclick="return resetAgain()">
</div>
<hr></hr>
	<INPUT VALUE="校检规则明细定义" TYPE=button class= cssbutton name="addbutton" onclick="return DataClick();">   
</table>
<br>
<br>

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
