<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	//程序名称：FinInterfaceCheck.jsp
	//程序功能：凭证核对
	//创建日期：2007-10-23 
	//创建人  ：m
	//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="java.util.*"%>

<%
	//添加页面控件的初始化。
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>	
	var operator = "<%=tGI.Operator%>";   //记录操作员
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构
	var comcode = "<%=tGI.ComCode%>";//记录登陆机构
</script>
<head>
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryPrint.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>

<SCRIPT src="FISCertificateCheckInput.js"></SCRIPT>
<%@include file="FISCertificateCheckInit.jsp"%>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

</head>

<body onload="initForm();">
<form method=post name=fm target="fraSubmit">
<!-- 流水号码 --> 
<input type=hidden name=serialNo>
<input type=hidden name=ExportExcelSQL>
<input type=hidden name=ClassType>
<table>
  	<tr> 
  		<td class= titleImg>请输入查询条件</td>   		 
  	</tr>
</table> 
    
<Div id="divSearch" style="display: ''">
	<table class=common>
	<TR class=common>
	    <td class=title>批次号</td>
	    <td class=input><input class=common name="batchno" id="batchno"></td>
		<TD class=title>核对类型</TD>
		<TD class=input><Input class=codeno name=checkType
			CodeData="0|^1|科目|M^2|凭证类型|M^3|凭证号码|M" verify="核对类型|notnull"
			ondblclick="showCodeListEx('checkType',[this,checkTypeName],[0,1],null,null,null,1);"
			onkeyup="showCodeListKeyEx('checkType',[this,checkTypeName],[0,1],null,null,null,1);"><input
			class=codename name=checkTypeName readonly=true elementtype=nacessary><font color=red>*</font></TD>	
		<TD class=title></TD>	
		<TD class=input></TD>				
	</TR>
	</table>	
</Div>
	
<Div id="divSearch1" style="display:none"> 	
	<table class=common>
	<TR class=common>
		<TD class=title>科目类型</TD>
		<TD class=input><Input class=codeno name=accountCodeType
			CodeData="0|^1|资产^2|负债^3|过渡^4|表外^6|损益" verify="科目类型|notnull"
			ondblclick="showCodeListEx('accountCodeType',[this,accountCodeTypeName],[0,1]);"
			onkeyup="showCodeListKeyEx('accountCodeType',[this,accountCodeTypeName],[0,1]);"><input
			class=codename name=accountCodeTypeName readonly=true elementtype=nacessary></TD>	
		<TD  class= title>科目</TD>
    	<TD  class= input><Input class=codeno name=accountCode verify="科目|NOTNULL" 
      		ondblclick="return showCodeList('fiaccountcode',[this,accountName],[0,1],null,[fm.accountCodeType.value],['othersign'],'1',null);" 
      		onkeyup="showCodeListKey('fiaccountcode',[this,accountName],[0,1],null,[fm.accountCodeType.value],['othersign'],'1',null)"><input 
      		class=codename name=accountName readonly=true elementtype=nacessary></TD>	
		<TD class=title>借贷标志</TD>
		<TD class=input><Input class=codeno name=FinItemType
			CodeData="0|^D|借方|M^C|贷方|M" verify="科目类型|notnull"
			ondblclick="showCodeListEx('FinItemType',[this,FinItemTypeName],[0,1],null,null,null,1,null);"
			onkeyup="showCodeListKeyEx('FinItemType',[this,FinItemTypeName],[0,1],null,null,null,1,null);"><input
			class=codename name=FinItemTypeName readonly=true elementtype=nacessary></TD>	      			
	</TR>
	<TR class=common>
		<TD  class= title>管理机构</TD>
      	<TD  class= input><Input class=codeno name=ManageCom verify="管理机构|NOTNULL" 
      		ondblclick="return showCodeList('comcode2',[this,PolicyComName],[0,1],null,null,null,'1',null);" 
      		onkeyup="showCodeListKey('comcode2',[this,PolicyComName],[0,1],null,null,null,'1',null)"><input 
      		class=codename name=PolicyComName readonly=true elementtype=nacessary></TD>		
		<TD class=title>起始时间</TD>
		<TD class=input><Input class="coolDatePicker"
			verify="起始时间|notnull&date" dateFormat="short" name=StartDay elementtype=nacessary value = '2007-02-01' ></TD>
		<TD class=title>结束时间</TD>
		<TD class=input><Input class="coolDatePicker"
			verify="结束时间|notnull&date" dateFormat="short" name=EndDay elementtype=nacessary></TD>
	</TR>		
	</table>	
</Div>	
 
<Div id="divSearch4" style="display:none"> 	
	<table class=common>  
	<TR class=common>
		<TD class=title>业务号码类型</TD>
		<TD class=input><Input class=codeno name=ContType
			CodeData="0|^01|暂收号|^02|保单号|^03|实收号|^04|实付号|^05|结算批次号" verify="业务号码类型|notnull"
			ondblclick="showCodeListEx('FIContType',[this,ContTypeName],[0,1],null,null,null,1,null);"
			onkeyup="showCodeListKeyEx('FIContType',[this,ContTypeName],[0,1],null,null,null,1,null);"><input
			class=codename name=ContTypeName readonly=true elementtype=nacessary></TD>		
        <TD  class= title>业务号码</TD>
        <TD  class= input><Input class= common name=tNo></TD> 
        <TD  class= title></TD>
        <TD  class= input></TD>        			
	</TR>
	</table>	
</Div>

<Div id="divSearch2" style="display:none"> 	
	<table class=common>  
	<TR class=common>
		<TD class=title>凭证大类</TD>
		<TD  class= input>
      <Input class=codeno name="SClassType"
      		ondblclick="return showCodeList('fivouchertype',[this,SClassTypeName],[0,1],null,null,null,1,null);" 
      		onkeyup="showCodeListKey('fivouchertype',[this,SClassTypeName],[0,1],null,null,null,1,null)"><input 
      		class=codename name=SClassTypeName readonly elementtype=nacessary></TD>		
    </TD>	
    <TD class=title></TD>	
		<TD class=input></TD>		
		<TD class=title></TD>	
		<TD class=input></TD>	
  </TR>
  <TR> 
		<TD  class= title>管理机构</TD>
    <TD  class= input><Input class=codeno name=ManageCom1 verify="管理机构|NOTNULL"  
      	 ondblclick="return showCodeList('comcode2',[this,PolicyComName1],[0,1],null,null,null,'1',null);" 
      	 onkeyup="showCodeListKey('comcode2',[this,PolicyComName1],[0,1],null,null,null,'1',null)"><input 
      	 class=codename name=PolicyComName1 readonly=true elementtype=nacessary>
    </TD>	  	 
		<TD class=title>起始日期</TD>
		<TD  class= input>
      <Input class="coolDatePicker" dateFormat="short" name=SDate value = '2007-02-01'>
    </TD>
    <TD class=title>终止日期</TD>
		<TD  class= input>
      <Input class="coolDatePicker" dateFormat="short" name=EDate>
    </TD>
	</TR>
	</table>	
</Div>
<Div id="divSearch3" style="display:none"> 	
	<table class=common>  
	<TR class=common>	
        <TD  class= title>凭证号码</TD>
        <TD  class= input><Input class= common name=CertificateID></TD> 
        <TD  class= title></TD>
        <TD  class= input></TD>        		
        <TD  class= title></TD>
        <TD  class= input></TD>  	
	</TR>
	</table>	
</Div>
<br>
<INPUT VALUE="查  询" class=cssButton TYPE=button onclick="CheckQueryData();"> 
<INPUT VALUE="查询明细" class=cssButton TYPE=button onclick="CheckMXQueryData();">
<hr>
<table>
	<tr>
		<td class=common><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divAllGrid);"></td>
		<td class=titleImg>查询结果</td>
	</tr>
</table>

<Div id="divAllGrid" style="display: ''" align=center>
<table class=common>
	<tr class=common>
		<td text-align: left colSpan=1><!-- 日结试算生成的批次 --> <span id="spanCheckQueryDataGrid"> </span></td>
	</tr>
</table>
<INPUT CLASS=cssbutton VALUE="首  页" TYPE=button onclick="CheckQueryDataGridTurnPage.firstPage();"> 
<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="CheckQueryDataGridTurnPage.previousPage();">
<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="CheckQueryDataGridTurnPage.nextPage();">
<INPUT CLASS=cssbutton VALUE="尾  页" TYPE=button onclick="CheckQueryDataGridTurnPage.lastPage();">
</div>
<hr>

<table>
  	<tr> 
  		<td class= titleImg>审核查询结果</td> 
  		<td></td>  		 
  	</tr>
</table> 
<table  class=common>
	<tr class=common>
		<TD class="title">审核结论</TD>
		<TD class=input>
			<Input class=codeno name=FICertifyUW verify="审核结论|NOTNULL" 
			    ondblclick="return showCodeList('ficertifyuw',[this,FICertifyUWName],[0,1]);" 
				onkeyup="return showCodeList('ficertifyuw',[this,FICertifyUWName],[0,1]);" ><input class=codename name=FICertifyUWName readonly=true elementtype=nacessary>
		</TD>	
		<TD class="title"></TD>
		<TD class=input></TD>
		<TD class="title"></TD>
		<TD class=input></TD>
	</tr>
    <tr class=common>
		<td class=title>审核评论</td>
		<td class=input><textarea class="common" name="" cols="50%" rows="3" ></textarea></td>
    </tr>
</table>
<br>
<input type="button" value="提  交" class="cssbutton" onclick="alert('提交成功！')">


<!-- <INPUT VALUE="导出Excel" class=cssButton TYPE=button onclick="ToExcel();"> -->
<!--  <INPUT VALUE="打印" class=cssButton TYPE=button onclick="printFinInterface();"> -->
<br>
<br>

</form>

<span id="spanCode" style="display: none; position:absolute; slategray"></span>

</body>
</html>
