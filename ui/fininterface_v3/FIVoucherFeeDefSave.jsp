<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIVoucherFeeDefSave.jsp
//程序功能：凭证费用定义
//创建日期：2011/8/27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
 //接收信息，并作校验处理。
 request.setCharacterEncoding("GBK");
 //输入参数
 //××××Schema t××××Schema = new ××××Schema();
 FIFeeTypeDefUI tFIFeeTypeDefUI = new FIFeeTypeDefUI(); 
 FIVoucherBnFeeSet tFIFeeTypeDefSet = new FIVoucherBnFeeSet();
 //输出参数
 CErrors tError = null;
               
 String FlagStr = "";
 String Content = "";
 String transact = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getAttribute("GI");
 
 //执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
 String pageflag  = request.getParameter("pageflag"); 
 transact = request.getParameter("fmtransact");
 
 //客户需求服务
	String tVoucherFeeGridNum[] = request.getParameterValues("VoucherFeeGridNo");
	String versionno[] = request.getParameterValues("VoucherFeeGrid1");
	String voucherid[] = request.getParameterValues("VoucherFeeGrid2");            //告知版别
	String bustypeid[] = request.getParameterValues("VoucherFeeGrid10");           //告知编码
	String feeid[] = request.getParameterValues("VoucherFeeGrid4"); 
	String feename[] = request.getParameterValues("VoucherFeeGrid5"); 
	String d_finitemid[] = request.getParameterValues("VoucherFeeGrid6"); 
	String d_finitemname[] = request.getParameterValues("VoucherFeeGrid7"); 
	String c_finitemid[] = request.getParameterValues("VoucherFeeGrid8"); 
	String c_finitemname[] = request.getParameterValues("VoucherFeeGrid9"); 
	int m = 0;
	    if (versionno != null) 
	    	m = versionno.length;
	     System.out.println("----财务规则信息----");
	     System.out.println("Mulline 行数 ："+m);
	    for (int i = 0; i < m; i++)	
			{  
	    		FIVoucherBnFeeSchema tFIFeeTypeDefSchema = new FIVoucherBnFeeSchema(); 
 				//存放Mulline中的数据
 				tFIFeeTypeDefSchema.setVersionNo(versionno[i]); 
 				System.out.println("versionno---------"+versionno[i]);                                                            
				tFIFeeTypeDefSchema.setVoucherID(voucherid[i]);  
				System.out.println("voucherid---------"+voucherid[i]);                                                             
				tFIFeeTypeDefSchema.setBusinessID(bustypeid[i]) ;  
				System.out.println("bustypeid---------"+bustypeid[i]);                                                            
				tFIFeeTypeDefSchema.setFeeID(feeid[i]) ;  		
				System.out.println("feeid---------"+feeid[i]);    		
 				tFIFeeTypeDefSchema.setFeeName(feename[i]);
 				System.out.println("feename---------"+feename[i]);                                                                
				tFIFeeTypeDefSchema.setD_FinItemID(d_finitemid[i]);   
				System.out.println("d_finitemid---------"+d_finitemid[i]);				                                                          
				tFIFeeTypeDefSchema.setD_FinItemName(d_finitemname[i]) ;   
				System.out.println("d_finitemname---------"+d_finitemname[i]);                                                          
				tFIFeeTypeDefSchema.setC_FinItemID(c_finitemid[i]) ;
				System.out.println("c_finitemid---------"+c_finitemid[i]);
				tFIFeeTypeDefSchema.setC_FinItemName(c_finitemname[i]) ;
				System.out.println("c_finitemname---------"+c_finitemname[i]);
				tFIFeeTypeDefSchema.setState("01") ;
				System.out.println("State---------01");
				//其他数据从后台获取
				tFIFeeTypeDefSet.add(tFIFeeTypeDefSchema);
			}

 

  //准备传输数据VData
  VData tVData = new VData();
  

  
  tVData.add(tG);
 if ("C".equals(pageflag)) {
 
			tVData.add(tFIFeeTypeDefSet);
		try {

			tFIFeeTypeDefUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
	}

	if ("X".equals(pageflag)) {
	}

 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr==""){
  tError = tFIFeeTypeDefUI.mErrors;
  if (!tError.needDealError()){                          
   Content = " 保存成功! ";
   FlagStr = "Success";
  }
  else                                                                           {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
