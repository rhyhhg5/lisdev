<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK" %>
<%
//程序名称：FIBusFeeTypeDefSave.jsp
//程序功能：业务交易费用定义
//创建日期：2011/8/25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="org.apache.commons.fileupload.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%
 //接收信息，并作校验处理。
 PubFun pub = new PubFun();
 ReadFinModelImport RI = new ReadFinModelImport();
 //输入参数
 //××××Schema t××××Schema = new ××××Schema();
 String FileName = "";
 String Path_Fn = "";
 int count = 0;
	
	//得到excel文件的保存路径
 	String ImportPath = request.getParameter("ImportPath");		
 	String path = application.getRealPath("").replace('\\','/')+'/';
 	String tFItemplateType = request.getParameter("FItemplateType");
 	String tVersionNo = request.getParameter("VersionNo");
 	
	System.out.println("ImportPath: "+ImportPath);
	System.out.println("Path: "+path);
	System.out.println("FItemplateType: "+tFItemplateType);
	System.out.println("VersionNo: "+tVersionNo);

	TransferData tTransferData = new TransferData();
	tTransferData.setNameAndValue("FItemplateType",tFItemplateType);
	tTransferData.setNameAndValue("VersionNo",tVersionNo);
	VData tVData = new VData();
	tVData.add(tTransferData);
		
DiskFileUpload fu = new DiskFileUpload();
// 设置允许用户上传文件大小,单位:字节
fu.setSizeMax(10000000);
// maximum size that will be stored in memory?
// 设置最多只允许在内存中存储的数据,单位:字节
fu.setSizeThreshold(4096);
// 设置一旦文件大小超过getSizeThreshold()的值时数据存放在硬盘的目录
fu.setRepositoryPath(path+"temp");
//开始读取上传信息

List fileItems = null;
try{
 
 fileItems = fu.parseRequest(request);
 
}
catch(Exception ex)
{
	ex.printStackTrace();
}

// 依次处理每个上传的文件 
Iterator iter = fileItems.iterator();
while (iter.hasNext()) {
  FileItem item = (FileItem) iter.next();
  //忽略其他不是文件域的所有表单信息
  if (!item.isFormField()) {
    String name = item.getName(); 
    long size = item.getSize();
    if((name==null||name.equals("")) && size==0)
      continue;
    ImportPath= path + ImportPath;
    FileName = name.substring(name.lastIndexOf("\\") + 1);
    FileName = FileName.substring(0,FileName.length()-4);
    
    System.out.println("-----------ImportPath:"+ ImportPath);
   
    System.out.println("-----------importpath:   "+FileName+pub.getCurrentDate()+".xls");
    String time = pub.getCurrentTime().split(":")[0]+"_"+pub.getCurrentTime().split(":")[1]+"_"+pub.getCurrentTime().split(":")[2];
    FileName = FileName+pub.getCurrentDate()+"_"+time+".xls";
    Path_Fn = ImportPath + FileName;
    //保存上传的文件到指定的目录
    
    try {
      item.write(new File(Path_Fn));
      count = 1;
       System.out.println("count="+ count);
    } catch(Exception e) {
      System.out.println("upload file error ...");
    }
  }
}
 //输出参数
 CErrors tError = null;           
 String FlagStr = "";
 String Content = "";
 
 GlobalInput tG = new GlobalInput(); 
 tG=(GlobalInput)session.getAttribute("GI");
 
 
 //从url中取出参数付给相应的schema
 //t××××Schema.set××××(request.getParameter("××××"));
 
 try{
  
  //传输schema
  //tVData.addElement(t××××Schema);
  
  	tVData.add(tG); 
 	RI.submitData(tVData,Path_Fn);   
 } 
	 catch(Exception ex){
	  Content = "保存失败，原因是:" + ex.toString();
	  FlagStr = "Fail";
 }
 
 //如果在Catch中发现异常，则不从错误类中提取错误信息
 if (FlagStr==""){
  tError = RI.mErrors;
  if (!tError.needDealError()){                          
   Content = " 保存成功! ";
   FlagStr = "Success";
  }
  else                                                                           {
   Content = " 保存失败，原因是:" + tError.getFirstError();
   FlagStr = "Fail";
  }
 }
 
 //添加各种预处理
%>                      
<%=Content%>
<html>
<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
