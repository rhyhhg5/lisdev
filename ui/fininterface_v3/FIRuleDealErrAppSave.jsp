<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：FIRuleDealErrAppSave.jsp
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%>
  <%@page import="com.sinosoft.lis.fininterface_v3.*" %>
  <%@page import="com.sinosoft.lis.fininterface_v3.tools.sequence.*" %>
  <%@page contentType="text/html;charset=GBK" %>
<%
	request.setCharacterEncoding("GBK");
 
	String tApplicant=request.getParameter("Applicant");
	String tBusTypeCode=request.getParameter("BusTypeCode");
	String tErroeType=request.getParameter("ErroeType");
	String tBusinessNo=request.getParameter("BusinessNo");
	String tDetailIndexID=request.getParameter("DetailIndexID");
	String tDetailReMark=request.getParameter("DetailReMark");
	String tCheckType = request.getParameter("CheckType");
	String tCQNo = request.getParameter("CQNo");
	String tProcessNode = request.getParameter("ProcessNode");
  
  
	String tRuleDealBatchNo = FinCreateSerialNo.getLogSerialNo();
	
	String tEventNo = FinCreateSerialNo.getEventNo();
	String tErrSerialNo = FinCreateSerialNo.getErrLogSerialNo();
	
	System.out.println("tApplicant==============" + tApplicant);
	System.out.println("tBusTypeCode==============" + tBusTypeCode);
	System.out.println("tErroeType==============" + tErroeType);
	System.out.println("tBusinessNo==============" + tBusinessNo);
	System.out.println("tDetailIndexID==============" + tDetailIndexID);
	System.out.println("tDetailReMark==============" + tDetailReMark);
	System.out.println("tCheckType==============" + tCheckType);
	System.out.println("tRuleDealBatchNo==============" + tRuleDealBatchNo);
	System.out.println("tEventNo==============" + tEventNo);
	System.out.println("tProcessNode==============" + tProcessNode);
  
  

	String FlagStr = "Fail";
	String Content = "";
	
	GlobalInput tG = new GlobalInput(); 
	tG=(GlobalInput)session.getAttribute("GI");
	
	FIRuleDealErrLogSchema tFIRuleDealErrLogSchema = new FIRuleDealErrLogSchema();
	FIRuleDealLogSchema tFIRuleDealLogSchema = new FIRuleDealLogSchema();
	FIRuleDealErrAppUI tFIRuleDealErrAppUI = new FIRuleDealErrAppUI();
  
	//从url中取出参数付给相应的schema
	tFIRuleDealLogSchema.setVersionNo("00000000000000000001");
	tFIRuleDealLogSchema.setCheckBatchNo(tRuleDealBatchNo);
	tFIRuleDealLogSchema.setEventNo(tEventNo);
	tFIRuleDealLogSchema.setCheckType(tCheckType);
	tFIRuleDealLogSchema.setDataSource("2");//人工录入
	tFIRuleDealLogSchema.setRuleResult("Fail");
	tFIRuleDealLogSchema.setOperator(tApplicant);
	tFIRuleDealLogSchema.setMakeDate(PubFun.getCurrentDate());
	tFIRuleDealLogSchema.setMakeTime(PubFun.getCurrentTime());
	
	tFIRuleDealErrLogSchema.setErrSerialNo(tErrSerialNo);
	tFIRuleDealErrLogSchema.setCheckBatchNo(tRuleDealBatchNo);
	tFIRuleDealErrLogSchema.setAserialno(tEventNo);
	tFIRuleDealErrLogSchema.setEventNo(tEventNo);
	tFIRuleDealErrLogSchema.setRuleID("0000000000");
	tFIRuleDealErrLogSchema.setErrInfo(tDetailReMark);
	tFIRuleDealErrLogSchema.setRulePlanID("0000000000");
	tFIRuleDealErrLogSchema.setDealState("0");
	tFIRuleDealErrLogSchema.setCertificateID(tBusTypeCode);
	tFIRuleDealErrLogSchema.setBusinessNo(tBusinessNo);
	tFIRuleDealErrLogSchema.setIndexCode(tDetailIndexID);
	tFIRuleDealErrLogSchema.setCQNo(tCQNo);
	tFIRuleDealErrLogSchema.setCallPointID(tProcessNode);
  
	
	VData oData = new VData();
	oData.add(tFIRuleDealErrLogSchema);
	oData.add(tFIRuleDealLogSchema);
	oData.add(tG);
  
	if(tFIRuleDealErrAppUI.submitData(oData,"INSERT"))
	{
	   //处理成功
	   System.out.println("Del Succ");
	   Content = " 保存成功! ";
	   FlagStr = "Succ";
	}else{
		System.out.println("Del Fail");
		Content = " 保存失败，原因是:" + tFIRuleDealErrAppUI.mErrors.getFirstError();
		FlagStr = "Fail";
	}

%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit('<%=FlagStr%>','<%=Content%>');
</script>
</html>

