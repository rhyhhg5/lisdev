//该文件中包含客户端需要处理的函数和事件

//程序名称：Fifindatacheck.js
//程序功能：元数据定义
//创建日期：2011/9/15
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();


//节点
function Code_T()
{
	var strSQL = "select codename from ldcode where codetype = 'fichecknode' and code='"+fm.Code_T.value+"' ";	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//判断是否查询成功
  	if (!turnPage.strQueryResult) 
  	{
	    alert("查询失败！");
	    return false;
    }  
	//查询成功则拆分字符串，返回二维数组
  	arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	return arrSelected;
}
//错误类型
function errtype()
{	
	var strSQL = "select code,codename from ldcode where codetype = 'fierrortypea' and code=(select errtype from FIVerifyRules where VersionNo='"+fm.VersionNo.value+"' and Ruledefid='"+fm.ParentRuleID.value+"') ";	
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) 
	{
	    alert("查询失败！");
	    return false;
    }  
	//查询成功则拆分字符串，返回二维数组
  	arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	return arrSelected;
}
//删除按钮
function button144(){
}

//修改按钮
function button145(){
}
function RuleDY()
{
	showInfo=window.open("./RuleInsert.jsp");
}

//查询按钮
function initQuery1()
{
	var strSQL = "select '"+fm.ruledefid.value+"',(select rulename from FIVerifyRuleDef b where b.versionno='"+fm.VersionNo.value+"' and b.ruledefid='"+fm.ruledefid.value+"'),ArithmeticID,ArithmeticName,subtype,(case subtype when '00' then '完整性校检' when '01' then '正确性校检' else '一致性校检' end) subtypename,node,(case node when '00' then '开始节点' when '99' then '结束节点' else '中间节点' end) nodetype,parentnode,(case state when '1' then '有效' when '0' then '无效' end) from FIVerifyRules a where VersionNo='"+fm.VersionNo.value+"' and ruledefid='"+fm.ruledefid.value+"'";
 	turnPage.queryModal(strSQL,DataRuleGrid);
}

function getData()
{
	var tSel = DataRuleGrid.getSelNo();
	if(tSel>0)
	{		
	     var strSQL = "select ArithmeticID,ArithmeticType,(case ArithmeticType when '01' then '校验规则' else '质检规则' end),subtype,(case subtype when '00' then '完整性校检' when '01' then '正确性校检' else '一致性校检' end) subtypename,"
	     strSQL=strSQL+" ErrType,(select codename from ldcode where codetype = 'fierrortypea' and code=ErrType),dealmode,(case DealMode when '1' then '类处理' when '2' then 'SQL处理' else '文件校验规则处理' end),node,(case node when '00' then '开始节点' when '99' then '结束节点' else '中间节点' end) nodetype,parentnode, "
	     strSQL=strSQL+"ArithmeticName,replace(FinDealSQL,'|','@') FinDealSQL,replace(BusDealSQL,'|','@') BusDealSQL,state from FIVerifyRules where VersionNo='"+fm.VersionNo.value+"' and ruledefid='"+fm.ruledefid.value+"' and ArithmeticID='"+DataRuleGrid.getRowColData(tSel-1,3)+"'";
	     turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
  	     //判断是否查询成功
  	     if (!turnPage.strQueryResult) {
	      alert("查询失败！");
	      return false;
         }  
	    //查询成功则拆分字符串，返回二维数组
  	    arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
  	    arrSelected[0][13]=arrSelected[0][13].split("@@").join("||");
		arrSelected[0][13]=arrSelected[0][13].split("@").join("|");
		arrSelected[0][14]=arrSelected[0][14].split("@@").join("||");
		arrSelected[0][14]=arrSelected[0][14].split("@").join("|");
  	    fm.ArithmeticID.value=arrSelected[0][0];
  	    fm.Codetype.value=arrSelected[0][1];
  	    fm.CodetypeName.value=arrSelected[0][2];
  	    fm.SubType.value=arrSelected[0][3];
  	    fm.SubTypeName.value=arrSelected[0][4];
  	    fm.errtype.value=arrSelected[0][6];
  	    fm.dealmode.value=arrSelected[0][7];
  	    fm.dealmodeName.value=arrSelected[0][8];
  	    fm.NodeType.value=arrSelected[0][9];
  	    fm.NodeTypeName.value=arrSelected[0][10];
  	    fm.ParentNode.value=arrSelected[0][11];
  	    fm.ReturnRemark.value=arrSelected[0][12];
  	    fm.FIDealSQL.value=arrSelected[0][13];
  	    fm.BuDealSQL.value=arrSelected[0][14];
  	    fm.State.value=arrSelected[0][15];

	}	
}

//关闭按钮
function closed()
{
	top.close();	
}


//备用定义按钮
function button147(){
}


//提交前的校验、计算  
function beforeSubmit(){
  //添加操作 
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();  
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
		showDiv(operateButton,"true"); 
		showDiv(inputButton,"false"); 
		//执行下一步操作
	}
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";  
	}
}
