//               该文件中包含客户端需要处理的函数和事件
var showInfo;
var mDebug="0";
var turnPage = new turnPageClass();          //使用翻页功能，必须建立为全局变量
var confirmFlag=false;

var todayDate = new Date();
var sdate = todayDate.getDate();
var smonth= todayDate.getMonth() +1;
var syear= todayDate.getYear();
var sToDate = syear + "-" +smonth+"-"+sdate

//提交，保存按钮对应操作
function submitForm()
{   
	var strSQL = "select VersionNo,RuleDefID,RuleName,ReturnRemark,RuleState from FIVerifyRuleDef where 1=1 and RuleType = '02'";
 
	fm.strSQLValue.value = strSQL;          
	turnPage.queryModal(strSQL, ErrLogGrid);
	
	if(ErrLogGrid.mulLineCount < 1){
		alert("没有找到对应的数据");
		return ;  	
	} 
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
  }
  else
  { 

    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   

    //showDiv(operateButton,"true"); 
    //showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}



//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
  try
  {
	  initForm();
  }
  catch(re)
  {
  	alert("在Proposal.js-->resetForm函数中发生异常:初始化界面错误!");
  }
}     

//返回按钮对应的操作
function returnParent()
{
  var arrReturn = new Array();
	var tSel = ErrLogGrid.getSelNo();

	if( tSel == 0 || tSel == null )
		//top.close();
		alert( "请您先选择一条记录，再点击返回按钮。" );
	else
	{

			try
			{
				arrReturn = getQueryResult();
				top.opener.afterQuery( arrReturn );
			}
			catch(ex)
			{
				//alert( "没有发现父窗口的afterQuery接口。" + ex );
				alert(ex.message);
			}
			top.close();

	}
}

function getQueryResult()
{
	var arrSelected = null;
	tRow = ErrLogGrid.getSelNo();
	if( tRow == 0 || tRow == null )
	{
	  return arrSelected;
	}
	arrSelected = new Array();

	var strSQL = "";	
	strSQL = "select RuleDefID,RuleName,ReturnRemark from FIVerifyRuleDef where 1=1 ";
	strSQL = strSQL + " and RuleDefID ='"+ErrLogGrid.getRowColData(tRow-1,2)+"' ";
	turnPage.strQueryResult  = easyQueryVer3(strSQL, 1, 0, 1);
	//判断是否查询成功
	if (!turnPage.strQueryResult) 
	{
    	alert("查询失败！");
    	return false;
    }  
	//查询成功则拆分字符串，返回二维数组
	arrSelected = decodeEasyQueryResult(turnPage.strQueryResult);
	return arrSelected;
}

function clearshowInfo()
{  
  
  showInfo.close();
}

function mf()
{  
 //fm.
}