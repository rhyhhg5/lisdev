<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FIMetaDataDefInput.jsp
 //程序功能：元数据定义
 //创建日期：2011/9/13
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="FIMetaDataAttDef.js"></SCRIPT>
  <%@include file="FIMetaDataAttDefInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FIMetaDataDefSave.jsp" method=post name=fm target="fraSubmit">
<br>
<div align = left>
	<table>
		<tr>
			<td class="titleImg" >元数据属性列表</td>
		</tr>
	</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanMetadataAttGrid" >
     </span> 
      </td>
   </tr>
</table>

<INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
<INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
<INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
<INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
</div>
<br>
<table class=common>
	<tr class=common>
		<td class=title>属性编码</td>
		<td class=input>
			<input class=common name=AttNo elementtype=nacessary>
		</td>
		<td class=title>属性名称</td>
		<td class=input>
			<input class=common name=AttName elementtype=nacessary>
		</td>
		<td class=title></td>
		<td class=input></td>		
	</tr>
	<tr class=common>
		<td class=title>是否配置值域</td>
		<td class="input"><Input class=codeno name= AttType 
				ondblClick="showCodeList('fiattype',[this,AttTypeName],[0,1],null,null,null,[1]);" 
				onkeyup="showCodeListKey('fiattype',[this,AttTypeName],[0,1],null,null,null,[1]);" readonly=true><input class=codename name=AttTypeName readonly=true elementtype=nacessary> 
		</td>
		<td class=title>属性状态</td>
		<td class="input"><Input class=codeno name= State 
				ondblClick="showCodeList('fiattstate',[this,StateName],[0,1],null,null,null,[1]);" 
				onkeyup="showCodeListKey('fiattstate',[this,StateName],[0,1],null,null,null,[1]);" readonly=true><input class=codename name=StateName readonly=true elementtype=nacessary> 
		</td>
		<td class=title></td>
		<td class=input></td>		
	</tr>
</table>
<br>
<INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
<INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();">
<INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">
<INPUT VALUE="重  置" TYPE=button class= cssbutton name= resetbutton onclick="initForm()">
<hr>
<input value="值域定义"  onclick="gotodefin()" class="cssButton" type="button" >
<br>

<input type=hidden name=hideOperate>
<input type=hidden name=MetadataNo>

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
