//该文件中包含客户端需要处理的函数和事件

//程序名称：FIBusInfoDef.js
//程序功能：业务交易信息定义
//创建日期：2011/8/25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

var Mul9GridTurnPage = new turnPageClass(); 
var AfterModBusGridTurnPage = new turnPageClass(); 

function initpage()
{
	initBusTypeInfoGrid();
	if("C"==pageflag)
	{
		var sql="select BusinessID,busclass,busclassname,propertycode,propertyname,remark,(select codename from ldcode where codetype = 'fiusage' and code = purpose),busvalues from FIBnTypeInfo where BusinessID = '"+BusTypeId+"' and purpose = '02'";
	}
	if("X"==pageflag)
	{
		var sql="select BusinessID,busclass,busclassname,propertycode,propertyname,remark,(select codename from ldcode where codetype = 'fiusage' and code = purpose),busvalues from FMBnTypeInfo where BusinessID = '"+BusTypeId+"' and maintno = '"+maintno+"' and purpose = '02'";
	}
	
	turnPage.queryModal(sql, BusTypeInfoGrid);
}

//选定一条数据
function dataselect()
{
	var tSel = BusTypeInfoGrid.getSelNo();
	fm.all('FIBusInfoID').value = BusTypeInfoGrid.getRowColData(tSel-1,4);
	fm.all('FIBusInfoIDName').value = BusTypeInfoGrid.getRowColData(tSel-1,5);
	fm.all('FIBusRemark').value = BusTypeInfoGrid.getRowColData(tSel-1,6);
	//fm.all('PurPose').value = BusTypeInfoGrid.getRowColData(tSel-1,7);
	fm.all('FeeValues').value = BusTypeInfoGrid.getRowColData(tSel-1,8);
	
	showCodeName();
	
}

function superaddClick()
{
	if((fm.all('FeeValuesB').value == null)||(fm.all('FeeValuesB').value == ''))
	{
		alert("该项不得为空，请重新填写！");
		fm.all('FeeValuesB').value = '';
		fm.all('FeeValuesBName').value = '';
		return false;
	}
	if((fm.all('FeeValues').value == null)||(fm.all('FeeValues').value == ''))
	{
		fm.all('FeeValues').value = fm.all('FeeValuesB').value;
		fm.all('FeeValuesB').value = '';
		fm.all('FeeValuesBName').value = '';
	}
	else
	{
		fm.all('FeeValues').value = fm.all('FeeValues').value + ";" +fm.all('FeeValuesB').value;
		fm.all('FeeValuesB').value = '';
		fm.all('FeeValuesBName').value = '';
	}
}//add by fx

function clearClick()
{
	fm.all('FeeValuesB').value = '';
	fm.all('FeeValuesBName').value = '';
	fm.all('FeeValues').value = '';
}//add by fx

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null){
	try
	{
		showInfo.focus();  
	}
	catch(ex)
	{
		showInfo=null;
	}
}
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
	
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	
		location.reload();
		
		showDiv(operateButton,"true"); 
		showDiv(inputButton,"false"); 
		
		//执行下一步操作
		
	}
	
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";  
	}
}

//增加按钮
function addClick()
{
	//校验参数完整性
	if(!beforeSubmit())
	{
		return false;	
	}
	fm.all('transact').value='insert';
	
	//校验是否为新增数据
//	var checksql = "select count(*) from FMBusTypeInfo where bustypeid = '"+BusTypeId+"' and maintno = '"+maintno+"' and propertycode = '"+fm.all('FIBusInfoID').value+"'";
	var checksql = "select count(*) from FIBnTypeInfo where BusinessID = '"+BusTypeId+"' and propertycode = '"+fm.all('FIBusInfoID').value+"' and Purpose='02' ";
	//alert(checksql);
	var reuslt=easyExecSql(checksql);
	if(reuslt!="0")
	{
		alert("该信息编码已存在，请确认！");
		return false;		
	}
	
	submitform();
}

//修改按钮
function deleteClick()
{
	//校验参数完整性
	if(!beforeSubmit())
	{
		return false;	
	}
	
	fm.all('transact').value='delete';
	
	//校验是否为新增数据
//	var checksql = "select count(*) from FMBusTypeInfo where bustypeid = '"+BusTypeId+"' and maintno = '"+maintno+"' and propertycode = '"+fm.all('FIBusInfoID').value+"'";
	var checksql = "select count(*) from FIBnTypeInfo where BusinessID = '"+BusTypeId+"'  and propertycode = '"+fm.all('FIBusInfoID').value+"' and Purpose='02' ";
	
	var reuslt=easyExecSql(checksql);
	if(reuslt=="0")
	{
		alert("该信息编码不存在，请确认！");
		return false;		
	}
	
	submitform();
}

//提交前校验
function beforeSubmit()
{
	if(fm.all('FIBusInfoID').value=="")
	{
		alert("信息编码不能为空！");
		return false;
	}
	if(fm.all('PurPose').value=="")
	{
		alert("信息用途不能为空！");
		return false;
	}

	return true;
}
//提交页面
function submitform()
{
	fm.action="./FIBusInfoDefSave.jsp";
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");    
	fm.submit();
}


