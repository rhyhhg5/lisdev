<%
//Creator :范昕	
//Date :2008-08-05
%>
<!--用户校验类-->

<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import = "com.sinosoft.utility.*"%>
<%@page import = "com.sinosoft.lis.schema.*"%>
<%@page import = "com.sinosoft.lis.vschema.*"%>
<%@page import = "com.sinosoft.lis.db.*"%>
<%@page import = "com.sinosoft.lis.vdb.*"%>

<%
%>
<script language="JavaScript">
<%
	GlobalInput globalInput = (GlobalInput)session.getValue("GI");
	
	if(globalInput == null) {
		out.println("网页超时，请重新登录");
		return;
	}
	String strOperator = globalInput.Operator;
%>   
function initInpBox()
{
  try
  {    
    fm.all('operateCom').value = '';
    fm.all('caiwucode').value = '';  
    fm.all('caiwuname').value = '';   
  }
  catch(ex)
  {
    alert("进行初始化是出现错误！！！！");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initBFGrid();
  }
  catch(re)
  {
    alert("BFDepartmentCodeNewInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}

function initBFGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=100;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="业务机构编码";    	//列名
      iArray[1][1]="60px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="业务机构名称";         			//列名
      iArray[2][1]="60px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[3]=new Array();
      iArray[3][0]="财务机构名称";         			//列名
      iArray[3][1]="80px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许
      
      iArray[4]=new Array();
      iArray[4][0]="财务机构编码";         			//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      BFGrid = new MulLineEnter( "fm" , "BFGrid" ); 
      BFGrid.mulLineCount = 0;   
      BFGrid.displayTitle = 1;
      BFGrid.canSel=1;
      BFGrid.locked = 1;	
      BFGrid.hiddenPlus = 1;
      BFGrid.hiddenSubtraction = 1;
      BFGrid.loadMulLine(iArray);  
      BFGrid.selBoxEventFuncName = "BankSelect";
      }
      catch(ex)
      {
        alert(ex);
      }
}
</script>