<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：VersionRuleQuery.jsp
//程序功能：版本信息查询页面
//创建日期：
//创建人  
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./CommonRuleQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./CommonRuleQueryInit.jsp"%>

<title>校检规则查询</title>
</head>
<body onload="initForm();initElementtype();">
<form  method=post name=fm target="fraSubmit">
  
<table>
    <tr class=common>
	    <td class=common>
	    	<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFIRulesVersion1);"></IMG>
	    	<td class=titleImg>查询条件 </td>
	    </td>
    </tr>
</table>

<Div  id= "divFIRulesVersion1" style= "display: ''">
	<table  class= common>
		<tr class= common>
			<TD class= title> 版本编号</TD>
			<TD class=input>
				 <Input class=common name=VersionNo >
			</TD>
			<TD class= title>规则编码</TD>
			<TD class=input>
				 <Input class="common" name=RuleID >
			</TD>
	         <TD class= title>规则名称</TD>
	         <TD class= input>
	            <Input class="common"  name=RuleName >
	         </TD>          
		</tr>
				
		<tr class= common>
			<TD  class= title>处理方式</TD>
			<TD  class= input>
				<input class="codeno" name="RuleDealMode" 
					ondblclick="return showCodeList('firulemode',[this,RuleDealModeName],[0,1]);" 
					onkeyup="return showCodeListKey('firulemode',[this,RuleDealModeName],[0,1]);" nextcasing=><input class="codename" name="RuleDealModeName" >
			</TD>          
			<td class="title">规则状态</td>
			<td class="common">
				<input class="codeno" name="RuleState" 
					ondblclick="return showCodeList('firulestate',[this,RuleStateName],[0,1]);" 
					onkeyup="return showCodeListKey('firulestate',[this,RuleStateName],[0,1]);" nextcasing=><input class="codename" name="RuleStateName" >				
			</td>
		</tr>	
	</table>
<br>	
    <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
    <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
</div>	
<br>        
<table>    	
    <tr>
		<td class=common>
			<IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFIRulesVersion2);">
    	</td>
    	<td class= titleImg> 版本信息查询结果</td>
    	</tr>
</table>
  
<Div  id= "divFIRulesVersion2" style= "display: ''" align=center>
   	<table  class= common>
    		<tr  class= common>
   	  	<td text-align: left colSpan=1>
				 <span id="spanCommonRuleGrid" >
				 </span>
		  	</td>
		</tr>
 	</table>
    <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
    <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
    <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
    <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
</div>  

</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

