<%
  //程序名称：FIMetaDataDefInit.jsp
  //程序功能：元数据定义
  //创建日期：2011/9/13
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">
var tMetaDataNo = '<%=request.getParameter("MetadataNo")%>';


function initForm(){
	try
	{
		fm.AttNo.value='';
		fm.AttName.value='';
		fm.AttType.value='';
		fm.AttTypeName.value='';
		fm.State.value='';
		fm.StateName.value='';
		
		initMetadataAttGrid();
		InitPage();
		fm.MetadataNo.value = tMetaDataNo;
	}
	catch(re){
		alert("FIMetaDataDefInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}


function initMetadataAttGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="元数据编号";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="属性编码";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="属性名称";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="是否含有值域";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=0;
		
		iArray[5]=new Array();
		iArray[5][0]="属性状态";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=0;
		
		iArray[6]=new Array();
		iArray[6][0]="属性类型";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=3;
		
		iArray[7]=new Array();
		iArray[7][0]="属性状态";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=3;


		MetadataAttGrid = new MulLineEnter( "fm" , "MetadataAttGrid" ); 

		MetadataAttGrid.mulLineCount=1;
		MetadataAttGrid.displayTitle=1;
		MetadataAttGrid.canSel=1;
		MetadataAttGrid.canChk=0;
		MetadataAttGrid.hiddenPlus=1;
		MetadataAttGrid.hiddenSubtraction=1;
		MetadataAttGrid.selBoxEventFuncName="getattdata";

		MetadataAttGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
