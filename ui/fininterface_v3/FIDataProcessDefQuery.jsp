<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
//程序名称：FinItemDefQuery.jsp
//程序功能：科目类型定义查询界面
//创建日期：2008-08-11
//创建人  ：ZhongYan
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page contentType="text/html;charset=GBK" %>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/Common.js" ></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="./FIDataProcessDefQuery.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
<%@include file="./FIDataProcessDefQueryInit.jsp"%>

<script>
	var VersionNo = '<%=request.getParameter("VersionNo")%>';
	var VersionState = '<%=request.getParameter("VersionState")%>';
</script>

<title>科目类型定义</title>
</head>
<body onload="initForm();initElementtype();">
  <form  method=post name=fm target="fraSubmit">
  
  <table>
    <tr class=common>
    <td class=common>
    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFIFinItemDef1);">
    </IMG>
    <td class=titleImg>
      查询条件
      </td>
    </td>
    </tr>
  </table>
    <Div  id= "divFIFinItemDef1" style= "display: ''">
<table class= common border=0 width=100%>
	<tr class= common>
		<TD class= title>规则编号</TD>
		<TD class=input>
				<Input class=common name=ProcessID >
		</TD>
		<TD class= title>规则名称</TD>
	    <TD class= input>                                            
	  		 <Input class=common name=ProcessName >
	    </TD>				
	</tr>
	<tr class= common>
		<TD class= title>规则类型</TD>
	    <TD class= input>
			<Input class=codeno name= ProcessType readonly=true   CodeData="0|^1|生成虚拟数据^2|现金流匹配^3|账务日期确定" ondblClick="showCodeListEx('FinItemType',[this,FinItemTypeMame],[0,1],null,null,null,[1]);" onkeyup="showCodeListKeyEx('FinItemType',[this,FinItemTypeMame],[0,1],null,null,null,[1]);"><input class=codename name=FinItemTypeMame readonly=true  >
	    </TD>
		<TD class= title>规则对象</TD>
		<TD class=input>
			<Input class=codeno name= ProcessObject readonly=true  CodeData="0|^1|业务类型^2|费用类型" ondblClick="showCodeListEx('FinItemType',[this,FinItemTypeMame1],[0,1],null,null,null,[1]);" onkeyup="showCodeListKeyEx('FinItemType',[this,FinItemTypeMame1],[0,1],null,null,null,[1]);"><input class=codename name=FinItemTypeMame1 readonly=true  >
		</TD>		    		    
	</tr>
	<tr class= common>
		<TD class= title>处理方式</TD>
	    <TD class= input>
	  		<Input class=codeno name= RuleDealMode readonly=true ondblClick="showCodeList('ruledealmode',[this,RuleDealModeName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKey('ruledealmode',[this,RuleDealModeName],[0,1],null,null,null,[1]);"><input class=codename name=RuleDealModeName readonly=true  >
	    </TD>			
	    <TD class= title>规则状态</TD>
	    <TD class= input>
	  		<Input class=codeno name= RuleState readonly=true ondblClick="showCodeList('rulestate',[this,ExtTypeName],[0,1],null,null,null,[1]);" onkeyup="showCodeListKey('rulestate',[this,ExtTypeName],[0,1],null,null,null,[1]);"><input class=codename name=ExtTypeName readonly=true  >
	    </TD>	    		    
	</tr>
	<tr class= common>
	</tr>
</table>         
<br>
          <INPUT VALUE="查  询" TYPE=button onclick="easyQueryClick();" class="cssButton">
          <INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
     </div>	
          
          
    <table>    	
    	<tr>
        	<td class=common>
			    <IMG  src= "../common/images/butExpand.gif" style= "cursor:hand;" OnClick= "showPage(this,divFIFinItemDef2);">
    		</td>
    		<td class= titleImg>
    			 科目类型定义查询结果
    		</td>
    	</tr>
    </table>

    
  	<Div  id= "divFIFinItemDef2" style= "display: ''" align=center>
      	<table  class= common>
       		<tr  class= common>
      	  	<td text-align: left colSpan=1>
  					 <span id="spanFinItemDefGrid" >
  					 </span>
  			  	</td>
  			</tr>
    	</table>
      <INPUT VALUE="首  页" TYPE=button onclick="turnPage.firstPage();" class="cssButton">
      <INPUT VALUE="上一页" TYPE=button onclick="turnPage.previousPage();" class="cssButton">
      <INPUT VALUE="下一页" TYPE=button onclick="turnPage.nextPage();" class="cssButton">
      <INPUT VALUE="尾  页" TYPE=button onclick="turnPage.lastPage();" class="cssButton">
  	</div>  

    	<INPUT type=hidden name=VersionNo value=''>
    	<INPUT type=hidden name=DealMode value=''>    	

  </form>
  <span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>

