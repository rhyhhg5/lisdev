<!-- FileName:QueryForPayFee.jsp  -->
<%@page contentType="text/html;charset=GBK"%>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	GlobalInput tGI = new GlobalInput();
	tGI = (GlobalInput) session.getValue("GI");
%>
<script>		
	var manageCom = "<%=tGI.ManageCom%>"; //记录登陆机构	
</script>
<head>
<title>校检计划查询</title>

<meta http-equiv="Content-Type" content="text/html; charset=GBK">
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<script src="./FIDiversityRuleQuery.js"></script>
<%@include file="./FIDiversityRuleQueryInit.jsp"%>

</head>
<body onload="initForm();">
<!--登录画面表格-->
<form name=fm target=fraSubmit method=post >
<table>
	<tr>
		<td><IMG src="../common/images/butExpand.gif" style="cursor:hand;" OnClick="showPage(this,divFCDay);"></td>
		<td class=titleImg>差异校检规则查询</td>
	</tr>
</table>

<Div id="divFCDay" style="display: ''" align=center>
	<table class=common align=center>
		<TR class=common>
			<TD class=title>规则编码</TD>
			<TD class=input>
				<Input class="common"  name=rulecode >
			</TD>
			<TD class=title>规则名称</TD>
			<TD class=input>
				<Input class="common" name=rulename >
			</TD>
			<TD class=title></TD>
			<TD class=input></TD>
		</TR>
	</table>
</Div>
<br>
	<TD class=title><Input class=cssButton type=Button value=" 查  询 " onclick="submitForm()"></TD>
	<TD class=title><Input class=cssButton type=Button value=" 返  回" onclick="returnParent()"></TD>
<br>
<br>
<Div id="Frame2" style="display: ''" align=center>

	<Table class=common>
		<tr>
			<td text-align: left colSpan=1><span id="spanErrLogGrid">
			</span></td>
		</tr>
	</Table>
	<INPUT VALUE="首页" class=cssButton TYPE=button onclick="getFirstPage();">
	<INPUT VALUE="上一页" class=cssButton TYPE=button onclick="getPreviousPage();"> 
	<INPUT VALUE="下一页" class=cssButton TYPE=button onclick="getNextPage();"> 
	<INPUT VALUE="尾页" class=cssButton TYPE=button onclick="getLastPage();">
</Div>


<input type=hidden	name=strSQLValue>
<input type=hidden  name=strSQLValueErr>
<input type=hidden	name=RuleDealBatchNo>

<span id="spanCode" style="display: none; position:absolute; slategray"></span>
</Form>
</body>
</html>
