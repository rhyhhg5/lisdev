<%@include file="../common/jsp/UsrCheck.jsp"%>
<%
//程序名称：CodeInput.jsp
//程序功能：
//创建日期：2002-08-16 17:44:43
//创建人  ：CrtHtml程序创建
//更新记录：  更新人    更新日期     更新原因/内容
%>
<!--用户校验类-->
  <%@page import="com.sinosoft.utility.*"%>
  <%@page import="java.util.*" %>
  <%@page import="com.sinosoft.lis.schema.*"%>
  <%@page import="com.sinosoft.lis.vschema.*"%>
  <%@page import="com.sinosoft.lis.fininterface_v3.*"%>
  <%@page import="com.sinosoft.lis.pubfun.*"%> 
  <%@page contentType="text/html;charset=GBK" %>
	<%@page import="java.io.*" %>
<%
	//接收信息，并作校验处理。
	//输入参数
	request.setCharacterEncoding("GBK");

	String FlagStr = "";
	String Content = "";

	//在此设置导出Excel的列名，应与sql语句取出的域相对应
	GlobalInput tGlobalInput = new GlobalInput(); 
	tGlobalInput = (GlobalInput)session.getValue("GI"); 
	ExportExcel.Format format = new ExportExcel.Format();
	
	ArrayList listCell = new ArrayList();
	ArrayList listLB = new ArrayList();
	ArrayList listColWidth = new ArrayList();
	format.mListCell=listCell;
	format.mListBL=listLB;
	format.mListColWidth=listColWidth;
	
	ExportExcel.Cell tCell=null;
	ExportExcel.ListBlock tLB=null;
	
	String tManageCom = tGlobalInput.ComCode;
	
	listColWidth.add(new String[]{"0","5000"});  
	String sql = request.getParameter("ExpSQL");
	System.out.println("Excel语句="+sql);
	
	tLB = new ExportExcel.ListBlock("001");
	tLB.colName = new String[]{"编码类型", "编码","编码名称", "编码别名", "其它标记"};
	tLB.sql = sql;
	tLB.row1 = 0;
	tLB.col1 = 0;
	tLB.InitData();
	listLB.add(tLB);
	try
	{
		response.reset();
		response.setContentType("application/octet-stream");
		
		//设置导出的xls文件名默认值
		String HeaderParam = "\""+"attachment;filename="+"code-000001"+".xls"+"\"";
		response.setHeader("Content-Disposition",HeaderParam);
		
		OutputStream outOS = response.getOutputStream();
		BufferedOutputStream bos = new BufferedOutputStream(outOS);
		ExportExcel excel = new ExportExcel();
		excel.write(format, bos);
		bos.flush();
		bos.close();
	}
	catch(Exception e)
	{
	  Content = "导出Excel失败!";
    FlagStr = "Fail"; 
	}
	if (!FlagStr.equals("Fail"))
	{
		Content = " 导出Excel成功! ";
		FlagStr = "Succ";
	}  
  //添加各种预处理
%>