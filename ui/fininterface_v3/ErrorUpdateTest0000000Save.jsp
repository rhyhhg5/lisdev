<%
//程序名称 :FIDataExtPlanExeSave.jsp
//
%>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page contentType="text/html;charset=GBK" %>

com\sinosoft\lis\fininterface_v3\ErrorUpdateTestUI.java

<%
	System.out.println("数据抽取开始执行Save页面");
	request.setCharacterEncoding("GBK");
	
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI"); //获得用户信息
	
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	
	
	VData tVData = new VData();
	
	TransferData tTransferData = new TransferData();
	
	ErrorUpdateTestUI tErrorUpdateTestUI = new ErrorUpdateTestUI();
	
	//获取前台参数	
 
	String IndexType = request.getParameter("IndexType");
	String IndexidNo = request.getParameter("IndexidNo");
	String Operator = "UPDATE";
	
	tTransferData.setNameAndValue("IndexType",IndexType);
	tTransferData.setNameAndValue("IndexidNo",IndexidNo);
	tTransferData.setNameAndValue("Operator",Operator);

//	FIVoucherDataDetailSchema tFIVoucherDataDetailSchema = new FIVoucherDataDetailSchema();
//	FIAbStandardDataSchema tFIAbStandardDataSchema = new FIAbStandardDataSchema();
	
	// 准备传输数据 VData
	tVData.add(tG);
	tVData.add(tTransferData);
	try
	{
		//tVData.addElement();
		
		if(!tErrorUpdateTestUI.submitData(tVData,Operator))
		{
			Content = "操作失败，原因是:" + tErrorUpdateTestUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}

	}
	catch(Exception ex)
	{
	    Content = "失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tErrorUpdateTestUI.mErrors;
		if (!tError.needDealError())
		{     
			Content = "修改操作已成功!";
			FlagStr = "Succ";
		}
		else
 		{
 			Content = " 操作失败，原因是:" + tError.getFirstError();
 			FlagStr = "Fail";
 		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>