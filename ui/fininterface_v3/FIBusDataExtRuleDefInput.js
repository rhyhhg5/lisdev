//               该文件中包含客户端需要处理的函数和事件

var showInfo;
var mDebug="0";
var mOperate="";
var turnPage = new turnPageClass();
window.onfocus=myonfocus;

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null) 
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}

//初始化修改页面
function initmaintpage()
{
	//查询版本信息
	var versioninfosql=" select a.versionno,(select codename from ldcode where codetype = 'versionstate' and code = a.versionstate )"
		+ " from firulesversion a,fimaintainapp b,fimaintainitem c where 1=1 and "
		+ " a.versionno = b.versionno and b.maintappno = c.maintappno and c.maintno = '"+maintno+"'";
	
	var versioninfo = easyExecSql(versioninfosql);
	if(versioninfo!='' && versioninfo != null)
	{
		fm.VersionNo.value = versioninfo[0][0];
		fm.VersionState2.value = versioninfo[0][1];
	}
	
	//初始化维护数据

}



/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
//针对版本信息查询子窗口返回的2维数组
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
		//alert(arrQueryResult);

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		//alert(arrResult);
		fm.all('VersionNo').value = arrResult[0][0];
		//VersionState显示为01，02，03；而VersionState2显示为正常、维护、删除
		fm.all('VersionState').value = arrResult[0][1];		
		fm.all('VersionState2').value = arrResult[0][2];				
		
		//当版本状态不为02-维护的时候，增删改按钮为灰色		
		if (arrResult[0][1] == "01"||arrResult[0][1] == "03"||arrResult[0][1] == ""||arrResult[0][1] == null)
		{				
			fm.all('addbutton').disabled = false;
			fm.all('updatebutton').disabled = true;		
			fm.all('deletebutton').disabled = true;	
		}
		if (arrResult[0][1] == "02")
		{
			fm.all('addbutton').disabled = false;
			fm.all('updatebutton').disabled = false;		
			fm.all('deletebutton').disabled = false;	
		}
	}
}


//针对主数据提取规则查询子窗口返回的2维数组
function afterQuery2( arrQueryResult )
{
	var arrResult = new Array();
		//alert(arrQueryResult);

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		//alert(arrResult);
		//fm.all('VersionNo').value = arrResult[0][0];
		arrResult[0][7]=arrResult[0][7].split("@@").join("||");
		arrResult[0][7]=arrResult[0][7].split("@").join("|");
		fm.all('ArithmeticID').value = arrResult[0][1];
		fm.all('ArithmeticName').value = arrResult[0][2];		
		fm.all('BuType').value = arrResult[0][3];
		fm.all('RunObject').value = arrResult[0][4];
		fm.all('DealMode').value = arrResult[0][5];
		fm.all('DealClass').value = arrResult[0][6];
		fm.all('MainSQL').value = arrResult[0][7];
		fm.all('FileName').value = arrResult[0][8];
		fm.all('ReturnRemark').value = arrResult[0][9];
		fm.all('State').value = arrResult[0][10];
										
		//在科目类型定义查询之后才能进行修改和删除操作
		fm.all('updatebutton').disabled = false;
		fm.all('deletebutton').disabled = false;
		if(fm.DealMode.value=='1')
		{
			 classdiv.style.display='';
			 sqldiv.style.display='none';
			 filediv.style.display='none';
			 fm.all('MainSQL').value='';
		}
			
		if(fm.DealMode.value=='2')
		{
			 classdiv.style.display='none';
			 sqldiv.style.display='';
			 filediv.style.display='none';
			 fm.all('DealClass').value='';
		}
	
		if(fm.DealMode.value=='3')
		{
			 filediv.style.display='';
			 sqldiv.style.display='none';
			 classdiv.style.display='none';
			 fm.all('FileName').value='';
		}	
		
		//来自于Common\javascript\Common.js，根据代码选择的代码查找并显示名称
		//showCodeName(); 
		//a.VersionNo,a.FinItemID,a.FinItemName,a.FinItemType,a.ItemMainCode,a.DealMode,a.ReMark
	}
}

//Click事件，当点击“查询”图片时触发该函数
//进入版本信息查询页面的查询按钮
function queryClick1()
{
	//下面增加相应的代码
	//mOperate="QUERY||MAIN";  
	showInfo=window.open("./FrameVersionRuleQueryForOther.jsp");
}

function queryClick2()
{	
	var VersionNo = fm.VersionNo.value;
	var VersionState = fm.VersionState2.value;
	showInfo=window.open("./FrameFIDataExtMainRuleQuery.jsp?VersionNo=" + VersionNo + "&VersionState=" + VersionState );  
}

//进入明细规则定义页面
function intoDetailRulePage()
{
	//插入条件
	if(fm.VersionNo.value == "")
	{
		alert("版本号为空！请先进行版本查询！");
		return false;
	}
	if(fm.ArithmeticID.value == "")
	{
		alert("主交易数据提取规则编号为空！请先进行查询！");
		return false;
	}
	
	showInfo=window.open("./FrameFIDataExtMainRuleDefInput.jsp?VersionNo="+fm.VersionNo.value+"&ArithmeticID="+fm.ArithmeticID.value+"&pageflag=C");  
}

//进入主规则补充
function intoMainRuleAddOnPage()
{
	//插入条件
	if(fm.VersionNo.value == "")
	{
		alert("版本号为空！请先进行版本查询！");
		return false;
	}
	if(fm.ArithmeticID.value == "")
	{
		alert("主交易数据提取规则编号为空！请先进行查询！");
		return false;
	}
	//showInfo=window.open("./FrameFIDataRuleExtMainAddOnDefInput.jsp?VersionNo="+fm.VersionNo.value+"&RuleID="+fm.RuleID.value+"&pageflag=C"); 
	showInfo=window.open("./FrameFIDataRuleExtMainAddOnDefInput.jsp?VersionNo="+fm.VersionNo.value+"&ArithmeticID="+fm.ArithmeticID.value+"&RuleLevel=02A&pageflag=C");  
}


function addClick()
{
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	//判断是否是新值插入
	var checksql="select count(*) from FIDataExtractRules where VersionNo = '"+fm.VersionNo.value+"' and ArithmeticID = '"+fm.ArithmeticID.value+"'";
	var re = easyExecSql(checksql);
	
	if(re!="0")
	{
		alert("规则编号已存在！");
		return false;
	}
	
	//为了防止双击，点击增加后，屏蔽"增加"按钮
	mOperate="INSERT||MAIN";
	submitForm();

}


//Click事件，当点击“删除”图片时触发该函数
function deleteClick()
{
  //下面增加相应的删除代码
  if ((fm.all("ArithmeticID").value==null)||(trim(fm.all("ArithmeticID").value)==''))
    alert("请确定要删除的主业务数据抽取规则编号！");
  
  else
  {

    if (confirm("您确实想删除该记录吗?"))
    {
		//判断是否有原纪录
		var checksql="select count(*) from FIDataExtractRules where VersionNo = '"+fm.VersionNo.value+"' and ArithmeticID = '"+fm.ArithmeticID.value+"'";
		var re = easyExecSql(checksql);
		
		if(re=="0")
		{
			alert("规则编号不存在！");
			return false;
		}
		mOperate="DELETE||MAIN";
		submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}


function updateClick()
{
  //提交前的检验
  if (!beforeSubmit()) //beforeSubmit()函数
  {
  	return false;
  }
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
	   //判断是否有原纪录
		var checksql="select count(*) from FIDataExtractRules where VersionNo = '"+fm.VersionNo.value+"' and ArithmeticID = '"+fm.ArithmeticID.value+"'";
		var re = easyExecSql(checksql);
		
		if(re=="0")
		{
			alert("规则编号不存在！");
			return false;
		}
		mOperate="UPDATE||MAIN";
		submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }
}


//重置按钮对应操作,Form的初始化函数在功能名+Init.jsp文件中实现，函数的名称为initForm()
function resetForm()
{
	try
	{
		initForm();
	}
	catch(re)
	{
		alert("FinItemDefInput.js-->resetForm函数中发生异常:初始化界面错误!");
	}
}


function submitForm()
{  
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	fm.hideOperate.value=mOperate;
	if (fm.hideOperate.value=="")
	{
		alert("操作控制数据丢失！");
	}
	fm.action="./DataExtMainRuleDefSave.jsp";
	//lockButton(); 
	fm.submit(); //提交

}



//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
	showInfo.close();
	//释放“增加”按钮

	if (FlagStr == "Fail" )
	{
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
		
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		//resetForm();
		mOperate="";
	}
	else
	{
		//alert(content);
		var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
		//parent.fraInterface.initForm();
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");

		//执行下一步操作
		//resetForm();
		if(mOperate=="DELETE||MAIN")
		{
			fm.all('ArithmeticID').value = '';   	
			fm.all('ArithmeticName').value = '';  	 
			fm.all('BuType').value = '';
			fm.all('RunObject').value = '';
			fm.all('DealMode').value = '';
			
			fm.all('ArithmeticID').readOnly = false;
			fm.all('updatebutton').disabled = true;		
			fm.all('deletebutton').disabled = true;  
		}
		mOperate="";
	}
}



//显示div，第一个参数为一个div的引用，第二个参数为是否显示，如果为"true"则显示，否则不显示
function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
		cDiv.style.display="none";
	}
}


//提交前的校验、计算
function beforeSubmit()
{
	if((fm.VersionNo.value=="")||(fm.VersionNo.value=="null"))
	{
		alert("请查询获取版本编号！");
		return ;
	}

	if((fm.ArithmeticID.value=="")||(fm.ArithmeticID.value=="null"))
	{
		alert("请您录入规则编号！");
		return ;
	}
  
	if((fm.ArithmeticName.value=="")||(fm.ArithmeticName.value=="null"))
	{
		alert("请您录入规则名称！");
		return ;
	}

	if((fm.BuType.value=="")||(fm.BuType.value=="null"))
	{
		alert("请您录入规则类型！");
		return ;
	}
  
	if((fm.RunObject.value=="")||(fm.RunObject.value=="null"))
	{
		alert("请您录入规则对象！");
		return ;
	}
  
	if((fm.DealMode.value=="")||(fm.DealMode.value=="null"))
	{
		alert("请您录入处理方式！");
		return ;
	}
	
	if((fm.State.value=="")||(fm.State.value=="null"))
	{
		alert("请您录入规则状态！");
		return ;
	}
  
	if (!verifyInput2()) 
	{
		return false;
	}

    return true;
}


//显示frmSubmit框架，用来调试
function showSubmitFrame(cDebug)
{
  if(cDebug=="1")
  {
			parent.fraMain.rows = "0,0,50,82,*";
  }
 	else {
  		parent.fraMain.rows = "0,0,0,82,*";
 	}
}


function resetAgain()
{

}

//代码选择后处理
function afterCodeSelect( cName, Filed)
{   
	if(fm.DealMode.value=='1')
	{
		 classdiv.style.display='';
		 sqldiv.style.display='none';
		 filediv.style.display='none';
		 fm.all('MainSQL').value='';
	}
			
	if(fm.DealMode.value=='2')
	{
		 classdiv.style.display='none';
		 sqldiv.style.display='';
		 filediv.style.display='none';
		 fm.all('DealClass').value='';
	}
	
	if(fm.DealMode.value=='3')
	{
		 filediv.style.display='';
		 sqldiv.style.display='none';
		 classdiv.style.display='none';
		 fm.all('FileName').value='';
	}	
}