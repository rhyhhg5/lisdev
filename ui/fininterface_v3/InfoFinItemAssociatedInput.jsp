 <html> 
<%
//创建日期：2011-9-23
//创建人  ：董健
//更新记录：  更新人    更新日期     更新原因/内容
%>
<%@page import="java.util.*"%> 
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>
<html>
<%
	String mStartDate = "";
	String mSubDate = "";
	String mEndDate = "";
	GlobalInput tGI = new GlobalInput();
	//PubFun PubFun=new PubFun();
	tGI = (GlobalInput)session.getValue("GI");
	System.out.println("1"+tGI.Operator);
	System.out.println("2"+tGI.ManageCom);
%>
<script>
	var VersionNo = <%=request.getParameter("VersionNo")%>;
	var VersionState = <%=request.getParameter("VersionState")%>;
	var FinItemID = <%=request.getParameter("FinItemID")%>;
	var pageflag = <%=request.getParameter("pageflag")%>;
	var maintno = <%=request.getParameter("maintno")%>;
</script>

<head >
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>  
<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>  
<script src="../common/Calendar/Calendar.js"></script>
<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
<SCRIPT src="InfoFinItemAssociatedInput.js"></SCRIPT>  
<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>

<%@include file="InfoFinItemAssociatedInputInit.jsp"%>
</head>

<body  onload="initForm();initElementtype();" >

<form name=fm   target=fraSubmit method=post>
<table>
	<tr>    		 
		 <td class= titleImg>科目关联专项定义 </td>   		 
	</tr>
</table>

<Div  id= "divAssociatedQuery" style= "display: ''">
<table  class= common>
	<tr  class= common>
		<td text-align: left colSpan=1>
			<span id="spanItemAssociatedGrid"></span>
		</td>
	</tr>
</table>
<Div  id= "divPage" align=center style= "display: 'none' ">      
	<INPUT CLASS=cssbutton VALUE="首页" TYPE=button onclick="turnPage.firstPage();">
	<INPUT CLASS=cssbutton VALUE="上一页" TYPE=button onclick="turnPage.previousPage();">
	<INPUT CLASS=cssbutton VALUE="下一页" TYPE=button onclick="turnPage.nextPage();">
	<INPUT CLASS=cssbutton VALUE="尾页" TYPE=button onclick="turnPage.lastPage();">
</Div>
</Div>

<br>
<hr>
      
<table class= common border=0 width=100%>
<tr class= common>
	<TD class= title>专项编号</TD>
	<TD class=input>
		<Input class=codeno name=AssociatedID ondblclick="return showCodeList('AssociatedID', [this,AssociatedName],[0,1],null,fm.VersionNo.value,'VersionNo');" onkeyup="return showCodeListKey('AssociatedID', [this,AssociatedName],[0,1],null,fm.VersionNo.value,'VersionNo');" verify="专项编号|NOTNULL"><input class=codename name=AssociatedName readonly=true elementtype=nacessary>
	</TD>		
	<TD class= title>描述</TD>
	<TD class= input>
		<Input class=common name=ReMark verify="描述|len<=500" >
	</TD>						 
</tr>	  
</table>

<p>       
<Div  id= "buttons"  style= "display: '' ">     
<INPUT VALUE="添  加" TYPE=button class= cssbutton name="addbutton" onclick="return addClick();">   
<!--INPUT VALUE="修  改" TYPE=button class= cssbutton name="updatebutton" onclick="return updateClick();"-->
<INPUT VALUE="删  除" TYPE=button class= cssbutton name="deletebutton" onclick="return deleteClick();">   
<INPUT VALUE="重  置" TYPE=button class= cssbutton name= resetbutton onclick="return resetAgain()">
</Div>
<!-- 页面控制参数 -->
<INPUT type=hidden name=hideOperate value=''>
<INPUT type=hidden name=VersionNo value=''> 
<INPUT type=hidden name=FinItemID value=''>
<INPUT type=hidden name=pageflag value=''>
<INPUT type=hidden name=maintno value=''>
<INPUT type=hidden name=AssociatedID1 value=''>     
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</form>

</body>
</html>
