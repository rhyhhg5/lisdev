<%
//程序名称 :FIDataExtPlanExeSave.jsp
//
%>

<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>
<%@page import="com.sinosoft.lis.vdb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page contentType="text/html;charset=GBK" %>

<%
	System.out.println("数据抽取开始执行Save页面");
	request.setCharacterEncoding("GBK");
	
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput)session.getValue("GI"); //获得用户信息
	
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	
	VData tVData = new VData();
	TransferData tTransferData = new TransferData();
	FIDataExtPlanExeUI tFIDataExtPlanExeUI = new FIDataExtPlanExeUI();
	FIDataExtractDefSet tFIDataExtractDefSet = new FIDataExtractDefSet();
	
	//获取前台参数	

	String StartDate =request.getParameter("StartDate");
	String EndDate =request.getParameter("EndDate");
	String FIExtType = request.getParameter("FIExtType"); //数据抽取类型	
	String IndexType = request.getParameter("IndexType");
	String IndexidNo = request.getParameter("IndexidNo");

	tTransferData.setNameAndValue("StartDate",StartDate);
	tTransferData.setNameAndValue("EndDate",EndDate);
	tTransferData.setNameAndValue("CallPointID","00"); //校检节点
	
	tTransferData.setNameAndValue("FIExtType",FIExtType);
	tTransferData.setNameAndValue("IndexType",IndexType);
	
	tTransferData.setNameAndValue("IndexidNo",IndexidNo);
	System.out.println("数据抽取开始,FIExtType:"+FIExtType);
	if("00".equals(FIExtType))
	{
		String tChk[] = request.getParameterValues("InpFIDataExtractGridChk");
		String VersionNo[]= request.getParameterValues("FIDataExtractGrid1");
		String RuleDefNo[]= request.getParameterValues("FIDataExtractGrid2");
		String indexcode[]= request.getParameterValues("FIDataExtractGrid5");
		int lineCount=tChk.length;
		
		FIDataExtractDefSchema tFIDataExtractDefSchema ;
		for( int i=0; i<lineCount; i++ ) 
		{
			if(tChk[i].equals("0"))
			{
				continue;
			}
			tFIDataExtractDefSchema = new FIDataExtractDefSchema();
			
			tFIDataExtractDefSchema.setVersionNo(VersionNo[i]);
			tFIDataExtractDefSchema.setRuleDefID(RuleDefNo[i]);
			tFIDataExtractDefSchema.setIndexCode(indexcode[i]);
			System.out.println("数据抽取开始,RuleDefNo:"+RuleDefNo[i]+",indexcode="+indexcode[i]);
			tFIDataExtractDefSet.add(tFIDataExtractDefSchema);
		}
	}
	
	// 准备传输数据 VData
	tVData.add(tG);
	tVData.add(tTransferData);
	tVData.add(tFIDataExtractDefSet);
	try
	{
		//tVData.addElement();
		
		if(!tFIDataExtPlanExeUI.submitData(tVData,FIExtType))
		{
			Content = "操作失败，原因是:" + tFIDataExtPlanExeUI.mErrors.getFirstError();
			FlagStr = "Fail";
		}

	}
	catch(Exception ex)
	{
	    Content = "失败，原因是:" + ex.toString();
	    FlagStr = "Fail";
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr=="")
	{
		tError = tFIDataExtPlanExeUI.mErrors;
		if (!tError.needDealError())
		{     
			Content = "操作已成功!";
			FlagStr = "Succ";
		}
		else
 		{
 			Content = " 操作失败，原因是:" + tError.getFirstError();
 			FlagStr = "Fail";
 		}
	}
%>
<html>
<script language="javascript">
	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>