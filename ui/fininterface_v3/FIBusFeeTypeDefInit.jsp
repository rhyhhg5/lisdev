<%
  //程序名称：FIBusFeeTypeDefInit.jsp
  //程序功能：业务交易费用定义
  //创建日期：2011/8/25
  //创建人  ： 董健
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">
var BusTypeId="";
var pageflag="";
var maintno="";
function initForm(){
	try
	{	
		initinputBox();		
		initBusFeeInfoGrid();		
		initFeeTypeInfoGrid();
		initpage();
	}
	catch(re){
		alert("FIBusFeeTypeDefInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initinputBox()
{
	BusTypeId = '<%=request.getParameter("BusTypeId")%>';
	pageflag = '<%=request.getParameter("pageflag")%>';
	maintno = '<%=request.getParameter("maintno")%>';
	
	fm.BusTypeId.value=BusTypeId;
	fm.maintno.value=maintno;
	fm.pageflag.value=pageflag;	
	if("Q"==fm.pageflag.value)
	{
		buttonsdiv1.style.display = 'none';
		buttonsdiv2.style.display = 'none';
	} 
}

function initBusFeeInfoGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="业务类型编码";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="费用编码";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="费用名称";
		iArray[3][1]="160px";
		iArray[3][2]=100;
		iArray[3][3]=0;
		
		iArray[4]=new Array();
		iArray[4][0]="信息编码";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=3;
		


		BusFeeInfoGrid = new MulLineEnter( "fm" , "BusFeeInfoGrid" ); 

		BusFeeInfoGrid.mulLineCount=2;
		BusFeeInfoGrid.displayTitle=1;
		BusFeeInfoGrid.canSel=1;
		BusFeeInfoGrid.canChk=0;
		BusFeeInfoGrid.hiddenPlus=1;
		BusFeeInfoGrid.hiddenSubtraction=1;
		BusFeeInfoGrid.selBoxEventFuncName = "getFeeTypeInfo";

		BusFeeInfoGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
function initFeeTypeInfoGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="信息编码";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="信息名称";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="信息维度";
		iArray[3][1]="30px";
		iArray[3][2]=100;
		iArray[3][3]=3;

		iArray[4]=new Array();
		iArray[4][0]="维度名称";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=3;
		
		iArray[5]=new Array();
		iArray[5][0]="用途";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=3;
		
		iArray[6]=new Array();
		iArray[6][0]="值域";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=3;
		
		iArray[7]=new Array();
		iArray[7][0]="用途编码";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=3;
		
		FeeTypeInfoGrid = new MulLineEnter( "fm" , "FeeTypeInfoGrid" ); 

		FeeTypeInfoGrid.mulLineCount=0;
		FeeTypeInfoGrid.displayTitle=1;
		FeeTypeInfoGrid.canSel=1;
		FeeTypeInfoGrid.canChk=0;
		FeeTypeInfoGrid.hiddenPlus=1;
		FeeTypeInfoGrid.hiddenSubtraction=1;
		FeeTypeInfoGrid.selBoxEventFuncName = "showfeeinfo";

		FeeTypeInfoGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
