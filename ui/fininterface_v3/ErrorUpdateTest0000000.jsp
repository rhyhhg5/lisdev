<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FIDataExtPlanExeInput.jsp
 //程序功能：数据抽取计划执行
 //创建日期：2011-8-24
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
// ;initElementtype()
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
  <SCRIPT src="ErrorUpdateTest0000000.js"></SCRIPT>
  <%@include file="ErrorUpdateTest0000000Init.jsp"%>
</head>
<body  onload="initForm();">
<form action="./ErrorUpdateTest0000000Save.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr>
    <td class="titleImg" >执行信息</td>
  </tr>
</table>
<table  class="common" >
  <tr class="common" style="display: none;">
    <td class="title">抽取起期</td><td class="input"><input class="coolDatePicker" dateFormat="short" name="StartDate" ></td>  
    <td class="title">抽取止期</td><td class="input"><input class="coolDatePicker" dateFormat="short" name="EndDate" ></td>  
  </tr>
  <tr class="common" style="display: none;">
    <td class="title">抽取类型</td>
    <td class="input">
	    <input class="codeno" name="FIExtType" 
		    ondblclick="return showCodeList('fiexttype',[this,FIExtTypeName],[0,1]);" 
		    onkeyup="return showCodeListKey('fiexttype',[this,FIExtTypeName],[0,1]);"><input class="codename" name="FIExtTypeName" >
    </td>  
  </tr>
	<tr id="tr2" class="common">
	
	<td class="title">业务索引号码</td>
    <td class="input">
    	<input class="common" name="IndexidNo" >
    </td> 
    
    <td class="title">校验类型</td>
    <td class="input">
		<Input class=codeno name=IndexType 
		    CodeData="0|^00|校验正常^01|校验错误" 
	    	ondblClick="showCodeListEx('type',[this,IndexTypeName],[0,1]);" 
	    	onkeyup="showCodeListKeyEx('type',[this,IndexTypeName],[0,1]);" ><input class=codename name=IndexTypeName readonly=true elementtype=nacessary>
  
  </tr>
  </div>
</table>
<br>
<input type="button" value="数据修改" class="cssbutton" onclick="return dataExt();">
<hr>
<div id="div1" style="display: none;">
<table>
  <tr>
    <td class="titleImg" >数据提取任务列表</td>
  </tr>
</table>
<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanFIDataExtractGrid" >
     </span> 
      </td>
   </tr>
</table>
</div>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
