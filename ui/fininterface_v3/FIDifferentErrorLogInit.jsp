 <% 
//程序名称：FIRuleDealLogQueryForDownLoadinit.jsp
%>
<!--用户校验类-->
<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%
     //添加页面控件的初始化。
%>                            

<script language="JavaScript">                           

function initInpBox()
{
  try
  {
	  	fm.reset();
		fm.all('StartDay').value = '';
		fm.all('EndDay').value = '';

  }
  catch(ex)
  {
    alert("FIQualityErrorLoginit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }      
}

function initSelBox()
{
  try
  {
  }
  catch(ex)
  {
    alert("FIQualityErrorLoginit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}

function initForm()
{
  try
  {
    initInpBox();
    initSelBox();
    initFIRuleDealLogGrid();
  }
  catch(re)
  {
    alert("FIQualityErrorLoginit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}
//
function initFIRuleDealLogGrid()
{                                  
	var iArray = new Array();      
	try
	{
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";            		//列宽
      iArray[0][2]=10;            			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="版本编号";         		//列名
      iArray[1][1]="100px";            		//列宽
      iArray[1][2]=170;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[2]=new Array();
      iArray[2][0]="质检执行批次";         		//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=100;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="事件流水号";         	//列名
      iArray[3][1]="100px";            		//列宽
      iArray[3][2]=100;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[4]=new Array();
      iArray[4][0]="事件结点";         		//列名
      iArray[4][1]="0px";            		//列宽
      iArray[4][2]=100;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[5]=new Array();
      iArray[5][0]="校验结果";         		//列名
      iArray[5][1]="40px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      iArray[6]=new Array();
      iArray[6][0]="校检规则编码";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=3;              			//是否允许输入,1表示允许，0表示不允许 
      
      FIRuleDealLogGrid = new MulLineEnter( "fm" , "FIRuleDealLogGrid" ); 
      FIRuleDealLogGrid.mulLineCount = 0;   
      FIRuleDealLogGrid.displayTitle = 1;
      FIRuleDealLogGrid.canSel=1;
      FIRuleDealLogGrid.locked = 1;	
      //FIRuleDealLogGrid.selBoxEventFuncName = "ReturnData";
	  FIRuleDealLogGrid.hiddenPlus = 1;
	  FIRuleDealLogGrid.hiddenSubtraction = 1;
      FIRuleDealLogGrid.loadMulLine(iArray);  
      FIRuleDealLogGrid.detailInfo="单击显示详细信息";
      

      //这些操作必须在loadMulLine后面
	}
	catch(ex)
	{
		alert(ex);
	}
}

</script>
