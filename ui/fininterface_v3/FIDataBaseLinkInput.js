var showInfo;
var mDebug="1";
window.onfocus=myonfocus;
//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null) 
	{
	  try
	  {
	    showInfo.focus();
	  }
	  catch(ex)
	  {
	    showInfo=null;
	  }
	}
}


//Click事件，当点击增加图片时触发该函数
function submitForm()
{
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	if(fm.all('DBType').value =="ORACLE")
	{
		fm.OperateType.value = "INSERT||MAIN1";
	}
 	if(fm.all('DBType').value =="INFORMIX")
	{
		fm.OperateType.value = "INSERT||MAIN2";
	}
 	if(fm.all('DBType').value =="SQLSERVER"||fm.all('DBType').value =="DB2")
	{
		fm.OperateType.value = "INSERT||MAIN3";
	}
 	if(fm.all('DBType').value =="WEBLOGICPOOL")
	{
		fm.OperateType.value = "INSERT||MAIN4";
 	}
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.action = './FIDataBaseLinkSave.jsp';
	fm.submit(); //提交
}

function resetAgain()
{
	fm.all('InterfaceCode').value = '';
  	fm.all('InterfaceName').value = '';
  	fm.all('DBType').value = '';
  	fm.all('IP1').value = '';
  	fm.all('Port1').value = '';
  	fm.all('DBName1').value = '';
  	fm.all('ServerName1').value = '';
  	fm.all('UserName1').value = '';
    fm.all('PassWord1').value = '';
    
    fm.all('IP2').value = '';
  	fm.all('Port2').value = '';
  	fm.all('DBName2').value = '';
  	fm.all('ServerName2').value = '';
  	fm.all('UserName2').value = '';
    fm.all('PassWord2').value = '';
    
    fm.all('IP3').value = '';
  	fm.all('Port3').value = '';
  	fm.all('DBName3').value = '';
  	fm.all('ServerName3').value = '';
  	fm.all('UserName3').value = '';
    fm.all('PassWord3').value = '';
    
    fm.all('IP4').value = '';
  	fm.all('Port4').value = '';
  	fm.all('DBName4').value = '';
  	fm.all('ServerName4').value = '';
  	fm.all('UserName4').value = '';
    fm.all('PassWord4').value = '';
    
     fm.all('InterfaceCode').readOnly = false;
   	 ORACLEdiv.style.display='none';
	 INFORMIXdiv.style.display='none'; 
	 SQLSERVERdiv.style.display='none';
	 WEBLOGICPOOLdiv.style.display='none';
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    fm.OperateType.value = "";
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    if( fm.OperateType.value == "DELETE||MAIN1"||fm.OperateType.value == "DELETE||MAIN2"||fm.OperateType.value == "DELETE||MAIN3"||fm.OperateType.value == "DELETE||MAIN4")
    {
	    fm.all('InterfaceCode').value = '';
	  	fm.all('InterfaceName').value = '';
	  	fm.all('DBType').value = '';
	  	fm.all('IP1').value = '';
	  	fm.all('Port1').value = '';
	  	fm.all('DBName1').value = '';
	  	fm.all('ServerName1').value = '';
	  	fm.all('UserName1').value = '';
	    fm.all('PassWord1').value = '';
	    
	    fm.all('IP2').value = '';
	  	fm.all('Port2').value = '';
	  	fm.all('DBName2').value = '';
	  	fm.all('ServerName2').value = '';
	  	fm.all('UserName2').value = '';
	    fm.all('PassWord2').value = '';
	    
	    fm.all('IP3').value = '';
	  	fm.all('Port3').value = '';
	  	fm.all('DBName3').value = '';
	  	fm.all('ServerName3').value = '';
	  	fm.all('UserName3').value = '';
	    fm.all('PassWord3').value = '';
	    
	    fm.all('IP4').value = '';
	  	fm.all('Port4').value = '';
	  	fm.all('DBName4').value = '';
	  	fm.all('ServerName4').value = '';
	  	fm.all('UserName4').value = '';
	    fm.all('PassWord4').value = '';
    
	     fm.all('InterfaceCode').readOnly = false;
	   	 ORACLEdiv.style.display='none';
		 INFORMIXdiv.style.display='none'; 
		 SQLSERVERdiv.style.display='none';
		 WEBLOGICPOOLdiv.style.display='none';
    }
    fm.OperateType.value = "";
  }
  	
}


//取消按钮对应操作
function cancelForm()
{
    showDiv(operateButton,"true");
    showDiv(inputButton,"false");
}


//Click事件，当点击“修改”图片时触发该函数
function updateClick()
{
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	if (confirm("您确实想修改该记录吗?"))
	{
	   	if(fm.all('DBType').value =="ORACLE")
	  	{
	 	 	fm.OperateType.value = "UPDATE||MAIN1";
		}
		if(fm.all('DBType').value =="INFORMIX")
	  	{
	 		fm.OperateType.value = "UPDATE||MAIN2";
		}
		if(fm.all('DBType').value =="SQLSERVER"||fm.all('DBType').value =="DB2")
	  	{
	 	 	fm.OperateType.value = "UPDATE||MAIN3";
		}
		if(fm.all('DBType').value =="WEBLOGICPOOL")
	  	{
	 	 	fm.OperateType.value = "UPDATE||MAIN4";
		}
	    var i = 0;
	    var showStr="正在更新数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	    fm.action = './FIDataBaseLinkSave.jsp';
	    fm.submit(); //提交
	}
	else
	{
		fm.OperateType.value = "";
		alert("您取消了修改操作！");
	}
}
//Click事件，当点击“查询”图片时触发该函数
function queryClick()
{
  showInfo=window.open("./FrameFIDataBaseLinkQuery.jsp");
}

function deleteClick()
{
	if((fm.all('InterfaceCode').value=="")||(fm.all('InterfaceCode').value=="null"))
	{
	    alert("请您录入接口编码！");
	    return ;
	}
	if (confirm("您确实要删除该记录吗？"))
	{
	  	if(fm.all('DBType').value =="ORACLE")
	  	{
	 	 	fm.OperateType.value = "DELETE||MAIN1";
		}
 		if(fm.all('DBType').value =="INFORMIX")
		{
			fm.OperateType.value = "DELETE||MAIN2";
 		}
 		if(fm.all('DBType').value =="SQLSERVER"||fm.all('DBType').value =="DB2")
		{
 	 		fm.OperateType.value = "DELETE||MAIN3";
 		}
 		if(fm.all('DBType').value =="WEBLOGICPOOL")
		{
 	 		fm.OperateType.value = "DELETE||MAIN4";
 		}
	    var i = 0;
	    var showStr="正在删除数据，请您稍候并且不要修改屏幕上的值或连接其他的界面";
	    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	    fm.action = './FIDataBaseLinkSave.jsp';
	    fm.submit();//提交
	}
	else
	{
	    fm.OperateType.value = "";
	    alert("您已经取消了修改操作！");
	}
}

function afterQuery( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		fm.all('InterfaceCode').value = arrResult[0][0];
		fm.all('InterfaceName').value = arrResult[0][1];
		fm.all('DBType').value = arrResult[0][2];
		if(fm.all('DBType').value =="ORACLE")
	  	{		
	  		ORACLEdiv.style.display='';
			INFORMIXdiv.style.display='none'; 
			SQLSERVERdiv.style.display='none';
			WEBLOGICPOOLdiv.style.display='none';
			 
			fm.all('IP2').value = '';
		  	fm.all('Port2').value = '';
		  	fm.all('DBName2').value = '';
		  	fm.all('ServerName2').value = '';
		  	fm.all('UserName2').value = '';
		    fm.all('PassWord2').value = '';
	    
		    fm.all('IP3').value = '';
		  	fm.all('Port3').value = '';
		  	fm.all('DBName3').value = '';
		  	fm.all('ServerName3').value = '';
		  	fm.all('UserName3').value = '';
		    fm.all('PassWord3').value = '';
		    
		    fm.all('IP4').value = '';
		  	fm.all('Port4').value = '';
		  	fm.all('DBName4').value = '';
		  	fm.all('ServerName4').value = '';
		  	fm.all('UserName4').value = '';
		    fm.all('PassWord4').value = '';
	    
			fm.all('IP1').value = arrResult[0][3];
			fm.all('Port1').value = arrResult[0][4];
			fm.all('DBName1').value = arrResult[0][5];
			fm.all('ServerName1').value = arrResult[0][6];
			fm.all('UserName1').value = arrResult[0][7];
			fm.all('PassWord1').value = arrResult[0][8];
		}
		if(fm.all('DBType').value =="INFORMIX")
	  	{		
			ORACLEdiv.style.display='none';
			INFORMIXdiv.style.display=''; 
			SQLSERVERdiv.style.display='none';
			WEBLOGICPOOLdiv.style.display='none';
			 
			fm.all('IP1').value = '';
		  	fm.all('Port1').value = '';
		  	fm.all('DBName1').value = '';
		  	fm.all('ServerName1').value = '';
		  	fm.all('UserName1').value = '';
		    fm.all('PassWord1').value = '';
		    
		    fm.all('IP3').value = '';
		  	fm.all('Port3').value = '';
		  	fm.all('DBName3').value = '';
		  	fm.all('ServerName3').value = '';
		  	fm.all('UserName3').value = '';
		    fm.all('PassWord3').value = '';
		    
		    fm.all('IP4').value = '';
		  	fm.all('Port4').value = '';
		  	fm.all('DBName4').value = '';
		  	fm.all('ServerName4').value = '';
		  	fm.all('UserName4').value = '';
		    fm.all('PassWord4').value = '';
	    
			fm.all('IP2').value = arrResult[0][3];
			fm.all('Port2').value = arrResult[0][4];
			fm.all('DBName2').value = arrResult[0][5];
			fm.all('ServerName2').value = arrResult[0][6];
			fm.all('UserName2').value = arrResult[0][7];
			fm.all('PassWord2').value = arrResult[0][8];
		}
		if(fm.all('DBType').value =="SQLSERVER"||fm.all('DBType').value =="DB2")
		{
			ORACLEdiv.style.display='none';
			INFORMIXdiv.style.display='none'; 
			SQLSERVERdiv.style.display='';
			WEBLOGICPOOLdiv.style.display='none';
			 
			fm.all('IP1').value = '';
			fm.all('Port1').value = '';
		  	fm.all('DBName1').value = '';
		  	fm.all('ServerName1').value = '';
		  	fm.all('UserName1').value = '';
		    fm.all('PassWord1').value = '';
	    
			fm.all('IP2').value = '';
		  	fm.all('Port2').value = '';
		  	fm.all('DBName2').value = '';
		  	fm.all('ServerName2').value = '';
		  	fm.all('UserName2').value = '';
		    fm.all('PassWord2').value = '';
		 
		    fm.all('IP4').value = '';
		  	fm.all('Port4').value = '';
		  	fm.all('DBName4').value = '';
		  	fm.all('ServerName4').value = '';
		  	fm.all('UserName4').value = '';
		    fm.all('PassWord4').value = '';
	    
			fm.all('IP3').value = arrResult[0][3];
			fm.all('Port3').value = arrResult[0][4];
			fm.all('DBName3').value = arrResult[0][5];
			fm.all('ServerName3').value = arrResult[0][6];
			fm.all('UserName3').value = arrResult[0][7];
			fm.all('PassWord3').value = arrResult[0][8];
		}
		if(fm.all('DBType').value =="WEBLOGICPOOL")
	  	{
	  		ORACLEdiv.style.display='none';
			INFORMIXdiv.style.display='none'; 
			SQLSERVERdiv.style.display='none';
			WEBLOGICPOOLdiv.style.display='';
			 
			fm.all('IP1').value = '';
		  	fm.all('Port1').value = '';
		  	fm.all('DBName1').value = '';
		  	fm.all('ServerName1').value = '';
		  	fm.all('UserName1').value = '';
		    fm.all('PassWord1').value = '';
	    
			fm.all('IP2').value = '';
		  	fm.all('Port2').value = '';
		  	fm.all('DBName2').value = '';
		  	fm.all('ServerName2').value = '';
		  	fm.all('UserName2').value = '';
		    fm.all('PassWord2').value = '';
		    
			fm.all('IP3').value = '';
		  	fm.all('Port3').value = '';
		  	fm.all('DBName3').value = '';
		  	fm.all('ServerName3').value = '';
		  	fm.all('UserName3').value = '';
		    fm.all('PassWord3').value = '';
	    
			fm.all('IP4').value = arrResult[0][3];
			fm.all('Port4').value = arrResult[0][4];
			fm.all('DBName4').value = arrResult[0][5];
			fm.all('ServerName4').value = arrResult[0][6];
			fm.all('UserName4').value = arrResult[0][7];
			fm.all('PassWord4').value = arrResult[0][8];
		}
		showCodeName(); 
		fm.all('InterfaceCode').readOnly = true;
	}
}

function afterCodeSelect( cName, Filed)
{   
	if(fm.DBType.value=='ORACLE')
	{
		ORACLEdiv.style.display='';
		INFORMIXdiv.style.display='none'; 
		SQLSERVERdiv.style.display='none';
		WEBLOGICPOOLdiv.style.display='none';
		 
		fm.all('IP2').value = '';
	  	fm.all('Port2').value = '';
	  	fm.all('DBName2').value = '';
	  	fm.all('ServerName2').value = '';
	  	fm.all('UserName2').value = '';
	    fm.all('PassWord2').value = '';
	    
	    fm.all('IP3').value = '';
	  	fm.all('Port3').value = '';
	  	fm.all('DBName3').value = '';
	  	fm.all('ServerName3').value = '';
	  	fm.all('UserName3').value = '';
	    fm.all('PassWord3').value = '';
	    
	    fm.all('IP4').value = '';
	  	fm.all('Port4').value = '';
	  	fm.all('DBName4').value = '';
	  	fm.all('ServerName4').value = '';
	  	fm.all('UserName4').value = '';
	    fm.all('PassWord4').value = '';
	}
			
	if(fm.DBType.value=='INFORMIX')
	{
		 ORACLEdiv.style.display='none';
		 INFORMIXdiv.style.display=''; 
		 SQLSERVERdiv.style.display='none';
		 WEBLOGICPOOLdiv.style.display='none';
		 
		fm.all('IP1').value = '';
	  	fm.all('Port1').value = '';
	  	fm.all('DBName1').value = '';
	  	fm.all('ServerName1').value = '';
	  	fm.all('UserName1').value = '';
	    fm.all('PassWord1').value = '';
	    
	    fm.all('IP3').value = '';
	  	fm.all('Port3').value = '';
	  	fm.all('DBName3').value = '';
	  	fm.all('ServerName3').value = '';
	  	fm.all('UserName3').value = '';
	    fm.all('PassWord3').value = '';
	    
	    fm.all('IP4').value = '';
	  	fm.all('Port4').value = '';
	  	fm.all('DBName4').value = '';
	  	fm.all('ServerName4').value = '';
	  	fm.all('UserName4').value = '';
	    fm.all('PassWord4').value = '';
	}			
	
	if(fm.DBType.value=='SQLSERVER'||fm.DBType.value=='DB2')
	{
		 ORACLEdiv.style.display='none';
		 INFORMIXdiv.style.display='none'; 
		 SQLSERVERdiv.style.display='';
		 WEBLOGICPOOLdiv.style.display='none';
		 
		fm.all('IP1').value = '';
	  	fm.all('Port1').value = '';
	  	fm.all('DBName1').value = '';
	  	fm.all('ServerName1').value = '';
	  	fm.all('UserName1').value = '';
	    fm.all('PassWord1').value = '';
    
		fm.all('IP2').value = '';
	  	fm.all('Port2').value = '';
	  	fm.all('DBName2').value = '';
	  	fm.all('ServerName2').value = '';
	  	fm.all('UserName2').value = '';
	    fm.all('PassWord2').value = '';
	 
	    fm.all('IP4').value = '';
	  	fm.all('Port4').value = '';
	  	fm.all('DBName4').value = '';
	  	fm.all('ServerName4').value = '';
	  	fm.all('UserName4').value = '';
	    fm.all('PassWord4').value = '';
	}	
	
	if(fm.DBType.value=='WEBLOGICPOOL')
	{
		 ORACLEdiv.style.display='none';
		 INFORMIXdiv.style.display='none'; 
		 SQLSERVERdiv.style.display='none';
		 WEBLOGICPOOLdiv.style.display='';
		 
		fm.all('IP1').value = '';
	  	fm.all('Port1').value = '';
	  	fm.all('DBName1').value = '';
	  	fm.all('ServerName1').value = '';
	  	fm.all('UserName1').value = '';
	    fm.all('PassWord1').value = '';
    
		fm.all('IP2').value = '';
	  	fm.all('Port2').value = '';
	  	fm.all('DBName2').value = '';
	  	fm.all('ServerName2').value = '';
	  	fm.all('UserName2').value = '';
	    fm.all('PassWord2').value = '';
    
		fm.all('IP3').value = '';
	  	fm.all('Port3').value = '';
	  	fm.all('DBName3').value = '';
	  	fm.all('ServerName3').value = '';
	  	fm.all('UserName3').value = '';
	    fm.all('PassWord3').value = '';
	}	

}

function beforeSubmit()
{
	if((fm.all('InterfaceCode').value=="")||(fm.all('InterfaceCode').value=="null"))
	{
	    alert("请您录入接口编码！");
	    return ;
	}
	if((fm.all('DBType').value=="")||(fm.all('DBType').value=="null"))
	{
	    alert("请您录入数据库类型！");
	    return ;
	}
  	if(fm.all('DBType').value =="ORACLE")
	{
	  	if((fm.all('IP1').value=="")||(fm.all('IP1').value==null))
	  	{
	  		alert("请输入IP地址！");
	  		return;
	  	}
  		if((fm.all('Port1').value=="")||(fm.all('Port1').value==null))
	  	{
	  		alert("请输入端口号！");
	  		return;
	  	}
	  	if((fm.all('DBName1').value=="")||(fm.all('DBName1').value==null))
	  	{
	  		alert("请输入数据库名称！");
	  		return;
	  	}
	  	if((fm.all('UserName1').value=="")||(fm.all('UserName2').value==null))
	  	{
	  		alert("请输入用户名！");
	  		return;
	  	}
	  	if((fm.all('PassWord1').value=="")||(fm.all('PassWord2').value==null))
	  	{
	  		alert("请输入密码！");
	  		return;
	  	}
	}
	if(fm.all('DBType').value =="INFORMIX")
	{
	  	if((fm.all('IP2').value=="")||(fm.all('IP2').value==null))
	  	{
	  		alert("请输入IP地址！");
	  		return;
	  	}
	  	if((fm.all('Port2').value=="")||(fm.all('Port2').value==null))
	  	{
	  		alert("请输入端口号！");
	  		return;
	  	}
	  	if((fm.all('DBName2').value=="")||(fm.all('DBName2').value==null))
	  	{
	  		alert("请输入数据库名称！");
	  		return;
	  	}
	  	if((fm.all('ServerName2').value=="")||(fm.all('ServerName2').value==null))
	  	{
	  		alert("请输入服务名称！");
	  		return;
	  	}
	  	if((fm.all('UserName2').value=="")||(fm.all('UserName2').value==null))
	  	{
	  		alert("请输入用户名！");
	  		return;
	  	}
	  	if((fm.all('PassWord2').value=="")||(fm.all('PassWord2').value==null))
	  	{
	  		alert("请输入密码！");
	  		return;
	  	}
	}
	if(fm.all('DBType').value =="SQLSERVER"||fm.all('DBType').value =="DB2")
	{
	  	if((fm.all('IP3').value=="")||(fm.all('IP3').value==null))
	  	{
	  		alert("请输入IP地址！");
	  		return;
	  	}
	  	if((fm.all('Port3').value=="")||(fm.all('Port3').value==null))
	  	{
	  		alert("请输入端口号！");
	  		return;
	  	}
	  	if((fm.all('DBName3').value=="")||(fm.all('DBName3').value==null))
	  	{
	  		alert("请输入数据库名称！");
	  		return;
	  	}
	  	if((fm.all('UserName3').value=="")||(fm.all('UserName2').value==null))
	  	{
	  		alert("请输入用户名！");
	  		return;
	  	}
	  	if((fm.all('PassWord3').value=="")||(fm.all('PassWord2').value==null))
	  	{
	  		alert("请输入密码！");
	  		return;
	  	}
	}
	if(fm.all('DBType').value =="WEBLOGICPOOL")
	{
	  	if((fm.all('IP4').value=="")||(fm.all('IP3').value==null))
	  	{
	  		alert("请输入IP地址！");
	  		return;
	  	}
	  	if((fm.all('Port4').value=="")||(fm.all('Port3').value==null))
	  	{
	  		alert("请输入端口号！");
	  		return;
	  	}
	  	if((fm.all('DBName4').value=="")||(fm.all('DBName4').value==null))
	  	{
	  		alert("请输入数据库名称！");
	  		return;
	  	}
	}
	return true;
}