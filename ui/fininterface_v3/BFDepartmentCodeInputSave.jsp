<%@include file="../common/jsp/UsrCheck.jsp"%>

<%
	//程序名称：BIDepartmentCodeInputSave.jsp
	//程序功能：业务机构代码转财务机构代码保存页面
	//创建日期：2013-05-06
	//创建人  ：卢翰林
	//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.wpleace.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html;charset=GBK"%>

<%
	request.setCharacterEncoding("GBK");

	LICodeTransSchema mLICodeTransSchema = new LICodeTransSchema();
	FICodeTransSchema mFICodeTransSchema = new FICodeTransSchema();
	BFDepartmentCodeInputUI mBFDepartmentCodeInputUI = new BFDepartmentCodeInputUI();
	
	CErrors tError = new CErrors();
	String tOperate = "INSERT";
	String FlagStr = "true";
	String Content = "更新成功";
	
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	
	String tCode = request.getParameter("Code");
	String tCodeName = request.getParameter("CodeName");
	String tCodeAlias = request.getParameter("CodeAlias");
	
	mLICodeTransSchema.setCode(tCode);
	mLICodeTransSchema.setCodeName(tCodeName);
	mLICodeTransSchema.setCodeAlias(tCodeAlias);
	
	mFICodeTransSchema.setCode(tCode);
	mFICodeTransSchema.setCodeName(tCodeName);
	mFICodeTransSchema.setCodeAlias(tCodeAlias);
	
	VData tVData = new VData();
	tVData.add(tG);
	tVData.add(mLICodeTransSchema);
	tVData.add(mFICodeTransSchema);
	try {
		if (!mBFDepartmentCodeInputUI.submitData(tVData, tOperate)) {
			if (mBFDepartmentCodeInputUI.mErrors.needDealError()) {
				tError.copyAllErrors(mBFDepartmentCodeInputUI.mErrors);
				FlagStr = "Fail";
				Content = "保存失败，" + tError.getFirstError();
			} else {
				FlagStr = "Fail";
				Content = "保存失败，但DataCheckUI没有提供详细错误信息！";
			}
		}
	} catch (Exception ex) {
		Content = "保存失败，原因是:" + ex.toString();
		FlagStr = "Fail";
	}
%>
<html>
	<script language="javascript">
		parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
	</script>
</html>