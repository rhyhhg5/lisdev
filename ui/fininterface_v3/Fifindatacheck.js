//该文件中包含客户端需要处理的函数和事件

//程序名称：Fifindatacheck.js
//程序功能：元数据定义
//创建日期：2011/9/15
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();
var mOperate="";

function RuleDY()
{
	if(fm.Codetype.value==''){
			alert("请选择校验类型");
			return false;
	}
	if(fm.Codetype.value=="ZJPLAN")
	//?type="+fm.Codetype.value+"&typename="+fm.CodetypeName.value+"
	  showInfo=window.open("./InfindatacheckInput.jsp");
	else 
	  showInfo=window.open("./InfindatacheckInputs.jsp");
}

//版本查询

function RulesVersionQuery()
{
	window.open("./FrameVersionRuleQuery.jsp");
}

//查询校验规则
function QueryData()
{

	var VersionNo = fm.all('VersionNo').value;
		if (VersionNo == null||VersionNo == '')
	  {
	  	alert("请先进行版本信息查询！");
	  	return;
	  }
	
	  showInfo=window.open("./FrameSefindatacheck.jsp?VersionNo=" + VersionNo +"");
}



//添加
function addClick(){

		 //提交前的检验
	  if (!beforeSubmit()) //beforeSubmit()函数
	  {
	  	return false;
	  }
	  
		if(fm.Codetype.value=="03")
		{
		
			if(fm.Code_T.value==''){
				alert("请选择校验节点");
				return false;
			}
			//?type="+fm.Codetype.value+"&typename="+fm.CodetypeName.value+"
			//  showInfo=window.open("./InfindatacheckInput.jsp");
			
	  	}
		  
		 mOperate="INSERT||MAIN";
		 submitForm();
		
  
}


//添加
function DataClick(){

		 //提交前的检验
	  if (!beforeSubmit()) 
	  {
	  	return false;
	  }
	else  
	{
	    showInfo=window.open("./FrameInfindatacheck.jsp?VersionNo="+fm.VersionNo.value+"&Ruleid="+fm.Ruleid.value+"");		   
	}
		
  
}

//更新
function updateClick(){

//提交前的检验
  if (!beforeSubmit()) //beforeSubmit()函数
  {
  	return false;
  }
  else
  {
    if (confirm("您确实想修改该记录吗?"))
    {
      mOperate="UPDATE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了修改操作！");
    }
  }

}
//删除按钮
function deleteClick(){
//下面增加相应的删除代码
  if((fm.Codetype.value=="")||(fm.Codetype.value=="null"))
  {
    alert("检验类型为空");
    return false;
  }
  if ((fm.all("Code").value==null)||(trim(fm.all("Code").value)==''))
  {
    alert("类型编码为空");
    return false;
  }
  else
  {

    if (confirm("您确实想删除该记录吗?"))
    {
      mOperate="DELETE||MAIN";
      submitForm();
    }
    else
    {
      mOperate="";
      alert("您取消了删除操作！");
    }
  }
}

//重置按钮
function resetAgain(){

	fm.all('Ruletype').value = '';
	fm.all('RuletypeName').value = '';
	fm.all('RuleDealSQL').value = '';
	fm.all('Ruleid').value = '';
	fm.all('CallPointID').value = '';
	fm.all('CallPointIDName').value = '';
	fm.all('RuleState').value = '';
	fm.all('RuleStateName').value = '';
	fm.all('RuleName').value = '';
	fm.all('errtype').value = '';
	fm.all('errtypeName').value = '';
	fm.all('ReturnRemark').value = '';
	
	
	alert('重置成功');
}


function submitForm()
{

 

  var i = 0;
  var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
  var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
  showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

  fm.hideOperate.value=mOperate;
  if (fm.hideOperate.value=="")
  {
    alert("操作控制数据丢失！");
  }
  //lockButton(); 
  fm.submit(); //提交

}

//提交前的校验、计算  
function beforeSubmit(){
  //添加操作 
    if(fm.Ruletype.value==''){
			alert("请选择校验类型");
			return false;
	}
	
	if((fm.RuleName.value=="")||(fm.RuleName.value=="null"))
	{
			alert("规则名称为空");
			return false;
	}
return true;
	
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");  
    mOperate=""; 
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}
