<%
	 /************************
	/*页面:QueryForPayFeeToExcel.jsp*/
	/*create:lijs*/
	/*创建时间:2007 version 1.0*/
	/*******************************/
%>
<%@page contentType="text/html;charset=GBK"%>
<%@page import="java.util.*"%>
<%@page import="java.io.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%
	//在此设置导出Excel的列名，应与sql语句取出的域相对应
	GlobalInput tGlobalInput = new GlobalInput();
	tGlobalInput = (GlobalInput) session.getValue("GI");

	ExportExcel.Format format = new ExportExcel.Format();
	ArrayList listCell = new ArrayList();
	ArrayList listLB = new ArrayList();
	ArrayList listColWidth = new ArrayList();
	format.mListCell = listCell;
	format.mListBL = listLB;
	format.mListColWidth = listColWidth;

	ExportExcel.Cell tCell = null;
	ExportExcel.ListBlock tLB = null;

	String tManageCom = tGlobalInput.ComCode;
	String strSQL = request.getParameter("strSQLValueErr");


	listColWidth.add(new String[] { "0", "5500" });
	listColWidth.add(new String[] { "1", "5000" });
	listColWidth.add(new String[] { "2", "15000" });
	listColWidth.add(new String[] { "3", "3000" });
	listColWidth.add(new String[] { "4", "3000" });

    /***排序通过前台传过来的OTHERNOTYPE　来判断用什么排序***/
	tLB = new ExportExcel.ListBlock("001");
	tLB.colName = new String[] { "校验批次号码", "校验规则", "错误日志", "校验人", "校验日期"};		
	tLB.sql = strSQL.trim();
	tLB.row1 = 0;
	tLB.col1 = 0;
	tLB.InitData();
	listLB.add(tLB);
	
	

	try {

		response.reset();
		response.setContentType("application/octet-stream");
		response.setHeader("Content-Disposition",
		"attachment; filename=ErrLogExcel_List.xls");

		OutputStream outOS = response.getOutputStream();
		BufferedOutputStream bos = new BufferedOutputStream(outOS);
		

		ExportExcel excel = new ExportExcel();
		excel.write(format, bos);
		bos.flush();
		bos.close();
	} catch (Exception e) {
		System.out.println("导出Excel失败！");
	}
%>
