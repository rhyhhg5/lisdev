<%
  //程序名称：FIVoucherTempDefInit.jsp
  //程序功能：凭证模板定义
  //创建日期：2011/8/27
  //创建人  ：   曹淑国
  //更新记录：  更新人    更新日期     更新原因/内容
%> 
<script type="text/javascript">

function initForm()
{
	try
	{
		initInpBox();
		//输入框重置
		fm.VoucherID.value='';
		fm.VoucherName.value='';
		fm.VoucherType.value='';
		fm.VoucherTypeName.value='';
		fm.Remark.value='';		
		if(fm.PageFlag.value=='C')
		{			
			//初始化   参数管理-凭证模板定义  			
			fm.all("divPageFlagC1").style.display = '';
			//fm.all("divPageFlagC2").style.display = '';
			fm.all("divButton1").style.display = '';
			//fm.all("divButton2").style.display = 'none';
			fm.all('updatebutton').disabled = true;		
		    fm.all('deletebutton').disabled = true;	
			//initDataC();
		}
		else if(fm.PageFlag.value=='X')
		{
			//初始化  版本管理-凭证模板定义  
			//fm.all("divPageFlagC1").style.display = 'none';
			//fm.all("divPageFlagC2").style.display = 'none';
			//initDataX();
			//fm.all("divButton1").style.display = 'none';
			//fm.all("divButton2").style.display = '';
			fm.all('updatebutton').disabled = true;		
		    fm.all('deletebutton').disabled = true;	
		}
		else if(fm.PageFlag.value=='Q')
		{
			//初始化  综合查询-凭证模板定义
			
		}
		
	}
	catch(re){
		alert("FIVoucherTempDefInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

// 输入框的初始化（单记录部分）
function initInpBox()
{ 
	fm.PageFlag.value = '<%=request.getParameter("pageflag")%>';
	fm.MaintNo.value = '<%=request.getParameter("maintno")%>';
}

// 下拉框的初始化
function initSelBox()
{
  try                 
  { 
  	if(1==1)
  	{
  		
  	}
  }
  catch(ex)
  {
    alert("在FIVoucherTempDefInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
} 
</script>
