//程序名称：FinInterfaceCheck.js
//程序功能：凭证核对
//创建日期：2007-10-23 
//创建人  ：m
//更新记录：  更新人    更新日期     更新原因/内容

var turnPage = new turnPageClass();//日结试算相关数据
var turnPage2 = new turnPageClass();//日结确认相关数据
var mFlag = "0";
var showInfo;
var CheckQueryDataGridTurnPage = new turnPageClass(); 

/****************************************************
*导出相关数据财务接口报表分明细和期间的统计
*********************************************************/ 
function ToExcel()
{
	if(CheckQueryDataGrid.mulLineCount=="0"){
		alert("没有查询到数据");
		return false;
	} 	 	
	fm.action="./FinInterfaceCheckExcel.jsp";
	fm.target="fraSubmit";
	fm.submit(); //提交
}
/************************
*PDF打印功能实现 
***************************/
function printFinInterface(){
	
	if(CheckQueryDataGrid.getSelNo()){	  	
		    var i = 0;
			var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
			showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   
		 	fm.action = "./TestInterfacePrintPDF.jsp?operator=printPDF";//测试PDF打印相关数据
		 	fm.target=".";	 
		 	fm.all("serialNo").value = CheckQueryDataGrid.getRowColData(CheckQueryDataGrid.getSelNo()-1, 1);
		 	fm.submit();
		 	showInfo.close();  
      }else{
        alert("请选择一条记录!");
      }

}

//查询明细按钮
function CheckMXQueryData()
{ 
	try
	{
		var tSel = CheckQueryDataGrid.getSelNo();
		if( tSel == 0 || tSel == null )
		{
			alert( "请您先选择一条记录!" );
			return false;
		}
		var vserialno = CheckQueryDataGrid.getRowColData(tSel-1,11);
		window.open("./FrameFISCertificateCheckInput.jsp?vserialno="+vserialno);
	}
	catch(Ex)
	{
		alert(Ex.message);
	}	
}
/********************
*对页面的条件限制
************************/
function beforeSubmit(){
	if(fm.checkType.value==''||fm.checkType.value==null){
		alert("请选择核对类型");
		fm.checkType.focus();
		return false;
	}
	  	
   if(fm.checkType.value=="1"){
	  if(fm.accountCode.value==''||fm.accountCode.value==null){
	  			alert("请选择科目类型及科目");
	  			fm.accountCodeType.focus();
	  			return false;
	  	}	  	
	  if(fm.FinItemType.value==''||fm.FinItemType.value==null){
	  			alert("请选择借贷标志");
	  			fm.FinItemType.focus();
	  			return false;
	  	}
	  if(fm.ManageCom.value==''||fm.ManageCom.value==null){
	  			alert("请选择管理机构");
	  			fm.ManageCom.focus();
	  			return false;
	  	}		  	  	
	  if(fm.StartDay.value==''||fm.StartDay.value==null){
	  			alert("请选择起始日期");
	  			fm.StartDay.focus();
	  			return false;
	  	}
	  if(fm.EndDay.value==''||fm.EndDay.value==null){
	  			alert("请选择终止日期");
	  			fm.EndDay.focus();
	  			return false;
	  	}
	  if(fm.EndDay.value<fm.StartDay.value){
					alert("起始时间大于终止时间,请重新输入!");
					fm.StartDay.focus();
					return false;
	  }
   }  		  		  	
   else if(fm.checkType.value=="4"){
 	  if(fm.ContType.value==''||fm.ContType.value==null){
	  			alert("请输入业务号码类型！");
	  			fm.ContType.focus();
	  			return false;
	  	}    
	  if(fm.tNo.value==''||fm.tNo.value==null){
	  			alert("请输入业务号码！");
	  			fm.tNo.focus();
	  			return false;
	  	}
	 }	  
   else if(fm.checkType.value=="2"){
 	  if(fm.SClassType.value==''||fm.SClassType.value==null){
	  			alert("请输入凭证类型！");
	  			fm.SClassType.focus();
	  			return false;
	  	}    
	  if(fm.SDate.value==''||fm.SDate.value==null){
	  			alert("请输入起始日期！");
	  			fm.SDate.focus();
	  			return false;
	  	}
	  if(fm.EDate.value==''||fm.EDate.value==null){
	  			alert("请输入结束日期！");
	  			fm.EDate.focus();
	  			return false;
	  	}
	 }	
	else if (fm.checkType.value=="3")
		{
				  if(fm.CertificateID.value==''||fm.CertificateID.value==null){
	  			alert("请输入凭证号码！");
	  			fm.CertificateID.focus();
	  			return false;
	  	}
		}
	return true;
}

/*********************************************************************
 *  后台执行完毕反馈信息
 *  描述: 后台执行完毕反馈信息
 *********************************************************************/
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{
  showInfo.close();
  if (FlagStr == "Fail" )
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
  }
  else
  {
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
    //执行下一步操作
  }
}
// add 2006-9-30 11:49
function showDiv(cDiv,cShow)
{
  if (cShow=="true")
  {
    cDiv.style.display="";
  }
  else
  {
    cDiv.style.display="none";
  }
}

function CheckQueryData()
{
  try
  {
	if(beforeSubmit()){
	
		//var showStr="正在查询数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
		//var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;  
		//showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");   			 	
	
	CheckQueryDataGrid.clearData("CheckQueryDataGrid");   
	 var strSQL = "";
	    if(fm.checkType.value=="4"){//核对类型^1|科目 ^2|业务代码	
				
			var tNoType = "";
			if(fm.ContType.value=="1"||fm.ContType.value=="3")  //个险保单号和个险投保单号
				tNoType = "a.contno";
			else if(fm.ContType.value=="2"||fm.ContType.value=="4") //团险保单号和团险投保单号
				tNoType = "a.grpcontno";
			else if(fm.ContType.value=="5") //暂交费号
				tNoType = "a.tempfeeno";
			else if(fm.ContType.value=="6") //实付号
				tNoType = "a.ActugetNo";
			else if(fm.ContType.value=="7") //实收号
				tNoType = "a.PayNo";
			else if(fm.ContType.value=="8") //个险保全批单号
				tNoType = "a.EndorsementNo";
			else if(fm.ContType.value=="9") //团险保全批单号
				tNoType = "a.EndorsementNo";
			else if(fm.ContType.value=="10") //赔案号
				tNoType = "a.CaseNo";
			strSQL = "select distinct "
	             +"a.batchno as batchno, "
	             +"b.managecom as managecom, "
	             +"b.finitemtype as listflag, "
	             +"b.accountcode as finiteminfo, "
	             +"(select certificatename from FICertificateTypeDef where certificateid = b.certificateid) as classinfo, "
	             +"b.riskcode as riskcode, "
	             +"b.salechnl as salechnl, "
	             +"b.accountdate as accountdate, "
	             +"a.contno as contno, "
	             +"a.BankAccNo as bankdetail, "
	             +"b.budget as budget, "
	             +"b.costcenter as costcenter, "
	             +"to_char(b.summoney,'FM999999999990.00') as money, "
	             +"b.certificateid as classtype, "
	             +"a.BusinessDetail as BusinessDetail, "
	             +"a.businessno as orzno "
	             +" from  FIAboriginalData a,FIDataTransResult b "
		       	 +" where a.aserialno = b.aserialno "
		       	 +" and b.summoney<>0"
	             +" and b.managecom like '" +manageCom + "%%'";
			if(document.getElementById( "batchno").value.length!=0){
				strSQL+=" and b.batchno='"+trim(fm.batchno.value)+"' ";
				
			}
			if(fm.ContType.value=="8"){
				strSQL = strSQL + "and "+tNoType+" in trim((select edoracceptno from lpedormain where edorno = '" + trim(fm.tNo.value) + "'))";
				}
			else if (fm.ContType.value=="9"){
			  strSQL = strSQL + "and "+tNoType+" in trim((select edoracceptno from lpedormain where edorno = '" + trim(fm.tNo.value) + "'))";
			  }	
			else{
				strSQL = strSQL + " and "+ tNoType +" = '" + trim(fm.tNo.value) + "'";
			}

				strSQL = strSQL +" order by accountdate,orzno";

	    }
	    else if(fm.checkType.value=="1"){//核对类型^1|科目 ^2|业务代码

	       	strSQL =  "select BatchNo,depcode,DCFLAG,AccountCode,riskcode,Chinal,chargedate,CostCenter,SumMoney,vouchertype as classinfo,VSerialNo from  FIVoucherDataGather a "
		       	 +" where 1 = 1 "
		       	 +" and a.accountcode like '"+fm.accountCode.value+"%%'"
		       	 +" and a.summoney<>0"
	             +" and a.depcode like '" +fm.ManageCom.value + "%%' "
	       		 +" and a.chargedate >= '" +fm.StartDay.value + "'"
	       		 +" and a.chargedate <= '" +fm.EndDay.value + "'"; 
	    		
	       	if(document.getElementById( "batchno").value.length!=0){
				strSQL+="  and a.batchno='"+trim(fm.batchno.value)+"'";
			}
	      if (fm.FinItemType.value == null || fm.FinItemType.value == '') 	
	      {
	      		strSQL = strSQL + " order by chargedate";
	      }
	      else
	    	{	   
		  	strSQL = strSQL 
		  	       + " and a.DCFLAG = '"  +fm.FinItemType.value + "'"
	       		   + " order by chargedate";
	      }
	    }
	    else if(fm.checkType.value=="2"){

				strSQL = "select BatchNo,depcode,DCFLAG,AccountCode,riskcode,Chinal,chargedate,CostCenter,SumMoney,vouchertype as classinfo,VSerialNo from  FIVoucherDataGather a "
		       	  +" where 1 = 1 "
		       	 +" and a.summoney<>0"
	       		 +" and a.vouchertype  = '" +fm.all("SClassType").value+"'"
	       		 +" and a.depcode like '" +fm.ManageCom1.value + "%%'"
	       		 +" and a.chargedate >= '" +fm.SDate.value + "'"
	       		 +" and a.chargedate <= '" +fm.EDate.value + "'";
				
	       		if(document.getElementById("batchno").value.length!=0){
					strSQL+="  and a.batchno='"+trim(fm.batchno.value)+"' order by chargedate";
				}else{
					strSQL+=" order by chargedate";
				}
	    	}
	    else if(fm.checkType.value=="3"){
				strSQL = "select BatchNo,depcode,DCFLAG,AccountCode,riskcode,Chinal,chargedate,CostCenter,SumMoney,vouchertype as classinfo,VSerialNo from  FIVoucherDataGather a "
		       	 +"where a.VoucherID = '"+fm.CertificateID.value+"' "
	             +"and a.summoney<>0 ";
	             if(fm.batchno.value==''||fm.batchno.value==null){
	            	 strSQL= strSQL+" order by chargedate";	
				  }else{
					 strSQL=strSQL+" and a.batchno='"+trim(fm.batchno.value)+"' order by chargedate";
					}   
	    	}
		//fm.ExportExcelSQL.value=strSQL;//保存sql，导出excel时用到
	    CheckQueryDataGridTurnPage.queryModal(strSQL,CheckQueryDataGrid);
		//showInfo.close();
		
		if(CheckQueryDataGrid.mulLineCount=="0"){
			alert("没有查询到数据");
		}	

		return true;		
		}
	  }
	  catch(ex)
	  {
	     alert(ex);
	   }
}

function afterCodeSelect(cCodeName, Field) 
{
	if(cCodeName == "checkType") 
	{  
		if(fm.checkType.value=="1"){
			fm.all("divSearch1").style.display = '';
			fm.all("divSearch2").style.display = 'none';
			fm.all("divSearch3").style.display = 'none';
			fm.all("divSearch4").style.display = 'none';
			fm.batchno.value='';
		}else if(fm.checkType.value=="2"){
			fm.all("divSearch2").style.display = '';
			fm.all("divSearch1").style.display = 'none';	
			fm.all("divSearch3").style.display = 'none';		
			fm.all("divSearch4").style.display = 'none';
			fm.batchno.value='';
		}else if(fm.checkType.value=="3"){
			fm.all("divSearch3").style.display = '';
			fm.all("divSearch2").style.display = 'none';
			fm.all("divSearch1").style.display = 'none';	
			fm.all("divSearch4").style.display = 'none';
			fm.batchno.value='';
		}else if(fm.checkType.value=="4"){
			fm.all("divSearch4").style.display = '';
			fm.all("divSearch2").style.display = 'none';
			fm.all("divSearch1").style.display = 'none';	
			fm.all("divSearch3").style.display = 'none';	
			fm.batchno.value='';
		}
		
	}
}

//显示数据的函数，和easyQuery及MulLine 一起使用
function showRecord(strRecord)
{

  //保存查询结果字符串
  turnPage.strQueryResult  = strRecord;

//alert(strRecord);
  
  //使用模拟数据源，必须写在拆分之前
  turnPage.useSimulation   = 1;  
    
  //查询成功则拆分字符串，返回二维数组
  var tArr = decodeEasyQueryResult(turnPage.strQueryResult);
  
  //与MULTILINE配合,使MULTILINE显示时的字段位置匹配数据库的字段位置
  var filterArray = new Array(0,9,4,1,7);
  //清空数据容器，两个不同查询共用一个turnPage对象时必须使用，最好加上，容错
  turnPage.arrDataCacheSet = clearArrayElements(turnPage.arrDataCacheSet);
  
  //过滤二维数组，使之与MULTILINE匹配
  turnPage.arrDataCacheSet = chooseArray(tArr, filterArray);
  
  //设置初始化过的MULTILINE对象，VarGrid为在初始化页中定义的全局变量
  //初始化的对象
  	 turnPage.pageDisplayGrid = CheckQueryDataGrid;       
              
  //设置查询起始位置
  turnPage.pageIndex       = 0;  
  
  //在查询结果数组中取出符合页面显示大小设置的数组
  var arrDataSet = turnPage.getData(turnPage.arrDataCacheSet, turnPage.pageIndex, turnPage.pageLineNum);
  
  //调用MULTILINE对象显示查询结果
  displayMultiline(arrDataSet, turnPage.pageDisplayGrid);	
  
  //控制是否显示翻页按钮
  if (turnPage.queryAllRecordCount > turnPage.pageLineNum) {
    try { window.divPage.style.display = ""; } catch(ex) { }
  } else {
    try { window.divPage.style.display = "none"; } catch(ex) { }
  }
  
  //必须将所有数据设置为一个数据块
  turnPage.blockPageNum = turnPage.queryAllRecordCount / turnPage.pageLineNum;
	
}
