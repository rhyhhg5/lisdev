<%
  //程序名称：FIDataExtPlanExeInit.jsp
  //程序功能：数据抽取计划执行
  //创建日期：2011-8-24
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">

function initForm(){
	try{
	    
		initFIDataExtractGrid();
		//initQuery();
	}
	catch(re){
		alert("FIDataExtPlanExeInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initInputBox(){
	try{
		fm.IndexType.value = "";
		fm.IndexidNo.value = "";
		fm.IndexTypeName.value = "";
	}
	catch(ex){
		alert(ex);
	}
}

function initFIDataExtractGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=90;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="流水号";
		iArray[1][1]="90px";
		iArray[1][2]=90;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="保单号";
		iArray[2][1]="90px";
		iArray[2][2]=100;
		iArray[2][3]=0;

		iArray[3]=new Array();
		iArray[3][0]="校验状态";
		iArray[3][1]="90px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="借贷类型";
		iArray[4][1]="90px";
	    iArray[4][2]=100;
		iArray[4][3]=0;


		FIDataExtractGrid = new MulLineEnter( "fm" , "FIDataExtractGrid" ); 

		FIDataExtractGrid.mulLineCount=2;
		FIDataExtractGrid.displayTitle=1;
		FIDataExtractGrid.canSel=0;
		FIDataExtractGrid.canChk=1;
		FIDataExtractGrid.hiddenPlus=1;
		FIDataExtractGrid.hiddenSubtraction=1;

		FIDataExtractGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
