<%
  //程序名称：FIMetaDataValueDefInit.jsp
  //程序功能：元数据值域定义
  //创建日期：2011/9/13
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">
var tMetaDataNo = '<%=request.getParameter("MetadataNo")%>';
var tAttNo = '<%=request.getParameter("AttNo")%>';

function initForm(){
	try
	{
		fm.MainValues.value='';
		fm.ValueMeaning.value='';
		fm.Remark.value='';
		
		initMetadataValueGrid();
		InitPage();
		fm.MetadataNo.value = tMetaDataNo;
		fm.AttNo.value = tAttNo;
	}
	catch(re){
		alert("FIMetaDataValueDefInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}

function initMetadataValueGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="元数据编码";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=0;

		iArray[2]=new Array();
		iArray[2][0]="值域流水号";
		iArray[2][1]="90px";
		iArray[2][2]=100;
		iArray[2][3]=3;

		iArray[3]=new Array();
		iArray[3][0]="属性编码";
		iArray[3][1]="145px";
		iArray[3][2]=100;
		iArray[3][3]=0;

		iArray[4]=new Array();
		iArray[4][0]="值域类型";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=3;
		
		iArray[5]=new Array();
		iArray[5][0]="值域类型名称";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=3;
		
		iArray[6]=new Array();
		iArray[6][0]="匹配符";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=3;
		
		iArray[7]=new Array();
		iArray[7][0]="主值域";
		iArray[7][1]="60px";
		iArray[7][2]=100;
		iArray[7][3]=0;
		
		iArray[8]=new Array();
		iArray[8][0]="次值域";
		iArray[8][1]="60px";
		iArray[8][2]=100;
		iArray[8][3]=3;
		
		iArray[9]=new Array();
		iArray[9][0]="值域含义";
		iArray[9][1]="160px";
		iArray[9][2]=100;
		iArray[9][3]=0;
		
		iArray[10]=new Array();
		iArray[10][0]="说明";
		iArray[10][1]="160px";
		iArray[10][2]=100;
		iArray[10][3]=0;
		
		MetadataValueGrid = new MulLineEnter( "fm" , "MetadataValueGrid" ); 
		MetadataValueGrid.mulLineCount=1;
		MetadataValueGrid.displayTitle=1;
		MetadataValueGrid.canSel=1;
		MetadataValueGrid.canChk=0;
		MetadataValueGrid.hiddenPlus=1;
		MetadataValueGrid.hiddenSubtraction=1;
		MetadataValueGrid.selBoxEventFuncName="getValuedata";

		MetadataValueGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}
</script>
