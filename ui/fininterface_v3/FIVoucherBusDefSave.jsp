<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp"%>

<%@page contentType="text/html;charset=GBK"%>
<%
//程序名称：FIVoucherFeeDefSave.jsp
//程序功能：凭证费用定义
//创建日期：2011/8/27
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容
%>

<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.fininterface_v3.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>

<%
	//接收信息，并作校验处理。
	//输入参数
	request.setCharacterEncoding("GBK");
	
	//××××Schema t××××Schema = new ××××Schema();
	FIVoucherBnUI tFIVoucherTypeBusUI = new FIVoucherBnUI(); 
	FIVoucherBnSchema tFIVoucherTypeBusSchema = new FIVoucherBnSchema();
	//输出参数
	CErrors tError = null;
	String FlagStr = "";
	String Content = "";
	String transact = "";

	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getAttribute("GI");

	//执行动作：insert 添加纪录，update 修改纪录，delete 删除纪录
	transact = request.getParameter("hideOperate");
	String voucherno = request.getParameter("VersionNo");
	String voucherid = request.getParameter("voucherid");
	String vouchername = request.getParameter("vouchername");
	String FIBusID = request.getParameter("FIBusID");
	String FIBusIDName = request.getParameter("FIBusIDName");
	String pageflag = request.getParameter("pageflag");

	//准备传输数据VData
	VData tVData = new VData();

	//传输schema
	//tVData.addElement(t××××Schema);

	tVData.add(tG);
	tVData.add(voucherno);
	if ("C".equals(pageflag)) {
		
			tFIVoucherTypeBusSchema.setVoucherID(voucherid);
			tFIVoucherTypeBusSchema.setVersionNo(voucherno);
			tFIVoucherTypeBusSchema.setVoucherName(vouchername);
			tFIVoucherTypeBusSchema.setBusinessID(FIBusID);
			tFIVoucherTypeBusSchema.setBusinessName(FIBusIDName);
			tVData.add(tFIVoucherTypeBusSchema);
		try {

			tFIVoucherTypeBusUI.submitData(tVData, transact);
		} catch (Exception ex) {
			Content = "保存失败，原因是:" + ex.toString();
			FlagStr = "Fail";
		}
	}

	if ("X".equals(pageflag)) {
	}

	//如果在Catch中发现异常，则不从错误类中提取错误信息
	if (FlagStr == "") {
		tError = tFIVoucherTypeBusUI.mErrors;
		if (!tError.needDealError()) {
			Content = " 保存成功! ";
			FlagStr = "Success";
		} else {
			Content = " 保存失败，原因是:" + tError.getFirstError();
			FlagStr = "Fail";
		}
	}

	//添加各种预处理
%>
<%=Content%>
<html>
	<script type="text/javascript">
parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
</script>
</html>
