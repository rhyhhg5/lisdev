//该文件中包含客户端需要处理的函数和事件

//程序名称：FIBusTypeDef.js
//程序功能：业务交易定义
//创建日期：2011/8/25
//创建人  ：
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
var mDebug="0";
window.onfocus=myonfocus;
var turnPage = new turnPageClass();



//查询按钮
function bustypequery()
{
	window.open("./FrameFiBusiTypeQueryInput.jsp");
}

/*********************************************************************
 *  查询返回明细信息时执行的函数,查询返回一个2维数组，数组下标从[0][0]开始
 *  参数  ：  查询返回的二维数组
 *  返回值：  无
 *********************************************************************
 */
//针对版本信息查询子窗口返回的2维数组
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		if(arrResult[0][1]!=null&&arrResult[0][1]!='')
			fm.all('BusTypeId').value = arrResult[0][1];
		
		if(arrResult[0][2]!=null&&arrResult[0][2]!='')
			fm.all('BusTypeName').value = arrResult[0][2];
		
		if(arrResult[0][3]!=null&&arrResult[0][3]!='')
			fm.all('FIBusType').value = arrResult[0][3];
		
		if(arrResult[0][4]!=null&&arrResult[0][4]!='')
			fm.all('FIDetailType').value = arrResult[0][4];
		
		if(arrResult[0][5]!=null&&arrResult[0][5]!='')
			fm.all('ObjectID').value = arrResult[0][5];
				
		if(arrResult[0][6]!=null&&arrResult[0][6]!='')
			fm.all('ObjectName').value = arrResult[0][6];
		
		if(arrResult[0][7]!=null&&arrResult[0][7]!='')
			fm.all('FIIndexCode').value = arrResult[0][7];
		
		if(arrResult[0][8]!=null&&arrResult[0][8]!='')
			fm.all('FIIndexName').value = arrResult[0][8];
		if(arrResult[0][8]!=null&&arrResult[0][8]!='')
			fm.all('FIBusTypename').value = arrResult[0][9];
		if(arrResult[0][8]!=null&&arrResult[0][8]!='')
			fm.all('FIDetailTypeName').value = arrResult[0][10];
		

	}
}


//业务信息定义按钮
function businfodef()
{
	var BusTypeId=fm.all('BusTypeId').value;
	var BusTypeName = fm.BusTypeName.value;
	
	if(BusTypeId==''||BusTypeId==null)
	{
		alert("业务交易编码不能为空！");
		return;
	}
	pageflag=fm.pageflag.value;
	window.open("./FrameFIBusInfoDefInput.jsp?BusTypeId="+BusTypeId+"&pageflag="+pageflag+"&maintno="+maintno+"&BusTypeName="+BusTypeName);
}

//费用信息定义按钮
function busfeetypeinfodef()
{
	var BusTypeId=fm.all('BusTypeId').value;
	var BusTypeName = fm.BusTypeName.value;
	if(BusTypeId==''||BusTypeId==null)
	{
		alert("业务交易编码不能为空！");
		return;
	}
	pageflag=fm.pageflag.value;
	window.open("./FrameFIBusFeeTypeDefInput.jsp?BusTypeId="+BusTypeId+"&pageflag="+pageflag+"&maintno="+maintno+"&BusTypeName="+BusTypeName);
}


//提交前的校验、计算  
function beforeSubmit()
{
	//添加操作 
	if(fm.all('BusTypeId').value=='')
	{
		alert("业务交易编号不能为空！");
		return false;
	}

	return true;		
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    //执行下一步操作
  }
  
  location.reload();
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";
  }
}
//增加按钮
function addClick()
{

	if(!beforeSubmit())
	{
		return false;	
	}
	
	fm.all('transact').value='insert';
	
	//校验是否为新增数据
//	var checksql = "select count(*) from fmbustypedef where bustypeid='"+fm.all('BusTypeId').value+"' and maintno = '"+fm.all('maintno').value+"'" ;
	var checksql = "select count(*) from FIBnTypeDef where BusinessID='"+fm.all('BusTypeId').value+"' " ;	
	var reuslt=easyExecSql(checksql);
	if(reuslt!="0")
	{
		alert("该业务类型编码已存在！");
		return false;		
	}
	submitForm();
}
//修改按钮
function updateClick()
{
	if(!beforeSubmit())
	{
		return false;	
	}
	fm.all('transact').value='update';
	//校验是否为修改数据
//	var checksql = "select count(*) from fmbustypedef where bustypeid='"+fm.all('BusTypeId').value+"' and maintno = '"+fm.all('maintno').value+"'" ;
	var checksql = "select count(*) from FIBnTypeDef where BusinessID='"+fm.all('BusTypeId').value+"' " ;	
	var reuslt=easyExecSql(checksql);
	//alert(reuslt);
	if(reuslt=="0")
	{
		alert("该业务类型编码不存在！");
		return false;		
	}
	submitForm();
}

//删除按钮
function deleteClick()
{
	if(!beforeSubmit())
	{
		return false;	
	}
	fm.transact.value='delete';
	submitForm();
}
//重置按钮
function resetAgain()
{
	initForm();
}

//提交页面
function submitForm()
{
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	if (fm.transact.value=="")
	{
		alert("操作控制数据丢失！");
	}
	fm.pageflag.value='C';
	fm.action="./FIBusTypeDefSave.jsp";
	//lockButton(); 
	fm.submit(); //提交
}
