<%@page contentType="text/html;charset=GBK" %>
<%@include file="../common/jsp/UsrCheck.jsp"%>

<html>

<%
 //程序名称：FifindatacheckInput.jsp
 //程序功能：校验定义
 //创建日期：2011/9/15
 //创建人  ：
 //更新记录：  更新人    更新日期     更新原因/内容
%>

<head>
  <SCRIPT src="../common/javascript/Common.js"></SCRIPT>
  <SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
  <SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
  <SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>  
  <SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
  <SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
  <LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
  <LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
 
  <SCRIPT src="Sefindatacheck.js"></SCRIPT>
  <%@include file="SefindatacheckInit.jsp"%>
</head>
<body  onload="initForm();initElementtype();">
<form action="./FifindatacheckSave.jsp" method=post name=fm target="fraSubmit">

<table>
  <tr> 
    <td class="titleImg" >校验类型定义</td>
  </tr>
</table>
<table  class="common" >
	<tr class="common">
	    <td class="title">校验类型</td><td class="input" >
	    	<input class="codeno" name="Ruletype" 
			    CodeData="0|^01|质检规则|^02|差异规则" verify="校验类型|notnull"
			    ondblclick="showCodeListEx('Ruletype',[this,RuletypeName],[0,1],null,null,null,1);"
				onkeyup="showCodeListKeyEx('Ruletype',[this,RuletypeName],[0,1],null,null,null,1);" readOnly onpropertychange="changetype();"><input class="codename" name="RuletypeName"  readonly=true elementtype=nacessary>	
		</td>
		<td id="td1" style="display:'';" class="title">校验节点</td>
		<TD  id="td2" style="display:'';" class= input>
			<Input class=codeno name=CallPointID verify="校验节点|NOTNULL" 
		      		ondblclick="return showCodeList('fichecknode',[this,CallPointIDName],[0,1],null,null,null,'1',null);" 
		      		onkeyup="showCodeListKey('fichecknode',[this,CallPointIDName],[0,1],null,null,null,'1',null)" readOnly><input class=codename name=CallPointIDName readonly=true >
	    </TD>	
		<td class="title"></td>
		<TD><input class="common" name="" type="hidden"></TD>		
	</tr>
</table><br>
<input value="查 询"   class="cssButton" type="button" onclick="QueryDate();">
<INPUT VALUE="返  回" TYPE=button onclick="returnParent();" class="cssButton">
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
<INPUT VALUE="关 闭" TYPE=button onclick="closed();" class="cssButton">
<br>
<br>
<table>
  <tr>
    <td class="titleImg" >校验内容选择</td>
  </tr>
</table>

<table  class= common>
   <tr  class= common>
      <td text-align: left colSpan=1>
     <span id="spanDataCheckGrid" >
     </span> 
      <br></td>
   </tr>
</table>
<Div  id= "DataCheckGridFY" style= "display: ''" align=center text-align:left>
 	  <INPUT VALUE="首  页" class = cssButton TYPE=button onclick="getFirstPage();"> 
      <INPUT VALUE="上一页" class = cssButton TYPE=button onclick="getPreviousPage();">
      <INPUT VALUE="下一页" class = cssButton TYPE=button onclick="getNextPage();">
      <INPUT VALUE="尾  页"  class =  cssButton TYPE=button onclick="getLastPage();">
</Div>
    
<input class="common" name="VersionNo" cols="100%" rows="2" type=hidden>
</form>
<span id="spanCode"  style="display: none; position:absolute; slategray"></span>
</body>
</html>
