//该文件中包含客户端需要处理的函数和事件
//程序名称：FIVoucherTempDef.js
//程序功能：凭证模板定义
//创建日期：2011/8/27
//创建人  ：曹淑国
//更新记录：  更新人    更新日期     更新原因/内容
 
var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

//Click事件，当点击“查询”图片时触发该函数
//进入版本信息查询页面的查询按钮
function queryClick1()
{
	showInfo=window.open("./FrameVersionRuleQueryForOther.jsp");
}
//针对版本信息查询子窗口返回的2维数组
function afterQuery( arrQueryResult )
{
	var arrResult = new Array();
	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		//alert(arrResult);
		fm.all('VersionNo').value = arrResult[0][0];
		//VersionState显示为01，02，03；而VersionState2显示为正常、维护、删除
		fm.all('VersionState').value = arrResult[0][1];		
		fm.all('VersionState2').value = arrResult[0][2];		
	}
	
	    //当版本状态不为02-维护的时候，增删改按钮为灰色		
	if (arrResult[0][1] == "01"||arrResult[0][1] == "03"||arrResult[0][1] == ""||arrResult[0][1] == null)
	{
		fm.all('addbutton').disabled = false;				
		fm.all('updatebutton').disabled = true;		
		fm.all('deletebutton').disabled = true;	
	}
	if (arrResult[0][1] == "02")
	{
		fm.all('addbutton').disabled = false;				
		fm.all('updatebutton').disabled = false;		
		fm.all('deletebutton').disabled = false;	
	}
	
	//showCodeName(); 
}
//凭证类型查询按钮
function Vouchertypequery()
{
	if(fm.VersionNo.value=='')
	{
		alert("请先进行版本查询！");
		return false;
	}
	showInfo=window.open("./FrameVoucherTypeQuery.jsp?versionno="+fm.VersionNo.value);
}

//针对凭证类型查询子窗口返回的2维数组
function afterQuery1( arrQueryResult )
{
	var arrResult = new Array();

	if( arrQueryResult != null )
	{
		arrResult = arrQueryResult;
		if(arrResult.length>0&&arrResult[0].length==6)
		{
			fm.all('VoucherID').value = arrResult[0][1];
			fm.all('VoucherName').value = arrResult[0][2];
			fm.all('VoucherType').value = arrResult[0][3];
			fm.all('VoucherTypeName').value = arrResult[0][4];
			fm.all('Remark').value = arrResult[0][5];
		}
	}		
}

//版本管理，自动初始化信息
function initDataX()
{
	var strSQL = "select a.Versionno,a.Versionstate,(select codename from ldcode  where code =Versionstate  and codetype = 'versionstate'),b.VoucherID,VoucherName,VoucherType,"
		+"(select codename from ldcode where code =VoucherType and codetype = 'fivouchertype'),Remark ,OperationType"
		+" from FIRulesVersion a,FIMaintainItem c,FMVoucherTypeDef b  where  a.Versionno = b.Versionno and b.Maintno= c.Maintno and b.Maintno = '"+fm.MaintNo.value+"'";
	
	var arrResult=easyExecSql(strSQL);
	if (arrResult!=null||arrResult!="")
	{
		fm.VersionNo.value = arrResult[0][0];
		fm.VersionState.value = arrResult[0][1];
		fm.VersionState2.value = arrResult[0][2];
		
		fm.VoucherID.value = arrResult[0][3];
		fm.VoucherName.value = arrResult[0][4];
		fm.VoucherType.value = arrResult[0][5];
		fm.VoucherTypeName.value = arrResult[0][6];
		fm.Remark.value = arrResult[0][7];
//		fm.Transact.value = arrResult[0][8];
	}
}
//参数管理，自动初始化信息
function initDataC()
{
	//自动初始化一个处在维护状态的版本
	var strSQL =" select a.VersionNo,a.VersionState, (select codename from ldcode where codetype = 'versionstate' and code = VersionState) from FIRulesVersion a  where  a.VersionState ='02' fetch first 1 rows only ";
	
	var arrResult=easyExecSql(strSQL);
	if(arrResult.length>0)
	{
		afterQuery( arrResult );
	}
	
}


//关联业务交易
function gotovoucher()
{
	if(fm.all('voucherid').value==''||fm.all('voucherid').value==null)
	{
		alert("凭证编码不能为空！");
		return;
	}
	window.open("./FrameFIVoucherBusDefInput.jsp?voucherid="+fm.all('voucherid').value+"&pageflag="+fm.PageFlag.value+"&versionno="+fm.all('versionno').value);
}

//定义凭证费用按钮
function gotovoucherfee()
{
	if(fm.all('voucherid').value==''||fm.all('voucherid').value==null)
	{
		alert("凭证编码不能为空！");
		return;
	}
	window.open("./FrameFIVoucherFeeDefInput.jsp?voucherid="+fm.all('voucherid').value+"&pageflag="+fm.PageFlag.value+"&versionno="+fm.all('versionno').value);
}

//增加
function addClick()
{
	//提交前的检验
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	var checksql = "select count(1) from FIVoucherDef where versionno='"+fm.all('VersionNo').value+"' and voucherid='"+fm.all('VoucherID').value+"' " ;	
	var reuslt=easyExecSql(checksql);
	if(reuslt!="0")
	{
		alert("该凭证类型编码已存在！");
		return false;		
	}
		fm.all('fmTransact').value='insert';
		submitForm();
   
}

//修改
function updateClick()
{
	//提交前的检验
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	
	var checksql = "select count(1) from FIVoucherDef where versionno='"+fm.all('VersionNo').value+"' and voucherid='"+fm.all('VoucherID').value+"' " ;	
	var reuslt=easyExecSql(checksql);
	if(reuslt=="0")
	{
		alert("该凭证类型编码不存在，请确认编码！");
		return false;		
	}
	
		fm.all('fmTransact').value='update';
		submitForm();
   
}
//删除
function deleteClick()
{
	//提交前的检验
	if (!beforeSubmit()) //beforeSubmit()函数
	{
		return false;
	}
	var checksql = "select count(1) from FIVoucherDef where versionno='"+fm.all('VersionNo').value+"' and voucherid='"+fm.all('VoucherID').value+"' " ;	
	var reuslt=easyExecSql(checksql);
	if(reuslt=="0")
	{
		alert("该凭证类型编码不存在，请确认编码！");
		return false;		
	}
	
	fm.all('fmTransact').value='delete';
	submitForm();
   
}
//重置
function resetAgain()
{
	initForm();	
}

function submitForm()
{
	if (fm.fmTransact.value=="")
	{
		alert("操作控制数据丢失！");
		return false;
	}
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
	showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	
	fm.submit(); //提交
}

//提交前的校验、计算  
function beforeSubmit()
{
	//添加操作 
	if(fm.VoucherID.value == "")
	{
		alert("凭证编码不能为空！");
		return false;
	}
	if(fm.VoucherName.value == "")
	{
		alert("凭证名称不能为空！");
		return false;
	}
	if(fm.VoucherType.value == "")
	{
		alert("凭证类型不能为空！");
		return false;
	}
	if(fm.VoucherTypeName.value == "")
	{
		alert("凭证类型名称不能为空！");
		return false;
	}
	if (!verifyInput2()) 
	{
  		return false;
	}
	return true;
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus()
{
	if(showInfo!=null)
	{
		try
		{
			showInfo.focus();  
		}
		catch(ex)
		{
			showInfo=null;
		}
	}
}


//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ,tTransact)
{
	showInfo.close();
	if (FlagStr == "Fail" )
	{             
		var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
		showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
	else
	{ 
		
	    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
	    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	    //执行下一步操作
	    if(tTransact=='delete')
		{
			location.reload();
		}
	}
}

function showDiv(cDiv,cShow)
{
	if (cShow=="true")
	{
		cDiv.style.display="";
	}
	else
	{
    	cDiv.style.display="none";  
	}
}
