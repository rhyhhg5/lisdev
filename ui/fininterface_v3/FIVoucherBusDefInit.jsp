<%
  //程序名称：FIVoucherBusDefInit.jsp
  //程序功能：凭证费用定义
  //创建日期：2011/8/27
  //创建人  ：
  //更新记录：  更新人    更新日期     更新原因/内容
%>
<script type="text/javascript">

function initForm(){
	try
	{	
		initInBox();
		initVoucherTypeGrid();		
		initpage();
		fm.voucherid.value=voucherid;
		fm.vouchername.value=reuslt;
		fm.pageflag.value='<%=request.getParameter("pageflag")%>';
		if("Q"==fm.pageflag.value)
		{
		
			buttonsdiv.style.display = 'none';
		} 
	}
	catch(re){
		alert("FIVoucherBusDefInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
	}
}
function initInBox()
{
	fm.VersionNo.value = versionno;
	
}

function initVoucherTypeGrid(){
	var iArray = new Array();
	try{
		iArray[0]=new Array();
		iArray[0][0]="序号";
		iArray[0][1]="30px";
		iArray[0][2]=100;
		iArray[0][3]=0;

		iArray[1]=new Array();
		iArray[1][0]="版本号";
		iArray[1][1]="60px";
		iArray[1][2]=100;
		iArray[1][3]=1;

		iArray[2]=new Array();
		iArray[2][0]="凭证编码";
		iArray[2][1]="60px";
		iArray[2][2]=100;
		iArray[2][3]=1;

		iArray[3]=new Array();
		iArray[3][0]="凭证名称";
		iArray[3][1]="60px";
		iArray[3][2]=100;
		iArray[3][3]=1;

		iArray[4]=new Array();
		iArray[4][0]="凭证类型";
		iArray[4][1]="60px";
		iArray[4][2]=100;
		iArray[4][3]=1;
		
		iArray[5]=new Array();
		iArray[5][0]="业务编码";
		iArray[5][1]="60px";
		iArray[5][2]=100;
		iArray[5][3]=1;

		iArray[6]=new Array();
		iArray[6][0]="业务名称";
		iArray[6][1]="60px";
		iArray[6][2]=100;
		iArray[6][3]=1;


		VoucherTypeGrid = new MulLineEnter( "fm" , "VoucherTypeGrid" ); 

		VoucherTypeGrid.mulLineCount=1;
		VoucherTypeGrid.displayTitle=1;
		VoucherTypeGrid.canSel=1;
		VoucherTypeGrid.canChk=0;
		VoucherTypeGrid.hiddenPlus=1;
		VoucherTypeGrid.hiddenSubtraction=1;
		VoucherTypeGrid.selBoxEventFuncName="selectdata";

		VoucherTypeGrid.loadMulLine(iArray);

	}
	catch(ex){
		alert(ex);
	}
}

</script>
