<!--用户校验类-->

<script language="JavaScript">


// 输入框的初始化
function initInpBox()
{
  try
  {
    
  }
  catch(ex)
  {
    alert("在VersionRuleQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
  }
}


// 下拉框的初始化
function initSelBox()
{
  try
  {
  	
  }
  catch(ex)
  {
    alert("在VersionRuleQueryInit.jsp-->InitSelBox函数中发生异常:初始化界面错误!");
  }
}


function initForm()
{
  try
  {
    initInpBox();
    initSelBox();    
    initCommonRuleGrid();
  }
  catch(re)
  {
    alert("VersionRuleQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
  }
}


function initCommonRuleGrid()
{
	var iArray = new Array();
      
      try
      {
      iArray[0]=new Array();
      iArray[0][0]="序号";         			//列名（此列为顺序号，列名无意义，而且不显示）
      iArray[0][1]="30px";         			//列宽
      iArray[0][2]=10;          			//列最大值
      iArray[0][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[1]=new Array();
      iArray[1][0]="版本编号";    	//列名
      iArray[1][1]="120px";            		//列宽
      iArray[1][2]=100;            			//列最大值
      iArray[1][3]=0;              			//是否允许输入,1表示允许，0表示不允许
 
      iArray[2]=new Array();
      iArray[2][0]="规则编码";         			//列名
      iArray[2][1]="80px";            		//列宽
      iArray[2][2]=60;            			//列最大值
      iArray[2][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[3]=new Array();
      iArray[3][0]="规则名称";         			//列名
      iArray[3][1]="120px";            		//列宽
      iArray[3][2]=60;            			//列最大值
      iArray[3][3]=0;              			//是否允许输入,1表示允许，0表示不允许      
      
      iArray[4]=new Array();
      iArray[4][0]="规则类型";         			//列名
      iArray[4][1]="80px";            		//列宽
      iArray[4][2]=60;            			//列最大值
      iArray[4][3]=0;              			//是否允许输入,1表示允许，0表示不允许

      iArray[5]=new Array();
      iArray[5][0]="规则明细类型";         		//列名
      iArray[5][1]="80px";            		//列宽
      iArray[5][2]=100;            			//列最大值
      iArray[5][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      
      iArray[6]=new Array();
      iArray[6][0]="处理方式";         		//列名
      iArray[6][1]="60px";            		//列宽
      iArray[6][2]=100;            			//列最大值
      iArray[6][3]=0;              			//是否允许输入,1表示允许，0表示不允许,2表示代码引用
      
      iArray[7]=new Array();
      iArray[7][0]="规则状态";         			//列名
      iArray[7][1]="50px";            		//列宽
      iArray[7][2]=100;            			//列最大值
      iArray[7][3]=0;              			//是否允许输入,1表示允许，0表示不允许      

      

      CommonRuleGrid = new MulLineEnter( "fm" , "CommonRuleGrid" ); 
      CommonRuleGrid.mulLineCount = 0;   
      CommonRuleGrid.displayTitle = 1;
      CommonRuleGrid.canSel=1;
      CommonRuleGrid.locked = 1;	
	  CommonRuleGrid.hiddenPlus = 1;
	  CommonRuleGrid.hiddenSubtraction = 1;
      CommonRuleGrid.loadMulLine(iArray);  
      CommonRuleGrid.detailInfo="单击显示详细信息";
     
      }
      
      catch(ex)
      {
        alert("初始化RulesVersionGrid时出错："+ ex);
      }

}

</script>