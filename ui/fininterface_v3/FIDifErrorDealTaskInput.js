
//创建日期： 
//创建人   jw
//更新记录：  更新人    更新日期     更新原因/内容
var showInfo;

var turnPage = new turnPageClass();
var turnPage1 = new turnPageClass();
var k = 0;

/*********************************************************************
 *  执行新契约扫描的“开始录入”
 *  描述:进入无扫描录入页面
 *  参数  ：  无
 *  返回值：  无
 *********************************************************************
 */
function ApplyInput()
{

	if(fm.DealType.value==''){
		alert("处理方式不能为空！");
		return ;
	}
	if(fm.DetailReMark.value==''){
		alert("处理描述不能为空！");
		return ;
	}
	var tSel = RBResultGrid.getSelNo();

	if( tSel == 0 || tSel == null )
	{	//top.close();
		alert( "请您先选择一条记录，再点击数据处理按钮。" );
		return false;	
	}	
	fm.all('ErrSerialNo').value = RBResultGrid.getRowColData(tSel-1,2);
    var showStr="正在提交数据，请您稍候并且不要修改屏幕上的值或链接其他页面";                                             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;                                             
    showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");	        
	fm.submit();
}

//查询
function QueryDate() 
{		 	
		var StartDate = fm.all('StartDate').value;
		var EndDate = fm.all('EndDate').value;
		var strSQL = ""; 
		
		strSQL = "select a.CheckBatchNo,b.ErrSerialNo,b.CQNo,b.IndexCode,(select codename from ldcode a where codetype = 'fiindexid' and code=b.indexcode fetch first 1 rows only),"+
			 " b.BusinessNo,b.Aserialno,b.ErrInfo,(case when a.CheckType='00' then '质量校检' else '差异校检' end) CheckTypeName,(select codename from ldcode where codetype = 'fierrstate' and code=b.DealState),a.Operator,a.makedate"+
			 " from FIRuleDealLog a,FIRuleDealErrLog b where a.CheckBatchNo=b.CheckBatchNo ";
		
		strSQL = strSQL+ getWherePart('b.CheckBatchNo','CheckBatchNo')
					   + getWherePart('b.CallPointID','CallPointID')
					   + getWherePart('b.CQNo','cqNo')
					   + getWherePart('b.DealState','State');	
		
		if(fm.all('StartDate').value != '')
		{
			strSQL = strSQL + " and makedate >= '"+fm.all('StartDate').value+"' ";
		}
		if(fm.all('EndDate').value != '')
		{
			strSQL = strSQL + " and makedate <= '"+fm.all('EndDate').value+"' ";
		}
  	
  		turnPage.queryModal(strSQL, RBResultGrid);
} 
//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content )
{ 
	try
	{
		showInfo.close(); 
		if (FlagStr == "Fail" )
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}
		else
		{ 
			var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;
			showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
		}	
	}catch(ex){}
	initForm();
}


function ReturnData()
{
	var tSel = RBResultGrid.getSelNo();	
	fm.CQNo.value = RBResultGrid.getRowColData(tSel-1,3);	
}
