//该文件中包含客户端需要处理的函数和事件

//程序名称：FIBusFeeTypeDef.js
//程序功能：业务交易费用定义
//创建日期：2011/8/25
//创建人  ： 董健
//更新记录：  更新人    更新日期     更新原因/内容

var showInfo;
window.onfocus=myonfocus;
var turnPage = new turnPageClass();

var Mul9GridTurnPage = new turnPageClass(); 
var Mul10GridTurnPage = new turnPageClass(); 

//初始化查询
function initpage()
{
	if("C"==pageflag||"Q"==pageflag)
	{
		var sql="select BusinessID,costid,costname,remark from FIBnFeeTypeDef where BusinessID='"+BusTypeId+"'";
	}
	if("X"==pageflag)
	{
		var sql="select BusinessID,costid,costname,remark from FIBnFeeTypeDef where BusinessID='"+BusTypeId+"' and maintno = '"+maintno+"'";
	}
	
	turnPage.queryModal(sql, BusFeeInfoGrid);
}

//单击后显示费用信息
function getFeeTypeInfo()
{
	var tSel = BusFeeInfoGrid.getSelNo();
	var costid = BusFeeInfoGrid.getRowColData(tSel-1,2);
	
	fm.costid.value = costid;
	
	fm.all('FIFeeCode').value=BusFeeInfoGrid.getRowColData(tSel-1,2);
	fm.all('FIFeeCodeName').value=BusFeeInfoGrid.getRowColData(tSel-1,3);
	fm.all('FIFeeRemark').value=BusFeeInfoGrid.getRowColData(tSel-1,4);
	//alert(costid);
	
	if("C"==pageflag||"Q"==pageflag)
	{
		var tsql="select propertycode,propertyname,busclass,busclassname,(select codename from ldcode where codetype = 'fiusage' and code=purpose),feevalues,purpose from FIBnFeeInfo where costid = '"+costid+"' and purpose = '01'";
	}
	if("X"==pageflag)
	{
		var tsql="select propertycode,propertyname,busclass,busclassname,(select codename from ldcode where codetype = 'fiusage' and code=purpose),feevalues,purpose from FIBnFeeInfo where costid = '"+costid+"' and maintno = '"+maintno+"' and purpose = '01'";
	}
	
	
	initFeeTypeInfoGrid();
  	turnPage.queryModal(tsql,FeeTypeInfoGrid);
}
//值域追加
function superaddClick()
{
	if((fm.all('FeeValuesB').value == null)||(fm.all('FeeValuesB').value == ''))
	{
		alert("该项不得为空，请重新填写！");
		fm.all('FeeValuesB').value = '';
		fm.all('FeeValuesBName').value = '';
		return false;
	}
	if((fm.all('FeeValues').value == null)||(fm.all('FeeValues').value == ''))
	{
		fm.all('FeeValues').value = fm.all('FeeValuesB').value;
		fm.all('FeeValuesB').value = '';
		fm.all('FeeValuesBName').value = '';
	}
	else
	{
		fm.all('FeeValues').value = fm.all('FeeValues').value + ";" +fm.all('FeeValuesB').value;
		fm.all('FeeValuesB').value = '';
		fm.all('FeeValuesBName').value = '';
	}
}//add by fx

function clearClick()
{
	fm.all('FeeValuesB').value = '';
	fm.all('FeeValuesBName').value = '';
	fm.all('FeeValues').value = '';
}//add by fx


//选择费用信息时带出
function showfeeinfo()
{
	var tsel=FeeTypeInfoGrid.getSelNo();
	
	fm.FIBusInfoID.value=FeeTypeInfoGrid.getRowColData(tsel-1,1);
	//fm.FIFeeInfoIDName.value=FeeTypeInfoGrid.getRowColData(tsel-1,2);
	//fm.BusClass.value=FeeTypeInfoGrid.getRowColData(tsel-1,3);
	//fm.BusClassName.value=FeeTypeInfoGrid.getRowColData(tsel-1,4);
	//fm.FeeValues.value=FeeTypeInfoGrid.getRowColData(tsel-1,6);
	//fm.PurPose.value=FeeTypeInfoGrid.getRowColData(tsel-1,7);
	//fm.PurPoseName.value=FeeTypeInfoGrid.getRowColData(tsel-1,5);
	showCodeName();
}


//提交前的校验、计算  
function beforeSubmit()
{
	//添加操作 
	if(fm.all('FIFeeCode').value=="")
	{
		alert("费用编码不能为空！");
		return false;
	}
	if(fm.all('FIFeeCodeName').value=="")
	{
		alert("费用名称不能为空！");
		return false;
	}

	return true;
}

function beforeSubmit1()
{
	//添加操作 
	if(fm.all('FIBusInfoID').value=="")
	{
		alert("信息编码不能为空！");
		return false;
	}

	if(fm.all('PurPose').value=="")
	{
		alert("信息用途不能为空！");
		return false;
	}
	
	//查询费用信息名称
//	var sql = "select codealias from ldcode where codetype = 'fiabdate' and codename = '"+fm.FIFeeInfoIDName.value+"'";
	
//	fm.propertyname.value = easyExecSql(sql);
	
	return true;
		
}

//使得从该窗口弹出的窗口能够聚焦
function myonfocus(){
 if(showInfo!=null){
   try{
     showInfo.focus();  
   }
   catch(ex){
     showInfo=null;
   }
 }
}

//提交后操作,服务器数据返回后执行的操作
function afterSubmit( FlagStr, content ){
  showInfo.close();
  if (FlagStr == "Fail" ){             
    var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
  }
  else{ 
    var urlStr="../common/jsp/MessagePage.jsp?picture=S&content=" + content ;  
    showModalDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
    location.reload();
    showDiv(operateButton,"true"); 
    showDiv(inputButton,"false"); 
    //执行下一步操作
  }
}

function showDiv(cDiv,cShow){
  if (cShow=="true"){
    cDiv.style.display="";
  }
  else{
    cDiv.style.display="none";  
  }
}

//费用类型添加按钮
function feetypeaddClick()
{
	if(!beforeSubmit())
	{
		return false;	
	}
	fm.fmtransact.value='insert';
	
	//写入代码
	var checksql = "select count(*) from FIBnFeeTypeDef where BusinessID='"+BusTypeId+"' and costid = '"+fm.FIFeeCode.value+"'";
	
	var re=easyExecSql(checksql);
	
	if(re!="0")
	{
		alert("该费用类型已经存在！");
		return false;
	}
	
	feetypesubmitform();
}
//费用类型修改按钮
function feetypeupdateClick()
{
	if(!beforeSubmit())
	{
		return false;	
	}
	fm.fmtransact.value='update';
	
	//写入代码
	var checksql = "select count(*) from FIBnFeeTypeDef where BusinessID='"+BusTypeId+"' and costid = '"+fm.FIFeeCode.value+"'";
	
	var re=easyExecSql(checksql);
	
	if(re=="0")
	{
		alert("该费用类型不存在！");
		return false;
	}
	
	feetypesubmitform();
}

//费用类型删除按钮
function feetypedeleteClick()
{
	if(!beforeSubmit())
	{
		return false;	
	}
	fm.fmtransact.value='delete';
	
	//写入代码
	var checksql = "select count(*) from FIBnFeeTypeDef where BusinessID='"+BusTypeId+"' and costid = '"+fm.FIFeeCode.value+"'";
	
	var re=easyExecSql(checksql);
	
	if(re=="0")
	{
		alert("该费用类型不存在！");
		return false;
	}
	feetypesubmitform();
}

//费用信息添加按钮
function feeinfoaddClick()
{
	if(!beforeSubmit1())
	{
		return false;	
	}
	fm.fmtransact.value='insert';
	
	//写入代码
	var checksql = "select count(*) from FIBnFeeInfo where costid = '"+fm.costid.value+"' and propertycode = '"+fm.FIBusInfoID.value+"' and purpose = '01'";
	
	var re=easyExecSql(checksql);
	
	if(re!="0")
	{
		alert("该费用类型已经存在！");
		return false;
	}
	feeinfosubmitform();
}

//费用信息更新按钮
function feeinfoupdateClick()
{
	if(!beforeSubmit1())
	{
		return false;	
	}
	fm.fmtransact.value='update';
	

	feeinfosubmitform();
}

//费用信息删除按钮
function feeinfodeleteClick()
{
	if(!beforeSubmit1())
	{
		return false;	
	}
	fm.fmtransact.value='delete';
	
	
	//写入代码

	var checksql = "select count(*) from FIBnFeeInfo where costid = '"+fm.costid.value+"' and propertycode = '"+fm.FIBusInfoID.value+"' and purpose = '01'";

	var re=easyExecSql(checksql);
	
	if(re=="0")
	{
		alert("该费用类型不存在！");
		return false;
	}
	feeinfosubmitform();
}
//费用类型维护提交
function feetypesubmitform()
{
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	if (fm.fmtransact.value=="")
	{
		alert("操作控制数据丢失！");
	}
	fm.action="./FIBusFeeTypeDefSave.jsp";
	//lockButton(); 
	fm.submit(); //提交
}
//费用信息维护提交
function feeinfosubmitform()
{
	var i = 0;
	var showStr="正在保存数据，请您稍候并且不要修改屏幕上的值或链接其他页面";
	var urlStr="../common/jsp/MessagePage.jsp?picture=C&content=" + showStr ;
		showInfo=window.showModelessDialog(urlStr,window,"status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");

	if (fm.fmtransact.value=="")
	{
		alert("操作控制数据丢失！");
	}
	
	fm.action="./FIBusFeeInfoDefSave.jsp";
	//lockButton(); 
	fm.submit(); //提交
}