/******************************************************************
 * 概述：jeasy_video 接口程序
 * 描述：建议此接口只创建一个插件实例。
 * 版本：V1.0.0.0-alpha
 * 历史：
 *     1）初始化版本
******************************************************************/
var cmd_Easyscan     = "00";
var cmd_ReadIDCard   = "01";
var cmd_BankCard   = "02";
//----------------------------------------------------------------

var activex = null;
var isinited = false;

//===========================================================================

///概要：页面初始化时发生
///说明：请在页面加载时调用，可以使用加载进度的插件，增加用户体验。
function jsinit(serverUri, managecom, usercode) {

    try {
        activex = new ActiveXObject("EasyScan.ActiveServer");
        if (activex.RuntimeVersion < '2.0.0.0') {
            //转到安装包的FWLink页面，请自己编写代码。主要用于网页的更新。
        }
        activex.clear(); //清空所有参数

        activex.ServerUri = serverUri;
        activex.ServerId = "";    //指定当前是UAT测试环境。
        activex.SourceMode = "Form"; //指定为弹出模式
        activex.ManageCom = managecom;// "86";    //当前登录的机构编码
        activex.UserCode = usercode;//"admin";  //当前登录的用户编码
		//注意：isVideoCapture=true
        activex.InitParams = "json={'isUseBoxno':'false','isUseScanOrder':'true','boxno':'','isUseIssue':'true','isStrictCheck':'true','isVideoCapture':'true'}";     //加载插件的传入的参数

        activex.AutoLoad = true;
        
		activex.AutoUpdate = true;    //开启自动更新功能，上线时，需要开启
		activex.AutoSettings = true;  //开启中心配置功能，上线时，需要开启
        activex.AutoFeedback = true; //关闭自动反馈功能。

        activex.load(); //开始加载插件
        //activex.about();//****弹出关于对话框，此代码用于调试！；******

        ///jsEvent();//***挂载JS事件，暂时不用时间挂载。
        isinited = true;
		
	//	alert("插件开启完成！");
    }
    catch (ex) {
        activex = null;
        if ("Automation 服务器不能创建对象" == ex.message) {
        } else {
            alert("ActiveX启动失败，请重新安装" + ex.message);
        }
    }
    return activex;

}

///概要：关闭资源
function jsclose() {

    if (activex != null)
        activex.free();
    activex = null;
}

///是否isHttp
function isHttp(str) {
    //var str = "http";
    if (str.indexOf("http") >= 0) {
        return true;
    }
    return false;
}

//概要：初始化事件挂载
///说明：挂载事件，可以在此处处理插件反馈的事件。
function jsEvent() {
    //事件挂载部分

    var dispEvent = function () {

        //插件关闭事件
        function activex::onCloseEvent(args) {
            //内部事件反馈，此功能暂时未和Session进行挂载。
            alert('JS Event 使用示例: 事件改变参数的值->' + args);
        };

        //插件调用反馈事件
        function activex::onCallEvent(cmd, args) {
            //内部事件反馈，此功能暂时未和Session进行挂载。
            alert('JS Event 使用示例: 事件改变参数的值->' + args);
        };

        //插件上下文内容改变的事件
        function activex::onContextEvent(token, args) {
            alert('onContext args' + token + args);
        };

        //插件错误事件事件
        function activex::onErrorEvent(source, args) {
            alert(args);

        };

        //----------
    };//end-function

}

//===========================================================================

///概要：方法调用接口
///说明：处理读取身份证、银行卡、条形码接口。
function jscall(cmd) {
    if (activex == null) {
        return ActiveXerror();
    }
    //打开客户端
    if (cmd == cmd_Easyscan) {
		try{
			alert(cmd_Easyscan);
			activex.call_6(cmd_Easyscan, "", "","","","","");
		}catch(ex)
		{
			alert("111="+ex.message);
		}
    }

    //读取身份证
    if (cmd == cmd_ReadIDCard) {
		try{
			var str = activex.call_6(cmd_ReadIDCard, "", "","","","","");
			var obj = eval('(' + str + ')');
//			return json.cardId;//返回身份证号
//			return JSON.parse(str);
			return obj.cardId;
		}catch(ex)
		{
			alert(ex.message);
		}
    }
	
	
	if (cmd == cmd_BankCard) {
		try{
			var str = activex.call_6(cmd_BankCard, "", "","","","","");
//			var json=JSON.parse(str);//读取的银行卡信息
//			return json.cardNo;//返回银行卡号
//			return JSON.parse(str);
			var obj = eval('(' + str + ')');
			return obj.cardNo;
		}catch(ex)
		{
			alert(ex.message);
		}
    }
  

  //  alert("方法参数不正确！");
}

///概要：方法调用接口
///描述：打开集成平台和打开离线上传平台。
function jsopen(cmd, systemcode, modulecode, title, imagejson, indexjson, markjson) {

    if (activex == null) {
        alert("ActiveX初始化失败，暂不支持此功能");
        return;
    }
    //打开集成平台
    if (cmd == cmd_OpenVideo) {
        var imagepar = JSON.stringify(imagejson);//将JSON对象编组为JSON字符串。
        var indexpar = JSON.stringify(indexjson);//将JSON对象编组为JSON字符串。
        var markpar = JSON.stringify(markjson);
        activex.call_6(cmd_OpenVideo, systemcode, modulecode, title, imagepar, indexpar,markpar);
        return;
    }
    //打开离线上传
    if (cmd == cmd_OpenZipFile) {
        var imagepar = JSON.stringify(imagejson);//将JSON对象编组为JSON字符串。
        var indexpar = JSON.stringify(indexjson);//将JSON对象编组为JSON字符串。
        activex.call_5(cmd_OpenZipFile, systemcode, modulecode, title, imagepar, indexpar);
        return;
    }
    alert("cmd:方法参数不正确！");
}

function ActiveXerror() {
    var str = '{"isOk":false,"errors":"ActiveX初始化失败，暂不支持此功能"}';
    return JSON.parse(str);
}

//===========================================================================
