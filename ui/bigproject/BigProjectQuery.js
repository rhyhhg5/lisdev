var turnPage = new turnPageClass();
var showInfo;

/* 查询 */
function query(){
	initBPDetailGrid();
	if(fm.ManageCom.value == null || fm.ManageCom.value == "" || fm.ManageCom.value == "null"){
		alert("管理机构必须填写！");
		return false;
	}
	var tManageCom = fm.ManageCom.value;
	var bpManageCom = "";
	if(tManageCom == "86"){
		bpManageCom = "8600";
	}
	if(tManageCom.length == 4){
		bpManageCom = tManageCom;
	}
	if(tManageCom.length > 4){
		bpManageCom = tManageCom.substr(0,4);
	}
	var strSQL = "select bigprojectno,bigprojectname,createdate,bpmanagecom "
		 	   + "from lcbigprojectinfo "
		 	   + "where bpmanagecom = '"+bpManageCom+"' "
		       + getWherePart('CreateDate', 'StartDate','>=')
		       + getWherePart('CreateDate', 'EndDate','<=')
		       + getWherePart('BigProjectNo', 'ProjectNo')
		       + getWherePart('BigProjectName', 'ProjectName','like');
		 	   
	turnPage.queryModal(strSQL,BPDetailGrid);
	if(!turnPage.strQueryResult){
		alert("没有查询到相关数据！");
		return false;
	}
}
/* 详细信息 */
function detail(){
	var checkFlag = 0;
	for(var i=0; i<BPDetailGrid.mulLineCount; i++){
	    if (BPDetailGrid.getSelNo(i)){
	    	checkFlag = BPDetailGrid.getSelNo();
	    	break;
	    }
	}
	if(checkFlag){
	  	var	tProjectNo = BPDetailGrid.getRowColData(checkFlag - 1, 1);
	  	if(tProjectNo == null || tProjectNo == "" || tProjectNo =="null"){
	  		alert("请查询项目！");
	  		return false;
	  	}
	  	showInfo = window.open("./BigProjectDetailMain.jsp?BigProjectNo="+tProjectNo);
	}else{
	  	alert("请选择项目！");
	  	return false;
	}
}