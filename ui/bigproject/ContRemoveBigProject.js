var turnPage = new turnPageClass();
var showInfo;

/* 查询 */
function query(){
	initPolListGrid();
	if(fm.ManageCom.value==null || fm.ManageCom.value=="" || fm.ManageCom.value=="null"){
		alert("管理机构不能为空！");
		return;
	}
	//从某个大项目解除归属保单，只查询出归属于本大项目的保单信息,具体执行为 tSQL的第//-_-行。
	var tSQL = "select lgc.grpcontno,lgc.managecom,lgc.signdate,codename('markettype',lgc.markettype),lgc.grpname,lgc.prtno,lcbpc.bigprojectyear "
		 	 + "from lcgrpcont lgc,lcbigprojectcont lcbpc "
		 	 + "where lgc.ManageCom like '"+fm.ManageCom.value+"%' "
		 	 + getWherePart('lgc.SignDate','StartDate','>=')
		 	 + getWherePart('lgc.SignDate','EndDate','<=') 
		 	 + getWherePart('lgc.GrpContNo','ContNo') 
		 	 + getWherePart('lcbpc.BigProjectYear','ProjectYear') 
		 	 + getWherePart('lcbpc.BigProjectNo','BPNumber')//-_-
		 	 + "and lgc.markettype in ('1','9') "
		 	 + "and lgc.prtno = lcbpc.prtno";
	turnPage.queryModal(tSQL,PolListGrid);
}

/* 移除项目 */
function removeProject(){
	var count = 0;
	for (var i = 0; i < PolListGrid.mulLineCount; i++) {
		if (PolListGrid.getChkNo(i) == true) {
			count++;
		}
	}
	if (count == 0) {
		alert("\u8bf7\u9009\u62e9\u9700\u8981\u5904\u7406\u7684\u4fdd\u5355\uff01");
		return;
	}
	fm.fmtransact.value = "DELETE||MAIN";
	fm.SeriaNo.value = getSeriaNo();
	fm.action = "ContRemoveBigProjectSave.jsp";
	var showStr = "\u6b63\u5728\u5904\u7406\u6570\u636e\uff0c\u8bf7\u60a8\u7a0d\u5019\u5e76\u4e14\u4e0d\u8981\u4fee\u6539\u5c4f\u5e55\u4e0a\u7684\u503c\u6216\u94fe\u63a5\u5176\u4ed6\u9875\u9762";
	var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + showStr;
	showInfo = window.showModelessDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:250px");
	fm.submit();
}

function afterSubmit(FlagStr, content, cOperate) {
	initBigProjectInfo();
	initPolListGrid();
	showInfo.close();
	if (FlagStr == "Fail"){
		var urlStr = "../common/jsp/MessagePage.jsp?picture=C&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");
	} else {
		var urlStr = "../common/jsp/MessagePage.jsp?picture=S&content=" + content;
		showModalDialog(urlStr, window, "status:no;help:0;close:0;dialogWidth:550px;dialogHeight:350px");   
	}
}

function getSeriaNo(){
	var seriaNo="";
	var newSeriaNo="";
	var tSql = "select max(serialno) from lcbigprojectconttrack";
	var arr = easyExecSql(tSql);
	if(arr){
		seriaNo = arr[0][0];
		if(seriaNo!=null && seriaNo!=""){
			var temp = Number(seriaNo) + 1 + "";
			var len = seriaNo.length - temp.length;
			for(var i=0; i<len; i++){
				newSeriaNo = newSeriaNo + "0";
			}
			newSeriaNo = newSeriaNo + temp;
		}else{
			newSeriaNo = "00000001";
		}
	}
	return newSeriaNo;
}

/* 返回 */
function cancle(){
	top.close();
}

function initBigProjectInfo(){
	fm.BPNumber.value = tBigProjectNo;
	var tSQL = "select bigprojectname,createdate,bpmanagecom from lcbigprojectinfo "
			 + "where bigprojectno = '"+tBigProjectNo+"'";
	var arr = easyExecSql(tSQL);
	if(arr){
		fm.BPName.value = arr[0][0];
		fm.CPDate.value = arr[0][1];
		fm.BPManageCom.value = arr[0][2];
	}
	var cSQL = "select nvl((select count(1) from lcbigprojectcont where bigprojectno='"+tBigProjectNo+"'),0) from dual";
	var count = easyExecSql(cSQL);
	fm.AttachedConts.value = count;
}