<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script>
	var ComCode = "<%=tGI.ManageCom%>";
	var tOperator = "<%=tGI.Operator%>";
	var OperatorFlag = "<%=request.getParameter("OperatorFlag")%>";
	var tBigProjectNo = "<%=request.getParameter("BigProjectNo")%>";
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<SCRIPT src="./BigProjectNew.js"></SCRIPT>
	<%@include file="./BigProjectNewInit.jsp"%>
</head>
<body onload="initForm();initElementtype();">
	<form action="" method="post" name="fm" target="fraSubmit">
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divQuery);">
				</td>
				<td class=titleImg>大项目要素信息</td>
			</tr>
		</table>
		<div id="divQuery" style="display:''">
			<table class=common>
				<tr class=common>
					<td class=title>项目名称</td>
					<td class=input>
						<input class=common name=ProjectName elementtype=nacessary>
						<%-- 
						--暂时不需要这个功能，注掉。
						<input class=cssButton type=button value="查询项目" name=QueryBP onclick="queryBPInfo();">
						--%>
					</td>
					<td class=title>项目编码</td>
					<td class=input>
						<input class=readonly name=ProjectNo readonly=true>
					</td>
					<td class=title>管理机构</td>
					<td class=input>
						<input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" 
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
						><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</td>
				</tr>
				<tr class=common>
					<td class=title>项目预估赔付率</td>
					<td class=input>
						<input class=common name=ProjectEstimateRate >
					</td>
				</tr>
			</table>
		</div>
		<input type=hidden name=ProjectSaveState>
		<input type=hidden name=OperatorFlag>
		<input type=hidden name=BPManageCom>
		<input type=button class=cssButton value=" 保  存 " onclick="save();">
		<table>
			<tr class=common>
 				<TD colspan=6>
         	     	<font size = 2 color=red>
         	      注： 项目预估赔付率精确到小数点后四位，例：录入为0.8，则预估赔付率为80%
         	    	</font>
        		</TD>				
       		</tr>
		</table>
	</form>
	<span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>