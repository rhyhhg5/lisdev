<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = tGlobalInput.ManageCom;
%>

<script language="JavaScript">
	function initInpBox(){
		try{
			fm.ManageCom.value = <%=strManageCom%>;
			if(fm.ManageCom.value != null){
				var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
				if(arrResult != null){
					fm.ManageComName.value = arrResult[0][0];
				}
			}
			fm.ProjectYear.value = tBigProjectYear;
			query();
			showAllCodeName();
		}catch(ex){
			alert("在AttachContQueryInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initForm(){
		try{
			initPolListGrid();
			initInpBox();
			initProjectInfo();
			initElementtype();
		}catch(re){
			alert("在AttachContQueryInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initPolListGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array();
		    iArray[0][0]="序号";   
		    iArray[0][1]="30px";    
		    iArray[0][2]=30;     
		    iArray[0][3]=0;        
		      
		    iArray[1]=new Array();
		    iArray[1][0]="合同号";      
		    iArray[1][1]="80px";        
		    iArray[1][2]=200;           
		    iArray[1][3]=0;             
		    
		    iArray[2]=new Array();
		    iArray[2][0]="管理机构";      
		    iArray[2][1]="80px";        
		    iArray[2][2]=200;           
		    iArray[2][3]=0;            
		        
		    iArray[3]=new Array();
		    iArray[3][0]="承保日期";      
		    iArray[3][1]="80px";        
		    iArray[3][2]=200;           
		    iArray[3][3]=0;            
		    
		    iArray[4]=new Array();
		    iArray[4][0]="保单类型";      
		    iArray[4][1]="80px";        
		    iArray[4][2]=200;           
		    iArray[4][3]=0;             
		      
		    iArray[5]=new Array();
		    iArray[5][0]="投保单位";      
		    iArray[5][1]="80px";        
		    iArray[5][2]=200;           
		    iArray[5][3]=0; 
		    
		    iArray[6]=new Array();
		    iArray[6][0]="项目年度";      
		    iArray[6][1]="80px";        
		    iArray[6][2]=200;           
		    iArray[6][3]=0;
		    
		    iArray[7]=new Array();
		    iArray[7][0]="印刷号";      
		    iArray[7][1]="80px";        
		    iArray[7][2]=200;           
		    iArray[7][3]=0;
		 
		    PolListGrid = new MulLineEnter("fm", "PolListGrid"); 
			  
		    PolListGrid.mulLineCount = 10;
		    PolListGrid.displayTitle = 1;
		    PolListGrid.locked = 1;
		    PolListGrid.canSel = 0;	
		    PolListGrid.canChk = 1;
		    PolListGrid.hiddenSubtraction = 1;
		    PolListGrid.hiddenPlus = 1;
		    
		    PolListGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>