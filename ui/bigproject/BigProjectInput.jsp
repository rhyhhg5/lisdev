<%@include file="../common/jsp/UsrCheck.jsp"%>
<%@page contentType="text/html;charset=GBK"%>
<html>
<%
	GlobalInput tGI = (GlobalInput) session.getValue("GI");
%>
<script>
	var ComCode = "<%=tGI.ManageCom%>";
	var tOperator = "<%=tGI.Operator%>";
</script>
<head>
	<SCRIPT src="../common/javascript/Common.js"></SCRIPT>
	<SCRIPT src="../common/javascript/VerifyInput.js"></SCRIPT>
	<SCRIPT src="../common/cvar/CCodeOperate.js"></SCRIPT>
	<SCRIPT src="../common/javascript/MulLine.js"></SCRIPT>
	<SCRIPT src="../common/javascript/EasyQuery.js"></SCRIPT>
	<SCRIPT src="../common/Calendar/Calendar.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryVer3.js"></SCRIPT>
	<SCRIPT src="../common/easyQueryVer3/EasyQueryCache.js"></SCRIPT>
	<LINK href="../common/css/Project.css" rel=stylesheet type=text/css>
	<LINK href="../common/css/mulLine.css" rel=stylesheet type=text/css>
	<script src="./BigProjectInput.js"></script>
	<%@include file="./BigProjectInit.jsp"%>
</head>
<body onload="initForm();">
	<form action="" method=post name=fm target="fraSubmit">
		<table>
			<tr>
				<td>
					<IMG src="../common/images/butExpand.gif" style="cursor:hand;"	OnClick="showPage(this,divQuery);">
				</td>
				<td class=titleImg>查询条件</td>
			</tr>
		</table>
		<div id="divQuery" style="display:''">
			<table class=common>
				<tr class=common>
					<td class=title>管理机构</td>
					<td class=input>
						<input class=codeNo name=ManageCom verify="管理机构|code:comcode&notnull" 
							ondblclick="return showCodeList('comcode',[this,ManageComName],[0,1],null,null,null,1);" 
							onkeyup="return showCodeListKey('comcode',[this,ManageComName],[0,1],null,null,null,1);"
						><input class=codename name=ManageComName readonly=true elementtype=nacessary>
					</td>
					<td class=title>建项日期起期</td>
					<td class=input>
						<input class=coolDatePicker name=StartDate>
					</td>
					<td class=title>建项日期止期</td>
					<td class=input>
						<input class=coolDatePicker name=EndDate>
					</td>
				</tr>
				<tr class=common>
					<td class=title>项目编号</td>
					<td class=input>
						<input class=common name=ProjectNo>
					</td>
					<td class=title>项目名称</td>
					<td class=input>
						<input class=common name=ProjectName>
					</td>
					<td class=title></td>
					<td class=input></td>
				</tr>
			</table>
		</div>
		<input type=button class=cssButton value=" 查 询 " onclick="query();" />
		<hr/>
		<table>
			<tr>
				<td class=common>
					<IMG src= "../common/images/butExpand.gif" style= "cursor:hand;" onclick="showPage(this,divBigPro);">
				</td>
				<td class=titleImg>项目信息</td>
			</tr>
		</table>
		<div id="divBigPro" style="display:''">
			<table class=common>
				<tr class=common>
					<td text-align: left colSpan=1>
						<span id="spanBIGPROGrid"></span>
					</td>
				</tr>
			</table>
			<table align=center>
				<INPUT VALUE="首  页" class="cssButton" TYPE=button onclick="getFirstPage();">
				<INPUT VALUE="上一页" class="cssButton" TYPE=button onclick="getPreviousPage();">
				<INPUT VALUE="下一页" class="cssButton" TYPE=button onclick="getNextPage();">
				<INPUT VALUE="尾  页" class="cssButton" TYPE=button onclick="getLastPage();">
			</table>
		</div>
		<hr/>
		<input type=hidden name="OperatorFlag">
		<input type=button class=cssButton value="新建项目" onclick="addProject();">
		<input type=button class=cssButton value="修改项目" onclick="modifyProject();">
	</form>
	<span id="spanCode" style="display:none; position:absolute; slategray"></span>
</body>
</html>