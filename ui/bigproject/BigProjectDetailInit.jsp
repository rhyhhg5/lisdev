<%@page import="com.sinosoft.utility.*"%>
<%
	GlobalInput tGlobalInput = (GlobalInput)session.getValue("GI");
	String strManageCom = tGlobalInput.ManageCom;
%>

<script language="JavaScript">
	function initInpBox(){
		try{
			fm.ManageCom.value = <%=strManageCom%>;
			if(fm.ManageCom.value != null){
				var arrResult = easyExecSql("select name from ldcom where comcode = '"+fm.all('ManageCom').value+"'");
				if(arrResult != null){
					fm.ManageComName.value = arrResult[0][0];
				}
			}
		}catch(ex){
			alert("在BPDetailInit.jsp-->InitInpBox函数中发生异常:初始化界面错误!");
		}
	}
	function initForm(){
		try{
			initInpBox();
			initBPYearInfoGrid();
			initProjectInfo();
			initElementtype();
		}catch(re){
			alert("在BPDetailInit.jsp-->InitForm函数中发生异常:初始化界面错误!");
		}
	}
	function initBPYearInfoGrid(){
		var iArray = new Array();
		try{
			iArray[0]=new Array();
		    iArray[0][0]="序号";   
		    iArray[0][1]="30px";    
		    iArray[0][2]=30;     
		    iArray[0][3]=0;        
		      
		    iArray[1]=new Array();
		    iArray[1][0]="项目年度";      
		    iArray[1][1]="80px";        
		    iArray[1][2]=200;           
		    iArray[1][3]=0;             
		    
		    BPYearInfoGrid = new MulLineEnter("fm", "BPYearInfoGrid"); 
			  
		    BPYearInfoGrid.mulLineCount = 0;
		    BPYearInfoGrid.displayTitle = 1;
		    BPYearInfoGrid.locked = 1;
		    BPYearInfoGrid.canSel = 1;	
		    BPYearInfoGrid.canChk = 0;
		    BPYearInfoGrid.hiddenSubtraction = 1;
		    BPYearInfoGrid.hiddenPlus = 1;
		    
		    BPYearInfoGrid.loadMulLine(iArray);
		}catch(ex){
			alert(ex);
		}
	}
</script>