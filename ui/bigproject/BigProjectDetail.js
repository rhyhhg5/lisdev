var turnPage = new turnPageClass();
var showInfo;

/* 已归属保单 */
function attachedPolicies(){
	var checkFlag = 0;
	for(i=0; i<BPYearInfoGrid.mulLineCount; i++){
		if(BPYearInfoGrid.getSelNo(i)){
			checkFlag = BPYearInfoGrid.getSelNo();
			break;
		}
	}
	if(checkFlag){
		var	tBigProjectYear = BPYearInfoGrid.getRowColData(checkFlag - 1, 1);
		if(tBigProjectYear == null || tBigProjectYear == "" || tBigProjectYear =="null"){
			alert("获取年度失败！");
			return false;
		}
		showInfo = window.open("./AttachContQueryMain.jsp?BigProjectNo="+tBigProjectNo+"&BigProjectYear="+tBigProjectYear);
	}else{
		alert("请选择年度！");
		return false;
	}
}

/* 初始化大项目信息 */
function initProjectInfo(){
	if(tBigProjectNo == null || tBigProjectNo == ""){
		alert("获取大项目编码失败！");
		return false;
	}
	var tSQL = "select bigprojectname,bigprojectno,bpmanagecom "
		     + "from lcbigprojectinfo "
		     + "where bigprojectno = '"+tBigProjectNo+"' ";
	var arr = easyExecSql(tSQL);
	if(arr){
		fm.BigProjectName.value = arr[0][0];
		fm.BigProjectNo.value = arr[0][1];
		fm.ManageCom.value = arr[0][2];
	}
	var tSTRSQL = "select bigprojectyear from lcbigprojectyear where bigprojectno='"+tBigProjectNo+"'";
	turnPage.queryModal(tSTRSQL,BPYearInfoGrid);
}

/* 退出 */
function cancle(){
	top.close();
}