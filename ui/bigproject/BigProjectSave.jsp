<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>


<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String tOperator = tG.Operator;
	String tComCode = tG.ComCode;
	
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	
	//得到前台页面的数据
	String tProjectName = request.getParameter("ProjectName");
	String tManageCom = request.getParameter("ManageCom");
	String tProjectNo = request.getParameter("ProjectNo");
	String tBPManageCom = request.getParameter("BPManageCom");
	String tProjectSaveState = request.getParameter("ProjectSaveState");
	String tOperatorFlag = request.getParameter("OperatorFlag");
	String tProjectEstimateRate = request.getParameter("ProjectEstimateRate");	
	
	String tCurrentDate = PubFun.getCurrentDate();
	String tCurrentTime = PubFun.getCurrentTime();
	
	//测试输出
	System.out.println("tProjectName:"+tProjectName);
	System.out.println("tManageCom:"+tManageCom);
	System.out.println("tProjectNo:"+tProjectNo);
	System.out.println("tOperator:"+tOperator);
	System.out.println("tBPManageCom:"+tBPManageCom);
	System.out.println("tCurrentDate:"+tCurrentDate);
	System.out.println("tCurrentTime:"+tCurrentTime);
	System.out.println("tProjectSaveState:"+tProjectSaveState);
	System.out.println("tOperatorFlag:"+tOperatorFlag);
	System.out.println("tProjectEstimateRate:"+tProjectEstimateRate);
	
	//在修改大项目时，"2".equals(tOperatorFlag)时，获得要修改大项目的信息
	LCBigProjectInfoDB tLCBigProjectInfoDB = new LCBigProjectInfoDB();
	String tSetSQL = "select * from lcbigprojectinfo where bigprojectno='"+tProjectNo+"'";
	LCBigProjectInfoSet tLCBigProjectInfoSet = new LCBigProjectInfoSet();
	
	LCBigProjectInfoSchema tLCBigProjectInfoSchema = new LCBigProjectInfoSchema();
	
	if("1".equals(tOperatorFlag)){
		tLCBigProjectInfoSchema.setBigProjectNo(tProjectNo);
		tLCBigProjectInfoSchema.setBigProjectName(tProjectName);
		tLCBigProjectInfoSchema.setOperator(tOperator);
		tLCBigProjectInfoSchema.setManageCom(tComCode);
		tLCBigProjectInfoSchema.setModifyDate(tCurrentDate);
		tLCBigProjectInfoSchema.setModifyTime(tCurrentTime);
		tLCBigProjectInfoSchema.setBPManageCom(tBPManageCom);
		tLCBigProjectInfoSchema.setInputOperator(tOperator);
		tLCBigProjectInfoSchema.setCreateDate(tCurrentDate);
		tLCBigProjectInfoSchema.setMakeDate(tCurrentDate);
		tLCBigProjectInfoSchema.setMakeTime(tCurrentTime);
		tLCBigProjectInfoSchema.setProjectEstimateRate(tProjectEstimateRate);
	}
	if("2".equals(tOperatorFlag)){
		tLCBigProjectInfoSet = tLCBigProjectInfoDB.executeQuery(tSetSQL);
		if(tLCBigProjectInfoSet.size() == 1){
			tLCBigProjectInfoSchema = tLCBigProjectInfoSet.get(1);
			tLCBigProjectInfoSchema.setBigProjectName(tProjectName);
			tLCBigProjectInfoSchema.setOperator(tOperator);
			tLCBigProjectInfoSchema.setManageCom(tComCode);
			tLCBigProjectInfoSchema.setModifyDate(tCurrentDate);
			tLCBigProjectInfoSchema.setModifyTime(tCurrentTime);
			tLCBigProjectInfoSchema.setProjectEstimateRate(tProjectEstimateRate);
		}
	}
	
	try{
		VData tVData = new VData();
		tVData.add(tLCBigProjectInfoSchema);
		BigProjectUI tBigProjectUI = new BigProjectUI();
		boolean b = tBigProjectUI.submitData(tVData, tOperatorFlag);
		if(b==true){
			FlagStr = "Success";
			Content = "数据保存成功！";
		}else{
			Content = "数据保存失败";
		}
	}catch(Exception e){
		Content = "数据提交失败";
		e.printStackTrace();
	}
	
	
%>

<html>
	<script language="javascript">
    	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  	</script>
</html>
