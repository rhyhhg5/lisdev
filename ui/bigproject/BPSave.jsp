<%@page contentType="text/html;charset=GBK" %>
<!--用户校验类-->
<%@include file="../common/jsp/UsrCheck.jsp" %>

<%@page import="com.sinosoft.lis.tb.*"%>
<%@page import="com.sinosoft.lis.pubfun.*"%>
<%@page import="com.sinosoft.utility.*"%>
<%@page import="com.sinosoft.lis.schema.*"%>
<%@page import="com.sinosoft.lis.vschema.*"%>
<%@page import="com.sinosoft.lis.db.*"%>


<%
	GlobalInput tG = new GlobalInput();
	tG = (GlobalInput) session.getValue("GI");
	String tOperator = tG.Operator;
	String tComCode = tG.ComCode;
	
	//输出参数
	CErrors tError = null;
	String FlagStr = "Fail";
	String Content = "";
	
	//得到前台页面的数据
	String tBigProjectName = request.getParameter("BigProjectName");
	String tBigProjectNo = request.getParameter("BigProjectNo");
	String tBigProjectYear = request.getParameter("BigProjectYear");
	String tBPManageCom = request.getParameter("BPManageCom");
	String tPrtNo = request.getParameter("PrtNo");
	String tSerialNo = request.getParameter("SerialNo");
	String fmtransact = request.getParameter("fmtransact");
	
	String tCurrentDate = PubFun.getCurrentDate();
	String tCurrentTime = PubFun.getCurrentTime();
	
	//测试输出
	System.out.println("tBigProjectName:"+tBigProjectName);
	System.out.println("tBigProjectNo:"+tBigProjectNo);
	System.out.println("tBigProjectYear:"+tBigProjectYear);
	System.out.println("tOperator:"+tOperator);
	System.out.println("tBPManageCom:"+tBPManageCom);
	System.out.println("tComCode:"+tComCode);
	System.out.println("tCurrentDate:"+tCurrentDate);
	System.out.println("tCurrentTime:"+tCurrentTime);
	System.out.println("tPrtNo:"+tPrtNo);
	System.out.println("tSerialNo:"+tSerialNo);
	System.out.println("fmtransact:"+fmtransact);
	
	/* 封装LCBigProjectInfoSchema数据，只有确定是新建的大项目或者要修改已有的大项目（系统中无存储）才会向后提交 */
	LCBigProjectInfoSchema tLCBigProjectInfoSchema = new LCBigProjectInfoSchema();
	String tBPInfoSQL = "select 1 from lcbigprojectinfo where bigprojectno='"+tBigProjectNo+"' and bigprojectname='"+tBigProjectName+"'";
	String BPInfoFlag = "";
	BPInfoFlag = new ExeSQL().getOneValue(tBPInfoSQL);
	if("".equals(BPInfoFlag)){
		tLCBigProjectInfoSchema.setBigProjectNo(tBigProjectNo);
		tLCBigProjectInfoSchema.setBigProjectName(tBigProjectName);
		tLCBigProjectInfoSchema.setCreateDate(tCurrentDate);
		tLCBigProjectInfoSchema.setInputOperator(tOperator);
		tLCBigProjectInfoSchema.setBPManageCom(tBPManageCom);
		tLCBigProjectInfoSchema.setMakeDate(tCurrentDate);
		tLCBigProjectInfoSchema.setMakeTime(tCurrentTime);
		tLCBigProjectInfoSchema.setOperator(tOperator);
		tLCBigProjectInfoSchema.setManageCom(tComCode);
		tLCBigProjectInfoSchema.setModifyDate(tCurrentDate);
		tLCBigProjectInfoSchema.setModifyTime(tCurrentTime);
	}
	/* 封装LCBigProjectContSchema数据,提交 */
	LCBigProjectContSchema tLCBigProjectContSchema = new LCBigProjectContSchema();
	tLCBigProjectContSchema.setBigProjectNo(tBigProjectNo);
	tLCBigProjectContSchema.setBigProjectYear(tBigProjectYear);
	tLCBigProjectContSchema.setPrtNo(tPrtNo);
	tLCBigProjectContSchema.setAttachedDate(tCurrentDate);
	tLCBigProjectContSchema.setAttachedTime(tCurrentTime);
	tLCBigProjectContSchema.setMakeDate(tCurrentDate);
	tLCBigProjectContSchema.setMakeTime(tCurrentTime);
	tLCBigProjectContSchema.setOperator(tOperator);
	tLCBigProjectContSchema.setManageCom(tComCode);
	tLCBigProjectContSchema.setModifyDate(tCurrentDate);
	tLCBigProjectContSchema.setModifyTime(tCurrentTime);
	/* 封装LCBigProjectContTrackSchema数据,提交 */
	LCBigProjectContTrackSchema tLCBigProjectContTrackSchema = new LCBigProjectContTrackSchema();
	tLCBigProjectContTrackSchema.setSerialNo(tSerialNo);
	tLCBigProjectContTrackSchema.setBigProjectNo(tBigProjectNo);
	tLCBigProjectContTrackSchema.setBigProjectYear(tBigProjectYear);
	tLCBigProjectContTrackSchema.setPrtNo(tPrtNo);
	tLCBigProjectContTrackSchema.setAttachedDate(tCurrentDate);
	tLCBigProjectContTrackSchema.setAttachedTime(tCurrentTime);
	if("INSERT".equals(fmtransact)){
		tLCBigProjectContTrackSchema.setAttachedState("00");
	}
	if("DELETE".equals(fmtransact)){
		tLCBigProjectContTrackSchema.setAttachedState("01");
	}
	tLCBigProjectContTrackSchema.setMakeDate(tCurrentDate);
	tLCBigProjectContTrackSchema.setMakeTime(tCurrentTime);
	tLCBigProjectContTrackSchema.setOperator(tOperator);
	tLCBigProjectContTrackSchema.setManageCom(tComCode);
	tLCBigProjectContTrackSchema.setModifyDate(tCurrentDate);
	tLCBigProjectContTrackSchema.setModifyTime(tCurrentTime);
	/* 封装LCBigProjectYearSchema数据,只有系统中无存储才会向后提交 */
	LCBigProjectYearSchema tLCBigProjectYearSchema = new LCBigProjectYearSchema();
	String tBPYearSQL = "select 1 from lcbigprojectyear where bigprojectno='"+tBigProjectNo+"' and bigprojectyear='"+tBigProjectYear+"'";
	String BPYear = "";
	BPYear = new ExeSQL().getOneValue(tBPYearSQL);
	if("".equals(BPYear)){
		tLCBigProjectYearSchema.setBigProjectNo(tBigProjectNo);
		tLCBigProjectYearSchema.setBigProjectYear(tBigProjectYear);
		tLCBigProjectYearSchema.setMakeDate(tCurrentDate);
		tLCBigProjectYearSchema.setMakeTime(tCurrentTime);
		tLCBigProjectYearSchema.setOperator(tOperator);
		tLCBigProjectYearSchema.setManageCom(tComCode);
		tLCBigProjectYearSchema.setModifyDate(tCurrentDate);
		tLCBigProjectYearSchema.setModifyTime(tCurrentTime);
	}
	
	try{
		VData tVData = new VData();
		tVData.add(tLCBigProjectInfoSchema);
		tVData.add(tLCBigProjectContSchema);
		tVData.add(tLCBigProjectContTrackSchema);
		tVData.add(tLCBigProjectYearSchema);
		BPUI tBPUI = new BPUI();
		boolean b = tBPUI.submitData(tVData, fmtransact);
		if(b==true){
			FlagStr = "Success";
			Content = "数据保存成功！";
		}else{
			Content = "数据保存失败";
		}
	}catch(Exception e){
		Content = "数据保存失败，原因是："+e.toString();
	}
%>

<html>
	<script language="javascript">
    	parent.fraInterface.afterSubmit("<%=FlagStr%>","<%=Content%>");
  	</script>
</html>
