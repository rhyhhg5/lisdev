package com.cbsws.picchobj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class BQPolicyInfo extends  PiccBaseXmlSch{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1210790423707897845L;
	
	private String RiskName;
	private String PolicyNo;
	private String CValiDate;
	private String PolicyState;
	public String getRiskName() {
		return RiskName;
	}
	public void setRiskName(String riskName) {
		RiskName = riskName;
	}
	public String getPolicyNo() {
		return PolicyNo;
	}
	public void setPolicyNo(String policyNo) {
		PolicyNo = policyNo;
	}
	public String getCValiDate() {
		return CValiDate;
	}
	public void setCValiDate(String cValiDate) {
		CValiDate = cValiDate;
	}
	public String getPolicyState() {
		return PolicyState;
	}
	public void setPolicyState(String policyState) {
		PolicyState = policyState;
	}
	

}
