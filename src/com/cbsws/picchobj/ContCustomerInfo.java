package com.cbsws.picchobj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class ContCustomerInfo extends  PiccBaseXmlSch{
	private static final long serialVersionUID = -4786465933806025992L;
	/** 姓名 **/
	private String CustomerName ;
	/** 证件类型 **/
	private String CustomerIDType;
	/** 证件号 **/
	private String CustomerIDNo;
	/** 保单状态 **/
	private String ContState;
	
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCustomerIDType() {
		return CustomerIDType;
	}
	public void setCustomerIDType(String customerIDType) {
		CustomerIDType = customerIDType;
	}
	public String getCustomerIDNo() {
		return CustomerIDNo;
	}
	public void setCustomerIDNo(String customerIDNo) {
		CustomerIDNo = customerIDNo;
	}
	public String getContState() {
		return ContState;
	}
	public void setContState(String contState) {
		ContState = contState;
	}
	
}
