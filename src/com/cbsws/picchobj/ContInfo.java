package com.cbsws.picchobj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class ContInfo extends  PiccBaseXmlSch{

	private static final long serialVersionUID = -4786465933806025992L;
	/** 保单号 **/
	private String ContNo;
	/** 投保人名称 */
    private String AppntName;
    /** 证件类型 */
	private String IDType;
	/** 证件号码 */
	private String IDNo;
	/** 签收日期 */
    private String GetpolDate;
	public String getContNo() {
		return ContNo;
	}
	public void setContNo(String contNo) {
		ContNo = contNo;
	}
	public String getAppntName() {
		return AppntName;
	}
	public void setAppntName(String appntName) {
		AppntName = appntName;
	}
	public String getIDType() {
		return IDType;
	}
	public void setIDType(String iDType) {
		IDType = iDType;
	}
	public String getIDNo() {
		return IDNo;
	}
	public void setIDNo(String iDNo) {
		IDNo = iDNo;
	}
	public String getGetpolDate() {
		return GetpolDate;
	}
	public void setGetpolDate(String getpolDate) {
		GetpolDate = getpolDate;
	}


}
