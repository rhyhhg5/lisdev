package com.cbsws.picchobj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class BQCustomerInfo extends  PiccBaseXmlSch{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4786465933806025992L;
	
	private String CustomerName ;
	private String CustomerIDType;
	private String CustomerIDNo;
	public String getCustomerName() {
		return CustomerName;
	}
	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}
	public String getCustomerIDType() {
		return CustomerIDType;
	}
	public void setCustomerIDType(String customerIDType) {
		CustomerIDType = customerIDType;
	}
	public String getCustomerIDNo() {
		return CustomerIDNo;
	}
	public void setCustomerIDNo(String customerIDNo) {
		CustomerIDNo = customerIDNo;
	}
	
}
