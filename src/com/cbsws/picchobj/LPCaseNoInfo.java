package com.cbsws.picchobj;

import com.cbsws.picchPub.xml.ctrl.xsch.PiccBaseXmlSch;

public class LPCaseNoInfo extends PiccBaseXmlSch{
	private static final long serialVersionUID = -1210790423707897845L;
	
	private String	CASENO;//核心理赔案件号
	private String	RGTSTATE;//核心理赔案件状态
	private String RGTDATE;//核心理赔案件受理日期(预留节点)
	private String RESERVE_DATA;//预留节点
	public String getCASENO() {
		return CASENO;
	}
	public void setCASENO(String cASENO) {
		CASENO = cASENO;
	}
	public String getRGTSTATE() {
		return RGTSTATE;
	}
	public void setRGTSTATE(String rGTSTATE) {
		RGTSTATE = rGTSTATE;
	}
	public String getRGTDATE() {
		return RGTDATE;
	}
	public void setRGTDATE(String rGTDATE) {
		RGTDATE = rGTDATE;
	}
	public String getRESERVE_DATA() {
		return RESERVE_DATA;
	}
	public void setRESERVE_DATA(String rESERVEDATA) {
		RESERVE_DATA = rESERVEDATA;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
